#!/usr/bin/perl

# Name:
#      apply_folders.pl
# 
# Description:
#      Create applyweb imap folders
#
# Written by:
#      Dale Moore


use strict;
use Mail::IMAPClient;
# use IO::Socket;
# use IO::Socket::SSL;
use Getopt::Long;

my $password_file = '/etc/not-backed-up/apply/password';
my $verbose = 0;
my $imaphost = 'imap.srv.cs.cmu.edu';
my $topfolder = 'Inbox';
my $folder = '2017';
my $user = 'applyweb';
# my $user = 'dales';
my @subfolders = (
    'Applyweb',
    'SCS',
    'ISR-EE',
    'CompBio',
    'RI',
    'ISRI',
    'SEI',
    'CNBC',
    'ICTI',
    'RI-SCHOLARS',
    'MSE',
    'MSE-Dist',
    'MSE-Campus',
    'VLIS',
    'PNC',
    'LT-IIS',
    'MITS',
    'MSIT-GM-Dist',
    'MSIT-eBiz',
    'MSIT-GM-Dist-INTL',
    'MSIT-GM-Dist-NATL',
    'MSIT-ITSM',
    'MSIT-ESE',
    'MSIT-Privacy',
    'CNBC-Grad-Training',
    'MSE-Portugal',
    'MSE-MSIT-Korea',
    'MS-HCII',
    'GM-CSE',
    'MSE-SEM',
    'RI-MS-RT-China',
    'Enterprise-Architecture-Fundamentals',
    'BIC',
    'REU-SE',
    'Statistics',
    'Advanced-Enterprise-Architecture',
    'COTS-Based-Integration',
    'Managing-Software-Outsourcing',
    'Systems-Integration',
    'Statistics-MS',
    'Challenge-Exam-EAF',
    'RI-MS-RT-Mexico',
    'RI-MS-RT-Dominican',
    'EA-Security',
    'Requirements-Engineering',
    'Software-Architecture',
    'Software-Project-Management',
    'RI-MS-RT-UK',
    'Security-for-Software-Engineers');

my @subsubfolders = (
    'Admission Password Reset',
    'Applicant Submission Reminder',
    'Application Submission',
    'Credit Card Processing',
    'New Student Account Confirmation',
    'Online Admissions Inquiry',
    'Online Application Edit Confirmation',
    'Password Reset Request',
    'Recommendation Reminder',
    'Recommendation Requested For');

GetOptions('passwdfile=s' => \$password_file,
	   'verbose+' => \$verbose,
	   'imaphost=s' => \$imaphost,
	   'folder=s' => \$folder,
	   'user=s' => \$user,
	   'subfolder=s' => \@subfolders,
           'subsubfolders=s' => \@subsubfolders);

sub createifnotexist {
    my ($imap,$f) = @_;
    if ( $imap->exists($f) ) {
	$verbose && print "Folder $f exists, no need to create\n";
    } elsif ( $imap->create($f) ) {
	$verbose && print "Folder $f created\n";
    }
    $imap->subscribe($f);
}


sub createfolder {
    open(PASSWD, $password_file) ||
	die "Cant open file $password_file";
    my $password = <PASSWD>;
    chomp($password);
    close(PASSWD);
    print "pw: $password\n";
    my $imap = new Mail::IMAPClient(Server => $imaphost,
				    User => $user,
				    Password => $password)
	|| die "Can't create new client for IMAP server $imaphost";

    $imap->starttls();


    foreach my $f ( "$topfolder.$folder" ) {
	createifnotexist($imap, $f);
	foreach my $sf ( map { "$f.$_" } @subfolders ) {
	    createifnotexist($imap, $sf);
	    foreach my $ssf ( map { "$sf.$_" } @subsubfolders ) {
		createifnotexist($imap, $ssf);
	    }
	}
    }
}

sub createsieve {
    print "require [\"fileinto\",\"reject\",\"vacation\",\"imapflags\",\"relational\",\"regex\",\"notify\"];\n";
    print "if header :matches \"X-Spam-Warning\" \"*\"\n";
    print "{\n";
    print "    fileinto \"INBOX.SPAM\";\n";
    print "}\n";

    foreach my $sf ( @subfolders ) {
	foreach my $ssf ( @subsubfolders ) {
	    print "elsif header :matches \"subject\" \"*[$sf:*:*] $ssf*\" {\n";
	    print "    fileinto \"INBOX.$folder.$sf.$ssf\";\n";
	    print "}\n";
	}
	print "elsif header :matches \"subject\" \"*[$sf\:\:]*\" {\n";
	print "    fileinto \"INBOX.$folder.$sf\";\n";
	print "}\n";
    }
    print "elsif header :matches \"subject\" \"*[::]*\" {\n";
    print "    fileinto \"INBOX.$folder.Applyweb\";\n";
    print "}\n";
}

createfolder;
createsieve;

exit 0;

