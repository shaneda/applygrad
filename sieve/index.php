
<? // include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; 
// include_once '../inc/specialCasesAdmin.inc.php';
?>
 
<?php

    $subfolders = array(
    'Admission Password Reset',
    'Applicant Submission Reminder',
    'Application Submission',
    'CMU Admissions Decision',  
    'Credit Card Processing',
    'New Student Account Confirmation',
    'Online Admissions Inquiry',
    'Online Application Edit Confirmation',
    'Password Reset Request',
    'Recommendation Reminder',
    'Recommendation Requested For');
    
    $special_cases = array(
        'MSIT-ESE' => 'jtlsmith+CCNotify@cs.cmu.edu',
        'ISR-EE' => 'elearn@andrew.cmu.edu',
        'VLIS' => 'jmlucas+CCNotify@cs.cmu.edu',
        'MSE-SEM' => 'jtlsmith+CCNotify@cs.cmu.edu',
        'MSE-Campus' => 'ens@cs.cmu.edu',
        'MSE-MSIT-Korea' => 'ens@cs.cmu.edu',
        'MITS' => 'ens@cs.cmu.edu',
        'MSE-Portugal' => 'ens@cs.cmu.edu',
        'MSE-Dist' => 'jtlsmith+CCNotify@cs.cmu.edu',
        'GM-CSE' => 'jtlsmith+CCNotify@cs.cmu.edu',
        'MSIT-SRM' => 'jtlsmith@cs.cmu.edu'
    );
    
    
    $current_year = 2014;
    
    $pretty_print = TRUE;
         
    function getAllDomains () {
        // all domains
        $sql = "SELECT name FROM domain WHERE name NOT LIKE '%archive%' ORDER BY name ASC";
    
        $result = mysql_query($sql) or die(mysql_error());
        $domains = array();
        while($row = mysql_fetch_array( $result )) {
            array_push ($domains, $row['name']);
        }
        return $domains;
    }
    
    function getExecEdDomains () {
        // all domains
        $sql = "select d.name from department 
                inner join lu_domain_department ldd on ldd.department_id = department.id
                inner join domain d on d.id = ldd.domain_id
                where department.oraclestring = 6210500000100021927036401";
    
        $result = mysql_query($sql) or die(mysql_error());
        $domains = array();
        while($row = mysql_fetch_array( $result )) {
            array_push ($domains, $row['name']);
        }
        return $domains;
    }
    
    function getPayingDomains () {
        //  domains with programs that have a fee
        $sql = "select distinct d.name from domain d
                inner join lu_domain_department ldd on ldd.domain_id =  d.id
                inner join lu_programs_departments lpd on lpd.department_id = ldd.department_id
                inner join programs p on p.id = lpd.program_id and p.baseprice > 0 
                where d.name not like '%archive%' and p.baseprice > 0
                order by d.name ASC";
    
        $result = mysql_query($sql) or die(mysql_error());
        $domains = array();
        while($row = mysql_fetch_array( $result )) {
            array_push ($domains, $row['name']);
        }
        $email_domains = array('ISR-EE');
        $eedomains = getExecEdDomains();
        foreach ($domains as $domain) {
            if (!in_array($domain, $eedomains)) {
                array_push($email_domains, $domain);
            }
        }
        return $email_domains;
    }
        
    function hierarchy ($folders, $subfolders)  {
        echo "<h2> Top Level Folders </h2>";
        foreach ($folders as $folder) {
             echo ($folder . "<br>");
        }
        echo "<h2>  Sublevel Folders </h2>";
        foreach ($subfolders as $folder) {
             echo ($folder . "<br>");
        }
        echo "<h2>  Desired Hierarchy under 2013  </h2>";
         foreach ($folders as $folder) {
             $redirect = is_special_case($folder);
             if ($redirect) {
                echo ($folder . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $redirect . "<br>"); 
             } else {
                echo ($folder . "<br>");
             }
             foreach ($subfolders as $sub) {
                 echo ("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      -  " . $sub . "<br>");
             }
         }
        
    }
    
    function print_header($pretty=FALSE) {
        echo ('require ["fileinto","reject","vacation","imapflags","relational","regex","notify"];');
        if ($pretty) {
            echo "<br />";
        } else {
            echo "&nbsp;";
        }
        echo ('if header :matches "X-Spam-Warning" "*" { fileinto "INBOX.SPAM"; }');
        if ($pretty) {
                echo "<br />";
            } else {
                echo "&nbsp;";
            }
    }
    
    function print_catch_garbage($pretty=FALSE) {
        echo ('elsif header :contains ["reply-to", "from"] "noreply" { fileinto "INBOX.noreply"; }');
        if ($pretty) {
                echo "<br />";
            } else {
                echo "&nbsp;";
            }
         echo ('elsif header :contains "from" "invitations@linkedin.com" { fileinto "INBOX.linkedin"; }');
        if ($pretty) {
                echo "<br />";
            } else {
                echo "&nbsp;";
            }
        echo ('elsif header :matches "X-LinkedIn-Class" "*" { fileinto "INBOX.linkedin"; }');
        if ($pretty) {
                echo "<br />";
            } else {
                echo "&nbsp;";
            }
    }
    
    
    function is_special_case ($domain) {
        global  $special_cases;
        if (array_key_exists($domain, $special_cases)) {
            return $special_cases[$domain];
        } else {
            return FALSE;
        }
    } 
    
    function print_credit_card ($folders, $redirects, $year, $pretty=FALSE) {
        echo ('elsif header :contains "subject" "Credit Card Processing" {');
        // process paying domains
        
        // get the doamins that use campus credit card processing
        $paying_domains = getPayingDomains();
        
        // loop over them and print the stanza
        $first_case = TRUE;   // use to determine to use if or else
        foreach ($folders as $folder) {
            // change exec ed domains to use ISR-EE mailbox
            
            // if this is the first case use if
             if ($first_case) {
                 echo ('if header :contains "to" "+' . $folder . '" {');
                 if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
                 $first_case = FALSE;
             } else {
                 echo ('elsif header :contains "to" "+' . $folder . '" {');
                 if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
             }
             if (in_array($folder, getExecEdDomains())) {
                echo ('fileinto "INBOX.' . $year . '.' . 'ISR-EE' . '.Credit Card Processing";');
             } else {
                 echo ('fileinto "INBOX.' . $year . '.' . $folder . '.Credit Card Processing";');
             }
             if ($pretty) {
                 echo ("<br />");
             } else {
                 echo ("&nbsp;");
             }
             // determine if a redirect clause is needed and print    
             $redirect = is_special_case($folder);
             if ($redirect) {
                echo ('redirect "' . $redirect . '";');
                if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 } 
             } 
            echo ('}');   // closes credit card section for domain
            if ($pretty) {
                         echo ("<br />");
                     } else {
                         echo ("&nbsp;");
                     }
            }       
           echo ('}');  // closes credit card section of sieve script 
           if ($pretty) {
                         echo ("<br />");
                     } else {
                         echo ("&nbsp;");
                     } 
    }
    
    function print_default_domain_clause ($domain, $year, $pretty=FALSE) {
        echo ('elsif header :matches "subject" "*[' . $domain . ':*:*]*" {');
        if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
        if (in_array($domain, getExecEdDomains())) {
                echo ('fileinto "INBOX.' . $year . '.' . 'ISR-EE";');
             } else {
                 echo ('fileinto "INBOX.' . $year . '.' . $domain . '";');
             }
        if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
        echo ('}');
        if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
    }
    
    function print_domain_stanza ($domain, $subfolders, $year, $pretty=FALSE) {
        foreach ($subfolders as $sub) {
            if ($sub == 'CMU Admissions Decision') {
                echo ('elsif header :matches "subject" ["*[' . $domain . ':*:*] ' . $sub . '*" , "*[' . $domain . ':*:*] ' . 'CMU SCS Admissions Decision' . '*"] {');
                
            }  else {
                echo ('elsif header :matches "subject" "*[' . $domain . ':*:*] ' . $sub . '*" {');
            }
            if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
            if (in_array($domain, getExecEdDomains())) {
                echo ('fileinto "INBOX.' . $year . '.' . 'ISR-EE' . '.' . $sub . '";');
             } else if ($domain == 'RI') {
                        echo ('fileinto "INBOX.' . $year . '.' . 'SCS' . '.' . $sub . '";');
                        } else {
                            echo ('fileinto "INBOX.'. $year . '.' . $domain . '.' . $sub . '";');
                        }
            if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
            if ($sub == 'Online Admissions Inquiry') {
                // determine if a redirect clause is needed and print    
                 $redirect = is_special_case($domain);
                 if ($redirect) {
                    echo ('redirect "' . $redirect . '";');
                    if ($pretty) {
                         echo ("<br />");
                     } else {
                         echo ("&nbsp;");
                     } 
                 }
            }
            echo ("stop;");
            if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
                
            echo ('}');
            if ($pretty) {
                     echo ("<br />");
                 } else {
                     echo ("&nbsp;");
                 }
            }
    }
    
    function print_catch_all ($year, $pretty=FALSE) {
        echo ('elsif header :matches "subject" "*[::]*" {');
        if ($pretty) {
             echo ("<br />");
         } else {
             echo ("&nbsp;");
         }
         echo ('fileinto "INBOX.' . $year . '.Applyweb";');
         if ($pretty) {
             echo ("<br />");
         } else {
             echo ("&nbsp;");
         }
         echo ('}');
         if ($pretty) {
             echo ("<br />");
         } else {
             echo ("&nbsp;");
         }
         echo ('elsif header :contains "to" ["applyweb", "applygrad"] {');
         if ($pretty) {
             echo ("<br />");
         } else {
             echo ("&nbsp;");
         }
         echo ('fileinto "INBOX.' . $year . '.Applyweb";');
         if ($pretty) {
             echo ("<br />");
         } else {
             echo ("&nbsp;");
         }
         echo ('}');
    }
    
    function print_all_domains ($subfolders, $year, $pretty=FALSE) {
        $domains = getAllDomains();
        foreach ($domains as $domain) {
            print_domain_stanza($domain, $subfolders, $year, $pretty);
            print_default_domain_clause($domain, $year, $pretty);
            }
    }
    
    $top_folders =  array_merge(array('Applyweb'), getAllDomains());
    print_header($pretty_print);
    print_catch_garbage($pretty_print);
    print_credit_card(getPayingDomains(), $special_cases, $current_year, $pretty_print);
    print_all_domains($subfolders, $current_year, $pretty_print);
    print_catch_all($current_year, $pretty_print);
    
 //   hierarchy($top_folders, $subfolders);
?>
