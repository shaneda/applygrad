<?php
  if ( !isset($_SESSION) ) {
    session_start();    
}

/*
* Error reporting level now handled in config files.
*/
//error_reporting(E_ALL);
//ini_set('display_errors','On');

include_once '../inc/config.php';
include_once '../inc/db_connect.php';

// PLB added redirect to HTTPS 12/9/09
if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' )
{ 
    $srvname = $_SERVER["SERVER_NAME"];
    $scrname = $_SERVER["SCRIPT_NAME"];
    header("Location: https://".$srvname.$scrname);
}

if (isset($_GET['d'])) {
    $departmentId =  $_GET['d'];
    if ($departmentId == 1) {
        $department = 'CSD';
    } else if ($departmentId == 2) {
                 $department = 'ML';
             }
    else {
        $department = -1;
    }
}

if (isset($_GET['yr'])) {
    $yearDesired =  $_GET['yr'];
    }

include_once '../webreports/inc/authorize.php';

$authenticated_user = $_SERVER['HTTP_EPPN'];

// Test cases 
//  CSD Admin
//$authenticated_user = 'deb@cs.cmu.edu';
//  ML Admin
//$authenticated_user = 'diane@cs.cmu.edu';
// ML student
// $authenticated_user = 'wbishop@cs.cmu.edu';
// CSD student
//$authenticated_user = 'adamblan@cs.cmu.edu';

include_once '../inc/webreportsDb.php';
$authFaculty = false;
if (strpos(strtoupper($authenticated_user), '@CS.CMU.EDU') !== false) {
    $authFaculty = true;
    } else if (strpos(strtoupper($authenticated_user), '@ANDREW.CMU.EDU') !== false) {
      	   $authFaculty = true;
    }
// $authFaculty = isFacultyOrAdmin($departmentId, $authenticated_user);
if (!$authFaculty) {
    die("You are not authorized to see this page.  Please contact the program administrator.");
} else {
     $semesterIn = 'Fall';
//          das_debug();

     $period = getPeriodInfo(getPeriodFromStartDate ($departmentId, $semesterIn, $yearDesired));

     $semester = $period['admission_term'];
     $year = $period['admission_year'];
     $defer_year = $year + 1;
}

  if ($departmentId == 1) {
            include_once '../webreports/inc/dbBlackfriday.php';
            include_once '../webreports/inc/dbConnectBF.php';
  
        } else {
             include_once '../webreports/inc/dbGsaudit.php';
             include_once '../webreports/inc/dbConnectBF.php';
        }
  
  $dbarray = array ($dbhost => (array ("dbuser" => $dbuser,
                                    "dbpassword" => $dbpassword,
                                    "dbname" => $dbname)));
    
    try {
        $bfhost = key($dbarray);
        $bfdb =  $dbarray[$dbhost]['dbname'];
        $bfdbuser = $dbarray[$dbhost]['dbuser'];
        $bfconnectionString = "mysql:host=" . $bfhost . ";dbname=" .  $bfdb;
        
        $bfdbconn = new PDO($bfconnectionString, $bfdbuser, $dbarray[$dbhost]['dbpassword'] );
    }
    catch(PDOException $e)
    {
    echo $e->getMessage();
    }                                
    
    if ($departmentId == 1) {        
  $sql = "select sd.student_id, CONCAT_WS(', ', u.last_name, u.first_name) as name, group_concat(si.institutes_name SEPARATOR ', ') as schools, 
CONCAT_WS(', ',sap.area_of_interest1, sap.area_of_interest2, sap.area_of_interest3) as interests,
sap.area_of_interest1, sap.area_of_interest2, sap.area_of_interest3,
sav.resume_filepath, sav.statement_of_purpose_filepath, sf.file_name 
from student_data sd
inner join user_role ur on ur.ur_id = sd.ur_id
inner join users u on u.user_id = ur.user_id
left outer join student_apps sa on sa.student_id = sd.student_id
left outer join student_app_vitals sav  on sav.app_id = sa.app_id
left outer join student_files sf on sf.student_id = sd.student_id and sf.file_type = 'photo'
left outer join student_institutes si on si.student_id = sd.student_id
left outer join student_app_programs sap on sap.app_id = sa.app_id
where sd.department = '" . $department . "' and sd.year = (select min(year) from student_data sd2 where sd2.status = 'Active' and sd2.department = '" . $department . "')
group by sd.student_id
order by name ASC";
      } else if  ($departmentId == 2) {
 $sql = "select sd.student_id, CONCAT_WS(', ', u.last_name, u.first_name) as name, group_concat(si.institutes_name SEPARATOR ', ') as schools,
 CONCAT_WS(', ',sap.area_of_interest1, sap.area_of_interest2, sap.area_of_interest3) as interests,
 sap.area_of_interest1, sap.area_of_interest2, sap.area_of_interest3,
 sav.resume_filepath, sav.statement_of_purpose_filepath, sf.file_name
 from student_data sd
 inner join user_role ur on ur.ur_id = sd.ur_id
 inner join users u on u.user_id = ur.user_id
 left outer join student_apps sa on sa.student_id = sd.student_id
 left outer join student_app_vitals sav  on sav.app_id = sa.app_id
 left outer join student_files sf on sf.student_id = sd.student_id and sf.file_type = 'photo'
 left outer join student_institutes si on si.student_id = sd.student_id
 inner join student_app_programs sap on sap.app_id = sa.app_id and sap.program = 'Ph.D. in Machine Learning'
 where sd.department = '" . $department . "' and sd.year = (select min(year) from student_data sd2 where sd2.status = 'Active'
 and sd2.department  = '" . $department . "')
  group by sd.student_id
  order by name ASC";
}

?>
 <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="Edit-Time-Data" href="file:///J|/academic/csd/grad/www/cmuonly/bios/NewStudents11_files/editdata.mso" />
    <title>
      PHD STUDENTS ENTERING <?=$department?> <?=$semester?> <?=$year?>
    </title>
    <link rel="themeData" href="file:///J|/academic/csd/grad/www/cmuonly/bios/NewStudents11_files/themedata.thmx" />
    <link rel="colorSchemeMapping" href="file:///J|/academic/csd/grad/www/cmuonly/bios/NewStudents11_files/colorschememapping.xml" />
    <style type="text/css">
/*<![CDATA[*/
    body {
    background-color: khaki;
    }
    :link { color: blue }
    :visited { color: purple }
    div.cleanuphtml-8 {text-align: center}
    a.cleanuphtml-7 {font-weight: bold}
    span.cleanuphtml-6 {text-decoration: underline}
    a.cleanuphtml-5 {text-decoration: underline}
    p.cleanuphtml-4 {text-align: center}
    h3.cleanuphtml-3 {text-align: center}
    hr.cleanuphtml-2 {text-align: center}
    h2.cleanuphtml-1 {text-align: center}
    /*]]>*/
    </style>
  <!-- Clean HTML generated by http://www.cleanuphtml.com/ -->
    <meta name="Generator" content="Cleanup HTML"></head>
  <body>
    <div class="WordSection1">
      <h2 class="cleanuphtml-1"></h2>
      <hr class="cleanuphtml-2" size="2" width="100%" />
      <h2 class="cleanuphtml-1"></h2>
      <h2 class="cleanuphtml-1">
        PHD STUDENTS ENTERING <?=$department?> <?=$semester?> <?=$year?> 
      </h2>
  <!---    <h3 class="cleanuphtml-3">
        Computer Science Department
      </h3>   -->
      <p class="cleanuphtml-4">
        Carnegie Mellon University
      </p>
      <div class="cleanuphtml-8">
        <table border="1" cellspacing="0" cellpadding="0" width="89%">
        <?php
            foreach ($bfdbconn->query($sql) as $row)
        {
	    if ($row['area_of_interest1'] != "") {
	       $interestList = $row['area_of_interest1'];
	    }
	    if ($row['area_of_interest2'] != "") {
	       $interestList .= ", " . $row['area_of_interest2'];
	    }
	    if ($row['area_of_interest3'] != "") {
               $interestList .= ", " . $row['area_of_interest3'];
	    }
			   
        ?>
          <tr>
            <td>
              <p>
                <a></a><b><?=$row['name']?></b>
              </p>
            </td>
            <td>
              <p class="cleanuphtml-4">
                <?=$row['schools']?>
              </p>
            </td>
            <td valign="top">
              <p class="cleanuphtml-4">
<!--                <?=$row['interests']?>  -->
		    <?=$interestList?>
              </p>
              <p class="cleanuphtml-4">
                 
              </p>
              <p class="cleanuphtml-4">
	      <?php
	      $photoFile = getStudentPicturePath($departmentId, $row['student_id']);
	      if ($departmentId == 1) {
	      ?>
		 <img src="<?=$photoFile?>" alt="no picture" name="_x0000_i1045" width="144" height="160" border="0" id="_x0000_i1045" alt="" />
	      <?php
	      } else if ($departmentId == 2) {
	      ?>
                <img src="<?=$photoFile?>" alt="no picture" name="_x0000_i1045" width="144" height="160" border="0" id="_x0000_i1045" alt="" />   
		<?php } ?>
              </p>
            </td>            
          </tr>
          <?php } ?>
          
        </table>
      </div>      
    </div>
  </body>
</html>
