<?php 

 // include_once '../inc/webreportsDb.php';

// Start a new session only if the including script 
// hasn't already started it. 
if ( !isset($_SESSION) ) {
    session_start();    
}

// PLB added redirect to HTTPS 12/9/09
if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' )
{ 
    $srvname = $_SERVER["SERVER_NAME"];
    $scrname = $_SERVER["SCRIPT_NAME"];
    header("Location: https://".$srvname.$scrname);
}

if ($_GET['unit']) {
    $department =  $_GET['unit'];
} else {
    $department = -1;
}

include_once '../webreports/inc/authorize.php';

if (isset ($_SERVER['HTTP_REMOTE_USER'])) {
    $authenticated_user = $_SERVER['HTTP_REMOTE_USER'];
} else if (isset ($_SERVER['REMOTE_USER']))  {
    $authenticated_user = $_SERVER['REMOTE_USER'];
} else if (isset ($_SESSION['userWebIso'])) {
    $authenticated_user =  $_SESSION['userWebIso'];
} else if (isset ($_SERVER['eppn'])) {
    $authenticated_user =  $_SERVER['eppn'];
} else if (isset ($_SERVER['HTTP_EPPN'])) {
    $authenticated_user =  $_SERVER['HTTP_EPPN'];
}
// DebugBreak();
// Test cases 
//  CSD Admin
// $authenticated_user = 'deb@cs.cmu.edu';
//  ML Admin
//$authenticated_user = 'diane@cs.cmu.edu';
// ML student
// $authenticated_user = 'wbishop@cs.cmu.edu';
// CSD student
//$authenticated_user = 'adamblan@cs.cmu.edu';

$isAuthorized = FALSE;
$authStudent = isStudent($department, $authenticated_user );
include_once './inc/webreportsDb.php';
$authFaculty = isFacultyOrAdmin($department, $authenticated_user);
if ($department == 6) {
    if ($authFaculty == "") {
        die("You are not authorized to see this page.  Please contact the program administrator.");
    } else {
        $isAuthorized = TRUE;
    }
} else if (($authStudent == "" && $authFaculty == "") // || $department == 1
    ) {
    die("You are not authorized to see this page.  Please contact the program administrator.");
} else {
    $isAuthorized = TRUE;
}

if ($isAuthorized) {
    
switch (intval($department)) {
    case 6:
        $department_name = getDepartmentName($department);

        $programs = getDepartmentPrograms($department);

        $test = date("Y");
        $semester = 'Fall';
        $year = $test;
        $period = getPeriodFromStartDate($department, $semester, $year);
        $perInfo = getPeriodInfo($period);
        $prevYear = $test - 1;
        $prevPeriod = getPeriodFromStartDate($department, $semester, $prevYear);
        $prevPeriodInfo =  getPeriodInfo($prevPeriod);
        $all_periods = array();
        $all_periods[] = $perInfo;
        $all_periods[] = $prevPeriodInfo;
        
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['prog'])) {
                $selectedProgId = $_POST['prog'];
            } else {
                $selectedProgId = 5;
            }
            if (isset($_POST['per1'])) {
                $selectedPeriodId = $_POST['per1'];
            } else {
                $selectedPeriodId = $period;
            }
            $period = getPeriodInfo($selectedPeriodId);
        } else {
            $period = getPeriodFromStartDate($department, 'Fall', date("Y"));
        } 
        break; 
    default:
        $department_name = getDepartmentName($department);

        $programs = getDepartmentPrograms($department);

        $test = date("Y");
        $semester = 'Fall';
        $year = $test;
        $period = getPeriodFromStartDate($department, $semester, $year);
        $perInfo = getPeriodInfo($period);
        $prevYear = $test - 1;
        $prevPeriod = getPeriodFromStartDate($department, $semester, $prevYear);
        $prevPeriodInfo =  getPeriodInfo($prevPeriod);
        $all_periods = array();
        $all_periods[] = $perInfo;
        $all_periods[] = $prevPeriodInfo;

        if (isset($_POST) && !empty($_POST)) {
            /*
            if (isset($_POST['prog'])) {
                $selectedProgId = $_POST['prog'];
            } else {
                $selectedProgId = 5;
            }
            */
            if (isset($_POST['per1'])) {
                $selectedPeriodId = $_POST['per1'];
            } else {
                $selectedPeriodId = $period;
            }
            $periodArray = getPeriodInfo($selectedPeriodId);
            $period = $periodArray['period_id'];
            $year = $periodArray['admission_year'];
        } else {
            $period = getPeriodFromStartDate($department, 'Fall', date("Y"));
        } 
        break; 
        /*
    default:   
        $department_name = getDepartmentName($department);
        $all_periods = getPeriodsFromDepartment($department);
        $periodArray = array();
        foreach ($all_periods as $aPeriod) {
            $periodArray[$aPeriod->getId()] = $aPeriod->getName(); 
        }
        DebugBreak();
        $period = getPeriodInfo(1047);
        $semester = "Fall";
        $year = 2016;
        $defer_year = $year + 1;
        $period = getPeriodFromStartDate($department, $semester, $year);
        $latestPeriod = getLatestOpenPeriod($all_periods);
        $period = getPeriodInfo($latestPeriod);
        if (isset($_POST)) {
            $selectedPeriodId = $_POST['per1'];
            $period = getPeriodInfo($selectedPeriodId);
        }  
        
        $semester = $period['admission_term'];
        $year = $period['admission_year'];
        $defer_year = $year + 1;
        $period = getPeriodFromStartDate($department, $semester, $year);
        break;
        */
}



?>

<TITLE>STUDENTS ADMITTED FOR <?php echo strtoupper($semester) . " " . $year; ?></TITLE>
<HTML><HEAD>
<TITLE>STUDENTS ADMITTED FOR <?php echo strtoupper($semester) . " " . $year; ?></TITLE>
<style type="text/css">
<!--
p.MsoNormal {
margin:0in;
margin-bottom:.0001pt;
font-size:12.0pt;
font-family:"Times New Roman";
}
.auto-style3 {
    margin-left: 120px;
}
.auto-style4 {
    margin-left: 40px;
}
.auto-style5 {
    margin-left: 80px;
}
.auto-style7 {
    text-align: center;
}
-->
</style>
</HEAD>
<?php
switch (intval($department)) {
    case 6:
        $defaultProgram = 5;
        if (isset($selectedProgId) || isset($selectedPeriodId)) {
            $dbApplicants = getAdmittedListBySingleProgram($selectedPeriodId, $selectedProgId, $department);
        } else {
                // $dbApplicants = getAdmittedList($period, $department);
                $dbApplicants = getAdmittedListBySingleProgram($period, $defaultProgram, $department);
        }
        break;
    default:
        $dbApplicants = getAdmittedList($period, $department);
        
        break;
} 
$nodecisionApps = $dbApplicants['nodecision'];        
$acceptedApps =  $dbApplicants['accept'];
$deferredApps =  $dbApplicants['defer'];
$declinedApps =  $dbApplicants['decline'];

?>

<BODY BGCOLOR="#f0e68c" TEXT="#21006b">
<form action="<?php echo $PHP_SELF;?>" method="post" name="form1" id="form1">
<?php if ($department == 6)  { ?>
Program: &nbsp;
<select name="prog" id="prog">
  <?php
    foreach ($programs as $prog) {
        if ($department == 6) {
            if ($prog['program_id'] == 5) {
                $prog['program_name'] = "Ph.D. in Language and Information Technologies";
            }
        }
        if (isset($selectedProgId) && $selectedProgId == $prog['program_id']) {
            ?>
            <option selected="selected" value=<?= $prog['program_id'] ?>><?= $prog['program_name'] ?></option>
        <?    
        } else {
            ?>
            <option value=<?= $prog['program_id'] ?>><?= $prog['program_name'] ?></option>
        <?}
    ?>
      
  <?php
    } ?>
</select> 
<?php } ?>
Period: &nbsp;
<select name="per1" id="per1">
  <?php
    foreach ($all_periods as $per) { 
        if (isset($selectedPeriodId) && $selectedPeriodId == $per['period_id']) {
        ?>
                <option selected="selected" value=<?= $per['period_id'] ?>><?= $per['umbrella_name']?></option>
        <?    
            } else {
                ?>
                    <option value=<?= $per['period_id'] ?>><?= $per['umbrella_name'] ?></option>
                <? }
        ?>
  <?php
       
    } ?>
</select>
 <input type="submit" value="Refresh" name="submit"> 
</form>
<CENTER><HR>
ADMITTED FOR <?php echo strtoupper($semester) . " " . $year; ?>
</CENTER>
<CENTER><H3><?php echo $department_name; ?></H3></CENTER>
<CENTER><h3>Carnegie Mellon University</H3></CENTER>
<HR>
<center>

<font color=red><B>THIS LIST IS NOT TO BE DISTRIBUTED OUTSIDE CARNEGIE MELLON UNIVERSITY</B></font>
<HR>
<P>
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%" >
<TR>
<TD align=left>
<A HREF="#coming">Definitely Coming to CMU</A>
<TD align=center><A HREF="#notcoming">Definitely Not Coming to CMU</A>
<TD align=right><A HREF="#pending">Decision Pending</A>
</table>
</center>
<HR>

<center>
<A Name="pending"></A>
<H3>INVITED STUDENTS</H3>

<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <TR>
    <TD align=center valign=center><B>STUDENT</B>
    <TD align=center valign=center><B>FROM</B>
    <TD align=center valign=center><B>AREA</B>
    <?php if ($department != 6) {  ?>    
    <TD align=center valign=center><B>FACULTY CONTACT</B>
    <TD align=center valign=center><B>STUDENT CONTACT</B>
    <?php } ?>
    <td>&nbsp;</td>
      <?php
      foreach ($nodecisionApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
    <? if ($department != 6) {  ?>
    <td align="center" valign="middle"><?php echo $app['faccontact']; ?></td>
    <td align="center" valign="middle"><?php echo $app['stucontact']; ?></td>
    <?php } ?>
  </tr>
    <?php  }  ?> 
 
</table>

<p>&nbsp;</p>
<p><A Name="coming"></A>
</p>
<H3>DEFINITELY COMING:</H3>
<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <TR>
    <TD width="323" align=center valign=center><B>STUDENT</B>
    <TD width="371" align=center valign=center><B>FROM</B>
    <TD width="282" align=center valign=center><B>AREA</B>
  </TR>
  <?php
      foreach ($acceptedApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
  </tr>
    <?php  }  ?>  
           
  </table>
<p>&nbsp;</p>
<H3>DEFERRED TO <?php echo strtoupper($semester) . " " . $defer_year; ?>:</H3>
<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <TR>
    <TD align=center valign=center><B>STUDENT</B>
    <TD align=center valign=center><B>FROM</B>
    <TD align=center valign=center><B>AREA</B>
  </TR>
  <?php
      foreach ($deferredApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
  </tr>
    <?php  }  ?>
</TABLE>
<p>&nbsp;</p>
<p><A Name="notcoming"></A></p>
<H3>DEFINITELY NOT COMING:</H3>
<TABLE WIDTH="100%" BORDER=1 CELLPADDING=1 CELLSPACING=0 >
  <TR>
    <TD align=center valign=center style="width: 145px"><B>STUDENT</B></TD>
    <?php 
    if ($department == 1) {   ?>
         <TD width="255" align=center valign=center><B>AREA</B></TD>
         <?php
    }
        ?>
    <TD width="164" align=center valign=center style="width: 317px">
    <p class="auto-style7"><B>FROM</B>
    </p> </TD>
    <TD width="255" align=center valign=center><B>GOING TO</B></TD>
    <?php 
    if ($department == 2) {   ?>
         <TD width="255" align=center valign=center><B>REASONS</B></TD>
         <?php
    }
        ?>
  </TR>
    <?php
      foreach ($declinedApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <?php 
    if ($department == 1) {   ?>
         <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
         <?php
    }
        ?>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['other_choice_location']; ?></td>
    <?php 
    if ($department == 2) {   ?>
         <td align="center" valign="middle"><?php echo $app['decision_reasons']; ?></td>
         <?php
    }
        ?>
  </tr>
    <?php  }  ?>
  </TABLE>
 

  </BODY>  
</HTML>

<?php } ?>





