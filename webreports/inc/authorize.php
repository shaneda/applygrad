<?php
    function isStudent ($departmentId, $userid) {
        if ($departmentId == 1) {
            include_once '../webreports/inc/dbBlackfriday.php';
            include_once '../webreports/inc/dbConnectBF.php';
            $department = 'CSD';
        } else {
             include_once '../webreports/inc/dbGsaudit.php';
             include_once '../webreports/inc/dbConnectBF.php';
             if ($departmentId == 2) {
                 $department = 'ML';
             } else {
                 $department = -1;
             }
        }
        $user = "";
	if ($department == 'ML'){
	     $sql = "select u.username
            from users u
            inner join user_role ur on ur.user_id = u.user_id
            inner join student_data sd on sd.ur_id = ur.ur_id 
	    and sd.status IN ('Active', 'LOA)
            and sd.department = '" . $department .
	                "' where UPPER(u.username) = UPPER('" . $userid . "')";
	} else {
	        $sql = "select u.username
		 from users u
            	 inner join user_role ur on ur.user_id = u.user_id
            	 inner join student_data sd on sd.ur_id = ur.ur_id and sd.status = 'Active'
            	 and sd.department = '" . $department .
            	 "' where UPPER(u.username) = UPPER('" . $userid . "')";
	}
            
        $result =  mysql_query($sql) or die(mysql_error());
        
        while ($row = mysql_fetch_array($result)) {
            $user =  $row['username'];
        }
            
          mysql_close($bfconn);
          return $user;
    }
    
    function isFacultyOrAdmin ($departmentId, $userid) {
        
        $user = "";
        $sql = "select ras.remote_auth_string
            from users_remote_auth_string ras
            inner join users u on u.id = ras.users_id
            inner join lu_users_usertypes luu on luu.user_id = u.id and (luu.usertype_id = 3 or luu.usertype_id = 1)
            inner join lu_user_department lud on lud.user_id = luu.id and lud.department_id = " . $departmentId .
            " where UPPER(ras.remote_auth_string) = UPPER('" . $userid ."')";
        $result =  mysql_query($sql) or die(mysql_error());
        
        while ($row = mysql_fetch_array($result)) {
            $user =  $row['remote_auth_string'];
        }
        
            
          return $user;
    
    } 

     function getStudentPicturePath($departmentId, $studentId) {
         if ($departmentId == 1) {
	    include_once '../webreports/inc/dbBlackfriday.php';
	    include_once '../webreports/inc/dbConnectBF.php';
	    $department = 'CSD';
	  } else {
	           include_once '../webreports/inc/dbGsaudit.php';
	           include_once '../webreports/inc/dbConnectBF.php';
	           if ($departmentId == 2) {
	              $department = 'ML';
	              } else {
	                       $department = -1;
	           }
	        }
	    $user = "";
	    $sql = "select sd.guid, u.last_name
                    from student_data sd
                    inner join user_role ur on ur.ur_id = sd.ur_id
                    inner join users u on u.user_id = ur.user_id
                    where sd.student_id = " . $studentId;

        $result =  mysql_query($sql) or die(mysql_error());
        $basepath = "";
        while ($row = mysql_fetch_array($result)) {
	     $initial = strtoupper(substr($row['last_name'], 0, 1));
	     if ($department == 'CSD') {
                 $basepath =  "../bfdata/{$department}/record/$initial/{$row['guid']}/photo.gif";
            } else if ($department == 'ML') {
	              $basepath =  "../gsdata/{$department}/record/$initial/{$row['guid']}/photo.gif";
		      }
         }
          mysql_close($bfconn);
	  return $basepath;
		    
}
?>
