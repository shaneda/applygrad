<?php
include_once '../inc/config.php'; 
include_once '../inc/db_connect.php';

function getDepartmentName ($id) {
    $sql = "select name from department where id = " . $id;
    
    $result =  mysql_query($sql) or die(mysql_error());
    
    while ($row = mysql_fetch_array($result)) {
        $dept =  $row['name'];
    }
    return $dept;
    
}

function getPeriodFromStartDate ($department, $semester, $year) {
    das_debug();
    $sql = "select p.period_id, p.start_date, p.end_date 
from period_program pp
inner join programs_unit on programs_unit.unit_id = pp.unit_id
inner join lu_programs_departments lpd on lpd.program_id = programs_unit.programs_id and lpd.department_id = " . $department
. " inner join unit u on u.unit_id = pp.unit_id
inner join unit parent_unit on parent_unit.unit_id = u.parent_unit_id
left outer join unit ppparent_unit on ppparent_unit.unit_id = parent_unit.parent_unit_id
inner join period p on p.period_id = pp.period_id
inner join period_umbrella pu on pu.period_id = p.period_id
where pu.admission_term = '" . $semester . "' and pu.admission_year = " . $year .
" group by period_id";
das_debug();
    $result =  mysql_query($sql) or die(mysql_error());
    
    while ($row = mysql_fetch_array($result)) {
        $period =  $row['period_id'];
    }
    return $period;
}

function getPeriodInfo ($period) {
    $infoQuery = "select * from period_umbrella p where p.period_id = " . $period;
    $result =  mysql_query($infoQuery) or die(mysql_error());
    
    while ($row = mysql_fetch_array($result)) {
        $info = $row;
    }
    return $info;
    
}

function getAdmittedList ($period, $department) {
    $allAdmittedQuery = "select a.id, concat(u.lastname, ', ', u.firstname) as name, lap.faccontact, lap.stucontact, group_concat(DISTINCT i.name) as areas, group_concat(DISTINCT ins.name) as schools, 
            sd.decision, sd.other_choice_location, sd.decision_reasons
            from application a
            inner join period_application pa on pa.application_id = a.id and pa.period_id = " . $period  .
            " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
            inner join lu_programs_departments lpd on lpd.program_id = lap.program_id and lpd.department_id = " . $department . 
            " left outer join student_decision sd on sd.application_id = lap.application_id and lpd.program_id = sd.program_id
            left outer join lu_application_interest lai on lai.app_program_id = lap.id
            left outer join interest i on i.id = lai.interest_id
            left outer join usersinst ui on ui.application_id = lap.application_id
            left outer join institutes ins on ins.id = ui.institute_id
            inner join lu_users_usertypes luu on luu.id = a.user_id
            inner join users u on u.id = luu.user_id
            group by lai.app_program_id, ui.application_id ";
            // deferrals
            if ($department == 1) {
                $deferrals = "";
                if ($period == 1402) {
                    $deferrals = "(69041, 68323, 50608, 67485, 67535, 62133, 75996)";
                } else if ($period == 1563) {
                      $deferrals = "(79948, 82306)";
                }  
                if ($deferrals != "") {
                    $allAdmittedQuery .= " UNION 
                    select a.id, concat(u.lastname, ', ', u.firstname) as name, lap.faccontact, lap.stucontact, group_concat(DISTINCT i.name) as areas, group_concat(DISTINCT ins.name) as schools, 
                    sd.decision, sd.other_choice_location, sd.decision_reasons
                    from application a
                    inner join period_application pa on pa.application_id = a.id and pa.application_id IN " 
                    . $deferrals 
                    . " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
                    inner join lu_programs_departments lpd on lpd.program_id = lap.program_id and lpd.department_id = 1 left outer join student_decision sd on sd.application_id = lap.application_id and lpd.program_id = sd.program_id
                    left outer join lu_application_interest lai on lai.app_program_id = lap.id
                    left outer join interest i on i.id = lai.interest_id
                    left outer join usersinst ui on ui.application_id = lap.application_id
                    left outer join institutes ins on ins.id = ui.institute_id
                    inner join lu_users_usertypes luu on luu.id = a.user_id
                    inner join users u on u.id = luu.user_id
                    group by lai.app_program_id, ui.application_id ";
                }
            }
            $allAdmittedQuery .= " order by name";
       
    $allResults = mysql_query($allAdmittedQuery) or die(mysql_error());
    
    $noDecision = array();
    $accept = array();
    $defer = array();
    $decline = array();
    
    while ($row = mysql_fetch_array($allResults)) {
        switch ($row['decision']) {
            case 'accept':
                $accept = array_merge($accept, array($row));
                break;
            case 'defer':
                $defer = array_merge($defer, array($row));
                break;
            case 'decline':
                $decline = array_merge($decline, array($row));
                break;
            default:
                $noDecision = array_merge($noDecision, array($row));
                break;
        }
    }
    
    $allResultsArray = array('nodecision'=>$noDecision);
    $test1 = array('accept'=>$accept);
    $test2 = array('defer'=>$defer);
    $test3 = array('decline'=>$decline);
    $allResultsArray =  $allResultsArray + $test1 + $test2 + $test3;
    return $allResultsArray;   
}

function getAdmittedListBySingleProgram($periodId, $programId, $departmentId) {
    if ($programId == 5 || $programId == 6) {
       $inClause = " IN (5, 6) "; 
    } else {
        $inClause = " IN (" . $programId . ") ";
    }
    if ($programId != 100035) {
        $allAdmittedQuery = "select a.id, concat(u.lastname, ', ', u.firstname) as name, lap.faccontact, lap.stucontact, group_concat(DISTINCT i.name) as areas, group_concat(DISTINCT ins.name) as schools, 
                sd.decision, sd.other_choice_location, sd.decision_reasons
                from application a
                inner join period_application pa on pa.application_id = a.id and pa.period_id = " . $periodId  .
         /*       " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
                and lap.program_id = " .  $programId .
                */
                " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
                and lap.program_id " . $inClause .
                " inner join application_decision ad on ad.application_id = lap.application_id and ad.admission_program_id = " . $programId .
                " inner join lu_programs_departments lpd on lpd.program_id = lap.program_id and lpd.department_id = " . $departmentId . 
                " left outer join student_decision sd on sd.application_id = lap.application_id and lpd.program_id = sd.program_id
                left outer join lu_application_interest lai on lai.app_program_id = lap.id
                left outer join interest i on i.id = lai.interest_id
                left outer join usersinst ui on ui.application_id = lap.application_id
                left outer join institutes ins on ins.id = ui.institute_id
                inner join lu_users_usertypes luu on luu.id = a.user_id
                inner join users u on u.id = luu.user_id
                group by lai.app_program_id, ui.application_id
                order by name";
    } else {
        $departmentId = 83;
        $allAdmittedQuery = "select a.id, concat(u.lastname, ', ', u.firstname) as name, lap.faccontact, lap.stucontact, group_concat(DISTINCT i.name) as areas, group_concat(DISTINCT ins.name) as schools, 
                sd.decision, sd.other_choice_location, sd.decision_reasons
                from application a
                inner join period_application pa on pa.application_id = a.id and pa.period_id = " . $periodId  .
         /*       " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
                and lap.program_id = " .  $programId .
                */
                " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
                and lap.program_id " . $inClause .
                " left outer join application_decision ad on ad.application_id = lap.application_id and ad.admission_program_id = " . $programId .
                " inner join lu_programs_departments lpd on lpd.program_id = lap.program_id and lpd.department_id = " . $departmentId . 
                " left outer join student_decision sd on sd.application_id = lap.application_id and lpd.program_id = sd.program_id
                left outer join lu_application_interest lai on lai.app_program_id = lap.id
                left outer join interest i on i.id = lai.interest_id
                left outer join usersinst ui on ui.application_id = lap.application_id
                left outer join institutes ins on ins.id = ui.institute_id
                inner join lu_users_usertypes luu on luu.id = a.user_id
                inner join users u on u.id = luu.user_id
                group by lai.app_program_id, ui.application_id
                order by name";
        
    }
            
    $allResults = mysql_query($allAdmittedQuery) or die(mysql_error());
    
    $noDecision = array();
    $accept = array();
    $defer = array();
    $decline = array();
    
    while ($row = mysql_fetch_array($allResults)) {
        switch ($row['decision']) {
            case 'accept':
                $accept = array_merge($accept, array($row));
                break;
            case 'defer':
                $defer = array_merge($defer, array($row));
                break;
            case 'decline':
                $decline = array_merge($decline, array($row));
                break;
            default:
                $noDecision = array_merge($noDecision, array($row));
                break;
        }
    }
    
    $allResultsArray = array('nodecision'=>$noDecision);
    $test1 = array('accept'=>$accept);
    $test2 = array('defer'=>$defer);
    $test3 = array('decline'=>$decline);
    $allResultsArray =  $allResultsArray + $test1 + $test2 + $test3;
    return $allResultsArray;
    
}

function getDepartmentPrograms($deptId) {
    $programs = array();
    $departmentCheck = " IN (" . $deptId;
    if ($deptId == 6) {
        $departmentCheck .= ", 83";
    }
    $departmentCheck .= ") ";
    $progSql = '
    select lpd.program_id, concat(d.name, " ", linkword, " ", fs.name) as program_name
    from lu_programs_departments lpd
    inner join programs p on p.id = lpd.program_id
    inner join fieldsofstudy fs on fs.id = p.fieldofstudy_id
    inner join degree d on d.id = p.degree_id
    where lpd.department_id' .  $departmentCheck .
    'order by lpd.program_id';
    $progResults = mysql_query($progSql) or die(mysql_error());
    while ($row = mysql_fetch_array($progResults)) {
        $programs[] = $row;
    }   
    return $programs;
}

function getPeriodsFromDepartment($deptId) {
    DebugBreak()  ;
    return array(1047);
}
?>
