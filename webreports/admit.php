<?php 
// Start a new session only if the including script 
// hasn't already started it. 
if ( !isset($_SESSION) ) {
    session_start();    
}

/*
* Error reporting level now handled in config files.
*/
//error_reporting(E_ALL);
//ini_set('display_errors','On');

include_once '../inc/config.php'; 
include_once '../inc/db_connect.php';

// PLB added redirect to HTTPS 12/9/09
if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' )
{ 
    $srvname = $_SERVER["SERVER_NAME"];
    $scrname = $_SERVER["SCRIPT_NAME"];
    header("Location: https://".$srvname.$scrname);
}

function getPeriodInfo ($period) {
    $infoQuery = "select * from period_umbrella p where p.period_id = " . $period;
    $result =  mysql_query($infoQuery) or die(mysql_error());
    
    while ($row = mysql_fetch_array($result)) {
        $info = $row;
    }
    return $info;
    
}

function getAdmittedList ($period, $department) {
    $allAdmittedQuery = "select a.id, concat(u.lastname, ', ', u.firstname) as name, lap.faccontact, lap.stucontact, group_concat(DISTINCT i.name) as areas, group_concat(DISTINCT ins.name) as schools, 
            sd.decision, sd.other_choice_location
            from application a
            inner join period_application pa on pa.application_id = a.id and pa.period_id = " . $period  .
            " inner join lu_application_programs lap on lap.application_id = pa.application_id and lap.admission_status = 2
            inner join lu_programs_departments lpd on lpd.program_id = lap.program_id and lpd.department_id = " . $department . 
            " left outer join student_decision sd on sd.application_id = lap.application_id and lpd.program_id = sd.program_id
            left outer join lu_application_interest lai on lai.app_program_id = lap.id
            inner join interest i on i.id = lai.interest_id
            inner join usersinst ui on ui.application_id = lap.application_id
            inner join institutes ins on ins.id = ui.institute_id
            inner join lu_users_usertypes luu on luu.id = a.user_id
            inner join users u on u.id = luu.user_id
            group by lai.app_program_id, ui.application_id
            order by name";
    $allResults = mysql_query($allAdmittedQuery) or die(mysql_error());
    
    $noDecision = array();
    $accept = array();
    $defer = array();
    $decline = array();
    
    while ($row = mysql_fetch_array($allResults)) {
        switch ($row['decision']) {
            case 'accept':
                $accept = array_merge($accept, array($row));
                break;
            case 'defer':
                $defer = array_merge($defer, array($row));
                break;
            case 'decline':
                $decline = array_merge($decline, array($row));
                break;
            default:
                $noDecision = array_merge($noDecision, array($row));
                break;
        }
    }
    
    $allResultsArray = array('nodecision'=>$noDecision);
    $test1 = array('accept'=>$accept);
    $test2 = array('defer'=>$defer);
    $test3 = array('decline'=>$decline);
    $allResultsArray =  $allResultsArray + $test1 + $test2 + $test3;
    return $allResultsArray;   
}

$period = getPeriodInfo(495);
$semester = $period['admission_term'];
$year = $period['admission_year'];
$defer_year = $year + 1;
?>
<HTML>
<HEAD>
<TITLE>STUDENTS ADMITTED FOR <?php echo strtoupper($semester) . " " . $year; ?></TITLE>
<style type="text/css">
<!--
p.MsoNormal {
margin:0in;
margin-bottom:.0001pt;
font-size:12.0pt;
font-family:"Times New Roman";
}
.auto-style3 {
    margin-left: 120px;
}
.auto-style4 {
    margin-left: 40px;
}
.auto-style5 {
    margin-left: 80px;
}
.auto-style7 {
    text-align: center;
}
-->
</style>
</HEAD>
<?php


$dbApplicants = getAdmittedList(495, 1);
$nodecisionApps = $dbApplicants['nodecision'];        
$acceptedApps =  $dbApplicants['accept'];
$deferredApps =  $dbApplicants['defer'];
$declinedApps =  $dbApplicants['decline'];

?>

<BODY BGCOLOR="#f0e68c" TEXT="#21006b">
<CENTER><HR>
ADMITTED FOR <?php echo strtoupper($semester) . " " . $year; ?>
</CENTER>
<CENTER><H3>Computer Science Department</H3></CENTER>
<CENTER><h3>Carnegie Mellon University</H3></CENTER>
<HR>
<center>

<font color=red><B>THIS LIST IS NOT TO BE DISTRIBUTED OUTSIDE CARNEGIE MELLON UNIVERSITY</B></font>
<HR>
<P>
</center>
<HR>
<table BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <tr>
    <td align="center" valign="middle"><strong>STUDENT</strong></td>
    <td align="center" valign="middle"><strong>FROM</strong></td>
    <td align="center" valign="middle"><strong>AREA</strong></td>
  </tr>
  <?php
      foreach ($acceptedApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
  </tr>
    <?php  }  ?>
  
</table>
<p>&nbsp;</p>
<H3>DEFERRED TO <?php echo strtoupper($semester) . " " . $defer_year; ?>:</H3>
<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <TR>
    <TD align=center valign="middle"><B>STUDENT</B>
    <TD align=center valign="middle"><B>FROM</B>
    <TD align=center valign="middle"><B>AREA</B>
  </TR>
  <?php
      foreach ($deferredApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
  </tr>
    <?php  }  ?>   
  </table>
  <p>&nbsp;</p>
<H3>NO DECISION TO <?php echo strtoupper($semester) . " " . $year; ?>:</H3>
<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <TR>
    <TD align=center valign="middle"><B>STUDENT</B>
    <TD align=center valign="middle"><B>FROM</B>
    <TD align=center valign="middle"><B>AREA</B>
  </TR>
  <?php
      foreach ($nodecisionApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
  </tr>
    <?php  }  ?>  
  </table>
  <p>&nbsp;</p>
<H3>DECLINED TO <?php echo strtoupper($semester) . " " . $year; ?>:</H3>
<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH="100%" >
  <TR>
    <TD align=center valign="middle"><B>STUDENT</B>
    <TD align=center valign="middle"><B>FROM</B>
    <TD align=center valign="middle"><B>AREA</B>
  </TR>
  <?php
      foreach ($declinedApps as $app) {
          ?>
  <tr>
    <td align="center" valign="middle"><?php echo $app['name']; ?></td>
    <td align="center" valign="middle"><p><?php echo $app['schools']; ?></p></td>
    <td align="center" valign="middle"><?php echo $app['areas']; ?></td>
  </tr>
    <?php  }  ?>
    
  </table>

  </BODY>
</HTML>
