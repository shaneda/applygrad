<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<!-- <meta HTTP-EQUIV="REFRESH" content="0; url=http://www.newdesign.cs.cmu.edu/error/error.html"> -->
 
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>

<head>
<? include_once '../inc/functions.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}
?>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_MS-HCII.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/app2.css" type="text/css">
<link rel="stylesheet" href="../css/MHCI_style.css" type="text/css">

<!-- InstanceBeginEditable name="head" -->
<?
$id = -1;
$err = "";
$resName = "";
$resSize = 0;
$resModDate = "";

$stateName = "";
$stateSize = "";
$stateModDate = "";
$resumeFileId = -1;
$statementFileId = -1;
  
 //RETRIEVE USER INFORMATION
$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=2";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$resName  = "resume.".$row['extension'];
	$resSize  = $row['size'];
	$resumeFileId = $row['id'];
	$resModDate = $row['moddate'];
} 

$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=4";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$stateName  = "statement.".$row['extension'];
	$stateSize  = $row['size'];
	$statementFileId = $row['id'];
	$stateModDate = $row['moddate'];
} 

if($resModDate == "" || $stateModDate == "")
{
	//updateReqComplete("resume.php", 0,1);
}
  
?>
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../app2.css" type="text/css" />
<script type="text/javascript" src="../inc/scripts.js"></script>
</head>
        <body style="margin: 0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data" ><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" style="height: 400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include './prereqsNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			<!-- InstanceBeginEditable name="EditNav" -->
            <tr>
            <td>
                <table>
                <colgroup>
                <col width=80% style=text-align:left;>
                <col width=20% style=text-align:right;>
                </colgroup>
                <tr>
            <td class=tblItem>
                
            </td>
            
            <td style="text-align:right; width:130px">    
         
                <? 
                if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                ){ ?>
                    <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                    <!-- DAS Removed - <? echo $sesEmail;?>   -->
                    <? } else
                                {
                                    if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                                    && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                                    && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                                    && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                                    && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                                    {
                                    //session_unset();
                                    //destroySession();
                                    //header("Location: index.php");
                                ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                                    }
                                }
             ?>
            </td>
            <tr>
            </table>
            </td>
            </tr>
            <!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">MHCI Placeout Opportunity</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" --><span class="tblItem">
			<?
$domainid = 37;
$qs = "?id=1&t=2";

if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='Placeout Intro' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}


// Function, variables, calls for displaying summary info.
function getStatusMessage($studentId, $summaryDataStatusArray, $prereqType) {
    $statusMessage = "Not yet begun, please read definition below"; 
       
    if ( isset($summaryDataStatusArray[$studentId][$prereqType]['reviewer_status']) && $summaryDataStatusArray[$studentId][$prereqType]['reviewer_status'] != "") {

        $reviewerStatus = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_status'];
        $reviewerExplanation = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_explanation'];
        $reviewerTimestamp = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_timestamp'];   
        $statusMessage = $reviewerStatus . " " . $reviewerExplanation . " " . $reviewerTimestamp;        
            
    } elseif  (isset($summaryDataStatusArray[$studentId][$prereqType]['student_status']) && $summaryDataStatusArray[$studentId][$prereqType]['student_status'] != "" ) { 
              $statusMessage =  $summaryDataStatusArray[$studentId][$prereqType]['student_status'] . " " . $summaryDataStatusArray[$studentId][$prereqType]['student_timestamp'];
    }
    
    return $statusMessage;
}

include "../inc/mhci_prereqsSummaryData.class.php";

$studentId = $_SESSION['userid'];
$summaryData = new MHCI_PrereqsSummaryData();
$statusArray = $summaryData->getStatus($studentId);

?>


<br>
<span class="errorSubTitle"><?=$err;?></span>

<hr size="1" noshade color="#990000">

            
            Your current placeout status:
            <br><br>
            <table>
            <!---    Hide the design stuff
                <tr>
                    <td class="tblItem" style="text-align:left; width:370px">
                        <b>Design</b>: <?php echo getStatusMessage($studentId, $statusArray, "design"); ?>
                    </td>
                    <td align="right" style="text-align:right; width:330px">
                        <input class="tblItem" name="btnUpload" value="Discuss Design" type="button" onClick="parent.location='mhci_prereqs_design.php<?=$qs;?>'"> 
                    </td>
                </tr>
            --->
            <tr>
                <td class="tblItem" style="text-align:left; width:370px">
                    <b>Programming</b>: <?php echo getStatusMessage($studentId, $statusArray, "programming"); ?>
                </td>
                <td align'"right" style="text-align:right; width:330px">
                    <input align="right" class="tblItem" name="btnUpload" value="Discuss Programming" type="button" onClick="parent.location='mhci_prereqs_programming.php<?=$qs;?>'">
                </td>
            </tr>
            <!---    Hide the statistics stuff
            <tr>
                <td class="tblItem" style="text-align:left; width:370px">
                    <b>Statistics</b>: <?php echo getStatusMessage($studentId, $statusArray, "statistics"); ?>
                </td>
                <td align="right" style="text-align:right; width:330px">
                    <input align="right" class="tblItem" name="btnUpload" value="Discuss Statistics" type="button" onClick="parent.location='mhci_prereqs_statistics.php<?=$qs;?>'">
                </td>
            </tr>
            
            </table>
            <hr size="1" noshade color="#990000">
            
			<? 
			$qs = "?id=1&t=2";
			?>

            
            
            </span>
 <?           $sql = "select content from content where name='Placeout Intro2' and domain_id=".$domainid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}
?>

                
            <hr size="1" noshade color="#990000">
            
            </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			
            <!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd -->
</html>
