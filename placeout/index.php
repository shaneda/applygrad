<html>
<!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<!--   <meta HTTP-EQUIV="REFRESH" content="0; url=https://applygrad.cs.cmu.edu/under_construction.html">      --->
<?php 
 /*
* If you haven't been there yet, go to checkPeriod.php
* to set $_SESSION['allow_edit'], etc. from the current period.
*/

/* if ( !( isset($_GET['periodChecked']) && $_GET['periodChecked'] == 1 ) 
    && isset($_GET['domain']) ) 
{
    header("Location: checkPeriod.php?domain=" . $_GET['domain']);
    exit;
}
*/
?>

<? include_once '../inc/config.php';?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php';
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once '../classes/DB_Applyweb/class.DB_Unit.php';
include_once '../classes/DB_Applyweb/class.DB_Period.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include_once '../classes/class.Period.php';
include_once '../classes/class.Unit.php'; 
include './classes/DB_PlaceOut_User.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_MS-HCII.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$accounts = array();
$recommender = false;
//session_unset($_SESSION['email']);
if(isset($_GET["l"]))
{
    $level = intval(htmlspecialchars($_GET["l"]));
    if($level != 1 || $level != 2)
    {
        //$err .= "Invalid domain request.<br>";
        $_SESSION['level'] = 2;
    }else
    {
        $_SESSION['level'] = $level;
    }
    
}

if(!isset($_POST['btnSubmit']) && !isset($_POST['btnLogin']))
{
    $_SESSION['firstname'] = "";
    $_SESSION['lastname'] = "";
    $_SESSION['userid'] = -1;
    $_SESSION['usermasterid'] = -1;
    $_SESSION['appid'] = -1;
    $_SESSION['email'] = "";
    
}



//ALLOW ADMIN TO ASSUME ROLE OF APPLICANT
if(isset($_GET['a']) && isset($_GET['uid']) && ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1))
{
    $_SESSION['appid'] = -1;
    $_SESSION['usermasterid'] = -1;
    $_SESSION['userid'] = -1;
    $_SESSION['firstname'] = "";
    $_SESSION['lastname'] = "";
    $_SESSION['email'] = "";
    $_SESSION['usertypeid'] = -1;
    
    $guid = addslashes(htmlspecialchars($_GET['uid']));
    //LOOKUP USER BY GUID
    $sql = $sql = "select users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email
        from users 
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        where guid = '".htmlspecialchars($guid)."' and usertypes.id=5";
        $result = mysql_query($sql) or die(mysql_error());
        $doLogin = false;
        while($row = mysql_fetch_array( $result )) 
        {
        
            // PLB added check for applicationId param 
            // to enable multiple applications per user 9/22/09.
            if ( isset($_GET['applicationId']) ) {
                $_SESSION['appid'] = $_GET['applicationId'];    
            }
            
            $_SESSION['usermasterid'] = $row['id'];
            $_SESSION['userid'] = $row['uid'];
            $_SESSION['firstname'] = $row['firstname'];
            $_SESSION['lastname'] = $row['lastname'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['usertypeid'] = $row['typeid'];
            $arr = array();
            //array_push($arr,$row['uid']);
            array_push($arr,$row['typeid']);
            array_push($arr,$row['typename']);
            array_push($accounts, $arr);
            $doLogin = true;
        }
        if($doLogin == true)
        {
            if (isset ($_GET['role'])) {
            header("Location: mhci_prereqs_intro.php?role=reviewer");
            } else  {
                header("Location: mhci_prereqs_intro.php");
            }
        }

}

if(isset($_GET["domain"]) )
{
    if($_SESSION['domainid'] != intval($_GET["domain"]))
    {
        $domain = intval(htmlspecialchars($_GET["domain"]));
        $sql = "select id, name from domain where id = ".$domain;
        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            
            $_SESSION['domainid'] = $row['id'];
            $_SESSION['domainname'] = $row['name'];
        }
        if($_SESSION['domainid'] < 1 || $_SESSION['domainname'] == "")
        {
            $err .= "Invalid domain request.<br>";
        }else
        {
            $srvname = $_SERVER["SERVER_NAME"];
            $scrname = $_SERVER["SCRIPT_NAME"];
            $query =  $_SERVER['QUERY_STRING'];
            header("Location: https://".$srvname.$scrname."?".$query);
        }
    }
}

if(isset($_POST['btnLogin']))
{
    if(isset($_POST['txtEmail']) && isset($_POST['txtPass']) )
    {
        if($recommender == true)
        {
            $passwordHash = htmlspecialchars(trim($_POST['txtPass']));
            
        }else
        {
            $passwordHash = sha1( htmlspecialchars( trim($_POST['txtPass']) ) );
        } 
    
    $MSHCIIUnit = new Unit(48);
    $MSHCIIPortugalUnit = new Unit(49);
    // DAS should add code here for no active placeout periods
 //   DebugBreak();
    $activeMSHCIIPeriods = $MSHCIIUnit->getActivePeriods(4);
    foreach ($activeMSHCIIPeriods as $activeMSHCIIPeriod) {
         $activeMSHCIIPlaceoutPeriods = $activeMSHCIIPeriod->getChildPeriods(4);
         $activeMSHCIIUmbrellaId = $activeMSHCIIPeriod->getId();
    }
    foreach ($activeMSHCIIPlaceoutPeriods as $activeMSHCIIPlaceoutPeriod) {
        $activeMSHCIIPeriodId = $activeMSHCIIPlaceoutPeriod->getId();
    }
    $activeMSHCIIPortugalPeriods = $MSHCIIPortugalUnit->getActivePeriods(4);
    foreach ($activeMSHCIIPortugalPeriods as $activeMSHCIIPortugalPeriod) {
        $activeMSHCIIPortugalPlaceoutPeriods = $activeMSHCIIPortugalPeriod->getChildPeriods(4);
        $activeMSHCIIPortugalUmbrellaId = $activeMSHCIIPortugalPeriod->getId();
    }
    foreach ($activeMSHCIIPlaceoutPeriods as $activeMSHCIIPortugalPlaceoutPeriod) {
        $activeMSHCIIPortugalPeriodId = $activeMSHCIIPortugalPlaceoutPeriod->getId();
    }
    
    if (isset($activeMSHCIIPeriodId) && isset($activeMSHCIIPortugalPeriodId) ) {
        if ($activeMSHCIIPeriodId == $activeMSHCIIPortugalPeriodId) {
            $activePlaceoutPeriod = $activeMSHCIIPeriodId;
        } else {
            DebugBreak();
        }
    } elseif (isset($activeMSHCIIPeriodId)) {
          $activePlaceoutPeriod = $activeMSHCIIPeriodId;
    } elseif (isset($activeMSHCIIPortugalPeriodId)) {
          $activePlaceoutPeriod = $activeMSHCIIPortugalPeriodId;
    }
    
    if (isset($activeMSHCIIUmbrellaId) && isset($activeMSHCIIPortugalUmbrellaId) ) {
        if ($activeMSHCIIUmbrellaId == $activeMSHCIIPortugalUmbrellaId) {
            $activeUmbrellaPeriod = $activeMSHCIIUmbrellaId;
        } else {
            DebugBreak();
        }
    } elseif (isset($activeMSHCIIUmbrellaId)) {
          $activeUmbrellaPeriod = $activeMSHCIIUmbrellaId;
    } elseif (isset($activeMSHCIIPortugalUmbrellaId)) {
          $activeUmbrellaPeriod = $activeMSHCIIPortugalUmbrellaId;
    }
    
   // DebugBreak();
    $testUser = new DB_PlaceOut_User();
    $isAuthenticated = $testUser->AuthenticatedUserP($_POST['txtEmail'], $passwordHash);
    if (!isset($isAuthenticated) || (sizeOf($isAuthenticated) < 1))  {
          $err .= "User authentication failed<br/>";
    } elseif (sizeOf($isAuthenticated) > 1)  {
        $err .= "Multiple users match credentials<br/>";
    } else {
        $luu_id = $isAuthenticated[0]['uid'];
    //    DebugBreak();
    }
    if (isset($luu_id)) {
        $isAuthorized = $testUser->AuthorizedUserP($luu_id, $activeUmbrellaPeriod);
        if (sizeOf($isAuthorized) < 1)  {
              $err .= "User authorization failed<br/>";
        } elseif (sizeOf($isAuthorized) > 1)  {
            // $err .= "Admitted to multiple programs<br/>";
        } else {
    //            DebugBreak();
        }
    } 

           
 /*       $sql = "select users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email
        from users 
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        inner join application on application.user_id = lu_users_usertypes.id
        inner join period_application on period_application.application_id = application.id 
        and period_application.period_id = ". $activeMSHCIIPeriodId . 
        " inner join  lu_application_programs on lu_application_programs.application_id = period_application.application_id
        and (lu_application_programs.program_id = 100000 or lu_application_programs.program_id = 100000)
        and lu_application_programs.admission_status = 2
        where email = '".addslashes(htmlspecialchars(trim($_POST['txtEmail'])))."' and password = '".$passwordHash."'";
       // DebugBreak();
        $result = mysql_query($sql) or die(mysql_error());
        */
        //echo $sql;
 //       while($row = mysql_fetch_array( $result )) 
  //      {
//  DebugBreak();
        if ($err == "") {
            $_SESSION['usermasterid'] = $isAuthenticated[0]['id'];
            $_SESSION['userid'] = $isAuthenticated[0]['uid'];
            $_SESSION['firstname'] = $isAuthenticated[0]['firstname'];
            $_SESSION['lastname'] = $isAuthenticated[0]['lastname'];
            $_SESSION['email'] = $isAuthenticated[0]['email'];
            $_SESSION['usertypeid'] = $isAuthenticated[0]['typeid'];
            $_SESSION['application_id'] = $isAuthorized[0]['id'];
            $_SESSION['activePlaceoutPeriod'] = $activePlaceoutPeriod;
            $arr = array();
            //array_push($arr,$row['uid']);
            array_push($arr,$isAuthenticated[0]['typeid']);
            array_push($arr,$isAuthenticated[0]['typename']);
            array_push($arr, $isAuthorized[0]['id']);
            array_push($accounts, $arr);
            //echo $row['email'];
            //echo $_SESSION['email'];
//        }
        }
        //echo "usertypeid: ".$_SESSION['usertypeid'];
        if($_SESSION['userid'] == -1 || $_SESSION['usermasterid'] == -1)
        {
            $err .= "Invalid username or password.<br>";
        }else
        {
            if(count($accounts) > 1)
            {
           //    DebugBreak();
            }
            else {
                if (sizeof($isAuthorized) > 0)   {
                    if (count($accounts) == 1) 
                        {
                            header("Location: mhci_prereqs_intro.php");
                            }
                } else {
                    $err .= "<br />You are not authorized";
                }
            }
        }
    }
    else{
        $err .= "Please enter both your email address and password.<br>";
        $_SESSION['userid'] = -1;
    }
}else
{
    if(isset($_POST['btnSubmit']))
    {
        if(intval($_POST['lbUserType']) > -1)
        {
            $sql = "select lu_users_usertypes.id as uid
            from users 
            inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
            inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
            where users.id =".$_SESSION['usermasterid'] . " and lu_users_usertypes.usertype_id=".intval($_POST['lbUserType']);
            $result = mysql_query($sql) or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $_SESSION['userid'] = $row['uid'];
                
            }
            $_SESSION['usertypeid'] = htmlspecialchars($_POST['lbUserType']);
            //echo $sql;
            header("Location: mhci_prereqs_intro.php");
        }else
        {
            $err .="Please select a user type to log in as.<br>";
        }
    }
    
}
//GET ACCOUNT TYPES
$sesEmail = $_SESSION['email'];

?>

<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
        <form accept-charset="utf-8" action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->            <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
				  //                    DebugBreak() ;
					?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px"><!-- InstanceBeginEditable name="EditRegion3" --><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
            <span class="errorSubtitle"><?=$err;?></span>            
            <div style="width:100%" class="tblItem">
            <?
 //           $domainid = 1;
            if(isset($_SESSION['domainid']))
            {
                if($_SESSION['domainid'] > -1)
                {
                    $domainid = $_SESSION['domainid'];
                }
            }
            if ($domainid == -1) {
                if ($_SESSION['A_usertypeid'] == 19) {
                    $deptRoleId = $_SESSION['roleDepartmentId'];
                    $domainid = 37;
                    $_SESSION['A_domanid'] = 37;
                    
                    
                } else {
                    header("Location: nodomain.php");
                }
            }
            
                $sql = "select content from content where name='Placeout Index Page' and domain_id=".$domainid;
            
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                echo html_entity_decode($row["content"]);
            }
            ?>
            
              <div align="center">
              <? if(count($accounts) > 1){ ?>
              <br>
              You are authorized to log in under more than one account type.<br>
            Please choose the account that you wish to use from the list below:
            <br>
            <? showEditText("", "listbox", "lbUserType", true,true, $accounts); ?>
            <? showEditText("Login", "button", "btnSubmit", true); ?>
            <span class="subtitle">
            
            </span><br>
            <? } else{ ?>
                <h2><strong>You are in a different system than you applied through.  Your password <br />
                        from the application system will not work.  Please use the <br /> 
                        <a href="forgotPassword.php" style="font-size: 18px; text-decoration: none">Forgot Password</a> <br />link
                        with the email address you used to apply to to set your password. </strong> </h2> <br /> <br />
             <table width="150" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td colspan="2" class="subTitle"><?
                    
                        echo "Returning Applicant";
                    
                    ?></td>
                  </tr>
                  <tr>
                    <td class="tblItem">User ID :</td>
                    <td class="tblItem"><input name="txtEmail" type="text" class="tblItem" id="txtEmail" maxlength="100" tabindex="1" ></td>
                  </tr>
                  <tr>
                    <td class="tblItem">Password:</td>
                    <td class="tblItem"><input name="txtPass" type="password" class="tblItem" maxlength="50" tabindex="2"></td>
                  </tr>
                  <tr>
                    <td class="tblItem"><input type="submit" name="btnLogin" value="Login" class="tblItem" tabindex="3" alt="Login"></td>
                    <td class="tblItem"><a href="forgotPassword.php<? if($recommender == true){echo "?r=1";} ?>">Forgot Password?</a> </td>
                  </tr>
                  <tr>
                    <td colspan="2" class="tblItem">
                    <? if($recommender != true){ ?>
                    <br>
                    <br>
                        <? 
                        //                        if($_SESSION['allow_edit'] == true && $_SESSION['domainid'] != 1 && $_SESSION['domainid'] !=3 ){ 
                   //     if($_SESSION['allow_edit'] == true ){
if ($_SESSION['expdate'] == "2009-12-17 00:00:00")
                        { $_SESSION['allow_edit'] = true; } else {
                       if($_SESSION['allow_edit'] == true ){
?>
                   <!--     <input type="button" name="btnNew" value="Register as New Applicant" class="tblItem" tabindex="3" alt="Register as New User" onClick="document.location='accountCreate.php'">  -->
                        <? }//end if ?>
                        <? }//end if ?>
                    <? } ?>
                    </td>
                  </tr>
                </table>
              <a href="accountCreate.php" ></a> <br>
            <? }//end if accounts ?>
 </div></div>
            <!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
            <!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
