<?php

include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once '../classes/DB_Applyweb/class.DB_Unit.php';
include_once '../classes/class.Unit.php'; 
include_once '../classes/DB_Applyweb/class.DB_Period.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include_once '../classes/class.Period.php';

$MSHCIIUnit = new Unit(48);
$MSHCIIPortugalUnit = new Unit(49);
// DAS should add code here for no active placeout periods
// DebugBreak();
$activeMSHCIIPeriods = $MSHCIIUnit->getActivePeriods(4);
foreach ($activeMSHCIIPeriods as $activeMSHCIIPeriod) {
     $activeMSHCIIPlaceoutPeriods = $activeMSHCIIPeriod->getChildPeriods(4);
     $activeMSHCIIUmbrellaId = $activeMSHCIIPeriod->getId();
}
foreach ($activeMSHCIIPlaceoutPeriods as $activeMSHCIIPlaceoutPeriod) {
    $activeMSHCIIPeriodId = $activeMSHCIIPlaceoutPeriod->getId();
}
$activeMSHCIIPortugalPeriods = $MSHCIIPortugalUnit->getActivePeriods(4);
foreach ($activeMSHCIIPortugalPeriods as $activeMSHCIIPortugalPeriod) {
    $activeMSHCIIPortugalPlaceoutPeriods = $activeMSHCIIPortugalPeriod->getChildPeriods(4);
    $activeMSHCIIPortugalUmbrellaId = $activeMSHCIIPortugalPeriod->getId();
}
foreach ($activeMSHCIIPlaceoutPeriods as $activeMSHCIIPortugalPlaceoutPeriod) {
    $activeMSHCIIPortugalPeriodId = $activeMSHCIIPortugalPlaceoutPeriod->getId();
}

if (isset($activeMSHCIIPeriodId) && isset($activeMSHCIIPortugalPeriodId) ) {
    if ($activeMSHCIIPeriodId == $activeMSHCIIPortugalPeriodId) {
        $activePlaceoutPeriod = $activeMSHCIIPeriodId;
    } else {
        DebugBreak();
    }
} elseif (isset($activeMSHCIIPeriodId)) {
      $activePlaceoutPeriod = $activeMSHCIIPeriodId;
} elseif (isset($activeMSHCIIPortugalPeriodId)) {
      $activePlaceoutPeriod = $activeMSHCIIPortugalPeriodId;
}
$_SESSION['activePlaceoutPeriod'] =  $activePlaceoutPeriod;
$_SESSION['userid'] = $_REQUEST['studentLuId'];
$_SESSION['application_id'] = $_REQUEST['appid'];
$_SESSION['appid'] = $_SESSION['application_id'];
$name = '';
$sql = "select users.lastname, users.firstname, users.middlename from application inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id ";
$sql = $sql . "inner join users on users.id = lu_users_usertypes.user_id WHERE application.id =" . $_SESSION['appid'];
$result = mysql_query($sql);
while($row = mysql_fetch_array( $result )) 
{
    $name = $row["lastname"]. ', ' . $row["firstname"]. ' ';
    if (isset($row["middlename"])) {
        $name = $name . $row["middlename"];
    }
}
if ($name != "") {
    $_SESSION['applicant_name'] = $name;
}
?>