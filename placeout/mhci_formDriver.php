<?php
header("Cache-Control: no-cache");
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/MHCI_style.css"/>
<script language="javascript" src="../inc/jquery-1.2.6.min.js"></script>
<script language="javascript" src="../inc/mhci_prereqs.js"></script>
</head>
<body>

<form action="" method="post"> 

<?php

include_once "../inc/db_connect.php";
include "../inc/mhci_prereqsAssessmentForm.class.php";
include "../inc/mhci_prereqsPortfolioForm.class.php";
include "../inc/mhci_prereqsCourseForm.class.php";
include "../inc/mhci_prereqsCourseFormController.class.php";
include "../inc/mhci_prereqsMultipleCourseForm.class.php"; 
include "../inc/mhci_prereqsConversationForm.class.php";
/*
include "../inc/mhci_prereqsCodeSampleForm.class.php";
include "../inc/mhci_prereqsProgrammingTestForm.class.php";
*/  

$studentId = 999999;
$reviewerId = 111111;

$userId = $studentId;
$view = "student";
if( isset($_REQUEST['role']) ) {
    if ($_REQUEST['role'] == "reviewer") {
        $userId = $reviewerId;
        $view = $_REQUEST['role'];
    }
} 

session_start();
$_SESSION['userId'] = $userId;


$formController = new MHCI_PrereqsCourseFormController($studentId, $view, "design", TRUE);
$formController->render();



?>

<div id="assessmentForm">

<?php

//$form1 = new MHCI_PrereqsAssessmentForm($studentId, $view, "design");
//$form1->render();

?>

</div> 
<hr/>
<div id="portfolio">

<?php 

//$form2 = new MHCI_PrereqsPortfolioForm($studentId, $view);
//$form2->render();

?>

</div> 
<hr/>
<div id="course">

<?php 
  /*
$form3 = new MHCI_PrereqsCourseForm($studentId, $view, "Design", FALSE);
$form3->setHeading("Course 1");
$form3Intro = <<<EOB
If you have taken courses in a Fine Arts or Applied Arts program that cover the visual and verbal vocabulary 
of communication design, the design process, and the communicative value of text and image, please list them here. 
Otherwise, you can skip this section. Courses in Computer Science do not fulfill this prerequisite.
EOB;
$form3->setIntro($form3Intro);
$form3->render();
*/ 
//$form3 = new MHCI_PrereqsMultipleCourseForm($studentId, $view, "Design", FALSE);
//$form3->render();
  
?>

</div> 
<hr/>
<div id="conversation">

<?php 



//$form4 = new MHCI_PrereqsConversationForm($studentId, $userId, $view, "design");
//$form4->render();

?>

</div>
<hr/>

<?php
/*



echo "<hr/>";

$form5 = new MHCI_PrereqsCodeSampleForm($studentId, $view);
$form5->render();

echo "<hr/>";

$form = new MHCI_PrereqsProgrammingTestForm($studentId, $view);
$form->render();

echo "<hr/>"; 

$form = new MHCI_PrereqsConversationForm($studentId, $userId, "statistics");
$form->render();
*/

?>

</form>

</body>
</html>
