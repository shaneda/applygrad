<?php
/*
* 
*/
//DebugBreak();
// Don't do anything if the domain id isn't valid.
if ( isset($_GET['domain']) && filter_var($_GET['domain'], FILTER_VALIDATE_INT) ) {
    $domainid = $_GET['domain'];
} else {   
    header("Location: nodomain.php");
    exit;
}

// Do the necessary standard includes.
include_once '../inc/config.php'; 
include_once '../inc/db_connect.php';

/* 
* Get the unit based on the domain id and get 0 || 1 
* umbrella period it has with an active submission period. 
* The include will set:
*   $unit
*   $unitId
*   $activePeriod
*   $activePeriodId
*/
include '../inc/applyPeriod.inc.php';

// Disable editing by default.
session_start();
$_SESSION['domainid'] = $domainid;
$_SESSION['allow_edit'] = FALSE;
$_SESSION['allow_edit_late'] = FALSE;
$_SESSION['expdate'] = '';
$_SESSION['expdate2']  = '';

$activeUmbrellaPeriod = NULL;
if ($activePeriodId) {

    $_SESSION['allow_edit'] = TRUE;
    $activeUmbrellaPeriod = $activePeriod;
    
} else {

    // Check for 0 || 1 umbrella period with an active editing period.
    $activePeriods = $unit->getActivePeriods(3);
    if ( count($activePeriods) > 0 ) {
        $activeUmbrellaPeriod = $activePeriods[0];    
    }    
}

if ($activeUmbrellaPeriod) {
    
    $_SESSION['allow_edit_late'] = TRUE;

    $submitPeriods = $activeUmbrellaPeriod->getChildPeriods(2);
    // There should be one (and only one) submission period.
    if ( count($submitPeriods) > 0 ) {       
        $_SESSION['expdate'] = $submitPeriods[0]->getEndDate();;    
    }
    
    $editPeriods = $activeUmbrellaPeriod->getChildPeriods(3);
    // There should be one (and only one) editing period.
    if ( count($editPeriods) > 0 ) {       
        $_SESSION['expdate2'] = $editPeriods[0]->getEndDate();;    
    }    
}

header("Location: index.php?periodChecked=1&domain=" . $domainid); 

// Replicating the code in applyPeriod.inc.php:
/*
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php'; 
include_once '../classes/DB_Applyweb/class.DB_Unit.php';
include_once '../classes/DB_Applyweb/class.DB_Period.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodApplication.php'; 
include_once '../classes/class.Unit.php';
include_once '../classes/class.Period.php';

$unitQuery = "SELECT unit_id FROM domain_unit WHERE domain_id = " . $domainid;
$unitResult = mysql_query($unitQuery);
$unitId = NULL;
while ($row = mysql_fetch_array($unitResult)) {
    $unitId = $row['unit_id'];
}
$unit = new Unit($unitId);

$activePeriodId = NULL;
$submitStart = $submitEnd = $editEnd = '0000-00-00 00:00:00';
$allowEdit = $allowEditLate = 'FALSE';

$umbrellasActiveSubmit = $unit->getActivePeriods(2);
if ( count($umbrellasActiveSubmit) > 0 ) {
    // Should only be max one umbrella!
    $activePeriodId = $umbrellasActiveSubmit[0]->getId();
    $allowEdit = $allowEditLate = 'TRUE';
    $submitPeriods = $umbrellasActiveSubmit[0]->getChildPeriods(2);
    // Should only be one submit period! 
    $submitStart = $submitPeriods[0]->getStartDate();
    $submitEnd = $submitPeriods[0]->getEndDate();    
}

$umbrellasActiveEdit = $unit->getActivePeriods(3);
if ( count($umbrellasActiveEdit) > 0 ) {
    // Should only be max one umbrella!
    $allowEditLate = 'TRUE';
    $editPeriods = $umbrellasActiveEdit[0]->getChildPeriods(3);
    // Should only be one edit period! 
    $editEnd = $editPeriods[0]->getEndDate();
    if (!$activePeriodId) {
        $activePeriodId = $umbrellasActiveEdit[0]->getId();    
    }
}

// Returning XML:
$dom = new DOMDocument('1.0', 'UTF-8');
$root = $dom->createElement('deadlines');
$dom->appendChild($root);
$root->appendChild( $dom->createElement( 'domainId', $domainid ) );
$root->appendChild( $dom->createElement( 'unitId', $unitId ) );
$root->appendChild( $dom->createElement( 'periodId', $unitId ) );
$root->appendChild( $dom->createElement( 'submitStart', $submitStart ) );
$root->appendChild( $dom->createElement( 'submitEnd', $submitEnd ) );
$root->appendChild( $dom->createElement( 'editEnd', $editEnd ) );
$root->appendChild( $dom->createElement( 'allowEdit', $allowEdit ) );
$root->appendChild( $dom->createElement( 'allowEditLate', $allowEditLate ) );
echo $dom->saveXML();
*/
?>