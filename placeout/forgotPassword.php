<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$email = "";
$guid = "";
$mailSent = false;
$recommender = false;

//GET USER INFO
if(isset($_POST['txtEmail']))
{
    $email = filter_input(INPUT_POST, 'txtEmail', FILTER_VALIDATE_EMAIL);
    if ($email) {
        $email = htmlspecialchars($email);
    }

	if ($email != "" && $email != "*" && check_email_address($email) !== false && strstr($email,"applygrad")===false)
	{
		$sql = "SELECT email, guid from users 
            where email = '". mysql_real_escape_string($email) ."'";
		$result = mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);

		if(mysql_num_rows($result) == 0)
		{
			$err .= "Email address not found.<br>";
		}
        
		$email = "";
		while($row = mysql_fetch_array( $result )) 
		{
			$email = $row['email'];
			$guid = $row['guid'];
	
			//SEND AN EMAIL TO EACH QUALIFYING EMAIL
			
			if($email != "" && $guid != "")
			{
				$sysemail = "";
				$link = "https://".$_SERVER['SERVER_NAME']."/apply/newPassword.php?email=".str_replace("+","%2b",$email)."&id=".$guid;
				if($recommender == true)
				{
					$link .= "&r=1";
				}
                if ( isset($_SESSION['domainid']) && $_SESSION['domainid'] > 0 ) {
                    $link .= "&domain=" . $_SESSION['domainid'];
                }
				$sql = "select sysemail from systemenv where domain_id=" . $_SESSION['domainid'];
				$result1 = mysql_query($sql)	
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
				while($row1 = mysql_fetch_array( $result1 ))
				{ 
					$sysemail = $row1['sysemail'];
				}
				
				//GET TEMPLATE CONTENT
				$sql = "select content from content where name='Forgot Password Letter' and domain_id=" . $_SESSION['domainid'];
				$result1 = mysql_query($sql)	
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
				while($row1 = mysql_fetch_array( $result1 )) 
				{
					$str = $row1["content"];
				}
				
				$vars = array(
				array('email', $email), 
				array('guid',$guid),
				array('link',$link)
				);
				$str = parseEmailTemplate2($str, $vars );
				
                //SEND EMAIL
				sendHtmlMail($sysemail, $email, "Password Reset Request", $str, "password");//to user
				$mailSent = true;
				
			}else
			{
				if(isset($_POST['btnSubmit']))
				{
					$err .= "Email account not found.<br>";
				}//end if submit
			}
		
		}//end while	
			
	}
    else
	{
		$err .= 'Invalid email address: "' . htmlspecialchars($_POST['txtEmail']) . '"<br>';
		$email = "";
	}
}//end btnsubmit

$pageTitle = 'Forgot Password';
include '../inc/tpl.pageHeaderApply.php'; 
?>

	<span class="errorSubTitle"><?=$err?></span>
	<span class="tblItem">
	<? if($email == "") { ?>
	Please enter your email address:<br>
    <input name="txtEmail" type="text" class="tblItem" id="txtEmail" maxlength="255">
    <input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="Submit">
    <br>
	<? } 
	if($mailSent == true && $err == ""){ ?>
        An email has been sent to you. In order to change your password, you must click on the link in the email. 
        You will then be prompted to enter a new password.
        <br>
		<br>
		<a href="index.php<? if($recommender == true){echo "?r=1";} ?>">Return to login page</a>			
	<? } ?>
	</span>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>