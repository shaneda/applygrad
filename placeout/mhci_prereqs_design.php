
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>

<? include_once '../inc/session.php';  ?> 
<? include_once '../inc/db_connect.php'; ?>
<? $exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";


if( isset($_REQUEST['role']) ) {
    if ($_REQUEST['role'] == "reviewer") {
        include_once '../placeout/mhci_session.php';
        $_SESSION['usertypeid']  = $_SESSION['A_usertypeid'];
        }
}

if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/MHCI_style.css"/>
<script language="javascript" src="../javascript/jquery-1.2.6.min.js"></script>
<script language="javascript" src="../inc/mhci_prereqs.js"></script>
<!-- InstanceBeginEditable name="head" -->
<?

?>
</head>
<link rel="stylesheet" href="../css/app2.css" type="text/css">
<link rel="stylesheet" href="../css/MHCI_style.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" onSubmit="return verifyForm()">
        <!-- InstanceEndEditable -->
        <div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
              <? 
            if($_SESSION['usertypeid'] != 6)
            {
                include './prereqsNav.php'; 
            }
           
            ?>
            <!-- InstanceEndEditable -->
			<!-- InstanceBeginEditable name="EditNav" -->
            <tr>
            <td>
                <table>
                <colgroup>
                <col width=80% style=text-align:left;>
                <col width=20% style=text-align:right;>
                </colgroup>
                <tr>
            
            <td style="text-align:right; width:700px">    
         
                <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                ){ ?>
                    <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                    <!-- DAS Removed - <? echo $sesEmail;?>   -->
                    <? } else
                        {
                            if (isset($_REQUEST['role']) && ($_REQUEST['role'] == "reviewer")) {
                                
                            }   else {
                                    if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                                    && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                                    && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                                    && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                                    && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                                    {
                                    //session_unset();
                                    //destroySession();
                                    //header("Location: index.php");

                                ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                                    }
                        }
                    } ?>
            </td>
            <tr>
            </table>
            </td>
            </tr>
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px">  <!-- InstanceBeginEditable name="EditRegion3" -->
         
            <div class="title">Design Placeout Opportunity Verification</div>
            <?php 
          //  DebugBreak();
            if (!isset($_REQUEST['role']) || ((isset($_REQUEST['role'])) && ($_REQUEST['role'] != "reviewer"))) {
             ?>             
            <button class="bodyButton-right" onClick="parent.location='mhci_prereqs_intro.php';return false;">
            Home</button>
            <?php } else {
                $studentName = $_SESSION['applicant_name']; 
                echo "<h4>". $studentName . "</h4>";
                $buttonLoc = "'../admin/administerPlaceout.php?unit=department&unitId=".$_SESSION['roleDepartmentId']."&programId=". $_REQUEST['pid'] . "&period=".$activeMSHCIIUmbrellaId."'"?> 
                <button class="bodyButton-right" onClick="parent.location=<?php echo $buttonLoc; ?>;return false;">
            Return to List</button>
            <?php } ?>
            <!-- InstanceEndEditable -->
			<br/><br/>

<?
include "../inc/mhci_prereqsAssessmentForm.class.php";
include "../inc/mhci_prereqsCourseForm.class.php";
include "../inc/mhci_prereqsCourseFormController.class.php";
include "../inc/mhci_prereqsPortfolioForm.class.php";
include "../inc/mhci_prereqsConversationForm.class.php";
include "../inc/mhci_prereqsNotification.class.php";
            
$studentId = $_SESSION['userid'];
$view = "student";
if( isset($_REQUEST['role']) ) {
    if ($_REQUEST['role'] == "reviewer") {
        $userId = $_SESSION['roleLuuId'];
        $view = $_REQUEST['role'];
    }
} 
?>

<br />
<?php
echo '<div class="bodyText">';
$sql = "select content from content where name='Placeout Design Intro' and domain_id=".$domainid;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}


/*
function formComplete() {
    global $assessmentForm, $courseFormController, $portfolioForm, $conversationForm;
 //   DebugBreak();
    if ($assessmentForm->getStudentAssessment() == "fulfilledFalse") {
        return TRUE;
    } elseif (($assessmentForm->getStudentAssessment() == "fulfilledTrue") &&
                (($courseFormController->verifyCourseInfoComplete()  ||  
                $portfolioForm->getPortfolioURL() <> '')))   //  && 
          //      (($conversationForm->getWantsComments() == TRUE && sizeof($conversationForm->getAllComments()) > 0) || 
          //      $conversationForm->getWantsComments() == FALSE)) 
            {
                    return TRUE;
                } else {
                        return FALSE;
                        }
}
*/

if ((isset($_REQUEST['TestSubmit'])) && $_REQUEST['TestSubmit'] == 'Change') {
    // DebugBreak();
    $assessmentForm->setFormSubmitted(FALSE);
    $portfolioForm->setFormSubmitted(FALSE);
    $conversationForm->setFormSubmitted(FALSE);
    $assessmentForm->updateDbReviewerStatus(FALSE);
    break;
}
?>
</div>
<br />

<div id="assessmentForm">

<?php
$assessmentForm = new MHCI_PrereqsAssessmentForm($studentId, $view, "design");
$assessmentForm->setApplicationId($_SESSION['application_id']);
$assessmentForm->setPlaceoutPeriodId($_SESSION['activePlaceoutPeriod']);
$assessmentForm->prepareDataForRender();
$assessmentForm->render();

?>

</div> 
<hr/>

<div id="courseForms">

<?php
$_SESSION['placeout_enabled'] = TRUE;
$courseFormController = new MHCI_PrereqsCourseFormController($studentId, $view, "design", TRUE);
if ($assessmentForm->getStudentAssessment() == "fulfilledTrue" || $assessmentForm->getStudentAssessment() != 'fulfilledFalse') {
    if ($assessmentForm->getReviewerStatus() == 'Approved plan:' || $assessmentForm->getReviewerStatus() == 'Fulfilled: undergraduate degree' 
    || $assessmentForm->getReviewerStatus() == 'Fulfilled:') {
        $courseFormController->render('bogus');
    } else {
          $courseFormController->render('enable');
    }
}
?>

</div>

 
<div id="portfolioForm">

<?php 

$portfolioForm = new MHCI_PrereqsPortfolioForm($studentId, $view);
if ($assessmentForm->getStudentAssessment() == "fulfilledTrue" || $assessmentForm->getStudentAssessment() != 'fulfilledFalse') {
    if ((isset($_REQUEST['TestSubmit'])) && $_REQUEST['TestSubmit'] == 'Submit for Review') {
  //      DebugBreak();
        $assessmentForm->setFormSubmitted(TRUE);
        $portfolioForm->setFormSubmitted(FALSE);
    //    $conversationForm->setFormSubmitted(TRUE);
     //   $assessmentForm->updateDbReviewerStatus(TRUE);
        }
        if ($assessmentForm->getReviewerStatus() == 'Approved plan:' || $assessmentForm->getReviewerStatus() == 'Fulfilled: undergraduate degree' 
    || $assessmentForm->getReviewerStatus() == 'Fulfilled:') {
             $portfolioForm->render('bogus');
    } else {
         $portfolioForm->render('enable');
    }
}

?>

</div>
<hr/>

<div id="conversationForm">

<?php 
$conversationForm = new MHCI_PrereqsConversationForm($studentId, $studentId, $view, "design");
// DAS DebugBreak();
if ($assessmentForm->getStudentAssessment() == "fulfilledTrue" || $assessmentForm->getStudentAssessment() != 'fulfilledFalse') {
    if ((isset($_REQUEST['TestSubmit'])) && $_REQUEST['TestSubmit'] == 'Submit for Review') {
  //      DebugBreak();
    //    $assessmentForm->setFormSubmitted(TRUE);
   //     $portfolioForm->setFormSubmitted(FALSE);
        $conversationForm->setFormSubmitted(TRUE);
    }
    if ($assessmentForm->getReviewerStatus() == 'Approved plan:' || $assessmentForm->getReviewerStatus() == 'Fulfilled: undergraduate degree' 
    || $assessmentForm->getReviewerStatus() == 'Fulfilled:') {
        $conversationForm->render('bogus');
    } else {
          $conversationForm->render(FALSE);
    }
}
?>

</div>
<?php 
/*
if (isset($_REQUEST['TestSubmit']) && $_REQUEST['TestSubmit']== 'Submit for Review') {
        $assessmentForm->setStudentStatus('Student Submitted');
        $assessmentForm->updateDbStudentStatus();
        $assessmentForm->setReviewerStatus('Student Submitted');
    } 
*/
      
// if ($assessmentForm->getStudentStatus() != 'Student Submitted') {
 //   $completedForm = formComplete();
      //  $assessmentForm->renderSubmit("Submit to Reviewer");
 //            DebugBreak();
/*    if ($completedForm) {
        if ($assessmentForm->getFormSubmitted() != TRUE)  {
        */
     // DAS    DebugBreak();
if ($assessmentForm->getReviewerStatus() == 'Approved plan:' || $assessmentForm->getReviewerStatus() == 'Fulfilled: undergraduate degree' 
    || $assessmentForm->getReviewerStatus() == 'Fulfilled:') {
        // do not display buttons
    } else {
        //display appropriate buttons
if (!isset($_REQUEST['role']) || ((isset($_REQUEST['role'])) && $_REQUEST['role'] != 'reviewer')) {
              echo <<<EOB
        <input type="submit" id="testSave" name="TestSave" class="bodyButton assessmentSubmit" value="Save without Submit" /> 
        
EOB;
   // DebugBreak();
        echo <<<EOS
         <input type="submit" id="testSubmit" name="TestSubmit" class="bodyButton assessmentSubmit" value="Submit for Review" />      

EOS;
} else {
       echo <<<EOS
         <input type="submit" id="testSubmit" name="TestSubmit" class="bodyButton assessmentSubmit" value="Send Email to Student" />      

EOS;
}
    }
/*
        } else {
              echo <<<EOB
        <input type="submit" id="testSave" name="TestSave" class="bodyButton assessmentSubmit" value="Save without Submit" disabled=true/> 
        
EOB;
            echo <<<EOS
         <input type="submit" id="testSubmit" name="TestSubmit" class="bodyButton assessmentSubmit" value="Submit for Review" disabled=true/>      

EOS;
        }
    } else {
        if ($assessmentForm->getFormSubmitted() != TRUE) {
             echo <<<EOB
        <input type="submit" id="testSave" name="TestSave" class="bodyButton assessmentSubmit" value="Save without Submit" /> 
        
EOB;
        } else {
              echo <<<EOB
        <input type="submit" id="testSave" name="TestSave" class="bodyButton assessmentSubmit" value="Save without Submit" disabled=true/> 
        
EOB;
        }
        echo <<<EOS
         <input type="submit" id="testSubmit" name="TestSubmit" class="bodyButton assessmentSubmit" value="Submit for Review" disabled=true/>      

EOS;
        
    }

 
 /*    
    {
        echo <<<EOB
         <input type="submit" id="testSubmit" name="TestSubmit" class="bodyButton assessmentSubmit" value="Submit for Review" />      

EOB;
    } 
    elseif (($completedForm != TRUE) && 
    (($assessmentForm->getReviewerStatus() != 'Submitted') && ($assessmentForm->getReviewerStatus() != 'In progress')) && 
    ($assessmentForm->getStudentStatus() !=  'Student Submitted')) {
        echo <<<EOB
        <input type="submit" id="testSave" name="TestSave" class="bodyButton assessmentSubmit" value="Save without Submit" /> 
        
EOB;
    }

    else  {
     //   DebugBreak();
        if ($completedForm != TRUE) {
          //  DebugBreak();
            echo <<<EOB
        <input type="submit" id="testSave" name="TestSave" class="bodyButton assessmentSubmit" value="{$assessmentForm->getSaveLabel()}"  />; 
        
EOB;
        }
    }
// }
*/
if (!isset($_REQUEST['role']) || (isset($_REQUEST['role'])) && $_REQUEST['role'] != 'reviewer') {
    if (isset($_REQUEST['TestSubmit']) && $_REQUEST['TestSubmit']== 'Submit for Review') {
        sendNotification ("design");
        }
}

if (isset($_REQUEST['role']) && $_REQUEST['role'] == 'reviewer') {
    if (isset($_REQUEST['TestSubmit']) && $_REQUEST['TestSubmit']== 'Send Email to Student') {
        //sendNotification ("design");
        sendStudentEmail($studentId, "design");
        // debugBreak();
        }
}
 ?>



        </form>
        </body>
<!-- InstanceEnd --></html>
