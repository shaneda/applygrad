<?php
session_start();

if ( isset($_SERVER['REMOTE_USER']) ) {
    $_SESSION['userWebIso'] = $_SERVER['REMOTE_USER'];    
} else if ( isset($_SERVER['HTTP_EPPN']) ) {
    $_SESSION['userWebIso'] = $_SERVER['HTTP_EPPN'];
    }

$viewApplication = "";
if ( isset($_REQUEST['viewApplication']) && isset($_REQUEST['viewDepartment']) ) {
    $validApplicationId = filter_var($_REQUEST['viewApplication'], FILTER_VALIDATE_INT);
    $validDepartmentId = filter_var($_REQUEST['viewDepartment'], FILTER_VALIDATE_INT);
    if ($validApplicationId && $validDepartmentId) {
        $viewApplication = "?viewApplication=" . $validApplicationId;
        $viewApplication .= "&viewDepartment=" . $validDepartmentId;
    }     
}

header('Location: ../admin/index.php' . $viewApplication);  
?>