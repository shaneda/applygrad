<?php
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
//include_once '../inc/functions.php'; 

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
} 
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    } 
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">

<?
	$allowUser = true;
	$departmentName = "";
	$s = -1;
	$sType = "(all)";

	$id = -1;
	if(isset($_GET['id']))
	{
		$id = intval($_GET['id']);
	}

	$sql = "select name from department where id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$departmentName = $row['name'];
	}
?>

<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>
<body>

  <form id="form1" name="form1" action="" method="post">

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><center><?=$departmentName?> Admitted Applicants 
    <br/>
    <?php echo $applicationPeriodDisplay; ?>
    </center></span><br />
<br />


	<?


	include '../inc/C_Spreadsheet_01a.php.inc';
	$spst = new Spreadsheet;

	$sql = "select
		concat(users.lastname, ', ',users.firstname) as name,

		GROUP_CONCAT(distinct institutes.name SEPARATOR '<BR>') as undergraduate_Institution,
		GROUP_CONCAT(distinct institutes2.name SEPARATOR '<BR>') as Graduate_Institution

		from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
        left outer join application on application.user_id = lu_users_usertypes.id";
        
        // PLB added period join 1/7/10
        if ($periodId) {
            $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
        }        
        
        $sql .= " left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
		left outer join institutes on institutes.id = usersinst.institute_id

		left outer join usersinst as usersinst2 on usersinst2.user_id = lu_users_usertypes.id and usersinst2.educationtype=2
		left outer join institutes as institutes2 on institutes2.id = usersinst2.institute_id


		left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id and lu_application_interest.choice=0
		left outer join interest on interest.id = lu_application_interest.interest_id  ";

	$spst->sql = $sql;
	$spst->sqlWhere = " where
	application.submitted=1
	and lu_application_programs.admission_status=2
	and lu_programs_departments.department_id=".$id;

    // PLB added period where 1/7/10
    if ($periodId) {
        $spst->sqlWhere .= " AND period_application.period_id = " .  $periodId;
    } 

	//$spst->sqlDirective = "distinct";
	$spst->sqlGroupBy = "lu_users_usertypes.id";

	$spst->sqlOrderBy = "users.lastname,users.firstname";
	$spst->showFilter = false;
	$spst->hideLinks = true;
	//$spst->editPage = "userroleEdit_student_formatted.php";
	$spst->doSpreadSheet();

?>



	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
</html>
