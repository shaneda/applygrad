<?php 
include "../inc/config.php";
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/View/class.VW_IndexApplicant.php";
include "../classes/DB_Applyweb/View/class.VW_IndexRecommender.php";
include "../classes/DB_Applyweb/View/class.VW_IndexScsUser.php";
//DebugBreak();

$searchString = filter_input(INPUT_GET, 'q', FILTER_SANITIZE_STRING, 
    array('flags' => FILTER_FLAG_NO_ENCODE_QUOTES) );
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);
$indexType = filter_input(INPUT_GET, 'index', FILTER_SANITIZE_STRING); 
switch($indexType) {

    case 'applicant':
    
        $userIndex = new VW_IndexApplicant();
        break;    

    case 'recommender':
    
        $userIndex = new VW_IndexRecommender();
        break;
    
    case 'scsUser':
    
        $userIndex = new VW_IndexScsUser();
        break;
    
    default:
        
        exit;
}

$resultArray = $userIndex->find($searchString, $periodId);
foreach ($resultArray as $row) {
    echo implode("|", $row) . "\n";
}
?>