<?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';


$deptId = -1; 
if(isset($_GET["id"]))
{
    $deptId = intval($_GET["id"]);
}
if(isset($_POST["deptId"]))
{
    $deptId = intval($_POST["deptId"]);
}

$show = 'all';
if( isset($_REQUEST["show"]) ) {
    $show = $_REQUEST["show"];
}

$deptName = "";
$sql = "select name from department where id=".$deptId;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $deptName = $row['name'];
}

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
}

$AOI_ONE=array();
$AOI_TWO=array();
$AOI_THREE=array();
$DECISION = array(array("A1",0), 
                  array("A2",0),
                  array("B1",0),
                  array("B2",0),
                  array("S", 0),
                  array("R", 0)
                  );

global $AOI_ONE;
global $AOI_TWO;
global $AOI_THREE;
global $DECISION;

function build_AOI_Arrays()
{
  $arr = array();
  $current_location = -1;
  $placeholder = array( "PLACEHOLDER", 0,0,0,0,0,0);

global $AOI_ONE;
global $AOI_TWO;
global $AOI_THREE;
global $DECISION;

  $sql = 'select id, name from interest order by id';

  $result = mysql_query($sql) or die(mysql_error(). $sql);
  while($row = mysql_fetch_array( $result ))
  { 
// echo "ROW[".$i."]=".$row[0]." , ".$row[1]."<BR>";
    for ($i=$current_location+1; $i<$row[0]; $i++) {
      // Push  place holder, there is a gap in the sequence
      array_push($AOI_ONE, $placeholder);
      array_push($AOI_TWO, $placeholder);
      array_push($AOI_THREE, $placeholder);

    }

    $current_location=$row[0];
    array_push($arr, $row[1] );
    array_push($arr, "0" ); // A1
    array_push($arr, "0" ); // A2
    array_push($arr, "0" ); // B1
    array_push($arr, "0" ); // B2
    array_push($arr, "0" ); // S
    array_push($arr, "0" ); // R
    array_push($AOI_ONE, $arr);
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );

    array_push($arr, $row[1] );
    array_push($arr, "0" ); // A1
    array_push($arr, "0" ); // A2
    array_push($arr, "0" ); // B1
    array_push($arr, "0" ); // B2
    array_push($arr, "0" ); // S
    array_push($arr, "0" ); // R
    array_push($AOI_TWO, $arr);
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );

    array_push($arr, $row[1] );
    array_push($arr, "0" ); // A1
    array_push($arr, "0" ); // A2
    array_push($arr, "0" ); // B1
    array_push($arr, "0" ); // B2
    array_push($arr, "0" ); // S
    array_push($arr, "0" ); // R
    array_push($AOI_THREE, $arr);
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
    array_pop($arr );
  }
/*
  for ($i=$current_location; $i>0; $i--)
  {
    echo "-".$i." ".$AOI_ONE[$i][0]." ".$AOI_ONE[$i][1]."<BR>";
  }  
*/
}


function collect_decision_stats($deptid, $periodId = NULL)
{ 

  global $show;
  global $AOI_ONE; 
  global $AOI_TWO;
  global $AOI_THREE;
  global $DECISION;

  $collect_aoi=0;

  $sql = ' select decision, LAP.application_id, LAP.program_id  '
       . ' from lu_application_programs as LAP, '
       . ' lu_programs_departments as LPD';

  if ($periodId) {
      $sql .= ', period_application AS PA';
  }

  $sql .= ' where '
       . ' LAP.decision IS NOT NULL and'
       . ' LAP.program_id =LPD.program_id and'
       . ' LPD.department_id='.$deptid.' ';

  if ($periodId) {
      $sql .= ' AND LAP.application_id = PA.application_id';
      if (is_array($periodId)) {
          $sql .= " AND PA.period_id IN (" .  implode(',', $periodId) . ")";    
      } else {
          $sql .= " AND PA.period_id = " . $periodId;
      }         
  }
  
if ($show != 'all') {
    $sql .= " and (admission_status = 1 or admission_status = 2)";     
}
       
  $result = mysql_query($sql) or die(mysql_error(). $sql);
  while($row = mysql_fetch_array( $result ))
  { 
    switch ($row[0]) {
    case "A1":
      $DECISION[0][1]++;
      $collect_aoi=1;
      break;
    case "A2":
      $DECISION[1][1]++;
      $collect_aoi=2;
      break;
    case "B1":
      $DECISION[2][1]++;
      $collect_aoi=3;
      break;
    case "B2":
      $DECISION[3][1]++;
      $collect_aoi=4;
      break;
    case "S":
      $DECISION[4][1]++;
      $collect_aoi=5;
      break;
    case "R":
      $DECISION[5][1]++;
      $collect_aoi=6;
      break;
    }
    if ( $collect_aoi>0 ) {
      $program_id = $row[2];
      collect_aoi_stats_for_decision( $row[1], $collect_aoi, $program_id);
    }
    $collect_aoi=0;
  }

}


function collect_aoi_stats_for_decision( $appid, $decision, $program_id )
{
  global $AOI_ONE; 
  global $AOI_TWO;
  global $AOI_THREE;
  global $DECISION;

  $sql = ' select '
       . ' LAI.interest_id, LAI.choice'
       . ' FROM '
       . ' lu_application_programs as LAP, '
       . ' lu_application_interest as LAI '
       . ' WHERE '
       . ' LAP.application_id='.$appid.' AND'
       . ' LAP.id = LAI.app_program_id AND '
       . ' LAP.program_id='.$program_id.' '
       . ' ORDER BY LAI.choice ';

// echo $sql."<BR>";

  $which_choice=1;
  $result = mysql_query($sql) or die(mysql_error(). $sql);
  while($row = mysql_fetch_array( $result ))
  { 

//echo "APP: ".$appid." ID: ".$row[0]." CHOICE: ".$row[1]." DECISION: ".$decision." WHICH_CHOICE: ".$which_choice." ";


    switch ($which_choice){
      case 1:
        $AOI_ONE[$row[0]][$decision]++;
//echo " WHAT: ".$AOI_ONE[$row[0]][0]." ";
        break;  
      case 2:
        $AOI_TWO[$row[0]][$decision]++;
//echo " WHAT: ".$AOI_TWO[$row[0]][0].<BR>";
        break;
      case 3:
        $AOI_THREE[$row[0]][$decision]++;
//echo " WHAT: ".$AOI_THREE[$row[0]][0]."<BR>";
        break;
    }
    $which_choice++;
//echo "WHICH_CHOICE: ".$which_choice."<BR>";
//echo "<BR>";
//    echo "InterestID ".$row[0]." ".$row[1]."<BR>";
  }
//echo "NEW LINE <BR>";       
}

function display_decision_stats()
{
  global $AOI_ONE; 
  global $AOI_TWO;
  global $AOI_THREE;
  global $DECISION;

  echo " <table border=0>";

  for ($i=0; $i<6; $i++) {

    echo "<tr>";
    echo "<td bgcolor=#CDCDCD>";
    echo "<b>".$DECISION[$i][0]."</b> Total: ".$DECISION[$i][1];
    echo "</td>";

    echo "<tr>";
    echo "</tr>";


    echo "<tr>";
    echo "<td>";
    for ($k=0; $k<76; $k++)
    {

      $x=$i+1;
      
      $aoiTotal = $AOI_ONE[$k][$x] + $AOI_TWO[$k][$x] + $AOI_THREE[$k][$x];
      if ($aoiTotal > 0) {
        echo $aoiTotal . " in ".$AOI_ONE[$k][0];
        echo ' (' . $AOI_ONE[$k][$x] . ', ' . $AOI_TWO[$k][$x] . ', ' . $AOI_THREE[$k][$x] . ')';
        echo "<BR>";
      }
      
      /*
      if ($AOI_ONE[$k][$x] > 0) {
        echo $AOI_ONE[$k][$x]." in ".$AOI_ONE[$k][0] . " (1)<BR>";
      }
      if ($AOI_TWO[$k][$x] > 0) {
        echo $AOI_TWO[$k][$x]." in ".$AOI_TWO[$k][0] . " (2)<BR>";
      }
      */

    }  

    echo "</td>";

    echo "</tr>";
    echo "<tr><td><hr></td></tr>";
    echo "</tr>";
  }

  echo "</table>";
}

function display_program_stats()
{

  global $AOI_ONE; 
  global $AOI_TWO;
  global $AOI_THREE;
  global $DECISION;

  echo " <table border=1>";

  echo "<tr>";
  $cell=-1;
  for ($k=1; $k<77; $k++)
  {
    if (strcmp($AOI_ONE[$k][0], "PLACEHOLDER") != 0){
    // new row every 3 
    $cell++;
    if ($cell == 3){
      echo "</tr><tr>";
      $cell=0;
    }
    echo "<td width=33%>";
    echo "<b>" .$AOI_ONE[$k][0]."</b><BR>";
    //echo " A1 ".$AOI_ONE[$k][1]."<BR>";
    echo " A1 " . ($AOI_ONE[$k][1] + $AOI_TWO[$k][1] + $AOI_THREE[$k][1]);
    echo " (" . $AOI_ONE[$k][1] . ", " . $AOI_TWO[$k][1] . ", " . $AOI_THREE[$k][1] . ")<BR>";
    //echo " A2 ".$AOI_ONE[$k][2]."<BR>";
    echo " A2 " . ($AOI_ONE[$k][2] + $AOI_TWO[$k][2] + $AOI_THREE[$k][2]);
    echo " (" . $AOI_ONE[$k][2] . ", " . $AOI_TWO[$k][2] . ", " . $AOI_THREE[$k][2] . ")<BR>";
    //echo " B1 ".$AOI_ONE[$k][3]."<BR>";
    echo " B1 " . ($AOI_ONE[$k][3] + $AOI_TWO[$k][3] + $AOI_THREE[$k][3]);
    echo " (" . $AOI_ONE[$k][3] . ", " . $AOI_TWO[$k][3] . ", " . $AOI_THREE[$k][3] . ")<BR>";
    //echo " B2 ".$AOI_ONE[$k][4]."<BR>";
    echo " B2 " . ($AOI_ONE[$k][4] + $AOI_TWO[$k][4] + $AOI_THREE[$k][4]);
    echo " (" . $AOI_ONE[$k][4] . ", " . $AOI_TWO[$k][4] . ", " . $AOI_THREE[$k][4] . ")<BR>";
    //echo " &nbsp; S ".$AOI_ONE[$k][5]."<BR>";
    echo " &nbsp; S " . ($AOI_ONE[$k][5] + $AOI_TWO[$k][5] + $AOI_THREE[$k][5]);
    echo " (" . $AOI_ONE[$k][5] . ", " . $AOI_TWO[$k][5] . ", " . $AOI_THREE[$k][5] . ")<BR>";
    //echo " &nbsp; R ".$AOI_ONE[$k][6]."<BR>";
    echo " &nbsp; R " . ($AOI_ONE[$k][6] + $AOI_TWO[$k][6] + $AOI_THREE[$k][6]);
    echo " (" . $AOI_ONE[$k][6] . ", " . $AOI_TWO[$k][6] . ", " . $AOI_THREE[$k][6] . ")<BR>";
    echo "<td>";
    }
  }
  echo "</tr>";
 
  echo "</table>";

}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Admission Committee Decision Breakdown';

$pageCssFiles = array(
    '../css/applicantGroups_assign.css' 
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

// period
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    
    if (is_array($periodId)) {
        // The scs period will be first and the compbio/mrsd period will be second
        $displayPeriodId = $periodId[1];    
    } else {
        $displayPeriodId = $periodId; 
    }

    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    if ($endDate == '') {
        $endDate = 'present';    
    }
    $applicationPeriodDisplay = '<br/>' . $startDate . '&nbsp;to&nbsp;' . $endDate;
    $description = $period->getDescription();
    if ($description) {
        $applicationPeriodDisplay .= ' (' . $description . ')';    
    }
}
?> 
<div id="pageHeading">
<div id="departmentName">
<?php
echo $deptName;
if ($applicationPeriodDisplay) {
    echo $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle"><?php echo $pageTitle; ?></div>
</div>
  <form id="form1" name="form1" action="" method="post">

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
    <div style="margin:7px; ">
    <?php
    if (is_array($periodId)) {
        $periodParam = 'period[]=' . implode('&period[]=', $periodId);    
    } else {
        $periodParam = 'period=' . $periodId;
    }
    ?>
    <a href="decision_breakdown.php?id=<?=$deptId?>&<?php echo $periodParam; ?>">Applicants by Decision</a>&nbsp;&nbsp;
    <a href="decision_breakdown_aoi.php?id=<?=$deptId?>&<?php echo $periodParam; ?>">Applicants by AOI / Decision</a>&nbsp;&nbsp;
    <b>Counts by Decision / AOI</b>&nbsp;&nbsp; 
    <br/><br/>

<?php
if ($show == 'all') {
    $allSelected = 'checked';
    $admittedSelected = '';    
} else {
    $allSelected = '';
    $admittedSelected = 'checked';
}
?>    
<b>Show:</b>
<input type="radio" name="show" value="all" <?php echo $allSelected; ?>> All 
<input type="radio" name="show" value="admitted" <?php echo $admittedSelected; ?>> Admitted Only
&nbsp;&nbsp;<input type="submit" value="Change" style="font-size: 10px;" />
<br/><br/>

<?php

// Main
build_AOI_Arrays();
collect_decision_stats($deptId, $periodId);
display_decision_stats();
display_program_stats();

?>
    </div>
    </td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>