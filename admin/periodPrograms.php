<?php
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Applyweb_Table.php'; 

include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Program.php';
include '../classes/DB_Applyweb/class.DB_ProgramType.php';
include '../classes/DB_Applyweb/class.DB_UnitRole.php'; 

include '../classes/DB_Applyweb/class.DB_Programs.php';
include '../classes/DB_Applyweb/class.DB_ProgramsUnit.php';
include '../classes/DB_Applyweb/class.DB_Department.php';
include '../classes/DB_Applyweb/class.DB_DepartmentUnit.php';
include '../classes/DB_Applyweb/class.DB_Domain.php';
include '../classes/DB_Applyweb/class.DB_DomainUnit.php';

include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_UnitPeriod.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/DB_Applyweb/class.DB_PeriodType.php';  
include '../classes/DB_Applyweb/Table/class.DB_PeriodUmbrella.php'; 

include '../classes/DB_Applyweb/Table/class.DB_ProgramGroup.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupProgram.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupType.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupGroupType.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupRole.php';

include '../classes/Unit/class.UnitBase.php';
include '../classes/Unit/class.Unit.php';
include '../classes/Unit/class.Program.php';

include '../classes/Period/class.PeriodBase.php';
include '../classes/Period/class.SubPeriod.php';
include '../classes/Period/class.UmbrellaPeriod.php';
include '../classes/Period/class.Period.php';
include '../classes/Period/class.ProgramGroup.php'; 

include '../classes/class.User.php';

require("HTML/QuickForm.php");
include '../classes/HTML_QuickForm/Period/class.NewProgramGroup_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.ProgramGroup_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.ProgramGroupProgram_QuickForm.php';

/*
* Handle the request variables.
*/
$edit = NULL;
if ( isset($_REQUEST['edit']) ) {
    $edit = $_REQUEST['edit'];
}

$period = $periodId = NULL;
$unit = $unitId = NULL;
if ( isset($_REQUEST['period_id']) &&  $_REQUEST['period_id'] != '' ) {

    $periodId = $_REQUEST['period_id'];
    $period = new Period($periodId);
    $unitId = $period->getUnitId();
    $unit = new Unit($unitId);

} else {
    $header = 'Location: https://' . $_SERVER['SERVER_NAME'];
    $header .= str_replace('periodPrograms.php', 'units.php', $_SERVER['PHP_SELF']);
    header($header);
    exit;    
}

$programGroupId = NULL;
if ( isset($_REQUEST['program_group_id']) && $_REQUEST['program_group_id'] ) {
    $programGroupId = $_REQUEST['program_group_id'];
    $programGroup = new ProgramGroup($programGroupId);
}

if ( $periodId  && !$programGroupId ) {
   
    if ( ( $edit != 'newProgramGroup'  && $edit != 'programGroup' )
        || ( isset($_REQUEST['submitProgramGroup']) && $_REQUEST['submitProgramGroup'] == 'Cancel') ) 
    {
        
        // This is the "default" request for the period, so get the latest programGroupId.
        $query = "SELECT * FROM program_group WHERE period_id = " . $periodId;
        $query .= " ORDER BY program_group_id LIMIT 1";
        $result = mysql_query($query);
        while ($row = mysql_fetch_array($result)) {
            $programGroupId = $row['program_group_id'];    
        }
    }
    
    if ($programGroupId) {
        $programGroup = new ProgramGroup($programGroupId);
    } else {
        $programGroup = new ProgramGroup(NULL, $periodId);
    }
    
}

/* 
* Get the user's permissions.
*/
$user = new User();
$user->loadFromSession();
$userRole = $user->getUserRole();

$allAdminUnitIds = $rootAdminUnitIds = $user->getAdminUnits();
$rootUnitDescendantIds = array();
foreach ($rootAdminUnitIds as $rootUnitId) {    
    if (!$unitId && !$edit) {
        $unitId = $rootUnitId;
        $unit = new Unit($unitId);
    }
    $rootUnit = new Unit($rootUnitId);
    $rootUnitDescendantIds = array_merge( $rootUnitDescendantIds, $rootUnit->getDescendantUnitIds() );
}
$allAdminUnitIds = array_merge($allAdminUnitIds, $rootUnitDescendantIds);

$allowAdmin = FALSE;
if ( ($userRole == 'Administrator' || $userRole == 'Super User') 
    && ( ($unitId && in_array($unitId, $allAdminUnitIds) ) || $edit)  ) 
{
    $allowAdmin = TRUE;        
}

/*
* If the user doesn't have admin rights for the unit, include the "view" template and exit.
*/
if (!$allowAdmin) { 
    include '../inc/tpl.periods.php';
    exit;
}

/*
* Otherwise... handle the "add new program group" form (only submits edit=newPeriod). 
*/
$newProgramGroupForm = new NewProgramGroup_QuickForm();
$newProgramGroupForm->handleSelf($period);
$newProgramGroupFormSubmitted = $newProgramGroupForm->isSubmitted();

/*
* Always handle the form for the program group.
*/
//DebugBreak();
$programGroupForm = new ProgramGroup_QuickForm();
$programGroupFormValid = $programGroupForm->handleSelf($programGroup, $period, $unit);
if ($programGroupFormValid && !$programGroupId) {
    // Redirect to the new program group.
    $newProgramGroupId = $programGroup->getId();
    $header = 'Location: https://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
    $header .= '?period_id=' . $period->getId(); 
    if ($newProgramGroupId) {
        $header .= '&program_group_id=' . $newProgramGroupId;    
    }
    header($header); 
    exit;
}

/*
* If working on a new period, stop after setting up the period form.
*/
if ($newProgramGroupFormSubmitted ) {
    // Set the period form defaults
    //$umbrellaForm->setDefaults( array('unit_id' => $unitId, 'period_type_id' => 1) ); 
    // Include the "view" template and exit.
    include '../inc/tpl.periodPrograms.php';
    exit;
}

/*
* Otherwise... handle the form for period_program table elements.
*/
$programGroupProgramForm = new ProgramGroupProgram_QuickForm();
$programGroupProgramForm->handleSelf($programGroup);

/*
* Temporary: get role arrays. 
*/
$roleUsers = array();
if ($programGroupId) {
    $DB_Applyweb = new DB_Applyweb();
    $roleUserQuery = "SELECT users.id AS users_id,
                        users.firstname,
                        users.lastname,
                        program_group_role.role_id
                        FROM program_group_role 
                        INNER JOIN users ON program_group_role.users_id = users.id
                        WHERE program_group_role.program_group_id = " . $programGroupId . "
                        ORDER BY users.lastname";
    $roleUsers = $DB_Applyweb->handleSelectQuery($roleUserQuery);
}
$admins = array();
$chairs = array();
$committee = array();
foreach ($roleUsers as $roleUser) {
    switch ($roleUser['role_id']) {
        case 1:
            $admins[] = $roleUser;
            break;    
        case 2:
            $chairs[] = $roleUser;
            break;
        case 3:
            $committee[] = $roleUser;
            break; 
        default:
    }
}
 
                    
/*
*  Include the "view" template.
*/
include '../inc/tpl.periodPrograms.php'; 
?>