<?php
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
?>
<head>
<link rel="stylesheet" media="screen" href="../css/myTable.css"/>
</head>
<table>
  <thead>
    <tr>
      <th>App Id</th>
      <th>Resume</th>
      <th>Statement</th>
      <th>GRE</th>
      <th>Transcript</th>
      <th>TOEFL</th>
      <th>Recommendations</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-column="First Name">James</td>
      <td data-column="Last Name">Matman</td>
      <td data-column="Job Title">Chief Sandwich Eater</td>
      <td data-column="Twitter">@james</td>
    </tr>
    <tr>
      <td data-column="First Name">Andor</td>
      <td data-column="Last Name">Nagy</td>
      <td data-column="Job Title">Designer</td>
      <td data-column="Twitter">@andornagy</td>
    </tr>
    <tr>
      <td data-column="First Name">Tamas</td>
      <td data-column="Last Name">Biro</td>
      <td data-column="Job Title">Game Tester</td>
      <td data-column="Twitter">@tamas</td>
    </tr>
    <tr>
      <td data-column="First Name">Zoli</td>
      <td data-column="Last Name">Mastah</td>
      <td data-column="Job Title">Developer</td>
      <td data-column="Twitter">@zoli</td>
    </tr>
    <tr>
      <td data-column="First Name">Szabi</td>
      <td data-column="Last Name">Nagy</td>
      <td data-column="Job Title">Chief Sandwich Eater</td>
      <td data-column="Twitter">@szabi</td>
    </tr>
  

<?php

$grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflcscores = array();
$toeflpscores = array();
$myInstitutes = array();

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period'];
}
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';
    } else {
        $applicationPeriodDisplay .= $periodDates;
    }
}

$programId = 23;
function getRecommenders($appid)
{
    global $buckley;
    $ret = array();
    $letter = "";
    //RETRIEVE USER INFORMATION
    $sql = "SELECT
    recommend.id,
    recommend.rec_user_id,
    recommend.datafile_id,
    recommend.submitted,
    recommend.recommendtype,
    users.title,
    users.firstname,
    users.lastname,
    users.email,
    users_info.address_perm_tel,
    lu_users_usertypes.id as uid,
    users_info.company,
    reminder_sent_count,
    datafileinfo.moddate,
    datafileinfo.size,
    datafileinfo.extension,
    recommendationtypes.name
    FROM recommend
    left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
    left outer join users on users.id = lu_users_usertypes.user_id
    left join users_info on users_info.user_id = lu_users_usertypes.id
    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
    left outer join recommendationtypes on recommendationtypes.id = recommend.recommendtype
    where application_id=".$appid. " order by recommend.id";
    $result = mysql_query($sql) or die(mysql_error().$sql);
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['rec_user_id']);
        array_push($arr, $row['firstname']);
        array_push($arr, $row['lastname']);
        array_push($arr, $row['title']);
        array_push($arr, $row['company']);
        array_push($arr, $row['email']);
        array_push($arr, $row['address_perm_tel']);
        array_push($arr, $row['recommendtype']);
        array_push($arr, $row['datafile_id']);
        array_push($arr, $row['moddate']);
        array_push($arr, $row['size']);
        array_push($arr, $row['extension']);
        array_push($arr,$row['reminder_sent_count']);
        array_push($arr,$row['name']);
        array_push($arr, 0);//ARRAY INFO
        array_push($ret, $arr);
    }

    $sql = "select buckleywaive from application where id=".$appid;
    $result = mysql_query($sql) or die(mysql_error().$sql);
    while($row = mysql_fetch_array( $result ))
    {
        $buckley = $row['buckleywaive'];
    }
    return $ret;
}





try {
        $bfconnectionString = "mysql:host=" . $db_host . ";dbname=" .  $db;

        $pdo = new PDO($bfconnectionString, $db_username, $db_password );
    }
    catch(PDOException $e)
    {
    echo $e->getMessage();
    }

$stmt = $pdo->prepare('select u.id, luu.id as luuid, u.guid, a.id as appid from application a
         inner join period_application pa on pa.application_id = a.id and pa.period_id = :periodId
         inner join lu_users_usertypes luu on luu.id = a.user_id
         inner join users u on u.id = luu.user_id
         where a.submitted = 1 and (a.paid = 1 or a.waive = 1)');

$stmt->execute(array(':periodId' => $periodId));

while ($row = $stmt->fetch())

{
    $uid = $row['luuid'];
    $umasterid = $row['id'];
    $appid = $row['appid'];
    $guid = $row['guid'];
    $grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflcscores = array();
$toeflpscores = array();
$myInstitutes = array();
    $sql = "SELECT
    usersinst.id,institutes.name,date_entered,
    date_grad,date_left,degreesall.name as degree,
    major1,major2,major3,minor1,minor2,gpa,gpa_major,
    gpascales.name as gpa_scale,transscriptreceived,datafile_id,educationtype,
    datafileinfo.moddate,
    datafileinfo.size,
    datafileinfo.extension
    FROM usersinst
    left outer join institutes on institutes.id = usersinst.institute_id
    left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
    left outer join gpascales on gpascales.id = usersinst.gpa_scale
    left outer join degreesall on degreesall.id = usersinst.degree
    where usersinst.application_id=".$appid. " order by usersinst.educationtype";
    $result = mysql_query($sql) or die(mysql_error());
    $comp = true;
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['name']);
        if($row['name'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['date_entered'],'-','/')  );
        if($row['date_entered'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['date_grad'],'-','/') );
        if($row['date_grad'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['date_left'],'-','/') );
        array_push($arr, $row['degree']);
        if($row['degree'] == ""){$comp = false; }
        array_push($arr, $row['major1']);
        array_push($arr, $row['major2']);
        array_push($arr, $row['major3']);
        array_push($arr, $row['minor1']);
        array_push($arr, $row['minor2']);
        array_push($arr, $row['gpa']);
        if($row['gpa'] == ""){$comp = false; }
        array_push($arr, $row['gpa_major']);
        if($row['gpa_major'] == ""){$comp = false; }
        array_push($arr, $row['gpa_scale']);
        if($row['gpa_scale'] == ""){$comp = false; }
        array_push($arr, $row['transscriptreceived']);
        array_push($arr, $row['datafile_id']);
        if($row['datafile_id'] == ""){$comp = false; }
        array_push($arr, $row['educationtype']);
        array_push($arr, $row['moddate']);
        array_push($arr, $row['size']);
        array_push($arr, $row['extension']);
        array_push($myInstitutes, $arr);
    }
//GET SCORES

//LOOK FOR GRE GENERAL SCORE
$sql = "select grescore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from grescore
left outer join datafileinfo on datafileinfo.id = grescore.datafile_id
where application_id = " .$appid;
// debugbreak();
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['verbalscore']);
    array_push($arr, $row['verbalpercentile']);
    array_push($arr, $row['quantitativescore']);
    array_push($arr, $row['quantitativepercentile']);
    array_push($arr, $row['analyticalwritingscore']);
    array_push($arr, $row['analyticalwritingpercentile']);
    array_push($arr, $row['analyticalscore']);
    array_push($arr, $row['analyticalpercentile']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($grescores,$arr);

}

//LOOK FOR TOEFL IBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'IBT'";
// debugbreak();
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    //echo $row['essay'];
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toefliscores,$arr);
}
//LOOK FOR TOEFL CBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'CBT'";
// debugbreak();
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflcscores,$arr);
}
//LOOK FOR TOEFL PBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'PBT'";
// debugbreak();
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflpscores,$arr);
}

//  REcomenders
$myRecommenders = getRecommenders($appid);
    ?>
    <tr>
      <td data-column="App Id"><?php echo $appid ?></td>
      <td data-column="Resume"><?php $resumeFilePath=getFilePath(2, 1, $uid, $umasterid, $appid);
        if ($resumeFilePath !== null) { ?>
            echo '<a target="_blank" href="<?=$resumeFilePath?>" ><?php echo "Resume"?></a>
            <br />';
        <?
         } ?> </td>
      <td data-column="Resume"><?php   $statementFilePath=getFilePath(4, 1, $uid, $umasterid, $appid);
      if ($statementFilePath !== null) {?>
        <a target="_blank" href=<?=$statementFilePath?> >Statement of Purpose</a>
      <? } ?> </td>
      
      <td data-column="Resume"><?php for($i = 0; $i < count($grescores); $i++){ 
      $greFilePath=getFilePath(6, $grescores[$i][0], $uid, $umasterid, $appid);
              if ($greFilePath !== null) {?>
              <a target="_blank" href=<?=$greFilePath?> >(Scores)</a>
              <? }} ?> </td>
      <td data-column="Transcript"><?php  
      for($i = 0; $i < count($myInstitutes); $i++){
      $filePath=getFilePath(1, $myInstitutes[$i][0], $uid, $umasterid, $appid, $myInstitutes[$i][15]);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Transcript)</a>
              <? }} ?> </td>
      <td data-column="TOEFL"><?php 
              
              for($i = 0; $i < count($toefliscores); $i++){
         if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
            { ?>

                <table border=1 cellpadding=0 cellspacing=0>
                  <tr>
                    <td colspan="6" align=center bgcolor="c5c5c5"><strong>TOEFL IBT</strong><?
                      $filePath=getFilePath(7, $toefliscores[$i][0], $uid, $umasterid, $appid, $toefliscores[$i][11]);
                      if ($filePath !== null) {?>
                      <a target="_blank" href=<?=$filePath?> >(Scores)</a>
                      <? } ?>
                      </td>

                  </tr>
                  
                </table>
                <? } } ?>
                <? for($i = 0; $i < count($toeflcscores); $i++){
            if($toeflcscores[$i][1] != "00/0000" && $toeflcscores[$i][1] != "")
            { ?>

                <table border=1 cellpadding=0 cellspacing=0>
                  <tr>
                    <td colspan="6" align=center bgcolor="c5c5c5"><strong>TOEFL CBT</strong><?
                      $filePath=getFilePath(9, $toeflcscores[$i][0], $uid, $umasterid, $appid, $toeflcscores[$i][11]);
                      if ($filePath !== null) {?>
                      <a target="_blank" href=<?=$filePath?> >(Scores)</a>
                      <? } ?>
                      </td>
                  </tr>
                  
                </table>
                <? } } ?>
                <? for($i = 0; $i < count($toeflpscores); $i++){
            if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
            {  ?>

                <table border=1 cellpadding=0 cellspacing=0>
                  <tr>
                    <td colspan="6" align=center bgcolor="c5c5c5"><strong>TOEFL PBT</strong><?
                      $filePath=getFilePath(10, $toeflpscores[$i][0], $uid, $umasterid, $appid, $toeflpscores[$i][11]);
                      if ($filePath !== null) {?>
                      <a target="_blank" href=<?=$filePath?> >(Scores)</a>
                      <? } ?>
                      </td>
                  </tr>
                  
                </table>
                <? }  } ?> 
      </td>
      <td data-column="Recommendations"> <table><?php
        
        for($i = 0; $i < count($myRecommenders); $i++){
      if($myRecommenders[$i][2] != "") {
      ?>
      <tr>
        <td>
        <? if($myRecommenders[$i][9] != ""){
            $recFilePath = getFilePath(3, $myRecommenders[$i][0], $uid, $umasterid, $appid, $myRecommenders[$i][9]);
            ?>
            <input name="txtRecId" type="hidden" value="<?=$myRecommenders[$i][9]?>">
            <? if($recFilePath != ""){ ?>
                <a href="<?=$recFilePath?>" target="_blank">
                    <?=$myRecommenders[$i][2]?> <?=$myRecommenders[$i][3]?>
                </a><?
                
            }}} ?> </td> </tr>  <?php  } ?> </table>
 
</tr>
<?php
      
// DebugBreak();
 
}

?>
</tbody>
</table>
</body>
</html>