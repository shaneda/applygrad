<?php
//DebugBreak();
header("Cache-Control: no-cache");

// include the config class
include("../inc/config.php");
//DebugBreak();
// db functions
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Payment.php";
include "../classes/DB_Applyweb/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php'; 
include "../classes/class.db_education.php";
include "../classes/class.db_publications.php";
include "../classes/class.db_recommendations.php";
include "../classes/class.db_test_scores.php";

// display functions
//include "../inc/administerApplicationsSingle.inc.php";

// Special cases.
include '../inc/specialCasesAdmin.inc.php';

// get the request arguments 
$applicationId = NULL;
if ( isset($_REQUEST['applicationId']) ) {
    $applicationId = $_REQUEST['applicationId'];
}
$page = $_GET['page'];

$unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
if ($unit == 'department') {
    $departmentId = $unitId;    
} else {
    $departmentId = NULL;
} 

switch($page) {

    case "testscores":
    case "greScore":
    case "greSubjectScore":
    case "toeflScore":
    case "ieltsScore":
    case "gmatScore":
    case "waiveToefl":

        $dbTestScores = new DB_TestScores();
        $greRecords = $dbTestScores->getGREGeneral($applicationId);
        $greSubjectRecords = $dbTestScores->getGRESubject($applicationId);
        $toeflRecords = $dbTestScores->getTOEFL($applicationId);
        $ieltsRecords = $dbTestScores->getIELTS($applicationId);
        $gmatRecords = $dbTestScores->getGMAT($applicationId);
        $waiveToefl = $dbTestScores->getWaiveToefl($applicationId);
        //displayTestScores($greRecords, $greSubjectRecords, $toeflRecords);
        include '../inc/tpl.administerApplicationsSingleTestScores.php';
        
        break;

    case "transcripts":

        $dbEducation = new DB_Education();
        $educationRecords = $dbEducation->getAll($applicationId);
        //displayEducation($educationRecords);
        include '../inc/tpl.administerApplicationsSingleTranscripts.php';
        break;

    case "recommendations":
        $recommendations = new DB_Recommendations();
        $recommendationRecords = $recommendations->getAll($applicationId);
        //displayRecommendations($recommendation_records);
        include '../inc/tpl.administerApplicationsSingleRecommendations.php';
        break;    
        
    case "payment":
    
        // Get the higher fee date for the application period.
        $DB_Applyweb = new DB_Applyweb();
        $higherFeeDateQuery = "SELECT higher_fee_date
            FROM period_umbrella
            INNER JOIN period_application ON period_umbrella.period_id = period_application.period_id
            WHERE period_application.application_id = " . $applicationId;
        $higherFeeDateRecord = $DB_Applyweb->handleSelectQuery($higherFeeDateQuery);
        $higherFeeDate = NULL;
        if (isset($higherFeeDateRecord[0]['higher_fee_date'])) {
            $higherFeeDate = $higherFeeDateRecord[0]['higher_fee_date'];    
        }    

        $paymentManager = new PaymentManager($applicationId, $higherFeeDate);
        include '../inc/tpl.administerApplicationsSinglePayment.php';
        break;

    case "publications":
        $publications = new DB_Publications();
        $publication_records = $publications->getPublications($applicationId);
        displayPublications($publication_records);
        break;

    case "copy":
    
        $DB_Applyweb = new DB_Applyweb();
        include '../inc/tpl.administerApplicationsSingleSuCopy.php';
        break;
        
    case "notes":
    
        $DB_Applyweb = new DB_Applyweb();
        
        $hideApplication = 0;
        $hideQuery = "SELECT hide FROM application WHERE application.id = " .  $applicationId;
        $hideRecords = $DB_Applyweb->handleSelectQuery($hideQuery);
        if (isset($hideRecords[0]['hide'])) {
            $hideApplication = $hideRecords[0]['hide'];
        }
        
        $noteQuery = "SELECT insert_time, note,
            CONCAT(users.firstname, ' ', users.lastname) AS insert_user_name
            FROM application_admin_note
            INNER JOIN users ON application_admin_note.insert_user_id = users.id
            WHERE application_admin_note.application_id = " . $applicationId . "
            ORDER BY application_admin_note.insert_time DESC";
        $noteRecords = $DB_Applyweb->handleSelectQuery($noteQuery);
        
        include '../inc/tpl.administerApplicationsSingleNotes.php';
        
        break;

    default:
    
        print "Admin case not found.";

}

?>