<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$sql = "";
$err = "";
$states = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				mysql_query("DELETE FROM states WHERE id=".$itemId)
				or die(mysql_error());
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select states.id,states.name, states.abbrev, countries.name as country
from states
left outer join countries on countries.id = states.country_id
order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['abbrev']);
	array_push($arr, $row['country']);
	array_push($states, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'States';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">States</span><br />
<br />
	<a href="stateEdit.php">Add state</a><br />
	<br />
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
		<td>Abbrev</td>
		<td>Country</td>
        <td width="60" align="right">Delete</td>
      </tr>
     <? for($i = 0; $i < count($states); $i++){?>
	<tr>
        <td><a href="stateEdit.php?id=<?=$states[$i][0]?>"><?=$states[$i][1]?></a></td>
		<td><?=$states[$i][2]?></td>
		<td><?=$states[$i][3]?></td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$states[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	<? } ?>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>