<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/adminTemplate2.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/AdminStyles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" name="form1" action="" method="post"><!-- InstanceEndEditable -->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Round 1 Groups<!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<a href="javascript: document.location = document.location;" >Refresh</a><br />
	<br />



  <?
  $sql = "SELECT distinct revgroup.id, revgroup.name FROM revgroup 
  inner join lu_application_groups on lu_application_groups.group_id = revgroup.id ";
	$result = mysql_query($sql) or die(mysql_error());
	$j = 0;
	while ($row = mysql_fetch_array($result))
	{
		echo "<table  border='0' cellpadding='2' cellspacing='0' align='left'><tr><td class='tblHead'>".$row["name"]."</td></tr>";
		$sql = "SELECT
		lu_application_groups.group_id,
		count(lu_application_groups.group_id) as count,
		countries.name as citizenship
		FROM lu_application_groups
		left outer join application on application.user_id = lu_application_groups.application_id
		left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
		left outer join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id

		where  (users_info.cit_country= 45 or users_info.cit_country= 101)
		and lu_application_groups.group_id = ".intval($row["id"])." and application.submitted = 1
		group by users_info.cit_country";
		$result1 = mysql_query($sql) or die(mysql_error());
		echo "<tr><td><table width='100%' border='0' cellspacing='0' cellpadding='1'>";
			while ($row1 = mysql_fetch_array($result1))
			{
			if($j % 2 == 0){
				$altClass ="tblItem";
			}else{
				$altClass ="tblItemAlt";
			}
			echo "<tr>
			<td class='".$altClass."'>".$row1["citizenship"]."</td>
			<td class='".$altClass."'>".$row1["count"]."</td>
			</tr>";
			$j++;
		}
		echo "<td class='tblItem' colspan='2'><hr></td>";
		
		$sql = "SELECT
		lu_application_groups.group_id,
		count(lu_application_groups.group_id) as count,
		if(users_info.gender = 'F','Female non-Indian or Asian','Male') as gender
		FROM lu_application_groups
		left outer join application on application.user_id = lu_application_groups.application_id
		left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
		left outer join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		
		where  
		users_info.gender = 'F' and
		(users_info.cit_country != 101 
		or users_info.cit_country != 45) 
		and lu_application_groups.group_id = '".$row["id"]."'
		and application.submitted = 1
		group by users_info.gender";
		$result1 = mysql_query($sql) or die(mysql_error());
		while ($row1 = mysql_fetch_array($result1))
		{
			if($j % 2 == 0){
				$altClass ="tblItem";
			}else{
				$altClass ="tblItemAlt";
			}
			echo "<tr >
			<td class='".$altClass."'>".$row1["gender"]."</td>
			<td class='".$altClass."'>".$row1["count"]."</td>
			</tr>";
			$j++;
		}
		
		$sql = "SELECT
		lu_application_groups.group_id,
		count(lu_application_groups.group_id) as count,
		if(users_info.gender = 'F','Female All','Male') as gender
		FROM lu_application_groups
		left outer join application on application.user_id = lu_application_groups.application_id
		left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
		left outer join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		
		where  users_info.gender = 'F'  and lu_application_groups.group_id = '".$row["id"]."'
		and application.submitted = 1
		group by users_info.gender";
		$result1 = mysql_query($sql) or die(mysql_error());
		while ($row1 = mysql_fetch_array($result1))
			{
			if($j % 2 == 0){
				$altClass ="tblItem";
			}else{
				$altClass ="tblItemAlt";
			}
			echo "<tr>
			<td class='".$altClass."'>".$row1["gender"]."</td>
			<td class='".$altClass."'>".$row1["count"]."</td>
			</tr>";
			$j++;
		}
		echo "<td class='tblItem' colspan='2'><hr></td>";
		
		$thisAoI = "";
		$lastAoI = "NULL";
		$aAoI = array();
		$sql = "SELECT distinct
			users.id,
			interest.name
			FROM lu_users_usertypes		
			left outer join users on users.id = lu_users_usertypes.user_id
			left outer join users_info on users_info.user_id = lu_users_usertypes.id
			left outer join application on application.user_id = lu_users_usertypes.id
			left outer join lu_application_programs on lu_application_programs.application_id = application.id
			left outer join programs on programs.id = lu_application_programs.program_id
			left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
			left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
			left outer join interest on interest.id = lu_application_interest.interest_id
			left outer join lu_application_groups on lu_application_groups.application_id = application.id

			

			where  (users_info.cit_country != 101 or users_info.cit_country != 45) and lu_application_groups.group_id = '".$row['id']."'
			and application.submitted = 1
			order by interest.name";
		$result1 = mysql_query($sql) or die(mysql_error());
		$count = 0;
		while ($row1 = mysql_fetch_array($result1))
		{
			$thisAoI = $row1['name'];
			if($thisAoI != "")
			{
				if($thisAoI == $lastAoI)
				{
					$aAoI[$count-1][1] = $aAoI[$count-1][1] + 1;
				}else
				{
					$arr = array($thisAoI, 1);
					array_push($aAoI, $arr);
					$count++;
				}
				$lastAoI = $thisAoI;
			}
		}
		for($i = 0; $i < count($aAoI); $i++)
		{
			if($j % 2 == 0){
				$altClass ="tblItem";
			}else{
				$altClass ="tblItemAlt";
			}
			echo "<tr>
			<td class='".$altClass."'>".truncate($aAoI[$i][0])."</td>
			<td class='".$altClass."'>".$aAoI[$i][1]."</td>
			</tr>";
			$j++;

		}
	
		echo "</table></td></tr></table>";
		$j++;

	}
  ?>
	
	
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
