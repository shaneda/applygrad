<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$deptId = -1;
$deptName = "";
$round = 1;
$sort = "name";
$showAllInterests = false;
$having = "";
$discuss = "";

if(isset($_GET['id']))
{
	$deptId = intval($_GET['id']);
}
if(isset($_GET['r']))
{
	$round = intval($_GET['r']);
}

if(isset($_GET['sort']))
{
	$sort = $_GET['sort'];
}else
{
	if(isset($_POST['sort']))
	{
		$sort = $_POST['sort'];
	}
}
if(isset($_POST['chkShowInt']))
{
	$showAllInterests = true;
}
$allApplicants = array();
$groups = array();
$applicants = array();

mysql_query("SET group_concat_max_len = 4096");
//GET ALL GROUPS AVAILABLE
$sql = "select revgroup.id, revgroup.name,department.name as dept
from revgroup
left outer join department on department.id = revgroup.department_id ";
if($deptId > -1)
{
	$sql .= " where department_id=".$deptId;
}else
{
	if($_SESSION['A_usertypeid'] != 0)
	{

		for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
		{
			if($i == 0)
			{
				$sql .= "where ";
			}else
			{
				$sql .= " or ";
			}
			$sql .= " department_id = ".$_SESSION['A_admin_depts'][$i];
		}
	}
}
$sql .= " order by name";

$result = mysql_query($sql) or die(mysql_error());
$i = 0;

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name'] . " - " . $row['dept']);
	array_push($groups, $arr);
	if($i > 0)
	{
	$deptName .= "<br>" ;
	}
	$deptName .= $row['dept'];
	$i++;
}


//GET ALL applicants AVAILABLE
$sql = "select application.id,
lu_users_usertypes.id as userid, firstname, lastname, countries.name as country,
countries.iso_code,
if(lu_application_interest.choice is null, 
	GROUP_CONCAT(distinct concat('<em>',degree.name, ' - ' ,fieldsofstudy.name, '</em>') SEPARATOR '<br>' ) , 
	GROUP_CONCAT(distinct concat('<em>',degree.name, ' - ' ,fieldsofstudy.name, ' (', lu_application_interest.choice+1, ')</em> - <strong>',  interest.name, '</strong>') ORDER BY lu_application_interest.choice SEPARATOR '<br>')
	) as interests,
application.round2,
GROUP_CONCAT(distinct revgroup.name SEPARATOR '<br>') as groups,
GROUP_CONCAT(distinct revgroup2.name SEPARATOR '<br>') as groups2
from  application
left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
left outer join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
left outer join countries on countries.id = users_info.address_perm_country
left outer join lu_application_programs on lu_application_programs.application_id = application.id
left outer join programs on programs.id = lu_application_programs.program_id
left outer join degree on degree.id = programs.degree_id
left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
left outer join interest on interest.id = lu_application_interest.interest_id 
left outer join lu_application_groups on lu_application_groups.application_id = application.id and lu_application_groups.round = 1
left outer join revgroup on revgroup.id = lu_application_groups.group_id 

left outer join lu_application_groups as lu_application_groups2  on lu_application_groups2.application_id = application.id and lu_application_groups2.round = 2
left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 


where (lu_users_usertypes.usertype_id > -1 ) and application.submitted=1 and application.id > 0";
/*

*/ 

if($deptId > -1)
{
	$sql .= "  and lu_programs_departments.department_id = ". $deptId;
}
else
{
	if($_SESSION['A_usertypeid'] != 0)
	{
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " and (";
		}
		for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
		{
			if($i > 0)
			{
				$sql .= " or ";
			}
			$sql .= " lu_programs_departments.department_id = ".$_SESSION['A_admin_depts'][$i];
		}
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " ) ";
		}
	}
}

switch($sort)
{
	case "name":
	$sql .= " group by application.id ".$having;
	$sql .= " order by lastname,firstname ";
	break;

	case "ctzn":
	$sql .= " group by application.id ".$having;
	$sql .= " order by countries.iso_code ";
	break;

	case "int1":
	//and (lu_programs_departments.department_id = 27) or (lu_programs_departments.department_id != 27	and lu_application_interest.choice = 0 )
	if($showAllInterests != true)
	{
		$sql .= " and ( lu_application_interest.choice = 0 or lu_application_interest.id IS NULL )
		group by lu_application_interest.interest_id, application.id ".$having."
		order by  lu_programs_departments.department_id, lu_application_programs.program_id ";
	}else
	{
		$sql .= " group by application.id ".$having."
		order by  lu_programs_departments.department_id, lu_application_programs.choice, lu_application_interest.interest_id, lu_application_interest.choice ";
	}
	break;
	
	case "int2":
	if($showAllInterests != true)
	{
		$sql .= " and ( lu_application_interest.choice = 1 or lu_application_interest.id IS NULL )
		group by lu_application_interest.interest_id, application.id ".$having."
		order by  lu_programs_departments.department_id, lu_application_programs.program_id ";
	}else
	{
		$sql .= " group by application.id ".$having."
		order by  lu_programs_departments.department_id, lu_application_programs.choice, lu_application_interest.interest_id, lu_application_interest.choice ";
	}
	break;
	
	case "int3":
	if($showAllInterests != true)
	{
		$sql .= " and ( lu_application_interest.choice = 2 or lu_application_interest.id IS NULL )
		group by lu_application_interest.interest_id, application.id ".$having."
		order by  lu_programs_departments.department_id, lu_application_programs.program_id ";
	}else
	{
		$sql .= " group by application.id ".$having."
		order by  lu_programs_departments.department_id, lu_application_programs.choice, lu_application_interest.interest_id, lu_application_interest.choice ";
	}
	break;

	case "grp":
	$sql .= " group by application.id ".$having . "
	order by lu_application_groups.group_id";
	break;
}

//echo $sql;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, "<a href='userroleEdit_student_formatted.php?v=1&r=1&id=".$row['userid']."' target='_blank'>".$row['lastname']. ", ".$row['firstname']."</a>" );
	array_push($arr, "<a href='javascript:;' title='".$row['country']."' >".$row['iso_code']."</a>");
	array_push($arr, $row['interests']);
	array_push($arr, $row['round2']);
	array_push($arr, "");//DISCUSS THIS CANDIDATE...
	array_push($arr, 0);//yes votes
	array_push($arr, 0);//no votes
	array_push($arr, $row['groups']);
	array_push($arr, $row['groups2']);
	array_push($allApplicants, $arr);
}

//DO SECONDARY QUERY TO WEED OUT NON-ROUND 2 VOTES
if($round == 2)
{
	$allApplicants2 = array();
	for($i = 0; $i < count($allApplicants); $i++)
	{
		$sql = "select review.id,
		CASE review.round2 WHEN 1 THEN 
			count(review.round2)
		END as yesVote,
		CASE review.round2 WHEN 0 THEN 
			count(review.round2)
		END as noVote,
		count(review.round2) as total
		 from review 
		inner join lu_users_usertypes on lu_users_usertypes.id = review.reviewer_id
		inner join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
		where application_id=".$allApplicants[$i][0];
		if($deptId > -1)
		{
			$sql .= "  and lu_user_department.department_id = ". $deptId;
		}
		else
		{
			if($_SESSION['A_usertypeid'] != 0)
			{
				if(count($_SESSION['A_admin_depts']) > 0)
				{
					$sql .= " and (";
				}
				for($j = 0; $j < count($_SESSION['A_admin_depts']); $j++)
				{
					if($j > 0)
					{
						$sql .= " or ";
					}
					$sql .= " lu_user_department.department_id = ".$_SESSION['A_admin_depts'][$j];
				}
				if(count($_SESSION['A_admin_depts']) > 0)
				{
					$sql .= " ) ";
				}
			}
		}
		$sql .= " group by review.application_id";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		while($row = mysql_fetch_array( $result ))
		{
			$display = false;
			$yes = intval($row['yesVote']);
			$no = intval($row['noVote']);
			$total = intval($row['total']);
			$allApplicants[$i][6] = $yes;
			$allApplicants[$i][7] = $no;
			
			if($yes + $no == $total)
			{
				if($yes == $total || $allApplicants[$i][4] == 1)
				{
					//UNANIMOUS OR OVERRIDDEN
					$display = true;
				}else
				{
					$display = false;
					if($yes == $no )
					{
						//TIED SO MEET TO DISCUSS
						$display = false;
						$allApplicants[$i][5] = "Yes";
						
					}
				}
			}
			//echo $allApplicants[$i][1]. " ".$row['id']. " yes: ".$row['yesVote']. " no: ".$row['noVote']." total: ".$row['total']."<br>";
			if($display == true)
			{
				//INSERT INTO THE $allApplicants2 ARRAY
				array_push($allApplicants2, $allApplicants[$i]);
			}
		}
	}
	$allApplicants = $allApplicants2;
}//end if round2

function getUserGroups($appId, $rnd=1)
{
	global $deptId;
	$ret = "";
	$sql = "select
	GROUP_CONCAT(revgroup.name SEPARATOR '<br>') as groups
	from  lu_application_groups
	left outer join revgroup on revgroup.id = lu_application_groups.group_id 
	where lu_application_groups.application_id = ".$appId . " and lu_application_groups.round=".$rnd;
	if($deptId > -1)
	{
		$sql .= " and revgroup.department_id=".$deptId;
	}else
	{
		if($_SESSION['A_usertypeid'] != 0)
		{
	
			for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
			{
				if($i == 0)
				{
					$sql .= "  and ( ";
				}else
				{
					$sql .= " or ";
				}
				$sql .= " revgroup.department_id = ".$_SESSION['A_admin_depts'][$i];
			}
			if(count($_SESSION['A_admin_depts']) > 0)
			{
				$sql .= " ) ";
			}
		}
	} 
	$sql .= " group by lu_application_groups.application_id";
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{
		$ret = $row['groups'];
	}
	return $ret;
}


//update
if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				mysql_query("DELETE FROM lu_application_groups WHERE id=".$itemId)	or die(mysql_error().$sql);
			}//END IF
		}//END IF DELETE
	}
}

if(isset($_POST['btnAdd']))
{
	foreach($vars as $key => $value)
		{
			$arr = split("_", $key);
			if(strstr($key, 'chkRev') !== false )
			{
				$tmpid = $arr[1];
				if($itemId != $tmpid || $tmpType != $arr[0])
				{
					$itemId = $tmpid;
					$tmpType=$arr[0];
					if($arr[0] == "chkRev")
					{
						$group = $_POST['lbGroup'];
						$doUpdate = true;
						$sql = "select id from lu_application_groups where application_id=".$itemId." and group_id=".$group." and round=".$round;
						$result = mysql_query($sql) or die(mysql_error());
						while($row = mysql_fetch_array( $result ))
						{
							$doUpdate = false;
						}
						if($doUpdate == true)
						{
							$sql = "insert into lu_application_groups(application_id,group_id, round)values(".$itemId.",".$group.", ".$round.")";
							mysql_query($sql)or die(mysql_error().$sql);
						}
					}
				}
			}
		}//END FOR
}

//GET applicants / GROUPS
$sql = "select lu_application_groups.id, firstname, lastname, revgroup.name, countries.name as country, groupDept.name as dept
from lu_application_groups
left outer join revgroup on revgroup.id = lu_application_groups.group_id
left outer join application on application.id = lu_application_groups.application_id
left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
left outer join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
left outer join countries on countries.id = users_info.cit_country

left outer join department as groupDept on groupDept.id = revgroup.department_id

where round = ".$round . " ";

if($deptId > -1)
{
	$sql .= " and revgroup.department_id = ". $deptId;
}
else
{
	if($_SESSION['A_usertypeid'] != 0)
	{
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " and (";
		}
		for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
		{
			if($i > 0)
			{
				$sql .= " or ";
			}
			$sql .= " revgroup.department_id = ".$_SESSION['A_admin_depts'][$i];
		}
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " ) ";
		}
	}
}
$sql .=" group by lu_application_groups.group_id, application.id order by revgroup.id, lastname, firstname";

//echo $sql;

$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['lastname']. ", ".$row['firstname'] . " - ".$row['country'] );
	array_push($arr, $row['name']. " - " .$row['dept']);
	array_push($applicants, $arr);
}
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" name="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Edit Applicant Assignments (Round <?=$round?>) <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td align="right"><table border="0" cellpadding="1" cellspacing="0">


	<?
	  $tmpGrp = "";
	  $count = 0;
	  $totalcount = 0;
	  for($i = 0; $i < count($applicants); $i++){
	  $count++;

	  if($applicants[$i][2] != $tmpGrp){
	  $totalcount +=$count;
	  $tmpGrp = $applicants[$i][2];

	  ?>
      <tr>
        <td>
		<? if($i > 0 ){

		echo $count;?> Users assigned

		<?
		$count = 0;
		 } ?><br />
		<strong><?=$applicants[$i][2]?></strong></td>
      </tr>
	  <? }//end if ?>
	  <? if($i == count($applicants)-1){ ?>
      <tr>
        <td>
		<?=$count?> Users assigned		</td>
      </tr>
	  <? } } ?>
	</table>
	<br /><?=$totalcount?>




      </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td class="tblHead">Available applicants </td>
        </tr>
      <tr>
        <td>Assign Checked users to group
           <? showEditText("", "listbox", "lbGroup", $_SESSION['A_allow_admin_edit'],true,$groups,true); ?>

           <? showEditText("Add", "button", "btnAdd", $_SESSION['A_allow_admin_edit']); ?>
           Show other choices <input name="chkShowInt" type="checkbox" value="1" <? if($showAllInterests==true){echo " checked ";} ?> />
           <? showEditText("Refresh", "button", "btnRefresh", $_SESSION['A_allow_admin_edit']); ?>
           <input name="sort" type="hidden" id="sort" value="<?=$sort?>" /></td>
        </tr>
      <tr>
        <td>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
		  <tr>
		    <td>&nbsp;</td>
			<td><strong><a href="applicantsGroups_assign.php?r=<?=$round?>&sort=name">Name</a></strong></td>
			<td><strong><a href="applicantsGroups_assign.php?r=<?=$round?>&sort=ctzn">CTZN</a></strong></td>
			<td><strong>Interests
			<a href="applicantsGroups_assign.php?r=<?=$round?>&sort=int1">1</a> 
			<a href="applicantsGroups_assign.php?r=<?=$round?>&sort=int2">2</a> 
			<a href="applicantsGroups_assign.php?r=<?=$round?>&sort=int3">3</a>
			</strong></td>
		    <? if($round == 2){ ?><td><strong>Totals</strong></td>
		    <? } ?>
		    <? if($round == 2){ ?><? } ?>
			<? if($round == 2){ ?><td><a href="applicantsGroups_assign.php?r=<?=$round?>&sort=ovr"><strong>Overridden</strong></a></td>
			<? } ?>
		    <td><strong><a href="applicantsGroups_assign.php?r=<?=$round?>&amp;sort=grp">Rnd 1 Groups</a></strong></td>
		    <? if($round == 2){ ?><td><strong><a href="applicantsGroups_assign.php?r=<?=$round?>&sort=grp">Rnd 2 Groups</a></strong></td><? } ?>
		  </tr>
		  <?

		  for($i = 0; $i < count($allApplicants); $i++)
		  {
		  $class = "tblItem";
		  if($i % 2 == 0)
		  {
			  $class = "tblItemAlt";
		  }
		  ?>
		  <tr class="<?=$class?>">
		    <td width="30"><?=$i+1?></td>
			<td width="150"><input name="chkRev_<?=$allApplicants[$i][0]?>" id="chkRev_<?=$allApplicants[$i][0]?>" type="checkbox" value="" /><?=$allApplicants[$i][1]?></td>
			<td width="35"><?=$allApplicants[$i][2]?></td>
			<td><?=$allApplicants[$i][3]?>&nbsp;</td>
		    <? if($round == 2){ ?><td>Yes:<?=$allApplicants[$i][6]?> No:<?=$allApplicants[$i][7]?></td><? } ?>
		    <? if($round == 2){ ?><? } ?>
			<? if($round == 2){ ?><td><? if($allApplicants[$i][4]== 1){echo "yes";}?></td><? } ?>
		    <td><?=$allApplicants[$i][8]?></td>
		    <? if($round == 2){ ?><td><?=$allApplicants[$i][9]?>&nbsp;</td><? } ?>
		  </tr>
		  <? } ?>
		</table>        </tr>
    </table></td>
    <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tr>
        <td colspan="2" class="tblHead">Round <?=$round?> Groups </td>
      </tr>
	  <?
	  $tmpGrp = "";
	  $count = 0;
	  for($i = 0; $i < count($applicants); $i++){
	  $count++;
	  if($applicants[$i][2] != $tmpGrp){
	  $tmpGrp = $applicants[$i][2];

	  ?>
      <tr>
        <td colspan="2">
		<? if($i > 0 ){ ?>
		<?=$count?> Users assigned
		<hr size="1" />
		<?
		$count = 0;
		 } ?>
		<strong><?=$applicants[$i][2]?></strong></td>
      </tr>
	  <? }//end if ?>
      <tr>
        <td><?=$applicants[$i][1]?></td>
        <td align="right"><? showEditText("X", "button", "btnDelete_".$applicants[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	  <? if($i == count($applicants)-1){ ?>
      <tr>
        <td colspan="2">
		<?=$count?> Users assigned
		<hr size="1" /></td>
      </tr>
	  <? } } ?>
    </table>
      <br /></td>
  </tr>
</table>
<br />
	<br />
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
