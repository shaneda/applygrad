<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

// PLB added scsUser, WebISO functions 6/30/09
include '../inc/sysusersEdit.inc.php';
include_once '../inc/prev_next.php';

/*
* Run through the main controller logic
*/
if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
	//header("Location: index.php");
}

$err = "";
$roles = array();
$usertypes = array();
$departments = array();
$department = array();
$firstname = "";
$middlename = "";
$lastname = "";
$email = "";
$pass = "";
$uid = -1;
$umasterid = -1;
$updated = false;
$info ="";
$guid = "";

// USER TYPES
$sql = "SELECT * FROM usertypes order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	if($_SESSION['A_usertypeid'] == 1 && $row['id'] == "0")
	{
	}
    else
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, trim($row['name']));
		array_push($usertypes, $arr);
	}
}

// USER TYPES
$sql = "SELECT * FROM department order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, trim($row['name']));
	array_push($departments, $arr);
}

// Declare empty $remoteAuthStrings array to avoid error on new user.
$remoteAuthStrings = array();

if(isset($_POST['btnAddRole']))
{
	$umasterid = $_POST['txtMasterid'];
	$firstname = htmlspecialchars($_POST['txtFirstname']);
	$middlename = htmlspecialchars($_POST['txtMiddlename']);
	$lastname = htmlspecialchars($_POST['txtLastname']);
	$email = htmlspecialchars($_POST['txtEmail']);
	$pass = sha1(htmlspecialchars($_POST['txtPass']));
    
    $remoteAuthStringsRaw = filter_input(INPUT_POST, 'remoteAuthStrings', FILTER_UNSAFE_RAW);
    $remoteAuthStrings = explodeNewline($remoteAuthStringsRaw);
    
	if($_POST['txtMasterid'] != -1)
	{
		$sql = "insert into lu_users_usertypes(user_id, usertype_id)values(".$_POST['txtMasterid'].", -1) ";
		mysql_query($sql) or die(mysql_error().$sql);
		$uid = mysql_insert_id();

		$sql = "insert into users_info(user_id )values(".$uid.") ";
		mysql_query($sql) or die(mysql_error().$sql);
	}
    else
	{
		$err .= "You must save this user first before adding roles.<br>";
	}
}

if( isset( $_POST ) && !isset($_POST['btnSubmit'])  && !isset($_POST['btnAddRole']))
{
	$vars = $_POST;
	$itemId = -1;
	$doDelete = false;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDel') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			$doDelete = true;
		}
	}//END FOR
	if($doDelete == true && $itemId != "")
	{
		$usertype = -1;
		if(isset($_POST["lbUserType_".$itemId])){
			$usertype = $_POST["lbUserType_".$itemId];
		}
		switch($usertype)
		{
			case "1":
			    break;
			case "2":
			    break;
			case "3":
			    break;
			case "4":
			    break;
			case "5"://STUDENT
				$sql = "DELETE FROM usersinst WHERE user_id=". $itemId;
				mysql_query($sql) or die(mysql_error());

				$appid = -1;
				$sql = "SELECT application_id from FROM application where user_id=".$itemId;
				$result = mysql_query($sql) or die(mysql_error());
				while($row = mysql_fetch_array( $result ))
				{
					$appid = $row['application_id'];
				}
				$sql = "DELETE FROM application WHERE id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM lu_application_advisor  WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM lu_application_appreqs  WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM lu_application_interest WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM lu_application_programs WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM publication WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM experience WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM fellowships WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM toefl WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM grescore WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM review WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM application WHERE user_id=". $itemId;
				mysql_query($sql) or die(mysql_error());

				$sql = "DELETE FROM recommend WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error());

				break;
                
			case "6"://RECOMMENDER
				$sql = "DELETE FROM recommend WHERE rec_user_id=". $itemId;
				mysql_query($sql) or die(mysql_error());
			    break;
		}//END SWITCH
        
        // Save role history before deletion.
        $historyQuery = "INSERT INTO lu_users_usertypes_history 
            (id, user_id, usertype_id, delete_users_id)
            SELECT id, user_id, usertype_id, " . $_SESSION['A_usermasterid'] . "
            FROM lu_users_usertypes WHERE id=" . $itemId;
        mysql_query($historyQuery);
        
		$sql = "DELETE FROM lu_users_usertypes WHERE id=" . $itemId;
		mysql_query($sql) or die(mysql_error().$sql);

		$sql = "DELETE FROM users_info WHERE user_id=" . $itemId;
		mysql_query($sql) or die(mysql_error().$sql);
	}

}//END IF

if(isset($_POST['btnSubmit']))
{
	$umasterid = $_POST['txtMasterid'];
	$uid = intval($_POST['txtUid']);
	$firstname = htmlspecialchars($_POST['txtFirstname']);
	$middlename = htmlspecialchars($_POST['txtMiddlename']);
	$lastname = htmlspecialchars($_POST['txtLastname']);
	$email = htmlspecialchars($_POST['txtEmail']);
	$pass = htmlspecialchars($_POST['txtPass']);
	$info = htmlspecialchars($_POST['txtInfo']);
	$guid = addslashes($_POST['txtGuid']);
    
    $remoteAuthStringsRaw = filter_input(INPUT_POST, 'remoteAuthStrings', FILTER_UNSAFE_RAW);
    $remoteAuthStrings = explodeNewline($remoteAuthStringsRaw);

	if($firstname == "")
	{
		$err .= "First name is required<br>";
	}
	if($lastname == "")
	{
		$err .= "Last name is required<br>";
	}
	if($email == "")
	{
		//$err .= "Email is required<br>";
	}
	if($email != "" && check_email_address($email)===false )
	{
		//$err .= "Email is invalid.<br>";
	}

	if($err == "")
	{
		if($umasterid != "-1")
		{
			$sql = "select password from users where id = ".$umasterid;
			$result = mysql_query($sql) or die(mysql_error());
			$createPass = false;
			while($row = mysql_fetch_array( $result ))
			{
				if($row['password'] == "")
				{
					$createPass = true;
					echo "No password, creating one";
				}
			}
			
			$sql = "select email from users where email = '".$email. "'";
			$result = mysql_query($sql) or die(mysql_error());
			$createPass = false;
			while($row = mysql_fetch_array( $result ))
			{
				//$err .= "Email exists in the database. Please choose another.<br>";
				break;
			}
			if($err == "")
			{
				$sql = "update users set
				firstname = '".addslashes($firstname)."',
				middlename = '".addslashes($middlename)."',
				lastname = '".addslashes($lastname)."',
				email = '".addslashes($email)."' ";
				if($pass != "")
				{
					$sql .= " ,password='".sha1($pass)."'" ;
				}else
				{
					//IF THERE IS NO PASSWORD ASSIGNED TO THIS USER, CREATE ONE
					if($createPass == true)
					{
						$sql .= " ,password='".sha1(trim(strtolower(addslashes($firstname))). "123")."'" ;
					}
				}
				if($guid == "")
				{
					$guid = makeGuid();
					$sql .= " ,guid='".$guid."'" ;
				}
				$sql .= " where id = ".$umasterid;
				mysql_query( $sql) or die(mysql_error());

				$sql = "update users_info set additionalinfo='".$info."' where user_id=".$uid;
				mysql_query( $sql) or die(mysql_error().$sql);
				$updated = true;
			}
		}
        else
		{
			if($pass == "")
			{
				$pass = sha1(STEM_GeneratePassword());
			}
			$doInsert = true;
			$sql = "select id from users where email='".$email."'";
			$result = mysql_query( $sql) or die(mysql_error());
			while($row = mysql_fetch_array( $result ))
			{
				//user already exists
				$doInsert = false;
				$pass = "";
			}
			if($doInsert  == true)
			{
				$sql = "insert into users(
				firstname,
				middlename,
				lastname,
				email,
				password)values(
				'".addslashes($firstname)."',
				'".addslashes($middlename)."',
				'".addslashes($lastname)."',
				'".addslashes($email)."',
				'".sha1($pass)."')";
				mysql_query( $sql) or die(mysql_error());
				$umasterid = mysql_insert_id();
				$updated = true;
			}
            else
			{
				$err .= "Account with this email address already exists.<br>";
			}
		}
	}
	//SAVE ROLES
	$vars = $_POST;
	$itemId = -1;
	$tmpid = -1;
	$arrItemId = array();
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'txtRoleId') !== false )
		{
			$arr = split("_", $key);
			$tmpid = $arr[1];
			if($itemId != $tmpid)
			{
				$itemId = $tmpid;
				array_push($arrItemId,$itemId);
			}
		}
	}//END FOR
	//ITERATE THROUGH IDS AND DO UPDATES
	//$err = "";
	for($i = 0; $i < count($arrItemId); $i++)
	{
		$department= htmlspecialchars($_POST["lbDepartment_".$arrItemId[$i]]);
		$olddept = htmlspecialchars($_POST["txtDeptId_".$arrItemId[$i]]);
		$usertype= htmlspecialchars($_POST["lbUsertype_".$arrItemId[$i]]);

		if($usertype == "" )
		{
			$err .="You must select a user type for item ".$i."<br>";
		}

		if($err == "")
		{
			$sql = "update lu_users_usertypes set usertype_id= ".$usertype." where id = ".$arrItemId[$i];
			mysql_query($sql) or die(mysql_error().$sql);

			$sql = "select id from users_info where user_id=".$arrItemId[$i];
			$result = mysql_query( $sql) or die(mysql_error());
			$doUpdate = false;
			while($row = mysql_fetch_array( $result ))
			{
				$doUpdate = true;
			}
			if($olddept != "")
			{
				$sql = "delete from lu_user_department where user_id = ".$arrItemId[$i] . " and department_id=".$olddept;
				mysql_query($sql) or die(mysql_error().$sql);
			}
			if($department != "")
			{
				if($usertype == "5" || $usertype == "6")
				{
					$err .= "Students and recommenders cannot be assigned to departments in item ".$i."<br>";
				}
				else
				{
					$sql = "insert into lu_user_department(user_id, department_id)values( ".$arrItemId[$i] . ", ".$department.")";
					mysql_query($sql) or die(mysql_error().$sql);
				}
			}
		}
	}
    
    // Save WebISO IDs/remote auth strings
    if( ($umasterid != -1) && ($err == "") ) {
        saveRemoteAuthStrings($umasterid, $remoteAuthStrings);
        deleteRemoteAuthStringOrphans($umasterid, $remoteAuthStrings);
    }
}

if(isset($_GET['id']))
{
    $umasterid = intval(htmlspecialchars($_GET['id']));

	//GET USER INFO
	$sql = "SELECT lu_users_usertypes.id,
	users.id as umasterid,
	users.firstname,
	users.middlename,
	users.lastname,
	users.email,
	users_info.additionalinfo,
	users.guid
	FROM users
	left outer join lu_users_usertypes on users.id = lu_users_usertypes.user_id
	left outer join users_info on users_info.user_id = lu_users_usertypes.id
	where users.id =".$umasterid;
	$result = mysql_query($sql)
	or die(mysql_error());

	while($row = mysql_fetch_array( $result ))
	{
		$firstname = stripslashes($row['firstname']);
		$middlename = stripslashes($row['middlename']);
		$lastname = stripslashes($row['lastname']);
		$email = stripslashes($row['email']);
		$uid = $row['id'];
		$umasterid = $row['umasterid'];
		$info = $row['additionalinfo'];
		$guid = $row['guid'];
	}
    
    $remoteAuthStrings = getRemoteAuthStrings($umasterid);
}

function getRoles($id)
{
	$sql = "select
	lu_users_usertypes.id,
	lu_users_usertypes.usertype_id,
	department.id as deptid,
	usertypes.name as typename
	from users
	left outer join lu_users_usertypes on lu_users_usertypes.user_id = users.id
	left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
	left outer join department on department.id = lu_user_department.department_id
	left outer join usertypes on usertypes.id = lu_users_usertypes.usertype_id
	where users.id=".$id." order by department.name, usertypes.name";
	$result = mysql_query($sql)	or die(mysql_error());
	$ret = array();
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();

		array_push($arr, $row['id']);
		array_push($arr, $row['usertype_id']);
		array_push($arr, $row['deptid']);
		array_push($arr, $row['typename']);
		array_push($ret, $arr);
	}
	return $ret;
}//END GETROLES

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit System User';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br />
<form accept-charset="utf-8" id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title" style="font-size: 18px;">Edit System User</span><br />
<br />

	<span class="errorSubtitle"><?=$err?></span>
	<? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1){ ?>
	<a href="sysusersEdit.php">Add Another User</a>
	<? } ?>
	<input name="txtMasterid" type="hidden" id="txtMasterid" value="<?=$umasterid?>" />
	<input name="txtUid" type="hidden" id="txtUid" value="<?=$uid?>" />
	<input name="txtGuid" type="hidden" id="txtGuid" value="<?=$guid?>" /> 
	<input class="tblItem" name="btnBack" type="button" id="btnBack" value="Back to Users" onclick="document.location='sysusers.php'" />
	
    <table width="700"  border="0" cellpadding="4" cellspacing="2">
      <tr>
        <td align="right"><strong>First Name: </strong></td>
        <td><span class="tblItem">
          <? showEditText($firstname, "textbox", "txtFirstname", $_SESSION['A_allow_admin_edit'],false,null,true,30); ?>
        </span></td>
        
        <td align="right" valign="top" rowspan="4"><strong>WebISO&nbsp;IDs:</strong></td>
        <td valign="top" rowspan="4">
            <span class="tblItem">
            <i>One per line, no separators, e.g.:<br>bovik@cs.cmu.edu<br>hbovik@andrew.cmu.edu</i>
            <?php
            // Echo textarea to prevent unwanted spaces
            echo '<textarea cols="30" rows="3" name="remoteAuthStrings" id="remoteAuthStrings">';
            foreach ($remoteAuthStrings as $remoteAuthString) {
                echo $remoteAuthString . "\n";
            }
            echo '</textarea>';
            ?>
            </span>
        </td>
        
      </tr>
      <tr>
        <td align="right"><strong>Middle Name: </strong></td>
        <td><span class="tblItem">
          <? showEditText($middlename, "textbox", "txtMiddlename", $_SESSION['A_allow_admin_edit'],false,null,true,30); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>Last Name: </strong></td>
        <td tabindex="3"><span class="tblItem">
          <? showEditText($lastname, "textbox", "txtLastname", $_SESSION['A_allow_admin_edit'],false,null,true,30); ?>
        </span></td>
        
      </tr>
      <tr>
        <td align="right"><strong>Email: </strong></td>
        <td><span class="tblItem">
          <?
		  $allowEdit = $_SESSION['A_allow_admin_edit'];
		  if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
		  {
		  	$allowEdit = false;
		  }
		  showEditText($email, "textbox", "txtEmail",  $allowEdit,false,null,true,30);
		  ?>
        </span></td>
        
      </tr>
      <tr>
        <td align="right"><strong>Additional Info:</strong> </td>
        <td colspan="3"><span class="tblItem">
          <? showEditText($info, "textbox", "txtInfo", $_SESSION['A_allow_admin_edit'],false,null,true,50); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>New Password</strong>: <em><br />
          (Leave blank to keep current password) </em></td>
        <td colspan="3"><span class="tblItem">

          <?
		  $tmpPass = $pass;
		  if(isset($_POST['btnSubmit']))
		  {
		  	$tmpPass = "";
		  }
		  showEditText("", "textbox", "txtPass", $_SESSION['A_allow_admin_edit']); ?>
		  <? if (isset($_POST['txtPass']) && isset($_POST['btnSubmit']))
		  {
		 	if($_POST['txtPass'] != "" && $updated == true)
			{
          		echo "<strong>Password set to <em>".$pass."</em>. Please make a note of this as the password cannot be retrieved later. </strong></span></td>";
			}
		   } ?>      </tr>

	  <!-- roles -->
	  <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1){ ?>
	  <tr class="tblItemAlt">
        <td align="right"><strong>Roles:</strong></td>
        <td colspan="3"><span class="subtitle">
          <? showEditText("Add a new role", "button", "btnAddRole", $_SESSION['A_allow_admin_edit']); ?>
        </span><br />
          <table   border="0" cellpadding="4" cellspacing="2">
            <tr>
			  <td ><strong>Department</strong>
                <em>(for faculty, committee members and administrators only)</em></td>
			  <td><strong>User Type(s)</strong></td>
			  <td>&nbsp;</td>
			</tr>

			<?
			$roles = getRoles($umasterid);
			for($i = 0; $i < count($roles); $i++)
			{
				if(($roles[$i][1] == "0" && $_SESSION['A_usertypeid'] != 0) )
				{
					//dont show superuser role if current user is not a superuser
				}else
				{
				if($roles[$i][1] != "")
				{
                    if ($_SESSION['A_usertypeid'] != 0) {
				?>
				<tr>
				  <td ><input name="txtRoleId_<?=$roles[$i][0]?>" type="hidden" id="txtRoleId_<?=$roles[$i][0]?>" value="<?=$roles[$i][0]?>" />
					<input name="txtDeptId_<?=$roles[$i][0]?>" type="hidden" id="txtDeptId_<?=$roles[$i][0]?>" value="<?=$roles[$i][2]?>" /><span class="tblItem">
					<? if (!isset ($roles[$i][2]) && $roles[$i][1] == -1 && $roles[$i][3] == 'unassigned' ) {
                            showEditText($roles[$i][2], "listbox", "lbDepartment_".$roles[$i][0], $_SESSION['A_allow_admin_edit'],false, $departments);
                           } else {
                               $limited_departments = array($departments[array_searchRecursive($roles[$i][2], $departments)[0]]);
                               showEditText($roles[$i][2], "listbox", "lbDepartment_".$roles[$i][0], true, false, $limited_departments);
                             //    showEditText($roles[$i][2], "textbox", "lbDepartment_".$roles[$i][0], false, null, true, 40);
                           
                          } ?>
				  </span></td>
				  <td><span class="tblItem">
                        <?  
                            if (!isset ($roles[$i][2]) && $roles[$i][1] == -1 && $roles[$i][3] == 'unassigned') {
                                showEditText($roles[$i][1], "listbox", "lbUsertype_".$roles[$i][0], $_SESSION['A_allow_admin_edit'],true, $usertypes);
                                } else {
                                          $limited_usertypes = array($usertypes[array_searchRecursive($roles[$i][1], $usertypes)[0]]);
                                          showEditText($roles[$i][1], "listbox", "lbUsertype_".$roles[$i][0], true, true, $limited_usertypes);
                                         //  showEditText($roles[$i][3], "textbox", "lbUsertype_".$roles[$i][0], false, null, true, 25);
                                         
                                        
                                }         ?>
				  </span></td>
                  <? 
                        if ($_SESSION['A_usertypeid'] == 1   //allow admins to delete student and faculty contacts
                                            and ($roles[$i][1] == 11 or $roles[$i][1] == 12))
                            {  ?>
                  <td><span class="tblItem"><span class="subtitle">
                        <?php
                                showEditText("X", "button", "btnDelRole_".$roles[$i][0], $_SESSION['A_allow_admin_edit']);
                            ?>
                  </span>
                  </span></td>
				   <?php }  ?>
				</tr>
				<?
                    }  // end if not superuser
                    else { ?>
                            <tr>
                                  <td ><input name="txtRoleId_<?=$roles[$i][0]?>" type="hidden" id="txtRoleId_<?=$roles[$i][0]?>" value="<?=$roles[$i][0]?>" />
                                    <input name="txtDeptId_<?=$roles[$i][0]?>" type="hidden" id="txtDeptId_<?=$roles[$i][0]?>" value="<?=$roles[$i][2]?>" /><span class="tblItem">
                                    <? showEditText($roles[$i][2], "listbox", "lbDepartment_".$roles[$i][0], $_SESSION['A_allow_admin_edit'],false, $departments); ?>
                                  </span></td>
                                  <td><span class="tblItem">
                                        <?  showEditText($roles[$i][1], "listbox", "lbUsertype_".$roles[$i][0], $_SESSION['A_allow_admin_edit'],true, $usertypes); ?>
                                  </span></td>
                                  <td><span class="tblItem"><span class="subtitle">
                                    <? if ($_SESSION['A_usertypeid'] == 1   //allow admins to delete student and faculty contacts
                                            and ($roles[$i][1] == 11 or $roles[$i][1] == 12))
                                            { 
                                                showEditText("X", "button", "btnDelRole_".$roles[$i][0], $_SESSION['A_allow_admin_edit']);
                                            } ?>
                                  </span>
                                  </span></td>
                                  <td><span class="tblItem"><span class="subtitle">
                        <?php
                                showEditText("X", "button", "btnDelRole_".$roles[$i][0], $_SESSION['A_allow_admin_edit']);
                            ?>
                  </span>
                  </span></td>
                                  
                            </tr>
                        
                <?php     }
				}//end if
				}//end if
			}//end for ?>
          </table></td>
      </tr>
	  <? }//end masterid ?>
	  <!-- end roles -->
      <tr>
        <td width="100" align="right">&nbsp;</td>
        <td><span class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </span></td>
      </tr>
    </table>

	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>