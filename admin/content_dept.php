<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$departments = array();
$content = array();
$deptid = -1;

if(isset($_POST['lbDepartment']))
{
	$deptid = htmlspecialchars($_POST['lbDepartment']);
}else
{
	if(isset($_GET['id']))
	{
		$deptid = htmlspecialchars($_GET['id']);
	}

}

// departments
$sql = "select distinct
department.id,
department.name
from
lu_user_department
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id ";
if($_SESSION['A_usertypeid'] != 0)
{
	$sql .= " left outer join users on users.id = lu_users_usertypes.user_id
		 where users.id=".$_SESSION['A_usermasterid']." and(lu_users_usertypes.usertype_id = ".$_SESSION['A_usertypeid'].") ";
}
$sql .= " order by department.name";

$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
//echo $_SESSION['A_userid'];
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($departments, $arr);
}



//PULL DATA FOR SELECTED PROGRAM
$sql = "select content.id, content.name, contenttypes.name as type from content
inner join contenttypes on contenttypes.id = content.contenttype_id where department_id=".$deptid . " order by contenttypes.name, content.name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['type']);
	array_push($content, $arr);

}


?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" name="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Page Content <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><strong><a href="home.php">Return Home</a></strong><br />
	<br />
	Select a Department for the set of pages to edit <br />
	<? showEditText($deptid, "listbox", "lbDepartment", $_SESSION['A_allow_admin_edit'], true, $departments); ?>
    <input name="btnRefresh" type="submit" class="tblItem" id="btnRefresh" value="Refresh" />
    <br />
    <br />
	<table width="700" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td>type</td>
        <td width="60" align="right">Delete</td>
      </tr>
      <? for($i = 0; $i < count($content); $i++){?>
      <tr>
        <td><a href="contentEdit_dept.php?id=<?=$content[$i][0]?>">
          <?=$content[$i][1]?>
        </a></td>
        <td><?=$content[$i][2]?></td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$content[$i][0], false); ?></td>
      </tr>
      <? } ?>
    </table>
	<br />
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
