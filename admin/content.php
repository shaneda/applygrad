 <?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$domains = array();
$content = array();
$domainid = -1;

if(isset($_POST['lbDomain']))
{
	$domainid = htmlspecialchars($_POST['lbDomain']);
}else
{
	if(isset($_GET['id']))
	{
		$domainid = htmlspecialchars($_GET['id']);
	}

}

// domains
/*
$sql = "select distinct
domain.id,
domain.name
from
lu_user_department
left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id
left outer join domain on domain.id = lu_domain_department.domain_id
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id ";
if($_SESSION['A_usertypeid'] != 0)
{
	$sql .= " left outer join users on users.id = lu_users_usertypes.user_id
		 where users.id=".$_SESSION['A_usermasterid']." and(lu_users_usertypes.usertype_id = ".$_SESSION['A_usertypeid'].") ";
}
$sql .= " order by domain.name";
*/
$sql = "select distinct
domain.id,
domain.name
from
domain
left outer join lu_domain_department on lu_domain_department.domain_id = domain.id
left outer join lu_user_department on lu_user_department.department_id = lu_domain_department.department_id
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id ";
if($_SESSION['A_usertypeid'] != 0)
{
    if($_SESSION['A_usertypeid'] == 1 && isset($_SESSION['A_admin_depts']))
    {
        if(count($_SESSION['A_admin_depts'])>0)
        {
            $sql .= " WHERE (";
            for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
            {
                if ($_SESSION['A_admin_depts'][$i] == NULL) {
                    continue;
                }
                
                if($i > 0 && $i < count($_SESSION['A_admin_depts']) 
                    && $_SESSION['A_admin_depts'][$i-1] != NULL)
                {
                    $sql .= " or ";
                }

                $sql .= " lu_domain_department.department_id =".$_SESSION['A_admin_depts'][$i] ;
            }
            $sql .= ") ";
        
        }
        
    } else {
    
        $sql .= " where lu_user_department.user_id=".$_SESSION['A_userid'];
        $sql .= " and(lu_users_usertypes.usertype_id = ".$_SESSION['A_usertypeid'].") ";
    }
}
$sql .= " order by domain.name";


$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
//echo $_SESSION['A_userid'];
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($domains, $arr);
}



//PULL DATA FOR SELECTED PROGRAM
$sql = "select content.id, content.name, contenttypes.name as type from content
inner join contenttypes on contenttypes.id = content.contenttype_id where domain_id=".$domainid . " order by contenttypes.name, content.name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['type']);
	array_push($content, $arr);

}
//DebugBreak();
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Page Content';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php'; 
?>

  <form id="form1" name="form1" action="" method="post"><!-- InstanceEndEditable -->

<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Page Content <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><strong><a href="home.php">Return Home</a></strong><br />
	<br />
	Select a domain for the set of pages to edit <br />
	<? showEditText($domainid, "listbox", "lbDomain", $_SESSION['A_allow_admin_edit'], true, $domains); ?>
    <input name="btnRefresh" type="submit" class="tblItem" id="btnRefresh" value="Refresh" />
    <br />
    <br />
	<table width="700" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td>type</td>
        <td width="60" align="right">Delete</td>
      </tr>
      <? for($i = 0; $i < count($content); $i++){?>
      <tr>
        <td><a href="contentEdit.php?id=<?=$content[$i][0]?>">
          <?=$content[$i][1]?>
        </a></td>
        <td><?=$content[$i][2]?></td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$content[$i][0], false); ?></td>
      </tr>
      <? } ?>
    </table>
	<br />
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>