<?php
// Standard includes.
/*
* // class.WebIsoLogin.php also has the session include
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/

include_once "../inc/session_admin.php";

/* 
* PLB added WebISO authentication/authorization, 6/29/09.
* WebIsoLogin redirects to home.php given WebISO credentials in $_SERVER['REMOTE_USER']
*/
if ( (!isset ($_GET['webIsoLogin']) ||  $_GET['webIsoLogin'] != 'false') 
    && (!isset($_GET['login']) || $_GET['login'] != 'alias') ) {
    include '../classes/class.WebIsoLogin.php';
    $webIsoLogin = new WebIsoLogin();
}
// Set default for printing role menu for non-webIso login.
$printRoleMenu = FALSE;

if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1) {
    $printRoleMenu = TRUE;    
}

/*
* Check for viewApplication, viewDepartment params.
* This is for RI to link directly to applications from their web site. 
*/
$viewApplicationId = NULL;
if ( isset($_REQUEST['viewApplication']) ) {
    $validApplicationId = filter_var($_REQUEST['viewApplication'], FILTER_VALIDATE_INT);
    if ($validApplicationId) {
        $viewApplicationId = $validApplicationId;
    }    
}

$viewDepartmentId = NULL;
if ( isset($_REQUEST['viewDepartment']) ) {
    $validDepartmentId = filter_var($_REQUEST['viewDepartment'], FILTER_VALIDATE_INT);
    if ($validDepartmentId) {
        $viewDepartmentId = $validDepartmentId;
    }    
}

/*
* Run through the main controller logic. 
*/
$err = "";
$accounts = array();

if(isset($_GET["domain"]))
{
	$domain = intval(htmlspecialchars($_GET["domain"]));
	$sql = "select id, name
	from domain
	where id = ".$domain;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$_SESSION['A_domainid'] = $row['id'];
		$_SESSION['A_domainname'] = $row['name'];
	}
	if($_SESSION['A_domainid'] < 1 || $_SESSION['A_domainname'] == "")
	{
		$err .= "Invalid domain request.<br>";
	}else
	{
		header("Location: index.php");
	}
}

//AUTO-LOGIN (CODE DUPLICATED DUE TO LACK OF TIME TO PROPERLY IMPLEMENT A METHOD :(( )
/*
if(  isset($_GET['u'])  && isset($_GET['id']) )
{
	$_SESSION['A_allow_admin_edit'] = false;
	$_SESSION['A_allow_admin_edit_late'] = false;
	$_SESSION['A_allow_admin_edit']= false;

	$_SESSION['A_firstname'] = "";
	$_SESSION['A_lastname'] = "";
	$_SESSION['A_email'] = "";

	$_SESSION['A_usermasterid'] = -1;
	$_SESSION['A_userid'] = -1;
	$_SESSION['A_usertypeid'] = -1;
	$_SESSION['A_usertypename'] = "";
	$_SESSION['A_admin_depts'] = array();
	
		
	if($_GET['u'] == "tRu2e" && intval($_GET['id']) > 0) //STUDENT CONTACT
	{
	
			$_SESSION['A_usermasterid'] = 0;
			//TO DO - MAKE $_SESSION['A_userid'] AN ARRAY
			$_SESSION['A_userid'] =0;
			$_SESSION['A_usertypeid'] = 11;
			$_SESSION['A_usertypename'] = "Student Contact";
			$_SESSION['A_email'] = "";
			$_SESSION['A_firstname'] = "Student";
			$_SESSION['A_lastname'] = "Contact";
			array_push($_SESSION['A_admin_domains'], 1);
			array_push($_SESSION['A_admin_depts'],intval($_GET['id']));
			$_SESSION['A_allow_admin_edit'] = false;
			$_SESSION['A_allow_admin_edit_late'] = false;
		$doLogin = true;
	}
	if($_GET['u'] == "VAtr8" && intval($_GET['id']) > 0) //FAC CONTACT
	{
	
		$_SESSION['A_usermasterid'] = 0;
		//TO DO - MAKE $_SESSION['A_userid'] AN ARRAY
		$_SESSION['A_userid'] =0;
		$_SESSION['A_usertypeid'] = 12;
		$_SESSION['A_usertypename'] = "Student Contact";
		$_SESSION['A_email'] = "";
		$_SESSION['A_firstname'] = "Faculty";
		$_SESSION['A_lastname'] = "Contact";
		array_push($_SESSION['A_admin_domains'], 1);
		array_push($_SESSION['A_admin_depts'],intval($_GET['id']));
		$_SESSION['A_allow_admin_edit'] = false;
		$_SESSION['A_allow_admin_edit_late'] = false;
		$doLogin = true;
	}
	if($doLogin == true)
	{
	$_SESSION['A_userid'] =0;
		header("Location: admitted.php?id=".intval($_GET['id']));
	}else
	{
		$err .= "ERROR: Invalid Login.<br>";
	}

}
*/

// "Alias" login - copied more or less intact from WebIsoLogin::setSession()
if( isset($_GET['login']) && $_GET['login'] == 'alias' && isset($_GET['luuId']) ) {
    
        $lu_users_usertypes_id = $_GET['luuId'];

        $_SESSION['A_allow_admin_edit'] = false;
        $_SESSION['A_allow_admin_edit_late'] = false;
        $_SESSION['A_allow_admin_edit']= false;

        $_SESSION['A_firstname'] = "";
        $_SESSION['A_lastname'] = "";
        $_SESSION['A_email'] = "";

        $_SESSION['A_usermasterid'] = -1;
        $_SESSION['A_userid'] = -1;
        $_SESSION['A_usertypeid'] = -1;
        $_SESSION['A_usertypename'] = "";
        $_SESSION['A_admin_domains'] = array(); // PLB added 9/16/09
        $_SESSION['A_admin_depts'] = array();
        
        // New session vars for roles
        $_SESSION['roleLuuId'] = -1;
        $_SESSION['roleDepartmentId'] = NULL;
        $_SESSION['roleDepartmentName'] = "";
        
        $sql = "(select
        users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email,
        lu_user_department.department_id as deptid,
        department.name as department_name,
        lu_domain_department.domain_id as domainid
        from users
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
        LEFT OUTER JOIN department ON lu_user_department.department_id = department.id
        left outer join lu_domain_department on lu_domain_department.department_id=lu_user_department.department_id
        where lu_users_usertypes.id = " . $lu_users_usertypes_id . ")";
        
        // PLB added unit query 9/15/09
        $sql .= " UNION (SELECT
        users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email,
        department.id as deptid,
        unit.unit_name as department_name,
        lu_domain_department.domain_id as domainid
        FROM users
        INNER JOIN lu_users_usertypes on lu_users_usertypes.user_id = users.id
        INNER JOIN usertypes on usertypes.id = lu_users_usertypes.usertype_id
        INNER JOIN unit_role on lu_users_usertypes.id = unit_role.lu_users_usertypes_id
        INNER JOIN unit ON unit_role.unit_id = unit.unit_id
        INNER JOIN domain_unit ON unit_role.unit_id = domain_unit.unit_id
        INNER JOIN lu_domain_department ON domain_unit.domain_id = lu_domain_department.domain_id
        INNER JOIN department ON lu_domain_department.department_id = department.id
        WHERE unit_role.lu_users_usertypes_id = " . $lu_users_usertypes_id . ")"; 
        
        
        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result ))
        {
            if($row['typeid']==1 || $row['typeid']==0 || $row['typeid']==2 || $row['typeid']==4 || 
                $row['typeid']==3 || $row['typeid']==10 || $row['typeid']==11 || $row['typeid']==17
                || $row['typeid']==19 || $row['typeid']==20)
            {
                $_SESSION['A_usermasterid'] = $row['id'];
                $_SESSION['A_userid'] = $row['uid'];
                $_SESSION['A_usertypeid'] = $row['typeid'];
                $_SESSION['A_usertypename'] = $row['typename'];
                $_SESSION['A_email'] = $row['email'];
                $_SESSION['A_firstname'] = $row['firstname'];
                $_SESSION['A_lastname'] = $row['lastname'];
                array_push($_SESSION['A_admin_domains'], $row['domainid']);
                array_push($_SESSION['A_admin_depts'], $row['deptid']);
                
                $_SESSION['roleLuuId'] = $row['uid'];
                $_SESSION['roleDepartmentId'] = $row['deptid'];
                $_SESSION['roleDepartmentName'] = $row['department_name'];

                $_SESSION['A_allow_admin_edit'] = true;
                $_SESSION['A_allow_admin_edit_late'] = true;
                $_SESSION['A_allow_admin_edit']= true; // ?????????????
            }
            if($row['typeid']==4 )
            {
                $_SESSION['A_allow_admin_edit'] = false;
                $_SESSION['A_allow_admin_edit_late'] = false;
                $_SESSION['A_allow_admin_edit']= false; // ?????????????

            }
        }
        if($_SESSION['A_userid'] == -1)
        {
            $err .= "Invalid username or password for ".$_POST['txtEmail'].".<br>";
            return FALSE;
        }

        // New: set a variable so that home.php only display actions for a single role.
        $_SESSION['adminByRole'] = TRUE;
        
        $_SESSION['login'] = 'alias';
    
        header("Location: home.php"); 
}


if(isset($_POST['btnLogin']))
{
	$_SESSION['A_allow_admin_edit'] = false;
	$_SESSION['A_allow_admin_edit_late'] = false;
	$_SESSION['A_allow_admin_edit']= false;

	$_SESSION['A_firstname'] = "";
	$_SESSION['A_lastname'] = "";
	$_SESSION['A_email'] = "";

	$_SESSION['A_usermasterid'] = -1;
	$_SESSION['A_userid'] = -1;
	$_SESSION['A_usertypeid'] = -1;
	$_SESSION['A_usertypename'] = "";
	$_SESSION['A_admin_depts'] = array();
	if(isset($_POST['txtEmail']) && isset($_POST['txtPass']) )
	{
		$passwordHash = sha1(htmlspecialchars($_POST['txtPass']));

		$sql = "select
		users.id, lu_users_usertypes.id as uid,
		usertypes.id as typeid,
		usertypes.name as typename,
		users.firstname,
		users.lastname,
		users.email,
		lu_user_department.department_id as deptid,
		lu_domain_department.domain_id as domainid
		from users
		inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
		inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
		left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
		left outer join lu_domain_department on lu_domain_department.department_id=lu_user_department.department_id
		where email = '".addslashes(htmlspecialchars($_POST['txtEmail']))."' and password = '".$passwordHash."' order by lu_users_usertypes.usertype_id desc";
		$result = mysql_query($sql) or die(mysql_error());
		//echo $sql;
		while($row = mysql_fetch_array( $result ))
		{
			if($row['typeid']==1 || $row['typeid']==0 || $row['typeid']==2 || $row['typeid']==4 || $row['typeid']==3 || $row['typeid']==10 || $row['typeid']==11 || $row['typeid']==19)
			{
				$_SESSION['A_usermasterid'] = $row['id'];
				//TO DO - MAKE $_SESSION['A_userid'] AN ARRAY
				$_SESSION['A_userid'] = $row['uid'];
				$_SESSION['A_usertypeid'] = $row['typeid'];
				$_SESSION['A_usertypename'] = $row['typename'];
				$_SESSION['A_email'] = $row['email'];
				$_SESSION['A_firstname'] = $row['firstname'];
				$_SESSION['A_lastname'] = $row['lastname'];
				array_push($_SESSION['A_admin_domains'], $row['domainid']);
				array_push($_SESSION['A_admin_depts'], $row['deptid']);

				$_SESSION['A_allow_admin_edit'] = true;
				$_SESSION['A_allow_admin_edit_late'] = true;
				$_SESSION['A_allow_admin_edit']= true;
			}
			if($row['typeid']==4 )
			{
				$_SESSION['A_allow_admin_edit'] = false;
				$_SESSION['A_allow_admin_edit_late'] = false;
				$_SESSION['A_allow_admin_edit']= false;

			}
		}
		if($_SESSION['A_userid'] == -1)
		{
			$err .= "Invalid username or password for ".$_POST['txtEmail'].".<br>";
		}else if ($_SESSION['A_usertypeid'] == 11) {
		  //   
            header("Location: admitted.php?id=".$_SESSION['A_admin_depts'][0]);
            } else {
			// PLB added role menu 7/2/09
            //header("Location: home.php");
            $printRoleMenu = TRUE;
		}
	}
	else{
		$err .= "Please enter both your email address and password.<br>";
		$_SESSION['A_userid'] = -1;
	}
}

if ($viewApplicationId && $viewDepartmentId){     
    $remoteUser = NULL;
    if ( isset($_SERVER['REMOTE_USER']) ) {
        $remoteUser = $_SERVER['REMOTE_USER'];   
    } elseif ( isset($_SESSION['userWebIso']) ) {
        $remoteUser = $_SESSION['userWebIso'];    
    } elseif ( isset($_SERVER['HTTP_EPPN']) ) {
        $remoteUser = $_SESSION['HTTP_EPPN'];
    }

    if ($remoteUser) {
        
        $applicationDepartmentIdQuery = "SELECT DISTINCT department_id FROM lu_application_programs
                                        INNER JOIN lu_programs_departments 
                                            ON lu_application_programs.program_id = lu_programs_departments.program_id
                                        WHERE lu_application_programs.application_id = " . $viewApplicationId;
        $applicationDepartmentIdQuery .= " AND lu_programs_departments.department_id = " . $viewDepartmentId;
        $applicationDepartmentIdResult =  mysql_query($applicationDepartmentIdQuery);
        $applicationDepartmentIds = array();
        while ( $row = mysql_fetch_array($applicationDepartmentIdResult) ) {
            $applicationDepartmentIds[] = $row['department_id'];        
        }

        $luUsersIdQuery = "SELECT DISTINCT users.id AS users_id, 
                            lu_users_usertypes.id AS lu_users_usertypes_id,
                            lu_user_department.department_id,
                            department.name AS department_name,
                            lu_domain_department.domain_id,
                            usertypes.id AS usertype_id, usertypes.name AS usertype_name
                            FROM lu_users_usertypes
                            INNER JOIN usertypes ON lu_users_usertypes.usertype_id = usertypes.id
                            INNER JOIN users ON lu_users_usertypes.user_id = users.id
                            INNER JOIN users_remote_auth_string on  users_remote_auth_string.users_id = users.id
                            INNER JOIN lu_user_department 
                                ON lu_users_usertypes.id = lu_user_department.user_id
                            INNER JOIN lu_domain_department 
                                ON lu_user_department.department_id = lu_domain_department.department_id
                            INNER JOIN department ON lu_user_department.department_id = department.id
                            WHERE usertype_id IN (1, 2, 3)";
        $luUsersIdQuery .= " AND  UPPER(users_remote_auth_string.remote_auth_string) = UPPER('" . $remoteUser . "') 
                            AND lu_user_department.department_id = " . $applicationDepartmentIds[0];
        
        //DebugBreak();
        $luUsersIdResult = mysql_query($luUsersIdQuery);
        $luUsersIds = array();
        while ( $row = mysql_fetch_array($luUsersIdResult) ) {
            $luUsersIds[$row['lu_users_usertypes_id']] = $row;        
        }
        
        $applicantLuuId = NULL;
        $applicantLuuIdQuery = "SELECT user_id FROM application WHERE id = " . $viewApplicationId;
        $applicantLuuIdResult = mysql_query($applicantLuuIdQuery);
        while ( $row = mysql_fetch_array($applicantLuuIdResult) ) {
            $applicantLuuId = $row['user_id'];        
        }
        
        foreach ($luUsersIds as $luuId => $luuRecord) {
            
            if ( in_array($luuRecord['department_id'], $applicationDepartmentIds) ) {
            
                $_SESSION['adminByRole'] = TRUE;
                $_SESSION['roleLuuId'] = $luuId;
                $_SESSION['A_usermasterid'] = $luuRecord['users_id'];
                $_SESSION['A_userid'] = $luuId;
                $_SESSION['A_usertypeid'] = 3;
                $_SESSION['A_usertypeid'] = $luuRecord['usertype_id'];
                $_SESSION['A_usertypename'] = $luuRecord['usertype_name'];
                $_SESSION['roleDepartmentName'] = $luuRecord['department_name'];
                array_push($_SESSION['A_admin_depts'], $luuRecord['department_id']);
                $headerString = "Location: ../review/userroleEdit_student_print.php";
                $headerString .= "?id=" . $applicantLuuId;
                $headerString .= "&applicationId=" . $viewApplicationId;
                
                header($headerString);      
                exit;
                
            }
        }
    
    }
}


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Log In';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/administerApplications.js',
    '../javascript/ajaxfileupload.js'
    );
 
$displayMenu = FALSE;

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
 
// Print the body html.
// PLB added WebISO authentication/authorization, 6/29/09
if ( isset($webIsoLogin) && $webIsoLogin->getStatus() == TRUE ) {
    // The user has authenticated with WebISO and applyweb but hasn't
    // selected a login role, so print the login role menu.
    
    // Set the user data array for the template.
    $usersData = $webIsoLogin->getUsersData();
    
    // Set the webiso string for the template.
    $webIso = $webIsoLogin->getWebIso();
    
    // Include the role menu template
    include '../inc/tpl.roleMenu.php';
    
    // Close the preceding html.
    echo "</body></html>";
    
    // exit the script;
    exit;
}

// PLB added 7/2/09.
if ($printRoleMenu) {
    // The user has authenticated with WebISO and applyweb but hasn't
    // selected a login role, so print the login role menu.
    
    // Set the user data array for the template.
    $query = "SELECT lu_users_usertypes.*, 
        users.firstname, users.lastname, users.email, usertypes.name as usertypes_name, 
        lu_user_department.department_id, department.name as department_name  
        FROM users, 
        lu_users_usertypes
            LEFT JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
            LEFT JOIN department ON lu_user_department.department_id = department.id,
        usertypes 
        WHERE users.id = " . $_SESSION['A_usermasterid'] . "
        AND users.id = lu_users_usertypes.user_id
        AND lu_users_usertypes.usertype_id = usertypes.id
        ORDER BY department.name, usertypes.name";        
    $result = mysql_query($query);
    $usersData = array();
    while ( $row = mysql_fetch_array($result) ) {
        $usersData[] = $row;
    }
    
    // Set an empty webiso string for the template.
    $webIso = '';
    
    // Include the role menu template
    include '../inc/tpl.roleMenu.php';

    // Include the standard page footer.
    include '../inc/tpl.pageFooter.php';
    
    // exit the script
    exit;
}

?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Login<!-- InstanceEndEditable --></span><br />
<br />

    
	<!-- InstanceBeginEditable name="mainContent" -->
	<div style="width:100%" class="tblItem">
      <div align="center">
          <?php
          
            // PLB added applywebAuth parameter to support choice of authentication method 9/1/09
            //DebugBreak();
            $applywebAuthSelected = FALSE;
            if ( isset($_GET['applywebAuth']) ) {
                $applywebAuthSelected = TRUE;    
            }
          
            if ($applywebAuthSelected) {
            ?>
            
        <br/><br/><br/> 
        <span class="errorSubtitle"><?=$err?>
        </span>
        <form id="form1" action="" method="post">
        <table width="150" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td align="right" class="tblItem">Email:</td>
            <td class="tblItem"><input name="txtEmail" type="text" class="tblItem" id="txtEmail" maxlength="100" tabindex="1" /></td>
            <td rowspan="2"><input type="submit" name="btnLogin" value="Login" class="tblItem" tabindex="3" alt="Login" /></td>
          </tr>
          <tr>
            <td align="right" class="tblItem">Password:</td>
            <td class="tblItem"><input name="txtPass" type="password" class="tblItem" maxlength="50" tabindex="2" /></td>
          </tr>
          <tr>
            <td align="right" class="tblItem">&nbsp;</td>
            <td class="tblItem"><a href="forgotPassword.php">Forgot Password?</a></td>
            <td>&nbsp;</td>
          </tr>
        </table>
                
            <?php    
            } else {
            
                // PLB added WebISO login msg 6/29/09
                if ( isset($webIsoLogin) && $webIsoLogin->getWebIso() ) {
                    // The user has authenticated with WebISO but not with applyweb.
                    echo '<p>Please contact your department administrator to set up WebISO login.</p>';
                } else {
                    // PLB added authentication "menu" 9/1/09
                    echo '<div style="width:250px; text-align: left; border: 1px solid black"><ul>
                        <li><b><a href="../webIso/login.php" style="font-size: 15px;">Login via WebISO</a></b></li>
                        <li style="margin-top: 10px;">
                        <a href="?applywebAuth=true" style="font-size: 15px;">Login using an Applyweb<br/>username/password</a>
                        </li>
                        </ul></div>';
                }
            
            
                
            }
          
            ?>
        </div>
	  </div>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>