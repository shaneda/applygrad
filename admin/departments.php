<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$sql = "";
$err = "";
$department = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				mysql_query("DELETE FROM department WHERE id=".$itemId) or die(mysql_error());
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select department.id, department.name, schools.name as schoolname, rank,
if(enable_round1 = 1, 'Enabled', '') as round1,
if(enable_round2 = 1, 'Enabled', '') as round2,
if(enable_round3 = 1, 'Enabled', '') as round3,
if(enable_round4 = 1, 'Enabled', '') as round4,
if(semiblind_review = 1, 'Enabled', '') as  semiblind_review,
if(enable_final = 1, 'Enabled', '') as final,
unit.unit_id,
unit.unit_name_short
from department
left outer join schools on schools.id = department.parent_school_id
LEFT OUTER JOIN department_unit ON department.id = department_unit.department_id
LEFT OUTER JOIN unit ON department_unit.unit_id = unit.unit_id
order by schools.name,rank,department.name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['schoolname']);
	array_push($arr, $row['rank']);
	array_push($arr, $row['round1']);
	array_push($arr, $row['round2']);
	array_push($arr, $row['semiblind_review']);
	array_push($arr, $row['final']);
    array_push($arr, $row['round3']);   // $arr[8]
    array_push($arr, $row['unit_id']);
    array_push($arr, $row['unit_name_short']);
    array_push($arr, $row['round4']);
	array_push($department, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Departments';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Departments</span><br />
<br />

	<a href="domainAssign.php">Assign Departments to Domain</a> | <a href="departmentEdit.php">Add Department </a><br />
	<br />
	<table width="750" border="0" cellpadding="4" cellspacing="2">
      <tr class="tblHead">
        <td>ID</td>
        <td>Name</td>
        <!--
        <td>Parent School</td>
        <td width="60">rank</td>
        --> 
        <td width="60">Round 1 </td>
        <td width="60">Round 2 </td>
        <td width="60">Round 3 </td>
        <td width="60">Round 4 </td>
		<td width="60">final</td>
		<td width="60">Semi-Blind Review </td>
        <td width="60">Unit </td>
        <td width="60" align="right">Delete</td>
      </tr>
     <? for($i = 0; $i < count($department); $i++){
	 $class = "tblItem";
	 if($i % 2 == 0)
	 {
	 	$class = "tblItemAlt";
	 }
	 $unitId = $department[$i][9];
     $unitName = $department[$i][10];
     ?>
	<tr class="<?=$class?>">
        <td><?=$department[$i][0]?></td>
        <td><a href="departmentEdit.php?id=<?=$department[$i][0]?>"><?=$department[$i][1]?></a></td>
        <!--
        <td><?=$department[$i][2]?></td>
        <td width="60"><?=$department[$i][3]?></td>
        --> 
        <td width="60"><?=$department[$i][4]?></td>
        <td width="60"><?=$department[$i][5]?></td>
        <td width="60"><?=$department[$i][8]?></td>
        <td width="60"><?=$department[$i][11]?></td>
		<td width="60"><?=$department[$i][7]?></td>
		<td width="60"><?=$department[$i][6]?></td>
        <td width="60">
        <?php
        $enableDelete = TRUE;
        $disabled = '';
        if (!$_SESSION['A_allow_admin_edit']) {
            $enableDelete = FALSE;
            $disabled = 'disabled';    
        } elseif ($unitId) {
            $enableDelete = FALSE;
            $disabled = 'disabled';
            echo '<a href="units.php?unit_id=' . $unitId . '">' . $unitName . '</a>';    
        }
        ?>
        </td>
        <td width="60" align="right">
        <?php
        //showEditText("X", "button", "btnDelete_".$department[$i][0], $enableDelete);
        if($_SESSION['A_usertypeid'] == 0 || 
            ( $_SESSION['A_usertypeid'] == 1 
            && $_SESSION['roleDepartmentName'] == 'School of Computer Science' ) )
        {
        ?>
         <input name="btnDelete_<?php echo $department[$i][0]; ?>" 
            type="submit" class="tblItem" id="btnDelete_<?php echo $department[$i][0]; ?>" value="X" 
            onClick="return confirmSubmit('<?php echo $department[$i][1]; ?>')" 
            <?php echo $disabled; ?> />
        <?php 
        }
        ?>
        </td>
      </tr>
	<? } ?>
    </table>

	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<script LANGUAGE="JavaScript">
<!--
// Nannette Thacker http://www.shiningstar.net
function confirmSubmit(val)
{
var agree=confirm("Are you sure you wish to delete record "+val+"?");
if (agree)
    return true ;
else
    return false ;
}
// -->
</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>