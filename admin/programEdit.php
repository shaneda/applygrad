<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$degrees = array();
$fields = array();
$departments = array();
$lockouts = array();
$interests = array();
$recommendations = array();
$recommendation = array();
$field = "";
$degree = "";
$department = "";
$oraclestring = "";
$lockout = array();
$price = "0.00";
$baseprice = "0.00";
$priceLate = "0.00";
$basepriceLate = "0.00";
$programId = -1;
$desc = "";
$url = "";
$rank = 0;
$linkword = "in";
$enabled = 0;

if(isset($_GET['id']))
{
	$programId = htmlspecialchars($_GET['id']);
}

// DEGREES
$result = mysql_query("SELECT * FROM degree order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name'] .' ('.$row['short'].')');
	array_push($degrees, $arr);
}

// RECOMMENDATION TYPES
$result = mysql_query("SELECT * FROM recommendationtypes order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($recommendations, $arr);
}

//DEPARTMENTS
$sql = "SELECT
    department.id,
    department.name
    FROM department order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row["id"]);
	array_push($arr, $row["name"]);
	array_push($departments, $arr);
}

//GET FIELDS
$sql = "SELECT distinct fieldsofstudy.id,
    fieldsofstudy.name
    FROM fieldsofstudy
    order by name";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row["id"]);
	array_push($arr, $row["name"]);
	array_push($fields, $arr);
}

//GET INTERESTS
//GET DATA
$sql = "select interest.id,
    interest.name
    from lu_programs_interests
    inner join interest on interest.id = lu_programs_interests.interest_id
    where lu_programs_interests.program_id=".$programId."
    order by interest.name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($interests, $arr);
}

//GET LOCKOUTS
$sql = "SELECT
    programs.id,
    fieldsofstudy.name,
    degree.name as degreename,
    programs.programprice
    FROM programs
    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
    inner join degree on degree.id = programs.degree_id ";
$sql .= " order by degree.name, fieldsofstudy.name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$dept = "";
	$depts = getDepartments($row[0]);
	if(count($depts) > 1)
	{
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$dept .= $depts[$i][1];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row[2] . " in ". $row[1].$dept);
	array_push($arr, $row[3]);
	array_push($lockouts, $arr);
}

if(isset($_POST['btnSubmit']))
{
	$programId = htmlspecialchars($_POST['txtProgramId']);
	$field = htmlspecialchars($_POST['lbField']);
	$key = marray_search($field,$fields);
	$fieldId = $fields[$key][0];
	$price = floatval(htmlspecialchars($_POST['txtPrice']));
	$desc = addslashes(htmlspecialchars($_POST['txtDesc']));
	$url = addslashes(htmlspecialchars($_POST['txtUrl']));
	$rank = intval($_POST['txtRank']);
	$linkword = addslashes(htmlspecialchars($_POST['txtLinkword']));
    $oraclestring = addslashes(htmlspecialchars($_POST['txtOraclestring']));
	$baseprice = floatval(htmlspecialchars($_POST['txtBasePrice']));
    $priceLate = floatval(htmlspecialchars($_POST['txtPriceLate']));
    $basepriceLate = floatval(htmlspecialchars($_POST['txtBasePriceLate']));
	if(isset($_POST['chkEnabled']))
	{
		$enabled = true;
	}
	if($key === false )
	{
		$err .="You have entered an invalid field of study name (".$field.")<br>";
	}
	$degree = htmlspecialchars($_POST['lbDegree']);
	if($degree == "")
	{
		$err .="Degree is required<br>";
	}

	if($err == "")
	{
		if($programId > -1)
		{
			$sql = "update programs set degree_id=".$degree.", 
			fieldofstudy_id=".$fieldId.", 
			programprice=".$price.",
            programprice_late=".$priceLate.", 
			description='".$desc."', 
			url='".$url."',
            oraclestring = '" . $oraclestring . "',
			rank=".$rank.", 
			linkword='".$linkword."', 
			baseprice=".$baseprice.",
            baseprice_late=".$basepriceLate.", 
			enabled = ".$enabled."
			where id=".$programId;
			mysql_query($sql)or die(mysql_error().$sql);
			//echo $sql . "<br>";
		}
		else
		{
			$sql = "insert into programs
                (degree_id, fieldofstudy_id, programprice, programprice_late, description, url, 
                oraclestring, rank, linkword, baseprice, baseprice_late, enabled)
                values(".$degree.",".$fieldId.", ".$price.", ".$priceLate.", '".$desc."', '".$url."', '"
                . $oraclestring . "', " .$rank.", '".$linkword."', ".$baseprice.",".$basepriceLate.",".$enabled.")";
			mysql_query($sql)or die(mysql_error().$sql);
			$programId = mysql_insert_id();
			//echo $sql . "<br>";
		}
		mysql_query("DELETE FROM lu_programs_departments WHERE program_id=".$programId)
		or die(mysql_error().$sql);

		mysql_query("DELETE FROM multiprogramlockouts WHERE program_id1=".$programId)
		or die(mysql_error().$sql);

		mysql_query("DELETE FROM lu_programs_recommendations WHERE program_id=".$programId)
		or die(mysql_error().$sql);

		mysql_query("DELETE FROM programs_applicationreqs WHERE programs_id=".$programId)
        or die(mysql_error().$sql);  
        
        $vars = $_POST;
		$itemId = -1;
		foreach($vars as $key => $value)
		{
			$arr = split("_", $key);
			if ( strstr($key, 'chkDepartment') !== false 
            || strstr($key, 'chkLockouts') !== false 
            || strstr($key, 'chkRecs') !== false
            || strstr($key, 'chkRequirement') !== false )
			{
				$tmpid = $arr[1];
				if($itemId != $tmpid || $tmpType != $arr[0])
				{
					$itemId = $tmpid;
					$tmpType=$arr[0];
					echo $arr[0];
					if($arr[0] == "chkDepartment")
					{
						$sql = "insert into lu_programs_departments(program_id, department_id)values(".$programId.",".$itemId.")";
						mysql_query($sql)or die(mysql_error().$sql);
					}
					if($arr[0] == "chkLockouts")
					{
						$sql = "insert into multiprogramlockouts(program_id1, program_id2)values(".$programId.",".$itemId.")";
						mysql_query($sql)or die(mysql_error().$sql);
					}
					if($arr[0] == "chkRecs")
					{
						$numReq = intval($_POST["txtRank_".$itemId]);
						$sql = "insert into lu_programs_recommendations(program_id, recommendationtype_id, numrequired)values(".$programId.",".$itemId.", ".$numReq.")";
						mysql_query($sql)or die(mysql_error().$sql);
					}
                    if($arr[0] == "chkRequirement")
                    {
                        $sql = "insert into programs_applicationreqs (applicationreqs_id, programs_id)
                                values (" . $itemId . ", " . $programId. ")";
                        mysql_query($sql)or die(mysql_error().$sql);
                    }
                }
			}
		}//END FOR
	header("Location: programs.php");
	}//END IF
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT
	department.id,
	department.name
	FROM lu_programs_departments
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

//GET PROGRAM INFO
$sql = "SELECT programs.id, programs.degree_id,programs.fieldofstudy_id, programprice, programprice_late, 
description, url, oraclestring, rank, linkword, baseprice, baseprice_late, enabled
FROM programs
where id=".$programId;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	//$programId = $row['id'];
	$degree = $row['degree_id'];
	$field = $row['fieldofstudy_id'];
	$price = $row['programprice'];
    $priceLate = $row['programprice_late'];
	$desc = $row['description'];
	$url = $row['url'];
    $oraclestring = $row['oraclestring'];
	$rank = intval($row['rank']);
	$linkword = $row['linkword'];
	$baseprice = $row['baseprice'];
    $basepriceLate = $row['baseprice_late'];
	$enabled = $row['enabled'];
}
$department = getDepartments($programId);

//GET PROGRAM LOCKOUTS
$sql = "SELECT id,program_id2
FROM multiprogramlockouts
where program_id1=".$programId;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row["program_id2"]);
	array_push($lockout, $arr);
}

//GET PROGRAM RECOMMENDATIONS
$sql = "SELECT
recommendationtype_id,numrequired
FROM programs
inner join lu_programs_recommendations on lu_programs_recommendations.program_id = programs.id
where program_id=".$programId;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row["recommendationtype_id"]);
	array_push($arr, $row["numrequired"]);
	array_push($recommendation, $arr);
}

//get requirements
$requirements = array();
$sql = "select id, short from applicationreqs order by name";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, str_replace("<br>"," ", $row['short']));
    array_push($requirements, $arr);
}

$requirement = array(); 
$sql = "select id, applicationreqs_id from programs_applicationreqs where programs_id = " . $programId;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['applicationreqs_id']);
    array_push($requirement, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Program';

$pageCssFiles = array();

$headJavascriptFiles = array(
    '../inc/prototype.js',
    '../inc/scriptaculous.js'
);

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Program</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>	<br />
	<a href="programs.php">Return to programs list</a>
    <table width="750" border="0" cellpadding="4" cellspacing="2">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtProgramId" type="hidden" id="txtProgramId" value="<?=$programId?>" />
          Degree Type:<br />
          </strong><a href="degreeEdit.php">Add New</a>         </td>
        <td class="tblItem">
          <? showEditText($degree, "listbox", "lbDegree", $_SESSION['A_allow_admin_edit'],false, $degrees); ?>        </td>
      </tr>
      <tr>
        <td align="right"><strong>Link Keyword:</strong> </td>
        <td><span class="tblItem">
          <? showEditText($linkword, "textbox", "txtLinkword", $_SESSION['A_allow_admin_edit'],false,null,true,10); ?>
        </span></td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Field of Study:<br />
          </strong><a href="fieldEdit.php">Add new</a>         </td>
        <td>
        <?
	    $str = "";
	    for($i = 0; $i < count($fields); $i++){
		    if($fields[$i][0] == $field)
		    {
			    $str = $fields[$i][1];
			    break;
		    }
	    }
	    showEditText($str, "textbox", "lbField", $_SESSION['A_allow_admin_edit'],false, $fields, true, 45);
	    ?>
        </td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Department(s):</strong><br />
          <a href="departmentEdit.php">Add new</a>          <br />          </td>
        <td><span class="tblItem">
          <? showEditText($department, "checklist", "chkDepartment", $_SESSION['A_allow_admin_edit'],false, $departments); ?>
        </span>
		<hr size="1" noshade="noshade" />		</td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Description:</strong></td>
        <td class="tblItem"><span class="subtitle">
          <? showEditText(html_entity_decode($desc), "textarea", "txtDesc", $_SESSION['A_allow_admin_edit'],false); ?>
        </span></td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Program URL</strong><em> (optional)</em><strong>:</strong></td>
        <td class="tblItem">
          <? showEditText($url, "textbox", "txtUrl", $_SESSION['A_allow_admin_edit'],false,null,true,50); ?>       </td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Lockouts:<em><br />
        </em></strong><em>          Prevents applicants from subscribing to similar programs</em> </td>
        <td class="tblItem">
          <? showEditText($lockout, "checklist", "chkLockouts", $_SESSION['A_allow_admin_edit'],false, $lockouts); ?>        </td>
      </tr>
      <tr>
        <td align="right"><strong>Interests:<br />
          </strong><a href="interestEdit.php?id=<?=$programId?>">Edit</a> </td>
        <td class="tblItem">
		<?
		for($i = 0; $i < count($interests); $i++)
		{
			if($i < count($interests) && $i > 0)
			{
				echo ", ";
			}
			echo $interests[$i][1];
		}
		?>		</td>
      </tr>
      
      <tr>
        <td align="right"><strong>Application Requirements:</strong> </td>
        <td >
          <? showEditText($requirement, "checklist", "chkRequirement", $_SESSION['A_allow_admin_edit'],false, $requirements); ?>
       </td>
      </tr>

      <tr>
        <td align="right"><strong>Recommendations Required:</strong> </td>
        <td class="tblItem">
			<table border="0" cellspacing="1" cellpadding="1">
			<? for($i = 0; $i< count($recommendations); $i++){ ?>
			<tr>
				<td><?
				$selected = "";
				$recrank = "";
				for($j = 0; $j < count($recommendation); $j++)
				{
					//echo $department[$j][0] ." ". $departments[$i][0];
					if($recommendation[$j][0] == $recommendations[$i][0])
					{
						$selected = 1;
						$recrank = $recommendation[$j][1];
						break;
					}
				}
				showEditText($selected, "checkbox", "chkRecs_".$recommendations[$i][0], $_SESSION['A_allow_admin_edit'],false, $recommendations);
				echo $recommendations[$i][1];
				?></td>
				<td><? showEditText($recrank, "textbox", "txtRank_".$recommendations[$i][0], $_SESSION['A_allow_admin_edit'],false); ?></td>
			</tr>
			<? }//end for ?>
			</table>		  </td>
      </tr>
      <tr>
        <td align="right"><strong>Oracle String:</strong> </td>
        <td ><? showEditText($oraclestring, "textbox", "txtOraclestring", $_SESSION['A_allow_admin_edit'],false,null,true,50); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Base Price:</strong> </td>
        <td ><? showEditText($baseprice, "textbox", "txtBasePrice", $_SESSION['A_allow_admin_edit'],false); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Price:</strong></td>
        <td >
          <? showEditText($price, "textbox", "txtPrice", $_SESSION['A_allow_admin_edit'],false); ?>    </td>
      </tr>
      <tr>
        <td align="right"><strong>Base Price Late:</strong> </td>
        <td ><? showEditText($basepriceLate, "textbox", "txtBasePriceLate", $_SESSION['A_allow_admin_edit'],false); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Price Late:</strong></td>
        <td >
          <? showEditText($priceLate, "textbox", "txtPriceLate", $_SESSION['A_allow_admin_edit'],false); ?>    </td>
      </tr>
      <tr>
        <td align="right"><strong>Rank:</strong></td>
        <td>
          <? showEditText($rank, "textbox", "txtRank", $_SESSION['A_allow_admin_edit'],false); ?>      </td>
      </tr>
      <tr>
        <td align="right"><strong>Enabled:</strong></td>
        <td class="tblItem"><? showEditText($enabled, "checkbox", "chkEnabled", $_SESSION['A_allow_admin_edit'],false); ?></td>
      </tr>
      <tr>
        <td width="200" align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>