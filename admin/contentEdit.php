    <?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$types = array();
$domains = array();
$name = "";
$content = "";
$type = "";
$domain = "";
$id = -1;
$moddate = "";
$moduser = -1;
if(isset($_POST['txtmailtemplatesId']))
{
	$id = intval(htmlspecialchars($_POST['txtmailtemplatesId']));
}else
{
	if(isset($_GET['id']))
	{
		$id = intval(htmlspecialchars($_GET['id']));
	}
}
if($id == "")
{
	header("Location: content.php");
}

$sql = "select id,name from domain order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($domains, $arr);
}

$sql = "select id,name from contenttypes order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($types, $arr);
}


if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$content = addslashes(htmlspecialchars($_POST['txtContent']));
	$type = htmlspecialchars($_POST['lbType']);
	$domain = htmlspecialchars($_POST['lbDomain']);

	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($type == "")
	{
		$err .= "Type is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update content set
			name='".$name."',
			content='".$content."',
			contenttype_id=".$type.",
			modifieddate='".date("Y-m-d h:i:s")."',
			modifiedby=".$_SESSION['A_userid']." where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			//echo $sql . "<br>";
		}else
		{
			$sql = "insert into content(name, content, contenttype_id, modifieddate, modifiedby)values('".$name."', '".$content."', ".$type.",'".date("Y-m-d h:i:s")."' , ".$_SESSION['A_userid'].")";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
		}
		header("Location: content.php?id=".$domain);
	}
}
$sql = "SELECT content.id,content.name,content.content,content.contenttype_id,content.domain_id,content.modifieddate,
users.firstname, users.lastname
FROM content
left outer join lu_users_usertypes on lu_users_usertypes.id = content.modifiedby
left outer join users on users.id = lu_users_usertypes.user_id
 where content.id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$content = $row['content'];
	$type = $row['contenttype_id'];
	$domain = $row['domain_id'];
	$moddate = formatUSDate($row['modifieddate']);
	$moduser = $row['firstname'] . " " . $row['lastname'];
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Page Content';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php'; 
?>
<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="../javascript/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		relative_urls : false,
		convert_urls : false
	});
</script>
<!-- /tinyMCE -->


  <form id="form1" name="form1" action="" method="post">

<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Edit Mail Template <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<span class="errorSubtitle"><?=$err?></span>
	<table width="100%"  border="0" cellpadding="4" cellspacing="2" class="tblItem">
      <tr>
        <td width="150" align="right"><strong>Name:</strong></td>
        <td><? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Last Modified:</strong> </td>
        <td><?
		if($moddate != "" && $moddate != "0000-00-00 00:00:00")
		{
			echo "<em>".$moddate ."</em> by ". $moduser;
		}

		?>
		</td>
      </tr>
      <tr>
        <td width="150" align="right"><strong>Content Type:</strong> </td>
        <td><? showEditText($type, "listbox", "lbType", $_SESSION['A_allow_admin_edit'], true, $types); ?></td>
      </tr>
      <? if($_SESSION['A_usertypeid'] == 0){ ?>
	  <tr>
        <td width="150" align="right"><strong>Domain:</strong></td>
        <td><? showEditText($domain, "listbox", "lbDomain", $_SESSION['A_allow_admin_edit'], true, $domains); ?> <span class="subtitle">
          <? showEditText("Refresh", "button", "btnRefresh", $_SESSION['A_allow_admin_edit']); ?>
        </span></td>
      </tr>
	 <? }else
	 {
	 	showEditText($domain, "hidden", "lbDomain", $_SESSION['A_allow_admin_edit'], false);
	 }
	 ?>
      <tr>
        <td width="150" align="right"><strong>Content:</strong></td>
        <td><textarea name="txtContent" cols="100" rows="30" class="tblItem" id="txtContent"><?=html_entity_decode($content)?></textarea></td>
      </tr>
      <tr>
        <td width="150" align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>