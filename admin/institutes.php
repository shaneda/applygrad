<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

$sql = "";
$err = "";
$institutes = array();
//DebugBreak();
if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{

				mysql_query("DELETE FROM institutes WHERE id=".$itemId)
				or die(mysql_error());
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select id,name
from institutes
order by name";

$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($institutes, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Institutes';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<form accept-charset="utf-8" id="form1" action="" method="post">
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Institutes</span><br />
<br />

	<a href="instituteEdit.php">Add institute</a><br />
	<br />
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td width="60" align="right">Delete</td>
      </tr>
     <? for($i = 0; $i < count($institutes); $i++){?>
	<tr>
        <td><a href="instituteEdit.php?id=<?=$institutes[$i][0]?>"><?=$institutes[$i][1]?></a></td>
        <td width="60" align="right">
        <?php
        //showEditText("X", "button", "btnDelete_".$institutes[$i][0], $_SESSION['A_allow_admin_edit']);   
        if($_SESSION['A_usertypeid'] == 0 || 
            ( $_SESSION['A_usertypeid'] == 1 && $_SESSION['roleDepartmentName'] == 'School of Computer Science' ) )
        {
        ?>
         <input name="btnDelete_<?php echo $institutes[$i][0]; ?>" 
            type="submit" class="tblItem" id="btnDelete_<?php echo $institutes[$i][0]; ?>" value="X" 
            onClick="return confirmSubmit('<?php echo $institutes[$i][1]; ?>')" />
        <?php 
        }
        ?>
        </td>
      </tr>
	<? } ?>
    </table>

	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<script LANGUAGE="JavaScript">
<!--
// Nannette Thacker http://www.shiningstar.net
function confirmSubmit(val)
{
var agree=confirm("Are you sure you wish to delete record "+val+"?");
if (agree)
    return true ;
else
    return false ;
}
// -->
</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>