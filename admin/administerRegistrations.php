<?php
$startTime = microtime(TRUE);
ini_set('memory_limit', '160M');
header("Cache-Control: no-cache");

// Standard includes.
include "../inc/config.php";
include "../inc/session_admin.php";

// Page-specific includes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Domain.php"; 
include "../classes/DB_Applyweb/class.DB_Department.php";
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminList.php";  
include "../classes/DB_Applyweb/class.VW_RegistrationListBase.php";
include "../classes/DB_Applyweb/class.VW_AdminListDomain.php";
include "../classes/DB_Applyweb/class.VW_AdminListPeriod.php";
include "../classes/DB_Applyweb/class.VW_AdminListRecommendationCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListGreSubjectCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListToeflCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListIeltsCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListGmatCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListFees.php";
include "../classes/DB_Applyweb/class.VW_AdminListPrograms.php";
include "../classes/DB_Applyweb/class.VW_AdminListNoteCount.php";
include "../classes/DB_Applyweb/class.VW_Programs.php"; 
include "../classes/class.RegistrationListData.php";
include "../classes/Structures_DataGrid/class.RegistrationList_DataGrid.php";
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
include '../inc/specialCasesAdmin.inc.php';

/*
* Handle the request variables.
*/
if ( isset($_REQUEST['unit']) && $_REQUEST['unit'] ) {
    $unit = $_REQUEST['unit'];
} else {
    //$unit = 'department';
    $unit = NULL;
}

if ( isset($_REQUEST['unitId']) && $_REQUEST['unitId'] ) {
    $unitId = $_REQUEST['unitId'];
} else {
    //$unitId = 1;
    $unitId = NULL;
}

if ( isset($_REQUEST['period']) && $_REQUEST['period'] ) {
    $periodId = $_REQUEST['period'];    
} else {
    if ( $_SESSION['A_usertypeid'] == 0 ) {
        // superuser
        $periodId = NULL;
    } else {
        header('Location: home.php');        
    } 
}

// Page size
if ( isset($_REQUEST['limit']) ) {
    $limit = $_REQUEST['limit'];
} else {
    $limit = 100;
}

// Program limit
if ( isset($_REQUEST['programId']) && $_REQUEST['programId'] != '' ) {
    $programId = $_REQUEST['programId'];
} else {
    $programId = NULL;
}

if ( isset($_REQUEST['department']) && $_REQUEST['department'] != '' ) {
    $departmentId = $_REQUEST['department'];
} else {
    $departmentId = NULL;
}

// Handle searchString and applicationId and set 
// the post-search "return to list" link accordingly.
$returnUrl = "administerRegistrations.php?unit=" . $unit . "&unitId=" . $unitId;
if ($periodId) {
    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids 
    if (is_array($periodId)) {
        foreach ($periodId as $id) {
            $returnUrl .= "&period[]=" . $id;    
        }   
    } else {
        $returnUrl .= "&period=" . $periodId;    
    }    
}
if ($programId) {
    $returnUrl .= "&programId=" . $programId;    
}
$returnLink = "";
$returnLinkOn = '<span class="return"><a href=" ';
$returnLinkOn .= $returnUrl . '" class="return" onClick="document.filterForm.submit(); return false;">';
$returnLinkOn .= 'Return to full list</a></span>';
$searchString = '';
$applicationId = NULL;
$luuId = NULL;
$showReturnLink = '';
if ( isset($_REQUEST['searchString']) ) {
    $searchString = $_REQUEST['searchString'];    
}
if ( isset($_REQUEST['applicationId']) ) {
    $showReturnLink = 'show';
    $applicationId = $_REQUEST['applicationId'];
}
if ( isset($_REQUEST['luuId']) ) {
    $luuId = $_REQUEST['luuId'];
}
if ( isset($_REQUEST['showReturnLink']) ) {
    $showReturnLink = $_REQUEST['showReturnLink'];    
}
if ( isset($_REQUEST['submitJumpTo']) || $showReturnLink == 'show' ) {
    $returnLink = $returnLinkOn;    
}

// Handle the filter-related request variables.
$paymentStatus = "allPayment";    // default paid or waived
if ( isset($_REQUEST['paymentStatus']) ) {
    $paymentStatus = $_REQUEST['paymentStatus'];
}
$unpaidUnwaivedSelected = $paidWaivedSelected = $paidSelected = $waivedSelected = $allPaymentSelected = '';
$paymentSelected = $paymentStatus . 'Selected';
$$paymentSelected = "selected";

// get the application period info
// PLB added 02/06/09
if ( isset($_REQUEST['period']) ) {

    $application_period_id = $_REQUEST['period'];
    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids 
    if (is_array($_REQUEST['period'])) {
        // The scs period will be first and the compbio period will be second
        $displayPeriodId = $_REQUEST['period'][1];    
    } else {
        $displayPeriodId = $_REQUEST['period']; 
    }
    
    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    if ($endDate == '') {
        $endDate = 'present';    
    }
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    if ($periodName) {
        $application_period_display = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $application_period_display = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $application_period_display = $periodDates;     
    }
    $application_period_display .= '<br/>';    

} else {
    $application_period_id = NULL;
    $start_date = NULL;
    $end_date = NULL;
    $application_period_display = "";
}

// Set the domain/department name
switch ($unit) {
    
    case 'domain':
    
        $dbUnit = new DB_Domain();
        break;
    
    case 'department':
    
        $dbUnit = new DB_Department();
        break;
    
    default:
        
        $dbUnit = NULL;   
}

if ($dbUnit && $unitId) {
    $unitRecord = $dbUnit->get($unitId);
    $unitName = $unitRecord[0]['name'];    
} else {
    $unitName = '';    
}

 
/* Instantiate the datagrid and data access class 
* and bind the data to the datagrid.
*/
//DebugBreak();
if (isset($programId)) {
    $adminListData = new RegistrationListData('program', $programId);    
} else {
    $adminListData = new RegistrationListData($unit, $unitId);    
}


if (!$periodId && (!$searchString || $searchString == '') 
    && $_SESSION['A_usertypeid'] == 0) {

    $noResults = 'Jump to applicant using name, email, or application id.';
    $adminListArray = array();
    
} else {
    
    $noResults = 'No applications found for this period with these filter settings.';
    $adminListArray = $adminListData->getData(
        $application_period_id, $applicationId, $searchString, 
        NULL, NULL, $paymentStatus,
        NULL, NULL, NULL,
        NULL, $luuId);   
}


if ($unit == 'department' && isIsreeDepartment($unitId)) {
    
    // Handle the visibility filter here
    if ($hiddenStatus == 'unhidden' || $hiddenStatus == 'hidden') {
        
        $tempArray = array();
        foreach ($adminListArray as $adminListRecord) {
            if ($hiddenStatus == 'unhidden' && $adminListRecord['hide'] == 0) {
                $tempArray[] = $adminListRecord;     
            } elseif ($hiddenStatus == 'hidden' && $adminListRecord['hide'] == 1) {
                $tempArray[] = $adminListRecord;     
            }
        }
        $adminListArray = $tempArray;    
    }
}
    

$dataGrid = new RegistrationList_DataGrid($adminListArray, $limit);    


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Administer Registrations';

$pageCssFiles = array(
    '../css/jquery.autocomplete.css',
    '../css/administerApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/administerRegistrations.js',
    '../javascript/ajaxfileupload.js'
    );
 
include '../inc/tpl.pageHeader.php';

// Render the view. 
?>
<div style="margin: 10px;">

<table cellspacing="2" width="100%" border="0">
    <tr valign="top">
        <td align="left">

<span class="title">
<?php 
if ($unitName) {
    echo $unitName . '<br/>';    
}
echo $application_period_display; 
?> 
<span style="font-size: 18px;">
Administer Registrations
<?php
if (!$periodId && $_SESSION['A_usertypeid'] == 0) {
    echo ' (SU)';    
}
?>
</span>  
</span>
     
        </td>
        <td  rowspan="2" align="right" width="40%">

<?php
include '../inc/tpl.administerRegistrationsFilterForm.php';
?>

    </td>  
        </tr>

        <tr valign="bottom">
        
        <td align="left">
<br/>

<form id="jumpToForm" name="jumpToForm" action="<?php echo $returnUrl; ?>" method="GET">   
    <b>Jump to applicant:</b><br/>
    <input type="text" id="suggest" name="searchString" size="40"/>
    <input type="hidden" id="luuId" name="luuId" />
    <input type="hidden" id="jumpToUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="jumpToUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <?php
    if ($periodId) {        
        // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod' . $id . '" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod" name="period" value="' . $periodId . '"/>';     
        }  
    }
    if ($programId) {
        echo '<input type="hidden" id="jumpToProgramId" name="programId" value="' . $programId . '" />';    
    }
    ?>
    <input type="hidden" id="jumpToLimit" name="limit" value="<?php echo $limit; ?>"/>
    <input type="hidden" name="submissionStatus" value="<?php echo $submissionStatus; ?>"/>
    <input type="hidden" name="completionStatus" value="<?php echo $completionStatus; ?>"/>
    <input type="hidden" name="paymentStatus" value="<?php echo $paymentStatus; ?>"/>
    <input type="hidden" name="testScoreStatus" value="<?php echo $testScoreStatus; ?>"/>
    <input type="hidden" name="transcriptStatus" value="<?php echo $transcriptStatus; ?>"/>
    <input type="hidden" name="recommendationStatus" value="<?php echo $recommendationStatus; ?>"/>
    <input type="hidden" name="round" value="<?php echo $round; ?>"/>
    <input type="hidden" name="hiddenStatus" value="<?php echo $hiddenStatus; ?>"/>
    <input type="hidden" name="showReturnLink" value="show"/> 
    <input type="submit" name="submitJumpTo" value="Go" />    
</form>
        </td>
    </tr>
</table>


<div style="text-align: right; float: right; margin: 5px; padding: 0px;"> 
<?php
if (!$periodId && $_SESSION['A_usertypeid'] == 0) {

    $DB_Domain = new DB_Domain();
    $unitItems = $DB_Domain->get();
    
    include '../inc/tpl.administerApplicationsDomainForm.php';
    
//} elseif ( $unit == 'department' && $unitId == 18 ) {
} else {   
    $programsView = new VW_Programs();
    $unitItems = $programsView->find($unit, $unitId);
    
    if (count($unitItems) > 1) {
    ?>

    <form id="programForm" name="programForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
        Limit to program: <select name="programId" onChange="document.programForm.submit();">
        <option value="">All <?php echo $unitName; ?> programs</option>
    <?php
        foreach ($unitItems as $program) {
            $selected = '';
            if ($program['program_id'] == $programId) {
                $selected = 'selected';
            }
            echo '<option value="' . $program['program_id'] . '" ' . $selected . '>' . $program['program_name'] . '</option>';    
        }
    ?>
        </select>
        <input type="hidden" id="programLimit" name="limit" value="<?php echo $limit; ?>" /> 
        <input type="hidden" id="limitUnit" name="unit" value="<?php echo $unit; ?>" />
        <input type="hidden" id="limitUnitId" name="unitId" value="<?php echo $unitId; ?>" />
        <?php
        if ($periodId) {      
            // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
            if (is_array($periodId)) {
                foreach ($periodId as $id) {
                    echo '<input type="hidden" id="limitPeriod" name="period[]" value="' . $id . '"/>';     
                }   
            } else {
                echo '<input type="hidden" id="limitPeriod" name="period" value="' . $periodId . '"/>';     
            } 
        }
        ?>
        <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
        <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
        <input type="hidden" name="submissionStatus" value="<?php echo $submissionStatus; ?>"/>
        <input type="hidden" name="completionStatus" value="<?php echo $completionStatus; ?>"/>
        <input type="hidden" name="paymentStatus" value="<?php echo $paymentStatus; ?>"/>
        <input type="hidden" name="testScoreStatus" value="<?php echo $testScoreStatus; ?>"/>
        <input type="hidden" name="transcriptStatus" value="<?php echo $transcriptStatus; ?>"/>
        <input type="hidden" name="recommendationStatus" value="<?php echo $recommendationStatus; ?>"/>
        <input type="hidden" name="round" value="<?php echo $round; ?>"/> 
        <input type="hidden" name="hiddenStatus" value="<?php echo $hiddenStatus; ?>"/>
        <input type="hidden" name="showReturnLink" value="<?php echo $showReturnLink; ?>"/>
    </form>
<?php
    }
} 
?>
</div> 
<br/> 

<div style="clear:both; ">


<div style="text-align: left; float: left; width: 100%; margin-left: 5px; margin-bottom: 5px; padding: 0px;">
<?php echo $returnLink; ?>
</div>

<div class="paging">
<?php $dataGrid->render(DATAGRID_RENDER_PAGER); ?>
</div>

<div style="text-align: left; float: left; margin-left: 5px; margin-bottom: 5px; padding: 0px;">

<?php
// Display info about the record set being displayed
$recordCount = $dataGrid->getRecordCount(); 
if ($recordCount >= 1) {
    ?>
    <form id="pageSizeForm" name="pageSizeForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET"> 
    <?php
    $recordStart = $dataGrid->getCurrentRecordNumberStart();
    $recordEnd = $dataGrid->getCurrentRecordNumberEnd();
    echo "Record(s) <b>" . $recordStart . " - " . $recordEnd . " of " . $recordCount . "</b>"; 
    if ($searchString) {
        if (mb_detect_encoding($searchString) == 'UTF-8') {
            $displayString = utf8_decode($searchString);
        } else {
            $displayString = $searchString;
        }
        echo ' for <i>' . htmlentities($displayString) . "</i>";
    }
    // Menu for changing page size
    $limit100Selected = $limit200Selected = $limit500Selected = '';
    $limitSelected = 'limit' . $limit . 'Selected';
    $$limitSelected = "selected";
    ?>
    &nbsp;&nbsp;
    <select name="limit" onChange="document.pageSizeForm.submit(); return false;">
        <option value="100" <?php echo $limit100Selected; ?>>100 per page</option>
        <option value="200" <?php echo $limit200Selected; ?>>200 per page</option>
        <option value="500"<?php echo $limit500Selected; ?>>500 per page</option>
    </select>
    <input type="hidden" id="limitUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="limitUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <?php
    if ($periodId) {      
        // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" id="limitPeriod" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" id="limitPeriod" name="period" value="' . $periodId . '"/>';     
        } 
    }
    if ($programId) {
        echo '<input type="hidden" id="limitProgramId" name="programId" value="' . $programId . '" />';    
    }
    ?>
    <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
    <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
    <input type="hidden" name="paymentStatus" value="<?php echo $paymentStatus; ?>"/>
    <input type="hidden" name="showReturnLink" value="<?php echo $showReturnLink; ?>"/>
    </form>
    <?php
         
} elseif ($recordCount == 0) {
    
    if ($searchString) {
        echo '<b>No matches found for ';
        echo  '<i>'. htmlentities( utf8_decode($searchString) ) . '</i></b>';   
    } else {
        echo '<b>' . $noResults . '</b>';
    }
}
?>
</div>

<br />
<div style="clear: left;">
<?php $dataGrid->render(); ?>
</div>

<div class="paging">
<?php $dataGrid->render(DATAGRID_RENDER_PAGER); ?>
</div>

</div>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>