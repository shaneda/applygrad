<?php
/*
* Shared includes 
*/                                                                             
$exclude_scriptaculous = TRUE;
include_once "../inc/session_admin.php";
include_once '../inc/functions.php';
define("CLASS_DIR", "../classes/");
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb_Table.php';
include '../inc/unitPeriodIncludes.inc.php';
include '../inc/specialCasesAdmin.inc.php';

/*
* GET variables
*/
$unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);
if (!$unit || !$unitId || !$periodId)
{
    header('Location: home.php');
    exit;    
}

/*
* DB 
*/
$dbApplyweb = new DB_Applyweb();

/*
* POST variables
*/
$selectUserSubmit = filter_input(INPUT_POST, 'selectUserSubmit', FILTER_SANITIZE_STRING);
$selectContentSubmit = filter_input(INPUT_POST, 'selectContentSubmit', FILTER_SANITIZE_STRING);
$cancelSelectContentSubmit = filter_input(INPUT_POST, 'cancelSelectContentSubmit', FILTER_SANITIZE_STRING);
$generatePdfSubmit = filter_input(INPUT_POST, 'generatePdfSubmit', FILTER_SANITIZE_STRING);
$cancelGeneratePdfSubmit = filter_input(INPUT_POST, 'cancelGeneratePdfSubmit', FILTER_SANITIZE_STRING);
$applicationId = filter_input(INPUT_POST, 'applicationId', FILTER_VALIDATE_INT);
$selectedLetterSections = filter_input(INPUT_POST, 'selectedLetterSections', FILTER_VALIDATE_BOOLEAN, FILTER_REQUIRE_ARRAY);

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Create Admission Letter';

$pageCssFiles = array(
    '../css/administerApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/administerApplications.js',
    );

// Get the <html><head></head> and <body> template
include '../inc/tpl.pageHeader.php';    
?>
<div id="pageContentMainDiv" style="margin: 5px;">
<div style="margin: 10px;">
<?php
echo mainHeading($periodId);

if (($selectUserSubmit || $selectContentSubmit) && !$applicationId)
{
?>
    <br><span class="errorSubtitle">You must select a user</span>
<?php
}
if ($generatePdfSubmit && $applicationId)
{
    include '../inc/admissionLetterGeneratedPdf.inc.php';
}
elseif (($selectUserSubmit && $applicationId) || $cancelGeneratePdfSubmit)
{
    include '../inc/tpl.admissionLetterSelectContent.php';    
}
elseif ($selectContentSubmit && $applicationId)
{
    include '../inc/tpl.admissionLetterCreate.php';    
} 
else
{
    include '../inc/tpl.admissionLetterSelectUser.php';    
}
?>

</div>
</div>


<?php
// Get the </body></html> template
include '../inc/tpl.pageFooter.php';

function mainHeading($periodId)
{
    global $pageTitle;
    $period = new Period($periodId);
    $unit = new Unit( $period->getUnitId() );
        
    $submissionPeriods = $period->getChildPeriods(2);
    if (count($submissionPeriods) > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    if ($periodName) {
        $application_period_display = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $application_period_display = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $application_period_display = $periodDates;     
    }
    
    return '<span class="title">' . $unit->getName() . '<br/>'
        . $application_period_display . '<br/> 
        <span style="font-size: 18px;">' . $pageTitle . '</span>  
        </span>'; 
}

function getApplicantInfo($applicationId)
{
    $query = "SELECT application.id AS application_id, 
        application.user_id AS lu_users_usertype_id,
        users.id AS users_id,
        users.guid,
        users.title, 
        users.firstname, 
        users.lastname, 
        users.email, 
        users_info.address_cur_street1 AS current_street1, 
        users_info.address_cur_street2 AS current_street2,
        users_info.address_cur_street3 AS current_street3,
        users_info.address_cur_street4 AS current_street4,
        users_info.address_cur_city AS current_city,
        cur_state.name AS current_state,
        users_info.address_cur_pcode AS current_postal_code,
        cur_country.name AS current_country,
        users_info.address_perm_street1 AS permanent_street1, 
        users_info.address_perm_street2 AS permanent_street2,
        users_info.address_perm_street3 AS permanent_street3,
        users_info.address_perm_street4 AS permanent_street4,
        users_info.address_perm_city AS permanent_city,
        perm_state.name AS permanent_state,
        users_info.address_perm_pcode AS permanent_postal_code,
        perm_country.name AS permanent_country
        FROM application
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
        LEFT OUTER JOIN states AS cur_state 
            ON users_info.address_cur_state = cur_state.id
        LEFT OUTER JOIN states AS perm_state 
            ON users_info.address_perm_state = perm_state.id
        LEFT OUTER JOIN countries AS cur_country 
            ON users_info.address_cur_country = cur_country.id
        LEFT OUTER JOIN countries AS perm_country 
            ON users_info.address_perm_country = perm_country.id
        WHERE application.id = " . intval($applicationId);

    global $dbApplyweb;
    
    $result = $dbApplyweb->handleSelectQuery($query);
    
    if (isset($result[0]))
    {
        return $result[0];
    }
    
    return NULL;
}

function getAdmittedPrograms($applicationId)
{
    $query = "SELECT application_id,
        admission_program_id,
        CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS program_name,
        NULL AS choice,
        NULL AS scholarship_amt
        FROM application_decision
        INNER JOIN programs ON application_decision.admission_program_id = programs.id
        INNER JOIN degree ON degree.id = programs.degree_id
        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
        WHERE application_id = " . intval($applicationId) . "
        ORDER BY choice";
    
    global $dbApplyweb;
    
    $result = $dbApplyweb->handleSelectQuery($query);
    
    return $result;    
}

function getAdmittedProgramsIni($applicationId)
{
    $query = "SELECT application_id,
        admission_program_id,
        CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS program_name,
        choice,
        scholarship_amt
        FROM application_decision_ini
        INNER JOIN programs ON application_decision_ini.admission_program_id = programs.id
        INNER JOIN degree ON degree.id = programs.degree_id
        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
        WHERE application_decision_ini.admission_status = 2 
        AND application_id = " . intval($applicationId) . "
        ORDER BY choice";
    
    global $dbApplyweb;
    
    $result = $dbApplyweb->handleSelectQuery($query);
    
    return $result;    
}

function getAdmittedProgramsDesignMasters($applicationId)
{
    $query = "SELECT application_id,
        admission_program_id,
        CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS program_name,
        choice
        FROM application_decision_design
        INNER JOIN programs ON application_decision_design.admission_program_id = programs.id
        INNER JOIN degree ON degree.id = programs.degree_id
        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
        WHERE application_decision_design.admission_status = 2 
        AND application_id = " . intval($applicationId) . "
        ORDER BY choice";

    global $dbApplyweb;
    
    $result = $dbApplyweb->handleSelectQuery($query);
    
    return $result;    
}

function getContentTemplates($unit, $unitId)
{
    $query = "SELECT * FROM content
        WHERE contenttype_id = 7 ";
    
    if ($unit == 'domain')
    {
        $query .= "AND domain_id = ";
    }
    else
    {
        $query .= "AND department_id = ";    
    }
    
    $query .= intval($unitId);
    
    global $dbApplyweb;
    
    return $dbApplyweb->handleSelectQuery($query, 'name');
}

function getAddress($applicantInfo)
{   
    $address = $applicantInfo['firstname'] . ' ' . $applicantInfo['lastname'] . '<br/>';
    $address .= $applicantInfo['current_street1']  . '<br/>';
    if (isset($applicantInfo['current_street2']) && $applicantInfo['current_street2'] != "")  {
        $address .= $applicantInfo['current_street2']  . '<br/>';
    }
    if (isset($applicantInfo['current_street3']) && $applicantInfo['current_street3'] != "")  {
        $address .= $applicantInfo['current_street3']  . '<br/>';
    }
    if (isset($applicantInfo['current_street4']) && $applicantInfo['current_street4'] != "")  {
        $address .= $applicantInfo['current_street4']  . '<br/>';
    }
    $address .= $applicantInfo['current_city'] . ', ' . $applicantInfo['current_state'];
    $address .= ' ' . $applicantInfo['current_postal_code'] . '<br/>';
    $address .= $applicantInfo['current_country'];
    return $address;
}

function parseContentTemplate($templateString, $values = NULL)
{
    if (is_array($values))
    {
        foreach ($values as $key => $value)
        {
            $templateString = str_replace("##" . $key . "##", $value, $templateString);    
        }
    }
    
    return html_entity_decode($templateString);
}

function getLetterSections()
{
    $letterSections = array(
        1 => array('name' => 'Admission Letter - Introduction', 'optional' => FALSE),
        2 => array('name' => 'Admission Letter - Acceptance Instructions', 'optional' => FALSE),
        3 => array('name' => 'Admission Letter - Conclusion', 'optional' => FALSE)
    );
    
    return $letterSections;
}

function getLetterSectionsDesign()
{
    $letterSections = array(
        1 => array('name' => 'Admission Letter - Introduction', 'optional' => FALSE),
        2 => array('name' => 'Admission Letter - Acceptance Instructions', 'optional' => FALSE),
        3 => array('name' => 'Admission Letter - Design Section 1', 'optional' => TRUE),
        4 => array('name' => 'Admission Letter - Design Section 2', 'optional' => TRUE),
        5 => array('name' => 'Admission Letter - Conclusion', 'optional' => FALSE)
    );
    
    return $letterSections;
}

function getLetterSectionsDesignPhd()
{
    $letterSections = array(
        1 => array('name' => 'Admission Letter - Introduction', 'optional' => FALSE),
        2 => array('name' => 'Admission Letter - Acceptance Instructions', 'optional' => FALSE),
        3 => array('name' => 'Admission Letter - Design Section 1', 'optional' => TRUE),
        4 => array('name' => 'Admission Letter - Design Section 2', 'optional' => TRUE),
        5 => array('name' => 'Admission Letter - Conclusion', 'optional' => FALSE)
    );
    
    return $letterSections;
}

function getLetterSectionsDesignDdes()
{
    $letterSections = array(
        1 => array('name' => 'Admission Letter - Introduction', 'optional' => FALSE),
        2 => array('name' => 'Admission Letter - Acceptance Instructions', 'optional' => FALSE),
        3 => array('name' => 'Admission Letter - Design Section 1', 'optional' => TRUE),
        4 => array('name' => 'Admission Letter - Design Section 2', 'optional' => TRUE),
        5 => array('name' => 'Admission Letter - Conclusion', 'optional' => FALSE)
    );
    
    return $letterSections;
}

function getLetterSectionsIni()
{
    $letterSections = array(
        1 => array('name' => 'Admission Letter - Introduction', 'optional' => FALSE),
        2 => array('name' => 'Admission Letter - Dual Program Admit', 'optional' => TRUE),
        3 => array('name' => 'Admission Letter - Acceptance Instructions', 'optional' => FALSE),
        4 => array('name' => 'Admission Letter - ACC Required', 'optional' => TRUE),
        5 => array('name' => 'Admission Letter - ACC Recommended', 'optional' => TRUE),
        6 => array('name' => 'Admission Letter - International Admit', 'optional' => TRUE),
        7 => array('name' => 'Admission Letter - Tuition Rate', 'optional' => FALSE),
        8 => array('name' => 'Admission Letter - EWF Fellowship', 'optional' => TRUE),
        9 => array('name' => 'Admission Letter - Pending Scholarship', 'optional' => TRUE),
        10 => array('name' => 'Admission Letter - No Scholarship', 'optional' => TRUE),
        11 => array('name' => 'Admission Letter - Partial Tuition Scholarship', 'optional' => TRUE),
        12 => array('name' => 'Admission Letter - Full Scholarship', 'optional' => TRUE),
        13 => array('name' => 'Admission Letter - Partial Scholarship + 3 Semester Research', 'optional' => TRUE),
        14 => array('name' => 'Admission Letter - Partial Scholarship + 2 Semester Research', 'optional' => TRUE),
        15 => array('name' => 'Admission Letter - Goel Award', 'optional' => TRUE),
        16 => array('name' => 'Admission Letter - FJM Award', 'optional' => TRUE),
        17 => array('name' => 'Admission Letter - SFS Candidate', 'optional' => TRUE),
        18 => array('name' => 'Admission Letter - EWF Candidate', 'optional' => TRUE),
        19 => array('name' => 'Admission Letter - Open House', 'optional' => TRUE),
        20 => array('name' => 'Admission Letter - Conclusion', 'optional' => FALSE)
    );
    
    return $letterSections;
}

function getFileLink($applicantInfo, $unitId)
{
    $filename = 'admissionLetter_' . $applicantInfo['application_id'] . '_' . $unitId . '.pdf';
    $path = $_SESSION['datafileroot'] . '/' . $applicantInfo['guid'] . '/' . $filename;
    $fileLink = '';
    
    if (file_exists($path))
    {
        $fileHref = 'fileDownload.php?file=' . urlencode($filename) .'&amp;guid=' . $applicantInfo['guid'];
        $fileLink = '<a href="' . $fileHref . '" target="_blank">' . $filename . '</a>'; 
    }
    
    return $fileLink; 
}
?>