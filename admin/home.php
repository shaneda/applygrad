<?php
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

/*
* Includes for period.
*/
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php'; 
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/class.Period.php';
include '../classes/class.Unit.php'; 
include '../inc/specialCasesAdmin.inc.php';

/*
* Run through the main controller logic 
*/
$domains = array();
$departments = array();
$department_id = -1;

$suDomainId = filter_input(INPUT_GET, 'suDomainId', FILTER_VALIDATE_INT);
$_SESSION['suDomainId'] = $suDomainId;

$suDepartmentId = filter_input(INPUT_GET, 'suDepartmentId', FILTER_VALIDATE_INT);
$_SESSION['suDepartmentId'] = $suDepartmentId;
// GET DOMAINS
$sql = "select distinct domain.id, domain.name
from ";
if($_SESSION['A_usertypeid'] != 0)  // is not a super user
{
	// PLB changed query to use lu_users_usertypes id instead of users id 9/16/09
    $sql .= "lu_user_department
		left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id
		left outer join domain on domain.id= lu_domain_department.domain_id
		left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id
		where lu_users_usertypes.id= " . intval($_SESSION['A_userid']) . " 
        and (lu_users_usertypes.usertype_id = " . intval($_SESSION['A_usertypeid']) . ") ";
}
elseif ($suDomainId != NULL)
{
    $sql .= " domain WHERE domain.id = " . $suDomainId;        
}
else
{
	$sql .= " domain ";
}
$sql .= " order by domain.name";
$result = mysql_query($sql) or die(mysql_error());

// PLB added domainCount, unit query 9/15/09
$domainCount = mysql_numrows($result);

$domainPeriods = array();
$showAdminByDomain = FALSE;
if ($domainCount == 0) {
    $domainUnitQuery = "SELECT domain.id, domain.name FROM domain
                        INNER JOIN domain_unit ON domain.id = domain_unit.domain_id
                        INNER JOIN unit_role ON domain_unit.unit_id = unit_role.unit_id
                        WHERE unit_role.lu_users_usertypes_id = " . $_SESSION['A_userid'];
    $result = mysql_query($domainUnitQuery) or die(mysql_error());
    $domainCount = mysql_numrows($result);
    if ($domainCount > 0) {
        $showAdminByDomain = TRUE;    
    }     
}

if (isset($_SESSION['roleDepartmentId'])) {
            $deptcheck =  $_SESSION['roleDepartmentId'];
        }  else {
            $deptcheck = -1;
        }

if ($suDepartmentId == null)
{
    // This is not an SU view of a single department,
    // so go ahead and get the domains.
    
    while($row = mysql_fetch_array( $result ))
    {
        if($row['name'] != "")
        {
            $arr = array();
            array_push($arr, $row['id']);
            array_push($arr, $row['name']);
            array_push($domains, $arr);
            
            $domainPeriods[$row['id']] = queryDomainPeriods($row['id']);
        }
    }
}

/*
* PLB added test for adminByRole session variable 7/2/09.
* This enables strict role-based use of the system when
* the role menu is used at login.
*/
$idWhere = "users.id = " .$_SESSION['A_usermasterid'];
if ( isset($_SESSION['adminByRole']) &&  $_SESSION['adminByRole'] == TRUE) {
    $idWhere = "lu_users_usertypes.id = " . intval($_SESSION['A_userid']);    
}

//GET DEPARTMENTS
$sql = "select
department.id,
department.name,
department.enable_round1,
department.enable_round2,
department.enable_round3,
department.enable_round4,
department.enable_final
from  ";
if($_SESSION['A_usertypeid'] != 0) // is not a super user  
{
     $sql .= " lu_user_department
    left outer join department on department.id = lu_user_department.department_id
    left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id
    left outer join users on users.id = lu_users_usertypes.user_id
    where " . $idWhere . " 
    and (lu_users_usertypes.usertype_id = " . intval($_SESSION['A_usertypeid']) . ")";
}
elseif($suDepartmentId != NULL)
{
    $sql .= " department WHERE department.id = " . $suDepartmentId;
}
elseif ($suDomainId != NULL)
{
    $sql .= " department INNER JOIN lu_domain_department
                ON lu_domain_department.department_id = department.id
                AND lu_domain_department.domain_id = " . $suDomainId;
}
else
{
	$sql .= " department ";
}
$sql .= " group by department.id order by department.name"; 
$result = mysql_query($sql) or die(mysql_error());

// PLB added departmentCount, unit query 9/15/09
$departmentCount = mysql_numrows($result);

$resultrows = mysql_query($sql) or die(mysql_error());
if ($departmentCount == 1 ) {
    $row =  mysql_fetch_array( $resultrows );
    $numRounds = 0;
    if ($row['enable_round1'] == 1) 
        $numRounds++;
    if ($row['enable_round2'] == 1) 
        $numRounds++;
    if ($row['enable_round3'] == 1) 
        $numRounds++;
    if ($row['enable_round4'] == 1) 
        $numRounds++;
}

$departmentPeriods = array();
if ($departmentCount == 0) {
    $domainIdArray = array();
    foreach ($domains as $domain) {
        $domainIdArray[] = $domain[0];
    } 
    
    $inDomainIds = implode(',', $domainIdArray);
    $departmentQuery = "SELECT DISTINCT department.id, department.name, department.enable_round1, 
                            department.enable_round2, department.enable_round3, department.enable_round4, department.enable_final
                            FROM department
                            INNER JOIN lu_domain_department ON department.id = lu_domain_department.department_id
                            WHERE lu_domain_department.domain_id IN (" . $inDomainIds . ")";                        
    $result = mysql_query($departmentQuery) or die(mysql_error());
    $departmentCount = mysql_numrows($result);    
} 

$displayCompbioDepartment = FALSE;
$displayMrsdDepartment = FALSE;

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['enable_round1']);
	array_push($arr, $row['enable_round2']);
	array_push($arr, $row['enable_final']);
    array_push($arr, $row['enable_round3']);    // $departments[$i][5]
    array_push($arr, $row['enable_round4']);
	array_push($departments, $arr);
    
    if ($row['id'] == 8) {
        $displayCompbioDepartment = TRUE;    
    }
    
    if ($row['id'] == 66) {
        $displayMrsdDepartment = TRUE;    
    }
    $departmentPeriods[$row['id']] = queryDepartmentPeriods($row['id']);   
}

/*
* PLB added 12/8/09: Determine for each department which periods 
* have committee view all comments turned on or off.
*/

/*
if ( isset($_REQUEST['commentsToggleDepartmentId']) 
    && isset($_REQUEST['commentsTogglePeriodId']) 
    && isset($_REQUEST['commentsToggle']) ) {

    $toggleQuery = "INSERT INTO department_review_config VALUES(
                    " . $_REQUEST['commentsToggleDepartmentId'] .",
                    " . $_REQUEST['commentsTogglePeriodId'] . ",
                    " . $_REQUEST['commentsToggle'] . "
                    ) ON DUPLICATE KEY UPDATE 
                    all_comments_on = " . $_REQUEST['commentsToggle'];
    $toggleResult = mysql_query($toggleQuery) or die(mysql_error());
}

$reviewCommentsOn = array();
$reviewCommentsOnQuery = "SELECT * FROM department_review_config";
$reviewCommentsOnResult = mysql_query($reviewCommentsOnQuery) or die(mysql_error());
while ($row = mysql_fetch_array($reviewCommentsOnResult)) {
    $reviewCommentsOn[$row['department_id']][$row['period_id']] = $row['all_comments_on'];        
}
*/

/*
* TEMPORARY: CompBio and MRSD are in their own domains, as well as in the SCS/RI domain.
* Re-use the following when making links for review, admission.
*/
$compbioDepartmentId = 8;
$activeCompbioPeriodId = NULL;
$mrsdDepartmentId = 66;
$activeMrsdPeriodId = NULL;
$activeScsPeriodId = NULL;
if ($displayCompbioDepartment || $displayMrsdDepartment) {

    $scsUnit = new Unit(1);
    $activeScsPeriods = $scsUnit->getActivePeriods(1, FALSE);
    foreach ($activeScsPeriods as $activeScsPeriod) {
        $activeScsPeriodId = $activeScsPeriod->getId();    
    }
    
    if ($displayCompbioDepartment) {
        $compbioUnit = new Unit(79);
        $activeCompbioPeriods = $compbioUnit->getActivePeriods(1, FALSE, FALSE);
        foreach ($activeCompbioPeriods as $activeCompbioPeriod) {
            $activeCompbioPeriodId = $activeCompbioPeriod->getId();    
        } 
    }
    
    if ($displayMrsdDepartment) {
        $mrsdUnit = new Unit(66);  
        $activeMrsdPeriods = $mrsdUnit->getActivePeriods(1, FALSE, FALSE);
        foreach ($activeMrsdPeriods as $activeMrsdPeriod) {
            $activeMrsdPeriodId = $activeMrsdPeriod->getId();    
        }   
    }
}


// Check for the viewPeriods request variable
$viewPeriods = 'current';
$viewPeriodsCurrentChecked = 'checked';
$viewPeriodsAllChecked = '';
if( isset($_REQUEST['viewPeriods']) ) { 
    
    if( $_REQUEST['viewPeriods'] == 'all' ) {
        
        $_SESSION['viewPeriods'] = 'all';
        $viewPeriods = 'all';
        $viewPeriodsCurrentChecked = '';
        $viewPeriodsAllChecked = 'checked';    
    
    } else {
        
        $_SESSION['viewPeriods'] = 'current';
        $viewPeriods = 'current';
        $viewPeriodsCurrentChecked = 'checked';
        $viewPeriodsAllChecked = '';           
    }
    
} elseif ( isset($_SESSION['viewPeriods']) && $_SESSION['viewPeriods'] == 'all') {

    $viewPeriods = 'all';
    $viewPeriodsCurrentChecked = '';
    $viewPeriodsAllChecked = 'checked';
}


/*
* New for ISR executive training 
*/
$adminUnit = getDepartmentUnit($_SESSION['roleDepartmentId']);

$requestPeriodId = NULL;
if ( isset($_REQUEST['period_id']) ) {
    $requestPeriodId = filter_var($_REQUEST['period_id'], FILTER_VALIDATE_INT);     
}

if (!$requestPeriodId && $adminUnit) {
    
    $unitPeriods = $adminUnit->getPeriods();
    
    // If there is one, make the first open period the default.
    foreach ($unitPeriods as $unitPeriod) {
        $unitPeriodStatus = $unitPeriod->getStatus();
        if ($unitPeriodStatus == 'active') {
            $requestPeriodId = $unitPeriod->getId();
            break;
        }
    }
    
    // If not, make the first past period the default.
    if (!$requestPeriodId && $viewPeriods == 'all') {
        foreach ($unitPeriods as $unitPeriod) {
            $unitPeriodStatus = $unitPeriod->getStatus();
            if ($unitPeriodStatus != 'future') {
                $requestPeriodId = $unitPeriod->getId();
                break;
            }
        }    
    }   
}

if ($_SESSION['A_usertypename'] == 'Administrator'
    && isIsreeDepartment($_SESSION['roleDepartmentId']) )
{
    include '../inc/tpl.adminHomeEet.php';
    exit;    
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Admissions Home';
if ($_SESSION['A_usertypeid'] == 0)
{
    $pageTitle .= ': SU';    
}
if ($suDepartmentId != NULL)
{
    $pageTitle .= ": " . $departments[0][1];
}
if ($suDomainId != NULL)
{
    $pageTitle .= ": " . $domains[0][1] . " (Domain)";
}

$pageCssFiles = array();
$pageJavascriptFiles = array();

 // Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

// Print the body html.
?>
<div style="padding-top: 20px;">   
    <div class="title" style="float: left; font-size: 18px;">
    <?php
        echo $pageTitle;
    ?>
    </div>
    <?php
    if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 18) 
    {    
        $viewPeriodsFormAction = $_SERVER['PHP_SELF'];
        if (isset($_SESSION['suDepartmentId']) && $_SESSION['suDepartmentId'] != NULL)
        {
            $viewPeriodsFormAction .= "?suDepartmentId=" . $_SESSION['suDepartmentId'];   
        }
        if (isset($_SESSION['suDomainId']) && $_SESSION['suDomainId'] != NULL)
        { 
            $viewPeriodsFormAction .= "?suDomainId=" . $_SESSION['suDomainId'];      
        }
    ?>
    <div style="float: right; margin-right: 15px;">
    <form accept-charset="utf-8" id="viewPeriodsForm" name="viewPeriodsForm" action="<?php echo $viewPeriodsFormAction; ?>" method="POST">
    <b>View Periods:</b>
    <input type="radio" name="viewPeriods" value="current" <?php echo $viewPeriodsCurrentChecked; ?> 
        onClick="document.viewPeriodsForm.submit(); return false;" /> Current
    <input type="radio" name="viewPeriods" value="all" <?php echo $viewPeriodsAllChecked; ?>
        onClick="document.viewPeriodsForm.submit(); return false;" /> All
    </form>
    </div>
    <?php
    } else if($_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 3)
    {    
        $viewPeriodsFormAction = $_SERVER['PHP_SELF'];
        if (isset($_SESSION['suDepartmentId']) && $_SESSION['suDepartmentId'] != NULL)
        {
            $viewPeriodsFormAction .= "?suDepartmentId=" . $_SESSION['suDepartmentId'];   
        }
        if (isset($_SESSION['suDomainId']) && $_SESSION['suDomainId'] != NULL)
        { 
            $viewPeriodsFormAction .= "?suDomainId=" . $_SESSION['suDomainId'];      
        }
    ?>
    <div style="float: right; margin-right: 15px;">
    <form accept-charset="utf-8" id="viewPeriodsForm" name="viewPeriodsForm" action="<?php echo $viewPeriodsFormAction; ?>" method="POST">
    <b>View Periods:</b>
    <input type="radio" name="viewPeriods" value="current" <?php echo $viewPeriodsCurrentChecked; ?> 
        onClick="document.viewPeriodsForm.submit(); return false;" /> Current
    <input type="radio" name="viewPeriods" value="all" <?php echo $viewPeriodsAllChecked; ?>
        onClick="document.viewPeriodsForm.submit(); return false;" /> Current and Previous
    </form>
    </div>
    <?php
    } 
    ?>
</div>
<br/><br/>
	<table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr align="center" class="tblHead">
        <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 4){ ?>
		<td width="33%">Application</td>
		<? } 
        if ($_SESSION['A_usertypeid']!= 18) {
        ?>
            <td width="33%">Evaluation</td> 
		<?
        } 
        if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1
            || $_SESSION['A_usertypeid'] == 18 ){ ?>
        <td width="33%">Admission</td>
		<? } ?>
      </tr>
      <tr valign="top">
	 <!-- APPLICATION COLUMN -->
	  <?
      if($_SESSION['A_usertypeid'] == 0 
            || $_SESSION['A_usertypeid'] == 1 
            || $_SESSION['A_usertypeid'] == 4 
            || $_SESSION['A_usertypeid'] == 21)
      { 
      ?>
        <td width="33%">
          
        <?
         // PLB added showAdminByDomain test 9/15/09
         if( ($_SESSION['A_usertypeid'] == 0 && $suDepartmentId == NULL) 
            || ($_SESSION['A_usertypeid'] == 1 && $showAdminByDomain)  )
         { 
            // You are a super user or unit administrator
         ?>
        <fieldset><legend>Applicants by Domain</legend>
         <?
         // PLB changed links to admin/administerApplications.php 7/21/09 
          for($i = 0; $i< count($domains); $i++)
          {
                echo "<span class='subtitle'>".$domains[$i][1]."</span><br>"; 
              
              // PLB added new period model 9/1/09
              $domainId = $domains[$i][0];
              $periodArray = getDomainPeriods($domainId);
              $displayedPeriodCount = 0;
              if ( count($periodArray) > 0) {
                  echo '<ul>';
                  foreach ($periodArray as $periodRecord) {
                      $periodId = $periodRecord['period_id'];
                      $periodLink = formatPeriodLink('domain', $domainId, $periodId, 'admin', $viewPeriods);
                      if ($periodLink) {
                          echo '<li>' . $periodLink . '</li>';
                          $displayedPeriodCount++;
                          // Add link for SCS to print folder labels
                          if ($domainId == 1 ) {
                             echo '<li> <a href="folder_labels.php?id=' . $domainId . '&period=' . $periodId . '">Print Folders Labels</a></li>';
                          }     
                      }
                  }                                                                                                                                                    
                  echo '</ul>';                
              } 
              if ($displayedPeriodCount == 0) {
                    echo '<ul><li><i>no admission periods found for this domain</i></li></ul>';
              }
          }
         ?>
         </fieldset>
         <? 
         } // end domain section 
         ?>
          
          <fieldset><legend>Applicants by Department</legend>
          <?php
		  for($i = 0; $i< count($departments); $i++)
		  {
		      echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
		  	
              // PLB added new period model 9/1/09
              $department_id = $departments[$i][0];
              $periodArray = getDepartmentPeriods($department_id);
              $displayedPeriodCount = 0;
              if ( count($periodArray) > 0) { 
                  echo '<ul>';
                  foreach ($periodArray as $periodRecord) {
                      $periodId = $periodRecord['period_id'];
                      $periodLink = formatPeriodLink('department', $department_id, $periodId, 'admin', $viewPeriods);
                      if ($periodLink) {
                        echo '<li style="margin-top: 5px;">' . $periodLink . '</li>';
                        $displayedPeriodCount++;
                   //     if (isIniDepartment($department_id)) {
                   //          echo '<li> <a href="folder_labels.php?id=' . $department_id . '&period=' . $periodId . '">Print Folders Labels</a></li>';
                   //     }       
                      }
                  }
                  echo '</ul>';                
              }
              if ($displayedPeriodCount == 0) {
                  echo '<ul><li><i>no admission periods found for this department</i></li></ul>';
              }
		  }
		  ?>
		  </fieldset>
		 
		<? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1){ ?>
		<fieldset><legend>Email Unsubmitted Applicants</legend>
		 <?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
			echo "<a href='remindUnsubmittedApps.php?id=".$departments[$i][0]."'>Send Email</a><br>";
		  }
		  ?></fieldset>

		  <fieldset><legend>Email Unsubmitted Recommenders</legend>
		 <?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
			echo "<a href='remindUnsubmittedRecs.php?id=".$departments[$i][0]."'>Send Email</a><br>";
		  }
		  ?></fieldset>

          <fieldset><legend>Email Unpaid Applicants</legend>
         <?
          for($i = 0; $i< count($departments); $i++)
          {
            echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
            echo "<a href='remindUnpaidApps.php?id=".$departments[$i][0]."'>Send Email</a><br>";
          }
          ?></fieldset>

          <?php
          if (isMsrtDepartment($_SESSION['roleDepartmentId'])) {
          ?>
          <fieldset><legend>Email Incomplete Applicants</legend>
         <?
          for($i = 0; $i< count($departments); $i++)
          {
              echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
              echo "<a href='remindIncompleteApps.php?id=".$departments[$i][0]."'>Send Email</a><br>";
          }
          ?></fieldset>   
          <?php
          }
          ?>
          
          
		<? }//end superuser or admin ?>
		  </td>
<? } ?>

<!-- REVIEW COLUMN -->
<? if($_SESSION['A_usertypeid'] != 18) { ?>
          
        <td width="33%">
		<? 
        if ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 
            || $_SESSION['A_usertypeid'] == 10) { 
        ?>
		<fieldset>
		    <legend>Group Assignments</legend>
		    <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td>Define Groups </td>
                <td><a href="reviewGroups.php?id=1">Edit</a></td>
                <td><a href="reviewGroups.php?id=2"></a></td>
              </tr>
              <tr>
                <td>Assign Reviewers </td>
                <td><a href="reviewGroups_assign.php?r=1">Round 1</a></td>
                <td><a href="reviewGroups_assign.php?r=2">Round 2</a> </td>
                <?php
                    if (isset($numRounds)) {
                        if ($numRounds > 2) {
                            ?>
                           <td><a href="reviewGroups_assign.php?r=3">Round 3</a> </td> 
                           <?php
                        }
                        if ($numRounds > 3) {
                            ?>
                           <td><a href="reviewGroups_assign.php?r=4">Round 4</a> </td> 
                           <?php
                        }
                    }
                    ?>
              </tr>
              <tr>
                <td colspan="3"><br/>Assign Applicants:<br/><br/>
                <?php
                for($i = 0; $i < count($departments); $i++) {
                    
                    echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
                    
                    $departmentId = $departments[$i][0];
                    $periodArray = getDepartmentPeriods($departmentId);
                    $activeSpecialPeriodId = NULL;
                    $displayedPeriodCount = 0;

                    if ( count($periodArray) > 0) { 
                        echo '<ul>';
                        
                        /* 
                        * Handle active compbio, mrsd dual-period links.
                        */
                        if ($activeCompbioPeriodId && $departmentId == $compbioDepartmentId) {
                            $activeSpecialPeriodId = $activeCompbioPeriodId;    
                        }

                        if ($activeMrsdPeriodId && $departmentId == $mrsdDepartmentId) {
                            $activeSpecialPeriodId = $activeMrsdPeriodId;    
                        }
                        
                        if ($activeSpecialPeriodId) {
                            $periodHeading = formatPeriodLink('department', $departmentId, $activeSpecialPeriodId, 'review', $viewPeriods);
                            if ($departmentId == 74)
                            {
                                // CSD MS
                                $baseHref = 'applicantsGroups_assign2.php?id=' . $departmentId . '&period=' . $activeSpecialPeriodId;    
                            }
                            else
                            {
                                $baseHref = 'applicantsGroups_assign.php?id=' . $departmentId . '&period=' . $activeSpecialPeriodId;
                            }
                            if ($activeScsPeriodId) {
                                $parameterReplacement = '&period[]=' . $activeScsPeriodId . '&period[]=';
                                $baseHref = str_replace('&period=', $parameterReplacement, $baseHref);
                            }
                            echo '<li style="margin-top: 5px;">' . $periodHeading;
                            echo '<br/><a href=" ' . $baseHref . '&r=1">Round 1</a>';
                            echo ' &nbsp;&nbsp;<a href=" ' . $baseHref . '&r=2">Round 2</a>';
                            echo '</li>';
                            $displayedPeriodCount++;     
                        }

                        // Handle other department periods and closed compbio, mrsd periods 
                        foreach ($periodArray as $periodRecord) {
                            $periodId = $periodRecord['period_id'];
                            //if ( ($departmentId != $compbioDepartmentId && $departmentId != $mrsdDepartmentId)
                            //    || ($periodId != $activeSpecialPeriodId && $periodId != $activeScsPeriodId) ) 
                            //{
                                $periodHeading = formatPeriodLink('department', $departmentId, $periodId, 'review', $viewPeriods);
                                if ($periodHeading) {
                                    echo '<li style="margin-top: 5px;">' . $periodHeading;
                                    if ($departmentId == 74)
                                    {
                                        // CSD MS
                                        echo '<br/><a href="applicantsGroups_assign2.php?id=' . $departmentId . '&r=1&period=' . $periodId . '">Round 1</a>';
                                        echo ' &nbsp;&nbsp;<a href="applicantsGroups_assign2.php?id=' . $departmentId . '&r=2&period=' . $periodId . '">Round 2</a>';
                                    }
                                    else
                                    {
                                        echo '<br/><a href="applicantsGroups_assign.php?id=' . $departmentId . '&r=1&period=' . $periodId . '">Round 1</a>';
                                        echo ' &nbsp;&nbsp;<a href="applicantsGroups_assign.php?id=' . $departmentId . '&r=2&period=' . $periodId . '">Round 2</a>';    
                                        if (isset($numRounds)) {
                                            if ($numRounds > 2) {
                                                echo ' &nbsp;&nbsp;<a href="applicantsGroups_assign.php?id=' . $departmentId . '&r=3&period=' . $periodId . '">Round 3</a>';
                                            }
                                            if ($numRounds > 3) {
                                                echo ' &nbsp;&nbsp;<a href="applicantsGroups_assign.php?id=' . $departmentId . '&r=4&period=' . $periodId . '">Round 4</a>';
                                            }
                                        }
                                    }
                                    echo '</li>';
                                    $displayedPeriodCount++;     
                                }
                            //}
                        }
                        echo '</ul>';                
                    }
                    if ($displayedPeriodCount == 0) {
                        echo '<ul><li><i>no admission periods found for this department</i></li></ul>';
                    }
                }
                ?>
                </td>
              </tr>
            </table>
		    </fieldset>
		<br />
		<? 
        // PLB added reviewer comments toggle for su, LTI admin, LTI admissions chair 12/8/09.       
        /*
        if ($_SESSION['A_usertypeid'] == 0 || in_array(6, $_SESSION['A_admin_depts']) ) {
        ?>
        <fieldset>
        <legend>Reviewer Comments/Scores</legend> 
        <?php
          for($i = 0; $i< count($departments); $i++) {
              $department_id = $departments[$i][0];
              echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
              $periodArray = getDepartmentPeriods($department_id);
              $displayedPeriodCount = 0;
              if ( count($periodArray) > 0) {
                  echo '<ul>';
                  foreach ($periodArray as $periodRecord) {
                    $periodId = $periodRecord['period_id'];
                    $periodHeading = formatPeriodLink('department', $department_id, $periodId, 'review', $viewPeriods);
                    if ($periodHeading) {
                        //DebugBreak();
                        $toggleIdPart = $department_id . '_' . $periodId;
                        $commentsOnChecked = $commentsOffChecked = '';
                        if (isset($reviewCommentsOn[$department_id][$periodId])) {
                            if(!$reviewCommentsOn[$department_id][$periodId]) {
                                $commentsOffChecked = 'checked';    
                            } else {
                                $commentsOnChecked = 'checked';
                            }        
                        } else {
                            $commentsOnChecked = 'checked';    
                        }
                        ?>
                        <li style="margin-top: 5px;"><?php echo $periodHeading; ?>
                        <br/>
                        <form accept-charset="utf-8" name="commentsToggleForm_<?php echo $toggleIdPart; ?>" 
                            id="commentsToggleForm_<?php echo $toggleIdPart; ?>" 
                            action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                        <input type="hidden" name="commentsToggleDepartmentId" 
                            id="commentsToggleDepartmentId_<?php echo $toggleIdPart; ?>"
                            value="<?php echo $department_id; ?>" />
                        <input type="hidden" name="commentsTogglePeriodId" 
                            id="commentsTogglePeriodId_<?php echo $toggleIdPart; ?>"
                            value="<?php echo $periodId; ?>" />
                        <input type="radio" value="1" name="commentsToggle" 
                            id="commentsToggleOn_<?php echo $toggleIdPart; ?>" 
                            class="commentsToggle" <?php echo $commentsOnChecked; ?> 
                            onClick="document.commentsToggleForm_<?php echo $toggleIdPart; ?>.submit(); return false;" /> 
                        Reviewer can view all
                        <br/>
                        <input type="radio" value="0"  name="commentsToggle" 
                            id="commentsToggleOff_<?php echo $toggleIdPart; ?>" 
                            class="commentsToggle" <?php echo $commentsOffChecked; ?> 
                            onClick="document.commentsToggleForm_<?php echo $toggleIdPart; ?>.submit(); return false;" /> 
                        Reviewer can only view her own
                        </form>
                        </li>
                        <?php
                        $displayedPeriodCount++;     
                    }
                  }
                  echo '</ul>';
              }
              if ($displayedPeriodCount == 0) {
                echo '<ul><li><i>no admission periods found for this department</i></li></ul>';
              }
          }
        ?>
        </fieldset>
        <br/>
        <?php
            }
        */
        }
        
        if( $_SESSION['A_usertypeid'] == 3 && isset($departments[0][0]) 
            && ($departments[0][0] == 1 || $departments[0][0] == 74) ) {
        ?>
        <fieldset>
            <legend>Ranking Groups</legend>
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td>Create Ranking Group</td>
                <td><a href="reviewGroups_edit.php">Create</a></td>
                <td></td>
              </tr>
              <tr>
                <td>Assign/Change Additional Reviewers</td>
                <td><a href="reviewGroups_assign.php?r=2">Round 2</a></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3"><br/>Assign Applicants:<br/><br/>
                <?php
                for($i = 0; $i < count($departments); $i++) {
                    
                    echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
                    
                    $departmentId = $departments[$i][0];
                    $periodArray = getDepartmentPeriods($departmentId);
                    $activeSpecialPeriodId = NULL;
                    $displayedPeriodCount = 0;

                    if ( count($periodArray) > 0) { 
                        echo '<ul>';
                        
                        /* 
                        * Handle active compbio, mrsd dual-period links.
                        */
                        if ($activeCompbioPeriodId && $departmentId == $compbioDepartmentId) {
                            $activeSpecialPeriodId = $activeCompbioPeriodId;    
                        }

                        if ($activeMrsdPeriodId && $departmentId == $mrsdDepartmentId) {
                            $activeSpecialPeriodId = $activeMrsdPeriodId;    
                        }
                        
                        if ($activeSpecialPeriodId) {
                            $periodHeading = formatPeriodLink('department', $departmentId, $activeSpecialPeriodId, 'review', $viewPeriods);
                            if ($departmentId == 74)
                            {
                                // CSD MS
                                $baseHref = 'applicantsGroups_assign2.php?id=' . $departmentId . '&period=' . $activeSpecialPeriodId;    
                            }
                            else
                            {
                                $baseHref = 'applicantsGroups_assign.php?id=' . $departmentId . '&period=' . $activeSpecialPeriodId;    
                            }
                            if ($activeScsPeriodId) {
                                $parameterReplacement = '&period[]=' . $activeScsPeriodId . '&period[]=';
                                $baseHref = str_replace('&period=', $parameterReplacement, $baseHref);
                            }
                            echo '<li style="margin-top: 5px;">' . $periodHeading;
                            echo '<br/><a href=" ' . $baseHref . '&r=1">Round 1</a>';
                            echo ' &nbsp;&nbsp;<a href=" ' . $baseHref . '&r=2">Round 2</a>';
                            echo '</li>';
                            $displayedPeriodCount++;     
                        }

                        // Handle other department periods and closed compbio, mrsd periods 
                        foreach ($periodArray as $periodRecord) {
                            $periodId = $periodRecord['period_id'];
                            if ( ($departmentId != $compbioDepartmentId && $departmentId != $mrsdDepartmentId)
                                || ($periodId != $activeSpecialPeriodId && $periodId != $activeScsPeriodId) ) 
                            {
                                $periodHeading = formatPeriodLink('department', $departmentId, $periodId, 'review', $viewPeriods);
                                if ($periodHeading) {
                                    echo '<li style="margin-top: 5px;">' . $periodHeading;
                                    if ($departmentId == 74)
                                    {
                                        // CSD MS
                                        echo '<br/><a href="applicantsGroups_assign2.php?id=' . $departmentId . '&r=2&period=' . $periodId . '">Round 2</a>';    
                                    }
                                    else
                                    {
                                        echo '<br/><a href="applicantsGroups_assign.php?id=' . $departmentId . '&r=2&period=' . $periodId . '">Round 2</a>';    
                                    }
                                    
                                    echo '</li>';
                                    $displayedPeriodCount++;     
                                }
                            }
                        }
                        echo '</ul>';                
                    }
                    if ($displayedPeriodCount == 0) {
                        echo '<ul><li><i>no admission periods found for this department</i></li></ul>';
                    }
                }
                ?>
                </td>
              </tr>
            </table>
            </fieldset>
        <br />
        <?
        } 

        if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 2 
            || $_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 10 || $_SESSION['A_usertypeid'] == 11
            || $_SESSION['A_usertypeid'] == 12 || $_SESSION['A_usertypeid'] == 19 || $_SESSION['A_usertypeid'] == 20){?>
		<fieldset><legend>Reviews</legend>
		<?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	
            echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";  
              
            $departmentId = $departments[$i][0];
            $activeSpecialPeriodId = NULL;
            
            if ( ($departmentId == 1 || $departmentId == 74)
                && $_SESSION['A_usertypeid'] != 11) {
            ?>
                
                <ul>
                <li>
                <b>Area-specific Ranking Advice</b>
                <br/><a href="documents/Areacriteria.pdf" target="_blank">Area Criteria</a>
                </li>
                <li>
                <b>Information on Universities</b>
                <br/><a href="documents/Foreignuniversitycommentarybycountry.pdf" target="_blank">Foreign Universities</a>
                <br/><a href="documents/AverageGPAByUniversity.xlsx" target="_blank">Average GPA By University</a>
                </li>
                <li>
                <b>Language Evaluation</b>
                <br/><a href="documents/Guidelines_TOEFL_2012.pdf" target="_blank">TOEFL Guidelines 2012</a>
                <br/><a href="documents/IELTS.pdf" target="_blank">IELTS Guidelines</a>
                <br/><a href="documents/TOEFLShortsummary.pdf" target="_blank">TOEFL Summary </a>
                </li>
                <li>
                <b>Understanding the GRE</b>
                <br/><a href="documents/ShortsummaryofevaluatingGREs.pdf" target="_blank">Evaluating GRE Scores</a>
                </li>
                
                </ul>
            <?php
            }
            elseif (($departmentId == 18 || $departmentId == 19 ||
                    $departmentId == 47 || $departmentId == 48 || $departmentId == 52 || $departmentId == 85)
                    && $_SESSION['A_usertypeid'] != 11) {
                   ?>
                <ul><li>
                <b>Instructions</b>
                <br/><a href="documents/2014_MSE_AdmissionsOnlineReviewInstr.pdf" target="_blank">Admission Online Review Instructions</a> 
                </li>
                </ul>
            <?php
            } elseif ($departmentId == 82 && $_SESSION['A_usertypeid'] != 11) {
                   ?>
                <ul><li>
                <b>Instructions</b>
                <br/><a href="documents/2016MITSOnlineApplicReviewInstr.pdf" target="_blank">Admission Online Review Instructions</a> 
                </li>
                </ul>
            <?php
            }
            $periodArray = getDepartmentPeriods($departmentId);
            if ($viewPeriods == 'all' && ($_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 3 )) {
                $periodArray = truncatePeriods ($periodArray);
            }
            $displayedPeriodCount = 0;
            if ( count($periodArray) > 0) { 
                echo '<ul>';

                // Handle other department periods and closed compbio periods
                foreach ($periodArray as $periodRecord) {
                    $periodId = $periodRecord['period_id'];
                    $periodHeading = formatPeriodLink('department', $departmentId, $periodId, 'review', $viewPeriods);
                    if ($periodHeading) {
                    
                        // turn on output buffer
                        ob_start();

                        echo '<li style="margin-top: 5px;">' . $periodHeading; 
                        echo "<br/>";                       

            if($_SESSION['A_usertypeid'] != 11)
			{
		
				if( ($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 0) ||
				    $_SESSION['A_usertypeid'] != 4 
				    && $_SESSION['A_usertypeid'] != 3
                    && $_SESSION['A_usertypeid'] != 20 )//faculty cannot access
				{

                    if ( $departments[$i][0] == 47)
                    {
                        echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=1&period=" . $periodId . "'>Evaluation</a> ";
                    } 
                    elseif ( $_SESSION['A_usertypeid'] == 19)
                    {
                        echo "<a href='../admin/administerPlaceout.php?unit=department&unitId=49&programId=100000&period=" . $periodId . "'>Placeout</a> ";
                        echo "<br />";
                        echo "<a href='../admin/administerPlaceout.php?unit=department&unit=47&programId=100001&period=" . $periodId . "'>Placeout - Portugal</a> ";
                    } else if (isSocialDecisionSciencesDepartment($departments[$i][0])) {
                            echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=1&period=" . $periodId . "'>Everyone</a> ";
                } 
                    else {
                        echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=1&period=" . $periodId . "'>Round 1</a> ";
					}
				}
                // DAS Allow CSD faculty to see applicants in round 1
                if( ($_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20)   // user type is faculty or area chair
                    && ($departments[$i][0] == 1 || $departments[$i][0] == 74 || $departments[$i][0] == 5)) // department is CSD or HCII
                {
                    echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=1&period=" . $periodId . "'>Round 1</a> ";
                }
				if ($departments[$i][3] == 1 
                //    && !($departments[$i][0] == 2 && ($_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20)) 
                    ) // if round 2 enabled for department (except ML faculty)
				{
                    
                    if (isSocialDecisionSciencesDepartment($departments[$i][0])) {
                         echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=2&period=" . $periodId . "'>Open House</a> "; 
                    } else {
                             echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=2&period=" . $periodId . "'>Round 2</a> ";
				             }
                }
                // CSD Committee Histogram
                if ($departments[$i][0] == 1 && $_SESSION['A_usertypeid'] == 2)
                {   
                    echo "<a href='../review/histogram.php?periodId=" . $periodId . "&departmentId=" . 
                        $departments[$i][0] . "&reviewerId=" . $_SESSION['A_userid'] . "'>Histogram</a> ";    
                }

                // Link to round 2 area rank for CSD
                if( ($_SESSION['A_usertypeid'] == 3)   // user type is faculty area chair
                    && ($departments[$i][0] == 1 || $departments[$i][0] == 74)) // department is CSD
                {
                    echo "<a href='../review/rankApplications.php?departmentId=".$departments[$i][0]."&round=2&period=" . $periodId . "'>Rank Round 2</a> ";
                }
                elseif ( ($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 10)   // admin, committee
                    && ($departments[$i][0] == 1 || $departments[$i][0] == 74) ) // department is CSD
                {
                    echo "<a href='../review/rankApplications.php?departmentId=".$departments[$i][0]."&round=2&period=" . $periodId . "'>Area Coordinator Rankings</a> ";    
                }
                
                if ($departmentId == 50) {
                    if( ($_SESSION['A_usertypeid'] == 1  || $_SESSION['A_usertypeid'] == 0)
                        && $departments[$i][5] == 1 )  //if round 3 enabled for admins and SU only 
                        {
                        echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=3&period=" . $periodId . "'>Round 3</a> ";
                    }
                } else {
                    if( ($_SESSION['A_usertypeid'] == 1  || $_SESSION['A_usertypeid'] == 2 
                            || $_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20 || $_SESSION['A_usertypeid'] == 10) 
                        && $departments[$i][5] == 1 ) // if round 3 enabled for department (faculty)
                    {
                        echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=3&period=" . $periodId . "'>Round 3</a> ";
                    }
                }
                
                if( ($_SESSION['A_usertypeid'] == 1  || $_SESSION['A_usertypeid'] == 2 
                        || $_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20 || $_SESSION['A_usertypeid'] == 10) 
                    && $departments[$i][6] == 1 ) // if round 4 enabled for department (faculty)
                {
                    echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=4&period=" . $periodId . "'>Round 4</a> ";
                }

				if(
				($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 0) ||
				    $departments[$i][4] == 1 )//if final enabled for department
				{
                    // DAS do not show for CSD Faculty or non-Admins for SDS
                    if(
                    (($_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20)   // user type is faculty or area chair
                    && ($departments[$i][0] == 1 || $departments[$i][0] == 74))   // department is CSD
                    || (isSocialDecisionSciencesDepartment($departments[$i][0]) 
                        && !isSocialDecisionSciencesAdministrator($departments[$i][0], $_SESSION['A_usertypeid'])))  { // SDS and not admin
                    } 
                    else 
                    {  
                        if (isRissDepartment($departments[$i][0])) {
                            echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=4&d=1&period=" . $periodId . "'>Final</a> "; 
                        }
                        else if ($departments[$i][5] == 1 && $periodId > 50) {
                            // ML, RI-Scholars need final to point to round 3 starting 2010 for 2011.
                            echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=3&d=1&period=" . $periodId . "'>Final</a> ";    
                        } else {
                            echo "<a href='../review/reviewApplications.php?id=".$departments[$i][0]."&s=1&r=2&d=1&period=" . $periodId . "'>Final</a> ";    
                        }
                        
					}
                    // DAS do not show for CSD Faculty
                    // PLB changed to hide for all faculty.
                    if(($_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20)
                        || (isSocialDecisionSciencesDepartment($departments[$i][0]) 
                            &&!isSocialDecisionSciencesAdministrator($departments[$i][0], $_SESSION['A_usertypeid'])))
                    { 
                    } 
                    else 
                    {
                        echo " <a href='../review/normalizations.php?id=".$departments[$i][0]."&period=" . $periodId . "'>Normalizations</a> ";
                    }
				}
			}
			if( ($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 0) 
			    || $departments[$i][4] == 1 )//if enabled for department
			{
               // DAS do not show for CSD Faculty
               if (( $_SESSION['A_usertypeid'] == 11 
                    || (($_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 20) 
                    &&  $departments[$i][0] == 74) 
                    )
                    || (isSocialDecisionSciencesDepartment($departments[$i][0]) 
                        && !isSocialDecisionSciencesAdministrator($departments[$i][0], $_SESSION['A_usertypeid']))
                    || ($_SESSION['A_usertypeid'] == 3 && isHistoryDepartment($deptcheck)))
               { 
                    echo "<a href='admitted_ext.php?id=".$departments[$i][0]."&period=" . $periodId . "'>Ext. List of Invited Applicants</a> ";
               } else {
				    echo "<a href='admitted_ext.php?id=".$departments[$i][0]."&period=" . $periodId . "'>Ext. List of Invited Applicants</a> ";
                    echo "<a href='decision_breakdown.php?id=".$departments[$i][0]."&period=" . $periodId . "'>Admission Committee Decision Breakdown</a> ";
                }
			}

            // PLB added promote/demote 2/6/09
            if( $_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 
                || $_SESSION['A_usertypeid'] == 10 ) {
                echo ' <a href="../review/promoteByDepartment.php?id=' . $departments[$i][0] . '&s=1&period=' . $periodId . '">';
                echo 'Promote/Demote</a>';  
            }
                        echo '</li>';
                        
                        // get buffer output and close
                        $periodEntry = ob_get_contents(); 
                        ob_end_clean();
                    
                        /* 
                        * Handle active compbio, mrsd dual-period links.
                        */
                        if ($activeCompbioPeriodId && $departmentId == $compbioDepartmentId) {
                            $activeSpecialPeriodId = $activeCompbioPeriodId;    
                        }

                        if ($activeMrsdPeriodId && $departmentId == $mrsdDepartmentId) {
                            $activeSpecialPeriodId = $activeMrsdPeriodId;    
                        }
                        
                        if ($periodId == $activeSpecialPeriodId) {
                            if ($activeScsPeriodId) {
                                $parameterReplacement = '&period[]=' . $activeScsPeriodId . '&period[]=';
                                $periodEntry = str_replace('&period=', $parameterReplacement, $periodEntry);
                            }    
                        } elseif ($activeSpecialPeriodId && $periodId == $activeScsPeriodId) {
                            $periodEntry = '';
                        }
                    
                        if ($periodEntry) {
                            echo $periodEntry;
                            $displayedPeriodCount++;    
                        }
                        
                    } // end if($periodHeading)
                
                } // end period for each
                echo '</ul>'; 
		  
            // close period count if
            } 
            
            if ($displayedPeriodCount == 0)
            {
                echo '<ul><li><i>no admission periods found for this department</i></li></ul>';
            }  
          }
		  ?>
		</fieldset>
		<? }//end if ?>
        <?php
        //   RI Summer Scholars only for email applicants by round 
        
        if ($_SESSION['roleDepartmentId'] == 50 && ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1)) {
        ?> 
        <fieldset><legend>Email Applicants by Round</legend>
         <?
          for($i = 0; $i< count($departments); $i++)
          {
            echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
            echo "<a href='emailApplicantGroups.php?id=".$departments[$i][0]."&r=2&period=". $requestPeriodId."'>Send Email to Round 2 Applicants</a><br>";
            echo "<a href='emailApplicantGroups.php?id=".$departments[$i][0]."&r=3&period=". $requestPeriodId."'>Send Email to Round 3 Applicants</a><br>";
            echo "<a href='emailApplicantGroups.php?id=".$departments[$i][0]."&r=4&period=". $requestPeriodId."'>Send Email to Round 4 Applicants</a><br>";
          }
          }?></fieldset>
          
        
        <?php
        /*
        *PLB added box for CSV export for super users and administrators       
        */
        if( $_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 
            || $_SESSION['A_usertypeid'] == 2 
            || ($_SESSION['A_usertypeid'] == 3  && !isHistoryDepartment($deptcheck)) 
            || $_SESSION['A_usertypeid'] == 20
            || $_SESSION['A_usertypeid'] == 10)
        { ?>
            <br>
            <fieldset><legend>Export CSV</legend>
            <?
            // PLB changed 02/09/09 
            for($i = 0; $i < count($departments); $i++) {
                
                echo "<span class='subtitle'>".$departments[$i][1]."</span><br>"; 
                
                $departmentId = $departments[$i][0];
                $activeSpecialPeriodId = NULL;
                
                // PLB added new period model 11/12/09
                $periodArray = getDepartmentPeriods($departmentId);
                if ($viewPeriods == 'all' && ($_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 3 )) {
                    $periodArray = truncatePeriods ($periodArray);
                }
                $displayedPeriodCount = 0;
                if ( count($periodArray) > 0) { 
                    echo '<ul>';
                    
                    /* 
                    * Handle active compbio, mrsd dual-period links.
                    */
                    if ($activeCompbioPeriodId && $departmentId == $compbioDepartmentId) {
                            $activeSpecialPeriodId = $activeCompbioPeriodId;    
                        }

                    if ($activeMrsdPeriodId && $departmentId == $mrsdDepartmentId) {
                        $activeSpecialPeriodId = $activeMrsdPeriodId;    
                    }
                    
                    if ($activeSpecialPeriodId) {
                        $periodHeading = formatPeriodLink('department', $departmentId, $activeSpecialPeriodId, 'review', $viewPeriods);
                        $base_dump_href = 'getDbDump.php?department_id=' . $departmentId . '&period=' . $activeSpecialPeriodId;
                        if ($activeScsPeriodId) {
                            $parameterReplacement = '&period[]=' . $activeScsPeriodId . '&period[]=';
                            $base_dump_href = str_replace('&period=', $parameterReplacement, $base_dump_href);
                        }
                        $application_dump_href =  $base_dump_href . '&dump=applications';
                        $review_dump_href = $base_dump_href . '&dump=reviews'; 
                        echo '<li style="margin-top: 5px;">' . $periodHeading;
                        echo '<br/><a href="' . $application_dump_href . '">Export Applications</a>';
                        if (isSocialDecisionSciencesDepartment($departments[$i][0])) {
                            if (isSocialDecisionSciencesAdministrator($departments[$i][0], $_SESSION['A_usertypeid'])) {
                                echo ' &nbsp;&nbsp;<a href="' . $review_dump_href . '">Export Reviews</a>';
                            }     
                        }
                        else if ($_SESSION['A_usertypeid'] != 3){
                            echo ' &nbsp;&nbsp;<a href="' . $review_dump_href . '">Export Reviews</a>';    
                        }
                        echo '</li>';
                        $displayedPeriodCount++;     
                    }

                    // Handle other department periods and closed compbio periods 
                    foreach ($periodArray as $periodRecord) {
                        $periodId = $periodRecord['period_id'];
                        if ( ($departmentId != $compbioDepartmentId && $departmentId != $mrsdDepartmentId)
                            || ($periodId != $activeSpecialPeriodId && $periodId != $activeScsPeriodId)
                            // DAS added this case for MRSD without any special periods
                            || ($periodId == $activeScsPeriodId && $departmentId == $mrsdDepartmentId)
                            || ($periodId == $activeScsPeriodId && $departmentId == $compbioDepartmentId))
                        {
                            $periodHeading = formatPeriodLink('department', $departmentId, $periodId, 'review', $viewPeriods);
                            if ($periodHeading) {
                                $base_dump_href = 'getDbDump.php?department_id=' . $departmentId . '&period=' . $periodId;
                                $application_dump_href =  $base_dump_href . '&dump=applications';
                                $review_dump_href = $base_dump_href . '&dump=reviews';                              
                                echo '<li style="margin-top: 5px;">' . $periodHeading;
                                echo '<br/><a href="' . $application_dump_href . '">Export Applications</a>';
                                if (isSocialDecisionSciencesDepartment($departments[$i][0])) {
                                    if (isSocialDecisionSciencesAdministrator($departments[$i][0], $_SESSION['A_usertypeid'])) {
                                        echo ' &nbsp;&nbsp;<a href="' . $review_dump_href . '">Export Reviews</a>';
                                        }     
                                }
                                else if ($_SESSION['A_usertypeid'] != 3) {
                                    echo ' &nbsp;&nbsp;<a href="' . $review_dump_href . '">Export Reviews</a>';    
                                }
                                echo '</li>';
                                $displayedPeriodCount++;     
                            }
                        }
                    }
                    echo '</ul>';                
                }
                if ($displayedPeriodCount == 0) {
                    echo '<ul><li><i>no admission periods found for this department</i></li></ul>';
                }
            }
            echo "</fieldset>";
        }
        ?>
        </td> 
        <?
        } // End if not a statistics read-only role.
        ?>

		<!-- ADMISSIONS COLUMN -->
		 <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1
            || $_SESSION['A_usertypeid'] == 18 ){
            
            /*
            * TEMPORARY KLUGE: until compbio, mrsd dual-periods are figured 
            * out for this section, set the active period ids to null so all periods 
            * will be displayed.
            */
            $activeSpecialPeriodId = NULL;
            $activeScsPeriodId = NULL; 
         ?>
         <td width="33%">
         <?       
         if ($_SESSION['A_usertypeid'] != 18) {       
         ?>
        <fieldset><legend>Invited Applicants</legend>

		<? 
        for($i = 0; $i< count($departments); $i++) { 
            $department_id = $departments[$i][0];
        ?>   
            <span class="subtitle"><?=$departments[$i][1]?></span>
            <ul> 
         <?php   
            // PLB added period support 1/7/10
            $periodArray = getDepartmentPeriods($department_id);
            $displayedPeriodCount = 0;
            if ( count($periodArray) > 0) { 
                foreach ($periodArray as $periodRecord) {
                    $periodId = $periodRecord['period_id'];
                    if ($periodId != $activeSpecialPeriodId && $periodId != $activeScsPeriodId) {
                        $periodHeading = formatPeriodLink('department', $department_id, $periodId, 'review', $viewPeriods);
                        if ($periodHeading) {                          
                            ?>
                            <li style="margin-top: 5px;"><?php echo $periodHeading; ?>
                            <br/>
                            <a href="admitted.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">List of Invited Applicants</a>  
                            <a href="admitted_ext.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">Ext. List</a><br />
                            <a href="admitted_text.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">CSV of Invited Applicants</a><br />
                            <?php
                            // PLB added interface to record student decisions 3/30/09
                            ?>
                            <a href="studentDecisionByDepartment.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">Invited Applicant Decisions</a><br />
                            
                            <? if ($department_id  == 1 || $department_id  == 74) { ?>           
                            <a href="admitted_door.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">Door List of Invited Applicants</a><br />
                                               <? } ?>
                            <a href="shippinglabels-admitted.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">Print Admitted Labels</a><br />
                            
                            <?php
                            if (isIniDepartment($department_id) || isDesignDepartment($department_id) 
                                || isDesignPhdDepartment($department_id) || isDesignDdesDepartment($department_id) 
                                || isMseMsitEbizDepartment($department_id) || isMseMsitDepartment($department_id)
                                || isMsaii($department_id) || isPrivacy($department_id))
                            {
                                if (!isMseMsitEbizDepartment($department_id) 
                                && !isDesignPhdDepartment($department_id) && !isDesignDdesDepartment($department_id) 
                                && !isMseMsitDepartment($department_id) && !isPrivacy($department_id) && !isMsaii($department_id))
                                {
                                ?>
                                <a href="admissionLetter.php?unit=department&unitId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>">Generate Letter PDF</a><br />    
                                <?php
                                }
                                ?>
                                <a href="administerRegistrations.php?unit=department&unitId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>">Administer Registrations</a><br /> 
                            <?php  
                            }
                            ?>
                            
                            
                            <?php
                               $sql = "select id,name from content where
            (contenttype_id=2)
            and (department_id=".intval($departments[$i][0]) . ") order by name";
            $result = mysql_query($sql) or die(mysql_error() . $sql);

            while($row = mysql_fetch_array( $result ))
            {
                ?>
                    <a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>&period=<?php echo $periodId; ?>" target="_blank">Preview</a> <?=$row['name']?><br>
                <?
            }
                            $displayedPeriodCount++;     
                        }
                    }
                }    
            
                echo '</li>';
            }
            if ($displayedPeriodCount == 0) {
                    echo '<li><i>no admission periods found for this department</i></li>';
            }
         
            echo '</ul>';
        } ?>
        </fieldset>
        <br />

<fieldset><legend>Rejected Applicants</legend>
<? 
for($i = 0; $i< count($departments); $i++) {
    $department_id = $departments[$i][0];
?>
<span class="subtitle"><?=$departments[$i][1]?></span>
    <ul>
        <?php
            if (($department_id == 74 || $department_id == 6) && $_SESSION['A_usertypeid'] == 1) {
                $periodId = $periodArray[0][0];
        ?>
         <li style="margin-top: 5px;">
            <a href="handle_rejects.php?departmentId=<? echo $departmentId; ?>">Grant Access for Other Programs</a><br /> 
            <?php 
            $sql = "select id,name from content where
                        (contenttype_id=6) and (department_id=".$department_id . ") order by name";
                        $result = mysql_query($sql) or die(mysql_error() . $sql);
                        while($row = mysql_fetch_array( $result ))
                        {
                            ?>
                            <a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>&departmentId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>" target="_blank">Preview</a> <?=$row['name']?><br/>
                            <?
                        }
            }?>               
         <?php 
            // PLB added period support 1/7/10
            $periodArray = getDepartmentPeriods($department_id);
            $displayedPeriodCount = 0;
            if ( count($periodArray) > 0) { 
                foreach ($periodArray as $periodRecord) {
                    $periodId = $periodRecord['period_id'];
                    if ($periodId != $activeSpecialPeriodId && $periodId != $activeScsPeriodId) {
                        $periodHeading = formatPeriodLink('department', $department_id, $periodId, 'review', $viewPeriods);
                        if ($periodHeading) {                          
                            ?>
                            <li style="margin-top: 5px;"><?php echo $periodHeading; ?>
                            <br/>
                            <a href="rejected.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">List of Rejected Applicants</a><br /> 
                            
                            <?php
                        
                        // don't show rejection letter link for non-csd scs departments
                        /*
                        if ($department_id  != 2
                            && $department_id  != 3
                            && $department_id  != 5
                            && $department_id  != 6
                            && $department_id  != 8
                            && $department_id  != 25
                            && $department_id  != 26 
                            ) 
                        {
                        */
                        
                        $sql = "select id,name from content where
                        (contenttype_id=3) and (department_id=".$departments[$i][0] . ") order by name";
                        $result = mysql_query($sql) or die(mysql_error() . $sql);
                        while($row = mysql_fetch_array( $result ))
                        {
                            ?>
                            <a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>&departmentId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>" target="_blank">Preview</a> <?=$row['name']?><br/>
                            <?
                        }
                        //}
                        
                        //if ($department_id  == 1) {
                        if ($department_id  == 1
                            || $department_id  == 74
                            || $department_id  == 2
                            || $department_id  == 3
                            || $department_id  == 5
                            || $department_id  == 6
                            || $department_id  == 25
                            || $department_id  == 26 
                            ) 
                        {
                            $sql = "select id,name from content where
                            (name='Happy Ph.D. Rejection Letter') and (domain_id=1) order by name";

                            $result = mysql_query($sql) or die(mysql_error() . $sql);
                            while($row = mysql_fetch_array( $result ))
                            {
                                ?>
                                <a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>&departmentId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>" target="_blank">Preview</a> <?=$row['name']?><br/>
                                <?
                            }
                            $sql = "select id,name from content where
                            (name='Happy M.S. Rejection Letter') and (domain_id=1) order by name";

                            $result = mysql_query($sql) or die(mysql_error() . $sql);
                            while($row = mysql_fetch_array( $result ))
                            {
                                ?>
                                <a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>&departmentId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>" target="_blank">Preview</a> <?=$row['name']?><br/>
                                <?
                            }
                        }
                            $displayedPeriodCount++;     
                        }
                    }
                }  
            
                echo '</li>';
            }
            if ($displayedPeriodCount == 0) {
                    echo '<li><i>no admission periods found for this department</i></li>';
            }
            
    echo '</ul>'; 
 } 
 ?>

</fieldset>
<br />

<?php
$showWaitlisted = FALSE;
foreach ($departments as $waitlistDepartment) {
    if ( ($waitlistDepartment[0] == 57) 
        || ($waitlistDepartment[0] == 58) 
        || ($waitlistDepartment[0] == 5)
        || ($waitlistDepartment[0] == 50)
        || ($waitlistDepartment[0] == 74)
        || ($waitlistDepartment[0] == 49)
        || ($waitlistDepartment[0] == 8) 
        || isIniDepartment($waitlistDepartment[0])
        || isDesignDepartment($waitlistDepartment[0])
        || isDesignPhdDepartment($waitlistDepartment[0])
        || isDesignDdesDepartment($waitlistDepartment[0])
        || isMseMsitDepartment($waitlistDepartment[0])) 
    {
        $showWaitlisted = TRUE;
        continue;
    }    
}
if ($showWaitlisted) {    
?>
<fieldset><legend>Waitlisted Applicants</legend> 
<? 
for($i = 0; $i< count($departments); $i++) {
    $department_id = $departments[$i][0];
?>
<span class="subtitle"><?=$departments[$i][1]?></span>
    <ul>
    <?php  
    $periodArray = getDepartmentPeriods($department_id);
    $displayedPeriodCount = 0;
    if ( count($periodArray) > 0) { 
        foreach ($periodArray as $periodRecord) {
            $periodId = $periodRecord['period_id'];
            if ($periodId != $activeSpecialPeriodId && $periodId != $activeScsPeriodId) {
                $periodHeading = formatPeriodLink('department', $department_id, $periodId, 'review', $viewPeriods);
                if ($periodHeading) {                          
                    ?>
                    <li style="margin-top: 5px;"><?php echo $periodHeading; ?>
                    <br/>
                    <a href="waitlisted.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">List 
                        of Waitlisted Applicants</a>
                    <br /> 
                    <?php
                    foreach (getWaitlistedContent($department_id) as $row) {
                    ?>
                    <a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>&departmentId=<?php echo $department_id; ?>&period=<?php echo $periodId; ?>" target="_blank">Preview</a> <?=$row['name']?>
                    <br>
                    <?php    
                    }
                    $displayedPeriodCount++;    
                }
            }
        }
        echo '</li>';
    }
    if ($displayedPeriodCount == 0) {
        echo '<li><i>no admission periods found for this department</i></li>';
    }
    echo '</ul>'; 
} ?>
</fieldset>
<br />
<?php
}

         } // End if not statistics read-only role.  
?>

<fieldset><legend>Reports / Statistics</legend>

<?
// Get the domain_unit(s) Q&D for now.
$domainUnitQuery = "SELECT DISTINCT unit.unit_id, unit.unit_name 
    FROM domain_unit INNER JOIN unit ON domain_unit.unit_id = unit.unit_id";
if ($suDomainId != NULL)
{
    $domainUnitQuery .= " WHERE domain_id = " . $suDomainId;        
}
elseif ($suDepartmentId != NULL)
{
    $domainUnitQuery .= " INNER JOIN lu_domain_department 
                            ON lu_domain_department.domain_id = domain_unit.domain_id
                            AND lu_domain_department.department_id = " . $suDepartmentId;
}
else
{
    $domainCount = count($_SESSION['A_admin_domains']);
    for ($i = 0; $i < $domainCount; $i++) 
    {
        if ($_SESSION['A_admin_domains'][$i] == NULL) {
            continue;
        }
        if($i == 0) {
            $domainUnitQuery .= " WHERE ";
        } else {
            if ($i == 1 && $_SESSION['A_admin_domains'][0] == NULL) { 
                $domainUnitQuery .= " WHERE ";    
            } else {
                $domainUnitQuery .= " OR ";
            }
        }
        $domainUnitQuery .= " domain_id = ".$_SESSION['A_admin_domains'][$i];
    }
}    
$domainUnitQuery .= " ORDER BY unit_id";
$domainUnitResult = mysql_query($domainUnitQuery) or die(mysql_error());
while ($row = mysql_fetch_array($domainUnitResult) ) {
    $unitId = $row['unit_id'];
    $unitName = $row['unit_name'];
    $unit = new Unit($unitId);
    $unitPeriods = $unit->getPeriods();
    $displayedPeriodCount = 0;
    ?>
    <span class="subtitle"><?php echo $unitName; ?></span>
    <ul>
    <?php
    foreach ($unitPeriods as $period) {
        $periodId = $period->getId();
        $periodHeading = formatPeriodLink('department', $unitId, $periodId, 'review', $viewPeriods);
        if ($periodHeading) {                          
            ?>
            <li style="margin-top: 5px;"><?php echo $periodHeading; ?>
            <br/>
            <a href="statistics.php?periodId=<?php echo $periodId; ?>&unitId=<?php echo $unitId; ?>">Statistics</a><br /> 
            <br/>
            </li>
            <?php
            $displayedPeriodCount++;     
        }
    }
    ?>
    </ul>
    <?php    
}

for($i = 0; $i< count($departments); $i++) {
    $department_id = $departments[$i][0];
?>
<span class="subtitle"><?=$departments[$i][1]?></span>
    <ul>
         <?php   
            // PLB added period support 1/7/10
            $periodArray = getDepartmentPeriods($department_id);
            $activeCompbioPeriodId = NULL;
            $activeScsPeriodId = NULL;
            $displayedPeriodCount = 0;
            if ( count($periodArray) > 0) { 
                foreach ($periodArray as $periodRecord) {
                    $periodId = $periodRecord['period_id'];
                    if ($periodId != $activeCompbioPeriodId && $periodId != $activeScsPeriodId) {
                        $periodHeading = formatPeriodLink('department', $department_id, $periodId, 'review', $viewPeriods);
                        if ($periodHeading) {                          
                            ?>
                            <li style="margin-top: 5px;"><?php echo $periodHeading; ?>
                            <br/>
                            <a href="decision_breakdown.php?id=<?=$departments[$i][0]?>&period=<?php echo $periodId; ?>">Admission Committee Decision Breakdown</a><br /> 
                            <br/>
                            </li>
                            <?php
                            $displayedPeriodCount++;     
                        }
                    }
                } 
            }
            if ($displayedPeriodCount == 0) {
                    echo '<li><i>no admission periods found for this department</i></li>';
            }
         ?>
</ul> 
<? } ?>

<!--
Admin Committee Reviews (Final)<br />
Committee Decisions
-->
</fieldset>
<br/>

<!--
<fieldset><legend>Statistics</legend>
Applicants Stats<br />
Applicants Stats (Accepted)<br />
Area of Interest Matrix<br />
Round 2 Normalized Matrix
</fieldset>
-->

</td>
<? } ?>
<!-- END ADMISSIONS COLUMN -->
      </tr>
    </table>

</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

// function to get application period info by department
// PLB 02/06/09
function getLatestApplicationPeriods($department_id, $max_periods=2) {

    // get the application period data from the db
    $application_periods_query = "SELECT application_periods.*,";
    $application_periods_query .= " DATE_FORMAT(start_date, '%c/%e/%Y') AS formatted_start_date,";
    $application_periods_query .= " DATE_FORMAT(end_date, '%c/%e/%Y') AS formatted_end_date";
    $application_periods_query .= " FROM application_periods";
    $application_periods_query .= " WHERE department_id = " . $department_id;
    $application_periods_query .= " ORDER BY end_date DESC, start_date DESC";
    $application_periods_result = mysql_query($application_periods_query) or die(mysql_error());
    
    //echo $application_periods_query;
    
    // initialize an array to hold the data
    $period_array = array();
    
    // fill the array with the max number of rows
    $period_count = 0;
    while( $period_row = mysql_fetch_array($application_periods_result) ) {
    
        if ($period_count < $max_periods) {
    
            $period_array[] = $period_row;
            $period_count++;
            
        } else {

            break;
        }         

    }
    
    return $period_array;
}

/*
function getDepartmentPeriods($departmentId) {
    
    $query = "SELECT DISTINCT period.*
                FROM period
                INNER JOIN unit ON period.unit_id = unit.unit_id
                INNER JOIN domain_unit ON unit.unit_id = domain_unit.unit_id
                INNER JOIN lu_domain_department ON domain_unit.domain_id = lu_domain_department.domain_id
                WHERE lu_domain_department.department_id = " . $departmentId . "
                AND period.parent_period_id IS NULL
                AND period.start_date <= NOW()
                ORDER BY period.start_date DESC";
    $result = mysql_query($query);
    $resultArray = array();
    while ($row = mysql_fetch_array($result)) {
        $resultArray[] = $row;    
    }
    return $resultArray;    
}
*/
// Go up from department program(s) instead of down from department domain(s)
function queryDepartmentPeriods($departmentId) {
    
    $query = "SELECT DISTINCT period.*
                FROM period
                INNER JOIN period_program ON period.period_id = period_program.period_id
                INNER JOIN programs_unit ON period_program.unit_id = programs_unit.unit_id
                INNER JOIN lu_programs_departments ON programs_unit.programs_id = lu_programs_departments.program_id
                WHERE lu_programs_departments.department_id = " . $departmentId . " 
                AND period.parent_period_id IS NULL
                AND period.start_date <= NOW()
                ORDER BY period.start_date DESC";
    $result = mysql_query($query);
    $resultArray = array();
    while ($row = mysql_fetch_array($result)) {
        $resultArray[] = $row;    
    }
    return $resultArray;    
}

function getDepartmentPeriods($departmentId) {
    
   global $departmentPeriods;
   return $departmentPeriods[$departmentId];
}

function queryDomainPeriods($domainId) {
    
    $query = "SELECT DISTINCT period.*
                FROM period
                INNER JOIN unit ON period.unit_id = unit.unit_id
                INNER JOIN domain_unit ON unit.unit_id = domain_unit.unit_id
                WHERE domain_unit.domain_id = " . $domainId . "
                AND period.parent_period_id IS NULL
                AND period.start_date <= NOW()
                ORDER BY period.start_date DESC";
    $result = mysql_query($query);
    $resultArray = array();
    while ($row = mysql_fetch_array($result)) {
        $resultArray[] = $row;    
    }
    return $resultArray;
}

function getDomainPeriods($domainId) {
    
   global $domainPeriods;
   return $domainPeriods[$domainId];
}

$masterPeriodList = array();
function formatPeriodLink($unit, $unitId, $periodId, $section = 'admin', $viewPeriods = 'current') {
    
    global $masterPeriodList;
    
    if (isset($masterPeriodList[$periodId]))
    {
        $period = $masterPeriodList[$periodId];    
    }
    else
    {
        $period = new Period($periodId);
        $masterPeriodList[$periodId] = $period;    
    }
    
    $activePeriod = $pastPeriod = FALSE;
    $status = $period->getStatus();
    if ($status == 'active') {
        $activePeriod = TRUE;
    }
    if ($status == 'closed') {
        // Return an empty link/heading if view is current only.
        if ($viewPeriods == 'current') {
            return '';
        }
        $pastPeriod = TRUE;
    }

    // Should only be one submission period.
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodDescription = $period->getDescription();
    $periodName = $period->getName(); 

    if ($periodName) {
        $periodLink = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $periodLink = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $periodLink = $periodDates;     
    }

    if ($section == 'admin') {
        $periodLinkStart = '<a href="administerApplications.php?unit=' . $unit;
        $periodLinkStart .= '&unitId=' . $unitId . '&period=' . $periodId . '">'; 
        $periodLink = $periodLinkStart . $periodLink . '</a>'; 
    } 
    
    if ($activePeriod) {
        $periodLink = '<b>' . $periodLink . '</b>';
    } elseif ($pastPeriod) {
        $periodLink = '<i>' . $periodLink . '</i>';    
    }
    
    return $periodLink; 
}


function getDepartmentUnit($departmentId) {
    
    if (!$departmentId) {
        return NULL;
    }
    
    $unitId = NULL;
    
    $query = "SELECT unit_id FROM department_unit 
                WHERE department_id = " . $departmentId;
    $query .= " LIMIT 1";
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $unitId = $row['unit_id'];
    }
    
    if ($unitId) {
        return new Unit($unitId);    
    } else {
        return NULL;
    }
}


function getWaitlistedContent($departmentId) {   
    $sql = "select id,name from content where
            (contenttype_id=5)
            and (department_id=". $departmentId . ") order by name";
    $result = mysql_query($sql) or die(mysql_error() . $sql);
    $results = array();
    while($row = mysql_fetch_array( $result )) {
        $results[] = $row;
    }
    return $results;
}

function truncatePeriods($periods) {
    if (isPeriodActive($periods[0])) {
        return array_slice($periods, 0, 3);
    } else {
        return array_slice($periods, 0, 2);
    }
}

function isPeriodActive ($period) {
    $date = date('Y-m-d H:i:s');
    $start = $period['start_date'];
    $end = $period['end_date'];
    if (isset($end)) {
        if ($date <= $end && $date >= $start){
            return TRUE;
        } else {
            return FALSE;
        }
    } else {
         if ($date >= $start){
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>