<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$questTypes = array(array('1', 'Text Box'), array('2','Short Text'), array('3','Long Text'),
array('4','Check List'), array('5','Radio List' , array('6','Radio Matrix'))
);
$questions = array();
$options = array();
$name = "";
$questType = "";
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" name="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Questionnaire Add/Edit <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>Questionnaire Name:</strong> </td>
        <td><span class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,20); ?>
        </span></td>
      </tr>
      <tr>
        <td width="200" align="right" valign="top"><strong>Questions:</strong></td>
        <td valign="top"><span class="tblItem"><span class="subtitle">
          <? showEditText("Add Question", "button", "btnAdd", $_SESSION['A_allow_admin_edit']); ?>
          <br />
        </span></span>
          <table width="100%"  border="0" cellspacing="2" cellpadding="2">
            <tr>
              <td width="50" align="right" valign="top"><strong>Type: </strong></td>
              <td><span class="tblItem">
                <? showEditText($questType, "listbox", "lbType", $_SESSION['A_allow_admin_edit'],false, $questTypes); ?>
                <span class="subtitle">
                <? showEditText("Refresh", "button", "btnRefresh", $_SESSION['A_allow_admin_edit']); ?>
                </span>                <strong>Required:
                </strong>
                <? showEditText("", "checkbox", "chkRequired", $_SESSION['A_allow_admin_edit'],false); ?>
</span></td>
            </tr>
            <tr>
              <td width="50" align="right" valign="top"><strong>Question:</strong></td>
              <td><span class="subtitle"><span class="tblItem">
                <? showEditText("", "textbox", "txtQuestion", $_SESSION['A_allow_admin_edit'],true,null,true,50); ?>
              </span></span></td>
            </tr>
			<? if(true){ ?>
            <tr valign="top">
              <td width="50" align="right"><strong>Options:</strong></td>
              <td>
                <? showEditText("Add Option", "button", "btnAddOpt", $_SESSION['A_allow_admin_edit']); ?>
                <table width="100%"  border="0" cellspacing="2" cellpadding="4">
                  <tr class="tblHead">
                    <td>Option Text </td>
                    <td>Value(s) - 1 per line </td>
                    <td>Delete</td>
                  </tr>
                  <tr valign="top">
                    <td><span class="subtitle"><span class="tblItem">
                      <? showEditText("", "textbox", "txtQuestion", $_SESSION['A_allow_admin_edit'],true,null,true,50); ?>
                    </span></span></td>
                    <td><span class="subtitle"><span class="tblItem">
                      <? showEditText("", "textarea", "txtValue", $_SESSION['A_allow_admin_edit'],true,null,true,20); ?>
                    </span></span></td>
                    <td width="50"><span class="tblItem">
                      <? showEditText("X", "button", "btnDelOpt_", $_SESSION['A_allow_admin_edit']); ?>
                    </span></td>
                  </tr>
                </table>
                </td>
            </tr>
			<? }//END OPTIONS ?>
          </table></td>
      </tr>
      <tr>
        <td width="200" align="right">&nbsp;</td>
        <td><span class="tblItem"><span class="subtitle">
          <? showEditText("Save Details", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </span></span></td>
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
