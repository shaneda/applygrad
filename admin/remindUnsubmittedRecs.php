<?PHP
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

include '../classes/class.Department.php'; 

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Email Unsubmitted Recommenders';

$pageCssFiles = array(
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br />
<form id="form1" name="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Send Email to Unsubmitted Recommenders <!-- InstanceEndEditable --></span><br />
<br />

<?php
$domains = array();
$content = array();
$deptId = -1;
//$sysemail = "applyweb+usubrecs@cs.cmu.edu";

switch ($hostname)
{
    case "APPLY.STAT.CMU.EDU":  
        $sysemail = "scsstats@cs.cmu.edu";
        break;
    case "APPLYGRAD-INI.CS.CMU.EDU":  
        $sysemail = "scsini@cs.cmu.edu";
        break;
    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
        $sysemail = "scsdiet@cs.cmu.edu";
        break;
    default:
        $sysemail = "applygrad@cs.cmu.edu";
 }

if(isset($_GET['id']))
{
    $deptId = intval($_GET['id']);
}

$domainArray = Department::getDomains($deptId);
$domainName = $domainArray[0]['name'];

/* PLB added query to get the content id to make the link go straight to the content 10/9/09. 
* Compbio (department id 8) is hard-coded to ensure the CompBio domain (domain id 2) 
* content id is retrieved when the department is in two domains.
*/
if ($deptId == 8) {
    $sql = "select content.id from content
    where content.name='Mass Recommendation Reminder Letter' and content.domain_id=2";    
} else {
    $sql = "select content.id from content
    left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
    where content.name='Mass Recommendation Reminder Letter' and lu_domain_department.department_id=".$deptId;    
}
$result = mysql_query($sql) or die(mysql_error());
$contentId = NULL;
while( $row = mysql_fetch_array( $result ) ) {
    $contentId = $row['id'];
}
if ($contentId) {
    $contentLink = "contentEdit.php?id=" . $contentId;
} else {
    $contentLink = "content.php";    
}

?>
	<!-- InstanceBeginEditable name="mainContent" -->
	<table width="600" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <!--
        <td><a href="content.php">(Edit email template) </a>&nbsp;</td>
        -->
        <td><a href="<?php echo $contentLink; ?>">(Edit email template) </a>&nbsp;</td>
        <td align="right"><? showEditText("Send", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
    </table>
	
	<br />
	<br />
<br />
	<?
//GET EMAIL TEMPLATE
$str = "";
/* 
* PLB hard-coded CompBio (department id 8) to ensure the CompBio domain (domain id 2) 
* content is retrieved when the department is in two domains 10/9/09 .
*/
//DebugBreak();
if ($deptId == 8) {
    $sql = "select content from content
    where content.name='Mass Recommendation Reminder Letter' and content.domain_id=2";    
} else {
    $sql = "select content from content
    left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
    where content.name='Mass Recommendation Reminder Letter' and lu_domain_department.department_id=".$deptId;    
}
/*
$sql = "select content from content
left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
where content.name='Mass Recommendation Reminder Letter' and lu_domain_department.department_id=".$deptId;
*/
//echo $sql;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$str = html_entity_decode($row["content"]);
}
?>
    <div id="div1" style="width:600px; background-color:#CCCCCC; padding:10px "><?=$str?></div><br />

<?
/*
* PLB added test to restrict query to applications from the active submission period 10/9/09.
* A department may belong to more than one domain, 
* but it should only be associated with one active submission period.
*/
$periodTypeId = 1;
include '../inc/emailUnsubmitted.inc.php';
//DebugBreak();

$sql = "select
	recommend.id,
	lu_users_usertypes.id as userid,
    period_application.application_id, 
	concat(users.lastname, ', ',users.firstname) as name,
	users.email,
	users.guid,
	
	users_b.firstname as firstname,
	users_b.lastname as lastname,
	users_b.email as appemail,
	lu_domain_department.domain_id as domainid,
	recommend.last_reminder_sent

	from lu_users_usertypes
	
	inner join users as users on users.id = lu_users_usertypes.user_id
	inner join recommend on recommend.rec_user_id = lu_users_usertypes.id
	left outer join application on application.id = recommend.application_id

	inner join lu_users_usertypes as lu_users_usertypes_b on lu_users_usertypes_b.id = application.user_id
	inner join users as users_b on users_b.id = lu_users_usertypes_b.user_id

	left outer join lu_application_programs on lu_application_programs.application_id = application.id
	left outer join programs on programs.id = lu_application_programs.program_id
	left outer join degree on degree.id = programs.degree_id
	left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
	left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id";
	
// PLB added period join 10/9/09 
$sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
    
$sql .= " where recommend.submitted < 1 and application.submitted = 1
	        and lu_programs_departments.department_id=".$deptId;
     
// PLB added period join 10/9/09
$activePeriodCount = count($activePeriodIds);
if ($activePeriodCount  == 0 ) {
    // Super kluge to retrieve 0 records when there are 0 active periods
    $sql .= " AND period_application.period_id = -1";
} elseif ($activePeriodCount == 1) {
    $sql .= " AND period_application.period_id = " . $activePeriodIds[0]; 
} else { 
    // Kluge to handle compbio's being in two, non-identical domains
    $sql .= " AND period_application.period_id IN (" . implode(',', $activePeriodIds) . ")";    
}
     
$sql .= " group by recommend.id order by users.email";
     
	$result = mysql_query($sql) or die(mysql_error().$sql);
	echo mysql_num_rows($result) . " Recommenders have not submitted letters.<br><br>";


if(isset($_POST['btnSubmit']) && $deptId > -1 )
{

	if($str != "")
	{

		//PULL USERS FOR THIS DEPARTMENT

		?>
		<table width="500" border="0" cellspacing="0" cellpadding="4">
		<?
		echo "Mail sent to:<br>";
		$str2 = "";
		while($row = mysql_fetch_array( $result ))
		{

            $applicationId = $row['application_id'];
            
            $mailSieveString = '[' . $domainName . ':';
            $mailSieveString .= implode( '-', getApplicantProgramIds($applicationId) ) . ':';
            $mailSieveString .= $applicationId . ']';

			if( $row['last_reminder_sent'] != date("Y-m-d") )
			{
				$vars = array(
				array('appemail',$row['appemail']),//STUDENT
				array('firstname',$row['firstname']),//STUDENT
				array('lastname',$row['lastname']),//STUDENT
				array('domainid',$row['domainid']),//STUDENT
				array('user', str_replace("+","%2b",$row['email']) ),//RECOMMENDER
				array('pass',$row['guid']),
				array('replyemail',$sysemail),
				);
				$str2 = parseEmailTemplate2($str, $vars );
				//SEND EMAIL
				sendHtmlMail($sysemail, $row['email'], "Recommendation Reminder Letter", $str2, "reminder",
                                "", $mailSieveString);//to user
				$sql = "update recommend set last_reminder_sent = '".date("Y-m-d")."' where id = ". $row['id'];
				mysql_query($sql) or die(mysql_error().$sql);
				//echo $str2 . "<br><br>";
				?>
				<tr>
				<td><strong><?=$row['name']?></strong></td>
				<td><em>(<?=$row['email']?>)</em></td>
				<td>for <?=$row['firstname']. " " . $row['lastname']?></td>
				<td><em>(<?=$row['last_reminder_sent']?>)</em></td>
			  </tr>
				<?
			}//end if
		 }//end while ?>
			</table>
	<?
	}else
	{
		echo "<strong>Email Template Cannot be Empty.</strong> Please edit the template <a href='content.php'>here<a/>";
	}//end if
}else
{
	echo "Mail will only be sent to those who DID NOT receive a reminder today:<br>";
	?>
	<table width="500" border="0" cellspacing="0" cellpadding="4">
	<? 	while($row = mysql_fetch_array( $result )){	?>
	  <tr>
		<td><strong><?=$row['name']?></strong></td>
		<td><em>(<?=$row['email']?>)</em></td>
		<td>for <?=$row['firstname']. " " . $row['lastname']?></td>
		<td><em>(<? if($row['last_reminder_sent']!=""){echo "sent ". $row['last_reminder_sent'];}?>)</em></td>
	  </tr>
	<? } ?>
	</table>
	<? } ?>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>