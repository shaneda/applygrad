<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<?
$id = -1;
$name = "";
$content = "";
$deptId = -1;

$sysemail = "applygrad+@cs.cmu.edu";
$p = 0;
if(isset($_GET['id']))
{
	$id = htmlspecialchars($_GET['id']);
}
if(isset($_GET['p']))
{
	if($_GET['p'] == "1")
	{
		$p = 1;
	}
}
$sql = "SELECT id,name,content, department_id FROM content where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result )) 
{
	$id = $row['id'];
	$name = $row['name'];
	$content = $row['content'];
	$deptId = $row['department_id'];
}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>SCS Applygrad</title>

</head>

<body>
<span class="tblItem">
<?
//DO STUFF HERE TO PARSE THE MESSAGE
$vars = array();

switch($name)
{
	case "Invitation Letter":
		$sql = "select 
		distinct
		firstname, lastname, email, address_cur_street1, gender, 
		address_cur_street2, 
		address_cur_street3, 
		address_cur_street4, 
		address_cur_city, 
		states.name as state, 
		address_cur_pcode, 
		countries.name as country,
		GROUP_CONCAT(concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
		lu_application_programs.faccontact,
		lu_application_programs.stucontact
		from  lu_users_usertypes 
		inner join users on users.id =  lu_users_usertypes.user_id
		inner join users_info on users_info.user_id= lu_users_usertypes.id
		left outer join countries on countries.id = address_cur_country
		left outer join states on states.id = address_cur_state
		left outer join application on application.user_id = lu_users_usertypes.id
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		
		where lu_application_programs.admission_status = 2
		and lu_programs_departments.department_id=".$deptId . "
		group by  application.id
		order by lastname, firstname
		";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		while($row = mysql_fetch_array( $result )) 
		{
			$gender = "";
			$program = $row['program'];
			
			$aPrograms = split(",", $program);
			if(count($aPrograms) > 1)
			{
				$program = "";
				for($i = 0; $i < count($aPrograms); $i++)
				{
					if($i == 0)
					{
						$program .= $aPrograms[$i] . " program";
					}else
					{
						if($i == count($aPrograms)-1)
						{
							$program .= " and the ". $aPrograms[$i]. " program";
						}else
						{
							$program .= ", the ". $aPrograms[$i]. " program";
						}
						
					}
				}
			}
			if($row['gender'] == "M")
			{
				$gender = "Mr. ";
			}
			if($row['gender'] == "F")
			{
				$gender = "Ms. ";
			}

			$address = ucwords(strtolower($row['address_cur_street1'])). "<br>";
			if($row['address_cur_street2'] != "")
			{
				$address .= ucwords(strtolower($row['address_cur_street2'])). "<br>";
			}
			if($row['address_cur_street3'] != "")
			{
				$address .= ucwords(strtolower($row['address_cur_street3'])). "<br>";
			}
			if($row['address_cur_street4'] != "")
			{
				$address .= ucwords(strtolower($row['address_cur_street4'])). "<br>";
			}
			$address .= $row['address_cur_city'];
			if($row['state'] != "")
			{
				$address .= ", ". $row['state']. " ";
			}
			if($row['address_cur_pcode'] != "")
			{
				$address .= " ". $row['address_cur_pcode']. "";
			}
                        $address .= "<br>";
			$address .= ucwords(strtolower($row['country']));

			$vars = array(
			array('date', date("F j, Y") ), 
			array('firstname',ucwords(strtolower($row['firstname']))),	
			array('lastname',ucwords(strtolower($row['lastname']))),	
			array('gender',$gender),	
			array('address',$address),	
			array('program',$program),
			array('faccontact',$row['faccontact']),
			array('stucontact',$row['stucontact'])
			);
			$content2 = parseEmailTemplate2($content, $vars );
			printData($content2);
		}
		
	break;
	case "Rejection Letter":
		$sql = "select 
		distinct
		application.id
		firstname, lastname, email, address_cur_street1, gender, 
		address_cur_street2, 
		address_cur_street3, 
		address_cur_street4, 
		address_cur_city, 
		states.name as state, 
		address_cur_pcode, 
		countries.name as country,
		GROUP_CONCAT(concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
		lu_application_programs.faccontact,
		lu_application_programs.stucontact
		from  lu_users_usertypes 
		inner join users on users.id =  lu_users_usertypes.user_id
		inner join users_info on users_info.user_id= lu_users_usertypes.id
		left outer join countries on countries.id = address_cur_country
		left outer join states on states.id = address_cur_state
		left outer join application on application.user_id = lu_users_usertypes.id
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id 
		left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id

		where (lu_application_programs.admission_status IS NULL 
		or lu_application_programs.admission_status != 2 )
		and application.submitted = 1
		and (lu_domain_department.domain_id=-1 ";
		for($i = 0; $i < count($_SESSION["A_admin_domains"]); $i++)
		{
			$sql .= " or lu_domain_department.domain_id= ". $_SESSION["A_admin_domains"][$i];
		}
		$sql .=") group by  application.id
		order by lastname, firstname
		";
		echo $sql. "<br>";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		while($row = mysql_fetch_array( $result )) 
		{
			$gender = "";
			$program = $row['program'];
			
			$aPrograms = split(",", $program);
			if(count($aPrograms) > 1)
			{
				$program = "";
				for($i = 0; $i < count($aPrograms); $i++)
				{
					if($i == 0)
					{
						$program .= $aPrograms[$i] . " program";
					}else
					{
						if($i == count($aPrograms)-1)
						{
							$program .= " and the ". $aPrograms[$i]. " program";
						}else
						{
							$program .= ", the ". $aPrograms[$i]. " program";
						}
						
					}
				}
			}
			

			$vars = array( 
			array('applicant', ucwords(strtolower($row['firstname'])) . " ".  ucwords(strtolower($row['lastname'])) ),			
			array('program',$program)
			);
			$content2 = parseEmailTemplate2($content, $vars );
			printData($content2);
			$sql = "update application set rejection_sent = 1 where id = ".$row["id"];
			//mysql_query($sql) or die(mysql_error() . $sql);
			
			//sendHtmlMail($sysemail, $row['email'], "CMU Admissions Decision", $content2, "rejection");//to user
		}
		
		
	break;
}
function printData($content)
{
	echo html_entity_decode($content) ."\n<br style='page-break-after:always;'>\n";
}
?>
</span>
</body>
</html>
