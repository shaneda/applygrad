<?PHP
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

include '../classes/class.Department.php'; 

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Email Applicants with Incomplete Applications';

$pageCssFiles = array(
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br />
<form id="form1" name="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Send Email to Applicants with Incomplete Applications</span><br />
<br />

<?php
$domains = array();
$content = array();
$deptId = -1;
//$sysemail = "applyweb+unpaidapps@cs.cmu.edu";
switch ($hostname)
{
    case "APPLY.STAT.CMU.EDU":  
        $sysemail = "scsstats@cs.cmu.edu";
        break;
    case "APPLYGRAD-INI.CS.CMU.EDU":  
        $sysemail = "scsini@cs.cmu.edu";
        break;
    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
        $sysemail = "scsdiet@cs.cmu.edu";
        break;
    default:
        $sysemail = "applygrad@cs.cmu.edu";
 }

if(isset($_GET['id']))
{
    $deptId = intval($_GET['id']);
}

$domainArray = Department::getDomains($deptId);
$domainName = $domainArray[0]['name']; 

/* PLB added query to get the content id to make the link go straight to the content 10/9/09. 
* Compbio (department id 8) is hard-coded to ensure the CompBio domain (domain id 2) 
* content id is retrieved when the department is in two domains.
*/
//DebugBreak();
if ($deptId == 8) {
    $sql = "select content.id from content
    where content.name='Applicant Completion Reminder' and content.domain_id=2";    
} else {
    $sql = "select content.id from content
    left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
    where content.name='Applicant Completion Reminder' and lu_domain_department.department_id=".$deptId;    
}
$result = mysql_query($sql) or die(mysql_error());
$contentId = NULL;
while( $row = mysql_fetch_array( $result ) ) {
    $contentId = $row['id'];
}
if ($contentId) {
    $contentLink = "contentEdit.php?id=" . $contentId;
} else {
    $contentLink = "content.php";    
}
?>
	<table width="600" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <!--
        <td><a href="content.php">(Edit email template)</a></td>
        -->
        <td><a href="<?php echo $contentLink; ?>">(Edit email template)</a></td>
        <td align="right"><? showEditText("Send", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
    </table>
	<br />
	<br />
<br />
	<?
//GET EMAIL TEMPLATE
$str = "";
/* 
* PLB hard-coded CompBio (department id 8) to ensure the CompBio domain (domain id 2) 
* content is retrieved when the department is in two domains 10/9/09 .
*/
if ($deptId == 8) {
    $sql = "select content from content
    where content.name='Applicant Payment Reminder' and content.domain_id=2";    
} else {
    $sql = "select content from content
    left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
    where content.name='Applicant Completion Reminder' and lu_domain_department.department_id=".$deptId;    
}
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$str = html_entity_decode($row["content"]);
}
?>
    <div id="div1" style="width:550px; background-color:#CCCCCC; padding:10px "><?=$str?></div><br />

<?

/*
* PLB added test to restrict query to applications from the active submission period 10/9/09.
* A department may belong to more than one domain, 
* but it should only be associated with one active submission period.
*/
//DebugBreak(); 
include '../inc/emailUnsubmitted.inc.php';

//PULL USERS FOR THIS DEPARTMENT
$sql = "select
	lu_users_usertypes.id as id,
    application.id AS application_id,
	concat(users.lastname, ', ',users.firstname) as name,
	users.email
	from lu_users_usertypes
	inner join users on users.id = lu_users_usertypes.user_id
	left outer join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join countries on countries.id = users_info.address_perm_country
	left outer join application on application.user_id = lu_users_usertypes.id
	left outer join lu_application_programs on lu_application_programs.application_id = application.id
	left outer join programs on programs.id = lu_application_programs.program_id
	left outer join degree on degree.id = programs.degree_id
	left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";

// PLB added period join 10/9/09
$sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
    
$sql .=	" where lu_users_usertypes.usertype_id = 5";

// PLB added period join 10/9/09
$activePeriodCount = count($activePeriodIds);
if ($activePeriodCount  == 0 ) {
    // Super kluge to retrieve 0 records when there are 0 active periods
    $sql .= " AND period_application.period_id = -1";
} elseif ($activePeriodCount == 1) {
    $sql .= " AND period_application.period_id = " . $activePeriodIds[0]; 
} else { 
    // Kluge to handle compbio's being in two, non-identical domains
    $sql .= " AND period_application.period_id IN (" . implode(',', $activePeriodIds) . ")";    
}

$sql .=	" AND application.submitted = 1
    AND application.sent_to_program != 1
	and lu_programs_departments.department_id=".$deptId . " group by users.email order by users.lastname, users.firstname";


$result = mysql_query($sql) or die(mysql_error().$sql);

if(isset($_POST['btnSubmit']) && $deptId > -1)
{

	if($str != "")
	{

		//PULL USERS FOR THIS DEPARTMENT

		?>
		<table width="500" border="0" cellspacing="0" cellpadding="4">
		<?
		echo "Mail sent to:<br>";
		$str2 = "";
		while($row = mysql_fetch_array( $result ))
		{

            $applicationId = $row['application_id'];
            
            $mailSieveString = '[' . $domainName . ':';
            $mailSieveString .= implode( '-', getApplicantProgramIds($applicationId) ) . ':';
            $mailSieveString .= $applicationId . ']';
            
			$vars = array(
			array('userid',$row['email'])
			);
			$str2 = parseEmailTemplate2($str, $vars );
			//SEND EMAIL
			sendHtmlMail($sysemail, $row['email'], "Applicant Completion Reminder", $str2, "reminder",
                            "", $mailSieveString);//to user
			?>
			<tr>
			<td><strong><?=$row['name']?></strong></td>
			<td><em>(<?=$row['email']?>)</em></td>
		  </tr>
			<? } ?>
			</table>
	<?
	}else
	{
		echo "<strong>Email Template Cannot be Empty.</strong> Please edit the template <a href='content.php'>here<a/>";
	}//end if
}else
{
//	echo "Mail will be sent to";
	?>
	<table width="500" border="0" cellspacing="0" cellpadding="4">
	<? 	$numemails2send = mysql_num_rows($result);
    echo "Mail will be sent to the following ".$numemails2send. " applicants:<br>";
     while($row = mysql_fetch_array( $result )){	?>
	  <tr>
		<td><strong><?=$row['name']?></strong></td>
		<td><em>(<?=$row['email']?>)</em></td>
	  </tr>
	<? } ?>
	</table>
	<? } ?>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>