<?php
//ini_set('memory_limit', '64M');

header("Cache-Control: no-cache");

include_once "../inc/config.php";
include_once "../inc/session_admin.php";

require('Structures/DataGrid.php');
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/class.db_application_list.php";
include "../classes/class.db_student_decision.php";
include "../classes/class.db_domain.php";
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
include_once '../inc/specialCasesAdmin.inc.php';

$pageTitle = 'Invited Applicant Decisions';

$pageCssFiles = array(
    '../css/normalizations.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/studentDecision.js'
    );
    
function getDomainFromDeptId($deptId) {
       $domain = array();
       if ($deptId == 3) {   // kludge for RI
           return 3;
       } else {
           $sql = "SELECT * from lu_domain_department
                where department_id = " . $deptId;
           $results = mysql_query($sql);
           while ($row = mysql_fetch_array($results)) {
                $domain[] = $row['domain_id'];
                }
           if (sizeof($domain) == 1) {
               return $domain[0];
           } else if (sizeof($domain) > 1) {   // kludgery for SCS/RI domain crap
                    $smallest = 999;
                    foreach ($domain as $value) {
                        if ($value < $smallest) {
                            $smallest = $value;
                        }
                    }
                    return $smallest;
                }
               
           }
       }
 
include '../inc/tpl.pageHeader.php';

// get an application list for department=1 from the db
if ( isset($_REQUEST['id']) ) {
    $department_id = $_REQUEST['id'];
} else {
    $department_id = 1;
}

if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period'];    
} else {
    $periodId = NULL;
}

$sql = "select name from department where id=".$department_id;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $departmentName = $row['name'];
}

$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    } 
}

$isIniDepartment = isIniDepartment($department_id);
$isDesignDepartment = isDesignDepartment($department_id);

$applicationList = new DB_ApplicationList();

$applicationListRecords = 
    $applicationList->getInvitedByDepartment($department_id, NULL, NULL, $periodId, $isIniDepartment, $isDesignDepartment);
$record_count = count($applicationListRecords);

// Create the DataGrid, Bind it's Data Source and Define it's columns
$dg = new Structures_DataGrid();

// bind application list data using array driver
$dg->bind($applicationListRecords ,array() , 'Array');

// create the columns
$dg->addColumn(new Structures_DataGrid_Column('', 'user_id', 'user_id', array('width' => '5%', 'align' => 'center'), null, 'printPrintLink()', $dg->getCurrentRecordNumberStart()));
$dg->addColumn(new Structures_DataGrid_Column('Name', 'name', 'name', array('width' => '20%', 'align' => 'left'), null, 'printFullName()'));
$dg->addColumn(new Structures_DataGrid_Column('Program', 'program', 'program', array('width' => '25%', 'align' => 'center'), null));
$dg->addColumn(new Structures_DataGrid_Column('Admission Status', 'admission_status', 'admission_status', array('width' => '10%', 'align' => 'center'), null, 'printAdmissionStatus()'));
$dg->addColumn(new Structures_DataGrid_Column('Applicant Decision', 'decision', 'decision', array('width' => '30%', 'align' => 'center'), null, 'printDecisionRadio()'));
/*   Text in column for LTI report that Krista and Kelly wanted
if ($department_id == 6 || $department_id == 83 || $department_id == 56 || $department_id == 20) {
    $dg->addColumn(new Structures_DataGrid_Column('Decision', 'decision', 'decision', array('width' => '30%', 'align' => 'center'), null, 'printDecisionText()'));
}
*/
$dg->addColumn(new Structures_DataGrid_Column('', '', '', array('width' => '10%', 'align' => 'center'), null));

if ($department_id == 1 || $department_id == 2 || $department_id ==88 
    || isEnglishMaDepartment($department_id) || isEnglishPhdDepartment($department_id)
 //   || isEnglishDomain(getDomainFromDeptId($department_id)) 
    || isIniDepartment($department_id)
    || isMseMsitEbizDepartment($department_id)
    || isDesignDepartment($department_id)
    || isDesignPhdDepartment($department_id)
    || isDesignDdesDepartment($department_id)
    || isMseMITS($department_id)
    || isMseCampus($department_id)
    || isMseESE($department_id)
    || isMsaii($department_id)
    || isPrivacy($department_id)
    || $departmentName == 'LTI-IIS'
    || $departmentName == 'Language Technologies Institute'
    || $departmentName == 'Master of Computational Data Science') {
    $dg->addColumn(new Structures_DataGrid_Column('', 'user_id', 'user_id', array('width' => '10%', 'align' => 'center'), null, 'printReplyFormLink()', $dg->getCurrentRecordNumberStart()));
}

// Define the Look and Feel
$dg->renderer->setTableHeaderAttributes(array('bgcolor' => '#CCCCCC'));
$dg->renderer->setTableEvenRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu')
    );
$dg->renderer->setTableOddRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu')
    );
$dg->renderer->setTableAttribute('width', '100%');
$dg->renderer->setTableAttribute('border', '0');
$dg->renderer->setTableAttribute('cellspacing', '0');
$dg->renderer->setTableAttribute('cellpadding', '5px');
$dg->renderer->setTableAttribute('class', 'datagrid applicants');
$dg->renderer->sortIconASC = '&uArr;';
$dg->renderer->sortIconDESC = '&dArr;';
?>

<div id="pageHeading">
<div id="departmentName">
<?php
echo $departmentName;
if ($applicationPeriodDisplay) {
    echo $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle">Invited Applicant Decisions
<span style="font-size: 15px;">(n=<?php echo $dg->getRecordCount(); ?>)</span> 
</div>
</div> 

<?php
// render the datagrid
echo '<div id="applicants" style="background: none;">';
$dg->render();
echo '</div>';

// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

// callback functions for datagrid rendering

function printPrintLink($params, $recordNumberStart) {
    extract($params);
    $user_id = $record['user_id'];
    $applicationId = $record['application_id'];
    $link = $params['currRow'] + $recordNumberStart . "." . "&nbsp;&nbsp;";
    $link .= "<a class=\"menu\" href=\"\" title=\"Print Application " . $applicationId . "\" ";
    if ($_SESSION['A_usertypeid'] == 11)  {
        // Student contacts should not be getting a link to this page, but if
        // they do end up here, they should get a link to the minimal application summary. 
        $link .= "onClick=\"window.open('../admin/userroleEdit_student_formatted.php";
    } else {
        $link .= "onClick=\"window.open('../review/userroleEdit_student_print.php";
    }
    $link .= "?id=" . $user_id . "&applicationId=" . $applicationId . "'); return false;\">Print</a>";
    return $link;
}

function printReplyFormLink($params, $recordNumberStart) {
    extract($params);
    $user_id = $record['user_id'];
    $applicationId = $record['application_id'];
//    $link = $params['currRow'] + $recordNumberStart . "." . "&nbsp;&nbsp;";
    $link = "<a class=\"menu\" href=\"\" title=\"Print Reply Form " . $applicationId . "\" ";
    
    $link .= "onClick=\"window.open('../replyform/admin.php";

    $link .= "?id=" . $user_id . "&applicationId=" . $applicationId . "'); return false;\">Decision Info</a>";
    return $link;
}


function printFullName($params)
{   
    extract($params);
    $name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">';
    $name .= $record['name'] . '</span><br />';
    $name .= '<a title="E-mail Applicant" href="mailto:' . $record['email'] . '">' . $record['email'] . '</a>'; 
    //return "<b>" . $record['name'] . "</b><br /><a title=\"E-mail Applicant\" href=\"mailto:" . $record['email'] . "\">" . $record['email'] . "</a>";
    return $name;
}


function printAdmissionStatus($params) {

    extract($params);

    $status = $record['admission_status'];

    switch ($status) {

        case 0:
        
            $status_string = "Reject";
            break;    
        
        case 1:
        
            $status_string = "Waitlist";
            break;
        
        case 2:
        
            $status_string = "Admit";
            break;  
        
        default:
                
           $status_string = "Reject";
    }
    
    return $status_string;
    
}

function printDecisionRadio($params) {

    extract($params);
    $application_id = $record['application_id'];
    $program_id = $record['program_id'];
    $decision = $record['decision'];
    $id_key = $application_id . '_' . $program_id;
    
    $accept_checked = $decline_checked = $defer_checked = "";

    switch ($decision) {

        case "accept":
        
            $accept_checked = "checked";
            break;
        
        case "decline":
        
            $decline_checked = "checked";
            break;  
        
        case "defer":
        
           $defer_checked = "checked";
           break;
    } 
    
    $radio = '<input type="radio" name="studentDecision_' . $id_key . '" id="decisionAccept_' . $id_key . '" class="studentDecision" value="accept"' . $accept_checked . '>';
    $radio .= 'Accept &nbsp;&nbsp;';
    $radio .= '<input type="radio" name="studentDecision_' . $id_key . '" id="decisionDecline_' . $id_key . '" class="studentDecision" value="decline"' . $decline_checked . '>';
    $radio .= 'Decline &nbsp;&nbsp;';
    $radio .= '<input type="radio" name="studentDecision_' . $id_key . '" id="decisionDefer_' . $id_key . '" class="studentDecision" value="defer"' . $defer_checked . '>';
    $radio .= 'Defer'; 

    return $radio;
}

function printDecisionText($params) {

    extract($params);
    $application_id = $record['application_id'];
    $program_id = $record['program_id'];
    $decision = $record['decision'];
    $id_key = $application_id . '_' . $program_id;
    
     return $decision;
}

?>