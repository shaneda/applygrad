<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$name = "";
$description = "";
$path = "";
$active = 0;
$id = -1;

$acontent = array(
	array(2,'Invitation Letter'),
	array(2,'Faculty Cover Sheet'),
	array(2,'Student Cover Sheet'),
	array(2,'Women@IT Letter'),
	array(3,'Rejection Letter'),
	array(4,'Submission Letter'),
	array(4,'Page Update Letter'),
	array(4,'Recommendation Letter'),
	array(4,'Recommendation Reminder Letter'),
	array(4,'Forgot Password Letter'),
	array(1,'Home Page'),
	array(1,'Home Page - Application Complete'),
	array(1,'Index Page'),
	array(1,'Index Page for Recommenders'),
	array(1,'Logout Page for Recommenders'),
	array(1,'New Student Account Confirmation'),
	array(1,'Applicant Submission Reminder'),
	array(1,'FAQ'),
	array(1,'Biographical Information'),
	array(1,'Programs'),
	array(1,'Test Scores'),
	array(1,'Resume and Statement'),
	array(1,'Recommenders'),
	array(1,'Recommendation Main Page'),
	array(1,'Recommendation Upload Page'),
	array(1,'Supplemental Information'),
	array(1,'College/University'),
	array(1,'Instructions'),
	array(1,'Offline message'),
	array(1,'Payment Mail Page'),
	array(1,'Payment Credit Page')
);

if(isset($_POST['txtdomainId']))
{
	$id = htmlspecialchars($_POST['txtdomainId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$description = htmlspecialchars($_POST['txtDesc']);
	$path = htmlspecialchars($_POST['txtPath']);
	if(isset($_POST['chkActive']))
	{
		$active = 1;
	}
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update domain set
			name='".$name."',
			description='".$description."',
			path='".$path."',
			active=".$active." where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			echo $sql . "<br>";
		}
		else
		{
			$sql = "insert into domain(name, description, path, active)values('".$name."','".$description."','".$path."',".$active.")";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}

		for($i = 0; $i < count($acontent); $i++)
		{
			$sql = "SELECT id FROM content where name='".$acontent[$i][1]."' and domain_id=".$id;
			$result = mysql_query($sql) or die(mysql_error() . $sql);
			$cid = -1;
			while($row = mysql_fetch_array( $result ))
			{
				$cid = $row['id'];
			}
			if($cid == -1)
			{
				$sql = "insert into content (name, contenttype_id,domain_id)values('".$acontent[$i][1]."',".$acontent[$i][0].",".$id.")";
				//mysql_query($sql) or die(mysql_error() . $sql);
			}
		}

		header("Location: domains.php");
	}
}
$sql = "SELECT id,name,description, path, active FROM domain where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$description = $row['description'];
	$path = $row['path'];
	$active = $row['active'];
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Domain';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Domain</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtdomainId" type="hidden" id="txtdomainId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>
        </td>
      </tr>
      <tr>
        <td align="right"><strong>Description:</strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($description, "textarea", "txtDesc", $_SESSION['A_allow_admin_edit'],false); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>Path:</strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($path, "textbox", "txtPath", $_SESSION['A_allow_admin_edit'], false,null,true,30); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>Active:</strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($active, "checkbox", "chkActive", $_SESSION['A_allow_admin_edit'],false); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>