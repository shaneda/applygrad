<?php
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Applyweb_Table.php';

include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Program.php';
include '../classes/DB_Applyweb/class.DB_ProgramType.php';
include '../classes/DB_Applyweb/class.DB_UnitRole.php';

include '../classes/DB_Applyweb/class.DB_Programs.php';
include '../classes/DB_Applyweb/class.DB_ProgramsUnit.php';
include '../classes/DB_Applyweb/class.DB_Department.php'; 
include '../classes/DB_Applyweb/class.DB_DepartmentUnit.php';
include '../classes/DB_Applyweb/class.DB_Domain.php';
include '../classes/DB_Applyweb/class.DB_DomainUnit.php';

include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_UnitPeriod.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/DB_Applyweb/class.DB_PeriodType.php'; 
include '../classes/DB_Applyweb/Table/class.DB_PeriodUmbrella.php';  

include '../classes/DB_Applyweb/Table/class.DB_ProgramGroup.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupProgram.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupType.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupGroupType.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupRole.php';

include '../classes/Unit/class.UnitBase.php';
include '../classes/Unit/class.Program.php';
include '../classes/Unit/class.Unit.php';

include '../classes/Period/class.PeriodBase.php';
include '../classes/Period/class.SubPeriod.php';
include '../classes/Period/class.UmbrellaPeriod.php';
include '../classes/Period/class.Period.php';
include '../classes/Period/class.ProgramGroup.php'; 

include '../classes/class.User.php';

require("HTML/QuickForm.php");
include '../classes/HTML_QuickForm/Period/class.NewPeriod_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.Period_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.PeriodProgram_QuickForm.php';

include '../classes/HTML_QuickForm/Period/class.UmbrellaPeriod_QuickForm.php';

/*
* Handle the request variables.
*/
$edit = NULL;
if ( isset($_REQUEST['edit']) ) {
    $edit = $_REQUEST['edit'];
}

$unit = $unitId = NULL;
if ( isset($_REQUEST['unit_id']) &&  $_REQUEST['unit_id'] != '' ) {
    $unitId = $_REQUEST['unit_id'];
    $unit = new Unit($unitId);
}

$umbrellaPeriod = $umbrellaPeriodId = NULL;
if ( isset($_REQUEST['period_id']) &&  $_REQUEST['period_id'] != '' ) {

    if ( isset($_REQUEST['period_type_id']) 
        && isset($_REQUEST['parent_period_id']) 
        && $_REQUEST['period_type_id'] != 1 ) 
    {
        // A subperiod form has been submitted.
        $umbrellaPeriodId = $_REQUEST['parent_period_id'];
            
    } else {
        
        // The umbrella period form has been submitted.
        $umbrellaPeriodId = $_REQUEST['period_id'];    
    }
    $umbrellaPeriod = new Period($umbrellaPeriodId);
    
    if (!$unitId) {
    
        $unitId = $umbrellaPeriod->getUnitId();
        $unit = new $unit($unitId);   
    }
    
} elseif ( isset($_REQUEST['parent_period_id']) 
    && isset($_REQUEST['period_type_id']) && $_REQUEST['period_type_id'] != 1 ) 
{ 
    // This is a (not currently possible?) case where new subperiod data 
    // has been submitted before new umbrella period data?  
    $umbrellaPeriodId = $_REQUEST['parent_period_id'];
    $umbrellaPeriod = new Period($umbrellaPeriodId);

} elseif ( $unitId  && $edit != 'newPeriod' ) {

    if ( $edit != 'period' 
        || ( isset($_REQUEST['submitPeriod']) && $_REQUEST['submitPeriod'] == 'Cancel') ) 
    {
        // A new umbrella period form has been cancelled.
        $periods = $unit->getPeriods();
        if (count($periods) > 0) {
            
            // Default to the latest available period.
            $umbrellaPeriod = $periods[0];
            $umbrellaPeriodId = $umbrellaPeriod->getId();
              
        } else {
            
            // The unit has no periods. 
            $umbrellaPeriod = new Period();
        }
    
    } else {
        
        // A new umbrella period has been started. 
        $umbrellaPeriod = new Period();    
    }     

} else {
    
    // A new, unitless umbrella period has been started.
    // The unit will be determined from the user permissions.
    $umbrellaPeriod = new Period(); 
}

/* 
* Get the user's permissions.
*/
$user = new User();
$user->loadFromSession();
$userRole = $user->getUserRole();

$allAdminUnitIds = $rootAdminUnitIds = $user->getAdminUnits();
$rootUnitDescendantIds = array();
foreach ($rootAdminUnitIds as $rootUnitId) {    
    if (!$unitId && !$edit) {
        $unitId = $rootUnitId;
        $unit = new Unit($unitId);
    }
    $rootUnit = new Unit($rootUnitId);
    $rootUnitDescendantIds = array_merge( $rootUnitDescendantIds, $rootUnit->getDescendantUnitIds() );
}
$allAdminUnitIds = array_merge($allAdminUnitIds, $rootUnitDescendantIds);

$allowAdmin = FALSE;
if ( ($userRole == 'Administrator' || $userRole == 'Super User') 
    && ( ($unitId && in_array($unitId, $allAdminUnitIds) ) || $edit)  ) 
{
    $allowAdmin = TRUE;        
}

/*
* If the user doesn't have admin rights for the unit, include the "view" template and exit.
*/
if (!$allowAdmin) { 
    include '../inc/tpl.periods.php';
    exit;
}

/*
* Otherwise... handle the "add new period" form (only submits edit=newPeriod). 
*/
$newPeriodForm = new NewPeriod_QuickForm();
$newPeriodForm->handleSelf($unit);
$newPeriodFormSubmitted = $newPeriodForm->isSubmitted();

/*
* Always handle the form for the umbrella period.
*/
$umbrellaForm = new UmbrellaPeriod_QuickForm();
$umbrellaFormValid = $umbrellaForm->handleSelf($umbrellaPeriod, $unit);
if ($umbrellaFormValid && !$umbrellaPeriodId) {
    // Redirect to the new period.
    header( 'Location: https://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . '?unit_id=' . $unit->getId() ); 
    exit;
}

/*
* If working on a new period, stop after setting up the period form.
*/
if ($newPeriodFormSubmitted ) {
    // Include the "view" template and exit.
    include '../inc/tpl.periods.php';
    exit;
}

/*
* Otherwise... handle the form for period_program table elements.
*/
$periodProgramForm = new PeriodProgram_QuickForm();
$periodProgramForm->handleSelf($umbrellaPeriod, $unit);

/*
* Handle the submission subperiod form (period table elements).
*/
$submissionPeriodForm = new Period_QuickForm('submissionPeriod');
$submissionPeriodForm->removeElement('description');
$submissionPeriodForm->removeElement('start_date');

$submissionPeriods = $umbrellaPeriod->getChildPeriods(2);
if ( count($submissionPeriods) > 0 ) {
    $submissionPeriod = $submissionPeriods[0];
} else {
    $submissionPeriod = new Period(NULL, $umbrellaPeriodId, 'application submission');    
} 
$submissionPeriodForm->handleSelf($submissionPeriod, $unit);

/*
* Handle the editing subperiod form (period table elements).
*/
$editingPeriodForm = new Period_QuickForm('editingPeriod');
$editingPeriodForm->removeElement('description');
$editingPeriodForm->removeElement('start_date');

$editingPeriods = $umbrellaPeriod->getChildPeriods(3);
if ( count($editingPeriods) > 0 ) {
    $editingPeriod = $editingPeriods[0];
} else {
    $editingPeriod = new Period(NULL, $umbrellaPeriodId, 'application editing');    
} 
$editingPeriodForm->handleSelf($editingPeriod, $unit);   

// Viewing subperiod not currently used.
/*
$viewingPeriodForm = new Period_QuickForm('viewingPeriod');
$viewingPeriodForm->removeElement('description');
$viewingPeriodForm->removeElement('start_date');

$viewingPeriods = $umbrellaPeriod->getChildPeriods(7);
if ( count($viewingPeriods) > 0 ) {
    $viewingPeriod = $viewingPeriods[0];
} else {
    $viewingPeriod = new Period(NULL, $umbrellaPeriodId, 'application viewing');    
} 
$viewingPeriodForm->handleSelf($viewingPeriod, $unit);
*/ 

/*
* Handle the placeout subperiod form (period table elements) for MHCI.
*/
if ($unitId == 47) {
    $placeoutPeriodForm = new Period_QuickForm('placeoutPeriod');
    $placeoutPeriods = $umbrellaPeriod->getChildPeriods(4);
    if ( count($placeoutPeriods) > 0 ) {
        $placeoutPeriod = $placeoutPeriods[0];
    } else {
        $placeoutPeriod = new Period(NULL, $umbrellaPeriodId, 'placeout review');    
    } 
    $placeoutPeriodForm->handleSelf($placeoutPeriod, $unit);   
}


/*
*  Include the "view" template.
*/
include '../inc/tpl.periods.php'; 
?>