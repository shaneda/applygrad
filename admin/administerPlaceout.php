<?php
$startTime = microtime(TRUE);
ini_set('memory_limit', '64M');
header("Cache-Control: no-cache");

// Standard includes.
include "../inc/config.php";
include "../inc/session_admin.php";

// Page-specific includes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Domain.php"; 
include "../classes/DB_Applyweb/class.DB_Department.php";
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminList.php";  
include "../classes/DB_Applyweb/View/class.VW_PlaceoutListBase.php";
include "../classes/Data/class.PlaceoutListData.php";
include "../classes/Structures_DataGrid/class.PlaceoutList_DataGrid.php";
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

//DebugBreak();
/*
* Handle the request variables.
*/
$unit = filter_input(INPUT_GET, 'unit', FILTER_UNSAFE_RAW);
if ($unit != 'department' && $unit != 'domain') {
    $unit = 'department';
}
$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
$programId = filter_input(INPUT_GET, 'programId', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);
$applicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT);
$luuId = filter_input(INPUT_GET, 'luuId', FILTER_VALIDATE_INT);
$searchString = filter_input(INPUT_GET, 'searchString', FILTER_UNSAFE_RAW);

// Page size
$limit = filter_input(INPUT_GET, 'limit', FILTER_VALIDATE_INT);
if (!$limit) {
    $limit = 100;
}

/*
* Handle the filter-related request variables.
*/
$designStatus = filter_input(INPUT_GET, 'designStatus', FILTER_UNSAFE_RAW);
if (!$designStatus) {
    $designStatus = "allDesign";    // default to no filter   
}
$allDesignSelected = $designNotBegunSelected = $designSubmittedSelected = '';
$designInProgressSelected = $designApprovedPlanSelected = '';
$designFulfilledDegreeSelected = $designFulfilledOtherSelected = '';
$designSelected = $designStatus . 'Selected';
$$designSelected = "selected";

$programmingStatus = filter_input(INPUT_GET, 'programmingStatus', FILTER_UNSAFE_RAW);
if (!$programmingStatus) {
    $programmingStatus = "allProgramming";    // default to no filter    
}
$allProgrammingSelected = $programmingNotBegunSelected = $programmingSubmittedSelected = '';
$programmingInProgressSelected = $programmingApprovedPlanSelected = '';
$programmingFulfilledDegreeSelected = $programmingFulfilledOtherSelected = '';
$programmingSelected = $programmingStatus . 'Selected';
$$programmingSelected = "selected";

/* remove statistics from placeout
$statisticsStatus = filter_input(INPUT_GET, 'statisticsStatus', FILTER_UNSAFE_RAW);
if (!$statisticsStatus) {
    $statisticsStatus = "allStatistics";    // default to no filter   
}
$allStatisticsSelected = $statisticsNotBegunSelected = $statisticsSubmittedSelected = '';
$statisticsInProgressSelected = $statisticsApprovedPlanSelected = '';
$statisticsFulfilledDegreeSelected = $statisticsFulfilledOtherSelected = '';
$statisticsSelected = $statisticsStatus . 'Selected';
$$statisticsSelected = "selected";
 */
 
/*
* Set up the main heading period info. 
*/
if ($periodId) {
    
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(4);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    if ($endDate == '') {
        $endDate = 'present';    
    }
    $application_period_display = '<br/>' . $startDate . '&nbsp;to&nbsp;' . $endDate;
    $description = $period->getDescription();
    if ($description) {
        $application_period_display .= ' (' . $description . ')';    
    }    

} else {
    $application_period_id = NULL;
    $start_date = NULL;
    $end_date = NULL;
    $application_period_display = "";
}


/*
* Handle searchString and applicationId and set 
* the post-search "return to list" link accordingly.
*/
$returnUrl = "administerPlaceout.php?unit=" . $unit . "&unitId=" . $unitId;
$returnUrl .= "&period=" . $periodId;
if ($programId) {
    $returnUrl .= "&programId=" . $programId;    
}
$returnLink = "";
$returnLinkOn = '<span class="return"><a href=" ';
$returnLinkOn .= $returnUrl . '" class="return" onClick="document.filterForm.submit(); return false;">';
$returnLinkOn .= 'Return to full list</a></span>';
$showReturnLink = '';
if ( isset($_REQUEST['showReturnLink']) ) {
    $showReturnLink = $_REQUEST['showReturnLink'];    
}
if ( isset($_REQUEST['submitJumpTo']) || $showReturnLink == 'show' ) {
    $returnLink = $returnLinkOn;    
}


/*
* Set the domain/department name
*/
switch ($unit) {
    
    case 'domain':
    
        $dbUnit = new DB_Domain();
        break;
    
    case 'department':
    default:
        
        $dbUnit = new DB_Department();
}
$unitRecord = $dbUnit->get($unitId);
$unitName = $unitRecord[0]['name'];
$_SESSION['domainname'] = $unitName;


/* 
* Instantiate the datagrid and data access class 
* and bind the data to the datagrid.
*/
//DebugBreak();
if (isset($programId)) {
    $placeoutListData = new PlaceoutListData('program', $programId);    
} else {
    $placeoutListData = new PlaceoutListData($unit, $unitId);    
}
$placeoutListArray = $placeoutListData->getData($periodId, $applicationId, $luuId, $searchString,
    $designStatus, $programmingStatus, $statisticsStatus);
$dataGrid = new PlaceoutList_DataGrid($placeoutListArray, $limit);


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Administer Placeout';

$pageCssFiles = array(
    '../css/jquery.autocomplete.css',
    '../css/administerApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/administerApplications.js'
    );
 
include '../inc/tpl.pageHeader.php';


/*
* Render the view. 
*/
?>
<br/>
<div style="margin: 10px;">

<table cellspacing="2" width="100%" border="0">
    <tr valign="top">
        <td align="left">

<span class="title">
<?php echo $unitName . $application_period_display; ?>
<br/><span style="font-size: 18px;">Administer Placeout</span>  
</span>
     
        </td>
        <td  rowspan="2" align="right" width="40%">

<div style="padding: 2px; width: auto; background: #F7F7F7;">

<form id="filterForm" name="filterForm" action="<?php echo $returnUrl; ?>" method="GET">
<input type="hidden" id="filterUnit" name="unit" value="<?php echo $unit; ?>" />
<input type="hidden" id="filterUnitId" name="unitId" value="<?php echo $unitId; ?>" />
<?php
if ($periodId) {        
    echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod" name="period" value="' . $periodId . '"/>';
}
if ($programId) {
    echo '<input type="hidden" id="jumpToProgramId" name="programId" value="' . $programId . '" />';    
}
?>
<input type="hidden" id="filterLimit" name="limit" value="<?php echo $limit; ?>" />
<input type="hidden" name="showReturnLink" value=""/>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr valign="right">
        <td align="right" id="designStatusLabel">Design:</td>
        <td align="left">
            <select class="filter" name="designStatus">
                <option value="designNotBegun" <?php echo $designNotBegunSelected; ?>>Not Begun/Submittted</option>
                <option value="designSubmitted" <?php echo $designSubmittedSelected; ?>>Submitted</option>
                <option value="designInProgress" <?php echo $designInProgressSelected; ?>>In Progress</option>
                <option value="designApprovedPlan" <?php echo $designApprovedPlanSelected; ?>>Approved Plan</option>
                <option value="designFulfilledDegree" <?php echo $designFulfilledDegreeSelected; ?>>Fulfilled Degree</option>
                <option value="designFulfilledOther" <?php echo $designFulfilledOtherSelected; ?>>Fulfilled Other</option> 
                <option value="allDesign" <?php echo $allDesignSelected; ?>>No Filter</option>
            </select>
        </td>
        <td align="center">
        <a href="" onClick="restoreDefaultPlaceoutFilter(); return false;">Restore Default Filter / List</a>
        </td>
    </tr>
    <tr valign="right">
        <td align="right"id="programmingStatusLabel">Programming:</td>
        <td align="left" colspan="2">
            <select class="filter" name="programmingStatus">
                <option value="programmingNotBegun" <?php echo $programmingNotBegunSelected; ?>>Not Begun/Submittted</option>
                <option value="programmingSubmitted" <?php echo $programmingSubmittedSelected; ?>>Submitted</option>
                <option value="programmingInProgress" <?php echo $programmingInProgressSelected; ?>>In Progress</option>
                <option value="programmingApprovedPlan" <?php echo $programmingApprovedPlanSelected; ?>>Approved Plan</option>
                <option value="programmingFulfilledDegree" <?php echo $programmingFulfilledDegreeSelected; ?>>Fulfilled Degree</option>
                <option value="programmingFulfilledOther" <?php echo $programmingFulfilledOtherSelected; ?>>Fulfilled Other</option> 
                <option value="allProgramming" <?php echo $allProgrammingSelected; ?>>No Filter</option>
            </select>
        </td>
    </tr>
    <!---   remove statistics
    <tr>
        <td align="right" id="statisticsStatusLabel">Statistics:</td>
        <td align="left" colspan="2">
            <select class="filter" name="statisticsStatus">
                <option value="statisticsNotBegun" <?php echo $statisticsNotBegunSelected; ?>>Not Begun/Submittted</option>
                <option value="statisticsSubmitted" <?php echo $statisticsSubmittedSelected; ?>>Submitted</option>
                <option value="statisticsInProgress" <?php echo $statisticsInProgressSelected; ?>>In Progress</option>
                <option value="statisticsApprovedPlan" <?php echo $statisticsApprovedPlanSelected; ?>>Approved Plan</option>
                <option value="statisticsFulfilledDegree" <?php echo $statisticsFulfilledDegreeSelected; ?>>Fulfilled Degree</option>
                <option value="statisticsFulfilledOther" <?php echo $statisticsFulfilledOtherSelected; ?>>Fulfilled Other</option> 
                <option value="allStatistics" <?php echo $allStatisticsSelected; ?>>No Filter</option>
            </select>
        </td>
    </tr>
    --->
    <tr>
        <td></td>
        <td align="left" colspan="2">
        <input type="submit" name="submitFilter" value="Filter / Refresh Full List"/>
        </td>
    </tr>
</table>
</form>

</div>
    </td>  
        </tr>

        <tr valign="bottom">
        
        <td align="left">
<br/>

<form id="jumpToForm" name="jumpToForm" action="<?php echo $returnUrl; ?>" method="GET">   
    <b>Jump to applicant:</b><br/>
    <input type="text" id="suggest" name="searchString" size="40"/>
    <input type="hidden" id="luuId" name="luuId" />
    <input type="hidden" id="jumpToUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="jumpToUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <?php
    if ($periodId) {        
        echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod" name="period" value="' . $periodId . '"/>';
    }
    if ($programId) {
        echo '<input type="hidden" id="jumpToProgramId" name="programId" value="' . $programId . '" />';    
    }
    ?>
    <input type="hidden" id="jumpToLimit" name="limit" value="<?php echo $limit; ?>"/>
    <input type="hidden" name="designStatus" value="<?php echo $designStatus; ?>"/>
    <input type="hidden" name="programmingStatus" value="<?php echo $programmingStatus; ?>"/>
  <!--- remove statistics
    <input type="hidden" name="statisticsStatus" value="<?php echo $statisticsStatus; ?>"/>
    --->
    <input type="hidden" name="showReturnLink" value="show"/> 
    <input type="submit" name="submitJumpTo" value="Go" />    
</form>
        </td>
    </tr>
</table>


<div style="text-align: right; float: right; margin-right: 5px; margin-bottom: 5px; padding: 0px;"> 
<?php
if ($unit == 'department' && $unitId == 18) {
 
    $programsView = new VW_Programs();
    $unitPrograms = $programsView->find($unit, $unitId);
    if (count($unitPrograms) > 1) {
    ?>

    <form id="programForm" name="programForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
        Limit to program: <select name="programId" onChange="document.programForm.submit();">
        <option value="">All <?php echo $unitName; ?> programs</option>
    <?php
        foreach ($unitPrograms as $program) {
            $selected = '';
            if ($program['program_id'] == $programId) {
                $selected = 'selected';
            }
            echo '<option value="' . $program['program_id'] . '" ' . $selected . '>' . $program['program_name'] . '</option>';    
        }
    ?>
        </select>
        <input type="hidden" id="programLimit" name="limit" value="<?php echo $limit; ?>" /> 
        <input type="hidden" id="limitUnit" name="unit" value="<?php echo $unit; ?>" />
        <input type="hidden" id="limitUnitId" name="unitId" value="<?php echo $unitId; ?>" />
        <?php
        if ($periodId) {        
            echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod" name="period" value="' . $periodId . '"/>';
        }
        ?>
        <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
        <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
        <input type="hidden" name="designStatus" value="<?php echo $designStatus; ?>"/>
        <input type="hidden" name="programmingStatus" value="<?php echo $programmingStatus; ?>"/>
        <!--- remove statistics
        <input type="hidden" name="statisticsStatus" value="<?php echo $statisticsStatus; ?>"/>
        --->
    </form>
<?php
    }
} 
?>
</div> 
<br/> 

<div style="clear:both; ">


<div style="text-align: left; float: left; width: 100%; margin-left: 5px; margin-bottom: 5px; padding: 0px;">
<?php echo $returnLink; ?>
</div>

<div class="paging">
<?php $dataGrid->render(DATAGRID_RENDER_PAGER); ?>
</div>

<div style="text-align: left; float: left; margin-left: 5px; margin-bottom: 5px; padding: 0px;">

<?php
// Display info about the record set being displayed
$recordCount = $dataGrid->getRecordCount(); 
if ($recordCount >= 1) {
    ?>
    <form id="pageSizeForm" name="pageSizeForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET"> 
    <?php
    $recordStart = $dataGrid->getCurrentRecordNumberStart();
    $recordEnd = $dataGrid->getCurrentRecordNumberEnd();
    echo "Record(s) <b>" . $recordStart . " - " . $recordEnd . " of " . $recordCount . "</b>"; 
    if ($searchString) {
        if (mb_detect_encoding($searchString) == 'UTF-8') {
            $displayString = utf8_decode($searchString);
        } else {
            $displayString = $searchString;
        }
        echo ' for <i>' . htmlentities($displayString) . "</i>";
    }
    // Menu for changing page size
    $limit100Selected = $limit200Selected = $limit500Selected = '';
    $limitSelected = 'limit' . $limit . 'Selected';
    $$limitSelected = "selected";
    ?>
    &nbsp;&nbsp;
    <!--
    <select name="limit" onChange="document.pageSizeForm.submit(); return false;">
        <option value="100" <?php echo $limit100Selected; ?>>100 per page</option>
        <option value="200" <?php echo $limit200Selected; ?>>200 per page</option>
        <option value="500"<?php echo $limit500Selected; ?>>500 per page</option>
    </select>
    -->
    <input type="hidden" id="limitUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="limitUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <?php
    if ($periodId) {      
        // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" id="limitPeriod" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" id="limitPeriod" name="period" value="' . $periodId . '"/>';     
        } 
    }
    if ($programId) {
        echo '<input type="hidden" id="limitProgramId" name="programId" value="' . $programId . '" />';    
    }
    ?>
    <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
    <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
    <input type="hidden" name="submissionStatus" value="<?php echo $submissionStatus; ?>"/>
    <input type="hidden" name="completionStatus" value="<?php echo $completionStatus; ?>"/>
    <input type="hidden" name="paymentStatus" value="<?php echo $paymentStatus; ?>"/>
    <input type="hidden" name="testScoreStatus" value="<?php echo $testScoreStatus; ?>"/>
    <input type="hidden" name="transcriptStatus" value="<?php echo $transcriptStatus; ?>"/>
    <input type="hidden" name="recommendationStatus" value="<?php echo $recommendationStatus; ?>"/>
    <input type="hidden" name="showReturnLink" value="<?php echo $showReturnLink; ?>"/>
    </form>
    <?php
         
} elseif ($recordCount == 0) {
    
    if ($searchString) {
        
        echo '<b>No matches found for ';
        echo  '<i>'. htmlentities( utf8_decode($searchString) ) . '</i></b>';   
    
    } else {
        echo '<b>No applications found for this period with these filter settings.</b>';
    }
}
?>
</div>



<br />
<div style="clear: left; background-color: #FFFFFF;">
<?php $dataGrid->render(); ?>
</div>

<div class="paging">
<?php $dataGrid->render(DATAGRID_RENDER_PAGER); ?>
</div>

</div>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>