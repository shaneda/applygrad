<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<?

$deptId = -1;
$cellWidth = 300;

if(isset($_GET['id']))
{
	$deptId = intval($_GET['id']);
}




?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>SCS Applygrad</title>

</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
<span class="tblItem">
<?

		$sql = "select 
		distinct
		firstname, lastname, email, address_cur_street1, gender, 
		address_cur_street2, 
		address_cur_street3, 
		address_cur_street4, 
		address_cur_city, 
		states.abbrev as state, 
		address_cur_pcode, 
		countries.name as country,
                address_cur_tel,
		GROUP_CONCAT(concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
		lu_application_programs.faccontact,
		lu_application_programs.stucontact
		from  lu_users_usertypes 
		inner join users on users.id =  lu_users_usertypes.user_id
		inner join users_info on users_info.user_id= lu_users_usertypes.id
		left outer join countries on countries.id = address_cur_country
		left outer join states on states.id = address_cur_state
		left outer join application on application.user_id = lu_users_usertypes.id
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		
		where lu_application_programs.admission_status = 2
		and lu_programs_departments.department_id=".$deptId . "
		group by  application.id
		order by lastname, firstname
		";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		$count = 0;
                
                $ups_headers = "Attention^Address1^Address2^Address3^Address4^City^State_Prov^PostalCode^Country^Telephone\n";
                $filename = "UPSLABEL.txt";
                $handle = fopen($filename, 'w+');
                fwrite($handle, $ups_headers);

		?><table cellspacing=4 cellpadding=4 ><tr><?
		while($row = mysql_fetch_array( $result )) 
		{
			$gender = "";
			$program = $row['program'];
			
			$aPrograms = split(",", $program);
			if(count($aPrograms) > 1)
			{
				$program = "";
				for($i = 0; $i < count($aPrograms); $i++)
				{
					if($i == 0)
					{
						$program .= $aPrograms[$i] . " program";
					}else
					{
						if($i == count($aPrograms)-1)
						{
							$program .= " and the ". $aPrograms[$i]. " program";
						}else
						{
							$program .= ", the ". $aPrograms[$i]. " program";
						}
						
					}
				}
			}
			if($row['gender'] == "M")
			{
				$gender = "Mr. ";
			}
			if($row['gender'] == "F")
			{
				$gender = "Ms. ";
			}
			$address = ucwords(strtolower($row['address_cur_street1'])). "<br>";
			if($row['address_cur_street2'] != "")
			{
				$address .= ucwords(strtolower($row['address_cur_street2'])). "<br>";
			}
			if($row['address_cur_street3'] != "")
			{
				$address .= ucwords(strtolower($row['address_cur_street3'])). "<br>";
			}
			if($row['address_cur_street4'] != "")
			{
				$address .= ucwords(strtolower($row['address_cur_street4'])). "<br>";
			}
			$address .= $row['address_cur_city'];
			if($row['state'] != "")
			{
				$address .= ", ". $row['state']. " ";
			}
			if($row['address_cur_pcode'] != "")
			{
				$address .= " ". $row['address_cur_pcode']. "";
			}
                        $address .= "<br>";
			$address .= ucwords(strtolower($row['country']));

			if($count % 6 == 0)
			{
				echo "</td ></tr></table><table cellspacing=4 cellpadding=4><tr>";
			}
			?><td valign=top width=<?=$cellWidth?>><?
			echo $gender.ucwords(strtolower($row['firstname'])). " ". ucwords(strtolower($row['lastname']))."<br>";
			echo $address;
			?></td><?
			$count++;
                        // Write shipping info for UPS
                        $dataline = $gender.ucwords(strtolower($row['firstname'])).' '
                                  . ucwords(strtolower($row['lastname'])).'^'
                                  . ucwords(strtolower($row['address_cur_street1'])).'^'
                                  . ucwords(strtolower($row['address_cur_street2'])).'^'
                                  . ucwords(strtolower($row['address_cur_street3'])).'^'
                                  . ucwords(strtolower($row['address_cur_street4'])).'^'
                                  . $row['address_cur_city'].'^'
                                  . $row['state']. '^'
                                  . $row['address_cur_pcode']. '^'
                                  . ucwords(strtolower($row['country'])). '^'
                                  . $row['address_cur_tel']. "\n";

                        fwrite($handle, $dataline);

		}
                fclose($handle);
?>
</tr></table>
</span>
</body>
<A HREF="./UPSLABEL.txt">Text File for UPS Shipping Label</A>
</html>
