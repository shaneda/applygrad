 <?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
include '../inc/specialCasesAdmin.inc.php';

$allowUser = true;
$departmentName = "";
$s = -1;
$sType = "(all)";

$id = -1;
if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
}

// PLB added periodId 11/24/09
$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
}


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$isIsreeDepartment = isIsreeDepartment($id);
if ($isIsreeDepartment) {
    $pageTitle = 'Registered Students';    
} else {
    $pageTitle = 'Payments';    
}


$pageCssFiles = array(
    '../css/applicantGroups_assign.css' 
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

// period
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    if ($periodName) {
        $applicationPeriodDisplay = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay = $periodDates;     
    }
}
?> 
<div id="pageHeading">
<div id="departmentName">
<?php
echo $departmentName;
if ($applicationPeriodDisplay) {
    echo '<br/>' . $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle"><?php echo $pageTitle; ?></div>
</div> 

<form accept-charset="utf-8" id="form1" name="form1" action="" method="post"> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">

	<?
	include '../inc/C_Spreadsheet_01a.php.inc';
	$spst = new Spreadsheet;
   
    /*
    * NOTE: for this #!%$&@!!@ spreadsheet business to work, 
    * the first column must be the user_id and the second column 
    * must be the application id. NO EXCEPTIONS. 
    */
    $sql = "SELECT 
    luu.id as id,
    p1.application_id AS application_id, u.lastname AS lastname, u.firstname as firstname, u.email as email, p1.payment_intent_date as payment_intent_date, p1.payment_status as payment_status
    from  payment p1
    left join payment p2 on (p1.application_id = p2.application_id
    AND p1.payment_intent_date < p2.payment_intent_date)
    inner join period_application pa on pa.application_id = p1.application_id
    and pa.period_id = " . $periodId .
    " inner join application a on a.id = p1.application_id
    and not(a.paid or a.waive)
    inner join lu_users_usertypes luu on luu.id = a.user_id
    inner join users u on u.id = luu.user_id
    where p2.payment_intent_date IS NULL
    AND p1.payment_status = 'pending'";
    
	$spst->sql = $sql;
	$spst->sqlOrderBy = "payment_intent_date";
    $spst->showFilter = false;
    
    if ($_SESSION['A_usertypeid'] == 11)  {
	    // Student contacts should not be getting a link to this page, but if
        // they do end up here, they should get a link to the minimal application summary. 
        $spst->editPage = "userroleEdit_student_formatted.php";
    } else {
      //  $spst->editPage = "../review/das_test_print.php";
    }
           
    $spst->doSpreadSheet();
?>

	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>
