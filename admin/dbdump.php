<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" --> 
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session_admin.php'; ?> 
<? include_once '../inc/db_connect.php'; ?> 
<? include_once '../inc/functions.php'; ?> 

<head> <?
echo $db."<BR>";

global $app_list;

global $states;
$states = array();

global $visatypes;
$visatypes = array();

global $ethnicity;
$ethnicity = array();

$app_list = array();

function create_dump_file( $filename )
{
  //  echo "create_dump_file\n<BR>";

  $handle = fopen($filename, 'w+');
  return $handle;
}

function close_dump_file( $fid )
{
  //  echo "close_dump_file\n<BR>";
  fclose($fid);
}

function get_applicant_list( $dept_code ) {
  $list = array();

  return $list;
}

function print_header_labels($fid)
{
$headerline =  'SPACER^appid^'
        . ' email^title^firstname^middlename^lastname^'
        . ' gender^dob^'
        . ' address_cur_street1^address_cur_street2^address_cur_street3^address_cur_street4^'
        . ' address_cur_city^CurrentState^address_cur_pcode^CurrentCountry^'
        . ' address_cur_tel^address_cur_tel_mobile^address_cur_tel_work^'
        . ' address_perm_street1^address_perm_street2^address_perm_street3^address_perm_street4^'
        . ' address_perm_city^PermState^address_perm_pcode^PermCountry^'
        . ' address_perm_tel^address_perm_tel_mobile^address_perm_tel_work^homepage^'
        . ' CountryofCitz^iso_code^visa status^ethnicity^'
        . ' PIER^WomenAtITFellowship^'

        . 'SPACER^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'

        . 'SPACER^'
        . 'gre_testdate^verbalscore^verbalpercentile^quantitativescore^quantitativepercentile^analyticalscore^'
        . 'analyticalpercentile^analyticalwritingscore^analyticalwritingpercentile^ '

        . 'SPACER^'
        . 'gresubject_testdate^name^score^percentile^ '

        . 'SPACER^'
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^ '
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^ '  
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^ '

        . 'SPACER^'
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        

        . 'SPACER^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'

  . 'SPACER^';

 fwrite($fid, $headerline);
 fwrite($fid, "\n");

}

function get_names($deptid, $fid)
{
  //echo "func get_name\n<br>";

  $num = 0;
  $states = array();
  $visatypes = array();
  $ethnicity = array();
  $yes_no = array("no","yes");

  $sqlA = 'select id, name from states order by id';
  $resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
  while($rowA = mysql_fetch_array( $resultA ))
    { 
      array_push($states, $rowA[1]);
    }


  $sqlA = 'select id, name from visatypes order by id';
  array_push($visatypes, " ");

  $resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
  while($rowA = mysql_fetch_array( $resultA ))
    { 
      array_push($visatypes, $rowA[1]);
    }

  $sqlA = 'select id, name from ethnicity  order by id';
  array_push($ethnicity, " ");

  $resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
  while($rowA = mysql_fetch_array( $resultA ))
    { 
      array_push($ethnicity, $rowA[1]);
    }


$sql3 = 'select distinct '
       . ' count(B.id) as cnt, B.id, B.user_id, C.admission_status '
       . ' from '
       . ' application as B join '
       . ' lu_application_programs as C, '
       . ' lu_programs_departments as D '
       . ' where '
       . ' B.submitted = 1 and'
       . ' B.id = C.application_id and'
       . ' C.program_id = D.program_id and'
       . ' D.department_id = '.$deptid.' '
  . ' group by B.id';

 $result3 = mysql_query($sql3) or die(mysql_error(). $sql3);
 while($row3 = mysql_fetch_array( $result3 ))
   {
     // echo $row3[0]." ".$row3[1]."<br> ";
     $appid = $row3[1];
     $user_id = $row3[2];
     $admit_status = $row3[3];

$sql = 'select '
        . ' B.id, G.id, G.user_id, F.email, F.title, F.firstname, F.middlename, F.lastname, '
        . ' G.gender, G.dob, '
        . ' G.address_cur_street1, G.address_cur_street2, G.address_cur_street3, G.address_cur_street4, '
        . ' G.address_cur_city, P.name as CurrentState, G.address_cur_pcode, N.name as CurrentCountry, '
        . ' G.address_cur_tel, G.address_cur_tel_mobile, G.address_cur_tel_work,'
        . ' G.address_perm_street1, G.address_perm_street2, G.address_perm_street3, G.address_perm_street4, '
        . ' G.address_perm_city, Q.name as PermState, G.address_perm_pcode, O.name as PermCountry, '
        . ' G.address_perm_tel, G.address_perm_tel_mobile, G.address_perm_tel_work, G.homepage,'
        . ' J.name as CountryofCitz, J.iso_code, G.visastatus, G.ethnicity,'
        . ' B.pier, B.womenfellowship '
        . ' from '
        . ' application as B Join '
        . ' lu_users_usertypes as E,'
        . ' users as F,'
        . ' users_info as G,'
        . ' countries as J,'
        . ' countries as N,'
        . ' countries as O,'
        . ' states as P,'
        . ' states as Q'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id = '.$appid.' and'
        . ' B.user_id=E.id and'
        . ' E.user_id =F.id and'
        . ' E.usertype_id=5 and'
        . ' G.user_id = E.id and'
        . ' G.cit_country = J.id and'
        . ' G.address_cur_country = N.id and'
        . ' G.address_perm_country = O.id and'
        . ' G.address_cur_state = P.id and'
        . ' G.address_perm_state = Q.id '
  . ' Order by B.id';

 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   {
     // echo $row[0]."<BR>";

     fwrite($fid, "**BEGIN**^");
     fwrite($fid, $appid );
     fwrite($fid, "^" );
     $luuid=0;
     $uid=0;

     for ($i=3; $i<39; $i++){
       switch( $i) {
       case 35: //VISA STATUS
	 fwrite($fid, $visatypes[$row[$i]]);
	 break;
       case 36: //Ethinicty
	 if ( $row[$i] >= 0 ) {
	   fwrite($fid, $ethnicity[$row[$i]]);
	 } else {
	   fwrite($fid, " ");
	 }
	 break;
       case 37: //PIER
       case 38: //WomenAtITFellowship
	 fwrite($fid, $yes_no[$row[$i]]);
	 break;
       default:
	 fwrite($fid, trim($row[$i]));
       }
       fwrite($fid, " ^");
     }


     fwrite($fid, "**END...BEGIN**^");
     get_programs($appid, 0, $fid);           
     fwrite($fid, "**END...BEGIN**^");
     get_gre($appid, $luuid, $uid, $fid);           
     fwrite($fid, "**END...BEGIN**^");
     get_gresubject($appid, $luuid, $uid, $fid);           
     fwrite($fid, "**END...BEGIN**^");
     get_TOEFLS($appid, $luuid, $uid, $fid);           
     fwrite($fid, "**END...BEGIN**^");
     get_rec($appid, $luuid, $uid, $fid);           
     fwrite($fid, "**END...BEGIN**^");
     get_edu($appid, $luuid, $uid, $fid);           
     fwrite($fid, "**END**^");

     fwrite($fid, "\n");
     $num++;
   }
          
   }
 return $num;


}

function get_programs($appid, $deptid, $fid) { // echo "func get_programs\n<br>";

$sql = 'select CONCAT(L.name, " ", K.linkword, " ", M.name) as program_name, C.id, C.admission_status'
        . ' from '
        . ' application as B join'
        . ' lu_application_programs as C,'
        . ' lu_programs_departments as D,'
        . ' programs as K,'
        . ' degree as L,'
        . ' fieldsofstudy as M'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id='.$appid.' and'
        . ' B.id=C.application_id and'
  . ' C.program_id=D.program_id and';

 if ( $deptid > 0 ) {
   $sql .= ' D.department_id='.$deptid.' and ';
 }

 $sql .= ' K.id = D.program_id and'
        . ' L.id = K.degree_id and '
        . ' M.id = K.fieldofstudy_id'
   . ' Order by C.choice';

 $num_of_inst = 4;
 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   { 
           
     if ($num_of_inst > 0) {
       for ($i=0; $i<1; $i++){
	 fwrite($fid, $row[$i]);
	 fwrite($fid, "^");
	 $progid=$row[1];
	 // echo "PROGID:".$progid."<BR>";
	 get_program_aoi($progid, $fid);
       }
       // Admission Decision for this program
       fwrite($fid, $row[2]);
       fwrite($fid, "^");
     }
     $num_of_inst--;
   }

 for ($i=$num_of_inst; $i>0; $i--) { // echo " F".$i." ";
   fwrite($fid, " ^^^^^" );
 }



}

function get_program_aoi($progid, $fid)
{
  // echo "func get_program_aoi\n<br>";

$sql = 'select I . name '
        . ' from '
        . ' lu_application_interest as D , '
        . ' interest as I '
        . ' where '
        . ' D.app_program_id='.$progid.' and'
  . ' D.interest_id = I.id Order By D.choice';
    
         
 $num_of_inst=3;
 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   { 
     for ($i=0; $i<1; $i++){
       fwrite($fid, $row[$i]);
       fwrite($fid, "^");
     }
     $num_of_inst--;
   }

 for ($i=$num_of_inst; $i>0; $i--) { // echo " F".$i." ";
   fwrite($fid, " ^" );
 }

}


function get_experience($uid, $aid,  $fid) { // echo "func get_experience\n<br>"; 
}


  function get_gre($appid, $luuid, $uid, $fid) { // echo "func get_gre\n<br>"; 
   $sql = 'select G.testdate , G.verbalscore , G.verbalpercentile , G.quantitativescore , G.quantitativepercentile , G.analyticalscore , '
        . ' G.analyticalpercentile , G.analyticalwritingscore , G.analyticalwritingpercentile '
        . ' from '
        . ' application as B join '
        . ' grescore as G '
        . ' where '
        . ' B . submitted = 1 and '
        . ' B.id=' . $appid.' and'
        . ' B.id = G.application_id '
      . ' Order by B.id';
        
    $gre_score_missing=1;
    $result = mysql_query($sql) or die(mysql_error(). $sql);
    while($row = mysql_fetch_array( $result ))
      { 
	$gre_score_missing=0;
	for ($i=0; $i<9; $i++){
	  fwrite($fid, $row[$i]);
	  fwrite($fid, "^");
	}
      }
    if ($gre_score_missing == 1) {
      fwrite($fid, "^^^^^^^^^");
    }

  }

  function get_gresubject($appid, $luuid, $uid, $fid) { // echo "func get_gresubject\n<br>";

$sql = 'select G.testdate , G.name , G.score , G.percentile '
        . ' from '
        . ' application as B join '
        . ' gresubjectscore as G '
        . ' where '
        . ' B.submitted = 1 and '
        . ' B.id=' . $appid.' and'
        . ' B.id = G.application_id '
  . ' Order by B.id';

 $subject_score_missing = 1;
 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   { 
     for ($i=0; $i<4; $i++){
       fwrite($fid, $row[$i]);
       fwrite($fid, "^");
     }
     $subject_score_missing = 0;
   }
 if ($subject_score_missing == 1) {
   fwrite($fid, " ^ ^ ^ ^");
 }
  }

  function get_TOEFLS($appid, $luuid, $uid, $fid) { // echo "func get_TOEFLS\n<br>";
       $sql = 'select T.type , T.testdate , T.section1 , T.section2 , T.section3 , T.essay , T.total , B.id '
        . ' from '
        . ' application as B join '
        . ' toefl as T '
        . ' where '
        . ' B.submitted = 1 and '
        . ' B.id ='.$appid.' and'
        . ' B.id = T.application_id '
	 . ' Order by B.id , T.type';

       $toefl_score_missing=1;
       $result = mysql_query($sql) or die(mysql_error(). $sql);
       while($row = mysql_fetch_array( $result ))
	 { 
           for ($i=0; $i<7; $i++){
             fwrite($fid, $row[$i]);
             fwrite($fid, "^");
           }
           $toefl_score_missing=0;
	 }
       if ($toefl_score_missing == 1) {
	 fwrite($fid, "^^^^^^^ ^^^^^^^ ^^^^^^^");
       }

  }

  function get_edu($appid, $luuid, $uid, $fid) { // echo "func get_edu\n<br>";

    $edu_type = array("undefined","ugrad", "grad", "additional" );

$sql = 'select'
        . ' C.name as UnivName, I.date_entered, I.date_grad, I.date_left, D.name as Degree, I.major1, I.major2, I.minor1, I.minor2, I.gpa, I.gpa_major, G.name as GPAScale, I.educationtype, I.user_id'
        . ' from '
        . ' application as B join'
        . ' usersinst as I,'
        . ' institutes as C,'
        . ' degreesall as D,'
        . ' gpascales as G'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id='.$appid.' and'
        . ' B.user_id=I.user_id and'
        . ' I.institute_id = C.id and'
        . ' I.degree = D.id and'
        . ' I.gpa_scale = G.id'
  . ' Order by I.educationtype';

 
 $num_of_inst = 5;
 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   { 
     if ($num_of_inst > 0) {
       for ($i=0; $i<13; $i++){
	 if ($i == 12) {
	   fwrite($fid, $edu_type[$row[$i]]);
	 } else {
	   fwrite($fid, $row[$i]);
	 }
	 fwrite($fid, "^");
       }
     }
     $num_of_inst--;
   }

 for ($i=$num_of_inst; $i>0; $i--) { // echo " F".$i." ";
   fwrite($fid, " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^" );
 }

  }

  function get_rec($appid, $luuid, $uid, $fid) { // echo "func get_rec\n<br>";

    $yes_no = array("no","yes");

$sql = 'select'
        . ' U.title, CONCAT(U.firstname, " ", U.lastname) as RecName, U.email, UI.company, UI.address_cur_tel, B.buckleywaive, B.id'
        . ' from '
        . ' application as B join'
        . ' recommend as R,'
        . ' lu_users_usertypes as LUU,'
        . ' users as U,'
        . ' users_info as UI'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id='.$appid.' and'
        . ' B.id=R.application_id and'
        . ' R.rec_user_id = LUU.id and'
        . ' LUU.user_id = U.id and'
        . ' LUU.id = UI.user_id'
  . ' Order by B.id';
        
 $num_of_recs = 5;
 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   { 
     if ($num_of_recs > 0) {
       for ($i=0; $i<6; $i++){

	 if ( $i == 5 ) {
	   fwrite($fid, $yes_no[$row[$i]]);
	 } else {
	   fwrite($fid, trim($row[$i]));
	 }
	 fwrite($fid, "^");
       }
     }
     $num_of_recs--;
   }

 // Fill for the missing recs
 // echo "FILL: ".$num_of_recs;
 for ($i=$num_of_recs; $i>0; $i--) { // echo " F".$i." ";
   fwrite($fid, " ^ ^ ^ ^ ^ ^" );
 }
 // echo "FILL: DONE<BR>";
  }

  function get_person_name($userid, $fid)
  {
   $sql = ' SELECT '
        . ' U.title, U.firstname, U.middlename, U.lastname, U.email'
        . ' FROM '
        . ' users as U, '
        . ' lu_users_usertypes as LUU '
        . ' WHERE '
        . ' LUU.id = '.$userid .' and '
     . ' U.id = LUU.user_id ';

   $result = mysql_query($sql) or die(mysql_error(). $sql);
   $row = mysql_fetch_array( $result );
   for ($i=0; $i<5; $i++){
     fwrite($fid, $row[$i]);
     fwrite($fid, "^");
   }
  }

  function get_scores($deptid, $fid)
  {
    // echo "func get_scores\n<br>";
    $records_written = 0;
    $yes_no = array("no","yes");

 $header = ' SPACER ^' 
        . 'Title^Firstname^Middlename^Lastnam^email^'
        . 'PIER^WomenAtITFellowship^'
        . ' SPACER ^'
        . 'user_id^application_id^reviewer_id^roleID^rev_firstname^rev_lastname^'
        . 'Role^CommentGrades^CommentSOP^Comments^Score(PHD)^'
        . 'Certainty(PHD)^Score(MS)^Certainty(MS)^Round2Advance^admit_vote^'
        . 'Recruited^Grad_name^Pertinent_info^Advise_time?^Commit_money?^'
        . 'Fund_source?^ReviewRound^Interview^Brilliance^Other_interest^'
        . 'fac_vote^Top5Rank^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . ' SPACER ^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'

   . ' END';


 fwrite($fid, $header);
 fwrite($fid, "\n");

$sql = 'SELECT'
        . ' A.pier, A.womenfellowship,'
        . ' A.user_id, R.application_id, R.reviewer_id, UT.id, U.firstname, U.lastname,'
        . ' UT.name, R.grades, R.statement, R.comments, R.point,'
        . ' R.point_certainty, R.point2, R.point_certainty, R.round2, R.admit_vote,'
        . ' R.recruited, R.grad_name, R.pertinent_info, R.advise_time, R.commit_money,'
        . ' R.fund_source, R.round, R.interview, R.brilliance, R.other_interest,'
        . ' R.fac_vote, R.rank'
        . ' FROM'
        . ' review as R ,'
        . ' lu_user_department as LUUD,'
        . ' application as A,'
        . ' lu_users_usertypes as LUUU,'
        . ' users as U,'
        . ' usertypes as UT'
        . ' WHERE'
        . ' R.reviewer_id = LUUD.user_id and'
        . ' LUUD.department_id='.$deptid.' and'
        . ' A.id = R.application_id and'
        . ' LUUU.id=R.reviewer_id and'
        . ' LUUU.user_id=U.id and'
        . ' UT.id = LUUU.usertype_id and'
        . ' R.supplemental_review is NULL'
        . ' ORDER BY R.application_id'
  . ' ';

 $result = mysql_query($sql) or die(mysql_error(). $sql);
 while($row = mysql_fetch_array( $result ))
   {
     // echo $row3[0]." ".$row[1]."<br> ";
     $userid = $row[2];
     $appid = $row[3];
     $revr_id = $row[4];
     $revr_type = $row[5];

     fwrite($fid, "**BEGIN**^");
     // Get CANDIDATE NAME
     get_person_name($userid, $fid);
     fwrite($fid, $yes_no[$row[0]]); // PIER
     fwrite($fid, "^");
     fwrite($fid, $yes_no[$row[1]]); // Women at IT   
     fwrite($fid, "^");

     fwrite($fid, "**END...BEGIN**^");

     // WRITE  REVIEW SCORES
     for ($i=2; $i<30; $i++){
       // STRIP NEWLINEs
       switch($i) {
       case 22: // Wiling to commit money?
       case 21: // Willing to advise?
       case 16: // Round 2 Advance?
	 if ($row[$i] <> "" ) {
	   fwrite($fid, $yes_no[$row[$i]]);
	 }
	 break;
       default:
	 $tmpstring = str_ireplace("\n", "", $row[$i] );
	 $clean_string = str_ireplace("\r", "", $tmpstring );
	 fwrite($fid, $clean_string);
       }
       fwrite($fid, "^");
     }

     // If Faculty reviewer write out multple records
     // Could have up to 6 scores
     // 2 programs per review applications 
     // only have 2 scores possible (phd and ms)
     $max_scores=6;
     if ($revr_type == 3) {
              $sql2 = ' SELECT '
                    . ' I.name, R.point, R.point2'
                    . ' FROM '
                    . ' review as R,'
                    . ' interest as I '
                    . ' WHERE '
                    . ' R.application_id='.$appid.' AND '
                    . ' R.reviewer_id='.$revr_id.' AND '
                    . ' R.supplemental_review > 0 AND '
		. ' I.id = R.supplemental_review ';
	      $result2 = mysql_query($sql2) or die(mysql_error(). $sql2);
	      while($row2 = mysql_fetch_array( $result2 ))
		{  

		  for ($i=0; $i<3; $i++){
		    fwrite($fid, $row2[$i]);
		    fwrite($fid, "^");
		  }
		  $max_scores--;
		}
     }
     // pad the remaining AOI place holders
     for ($i=$max_scores; $i>0; $i--) {
       fwrite($fid, "^^^");
     }


     //  GET PROGRAM APPLIED TOO
     fwrite($fid, "**END...BEGIN**^");
     get_programs( $appid, $deptid, $fid);
     fwrite($fid, "**END**");          
     fwrite($fid, "\n");
     $records_written++;
   }

 return $records_written;
  }







?>

</head>
<body>

<?

    echo "Starting data dump for Online Admission System system<BR><BR>"; 

   $status_fid = create_dump_file("DUMPSTATUS.txt");

  // Main
  $programs_list = array ( 
			  array(44, "COMPBIO_ARCHIVE")
			   );

  $count=1;
  for ($i=0; $i<$count; $i++){

    $deptid=$programs_list[$i][0];


    $filename = $programs_list[$i][1]."_REVIEWDUMP.txt";
    $fid = create_dump_file($filename);
    $num = get_scores( $deptid, $fid);
    close_dump_file($fid);
  $status = 'Dumping '.$programs_list[$i][1].' adm review data to file: <A HREF="https://rogue.fac.cs.cmu.edu/htdocs/_dev/admin/'
    .$filename.'" TARGET=_blank>'.$filename.'</A>....Num Records: '.$num.'<br>';
  echo $status;
  fwrite($status_fid, $status);
  fwrite($status_fid, "\n");

  $filename = $programs_list[$i][1]."_DATADUMP.txt";
  $fid = create_dump_file($filename);
  print_header_labels($fid);
  $num = get_names( $deptid, $fid);
  close_dump_file($fid);
  $status = 'Dumping '.$programs_list[$i][1].' application data to file: <A HREF="https://rogue.fac.cs.cmu.edu/htdocs/_dev/admin/'
    .$filename.'" TARGET=_blank>'.$filename.'</A>....Num Records: '.$num.'<br>';
  echo $status."<BR>";
  fwrite($status_fid, $status);
  fwrite($status_fid, "\n");
  }

  close_dump_file($status_fid);

?>
</body>
</html>
