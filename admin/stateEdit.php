<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$countries = array();
$name = "";
$abbrev = "";
$country = -1;
$id = -1;

$sql = "SELECT id,name FROM countries order by name";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($countries, $arr);
}

if(isset($_POST['txtschoolId']))
{
	$id = htmlspecialchars($_POST['txtschoolId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$abbrev = htmlspecialchars($_POST['txtAbbrev']);
	if(isset($_POST['lbCountry']))
	{
		$country = $_POST['lbCountry'];
	}
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}

	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update states set name='".$name."', abbrev='".$abbrev."', country_id=".$country." where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			echo $sql . "<br>";
		}
		else
		{
			$sql = "insert into states(name, abbrev, country_id)values('".$name."', '".$abbrev."', ".$country.")";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}
		header("Location: states.php");
	}
}
$sql = "SELECT id,name, abbrev, country_id FROM states where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$abbrev = $row['abbrev'];
	$country = $row['country_id'];
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit State';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit State</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtstatesId" type="hidden" id="txtschoolId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>        </td>
      </tr>
      <tr>
        <td align="right"><strong>Abbreviation:</strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($abbrev, "textbox", "txtAbbrev", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>Country:<br />
          <a href="countryEdit.php">Add new</a></strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($country, "listbox", "lbCountry", $_SESSION['A_allow_admin_edit'], true, $countries); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>