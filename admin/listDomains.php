<?php
include '../inc/config.php';
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';  
include '../classes/class.Unit.php';
include '../classes/class.Period.php';

$applyweb = new DB_Applyweb();

$domainQuery = "SELECT domain.*, domain_unit.unit_id 
            FROM domain
            LEFT JOIN domain_unit ON domain.id = domain_unit.domain_id 
            WHERE domain.name 
            NOT LIKE '%archive%'";
$domains = $applyweb->handleSelectQuery($domainQuery, 'id');

//DebugBreak();
echo '<h2>Domains/Programs and Their New Units/Periods</h2>';
$bgcolor = '#ffffff';
foreach ($domains as $domain) {
    
    $domainId = $domain['id'];
    $domainUnitId = $domain['unit_id'];
    $domainUnit = new Unit($domainUnitId);
    
    echo '<ul style="background-color: ' . $bgcolor . '">';
    echo '<li><a name="' . $domainId . '"><b>' . $domain['name'] . '</b> (domain id: ' . $domainId . ')</a>';
    if ($domainUnitId) {
        echo '<br/><i>corresponds to unit: ' . $domainUnit->getName();
        echo ' (<a href="listUnits.php#' . $domainUnitId . '">unit id: ' . $domainUnitId . '</a>)</i>';  
    }
    
    $departmentQuery = "SELECT department.* 
                        FROM department
                        INNER JOIN lu_domain_department ON department.id = lu_domain_department.department_id
                        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
                        WHERE lu_domain_department.domain_id = " . $domainId ."
                        ORDER BY department.id";
    $departments = $applyweb->handleSelectQuery($departmentQuery, 'id');
    if ( count($departments) > 0) {
        echo '<br/><br/><i>has department(s)</i>:<br/><br/>';
        foreach ($departments as $department) {
            $departmentId = $department['id'];
            echo '<ul><li><b>' . $department['name'] . '</b> (department id: ' . $departmentId . ')';
            
            $programsQuery = "SELECT programs.id, CONCAT( degree.name, ' ', linkword, ' ', fieldsofstudy.name ) AS name,
                            unit.unit_id, unit.unit_name
                            FROM programs
                            INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = programs.id
                            INNER JOIN degree ON programs.degree_id = degree.id
                            INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
                            LEFT OUTER JOIN programs_unit ON programs.id = programs_unit.programs_id
                            LEFT OUTER JOIN unit ON programs_unit.unit_id = unit.unit_id
                            WHERE lu_programs_departments.department_id = " . $departmentId . "
                            ORDER BY programs.id";
            $programs = $applyweb->handleSelectQuery($programsQuery, 'id');
            if ( count($programs) > 0) {
                echo '<br/><br/><i>has program(s)</i>:<br/><br/><ul>';
                foreach($programs as $program) {
                    $programId = $program['id'];
                    $programUnitId = $program['unit_id'];
                    echo '<li><b>' . $program['name'] . '</b> (program id: ' . $programId . ')'; 
                    if ($programUnitId) {
                        echo '<br/><i>corresponds to unit: ' . $program['unit_name'];
                        echo ' (<a href="listUnits.php#' . $programUnitId . '">unit id: ' . $programUnitId . '</a>)</i><br/><br/>';
                    }
                    echo '</li>';  
                }
                echo '</ul>';
            }
            
            echo '</li></ul>';
        }
                
    }

    
    $domainPeriods = $domainUnit->getPeriods();
    $display = '';
    if ( count($domainPeriods) > 0) {
        $display .= '<br/><i>has period(s)</i>:<br/><br/>';
        $display .= '<ul>';
        foreach ($domainPeriods as $period) {
            $display .= displayPeriod($period);
            /*
            $startTimestamp = strtotime( $period->getStartDate() );
            $startDate = date( 'm/d/Y',  $startTimestamp);
            $endTimestamp = strtotime( $period->getEndDate() );
            $endDate = date( 'm/d/Y',  $endTimestamp);
            $currentTimestamp = time();
            $activePeriodStyle = '';
            if ($currentTimestamp > $startTimestamp && $currentTimestamp < $endTimestamp) {
                $activePeriodStyle = 'style="border: 2px solid red;"';
            }
            $display .= '<li ' . $activePeriodStyle. '><b>'; 
            $display .= $startDate . ' - ' . $endDate;
            $display .= '</b> (period id: ' . $period->getId() . ', <a href="listUnits.php#' . $domainUnitId . '">unit id: ' . $domainUnitId .  '</a>)';
            $display .= '<br/><i>includes program(s)</i>:<ul>';
            foreach ($period->getPrograms() as $programId => $program) {
                $display .= '<li>' . $program . ' (unit id: ' . $programId .')</li>';    
            }
            $display .= '</ul><br/></li>';
            */    
        }
        $display .= '</ul>';
    }
    echo $display;
    
    echo '</li></ul>';
    //echo '<hr/>';
    
    if ($bgcolor == '#ffffff') {
        $bgcolor = '#dddddd';
    } else {
        $bgcolor = '#ffffff';
    }
}


function displayDescendants($unit) {

    global $applyweb;
    $unitId = $unit->getId();
    
    $display = '<ul>';
    $display .= '<li><a name="' . $unitId . '">' . $unit->getName() . ' (unit id: ' . $unitId . ')</a>';
    
    $domainQuery = "SELECT domain.id, domain.name 
                    FROM domain_unit
                    INNER JOIN domain ON domain_unit.domain_id = domain.id
                    WHERE domain_unit.unit_id = " . $unitId;
    $domainResult = $applyweb->handleSelectQuery($domainQuery);
    foreach ($domainResult as $domainRecord) {
        $display .= '<br/><i>corresponds to domain: ' . $domainRecord['name'] . ' (domain id: ' . $domainRecord['id'] . ')</i>';
    }
    
    $programQuery = "SELECT programs.id, CONCAT( degree.name, ' ', linkword, ' ', fieldsofstudy.name ) AS name
                        FROM programs_unit
                        INNER JOIN programs ON programs_unit.programs_id = programs.id
                        INNER JOIN degree ON programs.degree_id = degree.id
                        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
                        WHERE programs_unit.unit_id = " . $unitId;
    $programResult = $applyweb->handleSelectQuery($programQuery);
    foreach ($programResult as $programRecord) {
        $display .= '<br/><i>corresponds to program: ' . $programRecord['name'] . ' (program id: ' . $programRecord['id'] . ')</i>';
    }
    
    $unitPeriods = $unit->getPeriods();
    if ( count($unitPeriods) > 0) {
        $display .= '<br/><br/><i>has periods</i>:<br/><br/>';
        $display .= '<ul>';
        foreach ($unitPeriods as $period) {
            $display .= '<li>'; 
            $display .= date( 'm/d/Y', strtotime( $period->getStartDate() ) ); 
            $display .= ' - ' . date( 'm/d/Y', strtotime( $period->getEndDate() ) ) . ' (period id: ' . $period->getId() . ')';
            $display .= '<br/><i>includes programs</i>:<ul>';
            foreach ($period->getPrograms() as $programId => $program) {
                $display .= '<li>' . $program . ' (<a href="#' . $programId . '">unit id: ' . $programId .'</a>)</li>';    
            }
            $display .= '</ul><br/></li>';    
        }
        $display .= '</ul>';
    }
    
    $childUnits = $unit->getChildUnits();
    if ( count($childUnits) > 0 ) {
        $display .= '<br/><i>has subunits</i>:<br/>';
        foreach ($childUnits as $childUnit) {
            $display .= '<br/>';
            $display .= displayDescendants($childUnit);
        }        
    }

    $display .= '</li></ul>';
    
    return $display;  
}

function displayPeriod($period) {
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $status = $period->getStatus();
    $activePeriodStyle = 'style="margin: 2px;"';
    if ($status == 'active') {
        $activePeriodStyle = 'style="margin: 2px; border-left: 2px solid red;"';
    }
    if ($status == 'future') {
        $activePeriodStyle = 'style="margin: 2px; border-left: 2px solid orange;"';
    }
    $display = '<li ' . $activePeriodStyle. '><b>'; 
    $display .= $startDate . ' to ' . $endDate . '</b>';
    $periodDescription = $period->getDescription();
    if ($periodDescription) {
        $display .= ' (' . $periodDescription . ')';
    }
    $display .= ' (period id: ' . $period->getId() . ')';
    
    $subperiods = $period->getChildPeriods();
    $subperiodCount = count($subperiods);
    if ($subperiodCount > 0) {
        $display .= '<br/><i>includes subperiod(s)</i>:<ul>';
        foreach ($subperiods as $subperiod) {
            $display .= displayPeriod($subperiod);
        }
        $display .= '</ul>';    
    }
    
    $programs = $period->getPrograms();
    $programCount = count($programs);
    if ($programCount > 0) {
        $display .= '<br/><i>includes program(s)</i>:<ul>';
        foreach ($programs as $programId => $program) {
            $display .= '<li>' . $program . ' (<a href="#' . $programId . '">unit id: ' . $programId .'</a>)</li>';    
        }
        $display .= '</ul>';
    }
    $display .= '<br/></li>';
    
    return $display;     
}
  
?>