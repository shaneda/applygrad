<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$sql = "";
$err = "";
$domain = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{

				mysql_query("DELETE FROM domain WHERE id=".$itemId)
				or die(mysql_error());
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select id,name
from domain
order by name";

$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($domain, $arr);
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT
	department.id,
	department.name
	FROM lu_domain_department
	inner join department on department.id = lu_domain_department.department_id
	where lu_domain_department.domain_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error().$sql);

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Domains';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Domains</span><br />
<br />

	<a href="domainAssign.php">Assign Departments to Domain</a> | <a href="domainEdit.php">Add Domain </a><br />
	<br />
	<table width="700" border="0" cellpadding="4" cellspacing="2">
      <tr class="tblHead">

		<td>Name</td>
		<td>ID</td>
        <td>associated departments </td>
        <td width="60" align="right">Delete</td>
      </tr>
     <?
	 $class = "tblItem";
	 for($i = 0; $i < count($domain); $i++){
	 if($i % 2 == 0)
	 {
	 	$class = "tblItemAlt";
	 }else
	 {
	 	$class = "tblItem";
	 }
	 ?>
	<tr class="<?=$class?>">

		<td><a href="domainEdit.php?id=<?=$domain[$i][0]?>"><?=$domain[$i][1]?></a></td>
		<td><?=$domain[$i][0]?></td>
        <td>
		<?
		$depts = getDepartments($domain[$i][0]);
		$tmpdept = "-1";

		for($j = 0; $j < count($depts); $j++)
		{
			if($tmpdept != "-1")//SAME DEPT
			{
				echo ", ";
			}
			echo "<a href='departmentEdit.php?id=".$depts[$j][0]."'>".$depts[$j][1]."</a>";
			$tmpdept = $depts[$j][0];

		}
		?>
		</td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$domain[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	<? } ?>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>