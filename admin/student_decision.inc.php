<!-- PLB javascript -->
<script language="javascript" src="../review/javascript/jquery-1.2.6.js"></script>
<script language="javascript">

$(document).ready(function(){ 

    $("#student_decision").click(function(event){
    
        var $target = $(event.target);
        
        if ( $target.is("input.studentDecision") ) {

            var id_array = $target.attr('id').split('_');
            var applicationId = id_array[1];
            var programId = id_array[2];
            var decision = $target.attr('value');
            var msg = "Decision status updated: " + decision;
            msg += " (" + applicationId + "-" + programId + ")"
            //alert(msg);

            $.get("updateStudentDecision.php", { 
                application_id: applicationId,
                program_id: programId, 
                decision: decision
                },
                function(data){                             
                    alert(msg);
                },
                "text");
        }
    
    });    
    
});

</script>

<?php

// dummy values for testing
$application_id = 1234567;
$program_id = 321;

// include the db classes
include_once '../inc/config.php';
include "../review/classes/class.db_applyweb.php";
include "../review/classes/class.db_student_decision.php";

// Check the student_decision status. 
$db_student_decision = new DB_StudentDecision();
$student_decision_array = $db_student_decision->getStudentDecision($application_id, $program_id);  
$student_decision = $student_decision_array[0]['decision'];

$accept_checked = $decline_checked = $defer_checked = "";

switch ($student_decision) {

    case "accept":
    
        $accept_checked = "checked";
        break;
    
    case "decline":
    
        $decline_checked = "checked";
        break;  
    
    case "defer":
    
       $defer_checked = "checked";
       break;
} 

$id_key = $application_id . '_' . $program_id; 
?>

<div id="student_decision">
    <input type="radio" name="studentDecision_<?= $id_key ?>" id="decisionAccept_<?= $id_key ?>" class="studentDecision"  value="accept" <?= $accept_checked ?>> Accept
    <input type="radio" name="studentDecision_<?= $id_key ?>" id="decisionDecline_<?= $id_key ?>" class="studentDecision" value="decline"<?= $decline_checked ?>> Decline
    <input type="radio" name="studentDecision_<?= $id_key ?>" id="decisionDefer_<?= $id_key ?>" class="studentDecision" value="defer" <?= $defer_checked ?>> Defer
</div>