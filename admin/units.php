<?php
$startTime = microtime(TRUE);
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Applyweb_Table.php'; 

include '../classes/DB_Applyweb/Table/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Program.php';
include '../classes/DB_Applyweb/class.DB_ProgramType.php';
include '../classes/DB_Applyweb/class.DB_UnitRole.php';

include '../classes/DB_Applyweb/class.DB_Programs.php';
include '../classes/DB_Applyweb/class.DB_ProgramsUnit.php';
include '../classes/DB_Applyweb/class.DB_Department.php';
include '../classes/DB_Applyweb/class.DB_DepartmentUnit.php';
include '../classes/DB_Applyweb/class.DB_Domain.php';
include '../classes/DB_Applyweb/class.DB_DomainUnit.php';

include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_UnitPeriod.php'; 
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/DB_Applyweb/class.DB_PeriodType.php';
include '../classes/DB_Applyweb/Table/class.DB_PeriodUmbrella.php'; 
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroup.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupRole.php';  

include '../classes/Unit/class.UnitBase.php';
include '../classes/Unit/class.Program.php';
include '../classes/Unit/class.Unit.php';

include '../classes/Period/class.PeriodBase.php';
include '../classes/Period/class.SubPeriod.php';
include '../classes/Period/class.UmbrellaPeriod.php';
include '../classes/Period/class.Period.php';

include '../classes/class.User.php';

require("HTML/QuickForm.php");
include '../classes/HTML_QuickForm/Unit/class.NewUnit_QuickForm.php';
include '../classes/HTML_QuickForm/Unit/class.Unit_QuickForm.php';
include '../classes/HTML_QuickForm/Unit/class.DomainUnit_QuickForm.php';
include '../classes/HTML_QuickForm/Unit/class.DepartmentUnit_QuickForm.php';
include '../classes/HTML_QuickForm/Unit/class.ProgramUnit_QuickForm.php';
include '../classes/HTML_QuickForm/Unit/class.ManageUnitPeriods_QuickForm.php';



/*
* Determine the unit of interest and what, if anything, is being edited.
*/
$selectedUnitId = NULL;
$selectedUnitParentId = NULL;
$selectedUnitDescendantIds = array();
if ( isset($_REQUEST['unit_id']) &&  $_REQUEST['unit_id'] != '' ) {
    $selectedUnitId = $_REQUEST['unit_id'];
    $selectedUnit = new Unit($selectedUnitId);
    $selectedUnitParentId = $selectedUnit->getParentUnitId();
    $selectedUnitDescendantIds = $selectedUnit->getDescendantUnitIds();
} else {
    // Start with an "empty" unit.
    $selectedUnit = new Unit();
}

$edit = NULL;
if ( isset($_REQUEST['edit']) ) {
    $edit = $_REQUEST['edit'];
}

/* 
* Get the user's permissions.
*/
$user = new User();
$user->loadFromSession();
$userRole = $user->getUserRole();

$allAdminUnitIds = $rootAdminUnitIds = $user->getAdminUnits();
$rootUnitDescendantIds = array();
foreach ($rootAdminUnitIds as $rootUnitId) {    
    //if (!$selectedUnitId && !($edit && $_REQUEST['submitUnit'] != 'Cancel') ) {
    if (!$selectedUnitId && !$edit) { 
        // Make the root unit the selected unit by default.
        $selectedUnitId = $rootUnitId;
        $selectedUnit = new Unit($selectedUnitId);
    }
    $rootUnit = new Unit($rootUnitId);
    $rootUnitDescendantIds = array_merge( $rootUnitDescendantIds, $rootUnit->getDescendantUnitIds() );
}
$allAdminUnitIds = array_merge($allAdminUnitIds, $rootUnitDescendantIds);

$allowAdmin = FALSE;
if ( ($userRole == 'Administrator' || $userRole == 'Super User') 
    && ( ($selectedUnitId && in_array($selectedUnitId, $allAdminUnitIds) ) || $edit)  ) 
{
    $allowAdmin = TRUE;        
}

/*
* Enable period management only for the top-level units and MSE-MSIT "departments"; 
*/ 
$managePeriods = FALSE;
if ($selectedUnitParentId === NULL || $selectedUnitParentId == 21) {
    $managePeriods = TRUE;
}

/*
* If the user doesn't have admin rights for the unit, include the "view" template and exit.
*/
if (!$allowAdmin) { 
    include '../inc/tpl.units.php';
    exit;
}

/*
* Otherwise... handle the "add new unit" form (only submits edit=newUnit). 
*/
$newUnitForm = new NewUnit_QuickForm();
$newUnitForm->handleSelf();

/*
* Always handle the form for unit table elements.
*/
$unitForm = new Unit_QuickForm();
$unitFormValid = $unitForm->handleSelf($selectedUnit, $allAdminUnitIds);
if ( $unitFormValid && ( !$selectedUnitId || $unitForm->isSubmitted() ) ) {
    // Redirect to the new unit.
    reloadRedirect( $selectedUnit->getId() ); 
}  

/*
* If working on a new unit, stop after setting up the unit form.
*/
if ( $newUnitForm->isSubmitted() ) {
    // Include the "view" template and exit.
    include '../inc/tpl.units.php';
    exit;
}

/*
* Otherwise... handle the form for domain_unit table elements.
*/
$domainUnitForm = new DomainUnit_QuickForm();
$domainUnitFormValid = $domainUnitForm->handleSelf($selectedUnit);
if ( $domainUnitFormValid && $domainUnitForm->isSubmitted() ) {
    reloadRedirect( $selectedUnit->getId() ); 
}


/*
* Handle the form for department_unit table elements
*/
$departmentUnitForm = new DepartmentUnit_QuickForm();
$departmentUnitFormValid = $departmentUnitForm->handleSelf($selectedUnit);
if ( $departmentUnitFormValid && $departmentUnitForm->isSubmitted() ) {
    reloadRedirect( $selectedUnit->getId() );   
}

/*
* Handle the form for program_unit table elements
*/
$programUnitForm = new ProgramUnit_QuickForm();
$programUnitFormValid = $programUnitForm->handleSelf($selectedUnit);
if ( $programUnitFormValid && $programUnitForm->isSubmitted() ) {
    reloadRedirect( $selectedUnit->getId() );   
}

/*
* Handle the "manage periods" form.
*/
/*
$managePeriodsForm = new ManageUnitPeriods_QuickForm();
$managePeriodsForm->setDefaults( array('unit_id' => $selectedUnitId) );
*/


/*
* Temporary: get role arrays. 
*/
$roleUsers = array();
$DB_Applyweb = new DB_Applyweb();
if ($selectedUnitId) {
    $roleUserQuery = "SELECT users.id AS users_id,
                        users.firstname,
                        users.lastname,
                        unit_role.role_id
                        FROM unit_role 
                        INNER JOIN users ON unit_role.users_id = users.id
                        WHERE unit_role.unit_id = " . $selectedUnitId . "
                        ORDER BY users.lastname";
    $roleUsers = $DB_Applyweb->handleSelectQuery($roleUserQuery);
}
$admins = array();
$faculty = array();
foreach ($roleUsers as $roleUser) {
    switch ($roleUser['role_id']) {
        case 1:
            $admins[] = $roleUser;
            break;    
        case 4:
            $faculty[] = $roleUser;
            break;
        default:
    }
}


/*
*  Include the "view" template.
*/
include '../inc/tpl.units.php';

function reloadRedirect($unitId) {
    header( 'Location: https://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . '?unit_id=' . $unitId );
    exit;     
}
?>