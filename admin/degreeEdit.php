 <?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$name = "";
$abbrev = "";
$id = -1;
$requirement = array();
$requirements = array();

if(isset($_POST['txtdegreeId']))
{
	$id = htmlspecialchars($_POST['txtdegreeId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$abbrev = htmlspecialchars($_POST['txtAbbrev']);
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update degree set name='".$name."', short='".$abbrev."' where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
		}
		else
		{
			$sql = "insert into degree(name, short)values('".$name."', '".$abbrev."')";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}

		//mysql_query("DELETE FROM lu_degrees_applicationreqs WHERE degree_id=".$id)or die(mysql_error().$sql);

		$vars = $_POST;
		$itemId = -1;
		foreach($vars as $key => $value)
		{
			$arr = split("_", $key);
			if(strstr($key, 'chkRequirement') !== false)
			{
				$tmpid = $arr[1];
				if($itemId != $tmpid || $tmpType != $arr[0])
				{
					$itemId = $tmpid;
					$tmpType=$arr[0];
					//echo $arr[0];
					/*
                    if($arr[0] == "chkRequirement")
					{
						$sql = "insert into lu_degrees_applicationreqs(appreq_id, degree_id)values(".$itemId.",".$id.")";
						mysql_query($sql)or die(mysql_error().$sql);
						//echo $sql;
					}
                    */
				}
			}
		}//END FOR
		
        header("Location: degrees.php");
	}
}

//get requirements
$sql = "select id,short from applicationreqs order by name";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, str_replace("<br>"," ", $row['short']));
	array_push($requirements, $arr);
}

$sql = "select id,appreq_id from lu_degrees_applicationreqs  where degree_id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['appreq_id']);
	//array_push($arr, $row['appreq_id']);
	array_push($requirement, $arr);
}


$sql = "SELECT id,name,short FROM degree where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$abbrev = $row['short'];

}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Degree';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php'; 

?>
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Degree</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>
    <a href="degrees.php">Return to degrees list</a>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtdegreesId" type="hidden" id="txtdegreeId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'], true, null, true, 30); ?>        </td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Abbreviation:</strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($abbrev, "textbox", "txtAbbrev", $_SESSION['A_allow_admin_edit'],true,null,true,10); ?>
        </span></td>
      </tr>
      <!--
      <tr>
        <td align="right"><strong>Degree Requirements:</strong> </td>
        <td >
          <? //showEditText($requirement, "checklist", "chkRequirement", $_SESSION['A_allow_admin_edit'],false, $requirements); ?>
       </td>
      </tr>
      -->
      <tr>
        <td width="200" align="right">&nbsp;</td>
        <td >
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>