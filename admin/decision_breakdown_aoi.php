<?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

include '../inc/reviewListFunctions.inc.php';
include '../inc/inc.decisionBreakdownData.php';

// Main request vars
$departmentId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);

$sortable = FALSE;
$admittedOnly = TRUE;
$show = filter_input(INPUT_POST, 'show', FILTER_SANITIZE_STRING);
if ($show != 'admitted') {

    $show = 'all';
    $admittedOnly = FALSE;
    
    if ($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 10) {
        $sortable = TRUE;    
    }
}

$save = filter_input(INPUT_POST, 'save', FILTER_VALIDATE_INT);
if ( $save && ($save == 1) ) {
    $saveDepartmentId = filter_input(INPUT_POST, 'departmentId', FILTER_VALIDATE_INT);
    $savePeriodId = filter_input(INPUT_POST, 'periodId', FILTER_VALIDATE_INT);
    $saveInterestId = filter_input(INPUT_POST, 'interestId', FILTER_VALIDATE_INT);
    $saveInterest = filter_input(INPUT_POST, 'interest', FILTER_UNSAFE_RAW);
    $saveDecision = filter_input(INPUT_POST, 'decision', FILTER_UNSAFE_RAW);
    $rankings = json_decode($_POST['rankings']);
    saveDecisionAoiRank($saveDepartmentId, $savePeriodId, $saveInterestId, $saveDecision, $rankings);
    saveDecisionAoiRankMembers($saveDepartmentId, $savePeriodId, $saveInterestId, $saveDecision, $rankings);
    echo '1';
    exit;
}



// Period display
$departmentName = '';
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = '';
if ($periodId) {

    $applicationPeriodDisplay = getDepartmentName($departmentId);
    
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay .= '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Admission Committee Decision Breakdown';

$pageCssFiles = array(
    '../css/jquery.ui.all.css',
    '../css/reviewApplications.css',
    '../css/decisionBreakdown.css'
    );

$headJavascriptFiles = array(
    '../javascript/jquery-1.6.2.min.js',
    '../javascript//jquery-ui-1.8.16.custom.min.js',
    '../javascript/json2.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?> 
<div id="pageHeading">
    <div id="departmentName"><?php echo $applicationPeriodDisplay; ?></div>            
    <div id="sectionTitle"><?php echo $pageTitle; ?></div>
</div>

<div style="margin:7px; clear: both;">
<br/>
<form id="form1" name="form1" action="" method="post">
    
    <?php
    if (is_array($periodId)) {
        $periodParam = 'period[]=' . implode('&period[]=', $periodId);    
    } else {
        $periodParam = 'period=' . $periodId;
    }
    ?>
    <a href="decision_breakdown.php?id=<?=$departmentId?>&<?php echo $periodParam; ?>">Applicants by Decision</a>&nbsp;&nbsp;
    <b>Applicants by AOI / Decision</b>&nbsp;&nbsp;
    <a href="get_decision_counts.php?id=<?=$departmentId?>&<?php echo $periodParam; ?>">Counts by Decision / AOI</a>&nbsp;&nbsp; 
    <br/><br/>

<?php
if ($show == 'all') {
    $allSelected = 'checked';
    $admittedSelected = '';    
} else {
    $allSelected = '';
    $admittedSelected = 'checked';
}
?>    
<b>Show:</b>
<input type="radio" name="show" value="all" <?php echo $allSelected; ?>> All 
<input type="radio" name="show" value="admitted" <?php echo $admittedSelected; ?>> Admitted Only
&nbsp;&nbsp;<input type="submit" value="Change" style="font-size: 10px;" />
</form>
</div>

<div id="#content" style="clear: both; height: 100%;">
<?php
$aoiDecisionRecords = getAoiDecisionRecords($departmentId, $periodId, $admittedOnly);

foreach($aoiDecisionRecords as $interestId => $interestData) {
    echo makeAoiDecisionDiv($interestId, $interestData, $sortable) . "\n\n";
} 

?>
</div>


<form action="" method="POST" name="editForm" id="editForm">
    <input type='hidden' name='userid' value=''>
</form>

<script type="text/javascript">

function openForm(userid, formname) { 
    var editForm = document.getElementById('editForm');
    editForm.elements['userid'].value = userid;
    editForm.action = formname;
    editForm.target = '_blank';
    editForm.submit();
}

$(document).ready(function() {
    
    $(".draggable").draggable({
        handle: '.draggable_handle', 
        cursor: 'pointer', 
        stack: '.draggable'
    });
    
    $(".sortable").sortable({
        handle: 'span.name',
        containment: 'parent',
        cursor: '<?php echo $sortable ? 'pointer' : 'text'; ?>'    
    });
    
    $(".save").hover(
        function () {
            var style = "margin-top: 5px; border-top: 1px gray dotted; background-color: white;";
            $(this).parent().parent().attr('style', style);
        }, 
        function () {
            var style = "margin-top: 5px; border-top: 1px gray dotted;";
            $(this).parent().parent().attr('style', style);
        }
    );
    
    /*
    * pop-up confirm/cancel with note? 
    * http://jqueryui.com/demos/dialog/#modal-form
    */
    
    $(".save").click(function() {

        var idPieces = $(this).attr("id").split("_");
        var interestId = idPieces[1];
        var decision = idPieces[2];
        var departmentId = idPieces[3];
        var periodId = idPieces[4];
        
        var interest = $("#interest_" + interestId + "_name").text(); 

        var rankings = new Array();
        $("#groupMembers_" + interestId + "_" + decision).find("li").each(function (i) {
            var applicationIdPieces = $(this).attr("id").split("_");
            var applicationId = applicationIdPieces[1];
            rankings.push(applicationId);
        });

        $.post(
            'decision_breakdown_aoi.php', 
            { 
                save: "1",
                departmentId: departmentId,
                periodId: periodId,
                interest: interest,
                interestId: interestId,
                decision: decision, 
                rankings: JSON.stringify(rankings)
            },
            function(data){
                if (data != -1) {
                    alert(interest + ' ' + decision + ' rankings saved.');    
                } else {
                    alert(error);    
                }           
            },
            'text'
        );
        
    });
});
</script>


<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function makeAoiDecisionDiv($interestId, $interestData, $sortable = TRUE) {
    
    global $departmentId;
    global $periodId;
    $decisions = array('A1', 'A2', 'B1', 'B2', 'S', 'R');
    
    $div = <<<EOB
        <div id="interest_{$interestId}" class="draggable" >
        <div class="draggable_handle">
            <span id="interest_{$interestId}_name" style="font-weight: bold">{$interestData['interest']}</span>
            <i>({$interestData['record_count']})</i>
        </div>
        <div class="draggable_list">
EOB;

    foreach($decisions as $decision) {
        
        $decisionRecords = array(); 
        if (isset($interestData['records'][$decision])) {
            $decisionRecords = $interestData['records'][$decision];   
        }
        $decisionRecordCount = count($decisionRecords);
        
        if ($sortable && $decisionRecordCount > 0) {
            $class = 'sortable';
            $saveDiv = '<input type="button" value="Save" 
                id="save_' . $interestId . '_' . $decision . '_' . $departmentId . '_' . $periodId
                . '" class="save"  style="font-size: 10px;" />';
        } else {
            $class = 'viewable';
            $saveDiv = '';
        }
        
        
        $div .= <<<EOB
            <div id="groupMembers_{$interestId}_{$decision}" 
                style="margin-top: 5px; border-top: 1px gray dotted;">
            <div style="float: left;">
                <b>{$decision}</b> <i>({$decisionRecordCount})</i>
            </div>
            <div style="float: right;">
                {$saveDiv}    
            </div>
            <div>
            <ol class="{$class}" style="margin-top: 10px; clear: left;">
EOB;
        
        foreach ($decisionRecords as $record) {
            $div .= makeLi($record) ."\n";
        }
        
        $div .= <<<EOB
            </ol>
            </div>
            </div>
EOB;
    }

    $div .= <<<EOB
        </div>
        </div>
EOB;
    
    return $div;
}

function makeLi($record) {
    
    global $departmentId;
    
    $statusKey = array(
        0 => 'R',
        1 => 'W',
        2 => 'A'
    );
    
    $applicationId = $record['application_id'];
    $luuId = $record['luu_id'];
    $admissionStatus = $statusKey[$record['admission_status']];
    
    $li = '<li id="application_' . $applicationId . '" class="groupMember">';
    $li .= '<a href="javascript: openForm(\'' . $luuId . '\',';
    $li .= '\'../review/reviewApplicationSingle.php?applicationId=';
    $li .= $applicationId . '&v=2&r=2&d=' . $departmentId . '&showDecision=1\')">';
    $li .= 'View</a>&nbsp;&nbsp;';
    $li .= '<span class="name" style="cursor: pointer;">';
    $li .= $record['name'];
    $li .= '&nbsp;<i>(' . $admissionStatus . ')</i>';
    $li .= '</span>';  
    $li .= '</li>';

    return $li;
}
?>