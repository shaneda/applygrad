<?php
// Include the config class, which also has the 
// session and db includes.
include("../inc/config.php");

// Start session to get user.id
session_start();

// Include the data access classes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";

include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
include '../classes/class.Payment.php';

include "../classes/DB_Applyweb/Table/class.DB_PeriodApplication.php";
include "../classes/DB_Applyweb/Table/class.DB_LuApplicationPrograms.php";
include "../classes/DB_Applyweb/Table/class.DB_Users.php";
include "../classes/DB_Applyweb/Table/class.DB_UsersInfo.php";
include "../classes/DB_Applyweb/Table/class.DB_Recommend.php";
include "../classes/DB_Applyweb/Table/class.DB_Datafileinfo.php";  
include "../classes/Application/class.ApplicationData.php";

include "../classes/class.db_education.php";
include "../classes/class.db_publications.php";
include "../classes/class.db_recommendations.php";
include "../classes/class.db_test_scores.php";

$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include '../inc/specialCasesAdmin.inc.php';

$applicationId = NULL;
if ( isset($_REQUEST['applicationId']) ) {
    $applicationId = $_REQUEST['applicationId'];
}
$mode = $_REQUEST['mode'];
$returnString = "";

switch ($mode) {
    
    case "waiveToefl":
        
        $waiveToefl = $_GET['waiveToefl'];
        $dbTestScores = new DB_TestScores();
        $status = $dbTestScores->updateWaiveToefl($applicationId, $waiveToefl);
        $returnString = $status;
        break;
    
    case "greReceived":
        
        $greScoreId = $_GET['greScoreId'];
        $scoreReceived = $_GET['scoreReceived'];
        $dbTestScores = new DB_TestScores();
        $applicationId = $dbTestScores->getGreScoreApplicationId($greScoreId);
        $status = $dbTestScores->updateGREGeneralReceived($greScoreId, $scoreReceived);
        $returnString = $status;
        break;

    case "updateGreScore":
        
        $greScoreId = $_GET['greScoreId'];
        $testDate = $_GET['testDate'];
        $verbalScore = $_GET['verbalScore'];
        $verbalPercentile = $_GET['verbalPercentile'];
        $quantitativeScore = $_GET['quantitativeScore'];
        $quantitativePercentile = $_GET['quantitativePercentile'];
        $analyticalScore = $_GET['analyticalScore'];
        $analyticalPercentile = $_GET['analyticalPercentile'];
        $writingScore = $_GET['writingScore'];
        $writingPercentile = $_GET['writingPercentile'];
        
        if (check_Date3($testDate) == FALSE)
        {
            $returnString = 'Error: test date must be in (MM/YYYY) format.';
        }
        else
        {
            $dbTestScores = new DB_TestScores();
            $applicationId = $dbTestScores->getGreScoreApplicationId($greScoreId);
            $status = $dbTestScores->updateGREGeneralScore($greScoreId, 
                                        $verbalScore, $verbalPercentile, 
                                        $quantitativeScore, $quantitativePercentile,
                                        $analyticalScore, $analyticalPercentile,
                                        $writingScore, $writingPercentile, 
                                        formatMySQLdate2($testDate,'/','-'));
            $returnString = $status;
        }
        
        break;

    case "greSubjectReceived":

        $greSubjectScoreId = $_GET['greSubjectScoreId'];
        $scoreReceived = $_GET['scoreReceived'];
        $dbTestScores = new DB_TestScores();
        $status = $dbTestScores->updateGRESubjectReceived($greSubjectScoreId, $scoreReceived);
        $returnString = $status;
        break;

     case "updateGreSubjectScore":
        
        $greSubjectScoreId = $_GET['greSubjectScoreId'];
        $subjectName = $_GET['subjectName'];
        $subjectScore = $_GET['subjectScore'];
        $subjectPercentile = $_GET['subjectPercentile'];
        
        $dbTestScores = new DB_TestScores();
        $status = $dbTestScores->updateGRESubjectScore($greSubjectScoreId,
                                    $subjectName,
                                    $subjectScore, 
                                    $subjectPercentile);
        $returnString = $status;
        break;

    case "gmatReceived":
        
        $gmatScoreId = $_GET['gmatScoreId'];
        $scoreReceived = $_GET['scoreReceived'];
        $dbTestScores = new DB_TestScores();
        $status = $dbTestScores->updateGMATReceived($gmatScoreId, $scoreReceived);
        $returnString = $status;
        break;

    case "updateGmatScore":
        
        $gmatScoreId = $_GET['gmatScoreId'];
        $verbalScore = $_GET['verbalScore'];
        $verbalPercentile = $_GET['verbalPercentile'];
        $quantitativeScore = $_GET['quantitativeScore'];
        $quantitativePercentile = $_GET['quantitativePercentile'];
        $totalScore = $_GET['totalScore'];
        $totalPercentile = $_GET['totalPercentile'];
        $writingScore = $_GET['writingScore'];
        $writingPercentile = $_GET['writingPercentile'];
        
        $dbTestScores = new DB_TestScores();
        $status = $dbTestScores->updateGMATScore($gmatScoreId, 
                                    $verbalScore, $verbalPercentile, 
                                    $quantitativeScore, $quantitativePercentile,
                                    $totalScore, $totalPercentile,
                                    $writingScore, $writingPercentile);
        $returnString = $status;
        break;

    case "toeflReceived":
        
        $toeflScoreId = $_GET['toeflScoreId'];
        $scoreReceived = $_GET['scoreReceived'];
        $dbTestScores = new DB_TestScores();
        $applicationId = $dbTestScores->getToeflScoreApplicationId($toeflScoreId);
        $status = $dbTestScores->UpdateTOEFLReceived($toeflScoreId, $scoreReceived);
        $returnString = $status;
        break;
        
    case "updateToeflScore":
        
        $toeflScoreId = $_GET['toeflScoreId'];
        $testDate = $_GET['testDate'];
        $section1Score = $_GET['section1Score'];
        $section2Score = $_GET['section2Score'];
        $section3Score = $_GET['section3Score'];
        $essayScore = $_GET['essayScore'];
        $totalScore = $_GET['totalScore'];
        
        if (check_Date3($testDate) == FALSE)
        {
            $returnString = 'Error: test date must be in (MM/YYYY) format.';
        }
        else
        {
            $dbTestScores = new DB_TestScores();
            $applicationId = $dbTestScores->getToeflScoreApplicationId($toeflScoreId);
            $status = $dbTestScores->updateTOEFLScore($toeflScoreId, 
                                        $section1Score, $section2Score, 
                                        $section3Score, $essayScore,
                                        $totalScore,
                                        formatMySQLdate2($testDate,'/','-'));
            $returnString = $status;
        }

        break;

    case "ieltsReceived":
        
        $ieltsScoreId = $_GET['ieltsScoreId'];
        $scoreReceived = $_GET['scoreReceived'];
        $dbTestScores = new DB_TestScores();
        $applicationId = $dbTestScores->getIELTSApplicationId($ieltsScoreId);
        $status = $dbTestScores->updateIELTSReceived($ieltsScoreId, $scoreReceived);
        $returnString = $status;
        break;

    case "updateIeltsScore":

        $ieltsScoreId = $_GET['ieltsScoreId'];
        $listeningScore = $_GET['listeningScore'];
        $readingScore = $_GET['readingScore'];
        $writingScore = $_GET['writingScore'];
        $speakingScore = $_GET['speakingScore'];
        $overallScore = $_GET['overallScore'];
        
        $dbTestScores = new DB_TestScores();
        $applicationId = $dbTestScores->getIELTSApplicationId($ieltsScoreId);
        $status = $dbTestScores->updateIELTSScore($ieltsScoreId, $listeningScore, $readingScore, 
                                    $writingScore, $speakingScore, $overallScore);
        $returnString = $status;
        break;

    case "transcriptReceived":

        $transcriptId = $_GET['transcriptId'];
        $transcriptReceived = $_GET['transcriptReceived'];
        $dbEducation = new DB_Education();
        $applicationId = $dbEducation->getTranscriptApplicationId($transcriptId);
        $status = $dbEducation->updateTranscriptReceived($transcriptId, $transcriptReceived);
        $returnString = $status;
        break;
        
    case "updateConvertedGpa":
    
        $transcriptId = $_GET['transcriptId'];
        $convertedGpa = $_GET['convertedGpa'];
        $dbEducation = new DB_Education();
        $applicationId = $dbEducation->getTranscriptApplicationId($transcriptId);
        $status = $dbEducation->updateConvertedGpa($transcriptId, $convertedGpa);
        $returnString = $status;
        break;

    case "uploadRecommendation":
    
        $recommendationId = $_REQUEST['recommendationId'];  
        $applicantLuuId = $_REQUEST['applicantId'];
        $applicantGuid = $_REQUEST['applicantGuid'];
        
        $uploadMode = $_REQUEST['uploadMode'];
        
        $fileType = $_FILES['fileToUpload']['type']; 
        $fileExtension = substr( strrchr($_FILES['fileToUpload']['name'], "."), 1);
        $fileSize = $_FILES['fileToUpload']['size'];
        $section = 3; // code for recommendation
        $userData = $applicationId . "_" . $recommendationId;
                                                 
        $newFileDir =  $datafileroot . "/" . $applicantGuid . "/recommendation/";
        $newFileName = "recommendation_" . $userData . "." . $fileExtension;
        $newFilePath = $newFileDir . $newFileName;
        
        $databaseStatus = 1;
        // check to see that a file was actually submitted 
        if(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none'){
            
            $error = "No file selected.";
            $msg = "";
            $returnString = "{error: '" . $error . "', msg: '" . $msg . "'}";
            
        } else {
            
            // upload the file
            $returnString = handleAjaxFileUpload($newFileDir, $newFilePath);
            
            // check for file upload error
            $jsonObject = json_decode($returnString);
            $uploadError = $jsonObject->{'error'};
            
            // if no upload error
            if ( $uploadError == "" ) {
                
                // do the database insert/update 
                $dbRecommendations = new DB_Recommendations();
                if ($uploadMode == "update"){

                    $databaseStatus = $dbRecommendations->updateFile($recommendationId, 
                                            $fileType, $fileExtension, $fileSize, 
                                            $applicantLuuId, $section, $userData);        
                } else {
            
                    $databaseStatus = $dbRecommendations->addFile($recommendationId, 
                                            $fileType, $fileExtension, $fileSize, 
                                            $applicantLuuId, $section, $userData);
               }  
            } // end if upload error
        } // end file empty if/else

        // set return JSON based on database status       
        if ( $databaseStatus >= 1 ) {
             // do nothing to change return string messages
        } else {
            $error = "Database update failed.";
            $msg = "File not uploaded.";
            $returnString = "{error: '" . $error . "', msg: '" . $msg . "'}";
        }
        
        break;      
        
    case 'feeWaived':

        $waive = $_GET['feeWaived'];
        $paymentManager = new PaymentManager($applicationId);
        $status = $paymentManager->saveFeeWaived($waive);
        $returnString = $status; 
        break;

    case 'useLowerFee':

        $useLowerFee = $_GET['useLowerFee'];
        $paymentManager = new PaymentManager($applicationId);
        $status = $paymentManager->saveWaiveHigherFee($useLowerFee);
        $returnString = $status; 
        break;
        
    case 'paymentPaid':
    case 'newPayment':
        
        $paymentId = NULL;
        if ( isset($_GET['paymentId']) ) {
            $paymentId = $_GET['paymentId'];     
        }
        $paymentAmount = NULL;
        if ( isset($_GET['paymentAmount']) ) {
            $paymentAmount = $_GET['paymentAmount'];     
        }
        $paymentPaid = $_GET['paymentPaid'];
        $paymentType = $_GET['paymentType'];

        if ( ($paymentType == 1) || ($paymentType == 'Check') ) {
            $paymentType = '1';
        } elseif ( ($paymentType == 3) || ($paymentType == 'Voucher') ) {
            $paymentType = '3';
        } else {
            $paymentType = '2';
        }

        $paymentStatus = 'pending';
        if ($paymentPaid || $mode == 'newPayment') {
            $paymentStatus = 'paid';
        }   

        $payment = array(
            'payment_id' => $paymentId, 
            'payment_type' => $paymentType, 
            'payment_amount' => $paymentAmount, 
            'payment_status' => $paymentStatus,
            'last_mod_user_id' => $_SESSION['A_usermasterid']
            );
        
        $higherFeeDate = NULL;
        if ($mode == 'newPayment') {
        
            $payment['payment_intent_date'] = date('Y-m-d H:i:s'); 
        
            // Get higher fee date and program ids so payment items
            // so payment items can be created.
            $DB_Applyweb = new DB_Applyweb();
            
            $higherFeeDateQuery = "SELECT higher_fee_date FROM period_umbrella
                INNER JOIN period_application 
                    ON period_umbrella.period_id = period_application.period_id
                WHERE period_application.application_id = " .  $applicationId . "
                LIMIT 1";
            $higherFeeRecords = $DB_Applyweb->handleSelectQuery($higherFeeDateQuery);
            if (isset($higherFeeRecords[0]['higher_fee_date'])) {
                $higherFeeDate = $higherFeeRecords[0]['higher_fee_date'];    
            }    
            
            $programIdQuery = "SELECT program_id FROM lu_application_programs
                WHERE application_id = " . $applicationId;
            $programIdRecords = $DB_Applyweb->handleSelectQuery($programIdQuery, 'program_id');
            $programIds = array_keys($programIdRecords);
            
            $paymentManager = new PaymentManager($applicationId, $higherFeeDate);
            $paymentItems = getNewPaymentItems($paymentManager, $programIds);
            $status = $paymentManager->savePayment($payment, $paymentItems);
            
        } else {
         
            $paymentManager = new PaymentManager($applicationId);
            $status = $paymentManager->savePayment($payment);
        }

        $returnString = $status; 
        break;
    
    case 'paymentVoid':

        $paymentId = $_GET['paymentId'];
        $paymentManager = new PaymentManager($applicationId);
        $paymentRecord = array(
            'payment_id' => $paymentId,
            'payment_status' => 'void',
            'last_mod_user_id' => $_SESSION['A_usermasterid']
            );
        $status = $paymentManager->savePayment($paymentRecord);
        break;
    

    case 'applicationComplete':
    
        include "../classes/class.db_application.php"; 
        
        $applicationComplete = $_GET['applicationComplete'];
        $application = new DB_Application();
        $status = $application->updateComplete($applicationId, $applicationComplete);

        if ($status) 
        {
            $programQuery = "SELECT lu_application_programs.id AS program_id,
                                lu_programs_departments.department_id,
                                domain.id AS domain_id,
                                domain.name AS domain_name,
                                users.email
                                FROM application
                                INNER JOIN lu_users_usertypes
                                    ON application.user_id = lu_users_usertypes.id
                                INNER JOIN users
                                    ON lu_users_usertypes.user_id = users.id 
                                INNER JOIN lu_application_programs
                                    ON application.id = lu_application_programs.application_id
                                INNER JOIN lu_programs_departments 
                                    ON lu_application_programs.program_id = lu_programs_departments.program_id
                                INNER JOIN lu_domain_department
                                    ON lu_programs_departments.department_id = lu_domain_department.department_id
                                INNER JOIN domain
                                    ON lu_domain_department.domain_id = domain.id
                                WHERE lu_application_programs.application_id = " . $applicationId;
            $programResults = $application->handleSelectQuery($programQuery);

            foreach ($programResults as $programRecord) {

                if (!isIsreeDepartment($programRecord['department_id']))
                {
                    continue;
                }
                
                $luApplicationProgramsId = $programRecord['program_id'];
                if ($applicationComplete) {
                    $admissionStatus = 2;
                } else {
                    $admissionStatus = 0;
                }
                $admitQuery = "UPDATE lu_application_programs
                                SET admission_status = " . $admissionStatus;
                $admitQuery .= " WHERE lu_application_programs.id = " . $luApplicationProgramsId;
                $status = $application->handleUpdateQuery($admitQuery);

                if ($status && $applicationComplete) {
                
                    $content = '';
                    $contentQuery = "SELECT content 
                                    FROM content 
                                    WHERE name='Enrollment Notification' 
                                    AND domain_id = " . $programRecord['domain_id'];
                    $rows = $application->handleSelectQuery($contentQuery);
                    foreach ($rows as $row) {
                        $content = $row["content"];
                    }
                
                    if ($content) {
                        $content = parseEmailTemplate2($content);
                        switch ($hostname)
                        {
                            case "APPLY.STAT.CMU.EDU":  
                                $replyEmail = "scsstats@cs.cmu.edu";
                                break;
                            case "APPLYGRAD-INI.CS.CMU.EDU":  
                                $replyEmail = "scsini@cs.cmu.edu";
                                break;
                            case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                                $replyEmail = "scsdiet@cs.cmu.edu";
                                break;
                            default:
                                $replyEmail = "applygrad@cs.cmu.edu";
                            }
                         
                        $email = $isreeProgramRecord['email'];
                        $sieveString = '[' . $isreeProgramRecord['domain_name'] . ':'; 
                        $sieveString .= $isreeProgramRecord['program_id'] . ':' . $applicationId . ']';
                        sendHtmlMail($replyemail, $email, 
                            "Enrollment Notification", $content, "complete",
                            $isreeProgramRecord['domain_name'], $sieveString);                         
                    }
                }
            }
        }          
        
        $returnString = $status;
        break;

    case "fee_paid":
        
        $fee_paid = $_GET['fee_paid'];
        $amt = $_GET['amt']; 
        $payment = new DB_Payment();
        $status = $payment->updateFeePaid($applicationId, $fee_paid, $amt);
        $returnString = $status; 
        break;
    
    case "payment_amount":

        $payment_amount = $_GET['payment_amount'];
        $payment = new DB_Payment();
        $status = $payment->updatePaymentAmount($applicationId, $payment_amount);
        $returnString = $status;
        break;
        
    case "payment_reset":
        
        $payment = new DB_Payment();
        $status = $payment->resetPayment($applicationId);
        $returnString = $status;
        break;
        
    case "publication_status":
        
        $publication_id = $_GET['publication_id'];
        $publication_status = $_GET['status'];
        
        $publications = new DB_Publications();
        $query_status = $publications->updatePublicationStatus($publication_id, $publication_status); 
        
        $returnString = $query_status;
        break;
 
    case "application_submitted":
    
        include "../classes/class.db_application.php"; 
        
        $application_submitted = $_GET['application_submitted'];
        $application = new DB_Application();
        $status = $application->updateSubmitted($applicationId, $application_submitted);
        $returnString = $status; 
        break;
    
    case "copyApplication":

        include "../classes/DB_Applyweb/Table/class.DB_Application.php";
        
        $applicationId = $_GET['applicationId'];
        $periodId = $_GET['periodId'];
        if (is_array($_GET['programId'])) {
            $progamIds = $_GET['programId'];
        } else {
            $progamIds = array($_GET['programId']);
        }
        
        $returnString = handleApplicationCopy($applicationId, $periodId, $progamIds);
        
        break;

    case "saveNote":
        
        session_start();
        $applicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT);
        $note = filter_input(INPUT_GET, 'note', FILTER_SANITIZE_STRING); 
        $userId = $_SESSION['A_usermasterid'];
        
        if ( trim($note) ) {
            
            $DB_Applyweb = new DB_Applyweb();
            $saveNoteQuery =  "INSERT INTO application_admin_note (application_id, note, insert_user_id)
                VALUES ("
                . intval($applicationId) . ",'"
                . $DB_Applyweb->escapeString($note) . "',"
                . intval($userId) . 
                ")";
            $returnString = $DB_Applyweb->handleInsertQuery($saveNoteQuery);
                
        } else {
            
            $returnString = "0";    
        }
        
        break;

    case "hideApplication":
    
        $applicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT);
        $hideApplication = filter_input(INPUT_GET, 'hideApplication', FILTER_VALIDATE_INT);

        if ($applicationId && ($hideApplication == 0 || $hideApplication == 1)) {
            
            $DB_Applyweb = new DB_Applyweb();
            $hideApplicationQuery =  "UPDATE application SET hide = " . $hideApplication .
                " WHERE application.id = " . $applicationId;  
            $returnString = $DB_Applyweb->handleUpdateQuery($hideApplicationQuery);           
        
        } else {
        
            $returnString = "0";    
        }
            
        break;
        
    default:
        $returnString = "Unkown Case: " . $mode;
}

if ($returnString && isIniDepartment($_SESSION['roleDepartmentId']))
{
    logAccessIni($mode, $applicationId);
}


// Output the return data.
echo $returnString;


/*
* Function to handle file upload for upload_recommendation case 
*/
function handleAjaxFileUpload($newFileDir, $newFilePath) {
    $error = "";
    $msg = ""; 
    $fileElementName = 'fileToUpload';
    if(!empty($_FILES[$fileElementName]['error']))
    {
        switch($_FILES[$fileElementName]['error'])
        {

            case '1':
                $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                break;
            case '2':
                $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                break;
            case '3':
                $error = 'The uploaded file was only partially uploaded';
                break;
            case '4':
                $error = 'No file was uploaded.';
                break;

            case '6':
                $error = 'Missing a temporary folder';
                break;
            case '7':
                $error = 'Failed to write file to disk';
                break;
            case '8':
                $error = 'File upload stopped by extension';
                break;
            case '999':
            default:
                $error = 'No error code avaiable';
        }
    } elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
    {
        $error = 'No file was uploaded...';
    } else 
    {
            $msg .= "File Name: " . $_FILES['fileToUpload']['name'] . ", ";
            $msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']) . ", ";
            $msg .= " Copied As: " . $newFilePath;
            
            // PB - copy uploaded file
            $tmp_name = $_FILES['fileToUpload']['tmp_name'];

            // Get any upload error codes
            switch ($_FILES['fileToUpload']['error']) {
                case 0:
                   $uploadError = 'No upload error';
                   break;                    
                case 1:
                   $uploadError = 'Filesize exceeds system max';
                   break;
                case 2:
                   $uploadError = 'Filesize exceeds form max';
                   break;
                case 3:
                   $uploadError = 'Partial upload';
                   break;
                case 4:
                   $uploadError = 'No file uploaded';
                   break;
                case 6:
                   $uploadError = 'No temp folder';
                   break;
                case 7:
                   $uploadError = 'Disk write failed';
                   break;
                case 8:
                   $uploadError = 'Stopped by extension';
                   break;
                default:
                  $uploadError = 'Unknown upload error';  
            }
            
            // make the guid directory if not there already
            if ( !file_exists($newFileDir) ) {
                mkdir($newFileDir, 0775, TRUE);
            }
            
            if ( !move_uploaded_file($tmp_name, $newFilePath) ) {
                $error = "Upload of " . $newFilePath . " failed. (" . $uploadError . ")";
                $msg .= " " . $newFilePath . " not copied";
            }
            
            //for security reason, we force to remove all uploaded file
            @unlink($_FILES['fileToUpload']);        
    }
            
    /*
    $returnString = '{';
    $returnString .= '"error": "' . $error . '",';
    $returnString .= '"msg": "' . $msg . '"';
    $returnString .= '}';
    */
    $returnString = json_encode(array('error' => $error, 'msg' => $msg));

    return $returnString;
}

function handleApplicationCopy($applicationId, $targetPeriodId, $programIds = array()) {

    $error = '';
    $msg = '';

    // Check target period to see if user already has an application in it.
    $DB_Applyweb = new DB_Applyweb();
    $userApplicationQuery = "SELECT period_application.application_id
                                FROM application
                                INNER JOIN lu_users_usertypes
                                  ON application.user_id = lu_users_usertypes.id
                                INNER JOIN application AS application2
                                  ON lu_users_usertypes.id = application2.user_id
                                INNER JOIN period_application
                                  ON application2.id = period_application.application_id
                                WHERE application.id = " . $applicationId . "
                                AND period_application.period_id = " . $targetPeriodId;
    $userApplications = $DB_Applyweb->handleSelectQuery($userApplicationQuery); 
    
    if ( count($userApplications) > 0) {
    
        // The user already has an application in the target period.
        $msg = $userApplications[0]['application_id'];
        $error = 'duplicate user application';
        
    } else {

        // Copy the application.
        $newApplicationId = ApplicationData::replicate($applicationId, $targetPeriodId, $programIds);
        if ($newApplicationId) {
            $msg = $newApplicationId;    
        } else {
            $error = 'copy failed';
        }        
    }
    
    $returnString = json_encode(array('error' => $error, 'msg' => $msg));

    return $returnString;    
}

function getNewPaymentItems($paymentManager, $programIds) {
    
    // PaymentManager must be constructed with higher fee date 
    // for this to work properly.

    $programBalancesDue = $paymentManager->getProgramBalancesDue();
    $paymentItems = array();
    $paymentItemCount = 1;
    foreach($programIds as $programId) {

        if (array_key_exists($programId, $programBalancesDue)) {
        
            $programBalanceDue = $programBalancesDue[$programId];
            if($programBalanceDue > 0) {
                
                $paymentItems[] = array('program_id' => $programId, 'payment_item_amount' => $programBalanceDue);
                $paymentItemCount++;
            }
        }
    }
    
    return $paymentItems;    
    
}

?>