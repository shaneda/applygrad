 <?
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

/*
* data, logic 
*/
$err = "";
$name = "";
$department = -1;
$departments = array();
$path = "";
$active = 0;
$id = -1;

//get department
$sql = "select id, name from department ";
if($_SESSION['A_usertypeid'] != 0)
{

	for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
	{
        if ($_SESSION['A_admin_depts'][$i] == NULL) {
            continue;
        }

		if($i == 0)
		{
			$sql .= "where ";
		}else
		{
            if ($i == 1 && $_SESSION['A_admin_depts'][0] == NULL) { 
                $sql .= "where ";    
            } else {
                $sql .= " or ";
            }
		}
		$sql .= " id = ".$_SESSION['A_admin_depts'][$i];
	}
}
$sql .= " order by name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($departments, $arr);
}

if(isset($_POST['txtrevgroupId']))
{
	$id = htmlspecialchars($_POST['txtrevgroupId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$department = htmlspecialchars($_POST['txtDepartment']);

	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update revgroup set
			name='".$name."',
			department_id=".$department."
			where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			echo $sql . "<br>";
		}
		else
		{
            
            if ($_SESSION['A_usertypeid'] == 3) {
                $groupType = 2; // ranking group
            } else {
                $groupType = 1; // review group (default)    
            }
            
			$sql = "INSERT INTO revgroup (name, department_id, group_type) VALUES
                ('" . mysql_real_escape_string($name) . "', " 
                . intval($department) . "," . intval($groupType) . ")";
			mysql_query($sql);
			$id = mysql_insert_id();
            
            // Automatically add ranking group creator as round 2 member.
            if ($id && $_SESSION['A_usertypeid'] == 3) {
                $query = "INSERT INTO lu_reviewer_groups (reviewer_id, department_id, group_id, round)
                    VALUES (" . intval($_SESSION['A_userid']) . ", " . intval($department) 
                    . " , " . intval($id) . ", 2)";
                mysql_query($query);
            }
		}

        if ($_SESSION['A_usertypeid'] == 3) {
            header("Location: home.php");
            
        } else {
            header("Location: reviewGroups.php");
        }

        exit;
	}
}
$sql = "SELECT id,name, department_id FROM revgroup where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$department = intval($row['department_id']);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Group';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br />
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Edit Group <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<span class="errorSubtitle"><?=$err?></span>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtrevgroupId" type="hidden" id="txtrevgroupId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>        </td>
      </tr>
      <tr>
        <td align="right"><strong>Department:</strong></td>
        <td class="tblItem"> <? showEditText($department, "listbox", "txtDepartment", $_SESSION['A_allow_admin_edit'],true,$departments,true,30); ?></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>