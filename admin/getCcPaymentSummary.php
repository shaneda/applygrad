<?php
header("Cache-Control: no-cache");

include("../inc/config.php");

// TEMPORARY for testing
/*
$db = 'gradAdmissions2008Test';
$db_host = 'banshee.srv.cs.cmu.edu';
$db_password = 'onlyread';
$db_username = 'readonly';
*/

include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once '../classes/DB_Applyweb/class.DB_Applyweb_Table.php';
include_once '../classes/DB_Applyweb/Table/class.DB_CcTransaction.php';
include_once '../classes/DB_Applyweb/Table/class.DB_CcTransactionSummary.php';
include_once '../classes/DB_Applyweb/Table/class.DB_CcTransactionDetail.php';
include_once '../classes/class.CcTransaction.php';
include_once '../classes/DB_Applyweb/Table/class.DB_CcPaymentStatus.php';
include_once '../classes/DB_Applyweb/Table/class.DB_Payment.php';
include_once '../classes/DB_Applyweb/Table/class.DB_PaymentItem.php';
include_once '../classes/class.Payment.php';
include_once '../classes/class.PaymentManager.php';

$requestPaymentId = filter_input(INPUT_GET, 'paymentId', FILTER_VALIDATE_INT);
$requestApplicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT);
if (!$requestPaymentId && !$requestApplicationId) {
    exit;
}

$ccPayments = array();
if ($requestApplicationId) {

    $paymentManager = new PaymentManager($requestApplicationId);
    $payments = $paymentManager->getPayments();
    foreach ($payments as $paymentId => $payment) {
        if ($payment['payment_type'] == '2') {
            $ccPayments[$paymentId] = new Payment($paymentId);    
        }    
    }   

} elseif ($requestPaymentId) {

    $ccPayments[$requestPaymentId] = new Payment($requestPaymentId);    
}

$DB_Applyweb = new DB_Applyweb();
$ccTransactions = array();
foreach (array_keys($ccPayments) as $ccPaymentId) {

    $transactionQuery = "SELECT cc_id 
    FROM cc_transaction 
    WHERE payment_id = " . $ccPaymentId . "
    ORDER BY cc_id";
    $transactionRecords = $DB_Applyweb->handleSelectQuery($transactionQuery);    
    
    foreach ($transactionRecords as $transactionRecord) {
        $ccId = $transactionRecord['cc_id'];
        $ccTransactions[$ccPaymentId][$ccId] = new CcTransaction($ccId);    
    }
}

ksort($ccPayments);

$transactionCount = $authCount = $settleCount = 0;
$totalAuth = $totalSettle = $totalCredit = 0;

echo '<br>';
foreach ($ccPayments as $ccPaymentId => $ccPayment) {
    
    echo '<b>Payment ' . $ccPaymentId . ' | $' . $ccPayment->getAmount(); 
    echo ' | ' . $ccPayment->getIntentDate() . '</b><br><br>';

    ob_start();
    if (array_key_exists($ccPaymentId, $ccTransactions)) {
        
        $transactionCount++;
    
        echo '<ul>';
        foreach ($ccTransactions[$ccPaymentId] as $ccTransaction) {
            
            echo '<li>Transaction ' .  $ccTransaction->ccId . ' | ';
            echo $ccTransaction->cardholderNameFirst . ' ' . $ccTransaction->cardholderNameLast;
            
            echo '<ul>';
            foreach ($ccTransaction->summaryRecords as $summaryRecord) {
                
                echo '<li>';
                if ($summaryRecord['auth_code'] == 1 && $summaryRecord['auth_amount'] > 0) {
                    
                    $totalAuth += $summaryRecord['auth_amount'];
                    $authCount++;
                    
                    if ($summaryRecord['settle_code'] == 1 
                        && $summaryRecord['settle_amount'] == $summaryRecord['auth_amount']) {
                    
                        $totalSettle += $summaryRecord['settle_amount'];
                        $settleCount++;
                            
                        echo ' paid $' . $summaryRecord['auth_amount']; 
                                
                    } elseif ($summaryRecord['settle_code'] == 1 && $summaryRecord['settle_amount'] > 0) {
                    
                        $totalSettle += $summaryRecord['settle_amount'];
                        $settleCount++;
                        
                        echo ' authorized $' . $summaryRecord['auth_amount'];
                        echo  ', paid $' . $summaryRecord['settle_amount'];    
                        
                    } else {
                        
                        echo ' authorized $' . $summaryRecord['auth_amount'] . ', settle failed';
                    }
                    
                } elseif ($summaryRecord['credit_amount'] > 0) {
                    
                    $totalCredit += $summaryRecord['credit_amount'];
                    
                    echo ' credited $' . $summaryRecord['credit_amount'];
                        
                } else {
                    
                    $creditAttempt = FALSE;
                    foreach ($ccTransaction->detailRecords as $detailRecord) {
                        if ($detailRecord['transaction_type'] == 'credit') {
                            $creditAttempt = TRUE;    
                        }
                    }
                    
                    if ($creditAttempt) {
                        echo ' credit failed';    
                    } else {
                        echo ' authorization failed';
                    }
                }
                echo ' | ' . $summaryRecord['report_date'];
                echo ' ' . $summaryRecord['time'];
                
                echo '<ul>';
                foreach ($ccTransaction->detailRecords as $detailRecord) {
                
                    if ($detailRecord['report_date'] == $summaryRecord['report_date']) {
                    
                        echo '<li>' . $detailRecord['transaction_type'] . ' | $';
                        echo $detailRecord['item_price_each'] . ' | ';
                        echo $detailRecord['item_name'] . ' | ';
                        echo $detailRecord['item_gl_string'] . '</li>';
                    }
                }
                echo '</ul>';
                
                echo '</li>';
            }
            echo '</ul>';
            echo '</li>';  
        }
        echo '</ul>';   
    }
    
    $out = ob_get_contents();
    ob_end_clean();
    
    echo '<b><i>';
    if ($totalAuth > 0) {
        
        if ($totalCredit > 0) {
            
            $balance = $totalSettle - $totalCredit;
            echo '$' . $balance . ' balance paid; '; 
        }
        
        if ($totalSettle == 0) {
            
            echo 'CC issuer authorized, but did not pay, $' . $totalAuth;
            
        } elseif ($totalSettle >= $totalAuth) {
            
            echo 'CC issuer paid $' . $totalSettle;
            
            if ($settleCount > 1) {
                echo ' in ' . $settleCount . ' transactions';
            }
        
        } else {
        
            echo 'CC issuer authorized payment of $' . $totalAuth;
            echo 'but paid only $' . $totalSettle;
        }
        
        if ($totalCredit > 0) {
            
            $balance = $totalSettle - $totalCredit;
            echo '; $' . $totalCredit . ' credit was issued';
        }
        
    } else {
        
        if ($transactionCount > 0) {
        
            echo 'CC issuer did not authorize payment';

        } else {
            
            // Payment is pending until failure deadline has passed.
            
            // Base the failure deadline on the intent date and time
            $intentDateArray = explode(' ', $ccPayment->getIntentDate());
            $intentDate = $intentDateArray[0];
            $intentTime = $intentDateArray[1];
            $intentTimeArray = explode(':', $intentTime);
            $intentHour = intval($intentTimeArray[0]);
            
            $intentDateMidnightTimestamp = strtotime($intentDate . ' 23:59:59');
            if ($intentHour < 22) {
                
                // Intent before 10:00 PM, base failure on next day's report:
                // failed if more than 9 hours after midnight of intent day.
                $failureTimestamp = $intentDateMidnightTimestamp + 32400;
                
            } else {
                
                // Intent before 10:00 PM, base failure on day after next day's report,
                // in case ccps form not submitted before midnight:
                // failed if more than 33 hours after midnight of intent day.
                $failureTimestamp = $intentDateMidnightTimestamp + 32400;
            }
            
            $currentTimestamp = time();
            if ($ccPayment->getLastModUserId() != 0) {
                
                echo 'Payment created by administrator';    
                    
            } elseif ($currentTimestamp > $failureTimestamp) {
            
                echo 'Applicant did not complete submission to credit card processing system';
                    
            } else {
                
                echo 'Payment verification is pending';    
                
            } 
            
        }
        
        
    }
    
    echo '</b></i>';
    
    if ($transactionCount > 0) {
    
        echo '<br><br>CC Processing System Transactions:' . $out;    
    }
    
    echo '<br><br>';
}