<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
	$allowUser = true;
	$deptName = "";
	$s = -1;
	$round = 1;
	$myGroups = array();
	$allGroups = array();
	$txtSearch = "";
	$semiblind_review = 0;
	$yesCount = 0;
	$noCount = 0;
	$yesNoVotes = array();
	$areas = array();
	$showDecision = false;
	

	$id = -1;
	if(isset($_GET['id']))
	{
		$id = intval($_GET['id']);
	}
	if(isset($_GET['r']))
	{
		$round = intval($_GET['r']);
	}
	if(isset($_POST['txtSearch']))
	{
		$txtSearch = addslashes($_POST['txtSearch']);
	}
	if(isset($_GET['d']))
	{
		if($_GET['d'] == 1)
		{
			$showDecision = true;
		}
	}
	if(isset($_POST['d']))
	{
		if($_POST['d'] == 1)
		{
			$showDecision = true;
		}
	}

	if($round == 2)
	{
		//GET AREAS
		$sql = "select distinct interest.id,interest.name  from 
		application
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id	
		left outer join interest on interest.id =  lu_application_interest.interest_id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
		where lu_programs_departments.department_id = ".$id." and interest.name IS NOT NULL
		order by interest.name	
		";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		while($row = mysql_fetch_array( $result ))
		{
			$arr= array();
			array_push($arr, $row['id']);
			array_push($arr, $row['name']);
			array_push($areas, $arr);
		}
	}
	
	$sql = "select name, semiblind_review from department where id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$deptName = $row['name'];
		if(!isset($_POST['btnSemiblind']) && !isset($_POST['chkSemiblind']))
		{
			$semiblind_review = intval($row['semiblind_review']);
		}
	}
	//echo "area1 ".$semiblind_review."<br>";
	if(isset($_POST['btnSemiblind']))
	{
		//echo "value ". !intval($_POST['chkSemiblind']);
		if(intval($_POST['chkSemiblind']) == 1)
		{
			$semiblind_review = 0;
		}
		else
		{
			$semiblind_review = 1;
		}
		//echo "area2 ".$semiblind_review."<br>";

	}
	else
	{
		if(isset($_POST['chkSemiblind']))
		{
			$semiblind_review = intval($_POST['chkSemiblind']);
		}
		//echo "area3 ".$semiblind_review."<br>";
	}

	$sql = "select distinct revgroup.id, revgroup.name from revgroup 
	left outer join lu_application_groups on lu_application_groups.group_id = revgroup.id
	where revgroup.department_id=".$id . " and lu_application_groups.round =".$round." order by name ";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($allGroups, $arr);
	}

	//get my groups
	$sql = "select revgroup.id, revgroup.name from revgroup
	inner join lu_reviewer_groups on lu_reviewer_groups.group_id = revgroup.id
	where lu_reviewer_groups.reviewer_id=".$_SESSION['A_userid'] . " order by revgroup.name";
	$sql = "select id, name from revgroup where department_id=".$id . " order by name ";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($myGroups, $arr);
	}
	
	function getYesNoVotes( $deptId)
	{
		//FIRST GET ALL REVIEWS FOR THIS APPLICATION
		$ret = array();
		$tmpRet = array();
		$sql = "select id,application_id,reviewer_id, round2 from review ";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		$yes = 0;
		$no = 0;
		while($row = mysql_fetch_array( $result ))
		{
			$arr = array();
			array_push($arr, $row['id']);
			array_push($arr, $row['application_id']);
			array_push($arr, $row['reviewer_id']);
			array_push($arr, $row['round2']);
			array_push($ret, $arr);
		}
		for($i = 0; $i < count($ret); $i++)
		{
			$sql = "select department_id from lu_user_department where user_id=".$ret[$i][2] ;
			$result = mysql_query($sql) or die(mysql_error(). $sql);
			while($row = mysql_fetch_array( $result ))
			{
				if($row['department_id'] == $deptId)
				{
					//USER IS PART OF DEPARTMENT, SO SHOW THEIR REVIEW
					if($ret[$i][3] == 1)
					{
						$yes++;
					}
					if($ret[$i][3] == 0)
					{
						$no++;
					}
				}
			}
		}
		//$ret = $tmpRet;
		//return $ret;
		echo "Yes: ". $yes. "<br>";
		echo "No: ". $no. "<br>";
	}

?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>
<body>
  <!-- InstanceBeginEditable name="formRegion" -->
  <form id="form1" name="form1" action="" method="post">
  <!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="sysusersEdit.php?id=<?=$_SESSION['A_usermasterid']?>">Change Password</a>			<a href="logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "2":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "10":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" --><?=$deptName?> Admission Reviews Round <?=$round?><!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<?


	include '../inc/C_Spreadsheet_01a.php.inc';
	$spst = new Spreadsheet;


	/////////////////////////////////////
	//BUILD ARRAY FOR STATIC ADMIN GROUPS
	/////////////////////////////////////
	$query = "select distinct revgroup.name from revgroup
	left outer join lu_reviewer_groups
	where department =".$id." order by revgroup.name";
	$tempres =  mysql_query($sql) or die(mysql_error());
	$arr = array();
	$arr1 = array();
	$aAllGroups = array();
	while ($row = mysql_fetch_array($tempres))
	{
		array_push($arr, $row[0]);
	}

	array_push($arr1, array("admgroup","revgroup.name") );
	array_push($arr1, $arr);
	array_push($aAllGroups, $arr1);
	////////////////////////////////


	$sql = "select
		lu_users_usertypes.id,
		concat(users.lastname,', ', users.firstname) as Name,
		countries.name as ctzn,
		gender as M_F,

		GROUP_CONCAT(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) SEPARATOR '<BR>') as program,\n";
		if(isset($_POST['lbAoi']))
		{
			if($_POST["lbAoi"] != "")
			{
				$sql .="round(AVG(reviewSup.point),1) as facscore,\n";
			}
		}
		$sql .="GROUP_CONCAT(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  SEPARATOR '<BR>') as interest,
		GROUP_CONCAT(distinct institutes.name SEPARATOR '<BR>') as undergrad,\n ";
		
		
			///////////////////////
			/* ROUND 1 */
			///////////////////////////////////
			if($round == 1)
			{
			
			////////////////////////////
			//reviewer
			/////////////////////////////
			if(isset($_POST["lbGroup"]))
				{
					if($_POST["lbGroup"] != "")
					{
						$sql .= " GROUP_CONCAT( distinct 
						CASE lu_user_department.department_id WHEN ".$id. " THEN ";
		
							$sql .= " CASE lu_reviewer_groups.group_id WHEN ".$_POST["lbGroup"]." THEN 
							concat(SUBSTRING(users2.firstname,1,1), SUBSTRING(users2.lastname,1,1)) 
							END 
						
						END
						SEPARATOR '<BR>') ";
						
					}else
					{
						$sql .= " GROUP_CONCAT( distinct 
					CASE lu_user_department.department_id WHEN ".$id. " THEN concat(SUBSTRING(users2.firstname,1,1), SUBSTRING(users2.lastname,1,1))
					END  SEPARATOR '<BR>') ";
	
					}
				}else
				{
					$sql .= " GROUP_CONCAT( distinct 
					CASE lu_user_department.department_id WHEN ".$id. " THEN concat(SUBSTRING(users2.firstname,1,1), SUBSTRING(users2.lastname,1,1))
					END  SEPARATOR '<BR>') ";
						
				}
				$sql .= " as revr\n";
				///////////////////////
				//end reviewer
				///////////////////////
			
			
			
			/////////////////////////////////
			//GROUPS
			////////////////////////////////
			//MAY USE lu_application_groups
			$sql .=",\nGROUP_CONCAT(distinct if(revgroup.department_id = ".$id;
			if(isset($_POST["lbGroup"]))
			{
				if($_POST["lbGroup"] != "")
				{
					$sql .= " and lu_reviewer_groups.group_id=".$_POST["lbGroup"]."  ";
				}
			}
			$sql .= " , revgroup.name , '')  SEPARATOR '<BR>') as groups,\n ";
			//////////////////////////////////
			/* ROUND 2 PROMOTION? */
			//////////////////////////////////
			if(isset($_POST["lbGroup"]))
			{
				if($_POST["lbGroup"] != "")
				{
		
					$sql .= " GROUP_CONCAT( distinct 
					CASE revgroup.department_id WHEN ".$id . " THEN 
						CASE lu_reviewer_groups.group_id WHEN ".$_POST["lbGroup"]." THEN 
						concat('<input name=txtRevName type=hidden value=',users2.lastname,' />',
							CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' end, ': ', 
							SUBSTRING(if( review.supplemental_review is null, review.point,''),1,3)
						) 
						END
					END
					SEPARATOR '<BR>') ";
					
				}else
				{
					$sql .= " GROUP_CONCAT( distinct 
				CASE lu_user_department.department_id WHEN ".$id. " THEN concat('<input name=txtRevName type=hidden value=',users2.lastname,' />',
					CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' end, ': ', 
					SUBSTRING(if( review.supplemental_review is null, review.point,''),1,3))
				END  SEPARATOR '<BR>') ";

				}
			}else
			{
				$sql .= " GROUP_CONCAT( distinct 
				CASE lu_user_department.department_id WHEN ".$id. " THEN concat('<input name=txtRevName type=hidden value=',users2.lastname,' />',
					CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' end, ': ', 
					SUBSTRING(if( review.supplemental_review is null, review.point,''),1,3))
				END  SEPARATOR '<BR>') ";
					
			}
			$sql .= " as round2^\n";
			//GROUP_CONCAT(distinct CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' END  SEPARATOR '<BR>') as round2^\n";
		
			if($semiblind_review != 1)
			{
				/////////////////////////////////////
				//ranking///////////////////////////
				////////////////////////////////////
				if(isset($_POST["lbGroup"]))
				{
					if($_POST["lbGroup"] != "")
					{
						$sql .=",\n if(lu_application_groups.group_id=".$_POST["lbGroup"]." and review.round = ".$round.",  
						round( avg(  CASE lu_user_department.department_id WHEN ".$id. " THEN review.point END  ),2 ) , '') ";
					}else
					{
						$sql .= ",\n if( review.round = ".$round." ,round( avg(  CASE lu_user_department.department_id WHEN ".$id. " THEN review.point END  ),2 ),'' )";
	
					}
				}else
				{
					$sql .= ",\n if(review.round = ".$round." ,round( avg(  CASE lu_user_department.department_id WHEN ".$id. " THEN review.point END  ),2 ),'') ";
				}
				$sql .= " as rank,\n";
				
				///////////////////////////////////
				//RANKING FOR 2ND PROGRAM(ROBOTICS)
				///////////////////////////////////
				if($id == 3)
				{
					if(isset($_POST["lbGroup"]))
					{
						if($_POST["lbGroup"] != "")
						{
							$sql .=" if(lu_application_groups.group_id=".$_POST["lbGroup"]." and review.round = ".$round.",  
							round( avg(  CASE lu_user_department.department_id WHEN ".$id. " THEN review.point2 END  ),2 ) , '') ";
						}else
						{
							$sql .= "if( review.round = ".$round." ,round( avg(  CASE lu_user_department.department_id WHEN ".$id. " THEN review.point2 END  ),2 ),'' )";
		
						}
					}else
					{
						$sql .= " if(review.round = ".$round." ,round( avg(  CASE lu_user_department.department_id WHEN ".$id. " THEN review.point2 END  ),2 ),'') ";
					}
					$sql .= " as ms_rank,\n";
				}
				
				////////////////////////////////////////////////
				//end ranking//////////////////////////////////
				////////////////////////////////////////////////
				//$sql .= ",\n  round(avg( review.point),2) as ranking,   \n";
				$sql .="GROUP_CONCAT( distinct if(lu_user_department.department_id = ".$id . " and review.round=" .intval($round) . " ";
				if(isset($_POST["lbGroup"]))
				{
					if($_POST["lbGroup"] != "")
					{
						$sql .= " and lu_reviewer_groups.group_id=".$_POST["lbGroup"]."  ";
					}
				}
				$sql .= " , review.comments , '')  SEPARATOR '<BR><BR>') as comments ";
			}
		}
		///////////////////////////////////////////
		/*	END ROUND 1 */
		///////////////////////////////////////////
		
		
		//////////////////////////////////////
		/* ROUND 2 */
		///////////////////////////////////
		if($round == 2)
		{
			////////////////////////////
			//reviewer
			/////////////////////////////
			if($_SESSION["A_usertypeid"] == 3)
			{
				$sql .= " GROUP_CONCAT( distinct 
						concat(SUBSTRING(users3.firstname,1,1), SUBSTRING(users3.lastname,1,1))
						SEPARATOR '<BR>') ";
			}
			else
			{
				if(isset($_POST["lbGroup"]))
					{
						if($_POST["lbGroup"] != "")
						{
							$sql .= " GROUP_CONCAT( distinct 
							CASE lu_user_department.department_id WHEN ".$id. " THEN ";
			
								$sql .= " CASE lu_reviewer_groups2.group_id WHEN ".$_POST["lbGroup"]." THEN 
								concat(SUBSTRING(users3.firstname,1,1), SUBSTRING(users3.lastname,1,1)) 
								END 
							
							END
							SEPARATOR '<BR>') ";
							
						}else
						{
							$sql .= " GROUP_CONCAT( distinct 
						CASE lu_user_department.department_id WHEN ".$id. " THEN concat(SUBSTRING(users3.firstname,1,1), SUBSTRING(users3.lastname,1,1))
						END  SEPARATOR '<BR>') ";
		
						}
					}else
					{
						$sql .= " GROUP_CONCAT( distinct 
						CASE lu_user_department.department_id WHEN ".$id. " THEN concat(SUBSTRING(users3.firstname,1,1), SUBSTRING(users3.lastname,1,1))
						END  SEPARATOR '<BR>') ";
							
					}
				}
				$sql .= " as revr\n";
				///////////////////////
				//end reviewer
				///////////////////////
		
			////////////////////////////
			//YES/NO FOR ROUND 2////////
			////////////////////////////
			if(isset($_POST["lbGroup"]))
			{
				if($_POST["lbGroup"] != "")
				{
		
					$sql .= " ,\nGROUP_CONCAT( distinct 
					CASE revgroup.department_id WHEN ".$id . " THEN 
						/* CASE lu_reviewer_groups.group_id WHEN ".$_POST["lbGroup"]." THEN */
							concat(SUBSTRING(users2.firstname,1,1), SUBSTRING(users2.lastname,1,1),':',
								CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' end
							) 
						/* END*/
					END
					SEPARATOR '<BR>') ";
					
				}else
				{
					$sql .= " ,\nGROUP_CONCAT( distinct 
				CASE lu_user_department.department_id WHEN ".$id. " THEN concat(SUBSTRING(users2.firstname,1,1), SUBSTRING(users2.lastname,1,1),':',
					CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' end)
				END  SEPARATOR '<BR>') ";

				}
			}else
			{
				$sql .= " ,\nGROUP_CONCAT( distinct 
				CASE lu_user_department.department_id WHEN ".$id. " THEN concat(SUBSTRING(users2.firstname,1,1), SUBSTRING(users2.lastname,1,1),':',
					CASE review.round2 WHEN 1 THEN 'yes' WHEN 0 THEN 'no' end)
				END  SEPARATOR '<BR>') ";
					
			}
			$sql .= " as round2^\n";
			$sql .= ", case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote\n";
			/////////////////////////////////////////////
			if($_SESSION["A_usertypeid"]!=3)
			{
				//////////////////////////////////////////////
				$sql .=",\nGROUP_CONCAT(distinct if(revgroup.department_id = ".$id;
				if(isset($_POST["lbGroup"]))
				{
					if($_POST["lbGroup"] != "")
					{
						//$sql .= " and lu_application_groups.group_id=".$_POST["lbGroup"]."  ";
					}
				}
				$sql .= " , revgroup.name , '')  SEPARATOR '<BR>') as revgrp1 ";
			}//end not fac
			/////////////////////////////////////////////////
			$sql .=",\nGROUP_CONCAT(distinct if(revgroup2.department_id = ".$id;
			if(isset($_POST["lbGroup"]))
			{
				if($_POST["lbGroup"] != "")
				{
					$sql .= " and revgroup2.id=".$_POST["lbGroup"]."  ";
				}
			}
			$sql .= " , revgroup2.name , '')  SEPARATOR '<BR>') as revgrp2\n";
			
		}
		/* END ROUND 2 */
		/////////////////////////////////////////
		$sql .= "from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		inner join application on application.user_id = lu_users_usertypes.id
		inner join lu_application_programs on lu_application_programs.application_id = application.id
		inner join programs on programs.id = lu_application_programs.program_id
		inner join degree on degree.id = programs.degree_id
		inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
		left outer join institutes on institutes.id = usersinst.institute_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
		left outer join interest on interest.id = lu_application_interest.interest_id

		left outer join review on review.application_id = application.id 

		/*REVIEWERS - ROUND 1*/
		left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id AND review.round = 1
		left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
		left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
		/*REVIEW GROUPS - ROUND 1*/
		left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
		
		/*APPLICATION GROUPS - ROUND 1*/
		left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
		and lu_application_groups.round = 1 
		
		left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
		 ";
		if($round == 2)
		{
			/*REVIEWERS - ROUND 2*/
			$sql .= "
			left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id AND review.round = 2
			left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
			left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
		
			/*REVIEW GROUPS - ROUND 2*/
			left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
			and lu_reviewer_groups2.round = 2
			
			/*APPLICATION GROUPS - ROUND 2*/	
			left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
			and lu_application_groups2.round = 2	 
			left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
			 ";
			 if(isset($_POST["lbAoi"]))
			 {
			 	if($_POST["lbAoi"] != "")
			 	{
				 	$sql .= " left outer join review as reviewSup on reviewSup.application_id = application.id 
				 		and reviewSup.supplemental_review = ". intval($_POST["lbAoi"]);
				 	
			 	}
			 }
		}else
		{
			/*
			$sql .= " left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
			and lu_application_groups.round = 1 
			left outer join revgroup on revgroup.id = lu_application_groups.group_id  ";
			*/
		}

	$checked = "checked";
	$checkedTouch = "checked";
	$check1 = "";
	$check2 = " and (review.reviewer_id =".$_SESSION['A_userid']." ) ";

	$spst->sql = $sql;
	$spst->sqlWhere = " where application.submitted=1
		and review.supplemental_review IS NULL
		and lu_programs_departments.department_id=".$id . " ";
	if($round == 2)
	{
		//$spst->sqlWhere .= " and (revgroup1.department_id IS NULL or revgroup1.department_id = ".$id." ) ";
		//$spst->sqlWhere .= " and (revgroup2.department_id IS NULL or revgroup2.department_id = ".$id." ) ";
		$spst->sqlWhere .= " and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = ".$id."   ) 
		and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
		and (lu_application_programs.round2 != 2) ";
		$spst->having = " having round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted' ";

	}else
	{
		$spst->sqlWhere .= " and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = ".$id."   ) ";
	}

	if($txtSearch != "")
	{
		$spst->sqlWhere .= " and (
		users.firstname like '%".$txtSearch."%' 
		or users.lastname like '%".$txtSearch."%' 
		or users.email like '%".$txtSearch."%'
		or users2.firstname like '%".$txtSearch."%' 
		or users2.lastname like '%".$txtSearch."%' 
		or users2.email like '%".$txtSearch."%'
		or interest.name like '%".$txtSearch."%' "; 
		if($round == 2)
		{
			$spst->sqlWhere .= "or revgroup2.name like '%".$txtSearch."%' 
			or revgroup.name like '%".$txtSearch."%' 
			or users3.firstname like '%".$txtSearch."%' 
			or users3.lastname like '%".$txtSearch."%' 
			or users3.email like '%".$txtSearch."%'
			";
		}else
		{
			$spst->sqlWhere .= "or revgroup.name like '%".$txtSearch."%' ";
		}
		$spst->sqlWhere .= "or fieldsofstudy.name like '%".$txtSearch."%'
		or degree.name like '%".$txtSearch."%' 
		or institutes.name like '%".$txtSearch."%'
		or countries.name like '%".$txtSearch."%' ) ";
	}


	if(isset($_POST))
	{
			if(isset($_POST["lbGroup"]))
			{
				if($_POST["lbGroup"] != "")
				{
					
					if($round == 2)
					{
						$spst->sqlWhere .= " and (lu_application_groups2.group_id=".$_POST["lbGroup"]." )  ";
					}else
					{
						$spst->sqlWhere .= " and (lu_application_groups.group_id=".$_POST["lbGroup"]." )  ";
					}
				}
			}
			
			if(isset($_POST["lbAoi"]))
			{
				if($_POST["lbAoi"] != "")
				{
						$spst->sqlWhere .= " and (interest.id=".intval($_POST["lbAoi"])." )  ";
				}
			}

			if(isset($_POST["chkTouched"])){
					if($_POST["chkTouched"] == "1"){
						$spst->sqlWhere .= $check2;
						$checkedTouch = "checked";
					}
			}else{
				$checkedTouch = "";
			}
	}else{
		$spst->sqlWhere .= $check1 ;
		$spst->sqlWhere .= $check2 ;
		$checked = "checked";
		$checkedTouch = "checked";
	}

	if(isset($_SESSION['A_admin_depts']) && $_SESSION['A_usertypeid'] != 0)
	{
		$allowUser = false;
		for($i = 0; $i<count($_SESSION['A_admin_depts']); $i++)
		{
			//echo $_SESSION['A_admin_domains'][$i];
			if($_SESSION['A_admin_depts'][$i] == $id)
			{
				$allowUser = true;
			}
		}
	}

	//$spst->sqlDirective = "distinct";
	$spst->sqlGroupBy = " users.id  ";
	$spst->sqlOrderBy = "users.lastname,users.firstname";
	if(isset($_POST["lbAoi"]))
	{
		if($_POST["lbAoi"] != "")
		{
				$spst->sqlOrderBy = "facscore desc";
		}
	}
	
	
	$spst->multiEdit = 0;
	//$spst->counts = "admgroup";
	//$spst->sortfields = "reviewer";
	
	if($round == 2)
	{
		$spst->staticfields = $aAllGroups;
		if($_SESSION['A_usertypeid'] == 3)
		{
			$spst->editPage = "userroleEdit_student_formatted.php?v=3&r=2&d=".$id;
		}
		else
		{
			if($showDecision == true)
			{
				$spst->editPage = "userroleEdit_student_formatted.php?v=2&r=2&d=".$id."&showDecision=1";
			}else
			{
				$spst->editPage = "userroleEdit_student_formatted.php?v=2&r=2&d=".$id;
			}
		}
	}else
	{

		$spst->editPage = "userroleEdit_student_formatted.php?v=2&r=1&d=".$id;

	}
	

		$listbox = "<select name='lbGroup' id='lbGroup'>\n";
		$listbox .="<option value=''>All Groups</option>\n";
	
		for($i = 0; $i < count($allGroups); $i++ )
		{
			$selected = "";
			if(isset($_POST["lbGroup"]))
			{
				if($allGroups[$i][0] == $_POST["lbGroup"])
				{
					$selected = "selected";
				}
			}
			$listbox .="<option value='".$allGroups[$i][0]."' ".$selected.">".$allGroups[$i][1]."</option>\n";
		}
	  	$listbox .="</select>\n";
		$listbox .="<input name='btnGroupRefresh' type='button' value='Refresh' onClick=\"javascript: form1.action = '',form1.target = '_self', document.form1.submit();\"   />\n";
		$spst->special = "<br>See only applicants assigned to selected group. ".$listbox;
	if($_SESSION['A_usertypeid'] != 3)
	{		
		$spst->special .= "<br>See only applicants that you reviewed.<input name='chkTouched' id='chkTouched' type='checkbox' value='1' ".$checkedTouch." onClick=\"javascript: form1.action = '',form1.target = '_self',document.form1.submit();\">\n";
		$spst->special .= "<br><input name='chkSemiblind' id='chkSemiblind' type='hidden' value='".$semiblind_review."'>
			<input name='btnSemiblind' id='btnSemiblind' type='Submit' value='Toggle Semi-blind Review' onClick=\"javascript: form1.action = '',form1.target = '_self'\" >\n";
	}//end not faculty	
		$spst->special .= "<br><strong>For an accurate group count, deselect 'reviewer' from the selected fields list.</strong>\n";
		$spst->special .= "<br>Click <strong><a href='applicants_groups_graph.php' target='_blank'>here</a></strong> for a breakdown of group assignments.\n";
	
	
	$spst->special .= "<br>Search:<input name='txtSearch' id='txtSearch' type='textbox' value='".stripslashes($txtSearch)."' class='tblItem'><input name='btnSearch1' id='btnSearch1' type='Submit' class='tblItem'>\n";
	if($round == 2)
	{
		$listbox = "<select name='lbAoi' id='lbAoi'>\n";
		$listbox .="<option value=''>All Areas</option>\n";
	
		for($i = 0; $i < count($areas); $i++ )
		{
			$selected = "";
			if(isset($_POST["lbAoi"]))
			{
				if($areas[$i][0] == $_POST["lbAoi"])
				{
					$selected = "selected";
				}
			}
			$listbox .="<option value='".$areas[$i][0]."' ".$selected.">".$areas[$i][1]."</option>\n";
		}
		$listbox .="</select>\n";
		$listbox .="<input name='btnAreasRefresh' type='button' value='Refresh' onClick=\"javascript: form1.action = '',form1.target = '_self', document.form1.submit();\"   />\n";
		$spst->special .= "<br>See only applicants from the selected research area. ".$listbox;
	}
	
	if($_SESSION['A_usertypeid'] != 3)
	{	
		$spst->special .= getYesNoVotes($id);
	}
	if($allowUser == true)
	{
		echo "<strong>Welcome ".$_SESSION['A_firstname'].". ";
		/*		
		if(count($myGroups) > 0)
		{
			echo "You are a member of group(s) ";
		}else
		{
			echo "You are not a member of any group.";
		}
		for($i = 0; $i < count($myGroups); $i++)
		{
			if($i > 0 && $i < count($myGroups))
			{
				echo ", ";
			}
			echo $myGroups[$i][1];
		}
		*/
		echo "</strong>";
		echo "<br>";
		if($_SESSION['A_usertypeid'] == 3)
		{
			?>
			<br><div style="width: 600px; ">
			Your comments are very important and are used by both the admissions committee and the department head to decide whether or not to offer admission to the student.
			<br><br>
			Review as many applicants as possible in your areas of interest. You can select applicants who have indicated an interest in a specific research area using the drop-down menu below.
			<br><br>
			Please complete the review form at the end of each application. Be sure to indicate at least your top three choices. Discuss with your colleagues and enter a score that reflects the strength of the applicant  within a given research area. These individual scores will be averaged to rank order the applicants within the selected area.
			</div>			
			<?
			$spst->showFilter = false;
		}
		//echo "<br><br>".$spst->sql . $spst->sqlWhere." ".$spst->sqlGroupBy."<br><br>";
		mysql_query("SET group_concat_max_len = 4096");
		if($_SESSION["A_usertypeid"] == 3)
		{
			$spst->hideColumns = array('round2');
		}
		$spst->doSpreadSheet();
	}else
	{
		echo "You are not authorized to view this page";
	}
?>


	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
