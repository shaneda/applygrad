<?PHP
$startTime = microtime(TRUE);
ini_set('memory_limit', '64M');

// standard includes
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include '../inc/specialCasesAdmin.inc.php';

// DB_Applyweb
require "../classes/DB_Applyweb/class.DB_Applyweb.php";
$db_applyweb = new DB_Applyweb();  
                    
// Include the group, filter setup, and semiblind functions.
include '../inc/recyclablesListFunctions.inc.php';

// Include the data/datagrid classes.      
include "../classes/class.RecyclablesListData.php";
include "../classes/Structures_DataGrid/class.RecyclablesList_DataGrid.php";

// column filters
if ( isset($_REQUEST['columnFilterSelectValue']) )
{    
    $columnFilterSelectValue = $_REQUEST['columnFilterSelectValue'];
} else {
    $columnFilterSelectValue = '';    
}
if ( isset($_REQUEST['columnFilterValue']) )
{    
    $columnFilterValue = $_REQUEST['columnFilterValue'];
} else {
    $columnFilterValue = '';    
}

if ( isset($_REQUEST['tableSortList']) )
{    
    $tableSortList = $_REQUEST['tableSortList'];
} else {
    $tableSortList = '';    
}  

// search text
if ( isset($_POST['search_text']) ) $search_text = $_POST['search_text'];
else $search_text = ""; 

// Selected columns.
$selectedColumns = array();
if ( isset($_REQUEST['showColumns']) ) {
    $selectedColumns = $_REQUEST['showColumns'];    
}                                               

// Get the departments
$selectedDepartmentId = filter_input(INPUT_GET, 'selectedDepartmentId', FILTER_VALIDATE_INT);
$departments = getDepartments();

/*
* Set up header variables and include the standard page header.
*/
$pageTitle = 'Review Applications';

$pageCssFiles = array(
    '../css/reviewApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.tablesorter.js',
    '../javascript/recycleApplications.js'
    );
include '../inc/tpl.pageHeader.php';
?>

<div style="margin: 10px;">
    <span class="title">
        <span style="font-size: 18px;">Recyclable Applications</span>  
    </span>
</div>

<div style="margin: 10px; float: left;">
<form name="selectDepartment" action="" method="get">
    <select name="selectedDepartmentId">
    <?php
    foreach ($departments as $departmentId => $department)
    {
        $selected = '';
        if ($departmentId == $selectedDepartmentId)
        {
            $selected = 'selected';
        }
        
        echo '<option value="' . $departmentId . '"' . $selected . '>';
        echo $department['name'];
        echo '</option>'; 
    }
    ?>
    </select>
    <?php 
    makeHiddenShowColumns( $selectedColumns ); 
    if ($selectedDepartmentId)
    {
        $selectDepartmentOnclick = 'onclick="javascript:return confirm(\'Unsaved selections will be lost\')"';    
    }
    else
    {
        $selectDepartmentOnclick = '';
    }
    ?>
    <input type="submit" value="Select Department" <?php echo $selectDepartmentOnclick; ?>  />
</form>
</div>

<?php
/*
* Stop with department menu if no selection has been made.
*/
$selectedDepartmentName = NULL;
if ($selectedDepartmentId)
{
    $selectedDepartmentName = $departments[$selectedDepartmentId]['name'];
}
else
{
    include '../inc/tpl.pageFooter.php';
    exit;
}

/*
* Get the latest period. Exit if no period available.
*/
$periodId = NULL;
$latestPeriod = getLatestPeriod($selectedDepartmentId);
if (isset($latestPeriod[0]['period_id']))
{
    $periodId = $latestPeriod[0]['period_id'];    
}
else
{
?>
    <p style="font-weight: bold;"><?php echo $selectedDepartmentName; ?> has no recyclable applications.</p> 
<?php
    exit;  
}

/*
* Handle selections
*/
$postedApplicationIds = filter_input(INPUT_POST, 'applicationId', FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY);
$postedDepartmentId = filter_input(INPUT_POST, 'selectedDepartmentId', FILTER_VALIDATE_INT);
if ($postedDepartmentId)
{
    saveRecyclableSelections($_SESSION['A_usermasterid'], $_SESSION['A_userid'], 
        $postedDepartmentId, $periodId, $postedApplicationIds);
}
$selectedApplications = getRecyclableSelections($_SESSION['A_userid'], $selectedDepartmentId, $periodId);

/*
* Get the appropriate datagrid.
*/
$reviewList = new RecyclablesListData($selectedDepartmentId, $selectedDepartmentName);
$baseArray = $reviewList->getData($periodId, $search_text);    
$dataGrid = new RecyclablesList_DataGrid($search_text, $selectedColumns);
$dataGrid->bind($baseArray, array() , 'Array');
$recordCount = $dataGrid->getRecordCount();

// Get the datagrid columns.
$columns = $dataGrid->getColumns();

// set filter columns
$filterColumns = setFilterColumns($columns);

// Use datagrid columns as selected if not sent in REQUEST
// or if toggling semiblind
if (empty($selectedColumns)) {
    foreach ($columns as $column) {
        $selectedColumns[] = $column->fieldName;    
    }  
}

// Set the form action href
$formActionHref = $_SERVER['PHP_SELF'] . '?selectedDepartmentId=' . $selectedDepartmentId;

?>        
        <div id="refreshPage">
            <!--
			<form action="<?php echo $formActionHref; ?>" method="POST" name="refreshForm" id="refreshForm">
				<input type='hidden' name='search_text' value='<?= $search_text ?>'>	
                <input type='hidden' id='columnFilterSelectValue' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                <input type='hidden' id='columnFilterValue' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
                <input type='hidden' id='tableSortList' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                <?php makeHiddenShowColumns( $selectedColumns ); ?>
			</form>	
            -->	
		</div>
<!-- end b -->

<div id="displayOptions">

	<h3>Display Options</h3>
	<table border="0" cellpadding="0" cellspacing="1">   
        
        <tr>
            <!-- PLB added column picker -->
            <td rowspan="2" align="right" valign="top">
            <b>Columns:</b>
            </td>
            <td rowspan="2" valign="top">
            
            <form action="<?php echo $formActionHref; ?>" method="POST" name="columnsForm" id="columnsForm">
                <input type='hidden' name='search_text' value='<?= $search_text ?>'>
                <input type='hidden' id='columnFilterSelectValue' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                <input type='hidden' id='columnFilterValue' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
                <input type='hidden' id='tableSortList' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 

            <?php
            $requiredColumns = $dataGrid->getRequiredColumns();
            foreach ($requiredColumns as $field => $label) {
                echo <<<EOB
                <input type="hidden" name="showColumns[]" value="{$field}" />       
EOB;
            }
            
            $selectableColumns = $dataGrid->getSelectableColumns(); 
            foreach ($selectableColumns as $field => $label) {
                
                $checked = '';
                if ( $dataGrid->getColumnByField($field) ) {
                    $checked = 'checked';
                }
                
                echo <<<EOB
                <span style="white-space:nowrap;">
                <input type="checkbox" name="showColumns[]" 
                    value="{$field}" {$checked}/>{$label}
                </span>        
EOB;
            }
            
            ?>
            <br/>
            <input type='button' name='changeColumns' value='Change Columns' onclick="javascript:if(confirm('Unsaved selections will be lost')) { this.form.submit(); } else { return false; }">
            <?php
            $defaultColumnsHref = $_SERVER['PHP_SELF'] . '?selectedDepartmentId=' . $selectedDepartmentId;
            if ($columnFilterSelectValue) {
                $defaultColumnsHref .= '&columnFilterSelectValue=' . $columnFilterSelectValue;    
            }
            if ($columnFilterValue) {
                $defaultColumnsHref .= '&columnFilterValue=' . urlencode($columnFilterValue);    
            }
            echo '&nbsp;&nbsp;<a href="' . $defaultColumnsHref .'" onClick="javascript:if(confirm(\'Unsaved selections will be lost\')) { document.defaultColumnsForm.submit(); return false; } else { return false; }">Restore default columns</a>'; 
            ?>
            </form>
            
            <form name="defaultColumnsForm" id="defaultColumnsForm" action="<?php echo $formActionHref; ?>" method="POST">
                <input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
                <input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                <input type='hidden' name='search_text' value='<?= $search_text ?>'>
            </form>
            </td>
            
            
		</tr>
			
	</table>
</div>

<!-- APPLICANTS TABLE
  ---------------------------------------------------------------------------------  -->
 <script type="text/javascript">
    document.body.className = "js";
</script> 
 
 
 <div id="applicants">


  
 	<!-- PLB uncommented 01/12/09 -->
 	<?PHP
		if ( $search_text != "" )
		{
		// include clear search button
	?>
	<div id="clearSearch">				
		<form action="<?php echo $formActionHref; ?>" method="POST" name="clearSearchForm" id="clearSearchForm">	
            <?php makeHiddenShowColumns( $selectedColumns ); ?>
            <input type='submit' name='search_submit' value='Clear Search' onclick="javascript:return confirm('Unsaved selections will be lost');">
		</form>	
	</div>		
	<?PHP } ?> 	
	
	<div id="search">
		<form action="<?php echo $formActionHref; ?>" method="POST" name="searchForm" id="searchForm">
			<b>Search Applications:</b>
			<input type='text' name='search_text' value='<?= $search_text ?>'>
            <?php makeHiddenShowColumns( $selectedColumns ); ?>
            <input type='submit' name='search_submit' value='Search' onclick="javascript:return confirm('Unsaved selections will be lost');">
		</form>		
	</div>  
	
<?PHP
	if ($recordCount)
	{
?> 
	<div id="filter">
		<form action="<?php echo $formActionHref; ?>" name="filterForm" id="filterForm">
			<b>Filter:</b>
			<select name='column' id='column'>";
				<?= $filterColumns ?>
			</select> 	
            <input name='filt' id='columnFilter' type='text'>
            <input type="button" id="applyFilter" name="applyFilter" value="Filter" />
            <input type="button" id="clearFilter" name="clearFilter" value="Clear Filter" />
        </form>
	</div> 					
	
	<div id="sortTip">
		<span id="tip">TIP!</span> 
		Sort multiple columns simultaneously by holding down the shift key and clicking a second, third or even fourth column header! 
	</div>	
	
    <form action="<?php echo $formActionHref; ?>" method="post" name="applicationSelectionForm" id="applicationSelectionForm">
        <input type="hidden" name="selectedDepartmentId" value="<?php echo $selectedDepartmentId; ?>" />
        <input type='hidden' id='columnFilterSelectValue' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
        <input type='hidden' id='columnFilterValue' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
        <input type='hidden' id='tableSortList' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'>
        <input type='hidden' name='search_text' value='<?= $search_text ?>'> 
        <?php makeHiddenShowColumns( $selectedColumns ); ?>
	    <div id="dataGrid">
        <div style="clear: both;">
            &nbsp;
            <b><span id="rowCount"><?php echo $recordCount; ?></span> of <?php echo $recordCount; ?></b> 
            records displayed
            &nbsp;&nbsp;
            <input type="submit" value="Save Selections" />
            &nbsp;&nbsp;
            <input type="button" value="Export Selections" id="exportSelectionsButton" />
        </div>
        <?PHP
		    $dataGrid->render();
	    ?>
        </div>
    </form>
	
 	<?PHP
		}
		else if ( $search_text != "" )
		{
	?>
 			<div id='noResults'><b>Your search - <i><?= $search_text ?></i> - did not match any documents.</b></div>		
	<?PHP		
		} 
		else 
		{
			print "<div id='noResults'><b>" . $selectedDepartmentName . " has no recyclable applications.</b></div>";
		}
	?>

<?php
$exportSelectionsFormAction = 'getDbDump.php?department_id=' . $selectedDepartmentId . '&period=' . $periodId . '&dump=applications';        
?>	
<form name="exportSelectionsForm" id="exportSelectionsForm" 
    action="<?php echo $exportSelectionsFormAction; ?>" method="post" target="_blank">
</form>

<script type="text/javascript" language="javascript">
<?php
$tableSortListArray = array_map( 'intval', explode(',', $tableSortList) );
if ( count($tableSortListArray) % 2 == 0) {
    $tableSortListJson = json_encode( array_chunk($tableSortListArray, 2) );    
} else {
    $tableSortListJson = '[]';   
}
echo 'var globalTableSortList = "' . $tableSortListJson . '";';
?>
</script>

<script type="text/javascript" src="../javascript/jquery.shiftclick.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    $('input.shiftclick').shiftClick();

    $('#selectAllApplications').click( function() {
        if ($(this).attr("checked")) {
            $('.applicationCheckbox').not('.filterHidden').attr('checked', 'checked');    
        } else {
            $('.applicationCheckbox').not('.filterHidden').attr('checked', '');    
        }
    });
    
    $('#exportSelectionsButton').click( function() {
        $('#applicationSelectionForm').attr('action', $('#exportSelectionsForm').attr('action'));
        $('#applicationSelectionForm').attr('target', '_blank');
        $('#applicationSelectionForm').submit();   
    });
});
</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php'; 


function getDepartments()
{
    global $db_applyweb;
    
    $query = "SELECT id, name
                FROM department
                WHERE department.name NOT LIKE '%archive%'
                ORDER BY department.name";
    
    $departments = $db_applyweb->handleSelectQuery($query, 'id');
    
    return $departments;
}

function getLatestPeriod($departmentId)
{
    global $db_applyweb;
    
    $periodIdQuery = "SELECT MAX(period_program.period_id) AS latest_period_id 
                        FROM period_program
                        INNER JOIN programs_unit
                            ON period_program.unit_id = programs_unit.unit_id
                        INNER JOIN lu_programs_departments
                            ON programs_unit.programs_id = lu_programs_departments.program_id
                        WHERE  lu_programs_departments.department_id = " . intval($departmentId);
    
    $periodIdArray = $db_applyweb->handleSelectQuery($periodIdQuery);   
    
    if (isset($periodIdArray[0]['latest_period_id']))
    {
        $periodQuery = "SELECT 
                                period_umbrella.period_id,
                                period_umbrella.umbrella_name,
                                period.start_date,
                                period.end_date
                            FROM period_umbrella
                            INNER JOIN period 
                                ON period_umbrella.period_id = period.period_id
                            WHERE period_umbrella.period_id = " . $periodIdArray[0]['latest_period_id'];
        
        $period = $db_applyweb->handleSelectQuery($periodQuery);
        
        return $period;
    } 
    else
    {
        return null;
    }
}

function getRecyclableSelections($luUsersUsertypesId, $departmentId, $periodId)
{
    global $db_applyweb;
    
    $query = "SELECT application_id FROM recyclable_selection
        WHERE lu_users_usertypes_id = " . intval($luUsersUsertypesId) . "
        AND department_id = " . intval($departmentId) . "
        AND period_id = " . intval($periodId);
    
    $selectionArray = $db_applyweb->handleSelectQuery($query, 'application_id');   
    
    return array_keys($selectionArray);  
}

function saveRecyclableSelections($usersId, $luUsersUsertypesId, $departmentId, $periodId, $applicationIds)
{
    global $db_applyweb;
    
    $deleteQuery = "DELETE FROM recyclable_selection
    WHERE lu_users_usertypes_id = " . intval($luUsersUsertypesId) . "
    AND department_id = " . intval($departmentId);
    
    $db_applyweb->handleUpdateQuery($deleteQuery);
    
    foreach ($applicationIds as $applicationId)
    {
        $insertQuery = "INSERT INTO recyclable_selection 
            (users_id, lu_users_usertypes_id, department_id, period_id, application_id)
            VALUES("
            . intval($usersId) . ","
            . intval($luUsersUsertypesId) . ","
            . intval($departmentId) . ","
            . intval($periodId) . ","
            . intval($applicationId) . "
            )";
            
        $db_applyweb->handleInsertQuery($insertQuery);    
    }        
}
?>