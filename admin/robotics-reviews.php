<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?

$aAllRound2 = array();//NEW FOR 2008 - ALL ROUND 2 CANDIDATES
$aReviewers = array();
$aApplicants = array();
$aColumns = array();
$fieldsSel = array();
$round2Applicants = array();


$deptId = -1; //NEW FOR 2008
$deptName = "";
$round = 1;

function get_name ($applicant_id)  {
    $nameSql = "select users.firstname, users.lastname from users
inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
inner join application on application.user_id = lu_users_usertypes.id
where application.id = ".$applicant_id;
    $nameresult = mysql_query($nameSql) or die(mysql_error());
    $row = mysql_fetch_array( $nameresult );
    $result = array($row['firstname'], $row['lastname']);
    return $result;
}

function get_rev_name ($revId) {
    $nameSql = "select users.firstname, users.lastname from users
inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
where lu_users_usertypes.id = ".$revId;
    $nameresult = mysql_query($nameSql) or die(mysql_error());
    $row = mysql_fetch_array( $nameresult );
    $result = array($row['firstname'], $row['lastname']);
    return $result;
}


function sortReviewers ($reviewers) {
    $names = array();
    $sortedList = array();
    foreach ($reviewers as $rev) {
        list($fname, $lname) = get_rev_name($rev);
        array_push($names, array($lname,$fname));
    }
    sort($names);
    foreach ($names as $name) {
        list($lname, $fname) = $name;
        $nameSql = "select lu_users_usertypes.id from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        where users.firstname = '".$fname."' and users.lastname = '".$lname."'";
        $nameresult = mysql_query($nameSql) or die(mysql_error());
        while($row = mysql_fetch_array( $nameresult )) {
            if(in_array($row['id'], $reviewers)) {
                array_push($sortedList, $row['id']);
            }
        }
    }
   
    return $sortedList;
}

function get_programs ($appID) {
    $progList = array();
    $prog = "";
    $progSql = "select degree_id from programs 
inner join lu_application_programs on lu_application_programs.program_id = programs.id
where lu_application_programs.application_id = ".$appID;
        $progresult = mysql_query($progSql) or die(mysql_error());
        while($row = mysql_fetch_array( $progresult )) {
            if(!in_array($row['degree_id'], $progList)) {
                array_push($progList, $row['degree_id']);
            }
        }
  //      DebugBreak();
        if (sizeof($progList) == 1) {
            if (in_array(2,$progList)) {
                $prog = "M.S. Only";
            }
            else
            {
               if (in_array(3,$progList)) {
                $prog = "PhD Only"; 
            }
            }
        } else
            $prog = "Both";
            return $prog;
}

//PROCESS GET/POST VARS
if(isset($_GET)) {
    if(isset($_GET['deptId'])) {
        $deptId =  $_GET['deptId'];
    }
    else
      $deptId = 3;
    if(isset($_GET['r'])) {
        $round =  $_GET['r'];
    }
    else
      $round = 2;
}
else {
      $deptId = 3;
      $round = 2; 
}
	  
$sql1 = "select application.id as app_id from application 
where application.submitted = 1 and application.id in  
(select application_id from lu_application_programs 
where lu_application_programs.program_id 
in (select  program_id from lu_programs_departments where department_id = ".$deptId."))" ;

$result = mysql_query($sql1) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    if (!in_array($row['app_id'], $aApplicants)) {
        array_push($aApplicants, $row['app_id']);
    }
}
   
$sql2 = "select * from review where review.round = ".$round." and (review.application_id in 
(select application.id as app_id from application 
where application.submitted = 1 and application.id in  
(select application_id from lu_application_programs 
where lu_application_programs.program_id 
in (select  program_id from lu_programs_departments where department_id = ".$deptId.")
)))
and review.reviewer_id in (select lu_user_department.user_id from lu_user_department
where lu_user_department.department_id = ".$deptId.")";

// get the reviewr table information 
$result = mysql_query($sql2) or die(mysql_error());
$testcount = 1;
$applicationScores = array();
while($row = mysql_fetch_array( $result ))
{
    
      $revId = $row['reviewer_id'];
      if (!in_array($revId, $aReviewers)) {
            array_push($aReviewers, $revId);
      } 
      $appIDScore = $row['application_id'];
      $phdScore = $row['point'];
      $msScore =  $row['point2'];
      $newScoreArray = array($revId, $phdScore, $msScore);
      if (!in_array($appIDScore, $applicationScores)) {
          $applicationScores[$appIDScore][] = $newScoreArray;
      }
        else
            {
                array_push($applicationScores, $newScoreArray);
            }
        $programs = get_programs($appIDScore);
}

$sql3 = "SELECT DISTINCT application.id as app_id
FROM application, lu_users_usertypes, users,
   lu_application_programs, lu_programs_departments
/*
     , review
*/
WHERE application.submitted = 1
AND lu_programs_departments.department_id = 3 AND application.user_id = lu_users_usertypes.id AND users.id = lu_users_usertypes.user_id AND application.id = lu_application_programs.application_id
AND lu_application_programs.program_id = lu_programs_departments.program_id
/*
AND review.department_id = 3
AND application.id = review.application_id */

AND lu_application_programs.round2 != 2

AND (lu_application_programs.round2 = 1
   OR application.id NOT IN (
       SELECT DISTINCT application_id
       FROM review
       WHERE round2 != 1
       AND department_id = 3
      )
   )";
   
   $result = mysql_query($sql3) or die(mysql_error());
   debugbreak();
   while($row = mysql_fetch_array( $result ))
{
//    if (!in_array($row['app_id'], $round2Applicants)) {
 //       array_push($round2Applicants, $row['app_id']);
   // }
}

$sql4 = "select * from review where review.round = ".$round." and (review.application_id in 
(select application_id as app_id from (select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2, 
                case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    /* PLB added test not to display null scores as 0 */
                    if( (review.point is null) || (review.point = 0) , '', /* begin PLB if */
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                        
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                        ) /* end PLB if */ 
                        end /* end review.fac_vote case */
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        /* PLB added test not to display null scores as 0 */
                        if( (review.point2 is null) || (review.point2 = 0), '', /* begin PLB if */
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        
                        ) /* end PLB if */
                        end  /* end review.fac_vote case */
                        separator '|') as ms_rank ,
 
  /* PLB added phd_avg_rank, ms_avg_rank  */

  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank,                       
                        
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when 3 then 
                                            review.comments
                                        end
                                    
                        end
                    
                    end
                    )
                    separator '|') as comments 
                    
                    /* PLB added search select 01/12/09 */
                     
                    
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = 19404
            AND department_id = 3
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        
        
        /* PLB added search join 01/12/09 */
         
        
               where application.submitted=1 
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)   group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname) as foo
        
        where program like '%m.s%'
)
)
and review.reviewer_id in (select lu_user_department.user_id from lu_user_department
where lu_user_department.department_id = ".$deptId.")";

$msQuery = "select application_id as app_id from (select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2, 
                case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    /* PLB added test not to display null scores as 0 */
                    if( (review.point is null) || (review.point = 0) , '', /* begin PLB if */
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                        
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                        ) /* end PLB if */ 
                        end /* end review.fac_vote case */
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        /* PLB added test not to display null scores as 0 */
                        if( (review.point2 is null) || (review.point2 = 0), '', /* begin PLB if */
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        
                        ) /* end PLB if */
                        end  /* end review.fac_vote case */
                        separator '|') as ms_rank ,
 
  /* PLB added phd_avg_rank, ms_avg_rank  */

  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank,                       
                        
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when 3 then 
                                            review.comments
                                        end
                                    
                        end
                    
                    end
                    )
                    separator '|') as comments 
                    
                    /* PLB added search select 01/12/09 */
                     
                    
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = 19404
            AND department_id = 3
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        
        
        /* PLB added search join 01/12/09 */
         
        
               where application.submitted=1 
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)   group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname) as foo
        
        where program like '%m.s%'";
        
        $result = mysql_query($msQuery) or die(mysql_error());
   while($row = mysql_fetch_array( $result ))
{
    if (!in_array($row['app_id'], $round2Applicants)) {
        array_push($round2Applicants, $row['app_id']);
    }
}
    
    
$phdQuery =  "select application_id as app_id from (select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2, 
                case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    /* PLB added test not to display null scores as 0 */
                    if( (review.point is null) || (review.point = 0) , '', /* begin PLB if */
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                        
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                        ) /* end PLB if */ 
                        end /* end review.fac_vote case */
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        /* PLB added test not to display null scores as 0 */
                        if( (review.point2 is null) || (review.point2 = 0), '', /* begin PLB if */
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        
                        ) /* end PLB if */
                        end  /* end review.fac_vote case */
                        separator '|') as ms_rank ,
 
  /* PLB added phd_avg_rank, ms_avg_rank  */

  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank,                       
                        
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when 3 then 
                                            review.comments
                                        end
                                    
                        end
                    
                    end
                    )
                    separator '|') as comments 
                    
                    /* PLB added search select 01/12/09 */
                     
                    
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = 19404
            AND department_id = 3
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        
        
        /* PLB added search join 01/12/09 */
         
        
               where application.submitted=1 
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)   group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname) as foo
        
        where program like '%ph.D. in Robotics%'"   ;     

// get the reviewr table information 
$result4 = mysql_query($sql4) or die(mysql_error());

$test = mysql_query($sql4);
$testcount = mysql_num_rows($test);
$rd2applicationScores = array();
DebugBreak();
while($row = mysql_fetch_array($result4))
{
      DebugBreak() ;
      $revId = $row['reviewer_id'];
      if (!in_array($revId, $aReviewers)) {
            array_push($aReviewers, $revId);
      } 
      $appIDScore = $row['application_id'];
      $phdScore = $row['point'];
      $msScore =  $row['point2'];
      $newScoreArray = array($revId, $phdScore, $msScore);
      DebugBreak();
      if (!in_array($appIDScore, $rd2applicationScores)) {
          $rd2applicationScores[$appIDScore][] = $newScoreArray;
      }
        else
            {
                array_push($rd2applicationScores, $newScoreArray);
            }
        $programs = get_programs($appIDScore);
}  

DebugBreak();
$aReviewers = sortReviewers($aReviewers);

// put back for production echo "fname^lname^";
echo "fname^lname^";
foreach ($aReviewers as $rev) { 
    list(,$revLName) = get_rev_name($rev) ;
    echo $revLName."^";
}
echo "program^";
foreach ($aReviewers as $rev) { 
    list(,$revLName) = get_rev_name($rev) ;
    echo $revLName."^";
}
echo "program <br>" ;


foreach ($aApplicants as $key => $value) {
    $programs = get_programs($value);
    list($fname, $lname) = get_name($value);
// put back for production    echo $fname."^".$lname."^";
    echo $fname."^".$lname."^";  
    if (array_key_exists ($value, $applicationScores)) {
    $scoreList = $applicationScores[$value] ; 
 //   $found = FALSE;
    foreach ($aReviewers as $rev) {
        $found = FALSE;
        for ($i = 0; $i < sizeof($scoreList); $i++) {
            if (in_array($rev, $scoreList[$i])) {
                echo  $scoreList[$i][1]."^";     /// phd score
                $found = TRUE;  
            }
        //    else
          //      {
             //   echo "^";
              //  }
        }
        if ($found == FALSE)  echo "^"; 
    }
 //   DebugBreak();
    echo $programs."^";
    foreach ($aReviewers as $rev) {
        $found = FALSE;
        for ($i = 0; $i < sizeof($scoreList); $i++) {
            if (in_array($rev, $scoreList[$i])) {
                echo  $scoreList[$i][2]."^";   /// ms score
                $found = TRUE;
            }
      //      else
        //        {
          //          echo "^";
            //    }
        }
        if ($found == FALSE)  echo "^";  
    }
    echo $programs."^";
    }
    else  { // no scores
        foreach ($aReviewers as $rev) {
            echo "^";
        }
        $programs = get_programs($value);
        echo $programs."^";
        foreach ($aReviewers as $rev) {
            echo "^";
        }
        $programs = get_programs($value);
        echo $programs."^";
    }
    echo "<br>";       
    
    
    
 // DebugBreak();
}

