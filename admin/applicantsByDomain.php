<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
	$allowUser = true;
	$domainName = "";
	$s = -1;
	$sType = "(all)";

	$id = -1;
	if(isset($_GET['id']))
	{
		$id = intval($_GET['id']);
	}
	if(isset($_GET['s']))
	{
		if(intval($_GET['s']) == 1)
		{
			$sType = "(submitted)";
			$s = 1;
		}else
		{
			$sType = "(unsubmitted)";
			$s = 0;
		}

	}
	$sql = "select name from domain where id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$domainName = $row['name'];
	}
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" -->
  <form id="form1" name="form1" action="" method="post">
  <!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" --><?=$domainName?> Applicants <?=$sType?><!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<?


	include '../inc/C_Spreadsheet_01a.php.inc';
	$spst = new Spreadsheet;

	$sql = "select
		lu_users_usertypes.id as id,
		concat(users.lastname, ', ',users.firstname) as name,
		users.email,
		GROUP_CONCAT(concat(' ',degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
		users_info.gender as M_F,
		if(application.sent_to_program = 1,'yes','') as cmp,\n";
		if($s == -1)
		{
			$sql .= "if(application.submitted = 1,'yes','') as sbmd,\n";
		}
		$sql .= "if(application.paid = 1,'yes','') as pd,
		if(application.waive = 1,'yes','') as wd,
		countries.name as country
		from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		left outer join application on application.user_id = lu_users_usertypes.id
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id

		";

	$spst->sql = $sql;
	$spst->sqlWhere = " where (lu_users_usertypes.usertype_id = 5) and lu_domain_department.domain_id=".$id;
	if($s != -1)
	{
	 $spst->sqlWhere .= " and application.submitted=".$s." ";
	}
	if(isset($_SESSION['A_admin_depts']) && $_SESSION['A_usertypeid'] != 0)
	{
		$allowUser = false;



		for($i = 0; $i<count($_SESSION['A_admin_domains']); $i++)
		{
//echo $_SESSION['A_admin_domains'][$i];
			if($_SESSION['A_admin_domains'][$i] == $id)
			{
				$allowUser = true;
			}
		}

	}

	//$spst->sqlDirective = "distinct";
	$spst->sqlGroupBy = "users.id";
	if($s == -1)
	{
		$spst->sqlOrderBy = "application.submitted desc, application.paid desc";
	}else
	{
		$spst->sqlOrderBy = "users.lastname,users.firstname, programs.rank";
	}
	if($_SESSION['A_usertypeid'] == 4)//auditor
	{
		$spst->editPage = "userroleEdit_student_formatted.php";
	}else
	{
		$spst->editPage = "userroleEdit_student.php";
	}
	$spst->multiEdit = 0;
	//$spst->counts = "program";
	//$spst->special = "<br>Click <strong><a href='applicants_groups_graph.php' target='_blank'>here</a></strong> for a breakdown of group assignments.";
	if($allowUser == true)
	{
		$spst->doSpreadSheet();
	}else
	{
		echo "You are not authorized to view this page";
	}
?>


	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
