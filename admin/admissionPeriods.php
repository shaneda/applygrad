<?php
include_once '../inc/config.php';
include_once '../inc/session_admin.php';

// Instantiate the page controller class.
include '../classes/controller/class.AdmissionPeriod_Controller.php';
$controller = new AdmissionPeriod_Controller();

$pageTitle = $controller->getUnitName() . " Admission Periods";

// Set variables for use in the header/footer.
$pageCssFiles = array();
$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/admissionPeriods.js');

// Include the page header.
include '../inc/tpl.pageHeader.php';
?>

<div id="breadcrumbs" style="margin-left: 0px; margin-top: 10px; margin-bottom: 10px; padding: 0px;">
    <ul style="margin-left: 0; padding-left: 0; display: inline; border: none;">
        <li style="margin-left: 0; padding-left: 2px; border: none; list-style: none; display: inline;">
            <a href="./home.php">Home</a>
        </li>
        <li style="margin-left: 0; padding-left: 2px; border: none; list-style: none; display: inline;">
            &#187; <a href="./<?php echo $controller->getUnit(); ?>s.php"><?php echo ucfirst($controller->getUnit()) . 's'; ?></a>
        </li>
        <li style="margin-left: 0; padding-left: 2px; border: none; list-style: none; display: inline;"> 
            &#187; <a href="./<?php echo $controller->getUnit(); ?>Edit.php?id=<?php echo $controller->getUnitId(); ?>"><?php echo $controller->getUnitName(); ?></a>
        </li>
        <li style="margin-left: 0; padding-left: 2px; border: none; list-style: none; display: inline;"> 
            &#187; <?php echo $pageTitle; ?>
        </li>
    </ul>
</div>

<span class="title"><?php echo $pageTitle; ?></span>
<br />
<br />

<?php
$quickForm = $controller->getQuickForm();
$dataGrid = $controller->getDataGrid();

if ($quickForm) {
?>

<table border="0" width="100%">
    <tr valign="top">
        <td width="30%">    
<?php
    echo '<a href="'
        , htmlentities($_SERVER['PHP_SELF'])
        , '?unit=' . $controller->getUnit() . '&unitId=' . $controller->getUnitId() 
        , '">&laquo Return to full list</a>';
        
    if( $quickForm->isFrozen() ) {
        echo ' &nbsp;|&nbsp; <a href="'
            , htmlentities($_SERVER['PHP_SELF'])
            , '?unit=' . $controller->getUnit() . '&unitId=' . $controller->getUnitId()
            , '&action=create">Add new period</a>';
    }
    
    echo '<br/><br/>'; 
    
    $quickForm->display();
    
?>
        </td>
        <td align="left">Recent Periods:<br/><br/>
        <?php 
        if($dataGrid) {
            $dataGrid->removeColumn( $dataGrid->getColumnByField('admissionPeriodId') );
            $dataGrid->removeColumn( $dataGrid->getColumnByField('editDeadline') );
            $dataGrid->removeColumn( $dataGrid->getColumnByField('viewDeadline') );
            $dataGrid->removeColumn( $dataGrid->getColumnByField('admissionPeriodId') );
            $dataGrid->render();    
        }
         
        ?>
        </td>
    </tr>
</table>

<?php
} else {
    
    if ($dataGrid) {

        echo '<a href="'
            , htmlentities($_SERVER['PHP_SELF'])
            , '?unit=' . $controller->getUnit() . '&unitId=' . $controller->getUnitId()
            , '&action=create">Add new period</a><br/><br/>';
        
        $dataGrid->render();
            
    }
}

include '../inc/tpl.pageFooter.php';
?>