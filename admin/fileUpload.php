<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$id = -1;
$type = -1;
$err = "";
$isvalid = false;
$returnurl = "";
$title = "";
$returnurl = "";
$filetypes = "";

if(isset($_GET['id']))
{
	if($_GET['id'] != "")
	{
		$id = intval($_GET['id']);
	}
}
if(isset($_GET['t']))
{
	if($_GET['t'] != "")
	{
		$type = intval($_GET['t']);
	}
}


switch($type)
{
	case 1:
		//transcript
		$returnurl = "uni.php";
		$title = "Colleges/Universities";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 2:
		//resume
		$title = "Resume";
		$returnurl = "resume.php";
		$title = "Resume";
		$filetypes = "PDF, Word or Text";
		break;
	case 3:
		//recommendation
		$title = "Recommendation";
		$filetypes = "PDF, Word or Text";
		break;
	case 4:
		$returnurl = "resume.php";
		$title = "Statement of Purpose";
		$filetypes = "PDF, Word or Text";
		//statement
		break;
	case 5:
		//experience
		$returnurl = "suppinfo.php";
        $title = "Experience";
		$filetypes = "PDF, Word or Text";
		break;
	case 6:
		//gre score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 7:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 8:
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		//toefl score
		break;
	case 9:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 10:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
}//end switch
    
if(isset($_FILES["file"]))
{   
    // DAS quick hack for upload of recommendation letters using (original) admin interface
    if (isset($id) && $type == 3) {
        $recsql = "select recommend.id, recommend.application_id, application.user_id as userid, lu_users_usertypes.user_id as usermasterid from recommend 
inner join application on application.id = recommend.application_id
inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
where recommend.id = ".$id;
        $recresult = mysql_query($recsql) or die(mysql_error());
        while($row = mysql_fetch_array( $recresult )) {
            $ret = handle_upload_file($type, $row['userid'],$row['usermasterid'], $_FILES["file"], $row['application_id']."_".$id );
        }
        
    } else {
	$ret = handle_upload_file($type, $_SESSION['userid'],$_SESSION['usermasterid'], $_FILES["file"],$_SESSION['appid']."_".$id );
    }
	//echo $ret;
	$err = "";
	if(intval($ret) < 1)
	{
		$err = $ret."<br>";
	}else
	{
		$sql = "";
		switch($type)
		{
			case 1:
				//transcript
				$sql = "update usersinst set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 2:
				//resume
				$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=4";
				$result = mysql_query($sql) or die(mysql_error());				
				$doUpdate = false;
				while($row = mysql_fetch_array( $result )) 
				{
					$doUpdate= true;
				} 
				if($doUpdate== true)
				{
					updateReqComplete("resume.php", 1);
				}else
				{
					updateReqComplete("resume.php", 0);
				}
				break;
			case 3:
				//recommendation
                $sql = "select submitted from recommend where id = ".$id;
                $result = mysql_query($sql) or die(mysql_error());
                while($row = mysql_fetch_array( $result )) {
                        $numSubmissions = $row['submitted'];
                    }
                $nextSubmission = $numSubmissions + 1;
                $sql = "update recommend set datafile_id =".intval($ret).", submitted= " . $nextSubmission ." where id = ".$id;
                $result = mysql_query($sql) or die(mysql_error());
                $err = "Recommendation Letter successfully uploaded";
				break;
			case 4:
				//statement
				$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=2";
				$result = mysql_query($sql) or die(mysql_error());				
				$doUpdate = false;
				while($row = mysql_fetch_array( $result )) 
				{
					$doUpdate= true;
				} 
				if($doUpdate== true)
				{
					updateReqComplete("resume.php", 1);
				}else
				{
					updateReqComplete("resume.php", 0);
				}
				break;
			case 5:
				//experience
				$sql = "delete from experience where application_id=".intval($_SESSION['appid'])." and experiencetype=".intval($id);
				$result = mysql_query($sql) or die(mysql_error());
				$sql = "insert into experience(application_id, datafile_id, experiencetype )values(".intval($_SESSION['appid']).", ".intval($ret).", ".intval($id).")" ;
				break;
			case 6:
				//gre score
				$sql = "update grescore set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 7:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 8:
				$sql = "update gresubjectscore set datafile_id =".intval($ret)." where id = ".$id;
				//toefl score
				break;
			case 9:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 10:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
		}//end switch
		
	
		if($sql != "")
		{
			$result = mysql_query($sql) or die(mysql_error());
		}
		//echo $sql;
		header("Location: ".$returnurl);
		
	} 

}
  
  
  
  
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" -->
		<form action="" method="post" name="form1" enctype="multipart/form-data" id="form1"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" --><span class="title"><?=$title ?> File Upload </span><!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><span class="errorSubtitle"><?=$err;?></span><br />
<span class="tblItem">Your <?=$title ?> file must be submitted in <?=$filetypes?> format. Any other format will not be accepted. The maximum file size for your transcripts is 3MB.</span><br />
<!-- <span class="tblItem">Your file can be in image or pdf format. The maximum file size for your transcripts is 3MB</span><br> -->
            <br />
			<input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="30000000" />
			<input type="hidden" name="post_max_size" id="post_max_size" value="30000000" />
			<input type="hidden" name="upload_max_filesize" id="upload_max_filesize" value="30000000" />
            <input name="file" type="file" class="tblItem" maxlength="255">
            <? showEditText("Upload", "button", "btnSubmit",true); ?>
            <!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
