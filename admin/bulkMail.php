<?php
/*
* Shared includes 
*/
$exclude_scriptaculous = TRUE;
include_once "../inc/session_admin.php";
include_once '../inc/functions.php';
define("CLASS_DIR", "../classes/");
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb_Table.php';
include '../inc/specialCasesAdmin.inc.php';
include '../inc/unitPeriodIncludes.inc.php';
include '../inc/bulkMailFunctions.inc.php';

/*
* GET variables
*/
$unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);

/*
* POST variables
*/
$selectionSubmit = filter_input(INPUT_POST, 'selectionSubmit', FILTER_SANITIZE_STRING);
$confirmSelectionSubmit = filter_input(INPUT_POST, 'confirmSelectionSubmit', FILTER_SANITIZE_STRING);
$cancelSelectionSubmit = filter_input(INPUT_POST, 'cancelSelectionSubmit', FILTER_SANITIZE_STRING);
$contentId = filter_input(INPUT_POST, 'contentId', FILTER_SANITIZE_STRING);
$allUsersSelected = filter_input(INPUT_POST, 'allUsersSelected', FILTER_VALIDATE_BOOLEAN);
$selectedUsers = filter_input(INPUT_POST, 'selectedUsers', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Send Bulk Email';

$pageCssFiles = array(
    '../css/administerApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/administerApplications.js',
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';    
?>
<div id="pageContentMainDiv" style="margin: 5px;">
<div style="margin: 10px;">
<?php
echo mainHeading($periodId);

if ($selectionSubmit && !$contentId)
{
?>
    <br><span class="errorSubtitle">You must select an email template</span>
<?php
}

if ($selectionSubmit && !($allUsersSelected || is_array($selectedUsers)))
{
?>
    <br><span class="errorSubtitle">You must select one or more users</span>
<?php
}

if ($contentId && $selectionSubmit && ($allUsersSelected || is_array($selectedUsers)))
{
    include '../inc/tpl.bulkMailConfirmSelection.php';    
}
elseif ($contentId && $confirmSelectionSubmit && ($allUsersSelected || is_array($selectedUsers)))
{
    include '../inc/tpl.bulkMailSend.php';    
} 
else
{
    include '../inc/tpl.bulkMailSelection.php';    
}
?>

<?php
/*     
?>
<form name="selectionForm" id="selectionForm" action="" method="post">
<input type="hidden" name="allUsersSelected" id="allUsersSelected" value="0" /> 

<div style="clear: both; float: right;">
Mail Template: 
<?php
echo templateMenu($unit, $unitId, $contentId);
?>
<input type="submit" value="Send Mail to Selected Users">
</div>

<br><br>
<div style="clear: both;">
<?php $datagrid->render(); ?>
</div>

</form>
<?php
*/     
?>

</div>
</div>

<script type="text/javascript" src="../javascript/jquery.shiftclick.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
    $('input.shiftclick').shiftClick();
    
    $('#selectAllUsers').click( function() {
        if ($(this).attr("checked")) {
            $('.userCheckbox').attr('checked', 'checked');
            $('#allUsersSelected').attr('value', '1');        
        } else {
            $('.userCheckbox').attr('checked', '');
            $('#allUsersSelected').attr('value', '0');    
        }
    });
   
});
</script> 

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function mainHeading($periodId)
{
    global $pageTitle;
    $period = new Period($periodId);
    $unit = new Unit( $period->getUnitId() );
        
    $submissionPeriods = $period->getChildPeriods(2);
    if (count($submissionPeriods) > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    if ($periodName) {
        $application_period_display = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $application_period_display = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $application_period_display = $periodDates;     
    }
    
    return '<span class="title">' . $unit->getName() . '<br/>'
        . $application_period_display . '<br/> 
        <span style="font-size: 18px;">' . $pageTitle . '</span>  
        </span>'; 
}
?>