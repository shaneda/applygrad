<? include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
} 

	$allowUser = true;
	$departmentName = "";
	$s = -1;
	$sType = "(all)";

	$id = -1;
	if(isset($_GET['id']))
	{
		$id = intval($_GET['id']);
	}

	$sql = "select name from department where id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$departmentName = $row['name'];
	}

    $sql = "SELECT
        lu_users_usertypes.id as id,
        users.lastname, 
        users.firstname,
        users.email,
        institutes.name as undergrad,
        interest.name as interest,
        GROUP_CONCAT(concat(' ',degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
        lu_application_programs.faccontact,
        lu_application_programs.stucontact
        from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        left outer join application on application.user_id = lu_users_usertypes.id";
        if ($periodId) {
            $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
        }
        $sql .= " left outer join application_decision on application_decision.application_id = application.id
        left outer join programs on programs.id = application_decision.program_id
        left outer join degree on degree.id = programs.degree_id
        left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = application_decision.program_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id  
        LEFT OUTER JOIN lu_application_programs ON 
            (application_decision.application_id = lu_application_programs.application_id
            AND application_decision.program_id = lu_application_programs.program_id) 
        left outer join lu_application_interest on 
            (lu_application_interest.app_program_id = lu_application_programs.id 
            and (lu_application_interest.choice is null or lu_application_interest.choice=0) )
        left outer join interest on interest.id = lu_application_interest.interest_id 
        WHERE
        application.submitted=1
        and application_decision.admission_status = 2
        and lu_programs_departments.department_id = " . $id;
        if ($periodId) {
            $sql .= " AND period_application.period_id = " .  $periodId;
        } 
        $sql .= " GROUP BY lu_users_usertypes.id 
        UNION ";
        
	$sql .= " select
		lu_users_usertypes.id as id,
		users.lastname, 
		users.firstname,
		users.email,
		institutes.name as undergrad,
		interest.name as interest,
		GROUP_CONCAT(concat(' ',degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
		lu_application_programs.faccontact,
		lu_application_programs.stucontact
		from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		left outer join application on application.user_id = lu_users_usertypes.id";
        // PLB added period join 1/7/10
        if ($periodId) {
            $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
        }          
		$sql .= " left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
		left outer join institutes on institutes.id = usersinst.institute_id
		left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id 
			and (lu_application_interest.choice is null or lu_application_interest.choice=0)
		left outer join interest on interest.id = lu_application_interest.interest_id
		where
	application.submitted=1
	and lu_application_programs.admission_status=2
	and lu_programs_departments.department_id=".$id;
    
    // PLB added period where 1/7/10
    if ($periodId) {
        $sql .= " AND period_application.period_id = " .  $periodId;
    } 
    
    /*
    $sql .= " group by lu_users_usertypes.id
	order by users.lastname,users.firstname";
    */
    $sql .= " group by lu_users_usertypes.id
    order by lastname, firstname";
	$result = mysql_query($sql) or die(mysql_error());
	echo "lastname,firstname,email,undergrad,interest\n";
	while($row = mysql_fetch_array( $result ))
	{
		echo '"' . $row['lastname'] . '","'
        . $row['firstname'] . '","'
        . $row['email'] . '","'
        . $row['undergrad'] . '","'
        . $row['interest'] . '"';
        echo "\n";
	}
?>