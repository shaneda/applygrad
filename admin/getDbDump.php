<?php 

// Start the output buffer to catch stray output from includes, etc. 
// Leave on until ready to finish.
ob_start(); 

include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../inc/specialCasesAdmin.inc.php';

// Check GET for department and dump type
if ( isset($_GET['department_id']) ) {
    $department_id = $_GET['department_id']; 
} else {             
    $department_id = 1;
}

if ( isset($_GET['period']) ) {
    $period = $_GET['period']; 
} else {
    $period = NULL;
}

if ( isset($_GET['dump']) ) {
    $dump = $_GET['dump']; 
} else {
    $dump = "applications";
}

global $app_list;
$app_list = array();

global $states;
$states = array();

global $visatypes;
$visatypes = array();

global $ethnicity;
$ethnicity = array();

$isEnglishDepartment = false;
if (isEnglishMaDepartment($department_id) 
    || isEnglishPhdDepartment($department_id))
{
    $isEnglishDepartment = true;    
}

function create_dump_file( $filename )
{
//  echo "create_dump_file\n<BR>";

  $handle = fopen($filename, 'w+');
  return $handle;
}

function close_dump_file( $fid )
{
//  echo "close_dump_file\n<BR>";
  fclose($fid);
}

function get_applicant_list( $dept_code )
{
  $list = array();

  return $list;
}

function print_header_labels($fid)
{
    $headerline =  'SPACER^appid^'
        . ' email^title^firstname^middlename^lastname^organization^'
        . ' gender^dob^'
        . ' address_cur_street1^address_cur_street2^address_cur_street3^address_cur_street4^'
        . ' address_cur_city^CurrentState^address_cur_pcode^CurrentCountry^'
        . ' address_cur_tel^address_cur_tel_mobile^address_cur_tel_work^'
        . ' address_perm_street1^address_perm_street2^address_perm_street3^address_perm_street4^'
        . ' address_perm_city^PermState^address_perm_pcode^PermCountry^'
        . ' address_perm_tel^address_perm_tel_mobile^address_perm_tel_work^homepage^'
        . ' CountryofCitz^iso_code^visa status^native_tongue^ethnicity^'
        // PLB added race 1/29/10
        . 'race^'        
        . ' PIER^WomenAtITFellowship^'
        
        // added portfolio, video essay
        . 'portfolio_url^video_essay_url^video_essay_access_code^'

        . 'SPACER^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . ' program_name^aoi_1^aoi_2^aoi_3^admit_status^'

        . 'SPACER^'
        . 'gre_testdate^verbalscore^verbalpercentile^quantitativescore^quantitativepercentile^analyticalscore^'
        . 'analyticalpercentile^analyticalwritingscore^analyticalwritingpercentile^'

        . 'SPACER^'
        . 'gresubject_testdate^name^score^percentile^ '

        . 'SPACER^'
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^'
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^'  
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^';

        // PLB added headings for ielts
    $headerline .=  'SPACER^'
        . 'IELTS_testdate^listeningscore^readingscore^writingscore^speakingscore^overallscore^';        

    $headerline .=  'SPACER^'
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        

        . 'SPACER^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'

        . 'SPACER^';

    // PLB added headings for experience
    $headerline .= 'company_name1^start_date1^end_date1^years_experience1^'
		. 'company_name2^start_date2^end_date2^years_experience2^'
		. 'company_name3^start_date3^end_date3^years_experience3^'
		. 'company_name4^start_date4^end_date4^years_experience4^'
		. 'company_name5^start_date5^end_date5^years_experience5^'
        . 'SPACER^';

    // PLB added "possible advisors" heading
    $headerline .= 'possible_advisors^SPACER^';

    global $isEnglishDepartment;
    if ($isEnglishDepartment)
    {
    $headerline .= 'diversity_background^diversity_life_experience^how_hear_about^'
        . 'SPACER^';  
    }
    
    
  fwrite($fid, $headerline);
  fwrite($fid, "\n");

}

// PLB added application period where clause 02/09/09 
function get_names($deptid, $fid, $period_where="", $period_join="", $dump_all_submitted=false)
{
    //echo "func get_name\n<br>";

    $num = 0;
    $states = array();
    $visatypes = array();
    $ethnicity = array();
    $yes_no = array("no","yes");

    $sqlA = 'select id, name from states order by id';
	    $resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
	    while($rowA = mysql_fetch_array( $resultA ))
	    { 
		    array_push($states, $rowA[1]);
	    }


    $sqlA = 'select id, name from visatypes order by id';
	    array_push($visatypes, " ");

	    $resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
	    while($rowA = mysql_fetch_array( $resultA ))
	    { 
		    array_push($visatypes, $rowA[1]);
	    }

    $sqlA = 'select id, name from ethnicity  order by id';
	    //array_push($ethnicity, " ");
        $ethnicity[0] = " ";

	    $resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
	    while($rowA = mysql_fetch_array( $resultA ))
	    { 
		    //array_push($ethnicity, $rowA[1]);
            $ethnicity[$rowA['id']] = $rowA['name'];
	    }


    $sql3 = 'select distinct '
           . ' count(A.id) as cnt, A.id, A.user_id, C.admission_status '
           . ' from '
           . ' application as A ';
    if ($period_join) {
        $sql3 .= $period_join;
    }
    $sql3 .= ' join lu_application_programs as C, '
           . ' lu_programs_departments as D '
           . ' where '
           . ' A.submitted = 1 ';
           
    if (!$dump_all_submitted) 
        {
            $sql3 .=  'AND (A.paid = 1 OR A.waive = 1)';   
        }
    $sql3 .= ' AND A.id = C.application_id and'
            . ' C.program_id = D.program_id and'
            . ' D.department_id = '.$deptid.' '
            . $period_where 
            . ' group by A.id';

	$result3 = mysql_query($sql3) or die(mysql_error(). $sql3);
	while($row3 = mysql_fetch_array( $result3 ))
	    { 
    // echo $row3[0]." ".$row3[1]."<br> ";
               $appid = $row3[1];
               $user_id = $row3[2];
               $admit_status = $row3[3];

    $sql = 'select '
            . ' B.id, G.id, G.user_id, F.email, F.title, F.firstname, F.middlename, F.lastname, '
            . ' G.company, G.gender, G.dob, '
            . ' G.address_cur_street1, G.address_cur_street2, G.address_cur_street3, G.address_cur_street4, '
            . ' G.address_cur_city, P.name as CurrentState, G.address_cur_pcode, N.name as CurrentCountry, '
            . ' G.address_cur_tel, G.address_cur_tel_mobile, G.address_cur_tel_work,'
            . ' G.address_perm_street1, G.address_perm_street2, G.address_perm_street3, G.address_perm_street4, '
            . ' G.address_perm_city, Q.name as PermState, G.address_perm_pcode, O.name as PermCountry, '
            . ' G.address_perm_tel, G.address_perm_tel_mobile, G.address_perm_tel_work, G.homepage,'
            . ' J.name as CountryofCitz, J.iso_code, G.visastatus, G.native_tongue, G.ethnicity,'
    // PLB added race 1/29/10
            . ' GROUP_CONCAT(DISTINCT ipeds_race ORDER BY ipeds_race SEPARATOR ",") AS race,'
            . ' B.pier, B.womenfellowship, '
            // added portfolio, video essay
            . ' B.portfolio_link, video_essay.url AS video_essay_url, video_essay.access_code AS video_essay_access_code'
            . ' from '
            . ' application as B'
            . ' left join lu_users_usertypes as E on B.user_id = E.id'
            . ' left join users as F on E.user_id = F.id'
            . ' left join users_info as G on G.user_id = E.id'
    // PLB added race 1/29/10
            . ' LEFT OUTER JOIN applicant_ipeds_race ON E.id = applicant_ipeds_race.lu_users_usertypes_id'
            . ' LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id'
            . ' LEFT OUTER JOIN video_essay ON B.id = video_essay.application_id'
            . ' left join countries as J on G.cit_country = J.id'
            . ' left join countries as N on G.address_cur_country = N.id'
            . ' left join countries as O on G.address_perm_country = O.id'
            . ' left join states as P on G.address_cur_state = P.id'
            . ' left join states as Q on G.address_perm_state = Q.id'
            . ' where B.submitted=1';
            if (!$dump_all_submitted) 
            {
                $sql .=  ' AND (B.paid = 1 OR B.waive = 1)';   
            }
    $sql    .= ' AND B.id = '.$appid.' and'
            . ' E.usertype_id=5';
            
            if (isset($_POST['applicationId']))
            {
                $sql .= ' AND B.id IN (' . implode(',', $_POST['applicationId']) . ') ';    
            }
            
    // PLB added group by for race 1/29/10 
    $sql .= ' GROUP BY B.id'
            . ' Order by B.id';
            // PLB changed 01/07/09 to fix problem w/ straight join on countries
            // leaving out applications that have a null permanent address.
            /*
            . ' application as B Join '
            . ' lu_users_usertypes as E,'
            . ' users as F,'
            . ' users_info as G,'
            . ' countries as J,'
            . ' countries as N,'
            . ' countries as O,'
            . ' states as P,'
            . ' states as Q'
            . ' where '
            . ' B.submitted=1 and'
            . ' B.id = '.$appid.' and'
            . ' B.user_id=E.id and'
            . ' E.user_id =F.id and'
            . ' E.usertype_id=5 and'
            . ' G.user_id = E.id and'
            . ' G.cit_country = J.id and'
            . ' G.address_cur_country = N.id and'
            . ' G.address_perm_country = O.id and'
            . ' G.address_cur_state = P.id and'
            . ' G.address_perm_state = Q.id '
            . ' Order by B.id';
            */
                         
	    $result = mysql_query($sql) or die(mysql_error(). $sql);

        while($row = mysql_fetch_array( $result ))
	    { 
    // echo $row[0]."<BR>";

    fwrite($fid, "**BEGIN**^");
    fwrite($fid, $appid );
    fwrite($fid, "^" );
                $luuid=0;
                $uid=0;

                
                // PLB bumped cases by one to accomodate added race field 1/29/10
                //for ($i=3; $i<39; $i++){
                // PLB bumped cases by one again to accomodate added company field 1/06/11
                //for ($i=3; $i<40; $i++){
                // PLB bumped cases by one again to accomodate added native_tongue field 12/21/11
                //for ($i=3; $i<41; $i++){
                for ($i=3; $i<45; $i++){
                  switch( $i) {
                    
                    //case 35: //VISA STATUS
                    case 36: //VISA STATUS
                      fwrite($fid, $visatypes[$row[$i]]);
                      break;
                    
                    //case 36: //Ethinicty
                    //case 37: //Ethinicty
                    // native tongue is now $row[37]
                    case 38: //Ethinicty
                      if ( $row[$i] >= 0 ) {
                        fwrite($fid, $ethnicity[$row[$i]]);
                      } else {
                        fwrite($fid, " ");
                      }
                      break;
                      
                    // PLB bumped cases by one to accomodate added race field 1/29/10
                    //case 37: //PIER
                    //case 38: //WomenAtITFellowship
                    //case 38: //PIER
                    //case 39: //WomenAtITFellowship
                    //case 39: //PIER
                    //case 40: //WomenAtITFellowship
                    case 30: //PIER
                    case 41: //WomenAtITFellowship
                      fwrite($fid, $yes_no[$row[$i]]);
                      break;
                    default:
                      fwrite($fid, trim($row[$i]));
                  }
                  fwrite($fid, " ^");
                }

    fwrite($fid, "**END...BEGIN**^");
       if($dump_all_submitted) {
          get_programs($appid, 0, $fid, $dump_all_submitted); 
       } else {
            get_programs($appid, 0, $fid);
       }                       
    fwrite($fid, "**END...BEGIN**^");
        if($dump_all_submitted) {
            get_gre($appid, $luuid, $uid, $fid, $dump_all_submitted);
        } else {
           get_gre($appid, $luuid, $uid, $fid); 
        }           
    fwrite($fid, "**END...BEGIN**^");
    if($dump_all_submitted) {
        get_gresubject($appid, $luuid, $uid, $fid, $dump_all_submitted); 
    } else {
        get_gresubject($appid, $luuid, $uid, $fid);
    }          
    fwrite($fid, "**END...BEGIN**^");
    if($dump_all_submitted) {
                get_TOEFLS($appid, $luuid, $uid, $fid, $dump_all_submitted);
    } else {
         get_TOEFLS($appid, $luuid, $uid, $fid);
    }
    fwrite($fid, "**END...BEGIN**^");
    if($dump_all_submitted) {
                get_IELTS($appid, $luuid, $uid, $fid, $dump_all_submitted);
    } else {
        get_IELTS($appid, $luuid, $uid, $fid);
    }            
    fwrite($fid, "**END...BEGIN**^");
    if($dump_all_submitted) {
                get_rec($appid, $luuid, $uid, $fid, $dump_all_submitted);
    } else {
        get_rec($appid, $luuid, $uid, $fid);
    }           
    fwrite($fid, "**END...BEGIN**^");
    if($dump_all_submitted) {
                get_edu($appid, $luuid, $uid, $fid, $dump_all_submitted);
    } else {
        get_edu($appid, $luuid, $uid, $fid);
    }
    // PLB added experience
    fwrite($fid, "**END...BEGIN**^");
                getExperience($appid, $fid);

    // PLB added possible advisors
    fwrite($fid, "**END...BEGIN**^");
                // PLB changed 01/12/09
                //getPossibleAdvisors($appid, $fid);
                getPossibleAdvisors($deptid, $appid, $fid);           

    global $isEnglishDepartment;
    if ($isEnglishDepartment)
    {
        fwrite($fid, "**END...BEGIN**^");
        getEnglishColumns($appid, $fid);    
    }
                
    fwrite($fid, "**END**^");

                fwrite($fid, "\n");
                $num++;
              }
              
	    }
     return $num;
}

// PLB added function to get employment experience
function getExperience($appid, $fid)
{

	// Query to get all the applicant's experience records
	$experience_query = "SELECT company, start_date, end_date, years_exp
							FROM experience
							WHERE application_id = " . $appid;

	// Query the db
	$result = mysql_query($experience_query) or die(mysql_error(). $experience_query);    
    
    // Set the record count
    $num_experience_records = 5;
    
    // Loop the records
    while($row = mysql_fetch_array( $result )) {
    
    	// Get the row contents
    	$row_contents = $row['company'] . "^"
    					. $row['start_date'] . "^"
    					. $row['end_date'] . "^"
    					. $row['years_exp'] . "^";
    	
    	// Write the row contents
    	fwrite($fid, $row_contents);
        
        // Decrement the count
        $num_experience_records--;

    }
    
    // The record count should now be 
    // 5 minus the number of db rows for that application.
    // Fill out the remaining, empty rows.
    for ($i = $num_experience_records; $i > 0; $i--) {
    
    	fwrite($fid, "^^^^" );	
    
	}

}

// PLB added function to get possible advisors
// PLB added department id parameter 01/12/09
//function getPossibleAdvisors($appid, $fid)
function getPossibleAdvisors($deptid, $appid, $fid)
{
    // Start with possible advisors empty
    $possible_advisors = "";
    
    // Query to return one row/field with the names of all advisors separated by commas
    $advisors_query = "SELECT GROUP_CONCAT( names.name ) AS possible_advisors
                        FROM application
                        INNER JOIN (
                        (SELECT application_id, name
                        /* PLB changed 01/12/09
                        FROM lu_application_advisor
                        WHERE application_id = " . $appid . "
                        */
                        FROM lu_application_advisor,
                        lu_programs_departments
                        WHERE application_id = " . $appid;
   if ($deptid != 27) {
       // CNBC advisors are not identified by program id.
       $advisors_query .= "  AND lu_application_advisor.program_id = lu_programs_departments.program_id";    
   } 
   $advisors_query .= "  AND lu_programs_departments.department_id = " . $deptid . " 
                        AND name != ''
                        AND name IS NOT NULL)
                        UNION 
                        (SELECT application_id, concat( users.firstname, ' ', users.lastname ) AS name
                        FROM lu_application_advisor
                        INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = lu_application_advisor.advisor_user_id
                        INNER JOIN users ON users.id = lu_users_usertypes.user_id
                        /* PLB changed 01/12/09
                        WHERE application_id = " . $appid . ")
                        */
                        INNER JOIN lu_programs_departments ON lu_application_advisor.program_id = lu_programs_departments.program_id
                        WHERE lu_programs_departments.department_id = " . $deptid . "
                        AND application_id = " . $appid . ") 
                        ) as names on names.application_id = application.id
                        GROUP BY application.id";
    
    // Query the db for the possible advisors
    $result = mysql_query($advisors_query) or die(mysql_error(). $advisors_query);    
    while($row = mysql_fetch_array( $result )) {
        // Should only be one row
        $search  = array("\rn", "\n", "\r", "\t");
        $possible_advisors = str_replace($search, "", $row['possible_advisors']);
    }
    
    // write the string to the file
    fwrite($fid, $possible_advisors);
    fwrite($fid, "^");
    
}

function get_programs($appid, $deptid, $fid, $dump_all_submitted=false)
{
// echo "func get_programs\n<br>";

    $sql = 'select DISTINCT CONCAT(L.name, " ", K.linkword, " ", M.name) as program_name, C.id, C.admission_status'
            . ' from '
            . ' application as B join'
            . ' lu_application_programs as C,'
            . ' lu_programs_departments as D,'
            . ' programs as K,'
            . ' degree as L,'
            . ' fieldsofstudy as M'
            . ' where '
            . ' B.submitted=1 ';

    if (!$dump_all_submitted) 
        {
            $sql .=  'AND (B.paid = 1 OR B.waive = 1)';   
        }
        
    $sql .= ' AND B.id='.$appid.' and'
        . ' B.id=C.application_id and'
        . ' C.program_id=D.program_id and';

    if ( $deptid > 0 ) {
        $sql .= ' D.department_id='.$deptid.' and ';
    }

     $sql .= ' K.id = D.program_id and'
            . ' L.id = K.degree_id and '
            . ' M.id = K.fieldofstudy_id'
            . ' Order by C.choice';

     $num_of_inst = 4;
     
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           
           if ($num_of_inst > 0) {
             for ($i=0; $i<1; $i++){
               fwrite($fid, $row[$i]);
               fwrite($fid, "^");
               $progid=$row[1];
// echo "PROGID:".$progid."<BR>";
               get_program_aoi($progid, $fid);
             }
             // Admission Decision for this program
             fwrite($fid, $row[2]);
             fwrite($fid, "^");
           }
           $num_of_inst--;
	}

        for ($i=$num_of_inst; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^^^^^" );
        }



}

function get_program_aoi($progid, $fid)
{
// echo "func get_program_aoi\n<br>";
/*
$sql = 'select DISTINCT I . name '
        . ' from '
        . ' lu_application_interest as D , '
        . ' interest as I '
        . ' where '
        . ' D.app_program_id='.$progid.' and'
        . ' D.interest_id = I.id Order By D.choice';
*/    
    $sql = 'select DISTINCT I.name '
        . ' from '
        . ' lu_application_interest D '
        . ' INNER JOIN interest I on D.interest_id = I.id'
        . ' where '
        . ' D.app_program_id='.$progid
        . ' Order By D.choice';
         
        $num_of_inst=3;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{      //  debugbreak();
             for ($i=0; $i<1; $i++){
               fwrite($fid, $row[$i]);
               fwrite($fid, "^");
             }
           $num_of_inst--;
	}

        for ($i=$num_of_inst; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^" );
        }

}

function get_experience($uid, $aid,  $fid)
{
// echo "func get_experience\n<br>";
}

function get_gre($appid, $luuid, $uid, $fid, $dump_unpaid=false)
{
// echo "func get_gre\n<br>";
    $sql = 'select G.testdate , G.verbalscore , G.verbalpercentile , G.quantitativescore , G.quantitativepercentile , G.analyticalscore , '
            . ' G.analyticalpercentile , G.analyticalwritingscore , G.analyticalwritingpercentile '
            . ' from '
            . ' application as B join '
            . ' grescore as G '
            . ' where '
            . ' B . submitted = 1 and ';

    if (!$dump_unpaid) {
        $sql .= ' (B.paid = 1 OR B.waive = 1) AND ';
    }
    $sql .= ' B.id=' . $appid.' and'
            . ' B.id = G.application_id '
            . ' Order by B.id';
        
     $gre_score_missing=1;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           $gre_score_missing=0;
           for ($i=0; $i<9; $i++){
             fwrite($fid, $row[$i]);
             fwrite($fid, "^");
           }
	}
        if ($gre_score_missing == 1) {
           fwrite($fid, "^^^^^^^^^");
        }

}

function get_gresubject($appid, $luuid, $uid, $fid, $dump_unpaid=false)
{
// echo "func get_gresubject\n<br>";

    $sql = 'select G.testdate , G.name , G.score , G.percentile '
            . ' from '
            . ' application as B join '
            . ' gresubjectscore as G '
            . ' where '
            . ' B.submitted = 1 and ';
    if (!$dump_unpaid) {
        $sql .= ' (B.paid = 1 OR B.waive = 1) AND ';
    }
    $sql .= ' B.id=' . $appid.' and'
            . ' B.id = G.application_id '
            . ' Order by B.id';

            $subject_score_missing = 1;
	    $result = mysql_query($sql) or die(mysql_error(). $sql);
	    while($row = mysql_fetch_array( $result ))
	    { 
               for ($i=0; $i<4; $i++){
                 fwrite($fid, $row[$i]);
                 fwrite($fid, "^");
               }
               $subject_score_missing = 0;
	    }
            if ($subject_score_missing == 1) {
              fwrite($fid, " ^ ^ ^ ^");
            }
}

function get_TOEFLS($appid, $luuid, $uid, $fid, $dump_unpaid=false)
{
    $sql = 'select T.type , T.testdate , T.section1 , T.section2 , T.section3 , T.essay , T.total , B.id '
    . ' from '
    . ' application as B join '
    . ' toefl as T '
    . ' where '
    . ' B.submitted = 1 and ';
    if (!$dump_unpaid) {
        $sql .= ' (B.paid = 1 OR B.waive = 1) AND ';
    }
    $sql .= ' B.id ='.$appid.' and'
    . ' B.id = T.application_id '
    . ' Order by B.id , T.type';

    $toefl_score_missing=1;
	$toefl_score_count = 3;
    $result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
        for ($i=0; $i<7; $i++){
         fwrite($fid, $row[$i]);
         fwrite($fid, "^");
        }
        $toefl_score_missing=0;
        $toefl_score_count--;
	}
    if ($toefl_score_missing == 1) {
      fwrite($fid, "^^^^^^^ ^^^^^^^ ^^^^^^^");
    } else {
        // PLB added fix (for the apply-side fix) for when there isn't a record for each type.
        for ($i = 0; $i < $toefl_score_count; $i++) {
            fwrite($fid, "^^^^^^^");    
        }
    }
}

// PLB added IELTS 12/8/09
function get_IELTS($appid, $luuid, $uid, $fid)
{
    $sql = "SELECT 
            ieltsscore.testdate, 
            ieltsscore.listeningscore, 
            ieltsscore.readingscore, 
            ieltsscore.writingscore, 
            ieltsscore.speakingscore, 
            ieltsscore.overallscore
            FROM application 
            LEFT OUTER JOIN ieltsscore ON application.id = ieltsscore.application_id
            WHERE application.submitted = 1
            AND ";
    $sql .= " (application.paid = 1 OR application.waive = 1) 
            AND application.id = " . $appid . "
            ORDER BY application.id";

    $ielts_score_missing = 1;
    $result = mysql_query($sql) or die(mysql_error(). $sql);
    while($row = mysql_fetch_array( $result ))
    { 
        for ($i=0; $i<6; $i++){
            fwrite($fid, $row[$i]);
            fwrite($fid, "^");
        }
        $ielts_score_missing = 0;
    }
        if ($ielts_score_missing == 1) {
          fwrite($fid, "^^^^^^");
        }
}


function get_edu($appid, $luuid, $uid, $fid, $submitted_only = false)
{
// echo "func get_edu\n<br>";

    $edu_type = array("undefined","ugrad", "grad", "additional" );

    $sql = 'select C.name as UnivName, I.date_entered, I.date_grad, I.date_left, D.name as Degree, I.major1, I.major2, I.minor1, I.minor2, 
    I.gpa, I.gpa_major, G.name as GPAScale, I.educationtype, I.user_id 
    from  application as B 
    inner join usersinst as I on I.application_id = B.id
    inner join institutes as C on C.id = I.institute_id
    left outer join degreesall as D on D.id = I.degree 
    left outer join gpascales as G on G.id = I.gpa_scale
    where  B.submitted=1 ';

    if (!$submitted_only) 
        {
            $sql .=  'AND (B.paid = 1 OR B.waive = 1)';   
        }
        // 'and (B.paid = 1 OR B.waive = 1) AND'
    $sql .= ' and B.id= ' . $appid . 
            ' Order by I.educationtype';

 /*
'select'
        . ' C.name as UnivName, I.date_entered, I.date_grad, I.date_left, D.name as Degree, I.major1, I.major2, I.minor1, I.minor2, I.gpa, I.gpa_major, G.name as GPAScale, I.educationtype, I.user_id'
        . ' from '
        . ' application as B join'
        . ' usersinst as I,'
        . ' institutes as C,'
        . ' degreesall as D,'
        . ' gpascales as G'
        . ' where '
        . ' B.submitted=1 and (B.paid = 1 OR B.waive = 1) AND'
        . ' B.id='.$appid.' and'
        . ' B.id=I.application_id and'
        . ' I.institute_id = C.id and'
        . ' I.degree = D.id and'
        . ' I.gpa_scale = G.id'
        . ' Order by I.educationtype';
*/
        $num_of_inst = 5;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           if ($num_of_inst > 0) {
             for ($i=0; $i<13; $i++){
               if ($i == 12) {
                 fwrite($fid, $edu_type[$row[$i]]);
               } else {
                 fwrite($fid, $row[$i]);
               }
               fwrite($fid, "^");
             }
           }
           $num_of_inst--;
	}

        for ($i=$num_of_inst; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^" );
        }

}

function get_rec($appid, $luuid, $uid, $fid, $dump_unpaid=false)
{
// echo "func get_rec\n<br>";

    $yes_no = array("no","yes");

    $sql = 'select'
            . ' U.title, CONCAT(U.firstname, " ", U.lastname) as RecName, U.email, UI.company, UI.address_cur_tel, B.buckleywaive, B.id'
            . ' from '
            . ' application as B join'
            . ' recommend as R,'
            . ' lu_users_usertypes as LUU,'
            . ' users as U,'
            . ' users_info as UI'
            . ' where '
            . ' B.submitted=1 ';
            
    if (!$dump_unpaid) 
        {
            $sql .=  'AND (B.paid = 1 OR B.waive = 1)';   
        }
            // 'and (B.paid = 1 OR B.waive = 1) AND'
    $sql .=  'AND B.id='.$appid.' and'
        . ' B.id=R.application_id and'
        . ' R.rec_user_id = LUU.id and'
        . ' LUU.user_id = U.id and'
        . ' LUU.id = UI.user_id'
        . ' Order by B.id';
            
            $num_of_recs = 5;
	    $result = mysql_query($sql) or die(mysql_error(). $sql);
	    while($row = mysql_fetch_array( $result ))
	    { 
               if ($num_of_recs > 0) {
                 for ($i=0; $i<6; $i++){

                   if ( $i == 5 ) {
                     fwrite($fid, $yes_no[$row[$i]]);
                   } else {
                     fwrite($fid, trim($row[$i]));
                   }
                   fwrite($fid, "^");
                 }
               }
               $num_of_recs--;
	    }

            // Fill for the missing recs
    // echo "FILL: ".$num_of_recs;
            for ($i=$num_of_recs; $i>0; $i--) {
    // echo " F".$i." ";
              fwrite($fid, " ^ ^ ^ ^ ^ ^" );
            }
    // echo "FILL: DONE<BR>";
}

function get_person_name($userid, $fid)
{
   $sql = ' SELECT '
        . ' U.title, U.firstname, U.middlename, U.lastname, U.email'
        . ' FROM '
        . ' users as U, '
        . ' lu_users_usertypes as LUU '
        . ' WHERE '
        . ' LUU.id = '.$userid .' and '
        . ' U.id = LUU.user_id ';

	$result = mysql_query($sql) or die(mysql_error(). $sql);
	$row = mysql_fetch_array( $result );
        for ($i=0; $i<5; $i++){
          fwrite($fid, $row[$i]);
          fwrite($fid, "^");
        }
}

// PLB added application period where clause 02/09/09
function get_scores($deptid, $fid, $period_where="", $period_join="", $dump_all_submitted=false)
{
// echo "func get_scores\n<br>";
 $records_written = 0;
 $yes_no = array("no","yes");

$header = ' SPACER ^' 
        . 'Title^Firstname^Middlename^Lastname^email^'
        . 'PIER^WomenAtITFellowship^'
        . ' SPACER ^'
        . 'user_id^application_id^reviewer_id^roleID^rev_firstname^rev_lastname^';
if ($deptid == 25) {
    $header .= 'Role^CommentExperience^CommentResearch^CommentSOP^Comments^Score(PHD)^';     
} else {
    $header .= 'Role^CommentBackground^CommentGrades^CommentSOP^Comments^Score(PHD)^';    
}        
$header .= 'Certainty(PHD)^Score(MS)^Certainty(MS)^Round2Advance^admit_vote^';
if ($deptid == 25) {
    $header .= 'Recruited^Mentor/Grad_name^Recoms/Pertinent_info^Advise_time?^Commit_money?^';     
} else {
    $header .= 'Recruited^Grad_name^Pertinent_info^Advise_time?^Commit_money?^';    
}      
$header .= 'Fund_source?^ReviewRound^Interview^Brilliance^Experience^Fit^Other_interest^'
        . 'fac_vote^Top5Rank^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . 'AOI^Score1^Score2^'
        . ' SPACER ^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        . 'program_name^aoi_1^aoi_2^aoi_3^admit_status^'
        
        // PLB added headings for special consideration, technical screen, and language screen
        . ' SPACER ^'
        . 'special_consideration_requested^technical_screen_requested^language_screen_requested^'

        . ' SPACER ^' 
        . 'risk_factors^positive_factors^'
        
        . ' END';

        fwrite($fid, $header);
        fwrite($fid, "\n");

        
  // PLB new query for reviews 01/09/09
  // Join of application user_id to user table 
  // filters out old/orphan reviews.
  
  /*
  // original query
  $sql = 'SELECT'
        . ' A.pier, A.womenfellowship,'
        . ' A.user_id, R.application_id, R.reviewer_id, UT.id, U.firstname, U.lastname,'
        . ' UT.name, R.grades, R.statement, R.comments, R.point,'
        . ' R.point_certainty, R.point2, R.point_certainty, R.round2, R.admit_vote,'
        . ' R.recruited, R.grad_name, R.pertinent_info, R.advise_time, R.commit_money,'
        . ' R.fund_source, R.round, R.interview, R.brilliance, R.other_interest,'
        . ' R.fac_vote, R.rank'
        . ' FROM'
        . ' review as R ,'
        . ' lu_user_department as LUUD,'
        . ' application as A,'
        . ' lu_users_usertypes as LUUU,'
        . ' users as U,'
        . ' usertypes as UT'
        . ' WHERE'
        . ' R.reviewer_id = LUUD.user_id and'
        . ' LUUD.department_id='.$deptid.' and'
        . ' A.id = R.application_id and'
        . ' LUUU.id=R.reviewer_id and'
        . ' LUUU.user_id=U.id and'
        . ' UT.id = LUUU.usertype_id and'
        . ' R.supplemental_review is NULL'
        . ' ORDER BY R.application_id'
        . ' ';
  */  
  
  // new query
  $sql = 'SELECT'
        . ' A.pier, A.womenfellowship,'
        . ' A.user_id, R.application_id, R.reviewer_id, UT.id, U.firstname, U.lastname,'
        . ' UT.name, R.background, R.grades, R.statement, R.comments, R.point,'
        . ' R.point_certainty, R.point2, R.point_certainty, R.round2, R.admit_vote,'
        . ' R.recruited, R.grad_name, R.pertinent_info, R.advise_time, R.commit_money,'
        . ' R.fund_source, R.round, R.interview, R.brilliance, R.recommendations, R.publications, R.other_interest,'
        . ' R.fac_vote, R.rank'
        . ' FROM'
        . ' review as R ,'
        . ' lu_user_department as LUUD,'
        . ' application as A';
  if ($period_join) {
    $sql .= $period_join;
  }  
  $sql .= ', lu_users_usertypes as LUUU,'
        . ' users as U,'
        . ' usertypes as UT,'
        . ' lu_users_usertypes as application_LUUU,'
        . ' users as application_U'
        . ' WHERE'
        . ' A.user_id = application_LUUU.id and'
        . ' application_LUUU.user_id = application_U.id and'
        . ' R.reviewer_id = LUUD.user_id and'
        . ' LUUD.department_id='.$deptid.' and'
        . ' A.id = R.application_id and'
        . ' LUUU.id=R.reviewer_id and'
        . ' LUUU.user_id=U.id and'
        . ' UT.id = LUUU.usertype_id and'
        . ' R.supplemental_review is NULL'
        . $period_where
        . ' ORDER BY R.application_id'
        . ' ';
     
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
// echo $row3[0]." ".$row[1]."<br> ";
           $userid = $row[2];
           $appid = $row[3];
           $revr_id = $row[4];
           $revr_type = $row[5];

fwrite($fid, "**BEGIN**^");
// Get CANDIDATE NAME
           get_person_name($userid, $fid);
           fwrite($fid, $yes_no[$row[0]]); // PIER
           fwrite($fid, "^");
           fwrite($fid, $yes_no[$row[1]]); // Women at IT   
           fwrite($fid, "^");

fwrite($fid, "**END...BEGIN**^");

// WRITE  REVIEW SCORES
           for ($i=2; $i<34; $i++){
             // STRIP NEWLINEs
             switch($i) {
              case 23: // Wiling to commit money?
              case 22: // Willing to advise?
              case 17: // Round 2 Advance?
                if ($row[$i] <> "" ) {
                  fwrite($fid, $yes_no[$row[$i]]);
                }
                break;
              default:
                $tmpstring = str_ireplace("\n", "", $row[$i] );
                $clean_string = str_ireplace("\r", "", $tmpstring );
                fwrite($fid, $clean_string);
             }
             fwrite($fid, "^");
           }

          // If Faculty reviewer write out multple records
          // Could have up to 6 scores
          // 2 programs per review applications 
          // only have 2 scores possible (phd and ms)
          $max_scores=6;
          if ($revr_type == 3) {
              $sql2 = ' SELECT '
                    . ' I.name, R.point, R.point2'
                    . ' FROM '
                    . ' review as R,'
                    . ' interest as I '
                    . ' WHERE '
                    . ' R.application_id='.$appid.' AND '
                    . ' R.reviewer_id='.$revr_id.' AND '
                    . ' R.supplemental_review > 0 AND '
                    . ' I.id = R.supplemental_review ';
            $result2 = mysql_query($sql2) or die(mysql_error(). $sql2);
            while($row2 = mysql_fetch_array( $result2 ))
            {  

              for ($i=0; $i<3; $i++){
                fwrite($fid, $row2[$i]);
                fwrite($fid, "^");
              }
              $max_scores--;
            }
          }
          // pad the remaining AOI place holders
          for ($i=$max_scores; $i>0; $i--) {
            fwrite($fid, "^^^");
          }


//  GET PROGRAM APPLIED TOO
            fwrite($fid, "**END...BEGIN**^");
            get_programs( $appid, $deptid, $fid);

            // PLB added special consideration, phone screens
            fwrite($fid, "**END...BEGIN**^");
            getSpecialConsideration($appid, $fid);
            getPhoneScreens($appid, $fid);
            
            fwrite($fid, "**END...BEGIN**^");
            getRiskFactors($appid, $revr_id, $fid);
            getPositiveFactors($appid, $revr_id, $fid);

            fwrite($fid, "**END**");          
            fwrite($fid, "\n");
            $records_written++;
          }

       return $records_written;
}

// PLB added functions to get special consideration and phone screens
function getSpecialConsideration($appid, $fid)
{   
    // Query always to return one row with yes/no
    $special_consideration_query = "SELECT application.id,
                                    IF (special_consideration.requested = 1, 'yes', 'no') as special_consideration_requested
                                    FROM application
                                    LEFT OUTER JOIN (
                                        SELECT application_id, 
                                        MAX(special_consideration) as requested
                                        FROM special_consideration 
                                        GROUP BY application_id            
                                    ) as special_consideration ON application.id = special_consideration.application_id
                                    WHERE application.id = " .$appid;
    
    // Query the db
    $result = mysql_query($special_consideration_query) or die(mysql_error(). $special_consideration_query);    
    while($row = mysql_fetch_array( $result )) {
        // Should only be one row
        $special_consideration_requested = $row['special_consideration_requested'];
    }
    
    // write the string to the file
    fwrite($fid, $special_consideration_requested);
    fwrite($fid, "^");

}

function getPhoneScreens($appid, $fid)
{
    // Query always to return one row with yes/no for each field
    $phone_screen_query = "SELECT application.id,
                            IF (phone_screens.technical_screen_requested = 1, 'yes', 'no') as technical_screen_requested,
                            IF (phone_screens.language_screen_requested = 1, 'yes', 'no') as language_screen_requested
                            FROM application
                            LEFT OUTER JOIN (
                                SELECT application_id, 
                                MAX(technical_screen) as technical_screen_requested,
                                MAX(language_screen) as language_screen_requested
                                FROM phone_screens
                                GROUP BY application_id            
                            ) as phone_screens ON application.id = phone_screens.application_id
                            WHERE application.id = " .$appid;
    
    // Query the db 
    $result = mysql_query($phone_screen_query) or die(mysql_error(). $phone_screen_query);    
    while($row = mysql_fetch_array( $result )) {
        // Should only be one row
        $technical_screen_requested = $row['technical_screen_requested'];
        $language_screen_requested = $row['language_screen_requested'];
    }
    
    // write the strings to the file
    fwrite($fid, $technical_screen_requested);
    fwrite($fid, "^");
    fwrite($fid, $language_screen_requested);
    fwrite($fid, "^");   
    
}

function getRiskFactors($applicationId, $reviewerId, $fid)
{   
    $riskFactorArray = array();
    
    $isrseRiskFactorsQuery = "SELECT DISTINCT risk_factor
                            FROM review
                            INNER JOIN review_risk_factor
                              ON review.id = review_risk_factor.review_id
                            INNER JOIN risk_factor
                              ON review_risk_factor.risk_factor_id = risk_factor.risk_factor_id
                            WHERE review.application_id = " . $applicationId . "
                            AND review.reviewer_id = " . $reviewerId;
    $isrseRiskFactorsResult = mysql_query($isrseRiskFactorsQuery);
    while ( $row = mysql_fetch_array($isrseRiskFactorsResult) ) {
        if ( !in_array($row['risk_factor'], $riskFactorArray) ) {
            $riskFactorArray[] = $row['risk_factor'];    
        }    
    }
    
    $mseRiskFactorsQuery = "SELECT * FROM mse_risk_factors 
                            WHERE application_id = " . $applicationId . "
                            AND reviewer_id = " . $reviewerId;
    $mseRiskFactorsResult = mysql_query($mseRiskFactorsQuery);
    while ( $row = mysql_fetch_array($mseRiskFactorsResult) ) {
        if ($row['language']) {
            $riskFactorArray[] = 'Language';    
        }    
        if ($row['experience']) {
            $riskFactorArray[] = 'Experience';    
        }
        if ($row['academic']) {
            $riskFactorArray[] = 'Academic';    
        }  
        if ($row['other']) {
            $riskFactorArray[] = 'Other';    
        } 
    }
 
    $riskFactorsString = implode(',', $riskFactorArray);
    
    // write the string to the file
    fwrite($fid, $riskFactorsString);
    fwrite($fid, "^");
}

function getPositiveFactors($applicationId, $reviewerId, $fid)
{   
    $positiveFactorArray = array();
    
    $isrsePositiveFactorsQuery = "SELECT DISTINCT positive_factor
                                FROM review
                                INNER JOIN review_positive_factor
                                  ON review.id = review_positive_factor.review_id
                                INNER JOIN positive_factor
                                  ON review_positive_factor.positive_factor_id = positive_factor.positive_factor_id
                                WHERE review.application_id = " . $applicationId . "
                                AND review.reviewer_id = " .$reviewerId . "
                                UNION
                                SELECT CONCAT('Other: ', positive_factor_other) AS positive_factor
                                FROM review
                                INNER JOIN review_positive_factor_other
                                  ON review.id = review_positive_factor_other.review_id
                                WHERE review.application_id = " . $applicationId . " 
                                AND review.reviewer_id = " .$reviewerId;
    $isrsePositiveFactorsResult = mysql_query($isrsePositiveFactorsQuery);
    while ( $row = mysql_fetch_array($isrsePositiveFactorsResult) ) {
        if ( !in_array($row['positive_factor'], $positiveFactorArray) ) {
            $positiveFactorArray[] = $row['positive_factor'];    
        }    
    }
    
    $positiveFactorsString = implode(',', $positiveFactorArray);
    
    // write the string to the file
    fwrite($fid, $positiveFactorsString);
    fwrite($fid, "^");
}

function getEnglishColumns($appid, $fid)
{   
    $query = "SELECT application.id,
                dietrich_diversity.background,
                dietrich_diversity.life_experience,
                application.referral_to_program
                FROM application
                LEFT OUTER JOIN dietrich_diversity 
                    ON application.id = dietrich_diversity.application_id
                WHERE application.id = " .$appid;
    
    $result = mysql_query($query) 
        or die(mysql_error(). $special_consideration_query);    
    
    while($row = mysql_fetch_array($result)) 
    {
        fwrite($fid, $row['background'] . '^');
        fwrite($fid, $row['life_experience'] . '^');
        fwrite($fid, $row['referral_to_program'] . '^');
    }
}

// PLB Main "function"
// Get the department name from the db;
$department_name = "";
$department_query = "SELECT name FROM department WHERE id = " . $department_id; 
$department_result = mysql_query($department_query) or die(mysql_error(). $department_query);
while( $row = mysql_fetch_array( $department_result ) ) {

    $department_name = $row['name'];

} 

if (!$department_name) {

    ob_end_clean();
    echo "Department " . $department_id . " not found.";    
    
} else {

    $filename_start = str_replace(' ', '_', $department_name);
    //echo $filename_start;
    
    // PLB added check for application period 02/09/09
    if ($period) {
    
        $period_join = " INNER JOIN period_application ON A.id = period_application.application_id ";
        
        if (is_array($period)) {
            $filename_start .= '_period' . $period[1];
            $period_where = " AND ( period_application.period_id = ";
            $period_where .= implode(' OR period_application.period_id = ', $period) . ' )';
        } else {
            $filename_start .= '_period' . $period;
            $period_where = " AND period_application.period_id = " . $period;
        }
 
        /*
        $period_id = $_GET['period'];
        $period_array = getApplicationPeriod($period_id);
        
        $start_date = $period_array[0]['start_date'];
        $end_date =  $period_array[0]['end_date'];
        
        // Period where depends on the application table's being aliased as "A"
        $period_where = ' AND ( CAST(A.submitted_date AS DATE) BETWEEN ';
        $period_where .= 'CAST("' . $start_date . '" AS DATE) AND ';
        $period_where .= 'CAST("' . $end_date . '" AS DATE))';
        
        $formatted_start_date = date("mdy", strtotime($start_date));
        $formatted_end_date = date("mdy", strtotime($end_date));
        $filename_start .= "_" . $formatted_start_date . "-" . $formatted_end_date . ""; 
        */
    
    } else {
        
        $period_where = "";
        
    }
    $dump_all_submitted = isRissDepartment($department_id) || isReuseDepartment($department_id);
    if ( $dump == "reviews" ) {

        // Create the review dump file
        $filename = $filename_start . "_review_export.txt";
        $filePath = './export/' . $filename;
        $fid = create_dump_file($filePath);
        $num = get_scores( $department_id, $fid, $period_where, $period_join);
        close_dump_file($fid);
        
    } else {

        // Create the application dump file
        $filename = $filename_start . "_application_export.txt";
        $filePath = './export/' . $filename;
        $fid = create_dump_file($filePath);
        print_header_labels($fid);
        if ($dump_all_submitted) {
            $num = get_names( $department_id, $fid, $period_where, $period_join, $dump_all_submitted);
        } else {
            $num = get_names( $department_id, $fid, $period_where, $period_join);
        }
        close_dump_file($fid);         
        
    }
    
    // Turn off the output buffer.  
    ob_end_clean();
    
    // Read the file contents
    $handle = fopen($filePath, "r");
    $contents = fread($handle, filesize($filePath));
    fclose($handle);
    
    // Return the file contents as an attachment
    header("Pragma: public");
    header("Cache-Control: max-age=0");
    header("Content-Description: File Transfer");
    header('Content-Disposition: attachment; filename="' . $filename .'"');
    header('Content-Type: text/plain');
    echo $contents;


}


// function to get application period info by period id
// PLB 02/06/09
function getApplicationPeriod($period_id) {

    // get the application period data from the db
    $application_periods_query = "SELECT application_periods.* ";
    $application_periods_query .= " FROM application_periods";
    $application_periods_query .= " WHERE application_period_id = " . $period_id;
    $application_periods_query .= " ORDER BY end_date DESC, start_date DESC";
    $application_periods_result = mysql_query($application_periods_query) or die(mysql_error());
    
    //echo $application_periods_query;
    
    $period_array = array();
    
    while( $period_row = mysql_fetch_array($application_periods_result) ) {
            $period_array[] = $period_row;
    }
    
    return $period_array;

}

/*

echo "Starting data dump for Online Admission System system<BR><BR>";
$status_fid = create_dump_file("DUMPSTATUS.txt");
// Main
$programs_list = array ( 
                        array(1, "CSD"), 
                        array(2, "ML"),
                        array(3, "RI"),
                        array(5, "HCII"),
                        array(6, "LTI"),
                        array(8, "COMPBIO"),
                        array(18, "MSECAMPUS"),
                        array(19, "MSEDIST"),
                        array(20, "VLIS"),
                        array(25, "SE"),
                        array(26, "COS"),
                        array(27, "CNBC")
                        );

$count=12;
for ($i=0; $i<$count; $i++){

  $deptid=$programs_list[$i][0];


  $filename = $programs_list[$i][1]."_REVIEWDUMP.txt";
  $fid = create_dump_file($filename);
  $num = get_scores( $deptid, $fid);
  close_dump_file($fid);
  $status = 'Dumping '.$programs_list[$i][1].' adm review data to file: <A HREF="'
       .$filename.'" TARGET=_blank>'.$filename.'</A>....Num Records: '.$num.'<br>';
  echo $status;
  fwrite($status_fid, $status);
  fwrite($status_fid, "\n");

  $filename = $programs_list[$i][1]."_DATADUMP.txt";
  $fid = create_dump_file($filename);
  print_header_labels($fid);
  $num = get_names( $deptid, $fid);
  close_dump_file($fid);
  $status = 'Dumping '.$programs_list[$i][1].' application data to file: <A HREF="'
       .$filename.'" TARGET=_blank>'.$filename.'</A>....Num Records: '.$num.'<br>';
  echo $status."<BR>";
  fwrite($status_fid, $status);
  fwrite($status_fid, "\n");
}

 close_dump_file($status_fid);
 
 */
?>