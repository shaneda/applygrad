<?php
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Unit.php';
//include '../classes/DB_Applyweb/class.DB_UnitRole.php';
//include '../classes/DB_Applyweb/class.DB_Programs.php';
//include '../classes/DB_Applyweb/class.DB_ProgramsUnit.php';
//include '../classes/DB_Applyweb/class.DB_DomainUnit.php';
include '../classes/DB_Applyweb/class.DB_Domain.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';  

include '../classes/class.Unit.php';
include '../classes/class.Period.php';

require_once 'Structures/DataGrid.php';

include '../classes/class.StatisticsTableFactory.php';
include '../classes/Data/Statistics/class.StatisticsDataFactory.php';
include '../classes/Data/Statistics/class.ApplicantsByUnitData.php';
include '../classes/Data/Statistics/class.MseMsitBreakdownData.php';
include '../classes/Data/Statistics/class.UnitCrosstabData.php';
include '../classes/Data/Statistics/class.GenderCitizenshipCrosstabData.php';
include '../classes/Data/Statistics/class.ApplicantsByCitizenshipData.php';
include '../classes/Data/Statistics/class.ApplicantsByEthnicityData.php';
include '../classes/Data/Statistics/class.ApplicantsByGenderData.php';
include '../classes/Data/Statistics/class.ApplicantsByInstitutionData.php';
include '../classes/Data/Statistics/class.ApplicantsByNumProgramsData.php';
include '../classes/Structures_DataGrid/Statistics/class.StatisticsDataGridFactory.php';
include '../classes/Structures_DataGrid/Statistics/class.ApplicantsByUnit_DataGrid.php';
include '../classes/Structures_DataGrid/Statistics/class.MseMsitBreakdown_DataGrid.php'; 
include '../classes/Structures_DataGrid/Statistics/class.MseMsitBreakdownMsit_DataGrid.php';
include '../classes/Structures_DataGrid/Statistics/class.MseMsitBreakdownMba_DataGrid.php'; 
include '../classes/Structures_DataGrid/Statistics/class.UnitCrosstab_DataGrid.php';
include '../classes/Structures_DataGrid/Statistics/class.GenderCitizenshipCrosstab_DataGrid.php'; 
include '../classes/Structures_DataGrid/Statistics/class.ApplicantsByCitizenship_DataGrid.php';
include '../classes/Structures_DataGrid/Statistics/class.ApplicantsByEthnicity_DataGrid.php';
include '../classes/Structures_DataGrid/Statistics/class.ApplicantsByGender_DataGrid.php'; 
include '../classes/Structures_DataGrid/Statistics/class.ApplicantsByInstitution_DataGrid.php';
include '../classes/Structures_DataGrid/Statistics/class.ApplicantsByNumPrograms_DataGrid.php';


/*
* Handle the REQUEST variables.
*/
$periodId = NULL;
if ( isset($_REQUEST['periodId']) ) {
    $periodId = $_REQUEST['periodId'];    
}

$unitId = NULL;
if ( isset($_REQUEST['unitId']) ) {
    $unitId = $_REQUEST['unitId'];    
}

$view = 'applicantsByUnit';
if ( isset($_REQUEST['view']) ) {
    $view = $_REQUEST['view'];    
}

$limit = NULL;
$grain = NULL;
$grainId = 0;
$phdOnly = 0;
if ( isset($_REQUEST['limit']) ) {
    $limit = $_REQUEST['limit'];
    $limitArray = explode('_', $limit);
    $grain = $limitArray[0];
    $grainId = $limitArray[1];
    $phdOnly = $limitArray[2];    
}

if (!$unitId && !$periodId) {
    header('Location: index.php');
}

$unit = new Unit($unitId);
$period = new Period($periodId);

if ($unitId == 22) {

    $viewMenuItems = array(
        'Applicants by Unit'          => 'applicantsByUnit',
        'MSE/MSIT Breakdown'          => 'mseMsitBreakdown',
        'Applicants By Num Programs'  => 'applicantsByNumPrograms',
        'Applicants By Institution'   => 'applicantsByInstitution',
        'Applicants by Ethnicity'     => 'applicantsByEthnicity',
        'Applicants by Citizenship'   => 'applicantsByCitizenship',
        'Applicants by Gender'        => 'applicantsByGender',
        'Gender-Citizenship Crosstab' => 'genderCitizenshipCrosstab'  
        );

} else {
    
    $viewMenuItems = array(
        'Applicants by Unit'          => 'applicantsByUnit',
        'Unit Crosstab'               => 'unitCrosstab',
        'Applicants By Num Programs'  => 'applicantsByNumPrograms',
        'Applicants By Institution'   => 'applicantsByInstitution',
        'Applicants by Ethnicity'     => 'applicantsByEthnicity',
        'Applicants by Citizenship'   => 'applicantsByCitizenship',
        'Applicants by Gender'        => 'applicantsByGender',
        'Gender-Citizenship Crosstab' => 'genderCitizenshipCrosstab'  
        );    
    
}



switch ($view) {

    case 'applicantsByUnit':
        $limitMenuLabel = 'Show';
        $limitMenuItems = array(
            'Total' => 'total_0_0',
            'Total - PhD Only' => 'total_0_1',
            'by Department' => 'department_0_0',
            'by Department - PhD Only' => 'department_0_1',
            'by Program' => 'program_0_0' 
            );

        break;
    
    case 'mseMsitBreakdown':
        $limitMenuLabel = 'Show';
        $limitMenuItems = array(
            'MSE' => 'mse_21_0',
            'MSIT' => 'msit_29_0',
            'MBA/MSE' => 'mba_30_0',
            );

        break;

    case 'unitCrosstab':
        $limitMenuLabel = 'Show Submitted';
        $limitMenuItems = array(
            'by Department' => 'department_0_0',
            'by Department - PhD Only' => 'department_0_1',
            'by Program' => 'program_0_0' 
            );    
        break;
    
    case 'applicantsByCitizenship':
    case 'applicantsByEthnicity':
    case 'applicantsByGender':
    case 'applicantsByInstitution':
    case 'applicantsByNumPrograms':
    case 'genderCitizenshipCrosstab':
        $limitMenuLabel = 'Submitted by Unit';
    default:
        if ( !isset($limitMenuLabel) ) {
            $limitMenuLabel = 'Unit';    
        }
        $limitMenuItems = array(
            $unit->getName() => 'total_0_0',
            $unit->getName() . ' - PhD Only' => 'total_0_1'
            );
        $departmentArray = getUnitDepartments($unitId);
        foreach ($departmentArray as $department) {
            $departmentId = $department['id'];
            $departmentName = $department['name'];
            $limitMenuItems[$departmentName] =  'department_' . $departmentId . '_0';
            if ($departmentId == 3 || $departmentId == 6) {
                $limitMenuItems[$departmentName . ' - Phd Only'] =  'department_' . $departmentId . '_1';    
            }   
        }
        $periodProgramIds = $period->getPrograms();
        foreach ($periodProgramIds as $programId => $programName) {    
            $limitMenuItems[$programName] =  'program_' . $programId . '_0';        
        }
}
   
$tableFactory = new StatisticsTableFactory();
$datagrid = $tableFactory->createTable($periodId, $unitId, $view, 
                            $grain, $grainId, $phdOnly, 'datagrid');

/*
*  Include the "view" template.
*/
include '../inc/tpl.statistics.php';


function getUnitDepartments($unitId) {
    
    $DB_Applyweb = new DB_Applyweb();
    
    // Exclude departments that only include one program.
    $query = "SELECT DISTINCT department.id, department.name
                FROM department
                INNER JOIN lu_domain_department 
                    ON department.id = lu_domain_department.department_id
                INNER JOIN domain_unit ON lu_domain_department.domain_id = domain_unit.domain_id
                INNER JOIN lu_programs_departments 
                    ON department.id = lu_programs_departments.department_id
                INNER JOIN programs_unit 
                    ON lu_programs_departments.program_id = programs_unit.programs_id
                INNER JOIN unit AS program 
                    ON programs_unit.unit_id = program.unit_id
                WHERE domain_unit.unit_id = " . $unitId . "
                AND department.name NOT LIKE '%archiv%'
                AND program.parent_unit_id != " . $unitId . "
                ORDER BY department.name";
    
    $departmentArray = $DB_Applyweb->handleSelectQuery($query);
    return $departmentArray;
}

?> 