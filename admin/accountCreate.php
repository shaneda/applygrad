
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>CMU Online Admissions - Create New Account</title>
<!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
$ethnicities = array();
$err = "";
$mode = "edit";
$title = "";
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$ethnicity = "";
$pass = "";
$passC = "";
$dob = "";
$visaStatus = "";
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";

if(isset($_GET['mode']))
{
	if($_GET['mode'] != "")
	{
		$mode = $_GET['mode'];
	}
}
if($_SESSION['allow_edit'] == true)
{
	// COUNTRIES
	$result = mysql_query("SELECT * FROM countries order by name") or die(mysql_error());
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($arr, $row['iso_code']);
		array_push($countries, $arr);
	}
	
	
	
	//STATES
	
	$result = mysql_query("SELECT * FROM states order by country_id,name")
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($states, $arr);
	}
	
	
	
}//END IF


if(isset($_POST['btnSubmit'] ))
{
	$_SESSION['usermasterid'] == -1;
	$_SESSION['userid'] = -1;
	$sql = "";	
	$mode = $_POST['txtMode'];
	
	$fName = htmlspecialchars($_POST['txtFName']);
	$mName = htmlspecialchars($_POST['txtMName']);
	$lName = htmlspecialchars($_POST['txtLName']);
	
	$email = htmlspecialchars($_POST['txtEmail']);

	$pass = htmlspecialchars($_POST['txtPass']);
	$passC = htmlspecialchars($_POST['txtPassC']);
	//VERIFY FORM VARS HERE
	if($fName == "")
	{
		$err .= "First Name is Required<br>";
	}
	if($lName == "")
	{
		$err .= "Last Name is Required<br>";
	}
	if($email == "" || check_email_address($email)===false )
	{
		$err .= "Email is invalid.<br>";
	}
	
	if($pass == "" && $_SESSION['userid'] == -1)
	{
		$err .= "Password is required.<br>";
	}
	if($pass != $_POST['txtPassC'] && $_SESSION['userid'] == -1)
	{
		$err .= "Passwords do not match.<br>";
	}
	if(strlen($pass) < 6) // || strlen($pass) > 8
	{
		$err .= "Password must be at least 6 characters.<br>";
	}
/*	if(strlen($pass) < 6 || strlen($pass) > 8)
	{
		$err .= "Password must be between 6 and 8 characters.<br>";
	}   */
	$matched = ereg ("[0-9]+", $pass);
	if($matched != false      )
	{
	
	}else
	{
		$err .= "Password must contain at least 1 number.<br>";
	}
	
	
	
	//DO INSERT/UPDATE
	if($err == "")
	{
	
	
	//FIRST LOOKUP USER INFO BASED ON FIRSTNAME, LASTNAME
	//IF MATCHED, UPDATE USER
	$sql = "select users.id
		from users
		where email = '".htmlspecialchars($_POST['txtEmail'])."' ";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$err = "ERROR: ACCOUNT ALREADY EXISTS<br>";
		//$_SESSION['usermasterid'] = $row['id'];
	}
	
	//THEN DO LOOKUP ON USERTYPE - IF ONE EXISTS FOR USER, UPDATE, IF NOT, INSERT
	if($err == "")
	{
		if($_SESSION['usermasterid'] == -1)
		{
			$guid = makeGuid();
			$sql = "insert into users(email, password,title,firstname,middlename,lastname,initials, signup_date, verified, guid) 
			values( '".$email."', '".sha1($pass)."', '".$title."', '".$fName."', '".$mName."', '".$lName."', '".$initials."', '".date("Y-m-d h:i:s")."', 0, '".$guid."')";
			mysql_query( $sql) or die(mysql_error());
			$_SESSION['usermasterid'] = mysql_insert_id();
		}
		
		
		
		$sql = "SELECT * FROM lu_users_usertypes where user_id=".$_SESSION['usermasterid'] . "  and usertype_id=5";
		$result = mysql_query($sql)
		or die(mysql_error().$sql);
		$doInsert = true;
		while($row = mysql_fetch_array( $result )) 
		{
			$doInsert = false;
		}
		if($doInsert == true)
		{
			$sql = "insert into lu_users_usertypes(user_id, usertype_id,domain) values( ".$_SESSION['usermasterid'].", 5, NULL)";
			mysql_query( $sql) or die(mysql_error());
			$_SESSION['userid'] = mysql_insert_id();
			$_SESSION['usertypeid'] = 5;
			
			//USER INFO
			$sql = "SELECT * FROM users_info where user_id=".$_SESSION['userid'];
			$result = mysql_query($sql)
			or die(mysql_error());
			$doInsert = true;
			while($row = mysql_fetch_array( $result )) 
			{
				$doInsert = false;
			}
			if($doInsert == true)
			{
				$sql = "insert into users_info(user_id)values( ".$_SESSION['userid'].")";
				mysql_query( $sql) or die(mysql_error());
			}
			$_SESSION['firstname'] = $fName;
			$_SESSION['lastname'] = $lName;
			$_SESSION['email'] = $email;
			
			
			
		}
		
		
		
		
		header("Location: home.php");
		}//end if err
	}
}


//GET USER INFO
$sql = "SELECT * FROM users left outer join users_info on users_info.user_id = users.id where users.id=".$_SESSION['userid'];

$result = mysql_query($sql)
or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$email = $row['email'];
	$title = $row['title'];
	$fName = $row['firstname'];
	$mName = $row['middlename'];
	$lName = $row['lastname'];
	$initials = $row['initials'];
	
	$_SESSION['firstname'] = $row['firstname'];
	$_SESSION['lastname'] = $row['lastname'];
	$_SESSION['userid'] = $row['uid'];
	$_SESSION['email'] = $row['email'];
}
?>
<script language="javascript">

function validate(key,val)
{
	var err = '';
	if(val == '' || val.length == 0)
	{
		err += key + ' is required.';
	}
	if(err != '')
	{
		alert(err + ' ' + val);
		return false;
	}
}
</script>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" onSubmit="return verifyForm()"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../apply/logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../apply/index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			<div style="float:left;"> &nbsp;&nbsp;<a href="../apply/programs.php">Previous </a>
				&nbsp;&nbsp;&nbsp;<a href="../apply/scores.php">Next </a></div>
			<br style="clear:left">
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">New Account </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
            <div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
				<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
			</div>
            <span class="tblItem">&nbsp;= required</span>
			<span class="errorSubtitle"><br><br>

<?=$err;?></span>            
            <table width="100%" border="0" cellspacing="2" cellpadding="4">
              <tr>
                <td width="50%" class="tblHead2">Personal Details </td>
              </tr>
              <tr>
                <td valign="top" class="tblItem"><table width="100%" border="0" cellspacing="0" cellpadding="4">
                  <tr>
                    <td width="100" align="right" class="tblItem"><strong>First Name:</strong></td>
                    <td class="tblItem"><? showEditText($fName, "textbox", "txtFName", $_SESSION['allow_edit'], true,null,true,40); ?></td>
                  </tr>
                  <tr>
                    <td width="100" align="right" class="tblItem"><strong>Middle Name:</strong></td>
                    <td class="tblItem"><? showEditText($mName, "textbox", "txtMName", $_SESSION['allow_edit'],false,null,true,40); ?></td>
                  </tr>
                  <tr>
                    <td width="100" align="right" class="tblItem"><strong>Last Name:</strong></td>
                    <td class="tblItem"><? showEditText($lName, "textbox", "txtLName", $_SESSION['allow_edit'], true,null,true,40); ?></td>
                  </tr>

                  <tr>
                    <td width="100" align="right" class="tblItem"><strong>Email</strong>:</td>
                    <td class="tblItem"><? showEditText($email, "textbox", "txtEmail", $_SESSION['allow_edit'], true,null,true,40); ?></td>
                  </tr>
                  <tr>
                    <td width="100" align="right" class="tblItem"><strong>Password:</strong></td>
                    <td class="tblItem"><? 
					$req = false;
					if($pass == "" && $_SESSION['userid'] == -1)
					{
						$req = true;
					}
					showEditText($pass, "password", "txtPass", $_SESSION['allow_edit'], $req,null,true,10); ?></td>
                  </tr>
                  <tr>
                    <td width="100" align="right" class="tblItem"><strong>Confirm Password:</strong> </td>
                    <td class="tblItem"><?
					$req = false;
					if($pass == ""  && $_SESSION['userid'] == -1)
					{
						$req = true;
					}
					showEditText($passC, "password", "txtPassC", $_SESSION['allow_edit'], $req,null,true,10); ?></td>
                  </tr>
                  <tr>
                    <td align="right" class="tblItem">&nbsp;</td>
                    <td class="tblItem"><strong>All Fields are required. Please create a password that only you can remember. Password must be between 6-8 characters and contain at least 1 number. </strong></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="tblItem"><input name="txtMode" type="hidden" id="txtMode" value="<?=$mode;?>">
                  <input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="Save Details" ></td>
              </tr>
            </table>
            <!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../apply/contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
