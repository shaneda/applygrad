<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$sql = "";
$err = "";
$interests = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{

				mysql_query("DELETE FROM lu_application_interest WHERE interest_id=".$itemId)
				or die(mysql_error().$sql);

				mysql_query("DELETE FROM lu_programs_interests WHERE interest_id=".$itemId)
				or die(mysql_error().$sql);

				mysql_query("DELETE FROM interest WHERE id=".$itemId)
				or die(mysql_error().$sql);
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select interest.id,
interest.name
from interest
order by interest.name";

$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($interests, $arr);
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT
	department.id,
	department.name
	FROM lu_programs_departments

	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Interests';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Interests</span><br />
<br />
	<a href="interestEdit.php">Assign interest to program</a> | <a href="interestItemEdit.php">Add  Interest Item </a><br />
	<br />
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td width="60" align="right">Delete</td>
      </tr>
     <? for($i = 0; $i < count($interests); $i++){?>
	<tr>
        <td><a href="interestItemEdit.php?id=<?=$interests[$i][0]?>"><?=$interests[$i][1]?></a></td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$interests[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	<? } ?>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>