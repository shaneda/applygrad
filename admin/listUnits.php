<?php
include '../inc/config.php';
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';  
include '../classes/class.Unit.php';
include '../classes/class.Period.php';

$unitId = NULL;
if ( isset($_REQUEST['unitId']) ) {
    $unitId = $_REQUEST['unitId'];
}

$applyweb = new DB_Applyweb();

$query = "SELECT unit.* FROM unit WHERE parent_unit_id IS NULL";
if ($unitId) {
    $query = "SELECT unit.* FROM unit WHERE unit_id = " . $unitId; 
}
$rootUnits = $applyweb->handleSelectQuery($query, 'unit_id');

//DebugBreak();
echo '<h2>Units/Periods and Their Current Domains/Programs</h2>';
$bgcolor = '#ffffff';
foreach ($rootUnits as $rootUnit) {
    
    $unitId = $rootUnit['unit_id'];
    $unit = new Unit($unitId);
    
    $display = displayDescendants($unit, $bgcolor);
    //echo $display . '<hr/>';
    echo $display;
    
    if ($bgcolor == '#ffffff') {
        $bgcolor = '#dddddd';
    } else {
        $bgcolor = '#ffffff';
    }
}

function displayDescendants($unit, $bgcolor) {

    global $applyweb;
    $unitId = $unit->getId();
    
    $display = '<ul style="background-color:' . $bgcolor . ';">';
    $display .= '<li><a name="' . $unitId . '"><b>' . $unit->getName() . '</b> (unit id: ' . $unitId . ')</a>';
    
    $domainQuery = "SELECT domain.id, domain.name 
                    FROM domain_unit
                    INNER JOIN domain ON domain_unit.domain_id = domain.id
                    WHERE domain_unit.unit_id = " . $unitId;
    $domainResult = $applyweb->handleSelectQuery($domainQuery);
    foreach ($domainResult as $domainRecord) {
        $domainId = $domainRecord['id'];
        $display .= '<br/><i>corresponds to domain: ' . $domainRecord['name'];
        $display .= ' (<a href="listDomains.php#' . $domainId . '">domain id: ' . $domainId . '</a>)</i>';
    }
    
    $programQuery = "SELECT programs.id, CONCAT( degree.name, ' ', linkword, ' ', fieldsofstudy.name ) AS name
                        FROM programs_unit
                        INNER JOIN programs ON programs_unit.programs_id = programs.id
                        INNER JOIN degree ON programs.degree_id = degree.id
                        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
                        WHERE programs_unit.unit_id = " . $unitId;
    $programResult = $applyweb->handleSelectQuery($programQuery);
    foreach ($programResult as $programRecord) {
        $display .= '<br/><i>corresponds to program: ' . $programRecord['name'] . ' (program id: ' . $programRecord['id'] . ')</i>';
    }
    
    $unitPeriods = $unit->getPeriods();
    if ( count($unitPeriods) > 0) {
        $display .= '<br/><br/><i>has period(s)</i>:<br/><br/>';
        $display .= '<ul>';
        //DebugBreak();
        foreach ($unitPeriods as $period) {
            $display .= displayPeriod($period);
        }
        $display .= '</ul>';
    }
    
    $childUnits = $unit->getChildUnits();
    if ( count($childUnits) > 0 ) {
        $display .= '<br/><i>has subunits</i>:<br/>';
        foreach ($childUnits as $childUnit) {
            $display .= '<br/>';
            $display .= displayDescendants($childUnit, $bgcolor);
        }        
    }

    $display .= '</li></ul>';
    
    return $display;  
}

function displayPeriod($period) {
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $status = $period->getStatus();
    $activePeriodStyle = 'style="margin: 2px;"';
    if ($status == 'active') {
        $activePeriodStyle = 'style="margin: 2px; border-left: 2px solid red;"';
    }
    if ($status == 'future') {
        $activePeriodStyle = 'style="margin: 2px; border-left: 2px solid orange;"';
    }
    $display = '<li ' . $activePeriodStyle. '><b>'; 
    $display .= $startDate . ' to ' . $endDate . '</b>';
    $periodDescription = $period->getDescription();
    if ($periodDescription) {
        $display .= ' (' . $periodDescription . ')';
    }
    $display .= ' (period id: ' . $period->getId() . ')';
    
    $subperiods = $period->getChildPeriods();
    $subperiodCount = count($subperiods);
    if ($subperiodCount > 0) {
        $display .= '<br/><i>includes subperiod(s)</i>:<ul>';
        foreach ($subperiods as $subperiod) {
            $display .= displayPeriod($subperiod);
        }
        $display .= '</ul>';    
    }
    
    $programs = $period->getPrograms();
    $programCount = count($programs);
    if ($programCount > 0) {
        $display .= '<br/><i>includes program(s)</i>:<ul>';
        foreach ($programs as $programId => $program) {
            $display .= '<li>' . $program . ' (<a href="#' . $programId . '">unit id: ' . $programId .'</a>)</li>';    
        }
        $display .= '</ul>';
    }
    $display .= '<br/></li>';
    
    return $display;     
}
  
?>