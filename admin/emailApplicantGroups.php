<?PHP
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

include '../classes/class.Department.php';

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Email Applicants by Round';

$pageCssFiles = array(
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<script type="text/javascript">
    
    function toggleSelect( element, category )
    {
        var form = document.getElementById(category + 'Form');
        var checkboxStatus = element.checked;
        var formInputs = form.getElementsByTagName('input');
        for ( var i = 0; i < formInputs.length; i++)
        {
            if ( formInputs[i].name == 'application_id[]' ) 
            {
                formInputs[i].checked = false;
                if ( formInputs[i].disabled == false )
                {
                    formInputs[i].checked = checkboxStatus;            
                }
            }
        }
    }
    </script>

<br />
<?php
    if(isset($_GET['r']))
{
    $round = intval($_GET['r']);
} else {
    $round = 1;
}
?>
<form id="applicantsForm" name="applicantsForm" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
    <div style="margin:7px; ">
    <span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Send Email to Round <?=$round?> Applicants <!-- InstanceEndEditable --></span><br />
<br />

<?php
$domains = array();
$content = array();
$deptId = -1;
//$sysemail = "applyweb+usubapps@cs.cmu.edu";
switch ($hostname)
{
    case "APPLY.STAT.CMU.EDU":  
        $sysemail = "scsstats@cs.cmu.edu";
        break;
    case "APPLYGRAD-INI.CS.CMU.EDU":  
        $sysemail = "scsini@cs.cmu.edu";
        break;
    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
        $sysemail = "scsdiet@cs.cmu.edu";
        break;
    default:
        $sysemail = "applygrad@cs.cmu.edu";
 } 

if(isset($_GET['id']))
{
    $deptId = intval($_GET['id']);
}


$domainArray = Department::getDomains($deptId);
$domainName = $domainArray[0]['name'];
$thisDomainId =  $domainArray[0]['id'];
                
/* PLB added query to get the content id to make the link go straight to the content 10/9/09. 
* Compbio (department id 8) is hard-coded to ensure the CompBio domain (domain id 2) 
* content id is retrieved when the department is in two domains.
*/
if ($deptId == 8) {
    $sql = "select content.id from content
    where content.name='Applicant Submission Reminder' and content.domain_id=2";    
} else {
    $sql = "select content.id from content
    left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
    where content.name='Round " . $round . " Template' and content.domain_id=".$thisDomainId;    
}
$result = mysql_query($sql) or die(mysql_error());
$contentId = NULL;
while( $row = mysql_fetch_array( $result ) ) {
    $contentId = $row['id'];
}
if ($contentId) {
    $contentLink = "contentEdit.php?id=" . $contentId;
} else {
    $contentLink = "content.php";    
}
?>
    <!-- InstanceBeginEditable name="mainContent" -->
    <table width="600" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <!--
        <td><a href="content.php">(Edit email template)</a></td>
        -->
        <td><a href="<?php echo $contentLink; ?>">(Edit email template)</a></td>
        <td align="right"><? showEditText("Send", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
    </table>
    <br />
    <br />
<br />
    <?
//GET EMAIL TEMPLATE
$str = "";
/* 
* PLB hard-coded CompBio (department id 8) to ensure the CompBio domain (domain id 2) 
* content is retrieved when the department is in two domains 10/9/09 .
*/
if ($deptId == 8) {
    $sql = "select content from content
    where content.name='Applicant Submission Reminder' and content.domain_id=2";    
} else {
    $sql = "select content from content
    left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
    where content.name='Round " . $round . " Template' and content.domain_id=".$thisDomainId;    
}

$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $str = html_entity_decode($row["content"]);
}
?>
    <div id="div1" style="width:550px; background-color:#CCCCCC; padding:10px "><?=$str?></div><br />

<?

/*
* PLB added test to restrict query to applications from the active submission period 10/9/09.
* A department may belong to more than one domain, 
* but it should only be associated with one active submission period.
*/
//  include '../inc/emailUnsubmitted.inc.php';
/*  List by round

  SELECT DISTINCT
    period_application.application_id AS application_id,
    lu_users_usertypes.id AS id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name,
    users.email
    FROM application
    // student bio, application status 
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
     INNER JOIN period_application ON application.id = period_application.application_id 
      INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id 
      INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id 
      LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          round AS promotion_status_round
                          FROM promotion_status
                          WHERE promotion_status.department_id = 50
                          ) AS promotion_status ON application.id = promotion_status.application_id 
WHERE period_application.period_id = 402 AND lu_programs_departments.department_id = 50 
AND application.submitted = 1 
GROUP BY application.id 
ORDER BY users.lastname, users.firstname

ROUND 2 QUERY

SELECT DISTINCT
    application.id AS application_id,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name
      
    FROM application
    // student bio, application status 
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
     INNER JOIN period_application ON application.id = period_application.application_id 
      INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id 
      INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id 
      LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          MIN( IFNULL(round2, 0) ) as min_vote,
                          MAX( IFNULL(round2, 0) ) as max_vote,
                          COUNT(round2) AS vote_count 
                          FROM review
                          LEFT OUTER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
                          WHERE review.round = 1
                          AND review.fac_vote = '0'
                          AND review.round2 IS NOT NULL
                          AND ( review.committee_vote = 1
                          OR lu_users_usertypes.usertype_id IS NULL
                          OR lu_users_usertypes.usertype_id != 1 )
                          AND review.department_id = 50
                          GROUP BY application_id
                          ) AS round2_votes ON application.id = round2_votes.application_id 
                         LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          round AS promotion_status_round
                          FROM promotion_status
                          WHERE promotion_status.department_id = 50
                          ) AS promotion_status ON application.id = promotion_status.application_id 
WHERE period_application.period_id = 402 AND lu_programs_departments.department_id = 50 
AND (promotion_status_round = 2) AND application.submitted = 1 
GROUP BY application.id 
ORDER BY users.lastname, users.firstname




ROUND 3 QUERY

SELECT DISTINCT
    application.id AS application_id,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name
    FROM application
    // student bio, application status 
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
     INNER JOIN period_application ON application.id = period_application.application_id 
      INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id 
      INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id 
      LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          round AS promotion_status_round
                          FROM promotion_status
                          WHERE promotion_status.department_id = 50
                          ) AS promotion_status ON application.id = promotion_status.application_id 
WHERE period_application.period_id = 402 AND lu_programs_departments.department_id = 50 
AND promotion_status_round = 3 AND application.submitted = 1 
GROUP BY application.id 
ORDER BY users.lastname, users.firstname



ROUND 4 QUERY

    SELECT DISTINCT
    application.id AS application_id,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name
    FROM application
    // student bio, application status 
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    
     INNER JOIN period_application ON application.id = period_application.application_id 
      INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id 
      INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id 
      LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          round AS promotion_status_round
                          FROM promotion_status
                          WHERE promotion_status.department_id = 50
                          ) AS promotion_status ON application.id = promotion_status.application_id 
 WHERE period_application.period_id = 402 AND lu_programs_departments.department_id = 50 
 AND promotion_status_round = 4 AND application.submitted = 1 
 GROUP BY application.id 
 ORDER BY users.lastname, users.firstname



*/
//PULL USERS FOR THIS DEPARTMENT
$sql = "select
    lu_users_usertypes.id as id,
    period_application.application_id,
    concat(users.lastname, ', ',users.firstname) as name,
    users.email
    from lu_users_usertypes
    inner join users on users.id = lu_users_usertypes.user_id
    left outer join users_info on users_info.user_id = lu_users_usertypes.id
    left outer join countries on countries.id = users_info.address_perm_country
    left outer join application on application.user_id = lu_users_usertypes.id
    left outer join lu_application_programs on lu_application_programs.application_id = application.id
    left outer join programs on programs.id = lu_application_programs.program_id
    left outer join degree on degree.id = programs.degree_id
    left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
    left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";

// PLB added period join 10/9/09
$sql .= " INNER JOIN period_application ON application.id = period_application.application_id";

$sql .= " INNER JOIN promotion_status ON promotion_status.application_id = period_application.application_id AND promotion_status.round = ";
$sql .= $round;
    
$sql .=    " where lu_users_usertypes.usertype_id = 5";

// PLB added period join 10/9/09

if(isset($_GET['period']))
{   
    $activePeriodIds = array($_GET['period']);;
}

$activePeriodCount = count($activePeriodIds);
if ($activePeriodCount  == 0 ) {
    // Super kluge to retrieve 0 records when there are 0 active periods
    $sql .= " AND period_application.period_id = -1";
} elseif ($activePeriodCount == 1) {
    $sql .= " AND period_application.period_id = " . $activePeriodIds[0]; 
} else { 
    // Kluge to handle compbio's being in two, non-identical domains
    $sql .= " AND period_application.period_id IN (" . implode(',', $activePeriodIds) . ")";    
}

$sql .=  " and lu_programs_departments.department_id=".$deptId . " group by users.email order by users.lastname, users.firstname";

$result = mysql_query($sql) or die(mysql_error().$sql);

if(isset($_POST['btnSubmit']) && $deptId > -1)
{

    if($str != "")
    {

        //PULL USERS FOR THIS DEPARTMENT

        ?>
        <table width="500" border="0" cellspacing="0" cellpadding="4">
        <?  
        // DebugBreak();   
        echo "Mail sent to:<br>";
        $str2 = "";
        if (isset($_POST['application_id'])) {
            $applicationsToSendEmail =  $_POST['application_id'];
            // DebugBreak();
            while($row = mysql_fetch_array( $result ))
            {
                
                $applicationId = $row['application_id'];
                if (in_array($applicationId, $applicationsToSendEmail)) {
                    $mailSieveString = '[' . $domainName . ':';
                    $mailSieveString .= implode( '-', getApplicantProgramIds($applicationId) ) . ':';
                    $mailSieveString .= $applicationId . ']';
                    
                    $vars = array(
                    array('userid',$row['email'])
                    );
                    $str2 = parseEmailTemplate2($str, $vars );
                    //SEND EMAIL
                    sendHtmlMail($sysemail, $row['email'], "Applicant Submission Reminder", $str2, "reminder", 
                                    "", $mailSieveString);//to user
                    ?>
                    <tr>
                    <td><strong><?=$row['name']?></strong></td>
                    <td><em>(<?=$row['email']?>)</em></td>
                  </tr>  
                <? }
            }
        } ?>
            </table>
    <?
    }else
    {
        echo "<strong>Email Template Cannot be Empty.</strong> Please edit the template <a href='content.php'>here<a/>";
    }//end if 
}else
{
//    echo "Mail will be sent to";
    ?>
    <table width="500" border="0" cellspacing="0" cellpadding="4">
        <tr>
            <th>
                <input type='checkbox' id='nonMembersToggle' class='toggleCheckbox' name='toggle' value='' onclick='toggleSelect(this, "applicants")' />
            </th>
            <th align='center'>Name</th>
            <th>Email</th>                        
        </tr>
    <?     $numemails2send = mysql_num_rows($result);
    echo "Mail will be sent to the following ".$numemails2send. " applicants:<br>";
     while($row = mysql_fetch_array( $result )){    ?>
      <tr>
        <td class='even' align='center'>
                            <input type='checkbox' class='shiftclick' name='application_id[]' value='<?=$row['application_id']?>' />
        </td>
        <td class='even'>
            <a href='../review/userroleEdit_student_print.php?id=<?=$row['id']?>&amp;applicationId=<?=$row['application_id']?>' target='_blank'><?=$row['name']?></a>
        </td>
        <td><em>(<?=$row['email']?>)</em></td>
      </tr>
    <? } ?>
    </table>
    <? } ?>
    <!-- InstanceEndEditable -->
    </div>
    </td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>
<script type="text/javascript" src="../javascript/jquery-1.2.3.min.js"></script> 
<script type="text/javascript">
    jQuery.noConflict();
</script> 
<script type="text/javascript" src="../javascript/jquery.shiftclick.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('input.shiftclick').shiftClick();
    });
</script>