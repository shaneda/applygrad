<?php
// Standard applyweb includes
include "../inc/config.php";
include "../inc/session_admin.php";

// DB include
require "../classes/DB_Applyweb/class.DB_Applyweb.php";

// DataGrid include
require_once("Structures/DataGrid.php");

// DB
$db_applyweb = new DB_Applyweb();

function getDepartmentPrograms($allPrograms, $departmentNumber) {
    $deptProgs = array();
    foreach ($allPrograms as $prog) {
        if ($prog['department_id'] == $departmentNumber) {
         array_push($deptProgs, $prog['program_id']);      
        } 
    }
    return $deptProgs;
}

// Data
$requestDepartmentId =  intval($_GET['departmentId']);
$requestWaiveProgramId =  filter_input(INPUT_GET, 'waiveProgramId', FILTER_VALIDATE_INT);
$programs = GetPrograms();
$departmentPrograms =  getDepartmentPrograms($programs, $requestDepartmentId);
$departmentProgramsString = implode(",", $departmentPrograms);

// Page header
$pageTitle = 'Administer Applications';

$pageCssFiles = array(
    '../css/jquery.autocomplete.css',
    '../css/administerApplications.css'
    );
    
include '../inc/tpl.pageHeader.php';
?>


<div style="margin: 10px;">
    <span class="title">
        <span style="font-size: 18px;">Rejected Applications</span>  
    </span>
</div>

<div>
Select program to grant access to applicants:
</div>

<div style="margin: 10px;">

<form name="selectProgram" id="sendToProgram" action="" method="get">
    <select name="waiveProgramId">
        <?php
        foreach ($programs as $selectProgramId => $programData)
        {
            $selected = '';
            if ($selectProgramId == $requestWaiveProgramId)
            {
                $selected = 'selected';
            }
            
            echo '<option value="' . $selectProgramId . '"' . $selected . '>';
            echo $programData['department_name'] . ': ' . $programData['program_name'];
            echo '</option>'; 
        }
        ?>
    </select>
    <input type="hidden" name="departmentId" value=<?php echo $requestDepartmentId; ?> /> 
    <input type="submit" value="Submit" />
</form>

<?php

if ($requestWaiveProgramId != NULL)
{
    // Get remaining data
    $rejectsCount = 0;
    
    // just use the first one because need at least one program in a period, so just use the first one to determine period
    $latestPeriod = GetLatestPeriod($departmentPrograms[0]);
    if (isset($latestPeriod[0]['period_id']))
    {
        $rejects = GetRejects($latestPeriod[0]['period_id'], $departmentProgramsString, $requestWaiveProgramId);
        $rejectsCount = count($rejects);
    }
    if (isset($_POST) && sizeof($_POST) > 0) {
        $appsToAllow = $_POST['applicationId'];
        
            
            foreach ($appsToAllow as $currApp) {
                $waiveSql = "insert into rejection_waivers (lu_application_programs_id, permitted_program_id, waived)
                VALUES (" . $currApp . ", " . $requestWaiveProgramId . ", 1)  
                ON DUPLICATE KEY UPDATE waived = 1";
                
             $waivedPrograms = $db_applyweb->handleInsertQuery($waiveSql);
        }
        
        foreach ($rejects as $reject) {
            $curLapId = $reject['lap_id'];
            $test =  FALSE;  
            foreach($appsToAllow as $app) {
                    if (intval($app) == intval($curLapId)) {
                        $test = TRUE;
                        }
                    }

            if ($test === FALSE) {            
                $waiveSql = "insert into rejection_waivers (lu_application_programs_id, permitted_program_id, waived)
                VALUES (" . $reject['lap_id'] . ", " . $requestWaiveProgramId . ", 0) 
                ON DUPLICATE KEY UPDATE waived = 0";
            
                $waivedPrograms = $db_applyweb->handleInsertQuery($waiveSql);
            }
        }  
        $rejects = GetRejects($latestPeriod[0]['period_id'], $departmentProgramsString, $requestWaiveProgramId);
        $rejectsCount = count($rejects);      
    }
    
    echo '<br><div style="font-size: 14px; font-weight: bold;">';
    echo $programs[$departmentPrograms[0]]['department_name']; // .  ': ' . $programs[$requestProgramId]['program_name'];
    echo '<br>' . $latestPeriod[0]['umbrella_name'];
    echo ' (' . $latestPeriod[0]['start_date'] . ' - ' . $latestPeriod[0]['end_date'] . ')';
    echo '<br>' . $rejectsCount . ' rejected applications';
    echo '</div><br>';
    
    if ($rejectsCount > 0)
    {
        // create form/datagrid
      //  $action = 'allow_view_rejects.php?programId=' . $requestProgramId . '&period=' . $latestPeriod[0]['period_id'] . '&waiveProgramId=' . $requestWaiveProgramId;
      
        $action = './handle_rejects.php?waiveProgramId=' . $requestWaiveProgramId . '&departmentId=' . $requestDepartmentId;
        echo '<form name="exportApplications" action="' . $action . '" method="post">';
        $datagrid = new Structures_DataGrid();
        $datagrid->bind($rejects ,array() , 'Array');
        $datagrid->addColumn(new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null, 'createCheckbox()'));
        $datagrid->addColumn(new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '30%', 'align' => 'left'), null));
        $datagrid->addColumn(new Structures_DataGrid_Column('Application ID', 'application_id', 'application_id', 
                array('width' => '12%', 'align' => 'left'), null));
        $datagrid->addColumn(new Structures_DataGrid_Column('Luu ID', 'luu_id', 'luu_id', 
                array('width' => '12%', 'align' => 'left'), null));
        $datagrid->addColumn(new Structures_DataGrid_Column('Users ID', 'users_id', 'users_id', 
                array('width' => '12%', 'align' => 'left'), null));
        
   /*     $datagrid->addColumn(new Structures_DataGrid_Column('Waive for Privacy', 'application_id', 'application_id', 
                array('width' => '12%', 'align' => 'left'), null, 'createPrivacyCheckbox()'));
                */

        
        $datagrid->render();
        
        echo '<input type="submit" value="Allow Other Program Access" />';
 /*       echo '<input type="button" value="Save Permission" onclick="allowOtherProgram()" />';     */
        echo '</form>'; 
    }
}
?>
</div>

<?php
// Page footer
include '../inc/tpl.pageFooter.php';

function GetPrograms()
{
    global $db_applyweb;
    
    $query = "SELECT 
                    programs.id AS program_id,
                    department.id AS department_id,
                    department.name AS department_name, 
                    CONCAT(
                        degree.name,
                        ' ',
                        programs.linkword,
                        ' ',
                        fieldsofstudy.name
                    ) AS program_name
                FROM programs
                INNER JOIN lu_programs_departments
                    ON programs.id = lu_programs_departments.program_id
                INNER JOIN department
                    ON lu_programs_departments.department_id = department.id
                INNER JOIN degree 
                    ON degree.id = programs.degree_id
                INNER JOIN fieldsofstudy 
                    ON fieldsofstudy.id = programs.fieldofstudy_id
                WHERE programs.enabled = 1
                AND department.name NOT LIKE '%archive%'
                ORDER BY department_name, program_name";
    
    $programs = $db_applyweb->handleSelectQuery($query, 'program_id');
    
    return $programs;
}

function GetLatestPeriod($programId)
{
    global $db_applyweb;
    
    $periodIdQuery = "SELECT MAX(period_program.period_id) AS latest_period_id 
                        FROM period_program
                        INNER JOIN programs_unit
                            ON period_program.unit_id = programs_unit.unit_id
                        WHERE programs_unit.programs_id = " . intval($programId);
    
    $periodIdArray = $db_applyweb->handleSelectQuery($periodIdQuery);   
    
    if (isset($periodIdArray[0]['latest_period_id']))
    {
        $periodQuery = "SELECT 
                                period_umbrella.period_id,
                                period_umbrella.umbrella_name,
                                period.start_date,
                                period.end_date
                            FROM period_umbrella
                            INNER JOIN period 
                                ON period_umbrella.period_id = period.period_id
                            WHERE period_umbrella.period_id = " . $periodIdArray[0]['latest_period_id'];
        
        $period = $db_applyweb->handleSelectQuery($periodQuery);
        
        return $period;
    } 
    else
    {
        return null;
    }
}

function GetRejects($periodId, $programId, $permitted_program_id)
{
    global $db_applyweb;
  //  $permitted_program_id = 100036;
    $query = "SELECT 
                    application.id AS application_id, 
                    application.user_id AS luu_id, 
                    users.id AS users_id,
                    CONCAT(users.lastname, ', ', users.firstname) AS name,
                    if(application.masters_review_waiver = 1 
                        OR (rejection_waivers.waived = 1 AND rejection_waivers.permitted_program_id = " . $permitted_program_id
                . "), 1, 0) as waived_by_perm,
                    lu_application_programs.id as lap_id 
                FROM application
                INNER JOIN period_application
                    ON period_application.period_id = " . intval($periodId) . "
                    AND application.id = period_application.application_id
                INNER JOIN lu_application_programs 
                    ON lu_application_programs.program_id IN (" . $programId . ")"  . "
                    AND (
                        lu_application_programs.admission_status IS NULL
                        OR lu_application_programs.admission_status = 0
                    )
                    AND application.id = lu_application_programs.application_id
                INNER JOIN (
                        SELECT application_id, COUNT(*) AS admit_count
                        FROM lu_application_programs
                        WHERE admission_status IS NOT NULL
                        AND admission_status > 0    
                    ) AS admit_programs
                    ON application.id = admit_programs.admit_count = 0
                LEFT OUTER JOIN rejection_waivers ON rejection_waivers.lu_application_programs_id = lu_application_programs.id 
                AND lu_application_programs.program_id IN (" . $programId . ")" .
                " AND rejection_waivers.permitted_program_id = " . $permitted_program_id .
                " INNER JOIN lu_users_usertypes
                    ON application.user_id = lu_users_usertypes.id
                INNER JOIN users
                    ON lu_users_usertypes.user_id = users.id
                WHERE 
                    application.submitted = 1
                  /*  AND application.masters_review_waiver = 1  removed for testing  */   
                ORDER BY users.lastname, users.firstname";
     
    $rejects = $db_applyweb->handleSelectQuery($query);
    
    return $rejects;
}

function createCheckbox($params)
{
    extract($params);
   // debugbreak();
    $permittedStatus = "";
    if ($params['record']['waived_by_perm'] == 1) {
        $permittedStatus = "checked";
    }
    $checkbox = '<input type="checkbox" name="applicationId[]" id="applicationId[]" value="' . $record['lap_id'] . ' " '. $permittedStatus . ' />';
    return $checkbox;    
}

 
?>
