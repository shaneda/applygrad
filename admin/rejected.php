<?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
include '../inc/specialCasesAdmin.inc.php';

	$allowUser = true;
	$departmentName = "";
	$s = -1;
	$sType = "(all)";

	$id = -1;
	if(isset($_GET['id']))
	{
		$id = intval($_GET['id']);
	}
	
	$sql = "select name from department where id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$departmentName = $row['name'];
	}

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
}    

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Rejected/Waitlist Applicants';

$pageCssFiles = array(
    '../css/applicantGroups_assign.css' 
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }   
}
?>

<div id="pageHeading">
<div id="departmentName">
<?php
echo $departmentName;
if ($applicationPeriodDisplay) {
    echo $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle"><?php echo $pageTitle; ?></div>
</div> 

<?php
if (isset($_POST['saveEnableRecycling']))
{
    saveEnableRecycling();
}
$enableRecycling = getEnableRecycling();
$enableRecyclingNoChecked = 'checked="checked"'; 
$enableRecyclingYesChecked = '';
if ($enableRecycling)
{
    $enableRecyclingNoChecked = '';
    $enableRecyclingYesChecked = 'checked="checked"';    
}
$enableRecyclingDisabled = 'disabled';
if ($_SESSION['A_usertypeid'] == 0 || ($_SESSION['A_usertypeid'] == 1 && $_SESSION['roleDepartmentId'] == $id))
{
    $enableRecyclingDisabled = '';   
}  
?>
<div style="width: 40%; border: 1px dotted gray; padding: 5px;">
<form accept-charset="utf-8" name="enableRecyclingForm" id="enableRecyclingForm" action="" method="post">
Allow other departments to review rejected applications?
<input type="radio" name="enableRecycling" id="enableRecyclingNo" value="0" 
    <?php echo $enableRecyclingNoChecked; ?> <?php echo $enableRecyclingDisabled; ?> /> No
<input type="radio" name="enableRecycling" id="enableRecyclingYes" value="1" 
    <?php echo $enableRecyclingYesChecked; ?>  <?php echo $enableRecyclingDisabled; ?> /> Yes
&nbsp;&nbsp;
<input type="submit" name="saveEnableRecycling" id="saveEnableRecycling" value="Save" <?php echo $enableRecyclingDisabled; ?> />
</form>
</div>


  <form accept-charset="utf-8" id="form1" name="form1" action="" method="post">

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">

	<?
	include '../inc/C_Spreadsheet_01a.php.inc';
	$spst = new Spreadsheet;

    /*
    * NOTE: for this #!%$&@!!@ spreadsheet business to work, 
    * the first column must be the user_id and the second column 
    * must be the application id. NO EXCEPTIONS. 
    */
	$sql = "select
		lu_users_usertypes.id as id,
        application.id AS application_id,
		concat(users.lastname, ', ',users.firstname) as name,
		users.email,
		GROUP_CONCAT(concat(' ',degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,";
    if (isIniDepartment($id) || isDesignDepartment($id))
    {
        $sql .= " status.status";        
    }
    else
    {
        $sql .= " if( lu_application_programs.admission_status = 1 , 'Waitlist' , '') as status";    
    }    
        
		
        
	$sql .= " from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		left outer join application on application.user_id = lu_users_usertypes.id";

    // PLB added period join 11/24/09
    if ($periodId) {
        $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
    }

	$sql .= " left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id  ";

    if (isIniDepartment($id))
    {
        $sql .= " left outer join (SELECT application_id, 
            GROUP_CONCAT(DISTINCT IF(application_decision_ini.admission_status = 1, 'waitlist', 
                IF(application_decision_ini.admission_status = 4, 'strong waitlist' , NULL))) AS status 
            FROM application_decision_ini 
            GROUP BY application_id) AS status
            ON status.application_id = application.id ";        
    }
    elseif (isDesignDepartment($id))
    {
        $sql .= " left outer join (SELECT application_id, 
            GROUP_CONCAT(DISTINCT IF(application_decision_design.admission_status = 1, 'waitlist', 
                IF(application_decision_design.admission_status = 4, 'strong waitlist' , NULL))) AS status 
            FROM application_decision_design 
            GROUP BY application_id) AS status
            ON status.application_id = application.id ";        
    }
        
	$spst->sql = $sql;
	$spst->sqlWhere = " where 
	application.submitted=1	
	and (lu_application_programs.admission_status IS NULL or lu_application_programs.admission_status != 2)
	and lu_programs_departments.department_id=".$id;
	 
    // PLB added period where 11/24/09
    if ($periodId) {
        $spst->sqlWhere .= " AND period_application.period_id = " .  $periodId;
    }
    
    // PLB added check for admission to program in same department
    if (isIniDepartment($id))
    {
        $spst->sqlWhere .= " AND application.id NOT IN (
                            SELECT DISTINCT application_id
                            FROM application_decision_ini
                            INNER JOIN lu_programs_departments
                              ON application_decision_ini.admission_program_id = lu_programs_departments.program_id
                            WHERE application_decision_ini.admission_status = 2
                            AND lu_programs_departments.department_id = " . $id . "
                            )";   
    }
    elseif (isDesignDepartment($id))
    {
        $spst->sqlWhere .= " AND application.id NOT IN (
                            SELECT DISTINCT application_id
                            FROM application_decision_design
                            INNER JOIN lu_programs_departments
                              ON application_decision_design.admission_program_id = lu_programs_departments.program_id
                            WHERE application_decision_design.admission_status = 2
                            AND lu_programs_departments.department_id = " . $id . "
                            )";   
    }
    else
    {
        $spst->sqlWhere .= " AND application.id NOT IN (
                            SELECT DISTINCT application_id
                            FROM application_decision
                            INNER JOIN lu_programs_departments
                              ON application_decision.program_id = lu_programs_departments.program_id
                            WHERE application_decision.admission_status > 1
                            AND lu_programs_departments.department_id = " . $id . "
                            )"; 
    }
    if (!($id == 50 || $id == 97 /* || $id == 74 */ )) {
        $spst->sqlWhere .= " AND (application.paid = 1 OR application.waive = 1)";
    }
    
                        
	//$spst->sqlDirective = "distinct";
	$spst->sqlGroupBy = "lu_users_usertypes.id";
	
	$spst->sqlOrderBy = "users.lastname,users.firstname";
	$spst->showFilter = false;
    if ($_SESSION['A_usertypeid'] == 11)  {
        // Student contacts should not be getting a link to this page, but if
        // they do end up here, they should get a link to the minimal application summary. 
        $spst->editPage = "userroleEdit_student_formatted.php";
    } else {
        $spst->editPage = "../review/userroleEdit_student_print.php";
    }

    $spst->doSpreadSheet();

?>

	</div>
	</td>
  </tr>
</table>
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function saveEnableRecycling()
{
    global $id;
    global $periodId;
    $enableRecycling = filter_input(INPUT_POST, 'enableRecycling', FILTER_VALIDATE_BOOLEAN); 
    $usersId = $_SESSION['A_usermasterid'];
    
    $query = "INSERT INTO department_enable_recycling
        (department_id, period_id, enable_recycling, users_id) 
        VALUES (
        " . intval($id) . ",
        " . intval($periodId) . ",
        " . intval($enableRecycling) . ",
        " . intval($usersId) . "
        ) ON DUPLICATE KEY UPDATE
        enable_recycling = " . intval($enableRecycling) . ",
        users_id = " . intval($usersId);
    mysql_query($query);
}

function getEnableRecycling()
{
    global $id;
    global $periodId;
    
    $query = "SELECT department_id 
        FROM department_enable_recycling
        WHERE department_id = " . intval($id) . "
        AND period_id = "  . intval($periodId) . "
        AND enable_recycling = 1";
    $result = mysql_query($query);
    
    if (mysql_numrows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}
?>