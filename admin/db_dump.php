<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<?
global $app_list;

global $states;
$states = array();

global $visatypes;
$visatypes = array();

global $ethnicity;
$ethnicity = array();

$app_list = array();

function create_dump_file( $filename )
{
//  echo "create_dump_file\n<BR>";

  $handle = fopen($filename, 'w+');
  return $handle;
}

function close_dump_file( $fid )
{
//  echo "close_dump_file\n<BR>";
  fclose($fid);
}

function get_applicant_list( $dept_code )
{
  $list = array();

  return $list;
}

function print_header_labels($fid)
{
$headerline =  'SPACER^appid^'
        . ' email^title^firstname^middlename^lastname^'
        . ' gender^dob^'
        . ' address_cur_street1^address_cur_street2^address_cur_street3^address_cur_street4^'
        . ' address_cur_city^CurrentState^address_cur_pcode^CurrentCountry^'
        . ' address_cur_tel^address_cur_tel_mobile^address_cur_tel_work^'
        . ' address_perm_street1^address_perm_street2^address_perm_street3^address_perm_street4^'
        . ' address_perm_city^PermState^address_perm_pcode^PermCountry^'
        . ' address_perm_tel^address_perm_tel_mobile^address_perm_tel_work^homepage^'
        . ' CountryofCitz^iso_code^visa status^ethnicity^'

        . 'SPACER^'
        . ' program_name^aoi_1^aoi_2^aoi_3^'
        . ' program_name^aoi_1^aoi_2^aoi_3^'
        . ' program_name^aoi_1^aoi_2^aoi_3^'
        . ' program_name^aoi_1^aoi_2^aoi_3^'

        . 'SPACER^'
        . 'gre_testdate^verbalscore^verbalpercentile^quantitativescore^quantitativepercentile^analyticalscore^'
        . 'analyticalpercentile^analyticalwritingscore^analyticalwritingpercentile^ '

        . 'SPACER^'
        . 'gresubject_testdate^name^score^percentile^ '

        . 'SPACER^'
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^ '
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^ '  
        . 'TOEFL_type^testdate^section1^section2^section3^essay^total^ '

        . 'SPACER^'
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        
        . 'Rec_Title^Rec_Name^Rec_Email^Rec_Affliation^Rec_Phone^buckleywaive^'        

        . 'SPACER^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'
        . 'UnivName^date_entered^date_grad^date_left^Degree^major1^major2^minor1^'
        . 'minor2^gpa^gpa_major^GPAScale^edu type^'

        . 'SPACER^';

  fwrite($fid, $headerline);
  fwrite($fid, "\n");

}

function get_names($deptid, $fid)
{
//echo "func get_name\n<br>";

$states = array();
$visatypes = array();
$ethnicity = array();

$sqlA = 'select id, name from states order by id';
	$resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
	while($rowA = mysql_fetch_array( $resultA ))
	{ 
		array_push($states, $rowA[1]);
	}


$sqlA = 'select id, name from visatypes order by id';
	array_push($visatypes, " ");

	$resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
	while($rowA = mysql_fetch_array( $resultA ))
	{ 
		array_push($visatypes, $rowA[1]);
	}

$sqlA = 'select id, name from ethnicity  order by id';
	array_push($ethnicity, " ");

	$resultA = mysql_query($sqlA) or die(mysql_error(). $sqlA);
	while($rowA = mysql_fetch_array( $resultA ))
	{ 
		array_push($ethnicity, $rowA[1]);
	}


$sql3 = 'select distinct '
       . ' count(B.id) as cnt, B.id '
       . ' from '
       . ' application as B join '
       . ' lu_application_programs as C, '
       . ' lu_programs_departments as D '
       . ' where '
       . ' B.submitted = 1 and'
       . ' B.id = C.application_id and'
       . ' C.program_id = D.program_id and'
       . ' D.department_id = '.$deptid.' '
       . ' group by B.id';

	$result3 = mysql_query($sql3) or die(mysql_error(). $sql3);
	while($row3 = mysql_fetch_array( $result3 ))
	{ 
// echo $row3[0]." ".$row3[1]."<br> ";
           $appid = $row3[1];

$sql = 'select '
        . ' B.id, G.id, G.user_id, F.email, F.title, F.firstname, F.middlename, F.lastname, '
        . ' G.gender, G.dob, '
        . ' G.address_cur_street1, G.address_cur_street2, G.address_cur_street3, G.address_cur_street4, '
        . ' G.address_cur_city, P.name as CurrentState, G.address_cur_pcode, N.name as CurrentCountry, '
        . ' G.address_cur_tel, G.address_cur_tel_mobile, G.address_cur_tel_work,'
        . ' G.address_perm_street1, G.address_perm_street2, G.address_perm_street3, G.address_perm_street4, '
        . ' G.address_perm_city, Q.name as PermState, G.address_perm_pcode, O.name as PermCountry, '
        . ' G.address_perm_tel, G.address_perm_tel_mobile, G.address_perm_tel_work, G.homepage,'
        . ' J.name as CountryofCitz, J.iso_code, G.visastatus, G.ethnicity '
        . ' from '
        . ' application as B Join '
        . ' lu_users_usertypes as E,'
        . ' users as F,'
        . ' users_info as G,'
        . ' countries as J,'
        . ' countries as N,'
        . ' countries as O,'
        . ' states as P,'
        . ' states as Q'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id = '.$appid.' and'
        . ' B.user_id=E.id and'
        . ' E.user_id =F.id and'
        . ' E.usertype_id=5 and'
        . ' G.user_id = E.id and'
        . ' G.cit_country = J.id and'
        . ' G.address_cur_country = N.id and'
        . ' G.address_perm_country = O.id and'
        . ' G.address_cur_state = P.id and'
        . ' G.address_perm_state = Q.id '
        . ' Order by B.id';

	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
// echo $row[0]."<BR>";

fwrite($fid, "**BEGIN**^");
fwrite($fid, $appid );
fwrite($fid, "^" );
            $luuid=0;
            $uid=0;

            for ($i=3; $i<37; $i++){
              switch( $i) {
                case 35: //VISA STATUS
                  fwrite($fid, $visatypes[$row[$i]]);
                  break;
                case 36: //Ethinicty
                  fwrite($fid, $ethnicity[$row[$i]]);
                  break;
                default:
                  fwrite($fid, $row[$i]);
              }
              fwrite($fid, " ^");
            }


fwrite($fid, "**END...BEGIN**^");
            get_programs($appid, $luuid, $uid, $fid);           
fwrite($fid, "**END...BEGIN**^");
            get_gre($appid, $luuid, $uid, $fid);           
fwrite($fid, "**END...BEGIN**^");
            get_gresubject($appid, $luuid, $uid, $fid);           
fwrite($fid, "**END...BEGIN**^");
            get_TOEFLS($appid, $luuid, $uid, $fid);           
fwrite($fid, "**END...BEGIN**^");
            get_rec($appid, $luuid, $uid, $fid);           
fwrite($fid, "**END...BEGIN**^");
            get_edu($appid, $luuid, $uid, $fid);           
fwrite($fid, "**END...BEGIN**^");

            fwrite($fid, "\n");
          }
          
	}



}

function get_programs($appid, $luuid, $uid, $fid)
{
// echo "func get_programs\n<br>";

$sql = 'select CONCAT(L.name, " ", K.linkword, " ", M.name) as program_name, C.id'
        . ' from '
        . ' application as B join'
        . ' lu_application_programs as C,'
        . ' lu_programs_departments as D,'
        . ' programs as K,'
        . ' degree as L,'
        . ' fieldsofstudy as M'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id='.$appid.' and'
        . ' B.id=C.application_id and'
        . ' C.program_id=D.program_id and'
        . ' K.id = D.program_id and'
        . ' L.id = K.degree_id and '
        . ' M.id = K.fieldofstudy_id'
        . ' Order by C.choice';

        $num_of_inst = 4;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           
           if ($num_of_inst > 0) {
             for ($i=0; $i<1; $i++){
               fwrite($fid, $row[$i]);
               fwrite($fid, "^");
               $progid=$row[1];
// echo "PROGID:".$progid."<BR>";
               get_program_aoi($progid, $fid);
             }
           }
           $num_of_inst--;
	}

        for ($i=$num_of_inst; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^^^^" );
        }



}

function get_program_aoi($progid, $fid)
{
// echo "func get_program_aoi\n<br>";

$sql = 'select I . name '
        . ' from '
        . ' lu_application_interest as D , '
        . ' interest as I '
        . ' where '
        . ' D.app_program_id='.$progid.' and'
        . ' D.interest_id = I.id Order By D.choice';
    
         
        $num_of_inst=3;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
             for ($i=0; $i<1; $i++){
               fwrite($fid, $row[$i]);
               fwrite($fid, "^");
             }
           $num_of_inst--;
	}

        for ($i=$num_of_inst; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^" );
        }

}

function get_experience($uid, $aid,  $fid)
{
// echo "func get_experience\n<br>";
}

function get_gre($appid, $luuid, $uid, $fid)
{
// echo "func get_gre\n<br>";
$sql = 'select G.testdate , G.verbalscore , G.verbalpercentile , G.quantitativescore , G.quantitativepercentile , G.analyticalscore , '
        . ' G.analyticalpercentile , G.analyticalwritingscore , G.analyticalwritingpercentile '
        . ' from '
        . ' application as B join '
        . ' grescore as G '
        . ' where '
        . ' B . submitted = 1 and '
        . ' B.id=' . $appid.' and'
        . ' B.id = G.application_id '
        . ' Order by B.id';

	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           for ($i=0; $i<9; $i++){
             fwrite($fid, $row[$i]);
             fwrite($fid, "^");
           }
	}

}

function get_gresubject($appid, $luuid, $uid, $fid)
{
// echo "func get_gresubject\n<br>";

$sql = 'select G.testdate , G.name , G.score , G.percentile '
        . ' from '
        . ' application as B join '
        . ' gresubjectscore as G '
        . ' where '
        . ' B.submitted = 1 and '
        . ' B.id=' . $appid.' and'
        . ' B.id = G.application_id '
        . ' Order by B.id';

        $subject_score_missing = 1;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           for ($i=0; $i<4; $i++){
             fwrite($fid, $row[$i]);
             fwrite($fid, "^");
           }
           $subject_score_missing = 0;
	}
        if ($subject_score_missing == 1) {
          fwrite($fid, " ^ ^ ^ ^");
        }
}

function get_TOEFLS($appid, $luuid, $uid, $fid)
{
// echo "func get_TOEFLS\n<br>";
       $sql = 'select T.type , T.testdate , T.section1 , T.section2 , T.section3 , T.essay , T.total , B.id '
        . ' from '
        . ' application as B join '
        . ' toefl as T '
        . ' where '
        . ' B.submitted = 1 and '
        . ' B.id ='.$appid.' and'
        . ' B.id = T.application_id '
        . ' Order by B.id , T.type';

	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           for ($i=0; $i<7; $i++){
             fwrite($fid, $row[$i]);
             fwrite($fid, "^");
           }
	}


}

function get_edu($appid, $luuid, $uid, $fid)
{
// echo "func get_edu\n<br>";

$edu_type = array("undefined","ugrad", "grad", "additional" );

$sql = 'select'
        . ' C.name as UnivName, I.date_entered, I.date_grad, I.date_left, D.name as Degree, I.major1, I.major2, I.minor1, I.minor2, I.gpa, I.gpa_major, G.name as GPAScale, I.educationtype, I.user_id'
        . ' from '
        . ' application as B join'
        . ' usersinst as I,'
        . ' institutes as C,'
        . ' degreesall as D,'
        . ' gpascales as G'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id='.$appid.' and'
        . ' B.user_id=I.user_id and'
        . ' I.institute_id = C.id and'
        . ' I.degree = D.id and'
        . ' I.gpa_scale = G.id'
        . ' Order by I.educationtype';

 
        $num_of_inst = 5;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           if ($num_of_inst > 0) {
             for ($i=0; $i<13; $i++){
               if ($i == 12) {
                 fwrite($fid, $edu_type[$row[$i]]);
               } else {
                 fwrite($fid, $row[$i]);
               }
               fwrite($fid, "^");
             }
           }
           $num_of_inst--;
	}

        for ($i=$num_of_inst; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^" );
        }

}

function get_rec($appid, $luuid, $uid, $fid)
{
// echo "func get_rec\n<br>";

$yes_no = array("no","yes");

$sql = 'select'
        . ' U.title, CONCAT(U.firstname, " ", U.lastname) as RecName, U.email, UI.company, UI.address_cur_tel, B.buckleywaive, B.id'
        . ' from '
        . ' application as B join'
        . ' recommend as R,'
        . ' lu_users_usertypes as LUU,'
        . ' users as U,'
        . ' users_info as UI'
        . ' where '
        . ' B.submitted=1 and'
        . ' B.id='.$appid.' and'
        . ' B.id=R.application_id and'
        . ' R.rec_user_id = LUU.id and'
        . ' LUU.user_id = U.id and'
        . ' LUU.id = UI.user_id'
        . ' Order by B.id';
        
        $num_of_recs = 5;
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{ 
           if ($num_of_recs > 0) {
             for ($i=0; $i<6; $i++){

               if ( $i == 5 ) {
                 fwrite($fid, $yes_no[$row[$i]]);
               } else {
                 fwrite($fid, $row[$i]);
               }
               fwrite($fid, "^");
             }
           }
           $num_of_recs--;
	}

        // Fill for the missing recs
// echo "FILL: ".$num_of_recs;
        for ($i=$num_of_recs; $i>0; $i--) {
// echo " F".$i." ";
          fwrite($fid, " ^ ^ ^ ^ ^ ^" );
        }
// echo "FILL: DONE<BR>";
}
?>

</head>
<body>

<?
// Main

// DUMP RI DATA
echo "DUMPING RI DATA....";
$deptid=3;
$filename = "DATADUMP_RI.TXT";
$fid = create_dump_file($filename);
print_header_labels($fid);
get_names( $deptid, $fid);
close_dump_file($fid);
echo "DONE<BR><BR>";

// DUMP MSE CAMPUS DATA
echo "DUMPING MSECAMPUS DATA....";
$deptid=18;
$filename = "DATADUMP_MSECAMPUS.TXT";
$fid = create_dump_file($filename);
print_header_labels($fid);
get_names( $deptid, $fid);
close_dump_file($fid);
echo "DONE<BR><BR>";

// DUMP HCII DATA
echo "DUMPING HCII DATA....";
$deptid=5;
$filename = "DATADUMP_HCII.TXT";
$fid = create_dump_file($filename);
print_header_labels($fid);
get_names( $deptid, $fid);
close_dump_file($fid);
echo "DONE<BR><BR>";


// DUMP LTI DATA
echo "DUMPING LTI DATA....";
$deptid=6;
$filename = "DATADUMP_LTI.TXT";
$fid = create_dump_file($filename);
print_header_labels($fid);
get_names( $deptid, $fid);
close_dump_file($fid);
echo "DONE<BR><BR>";

?>
</body>
</html>