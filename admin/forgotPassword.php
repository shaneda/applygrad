<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$email = "";
$guid = "";
$mailSent = false;
$recommender = false;

if(isset($_GET['email']))
{
	$email = htmlspecialchars($_GET['email']);
}else
{
	if(isset($_POST['txtEmail']))
	{
		$email = htmlspecialchars($_POST['txtEmail']);
	}
}
if(isset($_GET["r"]))
{
	$recommender = true;
	
}
//GET USER INFO
//echo $email;
if(isset($_POST['txtEmail']))
{

	if($email != "" && $email != "*" && check_email_address($email) !== false && strstr($email,"applygrad")===false)
	{
		$email = addslashes($email);
		//echo $email . "<br>";
		$sql = "SELECT email, guid from users where email = '".$email."'";
		$result = mysql_query($sql)or die(mysql_error());
		
		
		//echo $sql;
		if(mysql_num_rows($result) == 0)
		{
			$err .= "Email address not found.<br>";
		}else
		{
            switch ($hostname)
{
    case "APPLY.STAT.CMU.EDU":  
        sendHtmlMail("scsstats@cs.cmu.edu", "dales+password@cs.cmu.edu", "Password reset request", $email, "passreset");
        break;
    case "APPLYGRAD-INI.CS.CMU.EDU":  
        sendHtmlMail("scsini@cs.cmu.edu", "dales+password@cs.cmu.edu", "Password reset request", $email, "passreset");
        break;
    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
        sendHtmlMail("scsdiet@cs.cmu.edu", "dales+password@cs.cmu.edu", "Password reset request", $email, "passreset");
        break;
    default:
        sendHtmlMail("applygrad@cs.cmu.edu", "dales+password@cs.cmu.edu", "Password reset request", $email, "passreset");
 }

		}
		$email = "";
		while($row = mysql_fetch_array( $result )) 
		{
			$email = $row['email'];
			$guid = $row['guid'];
	
			//SEND AN EMAIL TO EACH QUALIFYING EMAIL
			
			if($email != "" && $guid != "")
			{
				$sysemail = "";
				$link = "https://".$_SERVER['SERVER_NAME']."/admin/newPassword.php?email=".str_replace("+","%2b",$email)."&id=".$guid;
				if($recommender == true)
				{
					$link .= "&r=1";
				}
				$sql = "select sysemail from systemenv where domain_id=1";
				$result1 = mysql_query($sql)	or die(mysql_error());
				while($row1 = mysql_fetch_array( $result1 ))
				{ 
					$sysemail = $row1['sysemail'];
				}
				
				//GET TEMPLATE CONTENT
				$sql = "select content from content where name='Forgot Password Letter' and domain_id=1";
				$result1 = mysql_query($sql)	or die(mysql_error());
				while($row1 = mysql_fetch_array( $result1 )) 
				{
					$str = $row1["content"];
				}
				
				$vars = array(
				array('email', $email), 
				array('guid',$guid),
				array('link',$link)
				);
				$str = parseEmailTemplate2($str, $vars );
				//SEND EMAIL
				sendHtmlMail($sysemail, $email, "Password Reset Request", $str, "password");//to user
				//sendHtmlMail($sysemail, $sysemail, "Carnegie Mellon Admissions password reset", $str);//to applyweb
				$mailSent = true;
				
			}else
			{
				if(isset($_POST['btnSubmit']))
				{
					$err .= "Email account not found.<br>";
				}//end if submit
			}
		
		}//end while	
			
	}else
	{
		$err .= "invalid email address. ". $email. "<br>";
		$email = "";
	}
}//end btnsubmit


?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" --><span class="title">Forgot Password </span><!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
			<span class="errorSubTitle"><?=$err?></span>
			<span class="tblItem">
			<? if($email == "") { ?>
			Please enter your email address:<br />
            <input name="txtEmail" type="text" class="tblItem" id="txtEmail" maxlength="255" />
            <input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="Submit" />
            <br />
			<? } 
			if($mailSent == true && $err == ""){ ?>
				An email has been sent to you. In order to change your password, you must click on the link in the email. You will then be prompted to enter a new password. <br />
				<br />
			NOTE: You may recieve more than 1 email if you have more than 1 account registered with this email address. <br />
				<br />
				<a href="index.php<? if($recommender == true){echo "?r=1";} ?>">Return to login page</a>			
			<? } ?>
			</span><!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
