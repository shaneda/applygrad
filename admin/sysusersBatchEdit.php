<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
	header("Location: index.php");
}

$err = "";
$roles = array();
$usertypes = array();
$departments = array();
$department = "";
$firstname = "";
$middlename = "";
$lastname = "";
$email = "";
$pass = "";
$uid = -1;
$umasterid = -1;
$updated = false;
$info = "";


// USER TYPES
$sql = "SELECT * FROM usertypes order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	if($_SESSION['A_usertypeid'] == 1 && $row['id'] == "0")
	{

	}else
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, trim($row['name']));
		array_push($usertypes, $arr);
	}
}

// USER TYPES
$sql = "SELECT * FROM department order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, trim($row['name']));
	array_push($departments, $arr);
}



if( isset( $_POST ) && !isset($_POST['btnSubmit'])  )
{
	$vars = $_POST;
	$itemId = -1;
	$doDelete = false;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDel') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			$doDelete = true;
		}
	}//END FOR



}//END IF


if(isset($_POST['btnSubmit']))
{
	$department = intval($_POST['lbDepartment']);
	$info = htmlspecialchars($_POST['txtInfo']);
	$role = intval($_POST['lbUsertype']);
	$names = split("\n", $_POST['txtName']);
	$firstname = "";
	$middlename = "";
	$lastname = "";
	$email = "";
	for($i = 0; $i < count($names); $i++)
	{
		$arr = split(",", $names[$i]);
		if(count($arr) != 4)
		{
			$err .= "Data is not in correct format.<br>";
		}
		else
		{
			$firstname = trim($arr[0]);
			$middlename = trim($arr[1]);
			$lastname = trim($arr[2]);
			$email = trim($arr[3]);
			if($firstname == "")
			{
				$err .= "First name is required<br>";
			}
			if($lastname == "")
			{
				$err .= "Last name is required<br>";
			}
		}

		if($err == "")
		{

			$doInsert = true;
			$pass = sha1(trim(strtolower(addslashes($firstname))). "123");
			if($doInsert  == true)
			{
				//SAVE ROLES
				if($role == "5" || $role == "6")
				{
					$err .= "Students and recommenders cannot be assigned to departments in item ".$role."<br>";
				}else
				{
					$sql = "insert into users(
					firstname,middlename, lastname, email, password)
					values(
					'".addslashes($firstname)."',
					'".addslashes($middlename)."',
					'".addslashes($lastname)."',
					'".addslashes($email)."',
					'".$pass."')";
					mysql_query( $sql) or die(mysql_error().$sql);
					$umasterid = mysql_insert_id();
					$updated = true;

					$sql = "insert into lu_users_usertypes(user_id, usertype_id)values(".$umasterid.", ".$role.") ";
					mysql_query($sql) or die(mysql_error().$sql);
					$uid = mysql_insert_id();

					$sql = "insert into users_info(user_id, additionalinfo )values(".$uid.", '".$info."') ";
					mysql_query($sql) or die(mysql_error().$sql);

					$sql = "insert into lu_user_department(user_id, department_id)values( ".$uid . ", ".$department.")";
					mysql_query($sql) or die(mysql_error().$sql);
				}
			}
		}//end if err


	}//end for
}





?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Edit System Users <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><span class="errorSubtitle"><?=$err?></span>
	<a href="sysusersEdit.php">Add Another User</a>	<input name="txtMasterid" type="hidden" id="txtMasterid" value="<?=$umasterid?>" />
	<input name="txtUid" type="hidden" id="txtUid" value="<?=$uid?>" />
	<table width="700"  border="0" cellpadding="4" cellspacing="2">
      <tr>
        <td width="200" align="right"><strong>Names: </strong><em>(one per line)</em></td>
        <td><span class="tblItem">
          Firstname, middlename, lastname, email<br />
          <? showEditText("", "textarea", "txtName", $_SESSION['A_allow_admin_edit'],false,null,true,30); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>Role:</strong></td>
        <td><span class="tblItem">
          <?  showEditText("", "listbox", "lbUsertype", $_SESSION['A_allow_admin_edit'],true, $usertypes); ?>
        for department
        <? showEditText(-1, "listbox", "lbDepartment", $_SESSION['A_allow_admin_edit'],false, $departments); ?>
</span></td>
      </tr>
      <tr>
        <td align="right"><strong>Additional Info:</strong> </td>
        <td><span class="tblItem">
          <? showEditText($info, "textbox", "txtInfo", $_SESSION['A_allow_admin_edit'],false,null,true,50); ?>
        </span></td>
      </tr>

      <tr>
        <td width="200" align="right">&nbsp;</td>
        <td><span class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </span></td>
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
