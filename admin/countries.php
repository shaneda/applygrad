<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$sql = "";
$err = "";
$countries = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				mysql_query("DELETE FROM countries WHERE id=".$itemId)
				or die(mysql_error());
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select id,name
from countries
order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($countries, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Countries';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Countries</span><br />
<br />
    <a href="countryEdit.php">Add country</a><br />
	<br />
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td width="60" align="right">Delete</td>
      </tr>
     <? for($i = 0; $i < count($countries); $i++){?>
	<tr>
        <td><a href="countryEdit.php?id=<?=$countries[$i][0]?>"><?=$countries[$i][1]?></a></td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$countries[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	<? } ?>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>