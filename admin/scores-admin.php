
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
include_once '../apply/section_required.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$sectioncomplete = false;
$grecomplete = false;
$grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflcscores = array();
$toeflpscores = array();

$greScoreRange1 = array();
$greScoreRange2 = array();
$grePercRange1 = array();

$greSubs = array(
array("Computer Science","Computer Science"),
array("Mathematics","Mathematics"),
array("Physics","Physics"),
array("Chemistry","Chemistry"),
array("Biochemistry Cell and Molecular Biology","Biochemistry Cell and Molecular Biology"),
array("Biology","Biology"),
array("Psychology","Psychology"),
array("Other","Other, Please Specify")
);
$greSubRange = array();
$greSubPercRange = array();

$toeflRange1 = array(); // 0-30
$toeflRange2 = array(); // 0-300
$toeflRange3 = array(); // 0, 1-6 in .5 incre
$toeflRange4= array();  // 31-68
$toeflRange5 = array(); // 310-677
$toeflRange6 = array(); // 0-120
$toeflRange7 = array(); // 31-67

for($i = 200; $i < 801; $i += 10)
{
    $arr = array($i, $i);
    array_push($greScoreRange1, $arr);
}
for($i = 0; $i < 6.5; $i += 0.5)
{
    $arr = array($i, $i);
    array_push($greScoreRange2, $arr);
}
for($i = 1; $i < 100; $i += 1)
{
    $arr = array($i, $i);
    array_push($grePercRange1, $arr);
}
for($i = 200; $i < 1001; $i += 10)
{
    $arr = array($i, $i);
    array_push($greSubRange, $arr);
}
for($i = 1; $i < 100; $i += 1)
{
    $arr = array($i, $i);
    array_push($greSubPercRange, $arr);
}
// Range 0-30 
for($i = 0; $i < 31; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange1, $arr);
}
// range 0 to 300
for($i = 0; $i < 301; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange2, $arr);
}
/// range 0 , 1 to 6.5 in .5 incremements
$arr = array(0, 0);
array_push($toeflRange3, $arr);
for($i = 1; $i < 6.5; $i += 0.5)
{
    $arr = array($i, $i);
    array_push($toeflRange3, $arr);
}

// Range 31 to 68
for($i = 31; $i < 69; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange4, $arr);
}
// Range 310 to 677
for($i = 310; $i < 678; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange5, $arr);
}
// Range 0-120
for($i = 0; $i < 121; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange6, $arr);
}
// Range 31 to 67
for($i = 31; $i < 68; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange7, $arr);
}

$err = "";
//FIRST CHECK TO SEE IF RECORDS EXIST. IF NOT, ADD THEM
addGRE(false);
addGRESub(false);
addToefl(false, "IBT");    
addToefl(false, "CBT");    
addToefl(false, "PBT");    

/*    DAS commented
if( isset( $_POST ) )
{
    $vars = $_POST;
    $itemId = -1;
    foreach($vars as $key => $value)
    {
        if( strstr($key, 'btnDel') !== false ) 
        {
            $arr = split("_", $key);
            $item = $arr[1];
            $itemId = $arr[2];
            $sql = "";
            switch( $item)
            {
                case "1":
                    $sql = $sql = "update grescore set
                    testdate=NULL,    verbalscore=NULL,verbalpercentile=NULL,quantitativescore=NULL,
                    quantitativepercentile=NULL,analyticalwritingscore=NULL,analyticalwritingpercentile=NULL,
                    analyticalscore=NULL,analyticalpercentile=NULL,datafile_id=NULL where id=".$itemId;
                break;
                case "2":
                    $sql = "update gresubjectscore set testdate=NULL,name=NULL,score=NULL,percentile=NULL,datafile_id=NULL WHERE id=".$itemId;
                break;
                default:
                    $sql = "update toefl set testdate=NULL,section1=NULL,section2=NULL,section3=NULL,essay=NULL,
                    total=NULL,datafile_id=NULL WHERE id=".$itemId;
                break;
            }
            //echo $sql;
            mysql_query($sql) or die(mysql_error());
        }
        
    }
}

*/
function addGRE($allowDup)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from grescore where application_id = ".$_SESSION['appid'];
        $result = mysql_query($sql)    or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    if($doAdd == true)
    {
        $sql = "insert into grescore(application_id)values(".$_SESSION['appid'].")";
        mysql_query($sql)    or die(mysql_error().$sql);
    }
}
function addGRESub($allowDup)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from gresubjectscore where application_id = ".$_SESSION['appid'];
        $result = mysql_query($sql)    or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    if($doAdd == true)
    {
        $sql = "insert into gresubjectscore(application_id)values(".$_SESSION['appid'].")";
        mysql_query($sql)    or die(mysql_error().$sql);
    }
}

function addToefl($allowDup, $type)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from toefl where application_id = ".$_SESSION['appid']. " and type='".$type."'";
        $result = mysql_query($sql)    or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }

    }
    if($doAdd == true)
    {
        $sql = "insert into toefl(application_id,type)values(".$_SESSION['appid'].",'".$type."')";
        mysql_query($sql)    or die(mysql_error().$sql);
    }
}



//INSERT RECORD INTO CORRESPONDING TEST TABLE FOR USER 
if(isset($_POST['btnAdd']))
{
    $sql = "";
    switch($_POST['lbType'])
    {
        case "1":
            addGRE(false);
        break;
        case "2":
            addGRESub(false);
        break;
        case "3":
            addToefl(false, "IBT");                    
        break;
        case "4":
            addToefl(false, "CBT");    
        break;
        case "5":
            addToefl(false, "PBT");    
        break;
    }//END SWITCH
}//END POST BTNADD

if(isset($_POST['btnSave']))
{
    $greId = array();
    $greSubId = array();
    $toefliId = array();
    $toeflcId = array();
    $toeflpId = array();
    $tmpType="";
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if($arr[0] == "txtGreGenId" || $arr[0] == "txtGreSubId" || $arr[0] == "txtToeflIntId" || $arr[0] == "txtToeflCompId" || $arr[0] == "txtToeflPaperId" )
        {
            $tmpid = $arr[1];
            if($itemId != $tmpid || $tmpType != $arr[0])
            {
                $itemId = $tmpid;
                $tmpType=$arr[0];
                switch($arr[0])
                {
                    case "txtGreGenId":
                        array_push($greId,$itemId);
                    break;
                    case "txtGreSubId":
                        array_push($greSubId,$itemId);
                    break;
                    case "txtToeflIntId":
                        array_push($toefliId,$itemId);
                    break;
                    case "txtToeflCompId":
                        array_push($toeflcId,$itemId);
                    break;
                    case "txtToeflPaperId":
                        array_push($toeflpId,$itemId);
                    break;
                }//END SWITCH
            }//END IF
        }//END IF
    }//END FOR
    //ITERATE THROUGH IDS AND DO UPDATES
    for($i = 0; $i < count($greId); $i++)
    {
        //SET VARS
        $greGenDate = htmlspecialchars( $_POST["txtGreGenDate_".$greId[$i]] );
        $greGenVerScore = "NULL";
        $greGenVerPer = "NULL";
        $greGenQuanScore = "NULL";
        $greGenQuanPer = "NULL";
        $greGenWrScore = "NULL";
        $greGenWrPer = "NULL";
        $greGenAnScore = "NULL";
        $greGenAnPer = "NULL";
        
        if($_POST["txtGreGenVerScore_".$greId[$i]]  != "")
        {
            $greGenVerScore = strToNum($_POST["txtGreGenVerScore_".$greId[$i]]);
        }
        if($_POST["txtGreGenVerPer_".$greId[$i]]  != "")
        {
            $greGenVerPer = strToNum($_POST["txtGreGenVerPer_".$greId[$i]]);
        }
        if($_POST["txtGreGenQuanScore_".$greId[$i]]  != "")
        {
            $greGenQuanScore = strToNum($_POST["txtGreGenQuanScore_".$greId[$i]]);
        }
        if($_POST["txtGreGenQuanPer_".$greId[$i]]  != "")
        {
            $greGenQuanPer = strToNum($_POST["txtGreGenQuanPer_".$greId[$i]]);
        }
        if($_POST["txtGreGenWrScore_".$greId[$i]]  != "")
        {
            $greGenWrScore = floatval($_POST["txtGreGenWrScore_".$greId[$i]]);
        }
        if( $_POST["txtGreGenWrPer_".$greId[$i]] != "")
        {
            $greGenWrPer = strToNum($_POST["txtGreGenWrPer_".$greId[$i]]);
        }
        if($_POST["txtGreGenAnScore_".$greId[$i]]  != "")
        {
            $greGenAnScore = strToNum($_POST["txtGreGenAnScore_".$greId[$i]]);
        }
        if($_POST["txtGreGenAnPer_".$greId[$i]]  != "")
        {
            $greGenAnPer = strToNum($_POST["txtGreGenAnPer_".$greId[$i]]);
        }
        
        
        
        $localErr = "";
        //DO VALIDATION
        if(check_Date2($greGenDate) == false)
        {
            $localErr .= "GRE General Score date is invalid<br>";
        }else
        {
            $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-5);
            $arr = split("/", $greGenDate);
            $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
            if($thisdate < $pastdate )
            {
                //$localErr .= "GRE General Score date is too old. GRE scores are only valid for 5 years<br>";
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update grescore set
            testdate='".formatMySQLdate2($greGenDate,'/','-')."',
            verbalscore=".$greGenVerScore.",
            verbalpercentile=".$greGenVerPer.",
            quantitativescore=".$greGenQuanScore.",
            quantitativepercentile=".$greGenQuanPer.",
            analyticalwritingscore=".$greGenWrScore.",
            analyticalwritingpercentile=".$greGenWrPer.",
            analyticalscore=".$greGenAnScore.",
            analyticalpercentile=".$greGenAnPer."
            where id = ". $greId[$i]. " and application_id= " .$_SESSION['appid'];
            $result = mysql_query($sql)    or die(mysql_error().$sql);
            $sectioncomplete = true;
            $grecomplete = true;
        }
        else
        {
            $err .= $localErr;
        }
    }//END GRE UPDATE
    
    for($i = 0; $i < count($greSubId); $i++)
    {
        //SET VARS
        $greSubDate = $_POST["txtGreSubDate_".$greSubId[$i]];
        $greSubName = "NULL";
        if($_POST["lbGreSubName_".$greSubId[$i]] == "Other" || $_POST["lbGreSubName_".$greSubId[$i]] == "")
        {
            $greSubName = htmlspecialchars($_POST["txtGreSubName_".$greSubId[$i]]);
        }else
        {
            $greSubName = htmlspecialchars($_POST["lbGreSubName_".$greSubId[$i]]);
        }
        $greSubScore = "NULL";
        $greSubPer = "NULL";
        
        if($_POST["txtGreSubScore_".$greSubId[$i]] != "")
        {
            $greSubScore = strToNum($_POST["txtGreSubScore_".$greSubId[$i]]);
        }
        if($_POST["txtGreSubPer_".$greSubId[$i]] != "")
        {
            $greSubPer = strToNum($_POST["txtGreSubPer_".$greSubId[$i]]);
        }
        
        
        
        $localErr = "";
        //DO VALIDATION
        $doUpdate = false;
        if($greSubDate != "" && $greSubDate != "00/0000")
        {
            if(check_Date2($greSubDate) == false)
            {
                $localErr .= "GRE Subject Score date is invalid<br>";
            }else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $greSubDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "GRE Subject Score date is too old. Scores are only valid for 2 years<br>";
                }
                if($greSubName == "")
                {
                    $localErr .= "GRE Subject Score name is invalid<br>";
                    
                }
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update gresubjectscore set
            testdate='".formatMySQLdate2($greSubDate,'/','-')."',
            name='".$greSubName."',
            score=".$greSubScore.",
            percentile=".$greSubPer."
            where id = ". $greSubId[$i]. " and application_id= " .$_SESSION['appid'];
            $result = mysql_query($sql)    or die(mysql_error().$sql);
            $sectioncomplete = true;
        }else
        {
            $err .= $localErr;
        }
    }//END SUBJECT UPDATE
    
    for($i = 0; $i < count($toefliId); $i++)
    {
        
        //SET VARS
        $toeflIntDate = $_POST["txtToeflIntDate_".$toefliId[$i]];
        $toeflIntSect1 = "NULL";
        $toeflIntSect2 = "NULL";
        $toeflIntSect3 = "NULL";
        $toeflIntEssay = "NULL";
        $toeflIntTotal = "NULL";
        
        if($_POST["txtToeflIntSect1_".$toefliId[$i]] != "")
        {
            $toeflIntSect1 = strToNum($_POST["txtToeflIntSect1_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntSect2_".$toefliId[$i]] != "")
        {
            $toeflIntSect2 = strToNum($_POST["txtToeflIntSect2_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntSect3_".$toefliId[$i]] != "")
        {
            $toeflIntSect3 = strToNum($_POST["txtToeflIntSect3_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntEssay_".$toefliId[$i]] != "")
        {
            $toeflIntEssay = floatval($_POST["txtToeflIntEssay_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntTotal_".$toefliId[$i]] != "")
        {
            $toeflIntTotal = strToNum($_POST["txtToeflIntTotal_".$toefliId[$i]]);
        }

        
        $localErr = "";
        //DO VALIDATION
        if($toeflIntDate != "" && $toeflIntDate != "00/0000")
        {
            if(check_Date2($toeflIntDate) == false)
            {
                $localErr .= "TOEFL IBT date is invalid<br>";
            }
            else
            {
                
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $toeflIntDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "TOEFL IBT date is too old. Scores are only valid for 2 years<br>";
                }
            }
        }
        //DO UPDATE
        if($localErr == "" )
        {
            $sql = "update toefl set
            testdate='".formatMySQLdate2($toeflIntDate,'/','-')."',
            section1=".$toeflIntSect1.",
            section2=".$toeflIntSect2.",
            section3=".$toeflIntSect3.",
            essay=".$toeflIntEssay.",
            total=".$toeflIntTotal."
            where id = ".$toefliId[$i] . " and application_id= " .$_SESSION['appid'];
            $result = mysql_query($sql)    or die(mysql_error().$sql);
            $sectioncomplete = true;
            
        }else
        {
            $err .= $localErr;
        }
    }//END TOEFL IBT UPDATE
        
    for($i = 0; $i < count($toeflcId); $i++)
    {
        
        //SET VARS
        $toeflIntDate = $_POST["txtToeflCompDate_".$toeflcId[$i]];
        $toeflIntSect1 = "NULL";
        $toeflIntSect2 = "NULL";
        $toeflIntSect3 = "NULL";
        $toeflIntEssay = "NULL";
        $toeflIntTotal = "NULL";
        
        if($_POST["txtToeflCompSect1_".$toeflcId[$i]] != "")
        {
            $toeflIntSect1 = strToNum($_POST["txtToeflCompSect1_".$toeflcId[$i]]);
        }
        if($_POST["txtToeflCompSect2_".$toeflcId[$i]] != "")
        {
            $toeflIntSect2 = strToNum($_POST["txtToeflCompSect2_".$toeflcId[$i]]);
        }
        if($_POST["txtToeflCompSect3_".$toeflcId[$i]] != "")
        {
            $toeflIntSect3 = strToNum($_POST["txtToeflCompSect3_".$toeflcId[$i]]);
        }
        if($_POST["txtToeflCompEssay_".$toeflcId[$i]] != "")
        {
            $toeflIntEssay = floatval($_POST["txtToeflCompEssay_".$toeflcId[$i]]);
        }
        if($_POST["txtToeflCompTotal_".$toeflcId[$i]] != "")
        {
            $toeflIntTotal = strToNum($_POST["txtToeflCompTotal_".$toeflcId[$i]]);
        }
        
        $localErr = "";
        //DO VALIDATION
        if($toeflIntDate != "" && $toeflIntDate != "00/0000")
        {
            if(check_Date2($toeflIntDate) == false)
            {
                $localErr .= "TOEFL CBT date is invalid<br>";
            }
            else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $toeflIntDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "TOEFL CBT date is too old. Scores are only valid for 2 years<br>";
                }
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update toefl set
            testdate='".formatMySQLdate2($toeflIntDate,'/','-')."',
            section1=".$toeflIntSect1.",
            section2=".$toeflIntSect2.",
            section3=".$toeflIntSect3.",
            essay=".$toeflIntEssay.",
            total=".$toeflIntTotal."
            where id = ".$toeflcId[$i] . " and application_id= " .$_SESSION['appid'];
            $result = mysql_query($sql)    or die(mysql_error().$sql);
            $sectioncomplete = true;
            
        }else
        {
            $err .= $localErr;
        }
    }//END TOEFL CBT UPDATE
    
    for($i = 0; $i < count($toeflpId); $i++)
    {
        
        //SET VARS
        $toeflPaperDate = $_POST["txtToeflPaperDate_".$toeflpId[$i]];
        $toeflPaperSect1 = "NULL";
        $toeflPaperSect2 = "NULL";
        $toeflPaperSect3 = "NULL";
        $toeflPaperEssay = "NULL";
        $toeflPaperTotal = "NULL";
        
        if($_POST["txtToeflPaperSect1_".$toeflpId[$i]] != "")
        {
            $toeflPaperSect1 = strToNum($_POST["txtToeflPaperSect1_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperSect2_".$toeflpId[$i]] != "")
        {
            $toeflPaperSect2 = strToNum($_POST["txtToeflPaperSect2_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperSect3_".$toeflpId[$i]] != "")
        {
            $toeflPaperSect3 = strToNum($_POST["txtToeflPaperSect3_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperEssay_".$toeflpId[$i]] != "")
        {
            $toeflPaperEssay = floatval($_POST["txtToeflPaperEssay_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperTotal_".$toeflpId[$i]] != "")
        {
            $toeflPaperTotal = strToNum($_POST["txtToeflPaperTotal_".$toeflpId[$i]]);
        }

        //DO VALIDATION
        $localErr = "";
        if($toeflPaperDate != "" && $toeflPaperDate != "00/0000")
        {
            if(check_Date2($toeflPaperDate) == false)
            {
                $localErr .= "TOEFL PBT date is invalid<br>";
            }
            else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $toeflPaperDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "TOEFL PBT date is too old. Scores are only valid for 2 years<br>";
                }
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update toefl set
            testdate='".formatMySQLdate2($toeflPaperDate,'/','-')."',
            section1=".$toeflPaperSect1.",
            section2=".$toeflPaperSect2.",
            section3=".$toeflPaperSect3.",
            essay=".$toeflPaperEssay.",
            total=".$toeflPaperTotal."
            where id = ".$toeflpId[$i]. " and application_id= " .$_SESSION['appid'];
            $result = mysql_query($sql)    or die(mysql_error().$sql);
            $sectioncomplete = true;
            
        }else
        {
            $err .= $localErr;
        
        }
    }//END TOEFL PBT UPDATE
    
    
    if($sectioncomplete == true && $grecomplete == true)
    {
        updateReqComplete("scores.php", 1);
    //header("Location: home.php");
    }else
    {
          /* if the GRE scores are required, then don't mark as complete */
          /* if they are not required, then its ok to mark as complete   */
          /* there is really no way to tell if the applicant MUST submit */
          /* TOEFL scores. This has to be documented in the preamble text*/
          /* of this section, (supplied by the user via the admin i/f)   */
          if ($GRE_SCORES_REQUIRED[$domainid] == true ) {
        updateReqComplete("scores.php", 0);
          } else {
        updateReqComplete("scores.php", 1);                
          }
    }
    
}


//LOOK FOR GRE GENERAL SCORE
$sql = "select grescore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from grescore 
left outer join datafileinfo on datafileinfo.id = grescore.datafile_id
where application_id = " . $_SESSION['appid'];
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['verbalscore']);
    array_push($arr, $row['verbalpercentile']);
    array_push($arr, $row['quantitativescore']);
    array_push($arr, $row['quantitativepercentile']);
    array_push($arr, $row['analyticalwritingscore']);
    array_push($arr, $row['analyticalwritingpercentile']);
    array_push($arr, $row['analyticalscore']);
    array_push($arr, $row['analyticalpercentile']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($grescores,$arr); 
}
//LOOK FOR GRE SUBJECT SCORE
$sql = "select gresubjectscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from gresubjectscore
left outer join datafileinfo on datafileinfo.id = gresubjectscore.datafile_id
where application_id = " . $_SESSION['appid'];
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['name']);
    array_push($arr, $row['score']);
    array_push($arr, $row['percentile']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($gresubjectscores,$arr); 
}
//LOOK FOR TOEFL SCORE

//LOOK FOR TOEFL IBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " . $_SESSION['appid'] . " and toefl.type = 'IBT'";
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    //echo $row['essay'];
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toefliscores,$arr); 
}
//LOOK FOR TOEFL CBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " . $_SESSION['appid'] . " and toefl.type = 'CBT'";
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflcscores,$arr); 
}
//LOOK FOR TOEFL PBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " . $_SESSION['appid'] . " and toefl.type = 'PBT'";
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflpscores,$arr); 
}
?>
 <script language="JavaScript" type="text/JavaScript">
<!--
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to delete?");
if (agree)
    return true ;
else
    return false ;
}
//-->
</script>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
        <form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
	
            <!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Test Scores </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
           <span class="errorSubtitle"><?=$err;?></span>
<br>

<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
                <div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
            </div><span class="tblItem">= required</span><br>
            <br>
            <? showEditText("Save Scores", "button", "btnSave", $_SESSION['allow_edit']); ?>
            <hr size="1" noshade color="#990000">
            <? for($i = 0; $i < count($grescores); $i++){ ?>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><span class="subtitle">GRE General <? 
                if($grescores[$i][10] == 1)
                {
                    echo "<em>(Official Score Report Received)</em>";
                }else
                {
                    if($grescores[$i][1] != "00/0000" && $grescores[$i][1] != "")
                    {
                        echo "<em>(Official Score Report Not Received)</em>";
                    }
                }
                 ?></span></td>
                <td align="right"><span class="tblItem">
                  <? showEditText("Clear Scores", "button", "btnDel_1_".$grescores[$i][0], $_SESSION['allow_edit']); ?>
                  <input name="txtGreGenId_<?=$grescores[$i][0]?>" type="hidden" id="txtGreGenId_<?=$grescores[$i][0]?>" value="<?=$grescores[$i][0];?>">
</span></td>
              </tr>
            </table>
            
            <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              <tr>
                <td><strong>Test date (MM/YYYY): </strong></td>
                <td><span class="tblItem">
<!--                  <? showEditText($grescores[$i][1], "textbox", "txtGreGenDate_".$grescores[$i][0], $_SESSION['allow_edit'], true); ?>
-->
                      <? showEditText($grescores[$i][1], "textbox", "txtGreGenDate_".$grescores[$i][0], 
                                      $_SESSION['allow_edit'], $GRE_SCORES_REQUIRED[$domainid]); ?>
                </span></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
                <td><strong>Percentile:</strong></td>
              </tr>
              <tr>
                <td><strong>Verbal:</strong></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][2], "listbox", "txtGreGenVerScore_".$grescores[$i][0], $_SESSION['allow_edit'], false, $greScoreRange1); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][3], "listbox", "txtGreGenVerPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>
%                </span></td>
              </tr>
              <tr>
                <td><strong>Quantitative:</strong></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][4], "listbox", "txtGreGenQuanScore_".$grescores[$i][0], $_SESSION['allow_edit'], false,$greScoreRange1); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][5], "listbox", "txtGreGenQuanPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%
                </span></td>
              </tr>
              <tr>
                <td><strong>Analytical Writing: </strong></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][6], "listbox", "txtGreGenWrScore_".$grescores[$i][0], $_SESSION['allow_edit'], false,$greScoreRange2); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][7], "listbox", "txtGreGenWrPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%
                </span></td>
              </tr>
              <tr>
                <td><strong>Analytical:</strong></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][8], "listbox", "txtGreGenAnScore_".$grescores[$i][0], $_SESSION['allow_edit'], false,$greScoreRange1); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($grescores[$i][9], "listbox", "txtGreGenAnPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%
                </span></td>
              </tr>
            </table>
            <? 
            if($grescores[$i][1] != "00/0000")
            {
            $qs = "?id=".$grescores[$i][0]."&t=6"; ?>
            <br>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
            (if available)
            <?
            if($grescores[$i][12] != "")
            {
                showFileInfo("grescore.".$grescores[$i][14], $grescores[$i][13], formatUSdate2($grescores[$i][12]), getFilePath(6, $grescores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
            }
            
            
            if($_SESSION['allow_edit'] == true){ ?>
                <br>
            <? }//end if allowedit 
            }//end if date
            ?>
            
            <hr size="1" noshade color="#990000">
            <? } ?>
            <? for($i = 0; $i < count($gresubjectscores); $i++){ ?>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><span class="subtitle">GRE Subject <? 
                if($gresubjectscores[$i][5] == 1)
                {
                    echo "<em>(Official Score Report Received)</em>";
                }else
                {
                    if($gresubjectscores[$i][1] != "00/0000" && $gresubjectscores[$i][1] != "")
                    {
                        echo "<em>(Official Score Report Not Received)</em>";
                    }
                }
                 ?></span></td>
                <td align="right"><span class="tblItem">
                  <? showEditText("Clear Scores", "button", "btnDel_2_".$gresubjectscores[$i][0], $_SESSION['allow_edit']); ?>
                  <input name="txtGreSubId_<?=$gresubjectscores[$i][0]?>" type="hidden" id="txtGreSubId_<?=$gresubjectscores[$i][0]?>" value="<?=$gresubjectscores[$i][0]?>">
</span></td>
              </tr>
            </table>
            <span class="tblItem">            </span>
            <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              <tr>
                <td colspan="3"><strong>Test date (MM/YYYY): </strong>                  <? showEditText($gresubjectscores[$i][1], "textbox", "txtGreSubDate_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Subject:</strong></td>
                <td><strong>Score:</strong></td>
                <td><strong>Percentile:</strong></td>
              </tr>
              <tr>
                <td><? showEditText($gresubjectscores[$i][2], "listbox", "lbGreSubName_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false,$greSubs); ?>
                <br></td>
                <td valign="top"><? showEditText($gresubjectscores[$i][3], "listbox", "txtGreSubScore_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false,$greSubRange); ?></td>
                <td valign="top">      <? showEditText($gresubjectscores[$i][4], "listbox", "txtGreSubPer_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false, $greSubPercRange); ?>
                % </td>
              </tr>
              <tr>
                <td colspan="3"><? showEditText($gresubjectscores[$i][2], "textbox", "txtGreSubName_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false, null, true, 30); ?> If other please specify </td>
              </tr>
            </table>
            <? 
            if($gresubjectscores[$i][1] != "00/0000" && $gresubjectscores[$i][1] != "")
            {
            $qs = "?id=".$gresubjectscores[$i][0]."&t=8"; ?>
            <br>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
            (if available)
            <?
            
            if($gresubjectscores[$i][7] != "")
            {
                showFileInfo("gresubjectscore.".$gresubjectscores[$i][9], $gresubjectscores[$i][8], formatUSdate2($gresubjectscores[$i][7]), getFilePath(8, $gresubjectscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
            }
            if($_SESSION['allow_edit'] == true){ ?>
                <br>
            <? }//end if allowedit 
            }//end if date ?>
            <hr size="1" noshade color="#990000">
            <? } ?>

            <? for($i = 0; $i < count($toefliscores); $i++){ ?>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><span class="subtitle">TOEFL Internet-Based <? 
                if($toefliscores[$i][7] == 1)
                {
                    echo "<em>(Official Score Report Received)</em>";
                }else
                {
                    if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
                    {
                        echo "<em>(Official Score Report Not Received)</em>";
                    }
                }
                 ?></span></td>
                <td align="right"><span class="tblItem">
                  <? showEditText("Clear Scores", "button", "btnDel_3_".$toefliscores[$i][0], $_SESSION['allow_edit']); ?>
                  <input name="txtToeflIntId_<?=$toefliscores[$i][0]?>" type="hidden" id="txtToeflIntId_<?=$toefliscores[$i][0]?>" value="<?=$toefliscores[$i][0]?>">
                </span></td>
              </tr>
            </table>
            <span class="tblItem">            </span>
            <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              <tr>
                <td><strong>Test date (MM/YYYY): </strong></td>
                <td><? showEditText($toefliscores[$i][1], "textbox", "txtToeflIntDate_".$toefliscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
              </tr>
              <tr>
                <td><strong>Reading: </strong></td>
                <td><? showEditText($toefliscores[$i][4], "listbox", "txtToeflIntSect3_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
              </tr>
              
              <tr>
                <td><strong>Listening:</strong></td>
                <td><? showEditText($toefliscores[$i][3], "listbox", "txtToeflIntSect2_".$toefliscores[$i][0], $_SESSION['allow_edit'], false,$toeflRange1); ?></td>
              </tr>
              <tr>
                <td><strong>Speaking:</strong></td>
                <td><? showEditText($toefliscores[$i][2], "listbox", "txtToeflIntSect1_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
              </tr>
              <tr>
                <td><strong>Writing:</strong></td>
                <td><? showEditText($toefliscores[$i][5], "listbox", "txtToeflIntEssay_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
              </tr>
              <tr>
                <td><strong>Total Score:</strong></td>
                <td><? showEditText($toefliscores[$i][6], "listbox", "txtToeflIntTotal_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange6); ?></td>
              </tr>
            </table>
            <? 
            if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
            {
            $qs = "?id=".$toefliscores[$i][0]."&t=7"; ?>
            <br>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
            (if available)
            <?
            if($toefliscores[$i][8] != "")
            {
                showFileInfo("toeflscore.".$toefliscores[$i][10], $toefliscores[$i][9], formatUSdate2($toefliscores[$i][8]), getFilePath(7, $toefliscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $toefliscores[$i][11]));
            }
            if($_SESSION['allow_edit'] == true){ ?>
                <br>
            <? }//end if allowedit 
            }//end if date ?>
            <hr size="1" noshade color="#990000">
            <? } ?>
            <? for($i = 0; $i < count($toeflcscores); $i++){ ?>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><span class="subtitle">TOEFL Computer-Based 
                <? 
                if($toeflcscores[$i][7] == 1)
                {
                    echo "<em>(Official Score Report Received)</em>";
                }else
                {
                    if($toeflcscores[$i][1] != "00/0000" && $toeflcscores[$i][1] != "")
                    {
                        echo "<em>(Official Score Report Not Received)</em>";
                    }
                }
                 ?></span></td>
                <td align="right"><span class="tblItem">
                  <? showEditText("Clear Scores", "button", "btnDel_4_".$toeflcscores[$i][0], $_SESSION['allow_edit']); ?>
                  <input name="txtToeflCompId_<?=$toeflcscores[$i][0]?>" type="hidden" id="txtToeflCompId_<?=$toeflcscores[$i][0]?>" value="<?=$toeflcscores[$i][0]?>">
</span></td>
              </tr>
            </table>
            <span class="tblItem">            </span>
            <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              <tr>
                <td><strong>Test date (MM/YYYY): </strong></td>
                <td><? showEditText($toeflcscores[$i][1], "textbox", "txtToeflCompDate_".$toeflcscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
              </tr>
              
              <tr>
                <td><strong>Listening:</strong></td>
                <td><? showEditText($toeflcscores[$i][3], "listbox", "txtToeflCompSect2_".$toeflcscores[$i][0], $_SESSION['allow_edit'], false,$toeflRange1); ?></td>
              </tr>
              <tr>
                <td><strong>Structure/Writing:</strong></td>
                <td><? showEditText($toeflcscores[$i][2], "listbox", "txtToeflCompSect1_".$toeflcscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
              </tr>
              <tr>
                <td><strong>Reading: </strong></td>
                <td><? showEditText($toeflcscores[$i][4], "listbox", "txtToeflCompSect3_".$toeflcscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
              </tr>
              <tr>
                <td><strong>Total Score:</strong></td>
                <td><? showEditText($toeflcscores[$i][6], "listbox", "txtToeflCompTotal_".$toeflcscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange2); ?></td>
              </tr>
              <tr>
                <td><strong>Essay Rating:</strong></td>
                <td><? showEditText($toeflcscores[$i][5], "listbox", "txtToeflCompEssay_".$toeflcscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange3); ?></td>
              </tr>
            </table>
            <? 
            if($toeflcscores[$i][1] != "00/0000" && $toeflcscores[$i][1] != "")
            {
            $qs = "?id=".$toeflcscores[$i][0]."&t=9"; ?>
            <br>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
            (if available)
            <?
            if($toeflcscores[$i][8] != "")
            {
                showFileInfo("toeflscore.".$toeflcscores[$i][10], $toeflcscores[$i][9], formatUSdate2($toeflcscores[$i][8]), getFilePath(9, $toeflcscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $toeflcscores[$i][11]));
            }
            if($_SESSION['allow_edit'] == true){ ?>
                <br>
            <? }//end if allowedit 
            }//end if date ?>
            <hr size="1" noshade color="#990000">
            <? } ?>
            <? for($i = 0; $i < count($toeflpscores); $i++){ ?>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><span class="subtitle">TOEFL Paper-Based <? 
                if($toeflpscores[$i][7] == 1)
                {
                    echo "<em>(Official Score Report Received)</em>";
                }else
                {
                    if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
                    {
                        echo "<em>(Official Score Report Not Received)</em>";
                    }
                }
                 ?></span></td>
                <td align="right"><span class="tblItem">
                  <? showEditText("Clear Scores", "button", "btnDel_5_".$toeflpscores[$i][0], $_SESSION['allow_edit']); ?>
                  <input name="txtToeflPaperId_<?=$toeflpscores[$i][0]?>" type="hidden" id="txtToeflPaperId_<?=$toeflpscores[$i][0]?>" value="<?=$toeflpscores[$i][0]?>">
</span></td>
              </tr>
            </table>
            <span class="tblItem">            </span>
            <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              <tr>
                <td><strong>Test date (MM/YYYY): </strong></td>
                <td><? showEditText($toeflpscores[$i][1], "textbox", "txtToeflPaperDate_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
              </tr>
              <tr>
                <td><strong>Section 1 :</strong></td>
                <td><? showEditText($toeflpscores[$i][2], "listbox", "txtToeflPaperSect1_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange4); ?></td>
              </tr>
              <tr>
                <td><strong>Section 2 :</strong></td>
                <td><? showEditText($toeflpscores[$i][3], "listbox", "txtToeflPaperSect2_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange4); ?></td>
              </tr>
              <tr>
                <td><strong>Section 3 : </strong></td>
                <td><? showEditText($toeflpscores[$i][4], "listbox", "txtToeflPaperSect3_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange7); ?></td>
              </tr>
              <tr>
                <td><strong>Total Score :</strong></td>
                <td><? showEditText($toeflpscores[$i][6], "listbox", "txtToeflPaperTotal_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange5); ?></td>
              </tr>
              <tr>
                <td><strong>TWE Score :</strong></td>
                <td><? showEditText($toeflpscores[$i][5], "listbox", "txtToeflPaperEssay_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange3); ?></td>
              </tr>
            </table>
            <? 
            if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
            {
            $qs = "?id=".$toeflpscores[$i][0]."&t=10"; ?>
            <br>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
            (if available)
            <?
            if($toeflpscores[$i][8] != "")
            {
                showFileInfo("toeflscore.".$toeflpscores[$i][10], $toeflpscores[$i][9], formatUSdate2($toeflpscores[$i][8]), getFilePath(10, $toeflpscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $toeflpscores[$i][11]));
            }
            if($_SESSION['allow_edit'] == true){ ?>
                <br>
            <? }//end if allowedit 
            }//end if date ?>
            <hr size="1" noshade color="#990000">
            <? } ?>            
            <br>
            <? showEditText("Save Scores", "button", "btnSave", $_SESSION['allow_edit']); ?>
            <br>
            <span class="tblItem">            </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
            <div style="float:left;"> &nbsp;&nbsp;<a href="bio.php">< Previous </a>
                &nbsp; | &nbsp;<a href="uni.php">Next ></a></div>
            <br style="clear:left">
            <!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
