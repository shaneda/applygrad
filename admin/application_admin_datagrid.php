<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>test page</title>
<script language="javascript" src="./jqSajax/jquery-1.2.2.pack.js"> </script>
<script>
function getAjaxRequest() {
var ajaxRequest;  // The variable that makes Ajax possible! 
 try{
   // Opera 8.0+, Firefox, Safari
   ajaxRequest = new XMLHttpRequest();
 }catch (e){
   // Internet Explorer Browsers
   try{
      ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
   }catch (e) {
      try{
         ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
      }catch (e){
         // Something went wrong
         alert("Your browser broke!");
         return false;
      }
   }
 }
 return ajaxRequest;
}

function replaceDivElements (parent, child, new_value) {
    //document.getElementById("ajaxDiv").innerText =    ajaxDisplay.value;
   
      if(document.getElementById) { //Open standards method
      
        var childDiv = document.getElementById(child);
      
          
      }
      else if(document.all) { //IE method
        var childDiv=document.all[child]; 
      }

        
      var newDiv = document.createElement('div');
      //alert ("before adding child"+newDiv+"Child "+child ); 
      newDiv.id = childDiv.id ;
      
      newDiv.innerHTML = new_value;
      var parentElem =  document.getElementById(parent);
      while (parentElem.firstChild) {
         parentElem.removeChild(parentElem.firstChild);
      }
      parentElem.appendChild(newDiv)  ;
}

function ajaxFunctionUpdateInsert(id, name, origval, ask, colName){
     var ajaxRequest;  // The variable that makes Ajax possible!
      ajaxRequest =  getAjaxRequest();
     // Create a function that will receive data 
     // sent from the server and will update
     // div section in the same page.
     ajaxRequest.onreadystatechange = function(){
       if(ajaxRequest.readyState == 4){
           document.getElementById('workspace').innerHTML=ajaxRequest.responseText; 
       }
     }
     if (ask) {
        if (confirm("Are you sure you want to change "+colName)) {
            ajaxRequest.open("GET","results.php?index="+id+"&n="+name+"&v="+origval, true);
            ajaxRequest.send(null);
            }
     } 
     else {
            ajaxRequest.open("GET","results.php?index="+id+"&n="+name+"&v="+origval, true);
            ajaxRequest.send(null);
     }                         
}                              

function ajaxFunctionDelete(){
     var ajaxRequest;  // The variable that makes Ajax possible!
      ajaxRequest =  getAjaxRequest();
     // Create a function that will receive data 
     // sent from the server and will update
     // div section in the same page.
     ajaxRequest.onreadystatechange = function(){
       if(ajaxRequest.readyState == 4){
         replaceDivElements ( "parentForAjaxForm", "ajaxFormDiv",ajaxRequest.responseText); 
       }
     }
     // Now get the value from user and pass it to
     // server script.
     //alert (key);
     
      var checked_rec_key = getChkVal("ActionForm");
      var queryString = "?mode=d&RKEY="+checked_rec_key;//+"&DBGSESSID=1;d%3D1";
      ajaxRequest.open("GET","table_tblfred.php"+queryString, true);
      ajaxRequest.send(null); 
                               
}
//Browser Support Code          
function ajaxFunctionForm(key, start){
 var ajaxRequest;  // The variable that makes Ajax possible!
      ajaxRequest =  getAjaxRequest();
     // Create a function that will receive data 
     // sent from the server and will update
     // div section in the same page.
    
     ajaxRequest.onreadystatechange = function(){
     
       if(ajaxRequest.readyState == 4){
       //  alert ( ajaxRequest.responseText);
           replaceDivElements ( "parentForAjaxForm", "ajaxFormDiv",ajaxRequest.responseText); 
       }
     }
     // Now get the value from user and pass it to
     // server script.
     //alert (key);
 // Now get the value from user and pass it to
 // server script.
 //alert (key);
 var queryString = "?";
  
 if (typeof key != "undefined") 
   queryString = queryString + "&order_by=" + key;//+"&DBGSESSID=1;d%3D1";
 if (typeof start != "undefined") 
   queryString = queryString + "&start=" + start;
 
 ajaxRequest.open("GET", "table_tblfred.php" +  queryString, true);
 ajaxRequest.send(null); 
 }
 
 </script>
 
</head>
<body>
<div name="workspace" id="workspace">

</div>
<div>
<?php

require('Structures/DataGrid.php');


// sql with domain_id coded to 1


// Create the DataGrid, Bind it's Data Source and Define it's columns
$dg =& new Structures_DataGrid(10); // Display 20 per page
// bind datagrid using MDB2 driver   
    $ds_options = array(
      'dsn' => 'mysql://phdApp:phd321app@rogue.fac.cs.cmu.edu/gradAdmissions2008Dev',
    );
    $dg->bind('select lu_users_usertypes.id as id, concat(users.lastname, " ",users.firstname) as name,
        users.email,
        GROUP_CONCAT(concat(" ",degree.name, " ", programs.linkword, " ", fieldsofstudy.name)) as program,
        if(application.sent_to_program = 1,"yes"," ") as cmp,if(application.submitted = 1,"yes"," ") as sbmd,
        if(application.paid = 1,"yes"," ") as pd,
        if(application.waive = 1,"yes"," ") as wd,
        rec_half.requested, rec_half.submitted, rec_half.gre_rcvd, rec_half.gress_rcvd, rec_half.toefl_rcvd, usersinst.transscriptreceived as trans_rcvd
        from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join application on application.user_id = lu_users_usertypes.id
        left outer join lu_application_programs on lu_application_programs.application_id = application.id
        left outer join programs on programs.id = lu_application_programs.program_id
        left outer join degree on degree.id = programs.degree_id
        left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
        
        left outer join (select application.id as app_id,application.user_id AS user_id,
count(nullif(recommend.rec_user_id,-(1))) AS requested,
count(nullif(recommend.submitted,0)) AS submitted,
grescore.scorereceived AS gre_rcvd,gresubjectscore.scorereceived AS gress_rcvd,
test.submitted AS toefl_rcvd 
from recommend 
left join application on application.id = recommend.application_id 
left join grescore on application.id = grescore.application_id
left join gresubjectscore on application.id = gresubjectscore.application_id 
left join (select application_id, if(count(scorereceived) >= 1,1," ") as submitted from toefl
group by application_id) as test on application.id = test.application_id
group by application.id) as rec_half on application.id = rec_half.app_id
left outer join usersinst on usersinst.user_id = lu_users_usertypes.user_id
        
        where (lu_users_usertypes.usertype_id = 5) and lu_domain_department.domain_id = 1
        group by users.id', $ds_options, 'MDB2');
 //       order by users.lastname,users.firstname, programs.rank', $ds_options, 'MDB2');
//$dg->bind($user);

$roleList = array (1 => 'First', 2 => 'Second', 3 => 'Third');

//$dg->sortRecordSet($orderBy, $dir);
$dg->addColumn(new Structures_DataGrid_Column('Name', 'name', 'name', array('width' => '20%', 'align' => 'center'), null, 'printFullName()'));
$dg->addColumn(new Structures_DataGrid_Column('Email', 'email', 'email', array('width' => '20%'), null, 'printEmail()'));
$dg->addColumn(new Structures_DataGrid_Column('Transcript', 'trans_rcvd', 'trans_rcvd', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));
$dg->addColumn(new Structures_DataGrid_Column('GRE', 'gre_rcvd', 'gre_rcvd', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));
$dg->addColumn(new Structures_DataGrid_Column('Subject', 'gress_rcvd', 'gress_rcvd', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));
$dg->addColumn(new Structures_DataGrid_Column('TOEFL', 'toefl_rcvd', 'toefl_rcvd', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));
/* left in as an example of having a dropdown list as a colume
*/
// $dg->addColumn(new Structures_DataGrid_Column('Role', 'pd', null, array('width' => '20%', 'align' => 'center'), null, 'printRoleSelector()'));
$dg->addColumn(new Structures_DataGrid_Column('Paid', 'pd', 'paid', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));
$dg->addColumn(new Structures_DataGrid_Column('Waived', 'wd', 'waived', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));
$dg->addColumn(new Structures_DataGrid_Column('Recommendations<br>Submitted', 'submitted', 'submitted', array('width' => '10', 'align' => 'center'), null, 'printRecsSubmitted()')); 
$dg->addColumn(new Structures_DataGrid_Column('Submitted', 'sbmb', 'application submitted', array('width' => '10', 'align' => 'center'), null, 'printConfirmCheckbox()'));
$dg->addColumn(new Structures_DataGrid_Column('Complete', 'cmp', 'complete', array('width' => '10', 'align' => 'center'), null, 'printCheckbox()'));  

// Define the Look and Feel
$dg->renderer->setTableHeaderAttributes(array('bgcolor' => '#CCCCCC'));
$dg->renderer->setTableEvenRowAttributes(array('bgcolor' => '#FFFFFF'));
$dg->renderer->setTableOddRowAttributes(array('bgcolor' => '#EEEEEE'));
$dg->renderer->setTableAttribute('width', '100%');
$dg->renderer->setTableAttribute('cellspacing', '0');
$dg->renderer->setTableAttribute('cellpadding', '4');
$dg->renderer->setTableAttribute('class', 'datagrid');
$dg->renderer->sortIconASC = '&uArr;';
$dg->renderer->sortIconDESC = '&dArr;';

// Print the DataGrid and the Paging
// render sort form
    $hsf_options = array(
      'directionStyle' => 'radio',
      'textSubmit'     => 'Sort Grid',
      'sortFieldsNum'  => '3'
    );
    $dg->render('HTMLSortForm', $hsf_options);
$dg->render();
echo $dg->renderer->getPaging();

function printCheckbox($params)
{  // DebugBreak();
    extract($params);
    $return = '<input type="checkbox" name="idList[]" onclick="ajaxFunctionUpdateInsert('.$record['id'].', \''. $params['fieldName'].'\', \''. $record[$params['fieldName']].'\',false'.', \''. $params['columnName'].'\')" value="' . $record[$params['fieldName']] . '">';
    return $return;
}

function printConfirmCheckbox($params)
{  // DebugBreak();
    extract($params);
    $return = '<input type="checkbox" name="idList[]" onclick="ajaxFunctionUpdateInsert('.$record['id'].', \''. $params['fieldName'].'\', \''. $record[$params['fieldName']].'\',true'.', \''. $params['columnName'].'\')" value="' . $record[$params['fieldName']] . '">';
    return $return;
}

function printFullName($params)
{   
    extract($params);    
    return $record['name'];
}

function printRecsRequested($params)
{   
    extract($params);    
    return $record['requested'];
}

function printRecsSubmitted($params)
{   
    
    extract($params);  
    if ($record ['requested'] > 0) {
    return '<input size=6 type="text" name="idList[]" onclick="ajaxFunctionUpdateInsert('.$record['id'].')" value="' . $record['submitted']." of ".$record['requested'] . '">';
    } else return '<input size=6 type="text" name="idList[]" onclick="ajaxFunctionUpdateInsert('.$record['id'].')" value="0 of 0">';
}

function printEmail($params)
{   
    extract($params);    
    return $record['email'];
}
function printRoleSelector($params)
{
    global $roleList;

    extract($params);
    
    $html = "<select name=\"role_id\" onChange=\"ajaxFunctionUpdateInsert()\">";
    foreach ($roleList as $roleId => $roleName) {
   //     DebugBreak();
        if ($record['priority'] == $roleId) {
            $html .= "<option value=\"$roleId\" selected>$roleName</option>\n";
        } else {
            $html .= "<option value=\"$roleId\">$roleName</option>\n";
        }
    }
    $html .= '</select>';
    
    return $html;
}
function printEditLink($params)
{
    extract($params);
    return '<a href="edit.php?id=' . $record['id'] . '">Edit</a>';
}
?>
</div>
</body>
</html>



<?php

//  var newsOut = new Array () ;

/*
  $count = 0 ;
  foreach ($newsOutput as $nout) {
    echo "newsOut[".$count."] = ['".implode("','", $nout)."'] ;\n" ;
    $count ++ ;
  }
  */
?>
 <?php

function cleanUpString ($theString) {
  // dispose of new line characters and carriage returns,
  // and escape single quotes using preg_replace
  $findThese = Array ("/\r|\n/", "/'/") ;
  $useThese = Array ("", "\'") ;
  return preg_replace ($findThese, $useThese, $theString) ;
}
?>