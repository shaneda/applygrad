<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<?
$err = "";
$uid = -1;
$umasterid = -1;
$appid = -1;
$myPrograms = array();

$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
$ethnicities = array();
$titles = array(array('Mr.', 'Mr.'), array('Ms.','Ms.'));
$err = "";
$mode = "edit";
$title = "";
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$ethnicity = "";
$pass = "";
$passC = "";
$dob = "";
$visaStatus = -1;
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$street4 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$telMobile = "";
$telWork = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$streetP4 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";
$telPMobile = "";
$telPWork = "";
$countryCit = "";
$curResident = "";

$degrees = array();
$uniTypes = array();
$institutes = array();
$myInstitutes = array();
$scales = array();
$transUploaded = true;
$comp = false;

$arr = array(2, "Graduate");
array_push($uniTypes, $arr);
$arr = array(3, "Additional College/University");
array_push($uniTypes, $arr);

$sectioncomplete = false;
$grecomplete = false;
$grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflcscores = array();
$toeflpscores = array();
$greScoreRange1 = array();
$greScoreRange2 = array();
$grePercRange1 = array();

$pubs = array();
$fellows = array();
$crossDeptProgs = array();
$crossDeptProgsOther = "";
$advisors = array();//FACULTY DESIGNATED AS ADVISORS
$advisor1 = "";
$advisor2 = "";
$advisor3 = "";
$advisor4 = "";
$expResearch = "";
$expInd = "";
$exFileId = "";
$exFileDate = "";
$exFileSize = "";
$exFileExt = "";
$pier = "No";
$wFellow = "No";
$port = "No";
$portLink = "";
$prog = "";
$design = "";
$stats = "";
$enrolled = 0;
$referral = "";
$honors = "";
$otherUni = "";
$tmpCrossDeptProgsId = "";
$permission= 0;

$myRecommenders = array();
$buckley = false;

$myAreas = array();

if(isset($_GET['id']))
{
	$uid = intval($_GET['id']);
}
if($uid > -1)
{
	//GET USER INFO
	$sql = "SELECT 
	users.id as usermasterid,
	users.email,
	users.title,
	users.firstname,
	users.middlename,
	users.lastname,
	users.initials,
	users_info.gender,
	users_info.dob,
	users_info.address_cur_street1,
	users_info.address_cur_street2,
	users_info.address_cur_street3,
	users_info.address_cur_street4,
	users_info.address_cur_city,
	cur_states.name as currentstate,
	users_info.address_cur_pcode,
	cur_countries.name as currentcountry,
	users_info.address_cur_tel,
	users_info.address_cur_tel_mobile,
	users_info.address_cur_tel_work,
	users_info.address_perm_street1,
	users_info.address_perm_street2,
	users_info.address_perm_street3,
	users_info.address_perm_street4,
	users_info.address_perm_city,
	perm_states.name as permstate,
	users_info.address_perm_pcode,
	perm_states.name as permcountry,
	users_info.address_perm_tel,
	users_info.address_perm_tel_mobile,
	users_info.address_perm_tel_work,
	ethnicity.name as ethnicity,
	users_info.visastatus,
	users_info.homepage,
	cit_countries.name as citcountry,
	users_info.cur_pa_res,
	application.id as appid
	FROM lu_users_usertypes
	inner join users on users.id = lu_users_usertypes.user_id 
	left outer join users_info on users_info.user_id = lu_users_usertypes.id 
	left outer join states as cur_states on cur_states.id = users_info.address_cur_state
	left outer join states as perm_states on perm_states.id = users_info.address_perm_state
	left outer join countries as cur_countries on cur_countries.id = users_info.address_cur_country
	left outer join countries as perm_countries on perm_countries.id = users_info.address_perm_country
	left outer join countries as cit_countries on cit_countries.id = users_info.cit_country
	left outer join application on application.user_id = lu_users_usertypes.id 
	left outer join ethnicity on ethnicity.id = users_info.ethnicity
	where lu_users_usertypes.id=".$uid;
	//echo $sql;
	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$umasterid = $row['usermasterid'];
		$email = $row['email'];
		$title = $row['title'];
		$fName = $row['firstname'];
		$mName = $row['middlename'];
		$lName = $row['lastname'];
		$initials = $row['initials'];
		$gender = $row['gender'];
		$dob = formatUSdate($row['dob'],'-','/');
		$street1 = $row['address_cur_street1'];
		$street2 = $row['address_cur_street2'];
		$street3 = $row['address_cur_street3'];
		$street4 = $row['address_cur_street4'];
		$city = $row['address_cur_city'];
		$state = $row['currentstate'];
		$postal = $row['address_cur_pcode'];
		$country = $row['currentcountry'];
		$tel = $row['address_cur_tel'];
		$telMobile = $row['address_cur_tel_mobile'];
		$telWork = $row['address_cur_tel_work'];
		$streetP1 = $row['address_perm_street1'];
		$streetP2 = $row['address_perm_street2'];
		$streetP3 = $row['address_perm_street3'];
		$streetP4 = $row['address_perm_street4'];
		$cityP = $row['address_perm_city'];
		$stateP = $row['permstate'];
		$postalP = $row['address_perm_pcode'];
		$countryP = $row['permcountry'];
		$ethnicity = $row['ethnicity'];
		$visaStatus = $row['visastatus'];
		$homepage = $row['homepage'];
		$countryCit = $row['citcountry'];
		if($row['cur_pa_res'] == 1)
		{
			$curResident = "Yes";
		}
		$appid = $row['appid'];
	}
	
	//GET USER SELECTED PROGRAMS
	$sql = "SELECT programs.id, 
	degree.name as degreename,
	fieldsofstudy.name as fieldname, 
	choice, 
	lu_application_programs.id as itemid,
	programs.programprice,
	programs.baseprice,
	programs.linkword
	FROM lu_application_programs 
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	where lu_application_programs.application_id = ".$appid." group by programs.id order by choice";
	

	$result = mysql_query($sql) or die(mysql_error() . $sql);
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$dept = "";
		$baseprice = 0.0;
		$oracle = "";
		$depts = getDepartments($row[0]);
		$tmpDept = "";
		
		if(count($depts) > 1)
		{
			
			$dept = " - ";
			for($i = 0; $i < count($depts); $i++)
			{
				$dept .= $depts[$i][1];
				$oracle = $depts[$i][2];
				$baseprice = $depts[$i][3];
				if($i < count($depts)-1)
				{
					$dept .= "/";
				}
				
			}
		}
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['linkword']);
		array_push($arr, $row['fieldname'].$dept);
		array_push($arr, $row['choice']);
		array_push($arr, $row['itemid']);
		array_push($arr, $row['programprice']);
		array_push($arr, $oracle);
		array_push($arr, $row['baseprice']);
	
		array_push($myPrograms, $arr);
	}
		
		
}//END IF UID



function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT 
	department.id,
	department.name,
	department.oraclestring
	FROM lu_programs_departments 
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($arr, $row["oraclestring"]);
		array_push($ret, $arr);
	}
	return $ret;
}

//GET EDUCATION
$sql = "SELECT 
	usersinst.id,institutes.name,date_entered,
	date_grad,date_left,degreesall.name as degree,
	major1,major2,major3,minor1,minor2,gpa,gpa_major,
	gpascales.name as gpa_scale,transscriptreceived,datafile_id,educationtype,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM usersinst 
	left outer join institutes on institutes.id = usersinst.institute_id
	left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
	left outer join gpascales on gpascales.id = usersinst.gpa_scale
	left outer join degreesall on degreesall.id = usersinst.degree
	where usersinst.user_id=".$uid. " order by usersinst.educationtype";
	$result = mysql_query($sql) or die(mysql_error());
	$comp = true;
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		if($row['name'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_entered'],'-','/')  );
		if($row['date_entered'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_grad'],'-','/') );
		if($row['date_grad'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_left'],'-','/') );
		array_push($arr, $row['degree']);
		if($row['degree'] == ""){$comp = false; }
		array_push($arr, $row['major1']);
		array_push($arr, $row['major2']);
		array_push($arr, $row['major3']);
		array_push($arr, $row['minor1']);
		array_push($arr, $row['minor2']);
		array_push($arr, $row['gpa']);
		if($row['gpa'] == ""){$comp = false; }
		array_push($arr, $row['gpa_major']);
		if($row['gpa_major'] == ""){$comp = false; }
		array_push($arr, $row['gpa_scale']);
		if($row['gpa_scale'] == ""){$comp = false; }
		array_push($arr, $row['transscriptreceived']);
		array_push($arr, $row['datafile_id']);
		if($row['datafile_id'] == ""){$comp = false; }
		array_push($arr, $row['educationtype']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($myInstitutes, $arr);
	}
//GET SCORES

//LOOK FOR GRE GENERAL SCORE
$sql = "select grescore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from grescore 
left outer join datafileinfo on datafileinfo.id = grescore.datafile_id
where application_id = " .$appid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['verbalscore']);
	array_push($arr, $row['verbalpercentile']);
	array_push($arr, $row['quantitativescore']);
	array_push($arr, $row['quantitativepercentile']);
	array_push($arr, $row['analyticalwritingscore']);
	array_push($arr, $row['analyticalwritingpercentile']);
	array_push($arr, $row['analyticalscore']);
	array_push($arr, $row['analyticalpercentile']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['datafile_id']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($grescores,$arr); 
	
}
//LOOK FOR GRE SUBJECT SCORE
$sql = "select gresubjectscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from gresubjectscore
left outer join datafileinfo on datafileinfo.id = gresubjectscore.datafile_id
where application_id = " .$appid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['name']);
	array_push($arr, $row['score']);
	array_push($arr, $row['percentile']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['datafile_id']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($gresubjectscores,$arr); 
}
//LOOK FOR TOEFL SCORE

//LOOK FOR TOEFL IBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'IBT'";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['section1']);
	array_push($arr, $row['section2']);
	array_push($arr, $row['section3']);
	array_push($arr, $row['essay']);
	//echo $row['essay'];
	array_push($arr, $row['total']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($arr, $row['datafile_id']);
	array_push($toefliscores,$arr); 
}
//LOOK FOR TOEFL CBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'CBT'";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['section1']);
	array_push($arr, $row['section2']);
	array_push($arr, $row['section3']);
	array_push($arr, $row['essay']);
	array_push($arr, $row['total']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($arr, $row['datafile_id']);
	array_push($toeflcscores,$arr); 
}
//LOOK FOR TOEFL PBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'PBT'";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['section1']);
	array_push($arr, $row['section2']);
	array_push($arr, $row['section3']);
	array_push($arr, $row['essay']);
	array_push($arr, $row['total']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($arr, $row['datafile_id']);
	array_push($toeflpscores,$arr); 
}

//SUPPLEMENTAL INFO

$sql = "select pier, womenfellowship, portfoliosubmitted,portfolio_link, area1,area2,area3,
referral_to_program, other_inst, cross_dept_progs,cross_dept_progs_other, records_permission  
from application where id=".$appid;
$result = mysql_query($sql) or die(mysql_error());
$tmpProgs = "";
while($row = mysql_fetch_array( $result ))
{
	if($row['womenfellowship'] == 1)
	{
		$wFellow = "Yes";
	}
	if($row['pier'] == 1)
	{
		$pier = "Yes";
	}
	if($row['portfoliosubmitted'] == 1)
	{
		$port = "Yes";
	}
	$prog = $row['area1'];
	$design = $row['area2'];
	$stats = $row['area3'];
	$portLink = stripslashes($row['portfolio_link']);
	$referral = stripslashes($row['referral_to_program']);
	$otherUni = stripslashes($row['other_inst']);
	$tmpProgs = $row['cross_dept_progs'];
	$crossDeptProgsOther = $row['cross_dept_progs_other'];
	$permission = $row['records_permission'];
	
}

$sql = "select
id,
title,
author,
forum,
citation,
url,
status
from publication where application_id=".$appid . " order by id";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['title']);
	array_push($arr, $row['author']);
	array_push($arr, $row['forum']);
	array_push($arr, $row['citation']);
	array_push($arr, $row['url']);
	array_push($arr, $row['status']);
	array_push($pubs, $arr);
}
$sql = "select id,
name,
amount,
status,
applied_date,
award_date,
duration
from fellowships where application_id=".$appid;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['amount']);
	array_push($arr, $row['status']);
	array_push($arr, formatUSdate2($row['applied_date'],'-','/'));
	array_push($arr, formatUSdate2($row['award_date'],'-','/'));
	array_push($arr, $row['duration']);
	array_push($fellows, $arr);
}

function getExperience($type=1)
{
	global $appid;
	$ret = array();
	$sql = "select datafile_id,experiencetype, moddate,size,extension from experience
	inner join datafileinfo on datafileinfo.id =  experience.datafile_id
	where application_id=".$appid." and experiencetype=".$type;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['experiencetype']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($ret, $arr);
	}
	return $ret;
}


function getRecommenders($appid)
{
	global $buckley;
	$ret = array();
	$letter = "";
	//RETRIEVE USER INFORMATION
	$sql = "SELECT 
	recommend.id,
	recommend.rec_user_id,
	recommend.datafile_id,
	recommend.submitted,
	recommend.recommendtype,
	users.title,
	users.firstname,
	users.lastname,
	users.email,
	users_info.address_perm_tel, 
	lu_users_usertypes.id as uid,
	users_info.company,
	reminder_sent_count,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM recommend
	left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
	left outer join users on users.id = lu_users_usertypes.user_id
	left join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
	where application_id=".$appid. " order by recommend.id";
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['rec_user_id']);
		array_push($arr, $row['firstname']);
		array_push($arr, $row['lastname']);
		array_push($arr, $row['title']);
		array_push($arr, $row['company']);
		array_push($arr, $row['email']);
		array_push($arr, $row['address_perm_tel']);
		array_push($arr, $row['recommendtype']);
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($arr,$row['reminder_sent_count']);
		array_push($arr, 0);//ARRAY INFO
		array_push($ret, $arr);
	}
	
	$sql = "select buckleywaive from application where id=".$appid;
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$buckley = $row['buckleywaive'];
	}
	return $ret;
}
$myRecommenders = getRecommenders($appid);

function getMyAreas($id, $appid)
{
	$ret = array();
	$sql = "SELECT 
	interest.id,
	interest.name
	FROM 
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.program_id=".$id." and lu_application_programs.application_id=".$appid . " order by lu_application_interest.choice" ;

	$result = mysql_query($sql)	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}


/* Get Advisors CompBio */

function getAdvisorsCompBio( $appid ){
$sql = "select advisor_user_id, name from lu_application_advisor where application_id=".$appid . " order by id" ;
$result = mysql_query($sql) or die(mysql_error());
$myAdvisors = array();
$k=0;
while($row = mysql_fetch_array( $result ))
{
echo "K: " .$k ."\n<br>";
	if($k == 0)
	{
echo "ad_id: " . $row['advisor_user_id'] . "<br>";
echo "ad_id: " . $row['name'] . "<br>";
		if($row['advisor_user_id'] == "")
		{
			$advisor1 = $row['name'];
echo "YES";
		}else
		{
			$advisor1 = $row['advisor_user_id'];
		}
echo "ADVISOR 0: ".$advisor1."<BR>\n";
	}
	if($k == 1)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor2 = $row['name'];
		}else
		{
			$advisor2 = $row['advisor_user_id'];
		}
	}
	if($k == 2)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor3 = $row['name'];
		}else
		{
			$advisor3 = $row['advisor_user_id'];
		}
	}
	if($k == 3)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor4 = $row['name'];
		}else
		{
			$advisor4 = $row['advisor_user_id'];
		}
	}
	$k++;
}

}


?>
<html>
<head>
<title>Application of <?=$email?></title>
<link href="../css/app2.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor=#ffffff topmargin=5 leftmargin=10>

<!-- BIG TABLE -->
<table width=740 border=0 bordercolor=#000000  cellpadding=0 cellspacing=0>

<tr>
<td colspan=3>
<table border=0  cellpadding=0 cellspacing=0 width=100% bgcolor=#C5C5C5>
<!-- NAME TABLE -->

  <tr>
    <td rowspan=2 valign=left><font size="+1"><?=$title ." ".$fName ." ". $mName ." ". $lName ?></font></td>
    <td valign=middle><strong><?
      $filePath=getFilePath(2, 1, $uid, $umasterid, $appid);
      if ($filePath !== null) {?>
        <a target=new href=<?=$filePath?> >Resume</a></font> 
      <? } ?>
    </strong></td>

    <td valign=middle><strong><?
      $filePath=getFilePath(4, 1, $uid, $umasterid, $appid);
      if ($filePath !== null) {?>
        <a target=new href=<?=$filePath?> >Statement of Purpose</a></font> 
      <? } ?>
    </strong></td>
  </tr>
  <tr>
    <td valign=middle><strong><?
    $ex = getExperience(1);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href ="<?=getFilePath(5, 1, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Research Experience</a><?
    }
   ?></strong>&nbsp;</td>

    <td valign=middle><strong><?
    $ex = getExperience(2);
    for($i = 0; $i < count($ex); $i++)
    {
     ?><a href ="<?=getFilePath(5, 2, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Industry Experience</a><?
    }
    ?></strong>&nbsp;</td>
  </tr>
  <tr><td colspan=3>&nbsp</td></tr>
  
<!-- END NAME TABLE -->
</table>
</td>
</tr>


<tr>
<td colspan=3>
<table border=1 cellpadding=0 cellspacing=0 width=100%>
<!-- PROGRAMS TABLE -->

<?
for($i = 0; $i < count($myPrograms); $i++)
{
	?>

	<tr>
	<td width=250><strong><font size=-1><?=$myPrograms[$i][1] . " ".$myPrograms[$i][2] . " ".$myPrograms[$i][3] ?></font></strong.</td>
	<td width=489>&nbsp;
	<? 
	$myAreas = getMyAreas($myPrograms[$i][0],$appid);
	for($i = 0; $i< count($myAreas); $i++)
	{
		echo $myAreas[$i][1];
		if($i < count($myAreas)-1 )
		{
			echo ", ";
		}
	} 
	?>
	</td>
	</tr>
	<?
}
?>
<!-- END PROGRAMS TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100%>
<!-- ADDRESS TABLE -->


<tr>
<td width=369 bgcolor=c5c5c5 align=center><strong>Current Address</td>
<td width=370 bgcolor=c5c5c5 align=center><strong>Permanent Address</td>
</tr>

<tr>
<td width=369 align=left valign=top><Psmall>
<?
echo $street1;
if($street2 != "")
{
	echo "<br>".$street2;
}
if($street3 != "")
{
	echo "<br>".$street3;
}
if($street4 != "")
{
	echo "<br>".$street4;
}
echo "<br>".$city;
if($state != "")
{
	echo ", ".$state;
}
echo " ".$postal;
if($country != "")
{
	echo "<br>".$country;
}
if($tel != "")
{
	echo "<br>Tel: ".$tel;
}
if($telMobile != "")
{
	echo "<br>Mobile: ".$telMobile;
}
if($homepage != "")
{
	echo "<br><a href='".$homepage."' target='_blank'>".$homepage . "</a>";
}
if($curResident != NULL && $curResident != "")
{
	echo "<br>PA Resident: " . $curResident;
}
?></td>

<td width=370 align=left valign=top>
<?
echo "&nbsp;".$streetP1;
if($streetP2 != "")
{
	echo "<br>&nbsp;".$streetP2;
}
if($streetP3 != "")
{
	echo "<br>&nbsp;".$streetP3;
}
if($streetP4 != "")
{
	echo "<br>&nbsp;".$streetP4;
}
echo "<br>&nbsp;".$cityP;
if($stateP != "")
{
	echo ", ".$stateP;
}
echo " ".$postalP;
if($countryP != "")
{
	echo "<br>&nbsp;".$countryP;
}

?></td>
</tr>


<!-- END ADDRESS TABLE -->
</table>
</td>
</tr>


<tr>
<td colspan=3>
<table border=1  cellpadding=0 cellspacing=0 width=100%>
<!-- BIO TABLE -->

<tr bgcolor=c5c5c5>
 <td align=center width=280> <strong>Biographical Info</td>
 <td align=center width=459><strong>Immigration and Ethnic Info</td>
</tr>

<tr>
 <td align=center width=280>
 <table border=1 cellpadding=0 cellspacing=0>
  <tr>
   <td width=100 align=right><strong>Gender &nbsp; </td>
   <td width=179>&nbsp;<?=$gender?></td>
  </tr>

  <tr>
   <td width=100 align=right><strong>Date of Birth &nbsp;</td>
   <td width=179>&nbsp;<?=formatUSDate($dob)?></td>
  </tr>


  <tr>
   <td width=100 align=right><strong>Email &nbsp;</td>
   <td width=179>&nbsp;<?=$email?></td>
  </tr>
 </table>
 </td>

 <td width=459 align=left width=459 valign=top>
 <table border=1 cellpadding=0 cellspacing=0>
  <tr>
   <td width=100 align=right><strong>Citizenship</td>
   <td width=359 >&nbsp;<?=$countryCit?></td>
  </tr>

  <tr>
   <td width=100 align=right><strong>Ethnic &nbsp; </td>
   <td ><Psmall>&nbsp;<?=$ethnicity?></td>
  </tr>

  <tr>
   <td width=100 align=right>&nbsp; </td>
   <td><Psmall>&nbsp;</td>
  </tr>
 </table></td>
</tr>

<!-- END BIO TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=0 cellspacing=0 width=100%>
<!-- UNIV TABLE -->

<? for($i = 0; $i < count($myInstitutes); $i++){
if($myInstitutes[$i][1] != ""){
	$title = "";
	switch($myInstitutes[$i][16])
	{
		case "1":
			$title = "Undergraduate";
		break;
		case "2":
			$title = "Graduate";
		break;
		case "3":
			$title = "Additional";
		break;
	
	}//end switch
	?>

	<tr>
	<td bgcolor=c5c5c5 width=220><strong><?=$title?> College/University</td>
	<td width=520><strong>&nbsp;<?=$myInstitutes[$i][1]?> <?	
              $filePath=getFilePath(1, $myInstitutes[$i][0], $uid, $umasterid, $appid, $myInstitutes[$i][15]);
              if ($filePath !== null) {?>
              <a target=new href=<?=$filePath?> >(Transcript)</a></font> 
              <? } ?>
        </td>
	</tr>
	
	 <tr>
	  <td colspan=3>
	  <table border=1 cellpadding=0 cellspacing=0 width=100%>
	   <tr>
	    <td width=50 align=right><strong>Degree</strong>&nbsp;</td>
	    <td width=295>&nbsp;<?=$myInstitutes[$i][5]?></td>
	    <td width=125 align=right><strong><? if($myInstitutes[$i][16] == 3){ ?> 
		Date Graduated/Left 
		<? } else { ?> Date Graduated <? } ?></strong>&nbsp;</td>
	    <td width=95>&nbsp;<?=$myInstitutes[$i][3]?></td>
	    <td width=75 align=right><strong>GPA Overall&nbsp;</strong>

	    <td width=95>&nbsp;<?=$myInstitutes[$i][11]?> / <?=$myInstitutes[$i][13]?>		</td>
	   </tr>
	

	   <tr>
	    <td width=50 align=right><strong>Major</strong>&nbsp;</td>
	    <td width=517 colspan=3>&nbsp;<?=$myInstitutes[$i][6]?></td>
	    <td width=75 align=right>&nbsp;<strong>GPA Major</strong>&nbsp;</td>
	    <td width=95>&nbsp;<?=$myInstitutes[$i][12]?>	/ <?=$myInstitutes[$i][13]?>                </td>
           </tr>

	  </table>	
          </td>
	 </tr>
	<?
} }//end for myInstitutes
?>

<!-- END UNIV TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=0 cellspacing=0 width=100%>
<!-- SCORES TABLE -->

    <tr>
      <td width="33%" valign="top"><? for($i = 0; $i < count($grescores); $i++){ ?>
        <table border=1 cellpadding=0 cellspacing=0>
          <tr>

            <td colspan="5" align=center bgcolor="c5c5c5"><strong>GRE General Test</strong><?
              $filePath=getFilePath(6, $grescores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target=new href=<?=$filePath?> >(Scores)</a></font> 
              <? } ?>
              </td>

          </tr>
          <tr>
            <td width=146 align=right><strong>Test Date&nbsp;</strong></td>
            <td colspan=2>&nbsp;<?=$grescores[$i][1] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Verbal&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][2] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][3] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Quantitative&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][4] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][5] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Analytical Writing&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][6] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][7] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Analytical&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][8] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][9] ?></td>
          </tr>
        </table>
        <? } ?></td>

      <td width="33%" valign="top"><? for($i = 0; $i < count($gresubjectscores); $i++){
	 ?>
        <table border=1 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="5" align=center bgcolor="c5c5c5"><strong>GRE Subject Test</strong><?
              $filePath=getFilePath(8, $grescores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target=new href=<?=$filePath?> >(Scores)</a></font> 
              <? } ?>
              </td>
          </tr>
          <tr>
            <td width=163 align=right><strong>Test Date</strong></td>
            <td>&nbsp;<?=$gresubjectscores[$i][1] ?></td>
          </tr>
          <tr>
            <td width=163 align=right><strong>
              &nbsp;<?=$gresubjectscores[$i][2] ?>
            </strong></td>
            <td width=80>&nbsp;<?=$gresubjectscores[$i][3] ?></td>
          </tr>
        </table>
        <?  } ?></td>

      <td valign="top"><? for($i = 0; $i < count($toefliscores); $i++){ 
 if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
	{?>
     
        <table border=1 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="6" align=center bgcolor="c5c5c5"><strong>TOEFL IBT</strong><?
              $filePath=getFilePath(7, $toefliscores[$i][0], $uid, $umasterid, $appid, $toefliscores[$i][11]);
              if ($filePath !== null) {?>
              <a target=new href=<?=$filePath?> >(Scores)</a></font> 
              <? } ?>
              </td>

          </tr>
          <tr>
            <td width=65 align=right><strong>Test Date&nbsp;</strong></td>
            <td width=55>&nbsp;<?=$toefliscores[$i][1]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Reading&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][4]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Listening&nbsp;</strong></td>
            <td valign=middle>&nbsp;<?=$toefliscores[$i][3]?></td>
            <td width=89 align=right><strong>Total Score</strong> &nbsp;</td>
            <td width=49 align=left>&nbsp;<?=$toefliscores[$i][6]?></td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Speaking&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][2]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Writing&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][5]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
        </table>
        <? } } ?>
        <? for($i = 0; $i < count($toeflcscores); $i++){
	if($toeflcscores[$i][1] != "00/0000" && $toeflcscores[$i][1] != "")
	{ ?>

        <table border=0 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="6" align=center bgcolor="c5c5c5"><strong>TOEFL CBT</strong><?
              $filePath=getFilePath(9, $toeflcscores[$i][0], $uid, $umasterid, $appid, $toeflcscores[$i][11]);
              if ($filePath !== null) {?>
              <a target=new href=<?=$filePath?> >(Scores)</a></font> 
              <? } ?>
              </td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Test Date&nbsp;</strong></td>
            <td width=55>&nbsp;<?=$toeflcscores[$i][1]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Listening&nbsp;</strong></td>
            <td>&nbsp;<?=$toeflcscores[$i][3]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Structure/Writing&nbsp;</strong></td>
            <td valign=middle><?=$toeflcscores[$i][2]?></td>
            <td width=89 align=right><strong>Total Score</strong> &nbsp;</td>
            <td width=49 align=left>&nbsp;<?=$toeflcscores[$i][6]?></td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Reading&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][4]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Essay Rating &nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][5]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
        </table>
        <? } } ?>
        <? for($i = 0; $i < count($toeflpscores); $i++){
	if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
	{  ?>
    
        <table border=0 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="6" align=center bgcolor="c5c5c5"><strong>TOEFL PBT</strong><?
              $filePath=getFilePath(10, $toeflpscores[$i][0], $uid, $umasterid, $appid, $toeflpscores[$i][11]);
              if ($filePath !== null) {?>
              <a target=new href=<?=$filePath?> >(Scores)</a></font> 
              <? } ?>
              </td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Test Date&nbsp;</strong></td>
            <td width=55>&nbsp;<?=$toeflpscores[$i][1]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Section 1 &nbsp;</strong></td>
            <td>&nbsp;<?=$toeflpscores[$i][2]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Section 2 &nbsp;</strong></td>
            <td valign=middle><?=$toeflpscores[$i][3]?></td>
            <td width=89 align=right><strong>Total Score</strong> &nbsp;</td>
            <td width=49 align=left>&nbsp;<?=$toeflpscores[$i][6]?></td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Section 3 &nbsp;</strong></td>
            <td>&nbsp;<?=$toeflpscores[$i][4]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td align=right><strong>TWE Score&nbsp;</strong></td>
            <td>&nbsp;<?=$toeflpscores[$i][5]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
        </table>
        <? } } ?>

<!-- END SCORES TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100%>
<!-- RECOMMEND TABLE -->

      <tr>
        <td bgcolor="c5c5c5"><strong>Recommenders</strong></td>
        <td bgcolor="c5c5c5"><strong>Title / Affiliation </strong></td>
        <td bgcolor="c5c5c5"><strong>Email / Phone </strong></td>
        <td bgcolor="c5c5c5"><strong>Waive<br>Buckley</strong></td>
      </tr>
      <? for($i = 0; $i < count($myRecommenders); $i++){ 
	  if($myRecommenders[$i][2] != ""){
	  ?>
	  <tr>
        <td>
		<? if($myRecommenders[$i][9] != "")
		{
			?>
		<a href="<?=getFilePath(3, $myRecommenders[$i][0], $uid, $umasterid, $appid, $myRecommenders[$i][9])?>"><?=$myRecommenders[$i][2]?> <?=$myRecommenders[$i][3]?></a><?
		}else
		{
			echo $myRecommenders[$i][2]." " .$myRecommenders[$i][3];
		} ?>&nbsp;		
		</td>
        <td><?=$myRecommenders[$i][4]?> - <?=$myRecommenders[$i][5]?>&nbsp;</td>
        <td><?=$myRecommenders[$i][6]?> - <?=$myRecommenders[$i][7]?>&nbsp;</td>
        <td><? if($buckley === 1){ echo "Yes";}else{ echo "No";} ?></td>
      </tr>
	  <? } } ?>

<!-- END RECOMMEND TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100%>
<!-- PUBLICATION TABLE -->

  <tr><td colspan=4 bgcolor=c5c5c5 align=center><strong>Publications</strong></td></tr>
  <tr>
    <td valign="top" bgcolor=#C5C5C5><strong>Title:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Author(s):</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Name of Journal or Conference:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Status:</strong></td>
  </tr>
  <? for($i = 0; $i < count($pubs); $i++){ ?>
  <tr>
    <td valign="top">
            <? if($pubs[$i][5] != "") { 
			$header = "";
			if(strstr($pubs[$i][5], "http") === false)
			{
				$header = "http://";
			}
			?>
			<a href = "<?=$header.$pubs[$i][5] ?>" target="_blank"><?=$pubs[$i][1] ?></a>
			<? }else{ ?>
			<?=$pubs[$i][1] ?>
			<? } ?>		  </td>
    <td valign="top">
            <?=$pubs[$i][2] ?>&nbsp;</td>
    <td valign="top">
            <?= $pubs[$i][3] ?>&nbsp;</td>
    <td valign="top">
            <?= $pubs[$i][6] ?>&nbsp;</td>
  </tr>
   
  <? }//END PUBS LOOP ?>

<!-- END RECOMMEND TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100% class="tblItem">
<!-- FELLOWSHIP TABLE -->

  <tr><td colspan=6 bgcolor=c5c5c5 align=center><strong>Fellowships</strong></td></tr>
  <tr>
   <td valign="top" bgcolor=#C5C5C5><strong>Name</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Amount/Year</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Duration (years)</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Applied Date </strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Award Date</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Status:</strong></td>
  </tr>
  <? for($i = 0; $i < count($fellows); $i++){ ?>
  <tr>
   <td valign="top"><p><?= $fellows[$i][1]?>
    <br>
   </td>
   <td>$
     <?= $fellows[$i][2] ?></td>
   <td valign="top"><p>
     <?= $fellows[$i][6] ?>
     <br>
   </td>
   <td valign="top"><?= $fellows[$i][4] ?>&nbsp;</td>
   <td valign="top"><?= $fellows[$i][5] ?>&nbsp;</td>
   <td valign="top"><?= $fellows[$i][3] ?></td>
  </tr>
  <? } //END FELLOWS LOOP ?> 

<!-- END FELLOW TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100%>
<!-- MISC/SUPP TABLE -->

  <tr>
    <td  valign="top"><strong>Women@IT Fellowship:</strong> <?=$wFellow?></td>
    <td  valign="top"><strong>PIER:</strong> <?=$pier?></td>
    <td  valign="top"><strong>Sending Portfolio (HCII):</strong> <?=$port?>&nbsp;&nbsp;
    <strong>URL:</strong>&nbsp <?
    if ($portLink != NULL || $portLink != "" ) { ?>
      <a href=http://<?=$portLink?> target=_BLANK><?=$portLink?></a>
    <?}?>
  </tr>

<!-- END MISC/SUPP TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100% clas="tblItem">
<!-- COMPBIO TABLE TABLE -->

<tr><td colspan=3 align=center bgcolor=#C5C5C5><strong> Advisors </strong></td></tr>
<!-- ADVISORS -->
<tr>
  <td colspan=3 width=100%>
  <?
    getAdvisorsCompBio($appid);
    if($advisor1 != ""){echo $advisor1."<br>";}
    if($advisor1 != ""){echo $advisor2."<br>";}
    if($advisor1 != ""){echo $advisor3."<br>";}
    if($advisor1 != ""){echo $advisor4;}
  ?>
  </td>
</tr>

<!-- END COMPBIO TABLE -->
</table>
</td>
</tr>

<tr>
<td colspan=3>
<table border=1  cellpadding=1 cellspacing=0 width=100% clas="tblItem">
<!-- CNBC TABLE -->
<tr>
  <td bgcolor="#c5c5c5"><strong>Other Institutes </strong></td>
  <td bgcolor="#c5c5c5"><strong>Other Programs </strong></td>
</tr>

<tr>
  <td><?=$otherUni?></td>
  <td><?
	if($tmpCrossDeptProgsId != "")
	{
		$crossDeptProgs = $tmpCrossDeptProgsId;
	}
	if(!is_array($crossDeptProgs))
	{
		$crossDeptProgs = explode(",",$crossDeptProgs);
	}
	for($i = 0; $i < count($crossDeptProgs); $i++)
	{
		/*
		for($j = 0; $j < count(); $j++)
		{
		
		}
		*/
	}	
	?></td>
  </tr>

<!-- END CNBC TABLE -->
</table>
</td>
</tr>


</table>

</body>
</html>
