<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$name = "";
$abbrev = "";
$id = -1;
$requirement = array();
$requirements = array();

if(isset($_POST['txtdegreeId']))
{
	$id = htmlspecialchars($_POST['txtdegreeId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$abbrev = htmlspecialchars($_POST['txtAbbrev']);
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update degree set name='".$name."', short='".$abbrev."' where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
		}
		else
		{
			$sql = "insert into degree(name, short)values('".$name."', '".$abbrev."')";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}

		mysql_query("DELETE FROM lu_degrees_applicationreqs WHERE degree_id=".$id)or die(mysql_error().$sql);

		$vars = $_POST;
		$itemId = -1;
		foreach($vars as $key => $value)
		{
			$arr = split("_", $key);
			if(strstr($key, 'chkRequirement') !== false)
			{
				$tmpid = $arr[1];
				if($itemId != $tmpid || $tmpType != $arr[0])
				{
					$itemId = $tmpid;
					$tmpType=$arr[0];
					//echo $arr[0];
					if($arr[0] == "chkRequirement")
					{
						$sql = "insert into lu_degrees_applicationreqs(appreq_id, degree_id)values(".$itemId.",".$id.")";
						mysql_query($sql)or die(mysql_error().$sql);
						//echo $sql;
					}

				}
			}
		}//END FOR




		header("Location: degrees.php");
	}
}
//get requirements
$sql = "select id,short from applicationreqs order by name";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, str_replace("<br>"," ", $row['short']));
	array_push($requirements, $arr);
}

$sql = "select id,appreq_id from lu_degrees_applicationreqs  where degree_id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['appreq_id']);
	//array_push($arr, $row['appreq_id']);
	array_push($requirement, $arr);
}


$sql = "SELECT id,name,short FROM degree where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$abbrev = $row['short'];

}
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Edit Degree<!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<span class="errorSubtitle"><?=$err?></span>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtdegreesId" type="hidden" id="txtdegreeId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true); ?>        </td>
      </tr>
      <tr>
        <td width="200" align="right"><strong>Abbreviation:</strong></td>
        <td class="subtitle"><span class="tblItem">
          <? showEditText($abbrev, "textbox", "txtAbbrev", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>
        </span></td>
      </tr>
      <tr>
        <td align="right"><strong>Degree Requirements:</strong> </td>
        <td >
          <? showEditText($requirement, "checklist", "chkRequirement", $_SESSION['A_allow_admin_edit'],false, $requirements); ?>
       </td>
      </tr>
      <tr>
        <td width="200" align="right">&nbsp;</td>
        <td >
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
