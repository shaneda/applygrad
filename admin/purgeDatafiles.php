<?php
/*
* purgeDatafiles.php : clean up orphan datafile directories from the applyweb db.
*/

ini_set('memory_limit', '128M');
set_time_limit(600);
include_once '../inc/db_connect.php';
include_once '../inc/session_admin.php';
include '../classes/DB_Applyweb/class.DB_Applyweb.php';

if ($_SESSION['A_usertypeid'] != 0) {
    echo 'You are not authorized to purge datafiles.';
    exit;
}

$oldFileRoot = realpath($datafileroot);
$newFileRoot = $oldFileRoot . 'Purge';

/*
* Get all GUIDs in the DB, preferably after running admin/purgeApplications.php. 
*/
$DB_Applyweb = new DB_Applyweb();
$query = "SELECT users.guid, users.id
            FROM users
            INNER JOIN lu_users_usertypes ON users.id = lu_users_usertypes.user_id
            WHERE lu_users_usertypes.usertype_id = 5
            AND users.guid IS NOT NULL AND users.guid != ''";
$guidRecords = $DB_Applyweb->handleSelectQuery($query, 'guid');
$dbGuids = array_keys($guidRecords);

/*
* Loop all the GUID directories in the datafile root directory.
* If the directory does not have a matching DB GUID, it is an orphan. 
*/
$orphans = array();
if ($dh = opendir($datafileroot)) {
    while (($filename = readdir($dh)) !== false) { 
        $oldFilepath = $oldFileRoot . '/' . $filename;
        if ( is_dir($oldFilepath) && $filename != '.' && $filename != '..' ) {
            if ( !in_array($filename, $dbGuids) ) {
                $orphans[] = $filename;     
            }
        }
    }
    closedir($dh);
}
$orphanCount = count($orphans);

// Echo queries when testing; execute them when not. 
$commit = filter_input(INPUT_GET, 'commit', FILTER_VALIDATE_INT);
$doPurge = filter_input(INPUT_GET, 'doPurge', FILTER_VALIDATE_INT);
ob_start();
if ($doPurge) {
    
    if ( !file_exists('purge') ) {
        mkdir('purge', 0775);    
    }
    $reportTime = time();
    $reportFile = 'purge/datafilePurge' . date('Y-m-d-H-i-s', $reportTime) . '.txt';
    $fileHandle = fopen($reportFile, 'w');
    if (!$commit) {
        fwrite($fileHandle, "DRY RUN: ");    
    }
    fwrite($fileHandle, "Datafile Purge " . date('Y-m-d H:i:s', $reportTime) ."\n\n");
    fwrite($fileHandle, 
        "The following directories were moved from " . $oldFileRoot . " to " . $newFileRoot . ":\n\n");

    foreach ($orphans as $guid) {
        
        $oldFilepath = $oldFileRoot . '/' . $guid;
        $newFilepath = $newFileRoot . '/' . $guid;
        
        if ($commit) {
        
            if ( !file_exists($newFileRoot) ) {
                mkdir($newFileRoot, 0775);    
            }
            rename($oldFilepath, $newFilepath);    
        
        } else {
            
            echo "mv " . $oldFilepath . " " . $newFilepath ."<br/>\n";
        }
        
        fwrite($fileHandle, $guid . "\n");
    }

    /*
    fwrite($fileHandle, "\nThe following applicants did not have a guid directory:\n\n");
    foreach ($guidRecords as $guid => $guidRecord) {
        if ( !file_exists($datafileroot . '/' . $guid) ) {
            fwrite($fileHandle, "users.id:\t" . $guidRecord['id'] . "\n");
            fwrite($fileHandle, "users.guid:\t" . $guid . "\n\n");       
        }
    }
    */
    
    fclose($fileHandle);
}
$dryRunOutput = ob_get_contents();
ob_end_clean();

?>
<html>
<head>
    <title>Purge Datafiles</title>
</head> 
<body>

<?php
if ($doPurge) {

    if (!$commit) {
        echo "<p>DRY RUN</p>";
    }
    
    echo <<<EOB
    <p>{$orphanCount} orphan datafile directories moved to {$newFileRoot}</p>
    <a href="{$reportFile}" target="_blank">View Report</a>
    <br/><br/>
    <a href="purgeDatafiles.php">&laquo; Back</a>
    <br/><br/>
    {$dryRunOutput}
    
EOB;

} else {

    echo <<<EOB

    <form action="" method="get">
        <input type="hidden" name="doPurge" value="1" />
        <input type="radio" name="commit" value="0" checked="checked" /> Dry Run
        <input type="radio" name="commit" value="1" /> Commit
        <br/>
        <input type="submit" value="Purge Orphan Directories" />    
    </form>
    <p>There are {$orphanCount} orphaned datafile directories in {$oldFileRoot}:</p>
    <ul>
EOB;

    foreach ($orphans as $guid) {
        echo '<li>' . $guid . '</li>';
    }
    
    echo '</ul>';
}
?>

</body>
</html>