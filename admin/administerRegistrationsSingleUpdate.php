<?php
// Include the config class, which also has the 
// session and db includes.
include("../inc/config.php");

// Start session to get user.id
session_start();

// Include the data access classes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";

include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
include '../classes/class.RegistrationFeePaymentManager.php';
include '../classes/class.RegistrationFeePayment.php';

include "../classes/DB_Applyweb/Table/class.DB_PeriodApplication.php";
include "../classes/DB_Applyweb/Table/class.DB_LuApplicationPrograms.php";
include "../classes/DB_Applyweb/Table/class.DB_Users.php";
include "../classes/DB_Applyweb/Table/class.DB_UsersInfo.php";

$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include '../inc/specialCasesApply.inc.php';

$applicationId = NULL;
if ( isset($_REQUEST['applicationId']) ) {
    $applicationId = $_REQUEST['applicationId'];
}

$departmentId = filter_input(INPUT_GET, 'departmentId', FILTER_VALIDATE_INT);

$mode = $_REQUEST['mode'];

$returnString = "";

switch ($mode) 
{        
    case 'feeWaived':

        $waive = $_GET['feeWaived'];
        $paymentManager = new RegistrationFeePaymentManager($applicationId, $departmentId);
        $status = $paymentManager->saveFeeWaived($waive);
        $returnString = $status; 
        break;

    case 'paymentPaid':
    case 'newPayment':

        $paymentId = NULL;
        if ( isset($_GET['paymentId']) ) {
            $paymentId = $_GET['paymentId'];     
        }
        $paymentAmount = NULL;
        if ( isset($_GET['paymentAmount']) ) {
            $paymentAmount = $_GET['paymentAmount'];     
        }
        $paymentPaid = $_GET['paymentPaid'];
        $paymentType = $_GET['paymentType'];

        if ( ($paymentType == 1) || ($paymentType == 'Check') ) {
            $paymentType = '1';
        } elseif ( ($paymentType == 3) || ($paymentType == 'Voucher') ) {
            $paymentType = '3';
        } else {
            $paymentType = '2';
        }

        $paymentStatus = 'pending';
        if ($paymentPaid || $mode == 'newPayment') {
            $paymentStatus = 'paid';
        }   

        $payment = array(
            'id' => $paymentId, 
            'payment_type' => $paymentType, 
            'payment_amount' => $paymentAmount, 
            'payment_status' => $paymentStatus,
            'last_mod_user_id' => $_SESSION['A_usermasterid']
            );
        
        if ($mode == 'newPayment') 
        {
            $payment['payment_intent_date'] = date('Y-m-d H:i:s'); 
        }
        
        $paymentManager = new RegistrationFeePaymentManager($applicationId, $departmentId);
        $status = $paymentManager->savePayment($payment);

        $returnString = $status; 
        break;
    
    case 'paymentVoid':

        $paymentId = $_GET['paymentId'];
        $paymentManager = new RegistrationFeePaymentManager($applicationId, $departmentId);
        $paymentRecord = array(
            'id' => $paymentId,
            'payment_status' => 'void',
            'last_mod_user_id' => $_SESSION['A_usermasterid']
            );
        $status = $paymentManager->savePayment($paymentRecord);
        break;

    default:
        $returnString = "Unkown Case: " . $mode;
}


// Output the return data.
echo $returnString;
?>