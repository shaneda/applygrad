<?php
$startTime = microtime(TRUE);
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Applyweb_Table.php';

include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Program.php';
include '../classes/DB_Applyweb/class.DB_ProgramType.php';
include '../classes/DB_Applyweb/Table/class.DB_UnitRole.php';

include '../classes/DB_Applyweb/class.DB_Programs.php';
include '../classes/DB_Applyweb/class.DB_ProgramsUnit.php';
include '../classes/DB_Applyweb/class.DB_Department.php';
include '../classes/DB_Applyweb/class.DB_DepartmentUnit.php';
include '../classes/DB_Applyweb/class.DB_Domain.php'; 
include '../classes/DB_Applyweb/class.DB_DomainUnit.php';

include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_UnitPeriod.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/DB_Applyweb/Table/class.DB_PeriodUmbrella.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroup.php';
include '../classes/DB_Applyweb/Table/class.DB_ProgramGroupRole.php';

//include '../classes/DB_Applyweb/Table/class.DB_Users.php';

include '../classes/Unit/class.UnitBase.php';
include '../classes/Unit/class.Unit.php';
include '../classes/Unit/class.Program.php';

include '../classes/Period/class.PeriodBase.php';
include '../classes/Period/class.SubPeriod.php';
include '../classes/Period/class.UmbrellaPeriod.php';
include '../classes/Period/class.Period.php';

include '../classes/class.User.php';

require("HTML/QuickForm.php");
include '../classes/HTML_QuickForm/Period/class.NewProgramGroup_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.ProgramGroup_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.Period_QuickForm.php';
include '../classes/HTML_QuickForm/Period/class.PeriodProgram_QuickForm.php';

/*
* Handle the request variables.
* program_group_id
* search_user_id
*/
$unitId = NULL;
if ( isset($_REQUEST['unit_id']) ) {
    $unitId = filter_var($_REQUEST['unit_id'], FILTER_VALIDATE_INT);     
}
if ($unitId) {
    $unit = new Unit($unitId);
} else {
    $header = 'Location: https://' . $_SERVER['SERVER_NAME'];
    $header .= str_replace('unitRoles.php', 'units.php', $_SERVER['PHP_SELF']);
    header($header);
    exit;    
}

$roleId = 1;
if ( isset($_REQUEST['role_id']) ) {
    $roleId = filter_var($_REQUEST['role_id'], FILTER_VALIDATE_INT);
}

$removeUserId = NULL;
if ( isset($_REQUEST['remove_user_id']) ) {
    $removeUserId = filter_var($_REQUEST['remove_user_id'], FILTER_VALIDATE_INT);
}

$searchUserId = NULL;
if ( isset($_REQUEST['search_user_id']) ) {
    $searchUserId = filter_var($_REQUEST['search_user_id'], FILTER_VALIDATE_INT);
}

/* 
* Get the user's permissions.
*/
$user = new User();
$user->loadFromSession();
$userRole = $user->getUserRole();

$allAdminUnitIds = $rootAdminUnitIds = $user->getAdminUnits();
$rootUnitDescendantIds = array();
foreach ($rootAdminUnitIds as $rootUnitId) {    
    if (!$unitId) {
        $unitId = $rootUnitId;
        $unit = new Unit($unitId);
    }
    $rootUnit = new Unit($rootUnitId);
    $rootUnitDescendantIds = array_merge( $rootUnitDescendantIds, $rootUnit->getDescendantUnitIds() );
}
$allAdminUnitIds = array_merge($allAdminUnitIds, $rootUnitDescendantIds);

$allowAdmin = FALSE;
if ( ($userRole == 'Administrator' || $userRole == 'Super User') 
    && ( ($unitId && in_array($unitId, $allAdminUnitIds) ) || $edit)  ) 
{
    $allowAdmin = TRUE;        
}

/*
* If the user doesn't have admin rights for the unit, include the "view" template and exit.
*/
if (!$allowAdmin) { 
    include '../inc/tpl.unitRoles.php';
    exit;
}

$reviewRoles = array(
    2 => 'Admissions Chair',
    3 => 'Admissions Committee',
    4 => 'Faculty'
    );
    

$DB_UnitRole = new DB_UnitRole();
if ($unitId && $searchUserId && $roleId) {
    $record = array(
        'unit_id' => $unitId,
        'users_id' => $searchUserId,
        'role_id' => $roleId
        );
    $DB_UnitRole->save($record);    
}

if ($unitId && $removeUserId && $roleId) {
    $DB_UnitRole->delete($unitId, $removeUserId, $roleId);    
}

$roleUsers = array();
$roleUserQuery = "SELECT users.id AS users_id,
                    users.firstname,
                    users.lastname,
                    users.email
                    FROM unit_role 
                    INNER JOIN users ON unit_role.users_id = users.id
                    WHERE unit_role.unit_id = " . $unitId . "
                    AND unit_role.role_id = " . $roleId . "
                    ORDER BY users.lastname";
//DebugBreak();
$roleUsers = $DB_UnitRole->handleSelectQuery($roleUserQuery);

/*
* Set up a frozen program group form for display. 
*/
$programGroupForm = new ProgramGroup_QuickForm();
//$programGroupForm->handleSelf($programGroupId, $period, $unit);
$programGroupForm->removeElement('editProgramGroup');

/*
*  Include the "view" template.
*/
include '../inc/tpl.unitRoles.php'; 
?>