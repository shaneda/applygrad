<?PHP

/*
 * Name: adhocGroups.php
 * Created by: Paul Worona
 * December 2008
 *
 * Description:
 * Allow committee members to create adhoc groups for 
 * faculty for round 2
 */
 
// standard includes
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
 
// specific includes
include_once '../inc/class.AdhocGroupsData.php';
 include_once '../inc/class.AdhocGroupsDisplay.php';
 
// test id
// 2951

// base vars
$error = FALSE;
$success = FALSE;
$message = "";

// get application ID
if ( isset($_GET['aid']) ) $application_id = $_GET['aid'];
elseif ( isset($_POST['application_id']) ) $application_id = $_POST['application_id'];
else $application_id = FALSE;

// get department
if ( isset($_GET['department']) ) $department = $_GET['department'];
elseif ( isset($_POST['department']) ) $department = $_POST['department'];
else $department = FALSE;

// get uid
if ( isset($_GET['userid']) ) $userid = $_GET['userid'];
elseif ( isset($_POST['userid']) ) $userid = $_POST['userid'];
else $userid = FALSE;

// set referrer
$returnURL = $_SERVER['HTTP_REFERER'] . "&userid=" . $userid ; 


if ( $application_id and $department and $userid )
{
	// set the data object
	$data = new AdhocGroupsData( $application_id, $userid, $department );

	// Get the applicant info
	$applicant = $data->getApplicantData();
	$applicant = $applicant[0];

	if ( isset($_POST['submit']) ) 
	{
		$returnURL = $_POST['returnURL'];
		
		$FacultyUserID = $_POST['FacultyUserID'];
		$data->setFacultyData( $FacultyUserID );
		$assignedFaculty = $data->getAssignedFaculty();
		$unassignedFaculty = $data->getUnassignedFaculty();
		
		if ( $assignedFaculty )
		{
			// already a member of a group
			$error = TRUE;
			$message = "Already assigned to 
				{$assignedFaculty[0]['firstname']} {$assignedFaculty[0]['lastname']}";	
		} 
		elseif ( ! $unassignedFaculty )
		{
			// failed for some reason
			$error = TRUE;
			$message = "Failed to assign to faculty.
				<BR>Please try again.";	
		}
		else 
		{
			$groupname = $unassignedFaculty[0]['groupname'];
			$error = $data->assignApplicant($groupname);
			
			if ($error)
			{
				$message = "Failed to assign applicant.  Please try again.";
			} 
			else 
			{			
				//faculty 
				$faculty_firstname = $unassignedFaculty[0]['firstname'];
				$faculty_lastname = $unassignedFaculty[0]['lastname'];
				$faculty_email = $unassignedFaculty[0]['email'];
				
				// applicant
				$applicant_firstname = $applicant['firstname'];
				$applicant_lastname = $applicant['lastname'];
				
				// reviewer name
				$reviewer = $_SESSION['A_usertypename'];
				
				// message
				//$to = 'pworona@cs.cmu.edu';
				if ($dontsendemail) {
                    $to = 'pbergen+applyweb@cs.cmu.edu';    
                } else {
                    $to = $faculty_email;    
                }
                switch ($hostname)
                {
                    case "APPLY.STAT.CMU.EDU":  
                        $from = "scsstats@cs.cmu.edu";
                        break;
                    case "APPLYGRAD-INI.CS.CMU.EDU":  
                        $from = "scsini@cs.cmu.edu";
                        break;
                    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                        $from = "scsdiet@cs.cmu.edu";
                        break;
                    default:
                        $from = "applygrad@cs.cmu.edu";
                 }
                
				$subject = 'Apply Grad: Application to review';
				$headers = "From: $from \r\n";
	    		$headers .= "X-Mailer: php";
				$body = "$reviewer has determined that you might be interested in this candidate:\r\n";
				$body .= "$applicant_firstname $applicant_lastname\r\n\r\n";
                
                switch ($hostname)
                {
                    case "APPLY.STAT.CMU.EDU":  
                        $body .= "Please go to https://apply.stat.cmu.edu/admin and log in using Shibboleth";
                        $envelopeFrom = '-f scsstats@cs.cmu.edu';
                        break;
                    case "APPLYGRAD-INI.CS.CMU.EDU":  
                        $body .= "Please go to https://applygrad-ini.cs.cmu.edu/admin and log in using Shibboleth";
                        $envelopeFrom = '-f scsini@cs.cmu.edu';
                        break;
                    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                        $body .= "Please go to https://applygrad-dietrich.cs.cmu.edu/admin and log in using Shibboleth";
                        $envelopeFrom = '-f scsdiet@cs.cmu.edu';
                        break;
                    default:
                        $body .= "Please go to https://applygrad.cs.cmu.edu/admin and log in using Shibboleth";
                        $envelopeFrom = '-f applygrad@cs.cmu.edu';
                 }      
				
				$success = mail($to, $subject, $body, $headers, $envelopeFrom);
				
				if ( ! $success )
				{
					$message = "Applicant was assigned, but email to $to failed";
				} 
				else 
				{
					$success = TRUE;
					$message = "Applicant was successfully assigned.";	
				}
			}
		}
	}
	
	// get faculty info (assigned and unassigned)
	$data->setFacultyData();
	$assignedFaculty = $data->getAssignedFaculty();
	$unassignedFaculty = $data->getUnassignedFaculty();
}


$display = new AdhocGroupsDisplay( $application_id, $userid, $department );
$display->printHeader( $returnURL );

if ( ! $application_id ) 
{
	$message = "Missing application ID";
	$display->printErrorMessage( $message );
}
elseif ( ! $department ) 
{
	$message = "Missing Department";
	$display->printErrorMessage( $message );
}
elseif ( ! $userid ) 
{
	$message = "Missing User ID";
	$display->printErrorMessage( $message );
}
else
{
	if ( $error ) $display->printErrorMessage( $message );
	elseif ( $success) $display->printSuccessMessage( $message );
	$display->printApplicantInfo( $applicant );	
	$display->printAssignFacultyForm( $unassignedFaculty, $returnURL );
	$display->printAssignedFacultyList( $assignedFaculty );
}

$display->printFooter();
	
?>


