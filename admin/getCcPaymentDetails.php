<?php
header("Cache-Control: no-cache");

include("../inc/config.php");

// TEMPORARY for testing
$db = 'gradAdmissions2008Test';
$db_host = 'banshee.srv.cs.cmu.edu';
$db_password = 'onlyread';
$db_username = 'readonly';

include "../classes/DB_Applyweb/class.DB_Applyweb.php";
require_once("Structures/DataGrid.php");

$paymentId = filter_input(INPUT_GET, 'paymentId', FILTER_VALIDATE_INT);
if (!$paymentId) {
    exit;
}

$DB_Applyweb = new DB_Applyweb();

$transactionQuery = "SELECT * 
    FROM cc_transaction 
    WHERE payment_id = " . $paymentId;
$transactionRecords = $DB_Applyweb->handleSelectQuery($transactionQuery);
$transactionDatagrid = new Structures_DataGrid();
$transactionDatagrid->bind($transactionRecords , array(), 'Array');

$summaryQuery = "SELECT * 
    FROM cc_transaction_summary 
    WHERE payment_id = " . $paymentId;
$summaryRecords = $DB_Applyweb->handleSelectQuery($summaryQuery);
$summaryDatagrid = new Structures_DataGrid();
$summaryDatagrid->bind($summaryRecords , array(), 'Array');

$detailQuery = "SELECT * 
    FROM cc_transaction_detail 
    WHERE payment_id = " . $paymentId;
$detailRecords = $DB_Applyweb->handleSelectQuery($detailQuery);
$detailDatagrid = new Structures_DataGrid();
$detailDatagrid->bind($detailRecords , array(), 'Array');
?>

<br/>
<b>Transaction Records</b>
<br/>
<?php $transactionDatagrid->render(); ?>

<br/><br/>
<b>Summary Records</b>
<br/>
<?php $summaryDatagrid->render(); ?>

<br/><br/>
<b>Detail Records</b>
<br/>
<?php $detailDatagrid->render(); ?>