<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$domains = array();
$departments = array();
$department = array();
$domain = "-1";

if(isset($_POST['lbDomain']))
{
	$domain = htmlspecialchars($_POST['lbDomain']);
}else
{
	if(isset($_GET['id']))
	{
		$domain = htmlspecialchars($_GET['id']);
	}
}

if(isset($_POST['btnSubmit']))
{
	//SAVE DATA
	$vars = $_POST;
	$itemId = -1;

	mysql_query("DELETE FROM lu_domain_department WHERE domain_id=".$domain)
	or die(mysql_error().$sql);
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'chkDepartment') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			$itemRank = intval($_POST["txtRank_".$itemId]);
			if( $itemId > -1 )
			{
				$sql = "insert into lu_domain_department(domain_id, department_id, rank)values(".$domain.",".$itemId.", ".$itemRank.")";
				mysql_query($sql)
				or die(mysql_error().$sql);
				//echo $sql;
			}//END IF
		}//END IF

	}//END FOR

}

//PULL DATA FOR SELECTED domain
$sql = "select department.id,department.name, lu_domain_department.rank from lu_domain_department
inner join department on department.id = lu_domain_department.department_id
where domain_id=".$domain;
$result = mysql_query($sql) or die(mysql_error().$sql);
//echo $sql;
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['rank']);
	array_push($department, $arr);
}

// domains
$sql = "SELECT
id,name
FROM domain
order by name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($domains, $arr);
}

// departments
$sql = "SELECT
id, name
FROM department
order by name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row[1]);
	array_push($departments, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Domain Assignments';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Domain Assignments</span><br />
<br />

	Domain:<br />
	<? showEditText($domain, "listbox", "lbDomain", $_SESSION['A_allow_admin_edit'], true, $domains); ?>
	<input name="btnRefresh" type="submit" class="tblItem" id="btnRefresh" value="Refresh" />
	<br />
	Departments:<br />
	<table border="0" cellspacing="1" cellpadding="1">
	<tr>
	<td><strong>Department Name</strong></td>
	<td><strong>Sort Rank</strong></td>
	</tr>
      <? for($i = 0; $i < count($departments); $i++){ ?>
	  <tr>
        <td><?
		$selected = "";
		$rank = "";
		for($j = 0; $j < count($department); $j++)
		{
			//echo $department[$j][0] ." ". $departments[$i][0];
			if($department[$j][0] == $departments[$i][0])
			{
				$selected = 1;
				$rank = $department[$j][2];
				break;
			}
		}
		showEditText($selected, "checkbox", "chkDepartment_".$departments[$i][0], $_SESSION['A_allow_admin_edit'], true);
		echo $departments[$i][1];
		?></td>
        <td><? showEditText($rank, "textbox", "txtRank_".$departments[$i][0], $_SESSION['A_allow_admin_edit'],false); ?></td>
      </tr>
	  <? } ?>
    </table>

	<br />
	<br />
	<input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="Submit" />
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>