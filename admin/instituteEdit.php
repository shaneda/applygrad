<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

$err = "";
$name = "";
$id = -1;
if(isset($_POST['txtschoolId']))
{
	$id = htmlspecialchars($_POST['txtschoolId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName'], ENT_NOQUOTES, 'UTF-8');
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($err == "")
	{                                        
		if($id > -1)
		{
			$sql = "update institutes set name = '" . addslashes(trim($name)) 
                . "', users_id = " . $_SESSION['A_usermasterid'] . " where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			echo $sql . "<br>";
		}
		else
		{
			$sql = "insert into institutes (name, users_id) values ('"
                . addslashes(trim($name)) . "', " . $_SESSION['A_usermasterid'] . ")";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}
		header("Location: institutes.php");
	}
}
$sql = "SELECT id,name FROM institutes where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Institutes';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<form accept-charset="utf-8" id="form1" action="" accept-charset="utf-8" method="post">
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Institute</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtinstitutesId" type="hidden" id="txtschoolId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>
        </td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>