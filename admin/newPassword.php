<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$email = "";
$guid = "";
$passUpdated = false;
$allowContinue = true;
$recommender = false;
if(isset($_GET["r"]))
{
	$recommender = true;
	
}
if(isset($_GET['email']) && isset($_GET['id']))
{
	$email = addslashes($_GET['email']);
	$guid = addslashes($_GET['id']);
	$sql = "SELECT email, guid from users where email = '".$email."' and guid='".$guid."'";
	$result = mysql_query($sql)or die(mysql_error());
	$guid = "";
	while($row = mysql_fetch_array( $result )) 
	{
		$email = $row['email'];
		$guid = $row['guid'];
	}
	if($email == "")
	{
		$err .= "Invalid email ".$_GET['email'].".<br>";
		$allowContinue = false;
	}
}else
{
	if(isset($_POST['txtEmail']))
	{
		$email = htmlspecialchars($_POST['txtEmail']);
		$pass = htmlspecialchars($_POST['txtPass']);
		$pass2 = htmlspecialchars($_POST['txtPass2']);
		$guid = htmlspecialchars($_POST['txtGuid']);
		if($email == "" && $email != "*")
		{
			$err .= "Email is required.<br>";
		}
		if($pass == "")
		{
			$err .= "Password is required.<br>";
		}
		if($pass2 == "")
		{
			$err .= "Please confirm your password.<br>";
		}
		if($_POST['txtPass'] != $_POST['txtPass2'])
		{
			$err .= "Passwords do not match.<br>";
		}
		if($recommender != true)
		{
			$pass = sha1($pass);
		}
		if($err == "")
		{
			//$newguid = makeGuid();
			$sql = "update users set password='".$pass."' where email='".$email."' and guid = '".$guid."'";
			mysql_query( $sql) or die(mysql_error());
			if(mysql_affected_rows() > 0)
			{
				$passUpdated = true;
				
			}
	
			
		}
	}else
	{
		$err .= "No parameters specified<br>";
		$allowContinue = false;
	}
}
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" -->
		<form action="newPassword.php" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" --><span class="title">Password Reset </span><!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><span class="tblItem">
			<?=$err?>
			<?
			if($passUpdated == true)
			{
				?>
            	Your password has been changed. <strong><a href="index.php<? if($recommender == true){echo "?r=1";} ?>">Click here to login</a></strong>.
				<?
			}?>
            </span>
			<? if($passUpdated == false && $allowContinue == true){ ?>
            <table width="300" border="0" cellpadding="4" cellspacing="2" class="tblItem">
              <tr>
                <td align="right"><input name="txtEmail" type="hidden" id="txtEmail" value="<?=$email?>" />
                  <input name="txtGuid" type="hidden" id="txtGuid" value="<?=$guid?>" />
                New Password: </td>
                <td><? showEditText("", "password", "txtPass", true); ?></td>
              </tr>
              <tr>
                <td align="right">Confirm New Password: </td>
                <td><? showEditText("", "password", "txtPass2", true); ?></td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td><span class="subtitle">
                  <? showEditText("Submit", "button", "btnSubmit", true); ?>
                </span></td>
              </tr>
            </table>
			<? } ?>
            <!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
