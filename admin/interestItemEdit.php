<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$name = "";
$id = -1;
if(isset($_POST['txtInterestId']))
{
	$id = htmlspecialchars($_POST['txtInterestId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update interest set name='".$name."' where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			echo $sql . "<br>";
		}
		else
		{
			$sql = "insert into interest(name)values('".$name."')";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}
		header("Location: interests.php");
	}
}
$sql = "SELECT id,name FROM interest where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Interest Item';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Interest Item</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtInterestId" type="hidden" id="txtInterestId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true, null, true, 60); ?>
        </td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>