<?php 
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

/*
* data, logic 
*/
$deptId = -1;
$deptName = "";
$round = 1;
if(isset($_GET['id']))
{
	$deptId = intval($_GET['id']);
}
if(isset($_GET['r']))
{
	$round = intval($_GET['r']);
}
$allReviewers = array();
$groups = array();
$reviewers = array();


//GET ALL GROUPS AVAILABLE
$sql = "select revgroup.id, revgroup.name,department.name as dept
from revgroup
left outer join department on department.id = revgroup.department_id ";
if($deptId > -1)
{
	$sql .= " where department_id=".$deptId;
}
else
{
	if($_SESSION['A_usertypeid'] != 0)
	{
        $departmentCount = count($_SESSION['A_admin_depts']); 
		for($i = 0; $i < $departmentCount; $i++)
		{
			if ($_SESSION['A_admin_depts'][$i] == NULL) {
                continue;
            }
            if($i == 0)
			{
				$sql .= "WHERE (";
			}
            else
			{
				if ($i == 1 && $_SESSION['A_admin_depts'][0] == NULL) { 
                    $sql .= "WHERE (";    
                } else {
                    $sql .= " OR ";
                }
			}
			$sql .= " department_id = ".$_SESSION['A_admin_depts'][$i];
            if ($i == ($departmentCount - 1)) {
                $sql .= ")";
            }
		}
	}
}
if ($_SESSION['A_usertypeid'] == 3) {
    $sql .= " AND group_type = 2";
}
$sql .= " order by name";

$result = mysql_query($sql) or die(mysql_error());
$i = 0;
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name'] . " - " . $row['dept']);
	array_push($groups, $arr);
	if($i > 0)
	{
	$deptName .= "<br>" ;
	}
	$deptName .= $row['dept'];
	$i++;
}


//GET ALL REVIEWERS AVAILABLE
$sql = "select lu_users_usertypes.id, firstname, lastname, usertypes.name as usertype, department.name as dept
from lu_users_usertypes
INNER JOIN users on users.id = lu_users_usertypes.user_id
INNER JOIN usertypes on usertypes.id = lu_users_usertypes.usertype_id 
INNER JOIN lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
INNER JOIN department on department.id = lu_user_department.department_id
where (lu_users_usertypes.usertype_id = 1 or lu_users_usertypes.usertype_id = 2 
or lu_users_usertypes.usertype_id = 3 OR lu_users_usertypes.usertype_id = 20) ";
if($deptId > -1)
{
	$sql .= " and lu_user_department.department_id = ". $deptId;
}
else
{
	if($_SESSION['A_usertypeid'] != 0)
	{
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " and (";
		}
		$useOr = FALSE;
        for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
		{
			if ($_SESSION['A_admin_depts'][$i] == NULL) {
                continue;
            }
            if($useOr)
			{
				$sql .= " or ";
			}
			$sql .= " lu_user_department.department_id = ".$_SESSION['A_admin_depts'][$i];
            $useOr = TRUE;
		}
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " ) ";
		}
	}
}
if ($_SESSION['A_usertypeid'] == 3) {
    $sql .= " AND (usertypes.id = 3 OR usertypes.id = 20)";
}
$sql .= " order by users.lastname, users.firstname, usertypes.name";

$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    if ($round == 1 && $row['usertype'] == 'Faculty') {
        continue;
    }
    $arr = array();
	array_push($arr, $row['id']);
	array_push($arr, "<strong>" . $row['lastname']. ", ".$row['firstname'] . "</strong><br><em>" . $row['usertype']. ", ". $row['dept']."</em>");
	array_push($allReviewers, $arr);
}


//update
if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				mysql_query("DELETE FROM lu_reviewer_groups WHERE id=".$itemId)	or die(mysql_error().$sql);
			}//END IF
		}//END IF DELETE
	}
}

$errorMsg = '';
$checkedVals = array();
if(isset($_POST['btnAdd']))
{
    if ( !isset($_POST['lbGroup']) ||  !$_POST['lbGroup'] ) {
        
        $errorMsg = 'You must select a group.';
        foreach($vars as $key => $value) {
            if(strstr($key, 'chkRev') !== false ) {
                $arr = split("_", $key); 
                $checkedVals[] = $arr[1];    
            }    
        }
    
    } else {
    
        foreach($vars as $key => $value)
		{
			$arr = split("_", $key);
			if(strstr($key, 'chkRev') !== false )
			{
				$tmpid = $arr[1];
				if($itemId != $tmpid || $tmpType != $arr[0])
				{
					$itemId = $tmpid;
					$tmpType=$arr[0];
					if($arr[0] == "chkRev")
					{
						$group = $_POST['lbGroup'];
						$doUpdate = true;
						$sql = "select id from lu_reviewer_groups where reviewer_id=".$itemId." and group_id=".$group." and round=".$round;
						$result = mysql_query($sql) or die(mysql_error());
						while($row = mysql_fetch_array( $result ))
						{
							$doUpdate = false;
						}
						if($doUpdate == true)
						{
							$sql = "insert into lu_reviewer_groups(reviewer_id,department_id, group_id, round)values(".$itemId.",".$deptId.",".$group.", ".$round.")";
							mysql_query($sql)or die(mysql_error().$sql);
						}
					}
				}
			}
		}//END FOR
        
    }
}

//GET REVIEWERS / GROUPS
$sql = "select
lu_reviewer_groups.id,
firstname,
lastname,
revgroup.name,
usertypes.name as usertype,
department.name as dept,
groupDept.name as groupDept

from lu_reviewer_groups
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_reviewer_groups.reviewer_id
left outer join usertypes on usertypes.id = lu_users_usertypes.usertype_id
left outer join users on users.id = lu_users_usertypes.user_id

left outer join revgroup on revgroup.id = lu_reviewer_groups.group_id
left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id

left outer join department on department.id = lu_user_department.department_id
left outer join department as groupDept on groupDept.id = revgroup.department_id
where round = ".$round . " ";
if($deptId > -1)
{
	$sql .= " and lu_user_department.department_id = ". $deptId;
}
else
{
	if($_SESSION['A_usertypeid'] != 0)
	{
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " and (";
		}
		$useOr = FALSE;
        for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
		{

            if ($_SESSION['A_admin_depts'][$i] == NULL) {
                continue;
            }
            if($useOr)
			{
				$sql .= " or ";
			}
			$sql .= " lu_user_department.department_id = ".$_SESSION['A_admin_depts'][$i];
            $useOr = TRUE;
		}
		if(count($_SESSION['A_admin_depts']) > 0)
		{
			$sql .= " ) ";
		}
	}
}
if ($_SESSION['A_usertypeid'] == 3) {
    $sql .= " AND group_type = 2";
}
$sql .=" order by revgroup.name, firstname";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{   
    $arr = array();
	array_push($arr, $row['id']);
	array_push($arr, "<strong>" . $row['firstname']. " ".$row['lastname'] . "</strong><br><em>" . $row['usertype']. ", ". $row['dept']."</em>");
	array_push($arr, $row['name'] . " - " . $row['groupDept']);
	array_push($reviewers, $arr);
}
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
if ($_SESSION['A_usertypeid'] == 3) {
    $pageTitle = 'Edit Ranking Groups';
} else {
    $pageTitle = 'Edit Committee Groups';
}
$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br />
<form id="form1" name="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><? echo $pageTitle; ?> (Round <?=$round?>)</span><br />
<br />

<table width="750" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="50%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td class="tblHead">Available Committee/faculty Members </td>
        </tr>
      <tr>
        <td>Assign Checked users to group
           <? showEditText("", "listbox", "lbGroup", $_SESSION['A_allow_admin_edit'],true,$groups,true); ?>
           <span class="subtitle">
           <? showEditText("Add", "button", "btnAdd", $_SESSION['A_allow_admin_edit']); ?>
           </span>
        <?php
        if ($errorMsg) {
            echo '<br/><span class="errorSubtitle">' . $errorMsg . '</span>';
        }
        ?>
        </td>
        </tr>
      <tr>
        <td>
		 <? 
         //showEditText("", "checklist", "chkRev", $_SESSION['A_allow_admin_edit'],true,$allReviewers,true);
         showEditText($checkedVals, "checklist", "chkRev", $_SESSION['A_allow_admin_edit'],true,$allReviewers,true); 
         ?>		  </td>
        </tr>
    </table></td>
    <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tr>
        <td colspan="2" class="tblHead">Round <?=$round?> Groups </td>
      </tr>
	  <?
	  $tmpGrp = "";
	  for($i = 0; $i < count($reviewers); $i++){

	  if($reviewers[$i][2] != $tmpGrp){
	  $tmpGrp = $reviewers[$i][2];
	  ?>
      <tr>
        <td colspan="2">
		<? if($i > 0 ){ ?>
		<hr size="1" />
		<? } ?>
		<strong><?=$reviewers[$i][2]?></strong></td>
      </tr>
	  <? }//end if ?>
      <tr>
        <td width="88%"><?=$reviewers[$i][1]?></td>
        <td width="12%" align="right"><? showEditText("X", "button", "btnDelete_".$reviewers[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	  <? if($i == count($reviewers)-1){ ?>
      <tr>
        <td colspan="2"><hr size="1" /></td>
      </tr>
	  <?

	  } } ?>
    </table>
      <br /></td>
  </tr>
</table>
<br />
	<br />
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>