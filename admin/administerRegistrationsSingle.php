<?php
header("Cache-Control: no-cache");

include("../inc/config.php");
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
include '../classes/class.RegistrationFeePaymentManager.php';
include '../classes/class.RegistrationFeePayment.php';

// Special cases.
include '../inc/specialCasesAdmin.inc.php';

// get the request arguments 
$applicationId = NULL;
if ( isset($_REQUEST['applicationId']) ) {
    $applicationId = $_REQUEST['applicationId'];
}
$page = $_GET['page'];

$unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);

$departmentId = filter_input(INPUT_GET, 'departmentId', FILTER_VALIDATE_INT);

switch($page) {

    
    case "payment":
    
        $paymentManager = new RegistrationFeePaymentManager($applicationId, $departmentId);
        include '../inc/tpl.administerRegistrationsSinglePayment.php';
        break;

    default:
    
        print "Admin case not found.";

}

function getProgramDepartmentId($programId)
{
    $query = "SELECT department_id FROM lu_programs_departments
        WHERE program_id = " . $programId . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        return $row['department_id'];
    }
    return NULL;    
}
?>