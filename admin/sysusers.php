<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

// PLB added scsUser, WebISO functions 6/30/09
include '../inc/sysusersEdit.inc.php';

// Include unit/period classes
define("CLASS_DIR", "../classes/");
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb_Table.php';
include '../inc/unitPeriodIncludes.inc.php';

/*
* Run through the main controller logic
*/

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
	header("Location: index.php");
}
$err = "";
$users = array();
$userTypes = array();
$usertype = "";
$lastname = "";
$firstname = "";

/*
* PLB added $formSubmitted and changed $usertype default 06/25/09.
* On the initial request, this makes the "usertype" drop down 
* default to "choose one" and, with the test on line 225,
* eliminates the default query/data display.
*/
$formSubmitted = TRUE;
if(!isset($_POST['btnSearch']))
{
	//$usertype = "5";
    $usertype = "";
    $formSubmitted = FALSE;
}

if(isset($_POST['lbUserType']))
{
	$usertype = $_POST['lbUserType'];
}
if(isset($_POST['txtLastname']))
{
	$lastname = addslashes($_POST['txtLastname']);
}
if(isset($_POST['txtFirstname']))
{
	$firstname = addslashes($_POST['txtFirstname']);
}

$vars = $_POST;
foreach($vars as $key => $value)
{
	if( strstr($key, 'btnDel') !== false )
	{
		// PLB added formSubmitted for delete 6/30/09.
        $formSubmitted = TRUE;
        
        $arr = split("_", $key);
		$itemId = $arr[1];

		$usermasterid = $itemId;
		$sql = "SELECT id from lu_users_usertypes where user_id=".$usermasterid;
		$result = mysql_query($sql) or die(mysql_error().$sql);
		while($row = mysql_fetch_array( $result ))
		{
			$itemId = $row['id'];
		}
        
		$sql = "DELETE FROM usersinst WHERE user_id=". $itemId;
		mysql_query($sql) or die(mysql_error().$sql);

		$sql = "DELETE FROM users_info WHERE user_id=". $itemId;
		mysql_query($sql) or die(mysql_error().$sql);

		$sql = "DELETE FROM lu_user_department WHERE user_id=". $itemId;
		mysql_query($sql) or die(mysql_error().$sql);

		$appid = -1;
		$sql = "SELECT id from application where user_id=".$itemId;
		$result = mysql_query($sql) or die(mysql_error().$sql);
		while($row = mysql_fetch_array( $result ))
		{
			$appid = $row['id'];
		}
		$sql = "DELETE FROM application WHERE id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM lu_application_advisor  WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM lu_application_appreqs  WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "select id FROM lu_application_programs WHERE application_id=". $appid;
		$result = mysql_query($sql) or die(mysql_error().$sql);
		while($row = mysql_fetch_array( $result ))
		{
			$sql = "DELETE FROM lu_application_interest WHERE app_program_id=". $row['id'];
			mysql_query($sql) or die(mysql_error(). $sql);
		}

		$sql = "DELETE FROM lu_application_programs WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM publication WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM experience WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM fellowships WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM toefl WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM grescore WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM review WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		//$sql = "DELETE FROM application WHERE user_id=". $itemId;
		//mysql_query($sql) or die(mysql_error());

		$sql = "DELETE FROM recommend WHERE application_id=". $appid;
				mysql_query($sql) or die(mysql_error(). $sql);
				
		$sql = "DELETE FROM lu_reviewer_groups WHERE reviewer_id=". $itemId;
		mysql_query($sql) or die(mysql_error(). $sql);
		
		$sql = "DELETE FROM lu_application_groups WHERE application_id=". $appid;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM lu_users_usertypes WHERE id=". $itemId;
		mysql_query($sql) or die(mysql_error(). $sql);

		$sql = "DELETE FROM users WHERE id=". $usermasterid;
		mysql_query($sql) or die(mysql_error(). $sql);
        
        // Delete users_remote_auth_string data.
        deleteRemoteAuthStrings($usermasterid);
    }
}//END FOR

//GET USER TYPES
$sql = "SELECT * FROM usertypes order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($userTypes, $arr);
}

//DebugBreak();
//GET DATA
$sql = "SELECT lu_users_usertypes.id,
users.id as umasterid,
users.firstname,
users.lastname,
users.email,
users_info.additionalinfo
FROM users
left outer join lu_users_usertypes on lu_users_usertypes.user_id = users.id
left outer join users_info on users_info.user_id = lu_users_usertypes.id  ";

if($_SESSION['A_usertypeid'] != 0)
{
	//get info about specific departments
	$sql .= " left outer join application on application.user_id = lu_users_usertypes.id
		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
		left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
		";

}
//$sql .= " where ( lu_users_usertypes.usertype_id > -2 or lu_users_usertypes.usertype_id = NULL)";
$sql .= " where  users.id > -1 ";

if($_SESSION['A_usertypeid'] != 0)
{
    // Allow administrators to find users already assigned to other departments
    if ($_SESSION['A_usertypeid'] != 1 || (isset($usertype) && $usertype != "")) {
	    if(isset($_SESSION['A_admin_depts']))
	    {
		    if(count($_SESSION['A_admin_depts'])>0)
		    {
			    $sql .= " and (";
			    for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
			    {
                    
                    /*
                    * Guess what? $_SESSION['A_admin_depts'][$i][0] is not a 2D array!
                    */
                    
                    // PLB added test/correction for null department value 
                    // from an "unassigned" role. 10/26/09
                    //if ($_SESSION['A_admin_depts'][$i][0] == NULL) {
                    if ($_SESSION['A_admin_depts'][$i] == NULL) {
                        continue;
                    }
                    
                    if($i > 0 && $i < count($_SESSION['A_admin_depts']) 
                        // && $_SESSION['A_admin_depts'][$i-1][0] != NULL)
                        && $_SESSION['A_admin_depts'][$i-1] != NULL)
				    {
					    $sql .= " or ";
				    }
				    //$sql .= " lu_programs_departments.department_id =".$_SESSION['A_admin_depts'][$i][0] ;
				    //$sql .= " or lu_user_department.department_id =".$_SESSION['A_admin_depts'][$i][0] ;
                    $sql .= " lu_programs_departments.department_id =".$_SESSION['A_admin_depts'][$i] ;
                    $sql .= " or lu_user_department.department_id =".$_SESSION['A_admin_depts'][$i] ;
                }
			    if($usertype == 6 && ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1))
			    {
				    $sql .=" or lu_user_department.department_id IS NULL ";
			    }
			    $sql .= ") ";
		    }
	    }
    }

}
if($usertype != "")
{
	$sql .=" and lu_users_usertypes.usertype_id=".$usertype;
}
if($firstname != "")
{
	$sql .=" and users.firstname like '%".$firstname."%' ";
}
if($lastname != "")
{
	$sql .=" and users.lastname like '%".$lastname."%' ";
}

$sql .=" group by users.id order by lastname,firstname";

/*
* PLB added formSubmitted test 06/25/09. 
* No query will be executed and no data will be displayed without form submission.
*/

if ($formSubmitted) {
    $result = mysql_query($sql)
    or die(mysql_error()."<br>".$sql);
    //echo $sql;
    while($row = mysql_fetch_array( $result ))
    {
	    $arr = array();
	    array_push($arr, $row['id']);
	    array_push($arr, stripslashes($row['firstname']));
	    array_push($arr, stripslashes($row['lastname']));
	    array_push($arr, $row['email']);
	    array_push($arr, $row['umasterid']);
	    array_push($arr, $row['additionalinfo']);
	    array_push($users, $arr);
    }

    // Set array with all admin period ids.
    $adminPeriodIds = array();
    //DebugBreak();
    $adminDeptIds = array_unique($_SESSION['A_admin_depts']);
    foreach ($adminDeptIds as $deptId) {
        if ($deptId) {
            $unit = getDepartmentUnit($deptId);
            if ($unit) {
                $unitPeriodIds = $unit->getPeriodIds(); 
                $unitParent = $unit->getParentUnit();
                if ($unitParent) {
                    $parentPeriodIds = $unitParent->getPeriodIds();
                    $unitPeriodIds = array_merge( $unitPeriodIds, $parentPeriodIds );    
                }
                $adminPeriodIds = array_merge($unitPeriodIds, $adminPeriodIds);     
            } else {
                //echo $deptId .'-';
            }
        }  
    }
    
    $applications = getApplications();
    //DebugBreak();
}

function getRoles($id)
{
	$sql = "select
	lu_users_usertypes.id,
	lu_users_usertypes.usertype_id,
	department.name as deptname,
	usertypes.name as typename
	from users
	left outer join lu_users_usertypes on lu_users_usertypes.user_id = users.id
	left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
	left outer join department on department.id = lu_user_department.department_id
	left outer join usertypes on usertypes.id = lu_users_usertypes.usertype_id
	where users.id=".$id." order by department.name, usertypes.name";
	$result = mysql_query($sql)
	or die(mysql_error().$sql);
	$ret = array();
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['usertype_id']);
		array_push($arr, $row['deptname']);
		array_push($arr, $row['typename']);
		array_push($ret, $arr);
	}
	return $ret;
}


function getApplications($luUsersUsertypesId = NULL) {

    global $adminPeriodIds;
    $records = array();
    
    if ( count($adminPeriodIds) == 0 ) {
        return $records;
    } 
    
    $query = "SELECT 
                application.user_id AS lu_users_usertypes_id,
                application.id AS application_id, 
                period.period_id,
                period_umbrella.umbrella_name,
                GROUP_CONCAT(DISTINCT department.id) AS department_id,
                GROUP_CONCAT(DISTINCT department.name) AS department_name
                FROM application
                LEFT OUTER JOIN period_application
                    ON application.id = period_application.application_id
                LEFT OUTER JOIN period
                  ON period_application.period_id = period.period_id
                LEFT OUTER JOIN period_umbrella
                  ON period.period_id = period_umbrella.period_id
                LEFT OUTER JOIN lu_application_programs
                  ON application.id = lu_application_programs.application_id
                LEFT OUTER JOIN lu_programs_departments
                  ON lu_application_programs.program_id = lu_programs_departments.program_id
                LEFT OUTER JOIN department
                  ON lu_programs_departments.department_id = department.id";
    if ($luUsersUsertypesId) {
        $query .= " WHERE application.user_id = " . $luUsersUsertypesId;   
    } else {
        $query .= " WHERE 1=1";
    }           
    if ($_SESSION['A_usertypeid'] != 0) {
        $query .= " AND period_application.period_id IN (" . implode(',', $adminPeriodIds) . ")";
    }
    $query .= " GROUP BY application.id
                ORDER BY application.id, department.id";

    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        if ($luUsersUsertypesId) {
            $records[] = $row;    
        } else {
            $records[$row['lu_users_usertypes_id']][] = $row;
        }
    }
    return $records;
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'System Users';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br/>
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title" style="font-size: 18px;">System Users</span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><span class="errorSubtitle"><?=$err?></span>
	<strong><a href="sysusersEdit.php">Add New User</a></strong>	
    <!--
    <a href="sysusersBatchEdit.php"><strong>Batch Add Users</strong></a> 
	-->
    <br /><br />
	Filter results by:
	<table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>usertype</td>
        <td>Lastname</td>
        <td>Firstname</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><span class="tblItem">
          <? 
          $allowEdit = FALSE;
          if ($_SESSION['A_usertypeid'] == 0 
                || $_SESSION['A_usertypeid'] == 1
                || $_SESSION['A_allow_admin_edit']) 
          {
              $allowEdit = TRUE;
          }
          showEditText($usertype, "listbox", "lbUserType", $allowEdit,true, $userTypes); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText(stripslashes($lastname), "textbox", "txtLastname", $allowEdit); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText(stripslashes($firstname), "textbox", "txtFirstname", $allowEdit); ?>
        </span></td>
        <td><? showEditText("Search", "button", "btnSearch", $allowEdit); ?></td>
      </tr>
    </table>
	To edit the user's roles, click on the user's name<br />
	To access further information on the user's role, use the link in the Roles column <br />
	<table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr class="tblHead">
	  	<td>umasterid</td>
	  	<td>userid</td>
        <td>name</td>
        <td>email</td>
        <td>role(s)</td>
		<td>Info</td>
        <td>delete</td>
      </tr>
	  <?
	  $class = "tblItem";
	  for($i = 0; $i < count($users); $i++){
	  if($i % 2 == 0)
	  {
	  	 $class = "tblItem";
	  }else
	  {
	  	 $class = "tblItemAlt";
	  }

	  ?>
      <tr class="<?=$class?>">
	  <td><?=$users[$i][4]?></td>
	  <td><?=$users[$i][0]?></td>
        <td><a href="sysusersEdit.php?id=<?=$users[$i][4]?>"><?=$users[$i][2]?>, <?=$users[$i][1]?></a> </td>
        <td><?=$users[$i][3]?></td>
        <td>
		<div id="list"><ul>
		<?
		$roles = getRoles($users[$i][4]);
		$tmpDept = -1;
		for($j = 0; $j < count($roles); $j++)
		{
			$link = "#";
			switch($roles[$j][3])
			{
				case "Student":
					$link = "userroleEdit_student.php?id=".$users[$i][0];
				    break;
				case "Recommender":
					$link = "userroleEdit_recommender.php?id=".$users[$i][0];
				    break;
                // PLB added auto-login for SU 10/26/09
                case "Administrator":
                case "Admissions Chair":
                case "Admissions Committee":
                case "Faculty":
                case "Student Contact":
                case "Area Coordinator":
                    if ($_SESSION['A_usertypeid'] == 0) {
                        $link = "index.php?luuId=" . $roles[$j][0] . "&login=alias";    
                    }
                    break;
                default:
			}
			if($tmpDept == $roles[$j][2] )//SAME DEPT
			{
				if ($roles[$j][3] == 'Student') {
                    //$studentApplications = getApplications($roles[$j][0]);
                    $studentApplications = (isset($applications[$roles[$j][0]])) ? $applications[$roles[$j][0]] : array();  
                    foreach ($studentApplications  as $applicationRecord ) {
                        echo makeApplicationLink($applicationRecord);    
                    }   
                } else {
                    echo ", <a href='".$link."' target='_blank' title='id:".$roles[$j][0]."'>".$roles[$j][3]."</a>";    
                }
                
			}else
			{
				if($tmpDept != "")
				{
					echo "</em></li></ul></li>";//CLOSE OUT THE PREVIOUS ONE
				}
				if($roles[$j][2] != "")
				{
					echo " <li><strong>".$roles[$j][2].":</strong><ul><li><em>";
				}
                if ($roles[$j][3] == 'Student') {
                    if ($roles[$j][0] == 17230) {
                        //DebugBreak();
                    }
                    //$studentApplications = getApplications($roles[$j][0]);
                    $studentApplications = (isset($applications[$roles[$j][0]])) ? $applications[$roles[$j][0]] : array();
                    foreach ( $studentApplications as $applicationRecord ) {
                        $applicationLink = makeApplicationLink($applicationRecord);
                        echo $applicationLink;    
                    }    
                } else {
				    echo "<a href='".$link."' target='_blank' title='id:".$roles[$j][0]."'>".$roles[$j][3]."</a>";
				}
                $tmpDept = $roles[$j][2];
			}
		}
		?>
        </ul></div></td>
		<td><em><?=$users[$i][5]?></em></td>
        <td><? if($_SESSION['A_usertypeid'] == 0){ ?>
         <!-- PLB fixed javascript unquoted string arg error 6/29/30 -->
         <input name="btnDel_<?=$users[$i][4]?>" type="submit" class="tblItem" id="btnDel_<?=$users[$i][4]?>" value="X" onClick="return confirmSubmit('<?=$users[$i][3]?>')" />
			<? } ?>
		  </td>
      </tr>
	  <? }//end users loop ?>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<script LANGUAGE="JavaScript">
<!--
// Nannette Thacker http://www.shiningstar.net
function confirmSubmit(val)
{
var agree=confirm("Are you sure you wish to delete record "+val+"?");
if (agree)
    return true ;
else
    return false ;
}
// -->
</script>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function getDepartmentUnit($departmentId, $return = 'object') {
    
    if (!$departmentId) {
        return NULL;
    }
    
    $unitId = NULL;
    
    $query = "SELECT unit_id FROM department_unit 
                WHERE department_id = " . $departmentId;
    $query .= " LIMIT 1";
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $unitId = $row['unit_id'];
    }
    
    if ($unitId) {
        if ($return == 'object') {
            return new Unit($unitId);    
        } else {
            return $unitId;    
        }
    } else {
        return NULL;
    }
}

function makeApplicationLink($applicationRecord) {
    
    $link = 'Applicant: ';
    $departmentIds = explode(',', $applicationRecord['department_id']);
    $departmentNames = explode(',', $applicationRecord['department_name']);
    $i = 0;
    foreach($departmentIds as $departmentId) {
        if ( $_SESSION['A_usertypeid'] == 0 
            || in_array($departmentId, $_SESSION['A_admin_depts']) ) 
        {
            $linkHref = 'administerApplications.php?unit=department&unitId=' . $departmentId;
            $linkHref .= '&period=' . $applicationRecord['period_id'];
            $linkHref .= '&applicationId=' . $applicationRecord['application_id']; 
            $link .= '<a href="' . $linkHref . '" target="_blank">';
            $link .= $departmentNames[$i] . ', ' . $applicationRecord['umbrella_name'] . '</a><br/>';
            break;
        } else {
            $link .= $departmentNames[$i] . ', ' . $applicationRecord['umbrella_name'] . '<br/>';    
        }
        $i++;
    }
    
    return $link;
}
?>