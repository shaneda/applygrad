<?php
$startTime = microtime(TRUE);
ini_set('memory_limit', '128M');
header("Cache-Control: no-cache");

// Standard includes.
include "../inc/config.php";
include "../inc/session_admin.php";

// TEMPORARY for testing
/*
$db = 'gradAdmissions2008Test';
$db_host = 'banshee.srv.cs.cmu.edu';
$db_password = 'onlyread';
$db_username = 'readonly';
*/

// Page-specific includes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Domain.php"; 
include "../classes/DB_Applyweb/class.DB_Department.php";
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminList.php";  
include "../classes/DB_Applyweb/class.VW_AdminListBase.php";
include "../classes/class.CcPaymentListData.php";
include "../classes/Structures_DataGrid/class.CcPaymentList_DataGrid.php";

$unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
if (!$unit) {
    $unit = filter_input(INPUT_POST, 'unit', FILTER_SANITIZE_STRING);    
} 
if (!($unit == 'department' || $unit == 'domain')) {
    header('Location: home.php');
    exit;
}

$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
if (!$unitId) {
    $unitId = filter_input(INPUT_POST, 'unitId', FILTER_VALIDATE_INT);    
}
if (!$unitId) {
    header('Location: home.php');
    exit;    
}

$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);
if (!$periodId) {
    $periodId = filter_input(INPUT_POST, 'period', FILTER_VALIDATE_INT);    
}
if (!$periodId) {
    if ( isset($_REQUEST['period']) && is_array($_REQUEST['period']) ) {
        // Handle array of period ids.
        foreach($_REQUEST['period'] as $requestPeriod) {
            $periodId[] = intval($requestPeriod);     
        }
    } elseif ( $_SESSION['A_usertypeid'] == 0 ) {
        $periodId = NULL;
    } else {
        header('Location: home.php');
        exit;        
    }    
} 

$applicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT);
if (!$applicationId) {
    $applicationId = filter_input(INPUT_POST, 'applicationId', FILTER_VALIDATE_INT);    
} 

// Page size
if ( isset($_REQUEST['limit']) ) {
    $limit = $_REQUEST['limit'];
} else {
    $limit = 100;
}  

// Handle searchString and applicationId and set 
// the post-search "return to list" link accordingly.
$returnUrl = "administerCcPayments.php?unit=" . $unit . "&unitId=" . $unitId;
if ($periodId) {
    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids 
    if (is_array($periodId)) {
        foreach ($periodId as $id) {
            $returnUrl .= "&period[]=" . $id;    
        }   
    } else {
        $returnUrl .= "&period=" . $periodId;    
    }    
}
/*
if ($programId) {
    $returnUrl .= "&programId=" . $programId;    
}
*/
$returnLink = "";
$returnLinkOn = '<span class="return"><a href=" ';
$returnLinkOn .= $returnUrl . '" class="return" onClick="document.filterForm.submit(); return false;">';
$returnLinkOn .= 'Return to full list</a></span>';
$searchString = '';
$applicationId = NULL;
$luuId = NULL;
$showReturnLink = '';
if ( isset($_REQUEST['searchString']) ) {
    $searchString = $_REQUEST['searchString']; 
    $returnLink = $returnLinkOn;   
}
if ( isset($_REQUEST['applicationId']) ) {
    $applicationId = $_REQUEST['applicationId'];
}
if ( isset($_REQUEST['luuId']) ) {
    $luuId = $_REQUEST['luuId'];
}
if ( isset($_REQUEST['showReturnLink']) ) {
    $showReturnLink = $_REQUEST['showReturnLink'];    
}
if ( isset($_REQUEST['submitJumpTo']) || $showReturnLink == 'show' ) {
    $returnLink = $returnLinkOn;    
}

// Handle the filter-related request variables.
$feeStatus = "allFeeStatus";    // default paid or waived
if ( isset($_REQUEST['feeStatus']) ) {
    $feeStatus = $_REQUEST['feeStatus'];
}
$unpaidUnwaivedFeeSelected = $paidWaivedFeeSelected = $paidFeeSelected 
    = $waivedFeeSelected = $allFeeStatusSelected = '';
$feeStatusSelected = $feeStatus . 'Selected';
$$feeStatusSelected = "selected";

$awPaymentStatus = "allAw";
if ( isset($_REQUEST['awPaymentStatus']) ) {
    $awPaymentStatus = $_REQUEST['awPaymentStatus'];
}
$paidAwSelected = $voidAwSelected = $pendingAwSelected
    = $pendingPaidAwSelected = $allAwSelected = '';
$awPaymentStatusSelected = $awPaymentStatus . 'Selected';
$$awPaymentStatusSelected = "selected";

$ccpsStatus = "allCcps";
if ( isset($_REQUEST['ccpsStatus']) ) {
    $ccpsStatus = $_REQUEST['ccpsStatus'];
}
$paidCcpsSelected = $failedCcpsSelected = $pendingCcpsSelected 
    = $pendingPaidCcpsSelected = $refundedCcpsSelected = $allCcpsSelected = '';
$ccpsStatusSelected = $ccpsStatus . 'Selected';
$$ccpsStatusSelected = "selected";

// get the application period info
if ( isset($_REQUEST['period']) ) {

    $application_period_id = $_REQUEST['period'];
    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids 
    if (is_array($_REQUEST['period'])) {
        // The scs period will be first and the compbio period will be second
        $displayPeriodId = $_REQUEST['period'][1];    
    } else {
        $displayPeriodId = $_REQUEST['period']; 
    }
    
    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    if ($endDate == '') {
        $endDate = 'present';    
    }
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    if ($periodName) {
        $application_period_display = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $application_period_display = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $application_period_display = $periodDates;     
    }
    $application_period_display .= '<br/>';    

} else {
    $application_period_id = NULL;
    $start_date = NULL;
    $end_date = NULL;
    $application_period_display = "";
}

// Set the domain/department name
switch ($unit) {
    
    case 'domain':
    
        $dbUnit = new DB_Domain();
        break;
    
    case 'department':
    
        $dbUnit = new DB_Department();
        break;
    
    default:
        
        $dbUnit = NULL;   
}

if ($dbUnit) {
    $unitRecord = $dbUnit->get($unitId);
    $unitName = $unitRecord[0]['name'];    
} else {
    $unitName = '';    
}

/*
* GET THE DATA 
*/
$ccPaymentListData = new CcPaymentListData($unit, $unitId);
if ($feeStatus == 'allFeeStatus') {
    $applicationPaymentStatus = 'allPayment';
} else {
    $applicationPaymentStatus = str_replace('Fee', '', $feeStatus);    
}

$ccPaymentListArray = $ccPaymentListData->getData($periodId, $applicationId, $searchString,
    'all', 'all', $applicationPaymentStatus, 'all', 'all', 'all', 'all', $luuId);

$filteredPaymentArray = array();    
if (!$searchString && !($awPaymentStatus == 'allAw' && $ccpsStatus == 'allCcps')) {

    $filteredPaymentArray = filterData($ccPaymentListArray, $awPaymentStatus, $ccpsStatus);
    
} else {
    
    $filteredPaymentArray = $ccPaymentListArray;    
}

$dataGrid = new CcPaymentList_DataGrid($filteredPaymentArray, $limit);

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Administer CC Payments';

$pageCssFiles = array(
    '../css/jquery.autocomplete.css',
    '../css/administerApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.livequery.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/administerApplications.js',
    '../javascript/jquery.shiftclick.js'
    );
 
include '../inc/tpl.pageHeader.php';

// Render the view.
?>
<div style="margin: 10px;">

<table cellspacing="2" width="100%" border="0">
    <tr valign="top">
        <td align="left">

<span class="title">
<?php 
if ($unitName) {
    echo $unitName . '<br/>';    
}
echo $application_period_display; 
?> 
<span style="font-size: 18px;">
Administer Credit Card Payments
<?php
if (!$periodId && $_SESSION['A_usertypeid'] == 0) {
    echo ' (SU)';    
}
?>
</span>  
</span>
        </td>
        <td align="right" width="40%">
<div style="padding: 2px; width: auto; background: #F7F7F7; text-align: left;">
<form id="filterForm" name="filterForm" action="<?php echo $returnUrl; ?>" method="GET">
<input type="hidden" id="filterUnit" name="unit" value="<?php echo $unit; ?>" />
<input type="hidden" id="filterUnitId" name="unitId" value="<?php echo $unitId; ?>" />
<?php
if ($periodId) {    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
    if (is_array($periodId)) {
        foreach ($periodId as $id) {
            echo '<input type="hidden" id="filterPeriod" name="period[]" value="' . $id . '"/>';    
        }   
    } else {
        echo '<input type="hidden" id="filterPeriod" name="period" value="' . $periodId . '"/>';     
    }    
}    
?>
<input type="hidden" id="filterLimit" name="limit" value="<?php echo $limit; ?>" />
<input type="hidden" name="showReturnLink" value=""/>
<table border="0" cellspacing="0" cellpadding="2" width="99%">
    <tr valign="middle">
        <td align="right" id="feeStatusLabel" width="30%">App Fee Status:</td>
        <td align="left" colspan="2">
            <select class="filter" name="feeStatus">
                <option value="unpaidUnwaivedFee" <?php echo $unpaidUnwaivedFeeSelected; ?>>Unpaid / Unwaived</option>
                <option value="paidWaivedFee" <?php echo $paidWaivedFeeSelected; ?>>Paid / Waived</option>
                <option value="paidFee" <?php echo $paidFeeSelected; ?>>Paid</option>
                <option value="waivedFee" <?php echo $waivedFeeSelected; ?>>Waived</option>
                <option value="allFeeStatus" <?php echo $allFeeStatusSelected; ?>>No Filter</option>
            </select>
        </td>
    </tr>
    <tr valign="middle">
        <td align="right" id="awPaymentStatusLabel" width="30%">Payment Status:</td>
        <td align="left" width="35%">
            <select class="filter" name="awPaymentStatus">
                <option value="pendingAw" <?php echo $pendingAwSelected; ?>>Pending</option> 
                <option value="paidAw" <?php echo $paidAwSelected; ?>>Paid</option>
                <option value="pendingPaidAw" <?php echo $pendingPaidAwSelected; ?>>Pending or Paid</option>
                <option value="voidAw" <?php echo $voidAwSelected; ?>>Void</option>
                <option value="allAw" <?php echo $allAwSelected; ?>>No Filter</option>
            </select>
        </td>
        <td align="left">
            <a href="<?php echo $_SERVER['REQUEST_URI']?>" onClick="setNoPaymentFilter(); return false;">Full&nbsp;List&nbsp;w/&nbsp;Filter&nbsp;Off</a>
        </td>
    </tr>
    <tr valign="middle">
        <td align="right" id="ccpsStatusLabel" width="33%">CCPS Status:</td>
        <td align="left" width="35%">
            <select class="filter" name="ccpsStatus">
                <option value="failedCcps" <?php echo $failedCcpsSelected; ?>>Failed</option>
                <option value="paidCcps" <?php echo $paidCcpsSelected; ?>>Paid</option>
                <option value="pendingCcps" <?php echo $pendingCcpsSelected; ?>>Pending</option>
                <option value="pendingPaidCcps" <?php echo $pendingPaidCcpsSelected; ?>>Pending or Paid</option>
                <option value="refundedCcps" <?php echo $refundedCcpsSelected; ?>>Refunded</option>
                <option value="allCcps" <?php echo $allCcpsSelected; ?>>No Filter</option>
            </select>
        </td>
        <td align="left" width="30%">
            <input type="submit" name="submitFilter" value="Filter/Refresh Full List"/>
        </td>
    </tr>
</table>
</form>
</div> 

    </td>  
        </tr>

        <tr valign="bottom">       
        <td align="left">
<br/>

<form id="jumpToForm" name="jumpToForm" action="<?php echo $returnUrl; ?>" method="GET">   
    <b>Jump to applicant:</b><br/>
    <input type="text" id="suggest" name="searchString" size="40"/>
    <input type="hidden" id="luuId" name="luuId" />
    <input type="hidden" id="jumpToUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="jumpToUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <?php
    if ($periodId) {        
        // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod' . $id . '" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" class="jumpToPeriod" id="jumpToPeriod" name="period" value="' . $periodId . '"/>';     
        }  
    }
    ?>
    <input type="hidden" id="jumpToLimit" name="limit" value="<?php echo $limit; ?>"/>
    <input type="hidden" name="feeStatus" value="<?php echo $feeStatus; ?>"/>
    <input type="hidden" name="ccpsStatus" value="<?php echo $ccpsStatus; ?>"/>
    <input type="hidden" name="awPaymentStatus" value="<?php echo $awPaymentStatus; ?>"/>
    <input type="hidden" name="showReturnLink" value="show"/> 
    <input type="submit" name="submitJumpTo" value="Go" />    
</form>
        </td>

        <td>
<div style="padding: 2px; width: auto; background: #F7F7F7; text-align: left;">
<form id="updateForm" name="updateForm" method="POST">
<input type="hidden" id="updateUnit" name="unit" value="<?php echo $unit; ?>" />
<input type="hidden" id="updateUnitId" name="unitId" value="<?php echo $unitId; ?>" />
<?php
if ($periodId) {    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
    if (is_array($periodId)) {
        foreach ($periodId as $id) {
            echo '<input type="hidden" id="filterPeriod" name="period[]" value="' . $id . '"/>';    
        }   
    } else {
        echo '<input type="hidden" id="filterPeriod" name="period" value="' . $periodId . '"/>';     
    }    
}    
?>
<input type="hidden" id="updateLimit" name="limit" value="<?php echo $limit; ?>" />
<input type="hidden" name="showReturnLink" value=""/>
<input type="hidden" name="referer" value="<?php echo $_SERVER['SCRIPT_NAME']; ?>"/>
<input type="hidden" name="queryString" value="<?php echo $_SERVER['QUERY_STRING']; ?>"/>
<table border="0" cellspacing="0" cellpadding="2" width="99%">
    <tr valign="middle">
        <td align="right" id="updatePaymentsLabel" width="30%">Update checked as:</td>
        <td align="left" width="35%">
            <select name="updatePaymentsValue" id="updatePaymentsValue">
                <option value="">Select One</option>
                <option value="paid">Paid</option>
                <option value="void">Void</option>
            </select>
        </td>
        <td align="left" width="35%">
            <input type="button" id="updatePayments" name="updatePayments" value="Update Payments"/>
        </td>
    </tr>
</table>
</form>
</div> 
</td> 
        
    </tr>
</table>


<br/> 

<div style="clear:both; ">

<div style="text-align: left; float: left; width: 100%; margin-left: 5px; margin-bottom: 5px; padding: 0px;">
<?php echo $returnLink; ?>
</div>

<div class="paging">
<?php $dataGrid->render(DATAGRID_RENDER_PAGER); ?>
</div>

<div style="text-align: left; float: left; margin-left: 5px; margin-bottom: 5px; padding: 0px;">

<?php
// Display info about the record set being displayed
$recordCount = $dataGrid->getRecordCount(); 
if ($recordCount >= 1) {
    ?>
    <form id="pageSizeForm" name="pageSizeForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET"> 
    <?php
    $recordStart = $dataGrid->getCurrentRecordNumberStart();
    $recordEnd = $dataGrid->getCurrentRecordNumberEnd();
    echo "Record(s) <b>" . $recordStart . " - " . $recordEnd . " of " . $recordCount . "</b>"; 
    if ($searchString) {
        if (mb_detect_encoding($searchString) == 'UTF-8') {
            $displayString = utf8_decode($searchString);
        } else {
            $displayString = $searchString;
        }
        echo ' for <i>' . htmlentities($displayString) . "</i>";
    }
    // Menu for changing page size
    $limit100Selected = $limit200Selected = $limit500Selected = '';
    $limitSelected = 'limit' . $limit . 'Selected';
    $$limitSelected = "selected";
    ?>
    &nbsp;&nbsp;
    <select name="limit" onChange="document.pageSizeForm.submit(); return false;">
        <option value="100" <?php echo $limit100Selected; ?>>100 per page</option>
        <option value="200" <?php echo $limit200Selected; ?>>200 per page</option>
        <option value="500"<?php echo $limit500Selected; ?>>500 per page</option>
    </select>
    <input type="hidden" id="limitUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="limitUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <input type="hidden" id="limitCcpsStatus" name="ccpsStatus" value="<?php echo $ccpsStatus; ?>" />
    <input type="hidden" id="limitAwPaymentStatus" name="awPaymentStatus" value="<?php echo $awPaymentStatus; ?>"/>
    <?php
    if ($periodId) {      
        // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" id="limitPeriod" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" id="limitPeriod" name="period" value="' . $periodId . '"/>';     
        } 
    }
    ?>
    <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
    <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
    <input type="hidden" name="showReturnLink" value="<?php echo $showReturnLink; ?>"/>
    </form>
    <?php
         
} elseif ($recordCount == 0) {
    
    if ($searchString) {
        echo '<b>No matches found for ';
        echo  '<i>'. htmlentities( utf8_decode($searchString) ) . '</i></b>';   
    } else {
        echo '<b>No cc payments found for this period with these filter settings.</b>';
    }
}
?>
</div>

<br />
<div style="clear: left;">
<form name="paymentSelectionForm" id="paymentSelectionForm" action="updatePayments.php" method="POST">

<?php $dataGrid->render(); ?>

<input type="hidden" name="updatePaymentsValue" id="selectionUpdatePaymentsValue">
<input type="hidden" name="referer" value="<?php echo $_SERVER['SCRIPT_NAME']; ?>"/>
<input type="hidden" name="queryString" value="<?php echo $_SERVER['QUERY_STRING']; ?>"/>
</form>
</div>

<div class="paging">
<?php $dataGrid->render(DATAGRID_RENDER_PAGER); ?>
</div>

</div>

<script type="text/javascript" src="../javascript/jquery.shiftclick.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
    $('input.shiftclick').shiftClick();
    
    $('#selectAllPayments').click( function() {
        if ($(this).attr("checked")) {
            $('.paymentCheckbox').attr('checked', 'checked');    
        } else {
            $('.paymentCheckbox').attr('checked', '');    
        }
    });

    $('#updatePayments').click( function() { 
        
        if ($('#updatePaymentsValue').val() == '') {
            alert('You must select "paid" or "void" as an update value.');
            return false;
        }
        
        //$('.paymentCheckbox:input').clone().appendTo('#updateForm');
        
        var checkedCount = $('.paymentCheckbox:checked').size();
        if (checkedCount == 0) {
            alert('You must check one or more payments.');
            return false;           
        }
        
        var confirmUpdate = confirm('Update ' + checkedCount + ' payment records?');
        if (confirmUpdate == true) {
            $('#selectionUpdatePaymentsValue').val($('#updatePaymentsValue').val());
            $('#paymentSelectionForm').submit();    
        }
    });    
});

function setNoPaymentFilter() {
    document.filterForm.feeStatus.value = 'allFeeStatus';
    document.filterForm.awPaymentStatus.value = 'allAw';
    document.filterForm.ccpsStatus.value = 'allCcps';
    document.filterForm.submit();
    return false;
}
</script> 

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function filterData($unfilteredCcPaymentArray, $awPaymentStatus, $ccpsStatus) {

    $awStatusKey = array(
        'pendingAw' => array('pending'),
        'paidAw' => array('paid'),
        'pendingPaidAw' => array('pending', 'paid'),
        'voidAw' => array('void'),
        'allAw' => array('pending', 'paid', 'void')
    );
    
    $ccpsStatusKey = array(
        'pendingCcps' => array('pending'),
        'paidCcps' => array('paid in full'),
        'pendingPaidCcps' => array('pending', 'paid in full'),
        'failedCcps' => array('payment failed'),
        'refundedCcps' => array('payment refunded'),
        'allCcps' => array('pending', 'paid in full', 'payment failed', 'payment refunded')
    );

    $filteredCcPaymentArray = array();
    
    foreach ($unfilteredCcPaymentArray as $ccPaymentRecord) {

        if (in_array($ccPaymentRecord['payment_status'], $awStatusKey[$awPaymentStatus])
            && in_array($ccPaymentRecord['ccps_status'], $ccpsStatusKey[$ccpsStatus]))
        {
            $filteredCcPaymentArray[] = $ccPaymentRecord;    
        }
    }    
    
    return $filteredCcPaymentArray;
}
?>