<? 
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php'; 
include_once '../inc/specialCasesAdmin.inc.php'; 
// include_once '../inc/specialCasesApply.inc.php';

$id = -1;
$name = "";
$content = "";
$deptId = -1;
switch ($hostname)
{
    case "APPLY.STAT.CMU.EDU":  
        $sysemail = "scsstats@cs.cmu.edu";
        break;
    case "APPLYGRAD-INI.CS.CMU.EDU":  
        $sysemail = "scsini@cs.cmu.edu";
        break;
    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
        $sysemail = "scsdiet@cs.cmu.edu";
        break;
    case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
        $sysemail = "scsdesgn@cs.cmu.edu";
        break;
    default:
        $sysemail = "applygrad@cs.cmu.edu";
 }

$p = 0;
if(isset($_GET['id']))
{
	$id = htmlspecialchars($_GET['id']);
}
if(isset($_GET['p']))
{
	if($_GET['p'] == "1")
	{
		$p = 1;
	}
}

$departmentId = NULL;
if ( isset($_REQUEST['departmentId']) ) {
    $departmentId = $_REQUEST['departmentId']; 
} 

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
} 

$sql = "SELECT id,name,content, department_id, domain_id FROM content where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$content = $row['content'];
	$deptId = $row['department_id'];
    $domain = $row['domain_id'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SCS Applygrad</title>
<script type="text/javascript" language="JavaScript">
<!--
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to continue?");
if (agree)
    return true ;
else
    return false ;
}
// -->
</script>
</head>

<body><form action="" method="post" name="form1">

<?
//DO STUFF HERE TO PARSE THE MESSAGE
$vars = array();
switch($name)
{
	// PLB added two new cases for CSD
    case "Acceptance Letter":
    case "Acceptance Letter (Portugal)":
    case "Invitation Letter":
    case "Invitation Email":
        if (!isDesignDepartment($deptId)) {
		    $sql = "select
		    distinct
            application.id AS application_id,
		    firstname, lastname, email, address_cur_street1, gender,
		    address_cur_street2,
		    address_cur_street3,
		    address_cur_street4,
		    address_cur_city,
		    states.name as state,
		    address_cur_pcode,
		    countries.name as country,
		    GROUP_CONCAT( DISTINCT 
                IF (
                    (programs.id = 5 OR programs.id = 6)
                    AND application_decision.admission_program_id IS NOT NULL,
                    IF (
                        application_decision.admission_program_id = 5,
                        'Ph.D. in Language Technologies',
                        'M.S. in Language Technologies'     
                    ),
                    concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                )
            ) AS program,
		    lu_application_programs.faccontact,
		    lu_application_programs.stucontact
		    from  lu_users_usertypes
		    inner join users on users.id =  lu_users_usertypes.user_id
		    inner join users_info on users_info.user_id= lu_users_usertypes.id
		    left outer join countries on countries.id = address_cur_country
		    left outer join states on states.id = address_cur_state
		    INNER JOIN application on application.user_id = lu_users_usertypes.id";
            
            // PLB added period join 1/7/10
            if ($periodId) {
                $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
            }        
            
            $sql .= " left outer join lu_application_programs on lu_application_programs.application_id = application.id
		    left outer join programs on programs.id = lu_application_programs.program_id
		    left outer join degree on degree.id = programs.degree_id
		    left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		    left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id

            LEFT OUTER JOIN application_decision
                ON application.id = application_decision.application_id
                AND programs.id = application_decision.program_id
            left outer join application_program_letters on application_program_letters.lu_application_programs_id = lu_application_programs.id
          
            
		    where lu_application_programs.admission_status = 2
		    and lu_programs_departments.department_id=".$deptId;

            // PLB added period where 1/7/10
            if ($periodId) {
                $sql .= " AND period_application.period_id = " .  $periodId;
            } 
            
            $sql .= " 
            AND (application_program_letters.admit_sent IS NULL OR application_program_letters.admit_sent != 1)
            group by  application.id ";

            if ($deptId == 1) {
            
                if ($name == "Acceptance Letter (Portugal)") {
                
                     $sql .= "having program LIKE '%portugal%' ";
                
                } else {
                
                    $sql .= "having program NOT LIKE '%portugal%' ";
                }            
            }
            
            $sql .= "order by lastname, firstname";
        } else {
            // is Design department
            /******************************************************************************************************************
             select
        distinct
        application.id AS application_id,
        firstname, lastname, email, address_cur_street1, gender,
        address_cur_street2,
        address_cur_street3,
        address_cur_street4,
        address_cur_city,
        states.name as state,
        address_cur_pcode,
        countries.name as country,
        GROUP_CONCAT( DISTINCT 
                concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
            ) AS program,
        lu_application_programs.faccontact,
        lu_application_programs.stucontact
        
        from  lu_users_usertypes
        inner join users on users.id =  lu_users_usertypes.user_id
        inner join users_info on users_info.user_id= lu_users_usertypes.id
        left outer join countries on countries.id = address_cur_country
        left outer join states on states.id = address_cur_state
        INNER JOIN application on application.user_id = lu_users_usertypes.id 
          INNER JOIN period_application ON application.id = period_application.application_id 
        left outer join lu_application_programs on lu_application_programs.application_id = application.id
        left outer join programs on programs.id = lu_application_programs.program_id
        left outer join degree on degree.id = programs.degree_id
        left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        inner join application_decision_design on application_decision_design.application_id = application.id
        where application_decision_design.admission_status = 2
            AND lu_programs_departments.department_id=95 AND period_application.period_id = 715 
        AND (application.invitation_email_sent IS NULL OR application.invitation_email_sent != 1)
        group by  application.id order by lastname, firstname
            */
            $sql = "select
        distinct
        application.id AS application_id,
        firstname, lastname, email, address_cur_street1, gender,
        address_cur_street2,
        address_cur_street3,
        address_cur_street4,
        address_cur_city,
        states.name as state,
        address_cur_pcode,
        countries.name as country,
        GROUP_CONCAT( DISTINCT 
                concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
            ) AS program,
        lu_application_programs.faccontact,
        lu_application_programs.stucontact
        from  lu_users_usertypes
        inner join users on users.id =  lu_users_usertypes.user_id
        inner join users_info on users_info.user_id= lu_users_usertypes.id
        left outer join countries on countries.id = address_cur_country
        left outer join states on states.id = address_cur_state
        INNER JOIN application on application.user_id = lu_users_usertypes.id";
        
        // PLB added period join 1/7/10
        if ($periodId) {
            $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
        }        
        
        $sql .= " left outer join application_decision_design on application_decision_design.application_id = application.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        left outer join programs on programs.id = application_decision_design.admission_program_id
        left outer join degree on degree.id = programs.degree_id
        left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join application_program_letters on application_program_letters.lu_application_programs_id = lu_application_programs.id
 
        where application_decision_design.admission_status = 2
            AND lu_programs_departments.department_id=".$deptId;

        // PLB added period where 1/7/10
        if ($periodId) {
            $sql .= " AND period_application.period_id = " .  $periodId;
        } 
        
        $sql .= " 
        AND (application_program_letters.admit_sent IS NULL OR application_program_letters.admit_sent != 1)
        group by  application.id ";

        
        
        $sql .= "order by lastname, firstname";
        }
        
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		echo "<div style='margin-left:57px; margin-right:45px; margin-top:0px'>";

        if ($name == 'Invitation Email') {
        ?>
            <input name="btnSubmit" type="submit" id="btnSubmit" value="Send Invitation Emails" onClick="return confirmSubmit()"><br>
        <?php 
        }
        while( $row = mysql_fetch_array( $result ) ) {
            $invitees[] = $row;        
        }
        
         if ($name == 'Invitation Email') {
             echo count($invitees) . " invited applicants returned.<br><br><br>";
         }
  
        if (count($invitees) > 0) {
            foreach ($invitees as $row)
		    {
			    $gender = "";
			    $program = $row['program'];

			    $aPrograms = split(",", $program);
			    if(count($aPrograms) > 1)
			    {
				    $program = "";
				    for($i = 0; $i < count($aPrograms); $i++)
				    {
					    if($i == 0)
					    {
						    $program .= $aPrograms[$i] . " program";
					    }
                        else
					    {
						    if($i == count($aPrograms)-1)
						    {
							    $program .= " and the ". $aPrograms[$i]. " program";
						    }
                            else
						    {
							    $program .= ", the ". $aPrograms[$i]. " program";
						    }
					    }
				    }
			    } else {
                    
                    $program .= ' program';
                    
                }
			    if($row['gender'] == "M")
			    {
				    $gender = "Mr. ";
			    }
			    if($row['gender'] == "F")
			    {
				    $gender = "Ms. ";
			    }

			    $address = $row['address_cur_street1'];
			    if($row['address_cur_street2'] != "")
			    {
				    $address .= "<br>".$row['address_cur_street2'];
			    }
			    if($row['address_cur_street3'] != "")
			    {
				    $address .= "<br>". $row['address_cur_street3'];
			    }
			    if($row['address_cur_street4'] != "" && $row['address_cur_street4'] !== $row['address_cur_street3'])
			    {
				    $address .=  "<br>". $row['address_cur_street4'];
			    }
			    if($row['address_cur_street3'] != $row['address_cur_city'] . " " .$row['address_cur_pcode'] ){
				    $address .= "<br>". $row['address_cur_city'];
				    if($row['state'] != "")
				    {
					    $address .= ", ". $row['state']. " ";
				    }
				    if($row['address_cur_pcode'] != "")
				    {
					    $address .= " ". $row['address_cur_pcode']. "";
				    }
			    }
			    if(ucwords(strtolower($row['country'])) != "United States")
			    {
				    $address .= "<br>";
				    $address .= ucwords(strtolower($row['country']));
			    }

                $firstname = $row['firstname'];
                if (strtoupper($firstname) == $firstname) {
                    $firstnameArray = explode('-', strtolower(trim($row['firstname'])));
                    $firstname = implode('-', array_map('ucwords', $firstnameArray));     
                }
                
                $lastname = $row['lastname'];           
                if (strtoupper($lastname) == $lastname) {
                    $lastnameArray = explode('-', strtolower(trim($row['lastname'])));
                    $lastname = implode('-', array_map('ucwords', $lastnameArray));    
                }
                
			    $vars = array(
			        array('date', date("F j, Y") ),
			        array('firstname', $firstname),
			        array('lastname', $lastname),
			        array('gender',$gender),
			        array('address',$address),
			        array('program',$program),
			        array('faccontact',$row['faccontact']),
			        array('stucontact',$row['stucontact'])
			    );
                
                $content2 = parseEmailTemplate2($content, $vars );
			     
                if ($name == 'Invitation Email') {
                
                    if(isset($_POST['btnSubmit']))
                    {    
                         $app_program_query = "select lap.id from lu_application_programs lap 
                            inner join lu_programs_departments lpd on lpd.program_id = lap.program_id
                            and lpd.department_id = " . $deptId .
                            " where application_id = " . $row['application_id'] 
                            . " and admission_status = 2";
                    
                    $lap_results = mysql_query($app_program_query) or die(mysql_error() . $app_program_query);
                                      
                    while( $lap_row = mysql_fetch_array( $lap_results ) ) {
                    
        
                    // New way based upon program 
                        
                        $sql = "insert into application_program_letters (lu_application_programs_id, admit_sent, admit_sent_date) 
                                VALUES (" . $lap_row['id'] . ", 1, NOW()) ON DUPLICATE KEY UPDATE admit_sent = 1, admit_sent_date = NOW()";
                        mysql_query($sql) or die(mysql_error() . $sql);
                    }
                    
        
                     /*   old way
                        $sql = "update application set invitation_email_sent = 1 where id = ".$row["application_id"];
                        mysql_query($sql) or die(mysql_error() . $sql);
                     */
                        
                        $mailSieveString = '[' . getDomainName($deptId) . '::';
                        $mailSieveString .= $row["application_id"] . ']';
                        
                        if ($domain == 11) {
                            sendHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "invitation", $domain, $mailSieveString);
                        } elseif ($domain == 49) {
                            sendHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "invitation", $domain, $mailSieveString);
                        } else {
                            sendHtmlMail($sysemail, $row['email'], "CMU Admissions Decision", $content2, "invitation", $domain, $mailSieveString);
                        }
                    
                      //  echo "<br><b>Invitation Emails Sent.</b><br><br>";
                    } 
                    
                    if ($domain == 11) {
                        echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "invitation", $domain ) ;
                    } elseif ($domain == 49) { 
                        echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "invitation", $domain);
                    } else {
                        echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU Admissions Decision", $content2, "invitation", $domain ) ;
                    }
                              
                } else {    
                
			        printData($content2);
                }
		    }
        }
        if(isset($_POST['btnSubmit']) && $name == 'Invitation Email') {
              echo "<br><b>Invitation Emails Sent.</b><br><br>";
        }
        
		echo "</div>";
	    break;

    case "Waitlisted Letter":
        
        ?>
        <input name="btnSubmit" type="submit" id="btnSubmit" value="Send Waitlisted Emails" onClick="return confirmSubmit()"><br>
        <?
        $domainSql = "select domain_id from lu_domain_department where department_id = ".$deptId;
        $domainResult = mysql_query($domainSql) or die(mysql_error() . $domainSql);
        $domainRow = mysql_fetch_array( $domainResult );
        $domain = $domainRow['domain_id'];
        
        if (isIniDepartment($deptId))
        {
            $waitlistees = getIniWaitlistees($deptId, $periodId);    
        }
        else if (isDesignDepartment($deptId)) 
        {
            $waitlistees = getDesignWaitlistees($deptId, $periodId);
        }
        else
        {
            $waitlistees = getWaitlistees($deptId, $periodId);
        }
        
        echo count($waitlistees) . " rows returned.<br><br><br>";
        
        foreach($waitlistees as $row)
        {
            if(isDesignDepartment($deptId)) {
                $program = $row['program'];

                $aPrograms = split(",", $program);
                if(count($aPrograms) > 1)
                {
                    $program = "";
                    for($i = 0; $i < count($aPrograms); $i++)
                    {
                        if($i == 0)
                        {
                            $program .= $aPrograms[$i] . " program";
                        }
                        else
                        {
                            if($i == count($aPrograms)-1)
                            {
                                $program .= " and the ". $aPrograms[$i]. " program";
                            }
                            else
                            {
                                $program .= ", the ". $aPrograms[$i]. " program";
                            }
                        }
                    }
                } else {
                    
                    $program .= ' program';
                    
                }
                $vars = array(
                    array('date', date("F j, Y") ),
                    array('firstname', ucwords(strtolower($row['firstname']))  ),
                    array('lastname',ucwords(strtolower($row['lastname']))),
                    array('program', $program ),
                );
            } else {
                $vars = array(
                    array('date', date("F j, Y") ),
                    array('firstname', ucwords(strtolower($row['firstname']))  ),
                    array('lastname',ucwords(strtolower($row['lastname']))),
                );
            }
            $content2 = parseEmailTemplate2($content, $vars );
            if(isset($_POST['btnSubmit']))
            {
                $sql = "update application set waitlist_sent = 1 where id = ".$row["application_id"];
                /////////////////////////////////////////////////
                //UNCOMMENT THESE 2 LINES TO ENABLE EMAIL SENDING
                /////////////////////////////////////////////////
                mysql_query($sql) or die(mysql_error() . $sql);
                
                $mailSieveString = '[' . getDomainName($departmentId) . '::';
                $mailSieveString .= $row["application_id"] . ']';
                
                if ($domain == 11) {
                    sendHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "waitlist", $domain, $mailSieveString);
                } elseif ($domain == 49) {
                    sendHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "waitlist", $domain, $mailSieveString);
                } else {
                    sendHtmlMail($sysemail, $row['email'], "CMU Admissions Decision", $content2, "waitlist", $domain, $mailSieveString);
                }
            } 

            if ($domain == 11) {
                echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain ) ;//to user
             } elseif ($domain == 49) { 
                echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain);
             } elseif (isIniDepartment($deptId)) { 
                echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU INI Admissions Decision", $content2, "rejection", $domain);
             } else {
                echo  $row["application_id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU Admissions Decision", $content2, "rejection", $domain ) ;//to user
            }
        }
        if(isset($_POST['btnSubmit']))
        {
            echo "<br>Waitlisted Emails Sent.<br>";
        }        

        break;

	case "Rejection Letter":

        ?>
		<input name="btnSubmit" type="submit" id="btnSubmit" value="Send Rejection Emails" onClick="return confirmSubmit()"><br>
        <?

        /*  DAS added to handle admins with multiple domains to use only the domain 
        associated with current department  */
        $domainSql = "select domain_id from lu_domain_department where department_id = ".$deptId;
        $domainResult = mysql_query($domainSql) or die(mysql_error() . $domainSql);
        $domainRow = mysql_fetch_array( $domainResult );
        $deptDomain = $domainRow['domain_id'];

        if ($departmentId == 3) {
            // Exclude happy rejectees.
            $unhappyRejectees = getDepartmentRejectees($deptDomain, $deptId, $periodId, FALSE);
          //  $unhappyRejectees = getUnhappyRejectees($deptDomain, $deptId, $periodId);
        }
        elseif (isIniDepartment($deptId))
        {
            $unhappyRejectees = getIniRejectees($deptDomain, $deptId, $periodId);    
        } 
        else {
            $unhappyRejectees = getDepartmentRejectees($deptDomain, $deptId, $periodId, FALSE);    
        }
        
        echo count($unhappyRejectees) . " rows returned.<br><br><br>";
          
		foreach ($unhappyRejectees as $row)
        {
			
            $applicationId = $row['id'];
            
            $gender = "";
            if($row['gender'] == "M")
            {
                $gender = "Mr. ";
            }
            if($row['gender'] == "F")
            {
                $gender = "Ms. ";
            }
			$program = $row['program'];
            $domain = $deptDomain;
			if($domain == "RI")
			{
				$domain = "SCS";
			}

			$aPrograms = explode('^', $program);

			if(count($aPrograms) > 1)
			{
				$program = "";
				for($i = 0; $i < count($aPrograms); $i++)
				{
					if($i == 0)
					{
						$program .= $aPrograms[$i] . " program";
					}else
					{
						if($i == count($aPrograms)-1)
						{  
                            if (isDesignDepartment($departmentId)) {
                                 $program .= " and the ". $aPrograms[$i]. " program";
                            } else {
							    $program .= " or the ". $aPrograms[$i]. " program";
                            }
						}else
						{
							$program .= ", the ". $aPrograms[$i]. " program";
						}

					}
				}
			}
			if (count($aPrograms) == 1)
			{
				$program .= " program";
			}
            
            $firstname = $row['firstname'];
            if (strtoupper($firstname) == $firstname) {
                $firstnameArray = explode('-', strtolower(trim($row['firstname'])));
                $firstname = implode('-', array_map('ucwords', $firstnameArray));     
            }
            
            $lastname = $row['lastname'];           
            if (strtoupper($lastname) == $lastname) {
                $lastnameArray = explode('-', strtolower(trim($row['lastname'])));
                $lastname = implode('-', array_map('ucwords', $lastnameArray));    
            }

            if ($gender && ($departmentId == 3 || $departmentId == 66)) {
                $applicant = $gender . $lastname;
            } else {
                $applicant = $firstname;   
            }

			$vars = array(
			    array('applicant', $applicant),
			    array('program', $program),
                array('firstname', $firstname),
                array('lastname', $lastname),
                array('date', date("F j, Y"))
			);
			$content2 = parseEmailTemplate2($content, $vars );
			if(isset($_POST['btnSubmit']))
			{
				// old way based upon application
                //  $sql = "update application set rejection_sent = 1 where id = ".$row["id"];
                
                $app_program_query = "select lap.id from lu_application_programs lap 
                        inner join lu_programs_departments lpd on lpd.program_id = lap.program_id
                        and lpd.department_id = " . $departmentId .
                        " where application_id = " . $row['id']
                . " and (admission_status IS NULL or admission_status = 0)";
                
                $lap_results = mysql_query($app_program_query) or die(mysql_error() . $app_program_query);
                                  
                while( $lap_row = mysql_fetch_array( $lap_results ) ) {
                
    
                // New way based upon program 
                    
                    $sql = "insert into application_program_letters (lu_application_programs_id, rejection_sent, rejection_sent_date) 
                            VALUES (" . $lap_row['id'] . ", 1, NOW()) ON DUPLICATE KEY UPDATE rejection_sent = 1, rejection_sent_date = NOW()";
                    mysql_query($sql) or die(mysql_error() . $sql);
                }
				/////////////////////////////////////////////////
				//UNCOMMENT THESE 2 LINES TO ENABLE EMAIL SENDING
				/////////////////////////////////////////////////
				
                
                $mailSieveString = '[' . getDomainName($departmentId) . '::';
                $mailSieveString .= $row["id"] . ']';
                
                if ($domain == 11) {
                    sendHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                } elseif ($domain == 49) {
                    sendHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                } elseif ($domain == 37) {
                    if ($departmentId == 53) {
                        sendHtmlMail($sysemail, $row['email'], "CMU MHCI Portugal Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                    } else {
                          sendHtmlMail($sysemail, $row['email'], "CMU MHCI Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                    }
                } elseif (isPhilosophyDepartment($deptId)) { 
                    sendHtmlMail($sysemail, $row['email'], "CMU Philosophy Admissions Decision", $content2, "rejection", $domain);
                } else {
                    sendHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                }
            }
            if ($domain == 11) {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain ) ;//to user
            } elseif ($domain == 49) { 
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain);
            } elseif ($domain == 37) {
                if ($departmentId == 53) {
                    echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MHCI Portugal Admissions Decision", $content2, "rejection", $domain ) ;//to user)
                } else {
                    echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MHCI Admissions Decision", $content2, "rejection", $domain ) ;//to user
                }
            } elseif (isPhilosophyDepartment($deptId)) { 
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU Philosophy Admissions Decision", $content2, "rejection", $domain);
            } elseif (isIniDepartment($deptId)) {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU INI Admissions Decision", $content2, "rejection", $domain ) ;//to user
            } else {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain ) ;//to user
            }
		}
		if(isset($_POST['btnSubmit']))
		{
			echo "<br>Rejection Emails Sent.<br>";
		}

	break;

    case "Happy Rejection Letter":
    case "Happy M.S. Rejection Letter":
    case "Happy Ph.D. Rejection Letter":    

        ?>
        <input name="btnSubmit" type="submit" id="btnSubmit" value="Send Rejection Emails" onClick="return confirmSubmit()"><br>
        <?
        
        $domainSql = "select domain_id from lu_domain_department where department_id = ".$departmentId;
        $domainResult = mysql_query($domainSql) or die(mysql_error() . $domainSql);
        $domainRow = mysql_fetch_array( $domainResult );
        $deptDomain = $domainRow['domain_id'];
        
        if ($name == 'Happy M.S. Rejection Letter') {
            $happyRejectees = getHappyMsRejectees($deptDomain, $departmentId, $periodId);     
        } else {
            $happyRejectees = getHappyPhdRejectees($deptDomain, $departmentId, $periodId);     
        }

        echo count($happyRejectees) . " rows returned.<br><br><br>";

        foreach($happyRejectees as $row)
        {
            $vars = array(
            array('firstname', ucwords(strtolower($row['firstname']))  ),
            array('rejDept',$row['Rej_Dept']),
            array('acceptDept',$row['Accept_Dept']),
            array('progField',$row['progField']),
            array('rejectField',$row['rejectField'])
            );
            
            $domain = "SCS";
 
            $content2 = parseEmailTemplate2($content, $vars );
            if(isset($_POST['btnSubmit']))
            {
                // old way   $sql = "update application set rejection_sent = 1 where id = ".$row["id"];
                
                $sql = "insert into application_program_letters (lu_application_programs_id, rejection_sent, rejection_sent_date) 
                        VALUES (" . $row['lap_id'] . ", 1, NOW()) ON DUPLICATE KEY UPDATE rejection_sent = 1, rejection_sent_date = NOW()";
                /////////////////////////////////////////////////
                //UNCOMMENT THESE 2 LINES TO ENABLE EMAIL SENDING
                /////////////////////////////////////////////////
                mysql_query($sql) or die(mysql_error() . $sql);
                
                $mailSieveString = '[' . getDomainName($departmentId) . '::';
                $mailSieveString .= $row["application_id"] . ']';
                
                if ($domain == 11) {
                    sendHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                } elseif ($domain == 49) {
                    sendHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                } else {
                    sendHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                }
            }
            
            if ($domain == 11) {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain );//to user
            } elseif ($domain == 49) { 
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain);
            } else {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain );//to user
            }
        }
        if(isset($_POST['btnSubmit']))
        {
            echo "<br>Rejection Emails Sent.<br>";
        }

    break;
    
    case "Invite Reject":
    case "Request Permission for LSE"
        ?>
        <input name="btnSubmit" type="submit" id="btnSubmit" value="Send Permission Emails" onClick="return confirmSubmit()"><br>
        <?

        /*  DAS added to handle admins with multiple domains to use only the domain 
        associated with current department  */
        $domainSql = "select domain_id from lu_domain_department where department_id = ".$deptId;
        $domainResult = mysql_query($domainSql) or die(mysql_error() . $domainSql);
        $domainRow = mysql_fetch_array( $domainResult );
        $deptDomain = $domainRow['domain_id'];
        // DebugBreak();
        $unhappyRejectees = getDepartmentRound2Rejectees($deptDomain, $deptId, $periodId, FALSE);    
    
        
        echo count($unhappyRejectees) . " rows returned.<br><br><br>";
                  
        foreach ($unhappyRejectees as $row)
        {
            
            $applicationId = $row['id'];
            
            $gender = "";
            if($row['gender'] == "M")
            {
                $gender = "Mr. ";
            }
            if($row['gender'] == "F")
            {
                $gender = "Ms. ";
            }
            $program = $row['program'];
            $domain = $deptDomain;
            if($domain == "RI")
            {
                $domain = "SCS";
            }

            $aPrograms = explode('^', $program);

            if(count($aPrograms) > 1)
            {
                $program = "";
                for($i = 0; $i < count($aPrograms); $i++)
                {
                    if($i == 0)
                    {
                        $program .= $aPrograms[$i] . " program";
                    }else
                    {
                        if($i == count($aPrograms)-1)
                        {
                            $program .= " or the ". $aPrograms[$i]. " program";
                        }else
                        {
                            $program .= ", the ". $aPrograms[$i]. " program";
                        }

                    }
                }
            }
            if (count($aPrograms) == 1)
            {
                $program .= " program";
            }
            
            $firstname = $row['firstname'];
            if (strtoupper($firstname) == $firstname) {
                $firstnameArray = explode('-', strtolower(trim($row['firstname'])));
                $firstname = implode('-', array_map('ucwords', $firstnameArray));     
            }
            
            $lastname = $row['lastname'];           
            if (strtoupper($lastname) == $lastname) {
                $lastnameArray = explode('-', strtolower(trim($row['lastname'])));
                $lastname = implode('-', array_map('ucwords', $lastnameArray));    
            }

            if ($gender && ($departmentId == 3 || $departmentId == 66)) {
                $applicant = $gender . $lastname;
            } else {
                $applicant = $firstname;   
            }

            $vars = array(
                array('applicant', $applicant),
                array('program', $program)
            );
            $content2 = parseEmailTemplate2($content, $vars );
            if(isset($_POST['btnSubmit']))
            {
                // old way based upon application
                //  $sql = "update application set rejection_sent = 1 where id = ".$row["id"];
                
                $app_program_query = "select lap.id from lu_application_programs lap 
                        inner join lu_programs_departments lpd on lpd.program_id = lap.program_id
                        and lpd.department_id = " . $departmentId .
                        " where application_id = " . $row['id']
                . " and (admission_status IS NULL or admission_status = 0)";
                
                $lap_results = mysql_query($app_program_query) or die(mysql_error() . $app_program_query);
                                  
                while( $lap_row = mysql_fetch_array( $lap_results ) ) {
                
    
                // New way based upon program 
                    
                    $sql = "insert into application_program_letters (lu_application_programs_id, rejection_sent, rejection_sent_date) 
                            VALUES (" . $lap_row['id'] . ", 1, NOW()) ON DUPLICATE KEY UPDATE rejection_sent = 1, rejection_sent_date = NOW()";
                    mysql_query($sql) or die(mysql_error() . $sql);
                }
                /////////////////////////////////////////////////
                //UNCOMMENT THESE 2 LINES TO ENABLE EMAIL SENDING
                /////////////////////////////////////////////////
                
                
                $mailSieveString = '[' . getDomainName($departmentId) . '::';
                $mailSieveString .= $row["id"] . ']';
                
                if ($domain == 11) {
                    sendHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                } elseif ($domain == 49) {
                    sendHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                } elseif ($departmentId == 74) {
                
                $sysemail = "tracyf@cs.cmu.edu";
                echo  $row["id"]. "<br>". sendHtmlMail($sysemail, $row['email'], "Request to Review Carnegie Mellon Application for Professional Masters in Learning Science and Engineering", $content2, "rejection", $domain ) ;//to user
            } else {
                    sendHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain, $mailSieveString);
                }
            }
 
            if ($domain == 11) {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CNBC Admissions Decision", $content2, "rejection", $domain ) ;//to user
            } elseif ($domain == 49) { 
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU MSP Admissions Decision", $content2, "rejection", $domain);
            } elseif ($departmentId == 74) {
                $sysemail = "tracyf@cs.cmu.edu";
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain ) ;//to user
            } else {
                echo  $row["id"]. "<br>". printHtmlMail($sysemail, $row['email'], "CMU SCS Admissions Decision", $content2, "rejection", $domain ) ;//to user
            }
        }
        if(isset($_POST['btnSubmit']))
        {
            echo "<br>Invite Reject Emails Sent.<br>";
        }

    break;
}
function printData($content)
{
	echo html_entity_decode($content) ."\n<br style='page-break-after:always;'>\n";
}
?>
</form>
</body>
</html>
<?php

function getAllRejectees($departmentId, $periodId) {
    
    $query = "SELECT DISTINCT lu_application_programs.application_id, lu_application_programs.id AS lap_id,
        users.email, users.lastname, users.firstname, users_info.gender
        FROM lu_application_programs
        INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN period_application ON lu_application_programs.application_id = period_application.application_id
        INNER JOIN application ON lu_application_programs.application_id = application.id
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        INNER JOIN users_info ON users_info.user_id = lu_users_usertypes.id
        LEFT OUTER join application_program_letters apl on apl.lu_application_programs_id = lu_application_programs.id
        WHERE (lu_application_programs.admission_status = 0 OR lu_application_programs.admission_status IS NULL)
        AND application.submitted = 1";
    if ($departmentId != 50) {
        $query .= " AND (application.paid = 1 OR application.waive = 1)";
    }     
    $query .= " AND (apl.rejection_sent IS NULL OR apl.rejection_sent != 1)
        AND lu_programs_departments.department_id = " . $departmentId . "
        AND period_application.period_id = " . $periodId . " 
        ORDER BY users.lastname, users.firstname";
        
    $result = mysql_query($query) or die(mysql_error() . $query);

    $allRejectees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $allRejectees[] = $row;        
    }
    
    return $allRejectees;   
}

function getIniRejectees($deptDomain, $deptId, $periodId) {
    
    $query = "select DISTINCT rl.*
        from (select DISTINCT rp.app_id, rp.email, rp.lastname, rp.firstname, rp.gender 
        from (select lap.application_id as app_id, lap.program_id,  
        if(admission_status IS NULL or admission_status = 0, 1, 0) as rejected, 
        if(admission_status = 1, 1, 0) as waitlist, if(admission_status = 2, 1, 0) as admit, 
        users.email, users.lastname, users.firstname, users_info.gender
        from lu_application_programs lap
        inner join period_application pa on pa.application_id = lap.application_id and pa.period_id = " . $periodId .
        " inner join application a on a.id = pa.application_id and a.submitted = 1
        AND (a.paid = 1 OR a.waive = 1)
        inner join lu_programs_departments lpd on lpd.program_id = lap.program_id 
        and lpd.department_id = " . $deptId . 
        " left outer join application_program_letters apl on apl.lu_application_programs_id = lap.id 
        and (apl.rejection_sent IS NULL or  apl.rejection_sent = 0)
        INNER JOIN application ON lap.application_id = application.id
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        INNER JOIN users_info ON users_info.user_id = lu_users_usertypes.id 
        group by lap.application_id) rp
        where rp.rejected = 1 and rp.app_id not in
        (select ap.app_id from (select lap.application_id as app_id, lap.program_id, 
        if(admission_status IS NULL or admission_status = 0, 1, 0) as rejected, 
        if(admission_status = 1, 1, 0) as waitlist, if(admission_status = 2, 1, 0) as admit
        from lu_application_programs lap
        inner join period_application pa on pa.application_id = lap.application_id 
        and pa.period_id = " . $periodId . 
        " inner join application a on a.id = pa.application_id and a.submitted = 1
        AND (a.paid = 1 OR a.waive = 1)
        inner join lu_programs_departments lpd on lpd.program_id = lap.program_id 
        and lpd.department_id = " . $deptId . ") ap
        where ap.admit = 1 or ap.waitlist = 1) ) rl
        inner join lu_application_programs lap2 on lap2.application_id = rl.app_id
      inner join lu_programs_departments lpd2 on lpd2.program_id = lap2.program_id
      and lpd2.department_id = " . $deptId .
      " left outer join application_program_letters apl2 on apl2.lu_application_programs_id = lap2.id
      
      where (apl2.rejection_sent IS NULL or apl2.rejection_sent <> 1)
      AND rl.app_id NOT IN (
                            SELECT DISTINCT application_id
                            FROM application_decision_ini
                            INNER JOIN lu_programs_departments
                              ON application_decision_ini.admission_program_id = lu_programs_departments.program_id
                            WHERE application_decision_ini.admission_status IS NOT NULL 
                            AND application_decision_ini.admission_status > 0
                            AND lu_programs_departments.department_id = " . $deptId . "
                            )";
      
    $result = mysql_query($query) or die(mysql_error() . $query);

    $allRejectees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $allRejectees[] = $row;        
    }

    $rejectPrograms = getRejectPrograms('department', $deptId, $periodId);
    
    $unhappyRejectees = array();
    
    foreach ($allRejectees as $rejectee) {
        
        $applicationId = $rejectee['app_id'];
      //  DebugBreak();
        
        $rejecteeRecord = array();
     //   if ($includeHappyRejectees  || !isset($admitPrograms[$applicationId] )) {
            $rejecteeRecord['id'] = $applicationId;
            $rejecteeRecord['firstname'] = $rejectee['firstname'];
            $rejecteeRecord['lastname'] = $rejectee['lastname'];
            $rejecteeRecord['email'] = $rejectee['email'];
            $rejecteeRecord['gender'] = $rejectee['gender'];     
            $applicantRejectPrograms = array();
            
            foreach ($rejectPrograms[$applicationId] as $rejectProgram) {
                $applicantRejectPrograms[] = $rejectProgram['degree'] . ' ' . 
                $rejectProgram['linkword'] . ' ' . $rejectProgram['fieldsofstudy'];    
            }
            $rejecteeRecord['program'] = implode('^', $applicantRejectPrograms);
            $unhappyRejectees[] = $rejecteeRecord; 
        }  
//    }
    
    return $unhappyRejectees;   
}


function getDepartmentRejectees($deptDomain, $deptId, $periodId, $includeHappyRejectees=TRUE) {
    
    $query = "select DISTINCT rl.*
        from (select DISTINCT rp.app_id, rp.email, rp.lastname, rp.firstname, rp.gender 
        from (select lap.application_id as app_id, lap.program_id,  
        if(admission_status IS NULL or admission_status = 0, 1, 0) as rejected, 
        if(admission_status = 1, 1, 0) as waitlist, if(admission_status = 2, 1, 0) as admit, 
        users.email, users.lastname, users.firstname, users_info.gender
        from lu_application_programs lap
        inner join period_application pa on pa.application_id = lap.application_id and pa.period_id = " . $periodId .
        " inner join application a on a.id = pa.application_id and a.submitted = 1";
        if (!($deptId == 50 /* || $deptId == 74 */)) {
        $query .= " AND (a.paid = 1 OR a.waive = 1)";
    }
        $query .= " inner join lu_programs_departments lpd on lpd.program_id = lap.program_id 
        and lpd.department_id = " . $deptId . 
        " left outer join application_program_letters apl on apl.lu_application_programs_id = lap.id 
        and (apl.rejection_sent IS NULL or  apl.rejection_sent = 0)
        INNER JOIN application ON lap.application_id = application.id
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        INNER JOIN users_info ON users_info.user_id = lu_users_usertypes.id 
        group by lap.application_id) rp
        where rp.rejected = 1 and rp.app_id not in
        (select ap.app_id from (select lap.application_id as app_id, lap.program_id, 
        if(admission_status IS NULL or admission_status = 0, 1, 0) as rejected, 
        if(admission_status = 1, 1, 0) as waitlist, if(admission_status = 2, 1, 0) as admit
        from lu_application_programs lap
        inner join period_application pa on pa.application_id = lap.application_id 
        and pa.period_id = " . $periodId . 
        " inner join application a on a.id = pa.application_id and a.submitted = 1";
        
        if (!($deptId == 50 /* || $deptId == 74 */)) {
        $query .= " AND (a.paid = 1 OR a.waive = 1)";
    }
        $query .= " inner join lu_programs_departments lpd on lpd.program_id = lap.program_id 
        and lpd.department_id = " . $deptId . ") ap
        where ap.admit = 1 or ap.waitlist = 1) ) rl
        inner join lu_application_programs lap2 on lap2.application_id = rl.app_id
      inner join lu_programs_departments lpd2 on lpd2.program_id = lap2.program_id
      and lpd2.department_id = " . $deptId .
      " left outer join application_program_letters apl2 on apl2.lu_application_programs_id = lap2.id
      
      where apl2.rejection_sent IS NULL or apl2.rejection_sent <> 1";
    
    $result = mysql_query($query) or die(mysql_error() . $query);

    $allRejectees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $allRejectees[] = $row;        
    }

    $rejectPrograms = getRejectPrograms('department', $deptId, $periodId);
    
    $unhappyRejectees = array();
    
    foreach ($allRejectees as $rejectee) {
        
        $applicationId = $rejectee['app_id'];
      //  DebugBreak();
        
        $rejecteeRecord = array();
     //   if ($includeHappyRejectees  || !isset($admitPrograms[$applicationId] )) {
            $rejecteeRecord['id'] = $applicationId;
            $rejecteeRecord['firstname'] = $rejectee['firstname'];
            $rejecteeRecord['lastname'] = $rejectee['lastname'];
            $rejecteeRecord['email'] = $rejectee['email'];
            $rejecteeRecord['gender'] = $rejectee['gender'];     
            $applicantRejectPrograms = array();
            
            foreach ($rejectPrograms[$applicationId] as $rejectProgram) {
                $applicantRejectPrograms[] = $rejectProgram['degree'] . ' ' . 
                $rejectProgram['linkword'] . ' ' . $rejectProgram['fieldsofstudy'];    
            }
            $rejecteeRecord['program'] = implode('^', $applicantRejectPrograms);
            $unhappyRejectees[] = $rejecteeRecord; 
        }  
//    }
    
    return $unhappyRejectees;   
}

function getDepartmentRound2Rejectees($programId, $deptId, $periodId, $includeHappyRejectees=TRUE) {
    
    $query =  "select DISTINCT a.id as app_id, u.lastname, u.firstname, u.email, ui.gender
            from period_application pa
            inner join application a on a.id = pa.application_id and a.submitted = 1 and (a.paid =1 or a.waive = 1)
            inner join lu_application_programs lap on lap.application_id = a.id
            inner join lu_programs_departments lpd on lpd.department_id = " . $deptId . " and lpd.program_id = lap.program_id
            inner join promotion_status ps on ps.application_id = lap.application_id and ps.round = 2 
            and ps.department_id = lpd.department_id
            inner join lu_users_usertypes luu on luu.id = a.user_id
            inner join users_info ui on ui.user_id = a.user_id
            inner join users u on u.id = luu.user_id 
            where pa.period_id = " . $periodId . " and lap.application_id not in
                    (select ap.app_id from (select lap.application_id as app_id, lap.program_id, 
                    if(admission_status IS NULL or admission_status = 0, 1, 0) as rejected, 
                    if(admission_status = 1, 1, 0) as waitlist, if(admission_status = 2, 1, 0) as admit
                    from lu_application_programs lap
                    inner join period_application pa on pa.application_id = lap.application_id 
                    and pa.period_id = 495 inner join application a on a.id = pa.application_id and a.submitted = 1"; 
    if ($deptId != 50) {
        $query .= " AND (a.paid = 1 OR a.waive = 1)";
        }
    $query .= " inner join lu_programs_departments lpd on lpd.program_id = lap.program_id 
                    and lpd.department_id = " . $deptId . ") ap
                    where ap.admit = 1 or ap.waitlist = 1 )
            order by u.lastname, u.firstname";
    // DebugBreak();
    $result = mysql_query($query) or die(mysql_error() . $query);

    $allRejectees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $allRejectees[] = $row;        
    }

    $rejectPrograms = getRejectRound2Programs('department', $deptId, $periodId);
     
    $unhappyRejectees = array();
    
    foreach ($allRejectees as $rejectee) {
        
        $applicationId = $rejectee['app_id'];
      //  DebugBreak();
        
        $rejecteeRecord = array();
     //   if ($includeHappyRejectees  || !isset($admitPrograms[$applicationId] )) {
            $rejecteeRecord['id'] = $applicationId;
            $rejecteeRecord['firstname'] = $rejectee['firstname'];
            $rejecteeRecord['lastname'] = $rejectee['lastname'];
            $rejecteeRecord['email'] = $rejectee['email'];
            $rejecteeRecord['gender'] = $rejectee['gender'];     
            $applicantRejectPrograms = array();
            
            foreach ($rejectPrograms[$applicationId] as $rejectProgram) {
                $applicantRejectPrograms[] = $rejectProgram['degree'] . ' ' . 
                $rejectProgram['linkword'] . ' ' . $rejectProgram['fieldsofstudy'];    
            }
            $rejecteeRecord['program'] = implode('^', $applicantRejectPrograms);
            $unhappyRejectees[] = $rejecteeRecord; 
        }  
//    }
    
    return $unhappyRejectees;   
}


function getRejectPrograms($unit, $unitId, $periodId) {

    if ($unit == 'domain' && $unitId == 3) {
        $unitId = 1;
    }

    $query = "SELECT DISTINCT lu_application_programs.application_id, 
        lu_programs_departments.department_id, department.name AS department,
        lu_application_programs.id AS lap_id, lu_application_programs.program_id,
        degree.name AS degree, programs.linkword, fieldsofstudy.name as fieldsofstudy
        FROM lu_application_programs
        INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN department ON lu_programs_departments.department_id = department.id
        INNER JOIN lu_domain_department ON lu_programs_departments.department_id = lu_domain_department.department_id
        INNER JOIN programs ON lu_application_programs.program_id = programs.id
        INNER JOIN degree ON programs.degree_id = degree.id
        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
        INNER JOIN application ON lu_application_programs.application_id = application.id
        INNER JOIN period_application ON lu_application_programs.application_id = period_application.application_id
        LEFT OUTER join application_program_letters apl on apl.lu_application_programs_id = lu_application_programs.id
        WHERE (lu_application_programs.admission_status = 0 OR lu_application_programs.admission_status IS NULL)
        AND (apl.rejection_sent IS NULL OR apl.rejection_sent != 1)";
    if ($unit == 'domain') {
        $query .= " AND lu_domain_department.domain_id = " . $unitId;  
    } else {
        $query .= " AND lu_programs_departments.department_id = " . $unitId;
    }
    $query .= " AND period_application.period_id = " . $periodId;
    $query .= " ORDER BY application.id";
        
    $result = mysql_query($query) or die(mysql_error() . $query);

    $rejectPrograms = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $applicationId = $row['application_id'];
        $lapId = $row['lap_id'];
        $rejectPrograms[$applicationId][] = $row;        
    }
    
    return $rejectPrograms;       
    
}

function getRejectRound2Programs($unit, $unitId, $periodId) {

    if ($unit == 'domain' && $unitId == 3) {
        $unitId = 1;
    }

    $query = "SELECT DISTINCT lu_application_programs.application_id, 
        lu_programs_departments.department_id, department.name AS department,
        lu_application_programs.id AS lap_id, lu_application_programs.program_id,
        degree.name AS degree, programs.linkword, fieldsofstudy.name as fieldsofstudy
        FROM lu_application_programs
        INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN department ON lu_programs_departments.department_id = department.id
        INNER JOIN lu_domain_department ON lu_programs_departments.department_id = lu_domain_department.department_id
        INNER JOIN programs ON lu_application_programs.program_id = programs.id
        INNER JOIN degree ON programs.degree_id = degree.id
        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
        INNER JOIN application ON lu_application_programs.application_id = application.id
        INNER JOIN period_application ON lu_application_programs.application_id = period_application.application_id
        LEFT OUTER join application_program_letters apl on apl.lu_application_programs_id = lu_application_programs.id
        WHERE (lu_application_programs.admission_status = 0 OR lu_application_programs.admission_status IS NULL)
      /*  AND (apl.rejection_sent IS NULL OR apl.rejection_sent != 1) */";
    if ($unit == 'domain') {
        $query .= " AND lu_domain_department.domain_id = " . $unitId;  
    } else {
        $query .= " AND lu_programs_departments.department_id = " . $unitId;
    }
    $query .= " AND period_application.period_id = " . $periodId;
    $query .= " ORDER BY application.id";
   //  DebugBreak();   
    $result = mysql_query($query) or die(mysql_error() . $query);

    $rejectPrograms = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $applicationId = $row['application_id'];
        $lapId = $row['lap_id'];
        $rejectPrograms[$applicationId][] = $row;        
    }
    
    return $rejectPrograms;       
    
}


function getAdmitPrograms($unit, $unitId, $periodId) {

    if ($unit == 'domain' && $unitId == 3) {
        $unitId = 1;
    }
    
    $query = "SELECT DISTINCT lu_application_programs.application_id, 
        lu_programs_departments.department_id, department.name AS department, 
        lu_application_programs.id as lap_id, lu_application_programs.program_id,
        degree.name AS degree, programs.linkword, fieldsofstudy.name as fieldsofstudy
        FROM lu_application_programs
        INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN department ON lu_programs_departments.department_id = department.id 
        INNER JOIN lu_domain_department ON lu_programs_departments.department_id = lu_domain_department.department_id 
        INNER JOIN programs ON lu_application_programs.program_id = programs.id
        INNER JOIN degree ON programs.degree_id = degree.id
        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
        INNER JOIN application ON lu_application_programs.application_id = application.id
        INNER JOIN period_application ON lu_application_programs.application_id = period_application.application_id
        LEFT OUTER join application_program_letters apl on apl.lu_application_programs_id = lu_application_programs.id
        WHERE lu_application_programs.admission_status = 2
        AND (apl.rejection_sent IS NULL OR apl.rejection_sent != 1)";
    if ($unit == 'domain') {
        $query .= " AND lu_domain_department.domain_id = " . $unitId;  
    } else {
        $query .= " AND lu_programs_departments.department_id = " . $unitId;
    }
        $query .= " AND period_application.period_id = " . $periodId;
      
    $result = mysql_query($query) or die(mysql_error() . $query);

    $admitPrograms = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $applicationId = $row['application_id'];
        $lapId = $row['lap_id'];
        $admitPrograms[$lapId][] = $row;        
    }
    
    return $admitPrograms;    
}


function getLtiMastersAdmits() {

    $query = "SELECT * FROM lti_masters_admits";
    
    $result = mysql_query($query) or die(mysql_error() . $query);
    
    $mastersAdmitIds = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $mastersAdmitIds[] = $row['application_id'];        
    }
    
    return $mastersAdmitIds; 
        
}


function getUnhappyRejectees($domainId, $departmentId, $periodId, $includeHappyRejectees = TRUE) {
    
    if ($domainId == 3) {
        $domainId = 1;
    }
    
    $allRejectees = getAllRejectees($departmentId, $periodId);
    
    /*
    if ($domainId == 1) {
        $rejectPrograms = getRejectPrograms('domain', $domainId, $periodId);
        $admitPrograms = getAdmitPrograms('domain', $domainId, $periodId);    
    } else {
        $rejectPrograms = getRejectPrograms('department', $departmentId, $periodId);
        $admitPrograms = getAdmitPrograms('department', $departmentId, $periodId);    
    }
    */
    
    // Always restrict admit/reject programs by department 
    // to prevent cross-department notification.
    $rejectPrograms = getRejectPrograms('department', $departmentId, $periodId);
    $admitPrograms = getAdmitPrograms('department', $departmentId, $periodId);  
    
    $unhappyRejectees = array();
    
    foreach ($allRejectees as $rejectee) {
        
        $applicationId = $rejectee['application_id'];
      //  DebugBreak();
        $lap_id = $rejectee['lap_id'];
        
        $rejecteeRecord = array();
        if ($includeHappyRejectees || !isset($admitPrograms[$lap_id]) ) {
            $rejecteeRecord['id'] = $applicationId;
            $rejecteeRecord['lap_id'] = $lap_id;
            $rejecteeRecord['firstname'] = $rejectee['firstname'];
            $rejecteeRecord['lastname'] = $rejectee['lastname'];
            $rejecteeRecord['email'] = $rejectee['email'];
            $rejecteeRecord['gender'] = $rejectee['gender'];     
            $applicantRejectPrograms = array();
            foreach ($rejectPrograms[$lap_id] as $rejectProgram) {
                $applicantRejectPrograms[] = $rejectProgram['degree'] . ' ' . 
                $rejectProgram['linkword'] . ' ' . $rejectProgram['fieldsofstudy'];    
            }
            $rejecteeRecord['program'] = implode('^', $applicantRejectPrograms);
            $unhappyRejectees[] = $rejecteeRecord; 
        }  
    }
    
    return $unhappyRejectees;
}


function getHappyPhdRejectees($domainId, $departmentId, $periodId) {

    if ($domainId == 3) {
        $domainId = 1;
    }

    $allRejectees = getAllRejectees($departmentId, $periodId);
    if ($domainId == 1) {
        $rejectPrograms = getRejectPrograms('domain', $domainId, $periodId);
        $admitPrograms = getAdmitPrograms('domain', $domainId, $periodId);    
    } else {
        $rejectPrograms = getRejectPrograms('department', $departmentId, $periodId);
        $admitPrograms = getAdmitPrograms('department', $departmentId, $periodId);    
    }
    
    $ltiMastersAdmits = getLtiMastersAdmits();

    $happyPhdRejectees = array();
    foreach ($allRejectees as $rejectee) {
        
        $applicationId = $rejectee['application_id']; 
        
        if ( !isset($admitPrograms[$applicationId]) ) {
            continue;
        }
        
        $phdAdmit = FALSE;
        $admitDepartmentIds = array();
        $admitFieldsofstudy = array();
        $applicantAdmitPrograms = array(); 
        foreach($admitPrograms[$applicationId] as $admitProgram) {
            
            // Must have at least one Phd admit from another dept.
            if ($admitProgram['department_id'] != $departmentId) {
                
                if ( trim($admitProgram['degree']) == 'Ph.D.' ) {               
                    $phdAdmit = TRUE;            
                } elseif ( trim($admitProgram['degree']) == 'Ph.D./M.S.' ) {            
                    if ( !in_array($applicationId, $ltiMastersAdmits) ) {
                        $phdAdmit = TRUE;    
                    }                        
                }
            
                $admitDepartmentIds[] = $admitProgram['department_id'];
                if ( !in_array($admitProgram['fieldsofstudy'], $admitFieldsofstudy) ) {                
                    $admitFieldsofstudy[] = $admitProgram['fieldsofstudy'];
                    $applicantAdmitPrograms[] = $admitProgram['degree'] . ' ' . 
                                $admitProgram['linkword'] . ' ' . $admitProgram['fieldsofstudy'];    
                }            
            }    
        }
        
        if (!$phdAdmit) {
            continue;
        }
        
        $rejectFieldsofstudy = array();
        foreach ($rejectPrograms[$applicationId] as $rejectProgram) {
            if ( !in_array($rejectProgram['department_id'], $admitDepartmentIds) ) {
                if ( !in_array($rejectProgram['fieldsofstudy'], $rejectFieldsofstudy) ) {
                    $rejectFieldsofstudy[] = $rejectProgram['fieldsofstudy'];    
                }
            }
        }

        $rejecteeRecord = array();        
        $rejecteeRecord['id'] = $applicationId;
        $rejecteeRecord['firstname'] = $rejectee['firstname'];
        $rejecteeRecord['email'] = $rejectee['email']; 
        $rejecteeRecord['rejectField'] = $rejectPrograms[$applicationId][0]['fieldsofstudy'];
        //$rejecteeRecord['rejectField'] = implode(' and ', $rejectFieldsofstudy);
        $rejecteeRecord['Rej_Dept'] = $rejectPrograms[$applicationId][0]['department'];
        $rejecteeRecord['Accept_Dept'] = $admitPrograms[$applicationId][0]['department'];
        $rejecteeRecord['progField'] = implode(' and ', $admitFieldsofstudy);
        //$rejecteeRecord['progField'] = implode(' and ', $applicantAdmitPrograms);
        
        $happyPhdRejectees[] = $rejecteeRecord; 
    }
    
    return $happyPhdRejectees;    
}

function getHappyMsRejectees($domainId, $departmentId, $periodId) {

    $allRejectees = getAllRejectees($departmentId, $periodId);
     if ($domainId == 1) {
        $rejectPrograms = getRejectPrograms('domain', $domainId, $periodId);
        $admitPrograms = getAdmitPrograms('domain', $domainId, $periodId);    
    } else {
        $rejectPrograms = getRejectPrograms('department', $departmentId, $periodId);
        $admitPrograms = getAdmitPrograms('department', $departmentId, $periodId);    
    }
    $ltiMastersAdmits = getLtiMastersAdmits();

    $happyMsRejectees = array();
    foreach ($allRejectees as $rejectee) {
        
        $applicationId = $rejectee['application_id']; 
        
        if ( !isset($admitPrograms[$applicationId]) ) {
            continue;
        }
        
        $msAdmit = FALSE;
        $admitDepartmentIds = array();
        $admitFieldsofstudy = array();
        $applicantAdmitPrograms = array();
        foreach($admitPrograms[$applicationId] as $admitProgram) {
            
            // Must have at least one Phd admit from another dept.
            if ($admitProgram['department_id'] != $departmentId) {
                
                if ( trim($admitProgram['degree']) == 'M.S.' ) {               
                    $msAdmit = TRUE;            
                } elseif ( trim($admitProgram['degree']) == 'Ph.D./M.S.' ) {            
                    if ( in_array($applicationId, $ltiMastersAdmits) ) {
                        $msAdmit = TRUE;
                        $admitProgram['degree'] = 'M.S.';    
                    }                        
                }
            
                $admitDepartmentIds[] = $admitProgram['department_id'];
                if ( !in_array($admitProgram['fieldsofstudy'], $admitFieldsofstudy) ) {                
                    $admitFieldsofstudy[] = $admitProgram['fieldsofstudy'];
                    $applicantAdmitPrograms[] = $admitProgram['degree'] . ' ' . 
                                $admitProgram['linkword'] . ' ' . $admitProgram['fieldsofstudy'];    
                }            
            }    
        }
        
        if (!$msAdmit) {
            continue;
        }
        
        
        $rejectFieldsofstudy = array();
        foreach ($rejectPrograms[$applicationId] as $rejectProgram) {
            if ( !in_array($rejectProgram['department_id'], $admitDepartmentIds) ) {
                $rejectFieldsofstudy[] = $rejectProgram['fieldsofstudy'];
            }    
            
        }
        
        $rejecteeRecord = array();        
        $rejecteeRecord['id'] = $applicationId;
        $rejecteeRecord['firstname'] = $rejectee['firstname'];
        $rejecteeRecord['email'] = $rejectee['email']; 
        //$rejecteeRecord['rejectField'] = $rejectPrograms[$applicationId][0]['fieldsofstudy'];
        $rejecteeRecord['rejectField'] = implode(' and ', $rejectFieldsofstudy);
        $rejecteeRecord['Rej_Dept'] = $rejectPrograms[$applicationId][0]['department'];
        $rejecteeRecord['Accept_Dept'] = $admitPrograms[$applicationId][0]['department'];
        $rejecteeRecord['progField'] = implode(' and ', $admitFieldsofstudy);
        //$rejecteeRecord['progField'] = implode(' and ', $applicantAdmitPrograms);
        
        $happyMsRejectees[] = $rejecteeRecord; 
    }
    
    return $happyMsRejectees;
}


function getHappyRejectees() {

    global $periodId;    

    $sql = "select r.application_id as id, users.email, users.lastname, users.firstname, 
            rejDegree.name as rejDegree, rejDept.name as Rej_Dept, rejFieldsofstudy.name as rejectField,
            acDegree.name as acDegree, acDept.name as Accept_Dept, 
            acFieldsofstudy.name as progField, acPrograms.id AS Accept_program_id
            from lu_application_programs as r";

    if ($periodId) {
        $sql .= " INNER JOIN period_application ON r.application_id = period_application.application_id";
    }
 
    $sql .= " inner join lu_application_programs as a on a.application_id = r.application_id and 
    ((r.admission_status = 0 or r.admission_status is NULL) and (a.admission_status = 2))
    inner join application as rapp on rapp.id = r.application_id
    inner join application as aapp on aapp.id = a.application_id
    inner join programs as rejPrograms on rejPrograms.id = r.program_id and rejPrograms.degree_id = 3
    inner join programs as acPrograms on acPrograms.id = a.program_id
    inner join degree as rejDegree on rejDegree.id = rejPrograms.degree_id and rejDegree.name like '%Ph.D%'
    inner join degree as acDegree on acDegree.id = acPrograms.degree_id
    inner join lu_programs_departments as rejDepts on rejDepts.program_id = r.program_id
    inner join lu_programs_departments as acDepts on acDepts.program_id = a.program_id
    inner join department as rejDept on rejDept.id = rejDepts.department_id and rejDept.name NOT like '%archive%'
    inner join department as acDept on acDept.id = acDepts.department_id and acDept.name NOT like '%archive%'
    
    INNER JOIN lu_domain_department ON rejDept.id = lu_domain_department.department_id
    
    inner join lu_users_usertypes as appuserid on appuserid.id = rapp.user_id
    inner join users on users.id = appuserid.user_id
    inner join fieldsofstudy as acFieldsofstudy on acFieldsofstudy.id = acPrograms.fieldofstudy_id
    INNER JOIN fieldsofstudy as rejFieldsofstudy ON rejFieldsofstudy.id = rejPrograms.fieldofstudy_id
    WHERE lu_domain_department.domain_id = 1 
    AND (aapp.submitted = 1 or rapp.submitted = 1) 
    and rejDept.name != acDept.name 
    
    AND acPrograms.id != 12
    AND r.application_id NOT IN (SELECT * FROM lti_masters_admits)
    
    and (rapp.rejection_sent IS NULL OR rapp.rejection_sent != 1)";

    if ($periodId) {
        $sql .= " AND period_application.period_id = " .  $periodId;
    }
        
    $sql .= " order by rejDept.name, users.lastname, users.firstname";
    
    $result = mysql_query($sql) or die(mysql_error() . $sql);

    $happyRejectees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $happyRejectees[] = $row;        
    }
    
    return $happyRejectees;
}

function isRiAdmit($applicationId) {

    $query = "SELECT * FROM lu_application_programs
                WHERE (program_id = 4 OR program_id = 12 OR program_id = 17)
                AND (admission_status = 1 OR admission_status = 2)
                AND application_id = " . $applicationId;
    
    $result = mysql_query($query) or die(mysql_error() . $sql);
    
    if ( mysql_numrows($result) > 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function getWaitlistees($departmentId, $periodId) {

    $sql = "SELECT DISTINCT lu_application_programs.application_id,
        users.email, users.lastname, users.firstname
        FROM lu_application_programs
        INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN period_application ON lu_application_programs.application_id = period_application.application_id
        INNER JOIN application ON lu_application_programs.application_id = application.id
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        LEFT OUTER JOIN application_program_letters ON application_program_letters.lu_application_programs_id = lu_application_programs.id
        WHERE lu_application_programs.admission_status = 1
        AND application.submitted = 1
        AND (application_program_letters.waitlist_sent IS NULL OR application_program_letters.waitlist_sent != 1)
        AND lu_programs_departments.department_id = " . $departmentId . "
        AND period_application.period_id = " . $periodId . " 
        ORDER BY users.lastname, users.firstname";
    
    $result = mysql_query($sql) or die(mysql_error() . $sql);

    $waitlistees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $waitlistees[] = $row;        
    }
    
    return $waitlistees;    
}

function getIniWaitlistees($departmentId, $periodId) {

    $sql = "SELECT DISTINCT lu_application_programs.application_id,
        users.email, users.lastname, users.firstname
        FROM lu_application_programs
        INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN period_application ON lu_application_programs.application_id = period_application.application_id
        INNER JOIN application ON lu_application_programs.application_id = application.id
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        LEFT OUTER JOIN application_program_letters ON application_program_letters.lu_application_programs_id = lu_application_programs.id
        WHERE lu_application_programs.admission_status IN (1, 4)
        AND application.submitted = 1
        AND (application_program_letters.waitlist_sent IS NULL OR application_program_letters.waitlist_sent != 1)
        AND lu_programs_departments.department_id = " . $departmentId . "
        AND period_application.period_id = " . $periodId . " 
        AND lu_application_programs.application_id NOT IN
        (SELECT DISTINCT application_id
            FROM application_decision_ini
            INNER JOIN lu_programs_departments
              ON application_decision_ini.admission_program_id = lu_programs_departments.program_id
            WHERE application_decision_ini.admission_status IS NOT NULL 
            AND application_decision_ini.admission_status = 2
            AND lu_programs_departments.department_id = " . $departmentId . "
        )
        ORDER BY users.lastname, users.firstname";
    
    $result = mysql_query($sql) or die(mysql_error() . $sql);

    $waitlistees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $waitlistees[] = $row;        
    }
    
    return $waitlistees;    
}

function getDesignWaitlistees($departmentId, $periodId) {
    
    /*$sql = "SELECT DISTINCT application_decision_design.application_id,
        users.email, users.lastname, users.firstname,
        GROUP_CONCAT( DISTINCT 
                concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
            ) AS program
        FROM application_decision_design
        INNER JOIN lu_programs_departments ON application_decision_design.admission_program_id = lu_programs_departments.program_id
        INNER JOIN period_application ON application_decision_design.application_id = period_application.application_id
        INNER JOIN application ON application_decision_design.application_id = application.id
        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        LEFT OUTER JOIN programs on programs.id = application_decision_design.admission_program_id
        LEFT OUTER JOIN degree on degree.id = programs.degree_id
        LEFT OUTER JOIN fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        LEFT OUTER JOIN application_program_letters ON application_program_letters.lu_application_programs_id = lu_application_programs.id
        WHERE  application_decision_design.admission_status = 1
        AND  application.submitted = 1
        AND (application_program_letters.waitlist_sent IS NULL OR application_program_letters.waitlist_sent != 1)
        AND lu_programs_departments.department_id = " . $departmentId . "
        AND period_application.period_id = " . $periodId . " 
        AND application_decision_design.application_id NOT IN
        (SELECT DISTINCT application_id
            FROM application_decision_design
            INNER JOIN lu_programs_departments
              ON application_decision_design.admission_program_id = lu_programs_departments.program_id
            WHERE application_decision_design.admission_status IS NOT NULL 
            AND application_decision_design.admission_status = 2
            AND lu_programs_departments.department_id = " . $departmentId . "
        )
        ORDER BY users.lastname, users.firstname";
        */
    $sql = "    select   lu_users_usertypes.id as id,
        application.id as application_id,
        concat(users.lastname, ', ',users.firstname) as name,
        users.lastname, users.firstname,
        users.email, if( admit_programs2.id,
                    group_concat(
                    distinct
                    concat(' ', admit_degree2.name, ' ', admit_programs2.linkword, ' ', admit_fieldsofstudy2.name)
                    order by admit_programs2.id
                    ),
                    group_concat(
                    distinct
                    concat(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                    order by programs.id
                    )  
                ) as program,
                group_concat(distinct if(application_decision_design.admission_status = 1, 'waitlist', 
                    if(application_decision_design.admission_status = 4, 'strong waitlist' , null))) as status, lu_application_programs.faccontact,
        lu_application_programs.stucontact from application
        inner join period_application on application.id = period_application.application_id

        inner join lu_users_usertypes on application.user_id = lu_users_usertypes.id
        inner join users on lu_users_usertypes.user_id = users.id
        inner join users_info on lu_users_usertypes.id = users_info.user_id
        left outer join countries on users_info.cit_country = countries.id

        inner join lu_application_programs on application.id = lu_application_programs.application_id
        inner join programs on lu_application_programs.program_id = programs.id
        inner join degree on programs.degree_id = degree.id
        inner join fieldsofstudy on programs.fieldofstudy_id = fieldsofstudy.id
        inner join lu_programs_departments
          on lu_application_programs.program_id = lu_programs_departments.program_id
        left outer join lu_application_interest
          on lu_application_programs.id = lu_application_interest.app_program_id
        left outer join interest on lu_application_interest.interest_id = interest.id
          left join usersinst
              on lu_users_usertypes.id = usersinst.user_id and usersinst.educationtype = 1
            left outer join institutes on usersinst.institute_id = institutes.id
            left outer join usersinst as usersinst2
              on lu_users_usertypes.id = usersinst2.user_id and usersinst2.educationtype = 2
            left outer join institutes as institutes2 on usersinst2.institute_id = institutes2.id left outer join application_decision_design
                  on lu_application_programs.application_id = application_decision_design.application_id
                left outer join programs as admit_programs2
                  on admit_programs2.id = application_decision_design.admission_program_id
                left outer join degree as admit_degree2
                  on admit_degree2.id = admit_programs2.degree_id
                left outer join fieldsofstudy as admit_fieldsofstudy2
                  on admit_fieldsofstudy2.id = admit_programs2.fieldofstudy_id
                where application.submitted = 1
                and (lu_application_programs.admission_status = 1
                  or application_decision_design.admission_status = 1
                  or lu_application_programs.admission_status = 4
                  or application_decision_design.admission_status = 4)
                and lu_programs_departments.department_id = " . $departmentId . " and period_application.period_id = " . $periodId . " and application.id not in (
                        select distinct application_id
                        from application_decision_design
                        inner join lu_programs_departments
                          on application_decision_design.admission_program_id = lu_programs_departments.program_id
                        where application_decision_design.admission_status = 2 
                        and lu_programs_departments.department_id = " . $departmentId . "
                        ) group by application.id     order BY name";
        
    $result = mysql_query($sql) or die(mysql_error() . $sql);

    $waitlistees = array();
    while( $row = mysql_fetch_array( $result ) ) {
        $waitlistees[] = $row;        
    }
    
    return $waitlistees;    
}

function getDomainName($departmentId) {
    
    $query = "SELECT domain.name FROM domain
                INNER JOIN lu_domain_department 
                ON domain.id = lu_domain_department.domain_id
                WHERE lu_domain_department.department_id = " . $departmentId . "
                ORDER BY domain.id
                LIMIT 1";
    $result = mysql_query($query);
    
    $domainName = '';
    while ($row = mysql_fetch_array($result)) {
        $domainName = $row['name'];
    }
    
    return $domainName;
}

?>