<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

// Include the db, unit/period, quickform classes.
define("CLASS_DIR", "../classes/");
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb_Table.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_VW_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminList.php';  
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListBase.php';
include CLASS_DIR . 'class.Department.php'; 
include '../inc/unitPeriodIncludes.inc.php';
require("HTML/QuickForm.php"); 

/*
* Handle the request variables
*/
$periodId = filter_input(INPUT_GET, 'periodId', FILTER_VALIDATE_INT);
$departmentId = filter_input(INPUT_GET, 'departmentId', FILTER_VALIDATE_INT);
$applicants = filter_input(INPUT_POST, 'applicants', FILTER_SANITIZE_STRING); 
if (!$applicants) {
    $applicants = 'all';
}
$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
if (!$action) {
    $action = 'new';
}

/*
* Get the period/unit info.
*/ 
$period = new Period($periodId);
$unit = new Unit( $period->getUnitId() );
$submissionPeriods = $period->getChildPeriods(2);
if (count($submissionPeriods) > 0) {
    $displayPeriod = $submissionPeriods[0];  
} else {
    $displayPeriod = $period;
}
$startDate = $displayPeriod->getStartDate('Y-m-d');
$endDate = $displayPeriod->getEndDate('Y-m-d');
$periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
$periodName = $period->getName();
$periodDescription = $period->getDescription();
if ($periodName) {
    $application_period_display = $periodName . ' (' . $periodDates . ')';
} elseif ($periodDescription) {
    $application_period_display = $periodDescription . ' (' . $periodDates . ')';    
} else {
    $application_period_display = $periodDates;     
}

/*
* Define and an email quickform.
*/
$domainArray = Department::getDomains($departmentId);
$domainName = $domainArray[0]['name'];
switch ($hostname)
{
    case "APPLY.STAT.CMU.EDU":  
        $systemEmail = "scsstats@cs.cmu.edu";
        break;
    case "APPLYGRAD-INI.CS.CMU.EDU":  
        $systemEmail = "scsini@cs.cmu.edu";
        break;
    case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
        $systemEmail = "scsdiet@cs.cmu.edu";
        break;
    default:
        $systemEmail = "applygrad@cs.cmu.edu";
 }

$emailForm = new HTML_QuickForm('emailApplicants', 'post', $_SERVER['REQUEST_URI'],  '', null, true);
$emailForm->addElement('hidden', 'secret', '');
$emailForm->addElement('hidden', 'applicants', $applicants);
$emailForm->addElement('static', 'from', 'From:', 'Carnegie Mellon Admissions &lt;' . $systemEmail . '&gt;');
$emailForm->addElement('text', 'subject', 'Subject:', array('size' => 65, 'maxlength' => 100));

/*
* Process the applicant selector and email forms.
*/
$applicantView = new VW_AdminListBase();
$allSelected = $unsubmittedSelected = $unpaidSelected = '';
$messageContent = '';
$notification = 'PREVIEW: The following will be sent to the listed recipients.';
switch ($applicants) {
        
    case 'unsubmitted':
        
        $unsubmittedSelected = 'checked';
             
        $subjectElement = $emailForm->getElement('subject');
        $subjectElement->setValue('Applicant Submission Reminder');
        $subjectElement->freeze();
        
        $messageContent = html_entity_decode( 
            Department::getContent($departmentId, 'Applicant Submission Reminder')
            );
        $emailForm->addElement('hidden', 'message', $messageContent);
        $emailForm->addElement('static', 'messageDisplay', 'Message:', $messageContent); 
        $emailForm->addElement('submit', 'action', 'Send');
        
        $applicantRawArray = $applicantView->find('department', $departmentId, $periodId, 
                                                NULL, '', 'unsubmitted'); 
        break;
    
    case 'unpaid':
        
        $unpaidSelected = 'checked'; 
        
        $subjectElement = $emailForm->getElement('subject');
        $subjectElement->setValue('Applicant Payment Reminder');
        $subjectElement->freeze();
        
        $messageContent = html_entity_decode( 
            Department::getContent($departmentId, 'Applicant Payment Reminder')
            );
        $emailForm->addElement('hidden', 'message', $messageContent);
        $emailForm->addElement('static', 'messageDisplay', 'Message:', $messageContent);
        $emailForm->addElement('submit', 'action', 'Send');
        
        $applicantRawArray = $applicantView->find('department', $departmentId, $periodId, 
                                                NULL, '', 'submitted', 'all', 'unpaidUnwaived'); 
        break;
    
    case 'all':
    default:
        
        $allSelected = 'checked';
        
        $notification = '';
        
        $emailForm->addElement('textarea', 'message', 'Message:', array('rows' => 10, 'cols' => 50)); 
        $emailForm->addElement('submit', 'action', 'Preview');
        
        $applicantRawArray = $applicantView->find('department', $departmentId, $periodId, 
                                                NULL, '', 'submitted');
        break;
}

// Omit "hidden" applications from final applicant array.
$applicantArray = array();
foreach ($applicantRawArray as $applicantRaw) {
    if ($applicantRaw['hide'] == 0) {
        $applicantArray[] = $applicantRaw;    
    }    
}

$applicantCount = count($applicantArray);

/*
*  Process the email form
*/  
$emailForm->addRule('subject', 'Subject must not be empty', 'required', null, 'server', true);
$emailForm->addRule('message', 'Message must not be empty', 'required', null, 'server', true);
if ( $emailForm->validate() ) {
        
    $submitValue = $emailForm->getSubmitValue('action');

    switch($submitValue) {

        case 'New Email':
        
            if ($applicants == 'all') {
                $subjectElement = $emailForm->getElement('subject');
                $subjectElement->setValue('');
                $messageElement = $emailForm->getElement('message');
                $messageElement->setValue('');     
            } else {
                $sessionSecret = md5( uniqid( rand(), true) );
                $_SESSION['EMAIL_FORM_SECRET'] = $sessionSecret;
                $secretElement = $emailForm->getElement('secret');
                $secretElement->setValue($sessionSecret);
            }
            break;
        
        case 'Send':
                       
            $formSecret = $emailForm->exportValue('secret');
            if(isset($_SESSION['EMAIL_FORM_SECRET'])) {
            
                if ( strcasecmp( $formSecret, $_SESSION['EMAIL_FORM_SECRET']) === 0 ) {
    
                    foreach($applicantArray as $applicant) {
                    
                        $applicationId = $applicant['application_id'];
                        
                        $mailSieveString = '[' . $domainName . ':';
                        $mailSieveString .= implode( '-', getApplicantProgramIds($applicationId) ) . ':';
                        $mailSieveString .= $applicationId . ']';
                        
                        $recipient = $applicant['email'];
                        $subject = $emailForm->exportValue('subject');
                        $content = parseEmailTemplate2( 
                            $emailForm->exportValue('message'), 
                            array( array('userid', $applicant['email']) )
                        );
                        $content = '<pre>' . $content . '</pre>';
                        sendHtmlMail($systemEmail, $recipient, $subject, $content, 'reminder',
                            '', $mailSieveString);
                    }
                    
                    $notification = 'SENT: The following has been sent to the listed recipients.'; 
                    
                    $secretElement = $emailForm->getElement('secret');
                    $secretElement->setValue('');
                    unset($_SESSION['EMAIL_FORM_SECRET']);
                
                } else {
                
                    $notification = 'ERROR: Bad submission.';    
                }
              
            } else {
                
                $notification = 'EMAIL ALREADY SENT: Check the system email account for confirmation.';    
            }
            
            $emailForm->removeElement('action');
            $emailForm->addElement('submit', 'action', 'New Email');
            $emailForm->freeze();
            break;

        case 'Preview':
            
            $notification = 'PREVIEW: The following will be sent to the listed recipients.'; 
            
            $sessionSecret = md5( uniqid( rand(), true) );
            $_SESSION['EMAIL_FORM_SECRET'] = $sessionSecret;
            $secretElement = $emailForm->getElement('secret');
            $secretElement->setValue($sessionSecret);
            
            $emailForm->removeElement('action');
            $emailForm->removeElement('action');
            $emailForm->addElement('submit', 'action', 'Edit');
            $emailForm->addElement('submit', 'action', 'Send');
            $emailForm->freeze();
            break;

        case 'Edit': 
        default:            

            $sessionSecret = md5( uniqid( rand(), true) );
            $_SESSION['EMAIL_FORM_SECRET'] = $sessionSecret;
            $secretElement = $emailForm->getElement('secret');
            $secretElement->setValue($sessionSecret);

            $emailForm->removeElement('action');
            $emailForm->addElement('submit', 'action', 'Preview');               
    }

} elseif ($applicants != 'all') {
    
    $sessionSecret = md5( uniqid( rand(), true) );
    $_SESSION['EMAIL_FORM_SECRET'] = $sessionSecret;
    $secretElement = $emailForm->getElement('secret');
    $secretElement->setValue($sessionSecret);
}


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Email Applicants';

$pageCssFiles = array(
    '../css/administerApplications.css'
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<br/>
<span class="title">
<?php echo $unit->getName() ?>
<br/>
<?php echo $application_period_display; ?> 
<br/>
<span style="font-size: 18px;">Email Applicants</span>  
</span>
<br/><br/>

<div style="margin:5px; padding: 5px;"> 

    <div style="margin: 0px; float: left; width: 30%;">
    <div>
    <b>Send To:</b><br/>
    <form id="recipientsForm" name="recipientsForm" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
        <input type="radio" name="applicants" value="all" <?php echo $allSelected; ?>
            onClick="document.recipientsForm.submit(); return false;" /> All (Submitted)<br/>
        <input type="radio" name="applicants" value="unsubmitted"  <?php echo $unsubmittedSelected; ?>
            onClick="document.recipientsForm.submit(); return false;" /> Unsubmitted<br/>
        <input type="radio" name="applicants" value="unpaid"  <?php echo $unpaidSelected; ?>
            onClick="document.recipientsForm.submit(); return false;" /> Unpaid
    </form>
    <br/>
    Recipients (n=<?php echo $applicantCount; ?>):
    </div>
    <br/>
    <div style="width: 100%; height: 300px; overflow: auto;">
    <ul>
    <?php 
    $recipients = array();
    foreach ($applicantArray as $applicant) {
        echo '<li style="margin-bottom: 4px;">' . $applicant['name'] . '<br/>';
        echo '<i>' . $applicant['email'] . '</i></li>';
    }
    ?>
    </ul>
    </div>
    </div>
    
    <div style="margin-left: 10px; float: left; width: 60%;">
    <?php
    echo '<b>' . $notification . '</b><br/><br/>';
    $emailForm->display();
    ?>
    </div>

</div>

<div style="clear: both;"></div>
<br/>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>