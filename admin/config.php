 <?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$domains = array();
$admins = array();
$coordinator = "";
$domain = -1;
$expDate = "";
$expDate2 = "";
$price = 0.00;
$email = "";


//GET DOMAINS

$sql = "select distinct
domain.id,
domain.name
from
domain
left outer join lu_domain_department on lu_domain_department.domain_id = domain.id
left outer join lu_user_department on lu_user_department.department_id = lu_domain_department.department_id
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id ";
if($_SESSION['A_usertypeid'] != 0)
{
    if($_SESSION['A_usertypeid'] == 1 && isset($_SESSION['A_admin_depts']))
    {
        if(count($_SESSION['A_admin_depts'])>0)
        {
            $sql .= " WHERE (";
            for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
            {
                if ($_SESSION['A_admin_depts'][$i] == NULL) {
                    continue;
                }
                
                if($i > 0 && $i < count($_SESSION['A_admin_depts']) 
                    && $_SESSION['A_admin_depts'][$i-1] != NULL)
                {
                    $sql .= " or ";
                }

                $sql .= " lu_domain_department.department_id =".$_SESSION['A_admin_depts'][$i] ;
            }
            $sql .= ") ";
        
        }
        
    } else {
    
	    $sql .= " where lu_user_department.user_id=".$_SESSION['A_userid'];
        $sql .= " and(lu_users_usertypes.usertype_id = ".$_SESSION['A_usertypeid'].") ";
    }
}
$sql .= " order by domain.name";
//DebugBreak(); 
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($domains, $arr);
}
//GET ADMINS
$sql = "select
lu_users_usertypes.id,
users.firstname,
users.lastname
from lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id
where lu_users_usertypes.usertype_id=1 order by lastname,firstname";
//echo $sql;
$result = mysql_query($sql)	or die(mysql_error());
$ret = array();
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['lastname'] . ", " . $row['firstname']);
	array_push($admins, $arr);
}


if(isset($_POST['lbDomain']))
{
	$domain = htmlspecialchars($_POST['lbDomain']);
}else
{
	if(isset($_GET['id']))
	{
		$domain = htmlspecialchars($_GET['id']);
	}

}

if(isset($_POST['btnSubmit']))
{
	$expDate = htmlspecialchars($_POST['txtExpDate']);
	$expDate2 = htmlspecialchars($_POST['txtExpDate2']);
	$email = htmlspecialchars($_POST['txtEmail']);
	$price = floatval($_POST['txtPrice']);
	$coordinator = $_POST['lbCoordinator'];

	if($expDate == "")
	{
		$err .= "Submission expiration date is required.<br>";
	}
	if(check_Date($expDate) == false)
	{
		$err .= "Submission expiration date is invalid.<br>";
	}
	if($email == "")
	{
		$err .= "System email is required.<br>";
	}
	if($err == "")
	{
		//DO INSERT/UPDATE
		$doInsert = true;


		if($coordinator == "")
		{
			$err .= "Coordinator is required.<br>";
		}

		if($err == "")
		{
			$sql = "select id from systemenv where domain_id=".$domain;
			$result = mysql_query($sql) or die(mysql_error());
			while($row = mysql_fetch_array( $result ))
			{
				$doInsert = false;
			}
			if($doInsert == true)
			{
				$sql = "insert into systemenv
				(domain_id, expdate, expdate2, appbaseprice, coorduser_id, sysemail)
				values
				(".$domain.", '".formatMySQLdate($expDate,'/','-')."','".formatMySQLdate($expDate2,'/','-')."',".$price.", ".$coordinator.", '".$email."'  )";
				$result = mysql_query($sql) or die(mysql_error());
			}
			else
			{
				$sql = "update systemenv
				set
				expdate = '".formatMySQLdate($expDate,'/','-')."',
				expdate2 = '".formatMySQLdate($expDate2,'/','-')."',
				appbaseprice = ".$price.",
				coorduser_id = ".$coordinator.",
				sysemail = '".$email."'
				where domain_id=".$domain;
				$result = mysql_query($sql) or die(mysql_error());
			}
		}//END IF ERR
	}

}
//GET DATA
	$sql = "select * from systemenv where domain_id=".$domain;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$expDate = formatUSdate($row['expdate'],'-','/');
		$expDate2 = formatUSdate($row['expdate2'],'-','/');
		$coordinator = $row['coorduser_id'];
		$price = $row['appbaseprice'];
		$email = $row['sysemail'];
	}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'System Environment';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';    
?>
<form id="form1" action="" method="post">
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Configure System Variables <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<span class="errorSubtitle"><strong><?=$err?></strong></span><strong>Select Domain to Edit: </strong>
	<? showEditText($domain, "listbox", "lbDomain", $_SESSION['A_allow_admin_edit'], true, $domains); ?>
    <input name="btnRefresh" type="submit" class="tblItem" id="btnRefresh" value="Refresh" />
    <br />
	<table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="250" align="right"><strong>On-time submission expiration Date:</strong> </td>
        <td class="tblItem">
          <? showEditText($expDate, "textbox", "txtExpDate", $_SESSION['A_allow_admin_edit'], true,null,true,20); ?>
        </td>
      </tr>
      <tr>
        <td width="250" align="right"><strong>Extended Submission Expiration Date:<br />
          </strong>(Leave blank if N/A)        </td>
        <td class="tblItem">
          <? showEditText($expDate2, "textbox", "txtExpDate2", $_SESSION['A_allow_admin_edit'],false,null,true,20); ?>
        </td>
      </tr>
      <tr>
        <td width="250" align="right"><strong>Application Base Price: </strong></td>
        <td class="tblItem">
          <? showEditText($price, "textbox", "txtPrice", $_SESSION['A_allow_admin_edit'], true); ?>
        </td>
      </tr>
      <tr>
        <td width="250" align="right"><strong>Admission Coordinator:</strong> <br />
          (Must first be an administrator) </td>
        <td class="tblItem">
          <? showEditText($coordinator, "listbox", "lbCoordinator", $_SESSION['A_allow_admin_edit'],true, $admins); ?>
        </td>
      </tr>
      <tr>
        <td width="250" align="right"><strong>System Email Address:</strong> </td>
        <td><span class="tblItem">
          <? showEditText($email, "textbox", "txtEmail", $_SESSION['A_allow_admin_edit'], true,null,true,20); ?>
        </span></td>
      </tr>
      <tr>
        <td width="250" align="right">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="250" align="right">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="250" align="right">&nbsp;</td>
        <td class="subtitle">
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
        </td>
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>