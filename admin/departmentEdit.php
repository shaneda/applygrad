<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$err = "";
$schools = array();
$school = "";
$name = "";
$oracle = "";
$rank = 0;
$ccemail = "";
$id = -1;
$round1 = 0;
$round2 = 0;
$round3 = 0;
$round4 = 0;
$final = 0;
$semiblind_review = 0;

$acontent = array(
	array(2,'Invitation Letter'),
	array(2,'Faculty Cover Sheet'),
	array(2,'Student Cover Sheet'),
	array(2,'Women@IT Letter'),
	array(3,'Rejection Letter'),
	array(4,'Submission Letter'),
	array(4,'Page Update Letter'),
	array(4,'Recommendation Letter'),
	array(4,'Recommendation Reminder Letter'),
	array(4,'Forgot Password Letter'),
	array(1,'Home Page'),
	array(1,'Home Page - Application Complete'),
	array(1,'Index Page'),
	array(1,'Index Page for Recommenders'),
	array(1,'Logout Page for Recommenders'),
	array(1,'New Student Account Confirmation'),
	array(1,'Applicant Submission Reminder'),
	array(1,'FAQ'),
	array(1,'Biographical Information'),
	array(1,'Programs'),
	array(1,'Test Scores'),
	array(1,'Resume and Statement'),
	array(1,'Recommenders'),
	array(1,'Recommendation Main Page'),
	array(1,'Recommendation Upload Page'),
	array(1,'Supplemental Information'),
	array(1,'College/University'),
	array(1,'Instructions'),
	array(1,'Offline message'),
	array(1,'Payment Mail Page'),
	array(1,'Payment Credit Page')
);


if(isset($_POST['txtdepartmentId']))
{
	$id = htmlspecialchars($_POST['txtdepartmentId']);
}else
{
	if(isset($_GET['id']))
	{
		$id = htmlspecialchars($_GET['id']);
	}
}
if(isset($_POST['btnSubmit']))
{
	$name = htmlspecialchars($_POST['txtName']);
	$school = htmlspecialchars($_POST['lbSchool']);
	$oracle = htmlspecialchars($_POST['txtOracle']);
	$rank = intval($_POST['txtRank']);
	$ccemail = htmlspecialchars($_POST['txtCcEmail']);
	if(isset($_POST['chkRound1']))
	{
		$round1 = 1;
	}
	if(isset($_POST['chkRound2']))
	{
		$round2 = 1;
	}
    if(isset($_POST['chkRound3']))
    {
        $round3 = 1;
    }
    if(isset($_POST['chkRound4']))
    {
        $round4 = 1;
    }
	if(isset($_POST['chkFinal']))
	{
		$final = 1;
	}
	if(isset($_POST['chkSemiblind_review']))
	{
		$semiblind_review = 1;
	}
	if($name == "")
	{
		$err .= "Name is required.<br>";
	}
	if($school == "")
	{
		$err .= "School is required.<br>";
	}
	if($err == "")
	{
		if($id > -1)
		{
			$sql = "update department set name='".$name."', parent_school_id=".$school.", 
            oraclestring='".$oracle."', rank=".$rank.", 
			cc_email = '".$ccemail."',enable_round1=".$round1.", 
            enable_round2=".$round2.",enable_round3=".$round3.", 
            enable_final = ".$final.",enable_round4=".$round4.",
			semiblind_review=".$semiblind_review."
			where id=".$id;
			mysql_query($sql)or die(mysql_error().$sql);
			echo $sql . "<br>";
		}
		else
		{
			$sql = "insert into department(name, parent_school_id, oraclestring, rank, cc_email, 
            enable_round1, enable_round2, enable_round3, enable_final, enable_round4,
			semiblind_review)values('".$name."', ".$school.", '"
            .$oracle."', ".$rank.", '".$ccemail."', "
            .$round1 . ", " . $round2 . ", " . $round3 . ", " . $final . ", " . $round4 . ", "
            .$semiblind_review.")";
			mysql_query($sql)or die(mysql_error().$sql);
			$id = mysql_insert_id();
			//echo $sql . "<br>";
		}
		
		for($i = 0; $i < count($acontent); $i++)
		{
			$sql = "SELECT id FROM content where name='".$acontent[$i][1]."' and department_id=".$id;
			$result = mysql_query($sql) or die(mysql_error() . $sql);
			$cid = -1;
			while($row = mysql_fetch_array( $result ))
			{
				$cid = $row['id'];
			}
			if($cid == -1)
			{
				$sql = "insert into content (name, contenttype_id,department_id)values('".$acontent[$i][1]."',".$acontent[$i][0].",".$id.")";
				mysql_query($sql) or die(mysql_error() . $sql);
			}
		}		
		
		header("Location: departments.php");
	}
}

//GET DATA
$sql = "select id,name
from schools
order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($schools, $arr);
}

$sql = "SELECT id,name,parent_school_id,oraclestring, rank, cc_email,
enable_round1, enable_round2, enable_round3, enable_final, enable_round4, semiblind_review   
FROM department where id=".$id;
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$id = $row['id'];
	$name = $row['name'];
	$school = $row['parent_school_id'];
	$oracle = $row['oraclestring'];
	$rank = intval($row['rank']);
	$ccemail = $row['cc_email'];
	$round1 = $row['enable_round1'];
	$round2 = $row['enable_round2'];
    $round3 = $row['enable_round3'];
    $round4 = $row['enable_round4'];
	$semiblind_review = $row['semiblind_review'];
	$final = $row['enable_final'];
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Department';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
 
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Department</span><br />
<br />
	<span class="errorSubtitle"><?=$err?></span>
<a href="departments.php">&lt;&lt; Return to department list</a>
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr>
        <td width="200" align="right"><strong>
          <input name="txtdepartmentId" type="hidden" id="txtdepartmentId" value="<?=$id?>" />
          Name:</strong></td>
        <td class="tblItem">
          <? showEditText($name, "textbox", "txtName", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>        </td>
      </tr>
      <tr>
        <td align="right"><strong>Parent School:<br />
          </strong><a href="schoolEdit.php">Add new</a>         </td>
        <td class="tblItem"><? showEditText($school, "listbox", "lbSchool", $_SESSION['A_allow_admin_edit'], true, $schools); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Oracle Charge String:</strong> </td>
        <td >
          <? showEditText($oracle, "textbox", "txtOracle", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?>       </td>
      </tr>
      <tr>
        <td align="right"><strong>Credit Card Contact Email:</strong> </td>
        <td ><? showEditText($ccemail, "textbox", "txtCcEmail", $_SESSION['A_allow_admin_edit'],true,null,true,30); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Rank Order:</strong> </td>
        <td >
          <? showEditText($rank, "textbox", "txtRank", $_SESSION['A_allow_admin_edit'],true,null,true,10); ?>       </td>
      </tr>
      <tr>
        <td align="right"><strong>Enable Round 1: </strong></td>
        <td ><? showEditText($round1, "checkbox", "chkRound1", $_SESSION['A_allow_admin_edit'],true,null,true); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Enable Round 2:</strong> </td>
        <td ><? showEditText($round2, "checkbox", "chkRound2", $_SESSION['A_allow_admin_edit'],true,null,true); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Enable Round 3:</strong> </td>
        <td ><? showEditText($round3, "checkbox", "chkRound3", $_SESSION['A_allow_admin_edit'],true,null,true); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Enable Round 4:</strong> </td>
        <td ><? showEditText($round4, "checkbox", "chkRound4", $_SESSION['A_allow_admin_edit'],true,null,true); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Enable Final:</strong> </td>
        <td ><? showEditText($final, "checkbox", "chkFinal", $_SESSION['A_allow_admin_edit'],true,null,true); ?></td>
      </tr>
      <tr>
        <td align="right"><strong>Semi-Blind Review:</strong> </td>
        <td ><? showEditText($semiblind_review, "checkbox", "chkSemiblind_review", $_SESSION['A_allow_admin_edit'],true,null,true); ?></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td >
          <? showEditText("Save", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>        </td>
      </tr>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>