<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$programs = array();
$interests = array();
$interest = array();
$program = "-1";

if(isset($_POST['lbProgram']))
{
	$program = htmlspecialchars($_POST['lbProgram']);
}else
{
	if(isset($_GET['id']))
	{
		$program = htmlspecialchars($_GET['id']);
	}

}
if(isset($_POST['btnSubmit']))
{
	//SAVE DATA
	$vars = $_POST;
	$itemId = -1;

	mysql_query("DELETE FROM lu_programs_interests WHERE program_id=".$program)
	or die(mysql_error().$sql);
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'chkInterest') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				$sql = "insert into lu_programs_interests(program_id, interest_id)values(".$program.",".$itemId.")";
				mysql_query($sql)
				or die(mysql_error().$sql);
			}//END IF
		}//END IF

	}//END FOR

}
//PULL DATA FOR SELECTED PROGRAM
$sql = "select interest_id from lu_programs_interests where program_id=".$program;
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row[0]);
	array_push($interest, $arr);
}

// PROGRAMS
$sql = "SELECT
programs.id,
fieldsofstudy.name,
degree.name as degreename
FROM programs
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
inner join degree on degree.id = programs.degree_id
order by name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$dept = "";
	$depts = getDepartments($row[0]);
	if(count($depts) > 1)
	{
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$dept .= $depts[$i][1];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row[2]. " in " . $row[1].$dept);
	array_push($programs, $arr);
}

// INTERESTS
$sql = "SELECT
id, name
FROM interest
order by name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row[1]);
	array_push($interests, $arr);
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT
	department.id,
	department.name
	FROM lu_programs_departments
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error().$sql);

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Interest';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit Interest</span><br />
<br />
	Program:<br />
	<? showEditText($program, "listbox", "lbProgram", $_SESSION['A_allow_admin_edit'], true, $programs); ?>
	<input name="btnRefresh" type="submit" class="tblItem" id="btnRefresh" value="Refresh" />
	<br />
	Interests:<br />
	<? showEditText($interest, "checklist", "chkInterest", $_SESSION['A_allow_admin_edit'], true, $interests); ?>
	<br />
	<br />
	<input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="Submit" />
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>