<?php
// Include the config class, which also has the 
// session and db includes.
include("../inc/config.php");

// DB, Payment includes
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
include '../classes/class.Payment.php';

session_start();

$referer = filter_input(INPUT_POST, 'referer', FILTER_SANITIZE_STRING);
$queryString = filter_input(INPUT_POST, 'queryString', FILTER_SANITIZE_STRING);
$payments = filter_input(INPUT_POST, 'payments', FILTER_VALIDATE_BOOLEAN, FILTER_REQUIRE_ARRAY);
$updatePaymentsValue = filter_input(INPUT_POST, 'updatePaymentsValue', FILTER_SANITIZE_STRING);

if ($referer) {
    $href = $referer;
    if ($queryString) {
        $href .= '?' . $queryString;
    }
    echo '<a href="' . $href . '">&laquo; Back</a>';
} else {
    $href = 'home.php';
    header('Location: ' . $href);
    exit;
}

if ($payments && $updatePaymentsValue) {

    foreach (array_keys($payments) as $paymentId) {
     
        $payment = new Payment(intval($paymentId));
        $applicationId = $payment->getApplicationId();        
        $paymentManager = new PaymentManager($applicationId);

        $paymentValues = array(
            'payment_id' => $payment->getId(),
            'payment_status' => $updatePaymentsValue,
            'last_mod_user_id' => intval($_SESSION['A_usermasterid']) 
            );
   
        $paymentManager->savePayment($paymentValues);
    }  
}

header('Location: ' . $href);
exit;
?>