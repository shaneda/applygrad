<?php 
include "../inc/config.php";
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_RegistrationsApplicantIndex.php";

if ( isset($_REQUEST['unit']) ) {
    $unit = $_REQUEST['unit'];
} else {
    $unit = NULL;
}

if ( isset($_REQUEST['unitId']) ) {
    $unitId = $_REQUEST['unitId'];
} else {
    $unitId = NULL;
}

if ( isset($_REQUEST['periodId']) ) {
    $periodId = $_REQUEST['periodId'];
} else {
    $periodId = NULL;
}

if ( isset($_REQUEST['q']) ) {
    $searchString = $_REQUEST['q'];
} else {
    $searchString = "";
}

$userIndex = new VW_RegistrationsApplicantIndex(); 
$resultArray = $userIndex->find($unit, $unitId, $periodId, $searchString);

foreach ($resultArray as $row) {
    echo implode("|", $row) . "\n";
}
 
?>