 <?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
include '../inc/specialCasesAdmin.inc.php';

$allowUser = true;
$departmentName = "";
$s = -1;
$sType = "(all)";

$id = -1;
if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
}

// PLB added periodId 11/24/09
$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
}

	
	$sql = "select name from department where id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$departmentName = $row['name'];
	}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$isIsreeDepartment = isIsreeDepartment($id);
if ($isIsreeDepartment) {
    $pageTitle = 'Registered Students';    
} else {
    $pageTitle = 'Admitted Applicants';    
}


$pageCssFiles = array(
    '../css/applicantGroups_assign.css' 
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

// period
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    if ($periodName) {
        $applicationPeriodDisplay = $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay = $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay = $periodDates;     
    }
}
?> 
<div id="pageHeading">
<div id="departmentName">
<?php
echo $departmentName;
if ($applicationPeriodDisplay) {
    echo '<br/>' . $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle"><?php echo $pageTitle; ?></div>
</div> 

<form id="form1" name="form1" action="" method="post"> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">

	<?
	include '../inc/C_Spreadsheet_01a.php.inc';
	$spst = new Spreadsheet;
   
    /*
    * NOTE: for this #!%$&@!!@ spreadsheet business to work, 
    * the first column must be the user_id and the second column 
    * must be the application id. NO EXCEPTIONS. 
    */
    $sql = "SELECT 
    lu_users_usertypes.id as id,
    application.id AS application_id,";
    if (isRiMsRtChinaDepartment($id))
    {
        $sql .= " CONCAT(UPPER(users.lastname), ', ',users.firstname) AS name,";
    }
    else
    {
        $sql .= " CONCAT(users.lastname, ', ',users.firstname) AS name,";   
    }
    $sql .= " users.email, ";

    if ( ($id == 6) || isMseMsitDepartment($id) ) {
    
        $sql .= "GROUP_CONCAT(
                      DISTINCT
                      CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                      ORDER BY programs.id
                      ) AS apply_program,
                /*
                GROUP_CONCAT(
                  DISTINCT
                  CONCAT(' ', admit_degree.name, ' ', admit_programs.linkword, ' ', admit_fieldsofstudy.name)
                  ORDER BY admit_programs.id
                  ) AS admit_program,
                */
                IF( admit_programs.id,
                    GROUP_CONCAT(
                    DISTINCT
                    CONCAT(' ', admit_degree.name, ' ', admit_programs.linkword, ' ', admit_fieldsofstudy.name)
                    ORDER BY admit_programs.id
                    ),
                    GROUP_CONCAT(
                    DISTINCT
                    CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                    ORDER BY programs.id
                    )  
                ) AS admit_program,
                ";
    } 
    elseif (isIniDepartment($id)) 
    {
        $sql .= "GROUP_CONCAT(
                      DISTINCT
                      CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                      ORDER BY programs.id
                ) AS apply_program,
                GROUP_CONCAT(
                    DISTINCT
                    IF (application_decision_ini.admission_status = 2,
                        CONCAT(' ', admit_degree2.name, ' ', admit_programs2.linkword, ' ', admit_fieldsofstudy2.name),
                        NULL)
                ) AS admit_program,
                ";
    }
    elseif (isDesignDepartment($id)) 
    {
        $sql .= "GROUP_CONCAT(
                      DISTINCT
                      CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                      ORDER BY programs.id
                ) AS apply_program,
                GROUP_CONCAT(
                    DISTINCT
                    IF (application_decision_design.admission_status = 2,
                        CONCAT(' ', admit_degree2.name, ' ', admit_programs2.linkword, ' ', admit_fieldsofstudy2.name),
                        NULL)
                ) AS admit_program,
                ";            
    } 
    else 
    {

        $sql .= "GROUP_CONCAT(
                  DISTINCT
                  CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                  ORDER BY programs.id
                  ) AS program,
                  ";        
    }
    
    $sql .= "lu_application_programs.faccontact,
    lu_application_programs.stucontact

    FROM application
    INNER JOIN period_application ON application.id = period_application.application_id

    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    LEFT OUTER JOIN countries ON users_info.cit_country = countries.id

    INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
    INNER JOIN programs ON lu_application_programs.program_id = programs.id
    INNER JOIN degree ON programs.degree_id = degree.id
    INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
    INNER JOIN lu_programs_departments
      ON lu_application_programs.program_id = lu_programs_departments.program_id
    LEFT OUTER JOIN lu_application_interest
      ON lu_application_programs.id = lu_application_interest.app_program_id
    LEFT OUTER JOIN interest ON lu_application_interest.interest_id = interest.id

    LEFT OUTER JOIN application_decision
      ON application.id = application_decision.application_id
      AND lu_application_programs.program_id = application_decision.program_id
    LEFT OUTER JOIN programs AS admit_programs
      ON application_decision.admission_program_id = admit_programs.id
    LEFT OUTER JOIN degree AS admit_degree
      ON admit_programs.degree_id = admit_degree.id
    LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy
      ON admit_programs.fieldofstudy_id = admit_fieldsofstudy.id";
      
    if (isIniDepartment($id))
    {
        $sql .= " LEFT OUTER JOIN application_decision_ini
              ON lu_application_programs.application_id = application_decision_ini.application_id
            LEFT OUTER JOIN programs AS admit_programs2
              ON admit_programs2.id = application_decision_ini.admission_program_id
            LEFT OUTER JOIN degree AS admit_degree2
              ON admit_degree2.id = admit_programs2.degree_id
            LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy2
              ON admit_fieldsofstudy2.id = admit_programs2.fieldofstudy_id
            WHERE application.submitted = 1
            AND (lu_application_programs.admission_status = 2
              OR application_decision_ini.admission_status = 2)
            AND lu_programs_departments.department_id = " . $id;
    }
    elseif (isDesignDepartment($id))
    {
        $sql .= " LEFT OUTER JOIN application_decision_design
              ON lu_application_programs.application_id = application_decision_design.application_id
            LEFT OUTER JOIN programs AS admit_programs2
              ON admit_programs2.id = application_decision_design.admission_program_id
            LEFT OUTER JOIN degree AS admit_degree2
              ON admit_degree2.id = admit_programs2.degree_id
            LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy2
              ON admit_fieldsofstudy2.id = admit_programs2.fieldofstudy_id
            WHERE application.submitted = 1
            AND (lu_application_programs.admission_status = 2
              OR application_decision_design.admission_status = 2)
            AND lu_programs_departments.department_id = " . $id;
    }
    else
    {
        $sql .= " WHERE application.submitted = 1
            AND (lu_application_programs.admission_status = 2
              OR application_decision.admission_status = 2)
            AND lu_programs_departments.department_id = " . $id;    
    }

    if ($periodId) {
        $sql .= " AND period_application.period_id = " .  $periodId;
    } 
    $sql .= " GROUP BY application.id ";
	$spst->sql = $sql;
	$spst->sqlOrderBy = "name";
    $spst->showFilter = false;
    if ($_SESSION['A_usertypeid'] == 11)  {
	    // Student contacts should not be getting a link to this page, but if
        // they do end up here, they should get a link to the minimal application summary. 
        $spst->editPage = "userroleEdit_student_formatted.php";
    } else {
        $spst->editPage = "../review/userroleEdit_student_print.php";
    }
           
    $spst->doSpreadSheet();
?>

	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>
