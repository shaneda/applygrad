<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/adminTemplate2.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/AdminStyles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$reqs = array();
$compReqs = array();
$apps = array();
$progs = array();
$scores = array();
$universities = array();
$recommenders = array();
$statii = array(array('0', 'Unsubmitted'), array('1','Submitted'));
$status = "";
$appName = "";
$firstname = "";
$lastname = "";
$country = "";
$email = "";
$appid = -1;
$uid = -1;
$umasterid = -1;
$guid = "";
$amt="";
$domain = 1;
$gender = "";
$appSent = 0;

if(isset($_GET['id']))
{
	$uid = intval(htmlspecialchars($_GET['id']));
	
}
if(isset($_POST['userid']))
{
	$uid = intval(htmlspecialchars($_POST['userid']));
	
}
if(isset($_POST['usermasterid']))
{
	$umasterid = intval(htmlspecialchars($_POST['usermasterid']));
	
}
$vars = $_POST;
foreach($vars as $key => $value)
{

	//echo $key." ".$value."<br>";
	if( strstr($key, 'btnPaymentStatus') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
		
		$paid = 0;
		$date="";
		$amt = floatval($_POST["txtPayAmt_".$itemId]);
		if($_POST["txtPaymentStatus_".$itemId] == 1)
		{
			$paid = 0;
			$date = date("Y-m-d h:i:s");
		}else
		{
			$paid = 1;
			$date = date("Y-m-d h:i:s");
			$amt = floatval($_POST["txtPayAmt_".$itemId]);
		}
		
		$sql = "update application set paid=". $paid.", paymentdate='".$date."', paymentamount=".$amt." where id=".$itemId;
		mysql_query($sql) or die(mysql_error());
		//echo $sql;
	}
	if( strstr($key, 'btnWaiveStatus') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
		$waived = 0;
		if($_POST["txtWaiveStatus_".$itemId] == 1)
		{
			$waived = 0;
		}else
		{
			$waived = 1;
		}
		$sql = "update application set waive=". $waived." where id=".$itemId;
		mysql_query($sql) or die(mysql_error());
		
	}
	if( strstr($key, 'btnResetPayment') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];		
		$sql = "update application set paid=0, paymentamount=NULL,paymentdate=NULL,paymenttype=NULL where id=".$itemId;
		mysql_query($sql) or die(mysql_error());
		
	}
	//btnUpdatePayment_
	if( strstr($key, 'btnUpdatePayment') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];

		$date="";
		$amt = floatval($_POST["txtPayAmt_".$itemId]);
		
		$paid = 1;
		$date = date("Y-m-d h:i:s");
		$amt = floatval($_POST["txtPayAmt_".$itemId]);
		
		$sql = "update application set paymentdate='".$date."', paymentamount=".$amt." where id=".$itemId;
		mysql_query($sql) or die(mysql_error());
		//echo $sql;
	}
	if( strstr($key, 'btnScoreRec') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
		$modid = $arr[2];
		$submitted = 0; 
		$type = $_POST["txtScoreRecType_".$itemId. "_".$modid];
		
		if($_POST["txtScoreRec_".$itemId."_".$modid] == 1)
		{
			$submitted = 0;
		}else
		{
			$submitted = 1;
			$date = date("Y-m-d h:i:s");
		}
		$sql = "";
		switch($type)
		{
			case "GRE General":
				$sql = "update grescore set scorereceived=". $submitted." where id=".$itemId;
				break;
			case "GRE Subject":
				$sql = "update gresubjectscore set scorereceived=". $submitted." where id=".$itemId;
				break;
			case "TOEFL IBT":
				$sql = "update toefl set scorereceived=". $submitted." where id=".$itemId;
				break;
			case "TOEFL CBT":
				$sql = "update toefl set scorereceived=". $submitted." where id=".$itemId;
				break;
			case "TOEFL PBT":
				$sql = "update toefl set scorereceived=". $submitted." where id=".$itemId;
				break;
		}
		mysql_query($sql) or die(mysql_error());
		
	}
	
	if( strstr($key, 'btnTranscriptRec') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
		$submitted = 0; 
		if($_POST["txtScoreRec_".$itemId] == 1)
		{
			$submitted = 0;
		}else
		{
			$submitted = 1;
		}
		$sql = "update usersinst set transscriptreceived=". $submitted." where id=".$itemId;
		mysql_query($sql) or die(mysql_error());
		
	}
	
	if( strstr($key, 'btnSubmitStatus') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
		
		$submitted = 0;
		$date="";
		if($_POST["txtSubmitStatus_".$itemId] == 1)
		{
			$submitted = 0;
		}else
		{
			$submitted = 1;
			$date = date("Y-m-d h:i:s");
		}
		$sql = "update application set submitted=". $submitted.", submitted_date='".$date."' where id=".$itemId;
		mysql_query($sql) or die(mysql_error());
		
	}
	
	if( strstr($key, 'btnPromote') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
		
		$promoted = 0;
		$date="";
		if($_POST["txtPromoteStatus_".$itemId] == 1 || $_POST["txtPromoteStatus_".$itemId] == 2)
		{
			$promoted = 0;
		}else
		{
			$promoted = 1;
			$date = date("Y-m-d h:i:s");
		}
		$sql = "update lu_application_programs set round2=". $promoted." where application_id= ".intval($_POST['txtAppId'])." and program_id=".$itemId;
		//echo $sql;
		mysql_query($sql) or die(mysql_error());
		
	}
	
	if( strstr($key, 'btnDemote') !== false ) 
	{
		$arr = split("_", $key);
		$itemId = $arr[1];
	
		$promoted = 2;
		$date = date("Y-m-d h:i:s");
		
		$sql = "update lu_application_programs set round2=". $promoted." where application_id= ".intval($_POST['txtAppId'])." and program_id =".$itemId;
		//echo $sql;
		mysql_query($sql) or die(mysql_error());
		
	}
	
}//END FOR

if(isset($_POST['btnSubmit']))
{
	$appid = intval($_POST['txtAppId']);
	//UPDATE APPLICATION
	if(isset($_POST['chkSent']))
	{
		$appSent = 1;
	}
	$sql = "update application set sent_to_program=". $appSent." where id=".$appid;
	mysql_query($sql) or die(mysql_error());

	//UPDATE SCORES
	$vars = $_POST;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'txtScoreRecType') !== false ) 
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			$modid = $arr[2];
			$type = $_POST["txtScoreRecType_".$itemId. "_".$modid];
			
			$sql = ""; 
			switch($type)
			{
				case "GRE General":
					$greGenVerScore = "NULL";
					$greGenVerPer = "NULL";
					$greGenQuanScore = "NULL";
					$greGenQuanPer = "NULL";
					$greGenWrScore = "NULL";
					$greGenWrPer = "NULL";
					$greGenAnScore = "NULL";
					$greGenAnPer = "NULL";
					
					
					
					if($_POST["txtGreGenVerScore_".$itemId. "_".$modid]  != "")
					{
						$greGenVerScore = strToNum($_POST["txtGreGenVerScore_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenVerPer_".$itemId. "_".$modid]  != "")
					{
						$greGenVerPer = strToNum($_POST["txtGreGenVerPer_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenQuanScore_".$itemId. "_".$modid]  != "")
					{
						$greGenQuanScore = strToNum($_POST["txtGreGenQuanScore_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenQuanPer_".$itemId. "_".$modid]  != "")
					{
						$greGenQuanPer = strToNum($_POST["txtGreGenQuanPer_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenWrScore_".$itemId. "_".$modid]  != "")
					{
						$greGenWrScore = floatval($_POST["txtGreGenWrScore_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenWrPer_".$itemId. "_".$modid] != "")
					{
						$greGenWrPer = strToNum($_POST["txtGreGenWrPer_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenAnScore_".$itemId. "_".$modid]  != "")
					{
						$greGenAnScore = strToNum($_POST["txtGreGenAnScore_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreGenAnPer_".$itemId. "_".$modid]  != "")
					{
						$greGenAnPer = strToNum($_POST["txtGreGenAnPer_".$itemId. "_".$modid]);
					}
				
					$sql = "update grescore set
					verbalscore=".$greGenVerScore.",             
					verbalpercentile=".$greGenVerPer.",        
					quantitativescore=".$greGenQuanScore.",            
					quantitativepercentile=".$greGenQuanPer.",              
					analyticalscore=".$greGenAnScore.",            
					analyticalpercentile=".$greGenAnPer.",            
					analyticalwritingscore=".$greGenWrScore.",          
					analyticalwritingpercentile=".$greGenWrPer." 
					where id=".$itemId;
					break;
				case "GRE Subject":
					$greSubName = "NULL";
					$greSubScore = "NULL";
					$greSubPerc = "NULL";
					if($_POST["txtGreSubName_".$itemId. "_".$modid] != "")
					{
						$greSubName = htmlspecialchars($_POST["txtGreSubName_".$itemId. "_".$modid]);
					}
					if($_POST["txtGreSubScore_".$itemId. "_".$modid] != "")
					{
						$greSubScore = $_POST["txtGreSubScore_".$itemId. "_".$modid];
					}
					if($_POST["txtGreSubPer_".$itemId. "_".$modid] != "")
					{
						$greSubPerc = $_POST["txtGreSubPer_".$itemId. "_".$modid];
					}
					$sql = "update gresubjectscore set
					name='".$greSubName."', 
					score=".$greSubScore.", 
					percentile=".$greSubPerc."
					where id=".$itemId;
					break;
				case "TOEFL IBT":
					$sect1 = "NULL";
					$sect2 = "NULL";
					$sect3 = "NULL";
					$essay = "NULL";
					$scoreTotal = "NULL";
					
					if($_POST["txtToeflIntSect1_".$itemId. "_".$modid] != "")
					{
						$sect1 = strToNum($_POST["txtToeflIntSect1_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflIntSect2_".$itemId. "_".$modid] != "")
					{
						$sect2 = strToNum($_POST["txtToeflIntSect2_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflIntSect3_".$itemId. "_".$modid] != "")
					{
						$sect3 = strToNum($_POST["txtToeflIntSect3_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflIntEssay_".$itemId. "_".$modid] != "")
					{
						$essay = floatval($_POST["txtToeflIntEssay_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflIntTotal_".$itemId. "_".$modid] != "")
					{
						$scoreTotal = strToNum($_POST["txtToeflIntTotal_".$itemId. "_".$modid]);
					}
					$sql = "update toefl set
					section1=".$sect1.",         
					section2=".$sect2.",           
					section3=".$sect3.",              
					essay=".$essay.",              
					total=".$scoreTotal." 
					where id=".$itemId;
					
					break;
				case "TOEFL CBT":
					$sect1 = "NULL";
					$sect2 = "NULL";
					$sect3 = "NULL";
					$essay = "NULL";
					$scoreTotal = "NULL";
					
					if($_POST["txtToeflCompSect1_".$itemId. "_".$modid] != "")
					{
						$sect1 = strToNum($_POST["txtToeflCompSect1_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflCompSect2_".$itemId. "_".$modid] != "")
					{
						$sect2 = strToNum($_POST["txtToeflCompSect2_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflCompSect3_".$itemId. "_".$modid] != "")
					{
						$sect3 = strToNum($_POST["txtToeflCompSect3_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflCompEssay_".$itemId. "_".$modid] != "")
					{
						$essay = floatval($_POST["txtToeflCompEssay_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflCompTotal_".$itemId. "_".$modid] != "")
					{
						$scoreTotal = strToNum($_POST["txtToeflCompTotal_".$itemId. "_".$modid]);
					}
					$sql = "update toefl set
					section1=".$sect1.",         
					section2=".$sect2.",           
					section3=".$sect3.",              
					essay=".$essay.",              
					total=".$scoreTotal." 
					where id=".$itemId;
					break;
				case "TOEFL PBT":
					$sect1 = "NULL";
					$sect2 = "NULL";
					$sect3 = "NULL";
					$essay = "NULL";
					$scoreTotal = "NULL";
					
					if($_POST["txtToeflPaperSect1_".$itemId. "_".$modid] != "")
					{
						$sect1 = strToNum($_POST["txtToeflPaperSect1_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflPaperSect2_".$itemId. "_".$modid] != "")
					{
						$sect2 = strToNum($_POST["txtToeflPaperSect2_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflPaperSect3_".$itemId. "_".$modid] != "")
					{
						$sect3 = strToNum($_POST["txtToeflPaperSect3_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflPaperEssay_".$itemId. "_".$modid] != "")
					{
						$essay = floatval($_POST["txtToeflPaperEssay_".$itemId. "_".$modid]);
					}
					if($_POST["txtToeflPaperTotal_".$itemId. "_".$modid] != "")
					{
						$scoreTotal = strToNum($_POST["txtToeflPaperTotal_".$itemId. "_".$modid]);
					}
					$sql = "update toefl set
					section1=".$sect1.",         
					section2=".$sect2.",           
					section3=".$sect3.",              
					essay=".$essay.",              
					total=".$scoreTotal." 
					where id=".$itemId;
			
					break;
			}
			mysql_query($sql) or die(mysql_error().$sql);
			
		}//end if		
	}//end for
		
	
}//end if submit

//GET USER INFO
$sql = "SELECT id, name, paid, submitted, submitted_date,paymentdate,paymentamount,waive,
sent_to_program,round2
from application where user_id = ". $uid;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['paid']);
	array_push($arr, $row['submitted']);
	array_push($arr, formatUSdate($row['submitted_date']));
	array_push($arr, formatUSdate($row['paymentdate']));
	array_push($arr, $row['paymentamount']);
	array_push($arr, $row['waive']);
	array_push($arr, $row['sent_to_program']);
	array_push($arr, $row['round2']);
	array_push($apps, $arr);
}

$sql = "SELECT users.id as umasterid, firstname, lastname, email, countries.name as country, guid, gender 
from lu_users_usertypes 
inner join users on users.id = lu_users_usertypes.user_id 
inner join users_info on users_info.user_id = lu_users_usertypes.id
left outer join countries on countries.id = address_perm_country
where lu_users_usertypes.id = ". $uid;
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	$firstname = $row['firstname'];
	$lastname = $row['lastname'];
	$country = $row['country'];
	$email = $row['email'];
	$guid = $row['guid'];
	$gender = $row['gender'];
	$umasterid = $row['umasterid'];
}


function getReqs($appid)
{
	$ret = array();
	$appid = intval(htmlspecialchars($appid));
	//GET REQUIREMENTS
	/*
    $sql = "SELECT applicationreqs.id, 
	applicationreqs.name, 
	applicationreqs.linkname, 
	application.name as appname, 
	lu_application_appreqs.id as compreqid, 
	lu_application_appreqs.last_modified 
	FROM `lu_application_programs`
	inner join programs on programs.id = lu_application_programs.`program_id`
	inner join degree on degree.id = programs.degree_id
	inner join lu_degrees_applicationreqs on lu_degrees_applicationreqs.degree_id = programs.degree_id
	inner join applicationreqs on applicationreqs.id = lu_degrees_applicationreqs.appreq_id
	inner join application on application.id = lu_application_programs.application_id
	left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
		and lu_application_appreqs.req_id = applicationreqs.id
	where lu_application_programs.application_id = " . $appid . " group by applicationreqs.id";
	*/
    $sql = "SELECT applicationreqs.id, 
    applicationreqs.name, 
    applicationreqs.linkname, 
    application.name as appname, 
    lu_application_appreqs.id as compreqid, 
    lu_application_appreqs.last_modified 
    FROM `lu_application_programs`
    inner join programs on programs.id = lu_application_programs.`program_id`
    inner join programs_applicationreqs on programs_applicationreqs.programs_id = programs.id
    inner join applicationreqs on applicationreqs.id = programs_applicationreqs.applicationreqs_id
    inner join application on application.id = lu_application_programs.application_id
    left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
        and lu_application_appreqs.req_id = applicationreqs.id
    where lu_application_programs.application_id = " . $appid . " group by applicationreqs.id";
    $result = mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($arr, $row['linkname']);	
		array_push($arr, $row['compreqid']);	
		array_push($arr, $row['last_modified']);	
		array_push($ret, $arr);
	}
	return $ret;
}

function getUniversities($uid)
{
	$ret = array();
	$sql = "SELECT 
	usersinst.id,institutes.name,date_entered,
	date_grad,date_left,degree,
	major1,major2,major3,minor1,minor2,gpa,gpa_major,
	gpa_scale,transscriptreceived,datafile_id,educationtype,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM usersinst 
	left outer join institutes on institutes.id = usersinst.institute_id
	left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
	where usersinst.user_id=".$uid. " order by usersinst.educationtype";
	$result = mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($arr, formatUSdate2($row['date_entered'],'-','/')  );
		array_push($arr, formatUSdate2($row['date_grad'],'-','/') );
		array_push($arr, formatUSdate2($row['date_left'],'-','/') );
		array_push($arr, $row['degree']);
		array_push($arr, $row['major1']);
		array_push($arr, $row['major2']);
		array_push($arr, $row['major3']);
		array_push($arr, $row['minor1']);
		array_push($arr, $row['minor2']);
		array_push($arr, $row['gpa']);
		array_push($arr, $row['gpa_major']);
		array_push($arr, $row['gpa_scale']);
		array_push($arr, $row['transscriptreceived']);
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['educationtype']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($ret, $arr);
	}
return $ret;
}//end get unis

function getRecs($appid)
{

	global $buckley;
	$ret = array();
	$letter = "";
	//RETRIEVE USER INFORMATION
	$sql = "SELECT 
	recommend.id,
	recommend.rec_user_id,
	recommend.datafile_id,
	recommend.submitted,
	recommend.recommendtype,
	users.id as usermasterid,
	users.title,
	users.firstname,
	users.lastname,
	users.email,
	users_info.address_perm_tel, 
	lu_users_usertypes.id as uid,
	users_info.company,
	reminder_sent_count,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM recommend
	left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
	left outer join users on users.id = lu_users_usertypes.user_id
	left join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
	where recommend.application_id=".$appid. " order by recommend.id";
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['rec_user_id']);
		array_push($arr, stripslashes($row['firstname']));
		array_push($arr, stripslashes($row['lastname']));
		array_push($arr, stripslashes($row['title']));
		array_push($arr, stripslashes($row['company']));
		array_push($arr, stripslashes($row['email']));
		array_push($arr, stripslashes($row['address_perm_tel']));
		array_push($arr, $row['recommendtype']);
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($arr,$row['reminder_sent_count']);
		array_push($arr, 0);//ARRAY INFO
		array_push($arr, $row['usermasterid']);
		array_push($ret, $arr);
	}
	
	$sql = "select buckleywaive from application where id=".$appid;
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$buckley = $row['buckleywaive'];
	}
	return $ret;
}//end get unis

//btnPaymentStatus

if(isset($_POST['btnSubmit']))
{
	//UPDATE
	/*
	$status = $_POST['lbStatus'];
	if($status == "")
	{
		$err .= "Status is required.";
	}
	if($err == "")
	{
		$sql = "update application set submitted=".$status." where id=".$appid;
		mysql_query( $sql) or die(mysql_error()."<br>".$sql);
	}
	*/
}

function getProgs($appid)
{
	$ret = array();
	$sql = "SELECT  programs.id, degree.name as degreename,fieldsofstudy.name as fieldname,
	lu_domain_department.domain_id, programs.linkword,
	lu_application_programs.round2
	from lu_application_programs
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
	left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
	where lu_application_programs.application_id = ". $appid . " group by programs.id order by lu_application_programs.choice ";
	$result = mysql_query($sql) or die(mysql_error());
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['fieldname']);
		array_push($arr, $row['domain_id']);
		array_push($arr, $row['linkword']);
		array_push($arr, $row['round2']);
		array_push($ret, $arr);
	}
	return $ret;
}
function getScores($appid)
{

	$ret = array();
	//LOOK FOR GRE GENERAL SCORE
	$sql = "select id,testdate,scorereceived,
	verbalscore,             
	verbalpercentile,        
	quantitativescore,            
	quantitativepercentile,              
	analyticalscore,            
	analyticalpercentile,            
	analyticalwritingscore,          
	analyticalwritingpercentile 
 	from grescore where application_id = " . $appid;
	$result = mysql_query($sql)	or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, formatUSdate2($row['testdate'],'-','/'));
		array_push($arr, "GRE General");
		array_push($arr, $row['scorereceived']);
		array_push($arr, $row['verbalscore']);            
		array_push($arr, $row['verbalpercentile']);       
		array_push($arr, $row['quantitativescore']);           
		array_push($arr, $row['quantitativepercentile']);             
		array_push($arr, $row['analyticalscore']);            
		array_push($arr, $row['analyticalpercentile']);            
		array_push($arr, $row['analyticalwritingscore']);         
		array_push($arr, $row['analyticalwritingpercentile']);
		array_push($ret,$arr); 
	}
	$sql = "select id,testdate,scorereceived, name, score, percentile from gresubjectscore where application_id = " . $appid;
	$result = mysql_query($sql)	or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, formatUSdate2($row['testdate'],'-','/'));
		array_push($arr, "GRE Subject");
		array_push($arr, $row['scorereceived']);
		array_push($arr, $row['name']);
		array_push($arr, $row['score']);
		array_push($arr, $row['percentile']);
		array_push($ret,$arr); 
	}
	$sql = "select id,testdate,scorereceived,type,
	section1,         
	section2,           
	section3,              
	essay,              
	total 
	from toefl where application_id = " . $appid;
	$result = mysql_query($sql)	or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, formatUSdate2($row['testdate'],'-','/'));
		array_push($arr, "TOEFL ". $row['type']);
		array_push($arr, $row['scorereceived']);
		array_push($arr, $row['section1']);
		array_push($arr, $row['section2']);
		array_push($arr, $row['section3']);
		array_push($arr, $row['essay']);
		array_push($arr, $row['total']);
		array_push($ret,$arr); 
	}
	return $ret;

}//end getscores

?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" action="" method="post"><!-- InstanceEndEditable -->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Edit Applicant<!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" --><br />
	<table width="750" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><input name="btnPrint" type="button" id="btnPrint" class="tblItem" value="Print Application" onclick="window.open('../review/userroleEdit_student_print.php?id=<?=$uid?>') "    /></td>
        <td align="right"><input name="btnClose" type="button" id="btnClose" class="tblItem" value="Close Window" onclick="window.close() " /></td>
      </tr>
    </table>
	<table width="750" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td valign="top"><span class="title">
          <?=$lastname?>
          ,
          <?=$firstname?>
        </span> <em>(
        <?=$gender?>
        )<br />
        <?=$country?>
        <br />
        <?=$email?><br />
        <br />
        </em></td>
        <td align="right" valign="top"><br />
          <br />
          <br /></td></tr>
    </table>
	<table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr valign="top">
        <td>
          <span class="tblItem">          </span>
          <table width="750" border="0" cellspacing="2" cellpadding="2">
            <tr>
              <td><span class="tblItem">
                <? 
				
				$lastMod = "Never";
				$tmpLastMod = "";
				for($i = 0; $i < count($compReqs); $i++)
				{
					if(strtotime($compReqs[$i][4]) > strtotime($tmpLastMod))
					{
						$tmpLastMod = $compReqs[$i][4];
						$lastMod = $tmpLastMod;
					}
				}
				
				for($i = 0; $i < count($apps); $i++)
				{ 
					$paidBool = 0;
					$submitBool = 0;
					$paid = "not paid";
					$submitted = "Not Submitted";
					$amt = $apps[$i][6];
					$appSent = intval($apps[$i][8]);
					
					$progs = getProgs($apps[$i][0]);
					if(count($progs) > 0)
					{
						$domain = $progs[0][3];
					}
					
					$submitBtnText = "Submit";
					$paymentBtnText = "Payment Received";
					$waiveBtnText = "Waive Payment";
					
					
					$compReqs=getReqs($apps[$i][0]);
					$tmpLastMod = "";
					
					for($j=0; $j<count($compReqs);$j++)
					{
						if($compReqs[$j][4] != "")
						{
							if(strtotime($compReqs[$j][4]) > strtotime($tmpLastMod) || $tmpLastMod == "")
							{
								$lastMod = $compReqs[$j][4];
							}
							$tmpLastMod = $compReqs[$j][4];
						}
					}
					
					if( $apps[$i][3] == 1)
					{
						$submitted = "Submitted on ". $apps[$i][4];
						$submitBool = 1;
						$submitBtnText = "Unsubmit";
					}
					if($apps[$i][6] != "" && $apps[$i][2] == 0)
					{
						$paid = "submitted payment of ".$apps[$i][6]." on ".$apps[$i][5];
						$paidBool = 0;
						$paymentBtnText = "Confirm Receipt of Payment";
					}
					if($apps[$i][6] != "" && $apps[$i][2] == 1)
					{
						$paid = "<strong>received payment of ".$apps[$i][6]." on ".$apps[$i][5]."</strong>";
						$paidBool = 1;
						$paymentBtnText = "Undo Payment Received";
					}
					if($apps[$i][7] == 1)
					{
						$waiveBtnText = "Unwaive Payment";
						$paid = "<strong>Payment Waived</strong>";
					}
					?>
                <a href="../apply/index.php?uid=<?=$guid?>&a=1&domain=<?=$domain?>" target="_blank">Log in as user</a></span></td>
              <td align="right">&nbsp;</td>
            </tr>
          </table>
          <br />

		  	  <span class="tblItem" >
				<input name="txtAppId" type="hidden" id="txtAppId" value="<?=$apps[$i][0]?>" />
				<input name="txtSubmitStatus_<?=$apps[$i][0]?>" type="hidden" id="txtSubmitStatus_<?=$apps[$i][0]?>" value="<?=$submitBool?>" />
				<input name="txtPaymentStatus_<?=$apps[$i][0]?>" type="hidden" id="txtPaymentStatus_<?=$apps[$i][0]?>" value="<?=$paidBool?>" />				
				<input name="txtWaiveStatus_<?=$apps[$i][0]?>" type="hidden" id="txtWaiveStatus_<?=$apps[$i][0]?>" value="<?=intval($apps[$i][7])?>" />
				<input name="userid" type="hidden" id="userid" value="<?=$uid?>" />	
				<input name="usermasterid" type="hidden" id="usermasterid" value="<?=$umasterid?>" />				

				<span class="title">Programs Selected:</span><br>
				<div id='list'>
				<table width="800" border="0" cellspacing="0" cellpadding="4">
				  <?
					//PROGRAMS
					for($j = 0; $j < count($progs); $j++)
					{ 
						?>
						<tr>
						<td>&nbsp;&nbsp;<input name="txtPromoteStatus_<?=$progs[$j][0]?>" type="hidden" id="txtPromoteStatus_<?=$progs[$j][0]?>" value="<?=intval($progs[$j][5])?>" />
						</td>
						<td><strong><?=$progs[$j][1]." ".$progs[$j][4]." " .$progs[$j][2]?></strong></td>
						<td>Status:
						<?
						//PROMOTION STUFF
						$promoStatus = "Normal";
						switch($progs[$j][5])
						{
						case 0:
						$promoStatus = "Normal";
						break;
						case 1:
						$promoStatus = "Promoted";
						break;
						case 2:
						$promoStatus = "Demoted";
						break;
						
						}
						echo $promoStatus."<br>";
						$promoteStatus = "Promote to Round 2";
						
						if($progs[$j][5] == 1 || $progs[$j][5] == 2)
						{
						$promoteStatus = "Reset Promotion";
						}
						
						showEditText($promoteStatus, "button", "btnPromote_".$progs[$j][0], $_SESSION['A_allow_admin_edit']); 
						
						showEditText("Demote from Round 2", "button", "btnDemote_".$progs[$j][0], $_SESSION['A_allow_admin_edit']); 
						//END PROMOTION
						?>
						</td>
					 	</tr>
						<?
					}
					$scores = getScores($apps[$i][0]);
					?>
				  
				</table>
				</div>
				</span>
			  	<br />
				<? 
				
				?>
			  	<hr align="left" width='750' size='1' noshade="noshade" color='#990000' />
			  	<span class="tblItem" >
			  	<table width="750" border="0" cellpadding="2" cellspacing="2">
			  	  <tr>
			  	    <td width="150" valign="top"><strong>Submission Status: </strong></td>
	  <td valign="top"><em><?=$submitted?></em>
	  &nbsp;&nbsp;&nbsp;</td>
	  <td valign="top"><strong>Last modified: </strong><em>
	    <?=formatUSdate($lastMod)?>
	  </em> </td>
	  <td align="right" valign="top">
	    <? showEditText($submitBtnText, "button", "btnSubmitStatus_".$apps[$i][0], $_SESSION['A_allow_admin_edit']); ?>	</td>
    </tr>
                </table>
			  	</span>
			  	<table width="750" border="0" cellpadding="2" cellspacing="2">
                  
                  <tr>
                    <td width="150" valign="top"><strong>Payment Status: </strong></td>
                    <td valign="top"><strong>Total Fees:</strong> $
                        <?=totalCost($apps[$i][0])?>
                      (
                      <?=$paid?>
                      )</td>
                    <td align="right"><? 
	/*
		array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['paid']);
	array_push($arr, $row['submitted']);
	array_push($arr, formatUSdate($row['submitted_date']));
	array_push($arr, formatUSdate($row['paymentdate']));
	array_push($arr, $row['paymentamount']);
	array_push($arr, $row['waive']);
	array_push($arr, $row['sent_to_program']);
	*/	
	  if($apps[$i][7] != 1)//NOT WAIVED
	  {
		  //waived so don't show payment stuff
		  if($amt == "")
		  {
		  $amt = totalCost($apps[$i][0]);
		  }
		  echo "$";
		  showEditText($amt, "textbox", "txtPayAmt_".$apps[$i][0], $_SESSION['A_allow_admin_edit'],false,null,true,10); 
		  echo "<br>";
		  showEditText($paymentBtnText, "button", "btnPaymentStatus_".$apps[$i][0], $_SESSION['A_allow_admin_edit']); 
		  if($apps[$i][5] != "")//IF THERE IS SOME PAYMENT, DISPLAY THE RESET BUTTON
		  {
		  	
			showEditText("Reset Payment Status", "button", "btnResetPayment_".$apps[$i][0], $_SESSION['A_allow_admin_edit']); 
		  }
		  showEditText("Update Amount", "button", "btnUpdatePayment_".$apps[$i][0], $_SESSION['A_allow_admin_edit']); 
	  }
	 
	  showEditText($waiveBtnText, "button", "btnWaiveStatus_".$apps[$i][0], $_SESSION['A_allow_admin_edit']); 
	  ?>                    </td>
                  </tr>
                </table>
			  <br />
			  <? showEditText("Save Applicant Info", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
			   <? 
			  echo 
			  showEditText($appSent, "checkbox", "chkSent", $_SESSION['A_allow_admin_edit']);  ?>
Sent to Program
<hr align="left" width='750' size='1' noshade color='#990000'>
	<strong>Test Scores</strong><br />	
	<table width="750" border="0" cellspacing="1" cellpadding="1">
      <tr class="tblHead">
        <td>Test Scores </td>
        <td>Test date</td>
        <td>Score Report  Status</td>
      </tr>
	  <? for($j = 0; $j<count($scores);$j++){ 
	   if($scores[$j][1] != "" && $scores[$j][1] != "00/0000")
	{
	  ?>
      <tr>
        <td><strong><?=$scores[$j][2]?></strong></td>
        <td><em><?=$scores[$j][1]?></em> </td>
        <td align="right">
		<?
		$received = "Not Received";
		$recVal = 0;
		  if($scores[$j][3] == 1)
		  {
		  	$received = "Received";
			$recVal = 1;
		  }
		  showEditText($received, "button", "btnScoreRec_".$scores[$j][0]."_".$j, $_SESSION['A_allow_admin_edit']); 
		?>
          <input name="txtScoreRec_<?=$scores[$j][0]?>_<?=$j?>" type="hidden" id="txtScoreRec_<?=$scores[$j][0]?>_<?=$j?>" value="<?=$recVal?>" />
		  <input name="txtScoreRecType_<?=$scores[$j][0]?>_<?=$j?>" type="hidden" id="txtScoreRecType_<?=$scores[$j][0]?>_<?=$i?>" value="<?=$scores[$j][2]?>" />       </td>
      </tr>
	  <tr><td colspan="3">
	  <? 
	 
	  switch($scores[$j][2])
	  { 
	  	case "GRE General":
		?>
		<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
                <td><strong>Percentile:</strong></td>
              </tr>
              <tr>
                <td><strong>Verbal:</strong></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][4], "textbox", "txtGreGenVerScore_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][5], "textbox", "txtGreGenVerPer_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>%                </span></td>
              </tr>
              <tr>
                <td><strong>Quantitative:</strong></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][6], "textbox", "txtGreGenQuanScore_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][7], "textbox", "txtGreGenQuanPer_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>%
                </span></td>
              </tr>
              <tr>
                <td><strong>Analytical Writing: </strong></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][10], "textbox", "txtGreGenWrScore_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][11], "textbox", "txtGreGenWrPer_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>%
                </span></td>
              </tr>
              <tr>
                <td><strong>Analytical:</strong></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][8], "textbox", "txtGreGenAnScore_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>
                </span></td>
                <td><span class="tblItem">
                  <? showEditText($scores[$j][9], "textbox", "txtGreGenAnPer_".$scores[$j][0]."_".$i, $_SESSION['A_allow_admin_edit'], false); ?>%
                </span></td>
              </tr>
            </table>
		<?
		break;
		case "GRE Subject";
		
		?>
		<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              
              <tr>
                <td><strong>Subject:</strong></td>
                <td><strong>Score:</strong></td>
                <td><strong>Percentile:</strong></td>
              </tr>
              <tr>
                <td>
				<? showEditText($scores[$j][4], "textbox", "txtGreSubName_".$scores[$j][0]."_".$i,$_SESSION['A_allow_admin_edit'], false, null, true, 30); ?></td>
                <td valign="top"><? showEditText($scores[$j][5], "textbox", "txtGreSubScore_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
                <td valign="top">      <? showEditText($scores[$j][6], "textbox", "txtGreSubPer_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?>
                % </td>
              </tr>
            </table>
		<?
		break;
		case "TOEFL IBT":
		?>
		<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
              </tr>
              <tr>
                <td><strong>Reading: </strong></td>
                <td><? showEditText($scores[$j][6], "textbox", "txtToeflIntSect3_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              
              <tr>
                <td><strong>Listening:</strong></td>
                <td><? showEditText($scores[$j][5], "textbox", "txtToeflIntSect2_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Speaking:</strong></td>
                <td><? showEditText($scores[$j][4], "textbox", "txtToeflIntSect1_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Writing:</strong></td>
                <td><? showEditText($scores[$j][7], "textbox", "txtToeflIntEssay_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Total Score:</strong></td>
                <td><? showEditText($scores[$j][8], "textbox", "txtToeflIntTotal_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
            </table>
		<?
		break;
		case "TOEFL CBT":
		?>
		<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
              </tr>
              
              <tr>
                <td><strong>Listening:</strong></td>
                <td><? showEditText($scores[$j][5], "textbox", "txtToeflCompSect2_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Structure/Writing:</strong></td>
                <td><? showEditText($scores[$j][4], "textbox", "txtToeflCompSect1_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Reading: </strong></td>
                <td><? showEditText($scores[$j][6], "textbox", "txtToeflCompSect3_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Total Score:</strong></td>
                <td><? showEditText($scores[$j][8], "textbox", "txtToeflCompTotal_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Essay Rating:</strong></td>
                <td><? showEditText($scores[$j][7], "textbox", "txtToeflCompEssay_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
            </table>
		<?
		break;
		case "TOEFL PBT":
		?>
		<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
              
              <tr>
                <td>&nbsp;</td>
                <td><strong>Score:</strong></td>
              </tr>
              <tr>
                <td><strong>Section 1 :</strong></td>
                <td><? showEditText($scores[$j][4], "textbox", "txtToeflPaperSect1_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Section 2 :</strong></td>
                <td><? showEditText($scores[$j][5], "textbox", "txtToeflPaperSect2_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Section 3 : </strong></td>
                <td><? showEditText($scores[$j][6], "textbox", "txtToeflPaperSect3_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>Total Score :</strong></td>
                <td><? showEditText($scores[$j][8], "textbox", "txtToeflPaperTotal_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
              <tr>
                <td><strong>TWE Score :</strong></td>
                <td><? showEditText($scores[$j][7], "textbox", "txtToeflPaperEssay_".$scores[$j][0]."_".$j,$_SESSION['A_allow_admin_edit'], false); ?></td>
              </tr>
            </table>
		<?
		break;
	   }//end switch 
	  
	   ?>
	   <hr align="left" width='750' size='1' noshade color='#990000'>
	  <? 
	   }//end if no date
	  }//end for scores?>
	  </td></tr>
    </table>
	<br />
	<hr align="left" width='750' size='1' noshade color='#990000'>
	<strong>Colleges/Universities	</strong>
	<table width="750" border="0" cellpadding="1" cellspacing="1">
	<? 
	$universities = getUniversities($uid);
	?>
  <tr>
    <td class="tblHead">Colleges/Universities</td>
    <td class="tblHead">Dates Attended </td>
    <td class="tblHead">Transcript Status</td>
  </tr>
  <? for($j = 0; $j < count($universities); $j++){ ?>
  <tr>
    <td><strong>
      <?=$universities[$j][1]?>
    </strong></td>
    <td><em>
      <?=$universities[$j][2]?> - <?=$universities[$j][3]?>
    </em></td>
    <td align="right">
      <? 
		  $received = "Not Received";
		  $recVal = 0;
		  if($universities[$j][14] == 1)
		  {
		  	$received = "Received";
			$recVal = 1;
		  }
		  showEditText($received, "button", "btnTranscriptRec_".$universities[$j][0], $_SESSION['A_allow_admin_edit']); 
		  ?>
		  <input name="txtScoreRec_<?=$universities[$j][0]?>" type="hidden" id="txtScoreRec_<?=$scores[$i][0]?>" value="<?=$recVal?>" />    </td>
  </tr>
  <? } ?>
</table>
	
	
	
	<strong>Recommenders </strong>
	
    <table width="750" border="0" cellpadding="1" cellspacing="1">
      <? 
	$recommenders = getRecs($apps[$i][0]);
	?>
      <tr>
        <td class="tblHead">name</td>
        <td align="right" class="tblHead">letter of recommendation  Status</td>
      </tr>
      <? for($j = 0; $j < count($recommenders); $j++){ ?>
      <tr>
        <td><strong>
          <?=$recommenders[$j][2]. " " . $recommenders[$j][3]?>
        </strong></td>
        <td align="right">	  
            <? 
			$qs = "?id=".$recommenders[$j][0]."&t=3"; ?>
            <br>
            <input  class="tblItem" name="btnUpload" value="Upload Recommendation Letter" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
            </strong><br>
            <? 
			if($recommenders[$j][9] != "")
			{
				echo "id:". $recommenders[$j][0] . " uid:".$recommenders[$j][1]. " umasterid:". $umasterid . " appid:" . $apps[$i][0];
				$path = getFilePath(3, $recommenders[$j][0], $recommenders[$j][1], $umasterid, $apps[$i][0], $recommenders[$j][9]);
				//echo "path:" . $path;
				showFileInfo("recommendation.".$recommenders[$j][12], $recommenders[$j][11], formatUSdate($recommenders[$j][10]), $path );			
				
			}//end moddate
			
			?>
			</td>
      </tr>
      <? } ?>
    </table>
	
    <? }// end for apps ?>		<br />
	<? showEditText("Save Applicant Info", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
	<br /></td>
        </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
