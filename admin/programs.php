<?php
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

if($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
{
    header("Location: index.php");
}

$sql = "";
$err = "";
$programs = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{
				mysql_query("DELETE FROM lu_programs_departments WHERE program_id=".$itemId)
				or die(mysql_error().$sql);

				mysql_query("DELETE FROM lu_programs_recommendations WHERE program_id=".$itemId)
				or die(mysql_error().$sql);

				mysql_query("DELETE FROM multiprogramlockouts WHERE program_id1=".$itemId)
				or die(mysql_error().$sql);

				mysql_query("DELETE FROM lu_programs_interests WHERE program_id=".$itemId)
				or die(mysql_error().$sql);

				mysql_query("DELETE FROM programs WHERE id=".$itemId)
				or die(mysql_error().$sql);
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select distinct programs.id,
fieldsofstudy.name,
degree.name as degreename,
programs.programprice,
programs.rank,
programs.linkword,
programs.baseprice, 
if(enabled = 1, 'yes','') as enabled,
programs.oraclestring
from programs
left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
inner join degree on degree.id = programs.degree_id
left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
";
if($_SESSION['A_usertypeid'] != 0)
{
	$sql .= " inner join lu_user_department on lu_user_department.department_id = lu_programs_departments.department_id
	where (lu_user_department.user_id = " .$_SESSION['A_userid']. ") and (lu_programs_departments.department_id=-1 ";
	for($i = 0; $i < count($_SESSION['A_admin_depts']);$i++)
	{
		$sql .= " or lu_programs_departments.department_id=".$_SESSION['A_admin_depts'][$i];
	}
	$sql .= " ) ";
}
$sql .= " order by lu_programs_departments.department_id, rank, degree.name, fieldsofstudy.name";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['degreename']);
	array_push($arr, $row['programprice']);
	array_push($arr, $row['rank']);
	array_push($arr, $row['linkword']);
	array_push($arr, $row['baseprice']);
	array_push($arr, $row['enabled']);
    array_push($arr, $row['oraclestring']);
	array_push($programs, $arr);
}

function getDepts($id)
{
	$ret = array();
	$sql = "select distinct department.id, department.name from programs
		inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
		inner join department on department.id = lu_programs_departments.department_id
		where lu_programs_departments.program_id=".$id."
		order by department.name";
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($ret, $arr);
	}
	return $ret;
}

function getLockouts($id)
{
	$ret = array();
	$sql = "select distinct program_id1, program_id2 from multiprogramlockouts
		where multiprogramlockouts.program_id1=".$id;
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result ))
	{
		if($row['program_id1'] == $id)
		{
			$arr = array();
			array_push($arr, $row['program_id2']);
			array_push($ret, $arr);
		}else
		{
			$arr = array();
			array_push($arr, $row['program_id1']);
			array_push($ret, $arr);
		}
	}
	return $ret;
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT
	department.id,
	department.name
	FROM lu_programs_departments

	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

function getProgramName($id)
{
	$ret = array();
	//GET DATA
	$sql = "select programs.id,
	fieldsofstudy.name,
	degree.name as degreename,
	programs.linkword
	from programs
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	inner join degree on degree.id = programs.degree_id
	where programs.id=".$id;
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result ))
	{
		$dept = "";
		$depts = getDepartments($row[0]);
		if(count($depts) > 1)
		{
			$dept = " - ";
			for($i = 0; $i < count($depts); $i++)
			{
				$dept .= $depts[$i][1];
				if($i < count($depts)-1)
				{
					$dept .= "/";
				}
			}
		}
		$arr = array();
		array_push($arr, $row['id']);
		//array_push($arr, $row['name']);
		array_push($arr, $row['degreename']. " ".$row['linkword']." ". $row[1].$dept);
		array_push($ret, $arr);
	}
	return $ret;
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Programs';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Programs</span><br />
<br />
    <a href="programEdit.php">Add a new Program
    </a>    <br />
    <br />
    Programs are the joining together of a degree type with
    a field of study for a particular department. <br />
    You can create cross-departmental programs by selecting more than one department. <br />
    To prevent an applicant from choosing similar programs, you can select one or more lockouts so they will not be selectable on the application interface.<br />
    <table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td>Department(s)</td>
        <td>lockouts</td>
        <td>Oracle String</td>
        <td>Baseprice</td>
        <td>Price</td>
        <td>rank</td>
        <td>Enabled</td>
        <td>Delete</td>
      </tr>
	  <? for($i = 0; $i < count($programs); $i++){
	   $class = "tblItem";
	 if($i % 2 == 0)
	 {
	 	$class = "tblItemAlt";
	 }
	  ?>
      <tr class="<?=$class?>">
        <td><a href="programEdit.php?id=<?=$programs[$i][0]?>"><?=$programs[$i][2]?> <?=$programs[$i][5]?> <?=$programs[$i][1]?></a></td>
        <td><?
			$depts = getDepts($programs[$i][0]);
			$tmpdept = "-1";

			for($j = 0; $j < count($depts); $j++)
			{
				if($tmpdept != "-1")//SAME DEPT
				{
					echo ", ";
				}
				echo $depts[$j][1];
				$tmpdept = $depts[$j][0];

			}
		?></td>
        <td><?
			$locks = getLockouts($programs[$i][0]);
			$tmplock = "-1";

			for($j = 0; $j < count($locks); $j++)
			{
				$name = getProgramName($locks[$j][0]);
				if($tmplock != "-1")//SAME DEPT
				{
					echo ", ";
				}
				if(count($name) > 0){
					echo $name[0][1];
				}
				$tmplock = $locks[$j][0];

			}
		?></td>
        <td align="left"><?=$programs[$i][8]?></td>
        <td align="left"><?=$programs[$i][6]?></td>
        <td align="left"><a href="programEdit.php?id=<?=$programs[$i][0]?>">
          <?=$programs[$i][3]?>
        </a></td>
        <td>
          <?=$programs[$i][4]?>        </td>
        <td><?=$programs[$i][7]?></td>
        <td align="right"><? showEditText("X", "button", "btnDelete_".$programs[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	  <? } ?>
    </table>
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>