<?php
include_once '../inc/db_connect.php';
include '../classes/DB_Applyweb/class.DB_Applyweb.php';   

$db_applyweb = new DB_Applyweb();
$usersinstSelectQuery = "SELECT id, user_id FROM usersinst WHERE user_id != -1 ORDER BY id";
$usersinstRecords = $db_applyweb->handleSelectQuery($usersinstSelectQuery);
echo count($usersinstRecords) . " records selected<br/>\n";

$updateCount = 0;
//DebugBreak();
foreach ($usersinstRecords as $usersinstRecord) {
    $usersinstId = $usersinstRecord['id'];
    $luUsersUsertypesId = $usersinstRecord['user_id'];
    $applicationQuery = "SELECT application.id FROM application WHERE user_id = " . $luUsersUsertypesId;
    $applicationRecords = $db_applyweb->handleSelectQuery($applicationQuery);
    $applicationId = NULL;
    foreach ($applicationRecords as $applicationRecord) {
        $applicationId = $applicationRecord['id'];
    }
    if ($applicationId) {
        $usersinstUpdateQuery = "/* user_id = " . $luUsersUsertypesId . " */
                                UPDATE usersinst SET application_id = " . $applicationId .
                                " WHERE id = " . $usersinstId;
        //echo $usersinstUpdateQuery . "<br/>\n";
        $db_applyweb->handleUpdateQuery($usersinstUpdateQuery);
        $updateCount++;
    }
}
echo $updateCount . " records updated";  

?>