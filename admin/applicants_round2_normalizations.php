<?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

$aAllRound2 = array();//NEW FOR 2008 - ALL ROUND 2 CANDIDATES
$aReviewers = array();
$aApplicants = array();
$aApplicants2 = array();
$aApplicants3 = array();
$aColumns = array();
$fieldsSel = array();
$avgAll = 0;
$lastSortBy = "1";
$sortBy = "1";
$sortBy2 = "";
$condition1 = "";
$condition2 = "";
$condition3 = "";
$condition4 = "";
$lastSortBy = "1";
$direction = "ASC";
$showWeights = "0"; //0 == false
$revFieldsBeginAt = 0;
//$appForm = "userroleEdit_student_formatted.php?v=2&r=2&d=1&showDecision=1";
$appForm = "../review/userroleEdit_student_review.php?v=2&r=2&showDecision=1";
$deptId = -1; //NEW FOR 2008
$deptName = "";
$sumAll = 0;
$avgAll = 0;
$colspan = 1;

//PROCESS GET/POST VARS

if(isset($_GET["id"]))
{

	$deptId = intval($_GET["id"]);
	
}

// PLB added periodId 11/24/09
$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
}



if(isset($_POST) )
{
/*
	foreach($_POST as $key=>$val)
	{
		echo "key: ".$key . " val: ".$val."<br>";
	}
*/
	if(isset($_POST["lbFields"]))
	{
		foreach($_POST["lbFields"] as $field)
		{
			array_push($fieldsSel, $field);
		}
	}
	if(isset($_POST["lastsort"]))
	{
		$lastSortBy = htmlspecialchars($_POST["lastsort"]);
	}
	if(isset($_POST["sortdir"]))
	{
		$direction = htmlspecialchars($_POST["sortdir"]);
	}

	if(isset($_POST["sort2"]))
	{
		$sortBy2 = $_POST["sort2"];
	}
	if(isset($_POST["sort"]))
	{
		$sortBy = $_POST["sort"];
		if($sortBy2 == ""){
			if($lastSortBy == $sortBy)
			{
				if($direction == "ASC")
				{
					$direction = "DESC";
				}else
				{
					$direction = "ASC";
				}
			}else
			{
				$direction = "ASC";
				$lastSortBy = $sortBy;
			}
		}
	}
	if(isset($_POST["chkWeights"]))
	{
		$showWeights = $_POST["chkWeights"];
	}
	if(isset($_POST["condition1"]))
	{
		$condition1 = htmlspecialchars($_POST["condition1"]);
	}
	if(isset($_POST["condition2"]))
	{
		$condition2 = htmlspecialchars($_POST["condition2"]);
	}
	if(isset($_POST["condition3"]))
	{
		$condition3 = htmlspecialchars($_POST["condition3"]);
	}
	if(isset($_POST["condition4"]))
	{
		$condition4 = htmlspecialchars($_POST["condition4"]);
	}
	if(isset($_POST["deptId"]))
	{
		$deptId = intval($_POST["deptId"]);
	}
}


$sql = "select name from department where id=".$deptId;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$deptName = $row['name'];
}	


function getReviewerWeight($memberini, &$aReviewers)
{

	$ret = "";

	for($i = 0; $i<count($aReviewers); $i++)
	{
		if($aReviewers[$i][0] == $memberini)
		{
			$ret = $aReviewers[$i][6];
			break;
    	}
	}

	return $ret;

}

function safeDivide($num1, $num2)
{
	$ret = 0;
	if(floatval($num2) > 0)
	{
		$ret = $num1/ $num2;
	}
	return $ret;
}

//  DAS Not used anymore 4/6/2009 
function sort2d ($array, $index, $order='asc', $natsort=FALSE, $case_sensitive=FALSE)
{
	if(is_array($array) && count($array)>0)
	{  
	   foreach(array_keys($array) as $key)
       
		   $temp[$key]=$array[$key][$index];
		   if(!$natsort)
		        ($order=='asc')? asort($temp) : arsort($temp);
		  else
		  {
			 ($case_sensitive)? natsort($temp) : natcasesort($temp);
			 if($order!='asc')
				 $temp=array_reverse($temp,TRUE);
	   }

	   foreach(array_keys($temp) as $key)
		   (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
	   return $sorted;

  }
  return $array;
}


/////////////////////////////////
//FIRST GET ALL ROUND2 CANDIDATES
/////////////////////////////////
$sql = "select lu_users_usertypes.id,
application.id AS application_id,
concat(users.lastname,', ', users.firstname) as name,
countries.name as ctzn,
gender as m_f,

group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '<br>') as program,
group_concat(distinct 
concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  
separator '~') as interest,
group_concat(distinct institutes.name separator '<br>') as undergrad ,
 
concat(/*',',*/	group_concat( distinct 
if( review.supplemental_review is null, review.reviewer_id,'')	 ),',') as revrid ,

group_concat( distinct 
case review.round when 1 then 
 /* case lu_user_department.department_id */
 case review.department_id
   when " . $deptId . " then 
	case lu_users_usertypes2.usertype_id when 2 then
	concat(review.reviewer_id,'-',substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
	end
 end

when 2 then
 /* case lu_user_department2.department_id */ 
 case review.department_id
  when " . $deptId . " then
	case lu_users_usertypes3.usertype_id when 2 then
	concat(review.reviewer_id,'-',substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
	end
 end 

end
)  as revr,

group_concat( distinct 
case review.round when 1 then
 /* case lu_user_department.department_id */
 case review.department_id
   when " . $deptId . " then 
	case lu_users_usertypes2.usertype_id when 2 then
	concat(review.reviewer_id,'~', review.round,'~', 
	if(review.round2 IS NULL,'',case review.round2 when 1 then 'yes' when 0 then 'no' end), ': ', 
		/*
        substring(	if( review.supplemental_review is null, review.point,''),1,3)      )
	    */
        if( review.supplemental_review is null, ifnull(review.point , '') ,'') )
    end
 end  

when 2 then
 /* case lu_user_department2.department_id */ 
 case review.department_id
   when " . $deptId . " then 
	case lu_users_usertypes3.usertype_id when 2 then
		concat(review.reviewer_id,'~',review.round,'~', 
		if(review.round2 IS NULL,'', case review.round2 when 1 then 'yes' when 0 then 'no' end ), ': ', 
			/*
            substring(	if( review.supplemental_review is null, review.point,''),1,3) )
            */
            if( review.supplemental_review is null, ifnull(review.point , '') ,'') )
		  
	end
 end  

end
)  as round2,

/*
group_concat( distinct 
case review.round when 1 then
*/
 /* case lu_user_department.department_id */
/*
 case review.department_id
   when " . $deptId . " then 
	case lu_users_usertypes2.usertype_id when 2 then
	concat(review.reviewer_id,'~',review.round,'~',substring(users2.firstname,1,1), substring(users2.lastname,1,1),'~',if(review.round2 IS NULL,'',review.round2),'~',review.point  )
	end
 end  
when 2 then
*/
  /* case lu_user_department2.department_id */ 
/*
 case review.department_id
   when " . $deptId . " then 
	case lu_users_usertypes3.usertype_id when 2 then
	concat(review.reviewer_id,'~',review.round,'~',substring(users3.firstname,1,1), substring(users3.lastname,1,1),'~',if(review.round2 IS NULL,'',review.round2),'~',review.point  )
	end
 end  

end
)  as round2a,
*/

GROUP_CONCAT(distinct 
case lu_programs_departments.department_id 
  when  ".$deptId. " THEN
lu_application_programs.decision
end
SEPARATOR '<BR>') as decision,
			
case MAX( IFNULL(lu_application_programs.round2, 0) ) 
  when 0 then '' 
  when 1 then 'promoted' 
  when 2 then 'demoted' 
end as promote,

group_concat(distinct if(revgroup.department_id = 1 , revgroup.name , '')  separator '<br>') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 1 , revgroup2.name , '')  separator '<br>') as revgrp2,

group_concat( distinct 
case review.round when 1 then
	 /* case lu_user_department.department_id */
     case review.department_id 
       when " . $deptId . " then 
		review.comments  
	 end 
when 2 then
	  /* case lu_user_department2.department_id */ 
      case review.department_id 
       when " . $deptId . " then 
		review.comments
	 end		
end
separator '<br><br>') as comments

from lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
left outer join countries on countries.id = users_info.cit_country
inner join application on application.user_id = lu_users_usertypes.id";

// PLB added period join 11/24/09
if ($periodId) {
    $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
}

$sql .= " inner join lu_application_programs on lu_application_programs.application_id = application.id
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
left outer join institutes on institutes.id = usersinst.institute_id
left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
left outer join interest on interest.id = lu_application_interest.interest_id
left outer join review on review.application_id = application.id 

/*reviewers - round 1*/
left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
left outer join users as users2 on users2.id = lu_users_usertypes2.user_id

/*review groups - round 1*/
left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1

/*application groups - round 1*/
left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
and lu_application_groups.round = 1 
left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
left outer join users as users3 on users3.id = lu_users_usertypes3.user_id

/*review groups - round 2*/
left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
and lu_reviewer_groups2.round = 2

/*application groups - round 2*/	
left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
and lu_application_groups2.round = 2	 
left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
where application.submitted=1
and review.supplemental_review IS NULL
and lu_programs_departments.department_id=".$deptId. " 
/* 
and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = ".$deptId. "   ) 
*/
and (lu_application_programs.round2 IS NULL or lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
and (lu_application_programs.round2 != 2)  
/*
and review.fac_vote = 0
*/
AND (review.fac_vote = 0 || review.fac_vote IS NULL)
";

// PLB added period where 11/24/09
if ($periodId) {
    $sql .= " AND period_application.period_id = " .  $periodId;
}
  
$sql .= " group by  users.id 
  
/*having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')*/  
order BY users.lastname,users.firstname";

$result = mysql_query($sql) or die(mysql_error() . "<br><br>".$sql);
// $applicationIds will become array of application ids indexed by lu_users_usertypes id
$applicationIds = array();
while($row = mysql_fetch_array( $result ))
{
	// PLB added the separate $applicationIds array so each applicant's application id
    // can be had without fear of messing up all the subsequent code that uses numerical indices
    // to get data from $aAllRound2 and thus depends on having $aAllRound2's fields 
    // in the order (and number?) specified below.
    $luUserUsertypesId = $row["id"];
    $applicationId = $row["application_id"];
    $applicationIds[$luUserUsertypesId] = $applicationId;
    
    $arr = array();
	array_push($arr, $row["id"]);
	array_push($arr, $row["name"]);
	array_push($arr, $row["ctzn"]);
	array_push($arr, $row["m_f"]);
	array_push($arr, $row["program"]);
	array_push($arr, $row["interest"]);
	array_push($arr, $row["undergrad"]);
	array_push($arr, $row["revrid"]);
	array_push($arr, $row["revr"]);
	array_push($arr, $row["round2"]);
	array_push($arr, $row["decision"]);
	array_push($arr, $row["promote"]);
	array_push($arr, $row["revgrp1"]);
	array_push($arr, $row["revgrp2"]);
	array_push($arr, $row["comments"]);
	array_push($aAllRound2, $arr);
	//echo $row["name"] . " ".  $row["round2a"]."<br>";
}
//DebugBreak();
/////////////////////////////
//END GET ROUND 2 CANDITDATES
/////////////////////////////

//GET REVIEWERS WHO HAVE MADE REVIEWS
//version 2008
$tmpRevs = array();

for($i = 0; $i < count($aAllRound2); $i++)
{
	//EXTRACT ARRAY INTO ID-XX
	$arr = split(",", $aAllRound2[$i][8]);//REVR COLUMN
	for($j = 0; $j < count($arr); $j++)
	{
		$doAdd = true;
		//EXTRACT ARRAY ITEM INTO INTO [ID][XX]
		$arr2 = split("-", $arr[$j]);
        
        // PLB added 1/5/10
        if ($arr2[0] == '') {
            $doAdd = FALSE;    
        }
        
		//LOOK IN TMPREVS TO SEE IF ITEM EXISTS
		for($k = 0; $k < count($tmpRevs); $k++)
		{

			if($tmpRevs[$k][0] == $arr2[0])
			{
				$doAdd = false;
				break;
			}
		}
		
		if($doAdd == true)
		{
			$arr3 = array();
			//echo $arr[$j]."<br>";
			array_push($arr3, $arr2[0]);
			if (sizeof($arr2) > 1) {array_push($arr3, $arr2[1]); }
			array_push($tmpRevs, $arr3);
			//echo $arr2[1]. "<br>";
		}	
	}
}


array_push($aColumns, "Org Ord");
array_push($aColumns, "Wgt Ord");
array_push($aColumns, "Rank Chg");
array_push($aColumns, "Name");
array_push($aColumns, "GDR");
array_push($aColumns, "CTZN");
array_push($aColumns, "UGRAD");
array_push($aColumns, "Interest");
array_push($aColumns, "Unit");
array_push($aColumns, "Dec");
array_push($aColumns, "AVG");
array_push($aColumns, "Wgt AVG");
array_push($aColumns, "# REV");//ARRAY INDEX 12

$revFieldsBeginAt = end(array_keys($aColumns));
$i = 0;
$numAll = 0;

	for($x = 0; $x < count($tmpRevs); $x++)
	{
		array_push($aColumns, $tmpRevs[$x][1]);
		array_push($aColumns, "Wgt<br>".$tmpRevs[$x][1]);

		//GET SCORES FOR REVIEWER - version 2008
		$aScores = array();
		$scoreSum = 0;
		$scoreNum = 0;		
		
		for($j = 0; $j < count($aAllRound2); $j++)
		{
			//	SPLIT THE REVRID COLUMN INTO INDIVIDUAL REVIEWERS ID*no: 3.0
			$revrs = split(",", $aAllRound2[$j][9]);
			//echo $aAllRound2[$j][9]."<br>";
			for($k = 0; $k < count($revrs); $k++)
			{
				$point = 0;
				//SPLIT ARRAY INTO [REVIEWER],[VOTE]
				$arr = split('~', $revrs[$k]);
				//echo $arr[0] ." ".  $tmpRevs[$x][0]."<br>";
				if($arr[0] == $tmpRevs[$x][0])//REIVIEWER ID MATCHES
				{
					//echo "match";
					//SPLIT VOTE INTO [YES/NO][SCORE]
					//echo $arr[1]."<br>";
					if(count($arr) > 1)
					{	
						
						$arr2 = split(":", $arr[2]);
						$point = trim($arr2[1]);
						//echo "point: ". $point."<br>";
                        //DAS debug
						//if ($arr2[0] == "no" && $arr[1] == 1 && $aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted")
						if ($arr2[0] == "no" && $arr[1] == 1 && $aAllRound2[$j][11] != 1 && $aAllRound2[$j][11] != "promoted")
                        {					
                            $showitem = false;
						}
						else
						{
						if($point > 0)
						{
							$scoreSum += $point;
							$scoreNum++;
							$sumAll += $point;
							$numAll++;
						}
						}
						break;
					}
				}
				
				
			}
		}
		$avg = 0;
		if($scoreNum > 0)
		{
			$avg = $scoreSum/$scoreNum; //AVG FOR THIS REVIEWER
		}
		//USE 0 FOR WEIGHT SINCE WE HAVENT CALCULATED THAT YET
		$arr1 = array($tmpRevs[$x][0],  $tmpRevs[$x][1],$aScores,$scoreSum,$scoreNum,$avg,0);
		array_push($aReviewers, $arr1);
		$i++;

	}

	if($numAll > 0)
	{
		$avgAll = $sumAll/$numAll;
	}

	//GET WEIGHTS FOR REVIEWER
	for($i = 0; $i < count($aReviewers); $i++)
	{
		if($aReviewers[$i][5] > 0)
		{
			$aReviewers[$i][6] = $avgAll/$aReviewers[$i][5];
		}
	}

    
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Round 2 Normalizations';

$pageCssFiles = array(
    '../css/normalizations.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.tablesorter.js',
    '../javascript/normalizations.js',
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

// period

$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    if ($endDate == '') {
        $endDate = 'present';    
    }
    $applicationPeriodDisplay = '<br/>' . $startDate . '&nbsp;to&nbsp;' . $endDate;
    $description = $period->getDescription();
    if ($description) {
        $applicationPeriodDisplay .= ' (' . $description . ')';    
    }
}
?> 
<div id="pageHeading">
<div id="departmentName">
<?php
echo $deptName;
if ($applicationPeriodDisplay) {
    echo $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle">Round 2 Normalizations</div>
</div> 
<?


/////////////////////////////
//BUILD JAVASCRIPT HANDLERS//
/////////////////////////////
echo "<SCRIPT LANGUAGE='JavaScript'>\n";
echo "<!--\n";
echo "function doFormSubmit(sort, sort2) { \n";
echo "document.form1.action = '';\n";
echo "document.form1.target = '_self';\n";
echo "if(sort != ''){\n";
echo "form1.sort.value = sort;\n";
echo "}\n";
echo "if(sort2 != ''){\n";
echo "document.form1.sort2.value = sort2;\n";
echo "}else{\n";
echo "document.form1.sort2.value = '';\n";
echo "}\n";
echo "document.form1.submit();\n";
echo "}\n";
///////////
//OPEN FORM
///////////
echo "function openForm(userid, formname) { \n";
echo "document.form1.action = formname;\n";
echo "document.form1.target = '_blank';\n";
echo "document.form1.userid.value = userid;\n";
echo "document.form1.submit();\n";
echo "}\n";
///////////
//CHECK ALL
///////////
echo "function checkAll(checked) { \n";
echo "var el = document.getElementById('form1');\n";
echo "for (var i = 0; i < el.elements.length; i++) {\n";
echo "el.elements[i].checked = checked;\n";
echo "}\n";
echo "}\n";
echo "//-->\n";
echo "</script>\n";

	echo "<form id='form1' action='' method='POST' name='form1'>\n";
	echo "<input name='sort' type='hidden' id='sort' value='".$sortBy."' />\n";
	echo "<input name='sort2' type='hidden' id='sort2' value='".$sortBy2."' />\n";
	echo "<input name='condition1' type='hidden' id='condition1' value='".$condition1."' />\n";
	echo "<input name='condition2' type='hidden' id='condition1' value='".$condition2."' />\n";
	echo "<input name='condition3' type='hidden' id='condition1' value='".$condition3."' />\n";
	echo "<input name='lastsort' type='hidden' id='lastsort' value='".$lastSortBy."' />\n";
	echo "<input name='sortdir' type='hidden' id='sortdir' value='".$direction."' />\n";
	echo "<input name='userid' type='hidden' id='userid' value='' />";
	echo "<input name='printapp' type='hidden' id='printapp' value='printapplication' />\n";
	
    // PLB added period input 11/24/09
    if ($periodId) {
        echo "<input name='period' type='hidden' id='period' value='" . $periodId . "' />";
    }
    
    $checked = "";
	if($showWeights=="1")
	{
		$checked = " checked ";
	}
?>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div id="applicants">

	<!-- InstanceBeginEditable name="mainContent" -->
	<?
	
	//THIS IS WHERE THE MAGIC HAPPENS
	/*
	$sql = "SELECT users.userid,
	concat(users.lastname, ', ',users.firstname) as name,
	applicationinfo.gender as gender,
	if(applicationinfo.noncit_country_of_citizenship is null,
	applicationinfo.perm_addr_country,applicationinfo.noncit_country_of_citizenship) as CTZN,
	usersundergraduteinst.name as ugrad,
	unitsareaofinterest.areaofinterest,
	units.unitname,
	decission,
	comments ";

	$stringbuilder = "";
	for ($i = 0; $i < count($aReviewers); $i++)
	{
		$stringbuilder .= ",\nIF(csd_adm_committee_review.email='".$aReviewers[$i][0]."',csd_adm_committee_review.point,null) as ". $aReviewers[$i][1];
	}
	";
	*/
	$result = array();
	for($x = 0; $x < count($aAllRound2); $x++)
	{
        $showitem = true;
		$arrFields = array();
        array_push($arrFields,$aAllRound2[$x][0] );//appid
		array_push($arrFields,$aAllRound2[$x][1] );//name
		array_push($arrFields,$aAllRound2[$x][3] );//gender
		array_push($arrFields,$aAllRound2[$x][2] );//ctzn
		array_push($arrFields,$aAllRound2[$x][6] );//ugrad
		array_push($arrFields,$aAllRound2[$x][5] );//aoi
		array_push($arrFields,$aAllRound2[$x][4] );//program
		array_push($arrFields,$aAllRound2[$x][10] );//decision
		array_push($arrFields,$aAllRound2[$x][14] );//comments
		//echo $aAllRound2[$x][1] . " ";
		//	SPLIT THE REVRID COLUMN INTO INDIVIDUAL REVIEWERS ID*no: 3.0
		$revrs = split(",", $aAllRound2[$x][9]);

		$reviewerCount = count($revrs);
        // PLB added 1/6/10: don't show apps with no reviews
        if ($reviewerCount == 1 && $revrs[0] == '' && 
            $aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted") {
            
            $showitem = false;
            continue;    
        }
        
        // Require min 2 votes for CSD
        if ($deptId == 1 && ($reviewerCount < 2 || $revrs[1] == '') &&
            $aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted") {

            $showitem = false;
            continue;
        }

        // Require min 2 votes for ML
        if ($deptId == 2 && ($reviewerCount < 3 || $revrs[1] == '' || $revrs[2] == '') &&
            $aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted") {

            $showitem = false;
            continue;
        }
        
        //RUN THROUGH ALL AVAILABLE REVIEWERS
		for($y = 0; $y < count($tmpRevs); $y++)
		{
			$thisPoint = null;
			//THE REVIEWERS FOR THIS APPLICATION
			for($k = 0; $k < count($revrs); $k++)
			{
				//SPLIT ARRAY INTO [REVIEWER],[VOTE]
				$arr = split('~', $revrs[$k]);
				//echo $arr[0] ." ".  $tmpRevs[$x][0]."<br>";
				if($arr[0] == $tmpRevs[$y][0])//REIVIEWER ID MATCHES
				{
					//echo "match";
					//SPLIT VOTE INTO [YES/NO][SCORE]
					//echo $arr[1]."<br>";
					if(count($arr) > 1)
					{	
						
						$arr2 = split(":", $arr[2]);
						$point = trim($arr2[1]);
                        //$point = number_format( floatval( trim($arr2[1]) ), 2 );
						//echo $arr2[0]." ". $point."<br>";
						//echo $arr2[0] ." ". $arr[1]."<br>";
						//IF THERE IS A ROUND 1 'NO' VOTE, DON'T SHOW
						/*
                        if($arr2[0] == "no" && $arr[1] == 1 && $aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted" )
                        {
							$showitem = false;
						}
                        */
                        
                        // CSD and ML can advance through voting (non-unanimity tested above).
                        if ($deptId == 1 || $deptId == 2) {
                            if( ($arr2[0] == "no" || $arr2[0] == NULL) && $arr[1] == 1 
                            && $aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted" )
                            {
                                $showitem = false;
                            } 
                        } else {
                            if($aAllRound2[$x][11] != 1 && $aAllRound2[$x][11] != "promoted" )
                            {
                                $showitem = false;
                            }                            
                        }
						if($point > 0)
						{
							$thisPoint =  $point;
						}
						break;
					}
				}
				
				
			}
            
			//ENTER THE REVIEWERS SCORE INTO THE ARRAY
		  	array_push($arrFields,$thisPoint );
		}//END FOR
		if($showitem == true)
		{
			array_push($result, $arrFields);
		}
	}

    ?>
    <div id="sortTip">
        <span id="tip">TIP!</span> 
        Sort multiple columns simultaneously by holding down the shift key and clicking a second, third or even fourth column header! 
    </div>    
    <?php
    //echo count($result) . " Applicants Showing";
    echo count($result) . " Applicants Total";

	$columnWidth = 0;
	$numRows = count($result);
	if($numRows > 0)
	{
		$columnWidth = count($result[0]);
	}
	
	$aResults = $result;
	//$aResults = mergeRows($result); //NO LONGER NECESSARY 


	for ($x = 0; $x < count($aResults); $x++)
	{
		$point = 0;
		$num = 0;
		$avg = 0;
		$weightAvgSum = 0;
		$weightAvgNum = 0;
		$arr = array();
		array_push($arr,$aResults[$x][0]);//USERID
		array_push($arr,0);//ORIG ORDER
		array_push($arr,0);//WEIGHT ORDER
		array_push($arr,0);//DELTA RANK
		array_push($arr,$aResults[$x][1]);//NAME
		array_push($arr,$aResults[$x][2]);//GENDER
		array_push($arr,$aResults[$x][3]);//CTZN
		array_push($arr,$aResults[$x][4]);//UGRAD
		array_push($arr,$aResults[$x][5]);//INTEREST
		array_push($arr,$aResults[$x][6]);//UNIT
		array_push($arr,$aResults[$x][7]);//DECISION
		array_push($arr,0);//AVG
		array_push($arr,0);//WEIGHTED AVG
		////////////////////////////////		
		//NEW CODE FOR 2008		
		////////////////////////////////
		$numThisRev = 0;
		for($i = 9; $i< $columnWidth; $i++)
		{
				if($aResults[$x][$i] > 0)
				{
					$numThisRev++;
				}
		}
		//array_push($arr,$aResults[$x][end(array_keys($aResults[$x]))]);//#REV
		array_push($arr,$numThisRev);//#REV
		for($i = 9; $i< $columnWidth; $i++)
		{
			array_push($arr, $aResults[$x][$i] );
			$revWeight = getReviewerWeight($aReviewers[$i-9][0],$aReviewers);
			$thisWeight = $aResults[$x][$i]  * $revWeight;
			if($thisWeight != ""){
				if($aResults[$x][$i] > 0)
				{
					$weightAvgSum += $thisWeight;
					$weightAvgNum++;

					$point += $aResults[$x][$i] ;
					$num++;
				}
				array_push($arr, round($thisWeight,3) );
			}
			else
			{
				array_push($arr, "");
			}

		}

		$arr[11] = round(safeDivide($point,$num),3);
		$arr[12] = round(safeDivide($weightAvgSum,$weightAvgNum),3);



		array_push($aApplicants, $arr);
		array_push($aApplicants2, $arr);
	}
    
    $avg  = array();
    $wgt = array();
    foreach ($aApplicants as $key => $row) {
        $avg[$key]  = $row[11];
        $wgt[$key] = $row[12];
    }

    //DO PRE-SORTS
    array_multisort($avg, SORT_ASC, $aApplicants);    
	
//	$aApplicants = sort2d ($aApplicants, 11);

	for($i = 0; $i < count($aApplicants); $i++)
	{
		$aApplicants[$i][1] = $i+1;
	}
	//RESORT BASED ON WEIGHTED AVG
    $avg  = array();
    $wgt = array();
    foreach ($aApplicants as $key => $row) {
        $avg[$key]  = $row[11];
        $wgt[$key] = $row[12];
    }
 //   $aApplicants2 = sort2d ($aApplicants2, 12); 
    array_multisort($avg, SORT_ASC, $wgt, SORT_ASC, $aApplicants); 
    for($i = 0; $i < count($aApplicants); $i++)
    {
        $aApplicants[$i][2] = $i+1;
        $aApplicants[$i][3] = $aApplicants[$i][1] - $aApplicants[$i][2];
    }   
    
    // put order back to original order
    $avg  = array();
    $wgt = array();
    foreach ($aApplicants as $key => $row) {
        $avg[$key]  = $row[11];
        $wgt[$key] = $row[12];
    }

    //DO PRE-SORTS
    array_multisort($avg, SORT_ASC, $aApplicants);

	//DISPLAY SORT FORM ELEMENTS
    /*
	echo "<table border='1' cellpadding='4'><tr class='tblHead'>\n";
	echo "<tr>";
	echo "<td>\n";
	echo "Selected Columns:<br><select name='lbFields[]' size='10' multiple='multiple' id='lbFields[]' class='tblItem'>\n";
	for($i = 0; $i< count($aColumns); $i++)
	{
		$selected = "";
		for($j = 0; $j < count($fieldsSel); $j++){
			if($fieldsSel[$j] == $i+1)
			{
					$selected = " selected ";
					break;
			}
		}
		$k = $i+1;
		echo "<option value=".$k." ".$selected .">".$aColumns[$i]."</option>\n";
	}
	echo "</select>\n";
	echo "</td>";
	echo "<td valign='top'><table><tr><td>\n";
	echo "Sort:</td><td><select id='sort' name='sort' class='tblItem'>\n";
	for($i = 0; $i< count($aColumns); $i++)
	{
		$selected = "";
		if($i+1 == $sortBy){
			$selected = " selected ";
		}
		$k = $i+1;
		echo "<option value=".$k." ".$selected .">".$aColumns[$i]."</option>\n";
	}
	echo "</select></td></tr>";


	echo "<tr><td colspan='2'>Filter By:</td></tr><tr><td>Interest:</td><td><select id='condition1' name='condition1' class='tblItem'>\n";
	$aAoi = array();
	for($i = 0; $i< count($aApplicants); $i++)
	{
		$isFound = false;
		$athisArea = split('~',$aApplicants[$i][8]);
		 
		for($k = 0; $k < count($athisArea); $k++)
		{
			$thisArea = trim(substr($athisArea[$k]  , 0,-3 ));
			for($j = 0; $j< count($aAoi); $j++)
			{
				if($aAoi[$j] == $thisArea)
				{
					$isFound = true;
					break;
				}
	
			}
			if($isFound == false && $thisArea != "")
			{
				array_push($aAoi, $thisArea);
			}
		}
	}
	echo "<option value='' ".$selected .">Select One</option>\n";
	for($i = 0; $i< count($aAoi); $i++)
	{
		$selected = "";
		if($aAoi[$i] == $condition1){
			$selected = " selected ";
		}
		echo "<option value='".$aAoi[$i]."' ".$selected .">".$aAoi[$i]."</option>\n";
	}
	echo "</select></td></tr>\n";

	echo "<tr><td>Gender:</td><td><select id='condition2' name='condition2' class='tblItem'>\n";
	$aAoi = array();
	for($i = 0; $i< count($aApplicants); $i++)
	{
		$isFound = false;
		for($j = 0; $j< count($aAoi); $j++)
		{
			if($aAoi[$j] == $aApplicants[$i][5])
			{
				$isFound = true;
				break;
			}

		}
		if($isFound == false && $aApplicants[$i][5] != "")
		{
			array_push($aAoi, $aApplicants[$i][5]);
		}
	}
	echo "<option value='' ".$selected .">Select One</option>\n";
	for($i = 0; $i< count($aAoi); $i++)
	{
		$selected = "";
		if($aAoi[$i] == $condition2){
			$selected = " selected ";
		}
		echo "<option value='".$aAoi[$i]."' ".$selected .">".$aAoi[$i]."</option>\n";
	}
	echo "</select></td></tr>\n";

	echo "<tr><td>Citizenship:</td><td><select id='condition3' name='condition3' class='tblItem'>\n";
	$aAoi = array();
	array_push($aAoi, "US");
	for($i = 0; $i< count($aApplicants); $i++)
	{
		$isFound = false;
		for($j = 0; $j< count($aAoi); $j++)
		{
			if($aAoi[$j] == $aApplicants[$i][6])
			{
				$isFound = true;
				break;
			}

		}
		if($isFound == false && $aApplicants[$i][6] != "")
		{
			array_push($aAoi, $aApplicants[$i][6]);
		}
	}
	echo "<option value='' ".$selected .">Select One</option>\n";
	for($i = 0; $i< count($aAoi); $i++)
	{
		$selected = "";
		if($aAoi[$i] == $condition3){
			$selected = " selected ";
		}
		echo "<option value='".$aAoi[$i]."' ".$selected .">".$aAoi[$i]."</option>\n";
	}
	echo "</select></td></tr>\n";

	echo "<tr><td>Decision:</td><td><select id='condition4' name='condition4' class='tblItem'>\n";
	$aAoi = array();
	for($i = 0; $i< count($aApplicants); $i++)
	{
		$isFound = false;
		for($j = 0; $j< count($aAoi); $j++)
		{
			if($aAoi[$j] == $aApplicants[$i][10])
			{
				$isFound = true;
				break;
			}

		}
		if($isFound == false && $aApplicants[$i][10] != "")
		{
			array_push($aAoi, $aApplicants[$i][10]);
		}
	}
	echo "<option value='' ".$selected .">Select One</option>\n";
	for($i = 0; $i< count($aAoi); $i++)
	{
		$selected = "";
		if($aAoi[$i] == $condition4){
			$selected = " selected ";
		}
		echo "<option value='".$aAoi[$i]."' ".$selected .">".$aAoi[$i]."</option>\n";
	}
	echo "</select></td></tr></table>\n";

	echo "</td>\n";
	echo "<td valign='top' align='right'>\n";
	echo "Show weights:<input name='chkWeights' type='checkbox' id='chkWeights' value='1' ".$checked." />";
	echo "<input name='btnRefresh' type='button' id='btnRefresh' value='Refresh' class='tblItem' onClick=\" javascript: form1.action = '',form1.target = '_self',document.form1.submit();  \" />\n";
	echo "</td>";
	echo "</tr>\n";
	echo "</table>\n";
    */

	//DISPLAY COLUMN HEADS
	echo "<table width='100%' border='0' cellpadding='4' cellspacing='1' id='applicantTable'>
        <thead>
        <tr class='tblHead'>\n";
	if(count($fieldsSel) > 0)
	{
		for($i = 0; $i< count($fieldsSel); $i++)
		{
			if ($i >= 13) {
                echo "<th width='30px'>".$aColumns[$fieldsSel[$i]]."</th>\n";
            } else {
                echo "<th>".$aColumns[$fieldsSel[$i]]."</th>\n";
		    }
        }

	}else
	{
		for($i = 0; $i< count($aColumns); $i++)
		{
			if($showWeights == 0 && $i > $revFieldsBeginAt && $i%2 == 0)
			{

			}else
			{
				if ($i >= 13) {
                    echo "<th width='30px'>".$aColumns[$i]."</th>\n";    
                } else {
                    echo "<th>".$aColumns[$i]."</th>\n";    
                }
                
			}
		}
	}
	echo "</tr></thead>\n<tbody>";


	//DISPLAY EVERYTHING
	$revFieldsBeginAt++; //CORRECTION FOR HIDDEN USERID FIELD IN $aApplicants
	$k=0;


	for($i = 0; $i < count($aApplicants); $i++)
	{
		$dispRec = true;
		if($condition1 != "")
		{
			if(strstr($aApplicants[$i][8], $condition1)  === false )
			{
				$dispRec = false;
			}
		}
		if($condition2 != "")
		{
			if($aApplicants[$i][5] != $condition2)
			{
				$dispRec = false;
			}
		}
		if($condition3 != "")
		{
			//echo $aApplicants[$i][6] . " ". $condition3."<br>";

			if($aApplicants[$i][6] == ""  && $condition3 == "US")
			{
			}else{
				if($aApplicants[$i][6] != $condition3)
				{
					$dispRec = false;
				}
			}
		}
		if($condition4 != "")
		{

			if($aApplicants[$i][10] != $condition4)
			{
				$dispRec = false;
			}
		}

		if($dispRec == true)
		{
			$altClass = "";
			if($k % 2 == 0){
				//$altClass =" class='tblItem'";
                $altClass =" class='tblItemAlt'";
			}else
			{
				$altClass =" class='tblItemAlt'";
			}

			echo "<tr ".$altClass.">";

			if(count($fieldsSel) > 0)
			{
				for($j=0; $j < count($fieldsSel); $j++)
				{
					switch($j)
					{
                        case 1:
                        case 2:
                            echo "<td>" . str_pad($aApplicants[$i][$fieldsSel[$j]], 3, '0', STR_PAD_LEFT) . "&nbsp;</td>\n";     
                            break;
                        case 4:
							//echo "<td><a href=\"javascript: openForm('".stripslashes($aApplicants[$i][0])."', '".$appForm."');\">".$aApplicants[$i][$fieldsSel[$j]]."</a></td>";
							$luUsersUsertypesId = stripslashes($aApplicants[$i][0]);
                            $applicationId = $applicationIds[$luUsersUsertypesId];
                            $name = $aApplicants[$i][$fieldsSel[$j]];
                            $url = $appForm . '&d=' . $deptId . '&applicationId=' . $applicationId;
                            echo "<td><a href=\"javascript: openForm('". $luUsersUsertypesId . "', '".$url."');\">" . $name . "</a></td>";
                            break;
						case 8:
							echo "<td>". str_replace('~',',<br>',$aApplicants[$i][$fieldsSel[$j]])."&nbsp;</td>\n";
							break;
						default:
							if ($j >= 13) {
                                echo "<td width='30px'>".$aApplicants[$i][$fieldsSel[$j]]."&nbsp;</td>\n";
                            } else {
                                echo "<td>".$aApplicants[$i][$fieldsSel[$j]]."&nbsp;</td>\n";
						    }
                        break;
					}
				}

			}else
			{
				for($j=1; $j < count($aApplicants[$i]); $j++)
				{
					if($showWeights == 0 && $j > $revFieldsBeginAt && $j%2 != 0)
					{

					}else
					{
						switch($j)
						{
                            case 1:
                            case 2:
                                echo "<td>" . str_pad($aApplicants[$i][$j], 3, '0', STR_PAD_LEFT) . "&nbsp;</td>\n";
                                break;    
                            case 3:
                                $rankArray = explode('-', $aApplicants[$i][$j]);
                                if ( count($rankArray) == 2) {
                                    $rankArray[1]  = str_pad($rankArray[1], 2, '0', STR_PAD_LEFT);
                                    $rank = implode('-', $rankArray);
                                } else {
                                    $rankArray[0]  = str_pad($rankArray[0], 2, '0', STR_PAD_LEFT);
                                    $rank = implode('', $rankArray);    
                                }
                                echo "<td>" . $rank . "&nbsp;</td>\n";
                                break;
                            case 4:
								//echo "<td><a href=\"javascript: openForm('".stripslashes($aApplicants[$i][0])."', '".$appForm."');\">".$aApplicants[$i][$j]."</a></td>";
								$luUsersUsertypesId = stripslashes($aApplicants[$i][0]);
                                $applicationId = $applicationIds[$luUsersUsertypesId];
                                $name = $aApplicants[$i][$j];
                                $url = $appForm . '&d=' . $deptId . '&applicationId=' . $applicationId;
                                echo "<td><a href=\"javascript: openForm('". $luUsersUsertypesId . "', '".$url."');\">" . $name . "</a></td>";
                                break;
							case 8:
								echo "<td>".str_replace('~',',<br>',$aApplicants[$i][$j])."&nbsp;</td>\n";
								break;
							default:
								if ($j >=13 ) {
                                    echo "<td width='30px'>".$aApplicants[$i][$j]."&nbsp;</td>\n";    
                                } else {
                                    echo "<td>".$aApplicants[$i][$j]."&nbsp;</td>\n";
                                }
                                break;
						}


					}
				}
			}
			echo "</tr>";
			$k++;
		}//END IF $dispRec
	}//end for


	$colSpan = 13;
	if(count($fieldsSel) > 0)
	{
		$colSpan = 0;
		for($i = 0; $i < count($fieldsSel); $i++)
		{
			if($fieldsSel[$i] <= $revFieldsBeginAt)
			{
				$colSpan++;
			}
		}
	}

	//REVIEWERS AGAIN
	echo "</tbody></table><br/><table width='100%' cellpadding='4' cellspacing='1'>
            <tr class='tblHead'>";

	//echo "<td colspan='".$colSpan."' align='right'><strong>Reviewer</strong></td>\n";
    echo "<td width='*' align='right'><strong>Reviewer</strong></td>\n";
	if(count($fieldsSel) > 0)
	{
		for($j=0; $j < count($fieldsSel); $j++)
		{
			if($fieldsSel[$j] > $revFieldsBeginAt)
			{
				echo "<td>".$aColumns[$fieldsSel[$j]]."</td>\n";
			}
		}

	}else
	{
		for ($i = 0; $i < count($aReviewers); $i++)
		{

			echo "<td align='center' width='30px'>".$aReviewers[$i][1]."</td>\n";
			if($showWeights == "1")
			{
				echo "<td align='center' width='30px'>WGT<br>".$aReviewers[$i][1]."</td>\n";
			}
		}
	}
	//SUM OF SCORES
	echo "<tr class='tblItemAlt'>";
	//echo "<td colspan='".$colSpan."' align='right'><strong>Sum of Scores</strong></td>\n";
	echo "<td width='*' align='right'><strong>Sum of Scores</strong></td>\n";
    if(count($fieldsSel) > 0)
	{
		for($j=0; $j < count($fieldsSel); $j++)
		{
			if($fieldsSel[$j] > $revFieldsBeginAt)
			{
				echo "<td  width='30px'>". $aReviewers[$fieldsSel[$j]][3]."</td>\n";
			}
		}

	}else
	{
		for ($i = 0; $i < count($aReviewers); $i++)
		{

			echo "<td  width='30px'>".$aReviewers[$i][3]."</td>";
			if($showWeights == "1")
			{
				echo "<td width='30px'></td>";
			}
		}
	}

	echo "</tr>";
	//NUM OF SCORES
	echo "<tr class='tblItemAlt'>";
	//echo "<td colspan='".$colSpan."' align='right'><strong>Num of Scores</strong></td>\n";
    echo "<td width='*' align='right'><strong>Num of Scores</strong></td>\n"; 
	if(count($fieldsSel) > 0)
	{
		for($j=0; $j < count($fieldsSel); $j++)
		{
			if($fieldsSel[$j] > $revFieldsBeginAt)
			{
				echo "<td width='30px'>". $aReviewers[$fieldsSel[$j]][4]."</td>\n";
			}
		}

	}else
	{
		for ($i = 0; $i < count($aReviewers); $i++)
		{

			echo "<td width='30px'>".$aReviewers[$i][4]."</td>";
			if($showWeights == "1")
			{
				echo "<td width='30px'></td>";
			}
		}
	}

	echo "</tr>";
	//AVG THIS REVIEWER
	echo "<tr class='tblItemAlt'>";
	//echo "<td colspan='".$colSpan."' align='right'><strong>Avg Evaluator Score</strong></td>\n";
    echo "<td width='*' align='right'><strong>Avg Evaluator Score</strong></td>\n";
	if(count($fieldsSel) > 0)
	{
		for($j=0; $j < count($fieldsSel); $j++)
		{
			if($fieldsSel[$j] > $revFieldsBeginAt)
			{
				echo "<td width='30px'>". round($aReviewers[$fieldsSel[$j]][5],3)."</td>\n";
			}
		}

	}else
	{
		for ($i = 0; $i < count($aReviewers); $i++)
		{

			echo "<td width='30px'>".round($aReviewers[$i][5],3)."</td>";
			if($showWeights == "1")
			{
				echo "<td width='30px'></td>";
			}
		}
	}
	echo "</tr>";
	//WEIGHT FACTOR
	echo "<tr class='tblItemAlt'>";
	//echo "<td colspan='".$colSpan."' align='right'><strong>Weight Factor</strong></td>\n";
    echo "<td width='*' align='right'><strong>Weight Factor</strong></td>\n";
	if(count($fieldsSel) > 0)
	{
		for($j=0; $j < count($fieldsSel); $j++)
		{
			if($fieldsSel[$j] > $revFieldsBeginAt)
			{
				echo "<td width='30px'>". round($aReviewers[$fieldsSel[$j]][6],3)."</td>\n";
			}
		}

	}else
	{
		for ($i = 0; $i < count($aReviewers); $i++)
		{

			echo "<td width='30px'>".round($aReviewers[$i][6],3)."</td>";
			if($showWeights == "1")
			{
				echo "<td width='30px'></td>";
			}
		}
	}
	echo "</tr>";
	//AVERAGE ALL
	echo "<tr class='tblItemAlt'>";
	//echo "<td colspan='".$colSpan."' align='right'><strong>Average of all Scores</strong></td>\n";
    echo "<td width='*' align='right'><strong>Average of all Scores</strong></td>\n"; 

	if(count($fieldsSel) > 0)
	{

		for($j=0; $j < count($fieldsSel); $j++)
		{
			if($fieldsSel[$j] > $revFieldsBeginAt)
			{
				$colspan++;
			}
		}

	}else
	{
		if($showWeights == "1")
		{
			$colspan = (count($aReviewers)*2+3);
		}else{
			$colspan = (count($aReviewers)+3);
		}
	}
	echo "<td colspan='".$colspan."'>".$sumAll. " / ".$numAll." = ".round($avgAll,3)."</td>\n";
	//echo "<td colspan='".$colspan."'>&nbsp;</td>\n";
	echo "</tr>";

	echo "</table>";


?>


	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>