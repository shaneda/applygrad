<?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

// Include review group class. 
include_once '../inc/class.ReviewGroups.php';
//include_once '../inc/class.ReviewGroupsPlb.php';

//include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

include '../inc/specialCasesAdmin.inc.php';

/*
* data, logic 
*/

$showFull = filter_input(INPUT_GET, 'showFull', FILTER_SANITIZE_STRING);
if (!$showFull) {
    $showFull = filter_input(INPUT_POST, 'showFull', FILTER_SANITIZE_STRING);    
}

// get data 
$reviewGroups = new ReviewGroups();
$reviewGroups->setGroupData();
$groups = $reviewGroups->getGroups();
$periodId = $reviewGroups->getPeriodId();
$departmentId = $reviewGroups->getDeptId();
$round = $reviewGroups->getRound();
$sort = $reviewGroups->getSort();
$searchString = $reviewGroups->getSearchString();
$selectedGroupID = $reviewGroups->getSelectedGroupID();
$members = $reviewGroups->getMembers();
$nonMembers = $reviewGroups->getNonMembers();

// set sort
if ( $sort ) $sortQuery = "&amp;sort=" . $sort;
else $sortQuery = "";

// create group list display
$groupList = "<div id='groupList'>";
foreach ( $groups as $groupID => $groupData )
{
	$groupName = $groupData['name'];
	$groupMemberCount = $groupData['memberCount'];	
		
	// determine if selected group
	if ( $groupID == $selectedGroupID )
	{
		$selected = "selected";
		$selectedGroupName = $groupName;
	} else {
		$selected = "";
	}
	$groupURL = $_SERVER['PHP_SELF'] . "?group=" . $groupID . "&amp;r=" . $round . $sortQuery;
    if ($periodId) {
        if (is_array($periodId)) {
            $groupURL .= '&period[]=' . implode('&period[]=', $periodId);
        } else {
            $groupURL .= '&amp;period=' . $periodId;
        }
    }
    if ($departmentId) {
        $groupURL .= '&amp;id=' . $departmentId;    
    }
	$groupList .= "<div id='group_$groupID' class='groupName $selected'><a href='$groupURL'>$groupName</a> 
		<span class='count'>($groupMemberCount)</span></div>";
}
$groupList .= "</div>";


// accurate count
$memberCount = 0;
foreach ( $members as $memberID )
{
	$applicant = $reviewGroups->getApplicant($memberID);
	if ($applicant) $memberCount++;
}

if ( $selectedGroupID )
{
	// beging members list
	$memberList = "<div id='memberList'>";
	$memberList .= "<h3>Members <span class='count'>(" . $memberCount . ")</span>";
    $memberList .= ' <span style="font-size: 12px;">';
    $memberList .= '<a href="applicantsGroups_assign.php?group=' . $selectedGroupID;
    if (is_array($periodId)) {
        $memberList .= '&period[]=' . implode('&period[]=', $periodId);    
    } else {
        $memberList .= '&period=' . $periodId;    
    }
    $memberList .= '&r=' . $round;
    if ($searchString) {
        $memberList .= '&searchString=' . $searchString;   
    } 
    if ($showFull == 'members') {
        $memberList .= '&showFull=nonmembers">&minus;&nbsp;Columns';  
    } else {
        $memberList .= '&showFull=members">+&nbsp;Columns';     
    }
    $memberList .= '</a></span></h3>';
		
	if ( $memberCount  > 0 )
	{		
        // create member list
        if ($showFull == 'members') {
            $memberList .= makeFullList('members', $members);   
        } else {
            $memberList .= makeBriefList('members', $members);    
        }
	}
	else
	{
		$memberList .= "No members have been assigned to this group";
	}
	$memberList .= "</div>";
	
	// create non member list
	$nonMemberCount = count($nonMembers);
	$nonMemberList = "<div id='nonMemberList'>";
	$nonMemberList .= "<h3>Non-Members&nbsp;<span class='count'>($nonMemberCount)</span>";
    $nonMemberList .= '&nbsp;<span style="font-size: 12px;">';
    $nonMemberList .= '<a href="applicantsGroups_assign.php?group=' . $selectedGroupID;
    if (is_array($periodId)) {
        $nonMemberList .= '&period[]=' . implode('&period[]=', $periodId);    
    } else {
        $nonMemberList .= '&period=' . $periodId;    
    }
    $nonMemberList .= '&r=' . $round; 
    if ($searchString) {
        $nonMemberList .= '&searchString=' . $searchString;   
    }
    if ($showFull == 'members') {
        $nonMemberList .= '&showFull=nonmembers">+&nbsp;Columns';  
    } else {
        $nonMemberList .= '&showFull=members">&minus;&nbsp;Columns';     
    }
    $nonMemberList .= '</a></span></h3>';
    
	if ( $nonMemberCount > 0 )
	{	
        if ($showFull == 'members') {
            $nonMemberList .= makeBriefList('nonmembers', $nonMembers);   
        } else {
            $nonMemberList .= makeFullList('nonmembers', $nonMembers);    
        }
    }
	else
	{
		$nonMemberList .= "No members to be assigned";
        if ( $searchString != "" && $showFull != 'members')
        {
        // include clear search button
            $nonMemberList .= <<<EOB
                &nbsp;&nbsp;
                <input type='button' name='clear_search' value='Clear Search'
                onClick='location.href="{$_SERVER['PHP_SELF']}?group={$selectedGroupID}&amp;r={$round}&amp;period={$periodId}&amp;id={$departmentId}&amp;showFull={$showFull}";' />    
EOB;
        }
	}
	
    
    $nonMemberList .= "</div>";

	$defaultGroupListDisplay = "none";
}

else
{
	$memberList = $nonMemberList = $selectedGroupName = "";
	$defaultGroupListDisplay = "inline";
}


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit Applicant Assignments';

$pageCssFiles = array(
    '../css/applicantGroups_assign.css'
    );

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

// period

$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids 
    if (is_array($periodId)) {
        // The scs period will be first and the compbio period will be second
        $displayPeriodId = $periodId[1];    
    } else {
        $displayPeriodId = $periodId; 
    }
    
    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    if ($endDate == '') {
        $endDate = 'present';    
    }
    $applicationPeriodDisplay = '<br/>' . $startDate . '&nbsp;to&nbsp;' . $endDate;
    $description = $period->getDescription();
    if ($description) {
        $applicationPeriodDisplay .= ' (' . $description . ')';    
    }
}

?>


<!-- begin b -->
<div id="pageHeading">
<div id="departmentName">
<?php
echo $reviewGroups->getDeptName();
if ($applicationPeriodDisplay) {
    echo $applicationPeriodDisplay;
}
?>
</div>
<div id="sectionTitle">Edit Applicant Assignments (Round <?= $round ?>)</div>
</div>
<!-- end b -->
<div id="groups">
	<div id="groupsPanel" style="float:left; display:<?= $defaultGroupListDisplay ?>; width:200px; background:#ffffff; z-index: 1000; border: 2px solid #0600F0; padding-left: 5px">
		<div id="groupsClose"><a href="" onclick="$('groupsPanel').hide(); return false;">X</a></div>
		<h2>Groups</h2>
	  	<?= $groupList; ?>
	</div>
	<div id="groupsHandle">
		<a href="" onclick="Effect.BlindRight('groupsPanel'); return false;">G<br/>R<br/>O<br/>U<br/>P<br/>S</a>
	</div>	
</div>

<?PHP if ($selectedGroupName != "") { ?>
<div id="applicants" >
<table border='0' cellpadding='5' cellspacing='5' width='98%' align='center' >
	<tr>
		<td valign="top" colspan='2'><h2><?= $selectedGroupName; ?></h2></td>
	</tr>
	<tr>
	<?php
    if ($showFull == 'members') {
    ?>
        <td valign='top'><?= $memberList; ?></td>
		<td valign="top" width='17%'><?= $nonMemberList; ?></td>
    <?php
    } else {
    ?>
        <td valign='top' width='17%'><?= $memberList; ?></td>
        <td valign="top"><?= $nonMemberList; ?></td>
    <?php
    }
    ?>
	</tr>
</table>
</div>

<?PHP } ?>

<script type="text/javascript" src="../inc/prototype.js"></script>
<script type="text/javascript" src="../inc/scriptaculous.js"></script> 
<script type="text/javascript">
    Effect.BlindRight = function(element) {
      element = $(element);
      var elementDimensions = element.getDimensions();
      return new Effect.Scale(element, 100, Object.extend({
        scaleContent: false,
        scaleY: false,
        scaleFrom: 0,
        scaleMode: {originalHeight: elementDimensions.height, originalWidth: elementDimensions.width},
        restoreAfterFinish: true,
        afterSetup: function(effect) {
          effect.element.makeClipping().setStyle({
            width: '0px',
            height: effect.dims[0] + 'px'
          }).show();
        },
        afterFinishInternal: function(effect) {
          effect.element.undoClipping();
        }
      }, arguments[1] || { }));
    };
    
    function toggleSelect( element, category )
    {
        var form = document.getElementById(category + 'Form');
        var checkboxStatus = element.checked;
        var formInputs = form.getElementsByTagName('input');
        for ( var i = 0; i < formInputs.length; i++)
        {
            if ( formInputs[i].name == 'application_id[]' ) 
            {
                formInputs[i].checked = false;
                if ( formInputs[i].disabled == false )
                {
                    formInputs[i].checked = checkboxStatus;            
                }
            }
        }
    }
    
    function filter (phrase, _id)
    {     
        var selectField = document.getElementById('column');
        var collNum = selectField.options[selectField.selectedIndex].value;
        var table = document.getElementById(_id);
        
        var words;
        var ele1;
        var ele2;

        for (var r = 1; r < table.rows.length; r++)
        {
            if ( collNum != -1)
            {
                words = phrase.value.toLowerCase();
                var cells = table.rows[r].cells;
                var match = 0;
                for ( var c = 0; c < cells.length; c++)
                {
                    if ( collNum == c )
                    {
                        ele1 = cells[c].innerHTML.replace(/<[^>]+>/g,"");
                        ele2 = ele1.replace(/&nbsp;/g, " ");
                        /*
                        for (var i = 0; i < words.length; i++) 
                        {
                            if (ele2.toLowerCase().indexOf(words[i])>=0)
                                match = 1;
                            else 
                            {
                                match = 0;
                                break;
                            }
                        }
                        */
                        if (ele2.toLowerCase().indexOf(words)>=0)
                        {
                            match = 1;
                            break;
                        } else {
                            match = 0;
                        }
                    }        
                }
                
                if ( match == 1 )
                {
                    var displayStyle = '';
                    var disabledValue = false;
                } else {
                    var displayStyle = 'none';
                    var disabledValue = true;
                }
                table.rows[r].style.display = displayStyle;
                table.rows[r].getElementsByTagName('input')[0].disabled = disabledValue;
            } 
            else 
            {   
                words = phrase.value.toLowerCase().split(" ");
                ele1 = table.rows[r].innerHTML.replace(/<[^>]+>/g,"");
                ele2 = ele1.replace(/&nbsp;/g, " ");
                var displayStyle = 'none';
                
                for (var i = 0; i < words.length; i++) 
                {
                    if (ele2.toLowerCase().indexOf(words[i])>=0)
                    {
                        displayStyle = '';
                        var disabledValue = false;
                    } else {
                        displayStyle = 'none';
                        var disabledValue = true;
                        break;
                    }
                }

                table.rows[r].style.display = displayStyle;
                table.rows[r].getElementsByTagName('input')[0].disabled = disabledValue;
            }
        }
    }
    
    function preventSubmit(event)
    {
        if (event.keyCode == 13)
        {
            return false;
        }
    }
    
</script>

<script type="text/javascript" src="../javascript/jquery-1.2.3.min.js"></script> 
<script type="text/javascript">
    jQuery.noConflict();
</script> 
<script type="text/javascript" src="../javascript/jquery.shiftclick.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('input.shiftclick').shiftClick();
    });
</script> 

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function makeBriefList($type = 'members', $applicantIds) {
        
    global $selectedGroupID;
    global $round;
    global $periodId;
    global $sortQuery;
    global $departmentId;
    global $searchString;
    global $reviewGroups;
    global $showFull; 
    
    if ($type == 'members') {
        $formName = 'membersForm';    
    } else {
        $formName = 'nonMembersForm';
    }

    $briefList = "<form action='{$_SERVER['PHP_SELF']}?group=" . $selectedGroupID . "&amp;r=" . $round . $sortQuery . 
        "' method='post' class='applicantForm' name='" . $formName . "' id='" . $formName . "'>";
    
    if (is_array($periodId)) {
        foreach ($periodId as $periodValue) {
            $briefList .= '<input type="hidden" name="period[]" value="' . $periodValue . '"/>';    
        }    
    } else {
        $briefList .= '<input type="hidden" name="period" value="' . $periodId . '"/>';     
    }
    
    if ($departmentId) {
        $briefList .= '<input type="hidden" name="id" value="' . $departmentId . '"/>';
    }

    if ($showFull) {
        $briefList .= '<input type="hidden" name="showFull" value="' . $showFull . '"/>';    
    }
    
    if ($searchString) {
        $briefList .= '<input type="hidden" name="searchString" value="' . $searchString . '"/>';     
    }

    if ($type == 'members') {
        $briefList .= "<div id='memberMenu' style='float: right;'>
                        <input type='submit' name='delete' value='Remove Selected &gt;&gt;' id='removeButton' />
                        </div>";
    } else {
        $briefList .= "<div id='nonmemberMenu' style='float: left;'>
                        <input type='submit' name='add' value='&lt;&lt; Add Selected' />
                        </div>"; 
    }
    
    $briefList .= "<br/><table border='0' cellpadding='0' cellspacing='1' width='100%'>";
    $briefList .= "<tr>
            <th><input type='checkbox' id='membersToggle' name='membersToggle' value='' onclick='toggleSelect(this, \"members\")' /></th>
            <th align='center'>Name</th>
            <!--
            <th align='center'>CITZN</th>
            -->
        </tr>";

    $rowNum = 0;
    foreach ( $applicantIds as $applicantId )
    {
        // determin if odd or even row
        $cellColor = ($rowNum % 2) ? 'odd' : 'even';
        
        $applicant = $reviewGroups->getApplicant($applicantId);
        if ($applicant)
        {
            $rowNum++;  // increment only if there is an applicant record
            
            // member info
            $firstName = $applicant['firstname'];
            $lastName = $applicant['lastname'];
            //$country = ucwords( strtolower($applicant['country']) );
            $country = $applicant['countryAbbr'];
            
            $adhoc = $applicant['adhoc'];
            if ( $adhoc ) $cellColor = 'adhoc';
            
            // member remove form/button
            $briefList .= "<tr>";
            $briefList .= "<td class='$cellColor' align='center'><input type='checkbox' class='shiftclick' name='application_id[]' value='$applicantId' /></td>";
            /*
            * HACK: the names are getting flipped in the data, and it's easier just to fix it here. 
            */
            //$briefList .= "<td class='$cellColor'>$lastName, $firstName</td>";
            $briefList .= "<td class='$cellColor'>$firstName, $lastName</td>";
            $briefList .= "<!--<td align='center' class='$cellColor'>$country</td>-->";
            $briefList .= "</tr>";
        }
    }
    $briefList .= "</table>";
    $briefList .= "</form>";
    
    return $briefList;
}

function makeFullList($type = 'nonmembers', $applicantIds) {    

    global $selectedGroupID;
    global $round;
    global $periodId;
    global $sortQuery;
    global $departmentId;
    global $searchString;
    global $reviewGroups;
    global $showFull; 
    
    if ($type == 'members') {
        $formName = 'membersForm';    
    } else {
        $formName = 'nonMembersForm';
    }
    
    $fullList = "<form action='{$_SERVER['PHP_SELF']}?group=" . $selectedGroupID . "&amp;r=" . $round . $sortQuery . 
        "' method='post' class='applicantForm' id='" . $formName . "' name='" . $formName . "'>";

    if (is_array($periodId)) {
        foreach ($periodId as $periodValue) {
            $fullList .= '<input type="hidden" name="period[]" value="' . $periodValue . '"/>';    
        }    
    } else {
        $fullList .= '<input type="hidden" name="period" value="' . $periodId . '"/>';     
    }
    
    if ($departmentId) {
        $fullList .= '<input type="hidden" name="id" value="' . $departmentId . '"/>';    
    }
    
    if ($showFull) {
        $fullList .= '<input type="hidden" name="showFull" value="' . $showFull . '"/>';    
    }

    // menu
    $fullList .= "<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
    $fullList .= "<tr>";
    
    // add button
    if ($type == 'nonmembers') {
        $fullList .= "<td width='30%'>
                <input type='submit' name='add' value='&lt;&lt; Add Selected' />
            </td>";        
    }

    // filter
    $fullList .= "<td width='40%'>";
    $fullList .= "<div id='include'>";
    $fullList .= "<b>Filter:</b> ";
    
    // select
    $fullList .= "<select name='column' id='column'>";
    if ( $round == 1 ) 
    {        
        $fullList .= "<option value='-1'>All</option>";
        $fullList .= "<option value='1'>Name</option>";
        $fullList .= "<option value='2'>Citizenship</option>";
        $fullList .= "<option value='3'>Gender</option>";
        $fullList .= "<option value='4'>Interests</option>";
        $fullList .= "<option value='5'>Institutions</option>";
        $fullList .= "<option value='6'>Groups</option>";
        $fullList .= "<option value='7'>R1 Count</option>";
        if ( isset($searchString) ) {
           $fullList .= "<option value='8'>Search</option>";
        }
    } 
    else 
    {
        $fullList .= "<option value='-1'>All</option>";
        $fullList .= "<option value='1'>Name</option>";
        $fullList .= "<option value='2'>Citizenship</option>";
        $fullList .= "<option value='3'>Gender</option>";
    /*    if ($departmentId == 3) {
            // Show programs for RI round 2
            $fullList .= "<option value='4'>Programs</option>";
        } else {
        */
            // Show interests otherwise
            $fullList .= "<option value='4'>Interests</option>";
      /*  }          */
        $fullList .= "<option value='5'>Totals</option>";
        $fullList .= "<option value='6'>Overridden</option>";
        if (!($departmentId == 3)) {
            // Don't show for RI round 2
            $fullList .= "<option value='7'>Institution</option>";
        }
        if (!isDesignDepartment($departmentId)) {
            $fullList .= "<option value='8'>Groups R1</option>";
            $fullList .= "<option value='9'>R1 Count</option>";
        }
        $fullList .= "<option value='10'>Groups R2</option>";
        $fullList .= "<option value='11'>R2 Count</option>";
        if ( isset($searchString) ) $fullList .= "<option value='12'>Search</option>";               
    }
    $fullList .= "</select> ";
    
    if ($type == 'members') {
        $fullList .= "<input name='filt' onkeyup=\"filter(this, 'memberTable', '1')\" type='text' />";        
    } else {
        $fullList .= "<input name='filt' onkeyup=\"filter(this, 'nonmemberTable', '1')\" type='text' />";    
    }

    $fullList .= "</div>";
    $fullList .= "</td>";
    
    //search
    if ($type != 'members') {
        $fullList .= "<td align='right'>";
        $fullList .= "<b>Search:</b> ";
        $fullList .= "<input type='text' id='searchString' name='searchString' ";
        if ( isset($searchString) ) {
            $fullList .= " value='" . $searchString . "' /> ";
        } else {
            $fullList .= " value='' > ";    
        }
        $fullList .= "<input type='submit' id='submitSearch' name='submitSearch' value='Go' />";
        if ( $searchString != "" )
        {
        // include clear search button
        $fullList .= <<<EOB
                <input type='button' name='clear_search' value='Clear Search'
                onClick='location.href="{$_SERVER['PHP_SELF']}?group={$selectedGroupID}&amp;r={$round}&amp;period={$periodId}&amp;id={$departmentId}&amp;showFull={$showFull}";' />    
EOB;
        }
        $fullList .= "</td>";
    }
    if ($type == 'members') {
        $fullList .= "<td width='30%'>
                <input type='submit' name='delete' value='Remove Selected &gt;&gt;' id='removeButton' />
            </td>";        
    }
    
    $fullList .= "</tr><tr><td colspan='3'>";
    
    // member table
    if ($type == 'nonmembers') {
        $fullList .= "<table border='0' cellpadding='' cellspacing='1' width='100%' id='nonmemberTable'>";       
    } else {
        $fullList .= "<table border='0' cellpadding='' cellspacing='1' width='100%' id='memberTable'>";    
    }
    
    
    // Table Header ( sorts commented ) 
    $fullList .= "<tr>";
    
    // toggle members checkbox
    if ($type == 'nonmembers') {
        $fullList .= "<th><input type='checkbox' id='nonMembersToggle' class='toggleCheckbox' name='toggle' value='' onclick='toggleSelect(this, \"nonMembers\")' /></th>";      
    } else {
        $fullList .= "<th><input type='checkbox' id='nonMembersToggle' class='toggleCheckbox' name='toggle' value='' onclick='toggleSelect(this, \"members\")' /></th>";   
    }
        
    // name
    $fullList .= "<th align='center'>Name</th>";
    
    // citizen
    $fullList .= "<th>Ctzn</th>";
    
    // Gender
    $fullList .= "<th>Gen</th>";
    
    // interests
    if ( // ($departmentId == 3 && $round == 2) || 
        isIniDepartment($departmentId)) {
        // Show programs for RI round 2
        $fullList .= "<th>Programs</th>";
    } else {
        // Show interests Otherwise
        $interestsSortHref = $_SERVER['PHP_SELF'] . "?group=" . $selectedGroupID . "&amp;r=" . $round;
        if ($periodId) {
            $interestsSortHref .= "&amp;period=" . $periodId;        
        }
        if ($showFull) {
            $interestsSortHref .= "&amp;showFull=" . $showFull;     
        }
        $fullList .= "<th>Interests 
                    <a href='" . $interestsSortHref . "&amp;sort=int1'>1</a> 
                    <a href='" . $interestsSortHref . "&amp;sort=int2'>2</a>
                    <a href='" . $interestsSortHref . "&amp;sort=int3'>3</a>
                    <a href='" . $interestsSortHref . "'>All</a>
                </th>";
    }
            
    // Round2: Totals
     if($round == 2)
         $fullList .= "<th>Totals</th>";

    // Round2: Overridden
     if($round == 2)
         $fullList .= "<th>Overridden</th>";                      
                     
    // institutions
    if ($departmentId == 3 && $round == 2) {
        // If RI round 2, don't show institution, but create empty column 
        // so filter column numbers don't have to be changed.
        $fullList .= "<th style='width: 0px;'></th>";
    } else {
        // Otherwise show institution
        $fullList .= "<th>Institution</th>";
    }
    
    if (!isDesignDepartment($departmentId)) {
        // group 1
        $fullList .= "<th align='center'>Groups R1</th>";
        $fullList .= "<th align='center'>R1 Count</th>";
    }

    // Round2: Groups 2
     if($round > 1) {
         $fullList .= "<th>Groups R2</th>";
         $fullList .= "<th>R2 Count</th>";
     }
     
     if($round > 2) {
         $fullList .= "<th>Groups R3</th>";
         $fullList .= "<th>R3 Count</th>";
     }
     
     if($round > 3) {
         $fullList .= "<th>Groups R4</th>";
         $fullList .= "<th>R4 Count</th>";
     }
                         
    // search context
    if ( $searchString )
    {
        $fullList .= "<th align='center' width='200px'>Search</th>";
    }
    
    $fullList .= "</tr>";

    $rowNum = 0;
    foreach ( $applicantIds as $applicantId )
    {
        
        // determin if odd or even row
        $cellColor = ($rowNum % 2) ? 'odd' : 'even';

        $applicant = $reviewGroups->getApplicant($applicantId);
        
        if ($applicant)
        {
            $rowNum++;  // increment only if there is an applicant record 
            
            $nameLink = $applicant['nameLink'];
            $country = $applicant['countryAbbr'];
            if ( $country == "" ) $country = "&nbsp;";
            $gender = $applicant['gender'];
            if ( $gender == "" ) $gender = "&nbsp;";
            $programs = $applicant['programs'];
            $interests = $applicant['interests'];
            $institution = $applicant['undergradName'];
            
            // totals
            $yes = $applicant['yes'];
            $no = $applicant['no'];
            
            // overridden
            $overridden = $applicant['overridden'];
            
            // groups
            $groups1 = $applicant['groups'];
            if ( $groups1 == "" ) $groups1 = "&nbsp;";
            $groups2 = $applicant['groups2'];
            if ( $groups2 == "" ) $groups2 = "&nbsp;";
            $groups3 = $applicant['groups3'];
            if ($groups3 == "" ) $groups3 = "&nbsp;";    
            $groups4 = $applicant['groups4'];
            if ($groups4 == "" ) $groups4 = "&nbsp;";    
            
            $adhoc = $applicant['adhoc'];
            if ( $adhoc ) $cellColor = 'adhoc';
            
            // search context text
            $searchContext = $applicant['searchContext'];
            if ( $searchString )
            {
                $ssParts = split(" ", $searchString);                        
                foreach ( $ssParts as $word )
                {
                    $replace = "<b>$word</b>";
                    $searchContext = str_replace( $word, $replace, $searchContext );
                }
            }
            
            // build rows
            $fullList .= "<tr>";
            
            // non member remove form/button
            $fullList .= "<td class='$cellColor' align='center'>";
            $fullList .= "<input type='checkbox' class='shiftclick' name='application_id[]' value='$applicantId' />";
            $fullList .= "</td>";
            
            // name
            $fullList .= "<td class='$cellColor'>";
            $fullList .= $nameLink;
            $fullList .= "</td>";

            // citizenship
            $fullList .= "<td align='center' class='$cellColor'>";
            $fullList .= $country;
            $fullList .= "</td>";

            // gender
            $fullList .= "<td align='center' class='$cellColor'>";
            $fullList .= $gender;
            $fullList .= "</td>";                        
                
            // interests / programs
            // html for the interests is in the query
            $fullList .= "<td class='$cellColor'>";
            if (   //($departmentId == 3 && $round == 2) || 
            isIniDepartment($departmentId)) {
                // Show programs for RI round 2
                $fullList .= str_replace('|', '<br>', $programs);                  
            } else {
                // Show interests otherwise
                $fullList .= $interests;    
            }
            $fullList .= "</td>";
                                      

            // Round 2: totals
            if ( $round == 2 )
            {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= "<div class='totals'>Yes:&nbsp;$yes<br/>No:&nbsp;$no</div>";
                $fullList .= "</td>";                            
            }
            
            // Round 2: overridden
            if ( $round == 2 )
            {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= $overridden;
                $fullList .= "</td>";                            
            }                    
                
            // institution      
            if (($departmentId == 3 && $round == 2)) {
                // If RI round 2, don't show institution, but create empty column 
                // so filter column numbers don't have to be changed.
                $fullList .= "<td class='$cellColor' style='width: 0px;'>";
            } else {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= "<div class='institution'>$institution</div>";                       
            }
            $fullList .= "</td>";
            if (!isDesignDepartment($departmentId)) {
                // groups 1
                $fullList .= "<td class='$cellColor'>";
                $fullList .= $groups1;
                $fullList .= "</td>";
                
                $fullList .= "<td class='$cellColor'>";
                $fullList .= getGroupCount($groups1);
                $fullList .= "</td>";                    
            }
            // Round 2: groups 2
            if ( $round > 1 )
            {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= $groups2;
                $fullList .= "</td>";    
                
                $fullList .= "<td class='$cellColor'>";
                $fullList .= getGroupCount($groups2);
                $fullList .= "</td>";                         
            }
            // Round 3: groups 3
            if ( $round > 2 )
            {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= $groups3;
                $fullList .= "</td>";    
                
                $fullList .= "<td class='$cellColor'>";
                $fullList .= getGroupCount($groups3);
                $fullList .= "</td>";                         
            } 
            // Round 4: groups 4                  
            if ( $round > 3 )
            {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= $groups4;
                $fullList .= "</td>";    
                
                $fullList .= "<td class='$cellColor'>";
                $fullList .= getGroupCount($groups4);
                $fullList .= "</td>";                         
            }  
            // search context
            if ( $searchString )
            {
                $fullList .= "<td class='$cellColor'>";
                $fullList .= "<div class='searchContext'>$searchContext</div>";
                $fullList .= "</td>";                        
            }
                                                                                                
            $fullList .= "</tr>";

        }
    }
    $fullList .= "</table></td></tr></table>";
    $fullList .= "</form>";
    
    return $fullList;        
}

function getGroupCount($groups) {

    $groupsTrimmed = trim($groups);
    
    if ($groupsTrimmed == '' || $groupsTrimmed == '&nbsp;') 
    {    
        $groupsCount = 0;
    }
    else
    {
        $groupsArray = explode('<br/>', $groupsTrimmed);
        $groupsCount = count($groupsArray);    
    }
    
    return $groupsCount;
}
?>