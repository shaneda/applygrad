<?PHP
$startTime = microtime(TRUE);
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;

// Include db classes.
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

include '../inc/reviewListFunctions.inc.php';
include '../inc/inc.decisionBreakdownData.php';

// Main request vars
$departmentId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);

$sortable = FALSE;
$admittedOnly = TRUE;
$show = filter_input(INPUT_POST, 'show', FILTER_SANITIZE_STRING);
if ($show != 'admitted') {

    $show = 'all';
    $admittedOnly = FALSE;
    
    if ($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 10) {
        $sortable = TRUE;    
    }
}

$save = filter_input(INPUT_POST, 'save', FILTER_VALIDATE_INT);
if ( $save && ($save == 1) ) {
    $saveDepartmentId = filter_input(INPUT_POST, 'departmentId', FILTER_VALIDATE_INT);
    $savePeriodId = filter_input(INPUT_POST, 'periodId', FILTER_VALIDATE_INT);
    $saveDecision = filter_input(INPUT_POST, 'decision', FILTER_UNSAFE_RAW);
    $rankings = json_decode($_POST['rankings']);
    saveDecisionRank($saveDepartmentId, $savePeriodId, $saveDecision, $rankings);
    saveDecisionRankMembers($saveDepartmentId, $savePeriodId, $saveDecision, $rankings);
    echo '1';
    exit;
}

// Period display
$departmentName = '';
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = '';
if ($periodId) {

    $applicationPeriodDisplay = getDepartmentName($departmentId);
    
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay .= '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Admission Committee Decision Breakdown';

$pageCssFiles = array(
    '../css/jquery.ui.all.css',
    '../css/reviewApplications.css',
    '../css/decisionBreakdown.css'
    );

$headJavascriptFiles = array(
    '../javascript/jquery-1.6.2.min.js',
    '../javascript//jquery-ui-1.8.16.custom.min.js',
    '../javascript/json2.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?> 
<div id="pageHeading">
    <div id="departmentName"><?php echo $applicationPeriodDisplay; ?></div>            
    <div id="sectionTitle"><?php echo $pageTitle; ?></div>
</div>

<div style="margin:7px; clear: both;">
<br/>
<form id="form1" name="form1" action="" method="post">
	
	<b>Applicants by Decision</b>&nbsp;&nbsp;
    <?php
    if (is_array($periodId)) {
        $periodParam = 'period[]=' . implode('&period[]=', $periodId);    
    } else {
        $periodParam = 'period=' . $periodId;
    }
    ?>
    <a href="decision_breakdown_aoi.php?id=<?=$departmentId?>&<?php echo $periodParam; ?>">Applicants by AOI / Decision</a>&nbsp;&nbsp;
    <a href="get_decision_counts.php?id=<?=$departmentId?>&<?php echo $periodParam; ?>">Counts by Decision / AOI</a>&nbsp;&nbsp; 
    <br/><br/>
<?php
if ($show == 'all') {
    $allSelected = 'checked';
    $admittedSelected = '';    
} else {
    $allSelected = '';
    $admittedSelected = 'checked';
}
?>    
<b>Show:</b>
<input type="radio" name="show" value="all" <?php echo $allSelected; ?>> All 
<input type="radio" name="show" value="admitted" <?php echo $admittedSelected; ?>> Admitted Only
&nbsp;&nbsp;<input type="submit" value="Change" style="font-size: 10px;" />
</form>
</div>

<div id="#content" style="clear: both; height: 100%;">
<?php
$decisionRecords = getDecisionRecords($departmentId, $periodId, $admittedOnly);
foreach($decisionRecords as $decision => $records) {
    echo makeDecisionDiv($decision, $records, $sortable) . "\n\n";
}  
?>
</div>

<form action="" method="POST" name="editForm" id="editForm">
    <input type='hidden' name='userid' value=''>
</form>

<script type="text/javascript">

function openForm(userid, formname) { 
    var editForm = document.getElementById('editForm');
    editForm.elements['userid'].value = userid;
    editForm.action = formname;
    editForm.target = '_blank';
    editForm.submit();
}

$(document).ready(function() {
    
    $(".draggable").draggable({
        handle: '.draggable_handle', 
        cursor: 'pointer', 
        stack: '.draggable'
    });
    
    $(".sortable").sortable({
        handle: 'span.name',
        containment: 'parent',
        cursor: '<?php echo $sortable ? 'pointer' : 'text'; ?>'    
    });

    $(".save").click(function() {
        
        var idPieces = $(this).attr("id").split("_");
        var decision = idPieces[1];
        var departmentId = idPieces[2];
        var periodId = idPieces[3];

        var rankings = new Array();
        $("#groupMembers_" + decision).find("li").each(function (i) {
            var applicationIdPieces = $(this).attr("id").split("_");
            var applicationId = applicationIdPieces[1];
            rankings.push(applicationId);
        });

        $.post(
            'decision_breakdown.php', 
            { 
                save: "1",
                departmentId: departmentId,
                periodId: periodId,
                decision: decision, 
                rankings: JSON.stringify(rankings)
            },
            function(data){
                if (data != -1) {
                    alert(decision + ' rankings saved.');    
                } else {
                    alert(error);    
                }           
            },
            'text'
        );
        
    });
});
</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function makeDecisionDiv($decision, $records, $sortable = TRUE) {
    
    global $departmentId;
    global $periodId;
    
    $recordCount = count($records);
    
    if ($sortable) {
        $class = 'sortable';
        $saveDiv = '<input type="button" value="Save" 
            id="save_' . $decision . '_' . $departmentId . '_' . $periodId
            . '" class="save"  style="font-size: 10px;" />';
    } else {
        $class = 'viewable';
        $saveDiv = '';
    }
    
    $div = <<<EOB
        <div id="groupMembers_{$decision}" class="draggable" >
        <div class="draggable_handle">
            <b>{$decision}</b> <i>({$recordCount})</i>
        </div>
        <div>
            {$saveDiv}    
        </div>
        <div class="draggable_list">
        <ol class="{$class}" style="margin-top: 10px; clear: left;">
EOB;
    foreach ($records as $record) {
        $div .= makeLi($record) ."\n";
    }
    $div .= <<<EOB
        </ol>
        </div>
        </div>
EOB;

    return $div;
}

function makeLi($record) {
    
    global $departmentId;
    
    $statusKey = array(
        0 => 'R',
        1 => 'W',
        2 => 'A'
    );
    
    $applicationId = $record['application_id'];
    $luuId = $record['luu_id'];
    $admissionStatus = $statusKey[$record['admission_status']];
    
    $li = '<li id="application_' . $applicationId . '" class="groupMember">';
    $li .= '<a href="javascript: openForm(\'' . $luuId . '\',';
    $li .= '\'../review/reviewApplicationSingle.php?applicationId=';
    $li .= $applicationId . '&v=2&r=2&d=' . $departmentId . '&showDecision=1\')">';
    $li .= 'View</a>&nbsp;&nbsp;';
    $li .= '<span class="name" style="cursor: pointer;">';
    $li .= $record['name'];
    $li .= '&nbsp;<i>(' . $admissionStatus . ')</i>';
    $li .= '</span>';  
    $li .= '</li>';

    return $li;
}
?>