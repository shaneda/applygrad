<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->

<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" name="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <?
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="sysusersEdit.php?id=<?=$_SESSION['A_usermasterid']?>">Change Password</a>			<a href="logout.php"><span class="subtitle">Logout</span></a>
			<? } ?>			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <?
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "2":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:

			break;

		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Send Email to Unsubmitted Recommenders <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<? showEditText("Send", "button", "btnSubmit", $_SESSION['A_allow_admin_edit']); ?>
	<br />
	<a href="content.php">(Edit email template) </a><br />
<br />
	<?
$domains = array();
$content = array();
$deptId = -1;
$sysemail = "applygrad+@cs.cmu.edu";


if(isset($_GET['id']))
{
	$deptId = intval($_GET['id']);
}

//GET EMAIL TEMPLATE
$str = "";
$sql = "select content from content
left outer join lu_domain_department on lu_domain_department.domain_id = content.domain_id
where content.name='Recommendation Reminder Letter' and lu_domain_department.department_id=".$deptId;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$str = html_entity_decode($row["content"]);
}
?>
    <div id="div1" style="width:600px; background-color:#CCCCCC; padding:10px "><?=$str?></div><br />

<?
$sql = "select
		lu_users_usertypes.id as userid,
		concat(users.lastname, ', ',users.firstname) as name,
		users.email,
		users.guid,
		
		users_b.firstname as firstname,
		users_b.lastname as lastname,
		users_b.email as appemail,
		lu_domain_department.domain_id as domainid

		from lu_users_usertypes
		
		inner join users as users on users.id = lu_users_usertypes.user_id
		inner join recommend on recommend.rec_user_id = lu_users_usertypes.id
		left outer join application on application.id = recommend.application_id

		inner join lu_users_usertypes as lu_users_usertypes_b on lu_users_usertypes_b.id = application.user_id
		inner join users as users_b on users_b.id = lu_users_usertypes_b.user_id

		left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
		where
		recommend.submitted != 1
		and lu_programs_departments.department_id=".$deptId . " group by recommend.id order by users.email";
	$result = mysql_query($sql) or die(mysql_error().$sql);

if(isset($_POST['btnSubmit']) && $deptId > -1)
{

	if($str != "")
	{

		//PULL USERS FOR THIS DEPARTMENT

		?>
		<table width="500" border="0" cellspacing="0" cellpadding="4">
		<?
		echo "Mail sent to:<br>";
		$str2 = "";
		while($row = mysql_fetch_array( $result ))
		{
			$vars = array(
			array('appemail',$row['appemail']),//STUDENT
			array('firstname',$row['firstname']),//STUDENT
			array('lastname',$row['lastname']),//STUDENT
			array('domainid',$row['domainid']),//STUDENT
			array('user', str_replace("+","%2b",$row['email']) ),//RECOMMENDER
			array('pass',$row['guid']),
			array('replyemail',$sysemail),
			);
			$str2 = parseEmailTemplate2($str, $vars );
			//SEND EMAIL
			sendHtmlMail($sysemail, $row['email'], "Recommendation Reminder Letter", $str2, "reminder");//to user
			//echo $str2 . "<br><br>";
			?>
			<tr>
			<td><strong><?=$row['name']?></strong></td>
			<td><em>(<?=$row['email']?>)</em></td>
			<td>for <?=$row['firstname']. " " . $row['lastname']?></td>
		  </tr>
			<? } ?>
			</table>
	<?
	}else
	{
		echo "<strong>Email Template Cannot be Empty.</strong> Please edit the template <a href='content.php'>here<a/>";
	}//end if
}else
{
	echo "Mail will be sent to:<br>";
	?>
	<table width="500" border="0" cellspacing="0" cellpadding="4">
	<? 	while($row = mysql_fetch_array( $result )){	?>
	  <tr>
		<td><strong><?=$row['name']?></strong></td>
		<td><em>(<?=$row['email']?>)</em></td>
		<td>for <?=$row['firstname']. " " . $row['lastname']?></td>
	  </tr>
	<? } ?>
	</table>
	<? } ?>

	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
