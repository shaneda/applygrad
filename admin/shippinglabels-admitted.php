<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? //include_once '../inc/functions.php'; ?>
<?
define('FPDF_FONTPATH','../inc/fpdf/font/');
require_once('../inc/fpdf/PDF_Label.php');

$deptId = 1;
//if(isset($_GET['id']))
//{
	//$deptId = intval($_GET['id']);
//}

// PLB uncommented deptId check 03/04/09
if(isset($_GET['id']))
{
    $deptId = intval($_GET['id']);
}

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
} 

?>

<?

		$sql = "select 
		distinct
		firstname, lastname, email, address_cur_street1, gender, 
		address_cur_street2, 
		address_cur_street3, 
		address_cur_street4, 
		address_cur_city, 
		states.abbrev as state, 
		address_cur_pcode, 
		countries.name as country,
                address_cur_tel,
		GROUP_CONCAT(concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)) as program,
		lu_application_programs.faccontact,
		lu_application_programs.stucontact
		from  lu_users_usertypes 
		inner join users on users.id =  lu_users_usertypes.user_id
		inner join users_info on users_info.user_id= lu_users_usertypes.id
		left outer join countries on countries.id = address_cur_country
		left outer join states on states.id = address_cur_state
        left outer join application on application.user_id = lu_users_usertypes.id";
        
        // PLB added period join 1/7/10
        if ($periodId) {
            $sql .= " INNER JOIN period_application ON application.id = period_application.application_id";
        }        
        
        $sql .= " left outer join lu_application_programs on lu_application_programs.application_id = application.id
		left outer join programs on programs.id = lu_application_programs.program_id
		left outer join degree on degree.id = programs.degree_id
		left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		
		where lu_application_programs.admission_status = 2
		and lu_programs_departments.department_id= ".$deptId;

        // PLB added period where 1/7/10
        if ($periodId) {
            $sql .= " AND period_application.period_id = " .  $periodId;
        } 
		
        $sql .= " group by  application.id
		order by lastname, firstname
		";
		$result = mysql_query($sql) or die(mysql_error() . $sql);
		$count = 0;
                
      //          $ups_headers = "Attention^Address1^Address2^Address3^Address4^City^State_Prov^PostalCode^Country^Telephone\n";
      //          $filename = "UPSLABEL.txt";
      //          $handle = fopen($filename, 'w+');
      //          fwrite($handle, $ups_headers);

		ob_clean();
        $pdf = new PDF_Label('5363', 'mm', 1, 2);

$pdf->Open();
$pdf->AddPage();
//DebugBreak();
		while($row = mysql_fetch_array( $result )) 
		{
			$gender = "";
            $address1 = "";
             $address2 = "";
              $address3 = "";
               $address4 = "";
               $state = ""; 
               $currentPCode = "";
               $country  = "";
		/*	$program = $row['program'];
			
			$aPrograms = split(",", $program);
			if(count($aPrograms) > 1)
			{
				$program = "";
				for($i = 0; $i < count($aPrograms); $i++)
				{
					if($i == 0)
					{
						$program .= $aPrograms[$i] . " program";
					}else
					{
						if($i == count($aPrograms)-1)
						{
							$program .= " and the ". $aPrograms[$i]. " program";
						}else
						{
							$program .= ", the ". $aPrograms[$i]. " program";
						}
						
					}
				}
			}
            */
			if($row['gender'] == "M")
            {
                $gender = "Mr. ";
            }
            if($row['gender'] == "F")
            {
                $gender = "Ms. ";
            }
			$address1 = ucwords(strtolower($row['address_cur_street1']));
			if($row['address_cur_street2'] != "")
			{
				$address2 = ucwords(strtolower($row['address_cur_street2']));
			}
			if($row['address_cur_street3'] != "")
			{
				$address3 = ucwords(strtolower($row['address_cur_street3']));
			}
			if($row['address_cur_street4'] != "")
			{
				$address4 = ucwords(strtolower($row['address_cur_street4']));
			}
			$city = $row['address_cur_city'];
			if($row['state'] != "")
			{
				$state = $row['state'];
			}
			if($row['address_cur_pcode'] != "")
			{
				$currentPCode = " ". $row['address_cur_pcode']. "";
			}
                        
			$country = ucwords(strtolower($row['country']));

	//		if($count % 6 == 0)
	//		{
	//			echo "</td ></tr></table><table cellspacing=4 cellpadding=4><tr>";
	//		}
	//		?><?
	//		echo $gender.ucwords(strtolower($row['firstname'])). " ". ucwords(strtolower($row['lastname']))."<br>";
	//		echo $address;
	//		?><?
	//		$count++;
                        // Write shipping info for UPS
   /*                     $dataline = $gender.ucwords(strtolower($row['firstname'])).' '
                                  . ucwords(strtolower($row['lastname'])).'^'
                                  . ucwords(strtolower($row['address_cur_street1'])).'^'
                                  . ucwords(strtolower($row['address_cur_street2'])).'^'
                                  . ucwords(strtolower($row['address_cur_street3'])).'^'
                                  . ucwords(strtolower($row['address_cur_street4'])).'^'
                                  . $row['address_cur_city'].'^'
                                  . $row['state']. '^'
                                  . $row['address_cur_pcode']. '^'
                                  . ucwords(strtolower($row['country'])). '^'
                                  . $row['address_cur_tel']. "\n";

                        fwrite($handle, $dataline);
               */         
                        // build name line
                        $entireAddress = array();
                        $nameLine = $gender.ucwords(strtolower($row['firstname'])).' '
                                  . ucwords(strtolower($row['lastname']));
                        array_push($entireAddress, $nameLine);
                        array_push($entireAddress, $address1);
                        $numAdditionalLines = 0;
                        if ($address2 != "") { 
                            array_push($entireAddress, $address2);
                            $numAdditionalLines++; }
                        if ($address3 != "") { 
                            array_push($entireAddress, $address3);
                            $numAdditionalLines++; }
                        if ($address4 != "") { 
                            array_push($entireAddress, $address4);
                            $numAdditionalLines++; }
                        $cityLine = "";
                        if($city != "") {$cityLine .= $city.", ";}
                        if ($state != "") {$cityLine .= $state." ";}
                        if ($currentPCode != "") {$cityLine .= $currentPCode;}
                        array_push($entireAddress, $cityLine);
                        $countryLine = "";
                        if ($country != "" && $country != "United States") {
                            $countryLine = $country;
                        }
                        if ($countryLine != "") {
                            array_push($entireAddress, $countryLine);
                            $numAdditionalLines++;
                        }
                       
                        switch  ($numAdditionalLines) {
                            case 0:
                                $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2]));
                                break;
                            case 1:
                                $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3]));
                                break;
                            case 2:
                                $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3], $entireAddress[4]));
                                break;
                            case 3:
                                $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3], $entireAddress[4], $entireAddress[5]));
                                break;
                            case 4:
                                $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3], $entireAddress[4], $entireAddress[5], $entireAddress[6]));
                                break;
                        }
                                  
              //          $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s, %s, %s",$nameLine, 'Immeuble Titi', 'av. fragonard', '06000', 'NICE', 'FRANCE'));

		}
          //      fclose($handle);
                $pdf->Output();
?>

