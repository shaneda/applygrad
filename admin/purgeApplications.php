<?php
/*
* purgeUnsubmitted.php : clean up orphan and unsubmitted applications from the applyweb db.
*/
ini_set('memory_limit', '128M');
set_time_limit(600);
include_once '../inc/db_connect.php';
include_once '../inc/session_admin.php';
define("CLASS_DIR", "../classes/");
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Applyweb_Table.php';
include '../inc/unitPeriodIncludes.inc.php';

if ($_SESSION['A_usertypeid'] != 0) {
    echo 'You are not authorized to purge applications.';
    exit;
}

/*
* Instantiate a (global) DB_Applyweb object to handle applyweb queries.
*/
$db_applyweb = new DB_Applyweb();

/*
* Pick the low hanging fruit: find orphan applications, i.e., applications 
* that can't be matched to any lu_users_usertypes or users records.
*/
$orphanQuery = "SELECT application.id AS application_id,
                    application.user_id AS lu_users_usertypes_id,
                    lu_users_usertypes.usertype_id,
                    users.id AS users_id,
                    users.guid,
                    users_info.id AS users_info_id
                    FROM application
                    LEFT OUTER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
                    LEFT OUTER JOIN users ON lu_users_usertypes.user_id = users.id
                    LEFT OUTER JOIN users_info ON application.user_id = users_info.user_id
                    WHERE application.user_id = -1
                    OR lu_users_usertypes.id IS NULL
                    OR lu_users_usertypes.user_id = -1
                    OR users.id IS NULL
                    ORDER BY application.id";
//$orphanQuery .= " LIMIT 500";
$orphanApplications = $db_applyweb->handleSelectQuery($orphanQuery);
$orphanApplicationCount = count($orphanApplications);

/*
* Find all unsubmitted applications that aren't part of an active period. 
*/

// Get the period_ids of all active periods
$periodQuery = "SELECT * FROM period WHERE period_type_id = 1";
$periodRecords = $db_applyweb->handleSelectQuery($periodQuery);
$activePeriodIds = array();
foreach ($periodRecords as $periodRecord) {
    $periodId = $periodRecord['period_id'];
    $period = new Period($periodId);
    $status = $period->getStatus();
    if ($status == 'active') {
        $activePeriodIds[] = $periodId;    
    }
}
$activePeriodInString = implode(',', $activePeriodIds);

// Get the earliest start date of the active periods.
$minDateQuery = "SELECT MIN(start_date) AS start_date 
                FROM period 
                WHERE period_id IN (" . $activePeriodInString . ")";
$minDateRecords = $db_applyweb->handleSelectQuery($minDateQuery);
foreach ($minDateRecords as $minDateRecord) {
    $minDateString = $minDateRecord['start_date'];
}

// Get all unsubmitted applications that are not associated with active periods
// and were not started after the earliest active start date (in case) 
$unsubmittedQuery = "SELECT application.id AS application_id, 
                        application.user_id AS lu_users_usertypes_id,
                        lu_users_usertypes.usertype_id, 
                        lu_users_usertypes.user_id AS users_id,
                        users.guid,
                        users_info.id AS users_info_id
                        FROM application
                        LEFT OUTER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
                        LEFT OUTER JOIN period_application on application.id = period_application.application_id
                        LEFT OUTER JOIN users_info ON application.user_id = users_info.user_id
                        LEFT OUTER JOIN users ON lu_users_usertypes.user_id = users.id
                        WHERE submitted = 0
                        AND 
                        ( 
                            (period_application.period_id IS NOT NULL 
                            AND period_application.period_id NOT IN (" . $activePeriodInString . ")
                            )
                        OR
                            (period_application.period_id IS NULL
                            AND application.created_date < '" . $minDateString . "' 
                            )
                        )
                        ORDER BY application.created_date";
//$unsubmittedQuery .= " LIMIT 500";
$unsubmittedApplications = $db_applyweb->handleSelectQuery($unsubmittedQuery, 'application_id');
$unsubmittedApplicationIds = array_keys($unsubmittedApplications);
$unsubmittedApplicationCount = count($unsubmittedApplications);


/*
* Handle the purges upon form submission.
*/

// Echo queries when testing; execute them when not. 
$echoOnly = TRUE;
$commit = filter_input(INPUT_GET, 'commit', FILTER_VALIDATE_INT);
if ($commit) {
    $echoOnly = FALSE;    
}
$doPurge = filter_input(INPUT_GET, 'doPurge', FILTER_VALIDATE_INT);
ob_start();
if ($doPurge) {
    
    /*
    * Set up a db connection to use for copying purged records to the purge db. 
    */
    $copyConnection = mysql_connect($db_host, $db_username, $db_password);

    /*                                                                      
    * Assemble a (global) array of the names of tables that have an application_id (fk) field.
    */
    $schemaConnection = mysql_connect($db_host, $db_username, $db_password);
    mysql_selectdb('information_schema', $schemaConnection);
    $schemaQuery = "SELECT DISTINCT TABLE_NAME
                    FROM COLUMNS
                    WHERE TABLE_SCHEMA LIKE '" . $db . "'
                    AND TABLE_NAME NOT LIKE 'vw_%'
                    AND TABLE_NAME NOT LIKE 'bf_%' 
                    AND COLUMN_NAME = 'application_id'";
    $schemaResult = mysql_query($schemaQuery, $schemaConnection);
    $fkApplicationIdTables = array();
    while ($row = mysql_fetch_array($schemaResult)) {
        $fkApplicationIdTables[] = $row['TABLE_NAME'];    
    }
    mysql_close($schemaConnection);

    /*
    * Assemble an array of names of tables that have a user_id (fk) field.
    * (This is only used to do the table optimizations at the end.) 
    */
    $fkUserIdTables = array(
        'datafileinfo', 
        'usersinst',
        'users_info',
        'lu_user_department',
        'users',
        'lu_users_usertypes',
        'application'
        );

    /*
    * Set up a (global) array to hold the delete counts.
    */
    $deleteCounts = array();
    $deleteCounts['total_applications_deleted'] = 0;

    /*
    * Pick the low hanging fruit: delete orphan applications, i.e., applications 
    * that can't be matched to any lu_users_usertypes or users records.
    */
    deleteApplicationRecords($orphanApplications);

    /*
    * Delete all unsubmitted applications that aren't part of an active period. 
    */
    deleteApplicationRecords($unsubmittedApplications);

    /*
    * Clean up overhead on the affected tables.
    * (Queries will be slooow if this isn't done.)
    */
    foreach ($fkApplicationIdTables as $table) {
        $optimizeQuery = "OPTIMIZE TABLE " . $table;
        if ($echoOnly) {
            echo $optimizeQuery . "<br/>\n";
        } else {
            $db_applyweb->handleInsertQuery($optimizeQuery);    
        }
    }
    foreach ($fkUserIdTables as $table) {
        $optimizeQuery = "OPTIMIZE TABLE " . $table;
        if ($echoOnly) {
            echo $optimizeQuery . "<br/>\n";
        } else {
            $db_applyweb->handleInsertQuery($optimizeQuery);    
        }
    }

    /*
    * Catpture, echo output 
    */
    if ( !file_exists('purge') ) {
        mkdir('purge', 0775);    
    }
    $purgeDb = $db . 'Purges'; 
    $reportTime = time(); 
    $reportFile = 'purge/purgeApplications' . date('Y-m-d-H-i-s', $reportTime) . '.txt';
    if ($echoOnly) {
        $outputHeading = 'DRY RUN: ';
    } else {
        $outputHeading = '';    
    }
    $outputHeading .= "Applications purged from " . $db . " on " . $hostname . "\n";
    $outputHeading .= "Moved to " . $purgeDb . " on " . $hostname . "\n";
    $outputHeading .= date('r', $reportTime) . "\n\n";
    $outputText = $outputHeading . print_r($deleteCounts, TRUE);
    $fp = fopen($reportFile, 'w');
    fwrite($fp, $outputText);
    fclose($fp);
}
$dryRunOutput = ob_get_contents();
ob_end_clean(); 
?>

<html>
<head>
    <title>Purge Applications</title>
</head> 
<body>

<?php
if ($doPurge) {
    
    if ($echoOnly) {
        echo "<p>DRY RUN</p>";
    }

    echo <<<EOB
    <p>{$orphanApplicationCount} orphaned applications moved to {$purgeDb}</p>
    <p>{$unsubmittedApplicationCount} unsubmitted applications moved to {$purgeDb}</p> 
    <a href="{$reportFile}" target="_blank">View Report</a>
    <br/><br/>
    <a href="purgeApplications.php">&laquo; Back</a>
    <br/><br/>
    {$dryRunOutput}
EOB;

} else {
    
    echo <<<EOB

    <p>There are {$orphanApplicationCount} orphaned applications in {$db}</p>
    <p>There are {$unsubmittedApplicationCount} unsubmitted applications in {$db}</p> 
    <br/>
    <form action="" method="get">
        <input type="hidden" name="doPurge" value="1" />
        <input type="radio" name="commit" value="0" checked="checked" /> Dry Run
        <input type="radio" name="commit" value="1" /> Commit
        <br/>
        <input type="submit" value="Purge Orphan/Unsubmitted Applications" />    
    </form>
    
EOB;

}
?>

</body>
</html>


<?php
/*
* Function to delete all application-related records. 
*/
function deleteApplicationRecords($applicationRecords, $deleteUserRecords = TRUE) {

    // Cheating - these are set first thing in the script.
    global $echoOnly;
    global $db_applyweb; 
    global $fkApplicationIdTables;
    global $unsubmittedApplicationIds;
    global $deleteCounts;
    
    foreach ($applicationRecords as $applicationRecord) {
        
        $applicationId = $applicationRecord['application_id'];
        $luUsersUsertypesId = $applicationRecord['lu_users_usertypes_id'];
        $usertypeId = $applicationRecord['usertype_id'];
        $usersId = $applicationRecord['users_id'];
        $guid = $applicationRecord['guid'];
        $users_infoId = $applicationRecord['users_info_id'];  
        $deleteCounts[$applicationId] = array(
            'application_id' => $applicationId,
            'lu_users_usertypes_id' => $luUsersUsertypesId,
            'usertype_id' => $usertypeId,
            'users_id' => $usersId,
            'users_info_id' => $users_infoId,
            'deleted_record_counts' => array()
            );
        
        /* 
        * Delete records from tables that have an application_id foreign key.
        */
        foreach ($fkApplicationIdTables as $fkApplicationIdTable) {
            
            $deleteCounts[$applicationId]['deleted_record_counts'][$fkApplicationIdTable] = 0;
            
            $fkApplicationIdQueryFrom = "FROM " . $fkApplicationIdTable; 
            $fkApplicationIdQueryFrom .= " WHERE application_id = " . $applicationId;
            $fkApplicationIdSelectQuery = "SELECT * " . $fkApplicationIdQueryFrom; 
            $fkApplicationIdDeleteQuery = "DELETE " . $fkApplicationIdQueryFrom;
            
            // Take care of the recommendform table.
            if ($fkApplicationIdTable == 'recommend') {
                
                $deleteCounts[$applicationId]['deleted_record_counts']['recommendforms'] = 0;
                
                $recommendRecords = $db_applyweb->handleSelectQuery($fkApplicationIdSelectQuery);
                foreach ($recommendRecords as $recommendRecord) {
                    $recommendId = $recommendRecord['id'];
                    $recommendformsQueryFrom = "FROM recommendforms WHERE recommend_id = " . $recommendId;
                    $recommendformsSelectQuery =  "SELECT * " . $recommendformsQueryFrom; 
                    $recommendformsDeleteQuery = "DELETE " . $recommendformsQueryFrom;
                    movePurgedRecords('recommendforms', $recommendformsSelectQuery);
                    if ($echoOnly) {
                        $recommendformsRecords = $db_applyweb->handleSelectQuery($recommendformsSelectQuery);
                        $recommendformsRecordCount = count($recommendformsRecords);
                        echo $recommendformsDeleteQuery . "<br/>\n";    
                    } else {
                        $recommendformsRecordCount = $db_applyweb->handleInsertQuery($recommendformsDeleteQuery); 
                    } 
                    if ($recommendformsRecordCount > 0) {
                        $deleteCounts[$applicationId]['deleted_record_counts']['recommendforms'] += $recommendformsRecordCount;
                    } else {
                        $deleteCounts[$applicationId]['deleted_record_counts']['recommendforms'] = 0;
                    }
                }
            }
            
            movePurgedRecords($fkApplicationIdTable, $fkApplicationIdSelectQuery);
            if ($echoOnly) {
                $fkApplicationIdTableRecords = $db_applyweb->handleSelectQuery($fkApplicationIdSelectQuery);
                $fkApplicationIdTableRecordCount = count($fkApplicationIdTableRecords);
                echo $fkApplicationIdDeleteQuery . "<br/>\n";    
            } else {
                $fkApplicationIdTableRecordCount = $db_applyweb->handleInsertQuery($fkApplicationIdDeleteQuery); 
            } 
            if ($fkApplicationIdTableRecordCount > 0) {
                $deleteCounts[$applicationId]['deleted_record_counts'][$fkApplicationIdTable] += $fkApplicationIdTableRecordCount;
            } else {
                if ($fkApplicationIdTable == 'recommend') {
                    // No recommend records were deleted, so no recommendform records were deleted.
                    $deleteCounts[$applicationId]['deleted_record_counts']['recommendforms'] = 0;
                }
                $deleteCounts[$applicationId]['deleted_record_counts'][$fkApplicationIdTable] = 0;
            }
        }
        
        /*
        * Delete records from tables that have an lu_users_usertypes foreign key.
        */
        if ($luUsersUsertypesId !== NULL) {
            
            $deleteCounts[$applicationId]['deleted_record_counts']['datafileinfo'] = 0;
            
            /*
            * Delete any datafileinfo records. 
            */
            $datafileinfoQueryFrom = "FROM datafileinfo WHERE user_id = " . $luUsersUsertypesId;
            $datafileinfoQueryFrom .= " AND userdata LIKE '" . $applicationId . "_%'";
            $datafileinfoSelectQuery = "SELECT * " . $datafileinfoQueryFrom;
            $datafileinfoDeleteQuery = "DELETE " . $datafileinfoQueryFrom;
            movePurgedRecords('datafileinfo', $datafileinfoSelectQuery);
            if ($echoOnly) {
                $datafileinfoRecords = $db_applyweb->handleSelectQuery($datafileinfoSelectQuery);
                $datafileinfoRecordCount = count($datafileinfoRecords);
                echo $datafileinfoDeleteQuery . "<br/>\n";    
            } else {
                $datafileinfoRecordCount = $db_applyweb->handleInsertQuery($datafileinfoDeleteQuery);    
            }            
            if ($datafileinfoRecordCount > 0) {
                $deleteCounts[$applicationId]['deleted_record_counts']['datafileinfo'] += $datafileinfoRecordCount;
            } else {
                $deleteCounts[$applicationId]['deleted_record_counts']['datafileinfo'] = 0;
            }

            /*
            *  Delete any usersinst records. 
            */
            // NOTE: This is now handled above with the application_id fk tables.
            /*
            $usersinstQueryFrom = "FROM usersinst WHERE user_id = " . $luUsersUsertypesId;
            $usersinstSelectQuery = "SELECT * " . $usersinstQueryFrom;
            $usersinstDeleteQuery = "DELETE " . $usersinstQueryFrom;
            movePurgedRecords('usersinst', $usersinstSelectQuery);
            if ($echoOnly) {
                $usersinstRecords = $db_applyweb->handleSelectQuery($usersinstSelectQuery);
                $usersinstRecordCount = count($usersinstRecords);
                echo $usersinstDeleteQuery . "<br/>\n";    
            } else {
                $usersinstRecordCount = $db_applyweb->handleInsertQuery($usersinstDeleteQuery);    
            }            
            if ($usersinstRecordCount > 0) {
                $deleteCounts[$applicationId]['deleted_record_counts']['usersinst'] += $usersinstRecordCount;
            } else {
                // Add here, because this key value was set above.
                $deleteCounts[$applicationId]['deleted_record_counts']['usersinst'] += 0;
            }
            */

            /*
            * Delete the user-related records (for students only).
            */
            $otherApplicationsQuery = "SELECT id FROM application 
                                        WHERE user_id = " . $luUsersUsertypesId;
            $otherApplicationRecords = $db_applyweb->handleSelectQuery($otherApplicationsQuery);
            if ( count($otherApplicationRecords) > 0 ) {
                // The user has other applications in the system, so don't delete the user records.
                foreach($otherApplicationRecords as $applicationRecord) {
                    if ( !in_array($applicationRecord['id'], $unsubmittedApplicationIds) ) {
                        $deleteUserRecords = FALSE;
                        continue;    
                    }
                }       
            }
            
            $deleteCounts[$applicationId]['deleted_record_counts']['users_info'] = 0;
            $deleteCounts[$applicationId]['deleted_record_counts']['lu_user_department'] = 0;
            $deleteCounts[$applicationId]['deleted_record_counts']['users'] = 0;
            $deleteCounts[$applicationId]['deleted_record_counts']['lu_users_usertypes'] = 0;
            $deleteCounts[$applicationId]['datafile_directory_moved'] = 0;

            if ($usertypeId == 5 && $deleteUserRecords == TRUE) {
                
                /*
                * Delete the users_info record. (Should only be one.)
                */
                $users_infoQueryFrom = "FROM users_info WHERE user_id = " . $luUsersUsertypesId;
                $users_infoSelectQuery = "SELECT * " . $users_infoQueryFrom;
                $users_infoDeleteQuery = "DELETE " . $users_infoQueryFrom;
                movePurgedRecords('users_info', $users_infoSelectQuery);
                if ($echoOnly) {
                    $users_infoRecords = $db_applyweb->handleSelectQuery($users_infoSelectQuery);
                    $users_infoRecordCount = count($users_infoRecords);
                    echo $users_infoDeleteQuery . "<br/>\n";    
                } else {
                    $users_infoRecordCount = $db_applyweb->handleInsertQuery($users_infoDeleteQuery);    
                }            
                if ($users_infoRecordCount > 0) {
                    $deleteCounts[$applicationId]['deleted_record_counts']['users_info'] += $users_infoRecordCount;
                } else {
                    $deleteCounts[$applicationId]['deleted_record_counts']['users_info'] = 0;
                }
                
                /* 
                * Delete the lu_user_department record. (Actually, there shouldn't be any of these.)
                */
                $lu_user_departmentQueryFrom = "FROM lu_user_department WHERE user_id = " . $luUsersUsertypesId;
                $lu_user_departmentSelectQuery = "SELECT * " . $lu_user_departmentQueryFrom;
                $lu_user_departmentDeleteQuery = "DELETE " . $lu_user_departmentQueryFrom;
                movePurgedRecords('lu_user_department', $lu_user_departmentSelectQuery);
                if ($echoOnly) {
                    $lu_user_departmentRecords = $db_applyweb->handleSelectQuery($lu_user_departmentSelectQuery);
                    $lu_user_departmentRecordCount = count($lu_user_departmentRecords);
                    echo $lu_user_departmentDeleteQuery . "<br/>\n";    
                } else {
                    $lu_user_departmentRecordCount = $db_applyweb->handleInsertQuery($lu_user_departmentDeleteQuery);    
                }            
                if ($lu_user_departmentRecordCount > 0) {
                    $deleteCounts[$applicationId]['deleted_record_counts']['lu_user_department'] += $lu_user_departmentRecordCount;
                } else {
                    $deleteCounts[$applicationId]['deleted_record_counts']['lu_user_department'] = 0;
                }
                
                /*
                * Delete the users record. (Should only be one.)
                */                
                if ($usersId !== NULL) {
                    
                    // Find out whether the user has any non-student roles assigned.
                    $userRoleQuery = "SELECT id FROM lu_users_usertypes 
                                        WHERE usertype_id != 5 
                                        AND user_id = " . $usersId;
                    $userRoleRecords = $db_applyweb->handleSelectQuery($userRoleQuery);
                    
                    // Delete the users record if student is the only role assigned.
                    if ( count($userRoleRecords) == 0 ) {
                        
                        $usersQueryFrom = "FROM users WHERE id = " . $usersId;
                        $usersSelectQuery = "SELECT * " . $usersQueryFrom;
                        $usersDeleteQuery = "DELETE " . $usersQueryFrom;
                        movePurgedRecords('users', $usersSelectQuery);
                        if ($echoOnly) {
                            $usersRecords = $db_applyweb->handleSelectQuery($usersSelectQuery);
                            $usersRecordCount = count($usersRecords);
                            echo $usersDeleteQuery . "<br/>\n";    
                        } else {
                            $usersRecordCount = $db_applyweb->handleInsertQuery($usersDeleteQuery);    
                        }            
                        if ($usersRecordCount > 0) {
                            $deleteCounts[$applicationId]['deleted_record_counts']['users'] += $usersRecordCount;
                        } else {
                            $deleteCounts[$applicationId]['deleted_record_counts']['users'] = 0;
                        }
                    }    
                }
                
                /*
                * Delete the lu_users_usertypes record.
                */
                $lu_users_usertypesQueryFrom = "FROM lu_users_usertypes WHERE id = " . $luUsersUsertypesId;
                $lu_users_usertypesSelectQuery = "SELECT * " . $lu_users_usertypesQueryFrom;
                $lu_users_usertypesDeleteQuery = "DELETE " . $lu_users_usertypesQueryFrom;
                movePurgedRecords('lu_users_usertypes', $lu_users_usertypesSelectQuery);
                if ($echoOnly) {
                    $lu_users_usertypesRecords = $db_applyweb->handleSelectQuery($lu_users_usertypesSelectQuery);
                    $lu_users_usertypesRecordCount = count($lu_users_usertypesRecords);
                    echo $lu_users_usertypesDeleteQuery . "<br/>\n";    
                } else {
                    $lu_users_usertypesRecordCount = $db_applyweb->handleInsertQuery($lu_users_usertypesDeleteQuery);    
                }            
                if ($lu_users_usertypesRecordCount > 0) {
                    $deleteCounts[$applicationId]['deleted_record_counts']['lu_users_usertypes'] += $lu_users_usertypesRecordCount;
                } else {
                    $deleteCounts[$applicationId]['deleted_record_counts']['lu_users_usertypes'] = 0;
                }

                /*
                * Move the user's GUID directory.
                */
                //DebugBreak();
                $guidMoved = moveDatafiles($guid);
                if ($guidMoved) {
                    $deleteCounts[$applicationId]['datafile_directory_moved'] = 1;    
                }
            
            } else {
                $deleteCounts[$applicationId]['deleted_record_counts']['users_info'] = 0;
                $deleteCounts[$applicationId]['deleted_record_counts']['lu_user_department'] = 0;
                $deleteCounts[$applicationId]['deleted_record_counts']['users'] = 0;
                $deleteCounts[$applicationId]['deleted_record_counts']['lu_users_usertypes'] = 0;    
            }
            
        }

        /*
        * Finally, delete the application record.
        */
        $deleteCounts[$applicationId]['deleted_record_counts']['application'] = 0;
        
        $applicationQueryFrom = "FROM application WHERE id = " . $applicationId;
        $applicationSelectQuery = "SELECT * " . $applicationQueryFrom;
        $applicationDeleteQuery = "DELETE " . $applicationQueryFrom;
        movePurgedRecords('application', $applicationSelectQuery);
        if ($echoOnly) {
            $applicationRecords = $db_applyweb->handleSelectQuery($applicationSelectQuery);
            $applicationRecordCount = count($applicationRecords);
            echo $applicationDeleteQuery . "<br/><br/>\n";    
        } else {
            $applicationRecordCount = $db_applyweb->handleInsertQuery($applicationDeleteQuery);    
        }            
        if ($applicationRecordCount > 0) {
            $deleteCounts['total_applications_deleted']++;
            $deleteCounts[$applicationId]['deleted_record_counts']['application'] += $applicationRecordCount;
        } else {
            $deleteCounts[$applicationId]['deleted_record_counts']['application'] = 0;
        }
    }
   
    return TRUE;
}


/*
* Function to copy purged records to purge db
*/
function movePurgedRecords($table, $selectQuery) {
    
    global $echoOnly;
    global $db;
    global $copyConnection;
    mysql_select_db($db, $copyConnection);
    
    $purgesDb = $db . 'Purges';
    
    $query = "INSERT INTO " . $purgesDb . "." . $table . " " . $selectQuery;
    
    if ($echoOnly) {
        echo $query . "<br/>\n";    
    } else {
        $result = mysql_query($query, $copyConnection);
    }
    return TRUE;    
}


function moveDatafiles($guid) {
    
    global $echoOnly;
    global $datafileroot;
    
    $oldFileRoot = realpath($datafileroot);
    $newFileRoot = $oldFileRoot . 'Purge';
    
    $oldFilepath = $oldFileRoot . '/' . $guid;
    $newFilepath = $newFileRoot . '/' . $guid;
    
    if ( file_exists($oldFilepath) ) {
        
        if ($echoOnly) {
        
            echo "mv " . $oldFilepath . " " . $newFilepath ."<br/>\n";    
        
        } else {
        
            if ( !file_exists($newFileRoot) ) {
                mkdir($newFileRoot, 0775);    
            }
            rename($oldFilepath, $newFilepath);    
        }
        
        return TRUE;
        
    } else {
        
        return FALSE;
    }
}
?>