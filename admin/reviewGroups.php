<?
error_reporting(E_ALL);
ini_set('display_errors','On');

// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

/*
* data, logic 
*/
$sql = "";
$err = "";
$revgroup = array();

if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{

				mysql_query("DELETE FROM revgroup WHERE id=".$itemId)
				or die(mysql_error());
			}//END IF
		}//END IF DELETE
	}
}

//GET DATA
$sql = "select revgroup.id,revgroup.name, department.name as dept
from revgroup
left outer join department on department.id = revgroup.department_id ";
if($_SESSION['A_usertypeid'] != 0)
{

	for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
	{

        if ($_SESSION['A_admin_depts'][$i] == NULL) {
            continue;
        }

		if($i == 0)
		{
			$sql .= "where ";
		}else
		{
            if ($i == 1 && $_SESSION['A_admin_depts'][0] == NULL) { 
                $sql .= "where ";    
            } else {
                $sql .= " or ";
            }
		}
		$sql .= " department.id = ".$_SESSION['A_admin_depts'][$i];
	}
}
$sql .= " order by name";

$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['dept']);
	array_push($revgroup, $arr);
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Review Groups';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<br />
<form id="form1" action="" method="post"><!-- InstanceEndEditable --> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Review Groups<!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->	<a href="reviewGroups_edit.php">Add Review Group</a><br />
	<br />
	<table width="500" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblHead">
        <td>Name</td>
        <td>Department</td>
        <td width="60" align="right">Delete</td>
      </tr>
     <? for($i = 0; $i < count($revgroup); $i++){?>
	<tr>
        <td><a href="reviewGroups_edit.php?id=<?=$revgroup[$i][0]?>"><?=$revgroup[$i][1]?></a></td>
        <td>
          <?=$revgroup[$i][2]?></td>
        <td width="60" align="right"><? showEditText("X", "button", "btnDelete_".$revgroup[$i][0], $_SESSION['A_allow_admin_edit']); ?></td>
      </tr>
	<? } ?>
    </table>



	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>