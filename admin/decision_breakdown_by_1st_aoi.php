<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applygrad</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$deptId = -1; 
$deptName = "";

$decisionVals = array(
array('A1', 'A1'),
array('A2', 'A2'),
array('B1', 'B1'),
array('B2', 'B2'),
array('S', 'S'),
array('R', 'R'),
);
$aStatus = 
array(array(0,"Reject"),array(1,"Waitlist"),array(2,"Admit"),array(3,"Reset")   );

if(isset($_GET["id"]))
{
	$deptId = intval($_GET["id"]);
}
if(isset($_POST["deptId"]))
{
	$deptId = intval($_POST["deptId"]);
}

$sql = "select name from department where id=".$deptId;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$deptName = $row['name'];
}	

$areas = array();

//GET AREAS
$sql = "select distinct interest.id,interest.name  from 
application
left outer join lu_application_programs on lu_application_programs.application_id = application.id
left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id	
left outer join interest on interest.id =  lu_application_interest.interest_id
left outer join programs on programs.id = lu_application_programs.program_id
left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
where lu_programs_departments.department_id = ".$deptId." and interest.name IS NOT NULL
order by interest.name	
";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr= array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($areas, $arr);
}

/////////////////////////////////
//FIRST GET ALL ROUND2 CANDIDATES
/////////////////////////////////
function getDecision($decision, $area)
{
	global $deptId;
	$ret = array();
	$sql = "select lu_users_usertypes.id,
	concat(users.lastname,', ', users.firstname) as name,
	countries.name as ctzn,
	gender as m_f,
	
	group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '<br>') as program,
	group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '<br>') as interest,

	
	GROUP_CONCAT(distinct 
	case lu_programs_departments.department_id when  ".$deptId. " THEN
	lu_application_programs.decision
	end
	SEPARATOR '<BR>') as decision,
	
	GROUP_CONCAT(distinct 
	case lu_programs_departments.department_id when  ".$deptId. " THEN
	lu_application_programs.admission_status
	end
	SEPARATOR '<BR>') as admission_status,
				
	case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote
	
	from lu_users_usertypes
	inner join users on users.id = lu_users_usertypes.user_id
	left outer join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join countries on countries.id = users_info.cit_country
	inner join application on application.user_id = lu_users_usertypes.id
	inner join lu_application_programs on lu_application_programs.application_id = application.id
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	
	left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
	left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	left outer join interest on interest.id = lu_application_interest.interest_id
	
	
	/*application groups - round 1*/
	left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
	and lu_application_groups.round = 1 
	left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
	 
	where application.submitted=1
	and lu_programs_departments.department_id=".$deptId. "  
	and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = ".$deptId. "   ) 
	and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
	and (lu_application_programs.round2 != 2) 
	and lu_application_programs.decision = '".$decision."' and (admission_status = 1 or admission_status = 2) 
	and lu_application_interest.interest_id=".$area." 
	and lu_application_interest.choice = 0
    group by  users.id     
	
	order BY users.lastname,users.firstname";
	$result = mysql_query($sql) or die(mysql_error() . "<br><br>".$sql);
	while($row = mysql_fetch_array( $result ))
	{
			$arr = array();
			array_push($arr, $row["id"]);
			array_push($arr, $row["name"]);
			array_push($arr, $row["decision"]);		
			array_push($arr, $row["admission_status"]);		
			array_push($ret, $arr);
			
	}
	return $ret;
}//end function
/////////////////////////////
//END GET ROUND 2 CANDITDATES
/////////////////////////////





?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" -->
  <form id="form1" name="form1" action="" method="post">
  <!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" --><?=$deptName?> Admission Reviews Round 2 Decisions<!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<a href="get_decision_counts.php" >Show Decisions by Area of Interest</a> <a href="decision_breakdown.php?id=<?=$deptId?>" >Show Decisions by Decision Group</a>

<table cellspacing=4 cellpadding=4 WIDTH=100%>
<tr>
<?
for($i = 0; $i < count($areas); $i++ )
{
	$thisCount = 0;
	$class = "tblItem";
	if($i % 2 == 0)
	{
		$class = "tblItemAlt";
	}
	if($i % 6 == 0)
	{
		echo "</td></tr></table><table WIDTH=100%><tr>";//MAKE A NEW TABLE SO IT'S NOT SO WIDE
	}
	echo "<td class=".$class." valign=top><strong>".$areas[$i][1]."</strong><br>";
	for($j = 0; $j < count($decisionVals); $j++)
	{
		echo "<strong>".$decisionVals[$j][1]."</strong>";
		$arr = getDecision($decisionVals[$j][1], $areas[$i][0]);
		echo " <em>(".count($arr).")</em><br>";
		for($k = 0; $k < count($arr); $k++)
		{
			echo "&nbsp;&nbsp;&nbsp;".$arr[$k][1]." <em>(".substr  ( $aStatus[ $arr[$k][3] ][1], 0  ,1 ).")</em><br>";
			$thisCount ++;
		}
	}	
	echo $thisCount . " applicants in " . $areas[$i][1];
	
	
	echo "</td>";
}?>
</tr>
</table>





	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
