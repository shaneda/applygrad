<?php
// Standard includes.
include_once "../inc/session_admin.php";
/* session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/

// Include file download handler.
include '../classes/class.FileDownload.php';

// Determine the name of the datafile being requested.
$file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

// Determine the guid of the datafile being requested.
$guid = filter_input(INPUT_GET, 'guid', FILTER_SANITIZE_STRING);

// Determine the requester's usertype.
$requesterUsertypeId = NULL;
if ( isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] >= 0) {
    $requesterUsertypeId = $_SESSION['A_usertypeid'];    
}

// Determine departmentId(s) under which the requester is logged in.
$requesterRoleDepartmentIds = array();
if ( isset($_SESSION['A_admin_depts']) && count($_SESSION['A_admin_depts']) > 0) {
    foreach ($_SESSION['A_admin_depts'] as $adminDepartmentId) {
        if ($adminDepartmentId !== NULL && $adminDepartmentId > 0 
            && !in_array($adminDepartmentId, $requesterRoleDepartmentIds) )
        {
            $requesterRoleDepartmentIds[] = $adminDepartmentId;    
        }
    }    
}

// Exit if file, guid, usertype and roleDepartmentIds cannot be determined.
if ( !( $file && $guid && $requesterUsertypeId !== NULL && 
    (count($requesterRoleDepartmentIds) > 0 || $requesterUsertypeId == 0) ) ) 
{
    exit;
}

// Instantiate a new file download with the requested datafile id.
try {
    $download = new FileDownload($file, $guid);    
} catch (Exception $e) {
    if ($e->getCode() == 404) {
        header("HTTP/1.0 404 Not Found");
        echo "File not found.";
        exit;
    }    
}

// Allow download based on requester's usertype and/or role department.
$doDownload = FALSE;
if ($requesterUsertypeId == 0) {                          
    
    // Allow SU to view any file.
    $doDownload = TRUE;        

} elseif (($requesterUsertypeId == 1 || $requesterUsertypeId == 2 || $requesterUsertypeId == 10)
    && applicationIsRecyclable($download->fileApplicationId)) {
    
    // Allow admins, chairs, and committee members to view files
    // from recyclable application, regardless of department.
    $doDownload = TRUE;    
    
} else {
    // Allow users from departments to which the applicant has applied.    
    $fileReqArray = explode('_', $file);
    $fileType = $fileReqArray[0];
    if (($requesterUsertypeId == 11 && ( $fileType == 'resume' || $fileType == 'statement'))
         ||  $requesterUsertypeId != 11) {
        // Get the application department ids.
        $fileApplicationId = $download->fileApplicationId;
        $fileDepartmentIdQuery = "SELECT lu_programs_departments.department_id
                                FROM lu_application_programs
                                INNER JOIN lu_programs_departments 
                                ON lu_application_programs.program_id = lu_programs_departments.program_id
                                WHERE lu_application_programs.application_id = " . $fileApplicationId;
        $fileDepartmentIdResult = mysql_query($fileDepartmentIdQuery);
        
        // Check against requester's roleDepartmentIds.
        while ($row = mysql_fetch_array($fileDepartmentIdResult)) {
            if ( in_array($row['department_id'], $requesterRoleDepartmentIds) ) {              
                $doDownload = TRUE;
                break;    
            }    
        }    
    }
}

// Download or return 403 error.
if ($doDownload) {
    
    echo $download->serve();
    
} else {
    
    header("HTTP/1.0 403 Unauthorized");
    echo "You are not permitted to view this file.";
    exit;    
}

function applicationIsRecyclable($applicationId)
{
    $query = "SELECT application.id AS application_id, 
        MIN(IFNULL(enable_recycling, 0)) AS enable_recycling,
        MAX(admission_status) AS admission_status
        FROM application 
        INNER JOIN period_application
            ON application.id = period_application.application_id
        INNER JOIN lu_application_programs 
            ON application.id = lu_application_programs.application_id
        INNER JOIN lu_programs_departments 
            ON lu_application_programs.program_id = lu_programs_departments.program_id
        LEFT OUTER JOIN department_enable_recycling
            ON lu_programs_departments.department_id = department_enable_recycling.department_id
            AND period_application.period_id = department_enable_recycling.period_id
        WHERE application.id = " . intval($applicationId) .
        " GROUP BY application_id 
        HAVING enable_recycling = 1
        AND (admission_status IS NULL OR admission_status = 0)";
        
     $result = mysql_query($query);
     
     if (mysql_numrows($result) > 0)
     {
        return TRUE;    
     }   
     
     return FALSE; 
}
?>