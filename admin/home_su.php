<?php
include_once "../inc/session_admin.php";

//GET DOMAINS
$domains = array();
$domainSql = "SELECT DISTINCT domain.id, domain.name
        FROM domain 
        ORDER BY domain.name";
$domainResult = mysql_query($domainSql);
while($row = mysql_fetch_array($domainResult))
{
    $domains[] = $row;
}

//GET DEPARTMENTS
$departments = array();
$departmentSql = "SELECT DISTINCT department.id, department.name
        FROM department
        ORDER BY department.name";
$departmentResult = mysql_query($departmentSql);
while($row = mysql_fetch_array($departmentResult))
{
    $departments[] = $row;
}

// Get the <head></head> and <body> template
$pageTitle = 'Admissions Home: SU';
$pageCssFiles = array();
$pageJavascriptFiles = array();
include '../inc/tpl.pageHeader.php';
?>

<div style="padding-top: 20px;">   
    <div class="title" style="float: left; font-size: 18px;">
    <?php
        echo $pageTitle;
    ?>
    </div>
</div>

<br/> <br/>
<div style="clear: both; margin-left: 10px;">
    <a href="home.php">All-In-One Home</a>
</div>

<div style="width: 40%; margin-left: 10px; float: left; clear: left;">
    <h3>Departments</h3>
    <ul>
    <?php
    foreach($departments as $department)
    {
        echo '<li><a href="home.php?suDepartmentId=' . $department['id'] . '">' . $department['name'] . '</a></li>';
    }
    ?>
    </ul>
</div>

<div style="width: 40%; float: left;">
    <h3>Domains</h3>
    <ul>
    <?php
    foreach($domains as $domain)
    {
        echo '<li><a href="home.php?suDomainId=' . $domain['id'] . '">' . $domain['name'] . '</a></li>';
    }
    ?>
    </ul>
</div>

<div style="clear: both;"></div>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>
