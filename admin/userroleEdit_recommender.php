<?
// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// Include functions.php exluding scriptaculous
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';

// PLB added scsUser, WebISO functions 6/30/09
include '../inc/sysusersEdit.inc.php';

// Include unit/period classes
define("CLASS_DIR", "../classes/");
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb_Table.php';
include '../inc/unitPeriodIncludes.inc.php';

$err = "";
$uid= -1;
$guid = -1;
$umasterid = -1;
$fname = "";
$lname = "";
$title = "";
$email = "";
$affiliation = "";
$phone = "";
$domain = 1;

$users = array();
$allStudents = array();


if(isset($_GET['id']))
{
	$uid= intval(htmlspecialchars($_GET['id']));
}
if(isset($_POST['id']))
{
	$uid= intval(htmlspecialchars($_POST['id']));
}
//DebugBreak();
if(isset($_POST))
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelete') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			if( $itemId > -1 )
			{

				$sql = "DELETE FROM recommend WHERE id=" . $itemId;
				mysql_query($sql) or die(mysql_error().$sql );
			}//END IF
		}//END IF DELETE
	}
}


//RETRIEVE USER INFORMATION
$sql = "SELECT
users.id,
users.title,
users.firstname,
users.lastname,
users.email,
users_info.address_perm_tel,
lu_users_usertypes.id as uid,
users_info.company,
users.guid
FROM lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
where lu_users_usertypes.id=".$uid;
//echo $sql;
$result = mysql_query($sql) or die(mysql_error().$sql);

while($row = mysql_fetch_array( $result ))
{
	$umasterid =  $row['id'];
	$title =  $row['title'];
	$fname =  $row['firstname'];
	$lname = $row['lastname'];
	$email = $row['email'];
	$phone = $row['address_perm_tel'];
	$affiliation = $row['company'];
	$guid = $row['guid'];
}

$sql = "select
application.id AS application_id,
period_application.period_id,
umbrella_name,
lu_programs_departments.department_id,
MIN(domain_id) AS domain_id,
lu_users_usertypes.id as luu_id,
users.firstname,
users.lastname,
users.id as umasterid,
degree.name as degreename,
fieldsofstudy.name as fieldname,
recommend.submitted,
recommend.id as recommendid,
rec_users.id AS rec_users_id,
rec_users_usertypes.id AS rec_luu_id,
rec_users.email AS rec_email
FROM
recommend
inner join application on application.id = recommend.application_id
INNER JOIN period_application ON application.id = period_application.application_id
INNER JOIN period_umbrella ON period_application.period_id = period_umbrella.period_id
inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
inner join users on users.id = lu_users_usertypes.user_id
inner join lu_application_programs on lu_application_programs.application_id = application.id
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
INNER JOIN lu_programs_departments ON programs.id = lu_programs_departments.program_id
INNER JOIN lu_domain_department
  ON lu_programs_departments.department_id = lu_domain_department.department_id
INNER JOIN lu_users_usertypes AS rec_users_usertypes
  ON rec_users_usertypes.id = recommend.rec_user_id
INNER JOIN users AS rec_users
  ON rec_users.id = rec_users_usertypes.user_id
where rec_user_id=".$uid. " and  lu_application_programs.choice =1
GROUP BY application.id";
$result = mysql_query($sql) or die(mysql_error().$sql);

while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['luu_id']);
	array_push($arr, $row['firstname']);
	array_push($arr, $row['lastname']);
	array_push($arr, $row['umasterid']);
	array_push($arr, $row['degreename']);
	array_push($arr, $row['fieldname']);
	array_push($arr, $row['submitted']);
	array_push($arr, $row['recommendid']);
    array_push($arr, $row['application_id']); // $users[8]
    array_push($arr, $row['period_id']); 
    array_push($arr, $row['department_id']);
    array_push($arr, $row['umbrella_name']);  
    array_push($arr, $row['rec_users_id']); // $users[12]
    array_push($arr, $row['rec_luu_id']);
    array_push($arr, $row['rec_email']);
    array_push($arr, $row['domain_id']);
	array_push($users, $arr);
}
if(isset($_POST['btnAdd']))
{
	$sql = "select
	lu_users_usertypes.id as id,
	users.firstname,
	users.lastname,
	users.id as umasterid,
	degree.name as degreename,
	fieldsofstudy.name as fieldname,
	application.id as appid
	from
	recommend
	inner join application on application.id = recommend.application_id
	inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
	inner join users on users.id = lu_users_usertypes.user_id
	inner join lu_application_programs on lu_application_programs.application_id = application.id
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	where rec_user_id !=".$uid. " and lu_application_programs.choice =1 group by application.id";
	$result = mysql_query($sql) or die(mysql_error().$sql);

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['appid']);
		array_push($arr, $row['lastname'].", ".$row['firstname']. " - " . $row['degreename'] . " in " . $row['fieldname']);
		array_push($allStudents, $arr);
	}
}
if(isset($_POST['btnAdd2']))
{
	$sql = "select id from recommend where rec_user_id=".$uid . " and application_id=".intval($_POST['lbName']) ;
	$result = mysql_query($sql)or die(mysql_error());
	$dup = false;
	while($row = mysql_fetch_array( $result ))
	{
		$dup = true;
	}
	if($dup == false)
	{
		$sql = "insert into recommend(rec_user_id, application_id)values(".$uid.", ".intval($_POST['lbName']).")";
		$result = mysql_query($sql)or die(mysql_error());
	}
}

/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Edit User Role - Recommender';

$pageCssFiles = array();

$pageJavascriptFiles = array(
    '../inc/scripts.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<form id="form1" action="" method="post">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title">Edit User Role - Recommender</span>
    <br />
    <br />

	<strong>Personal Information</strong> - <a href="sysusersEdit.php?id=<?=$umasterid?>">Edit</a><br />
	<!--
    <a href="../apply/index.php?uid=<?=$guid?>&u=<?=str_replace("+","%2b", $email)?>&domain=<?=$domain?>&r=1" target="_blank">Login as user</a> <br />
	-->
    <table width="700" border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td width="100" align="right"><strong>First Name:</strong></td>
        <td><?=$fname?></td>
        <td width="100" align="right"><strong>Last Name:</strong></td>
        <td><?=$lname?></td>
      </tr>
      <tr>
        <td width="100" align="right"><strong>Title:</strong></td>
        <td><?=$title?></td>
        <td width="100" align="right"><strong>Affiliation:</strong></td>
        <td><?=$affiliation?></td>
      </tr>
      <tr>
        <td width="100" align="right"><strong>Email:</strong></td>
        <td><?=$email?></td>
        <td width="100" align="right"><strong>Phone:</strong></td>
        <td><?=$phone?></td>
      </tr>
    </table>
	<br />
	<strong>Recommendations Assigned</strong>
	 <? 
     /*
     showEditText("Add Assignment", "button", "btnAdd", $_SESSION['A_allow_admin_edit']); ?>
	 <? if(isset($_POST['btnAdd'])){ ?>
	 <div>Name of student to add recommendation to: <?	showEditText("", "textbox", "lbName", $_SESSION['A_allow_admin_edit'],true, $allStudents,true,40); ?>
	 <? showEditText("Add", "button", "btnAdd2", $_SESSION['A_allow_admin_edit']); ?>
	 </div>
	 <? } 
     */
     ?>
	<br />
	<table width="700" border="0" cellspacing="1" cellpadding="3">
      <tr class="tblHead">
        <td>name</td>
        <td>degree/program</td>
        <td>submitted</td>
        <td></td>
        <td>delete</td>
      </tr>
	  <? for($i = 0; $i < count($users); $i++) { 
        
      ?>
      <tr onMouseOver="style.backgroundColor='white';" 
        onMouseOut="style.backgroundColor='transparent';">
        <td>
        <?php
        $applicationId = $users[$i][8];
        $departmentId = $users[$i][10];
        $periodId = $users[$i][9];
        $applicantName = $users[$i][2]. ", ".$users[$i][1];
        /*
        <a href="sysusersEdit.php?id=<?=$users[$i][3]?>"><?=$applicantName?></a>  
        */
        if ( $_SESSION['A_usertypeid'] == 0 
            || in_array($users[$i][10], $_SESSION['A_admin_depts']) ) 
        {
            $linkHref = 'administerApplications.php?unit=department&unitId=' . $departmentId;
            $linkHref .= '&period=' . $periodId;
            $linkHref .= '&applicationId=' . $applicationId; 
            $link = '<a href="' . $linkHref . '" target="_blank">';
            $link .= $applicantName . '</a><br/>';
            echo $link;
        } else {
            echo $applicantName;    
        }
        ?>
        
        </td>
        <td><?=$users[$i][4]?> in <?=$users[$i][5]?>, <?=$users[$i][11]?></td>
        <td><? 	if($users[$i][6] == 1){echo "Yes";}	?></td>
        <td>
        <?php
        if ( $_SESSION['A_usertypeid'] == 0 
            || in_array($users[$i][10], $_SESSION['A_admin_depts']) ) 
        {
            $recUsername = $users[$i][14];
            $recUsercode = $users[$i][12] . '-' . $users[$i][15];
            $recUsercode .= '-' . $users[$i][13] . '-' . $applicationId;
        ?>
           <a href="" 
           onClick="submitRecommenderForm('<?php echo $recUsername; ?>','<?php echo $recUsercode; ?>'); return false;">
           Login as Recommender</a>
        <?php
        }
        ?>
        
        </td>
        <td align="right">
        <? // showEditText("X", "button", "btnDelete_".$users[$i][7], $_SESSION['A_allow_admin_edit']);
        if( $_SESSION['A_usertypeid'] == 0 
            || in_array($departmentId, $_SESSION['A_admin_depts']) ) 
        { ?>
         <input name="btnDelete_<?=$users[$i][7]?>" type="submit" class="tblItem" id="btnDelete_<?=$users[$i][7]?>" 
            value="X" onClick="return confirmSubmit('<?=$applicantName?>')" />
            <? } ?>
        </td>
      </tr>
	  <? } ?>
    </table>
	<br />
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>

<form id="recommender_form" action="../apply/recommender.php" 
    method="post" target="_blank" >
    <input type="hidden" name="aliasLogin" value="1" />
    <input type="hidden" name="username" />
    <input type="hidden" name="usercode" />
</form>

<script LANGUAGE="JavaScript">
<!--
// Nannette Thacker http://www.shiningstar.net
function confirmSubmit(val)
{
var agree=confirm("Are you sure you wish to delete recommendation for "+val+"?");
if (agree)
    return true ;
else
    return false ;
}
// -->

function submitRecommenderForm($username, $usercode) {
    document.forms.recommender_form.username.value = $username;
    document.forms.recommender_form.usercode.value = $usercode;
    document.forms.recommender_form.submit();
    return false;
}

</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>