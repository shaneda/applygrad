<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? //include_once '../inc/functions.php'; ?>
<?
define('FPDF_FONTPATH','../inc/fpdf/font/');
require_once('../inc/fpdf/PDF_Label.php');
require_once("../inc/folder_label_data.php");

$deptId = 1;
//if(isset($_GET['id']))
//{
	//$deptId = intval($_GET['id']);
//}

// PLB uncommented deptId check 03/04/09
if(isset($_GET['id']))
{
    $deptId = intval($_GET['id']);
}

$periodId = NULL;
if ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period']; 
} 

?>

<?
        $labelConfig = new FolderLabelData();
        $labelConfig->setPeriod($periodId);
        $labelConfig->getDbData($periodId);
        $labelConfig->getDateTimesForLabels($periodId);
        
        if (!is_null($labelConfig->getLastTimeRan()) && !is_null($labelConfig->getNextStartTime())) {
            if (is_null($labelConfig->getLastTimeRan()) || ($labelConfig->getStartTime() > $labelConfig->getLastTimeRan())) {
		    $sql = "select 
		    distinct
		    firstname, lastname, email, date_format(dob, '%m/%d/%Y') as bd, toefl.testdate as toefldate, ieltsscore.testdate as ieltsdate 
		    from  lu_users_usertypes 
		    inner join users on users.id =  lu_users_usertypes.user_id
		    inner join users_info on users_info.user_id= lu_users_usertypes.id
            left outer join application on application.user_id = lu_users_usertypes.id
            left outer join toefl on toefl.application_id = application.id
            left outer join ieltsscore on ieltsscore.application_id = application.id";
            
            // PLB added period join 1/7/10
            if ($periodId) {
                $sql .= " INNER JOIN period_application ON application.id = period_application.application_id 
                        and period_application.period_id = " . $periodId;
            }        
            
            $sql .= " left outer join lu_application_programs on lu_application_programs.application_id = application.id
		    left outer join programs on programs.id = lu_application_programs.program_id
		    left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		    
		    /* where  lu_application_programs.admission_status = 2
		    and lu_programs_departments.department_id= */
            where ((paid = 1 and application.paymentdate >= '" . $labelConfig->getLastTimeRan() . "' and application.paymentdate <= '" .
            $labelConfig->getStartTime() ."') or ((waive = 1 and submitted = 1) and application.waivedate >= '" . $labelConfig->getLastTimeRan() . 
            "' and application.waivedate <= '" . $labelConfig->getStartTime() ."'))"; 
		    // for debugging
        //    $sql .= " group by  application.id
        //    order by application.submitted_date
        //    ";
            
            $sql .= " group by  application.id
		    order by lastname, firstname
		    ";
            // query  DebugBreak();
		    $result = mysql_query($sql) or die(mysql_error() . $sql);
		    $count = 0;
                    
          //          $ups_headers = "Attention^Address1^Address2^Address3^Address4^City^State_Prov^PostalCode^Country^Telephone\n";
          //          $filename = "UPSLABEL.txt";
          //          $handle = fopen($filename, 'w+');
          //          fwrite($handle, $ups_headers);

		    ob_clean();
            /*
                DAS - This is where to add the check for where to start the printing of the labels
                The print method is fill all the labels in the first column and then fill the labels in the second column
                Change teh first 1 to be the column and the second 1 to be the row of the first label to be printed
            */
            $pdf = new PDF_Label('5366', 'mm', $labelConfig->getStartLabelX(), $labelConfig->getStartLabelY());

    $pdf->Open();
    $pdf->AddPage();
    //DebugBreak();
		    while($row = mysql_fetch_array( $result )) 
		    {
			    $email = "";
                $dob = "";
                $langtest = "";
			    if($row['email'] != "")
			    {
				    $email = $row['email'];
			    }
                if($row['bd'] != "")
                {
                    $dob = $row['bd'];
                }
			    if($row['toefldate'] != "")
                {
                    $langtest = "TOEFL";
                }
                if($row['ieltsdate'] != "" && $row['ieltsdate'] != "0000-00-00")
                {
                    $langtest = "IELTS";
                }

	    //		if($count % 6 == 0)
	    //		{
	    //			echo "</td ></tr></table><table cellspacing=4 cellpadding=4><tr>";
	    //		}
	    //		?><?
	    //		echo $gender.ucwords(strtolower($row['firstname'])). " ". ucwords(strtolower($row['lastname']))."<br>";
	    //		echo $address;
	    //		?><?
	    //		$count++;
                            // Write shipping info for UPS
       /*                     $dataline = $gender.ucwords(strtolower($row['firstname'])).' '
                                      . ucwords(strtolower($row['lastname'])).'^'
                                      . ucwords(strtolower($row['address_cur_street1'])).'^'
                                      . ucwords(strtolower($row['address_cur_street2'])).'^'
                                      . ucwords(strtolower($row['address_cur_street3'])).'^'
                                      . ucwords(strtolower($row['address_cur_street4'])).'^'
                                      . $row['address_cur_city'].'^'
                                      . $row['state']. '^'
                                      . $row['address_cur_pcode']. '^'
                                      . ucwords(strtolower($row['country'])). '^'
                                      . $row['address_cur_tel']. "\n";

                            fwrite($handle, $dataline);
                   */         
                            // build name line
                            $entireAddress = array();
                            $nameLine = ucwords(strtolower($row['lastname'])).', '
                                      . ucwords(strtolower($row['firstname']));
                            array_push($entireAddress, $nameLine);
                            $secondLine = "";
                            $thirdLine = "";
                            
                            if ($dob != "") {
                                $thirdLine .= $dob . "      ";
                            }
                            if ($email != "") {
                                $thirdLine .= $email . "     ";
                            }
                            if ($langtest != "") {
                                $thirdLine .= $langtest;
                            }
                          //  array_push($entireAddress, "");
                            array_push($entireAddress, $secondLine);
                            array_push($entireAddress, $thirdLine);
                            
                           
                     /*       switch  ($numAdditionalLines) {
                                case 0:
                                    $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2]));
                                    break;
                                case 1:
                                    $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3]));
                                    break;
                                case 2:
                                    $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3], $entireAddress[4]));
                                    break;
                                case 3:
                                    $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3], $entireAddress[4], $entireAddress[5]));
                                    break;
                                case 4:
                                    $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s\n%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2], $entireAddress[3], $entireAddress[4], $entireAddress[5], $entireAddress[6]));
                                    break;
                            }
                            */
                            $pdf->Add_PDF_Folder_Label(sprintf("%s\n%s\n%s",$entireAddress[0], $entireAddress[1], $entireAddress[2]));
                                      
                  //          $pdf->Add_PDF_Label(sprintf("%s\n%s\n%s\n%s, %s, %s",$nameLine, 'Immeuble Titi', 'av. fragonard', '06000', 'NICE', 'FRANCE'));

		    }
              //      fclose($handle);
                    $pdf->Output();
                    $labelConfig->updateDbData($pdf);
            } //end of must run
        } else {
            echo "No new labels to print";
        }
       //         echo ("countx: " . $pdf->_COUNTX . "  county" . $pdf->_COUNTY);
?>

