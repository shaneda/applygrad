<?php 
include "../inc/config.php";
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminApplicantIndex.php";

if ( isset($_REQUEST['unit']) ) {
    $unit = $_REQUEST['unit'];
} else {
    //$unit = 'department';
    $unit = NULL;
}

if ( isset($_REQUEST['unitId']) ) {
    $unitId = $_REQUEST['unitId'];
} else {
    //$unitId = 1;
    $unitId = NULL;
}

if ( isset($_REQUEST['periodId']) ) {
    $periodId = $_REQUEST['periodId'];
} else {
    $periodId = NULL;
}

if ( isset($_REQUEST['q']) ) {
    $searchString = $_REQUEST['q'];
} else {
    $searchString = "";
}

// NOTE: $submitted is ultimately not passed to $userIndex->find()
if ( isset($_REQUEST['submitted']) ) {
    switch ($_REQUEST['submitted']) {        
        case 'unsubmitted':
            $submitted = 0;
        case 'submitted':
            $submitted = 1;
        default:
            $submitted = NULL;    
    }
    $submitted = $_REQUEST['submitted'];
} else {
    $submitted = NULL;
}
//DebugBreak();
$userIndex = new VW_AdminApplicantIndex(); 
$resultArray = $userIndex->find($unit, $unitId, $periodId, $searchString);

foreach ($resultArray as $row) {
    echo implode("|", $row) . "\n";
}
 
?>