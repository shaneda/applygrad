#!/usr/local/bin/php5
<?php
/*
* Includes needed to parse reports and update DB.
*/
include_once '/usr0/wwwsrv/htdocs/_dev/inc/commandlineConfig.inc.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/class.CcTransactionReport.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/class.DB_Applyweb.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/class.DB_Applyweb_Table.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_CcTransactionReport.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_CcTransaction.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_CcTransactionSummary.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_CcTransactionDetail.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_CcPaymentStatus.php';

/*
* Includes needed to update payments.
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_Payment.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/DB_Applyweb/Table/class.DB_PaymentItem.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/class.Payment.php';
include_once '/usr0/wwwsrv/htdocs/_dev/classes/class.PaymentManager.php';
*/

/*
* Get configuration settings.
*/
$config = parse_ini_file('parseOutload.ini');

/*
* Rsync the reports from relay to the local directory. 
*/
$relayStatus = rsyncFromRelay();
if ($relayStatus['error']) {
    echo "rsync from relay failed\n";
}

/*
* Get an array of rsynced report filepaths. 
*/
$reportsToParse = getReportsToParse();
if (count($reportsToParse) == 0) {
    echo "no reports to parse\n";
    exit;    
}

/*
* Process the non-stats reports. 
*/
if (count($reportsToParse[$config['store_main']]) > 0 || count($reportsToParse[$config['store_isree']]) > 0) {
    
    // Set up DB connection
    $db_host = $config['main_db_host'];
    $db = $config['main_db'];
    $db_username = $config['main_db_username'];
    $db_password = $config['main_db_password'];
    
    /*
    * Instantiate DB classes. 
    */
    $DB_CcTransactionReport = new DB_CcTransactionReport();
    $DB_CcTransaction = new DB_CcTransaction();
    $DB_CcTransactionSummary = new DB_CcTransactionSummary();
    $DB_CcTransactionDetail = new DB_CcTransactionDetail();
    $DB_CcPaymentStatus = new DB_CcPaymentStatus(); 
    
    foreach ($reportsToParse[$config['store_main']] as $reportFilePath) {
        processReport($reportFilePath);   
    }
    
    foreach ($reportsToParse[$config['store_isree']] as $reportFilePath) {
        processReport($reportFilePath);
    }
}

/*
* Process the stats reports. 
*/
if (count($reportsToParse[$config['store_stats']]) > 0) {

    // Set up DB connection
    $db_host = $config['stats_db_host'];
    $db = $config['stats_db'];
    $db_username = $config['stats_db_username'];
    $db_password = $config['stats_db_password'];
    
    /*
    * Re-instantiate DB classes. 
    */
    $DB_CcTransactionReport = new DB_CcTransactionReport();
    $DB_CcTransaction = new DB_CcTransaction();
    $DB_CcTransactionSummary = new DB_CcTransactionSummary();
    $DB_CcTransactionDetail = new DB_CcTransactionDetail();
    $DB_CcPaymentStatus = new DB_CcPaymentStatus();
    
    foreach ($reportsToParse[$config['store_stats']] as $reportFilePath) {
        processReport($reportFilePath);
    }
}

/*
* Move reports from the local directory to AFS. 
*/
$afsStatus = rsyncToAfs();


/*
* Remove reports from local directory. 
*/
if (!$afsStatus['error']) {
    //$localDeleteStatus = deleteLocalFile('*.csv');    
}


/*
* Remove reports from relay. 
*/
if (!$afsStatus['error']) {
    //$remoteDeleteStatus = deleteRemoteFile('*.csv');    
}


/*********************************************************
* Functions 
*/

function rsyncFromRelay() {
    
    global $config;
    
    $status = array('output' => array(), 'error' => '');
    
    $command = 'rsync -avz -e "ssh -i ' . $config['relay_user_key'] . '" ';
    $command .= $config['relay_user'] . ':' . $config['relay_file_dir'] . '*.csv ' . $config['local_file_dir'];
    
    //echo $command . "\n";
    exec($command, $status['output'], $status['error']); 
    
    return $status;
}

function rsyncToAfs() {
    
    global $config;
    
    $status = array('output' => array(), 'error' => '');
    
    $kinitCmd = '/usr/local/bin/kinit -k -t ';
    $kinitCmd .= $config['afs_user_keytab'] . ' ' . $config['afs_user'];
    //echo $kinitCmd . "\n";
    exec($kinitCmd, $status['output'], $status['error']);
    
    //print_r($status);
    //echo "\n";
    
    if ($status['error']) {
        return $status;
    }
    
    $mainStatus = rsyncStoreToAfs($config['store_main']);
    $isreeStatus = rsyncStoreToAfs($config['store_isree']);
    $statsStatus = rsyncStoreToAfs($config['store_stats']);
    
    $kdestroyCmd = '/usr/local/bin/kdestroy';
    //echo $kdestroyCmd . "\n";
    shell_exec($kdestroyCmd);
    
    if ($mainStatus['error'] || $isreeStatus['error'] || $statsStatus['error']) {
        $status['error'] = 1;    
    }
    
    $status['output'] = array_merge($mainStatus['output'], $isreeStatus['output'], $statsStatus['output']);
    
    return $status;
}

function rsyncStoreToAfs($store) {
    
    global $config;
    
    $status = array('output' => array(), 'error' => '');
    
    $command = 'rsync -tvz ' . $config['local_file_dir'] . $store . '_* ';
    $command .= $config['afs_file_dir'] . $store . '/';
    
    //echo $command . "\n";
    exec($command, $status['output'], $status['error']); 
   
    //print_r($status);
    //echo "\n";
   
    return $status;
}


function getReportsToParse() {
    
    global $config;
    
    $reportsToParse = array(
        $config['store_main'] => array(),
        $config['store_isree'] => array(),
        $config['store_stats'] => array()    
    );

    if ( $dirHandle = opendir($config['local_file_dir']) ) {

        while (false !== ($fileName = readdir($dirHandle))) {

            $pathInfo = pathinfo($fileName);            
            if ($pathInfo['extension'] != 'csv') 
            {
                continue;        
            }
            
            $filePath = $config['local_file_dir'] . $fileName;
            $fileNameArray = explode('_', $fileName);
            $store = $fileNameArray[0];
            $reportsToParse[$store][] = $filePath;
        }
        
        closedir($dirHandle);
    }    
    
    return $reportsToParse;    
}


function processReport($reportFilePath) {
    
    // Instantiate transaction report instance.
    $transactionReport = new CcTransactionReport($reportFilePath);
    
    // Get the report file attributes, contents
    $reportMetadata = $transactionReport->getMetadata();
    if (!$reportMetadata) {
        return FALSE;    
    }

    // Save the report metadata.
    $saveMetadata = saveReportMetadata($reportMetadata);
    if (!$saveMetadata) {
        return FALSE;
    }
    
    // Save the transaction data.
    $reportData = $transactionReport->getData();
    $saveDataStatus = saveReportData($reportData);
    
    // Update the affected payments.
    $updatePaymentIds = $saveDataStatus['updatePaymentIds'];
    $updateCcPaymentStatuses = updateCcPaymentStatuses($updatePaymentIds);
}


function saveReportMetadata($reportMetadata) {
    
    global $DB_CcTransactionReport;
    
    return $DB_CcTransactionReport->save($reportMetadata);  
}


function saveReportData($reportData) {

    // Track errors, payments that need to be updated.
    $status = array(
        'updatePaymentIds' => array(),
        'errors' => array()
    );
    
    // Get store number, report date; they apply to all transaction records.
    $storeNumber = $reportData['head']['store_number'];
    $reportDate = $reportData['head']['report_date'];

    foreach ($reportData['summary_records'] as $summaryRecord) {
        
        // Add report-wide values to summary record.
        $summaryRecord['store_number'] = $storeNumber;
        $summaryRecord['report_date'] = $reportDate;

        // Get application, payment ids from merchant ref number.
        $merchantRefNoArray = explode('_', $summaryRecord[6]);
        $applicationId = $merchantRefNoArray[0];
        $paymentId = $merchantRefNoArray[1];
        
        // Add application, payment ids to summary record.
        $summaryRecord['application_id'] = $applicationId;
        $summaryRecord['payment_id'] = $paymentId;
                                
        // Add re-formatted transaction date to summary record.
        $transactionDate = date('Y-m-d', strtotime($summaryRecord[2]));
        $summaryRecord['transaction_date'] = $transactionDate;
        
        // Get cc_id for easy reference below.
        $ccId = $summaryRecord[1];
        
        // Save transaction record;
        $saveTransaction = saveTransaction($summaryRecord);
        if (!$saveTransaction) {
            $status['errors'][] = 'Unable to save transaction ' . $ccId;     
        }
        
        // Save transaction summary record;
        $saveTransactionSummary = saveTransactionSummary($summaryRecord);
        if (!$saveTransactionSummary) {
            $status['errors'][] = 'Unable to save transaction summary ' . $ccId;     
        }
        
        // Add payment id to update array (don't worry about duplicates).
        $status['updatePaymentIds'][$applicationId][] = $paymentId;
        
        // Save transaction detail records.
        if (isset($reportData['detail_records'][$ccId]) && is_array($reportData['detail_records'][$ccId])) {
            $i = 0;
            foreach($reportData['detail_records'][$ccId] as $detailRecord) {
                $saveTransactionDetail = saveTransactionDetail($summaryRecord, $detailRecord);
                if (!$saveTransactionDetail) {
                    $status['errors'][] = 'Unable to save transaction detail ' . $ccId . '[' . $i . ']';     
                }
                $i++;    
            }
        }
    }
    
    return $status;  
}


function saveTransaction($transactionSummary) {

    global $DB_CcTransaction;
    
    $transactionRecord = array(
        'cc_id' => $transactionSummary[1],
        'application_id' => $transactionSummary['application_id'],
        'payment_id' => $transactionSummary['payment_id'],
        'store_number' => $transactionSummary['store_number'],
        'cardholder_name_last' => $transactionSummary[7],
        'cardholder_name_first' => $transactionSummary[8]
    );

    return $DB_CcTransaction->save($transactionRecord);
}


function saveTransactionSummary($reportSummaryRecord) {

    global $DB_CcTransactionSummary;

    $transactionSummary = array(
        'cc_id' => $reportSummaryRecord[1],
        'date' => $reportSummaryRecord['transaction_date'],
        'time' => $reportSummaryRecord[3],
        'report_date' => $reportSummaryRecord['report_date'],
        'payment_id' => $reportSummaryRecord['payment_id'],
        'auth_code' => $reportSummaryRecord[21],
        'auth_message' => $reportSummaryRecord[22],
        'settle_code' => $reportSummaryRecord[23],
        'settle_message' => $reportSummaryRecord[24],
        'auth_amount' => $reportSummaryRecord[25],
        'settle_amount' => $reportSummaryRecord[26],
        'credit_amount' => $reportSummaryRecord[27]
    );
    
    return $DB_CcTransactionSummary->save($transactionSummary);
}


function saveTransactionDetail($reportSummaryRecord, $reportDetailRecord) {
    
    global $DB_CcTransactionDetail;
    
    $transactionDetail = array(
        'cc_id' => $reportDetailRecord[1],
        'date' => $reportSummaryRecord['transaction_date'],
        'time' => $reportSummaryRecord[3],
        'report_date' => $reportSummaryRecord['report_date'],
        'payment_id' => $reportSummaryRecord['payment_id'],
        'transaction_type' => $reportDetailRecord[2],
        'item_name' => $reportDetailRecord[3],
        'item_qty' => $reportDetailRecord[5],
        'item_price_each' => $reportDetailRecord[6],
        'item_gl_string' => $reportDetailRecord[7]
    );
    
    return $DB_CcTransactionDetail->save($transactionDetail);
}


function updateCcPaymentStatuses($applicationPaymentIds) {
    
    $updateStatus = array();
    
    foreach ($applicationPaymentIds as $applicationId => $paymentIdArray) {

        foreach (array_unique($paymentIdArray) as $paymentId) {
            
            $updateStatus[$paymentId] = updateCcPaymentStatus($paymentId);
        }
    }
    
    return $updateStatus;
}


function updateCcPaymentStatus($paymentId) {

    global $DB_CcTransactionSummary;
    global $DB_CcPaymentStatus;
    
    $paymentStatus = array(
                'payment_id' => $paymentId,
                'auth_total' => 0,
                'settle_total' => 0,
                'credit_total' => 0
            );
            
    $transactionSummaryRecords = $DB_CcTransactionSummary->find(NULL, $paymentId);
    foreach ($transactionSummaryRecords as $transactionSummaryRecord) {
        
        if ($transactionSummaryRecord['auth_amount'] > 0) {
            $paymentStatus['auth_total'] += $transactionSummaryRecord['auth_amount'];     
        }
        
        if ($transactionSummaryRecord['settle_amount'] > 0) {
            $paymentStatus['settle_total'] += $transactionSummaryRecord['settle_amount'];     
        }            

        if ($transactionSummaryRecord['credit_amount'] > 0) {
            $paymentStatus['credit_total'] += $transactionSummaryRecord['credit_amount'];     
        }            
    }
    
    return $DB_CcPaymentStatus->save($paymentStatus);
}


function deleteLocalFile($fileName) {

    global $config;
    
    $status = array('output' => array(), 'error' => '');

    $command = 'rm -f ' . $config['local_file_dir'] . $fileName;    

    exec($command, $status['output'], $status['error']); 
    
    return $status;    
} 


function deleteRemoteFile($fileName) {
    
    global $config;
    
    $status = array('output' => array(), 'error' => '');

    $command = 'ssh -i ' . $config['relay_user_key'] . ' ' . $config['relay_user'];
    $command .= ' rm -f ' . $config['relay_file_dir'] . $fileName;    

    exec($command, $status['output'], $status['error']); 
    
    return $status;
}
?>