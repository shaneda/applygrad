<?php
/*
* This script is intended to be called only by the 
* CMU credit card processing system.
* 
* The script takes only one parameter, merchant_ref_no.  
* The value of merchant_ref_no should be in the form of 
* applicationId_paymentId, e.g., 1234_5678.  
* If merchant_ref_no validates as two integers > 0, 
* the corresponding DB payment record is updated.
*/

include '../inc/config.php';
include '../inc/db_connect.php';

$validApplicationId = FALSE;
$validPaymentId = FALSE;

if ( isset($_GET['merchant_ref_no']) ) {
    
    $referenceNumberArray = explode('_', $_GET['merchant_ref_no']);
    
    if ( count($referenceNumberArray == 2) ) {
        
        $validApplicationId = filter_var($referenceNumberArray[0], FILTER_VALIDATE_INT);
        $validPaymentId = filter_var($referenceNumberArray[1], FILTER_VALIDATE_INT);
    }

} else {

    echo 'Missing input';
    exit;     
    
}

if ($validApplicationId && $validPaymentId) {

    /*
    $query = "UPDATE payment set cc_auth_notification = 1, cc_auth_notification_date = NOW() 
                WHERE payment_id = " . $validPaymentId;
    */
    $query = "INSERT INTO cc_auth_notification VALUES(" . $validPaymentId . ", NOW())";
    mysql_query($query) or die();

} else { 

    echo 'Invalid input';
    exit;
    
}
?>