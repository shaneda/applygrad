<?php

function getDecisionRecords($departmentId, $periodId, $admittedOnly = FALSE) {

    $decisionRecords = array(
        'A1' => array(),
        'A2' => array(),
        'B1' => array(),
        'B2' => array(),
        'S' => array(),
        'R' => array()
    );

    $query = "SELECT application.id AS application_id,
        lu_users_usertypes.id AS luu_id,
        CONCAT(users.lastname,', ', users.firstname) AS name,
        lu_application_programs.decision,
        IFNULL(lu_application_programs.admission_status, 3) AS admission_status,
        decision_rank_member.rank
        FROM application
        INNER JOIN lu_users_usertypes
          ON application.user_id = lu_users_usertypes.id
        INNER JOIN users
          ON users.id = lu_users_usertypes.user_id
        INNER JOIN period_application
          ON application.id = period_application.application_id
        INNER JOIN lu_application_programs
          ON lu_application_programs.application_id = application.id
        INNER JOIN lu_programs_departments
          ON lu_programs_departments.program_id = lu_application_programs.program_id
        LEFT OUTER JOIN decision_rank_member
          ON lu_programs_departments.department_id = decision_rank_member.department_id
          AND application.id = decision_rank_member.application_id
        WHERE application.submitted = 1
        AND lu_programs_departments.department_id = " . $departmentId . "
        AND (lu_application_programs.round2 = '0' OR lu_application_programs.round2 = '1')
        AND lu_application_programs.decision IN ('A1', 'A2', 'B1', 'B2', 'S', 'R')
        AND period_application.period_id = " . $periodId;
        
    if ($admittedOnly) {
        $query .= " AND (lu_application_programs.admission_status = 1
            OR lu_application_programs.admission_status = 2)";
    }
        
    $query .= " ORDER BY decision, rank, users.lastname,users.firstname";
    
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result)) {
        switch ($row['decision']) {
            case 'A1':
                $decisionRecords['A1'][] = $row;
                break;
            case 'A2':
                $decisionRecords['A2'][] = $row;
                break;
            case 'B1':
                $decisionRecords['B1'][] = $row;
                break;
            case 'B2':
                $decisionRecords['B2'][] = $row;
                break;
            case 'S':
                $decisionRecords['S'][] = $row;
                break;
            case 'R':
                $decisionRecords['R'][] = $row;
                break;
            default:
        } 
    }
    
    return $decisionRecords;
}

function saveDecisionRank($departmentId, $periodId, $decision, $rankings) {
    
    $query = "INSERT INTO decision_rank 
            (department_id, period_id, decision, lu_users_usertypes_id, ranking)
            VALUES (" . intval($departmentId) . ","
            . intval($periodId) . ", 
            '" . mysql_real_escape_string($decision) . "',
            '" . intval($_SESSION['A_userid']) . "',
            '" . mysql_real_escape_string(implode(',', $rankings)) . "')";
    mysql_query($query);

    return TRUE;    
}

function saveDecisionRankMembers($departmentId, $periodId, $decision, $rankings) {

    $rankingCount = count($rankings);
    for ($i = 0; $i < $rankingCount; $i++) {
        $query = "INSERT INTO decision_rank_member VALUES
            (" . intval($departmentId) . ","
            . intval($periodId) . ", 
            '" . mysql_real_escape_string($decision) . "',"
            . intval($rankings[$i]) . ","
            . ($i + 1) . 
            ") ON DUPLICATE KEY UPDATE rank = " . ($i + 1);
        mysql_query($query);
    } 

    return TRUE;
}


function getAoiDecisionRecords($departmentId, $periodId, $admittedOnly = FALSE) {

    $aoiDecisionRecords = array();
    
    $query = "SELECT application.id AS application_id,
        lu_users_usertypes.id AS luu_id,
        CONCAT(users.lastname,', ', users.firstname) AS name,
        interest.id AS interest_id,
        interest.name AS interest,
        lu_application_programs.decision,
        IFNULL(lu_application_programs.admission_status, 3) AS admission_status,
        decision_aoi_rank_member.rank
        FROM application
        INNER JOIN lu_users_usertypes
          ON application.user_id = lu_users_usertypes.id
        INNER JOIN users
          ON users.id = lu_users_usertypes.user_id
        INNER JOIN period_application
          ON application.id = period_application.application_id
        INNER JOIN lu_application_programs
          ON lu_application_programs.application_id = application.id
        INNER JOIN lu_programs_departments
          ON lu_programs_departments.program_id = lu_application_programs.program_id
        INNER JOIN lu_application_interest
          ON lu_application_programs.id = lu_application_interest.app_program_id
        INNER JOIN interest
          ON lu_application_interest.interest_id = interest.id
        LEFT OUTER JOIN decision_aoi_rank_member
          ON lu_programs_departments.department_id = decision_aoi_rank_member.department_id
          AND lu_application_interest.interest_id = decision_aoi_rank_member.interest_id
          AND application.id = decision_aoi_rank_member.application_id
        WHERE application.submitted = 1
        AND lu_programs_departments.department_id = " . $departmentId . "
        AND (lu_application_programs.round2 = '0' OR lu_application_programs.round2 = '1')
        AND lu_application_programs.decision IN ('A1', 'A2', 'B1', 'B2', 'S', 'R')
        AND period_application.period_id = " . $periodId;
        
    if ($admittedOnly) {
        $query .= " AND (lu_application_programs.admission_status = 1
            OR lu_application_programs.admission_status = 2)";
    }
        
    $query .= " ORDER BY interest, decision, rank, users.lastname,users.firstname";
    
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result)) {
        
        $interestId = $row['interest_id'];
        $interest = $row['interest'];
        $decision = $row['decision'];
        
        $aoiDecisionRecords[$interestId]['interest'] = $interest;
        $aoiDecisionRecords[$interestId]['records'][$decision][] = $row;
        
        if (isset($aoiDecisionRecords[$interestId]['record_count'])) {
            $aoiDecisionRecords[$interestId]['record_count']++;    
        } else {
            $aoiDecisionRecords[$interestId]['record_count'] = 1;
        }
    }
    
    return $aoiDecisionRecords;
}

function saveDecisionAoiRank($departmentId, $periodId, $interestId, $decision, $rankings) {
    
    $query = "INSERT INTO decision_aoi_rank 
            (department_id, period_id, interest_id, decision, lu_users_usertypes_id, ranking)
            VALUES (" . intval($departmentId) . ","
            . intval($periodId) . ","
            . intval($interestId) . ", 
            '" . mysql_real_escape_string($decision) . "',
            '" . intval($_SESSION['A_userid']) . "',
            '" . mysql_real_escape_string(implode(',', $rankings)) . "')";
    mysql_query($query);

    return TRUE;    
}

function saveDecisionAoiRankMembers($departmentId, $periodId, $interestId, $decision, $rankings) {

    $rankingCount = count($rankings);
    for ($i = 0; $i < $rankingCount; $i++) {
        $query = "INSERT INTO decision_aoi_rank_member VALUES
            (" . intval($departmentId) . ","
            . intval($periodId) . ","
            . intval($interestId) . ", 
            '" . mysql_real_escape_string($decision) . "',"
            . intval($rankings[$i]) . ","
            . ($i + 1) . 
            ") ON DUPLICATE KEY UPDATE rank = " . ($i + 1);
        mysql_query($query);
    } 

    return TRUE;
}

?>