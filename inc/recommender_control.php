<?
Class Recommender
{

function Recommender()
{

}//END INIT FUNCTION

function displayTable($num, $recInfo)
{
$institutes = array();
$buckleys = array(array(1, 'I waive'), array(0,'I do not waive'));

$id = "";
$fName = "";
$lName = "";
$title = "";
$email = "";
$tel = "";
$sendmail = "";
$buckley = "";
$err = "";

if(is_array($recInfo))
{
	if(count($recInfo)> 0)
	{
		$id = $recInfo[0];
		$fName = $recInfo[6];
		$lName = $recInfo[7];
		$title = $recInfo[5];
		$email = $recInfo[8];
		$tel = $recInfo[9];
		$buckley = $recInfo[4];
	}
}

// INSTITUTES
$sql = "SELECT * FROM institutes order by name";
$result = mysql_query($sql) or die(mysql_error());


while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, trim($row['name']));

	array_push($institutes, $arr);
}



?>
<span class="subtitle">Recommender <?=$num;?></span>
<input name="txtRecord_<?=$num;?>" type="hidden" id="txtRecord_" value="<?=$id;?>" />
<table border=0 cellpadding=2 cellspacing=2 class="tblItem">
<tr>
<td><p><font color="#790000"><strong>*</strong></font> Recommender's First Name:<br>
  <? showEditText($fName, "textbox", "txtFname_".$num, $_SESSION['allow_edit'],true); ?>
</td>
<td><p><font color="#790000"><strong>*</strong></font> Recommender's Last Name:<br />
    <? showEditText($lName, "textbox", "txtLname_".$num, $_SESSION['allow_edit'],true); ?>
</td>
</tr>
<tr>
<td><p><font color="#790000"><strong>*</strong></font> Title:<br />
    <? showEditText($title, "textbox", "txtTitle_".$num, $_SESSION['allow_edit'],true); ?>
</td>
<td><p><font color="#790000"><strong>*</strong></font> Affiliation:<br />
    <? 
					$str = "";
					if(count($recInfo) > 0)
					{
						if (array_key_exists($recInfo[1], $institutes)) {
							$str = $institutes[$recInfo[1]][1];
						}
					}
					showEditText($str, "textbox", "lbUni_".$num, $_SESSION['allow_edit'],false, $institutes); ?>
</td>
</tr>
<tr>
<td valign=top><p><font color="#790000"><strong>*</strong></font> Email:<br />
    <? showEditText($email, "textbox", "txtEmail_".$num, $_SESSION['allow_edit'],true); ?>
</td>
<td valign=top><font color="#790000"><strong>*</strong></font> Phone:<br />
  <? showEditText($tel, "textbox", "txtTel_".$num, $_SESSION['allow_edit'],true); ?></td>
</tr>
<tr>
<td colspan=2><p><font color="#790000"><strong>*</strong></font> <b>Family Education Rights and Privacy Act(FERPA) (Buckley Amendment):</b><br>
Under the provisions of this act you have the right, if you enroll at Carnegie Mellon University, to review your educational records. The act further provides that you may waive your right to see recommendations for admission. Please indicate in the box below whether you wish to waive or not waive that right.<br>
<? showEditText($buckley, "listbox", "lbBuckley_".$num, $_SESSION['allow_edit'],true, $buckleys); ?>
</td>
</tr>
<tr>
<td colspan=2><p><b>
 Send email request to the recommender when SAVED.</b> 
    <? showEditText($sendmail, "checkbox", "chkSendmail_".$num, $_SESSION['allow_edit'],false); ?>
</td>
</tr>
</table>
<hr size="1" noshade="noshade" color="#790000" />

<?}//END DISPLAY TABLE

}//END CLASS
?>