<?php
/*
* "Template" functions called by administerApplicationsSingle.php to generate
* the html for the dynamic content div in administerApplications.php
*/

function displayTestScores($greGeneralRecords, $greSubjectRecords, $toeflRecords) {

    echo <<<EOB
    <table cellpadding="12px" border="0">
        <tr valign="top">    
EOB;
        echo '<td>';
        displayGREGeneralScore($greGeneralRecords[0]);
        echo '</td>';
         
        foreach ($greSubjectRecords as $greSubjectRecord) {
            if ( $greSubjectRecord['score'] != NULL ) {
                echo '<td style="border-left: 1px dotted #CCCCCC;">';
                displayGreSubjectScore($greSubjectRecord);
                echo '</td>';
            }
        }

        foreach ($toeflRecords as $toeflRecord) {
            if ($toeflRecord['testdate'] != NULL ) { 
                echo '<td style="border-left: 1px dotted #CCCCCC;">'; 
                displayTOEFLScore($toeflRecord);
                echo '</td>';
            }
            
        }
                
    echo <<<EOB
        </tr>
    </table>   
EOB;
}


function displayGREGeneralScore($greRecord) {

    $applicationId = $greRecord['application_id'];
    
    $greScoreId = $greRecord['id'];
    $greDate = date("m/Y", strtotime($greRecord['testdate']));
    
    $greVerbalScore = $greRecord['verbalscore'];
    $greVerbalPercentile = $greRecord['verbalpercentile'];

    $greQuantitativeScore = $greRecord['quantitativescore'];
    $greQuantitativePercentile = $greRecord['quantitativepercentile'];         

    $greAnalyticalScore = $greRecord['analyticalscore'];
    $greAnalyticalPercentile = $greRecord['analyticalpercentile'];

    $greWritingScore = $greRecord['analyticalwritingscore'];
    $greWritingPercentile = $greRecord['analyticalwritingpercentile']; 

    $receivedChecked = '';
    $receivedMessage = '';
    $receivedClass = 'confirm';
    if ($greRecord['scorereceived'] == 1) {
        $receivedChecked = 'checked';
        $receivedMessage .= ' rcd';
        $receivedClass = 'confirmComplete';
    }

    echo <<<EOB

    <form class="greScore" id="greScore_{$applicationId}">
    <table cellpadding="2px" cellspacing="2px" border="0">
    <tr>
        <td colspan="3">GRE General&nbsp;&nbsp;
        <input type="checkbox" class="greReceived" 
            id="greReceived_{$applicationId}_{$greScoreId}" {$receivedChecked} /> Received
        </td>
    </tr>
    <tr>
        <td colspan="3">Test Date: {$greDate}</td>
    </tr>
    <tr>
        <td></td>
        <td>Score</td>
        <td>Pctile</td>
    </tr>
    <tr>
        <td>Verbal</td>
        <td>
        <input type="text" size="3" class="greVerbalScore" 
            id="greVerbalScore_{$applicationId}_{$greScoreId}" value="{$greVerbalScore}" />
        </td>
        <td>
        <input type="text" size="3" class="greVerbalPercentile" 
            id="greVerbalPercentile_{$applicationId}_{$greScoreId}" value="{$greVerbalPercentile}" />
        </td>
    </tr>
    <tr>
        <td>Quantitative</td>
        <td>
        <input type="text" size="3" class="greQuantitativeScore" 
            id="greQuantitativeScore_{$applicationId}_{$greScoreId}" value="{$greQuantitativeScore}">
        </td>
        <td>
        <input type="text" size="3" class="greQuantitativePercentile" 
            id="greQuantitativePercentile_{$applicationId}_{$greScoreId}" value="{$greQuantitativePercentile}">
        </td>
    </tr>
    <tr>
        <td>Analytical</td>
        <td>
        <input type="text" size="3" class="gre_general_analytical_score" 
            id="greAnalyticalScore_{$applicationId}_{$greScoreId}" value="{$greAnalyticalScore}">
        </td>
        <td>
        <input type="text" size="3" class="gre_general_analytical_percentile" 
            id="greAnalyticalPercentile_{$applicationId}_{$greScoreId}" value="{$greAnalyticalPercentile}">
        </td>
    </tr>
    <tr>
        <td>Writing</td>
        <td>
        <input type="text" size="3" class="gre_general_writing_score" 
            id="greWritingScore_{$applicationId}_{$greScoreId}" value="{$greWritingScore}">
        </td>
        <td>
        <input type="text" size="3" class="gre_general_writing_percentile" 
            id="greWritingPercentile_{$applicationId}_{$greScoreId}" value="{$greWritingPercentile}">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <input type="submit" class="submitSmall updateGreScore" 
            id="updateGreScore_{$applicationId}_{$greScoreId}" value="Update GRE Score"/>
        </td>
    </tr>
    </table> 
    </form>

    <script>
        $('#greReceivedMessage_{$applicationId}').html('{$receivedMessage}');
        $('#greReceivedMessage_{$applicationId}').prev("span").attr({class:'{$receivedClass}'});    
    </script>

EOB;
}


function displayGreSubjectScore($greSubjectRecord) {
    
    $applicationId = $greSubjectRecord['application_id'];
    $greSubjectScoreId = $greSubjectRecord['id'];
    
    $greSubjectDate = date("m/Y", strtotime($greSubjectRecord['testdate']));
    $greSubjectName =  $greSubjectRecord['name'];
    $greSubjectScore = $greSubjectRecord['score'];
    $greSubjectPercentile = $greSubjectRecord['percentile'];

    $update_gre_subject_score_submit_id = $greSubjectRecord['id'] . "_update_gre_subject_score_submit";
    
    $receivedChecked = '';
    $receivedMessage = '';
    $receivedClass = 'confirm';
    if ($greSubjectRecord['scorereceived'] == 1) {
        $receivedChecked = 'checked';
        $receivedMessage .= ' rcd';
        $receivedClass = 'confirmComplete';
    }

    echo <<<EOB

    <form class="greSubjectScore" id="greSubjectScore_{$applicationId}"> 
    <table cellpadding="2px" cellspacing="2px" border="0">
        <tr>
            <td colspan="2">GRE Subject&nbsp;&nbsp;
            <input type="checkbox" class="greSubjectReceived" 
                id="greSubjectReceived_{$applicationId}_{$greSubjectScoreId}" {$receivedChecked} /> Received
            </td>
        </tr>
        <tr>
            <td colspan="3">Test Date: {$greSubjectDate}</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>Subject</td>
            <td>
            <input type="text" size="15" class="greSubjectName" 
                id="greSubjectName_{$applicationId}_{$greSubjectScoreId}" value="{$greSubjectName}">
            </td>
        </tr>
        <tr>
            <td>Score</td>
            <td>
            <input type="text" size="3" class="greSubjectScore" 
                id="greSubjectScore_{$applicationId}_{$greSubjectScoreId}" value="{$greSubjectScore}">
            </td>
        </tr>
        <tr>
            <td>Pctile</td>
            <td>
            <input type="text" size="3" class="greSubjectPercentile" 
                id="greSubjectPercentile_{$applicationId}_{$greSubjectScoreId}" value="{$greSubjectPercentile}">
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left">
            <input type="submit" class="submitSmall updateGreSubjectScore" 
                id="updateGreSubjectScore_{$applicationId}_{$greSubjectScoreId}" value="Update Subject Score"/>
            </td>
        </tr>
    </table> 
    </form> 

    <script>
        $('#greSubjectReceivedMessage_{$applicationId}').html('{$receivedMessage}');
        $('#greSubjectReceivedMessage_{$applicationId}').prev("span").attr({class:'{$receivedClass}'});    
    </script>
   
EOB;
}


function displayTOEFLScore($toeflRecord) {

    //$toeflRecord = $toeflRecords[0];
    
    // PLB added type 01/16/09
    $toeflType = $toeflRecord['type']; 

    $toefl_score_form_id = $toeflRecord['application_id'] . "_toeflscore";
    
    $toefl_received_id = $toeflRecord['id'] . "_toefl_received";
    
    $toefl_testdate = date("m/Y", strtotime($toeflRecord['testdate']));
    
    $toefl_section1_score_id = $toeflRecord['id'] . "_toefl_section1_score";
    $toefl_section1_score = $toeflRecord['section1'];
    
    $toefl_section2_score_id = $toeflRecord['id'] . "_toefl_section2_score";
    $toefl_section2_score = $toeflRecord['section2'];     

    $toefl_section3_score_id = $toeflRecord['id'] . "_toefl_section3_score";
    $toefl_section3_score = $toeflRecord['section3'];

    $toefl_essay_score_id = $toeflRecord['id'] . "_toefl_essay_score";
    $toefl_essay_score = $toeflRecord['essay'];
    
    $toefl_total_score_id = $toeflRecord['id'] . "_toefl_total_score";
    $toefl_total_score = $toeflRecord['total'];

    $update_toefl_score_submit_id = $toeflRecord['id'] . "_update_toefl_score_submit";
    
    // determine whether to have the "score received" box checked
    $received_checked = "";
    if ($toeflRecord['scorereceived'] == 1) {
        $received_checked = "checked";
    }

    // PLB added section label array for different types 01/16/09
    $section_labels = array(
                        'IBT' => array('section1' => 'Speaking', 'section2' => 'Listening', 'section3' => 'Reading', 'essay' => 'Writing'),
                        'PBT' => array('section1' => 'Section 1', 'section2' => 'Section 2', 'section3' => 'Section 3', 'essay' => 'TWE'),
                        'CBT' => array('section1' => 'Structure/Writing', 'section2' => 'Listening', 'section3' => 'Reading', 'essay' => 'Essay')
                        )
    
?>

<form class="toefl_score" id="<?= $toefl_score_form_id ?>">
<table cellpadding="2px" cellspacing="2px" border="0">
    <tr>
        <?php //// PLB added type display 01/16/09 ?>
        <td colspan="3">TOEFL <?= $toeflType ?> &nbsp;&nbsp;
        <input type="checkbox" class="toefl_received" id="<?= $toefl_received_id ?>"  <?= $received_checked ?> /> Received
        </td>
    </tr>
    <tr>
        <td colspan="3">Test Date: <?= $toefl_testdate ?></td>
    </tr>
    <tr>
        <?php // PLB changed section headings to use labels array 01/16/09 ?>
        <!--
        <td>Section 1</td>
        -->
        <td><?= $section_labels[$toeflType]['section1'] ?></td>
        <td>
        <input type="text" size="5" class="toefl_section1_score" id="<?= $toefl_section1_score_id ?>" value="<?= $toefl_section1_score ?>">
        </td>
    </tr>
    <tr>
        <!--
        <td>Section 2</td>
        -->
        <td><?= $section_labels[$toeflType]['section2'] ?></td>
        <td>
        <input type="text" size="5" class="toefl_section2_score" id="<?= $toefl_section2_score_id ?>" value="<?= $toefl_section2_score ?>">
        </td>
    </tr>
    <tr>
        <!--
        <td>Section 3</td>
        -->
        <td><?= $section_labels[$toeflType]['section3'] ?></td>
        <td>
        <input type="text" size="5" class="toefl_section3_score" id="<?= $toefl_section3_score_id ?>" value="<?= $toefl_section3_score ?>">
        </td>
    </tr>
    <tr>
        <!--
        <td>Essay</td>
        -->
        <td><?= $section_labels[$toeflType]['essay'] ?></td>
        <td>
        <input type="text" size="5" class="toefl_essay_score" id="<?= $toefl_essay_score_id ?>" value="<?= $toefl_essay_score ?>">
        </td>
    </tr>
    <tr>
        <td>Total</td>
        <td>
        <input type="text" size="5" class="toefl_total_score" id="<?= $toefl_total_score_id ?>" value="<?= $toefl_total_score ?>">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <!-- <input type="reset" value="Reset"> -->
        <input type="submit" class="submitSmall update_toefl_score_submit" 
            id="<?= $update_toefl_score_submit_id ?>" value="Update <?= $toeflType ?> Score"/>
        </td>
    </tr>
</table> 
</form>    
    
<?php   
}



function displayEducation($education_records) {
    
     $application_id = $education_records[0]['application_id'];
     $transcript_form_id = $application_id . "_transcripts"; 

    ?>
    
    <form id="<?= $transcript_form_id ?>">
    <table cellspacing="5px" cellpadding="5px" border="0">
    <?php
    foreach ($education_records as $record) {
        
        // if record isn't full of null values
        $trascript_counter = 0;
        if ($record['institute_id'] != NULL) {
        
            // determine whether to have the "transcript received" box checked
            $received_checked = "";
        
            // NOTE misspelling of field transcriptreceived with "ss"
            if ($record['transscriptreceived'] == 1) {
                $received_checked = "checked";
                }
        
            $transcript_received_id = $record['id'] . "_transcript_received";
        
            print "<tr valign=\"top\"><td>";
            //print $record['id'] . ".&nbsp;";
            print $record['name'] . "<br/>";
            print $record['degree'] . ", " . $record['major1'] . "<br/>";
            print $record['date_entered'] . " - ";
            if ( $record['date_grad'] ) {
                print $record['date_grad'];
            } else {
                print $record['date_left'];
            }
            ?>
            </td><td align="center">
            <input type="checkbox" class="transcript_received" id="<?= $transcript_received_id ?>" <?= $received_checked ?> /> Received
            </td></tr>
            <?php
            $transcript_counter++;
        }
    }
    
    if ( $transcript_counter == 0 ) {
        ?>
        <td colspan="3"><p>No transcripts found.</p></td> 
        <?php
    }
      
    print "</table></form>";
}


function displayRecommendations($recommendation_records) {
    
    global $datafileroot;
    
    // vars for use in forms
    $application_id = $recommendation_records[0]['application_id'];
    $applicant_id = $recommendation_records[0]['applicant_id'];
    $applicant_guid = $recommendation_records[0]['guid'];
    $recommendation_form_id = $application_id . "_recommendations";
    ?>

    <br />
    <form id="<?= $recommendation_form_id ?>">
    <input type="hidden" class="applicant_id" id="<?= $applicant_id ?>" />
    <input type="hidden" class="applicant_guid" id="<?= $applicant_guid ?>" />
    <table cellspacing="5px" cellpadding="5px" border="0">
    <?php
    $recommendation_count = 0;
    foreach ($recommendation_records as $record) {
                
        // if the record isn't full of null values, then display
        if ( $record['rec_user_id'] != -1 ) {
        
            print "<tr valign=\"top\"><td>";
            //print $record['id'] . ". ";
            print $record['lastname'] . ", " . $record['firstname'] . "<br/>";
            //print $record['company'];
            print "</td>";
        
            // more vars for use in forms
            $upload_recommendation_file_id = $record['id'] . "_upload_recommendation_file";
            $upload_recommendation_submit_id = $record['id'] . "_upload_recommendation_submit";

            // if the recommendation has been submitted with an uploaded file
            if ( $record['datafile_id'] ) {
                
                // vars for link to file - needs to be updated
                $filename = "recommendation_" . $record['userdata'] . "." . $record['extension'];                 
                $filepath = $datafileroot . "/" . $applicant_guid . "/recommendation/" . $filename;
                
                ?>
                <td>
                <?php
                $link = '<a class="file_link" href="' . $filepath . '" target="_blank">file</a>';
                print '<span class="confirm_complete">Received</span><br />(' . $link . ')';
                ?>
                </td>
                <td>
                <input type="hidden" class="upload_mode" value="update" />
                <!-- Note: name of file input must be "fileToUpload" for ajax function to work -->
                <input size="30" class="upload_recommendation_file" id="<?= $upload_recommendation_file_id ?>" name="fileToUpload" type="file" /> 
                <input class="upload_recommendation_submit" id="<?= $upload_recommendation_submit_id ?>" type="submit" value="Update Letter" />
                
                </td>
                <?php
            
            } else {
                          
                print "<td>";
                // if the recommendation was entered as "content"
                // check first to see if field contains more than "&lt;!--tml_entity_decode($content--&gt;" 
                $content_length = strlen($record['content']); 
                if ( $content_length > 50 ) {
                    print '<span class="confirm_complete">Received</span><br />(text)';
                } else {
                    print "Requested";
                }
                ?> 
                </td>
                <td>
                <input type="hidden" class="upload_mode" value="new" />
                <!-- Note: name of file input must be "fileToUpload" for ajax function to work -->  
                <input size="30" class="upload_recommendation_file" id="<?= $upload_recommendation_file_id ?>" name="fileToUpload" type="file" /> 
                <input class="upload_recommendation_submit" id="<?= $upload_recommendation_submit_id ?>" type="submit" value="Upload Letter" />
                </td>
                
            <?php
            }
            $recommendation_count++;
            print "</tr>";
        
        } 
    }
    
    // indicate no records found
    if ( $recommendation_count == 0 ) {
        ?>
        <td colspan="3"><p>No recommendations found.</p></td>
        <?php
    }
      
    print "</table></form>";
}











// ############## Functions no longer used #################

function displayBio($bioRecords) {
    echo '<p><b>' . $bioRecords[0]['lastname'] . ', ' . $bioRecords[0]['firstname'] . '</b></p>';
}


function displayPayment($payment_records, $total_fees_record) {
    
    $payment_record = $payment_records[0];
    
    $fee_waived = $payment_record['waive'];
    
    $total_fees = number_format($total_fees_record['total_fees'], 2);
    
    // change display if fee waived
    if ( $fee_waived )  {
        $form_disabled = "disabled";
        //print "<b><p>Fee Waived</p></b>";                
    } else {
        $form_disabled = "";
    } 

    // determine whether to have the "payment received" box checked
    $paid_checked = "";
    if ($payment_record['paid'] == 1) {
        $paid_checked = "checked";
    }

    $application_id = $payment_record['id'];
    $payment_amount = $payment_record['paymentamount'];
    $payment_date = date("d/m/Y H:i:s", strtotime($payment_record['paymentdate']));
    
    $payment_form_id = $application_id . "_payment";
    $fee_paid_id = $application_id . "_fee_paid";
    $update_payment_amount_id = $application_id . "_update_payment_amount";
    $update_payment_submit_id = $application_id . "_update_payment_submit";
    $reset_payment_submit_id = $application_id . "_reset_payment_submit";
    $total_fees_span_id =  $application_id . "_total_fees_span"; 
    
    ?>

    <form id="<?= $payment_form_id ?>">
    <table cellpadding="2px" cellspacing="2px">
 
        <tr>
        <td colspan="2">
        <?
        $checkbox_id = $application_id . "_fee_waived";
        if ( $fee_waived ) {
            $waived_checked = "checked";
        } else {
            $waived_checked = "";
        }
        print '<input type="checkbox" class="fee_waived" id="'. $checkbox_id . '" ' . $waived_checked . ' />';
        ?>
        Fee Waived
        </td>
        </tr>
        <tr>
            
            <td colspan="2">Total Fees: $<span id="<?= $total_fees_span_id ?>"><?= $total_fees ?></span> &nbsp; 
            <input type="checkbox" class="fee_paid" id="<?= $fee_paid_id ?>" <?= $paid_checked ?> <?= $form_disabled ?> /> Paid

            
            <?php 
            if ($payment_amount) {
                print "<br /><br />Payment:  $" . $payment_amount;
            }
            ?>
            </td>
        </tr>
    <?php
    if ($payment_record['paid'] == 1) {
        // show payment date
        ?>
        <tr>
            <td colspan="2">Payment Date: <?= $payment_date ?></td>
        </tr>
        <?php
    }
    ?>
        <tr>
            <td>New Payment Amount:</td>
            <td>
            <input type="text" size="5" class="update_payment_amount" id="<?= $update_payment_amount_id ?>" value="" <?= $form_disabled ?>> 
            <input type="submit" class="update_payment_submit" id="<?= $update_payment_submit_id ?>" value="Update Amount"  <?= $form_disabled ?>/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            <input type="submit" class="reset_payment_submit" id="<?= $reset_payment_submit_id ?>" value="Reset Payment"  <?= $form_disabled ?>/>
            </td>
        </tr>
    </table>
    </form>
    
    <?php     
}


function displayPublications($publication_records) {

    $publications_form_id = $publication_records[0]['application_id'] . "_publications";
?>

<form class="publications" id="<?= $publications_form_id ?>">
<table cellpadding="5px" cellspacing="10px" border="0">
    
    <?php
        if ( count($publication_records) > 0 ) {
            $publication_count = 0;
            foreach($publication_records as $publication_record) {
                if ( $publication_record['title'] != NULL ) {
                    print '<tr valign="top"><td width="60%">';
                    print $publication_record['title'] . ". ";
                    print $publication_record['author'] . ". ";
                    print $publication_record['forum'] . ". ";
                    if ($publication_record['citation'] != "") {
                        print $publication_record['citation'] . "."; 
                    }
                    print '</td><td width="40%">Status:&nbsp;';
                    makePublicationStatusSelect($publication_record['id'], $publication_record['status']);
                    print "</td></tr>";
                    $publication_count++;
                }
            }
            if ( $publication_count == 0 ) {
                print "<td>No publications found.</td>";
            }     
        } else {
            print "<td>No publications found.</td>"; 
        }          
    ?>
</table>
    
<?php
}

// function to use with displayPublications
function makePublicationStatusSelect($publication_id, $status) {
    $options = array("In Progress", "Submitted", "Accepted", "Not Accepted", "No Plans to Submit");
    ?>
    <select class="publication_status" id="<?= $publication_id ?>_publication_status" name="<?= $publication_id ?>_publication_status">
    <?php
    foreach($options as $option) {
        print '<option ="' . $option . '" ';
        if ($option == $status) {
            print 'selected';
        }
        print '>' . $option . '</option>';
    }
    ?>
    </select>
    <?php
}


/*
Simple function to print key/value pairs for fields in a db record
*/
function loopValues($db_records) {
    if ( is_array($db_records[0]) ) {
        foreach ($db_records as $record) {
            foreach ($record as $key => $value) {
                print $key . ": " . $value . "<br/>\n";
            }
        print "----<br/>\n";
        }    
    } else {
        foreach ($db_records as $key => $value) {
            print $key . ": " . $value . "<br/>\n";
        }    
    }
}
?>