<?php

// include the db classes
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once "../classes/class.db_phonescreens.php";
include_once "../classes/class.db_special_consideration.php";

// Check the special condsideration status. 
$db_special_consideration = new DB_SpecialConsideration();
//$special_considerations = $db_special_consideration->getSpecialConsiderationRequested($appid);
$specialConsiderationQuery = "SELECT special_consideration AS special_consideration_requested 
                                FROM special_consideration
                                WHERE application_id = " . $appid;
$specialConsiderationQuery .= " AND reviewer_id = " . $reviewerId;
$special_considerations = $db_special_consideration->handleSelectQuery($specialConsiderationQuery); 

// Set to zero here and loop db results to avoid error if null db set is returned.
$special_consideration_requested = 0;
foreach ($special_considerations as $special_consideration) {
    $special_consideration_requested = $special_consideration['special_consideration_requested'];
}

// Determine whether to have the checkbox checked.
if ($special_consideration_requested) {
    $special_consideration_checked = "checked";
} else {
    $special_consideration_checked = "";
}

// Check the phone screen status. 
$db_phonescreens = new DB_Phonescreens();
//$phonescreens = $db_phonescreens->getPhoneScreenRequested($appid);
$phonescreenQuery = "SELECT technical_screen AS technical_screen_requested, 
                        language_screen AS language_screen_requested
                        FROM phone_screens
                        WHERE application_id = " . $appid;
$phonescreenQuery .= " AND reviewer_id = " . $reviewerId;
$phonescreens = $db_phonescreens->handleSelectQuery($phonescreenQuery);

// Set to zero here and loop db results to avoid error if null db set is returned.
$technical_screen_requested = 0;
$language_screen_requested = 0;
foreach ($phonescreens as $phonescreen) {
    $technical_screen_requested = $phonescreen['technical_screen_requested'];
    $language_screen_requested = $phonescreen['language_screen_requested'];
}

// Determine whether to have the checkboxes checked.
if ($technical_screen_requested) {
    $technical_screen_checked = "checked";
} else {
    $technical_screen_checked = "";
} 
if ($language_screen_requested) {
    $language_screen_checked = "checked";
} else {
    $language_screen_checked = "";
}

// Only enable the inputs for admissions committee members and the admissions chair.
$special_consideration_disabled = "disabled";
$technical_screen_disabled = "disabled";
$language_screen_disabled = "disabled";
if ( $allowEdit &&
    ($_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 10 ) ) {
    $special_consideration_disabled = "";
    $technical_screen_disabled = "";
    $language_screen_disabled = "";
}

// Set up the mailto href.
/*
$mailto_href = "mailto:?subject=Applyweb: Request to review application";
$mailto_href .= "&body=" . $_SESSION['A_firstname'] . " " . $_SESSION['A_lastname']; 
$mailto_href .= " requests that you review the application of:%0A%0A";
$mailto_href .= $lName .", ".$fName ." ". $mName . "%0A";
$mailto_href .= $email . "%0A%0A";
// $pdf_path is set in pdf_link.inc.php
// $hostname is a session/global variable?
// must take care of relative path, hostname in all caps
$mailto_link_host = strtolower($hostname);
$mailto_link_path = str_replace( "..", "/htdocs", $pdf_path );
$mailto_href .= "https://" . $mailto_link_host . $mailto_link_path;
*/
$mailto_href = "../admin/adhocGroups.php?userid=" . $uid . "&aid=" . $appid . "&department=" . $thisDept;

?>
<input type="hidden" id="application_id" value="<?= $appid ?>" />
<input type="hidden" id="reviewer_id" value="<?= $reviewerId ?>" /> 

<table width="500px">

    <?php
    if ( $_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 10 ) {
    ?>
        <tr><td align="left" colspan="2">
        <div style="margin-bottom: 5px;">
        <a href="<?= $mailto_href ?>">Refer application to faculty reviewer</a>
        </div>
        </td></tr>
    <?php
    }
    ?>

    <tr>        
        <td align="left">
        <b>Give special consideration</b>
        <input type="checkbox" id="special_consideration" <?= $special_consideration_checked ?> <?= $special_consideration_disabled ?> />
        </td>
        <td align="center">
        <b>Request phone screen(s):</b>
        <span id="phone_screens">
        <input type="checkbox" id="technical_screen" <?= $technical_screen_checked ?> <?= $technical_screen_disabled ?> /> Technical
        <input type="checkbox" id="language_screen" <?= $language_screen_checked ?> <?= $language_screen_disabled ?> /> Language
        </span>
        </td>
    </tr>
             
</table>
<br />