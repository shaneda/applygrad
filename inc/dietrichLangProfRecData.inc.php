<?php
// Initialize variables
$myLangProfRecommender = array();
$langProfError =  '';
$recfirstName = '';
$recLastName = '';
$recTitle ='';
$recCompany = '';
$recEmail = '';
$recPhone = '';
$recLangOfInterest = '';

$myPrograms = array();

$applastname = "";
$appfirstname = "";
$appemail = "";
$appdegree = "";
$appfield = "";
$appschool = "";
$enddate = "";

//GET SYSTEM EMAIL
$domainid = 1;
if(isset($_SESSION['domainid']))
{
    if($_SESSION['domainid'] > -1)
    {
        $domainid = $_SESSION['domainid'];
    }
}
$sql = "select sysemail from systemenv where domain_id=".$domainid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{ 
    $sysemail = $row['sysemail'];
}


//GET USER INFO THAT WE'LL NEED
$sql = "SELECT programs.id, 
degree.name as degreename,
fieldsofstudy.name as fieldname, 
choice, 
lu_application_programs.id as itemid,
users.firstname,
users.lastname,
users.email,
department.name as departmentname,
schools.name as schoolname,
programs.linkword
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
inner join application on application.id = lu_application_programs.application_id
inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
inner join users on users.id = lu_users_usertypes.user_id
inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
inner join department on department.id = lu_programs_departments.department_id
inner join schools on schools.id = department.parent_school_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice limit 1";
//echo $sql."<br>";
//echo $_SESSION['userid']."<br>";

$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result )) 
{
    $applastname = stripslashes($row['lastname']);
    $appfirstname = stripslashes($row['firstname']);
    $appemail = stripslashes($row['email']);
    $appdegree = $row['degreename'];
    $appfield = $row['fieldname'];
    $appschool = $row['schoolname'];
    $arr = array();
    array_push($arr, $row['id']);
    array_push($myPrograms, $arr);
    
}

function addLangProfRec($allowDup, $type)
{
    
    $sql = "insert into lang_prof_recommend(application_id, rec_user_id)values(".$_SESSION['appid'].",-1)";
    mysql_query($sql) or die(mysql_error().$sql);    
}

function getInfo()
{
    $ret = array();
    //RETRIEVE USER INFORMATION
    $sql = "SELECT 
    lang_prof_recommend.id,
    lang_prof_recommend.rec_user_id,
    lang_prof_recommend.submitted,
    lang_prof_recommend.content,
    users.id as usermasterid,
    users.title,
    users.firstname,
    users.lastname,
    users.email,
    users_info.address_perm_tel, 
    lu_users_usertypes.id as uid,
    users_info.company,
    reminder_sent_count,
    lang_prof_recommender_info.title AS title_override,
    lang_prof_recommender_info.affiliation AS affiliation_override,
    lang_prof_recommender_info.phone AS phone_override,
    lang_prof_recommender_info.language_specialization
    FROM lang_prof_recommend
    left outer join lu_users_usertypes on lu_users_usertypes.id = lang_prof_recommend.rec_user_id
    left outer join users on users.id = lu_users_usertypes.user_id
    left join users_info on users_info.user_id = lu_users_usertypes.id
    LEFT OUTER JOIN lang_prof_recommender_info ON lang_prof_recommend.id = lang_prof_recommender_info.recommend_id
    where lang_prof_recommend.application_id=".$_SESSION['appid']. " order by lang_prof_recommend.id";
    $result = mysql_query($sql) or die(mysql_error().$sql);
    while($row = mysql_fetch_array( $result )) 
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['rec_user_id']);
        array_push($arr, stripslashes($row['firstname']));
        array_push($arr, stripslashes($row['lastname']));
        array_push($arr, stripslashes($row['title']));
        array_push($arr, stripslashes($row['company']));
        array_push($arr, stripslashes($row['email']));
        array_push($arr, stripslashes($row['address_perm_tel']));
        array_push($arr,$row['reminder_sent_count']);
        array_push($arr, $row['usermasterid']);
        array_push($arr, $row['content']);
        array_push($arr, stripslashes($row['title_override'])); // $arr[17]
        array_push($arr, stripslashes($row['affiliation_override']));
        array_push($arr, stripslashes($row['phone_override']));
        array_push($arr, stripslashes($row['submitted']));
        array_push($arr, stripslashes($row['language_specialization']));
        $ret = $arr;
    }
    return $ret;
}

function sendLangProfMail($type,$vars, $email)
{
    $str = "";
        $clean_str = "";
    global $domainid;
    global $sysemail;
    global $appfirstname;
    global $applastname;
    // DebugBreak();
    switch($type)
    {
        case 1:
            $sql = "select content from content where name='Lang Prof Recommendation Letter' and domain_id=".$domainid;
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $str = $row["content"];
            }
            $str = parseEmailTemplate2($str, $vars );
                        /* This cleans the string of the html representation of & */
                        /* which is causing problems for recommenders accessing the site */
                        /* needs to be fixed in the mail template tinyMCE tool */
                        $clean_str = str_replace("&amp;", "&", $str);

            sendHtmlMail($sysemail, $email, "Language Proficiency Recommendation Requested For ".$appfirstname." " . $applastname, $clean_str, "recommend");
        break;
        case 2:
            //GET TEMPLATE CONTENT
            $sql = "select content from content where name='Lang Prof Recommendation Reminder Letter' and domain_id=".$domainid;
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $str = $row["content"];
            }
            $str = parseEmailTemplate2($str, $vars );
                        /* This cleans the string of the html representation of & */
                        /* which is causing problems for recommenders accessing the site */
                        /* needs to be fixed in the mail template tinyMCE tool */
                        $clean_str = str_replace("&amp;", "&", $str);
            sendHtmlMail($sysemail, $email, "Language Proficiency Recommendation Reminder for ". $appfirstname." ".$applastname, $clean_str, "recreminder");
        break;
    }
}

function saveLangProfRecommender() {
    
    global $recfirstName;
    global $recLastName;
    global $recTitle;
    global $recCompany;
    global $recEmail;
    global $recPhone;
    global $recLangOfInterest;
    global $err;
    
    $recFirstName = filter_input(INPUT_POST, 'txtFname', FILTER_SANITIZE_STRING);
    $recLastName =  filter_input(INPUT_POST, 'txtLname', FILTER_SANITIZE_STRING);
    $recTitle =  filter_input(INPUT_POST, 'txtTitle', FILTER_SANITIZE_STRING);
    $recCompany =  filter_input(INPUT_POST, 'txtAffiliation', FILTER_SANITIZE_STRING);
    $recEmail =  filter_input(INPUT_POST, 'txtEmail', FILTER_VALIDATE_EMAIL);
    $recPhone =  filter_input(INPUT_POST, 'txtPhone', FILTER_SANITIZE_STRING);
    $recLangOfInterest =  filter_input(INPUT_POST, 'txtLangOfInterest', FILTER_SANITIZE_STRING);

    if ($recFirstName != '' && $recLastName != '' && $recEmail != '' && $recCompany != '' && $recTitle != '' && $recPhone != '' && $recLangOfInterest != '')
    { 
        $umasterid = -1;
        $uid = -1;
            //LOOKUP USER
            //echo "looking up user ";
            $sql = "select id from users where users.firstname='".addslashes($recFirstName)."' and users.lastname='".addslashes($recLastName)."' and users.email='".addslashes($recEmail)."'";
            $result = mysql_query($sql)or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $umasterid = $row['id'];
                //echo "record found " . $row['id'];
            }
            //IF USER DOESN'T EXIST, ADD
            if($umasterid == -1)
            {
                $pass = STEM_GeneratePassword();
                $guid = makeGuid();
                $sql = "insert into users(email, password,title,firstname,lastname, signup_date, verified, guid) 
                values( '".addslashes($recEmail)."', '".$pass."', '".addslashes($recTitle)."', '".addslashes($recFirstName)."', '".addslashes($recLastName)."', '".date("Y-m-d h:i:s")."', 0, '".$guid."')";
                $result = mysql_query($sql)or die(mysql_error());
                $umasterid = mysql_insert_id();
            }
            //FIND EXISTING RECOMMENDATION ACCOUNT FOR USER (IF EXISTS)
            $sql = "select lu_users_usertypes.id, users.id as umasterid from users 
                inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
                where lu_users_usertypes.user_id=".$umasterid . " and lu_users_usertypes.usertype_id=22";
            $result = mysql_query($sql) or die(mysql_error(). $sql);
            while($row = mysql_fetch_array( $result )) 
            {
                $uid = $row['id'];
                //echo "found user type ";
            }
            //INSERT RECOMMENDATION RECORD IF IT DOESN'T EXIST 
            if($uid == -1)
            {
                $sql = "insert into lu_users_usertypes(user_id, usertype_id,domain)values( ".$umasterid.", 22, NULL)";
                mysql_query( $sql) or die(mysql_error());
                $uid = mysql_insert_id();
                //echo "inserting recommend id ". $uid. "<br>";
            }
            //FIND USERINFO FOR USER
            $doInsert = true;
            $sql = "select id from users_info where user_id=".$uid;
            $result = mysql_query($sql)or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $doInsert = false;
            }
            if($doInsert == true)
            {
                $sql = "insert into users_info(user_id,company, address_cur_tel,address_perm_tel)values(".$uid.",'".addslashes($recCompany)."'    ,'".addslashes($recPhone)."',    '".addslashes($recPhone)."')";
                mysql_query( $sql) or die(mysql_error());
                //echo "inserting userinfo<br>";
            }
            
            //INSERT RECOMMENDATION RECORD
            $sql = "INSERT INTO lang_prof_recommend (application_id, rec_user_id) VALUES 
             (".$_SESSION['appid'].", ".$uid. ") ON DUPLICATE KEY UPDATE content = 'duplicate'"; 
            mysql_query( $sql) or die(mysql_error());
            $recId = mysql_insert_id();
            // PLB added 10/31/11: save recommender info entered by applicant
            $recommenderInfoInsertQuery = "INSERT INTO lang_prof_recommender_info VALUES
                (" . intval($recId) . ","
                . intval($uid) . ",'"
                . mysql_real_escape_string($recTitle) . "','"
                . mysql_real_escape_string($recCompany) . "','"
                . mysql_real_escape_string($recPhone) . "','"
                . mysql_real_escape_string($recLangOfInterest) . "') 
                ON DUPLICATE KEY UPDATE title = '" . mysql_real_escape_string($recTitle) . "', affiliation= '" . mysql_real_escape_string($recCompany) .
                "', phone = '" . mysql_real_escape_string($recPhone) . "', language_specialization = '" . mysql_real_escape_string($recLangOfInterest) . "'";
            mysql_query($recommenderInfoInsertQuery);
        
    } else {
        $langProfError =  'Not complete recommender to create record';
        echo $langProfError;
        $err .= $langProfError;
    }
}

function checkLangProfRecommender() {
    
}

function set_recommender ($app_id)  {
    $recommender = array();
    $sql = "select id from lang_prof_recommend where application_id = ".$_SESSION['appid'] ;
    $result = mysql_query($sql)    or die(mysql_error());
    $count = mysql_num_rows( $result );
/*
    for($i = $count; $i < 1; $i++)
    {
        addLangProfRec(false, 1);
    }
    */
    switch ($count) {
        case 0:
            $arr = array();
            array_push($arr, NULL);
            array_push($arr, NULL);
            array_push($arr, '');
            array_push($arr, '');
            array_push($arr, '');
            array_push($arr, '');
            array_push($arr, '');
            array_push($arr, '');
            array_push($arr, NULL);
            array_push($arr, NULL);
            array_push($arr, NULL);
            array_push($arr, ''); // $arr[17]
            array_push($arr, '');
            array_push($arr, '');
            array_push($arr, NULL);
            array_push($arr, '');
            $recommender = $arr;
            break;
        case 1:
            $recommender = getInfo();
            break;
        default:
            $recommender = getInfo();
            break;
            
    }
    return $recommender;
}

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveLangProfRecommender();
 //   checkLangProfRecommender();
}

// Anytime you come to the page make sure there is one record in the database
$doAdd = true;


//  $myLangProfRecommender = set_recommender($_SESSION['appid']);
    

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$langProfError)
{
    if(!isset($_POST['btnSave']))
    {
        $myLangProfRecommender = set_recommender($_SESSION['appid']);
    }//end if btnSave
        
}

if( isset( $_POST ) && !isset($_POST['btnAdd']) )
{
    $vars = $_POST;
    $itemId = -1;
    $uid = -1;
    foreach($vars as $key => $value)
    {
        if( strstr($key, 'btnSendmail') !== false ) 
        {
            $arr = split("_", $key);
            $email = "";
            $record = intval($_POST["txtRecord_".$arr[1]]);
            if($_POST["txtUid_".$arr[1]] != "")
            {
                $uid = $_POST["txtUid_".$arr[1]];
                $itemId = $_POST["txtRecord_".$arr[1]];
                $email = $_POST["txtEmail"];
                $reminderSentCount = intval($_POST["txtRecSent_".$arr[1]]);
            }
            if($reminderSentCount < 3)
            {
                $pass = "";
                $sql = "select users.guid, lu_users_usertypes.user_id, lu_users_usertypes.id from users  
                inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
                where lu_users_usertypes.id=".intval($_POST["txtUid_".$arr[1]]);
                $result = mysql_query($sql)or die(mysql_error());
                while($row = mysql_fetch_array( $result )) 
                {
                    $pass = $row['guid'];
                    $accesscode = $row['user_id']."-".$_SESSION['domainid']."-".$row['id'];
                    $accesscode .= "-" . $_SESSION['appid']; 
                }
                $vars = array(
                array('email',$email), 
                array('user',str_replace("+","%2b",$email)),    
                array('pass',$pass),    
                array('enddate',formatUSdate3($_SESSION['expdate']) ),
                array('replyemail',$sysemail),
                array('lastname',$applastname),
                array('firstname',$appfirstname),
                array('appemail',$appemail),
                array('appid',$_SESSION['appid']),
                array('degree',$appdegree),
                array('field',$appfield),
                array('school',$appschool),
                array('domainid',$_SESSION['domainid']),
                array('accesscode',$accesscode)
                );
                sendLangProfMail(1,$vars, $email);
                $reminderSentCount++;
                
                $sql = "update lang_prof_recommend set reminder_sent_count=".$reminderSentCount. " where id=".$itemId;
                mysql_query($sql)    or die(mysql_error().$sql);
            }else
            {
                $err .= "You cannot send more than 3 recommendation reminders.<br>";
                $err_2 .= "You cannot send more than 3 recommendation reminders.<br>";
            }
        }
        if( strstr($key, 'btnSendReminder') !== false ) 
        {
            $arr = split("_", $key);
            $email = "";
            $record = intval($_POST["txtRecord_".$arr[1]]);
            if($_POST["txtUid_".$arr[1]] != "")
            {
                $uid = $_POST["txtUid_".$arr[1]];
                $itemId = $_POST["txtRecord_".$arr[1]];
                $email = $_POST["txtEmail"];
                $reminderSentCount = intval($_POST["txtRecSent_".$arr[1]]);
            }
            if($reminderSentCount < 3)
            {
                $pass = "";
                $sql = "select guid, lu_users_usertypes.user_id, lu_users_usertypes.id from users 
                inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
                where lu_users_usertypes.id=".intval($_POST["txtUid_".$arr[1]]);
                $result = mysql_query($sql)or die(mysql_error());
                while($row = mysql_fetch_array( $result )) 
                {
                    $pass = $row['guid'];
                    $accesscode = $row['user_id']."-".$_SESSION['domainid']."-".$row['id'];
                    $accesscode .= "-" . $_SESSION['appid']; 
                }
                $vars = array(
                array('email', $email), 
                array('user',str_replace("+","%2b",$email)),
                array('pass',$pass),
                array('enddate',formatUSdate3($_SESSION['expdate']) ),
                array('replyemail',$sysemail),
                array('lastname',$applastname),
                array('firstname',$appfirstname),
                array('appemail',$appemail),
                array('degree',$appdegree),
                array('field',$appfield),
                array('school',$appschool),
                array('username',$email),
                array('domainid',$_SESSION['domainid']),
                array('accesscode',$accesscode)
                );
                sendLangProfMail(2,$vars, $email);
                $reminderSentCount++;
                
                $sql = "update lang_prof_recommend set reminder_sent_count=".$reminderSentCount. " where id=".$itemId;
                mysql_query($sql)    or die(mysql_error().$sql);
            }else
            {
                $err .= "You cannot send more than 3 recommendation reminders.<br>";
                $err_2 .= "You cannot send more than 3 recommendation reminders.<br>";
            }
        }
        if( strstr($key, 'btnDel') !== false ) 
        {
            $arr = split("_", $key);
            $umasterid = -1;
            if($_POST["txtUid_".$arr[1]] != "")
            {
                $uid = $_POST["txtUid_".$arr[1]]; 
                $umasterid = $_POST["txtUMasterid_".$arr[1]]; 
            }else
            {
                $err .= "record not found.";
            }
            /*
            //GET USER'S RECOMMENDER IDENTITY ID
            $sql = "select id from lu_users_usertypes where id=".$itemId. " and usertype_id = 6 ";
            $result = mysql_query($sql)    or die(mysql_error().$sql);
            while($row = mysql_fetch_array( $result )) 
            {
                $uid = $row['id'];
                $identityCounts++;
            }
            */
            $sql = "DELETE FROM lang_prof_recommend WHERE application_id=".$_SESSION['appid']. " and rec_user_id=" . $uid;
            mysql_query($sql) or die(mysql_error().$sql );
            
            // PLB added 10/31/11: delete extra recommender info entered by applicant
            $recommendId = $_POST["txtRecord_".$arr[1]];
            $recommenderInfoDeleteQuery = "DELETE FROM lang_prof_recommender_info WHERE recommend_id = " . intval($recommendId);
            mysql_query($recommenderInfoDeleteQuery);
            
            //CHECK TO SEE IF THIS RECOMMENDER IS ASSIGNED TO ANYONE ELSE. IF NOT, REMOVE RECOMMENDER IDENTITY ONLY
            $sql = "select id from lang_prof_recommend where rec_user_id=".$uid;
            $result = mysql_query($sql)    or die(mysql_error());
            $doDelete = true;
            while($row = mysql_fetch_array( $result )) 
            {
                $doDelete = false;
            }
            if($doDelete == true)
            {
                $sql = "DELETE FROM  lu_users_usertypes WHERE id=".$uid. " and usertype_id=6";
                mysql_query($sql) or die(mysql_error().$sql );
                if(mysql_affected_rows() > 0)
                {
                    $sql = "DELETE FROM usersinst WHERE user_id=".$uid;
                    mysql_query($sql) or die(mysql_error().$sql );
                    
                    $sql = "DELETE FROM  users_info WHERE user_id=".$uid;
                    mysql_query($sql) or die(mysql_error().$sql );
                    $updateApp = true;
                    
                    //SEE IF THERE ARE OTHER ACCOUNT TYPES ASSIGNED TO THIS USER. IF NOT, DELETE USER.
                    $sql = "select id from lu_users_usertypes where user_id=".$umasterid;
                    $result = mysql_query($sql)    or die(mysql_error());
                    if(mysql_num_rows($result) == 0)
                    {
                        $sql = "DELETE FROM  users WHERE id=".$umasterid;
                        mysql_query($sql) or die(mysql_error().$sql );
                    }else
                    {
                        while($row = mysql_fetch_array( $result )) 
                        {
                            //echo $row['id'. "<br>"];
                        }
                    }
                }
            }//end if dodelete
        }//end btnDel
    }//END FOR
    $myLangProfRecommender = set_recommender($_SESSION['appid']);
}//END IF

// Check and update if the requirement for language proficincy is complete
$comp = false;
for($i = 0; $i < count($myLangProfRecommender); $i++)
{
    if($myLangProfRecommender[2] == "")
    {
        $comp = false;
    }
}

/*
if(count($myLangProfRecommender) >=  1 && $comp == true && $err == "")
{
    
    if($comp == true)
    {
        if(isset($_POST['btnSave']))
        {
            updateReqComplete("language.php", 1);
        }else
        {
            updateReqComplete("language.php", 1, 1);
        }
    }
    
}else
{
    updateReqComplete("recommendations.php", 0,1, 0);
}   
// end check and update for requirement complete
*/

//     **************************************************************************************************************************************************************
    $allowEdit = $_SESSION['allow_edit'];
    $submitted = "";
    if($myLangProfRecommender[8] > 0)
    {
        $allowEdit = false;
    }
    $title = "";
    
       
    if($myLangProfRecommender[8] > 0)
    {
        $submitted = "(Language Proficiency Recommendation Requested)";
    } else {
        if( $myLangProfRecommender[6] != "" )  // Is there an email address? 
        //if( $myLangProfRecommender[6] != "" && $_SESSION['domainname'] != 'RI-MS-RT' ) // Is there an email address?
        {
            $submitted = "(NOTE: Email Request for language proficiency recommendation has NOT been sent)";
        }
    }
    $attCount = sizeof($myLangProfRecommender);

    if ($attCount < 17) {
        if ($myLangProfRecommender[14] > 0) 
        {
         $submitted = "(Language Proficiency Report Received)";
         }
    } /* else {
        if (($myLangProfRecommender[14] > 0) || ($myLangProfRecommender[10] != NULL))
        {
         $submitted = "(Language Proficiency Report Received)";
         }
    }
      */
    ?> 