<?php
if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
{
//header("Location: ../apply/offline.php");
}

/*
CONSTANT VALUES FOR THE APPLICATION
*/
$environment="development";

$db_host="rogue.fac.cs.cmu.edu";
// $db_username="phdAdmin";
$db_username="phdapp";
$db_password="phd321app";
$db="gradAdmissions2008Test";

//$datafileroot = "../data/".date("Y");
//$datafileroot = "../data/2009";
$datafileroot = "../data";

$dontsendemail = TRUE;
$admissionsContact = "applyweb+admissions@cs.cmu.edu"; 
$paymentEmail = "applyweb+payment@cs.cmu.edu";
$supportEmail = "applyweb+technical@cs.cmu.edu";

//$paymentProcessor = "https://ccard-submit.as.cmu.edu/cgi-bin/gather_info.cgi"; //PRODUCTION USING GENERIC COLLECTOR
//$paymentProcessor = "https://ccard-submit-test.as.cmu.edu/cgi-bin/submit_cc.cgi"; //PRODUCTION DIRECT POST
$paymentProcessor = "https://ccard-submit-test.as.cmu.edu/cgi-bin/gather_info.cgi"; //TESTING USING GENERIC COLLECTOR

if ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) {
    $magic_location = "/usr0/apache2/inc/magic-php-5.3";
    error_reporting(E_ALL ^ E_DEPRECATED);    
} else {
    $magic_location = "/usr0/apache2/inc/magic";
    error_reporting(E_ALL);    
}

ini_set('display_errors', 1);
?>