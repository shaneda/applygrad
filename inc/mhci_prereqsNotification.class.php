<?php
function getReviewers ($type, $period_id){
    $reviewers = array();
    
    $sql = "select mhci_prereqsReviewers.reviewer_luu_id as id, users.firstname as rev_firstname, users.lastname as rev_lastname, users.email as rev_email from mhci_prereqsReviewers ";
    $sql .= "inner join lu_users_usertypes on lu_users_usertypes.id = mhci_prereqsReviewers.reviewer_luu_id";
    $sql .= " inner join users on users.id = lu_users_usertypes.user_id";
    $sql .= " where mhci_prereqsReviewers.prereq_type = '";
    $sql .=  $type . "' and mhci_prereqsReviewers.placeout_period_id = " . $period_id;
    $sql .=  " and mhci_prereqsReviewers.email_notification = 1";

    $result = mysql_query($sql)    or die(mysql_error());
    while($row = mysql_fetch_array( $result )) 
    {   
        $rev = array();
        $rev['id']  = $row['id'];
        $rev['fname'] = $row['rev_firstname'];
        $rev['lname'] = $row['rev_lastname'];
        $rev['rev_email'] = $row['rev_email'];
        $reviewers[] = $rev;
    }
    return  $reviewers;
}

 function sendNotification ($prereqType) {  
    if(isset($_POST['TestSubmit']))
    {       $studentSql = "select users.lastname, users.firstname, users.email from users ";
            $studentSql .= "inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id ";
            $studentSql .= "and lu_users_usertypes.id = ";
            $studentSql .=  $_SESSION['userid'];
            $studentResult = mysql_query($studentSql)    or die(mysql_error());
             while($row = mysql_fetch_array( $studentResult )) 
            {
               // DebugBreak();
                $lastname = $row['lastname'];
                $firstname = $row['firstname'];
                $studentEmail = $row['email'];
            }
           // DebugBreak();
            $reviewerInfo = getReviewers($prereqType, $_SESSION['activePlaceoutPeriod']);

      //      $sql = "update application set submitted=1, submitted_date='".date("Y-m-d h:i:s")."' where id=".$_SESSION['appid'];
      //      mysql_query($sql) or die(mysql_error());
      //      $submitted = "Submitted";
      //      if ($hostname == "APPLY.STAT.CMU.EDU")  {
      //          $replyemail = "scsstats@cs.cmu.edu";
     //       } else {
                $replyemail = "applygrad@cs.cmu.edu";
       //     }
                    
            //GET MAIL TEMPLATE
            $str = "";
            $domain = 1;
            if($_SESSION['domainid'] > -1)
            {
                $domain = $_SESSION['domainid'];
            }
            $sql = "select content from content where name='Placeout Submitted Letter' and domain_id=".$domain;
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $str = $row["content"];
            }
            
/*            $degrees = "";
            for($i = 0; $i < count($myPrograms); $i++)
            {
                $degrees .= $myPrograms[$i][1] . " ". $myPrograms[$i][5]." ". $myPrograms[$i][2]."<br>";
            }
            */
            
            $vars = array(    
            array('enddate',formatUSdate($_SESSION['expdate']) ),
            array('replyemail',$replyemail),
            array('lastname',$lastname),
            array('firstname',$firstname),
            array('student_email',$studentEmail),
            array('prereq_type',$prereqType)
      //      array('degrees',$degrees)
            );
            $str = parseEmailTemplate2($str, $vars );
            foreach ($reviewerInfo as $reviewer)   {
                sendHtmlMail($replyemail, $reviewer['rev_email'], "Placeout Submission to Carnegie Mellon University", $str, "submit");
            }

    }
    }

function sendStudentEmail($studentId, $prereq_name) {
    global $activePlaceoutPeriod;
    if(isset($_POST['TestSubmit']))
    {       $studentSql = "select users.lastname, users.firstname, users.email from users ";
            $studentSql .= "inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id ";
            $studentSql .= "and lu_users_usertypes.id = ";
            $studentSql .=  $studentId;
            $studentResult = mysql_query($studentSql)    or die(mysql_error());
             while($row = mysql_fetch_array( $studentResult )) 
            {
               // DebugBreak();
                $lastname = $row['lastname'];
                $firstname = $row['firstname'];
                $studentEmail = $row['email'];
            }
                            $replyemail = "applygrad@cs.cmu.edu";
       //     }
                    
            //GET MAIL TEMPLATE
            $str = "";
            $domain = 1;
            if($_SESSION['domainid'] > -1)
            {
                $domain = $_SESSION['domainid'];
            }
            $content_title =  "Placeout " . ucfirst($prereq_name) . " Response";
            $sql = "select content from content where name='" . $content_title . "' and domain_id=".$domain;
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $str = $row["content"];
            }
            
/*            $degrees = "";
            for($i = 0; $i < count($myPrograms); $i++)
            {
                $degrees .= $myPrograms[$i][1] . " ". $myPrograms[$i][5]." ". $myPrograms[$i][2]."<br>";
            }
            */
            
            $vars = array(    
      //      array('enddate',formatUSdate($_SESSION['expdate']) ),
            array('replyemail',$replyemail),
            array('lastname',$lastname),
            array('firstname',$firstname),
            array('student_email',$studentEmail),
            array('prereq_type',$prereq_name)
      //      array('degrees',$degrees)
            );
            $reviewers = getReviewers($prereq_name, $activePlaceoutPeriod);
            $str = parseEmailTemplate2($str, $vars );
                sendHtmlMail($replyemail, "", "Placeout Submission to Carnegie Mellon University", $str, "submit");
    }
    
}

?>