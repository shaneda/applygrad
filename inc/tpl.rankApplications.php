<div style="margin: 10px 5px; padding: 5px; clear: both;">

    <div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%;">
        <ul id="viewMenu" class="menu">
        <?php
        foreach ($viewMenuItems as $itemLabel => $itemGroupId) {
            $href = '?departmentId=' . $departmentId . '&period=' . $periodId . '&round=' . $round . '&group=' . $itemGroupId;
            $class = $divId = $linkStyle = '';
            if ($itemGroupId == $selectedGroupId) {
                $class = ' class="selectedItem"';
                $divId = ' id="selectedMenuDiv"';
                $linkStyle = ' style="color: #990000;"';     
            }
            echo '<li' . $class . '><div ' . $divId . '><a href="' . $href . '">' . $itemLabel . '</a></div></li>';  
        }
        ?>    
        </ul>
    </div>

    <div style="margin-left: 20px; float: left; clear: right; width: 75%;">
        <div style="margin-left: 20px; clear: both;">
            <input type="button" value="Save Current Rank Order" 
                id="save_<?php echo $selectedGroupId . '_' . $round . '_' . $periodId; ?>" class="save" />
            <?php
            $revertHref = 'rankApplications.php?departmentId=' . $departmentId;
            $revertHref .= '&round=' . $round . '&period=' . $periodId;
            $revertHref .= '&group=' . $selectedGroupId;
            ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="<? echo $revertHref . '&sort=lastSaved'; ?>" class="menu">Show Last Saved Rank Order</a>
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="<? echo $revertHref . '&sort=scoreRank'; ?>" class="menu">Order by Committee Scores</a>
        </div>
        <br>
        <div style="vertical-align: top; margin: 5px 10px 10px 20px; clear: both;">
            <div id="lastSavedInfo">
            <?php
            if ($sort == 'lastSaved') {
                echo 'Last Saved: ';
                if ($commentDate) {
                    echo $commentDate . ', ' . $coordinatorName;    
                } else {
                    echo 'N/A';
                }
                echo '<br><br>';
            }
            ?>
            </div>
            Comment: 
            <br>
            <textarea id="groupRankComment_<?php echo $selectedGroupId; ?>" name="groupRankComment_<?php echo $selectedGroupId; ?>" 
                cols="40" rows="3"><?php echo htmlspecialchars($coordinatorComment); ?></textarea>
        </div>
        <div class="groupMembers" id="groupMembers_<?php echo $selectedGroupId; ?>" style="clear: both;">
        <ol class="sortable" style="font-size: 10pt; cursor: pointer;">
        <?php
        foreach ($displayGroupMembers as $groupMember) {
            echo makeLi($groupMember);
        }
        ?>
        </ol>
        </div>
    </div>

</div>

<div style="clear: both;"></div>
<br><br><br>