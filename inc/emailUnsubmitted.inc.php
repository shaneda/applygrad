<?php
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php'; 
include '../classes/DB_Applyweb/class.DB_Unit.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php'; 
include '../classes/class.Unit.php';
include '../classes/class.Period.php';

$unitQuery = "SELECT unit_id FROM domain_unit 
                INNER JOIN lu_domain_department ON domain_unit.domain_id = lu_domain_department.domain_id
                WHERE lu_domain_department.department_id = " . $deptId;
$unitResult = mysql_query($unitQuery);
$activePeriodIds = array();
if ( !isset($periodTypeId) ) {
    $periodTypeId = 2;   
}
while ( $row = mysql_fetch_array($unitResult) ) {
    $unitId = $row['unit_id'];
    $unit = new Unit($unitId); 
    // There should only be one active period per unit.
    $unitActivePeriods = $unit->getActivePeriods($periodTypeId);
    foreach ($unitActivePeriods as $unitActivePeriod) {
        $unitActivePeriodId = $unitActivePeriod->getId();
        if ( !in_array($unitActivePeriodId, $activePeriodIds) ) {
            $activePeriodIds[] = $unitActivePeriodId;   
        }                
    }
}
?>
