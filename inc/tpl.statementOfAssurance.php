<div style="font-size: 7pt;">

<p>
Carnegie Mellon University does not discriminate in admission, 
employment, or administration of its programs or activities on the basis 
of race, color, national origin, sex, handicap or disability, age, 
sexual orientation, gender identity, religion, creed, ancestry, belief, 
veteran status, or genetic information.  Furthermore, Carnegie Mellon 
University does not discriminate and is required not to discriminate in 
violation of federal, state, or local laws or executive orders.
</p>

<p>
Inquiries concerning the application of and compliance with this 
statement should be directed to the vice president for campus affairs, 
Carnegie Mellon University, 5000 Forbes Avenue, Pittsburgh, PA 15213, 
telephone 412-268-2056.
</p>

<p> 
Obtain general information about Carnegie Mellon University by calling 
412-268-2000.
</p>

<p>
Carnegie Mellon University publishes an annual campus security and fire
safety report describing the university's security, alcohol and drug,
sexual assault, and fire safety policies and containing statistics about
the number and type of crimes committed on the campus and the number and
cause of fires in campus residence facilities during the preceding three
years. You can obtain a copy by contacting the Carnegie Mellon Police
Department at 412-268-2323. The annual security and fire safety report
is also available online at 
<a href="http://www.cmu.edu/police/annualreports/" style="font-size: 7pt;">http://www.cmu.edu/police/annualreports/</a>.
</p>

</div>