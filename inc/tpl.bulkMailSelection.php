<?php
include CLASS_DIR . 'DB_Applyweb/class.DB_Domain.php'; 
include CLASS_DIR . 'DB_Applyweb/class.DB_Department.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_VW_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminList.php';  
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListBase.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListDomain.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListPeriod.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListRecommendationCount.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListGreSubjectCount.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListToeflCount.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListIeltsCount.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListGmatCount.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListFees.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListPrograms.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminListNoteCount.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_Programs.php';
include CLASS_DIR . 'class.BulkMailApplicantListData.php';
include CLASS_DIR . 'Structures_DataGrid/class.AdminList_DataGrid.php';
include CLASS_DIR . 'Structures_DataGrid/class.BulkMailApplicantList_DataGrid.php';  
include CLASS_DIR . 'class.Department.php'; 

$datagridDataClass = new BulkMailApplicantListData($unit, $unitId, $allUsersSelected, $selectedUsers);
$datagridData = $datagridDataClass->getData($periodId, 
    NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL);
$datagrid = new BulkMailApplicantList_DataGrid($datagridData, NULL, NULL, $allUsersSelected); 
?>

<form name="selectionForm" id="selectionForm" action="" method="post">
<input type="hidden" name="allUsersSelected" id="allUsersSelected" value="0" /> 

<div style="clear: both; float: right;">
Mail Template: 
<?php
echo templateMenu($unit, $unitId, $contentId);
?>
<input type="submit" name="selectionSubmit" value="Send Mail to Selected Users">
</div>

<br><br>
<div style="clear: both;">
<?php $datagrid->render(); ?>
</div>

</form>

<?php
function templateMenu($unit, $unitId, $contentId)
{
    $menuSelections = array();
    $mailTemplates = getMailTemplates($unit, $unitId);
    foreach($mailTemplates as $mailTemplate)
    {
        $functionName = getFunctionName($mailTemplate['id']);
        //if (function_exists($functionName))
        //{
            $menuSelections[] = array($mailTemplate['id'], $mailTemplate['name']);
        //}    
    }
    
    return showEditText($contentId, "listbox", "contentId", $_SESSION['A_allow_admin_edit'], true, $menuSelections, true);
}
?>