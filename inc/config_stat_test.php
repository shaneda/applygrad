<?php
if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
{
//header("Location: ../apply/offline.php");
}
  
/*
CONSTANT VALUES FOR THE APPLICATION
*/
$environment="test";

//  $db_host="localhost";
$db_host="web05.srv.cs.cmu.edu";
$db_username="remote";
$db_password="r3m0t3us3r";
$db="gradAdmissionsTest";

//$datafileroot = "../../data/".date("Y");
$datafileroot = "../data/2009";

$dontsendemail = TRUE; 
$admissionsContact = "scsstats+admissions@cs.cmu.edu";
$paymentEmail = "scsstats+payment@cs.cmu.edu";
$supportEmail = "scsstats+technical@cs.cmu.edu";

//$paymentProcessor = "https://ccard-submit.as.cmu.edu/cgi-bin/gather_info.cgi"; //PRODUCTION USING GENERIC COLLECTOR
//$paymentProcessor = "https://ccard-submit-test.as.cmu.edu/cgi-bin/submit_cc.cgi"; //PRODUCTION DIRECT POST
// $paymentProcessor = "https://ccard-submit-test.as.cmu.edu/cgi-bin/gather_info.cgi"; //TESTING USING GENERIC COLLECTOR
$paymentProcessor = "https://train.cashnet.com/";

if ( strpos( phpversion(),'ubuntu' ) !== FALSE)
{
    if ($hostname == "WEB28.SRV.CS.CMU.EDU") {
        $magic_location = "/usr0/apache2/_dev/inc/magic-php-5.3";
    } else {
    //$magic_location = "/usr/share/file/magic-php-5.3"; 
    $magic_location = "/usr0/apache2/webapps/Applygrad/www/inc/magic-php-5.3";
    }
//    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_STRICT);    
}
elseif ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);    
} else {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic";
    error_reporting(E_ALL ^ E_NOTICE);    
}


ini_set('display_errors', 1);
?>