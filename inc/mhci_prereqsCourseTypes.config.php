<?php

// 3D array indexed by prereq type, course type.
$prereqCourseTypes = array(

    "statistics" => array(
    
        "Multi-way ANOVA" => array(
            "datafileinfoSectionCode" => 11,
            "heading" => "Course that covers multi-way ANOVA",
            "intro" => <<<EOB
            If you have taken a course that covers multi-way ANOVA, please list it here 
            and upload a syllabus. Otherwise, you can skip this section.
EOB
            ,
            "renderUploadNote" => TRUE
            ),
         
         "Multi-factor regression" => array(
            "datafileinfoSectionCode" => 12,
            "heading" => "Course that covers multi-factor regression",

            "intro" => <<<EOB
            If you have taken a course that covers multi-factor regression, please list it here 
            and upload a syllabus. Otherwise, you can skip this section.
EOB
            ,
            "renderUploadNote" => TRUE
            ),  
        
        "Single-way ANOVA" => array(
            "datafileinfoSectionCode" => 13,        
            "heading" => "Course that covers single-way ANOVA",
            "intro" => <<<EOB
            If you have taken a course that covers single-way ANOVA, please list it here 
            and upload a syllabus. Otherwise, you can skip this section.
EOB
            ,
            "renderUploadNote" => TRUE
            ), 
                                                                        
        "Single-factor regression" => array(        
            "datafileinfoSectionCode" => 14,
            "heading" => "Course that covers single-factor regression",
            "intro" => <<<EOB
            If you have taken a course that covers single-factor regression, please list it here 
            and upload a syllabus. Otherwise, you can skip this section.
EOB
            ,
            "renderUploadNote" => TRUE
            )
    
        ),

    "design" => array(
        
        "Design" => array(       
            "datafileinfoSectionCode" => 15,
            "heading" => "Design Course",
            "intro" => <<<EOB
            If you have taken courses in a Fine Arts or Applied Arts program that cover 
            the visual and verbal vocabulary of communication design, the design process, 
            and the communicative value of text and image, please list them here. 
            Otherwise, you can skip this section. Courses in Computer Science do not 
            fulfill this prerequisite.
EOB
            ,
            "renderUploadNote" => FALSE
            )
        
        ),
    
    "programming" => array(
    
        "Programming" => array(
            "datafileinfoSectionCode" => 16,
            "heading" => "Programming Course",
            "intro" => <<<EOB
            If you have taken courses that cover a programming language such as Java or C++, 
            programming methodology and style, problem analysis, data abstraction, 
            and dynamic data, please list them here. Otherwise, you can skip this section.
EOB
            ,
            "renderUploadNote" => FALSE
            )
        
        )

    );

  
    /*
    * 2D array indexed by individual course type, e.g.,
    * 
    *   $courseTypes = array (
    *       "Multi-way ANOVA" => array(
    *           "datafileinfoSectionCode" => 11,
    *           "prereqType" => "statistics" ...
    *       )...
    *   );    
    */
    $courseTypes = array();
    foreach ($prereqCourseTypes as $prereqType => $prereqTypeArray) {
        
        foreach ($prereqTypeArray as $courseType => $courseTypeArray) {
            
            $courseTypeArray['prereqType'] = $prereqType;
            $courseTypes[$courseType] = $courseTypeArray;
            
        }   
        
    }

?>
