<?php
/*
* Functions used by review/reviewApplications.php 
*/

//
// group functions
//
function getDepartmentName($department_id)
{
    $query = "SELECT name FROM department where id= $department_id";
    $result = mysql_query($query) or die(mysql_error());
    $department_name = "";
    while($row = mysql_fetch_array( $result ))
    {
        $department_name = $row['name'];
    }
    return $department_name;
}

function getAllGroups( $departmentId, $round, $periodId )
{
    $query = "SELECT DISTINCT rg.name 
        FROM revgroup rg 
        INNER JOIN lu_application_groups lag
        ON lag.group_id = rg.id
        INNER JOIN period_application
        ON lag.application_id = period_application.application_id 
        WHERE rg.department_id = " . $departmentId . "
        AND lag.round = " . $round;
    if (is_array($periodId)) {
        $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
        $query .= " AND period_application.period_id IN " . $admissionPeriodIds;
    } else {
        $query .= " AND period_application.period_id = " . $periodId;   
    }   
    $query .= " ORDER BY name";
    $result = mysql_query( $query ) or die(mysql_error());
    
    $allGroups = array();
    while($row = mysql_fetch_array( $result ))
    {
        array_push($allGroups, $row['name']);
    }
    return $allGroups;
}

function getMyGroups( $reviewerId, $departmentId, $round, $periodId )
{
    //get my groups
    $query = "SELECT DISTINCT rg.name 
        FROM revgroup rg
        INNER JOIN lu_reviewer_groups lrg 
        ON lrg.group_id = rg.id
        INNER JOIN lu_application_groups lag
        ON lag.group_id = rg.id
        INNER JOIN period_application
        ON lag.application_id = period_application.application_id 
        WHERE rg.department_id = " . $departmentId . "
        AND lrg.reviewer_id = " . $reviewerId . "
        AND lrg.round = " . $round;
    if (is_array($periodId)) {
        $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
        $query .= " AND period_application.period_id IN " . $admissionPeriodIds;
    } else {
        $query .= " AND period_application.period_id = " . $periodId;   
    }   
    $query .= " ORDER BY rg.name";

    $result = mysql_query( $query ) or die(mysql_error());
    $myGroups = array();
    while($row = mysql_fetch_array( $result ))
    {
        array_push( $myGroups, $row['name']);
    }
    return $myGroups;
}


//
// filter setup functions
//
function  createGroupFilterSelect( $groups, $reviewerId, $departmentId, $round, $periodId, $columns )
{   
    // get groups col num
    $count = 0;
    foreach ( $columns as $col )
    {
        $columnName = $col->columnName;
        //if ( $columnName == "Groups" )
        if ( strstr($columnName, "Groups") !== FALSE )
        {
            $columnNum = $count;
        }    
        $count++;
    }
        
    if ( $groups == "myGroups" )
    {
        $groupNames = getMyGroups( $reviewerId, $departmentId, $round, $periodId );    
    } else {
        $groupNames = getAllGroups( $departmentId, $round, $periodId );
    }    
    
    // $columnNum will not be set when the groups column is deselected,
    // in which case the groupFilter selet should not be displayed.
    if ((count($groupNames) > 1 ) && isset($columnNum))
    {
        $select = "<select name='groupFilter' id='groupFilter' onchange='return filterGroups( this, $columnNum )'>";
        $select .= "<option value='-1'>All</option>";
        foreach ( $groupNames as $name )
        {
            $select .= "<option value='$name'>$name</option>";
        }
        $select .= "</select>";
    } 
    // only one group so no need to filter
    else 
    {
        $select = "";
    }
    return $select;
}

function setFilterColumns($columns)
{
    $count = 0;
    $filterColumns = "<option value='-1'>All Fields</option>";
    foreach ( $columns as $col )
    {
        $columnName = $col->columnName;
        if ( $columnName != "Edit" && $columnName != "" )
        {
            $filterColumns .= "<option value='$count'>$columnName</option>";
            //$count++;
        }
        $count++;    
    }
    return $filterColumns;
}

//
// semi-blind functions
//
function getDefaultSemiblind( $department_id )
{
    $query = "SELECT semiblind_review FROM department WHERE id = $department_id";
    $result = mysql_query($query) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        $semiblind_review = intval($row['semiblind_review']);
    }
    return $semiblind_review;
}

// Create hidden form inputs for the selected columns.
function makeHiddenShowColumns( $showColumns ) {
    
    foreach ($showColumns as $field) {
        echo '<input type="hidden" name="showColumns[]" value="' . $field . '"/>';
        echo "\n";
    }
    
}

function getUnitName($unit, $unitId)
{
    switch ($unit)
    {
        case 'domain':
        
            return getDomainName($unitId);
            break;
        
        case 'department':
        
            return getDepartmentName($unitId);
            break;

        case 'program':
        
            return getProgramName($unitId);
            break;
        
        default:
        
            return null;
    }
}

function getDomainName($domainId)
{
    $query = "SELECT name FROM domain WHERE id = " . intval($domainId);
    $result = mysql_query($query);
    $domainName = "";
    while($row = mysql_fetch_array( $result ))
    {
        $domainName = $row['name'];
    }
    return $domainName;
}

function getProgramName($programId)
{
    $query = "SELECT CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) 
        FROM program 
        INNER JOIN degree ON degree.id = programs.degree_id
        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
        WHERE id = " . intval($programId);
    $result = mysql_query($query);
    $domainName = "";
    while($row = mysql_fetch_array( $result ))
    {
        $domainName = $row['name'];
    }
    return $domainName;
}

function isValidUnit($unit, $unitId, $roleDepartmentId)
{
    switch ($unit)
    {
        case 'domain':
        
            return isValidDomainDepartment($unitId, $roleDepartmentId);
            break;
        
        case 'department':
        
            return $unitId == $roleDepartmentId;
            break;

        case 'program':
        
            return isValidProgramDepartment($unitId, $roleDepartmentId);
            break;
        
        default:
        
            return false;
    }    
}

function isValidDomainDepartment($domainId, $departmentId)
{
    $query = "SELECT id FROM lu_domain_department
        WHERE domain_id = " . intval($domainId) . "
        AND department_id = " . intval($departmentId);
    
    $result = mysql_query($query);
    
    if ($result && mysql_num_rows($result) > 0)
    {
        return true;    
    }
    
    return false;
}

function isValidProgramDepartment($programId, $departmentId)
{
    $query = "SELECT id FROM lu_programs_departments
        WHERE program_id = " . intval($programId) . "
        AND department_id = " . intval($departmentId);
    
    $result = mysql_query($query);
    
    if ($result && mysql_num_rows($result) > 0)
    {
        return true;    
    }
    
    return false;
}
?>