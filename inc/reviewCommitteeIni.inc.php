<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$ratingValues = array(
array(1, "1"),
array(2, "2"),
array(3, "3"),
array(4, "4"),
array(5, "5")
);

$ratingValues2 = array(
array(1, "1"),
array(2, "2"),
array(3, "3"),
array(4, "4"),
array(5, "5 Excellent &nbsp;&nbsp;"),
array(6, "[Reset]")
);

$alternativeProgramValues = array(
array(1, "MSIN"),
array(2, "MSIS"),
array(3, "MSIT-MOB"),
array(4, "MSIT-IS"),
array(5, "MSIT-SM"),
array(6, "[Reset]")
);
?> 

<input type="hidden" name="departmentId" value="<?php echo $departmentId; ?>" />
<input type="hidden" name="round" value="<?php echo $round; ?>" />
<input type="hidden" name="applicationId" value="<?php echo $applicationId; ?>" />
<input type="hidden" name="iniReviewId" value="<?php echo $iniReviewId; ?>" />

<div style="font-weight: bold; color: red;"><?php echo $saveError; ?></div>
<div style="font-weight: bold; color: green;">
<?php 
if (isset($_POST['btnSubmit']) && !$saveError)
{
    echo 'Review saved';    
}
?>
</div>

<p>
Overall Rating Scale: 
<br/>5= without a doubt admit, well-qualified applicant 
<br/>4= fulfills all the criteria, qualified applicant 
<br/>3= minimally qualified, top of the waitlist 
<br/>2= fulfills only some of the criteria, not fully qualified 
<br/>1= reject, does not fulfill the criteria</p>

<p>
Rating scale for experience areas: 
<br/>5= very good or excellent experience 
<br/>4= good 
<br/>3= average experience 
<br/>2= below average or minimal/weak experience 
<br/>1= n/a, no experience
</p>

<p style="color: red;">
* Required
</p>

<div>
    <div style="font-weight: bold; background-color: #eee; padding: 2px;">Technical Preparation</div>
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Technical Strengths and/or Weaknesses</span>
        <span style="color: red;">*</span>
        <br/>
        <span>Please list technical strengths and/or weaknesses relevant to the application:</span>
        <br/>
        <?php 
        ob_start();
        showEditText($technicalComments, "textarea", "technicalComments", $allowEdit, false, 80); 
        $technicalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $technicalCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Rating for Technical Preparation</span>
        <span style="color: red;">*</span>
        <br/>
        <span>Please rate the applicant's overall technical preparation:</span>
        <br/>
        Poor
        <?php 
        showEditText($technicalRating, "radiogrouphoriz2", "technicalRating", $allowEdit, 
            false, $ratingValues); 
        ?>
        Excellent
    </div>    
</div>

<br/>
<div>
    <div style="font-weight: bold; background-color: #eee; padding: 2px;">Academic Ability</div>
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Academic Strengths and/or Weaknesses</span>
        <span style="color: red;">*</span>
        <br/>
        <span>Please list academic strengths and/or weaknesses relevant to the application:</span>
        <br/>
        <?php 
        ob_start();
        showEditText($academicComments, "textarea", "academicComments", $allowEdit, false, 80); 
        $academicCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $academicCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">UG Academic Program</span>
        <span style="color: red;">*</span>
        <br/>
        <span>Does the UG program adequately prepare the applicant? Why or why not ? Please be specific:</span>
        <br/>
        <?php 
        ob_start();
        showEditText($ugProgramComments, "textarea", "ugProgramComments", $allowEdit, false, 80); 
        $ugProgramCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $ugProgramCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Rating for Academic Preparation</span>
        <span style="color: red;">*</span>
        <br/>
        <span>Please rate the applicant's overall academic preparation:</span>
        <br/>
        Poor
        <?php 
        showEditText($academicRating, "radiogrouphoriz2", "academicRating", $allowEdit, 
            false, $ratingValues); 
        ?>
        Excellent
    </div>    
</div>

<br/>
<div>
    <div style="font-weight: bold; background-color: #eee; padding: 2px;">Experience</div>
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Research and/or Project Work</span>
        <br/>
        <span>Has the applicant completed or engaged in research/project work relevant to the 
        program being applied for? Please explain (leave blank if N/A):</span>
        <br/>
        <?php 
        ob_start();
        showEditText($researchComments, "textarea", "researchComments", $allowEdit, false, 80); 
        $researchCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $researchCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Research/Project Work Rating</span>
        <br/>
        <span>Please rate the quality of the applicant's research/project work (leave blank if N/A):</span>
        <br/>
        Poor
        <?php 
        showEditText($researchRating, "radiogrouphoriz2", "researchRating", $allowEdit, 
            false, $ratingValues2); 
        ?>
    </div>  
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Work Experience</span>
        <br/>
        <span>Has the applicant acquired work experience that is relevant to the 
        program being applied for? Please explain (leave blank if N/A):</span>
        <br/>
        <?php 
        ob_start();
        showEditText($workExperienceComments, "textarea", "workExperienceComments", $allowEdit, false, 80); 
        $workExperienceCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $workExperienceCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Work Experience Rating</span>
        <br/>
        <span>Please rate the quality/relevance of the applicant's work experience (leave blank if N/A):</span>
        <br/>
        Poor
        <?php 
        showEditText($workExperienceRating, "radiogrouphoriz2", "workExperienceRating", $allowEdit, 
            false, $ratingValues2); 
        ?>
    </div>   
</div>

<br/>
<div>
    <div style="font-weight: bold; background-color: #eee; padding: 2px;">Leadership</div>
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Leadership Ability</span>
        <br/>
        <span>From what you have read and know about this applicant, through their academic ability, 
        professional experience, and/or any other related extracurricular activities, please identify any 
        leadership skills that you feel this applicant would bring to INI that would enhance and add to our 
        knowledge base, research, and reputation (leave blank if N/A):</span>
        <br/>
        <?php 
        ob_start();
        showEditText($leadershipComments, "textarea", "leadershipComments", $allowEdit, false, 80); 
        $leadershipCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $leadershipCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Leadership Ability Rating</span>
        <br/>
        <span>Please rate the applicant's leadership ability (leave blank if N/A):</span>
        <br/>
        Poor
        <?php 
        showEditText($leadershipRating, "radiogrouphoriz2", "leadershipRating", $allowEdit, 
            false, $ratingValues2); 
        ?>
    </div>    
</div>

<br/>
<div>
    <div style="font-weight: bold; background-color: #eee; padding: 2px;">Overall</div>
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Additional Comments</span>
        <br/>
        <span>Are there aspects of this applicant that you would like to comment on that support your 
        recommendation either in favor or against admitting this student?</span>
        <br/>
        <?php 
        ob_start();
        showEditText($additionalComments, "textarea", "additionalComments", $allowEdit, false, 80); 
        $additionalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $additionalCommentsTextarea); 
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Recommendation</span>
        <span style="color: red;">*</span>
        <br/>
        <span>Do you recommend this applicant for admission to INI?</span>
        <br/>
        Poor
        <?php 
        showEditText($overallRating, "radiogrouphoriz2", "overallRating", $allowEdit, 
            false, $ratingValues); 
        ?>
        Excellent
    </div>
    
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Alternative Program</span>
        <br/>
        <span>Is the applicant more appropriate for a different program 
        than the one being applied for? If so, which program?</span>
        <br/>
        <?php 
        ob_start();
        showEditText($alternativeProgram, "radiogrouphoriz2", "alternativeProgram", $allowEdit, 
            false, $alternativeProgramValues);
        $alternativeProgramRadioButtons = ob_get_contents(); 
        ob_end_clean();
        echo str_replace("<input", "<br/><input", $alternativeProgramRadioButtons);
        ?>
        
        <br/>
        <br/>
        <span style="font-weight: bold;">Alternative Program Explained</span>
        <br/>
        <span>Please elaborate on your Alternative Program recommendation above:</span>
        <br/>
        <?php 
        ob_start();
        showEditText($alternativeProgramComments, "textarea", "alternativeProgramComments", $allowEdit, false, 80); 
        $alternativeProgramCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $alternativeProgramCommentsTextarea); 
        ?>
    </div>    
</div>

<div style="margin-top: 10px;">
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
