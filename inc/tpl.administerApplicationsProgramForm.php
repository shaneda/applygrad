$programsView = new VW_Programs();
$unitPrograms = $programsView->find($unit, $unitId);
if (count($unitPrograms) > 1) {
?>

<form id="programForm" name="programForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
    Limit to program: <select name="programId" onChange="document.programForm.submit();">
    <option value="">All <?php echo $unitName; ?> programs</option>
<?php
    foreach ($unitPrograms as $program) {
        $selected = '';
        if ($program['program_id'] == $programId) {
            $selected = 'selected';
        }
        echo '<option value="' . $program['program_id'] . '" ' . $selected . '>' . $program['program_name'] . '</option>';    
    }
?>
    </select>
    <input type="hidden" id="programLimit" name="limit" value="<?php echo $limit; ?>" /> 
    <input type="hidden" id="limitUnit" name="unit" value="<?php echo $unit; ?>" />
    <input type="hidden" id="limitUnitId" name="unitId" value="<?php echo $unitId; ?>" />
    <?php
    if ($periodId) {      
        // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" id="limitPeriod" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" id="limitPeriod" name="period" value="' . $periodId . '"/>';     
        } 
    }
    ?>
    <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
    <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
    <input type="hidden" name="submissionStatus" value="<?php echo $submissionStatus; ?>"/>
    <input type="hidden" name="completionStatus" value="<?php echo $completionStatus; ?>"/>
    <input type="hidden" name="paymentStatus" value="<?php echo $paymentStatus; ?>"/>
    <input type="hidden" name="testScoreStatus" value="<?php echo $testScoreStatus; ?>"/>
    <input type="hidden" name="transcriptStatus" value="<?php echo $transcriptStatus; ?>"/>
    <input type="hidden" name="recommendationStatus" value="<?php echo $recommendationStatus; ?>"/>
    <input type="hidden" name="showReturnLink" value="<?php echo $showReturnLink; ?>"/>
</form>