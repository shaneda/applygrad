<?php
// Initialize variables
$dietrichFinancialQualifiedAssistance = NULL;
$dietrichFinancialReceivedLoans = NULL;
$dietrichFinancialReceivedScholarships = NULL;
$dietrichFinancialSupportSources = NULL;
$dietrichFinancialInterestedB2Trainig = NULL;
$dietrichFinancialError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveDietrichFinancial();
    checkRequirementsDietrichFinancial();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$dietrichFinancialError)
{
    $dietrichFinancialQuery = "SELECT qualified_assistance, received_loans, 
        received_scholarships, support_sources, interested_b2_training 
        FROM dietrich_financial_support
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $dietrichFinancialResult = mysql_query($dietrichFinancialQuery);
    while($row = mysql_fetch_array($dietrichFinancialResult))
    {
        $dietrichFinancialQualifiedAssistance = $row['qualified_assistance'];
        $dietrichFinancialReceivedLoans = $row['received_loans']; 
        $dietrichFinancialReceivedScholarships = $row['received_scholarships'];
        $dietrichFinancialSupportSources = $row['support_sources'];
        $dietrichFinancialInterestedB2Trainig = $row['interested_b2_training'];          
    }    
} 
?>

<span class="subtitle">Financial Support</span>
<br/>
<br/>
Please check all that apply:
<br/>
<?php
showEditText($dietrichFinancialQualifiedAssistance, "checkbox", "dietrichFinancialQualifiedAssistance", $_SESSION['allow_edit'], false);
?>
Have qualified for federal disadvantaged assistance
<br/>
<?php
showEditText($dietrichFinancialReceivedLoans, "checkbox", "dietrichFinancialReceivedLoans", $_SESSION['allow_edit'], false);
?>
Have received any of the following loans: Health Professional Student Loans (HPSL), Loans for Disadvantaged Student Program
<br/>
<?php
showEditText($dietrichFinancialReceivedScholarships, "checkbox", "dietrichFinancialReceivedScholarships", $_SESSION['allow_edit'], false);
?>
Have received scholarships from the U.S. Department of Health and Human Services under the Scholarship for Individuals with Exceptional Financial Need

<br/>
<br/>
Please list sources of financial support for your graduate study (other than personal of family assets).
Please give source, amount, duration and status (current or pending). Admission is not contingent
on this information.
<?php 
showEditText($dietrichFinancialSupportSources, "textarea", "dietrichFinancialSupportSources", $_SESSION['allow_edit']); 
?>

<br/>
<br/>
Among the departmental sources of support are training grants that have some special curricular requirements 
but do not require an extra year of academic work. Information about these training grants can be found at the 
websites listed below. If you are interested in this program, please check the box below:
<br/>
<?php
showEditText($dietrichFinancialInterestedB2Trainig, "checkbox", "dietrichFinancialInterestedB2Trainig", $_SESSION['allow_edit'], false);
?>
Behavioral Brain Research Training Program
<br/>URL:  
<a href="http://www.psy.cmu.edu/training/grantbehavioralbrainresearch.html">http://www.psy.cmu.edu/home/grantbehavioralbrainresearch.html</a>

<br/>
<hr size="1" noshade color="#990000">

<?php
function saveDietrichFinancial()
{
    global $dietrichFinancialQualifiedAssistance;
    global $dietrichFinancialReceivedLoans;
    global $dietrichFinancialReceivedScholarships;
    global $dietrichFinancialSupportSources;
    global $dietrichFinancialInterestedB2Trainig;
    global $dietrichFinancialError;

    $dietrichFinancialQualifiedAssistance = filter_input(INPUT_POST, 'dietrichFinancialQualifiedAssistance', FILTER_VALIDATE_BOOLEAN);
    $dietrichFinancialReceivedLoans = filter_input(INPUT_POST, 'dietrichFinancialReceivedLoans', FILTER_VALIDATE_BOOLEAN);
    $dietrichFinancialReceivedScholarships = filter_input(INPUT_POST, 'dietrichFinancialReceivedScholarships', FILTER_VALIDATE_BOOLEAN);
    $dietrichFinancialSupportSources = filter_input(INPUT_POST, 'dietrichFinancialSupportSources', FILTER_SANITIZE_STRING);
    $dietrichFinancialInterestedB2Trainig = filter_input(INPUT_POST, 'dietrichFinancialInterestedB2Trainig', FILTER_VALIDATE_BOOLEAN);
    
    // Check for existing record
    $existingRecordQuery = "SELECT id FROM dietrich_financial_support WHERE application_id = " . intval($_SESSION['appid']);
    $existingRecordResult = mysql_query($existingRecordQuery);
    if (mysql_num_rows($existingRecordResult) > 0)
    {
        // Update existing record
        $updateQuery = "UPDATE dietrich_financial_support SET
            qualified_assistance = " . intval($dietrichFinancialQualifiedAssistance) . ",
            received_loans = " . intval($dietrichFinancialReceivedLoans) . ",
            received_scholarships = " . intval($dietrichFinancialReceivedScholarships) . ",
            support_sources = '" . mysql_real_escape_string($dietrichFinancialSupportSources) . "',
            interested_b2_training = " . intval($dietrichFinancialInterestedB2Trainig) . "
            WHERE application_id = " . intval($_SESSION['appid']);
        mysql_query($updateQuery);
    }
    else
    {
        // Insert new record
        $insertQuery = "INSERT INTO dietrich_financial_support 
            (application_id, qualified_assistance, received_loans, received_scholarships,
                support_sources, interested_b2_training)
            VALUES (" 
            . intval($_SESSION['appid']) . "," 
            . intval($dietrichFinancialQualifiedAssistance) . ","
            . intval($dietrichFinancialReceivedLoans) . ","
            . intval($dietrichFinancialReceivedScholarships) . ",'"
            . mysql_real_escape_string($dietrichFinancialSupportSources) . "',"
            . intval($dietrichFinancialInterestedB2Trainig) . ")";
        mysql_query($insertQuery);
    }
}

function checkRequirementsDietrichFinancial()
{
    global $err;
    global $dietrichFinancialError;     
    
    if (!$err && !$dietrichFinancialError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>