<?php
if ($hideApplication) {
    $hideChecked = 'checked';
    $message = 'Hidden<br/>';
} else {
    $hideChecked = '';
    $message = '';    
}
?>
<div style="text-align: right; float: left; width: 40%;">
<form id="notes_<?php echo $applicationId; ?>">
    <div>
    Hide this application
    <input type="checkbox" class="hideApplication" 
        id="hideApplication_<?php echo $applicationId; ?>" <?php echo $hideChecked; ?> /> 
    </div>
    <br/>
    <textarea id="note_<?php echo $applicationId; ?>" cols="35" rows="6"></textarea>
    <br/><br/>
    <input type="button" class="saveNote" id="saveNote_<?php echo $applicationId; ?>" value="Save Note" />
</form>
</div>

<div style="margin-left: 10px; text-align: left; float: left; width: 50%;">
<ul>
<?php
$i = 0;
foreach ($noteRecords as $noteRecord) {
    echo '<li style="margin-top: 5px;">' . $noteRecord['note'] . '<br>';
    echo '<i>' . date( 'm/d/Y h:i A', strtotime($noteRecord['insert_time']) );
    echo ' - ' . $noteRecord['insert_user_name'] . '</i></li>';
    $i++;
}
$message .= $i . ' ';
if ($i > 1) {
    $message .= 'notes';    
} else {
    $message .= 'note';
}  
?>
</ul>
</div>

<?php
echo <<<EOB
<script>
    $('#notesMessage_{$applicationId}').html('{$message}');    
</script>
EOB;
?>