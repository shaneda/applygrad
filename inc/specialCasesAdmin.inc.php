<?php
include_once '../inc/db_connect.php';
include_once '../inc/specialCasesDomains.inc.php';

function isDomainDepartment($departmentId, $domainNames)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name IN ('" . implode("','", $domainNames) . "')
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;    
}

function isIsreeDepartment($departmentId)
{
     /*
    $isreeDomains = array(
        'Advanced-Big-Data',
        'Advanced-Enterprise-Architecture',
        'Applied-Data-Analytics',
        'Architectures-for-Mergers',
        'Big-Data-Systems',
        'Challenge-Exam-EAF',
        'COTS-Based-Integration',
        'Cybersecurity-Architectures',
        'Cybersecurity-Fundamentals',
        'EA-Security',
        'Enterprise-Architecture-Fundamentals',
        'Leadership-Cyber-Enabled-World',
        'Managing-Software-Outsourcing',
        'Principles-of-Architecture-Design',
        'Requirements-Engineering',
        'Security-for-Software-Engineers',
        'Software-Architecture',
        'Software-Project-Management',
        'Systems-and-Software-Security',
        'Systems-Integration' 
    );
    
 */
    $isree_departments = array();
    $query = "select lpd.department_id 
                from program p
                inner join programs_unit pu on pu.unit_id = p.unit_id
                inner join lu_programs_departments lpd on lpd.program_id = pu.programs_id and lpd.department_id NOT IN (51, 98, 87)
                where p.program_type_id = 4";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        array_push($isree_departments, $row['department_id']);
    }
    
    return in_array($departmentId, $isree_departments);
}

function isMseMsitDepartment($departmentId)
{
    $mseMsitDomains = array(
        'MITS',
        'MSE-Campus',
        'MSE-Dist',
        'MSE-MSIT-Korea',
        'MSE-Portugal',
        'MSE-SEM',
        'MSIT-eBiz',
        'MSIT-eBiz-US',
        'MSIT-GM-Dist-INTL',
        'MSIT-GM-Dist-NATL',
        'MSIT-ITSM',
        'MSIT-SASE',
        'MSIT-SE-Russia',
        'MSIT-SRM',
        'MSIT-ESE',
        'MSIT-GM'
    );
        
    return isDomainDepartment($departmentId, $mseMsitDomains);
}

function isMseMsitEbizDepartment($departmentId)
{
    $mseMsitEbizDomains = array(
        'MSIT-eBiz',
        'MSIT-eBiz-US'
    );
        
    return isDomainDepartment($departmentId, $mseMsitEbizDomains);
}

function isMseESE($departmentId) 
{
    if (isMseMsitDepartment($departmentId) && $departmentId == 44)  
    {
        return TRUE;
    } 
    else 
    {
        return FALSE;
    }       
}

function isMseMITS($departmentId) 
{   
    if (isMseMsitDepartment($departmentId) && $departmentId == 82)  
    {
        return TRUE;
    } 
    else 
    {
        return FALSE;
    }       
}

function isMseCampus($departmentId) 
{   
    if (isMseMsitDepartment($departmentId) && $departmentId == 18)  
    {
        return TRUE;
    } 
    else 
    {
        return FALSE;
    }       
}

function isMsrtDepartment($departmentId)
{
    $msrtDomains = array(
        'RI-MS-RT-China',
        'RI-MS-RT-Dominican',
        'RI-MS-RT-Mexico',
        'RI-MS-RT-UK' 
    );
        
    return isDomainDepartment($departmentId, $msrtDomains);
}

function isMshciiDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'MS-HCII' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isIniDepartment($departmentId)
{
    $iniDomains = array(
        'INI',
        'INI-Kobe' 
    );
        
    return isDomainDepartment($departmentId, $iniDomains);
}

function isEm2Department($departmentId)
{
    $em2Domains = array(
        'EM2' 
    );
        
    return isDomainDepartment($departmentId, $em2Domains);
}

function isDietrichDepartment($departmentId)
{
    $dietrichDomains = array(
      'English-MA',
      'History',
      'Philosophy',
      'Psychology',
      'Social-and-Decision-Sciences',
      'English-PhD',
      'ModLang-MA',
      'ModLang-PhD'
    );
        
    return isDomainDepartment($departmentId, $dietrichDomains);
}

function isHistoryDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'History' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isPhilosophyDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'Philosophy' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isHistoryApplication($applicationId)
{ 
    $query = "SELECT department.name AS department_name
        FROM lu_application_programs
        INNER JOIN lu_programs_departments
            ON lu_application_programs.program_id = lu_programs_departments.program_id
        INNER JOIN department
            ON lu_programs_departments.department_id = department.id
        WHERE lu_application_programs.application_id = " . intval($applicationId);
    
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        if (trim($row['department_name']) == 'History') 
        {
            return TRUE;
        }
    }
    
    return FALSE;
} 

function isPsychologyDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'Psychology' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isScsDepartment($departmentId) 
{    
    $query = "SELECT department_id FROM lu_domain_department WHERE domain_id = 1"; 
    
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result)) 
    {
        if ($departmentId == $row['department_id']) 
        {
            return TRUE;
        }    
    }
    
    return FALSE;   
}

function isCsDepartment($departmentId)
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'Computer Science Department'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}


function isSocialDecisionSciencesDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'Social-and-Decision-Sciences' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}


function isSocialDecisionSciencesAdministrator ($deptId, $roleId) {
    $isSDS = isSocialDecisionSciencesDepartment($deptId);
    if ($roleId == 0 || $roleId == 1) {
        $isAdmin = TRUE;
    }
    else {
            $isAdmin = FALSE;
    }
    if ($isSDS && $isAdmin) {
        return TRUE;
    }
    else {
        return FALSE;
    } 
}

function isRissDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'RI-SCHOLARS' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isReuseDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'REU-SE' 
        AND lu_domain_department.department_id = " . intval($departmentId);  

    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

/*
function isEnglishDomain($domainId) 
{
    global $englishDomains;
    return isDomain($domainId, $englishDomains);
}
*/

function isEnglishPhdDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'English-PhD' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isEnglishMaDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'English-MA' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

/*
function isPhilosophyDomain($domainId) 
{
    global $philosophyDomains;
    return isDomain($domainId, $philosophyDomains);
}
*/

function isDesignDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'Design' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isDesignPhdDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'Design-PhD' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isDesignDdesDepartment($departmentId)
{
    $query = "SELECT domain.name 
        FROM lu_domain_department 
        INNER JOIN domain ON lu_domain_department.domain_id = domain.id
        WHERE domain.name = 'Design-DDes' 
        AND lu_domain_department.department_id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isRiDepartment($departmentId)
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'Robotics Institute'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;    
}

function isRiMastersDepartment($departmentId)
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'Robotics Institute-Masters'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;    
}

function isRiMsRtChinaDepartment($departmentId)
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'RI-MS-RT-China'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isRiMscvDepartment($departmentId)
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'RI-MSCV'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}

function isMsaii($departmentId) 
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'MSAII'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        

    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;       
}

function isPrivacy($departmentId) 
{
    $query = "SELECT department.id
        FROM department 
        WHERE department.name = 'MSIT-Privacy'
        AND department.id = " . intval($departmentId);  
    
    $result = mysql_query($query);        

    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;       
}
?>