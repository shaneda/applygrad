<?php
$greSubjectScoreId = $greSubjectRecord['id'];

$greSubjectDate = date("m/Y", strtotime($greSubjectRecord['testdate']));
$greSubjectName =  $greSubjectRecord['name'];
$greSubjectScore = $greSubjectRecord['score'];
$greSubjectPercentile = $greSubjectRecord['percentile'];

$update_gre_subject_score_submit_id = $greSubjectRecord['id'] . "_update_gre_subject_score_submit";

$receivedChecked = '';
$receivedMessage = '';
$receivedClass = 'confirm';
if ($greSubjectRecord['scorereceived'] == 1) {
    $receivedChecked = 'checked';
    $receivedMessage .= ' rcd';
    $receivedClass = 'confirmComplete';
}

echo <<<EOB

<form class="greSubjectScore" id="greSubjectScore_{$applicationId}"> 
<table cellpadding="2px" cellspacing="2px" border="0">
    <tr>
        <td colspan="2">GRE Subject&nbsp;&nbsp;
        <input type="checkbox" class="greSubjectReceived" 
            id="greSubjectReceived_{$applicationId}_{$greSubjectScoreId}" {$receivedChecked} /> Received
        </td>
    </tr>
    <tr>
        <td colspan="3">Test Date: {$greSubjectDate}</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td>Subject</td>
        <td>
        <input type="text" size="15" class="greSubjectName" 
            id="greSubjectName_{$applicationId}_{$greSubjectScoreId}" value="{$greSubjectName}">
        </td>
    </tr>
    <tr>
        <td>Score</td>
        <td>
        <input type="text" size="3" class="greSubjectScore" 
            id="greSubjectScore_{$applicationId}_{$greSubjectScoreId}" value="{$greSubjectScore}">
        </td>
    </tr>
    <tr>
        <td>Pctile</td>
        <td>
        <input type="text" size="3" class="greSubjectPercentile" 
            id="greSubjectPercentile_{$applicationId}_{$greSubjectScoreId}" value="{$greSubjectPercentile}">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <input type="submit" class="submitSmall updateGreSubjectScore" 
            id="updateGreSubjectScore_{$applicationId}_{$greSubjectScoreId}" value="Update Subject Score"/>
        </td>
    </tr>
</table> 
</form> 

<script>
    $('#greSubjectReceivedMessage_{$applicationId}').html('{$receivedMessage}');
    $('#greSubjectReceivedMessage_{$applicationId}').prev("span").attr('class', '{$receivedClass}');    
</script>

EOB;

?>