<?php
// Group data
$DB_Applyweb = new DB_Applyweb();
$groupQuery = "SELECT revgroup.id AS group_id,
    revgroup.name AS group_name
    FROM revgroup
    INNER JOIN lu_reviewer_groups
        ON revgroup.id = lu_reviewer_groups.group_id
        AND lu_reviewer_groups.round = " . $round . " 
    WHERE revgroup.group_type = 2 
    AND lu_reviewer_groups.reviewer_id = " . $luuId . "
    ORDER BY group_name";               
$groups = $DB_Applyweb->handleSelectQuery($groupQuery);

$menuDefaultGroupId = 0;
$viewMenuItems = array();
$i = 1;
foreach ($groups as $group) {
    $viewMenuItems[$group['group_name']] = $group['group_id'];
    if ($i == 1) {
        $menuDefaultGroupId = $group['group_id'];   
    } 
    $i++;
}

// Set the menu selection.
if (!in_array($requestGroupId, $viewMenuItems)) {
    $selectedGroupId = $menuDefaultGroupId;    
} else {
    $selectedGroupId = $requestGroupId;    
}
    
// Application data
$groupMemberQuery = "SELECT application.id AS application_id, 
    application.user_id AS luu_id,
    CONCAT(users.lastname, ', ', users.firstname) AS name, 
    rank,
    0 AS score_average,
    0 AS score_rank
    FROM application 
    INNER JOIN period_application 
        ON application.id = period_application.application_id
    INNER JOIN lu_application_groups 
        ON application.id = lu_application_groups.application_id
    INNER JOIN lu_users_usertypes
        ON application.user_id = lu_users_usertypes.id 
    INNER JOIN users
        ON lu_users_usertypes.user_id = users.id
    LEFT OUTER JOIN group_rank_member
        ON application.id = group_rank_member.application_id
        AND lu_application_groups.group_id = group_rank_member.group_id
    WHERE lu_application_groups.group_id = " . $selectedGroupId . "
    AND lu_application_groups.round = " . $round . "
    AND period_application.period_id = " . $periodId . "
    ORDER BY rank, name";
$groupMembers = $DB_Applyweb->handleSelectQuery($groupMemberQuery, 'application_id');

// Scores data
$normalizationData = new NormalizationListData($departmentId, $periodId, NULL, $round);
$scores = $normalizationData->getScores(1);

$groupMembersByScoreRank = array();
$previousScore = 0;
$scoreCount = 0;
$scoreRank = 0;

foreach ($scores as $score) {

    $scoreApplicationId = $score['application_id'];

    if (array_key_exists($scoreApplicationId, $groupMembers)) {

        $scoreCount++;
        if ($score['score_average'] > $previousScore) {
            $scoreRank = $scoreCount;    
        }
        $previousScore = $score['score_average'];
     
        $groupMembers[$scoreApplicationId]['score_average'] = $score['score_average']; 
        $groupMembers[$scoreApplicationId]['score_rank'] = $scoreRank;
        $groupMembersByScoreRank[$scoreApplicationId] = $groupMembers[$scoreApplicationId];
    }
}

// Rank coordinator name, comment empty by default.
$coordinatorName = '';
$coordinatorComment = '';
$commentDate = '';

if ($sort == 'scoreRank') {
    
    $displayGroupMembers = $groupMembersByScoreRank;
    
} else {
    
    // Last saved ranking or alpha default.
    $displayGroupMembers = $groupMembers;
}

// Get last saved comment.
$coordinatorQuery = "SELECT
    group_rank_comment.lu_users_usertypes_id AS coordinator_luu_id, 
    CONCAT(users.firstname, ' ', users.lastname) AS coordinator_name,
    comment AS coordinator_comment,
    comment_date
    FROM group_rank_comment
    INNER JOIN lu_users_usertypes
        ON group_rank_comment.lu_users_usertypes_id = lu_users_usertypes.id
    INNER JOIN users
        ON lu_users_usertypes.user_id = users.id
    INNER JOIN (
        SELECT 
        IFNULL(group_id, 0) AS group_id,
        IFNULL(round, 0) AS round,
        IFNULL(period_id, 0) AS period_id,
        MAX(timestamp) AS comment_date
        FROM group_rank
        WHERE group_id = " . $selectedGroupId . "
        AND round = " . $round . " 
        AND period_id = " . $periodId . "
    ) AS group_rank 
        ON group_rank_comment.group_id = group_rank.group_id
        AND group_rank_comment.round = group_rank.round
        AND group_rank_comment.period_id = group_rank.period_id
    WHERE group_rank_comment.group_id = " . $selectedGroupId . "
    AND group_rank_comment.round = " . $round . " 
    AND group_rank_comment.period_id = " . $periodId . "
    LIMIT 1";
    $coordinatorRecord = $DB_Applyweb->handleSelectQuery($coordinatorQuery);
    if (isset($coordinatorRecord[0])) {
        $coordinatorName = $coordinatorRecord[0]['coordinator_name'];
        $coordinatorComment = $coordinatorRecord[0]['coordinator_comment'];
        $commentDate = $coordinatorRecord[0]['comment_date'];    
    }
?>