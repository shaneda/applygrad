<?php
$convertIncludeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');
include $convertIncludeBase . '../classes/class.FileConverter.php';
require_once $convertIncludeBase . '../pdf/dompdf-0.5.2/dompdf_config.inc.php';
     
// Get letter html
$letterHtml = '<html><head></head><body>';
ob_start();
include '../inc/tpl.admissionLetter.php';
$letterHtml .= ob_get_contents();
ob_end_clean();
$letterHtml .= '</body></html>';

// Save html to file
$filename = 'admissionLetter_' . $applicantInfo['application_id'] . '_' . $unitId;
$tempHtmlFile = '../pdf/dompdf-0.5.2/temp/' . $filename . '.html';
$tempHtmlFileWritten = FileConverter::string2file($letterHtml, $tempHtmlFile);

// Convert html file to pdf file
$tempPdfFile = '../pdf/dompdf-0.5.2/temp/' . $filename . '.pdf';
$tempPdfFileWritten = FileConverter::html2pdf2($tempHtmlFile, $tempPdfFile);

// Move and cleanup
$path = $_SESSION['datafileroot'] . '/' . $applicantInfo['guid'];
if (!file_exists($path) && !is_dir($path)) {
    mkdir($path);         
}

$finalPdfFile = $path . '/' . $filename . '.pdf';
$pdfFileMoved = rename($tempPdfFile, $finalPdfFile); 
$htmlFileDeleted = unlink($tempHtmlFile);

if ($pdfFileMoved && $unit == 'department')
{
    saveDbAdmissionLetter($applicantInfo['application_id'], $unitId, $selectedLetterSections);
}
?>

<br><br>
<?php
if ($pdfFileMoved)
{
?>
    <p style="font-size: large; font-weight: bold;">New letter saved.</p>
<?
}

include '../inc/tpl.admissionLetterApplicantInfo.php';    
?>

<br><br>
<form action="" method="post" name="selectContentSubmit">
    <input type="hidden" name="applicationId" value="<?php echo $applicationId; ?>" />
    <input type="submit" name="cancelGeneratePdfSubmit" value="Return to Select Sections" />
    <input type="submit" name="cancelSelectContentSubmit" value="Return to Select User" />
    <?php
    if (is_array($selectedLetterSections))
    {
        foreach ($selectedLetterSections as $selectedLetterSection => $value)
        {
        ?>
            <input type="hidden" name="selectedLetterSections[<?php echo $selectedLetterSection; ?>]" value="true" />
        <?php
        }
    }     
    ?>
</form>

<?php
function saveDbAdmissionLetter($applicationId, $departmentId, $selectedLetterSections)
{
    if (!is_array($selectedLetterSections))
    {
        return;
    }
    
    $sections = implode('||', array_keys($selectedLetterSections));
    
    $query = "INSERT INTO admission_letter (application_id, department_id, sections)
        VALUES(
        " . intval($applicationId) . ",
        " . intval($departmentId) . ",
        '" . mysql_real_escape_string($sections) . "'
        )
        ON DUPLICATE KEY UPDATE 
        sections = '" . mysql_real_escape_string($sections) . "'";
        
    $result = mysql_query($query);
    
    return $result;
}
?>