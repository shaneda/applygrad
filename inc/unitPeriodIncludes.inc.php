<?php
/*
* Depends on:
*   DB_Applyweb/class.DB_Applyweb.php
*   DB_Applyweb/class.DB_Applyweb_Table.php  
*/

include CLASS_DIR . 'DB_Applyweb/class.DB_Unit.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Program.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_ProgramType.php';
include CLASS_DIR . 'DB_Applyweb/Table/class.DB_UnitRole.php'; 

//include CLASS_DIR . 'DB_Applyweb/class.DB_Programs.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_ProgramsUnit.php';
//include CLASS_DIR . 'DB_Applyweb/class.DB_Department.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_DepartmentUnit.php';
//include CLASS_DIR . 'DB_Applyweb/class.DB_Domain.php'; 
include CLASS_DIR . 'DB_Applyweb/class.DB_DomainUnit.php';

include CLASS_DIR . 'DB_Applyweb/class.DB_Period.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_UnitPeriod.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_PeriodProgram.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_PeriodApplication.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_PeriodType.php';
include CLASS_DIR . 'DB_Applyweb/Table/class.DB_PeriodUmbrella.php';

include CLASS_DIR . 'DB_Applyweb/Table/class.DB_ProgramGroup.php';
include CLASS_DIR . 'DB_Applyweb/Table/class.DB_ProgramGroupProgram.php';
include CLASS_DIR . 'DB_Applyweb/Table/class.DB_ProgramGroupType.php';
include CLASS_DIR . 'DB_Applyweb/Table/class.DB_ProgramGroupGroupType.php';
include CLASS_DIR . 'DB_Applyweb/Table/class.DB_ProgramGroupRole.php';

include CLASS_DIR . 'Unit/class.UnitBase.php';
include CLASS_DIR . 'Unit/class.Program.php';
include CLASS_DIR . 'Unit/class.Unit.php';

include CLASS_DIR . 'Period/class.PeriodBase.php';
include CLASS_DIR . 'Period/class.SubPeriod.php';
include CLASS_DIR . 'Period/class.UmbrellaPeriod.php';
include CLASS_DIR . 'Period/class.Period.php';
include CLASS_DIR . 'Period/class.ProgramGroup.php';
?>