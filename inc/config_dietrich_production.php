<?php
if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
{
    //header("Location: ../apply/offline.php");
}

/*
CONSTANT VALUES FOR THE APPLICATION
*/
$environment="production";

$db="awDcProd";
$db_username="awDcProdAdmin";
$db_password="c8Li2OCIDod6";
$db_host="webuild-db.srv.cs.cmu.edu";

$datafileroot = "../data";

$dontsendemail = FALSE;
$admissionsContact = "scsdiet+admissions@cs.cmu.edu"; 
$paymentEmail = "scsdiet+payment@cs.cmu.edu";
$supportEmail = "scsdiet+technical@cs.cmu.edu";

// $paymentProcessor = "https://ccard-submit.as.cmu.edu/cgi-bin/gather_info.cgi"; //PRODUCTION USING GENERIC COLLECTOR
$paymentProcessor = "https://commerce.cashnet.com/"; //PRODUCTION

if ( strpos( phpversion(),'ubuntu' ) !== FALSE)
{
    // System magic file doesn't seem to work
    // $magic_location = "/usr/share/file/magic"; 
    $magic_location = "/usr0/apache2/webapps/ApplygradDietrich/www/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);    
}
elseif ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);    
} else {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic";
    error_reporting(E_ALL ^ E_NOTICE);    
}

ini_set('display_errors', 0);                                                                                                                                                                                   
?>