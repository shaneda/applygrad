<?php
$template = getTemplateContent($contentId);
?>

<h3>Mail Sent</h3>
<div>
    <b>(In production, there will be an automatic redirect to the selection view after sending mail.)</b>
    <br><br>
    <a href="<?php echo $_SERVER['REQUEST_URI'] ?>">&laquo; New Selection</a><br> 
</div>
<br>

<?php
    $emailRecords = getApplicantEmailData($unit, $unitId, $periodId);
    $output = sendBulkMail($contentId, $admissionsContact, $emailRecords, $selectedUsers, TRUE);
    echo str_replace('From:', '<hr><br>From:', $output);  
?>