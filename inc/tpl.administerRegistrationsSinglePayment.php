<?php
$DB_Applyweb = new DB_Applyweb();

$totalFee = $paymentManager->getFeeAmount();
$totalFeeString = '$' . number_format( $totalFee, 2);

$paymentsCount = count($paymentManager->getPayments()); 
$totalPaid = $paymentManager->getTotalPaid();
$totalPaidString = '$' . number_format( $totalPaid, 2);

$paymentStatus = '<span class="confirm">unpaid</span>';
$feePaid = $paymentManager->getFeePaid();
$paidChecked = '';
if ( $feePaid ) {
    $paidChecked = 'checked';
    $paymentStatus = '<span class="confirmComplete">paid</span>';
}

$feeWaived = $paymentManager->getFeeWaived();
$waivedLabel = 'Fee Waived';
$waivedChecked = '';
$formDisabled = '';
if ( $feeWaived ) {
    $waivedLabel = '<b>' . $waivedLabel . '</b>';
    $waivedChecked = 'checked';
    $formDisabled = 'disabled';
    $paymentStatus = '<span class="confirmComplete">waived</span>';
    $balanceDue = 0;
} else {    
    if ( $paymentsCount == 0  && $paymentManager->getFeePaid()) {
        $balanceDue = $totalFee - $totalPaid;
    } else {
        $balanceDue = $paymentManager->getBalanceUnpaid();     
    }       
}
$balanceDueString = '$' . number_format( $balanceDue, 2);

$newPaymentValue = '';
if ($balanceDue) {
    $newPaymentValue = $balanceDue;
}

$payments = $paymentManager->getPayments();
 
echo <<<EOB

<form id="payment_{$applicationId}">
<table cellpadding="5px" cellspacing="0px" border="0">

EOB;
  
echo <<<EOB

    <tr>
        <td width="100px" align="right">
        Total Fee:
        </td>
        <td width="50px" align="right"> 
        <span id="totalFees_{$applicationId}">{$totalFeeString}</span>
        </td>
        <td align="left" width="100px">
        <input type="checkbox" class="feeWaived" id="feeWaived_{$applicationId}_{$departmentId}" {$waivedChecked} /> 
        {$waivedLabel}
        </td>
        <td colspan="4"> 
        </td>
    </tr>
EOB;

$paymentCount = 1;
foreach ($payments as $payment) {
    
    if ( $payment['payment_status'] == 'void' || $payment['payment_status'] == 'refunded' ) {
        continue;
    }
    
    $paymentId = $payment['id'];
    $paymentAmount = '$' . $payment['payment_amount'];
    $paymentDate =  date( 'm/d/Y', strtotime($payment['payment_intent_date']) );

    switch ($payment['payment_type']) {
        
        case 1:
        
            $paymentType = 'Check';
            break;
            
        case 3:
        
            $voucherQuery = "SELECT users.guid, datafileinfo.* 
                            FROM payment_voucher 
                            INNER JOIN datafileinfo ON payment_voucher.datafileinfo_id = datafileinfo.id
                            INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
                            INNER JOIN users ON lu_users_usertypes.user_id = users.id 
                            WHERE payment_voucher.payment_id = " . $paymentId;
            $voucherRecords = $DB_Applyweb->handleSelectQuery($voucherQuery);
            $filePath = '';
            foreach ($voucherRecords as $row) {
                $fileName = "PaymentVoucher_" . $row['userdata'] . "." . $row['extension'];                 
                $filePath = $datafileroot . "/" . $row['guid'] . "/PaymentVoucher/" . $fileName;   
            }

            if ($filePath) {
                $paymentType = '<a href="' . $filePath . '" target="_blank">Voucher</a>';     
            } else {
            
                $paymentType = 'Voucher';     
            }

            break;        

        case 4:
        
            $paymentType = 'Other';
            break;
        
        case 5:
        
            $paymentType = 'Wire Transfer';
            break;

        case 2:
        default:
            $paymentType = 'CC';    
    }
    
    $systemStatusMessage = '&nbsp;';
    $paymentIntentTimestamp = strtotime($payment['payment_intent_date']);
    $showStatusTimestamp = strtotime('2010-09-15 00:00:00');
    if ( ($paymentType == 'CC') && ($paymentIntentTimestamp > $showStatusTimestamp) ) {

        /*
        * Show payment status for payments made after 9/15/2010:
        * < 2.5 days, no auth = no status message
        * < 2.5 days, auth no settle = amt pending message
        * auth + settle = amt paid message
        * > 2.5, no settle = not paid message
        */

        //$statusQuery = "SELECT * FROM cc_payment_status WHERE payment_id = " . $paymentId;
        //$statusRecord = $DB_Applyweb->handleSelectQuery($statusQuery); 
        if ( isset($statusRecord[0]['status_date']) ) {
            
            $authTotal = $statusRecord[0]['auth_total'];
            $netSettled = $statusRecord[0]['settle_total'] - $statusRecord[0]['credit_total'];
            $statusTimestamp = strtotime($statusRecord[0]['status_date']);
            $warnAfterSeconds = 216000;   // 2.5 days            
        
        } else {
            
            // No status info available.
            $authTotal = 0;
            $netSettled = 0;
            $statusTimestamp = time();
            $warnAfterSeconds = 259200;   // 3 days        
        }
        
        $secondsSinceIntent = $statusTimestamp - $paymentIntentTimestamp;
        //$secondsSinceIntent = 1; // FOR TESTING    

        $messageStyle = 'color: black;';
        $statusMessageEnd = '';

        if ( !($authTotal == 0
            && $secondsSinceIntent < $warnAfterSeconds) ) 
        {
            $systemStatusMessage = '<i>CC processing status:</i><br/>';                

            if ( ($netSettled >= $payment['payment_amount']) ) 
            {
                if ($netSettled > 0) {
                    $messageStyle = 'color: green; font-weight: bold;';
                    $statusMessageEnd = '$' . $netSettled . ' Paid';                      
                }
            }
            elseif ($netSettled < $payment['payment_amount']
                    && $secondsSinceIntent > $warnAfterSeconds) 
            {
                $messageStyle = ' color: red; font-weight: bold;';
                $statusMessageEnd = 'Not Paid';       
            } 
            else 
            {
                $statusMessageEnd = '$' . $statusRecord[0]['auth_total'] . ' payment pending';                 
            }
        
        }
        $systemStatusMessage .= '<span style="' . $messageStyle . '">' . $statusMessageEnd . '</span>';
    }
    
    $paidChecked = '';
    if ($payment['payment_status'] == 'paid') {
        $paidChecked = 'checked';    
    }
    
    if ($paymentCount == 1) {

        echo <<<EOB
        
        <tr>
            <td align="right" style="border-bottom: 1px dotted black;">Payments:</td>
            <td colspan="7" style="border-bottom: 1px dotted black;">&nbsp;</td>
        </tr>  
EOB;
    }
    
    echo <<<EOB
    
    <tr >
        <td align="right" style="border-bottom: 1px dotted black;">&nbsp;</td>
        <td align="right" style="border-bottom: 1px dotted black;">
            <span id="paymentAmount_{$applicationId}_{$paymentId}">{$paymentAmount}</span>
        </td>
        <td align="center" style="border-bottom: 1px dotted black;">
            <span id="paymentDate_{$applicationId}_{$paymentId}">{$paymentDate}</span>    
        </td>
        <td align="left" width="30px" style="border-bottom: 1px dotted black;">
            <span id="paymentType_{$applicationId}_{$paymentId}">{$paymentType}</span>
        </td>
        <td style="border-bottom: 1px dotted black;">
            <input type="checkbox" class="paymentPaid" id="paymentPaid_{$applicationId}_{$departmentId}_{$paymentId}" {$paidChecked} {$formDisabled} />
            Paid
        </td>
        <td align="right" style="border-bottom: 1px dotted black;">
            <input type="button" class="voidPayment" id="voidPayment_{$applicationId}_{$departmentId}_{$paymentId}" value="Void" style="font-size: 10px;" {$formDisabled} />
        </td>        
        <td width="5px" style="border-bottom: 1px dotted black;">&nbsp;</td>
        <td align="left" width="150px" style="border-bottom: 1px dotted black;">
            {$systemStatusMessage}
        </td>
    </tr>    
EOB;

    $paymentCount++;
}
    
echo <<<EOB
 
    <tr>
        <td align="right">
        Total Paid:
        </td>
        <td align="right"> 
        <span id="totalPaid_{$applicationId}">{$totalPaidString}</span>
        </td>
        <td colspan="6"></td>
    </tr>
    <tr>
        <td align="right">
        <b>Balance Due: </b>
        </td>
        <td align="right"> 
        <span id="balanceDue_{$applicationId}"><b>{$balanceDueString}</b></span>
        </td>
        <td colspan="6"></td>
    </tr>
    <tr>
        <td colspan="8"></td>
    </tr>
    <tr>
        <td align="right">New Payment:</td>
        <td align="left" colspan="7">
        $<input type="text" size="5" class="newPaymentAmount" id="newPaymentAmount_{$applicationId}" value="{$newPaymentValue}" {$formDisabled} />
        <input type="radio" class="newPaymentType" name="newPaymentType" id="newPaymentType_{$applicationId}_2" value="2" checked" /> CC 
        <input type="radio" class="newPaymentType" name="newPaymentType" id="newPaymentType_{$applicationId}_1" value="1" /> Check 
        <input type="radio" class="newPaymentType" name="newPaymentType" id="newPaymentType_{$applicationId}_3" value="3" /> Voucher 
        &nbsp;&nbsp;
        <input type="submit" class="submitNewPayment" id="submitNewPayment_{$applicationId}_{$departmentId}" value="Add Payment" style="font-size: 10px;" {$formDisabled} />
        </td>
    </tr>    

</table>
</form>
<script>
    $('#paymentMessage_{$applicationId}').html('{$paymentStatus}');   
</script>

EOB;


?>