<?php
$semiblind_review = 0; 

$interviewTypes = array(
    1 => 'other',
    2 => 'phone',
    3 => 'skype'
); 
?>

<br/>
<table width="500" border="0"  cellspacing="0px" cellpadding="2px">
   <? if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Committee <br>
      Scores:</strong></td>
    <td valign="top"><strong>
    <?
    
    for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" 
            && $committeeReviews[$i][30]==0 
            && $committeeReviews[$i][22] != 1 // Not a admin/coordinator score
            && $committeeReviews[$i][32]== $thisDept )//NOT A SUPPLEMENTAL OR FAC VOTE
        {   
            ?><input name="revdept" type="hidden" value="<?=$committeeReviews[$i][32]?>"><?
            echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3];
            if ($committeeReviews[$i][5])
            {
                echo ", " . $committeeReviews[$i][5];    
            } 
            if(isPsychologyDepartment($thisDept) || $thisDept == 74 || $round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            
            echo ' - ';
            
            //1st score
            ?>
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][10]?>">
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][11]?>">
            <?
            if($thisDept == 3 || $thisDept == 6)
            {
                // PLB added program test 11/10/09.
                if ($showPhdInput) {
                    echo " PhD Score: ". $committeeReviews[$i][10];    
                }
                if ($showMsInput) {
                    echo " MS Score: ".$committeeReviews[$i][11];//show point 2     
                }    
            }
            elseif($thisDept == 83)
            {
              /*
                if ($showPhdInput) {
                    echo " PhD Score: ". $committeeReviews[$i][10];    
                }
                */
                if ($showMsInput) {
                    echo " MS Score: ".$committeeReviews[$i][11];//show point 2     
                } else {
                     echo " Score: ".$committeeReviews[$i][11];//show point 2
                }   
            }
            elseif ( isMseMsitDepartment($thisDept) )
            {
                echo " Score: ". $committeeReviews[$i][10];
                if ($committeeReviews[$i][11]) {
                    echo ", MSIT Score: ". $committeeReviews[$i][11];    
                }
                
                // Subtract 1 from AQA scores so display matches their scoring key
                if ($committeeReviews[$i][43])
                {
                    echo '; English: ' . ($committeeReviews[$i][43] - 1);
                }
                
                if ($committeeReviews[$i][45])
                {
                    echo '; Ability to Program: ' . ($committeeReviews[$i][45] - 1);
                }
                
                if ($committeeReviews[$i][47])
                {
                    echo '; Foundational Knowledge: ' . ($committeeReviews[$i][47] - 1);
                }
                
                if ($committeeReviews[$i][49])
                {
                    echo '; Maturity/Leadership: ' . ($committeeReviews[$i][49] - 1);
                }
                
                if ($committeeReviews[$i][51])
                {
                    echo '; Understanding of Program: ' . ($committeeReviews[$i][51] - 1);
                }
                
                if ($committeeReviews[$i][53])
                {
                    echo '; Experience: ' . ($committeeReviews[$i][53] - 1);
                }
            } 
            elseif ( isDesignPhdDepartment($thisDept) || isDesignDdesDepartment($thisDept) )
            {
                echo " Score: ". $committeeReviews[$i][10];
                if ($committeeReviews[$i][11]) {
                    echo ", MSIT Score: ". $committeeReviews[$i][11];    
                }
            }
            elseif ( isPsychologyDepartment($thisDept) )
            {
                if ($committeeReviews[$i][23] == 3)
                {
                    echo " Score: ". $committeeReviews[$i][10];
                }
            }
            elseif ( isRiMastersDepartment($thisDept)) {
                echo " Score: ". $committeeReviews[$i][11];//show point 2
            }    
            else
            {
                echo " Score: ". $committeeReviews[$i][10];//show point 1
            }
            /*
            //2nd score
            if($thisDept == 3 && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][11];
                }
            }
            */
            //1st score
            if($thisDept == 3 || isPsychologyDepartment($thisDept))
            {
                /*
                But RI doesn't use certainty!!! 
                // PLB added program test 11/10/09.
                if ($showPhdInput) {
                    echo ", PhD Certainty: ". $committeeReviews[$i][28];    
                }
                if ($showMsInput) {
                    echo " MS Certainty: ".$committeeReviews[$i][28] ;//show certainty 2    
                }
                */
            }
            elseif($thisDept == 6 || isMseMsitDepartment($thisDept) || isDesignDepartment($thisDept)
                    || isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept))
            {
                // LTI, MSE-MSIT, Design don't use certainty
            }
            else
            {
                // PLB fixed - should be point1_certainty - 2/3/10
                //echo ", Certainty: ". $committeeReviews[$i][28];//show certainty 2 
                echo ", Certainty: ". $committeeReviews[$i][27];//show certainty 2
            }
            /*
            //2nd score
            
            if($thisDept == 3 && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][28];
                }
            }
            */
            
            if($committeeReviews[$i][13] == 1)
            {
                echo ", Round 2: Yes";
            }
            elseif ($committeeReviews[$i][13] != null && $committeeReviews[$i][13] == 0)
            {
                echo ", Round 2: No";
            }
            elseif ($committeeReviews[$i][13] == -1 && isPsychologyDepartment($thisDept))
            {
                echo ", Round 2: Possible";
            }
            
            if($committeeReviews[$i][39] == 1)
            {
                echo ", Round 3: Yes";
            }
            elseif ($committeeReviews[$i][39] != null && $committeeReviews[$i][39] == 0)
            {
                echo ", Round 3: No";
            }
            elseif ($committeeReviews[$i][39] == -1 && isPsychologyDepartment($thisDept))
            {
                echo ", Round 3: Possible";
            }
            
            echo "<br>";
        }
     } ?>
    </strong></td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" >    </td>
  </tr>

  <? if($semiblind_review != 1 && !isPsychologyDepartment($thisDept)) { ?>
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top">
    <? 
    for($i = 0; $i < count($committeeReviews); $i++)
    {
    
        if ( $committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" 
            && $committeeReviews[$i][32] == $thisDept )
        {
                ?>
                <a href="userroleEdit_student_review.php?applicationId=<?=$committeeReviews[$i][1]?>&d=<?=$thisDept?>&v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" 
                target="_blank">
                <?=$committeeReviews[$i][2] . " ". $committeeReviews[$i][3]?> round <?=$committeeReviews[$i][23]?></a> 
                (<?=$committeeReviews[$i][15]?>)<br>
              <?
    
        }
     } ?></tr>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
     <? } // end semiblind check for faculty reviews
     ?>
     
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Committee <br>Comments:</strong>      </td>
    <td valign="top">
    <? 
    for($i = 0; $i < count($committeeReviews); $i++){
        $reviewerReviewsSeminars = array();
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 
            && $committeeReviews[$i][32]== $thisDept && $committeeReviews[$i][22] != 1)
        {
            echo "<input name='txtReviewerId' type='hidden' value='".$committeeReviews[$i][4]."'><strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            echo "</strong><br>";
            if ($thisDept == 6 || $thisDept == 83) {
                // LTI fields
                if ($committeeReviews[$i][6]) {
                    echo '<i>English:</i> ' . $committeeReviews[$i][6] . '<br>';
                }
                $reviewerReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
                if ( count($reviewerReviewsSeminars) > 0) {
                    echo '<i>Add Interest:</i> ';
                    $reviewerReviewsSeminarsFull = array();
                    foreach ($reviewerReviewsSeminars as $reviewerReviewsSeminar) {
                        $reviewsSeminarId = $reviewerReviewsSeminar[0];
                        foreach ($ltiSeminars as $ltiSeminar) {
                            if ($ltiSeminar[0] == $reviewsSeminarId) {
                                $reviewerReviewsSeminarsFull[] = $ltiSeminar[1];
                            }
                        }
                    } 
                    echo implode(', ', $reviewerReviewsSeminarsFull) . '<br>';     
                }
                if ($committeeReviews[$i][26]) {
                    echo '<i>Other Interests:</i> ' . $committeeReviews[$i][26] . '<br>';
                }
                if ($committeeReviews[$i][8]) {
                    echo '<i>Academic Aptitude:</i> ' . $committeeReviews[$i][8] . '<br>';
                }
                if ($committeeReviews[$i][7]) {
                    echo '<i>Research Aptitude:</i> ' . $committeeReviews[$i][7] . '<br>';
                }
                if ($committeeReviews[$i][40]) {
                    echo '<i>Industrial Experience:</i> ' . $committeeReviews[$i][40] . '<br>';
                }
                if ($committeeReviews[$i][41]) {
                    echo '<i>Fit to LTI:</i> ' . $committeeReviews[$i][41] . '<br>';
                }
                if ($committeeReviews[$i][25]) {
                    echo '<i>Special Consideration:</i> ' . $committeeReviews[$i][25] . '<br>';
                }
                if ($committeeReviews[$i][9]) {
                    echo '<i>Other:</i> ' . $committeeReviews[$i][9] . '<br>';
                }
            } elseif ($thisDept == 25) {
                // ISR-SE fields
                if ($committeeReviews[$i][6]) {
                    echo '<i>Industry experience:</i> ' . $committeeReviews[$i][6] . '<br>';
                }
               if ($committeeReviews[$i][7]) {
                    echo '<i>Research involvement &amp; interest:</i> ' . $committeeReviews[$i][7] . '<br>';
                }
                if ($committeeReviews[$i][8]) {
                    echo '<i>Stmt of purpose:</i> ' . $committeeReviews[$i][8] . '<br>';
                }
                if ($committeeReviews[$i][18]) {
                    echo '<i>Recoms:</i> ' . $committeeReviews[$i][18] . '<br>';
                }
                $reviewerLuuId = $committeeReviews[$i][4];
                $reviewerRiskFactors = getRiskFactors($appid, $reviewerLuuId);
                $reviewerPosistiveFactors = getPositiveFactors($appid, $reviewerLuuId);
                if ( count($reviewerRiskFactors > 0 )) {
                    echo '<i>Risk factors:</i> ';
                    $isrRiskFactorsFull = array();
                    foreach ($reviewerRiskFactors as $riskFactorIndex) {
                        $isrRiskFactorsFull[] = $riskFactorKey[$riskFactorIndex];
                    } 
                    echo implode(', ', $isrRiskFactorsFull) . '<br>';
                }
                if ( count($reviewerPosistiveFactors > 0) || $committeeReviews[$i][33]) {
                    echo '<i>Positive factors:</i> ';
                    $reviewerPositiveFactorsFull = array();
                    foreach ($reviewerPosistiveFactors as $positiveFactorIndex) {
                        $reviewerPositiveFactorsFull[] = $positiveKey[$positiveFactorIndex];
                    } 
                    echo implode(', ', $reviewerPositiveFactorsFull); 
                    if ($committeeReviews[$i][33]) {
                        echo ', ' . $committeeReviews[$i][33];
                    }
                    echo '<br>';
                }
                if ($committeeReviews[$i][9]) {
                    echo '<i>Comments:</i> ' . $committeeReviews[$i][9] . '<br>';
                }
                if ($committeeReviews[$i][17]) {
                    echo '<i>Mentor candidates:</i> ' . $committeeReviews[$i][17] . '<br>';
                }
            } elseif ( isMseMsitDepartment($thisDept) || isDesignDepartment($thisDept) 
                || isDesignPhdDepartment($thisDept) || isDesignDdesDepartment($thisDept)) {
                
                if ($committeeReviews[$i][9]) {
                    echo $committeeReviews[$i][9]."<br>";   
                }
                
                if (isDesignDepartment($thisDept) || isDesignPhdDepartment($thisDept) || isDesignDdesDepartment($thisDept))
                {
                    if ($committeeReviews[$i][6]) 
                    {
                        echo '<i>Teaching Potential:</i> ' . $committeeReviews[$i][6] . '<br>';
                    }
                    
                    if ($committeeReviews[$i][7]) 
                    {
                        echo '<i>Campus Visit:</i> ' . $committeeReviews[$i][7] . '<br>';
                    }      
                }
                
                $reviewerLuuId = $committeeReviews[$i][4];
                $reviewerMseRiskFactors = 
                    $db_risk_factors->getRiskFactors($appid, $reviewerLuuId);

                if ( count($reviewerMseRiskFactors) > 0 ) {
                    echo '<i>Risk factors:</i> ';
                    $mseRiskFactorsFull = array();
                    foreach ($reviewerMseRiskFactors as $reviewerMseRiskFactor) {
                        if ($reviewerMseRiskFactor['language']) {
                            $mseRiskFactorsFull[] = 'Language';    
                        }
                        if ($reviewerMseRiskFactor['experience']) {
                            $mseRiskFactorsFull[] = 'Experience';    
                        }                        
                        if ($reviewerMseRiskFactor['academic']) {
                            $mseRiskFactorsFull[] = 'Academic';    
                        }
                        if ($reviewerMseRiskFactor['other']) {
                            $mseRiskFactorsFull[] = 'Other';    
                        }
                        if ($reviewerMseRiskFactor['other_text']) {
                            $mseRiskFactorsFull[] = $reviewerMseRiskFactor['other_text'];    
                        }
                    } 
                    echo implode(', ', $mseRiskFactorsFull) . '<br>';
                }
                
                $reviewerInterviewType = $committeeReviews[$i][35];
                if ($reviewerInterviewType) {
                    echo '<i>Interview ' . $committeeReviews[$i][34] . ', ';
                    if ($reviewerInterviewType == 1) {
                        echo $committeeReviews[$i][36];        
                    } else {
                        echo $interviewTypes[$reviewerInterviewType];
                    }    
                    echo ':</i> ' . $committeeReviews[$i][37] . '<br>';
                }
                
                $reviewerEnglishComments = $committeeReviews[$i][42];
                if ($reviewerEnglishComments) {
                    if (isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept)) {
                        echo '<i>Academics:</i> ' . $committeeReviews[$i][42] . '<br>';
                    } else {
                        echo '<i>English:</i> ' . $committeeReviews[$i][42] . '<br>';
                    }
                }
                
                $reviewerProgrammingComments = $committeeReviews[$i][44];
                if ($reviewerProgrammingComments) {
                    if (isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept)) {
                        echo '<i>Design:</i> ' . $committeeReviews[$i][44] . '<br>';
                    } else {
                        echo '<i>Ability to Program:</i> ' . $committeeReviews[$i][44] . '<br>';
                    }
                }
                
                $reviewerFoundationalComments = $committeeReviews[$i][46];
                if ($reviewerFoundationalComments) {
                    if (isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept)) {
                        echo '<i>Research Ready:</i> ' . $committeeReviews[$i][46] . '<br>';
                    } else {
                        echo '<i>Foundational Knowledge:</i> ' . $committeeReviews[$i][46] . '<br>';
                    }
                }
                
                $reviewerMaturityComments = $committeeReviews[$i][48];
                if ($reviewerMaturityComments) {
                    if (isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept)) {
                        echo '<i>Faculty Fit:</i> ' . $committeeReviews[$i][48] . '<br>';
                    } else{
                        echo '<i>Maturity/Leadership:</i> ' . $committeeReviews[$i][48] . '<br>';
                    }
                }
                
                $reviewerUnderstandingComments = $committeeReviews[$i][50];
                if ($reviewerUnderstandingComments) {
                    if (isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept)) {
                        echo '<i>Focus:</i> ' . $committeeReviews[$i][50] . '<br>';
                    } else {
                        echo '<i>Understanding of Program:</i> ' . $committeeReviews[$i][50] . '<br>';
                    }
                }
                
                $reviewerExperienceComments = $committeeReviews[$i][52];
                if ($reviewerExperienceComments) {
                    if (isDesignDdesDepartment($thisDept) || isDesignPhdDepartment($thisDept)) {
                        echo '<i>Teaching Ready:</i> ' . $committeeReviews[$i][52] . '<br>';
                    } else {
                        echo '<i>Experience:</i> ' . $committeeReviews[$i][52] . '<br>';
                    }
                }
                
            } else {
                // Just show "comments" field.
                echo $committeeReviews[$i][9]."<br>";    
            }
        }
     } ?>    </td>
  </tr>
  
    <?php
    // Let CSD MS see CSD PhD Comments
    if ($thisDept == 74)
    {
        // Check whether this candidate has applied to any CSD PhD programs
        $phdQuery = "SELECT review.*, users.firstname, users.lastname 
            FROM review
            INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
            INNER JOIN users ON lu_users_usertypes.user_id = users.id
            WHERE review.application_id = " . $appid . " 
            AND review.department_id = 1";
        $phdResult = mysql_query($phdQuery);
        
        if ($phdResult)
        {        
    ?>
    <tr>
      <td colspan="2"><hr align="left"></td>
    </tr>
    <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>CSD PhD <br>Comments:</strong>      </td>
    <td valign="top">
    <?
        while($row = mysql_fetch_array($phdResult))
        {
            echo '<strong>' . $row['firstname'] . ' ' . $row['lastname'] . '</strong><br>';
            echo $row['comments'] . '<br>';
        }
     ?>    
     </td>
  </tr>
  <? 
        } // end if $phdResult
    } // end if CSD MS
   
   }//end semiblind reviews
  
  if (isset($showSemiblindFull) && $showSemiblindFull) { 
  ?>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr> 
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Personal <br> Comments:</strong></td>
    <td valign="top"><?php echo $private_comments; ?></td>
  </tr>  
  <?php
  } // end semiblind check for faculty reviews 
  
  // PLB added coordinator notes 1/8/10
  if ($semiblind_review != 1) { ?>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
  <tr>  
    <td bgcolor="#DDDDDD" ><strong> Coordinator Notes: </strong> </td>
    <td>
    <?php

    $coordinatorComments = getCoordinatorComments($appid, $thisDept);
    foreach ($coordinatorComments as $coordinatorComment) {
        echo $coordinatorComment . '<br>';    
    }
    
  // Decision comments for admins and committee members.
  if( $_SESSION['A_usertypeid'] == 0
    ||  $_SESSION['A_usertypeid'] == 1
    ||  $_SESSION['A_usertypeid'] == 2
    ||  $_SESSION['A_usertypeid'] == 10
    ) 
    { 
        echo '<i>Final comments:</i> ';
        for($x = 0; $x < count($myPrograms); $x++)
        {
            $aDepts = split(",", $myPrograms[$x][14]);
            for($j = 0; $j < count($aDepts); $j++)
            {
                if($aDepts[$j] == $thisDept)
                {
                    echo $myPrograms[$x][13];
                }
            }
        }
    }   
    ?>    
    </td>
  </tr>
<?php
} // end semiblind check for faculty reviews ?>
          
</table>