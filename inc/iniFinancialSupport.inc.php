<?php
// Initialize variables
$iniFinancialSupportRequest = NULL;
$iniFinancialSupportAttendWithout = NULL;
$iniFinancialSupportReceiveOutsideType = '';
$iniFinancialSupportReceiveOutsideSource = '';
$iniFinancialSupportApplyOutsideType = '';
$iniFinancialSupportApplyOutsideSource = '';
$iniFinancialSupportFamilyType = '';
$iniFinancialSupportFamilyAmount = '';
$iniFinancialSupportOtherSource = '';
$iniFinancialSupportError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveIniFinancialSupport();
    checkRequirementsIniFinancialSupport();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$iniFinancialSupportError)
{
    $iniFinancialQuery = "SELECT * FROM ini_financial_support 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $iniFinancialResult = mysql_query($iniFinancialQuery);
    while($row = mysql_fetch_array($iniFinancialResult))
    {
        $iniFinancialSupportRequest = $row['request_consideration'];
        $iniFinancialSupportAttendWithout = $row['attend_without_support'];
        $iniFinancialSupportReceiveOutsideType = $row['receive_outside_support_type'];
        $iniFinancialSupportReceiveOutsideSource = $row['receive_outside_support_source'];
        $iniFinancialSupportApplyOutsideType = $row['apply_outside_support_type'];
        $iniFinancialSupportApplyOutsideSource = $row['apply_outside_support_source'];
        $iniFinancialSupportFamilyType = $row['family_support_type'];
        $iniFinancialSupportFamilyAmount = $row['family_support_amount']; 
        $iniFinancialSupportOtherSource = $row['other_support_source'];        
    }    
}  
?>

<span class="subtitle">Financial Support</span>
<?php
if ($iniFinancialSupportError)
{
?>
    <br>
    <span class="errorSubtitle"><?php echo $iniFinancialSupportError; ?></span>
<?php  
}
?>
<br/>
<br/>
Note: your answers to these questions will not affect your admission decision but you WILL NOT be considered for scholarships if you answer "no" to the first question
<br/>
<br/>
<strong style="color: red;">(Mandatory)</strong> Do you wish to be considered for financial support from Carnegie Mellon?
<br/>
<br/>
<?php
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);
showEditText($iniFinancialSupportRequest, "radiogrouphoriz4", "iniFinancialSupportRequest", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
<strong style="color: red;">(Mandatory)</strong> Would you attend Carnegie Mellon without financial aid from the university?
<br/>
<br/>
<?php
showEditText($iniFinancialSupportAttendWithout, "radiogrouphoriz4", "iniFinancialSupportAttendWithout", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
If you will be receiving financial support from an outside agency, please specifiy:
<br/>
<br/>
<span style="display: inline-block; width: 50px;">Type</span>
<?php
$iniFinancialSupportTypes = array(
    array('Employer', 'Employer'),
    array('Government Agency', 'Government Agency'),
    array('Private Foundation', 'Private Foundation')
);
showEditText($iniFinancialSupportReceiveOutsideType, "listbox", "iniFinancialSupportReceiveOutsideType", $_SESSION['allow_edit'], FALSE, $iniFinancialSupportTypes); 
?>
<br/>
<span style="display: inline-block; width: 50px;">Source</span>
<?php
showEditText($iniFinancialSupportReceiveOutsideSource, "textbox", "iniFinancialSupportReceiveOutsideSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
If you will be applying for financial support from an outside agency, please specifiy:
<br/>
<br/>
<span style="display: inline-block; width: 50px;">Type</span>
<?php
showEditText($iniFinancialSupportApplyOutsideType, "listbox", "iniFinancialSupportApplyOutsideType", $_SESSION['allow_edit'], FALSE, $iniFinancialSupportTypes);
?>
<br/>
<span style="display: inline-block; width: 50px;">Source</span>
<?php
showEditText($iniFinancialSupportApplyOutsideSource, "textbox", "iniFinancialSupportApplyOutsideSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
If you will be receiving financial support from family or friends, please specifiy:
<br/>
<br/>
<span style="display: inline-block; width: 50px;">Type</span>
<?php
$iniFamilySupportTypes = array(
    array('Parents', 'Parents'),
    array('Other Relatives', 'Other Relatives'),
    array('Friends', 'Friends')
);
showEditText($iniFinancialSupportFamilyType, "listbox", "iniFinancialSupportFamilyType", $_SESSION['allow_edit'], FALSE, $iniFamilySupportTypes); 
?>
<br/>
<span style="display: inline-block; width: 50px;">Amount</span>
<?php
showEditText($iniFinancialSupportFamilyAmount, "textbox", "iniFinancialSupportFamilyAmount", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
If your source of funding will be different from above, please specify:
<br/>
<?php
showEditText($iniFinancialSupportOtherSource, "textbox", "iniFinancialSupportInternationalSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<hr size="1" noshade color="#990000">

<?php
function saveIniFinancialSupport()
{
    global $iniFinancialSupportRequest;
    global $iniFinancialSupportAttendWithout;
    global $iniFinancialSupportReceiveOutsideType;
    global $iniFinancialSupportReceiveOutsideSource;
    global $iniFinancialSupportApplyOutsideType;
    global $iniFinancialSupportApplyOutsideSource;
    global $iniFinancialSupportFamilyType;
    global $iniFinancialSupportFamilyAmount;
    global $iniFinancialSupportOtherSource;
    global $iniFinancialSupportError;  
    
    $iniFinancialSupportRequest = filter_input(INPUT_POST, 'iniFinancialSupportRequest', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $iniFinancialSupportAttendWithout = filter_input(INPUT_POST, 'iniFinancialSupportAttendWithout', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $iniFinancialSupportReceiveOutsideType = filter_input(INPUT_POST, 'iniFinancialSupportReceiveOutsideType', FILTER_SANITIZE_STRING);
    $iniFinancialSupportReceiveOutsideSource = filter_input(INPUT_POST, 'iniFinancialSupportReceiveOutsideSource', FILTER_SANITIZE_STRING);
    $iniFinancialSupportApplyOutsideType = filter_input(INPUT_POST, 'iniFinancialSupportApplyOutsideType', FILTER_SANITIZE_STRING);
    $iniFinancialSupportApplyOutsideSource = filter_input(INPUT_POST, 'iniFinancialSupportApplyOutsideSource', FILTER_SANITIZE_STRING);
    $iniFinancialSupportFamilyType = filter_input(INPUT_POST, 'iniFinancialSupportFamilyType', FILTER_SANITIZE_STRING);
    $iniFinancialSupportFamilyAmount = filter_input(INPUT_POST, 'iniFinancialSupportFamilyAmount', FILTER_SANITIZE_STRING);
    $iniFinancialSupportOtherSource = filter_input(INPUT_POST, 'iniFinancialSupportInternationalSource', FILTER_SANITIZE_STRING);
    
    if ($iniFinancialSupportRequest === NULL)
    {
        $iniFinancialSupportError .= 'Financial support consideration is required<br>';    
    }
     
    if ($iniFinancialSupportAttendWithout === NULL)
    {
        $iniFinancialSupportError .= 'Attend without financial support is required<br>';    
    }
        
    if (!$iniFinancialSupportError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM ini_financial_support WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE ini_financial_support SET
                request_consideration = " . intval($iniFinancialSupportRequest) . ",
                attend_without_support = " . intval($iniFinancialSupportAttendWithout) . ",
                receive_outside_support_type = '" . mysql_real_escape_string($iniFinancialSupportReceiveOutsideType) . "',
                receive_outside_support_source = '" . mysql_real_escape_string($iniFinancialSupportReceiveOutsideSource) . "',
                apply_outside_support_type = '" . mysql_real_escape_string($iniFinancialSupportApplyOutsideType) . "',
                apply_outside_support_source = '" . mysql_real_escape_string($iniFinancialSupportApplyOutsideSource) . "',
                family_support_type = '" . mysql_real_escape_string($iniFinancialSupportFamilyType) . "',
                family_support_amount = '" . mysql_real_escape_string($iniFinancialSupportFamilyAmount) . "',
                other_support_source = '" . mysql_real_escape_string($iniFinancialSupportOtherSource) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO ini_financial_support 
                (application_id, request_consideration, attend_without_support, 
                    receive_outside_support_type, receive_outside_support_source, 
                    apply_outside_support_type, apply_outside_support_source, 
                    family_support_type, family_support_amount, other_support_source)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($iniFinancialSupportRequest) . "," 
                . intval($iniFinancialSupportAttendWithout) . ",'" 
                . mysql_real_escape_string($iniFinancialSupportReceiveOutsideType) . "','" 
                . mysql_real_escape_string($iniFinancialSupportReceiveOutsideSource) . "','" 
                . mysql_real_escape_string($iniFinancialSupportApplyOutsideType) . "','" 
                . mysql_real_escape_string($iniFinancialSupportApplyOutsideSource) . "','"
                . mysql_real_escape_string($iniFinancialSupportFamilyType) . "','"
                . mysql_real_escape_string($iniFinancialSupportFamilyAmount) . "','"
                . mysql_real_escape_string($iniFinancialSupportOtherSource) . "')";
            mysql_query($insertQuery);
        }
    }
}
    
function checkRequirementsIniFinancialSupport()
{
    global $err;
    global $iniFinancialSupportError;     
    
    if (!$err && !$iniFinancialSupportError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>