<?

function doLogin($user, $pass)
{
	$server = "webiso.andrew.cmu.edu/login.cgi";
	$protocol = "https://";
	$server = "test.php";
	$port = "443";
	$log = "";
	$connect_timeout = 30; 
	$sock = null;
	
	
		if (!$sock = @fsockopen(((strtolower($protocol) == "https://")?"ssl://":"").$server, $port, $errno, $errstr, $connect_timeout)) 
		{
            $log .= "Could not open connection. Error "  .$errno.": ".$errstr."<br />\n";
			echo $log;
            return false;
        }
        
        $headers["Host"] = $server.":".$port;
        
        if ($user != "" && $pwd != "") {
            $log .= "Authentication will be attempted<br />\n";
            $headers["Authorization"] = "Basic ".base64_encode($user.":".$pwd);
        }
        
        if (count($postvars) > 0) {
            $log .= "Variables will be POSTed<br />\n";
            $request = "POST ".$path." HTTP/1.0\r\n";
            $post_string = "";
            foreach ($postvars as $key=>$value) {
                $post_string .= "&".urlencode($key)."=".urlencode($value);
            }
            $post_string = substr($post_string,1);
            $headers["Content-Type"] = "application/x-www-form-urlencoded";
            $headers["Content-Length"] = strlen($post_string);
        } elseif (strlen($xmlrequest) > 0) {
            $log .= "XML request will be sent<br />\n";
            $request = $verb." ".$path." HTTP/1.0\r\n";
            $headers["Content-Length"] = strlen($xmlrequest);
        } else {
            $request = $verb." ".$path." HTTP/1.0\r\n";
        } 
		echo "<br />request: ".$request; 
	
	 	if (fwrite($sock, $request) === FALSE) {
            fclose($sock);
            $log .= "Error writing request type to socket<br />\n";
			echo $log;
            return false;
        }
        
        foreach ($headers as $key=>$value) {
            if (fwrite($sock, $key.": ".$value."\r\n") === FALSE) {
                fclose($sock);
                $log .= "Error writing headers to socket<br />\n";
				echo $log;
                return false;
            }
        }
        
        if (fwrite($sock, "\r\n") === FALSE) {
            fclose($sock);
            $log .= "Error writing end-of-line to socket<br />\n";
			echo $log;
            return false;
        }
        
        #echo "<br />post_string: ".$post_string;
        if (count($postvars) > 0) {
            if (fwrite($sock, $post_string."\r\n") === FALSE) {
                fclose($sock);
                $log .= "Error writing POST string to socket<br />\n";
				echo $log;
                return false;
            }
        } elseif (strlen($xmlrequest) > 0) {
            if (fwrite($sock, $xmlrequest."\r\n") === FALSE) {
                fclose($sock);
                $log .= "Error writing xml request string to socket<br />\n";
				echo $log;
                return false;
            }
        }
        
        return $sock; 
	
}

?>