<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_setTextOfLayer(objName,x,newText) { //v4.01
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (document.layers) {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}
//-->
</script>
<?
/////////////////////////////
//WRITTEN BY 
//BRIAN MARTIN
//CARNEGIE MELLON UNIVERSITY SCS
//2006
//
//THIS IS A SPREADSHEET CLASS WHICH PROVIDES 
//SORTABLE RESULTS BASED ON A SQL QUERY
////////////////////////////

Class Spreadsheet
{
	var $title;
	var $subtitle;
	var $sortBy; 		//PRETTY OBVIOUS - JUST THE FIELD NAME
	var $lastSortBy; 	//USED TO HOLD PREVIOUS SORT FOR ASC/DESC DETERMINATION
	var $sortBy2;
	var $sqlOrderBy;	//CAN BE SET INITIALLY
	var $direction;		//ASC/DESC
	var $connection;
	var $db;
	var $db_host;
	var $db_username;
	var $db_password;
	var $sql;
	var $sqlGroupBy;	//PART OF SQL STATEMENT - HELPS IN SORTING COMPLEX DATA. USE ONLY THE FIELD NAME AND DON'T INCLUDE THIS CLAUSE IN THE $SQL VAR!
	var $fields; 		//SQL SELECT FIELDS
	var $aAllFields;	//USED INTERNALLY - AN ARRAY OF ALL FIELDS IN SQL STATEMENT
	var $fieldsSel; 	//ARRAY OF SELECTED FIELDS FROM THE DROPDOWN
	var $appForm; 		//THE PAGE NAME USED TO VIEW USERS APPLICATIONS
	var $editPage;		//OPTIONAL. IF YOU WANT AN EDIT COLUMN TO SHOW. SPECIFY A URL OR PAGE NAME
	var $multiEdit;		//OPTIONAL, BOOLEAN (1 OR 0). IF YOU WANT THE EDIT PAGE TO CONTROL MULTIPLE ROWS. DISPLAYS A CHECKBOX ON THE PAGE
	var $sqlWhere; 		//WHERE CLAUSE
	var $sqlDirective;	//ANY DISTINCT, TOP X COMMANDS
	var $localWhere;
	var $special;		//ALLOWS INJECTION OF CUSTOM FORM ELEMENTS, ETC. GETS WRITTEN TO THE PAGE
	var $having;		//FOR AGGREGATED FIELDS, WHICH CAN'T BE USED IN WHERE CLAUSE. APPEND ^ TO FIELD ALIAS TO USE THIS.
	var $counts;		//PROVIDES FOR RUNNING TOTALS ON PROVIDED FIELDS
	var $homelink;
	var $hidesort;		//HIDES THE SEARCH BOX FOR A SIMPLER INTERFACE
	var $sortfields;	//IF SET, RESTRICTS THE FIELDS USED IN THE SEARCH BOX - COMMA DELIMINATED
	var $staticfields;	//PRE-SET FIELDS MULTI-DIMENSIONAL ARRAY ( 0[1], 1[2] )
	var $showFilter;
	var $hideColumns; //ARRAY OF COLUMN TO NOT DISPLAY
	var $hideLinks;   //disables all links on page
	
	

	function Spreadsheet()
	{
		
		include_once '../inc/db_connect.php';
		$this->title = "";
		$this->subtitle = "";
		$this->lastSortBy = "";
		$this->sortBy = "";
		$this->sortBy2 = "";
		$this->direction = "";
		$this->fieldsSel = array();
		$this->aAllFields = array();
		$this->sql = "";
		$this->sqlGroupBy = "";
		$this->sqlOrderBy = "";
		$this->appForm = "print2.php";
		//$this->editPage = "";
		$this->sqlWhere = "";
		$this->having = "";
		$this->sqlDirective = "";
		$this->special = "";
		$this->counts = "";
		$this->homelink = "";
		$this->hidesort = "";
		$this->sortfields = "";
		$this->staticfields = array();
		$this->showFilter = true;
		$this->hideColumns = array();
		$this->hideLinks = false;
		
		
		//DEBUG CODE FOR POST VARS
		/*
		foreach($_POST as $key=>$val)
		{
			echo "key: ".$key . " val: ".$val."<br>";
		}
		*/
		
		
		

	}

	function beginsWith( $str, $sub ) {
	   return ( substr( $str, 0, strlen( $sub ) ) === $sub );
	}
	function endsWith( $str, $sub ) {
	   return ( stristr($str, $sub) );
	}


	function doSpreadSheet()
	{
	
		if(isset($_POST) ){
			if(isset($_POST["lbFields"])){
				foreach($_POST["lbFields"] as $field)
				{
					array_push($this->fieldsSel, $field);
				}
			}
			if(isset($_POST["lastsort"]))
			{
				$this->lastSortBy = htmlspecialchars($_POST["lastsort"]);
			}
			if(isset($_POST["sortdir"]))
			{
				$this->direction = htmlspecialchars($_POST["sortdir"]);
			}

			if(isset($_POST["sort2"]))
			{
				$this->sortBy2 = $_POST["sort2"];		
			}
			if(isset($_POST["sort"]))
			{
				$this->sortBy = $_POST["sort"];	
				if($this->sortBy2 == ""){
					if($this->lastSortBy == $this->sortBy)
					{
						if($this->direction == "ASC")
						{
							$this->direction = "DESC";
						}else
						{
							$this->direction = "ASC";
						}
					}else
					{
						$this->direction = "ASC";
						$this->lastSortBy = $this->sortBy;
					}
				}
			}




		}
	
	
		$columnWidth = 0;
		$rowLength = 0;
		$sep = "\t";
		$aNotFields = array(); //NEED THIS TO ADD THE VALUES BACK INTO THE DROP-DOWNS
		
		/////////////////////////////
		//BUILD JAVASCRIPT HANDLERS//
		/////////////////////////////
		echo "<SCRIPT LANGUAGE='JavaScript'>\n";
		echo "<!--\n";
		echo "function doFormSubmit(sort, sort2) { \n";
		echo "if(sort != ''){\n";
		echo "document.form1.elements['sort'].value = sort;\n";
		echo "}\n";
		echo "if(sort2 != ''){\n";
		echo "document.form1.elements['sort2'].value = sort2;\n";
		echo "}else{\n";
		echo "document.form1.elements['sort2'].value = '';\n";
		echo "}\n";
		echo "submitform('','_self');\n";
		echo "}\n";
		///////////
		//OPEN FORM
		///////////
		echo "function openForm(userid, appid, formname) { \n";
		echo "document.form1.elements['userid'].value = userid;\n";
		echo "document.form1.elements['applicationId'].value = appid;\n";
        echo "submitform(formname, '_blank');\n";
		echo "}\n";
		///////////
		//CHECK ALL
		///////////
		echo "function checkAll(checked) { \n";
		echo "var el = document.forms['form1'];\n";
		echo "for (var i = 0; i < el.elements.length; i++) {\n";
		echo "el.elements[i].checked = checked;\n";
		echo "}\n";
		echo "}\n";
		echo "//-->\n";
		echo "</script>\n";
		//////////////////
		//BUILD SQL HERE!
		//////////////////
		$startpos = 0;
		$strlen = 0;
		$sql = $this->sql;
		$sql = strtolower($sql); //PRETTY EVERYTHING UP FOR USE
		
		
		//EXTRACT ALL FIELDS FROM STRING
		$startpos = strpos($sql, "select");
		$strlen = strpos($sql, "from");
		if ($strlen !== false) {
			$startpos +=6; //add some space
			$strlen -= $startpos;
		}
		
		$strFields = trim(substr($sql,$startpos,$strlen));
		//echo $strFields;
		$this->fields = split(",\n", $strFields );
		
		$aSql = array(2);
		$aSql[0] = substr($sql,0,$startpos);
		$aSql[1] = substr($sql,$strlen+$startpos);
		
		$sql = $aSql[0] .= " ";
		$sql .= $this->sqlDirective . " ";
		if(count($this->fieldsSel) > 0){
			//GET FIELDS FROM SELECT BOX
			for($i = 0; $i < count($this->fieldsSel); $i++){
				//ALWAYS ADD USERID
				if($i == 0 && $this->fieldsSel[$i] != $this->fields[0]){
					$sql .= str_replace("^","",$this->fields[0]) . ",";
				}
				//ADD THE REST
				$sql .= str_replace("\'", "'", $this->fieldsSel[$i]);
				if($i < count($this->fieldsSel)-1){
					$sql .= ",";	
				}
				$sql .= " ";
			}
		}else
		{
			//NO FIELDS SELECTED SO USE DEFAULT FROM ORIGINAL SQL QUERY
			$sql .= " ". $strFields ." ";
		}
		$sql .= $aSql[1]; //ADD THE 2ND PART BACK IN
		////////////////////////////
		//BUILD WHERE CLAUSE
		////////////////////////////
		foreach($_POST as $key=>$val)
		{
			if( strstr($key , 'lbSrchVal_')   !== false)
			{
			
				$field = str_replace("lbSrchVal_", "",  $key);
				/*
				$arr = split("_",$key);
				$field = "";
				for($i = 1; $i< count($arr);$i++)
				{
					$field .= $arr[$i];
					
					if($i < count($arr)-1)
					{
						$field .= "_";
					}
					
				}
				$field = str_replace("_", ".",  $field);
				*/
				//echo "lbCond_".$field."    ";
				$cond = $_POST["lbCond_".$field];
				$postBaseFieldName = $_POST["txtSelName_".$field];
				if($cond == "NOT")
				{
					$cond = "!=";
					//NECESSARY TO RE-ADD TO THE ITEM DROP-DOWNS
					$arr = array($field, $val);
					array_push($aNotFields, $arr);
				}else{
					$cond = "=";
				}
				if($val != "Select an item from the list..."){
					if( $this->endsWith( $field , "^" ) === false)
					{

						if($this->sqlWhere != ""){
							$this->sqlWhere .= " and ". $postBaseFieldName ." " . $cond." '".htmlspecialchars($val)."' " ;
						}else{
							$this->sqlWhere = " where ". $postBaseFieldName ." = '".htmlspecialchars($val)."' " ;
						}
						
					}else{

						if($this->sqlWhere != ""){
							$this->sqlWhere .= " and ". $postBaseFieldName ." " . $cond." '".htmlspecialchars($val)."' " ;
						}else{
							$this->sqlWhere = " where ". $postBaseFieldName ." " . $cond." '".htmlspecialchars($val)."' " ;
						}
					}
				}
				
				
			}//END IF
		}
		
		$sql .= " " . $this->sqlWhere; //ADD THE WHERE CLAUSE IF SET
		
		//PROCESS GROUPBY TO SORT OUT THOSE FIELDS NOT IN THE CURRENT QUERY
		
		if($this->sqlGroupBy != "")
		{
			//ONLY DO THIS ON POSTBACK
			$sql .= " group by ".$this->sqlGroupBy." ";
			/*if(isset($_POST["lbFields"])){
				
				
				$aGroupBy = split(",",$this->sqlGroupBy);
				
				$this->sqlGroupBy = "";
				
				for($i = 0; $i < count($aGroupBy); $i++)
				{
					for($j = 0; $j < count($this->fieldsSel); $j++)
					{
						//GET THE SHORT FIELD NAME HERE
						$fieldnameFull = trim($this->fieldsSel[$j]);
						$fieldname = $fieldnameFull;
						$pos = strpos($fieldnameFull, " as ");
						if($pos !== false)
						{
							$fieldname = trim( substr($fieldnameFull, $pos+3 )  );
						}
						//echo strtolower(trim($fieldname)) ." ". strtolower($aGroupBy[$i])."<br>";
						//CHECK FOR A MATCH, AS YOU CAN'T GROUP BY A FIELD NOT IN THE QUERY
						//echo "grouping by: " . strtolower(trim($fieldname)) ." ". strtolower($aGroupBy[$i]) . "<br>";
						if(strtolower(trim($fieldname)) == strtolower($aGroupBy[$i]) )
						{

							if($this->sqlGroupBy != ""){
								$this->sqlGroupBy .= ",".$aGroupBy[$i];
							}else{
								$this->sqlGroupBy .= $aGroupBy[$i];
							}

						}
					}
				}
				
				if($this->sqlGroupBy != ""){
					$sql .= " group by ".$this->sqlGroupBy." ";
				}
			}else{
				$sql .= " group by ".$this->sqlGroupBy." ";
			}*/
		}
		$sql .= " ".$this->having." ";
		$orderClause = "";
		if($this->sortBy != "")
		{
			$orderClause .= " order BY ".$this->sortBy;
			$orderClause .= " ". $this->direction;
		}
		
		if($this->sortBy2 != "")
		{
			if($this->sortBy == "")
			{
				$orderClause .= " order BY ".$this->sortBy2;
			}else
			{
				$orderClause .= ", ". $this->sortBy2;
			}
		}
		if($orderClause != ""){
			$sql .= $orderClause;
		}elseif($this->sqlOrderBy != "")
		{
			$sql .= " order BY ".$this->sqlOrderBy;
		}
		$sql = str_replace("^","",$sql);
		//echo $sql."<br><br>";
		///////////////////////////
		//SQL BUILDING FINISHED ///
		///////////////////////////
		
		$result = mysql_query($sql) or die(mysql_error().$sql);
		$columnWidth = mysql_num_fields($result);
		//$columnWidth = count($this->fields);
		$numRows = mysql_num_rows($result);
		/////////////////////
		//BUILD TOP TABLE//
		/////////////////////
		
		echo "<table><tr><td class='tblItem' valign='top'>";
		?>
		<table  border="0" cellpadding="4">
			<? if($this->showFilter == true){ ?>		    
		    <tr class='tblHead'>
		     <td>Selected fields</td>
		      <!-- <td>Refine results </td>
		      <td>By Value </td> -->
		    </tr>
		   <? } ?>
		   <tr>
		   <td  class='tblItem'>
		   <?
			/////////////////////
			//FORM FIELD SELECTOR
			/////////////////////
			
			echo "<input name='userid' type='hidden' id='userid' value='' />";
            echo "<input name='applicationId' type='hidden' id='applicationId' value='' />";
			echo "<input name='printapp' type='hidden' id='printapp' value='printapplication' />\n";
			echo "<input name='sort' type='hidden' id='sort' value='".$this->sortBy."' />\n";
			echo "<input name='sort2' type='hidden' id='sort2' value='".$this->sortBy2."' />\n";
			echo "<input name='lastsort' type='hidden' id='lastsort' value='".$this->lastSortBy."' />\n";
			echo "<input name='sortdir' type='hidden' id='sortdir' value='".$this->direction."' />\n";
			echo "<input type=HIDDEN name='hidesort' value='".$this->hidesort."' >\n";
			if( isset( $_SESSION['member'] ) )
			{
				echo "<input name='member' type='hidden' id='member' value='".$_SESSION['member']."' />\n";
			}
			if(isset($_POST['program_index'])){
				echo "<input name='program_index' type='hidden' id='program_index' value='".$_POST['program_index']."' />\n";
			}
			if(isset($_POST['program_name'])){
				echo "<input name='program_name' type='hidden' id='program_name' value='".$_POST['program_name']."' />\n";
			}
			
			if($this->showFilter == true){ 
				
				echo "\n<select name='lbFields[]' size='10' multiple='multiple' id='lbFields[]' class='tblItem' width='200'>\n";
				$aSortfields = array();
				if($this->sortfields != "")
				{
					$aSortfields = split(",", $this->sortfields);
				}
				for($i = 0; $i < count($this->fields); $i++){
					$fieldname = trim($this->fields[$i]);
					$fieldnameBase = trim($this->fields[$i]);
					$fieldnameFull = trim($this->fields[$i]);
					//echo $fieldname." ".$fieldnameBase."\n";
					$selected = "";
					$pos = strpos($fieldnameFull, " as ");
					if($pos !== false){
						$fieldname = trim( substr($fieldnameFull, $pos+3 )  );
						$fieldnameBase = trim( substr($fieldnameFull,0, $pos )  );
					}
					for($j = 0; $j < count($this->fieldsSel); $j++){
						if(str_replace("\'", "'", $this->fieldsSel[$j]) == $fieldnameFull)
						{
							$selected = "selected";
						}
					}
					
					echo "\n<option value=\"".$fieldnameFull."\" class='tblItem' ".$selected.">".$fieldname."</option>\n";
					$arr = array();
					//echo "aSortfields " . count($aSortfields);
					if(count($aSortfields) > 0){
						foreach($aSortfields  as $key => $value)
						{
							if($value == $fieldname)
							{
								array_push($arr, $fieldname);
								array_push($arr, $fieldnameBase);
								array_push($this->aAllFields , $arr);
							}
						}
					
					}else{
						array_push($arr, $fieldname);
						array_push($arr, $fieldnameBase);
						array_push($this->aAllFields , $arr);
					}
	
				}
				echo "</select>\n";
			} 
			//////////////////
			//END SELECTOR
			/////////////////
			 ?>
		      </td>
		     
		    </tr>
  		</table>
		
	
		
		<?
		if($this->showFilter == true){
			echo "<input name='btnRefresh' type='button' id='btnRefresh' value='Refresh' class='tblItem' onClick=\" javascript: form1.action = '',form1.target = '_self',document.form1.submit();  \" />\n";
		}		
		//////////////////
		//SPECIAL TAGS
		/////////////////
		echo $this->special;
		echo "</td><td valign='top' class='tblItem'>";
		//////////////////
		//SEARCH LAYER
		//////////////////
		echo "<div id='layerSearch' name='layerSearch'></div>";
		
		echo "</td><td class='tblItem' valign='top'>";
		/////////////////
		//COUNTS LAYER
		/////////////////
		if($this->hideLinks == false)
		{
			echo "<div id='layerCounts' name='layerCounts'></div>";
			echo "<br>Number of records: " .$numRows ;
		}
		echo "</td></tr></table>";
		if($this->editPage != "" && $this->multiEdit == 1 && $this->hideLinks == false)
		{
			echo "<input name='btnSelAll' type='button' id='btnSelAll' value='Check All' class='tblItem' onClick=\"javascript: checkAll(true);\" />";
			echo "<input name='btnSelAll' type='button' id='btnSelAll' value='Uncheck All' class='tblItem' onClick=\"javascript: checkAll(false);\" />";
			echo "<input type='button' name='btnEdit' value='Edit Selected Items' onClick=\"javascript: openForm('','".$this->editPage."');\" class='tblItem' />";
		}
		?>
		
		<!--
		//MAIN TABLE
		-->
		<? if($this->hideLinks == false){?>
			<hr>
		<? } ?>
		<table style="text-align: left; width: 100%;" border="0" cellpadding="2" cellspacing="0">
			<?
			/////////////////////
			//BUILD TABLE HEADER
			/////////////////////
			echo "<tr class='tblHead'><td width='20'><!-- # --></td>\t\n";
			if($this->editPage != "" && $this->multiEdit == 1)
			{
				echo "<td width='20'></td>\t\n";
			}
			if($this->editPage != "" && $this->multiEdit != 1)
			{
				echo "<td width='30'><strong>Edit</strong></td>\t\n";
			}
			for($i = 0; $i < $columnWidth; $i++){
				$fieldname = mysql_field_name($result, $i);
				$sortsymbol = "";
				$showCol = true;
				for($j = 0; $j < count($this->hideColumns); $j++)
				{
					if($fieldname == $this->hideColumns[$j])
					{
						$showCol = false;
					}
				}
				if($showCol == true)
				{
					if($this->sortBy == $fieldname)
					{
						if($this->direction == "ASC")
						{
							$sortsymbol = " <img src='images/arrowdn.gif' border=0>";
						}else
						{
							$sortsymbol = " <img src='images/arrowup.gif' border=0>";
						}
						
					}
					if($this->hideLinks == true)
					{
						$fieldname = str_replace('_', '&nbsp;',$fieldname);
						echo "<td><strong>".$fieldname."</strong></td>\t\n";
					}else
					{
						echo "<td><strong><a href=\"javascript: doFormSubmit('".$fieldname."','');\">".$fieldname."</a>". $sortsymbol."</strong></td>\t\n";
					}
				}
			}
			
			echo "</tr>\n";
			////////////////////
			//END TABLE HEADER
			///////////////////
			$i = 0;
			$j = 0;
			////////////////////
			//DO COUNTS INIT
			////////////////////
			$aFields = array();
			if($this->counts != "")
			{
				$aFields = split(",", $this->counts);
			}
			for($i = 0; $i< count($aFields); $i++)
			{
				//CREATE A 3-DIMENSIONAL ARRAY FOR FIELDNAME/KEY/VALUE (FIELDNAME/FIELDVALUE/COUNT)
				$arr = array();
				$arr1 = array();
				array_push($arr1, $arr); //THIS IS THE KEY/VAL PAIRING
				$name = $aFields[$i];
				$aFields[$i] = array($name); //re-initalize into array with field name as initial element
				array_push($aFields[$i], $arr1);
			}
			///////////////////////////
			//FOR THE FIELD DROP-DOWNS
			///////////////////////////
			for($i = 0; $i < count($this->aAllFields); $i++){
			
				$arr = array();//THE ARRAY OF VALUES
				$name = $this->aAllFields[$i];
				$this->aAllFields[$i] = array($name); //re-initalize into array with field name as initial element
				array_push($this->aAllFields[$i], $arr);
			}
			//echo "Static fields ";
			for($y = 0; $y < count($this->staticfields); $y++)
			{
				array_push($this->aAllFields, $this->staticfields[$y]);	
				echo $this->staticfields[$y][0][1] . "<br>";
			}
			
			////////////////////////////////////////
			//MAIN SQL RETURN LOOP
			////////////////////////////////////////
			$k = 0;
			while ($row = mysql_fetch_array($result))
			{
				
					$altClass = "";
					if($j % 2 == 0){
						$altClass =" class='tblItem'";
					}else
					{
						$altClass =" class='tblItemAlt'";
					}
					echo "<tr".$altClass."><td>".($j+1).".</td>\t\n";
					if($this->editPage != "" && $this->multiEdit == 1)
					{
						echo "<td><input name='chk1[]' id='chk1[]' type='checkbox' value='".stripslashes($row[0])."' /></td>\t\n";
					}
					if($this->editPage != "" && $this->multiEdit != 1 && $this->hideLinks == false){
						echo "<td><a href=\"javascript: openForm('";
                        echo stripslashes($row[0])."','";
                        echo stripslashes($row[1])."','";
                        echo $this->editPage."');\">Edit</a></td>";
					}	
					for($i = 0; $i < $columnWidth; $i++)
					{
						$record = "";
						$fieldname = mysql_field_name($result, $i);

						$showCol = true;
						for($m = 0; $m < count($this->hideColumns); $m++)
						{
							if($fieldname == $this->hideColumns[$m])
							{
								$showCol = false;
							}
						}				
				
						$link = "<a href=\"javascript: doFormSubmit('".$this->sortBy."','".$fieldname."');\">".stripslashes($row[$i])."</a>";
						if($this->hideLinks == true)
						{
							$link = stripslashes($row[$i]);
						}
						$record = $link;
						if($fieldname == "userid")
						{
							if($this->appForm != ""){
								if($this->hideLinks == true)
								{
									
								}
								else
								{
									$record = "<a href=\"javascript: openForm('".stripslashes($row[$i])."', '".$this->appForm."');\">View</a>";
								}
							}else
							{
							
							}
						}
						
						
						//////////////////////////
						//PROCESS THE 'NOT' FIELDS
						//////////////////////////
						for($y = 0; $y < count($aNotFields); $y++)
						{
						//echo $aNotFields[$y][0] ." ".$fieldname."<br>";
							if(str_replace("^","",$aNotFields[$y][0]) == $fieldname )
							{
								//echo "fieldname is same " . $fieldname ;
								
								//FIND A MATCHING FIELD
								for($x = 0; $x< count($this->aAllFields); $x++)
								{
									if(str_replace("^","",$this->aAllFields[$x][0][0]) == $fieldname )
									{		
										$isFound = false;
										for($k = 0; $k < count($this->aAllFields[$x][1]); $k++)
										{
											if($this->aAllFields[$x][1][$k] == $aNotFields[$y][1] && $aNotFields[$y][1] != "")
											{
												//echo "searching for " . $aNotFields[$y][1] . " finding " . $this->aAllFields[$x][1][$k] . "<br>";
	
												//SET COUNT
												$isFound = true;
											}
										}
										if($isFound == false)
										{
											//INSERT VALUE AND SET COUNT
											//$val = str_replace("'", "\'",  $aNotFields[$y][1] );
											//$val = str_replace ( "\"", "&quot;", $val );
											//echo "adding " .$aNotFields[$y][1] . " to the dropdown list " . $fieldname . "<br>";
	
											$val = addslashes ( $aNotFields[$y][1] );
											array_push($this->aAllFields[$x][1],  $val);
										}
									}
								}
							}		
						}
					
						///////////////////////////
						//FOR THE SEARCH DROP DOWNS
						///////////////////////////
						for($x = 0; $x< count($this->aAllFields); $x++)
						{
						//echo $this->aAllFields[$x][0][0] ." ".$fieldname."<br>";
						
							if(str_replace("^","",$this->aAllFields[$x][0][0]) == $fieldname )
							{
							
								$isFound = false;
								//FIND A MATCHING FIELD
								for($k = 0; $k < count($this->aAllFields[$x][1]); $k++)
								{
								
									if($this->aAllFields[$x][1][$k] == $row[$i] && $row[$i] != "")
									{
										//SET COUNT
										$isFound = true;
									}
								}
								if($isFound == false)
								{
									//INSERT VALUE AND SET COUNT
									//$val = str_replace("'", "\'",  $row[$i]);
									//$val = str_replace ( "\"", "&quot;", $val );
									
									$val = addslashes($row[$i]);
									array_push($this->aAllFields[$x][1],  $val);
								}
							}								
						}
				if($showCol == true)
				{
						echo "<td>".stripslashes($record)."</td>\t\n";
				}//end showcol
						//////////////////////
						//FOR THE FIELD COUNTS
						//////////////////////
						for($x = 0; $x< count($aFields); $x++)
						{
							
							if($aFields[$x][0] == $fieldname )
							{
								$isFound = false;
								//FIND A MATCHING FIELD
								for($k = 0; $k < count($aFields[$x][1]); $k++)
								{
									if(is_array($aFields[$x][1][$k]) && count($row) > $i)
									{
										if(count($aFields[$x][1][$k]) > 0)
										{
											if($aFields[$x][1][$k][0] == $row[$i] && $row[$i] != "")
											{
												//SET COUNT
												$isFound = true;
												$aFields[$x][1][$k][1] = $aFields[$x][1][$k][1]+1;
												//echo "count is :" . $aFields[$x][1][$k][1] . " ". $row[$i];
												
											}
										}
									}
								}
								if($isFound == false)
								{
									//INSERT VALUE AND SET COUNT
									$arr = array($row[$i],1);
									array_push($aFields[$x][1], $arr);
								}
							
							}
										
						}
					}
					
					echo "</tr>\n";
			
				$j++;
				
			}
			?></table><?
			/////////////////
			//OUTPUT COUNTS
			/////////////////
			$countsOut = "";
			if(count($aFields) > 0)
			{
				$countsOut = "";
				for($i = 0; $i < count($aFields); $i++)
				{
					$countsOut .= "<table cellpadding=\"4\" align=\"left\"  ><tr ><td class=\"tblHead\"  colspan=\"2\">".$aFields[$i][0]."</td></tr>";
					for($j = 0; $j < count($aFields[$i][1]); $j++)
					{
						if(is_array($aFields[$i][1][$j]))
						{
							if($aFields[$i][1][$j][0] != "")
							{
							$countsOut .= "<tr><td class=\"tblItem\" >".$aFields[$i][1][$j][0]."</td>";
							$countsOut .= "<td class=\"tblItem\" >".$aFields[$i][1][$j][1]."</td></tr>";
							}
						}
					}
					$countsOut .= "</table>";
				}
				
			}
			//////////////////////
			//OUTPUT SEARCH FIELDS
			//////////////////////
			$searchFieldsOut = "";
			if(count($this->aAllFields) > 0 && $this->hidesort != "true")
			{
			
				$searchFieldsOut = "";
				$searchFieldsOut .= "<table cellpadding=\"4\" class=\"tblItem\" >";

				for($i = 0; $i < count($this->aAllFields); $i++)
				{
					if(isset($_POST[ "lbSrchVal_".$this->aAllFields[$i][0][0] ]))
					{
						$selVal = htmlspecialchars($_POST[ "lbSrchVal_".$this->aAllFields[$i][0][0] ]);
						
						$searchFieldsOut .= "<tr><td>".$this->aAllFields[$i][0][0]."</td><td> <input name=\"txtSelName_".$this->aAllFields[$i][0][0]."\" id=\"txtSelName_".$this->aAllFields[$i][0][0]."\" type=\"hidden\" value=\"".str_replace("'", "\'",  $this->aAllFields[$i][0][1]) ."\" />             <select name=\"lbCond_".$this->aAllFields[$i][0][0]."\" id=\"lbCond_".$this->aAllFields[$i][0][0]."\"  class=\"tblItem\"><option>=</option><option>NOT</option></select></td> <td><select name=\"lbSrchVal_".$this->aAllFields[$i][0][0]."\" id=\"lbSrchVal_".$this->aAllFields[$i][0][0]."\" class=\"tblItem\">";
						//echo $this->aAllFields[$i][0][0] . " " . str_replace("'", "\'",  $this->aAllFields[$i][0][1])  . "<br>";
						$searchFieldsOut .= "<option>Select an item from the list...</option>";//default
						for($j = 0; $j < count($this->aAllFields[$i][1]); $j++)
						{
							$selected = "";
							if($this->aAllFields[$i][1][$j] != "")
							{ 
								//print records here
								if($this->aAllFields[$i][1][$j] == $selVal)
								{
									$selected = "selected";
								}
	
								$searchFieldsOut .= "<option value=\"".$this->aAllFields[$i][1][$j]."\" ".$selected.">".$this->aAllFields[$i][1][$j]."</option>";
							}
						}//end for
						$searchFieldsOut .= "</select></td></tr>";
					}//end issset
					
				}
				$searchFieldsOut .= "</table>";	
				//$searchFieldsOut .= addslashes("<input name='btnRefresh' type='button' id='btnRefresh' value='Refresh' class='tblItem' onClick=\" submitform('','_self');  \" />");

			
			}
			echo "<script>MM_setTextOfLayer('layerSearch','','". $searchFieldsOut."');</script>";	
			
			echo "<script>MM_setTextOfLayer('layerCounts','','".$countsOut."');</script>";
					
			
		}//end function doSpreadsheet
		
}//end class
?>

