<br><br>
<?php
include '../inc/tpl.admissionLetterApplicantInfo.php';    
?>

<br><br>
<form action="" method="post" name="selectContentSubmit">
    <input type="hidden" name="applicationId" value="<?php echo $applicationId; ?>" />
    <input type="submit" name="generatePdfSubmit" value="Save New Letter" />
    <input type="submit" name="cancelGeneratePdfSubmit" value="Return to Select Sections" />
    <?php
    if (is_array($selectedLetterSections))
    {
        foreach ($selectedLetterSections as $selectedLetterSection => $value)
        {
        ?>
            <input type="hidden" name="selectedLetterSections[<?php echo $selectedLetterSection; ?>]" value="true" />
        <?php
        }
    }     
    ?>
    
    <br><br>
    <?php
    include '../inc/tpl.admissionLetter.php';
    ?>
    
    <br>
    <input type="submit" name="generatePdfSubmit" value="Save New Letter" />
    <input type="submit" name="cancelGeneratePdfSubmit" value="Return to Select Sections" />
</form>


