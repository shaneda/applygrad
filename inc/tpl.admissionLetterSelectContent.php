<br><br>
<?php
include '../inc/tpl.admissionLetterApplicantInfo.php';    

if (!$selectedLetterSections && $unit == 'department')
{
    $selectedLetterSections = getDbSelectedLetterSections($applicantInfo['application_id'], $unitId);    
}
?>

<br>
<form action="" method="post" name="selectContentSubmit">
    <input type="submit" name="selectContentSubmit" value="Create New Letter" />
    <input type="submit" name="cancelSelectContentSubmit" value="Return to Select User" />
    <input type="hidden" name="applicationId" value="<?php echo $applicationId; ?>" />
    
    <p style="font-weight: bold;">Select letter sections:</p>
    <?php echo templateMenu($selectedLetterSections); ?> 

    <br><br>
    <input type="submit" name="selectContentSubmit" value="Create New Letter" />
    <input type="submit" name="cancelSelectContentSubmit" value="Return to Select User" />
</form>



<?php
function templateMenu($selectedLetterSections)
{
    global $unit;
    global $unitId;
    
    if ($unit == 'department' && isDesignDepartment($unitId))
    {
        $letterSections = getLetterSectionsDesign();
    }
    elseif ($unit == 'department' && isDesignPhdDepartment($unitId))
    {
        $letterSections = getLetterSectionsDesignPhd();
    }
    elseif ($unit == 'department' && isDesignDdesDepartment($unitId))
    {
        $letterSections = getLetterSectionsDesignPhd();
    }
    elseif ($unit == 'department' && isIniDepartment($unitId))
    {
        $letterSections = getLetterSectionsIni();
    }
    else
    {
        $letterSections = getLetterSections();
    }
    
    $templateMenu = '<div>';

    foreach ($letterSections as $key => $letterSection)
    {
        $templateMenu .= '<input class="userCheckbox shiftclick" type="checkbox" 
            name="selectedLetterSections[' . $letterSection['name'] . ']" id="checkbox_' . $key . '"'; 

        if (!$letterSection['optional'] ||
            (is_array($selectedLetterSections) && in_array($letterSection['name'], array_keys($selectedLetterSections))))
        {
            $templateMenu .= ' checked="checked" ';    
        }

        if (!$letterSection['optional'])
        {
            $templateMenu .= ' disabled="disabled" ';     
        } 
        
        $templateMenu .= '/> ' . str_replace('Admission Letter - ', '', $letterSection['name']);
        
        if (!$letterSection['optional'])
        {
            $templateMenu .= '<input type="hidden" name="selectedLetterSections[' . $letterSection['name'] . ']" value="true" />';     
        }
        
        $templateMenu .= '<br>';    
    }
    
    $templateMenu .= '</div>';
    
    return $templateMenu;
}

function getDbSelectedLetterSections($applicationId, $departmentId)
{
    $query = "SELECT sections FROM admission_letter WHERE
        application_id = " . intval($applicationId) . "
        AND department_id = " . intval($departmentId) . "
        LIMIT 1";
        
    $result = mysql_query($query);
    
    $sections = '';
    while ($row = mysql_fetch_array($result))
    {
        $sections = $row['sections'];    
    }
    
    $sectionKeys = explode('||', $sections);
    $selectedLetterSections = array();
    foreach($sectionKeys as $sectionKey)
    {
        $selectedLetterSections[$sectionKey] = TRUE;
    }
    
    return $selectedLetterSections;
}
?>