<?php

include_once "mhci_prereqsCourseTypes.config.php";

class MHCI_PrereqsCourseFormController {
    
    // Fields set in constructor.
    private $studentLuUsersUsertypesId;
    private $view;                                  // "student" || "reviewer" 
    private $prereqType;                            // "design" || "programming" || "statistics"
    private $enableAddNewCourse;                    // TRUE || FALSE                                                   
    
    private $addCourseRequestSubmitted = FALSE;
    private $addCourseButtonDisabled = "";
    private $coursesSubmitted = FALSE; 
    private $courseList = array();                       


    public function __construct($studentLuUsersUsertypesId, $view, $prereqType, $enableAddNewCourse=FALSE) {
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;
        $this->prereqType = $prereqType;
        $this->enableAddNewCourse = $enableAddNewCourse;
        
    }
    
    public function getCourseList() {
        return $this->courseList;
    }
    
    
    public function addToCourseList($course) {
 //       DebugBreak();
        if ($course[0] != -1) {
            array_push($this->courseList, $course);
        }
        return $this->courseList;        
    }
    
    
    public function render($disable) {
        // DAS DebugBreak();
        
        // Get the course types for this prereq from mhci_prereqsCourseTypes.config.php
        global $prereqCourseTypes;
        global $assessmentForm;
        $courseTypesToRender = $prereqCourseTypes[$this->prereqType];
        
        // Check for a new course submit in REQUEST
        $this->handleRequest();
        
        // Hold the course form output until the end.
        ob_start(); 
        
        // Render at least one form for each course type. 
        foreach ($courseTypesToRender as $key => $courseTypeArray) {

            // Get course Ids for this student/course type
            $courseType = $key;
            // DebugBreak();
            $courseDataArray = $this->getCourseData($courseType);
            
            // Render a form for each course id.
            $formName = $courseType;
            $formHeading = $courseTypeArray['heading'];
  //          $formIntro = $courseTypeArray['intro'];
            $sql = "select content from content where name='Placeout ". ucfirst($this->prereqType) . " Course Description' and domain_id= 37";

            $result = mysql_query($sql) or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $formIntro = html_entity_decode($row["content"]);
                } 
            $i = 0;
            do {
                
                // Number the course form name if there's more than one for that type.  
                if ( count($courseDataArray) > 1 ) {
                    $formName = $courseType . " " . ($i + 1);
                    $formHeading = $courseTypeArray['heading'] . " " . ($i + 1);    
                }
                
                // render the form.
                if (isset($courseDataArray[$i])) {
                    $courseId = $courseDataArray[$i];    
                } else {
                    // THIS IS A KLUGE so that a new course form can be rendered properly
                    // when there's no corresponding course record in the db yet.
                    $courseId = -1;   
                } 

                $courseInfoStatus = $this->renderCourseForm($courseType, $formName, $formHeading, $formIntro, $courseId);
                
                $teststatus = $assessmentForm->getAssessmentStatus();
        //        DebugBreak();
                // Disable the add course button if the course has not been submitted
               // if ( !isset($courseDataArray['submitted_to_reviewer']) || ($courseDataArray['submitted_to_reviewer'] == 0) ) {
               if ($teststatus == "Submitted")   {
                    $this->addCourseButtonDisabled = "disabled";    
                }
                
                $i++;
                
            } while ($i < count($courseDataArray));
           // DebugBreak();
            if ($this->addCourseRequestSubmitted) {

                // Render a new form with a numbered heading.
                $formName = $courseType . " " . ($i + 1);
                $formHeading = $courseTypeArray['heading'] . " " . ($i + 1);
                $this->renderCourseForm($courseType, $formName, $formHeading, $formIntro);
            } 
            
        }
        
        // Get the course form output.
        $courseFormOutput = ob_get_contents();
        ob_end_clean();
        
        // Output the course form(s).
        echo $courseFormOutput;
        
        if ( $this->enableAddNewCourse  && ($this->view == "student") ) {
             
                // Render the add course button accordingly.  
            
                if ($this->addCourseRequestSubmitted) {
                    $this->addCourseButtonDisabled = "disabled";
                } else {
                    $this->addCourseButtonDisabled = "";
                }

                $this->renderAddCourseButton();
        }
        
        return TRUE;
        
    }
    
    
    private function renderAddCourseButton() {

        echo <<<EOB

        <input type="submit" id="{$this->prereqType}_addCourseSubmit" name="{$this->prereqType}_addCourseSubmit" class="bodyButton addCourseSubmit" value="Add another course" {$this->addCourseButtonDisabled}/> 
        <hr /> 

EOB;
        
    }
    
    
    private function renderCourseForm($courseType, $formName, $formHeading, $formIntro, $courseId=-1) {
     //  debugBreak();
        global $assessmentForm;
        $courseForm = new MHCI_PrereqsCourseForm($this->studentLuUsersUsertypesId, $this->view, 
                        $courseType, $formName);
        $courseForm->setHeading($formHeading);
        $courseForm->setIntro($formIntro);
        $courseForm->setCourseId($courseId);
        $courseForm->setPlaceoutPeriodId($assessmentForm->getPlaceoutPeriodId ());
        $courseForm->setApplicationId($assessmentForm->getApplicationId());
        $courseForm->setFormSubmitted($assessmentForm->getFormSubmitted());
   //     if ($courseId == -1) {
   //         $courseForm->addCourseData();
    //    }
        if ($assessmentForm->getReviewerStatus() == 'Approved plan:' || $assessmentForm->getReviewerStatus() == 'Fulfilled: undergraduate degree' 
    || $assessmentForm->getReviewerStatus() == 'Fulfilled:') {
            $courseStatus = $courseForm->render("bogus");
    } else {
          $courseStatus = $courseForm->render("enable");
    }
 //       DebugBreak();
        echo "<hr/>";
        $courseArray = array();
        $courseArray[0] = $courseId;
        $courseArray[1] = $courseForm->getNewUploadInfoFromObject();
        $this->addToCourseList($courseArray);
       //   DebugBreak();
        return $courseArray;
        
    }
    
    private function addNewCourseForm($courseType, $formName, $formHeading, $formIntro, $courseId=-1) {
        global $assessmentForm;
        $courseForm = new MHCI_PrereqsCourseForm($this->studentLuUsersUsertypesId, $this->view, 
                        ucfirst($courseType), $formName);
        $courseForm->setHeading($formHeading);
        $courseForm->setIntro($formIntro);
        $courseForm->setCourseId($courseId);
        $courseForm->setPlaceoutPeriodId($assessmentForm->getPlaceoutPeriodId ());
        $courseForm->setApplicationId($assessmentForm->getApplicationId());
        $courseForm->setFormSubmitted($assessmentForm->getFormSubmitted());
        // Build from request information
        $requestLabel = ucfirst(str_replace(" ", "", $formName));
        if (isset ($_REQUEST[$requestLabel.'_courseName'])) {
            $courseForm->setCourseName($_REQUEST[$requestLabel.'_courseName']);
        }
        if (isset ($_REQUEST[$requestLabel .'_courseTime'])) {
            $courseForm->setCourseTime($_REQUEST[$requestLabel.'_courseTime']);
        }
        if (isset ($_REQUEST[$requestLabel .'_courseInstitution'])) {
            $courseForm->setCourseInstitution($_REQUEST[$requestLabel.'_courseInstitution']);
        }
        if (isset ($_REQUEST[$requestLabel.'_courseGrade'])) {
            $courseForm->setCourseGrade($_REQUEST[$requestLabel.'_courseGrade']);
        }
        if ($courseId == -1) {
        //    DebugBreak();
            $courseForm->addCourseData($courseForm->getFormSubmitted());
        }
    }
    
    public function verifyCourseInfoComplete() {
        $courseList = $this->getCourseList();
        $infoComplete = FALSE;
        foreach ($courseList as $course) {
  //      debugBreak();
            if ((isset($course[0]) && $course[0] > 0) && (isset($course[1]) && $course[1] != "")) {
                $infoComplete = TRUE;
            } else {
                $infoComplete = FALSE;
            }
         unset($course);   
    }
        return $infoComplete;
    }
    
    private function handleRequest() {
        if ( isset($_REQUEST[$this->prereqType . '_addCourseSubmit'])) {
            
            $this->addCourseRequestSubmitted = TRUE;    
        }
        $courseArray = $this->getCourseData($this->prereqType); 
        $nextCourseNum = sizeof($courseArray) + 1;
        $prereqPrintName = ucfirst($this->prereqType) . $nextCourseNum;
        //DebugBreak();
        if (((isset($_REQUEST[$prereqPrintName . '_courseName'])) && $_REQUEST[$prereqPrintName . '_courseName'] != NULL) ||
            ((isset($_REQUEST[$prereqPrintName . '_courseTime'])) && $_REQUEST[$prereqPrintName . '_courseTime'] != NULL) ||
            ((isset($_REQUEST[$prereqPrintName . '_courseInstitution'])) && $_REQUEST[$prereqPrintName . '_courseInstitution'] != NULL) ||
            ((isset($_REQUEST[$prereqPrintName . '_courseName'])) && $_REQUEST[$prereqPrintName . '_courseInstitution'] != NULL)) {
                $sql = "select content from content where name='Placeout Design Course Description' and domain_id= 37";
            $result = mysql_query($sql) or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $formIntro = html_entity_decode($row["content"]);
                }
             //    DebugBreak();
                $this->addNewCourseForm($this->prereqType, $this->prereqType . " " . $nextCourseNum, ucfirst($this->prereqType) . " Course " . $nextCourseNum, $formIntro, $courseId=-1);
            }
        
        return TRUE;
        
    }
    
    
    private function getCourseData($courseType) {
    //    debugBreak();
        $query = "SELECT id FROM mhci_prereqsCourses";
        $query .= " WHERE student_lu_users_usertypes_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND course_type = '{$courseType}' AND application_id = " . $_SESSION['application_id'];
        $query .= " AND period_id = " . $_SESSION['activePlaceoutPeriod'];
        $query .= " order by id ASC";
        //ho $query;
        $result = mysql_query($query);
        
        $idArray = array();
        while ($row = mysql_fetch_array($result)) {            
            $idArray[] = $row['id'];    
        }
        
        return $idArray;       
        
    }
    
    
    private function getSubmittedCourseData() {
        
        $query = "SELECT * FROM mhci_prereqsCourses";
        $query .= " WHERE student_lu_users_usertypes_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND course_type = '{$this->courseType}' AND application_id = " . $_SESSION['application_id'];
        $query .= " AND period_id = " . $_SESSION['activePlaceoutPeriod'];
        $query .= " AND program_id = " . $_SESSION['program_id'];;
        //echo $query;
        $result = mysql_query($query);
        
        $dataArray = array();
        while ($row = mysql_fetch_array($result)) {
            
            $dataArray[] = $row;
            
        }
        $this->submittedCourseData = $dataArray;       
        
    }

    
    private function getUnsubmittedUploadData() {

        $query = "SELECT mhci_prereqsDatafilesStub.*, mhci_prereqsCourseDatafiles.prereq_courses_id";
        $query .= " FROM mhci_prereqsDatafilesStub LEFT OUTER JOIN mhci_prereqsCourseDatafiles";
        $query .= " ON mhci_prereqsDatafilesStub.id = mhci_prereqsCourseDatafiles.datafile_id";
        $query .= " WHERE mhci_prereqsDatafilesStub.user_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND mhci_prereqsDatafilesStub.type = '{$this->documentType}'";
        $query .= " AND mhci_prereqsCourseDatafiles.prereq_courses_id IS NULL";
        //echo $query;
        
        $result = mysql_query($query);   
        
        $dataArray = array();
        $this->newCourseInProgress = FALSE;
        while ($row = mysql_fetch_array($result)) {
            $dataArray[] = $row;
            $this->newCourseInProgress = TRUE;
        }
        $this->unsubmittedUploadData = $dataArray;   
        
    }
    
    
}

?>
