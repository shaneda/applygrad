<?php
$ieltsRecord = $ieltsRecords[0];

$ieltsScoreId = $ieltsRecord['id'];
$ieltsDate = date("m/Y", strtotime($ieltsRecord['testdate']));
$ieltsListeningScore = $ieltsRecord['listeningscore'];
$ieltsReadingScore = $ieltsRecord['readingscore'];
$ieltsWritingScore = $ieltsRecord['writingscore'];
$ieltsSpeakingScore = $ieltsRecord['speakingscore'];
$ieltsOverallScore = $ieltsRecord['overallscore']; 

$receivedChecked = '';
$receivedMessage = '';
$receivedClass = 'confirm';
if ($ieltsRecord['scorereceived'] == 1) {
    $receivedChecked = 'checked';
    $receivedMessage .= ' rcd';
    $receivedClass = 'confirmComplete';
}

echo <<<EOB

<form class="ieltsScore" id="ieltsScore_{$applicationId}">
<table cellpadding="2px" cellspacing="2px" border="0">
<tr>
    <td colspan="2">IELTS&nbsp;&nbsp;
    <input type="checkbox" class="ieltsReceived" 
        id="ieltsReceived_{$applicationId}_{$ieltsScoreId}" {$receivedChecked} /> Received
    </td>
</tr>
<tr>
    <td colspan="2">Test Date: {$ieltsDate}</td>
</tr>
<tr>
    <td>Listening</td>
    <td>
    <input type="text" size="3" class="ielts_listening_score" 
        id="ieltsListeningScore_{$applicationId}_{$ieltsScoreId}" value="{$ieltsListeningScore}" />
    </td>
</tr>
<tr>
    <td>Reading</td>
    <td>
    <input type="text" size="3" class="ielts_reading_score" 
        id="ieltsReadingScore_{$applicationId}_{$ieltsScoreId}" value="{$ieltsReadingScore}">
    </td>
</tr>
<tr>
    <td>Writing</td>
    <td>
    <input type="text" size="3" class="ielts_writing_score" 
        id="ieltsWritingScore_{$applicationId}_{$ieltsScoreId}" value="{$ieltsWritingScore}">
    </td>
</tr>
<tr>
    <td>Speaking</td>
    <td>
    <input type="text" size="3" class="ielts_speaking_score" 
        id="ieltsSpeakingScore_{$applicationId}_{$ieltsScoreId}" value="{$ieltsSpeakingScore}">
    </td>
</tr>
<tr>
    <td>Overall</td>
    <td>
    <input type="text" size="3" class="ielts_overall_score" 
        id="ieltsOverallScore_{$applicationId}_{$ieltsScoreId}" value="{$ieltsOverallScore}">
    </td>
</tr>
<tr>
    <td colspan="3" align="left">
    <input type="submit" class="submitSmall updateIeltsScore" 
        id="updateIeltsScore_{$applicationId}_{$ieltsScoreId}" value="Update IELTS Score"/>
    </td>
</tr>
</table> 
</form>

<script>
    $('#ieltsReceivedMessage_{$applicationId}').html('{$receivedMessage}');
    $('#ieltsReceivedMessage_{$applicationId}').prev("span").attr('class', '{$receivedClass}');    
</script>

EOB;
?>