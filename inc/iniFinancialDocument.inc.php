<?php
include_once "specialCasesApply.inc.php";

foreach($_POST as $key => $value)
{
    if( strstr($key, 'btnDeleteFinancialSupportDoc') !== false )
    {
        $arr = split("_", $key);
        if ($arr[1] == $_SESSION['appid'])
        {
            $userdata = $arr[1] . '_' . $arr[2];
            $filePath = NULL;
            
            $sql1 = "SELECT id FROM datafileinfo 
                WHERE user_id = " . $_SESSION['userid'] . "
                AND section = 27
                AND userdata = '" . $userdata . "'
                LIMIT 1";
            $result1 = mysql_query($sql1);
            while ($row = mysql_fetch_array($result1))
            {
                $filePath = getFilePathRaw(27, $arr[2], $_SESSION['userid'],
                    $_SESSION['usermasterid'], $_SESSION['appid'], $row['id']);    
            }
            
            $sql2 = "DELETE FROM datafileinfo 
                WHERE user_id = " . $_SESSION['userid'] . "
                AND section = 27
                AND userdata = '" . $userdata . "'";
            mysql_query($sql2);
            
            /*
            if ($filePath && file_exists($filePath))
            {
                unlink($filePath);    
            }
            */
        }
    }
}

$financialSupportDocumentsQuery = "SELECT id, moddate, size, extension, userdata 
    FROM datafileinfo 
    WHERE user_id = " . $_SESSION['userid'] . " 
    AND section = 27
    AND userdata LIKE '" . $_SESSION['appid'] . "_%'
    ORDER BY userdata";
$financialSupportDocumentsResult = mysql_query($financialSupportDocumentsQuery);

$financialSupportDocuments = array();
$newFinancialSupportDocumentId = 1;
while($row = mysql_fetch_array($financialSupportDocumentsResult)) 
{
    $userdataArray = split('_', $row['userdata']);
    $documentId = $userdataArray[1];
    $row['document_id'] = $documentId;
    $financialSupportDocuments[] = $row;
    if (($documentId + 1) > $newFinancialSupportDocumentId)
    {
        $newFinancialSupportDocumentId = $documentId;    
    }    
}
?>

<span class="subtitle">Financial Support Letter</span>
<br>
<br>
<?php
    if (isEM2Domain($domainid)) {
?>
Please complete and upload a <a href="EM2_Financial_Support_Letter.pdf" target="_blank">Financial Support Letter</a>
<?php
    } else {
?>
Please complete and upload a <a href="INI_Financial_Support_Letter.pdf" target="_blank">Financial Support Letter</a>
<?php
    }
?>
<br> 
<br>

<?php
if($_SESSION['allow_edit'] == true)
{
    $qs = "?id=" . $newFinancialSupportDocumentId . "&t=27"; 
?>
    <input class="tblItem" name="btnUpload" value="Upload New Financial Support Letter" 
        type="button" onClick="parent.location='fileUpload.php<?php echo $qs; ?>'">
<?php 
}
?>   
<hr size="1">

<?php
$financialDocCounter = 1;
foreach ($financialSupportDocuments as $financialSupportDocument)
{
    /*
    if($_SESSION['allow_edit'] == true)
    {
        $qs = "?id=" . $financialSupportDocument['document_id'] . "&t=27"; 
    ?>
        <input class="tblItem" name="btnDeleteFinancialSupportDoc_<?php echo $financialSupportDocument['userdata']; ?>" 
            value="Delete Financial Support Document <?php echo $financialDocCounter; ?>" 
            type="submit">
    <?php 
    }
    */
    
    if($financialSupportDocument['moddate'] != "") 
    {
        showFileInfo("financialsupport" . $financialDocCounter . "." . $financialSupportDocument['extension'], 
            $financialSupportDocument['size'], formatUSdate2($financialSupportDocument['moddate']), 
            getFilePath(27, $financialSupportDocument['document_id'], 
                $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], 
                $financialSupportDocument['id']));
    }
    
    $financialDocCounter++;    
    ?>
    
    <hr size="1">
    <?php
}
?>

<hr size="1" noshade color="#990000">