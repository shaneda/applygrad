/* Tigra Menu items structure */
var MENU_ITEMS = [
	['Home', '../admin/home.php'],
	['Users', '../admin/sysusers.php'],
	['Data Access', null, null,
		['Page Content', '../admin/content.php'],
        ['Institutes', '../admin/institutes.php']
	],
	['System Environment', '../admin/config.php']
];
