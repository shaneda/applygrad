<?php
/*
* Includes for unit/period.
*/
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php'; 
include_once '../classes/DB_Applyweb/class.DB_Unit.php';
include_once '../classes/DB_Applyweb/class.DB_Period.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodApplication.php'; 
include_once '../classes/class.Unit.php';
include_once '../classes/class.Period.php';


$unitQuery = "SELECT unit_id FROM domain_unit WHERE domain_id = " . $domainid;
$unitResult = mysql_query($unitQuery);
$unitId = NULL;
while ($row = mysql_fetch_array($unitResult)) {
    $unitId = $row['unit_id'];
}
$unit = new Unit($unitId);

// There should only be one active period per domain.
$activePeriod = NULL;
$activePeriodId = NULL;

// Check for active submission.
$activePeriods = $unit->getActivePeriods();
foreach ($activePeriods as $activePeriod) {
    $activePeriodId = $activePeriod->getId();
}

// If submission inactive, check for active edit.
if (!$activePeriodId) {
    $activePeriods = $unit->getActivePeriods(3);
    foreach ($activePeriods as $activePeriod) {
        $activePeriodId = $activePeriod->getId();
    }    
}
?>