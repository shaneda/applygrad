<?php
$prodpattern = "BANSHEE.";
$prodpattern1 = "APPLYGRAD.";
$statspattern = "WEB05.";
$statspattern1 = "APPLY.STAT";
$shibpattern1 = "WEB08";
$shibpattern2 = "WEB24";
$ssnpattern1 = "APPLYGRAD-SSN.";
$dietrichPattern = "APPLYGRAD-DIETRICH";
$iniPattern = "APPLYGRAD-INI";
$web28pattern = "WEB28";
$americanmaidpattern = "AMERICANMAID";
$thetickpattern = "THETICK";
$devpattern = "_DEV";
$testpattern = "_TEST";

$output = php_uname('n');
$hostname = strtoupper($output);

$prodtest1 = stristr($hostname, $prodpattern);
$prodtest2 = stristr($hostname, $prodpattern1);
$stattest1 = stristr($hostname, $statspattern);
$stattest2 = stristr($hostname, $statspattern1);
$ssntest1 =  stristr($hostname, $ssnpattern1);
$shibtest = stristr($hostname, $shibpattern1);
$shibtest2 = stristr($hostname, $shibpattern2);
$dietrichTest = stristr($hostname, $dietrichPattern);
$iniTest = stristr($hostname, $iniPattern);
$web28Test =  stristr($hostname, $web28pattern);
$americanmaidTest =  stristr($hostname, $americanmaidpattern);
$thetickTest =  stristr($hostname, $thetickpattern);

// $currentScriptName =  $_SERVER['SCRIPT_NAME'];
$scriptname = strtoupper( realpath($_SERVER['SCRIPT_FILENAME']) );


// new servers test using path
$newScriptName = strtoupper($_SERVER['SCRIPT_FILENAME']);
$dietrichpath = "APPLYGRADDIETRICH";
$inipath = "APPLYGRADINI";
$designpath = "APPLYGRADDESIGN";

$pathDietrichTest = stristr($newScriptName, $dietrichpath);
$pathIniTest = stristr($newScriptName, $inipath);
$pathDesignTest = stristr($newScriptName, $designpath);

$configIncludeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');

if (($prodtest1 == "BANSHEE.SRV.CS.CMU.EDU") ||  ($prodtest2 == "APPLYGRAD.CS.CMU.EDU")) 
{  
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment 
      //  print "test";    
      include $configIncludeBase . '../inc/config_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include $configIncludeBase . '../inc/config_production.php';
    } 
}
elseif  (($stattest1 == "WEB05.SRV.CS.CMU.EDU") ||  ($stattest2 == "APPLY.STAT.CMU.EDU")) {
   if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
      include $configIncludeBase . '../inc/config_stat_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include $configIncludeBase . '../inc/config_stat_production.php';
    } 
}
elseif  ($shibtest == "WEB08.SRV.CS.CMU.EDU")  {
   if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
      include $configIncludeBase . '../inc/config_shib_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include $configIncludeBase . '../inc/config_shib_production.php';
    } 
}
elseif  ($shibtest2 == "WEB24.SRV.CS.CMU.EDU")  {
  if (stristr($scriptname, $testpattern) !== FALSE) {
    // On production box but in test environment
    //  print "test";
    include $configIncludeBase . '../inc/config_shib_test2.php';
  }
   else
     {
       // this is the production box in production mode
       //     print "production";
       include $configIncludeBase . '../inc/config_shib_production2.php';
     }
}
elseif ($ssntest1 == "APPLYGRAD-SSN.CS.CMU.EDU") {
   if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
      include $configIncludeBase . '../inc/config_ssn_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include $configIncludeBase . '../inc/config_ssn_production.php';
    } 
}
elseif ($dietrichTest == "APPLYGRAD-DIETRICH.CS.CMU.EDU")
{
    
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment   
        include $configIncludeBase . 'config_dietrich_test.php';
    }
    else
    {
        // this is the production box in production mode 
        
        include $configIncludeBase . 'config_dietrich_production.php';
        
    } 
}
elseif (stristr($pathDietrichTest, $dietrichpath) !== FALSE)  
{

    if (stristr($pathDietrichTest, $testpattern) !== FALSE)
    {
        // On production box but in test environment
        include $configIncludeBase . 'config_dietrich_test.php';
    }
    else
    {
        // this is the production box in production mode

        include $configIncludeBase . 'config_dietrich_production.php';

    }
}
elseif ($iniTest == "APPLYGRAD-INI.CS.CMU.EDU")  
{
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment 
          
        include $configIncludeBase . 'config_ini_test.php';
    }
    else
    {
        // this is the production box in production mode
     
        include $configIncludeBase . 'config_ini_production.php';
    } 
}
elseif (stristr($pathIniTest, $inipath) !== FALSE)
{

    if (stristr($pathIniTest, $testpattern) !== FALSE)
    {
        // On production box but in test environment
        include $configIncludeBase . 'config_ini_test.php';
    }
    else
    {
        // this is the production box in production mode

        include $configIncludeBase . 'config_ini_production.php';

    }
}
elseif (stristr($pathDesignTest, $designpath) !== FALSE)  
{

    if (stristr($pathDesignTest, $testpattern) !== FALSE)
    {
        // On production box but in test environment
        include $configIncludeBase . 'config_cfa_test.php';
    }
    else
    {
        // this is the production box in production mode

        include $configIncludeBase . 'config_cfa_production.php';

    }
}
elseif ($web28Test == "WEB28.SRV.CS.CMU.EDU")  
{   
    if (stristr($scriptname, $testpattern) !== FALSE)  {
//        include $configIncludeBase . 'config_dietrich_test.php';
       include $configIncludeBase . '../inc/config_test.php';
    } else {
//         include $configIncludeBase . 'config_dietrich_production.php';
        include $configIncludeBase . '../inc/config_test.php';
    }
    echo $db . "\n";
    echo $db_host   . "\n";
}
elseif ($americanmaidTest == "AMERICANMAID.SRV.CS.CMU.EDU")
{
    if (stristr($scriptname, $testpattern) !== FALSE)  {
//        include $configIncludeBase . 'config_dietrich_test.php';
	   include $configIncludeBase . '../inc/config_test.php';
    } else {
//         include $configIncludeBase . 'config_dietrich_production.php';
	    include $configIncludeBase . '../inc/config_production.php';
    }
    echo $db . "\n";
    echo $db_host   . "\n";
}
elseif ($thetickTest == "THETICK.SRV.CS.CMU.EDU")
{
    if (stristr($scriptname, $testpattern) !== FALSE)  {
//        include $configIncludeBase . 'config_dietrich_test.php';
       include $configIncludeBase . '../inc/config_test.php';
    } else {
//         include $configIncludeBase . 'config_dietrich_production.php';
        include $configIncludeBase . '../inc/config_production.php';
    }
    echo $db . "\n";
    echo $db_host   . "\n";
}
elseif (stristr($scriptname, $devpattern) === FALSE) 
{
              // this is the test environment on development box
            //  print "dev test";
        include $configIncludeBase . 'config_development_test.php';
    }
    else
        {
        // this is a development environment - _dev directory 
         //   print "dev";     
            include $configIncludeBase . 'config_development.php';
        }

?>