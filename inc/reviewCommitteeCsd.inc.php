<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

if ($thisDept == 2) {
    $voteVals = $machineLearningVoteVals;
} elseif ($thisDept == 8) {
    $voteVals = $compBioVoteVals; 
} else {
    $voteVals = $csdVoteVals;    
} 
?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<input name="showDecision" type="hidden" value="<?=$showDecision?>">
<input name="point2" type="hidden" value="<?=$point2?>">
<?php 
if($round == 2){
?>
    <input name="round2" type="hidden" value="<?=$round2?>">
<?php 
} 

if ($previousRoundData) {
    echo '<span style="font-size: 12px; font-weight: bold; font-style: italic;">';
    echo 'You reviewed this application in round ' . $reviewRound . '</span>';    
    echo '<br/><br/>';
}

include "../inc/special_phone_email.inc.php";

$allowEdit = TRUE;
$previousRoundData = FALSE;
for($i = 0; $i < count($committeeReviews); $i++) {
    if($committeeReviews[$i][29] == "")
        {
            echo "<strong>". $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            $committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
            for($j = 0; $j < count($committeeReviewsSeminars); $j++)
            {
                $idx = $committeeReviewsSeminars[$j][0];
                if($idx == "")
                {
                    $idx = 0;
                }else
                {
                    $idx = $idx  -1;
                }
                //echo $committeeReviewsSeminars[$j][0]. " ";
                if(marray_search($idx, $ltiSeminars) !== false)
                {
                    echo $ltiSeminars[ $idx ][1]. " ";
                }
                
            }
            
            echo "<br>";
        }
    if( $committeeReviews[$i][4] == $reviewerId 
        && $committeeReviews[$i][29] == "" ) 
    {   
        $comments = $committeeReviews[$i][9];
        $private_comments = $committeeReviews[$i][12];
        $point = $committeeReviews[$i][10];
        $pointCertainty = $committeeReviews[$i][27]; 

        if ($committeeReviews[$i][23] != $round) 
        {
            $previousRoundData = TRUE;
            $reviewRound = $committeeReviews[$i][23];
            $allowEdit = FALSE;
        }
    } 
}
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

    <tr>
        <td width="50"><strong>Comments:</strong></td>
        <td>
        <?php
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 80); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea); 
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Personal Comments: </strong> </td>
        <td>
        <?php
        ob_start();
        showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 40); 
        $personalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $personalCommentsTextarea); 
        ?>    
        </td>
    </tr>

    <tr valign="middle">
        <td class="label">Add Interest:</td>
        <td>
        <div style="width: 25%; margin-bottom: 10px; float: left;">
        <?php 
        if ($previousRoundData) {
            foreach ($committeeReviewsSeminars as $committeeReviewsSeminar) {
                $committeeReviewsSeminarId = $committeeReviewsSeminar[0];
                foreach ($csdAois1 as $csdAoi) {
                    if ($csdAoi[0] == $committeeReviewsSeminarId) {
                        echo $csdAoi[1] . ' ';
                    }
                }
            }
        } else {
            showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $csdAois1);
        }
        ?>
        </div>
        <div style="width: 25%; margin-bottom: 10px; float: left;">
        <?php 
        if ($previousRoundData) {
            foreach ($committeeReviewsSeminars as $committeeReviewsSeminar) {
                $committeeReviewsSeminarId = $committeeReviewsSeminar[0];
                foreach ($csdAois2 as $csdAoi) {
                    if ($csdAoi[0] == $committeeReviewsSeminarId) {
                        echo $csdAoi[1] . ' ';
                    }
                }
            }
        } else {
            showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $csdAois2);
        }
        ?>
        </div>
        <div style="width: 25%; margin-bottom: 10px;  float: left;">
        <?php 
        if ($previousRoundData) {
            foreach ($committeeReviewsSeminars as $committeeReviewsSeminar) {
                $committeeReviewsSeminarId = $committeeReviewsSeminar[0];
                foreach ($csdAois3 as $csdAoi) {
                    if ($csdAoi[0] == $committeeReviewsSeminarId) {
                        echo $csdAoi[1] . ' ';
                    }
                }
            }
        } else {
            showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $csdAois3);
        }
        ?>
        </div>
        <div style="width: 25%; margin-bottom: 10px;  float: left;">
        <?php 
        if ($previousRoundData) {
            foreach ($committeeReviewsSeminars as $committeeReviewsSeminar) {
                $committeeReviewsSeminarId = $committeeReviewsSeminar[0];
                foreach ($csdAois4 as $csdAoi) {
                    if ($csdAoi[0] == $committeeReviewsSeminarId) {
                        echo $csdAoi[1] . ' ';
                    }
                }
            }
        } else {
            showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $csdAois4);
        }
        ?>
        </div>
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Ranking:</strong></td>
        <td>
        <?php
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $voteVals);
        ?>
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Certainty:</strong></td>
        <td>
        <?php
        showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals); 
        ?>
        </td>
    </tr>
    
<?php 
if($round  == 1 ) { 
?>
  <tr>
    <td width="50"><strong> Round 2: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $round2 = $committeeReviews[$i][13];
        }
     }

    showEditText($round2, "radiogrouphoriz2", "round2", $allowEdit, false, array(array(1,"Yes"),array(0,"No")) ); ?>    </td>
  </tr>
 
<?php 
} 
?>

</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
