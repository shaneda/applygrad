<?php

class MHCI_PrereqsProgrammingTestForm
{

    // Constructor parameters.
    private $studentLuUsersUsertypesId;
    private $view;                      // "student" || "reviewer"
    
    // Fields set in constructor.
    private $prereqPrefix;
    private $prereqName;    
    
    // Fields for form/db data.
    private $testId;
    private $documentType = "ProgrammingTest";
    private $downloadTimestamp;
    private $uploadDatafileId;
    private $uploadInfo = array();      // 2D array filled from db query.

    // Fields for display elements.
    private $testDownloaded = FALSE;
    private $formHeading = "Placement Test";
    private $downloadButtonLabel = "Download programming test";
    private $uploadButtonLabel = "Upload completed program";
    private $downloadButtonDisabled = "";
    private $uploadButtonDisabled = "disabled";
    
     
    public function __construct($studentLuUsersUsertypesId, $view) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;
        $this->prereqPrefix = "programmingTest";
        $this->prereqName = "Placement test";
        
    }
    
    public function getUploadInfoFromObject() {
        return $this->uploadInfo;
    }
    
    public function getDownloadTimestamp() {
        return $this->downloadTimestamp;
    }
    
    public function render() {
   //     DebugBreak();
        $this->renderHeading();
        $this->getTest();
        if (isset($this->downloadTimestamp)) {
            $this->testDownloaded = TRUE;
            $this->downloadButtonDisabled = "disabled";
        }
        $this->getUploadInfo();
        $this->handleRequest();
        
        if ($this->view == "reviewer") {

            $this->renderReviewer();
            
        } else {
            
            $this->renderStudent();
            
        }
            
        return TRUE;
                    
    }
    
    
     
    
    private function renderStudent() {
    
        $this->renderIntro();
        
        if ($this->testDownloaded) {
            
            // A test download has been requested.
            // The download will be handled with javascript that opens a new window.
            // The script that outputs the test will insert the test record into the db on the first download request.
            // Otherwise, it will just display the test.
            // The assumption at this point is that there have been no problems with the download.
            
            //echo "download, no upload<br /><br />"; 
            
            // Wait a second for the test record to get inserted by the other script.
            sleep(1);
            
            // Get the test & upload records from the db.
            $this->getTest();
            $this->getUploadInfo();
            
            // Set the upload button to be enabled.
            $this->uploadButtonDisabled = '';

            
        } else {
            
            // Check whether it's a post-upload request.
       //      DebugBreak();
            // Get the test data from the db.
            $this->getTest();
            if (isset($this->downloadTimestamp)) {
                $this->testDownloaded = TRUE;
                $this->downloadButtonDisabled = "disabled";
                }
            
            if ($this->uploadDatafileId){
                
                // A new file has been uploaded.
                echo "upload complete<br /><br />";
                
                // Get the upload info.
                $this->getUploadInfo();
                
            } else {
                
                if ($this->testId) {
                    
                    // The test has been downloaded but not uploaded
                    echo "download, no upload<br /><br />";
                    
                    // Get the upload info.
                    $this->getUploadInfo();
                    
                    // Set the upload button to be enabled.
                    $this->uploadButtonDisabled = '';
                    
                } else {
                    
                    // This form subset has not been started.
               //     echo "no download<br /><br />";
                       
                }              
   
            }
            
            
        }
        
        // Render the buttons and timestamp info.
        $this->renderDownloadButton();
        $this->renderUploadButton();
                           
    }


    private function renderReviewer() {
        
        // Get the data from the db.
        $this->getTest();
        $this->getUploadInfo();
        
        // Render the timestamp info.
        echo '<div class="bodyText-italics">';
        $this->renderDownloadTime();
        echo "<br/>";
        $this->renderUploadTime();
        echo '</div><br/><div class="bodyText">';
        $this->renderUploadLink();
        echo "</div><br/>";        
        
    }
    
    
    private function handleRequest() {
   //     DebugBreak();
        if ( isset($_REQUEST[$this->prereqPrefix . '_downloadTest']) ) {
            
            $this->testDownloaded = TRUE;
            
        }
        
        return TRUE;
        
    }
    
    
    private function getTest() {
    
        $query = "SELECT * FROM mhci_prereqsProgrammingTests
                    WHERE mhci_prereqsProgrammingTests.student_lu_users_usertypes_id = " 
                    . $this->studentLuUsersUsertypesId;
        //echo $query . "<br />";
        $result = mysql_query($query);   
        
        while ($row = mysql_fetch_array($result)) {
            
            $this->testId = $row['id'];
            $this->downloadTimestamp = $row['download_timestamp'];
            $this->uploadDatafileId = $row['upload_datafileinfo_id'];
            
        }
        
    }

    
    private function addTest() {

        $query = "INSERT INTO mhci_prereqsProgrammingTests
                    (student_lu_users_usertypes_id, download_timestamp)
                    VALUES ({$this->studentLuUsersUsertypesId}, {$this->downloadTimestamp})";

        $result = mysql_query($query);
        
        return mysql_insert_id();        
           
    }
    
    
    private function getUploadInfo() {
    
        $query = "SELECT datafileinfo.*, lu_users_usertypes.user_id AS usermasterid
                    FROM datafileinfo
                    INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id";

        if ($this->uploadDatafileId) {
        
            $query .= " WHERE datafileinfo.id = {$this->uploadDatafileId}";
                
        } else {
            
            $query .= " WHERE datafileinfo.user_id = {$this->studentLuUsersUsertypesId}
                        AND datafileinfo.section = 18";
            
        }

        $query .= " ORDER BY datafileinfo.moddate DESC";
        //echo $query . "<br />";
        
        $result = mysql_query($query);   
        
        while ($row = mysql_fetch_array($result)) {
            
            $this->uploadInfo[] = $row;
            
        }
        
    }
    
    
    private function renderHeading() {
        
        echo '<div class="sectionTitle">' . $this->formHeading . '</div><br/><br/>'; 

    }


    private function renderIntro() {
        
        echo <<<EOB

        <div class="bodyText">
        You can fulfill the programming placeout by writing a program in any language 
        in a 48-hour contiguous period of time. The system will record the time when you 
        download the test. You must submit your program within 48 hours for it to be accepted. 
        The reviewer will update your placeout status after reviewing your solution.
        </div><br/>

EOB;
 
    }
    
    
    
    
    private function renderDownloadButton() {
   //     DebugBreak();
        echo <<<EOB
                
        <div class="bodyText-italics">
        <input type="submit" id="{$this->prereqPrefix}_downloadTest" name="{$this->prereqPrefix}_downloadTest" class="bodyButton" 
        value="{$this->downloadButtonLabel}" {$this->downloadButtonDisabled} onClick="window.open('mhci_programmingTest.php');" />

EOB;

        $this->renderDownloadTime();
        
        echo "</div><br/><br/>";
    
    }
    
    
    private function renderUploadButton() {
        
        $currentUnixTimestamp = strtotime("now");
        $downloadUnixTimestamp = strtotime($this->downloadTimestamp);
        $expirationUnixTimestamp = $downloadUnixTimestamp + 172800; // download time + 48 hours 
        
        if ($this->uploadDatafileId  || ($currentUnixTimestamp > $expirationUnixTimestamp)) {
        
            // The completed program has been uploaded or time has expired.
            // Disable the upload button.    
            $this->uploadButtonDisabled = 'disabled="disabled"';
            
        }
        
        echo <<<EOB
        
        <div class="bodyText-italics">
        <button id="{$this->prereqPrefix}_uploadDoc" class="bodyButton" 
        onClick="parent.location='../apply/fileUpload.php?id={$this->testId}&t=18';return false;" 
        {$this->uploadButtonDisabled} >{$this->uploadButtonLabel}</button> 

EOB;

        if ($this->uploadDatafileId) {
        
            // The file has been uploaded; show the upload time.
            $this->renderUploadTime();
            
        } else {
        
            if ( isset($this->downloadTimestamp) ) {
            
                if ($currentUnixTimestamp > $expirationUnixTimestamp) {
                
                    echo "&nbsp;&nbsp;Time has expired.";
                    
                } else {
                    
                    echo "&nbsp;&nbsp;Time expires " . date('Y-m-d H:i:s', $expirationUnixTimestamp); 
                    
                } 
            }    
            
        } 

        echo "</div>";
        
        //echo "current time: " . date('Y-m-d H:i:s', $currentUnixTimestamp);
        
    }  

    
    private function renderDownloadTime() {
        
        if ($this->downloadTimestamp) {
            
            echo "&nbsp;&nbsp;Downloaded on " . $this->downloadTimestamp;    
            
        }
   
    }
    
    
    private function renderUploadTime() {
        
        if ($this->uploadDatafileId) {
        
            echo "&nbsp;&nbsp;Uploaded on " . $this->uploadInfo[0]['moddate'];    
            
        }
            
    }
    
    
    private function renderUploadLink() {
        
        if ( isset($this->uploadInfo[0]) ) {
            
            $uploadArray = $this->uploadInfo[0];
            $studentGuid = getGuid($uploadArray['usermasterid']);
            $pathFilename = $this->documentType . "_" . $uploadArray['userdata'] . "." . $uploadArray['extension'];
            $displayFilename = $this->documentType . "." . $uploadArray['extension'];
            $path = $_SESSION['datafileroot'] . "/" . $studentGuid . "/" . $this->documentType . "/" . $pathFilename;
        
            echo <<<EOB

            &nbsp;&nbsp;<a href="" onClick="window.open('{$path}'); return false;">{$displayFilename}</a>
EOB;


        } 
    }

    
} 

?>