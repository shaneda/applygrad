<!--BEGIN MAIN TABLE -->
<!-- REC TABLE -->
<table width="650" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td><span class="subtitle">Language Proficiency Recommender <?=$submitted?></span> </td>
    <td align="right">
<!-- Display DEL Button only after this recommender has been saved -->
<?      // If the recommender in question has been created, that means its been saved
//echo $i . " " . $NUMRECS;
        if ($allowEdit == true)
        {
        showEditText("Delete", "button", "btnDelete_".$myLangProfRecommender[0], $_SESSION['allow_edit']); 
        }
?>
    <input name="txtRecord_<?=$myLangProfRecommender[0];?>" type="hidden" id="txtRecord_<?=$myLangProfRecommender[0];?>" value="<?=$myLangProfRecommender[0]?>" />
    <input name="txtUid_<?=$myLangProfRecommender[0];?>" type="hidden" id="txtUid_<?=$myLangProfRecommender[0];?>" value="<?=$myLangProfRecommender[1]?>" />
    <input name="txtUMasterid_<?=$myLangProfRecommender[0];?>" type="hidden" id="txtUMasterid_<?=$myLangProfRecommender[0];?>" value="<?=$myLangProfRecommender[9];?>">

    <input name="txtRecSent_<?=$myLangProfRecommender[0];?>" type="hidden" id="txtExtension_<?=$myLangProfRecommender[0];?>" value="<?=$myLangProfRecommender[8];?>">

</td>
  </tr>
</table>


<table border=0 cellpadding=1 cellspacing=1 class="tblItem" width="650">
<? 
    echo '<tr><td colspan="3">';
    showEditText("1a", "hidden", "lbType", $_SESSION['allow_edit'],true, null);
    echo '</td></tr>';
?>
<tr>
<td width="216"><strong>First Name:</strong><br>
<? showEditText($myLangProfRecommender[2], "textbox", "txtFname", $allowEdit,true,null,true,20); ?></td>
<td width="216"><strong>Last Name:</strong><br>
<? showEditText($myLangProfRecommender[3], "textbox", "txtLname", $allowEdit,true,null,true,20); ?></td>
<td width="218"><strong>Job Title:</strong><br>
<? 
if (isset($myLangProfRecommender[11]) && $myLangProfRecommender[11] != ''){
    $langProfRecTitle = $myLangProfRecommender[11];
} else {
    $langProfRecTitle = $myLangProfRecommender[4];    
}
showEditText($langProfRecTitle, "textbox", "txtTitle", $allowEdit,true,null,true,20); 
?>
</td>
</tr>
<tr>
<td><strong>Company/Institution:</strong><br>
<? 
if (isset($myLangProfRecommender[12]) && $myLangProfRecommender[12] != ''){
    $langProfRecAffiliation = $myLangProfRecommender[12];
} else {
    $langProfRecAffiliation = $myLangProfRecommender[5];    
}
showEditText($langProfRecAffiliation, "textbox", "txtAffiliation", $allowEdit,true,null,true,40); 
?>
</td>
<td><strong>Email:</strong><br>
<? showEditText($myLangProfRecommender[6], "textbox", "txtEmail", $allowEdit,true,null,true,30); ?> </td>
<td><strong>Phone:</strong><br>
<? 
if (isset($myLangProfRecommender[13]) && $myLangProfRecommender[13] != ''){
    $langProfRecPhone = $myLangProfRecommender[13];
} else {
    $langProfRecPhone = $myLangProfRecommender[7];    
}
showEditText($langProfRecPhone, "textbox", "txtPhone", $allowEdit,true,null,true,20,20); 
?>
</td>
</tr>
<tr>
<td width="216" colspan=3><strong>Language for recommender to evaluate your skills:</strong><br>
<? showEditText($myLangProfRecommender[15], "textbox", "txtLangOfInterest", $allowEdit,true,null,true,40); ?></td>
</tr>
<tr>
<td colspan=3>
<? 
// If recommendation already received, do not show any buttons 
if($myLangProfRecommender[14] == 0)
{   
if ($myLangProfRecommender[2] != ""  
    && ($submitted != "(Language Proficiency Report Received)"))
{    
    //if ( $_SESSION['domainname'] != 'RI-MS-RT' ) {
    
        if($myLangProfRecommender[1] != "" && $myLangProfRecommender[8] > 0)  { ?>
        <p><b>
         Send email reminder to language proficiency recommender.</b> 
        <? showEditText("Send Reminder", "button", "btnSendReminder_".$myLangProfRecommender[0], $_SESSION['allow_edit'],false); 
        }else{
        
        ?>
        <p><b>
         Send email request to language proficiency recommender.</b> <? showEditText("Send Request", "button", "btnSendmail_".$myLangProfRecommender[0], $_SESSION['allow_edit'],false); 
         
        } 
}
}
?>
</td>
</tr>
</table>
<!--END MAIN TABLE -->
<hr size="1" noshade color="#990000">
