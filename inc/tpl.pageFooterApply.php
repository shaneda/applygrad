            <br>
            <div style="float:left;"> 
            <?php
            if ( isset($prevPage) && ($prevPage != "" ) ) 
            {
               echo  "&nbsp;&nbsp;<a href=\"" . $prevPage . "\">< Previous </a>"; 
            }
            if (isset($nextPage) && ($nextPage != "") )
            {
                echo "&nbsp; | &nbsp;";
                echo "<a href=\"" . $nextPage . "\">Next ></a>"; 
            }
            ?>
            </div>
            <br>
                
            </div> <!-- end contentDiv -->

            <br>
            <div class="tblItem">
            &nbsp;&copy; <?php echo date("Y"); ?> Carnegie Mellon University
            <?php 
            if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false ){ 
            ?>
            &middot; <a href="../apply/contact.php">Report a Technical Problem</a>
            <?php
            }
            ?>
            </div>

            </div> <!-- end main (width 660) div -->
            
        </td>
    </tr>
</table>

</form>

<?php
if ( isset($defaultJavascriptFiles) ) { 
    foreach ($defaultJavascriptFiles as $javascriptFile) {   
        echo <<<EOB
        <script type="text/javascript" src="{$javascriptFile}"></script>\n    
EOB;
        }
}
 
if ( isset($footJavascriptFiles) ) { 
    foreach ($footJavascriptFiles as $footJavascriptFile) {   
        echo <<<EOB
        <script type="text/javascript" src="{$footJavascriptFile}"></script>\n    
EOB;
    }
}    
?>

</body>
</html>