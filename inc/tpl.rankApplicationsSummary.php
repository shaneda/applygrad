<div id="#content" style="clear: both; height: 100%;">
<?php
foreach($groups as $group) {
    echo makeGroupDiv($group) . "\n\n";
}  
?>
</div>

<div style="clear: both;"></div>
<br><br>

<?php
function makeGroupDiv($group) {
    
    $groupId = $group['group_id'];
    $groupName = $group['group_name'];
    $applications = $group['applications'];
    $applicationCount = count($applications);
    $coordinatorName = $group['coordinator_name'];
    $coordinatorComment = $group['coordinator_comment'];
    $commentDate = $group['comment_date'];
    
    $coordinatorDisplay = '';
    if ($coordinatorComment) {
        $coordinatorDisplay = $commentDate . ', ' . $coordinatorName . '<br>';
        $coordinatorDisplay .= str_replace("\n", '<br>', $coordinatorComment);    
    }
    
    $div = <<<EOB
        <div id="group_{$groupId}" class="draggable" >
        <div class="draggable_handle">
            <b>{$groupName} <i>({$applicationCount})</i></b>
        </div>
        <p style="margin: 0px 0px 5px 5px;">{$coordinatorDisplay}</p>
        <ol style="margin-top: 10px; clear: left;">
EOB;
    foreach ($applications as $application) {
        $div .= makeLi($application) ."\n";
    }
    $div .= <<<EOB
        </ol>
        <br><br><br>
        </div>
EOB;

    return $div;
}
?>