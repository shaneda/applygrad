/* Tigra Menu template structure */
var MENU_TPL = [
	{
		'width': 200,
		'height': 24,
		'left': 199,
		'top': 0,
		'hide_delay': 200,
		'expd_delay': 200,
		'css': {
			'outer' : ['m0l0oout', 'm0l0oover'],
			'inner' : ['m0l0iout', 'm0l0iover']
		},
		'block_left': 10,
		'block_top': 85
	},
	{
		/*'width': 120,*/
		'block_left': 50,
		'block_top': 25,
		'left': 0,
		'top': 23,
		'css': {
			'outer' : ['m0l1oout', 'm0l1oover'],
			'inner' : ['m0l1iout', 'm0l1iover']
		}
	}
];
