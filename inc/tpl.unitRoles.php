<?php
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Unit Roles';

$pageCssFiles = array(
    '../css/units.css',
    '../css/jquery.autocomplete.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.autocomplete.min.js',
    '../javascript/userIndex.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<br/>
<div id="bread" class="menu">
<ul>
    <li class="first"><a href="home.php">Home</a>
    <ul>
        <li>&#187; <a href="units.php?unit_id=<?php echo $unitId; ?>">Units</a> 
        <ul>
            <li>&#187;</li>
        </ul>
        </li>
    </ul>
    </li>
</ul>
</div>
<div id="unitName"><?php echo $unit->getName(); ?></div>
<div id="pageTitle"><?php echo $pageTitle; ?></div>


<div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%">
<?php
//DebugBreak();
$roleMenu = makeRoleMenu($unit, $roleId);
echo $roleMenu;
?>
</div>

<div id="table" style="margin: -5px 0px 0px 20px; float: left; clear: right; width: 75%;">

        <div style="margin: 10px;">
        <?php
        if (!$allowAdmin) {
        
            echo '<p>You are not authorized to manage admission periods.</p>';
            
        } else {
        
            if ($roleId == 1) {
                $userHeading = 'Administrators';
            } else {
                $userHeading = $reviewRoles[$roleId];    
            }    
            
        ?>
        
            <table width="100%" border="0" cellspacing="5" cellpadding="15">
            <tr valign="top">
            <td width="50%">

            <div class="legend"><?php echo $userHeading; ?></div>         
            <div class="outer">
            <div class="fieldset">  
            <?php    
            $userIndexAction = "";
            $userIndexType = "scsUser";
            $userIndexHiddenInputs = array(
                'unit_id' => $unitId,
                'role_id' => $roleId
                );
            include '../inc/tpl.userIndexForm.php';
            }
            ?>
            <!--
            <form id="myForm" name="myForm" action="" method="GET"> 
            <input type="text" id="suggest" name="searchString" size="40"/> 
            <input type="hidden" id="searchUserId" name="search_user_id" />
            <?php
            foreach ($userIndexHiddenInputs as $name => $value) {
                echo '<input type="hidden" name="' . $name . '" value="' . $value . '" />'; 
            }
            ?>
            <input type="submit" id="submitUserId" value="Add User" /> 
            </form>
            -->
            <br/>
            <div style="background-color: white;">
            <table  cellspacing="1" cellpadding="4" width="100%">
            <?php
            foreach ($roleUsers as $roleUser) {
                echo <<<EOB
                <tr valign="top" bgcolor="#EEEEEE" onMouseOver="style.backgroundColor='white';" 
                    onMouseOut="style.backgroundColor='#EEEEEE';">
                <td class="menu">
                <a href="sysusersEdit.php?id={$roleUser['users_id']}">
                {$roleUser['firstname']} {$roleUser['lastname']}</a>
                </td>
                <td>{$roleUser['email']}</td>
                <td>
                <a href="?unit_id={$unitId}&role_id={$roleId}&remove_user_id={$roleUser['users_id']}">
                Remove</a>
                </td>
                </tr>
EOB;
            }
            ?>
            </table>
            </div>
             
            </div>
            </div>       

            </td>

            <td width="50%">
            
            <!--
            <div class="legend">??</div>         
            <div class="outer">
            <div class="fieldset">            
            <?php
            //$programGroupForm->display();
            ?>
            </div>
            </div>            
            -->
            
            </td>
            </tr>
            </table>
        
        </div>
</div>

<div style="clear: both;"></div>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';


//########################################################################

function makeRoleMenu($unit, $selectedRoleId = NULL) {
    
    $groupId = $unit->getId(); 

    global $reviewRoles;
    
    $menu = '<ul class="menu menu-root" style="list-style-type: none;">';
    
    $roleAnchor = '<a title="(role id: 1)"';
    if ($selectedRoleId == 1) {
        $roleAnchor = '<div class="selected">' . $roleAnchor . '>';    
    } else {
        $roleAnchor = '<div>' . $roleAnchor;
        $roleAnchor .= ' href="?unit_id=' . $groupId . '&role_id=1">';
    }
    $roleAnchor .= 'Administrators</a></div>';    
    $menu .= '<li>' . $roleAnchor . '</li>';
    
    $roleAnchor = '<a title="(role id: 4)"';
    if ($selectedRoleId == 4) {
        $roleAnchor = '<div class="selected">' . $roleAnchor . '>';    
    } else {
        $roleAnchor = '<div>' . $roleAnchor;
        $roleAnchor .= ' href="?unit_id=' . $groupId . '&role_id=4">';
    }
    $roleAnchor .= 'Faculty</a></div>';    
    $menu .= '<li>' . $roleAnchor . '</li>';
    
    /*
    if ( $programGroup->isReviewGroup() ) {
    
        foreach ($reviewRoles as $roleId => $role) {
        
            $roleAnchor = '<a title="(role id: ' . $roleId . ')"'; 
            if ($selectedRoleId == $roleId) {
                $roleAnchor = '<div class="selected">' . $roleAnchor . '>';    
            } else {
                $roleAnchor = '<div>' . $roleAnchor;
                $roleAnchor .= ' href="?unit_id=' . $groupId . '&role_id=' . $roleId . '">';
            }    
            $roleAnchor .= $role . '</a></div>';
            $menu .= '<li>' . $roleAnchor . '</li>';            
        }        
    }
    */
    
    $menu .= '</ul>';
    
    return $menu;  
}

?>
