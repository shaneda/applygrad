<?php
include_once '../classes/class.Department.php';
include_once '../inc/specialCasesAdmin.inc.php';

// Initialize variables
$allowEdit = $_SESSION['A_allow_admin_edit'];
$includeSemiblind = TRUE;
$semiblind_review = 0;
$semiblindByDefault = 0;
$showDecision = FALSE;
$reviewerId = intval($_SESSION['roleLuuId']);
$departmentId = $thisDept;  // $thisDept is set in reviewApplicationSingle.php

$decisionVals = array(
    array('A1', 'A1'),
    array('A2', 'A2'),
    array('B1', 'B1'),
    array('B2', 'B2'),
    array('S', 'S'),
    array('R', 'R')
);

// TEMPORARY!!!!!
$committeeReviews = array();

$round = filter_input(INPUT_GET, 'r', FILTER_VALIDATE_INT);
if (!$round)
{
    filter_input(INPUT_POST, 'round', FILTER_VALIDATE_INT);    
}

$applicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT);
if (!$applicationId)
{
    filter_input(INPUT_POST, 'applicationId', FILTER_VALIDATE_INT);    
}

$iniReviewId = NULL;
$technicalComments = '';
$technicalRating = NULL;
$academicComments = '';
$ugProgramComments = '';
$academicRating = NULL;
$researchComments = '';
$researchRating = NULL;
$workExperienceComments = '';
$workExperienceRating = NULL;
$leadershipComments = '';
$leadershipRating = NULL;
$additionalComments = '';
$overallRating = NULL;
$alternativeProgramComments = '';
$alternativeProgram = NULL;
$adminComments = ''; 
$committeeReviews = array();
$adminReviews = array();
$moellerData = array();

$saveError = '';

// Set semiblind
setSemiblind();

// Save any changes
if(isset($_POST['btnSubmit']))
{
    saveReview();
}

if(isset($_POST["btnSubmitFinal"]))
{ 
    saveFinal();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$saveError)
{
    getReview();
}

getCommitteeReviews();
getAdminReviews();
getMoellerData(); 

function setSemiblind()
{
    global $showDecision;
    global $departmentId;
    global $semiblind_review;
    
    if(isset($_POST['showDecision']))
    {
        if($_POST['showDecision'] == 1)
        {
            $showDecision = true;
        }
    }
    
    if(isset($_GET['showDecision']))
    {
        if($_GET['showDecision'] == 1)
        {
            $showDecision = true;
        }
    }

    if($showDecision == false)
    {
        $sql = "select name, semiblind_review from department where id = " . $departmentId;
        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result ))
        {
            $semiblindByDefault = intval($row['semiblind_review']);
            if(!isset($_POST['btnSemiblind']) && !isset($_POST['chkSemiblind']))
            {
                $semiblind_review = intval($row['semiblind_review']);
            }
        }
        
        if(isset($_POST['btnSemiblind']))
        {
            if(intval($_POST['chkSemiblind']) == 1)
            {
                $semiblind_review = 0;
            }
            else
            {
                $semiblind_review = 1;
            }
        }
        else
        {
            if(isset($_POST['chkSemiblind']))
            {
                $semiblind_review = intval($_POST['chkSemiblind']);
            }
        }
    }
}

function getReview()
{
    if (hasAdminRole())
    {
        return getAdminReview();
    }
    
    global $iniReviewId;
    global $applicationId;
    global $reviewerId;
    global $technicalComments;
    global $technicalRating;
    global $academicComments;
    global $ugProgramComments;
    global $academicRating;
    global $researchComments;
    global $researchRating;
    global $workExperienceComments;
    global $workExperienceRating;
    global $leadershipComments;
    global $leadershipRating;
    global $additionalComments;
    global $overallRating;
    global $alternativeProgramComments;
    global $alternativeProgram;
    
    $query = "SELECT * FROM review_ini 
        WHERE application_id = " . $applicationId . "
        AND reviewer_id = " . $reviewerId . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniReviewId = $row['id'];
        $technicalComments = $row['technical_comments'];
        $technicalRating = $row['technical_rating'];
        $academicComments = $row['academic_comments'];
        $ugProgramComments = $row['ug_program_comments'];
        $academicRating = $row['academic_rating'];  
        $researchComments = $row['research_comments'];
        $researchRating = $row['research_rating'];
        $workExperienceComments = $row['work_experience_comments'];
        $workExperienceRating = $row['work_experience_rating'];
        $leadershipComments = $row['leadership_comments'];
        $leadershipRating = $row['leadership_rating'];
        $additionalComments = $row['additional_comments']; 
        $overallRating = $row['overall_rating']; 
        $alternativeProgramComments = $row['alternative_program_comments']; 
        $alternativeProgram = $row['alternative_program'];        
    }    
}

function getAdminReview()
{
    global $iniReviewId;
    global $applicationId;
    global $reviewerId;
    global $adminComments;
    
    $query = "SELECT * FROM review_ini_admin 
        WHERE application_id = " . $applicationId . "
        AND reviewer_id = " . $reviewerId . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniReviewId = $row['id'];
        $adminComments = $row['comments'];     
    }    
}  

function getCommitteeReviews()
{
    global $applicationId;
    global $departmentId;
    global $committeeReviews;
    
    $query = "SELECT review_ini.*, users.lastname, review_ini_alternative_program.value as alternative_program_name 
        FROM review_ini 
        INNER JOIN lu_users_usertypes 
            ON review_ini.reviewer_id = lu_users_usertypes.id
        INNER JOIN users
            ON lu_users_usertypes.user_id = users.id
        LEFT OUTER JOIN review_ini_alternative_program
            ON review_ini.alternative_program = review_ini_alternative_program.id
        WHERE application_id = " . $applicationId . "
        AND department_id = " . $departmentId;
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $committeeReviews[] = $row;
    }
}

function getAdminReviews()
{
    global $applicationId;
    global $departmentId;
    global $adminReviews;
    
    $query = "SELECT review_ini_admin.*, users.lastname 
        FROM review_ini_admin 
        INNER JOIN lu_users_usertypes 
            ON review_ini_admin.reviewer_id = lu_users_usertypes.id
        INNER JOIN users
            ON lu_users_usertypes.user_id = users.id
        WHERE application_id = " . $applicationId . "
        AND department_id = " . $departmentId;
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $adminReviews[] = $row;
    }
}

function getMoellerData()
{
    global $applicationId;
    global $moellerData;
    
    $moellerData['score'] = $moellerData['rank'] = NULL;
    $moellerData['n_count'] = $moellerData['n_group'] = NULL;
    
    $query = "SELECT * FROM moeller_data WHERE id = " . $applicationId;
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result))
    {
        $moellerData = $row;
    }
}

// TEMPORARY!!!!!!!!!!
function getCoordinatorComments()
{
    return array();
}

function saveReview()
{
    if (hasAdminRole())
    {
        return saveAdminReview();
    }
    
    global $iniReviewId;
    global $applicationId;
    global $reviewerId;
    global $departmentId;
    global $round;
    global $technicalComments;
    global $technicalRating;
    global $academicComments;
    global $ugProgramComments;
    global $academicRating;
    global $researchComments;
    global $researchRating;
    global $workExperienceComments;
    global $workExperienceRating;
    global $leadershipComments;
    global $leadershipRating;
    global $additionalComments;
    global $overallRating;
    global $alternativeProgramComments;
    global $alternativeProgram;
    global $saveError; 
    
    $iniReviewId = filter_input(INPUT_POST, 'iniReviewId', FILTER_VALIDATE_INT);
    $applicationId = filter_input(INPUT_POST, 'applicationId', FILTER_VALIDATE_INT);
    $technicalComments = filter_input(INPUT_POST, 'technicalComments', FILTER_SANITIZE_STRING);
    $technicalRating = filter_input(INPUT_POST, 'technicalRating', FILTER_VALIDATE_INT);
    $academicComments = filter_input(INPUT_POST, 'academicComments', FILTER_SANITIZE_STRING);
    $ugProgramComments = filter_input(INPUT_POST, 'ugProgramComments', FILTER_SANITIZE_STRING);
    $academicRating = filter_input(INPUT_POST, 'academicRating', FILTER_VALIDATE_INT);
    $researchComments = filter_input(INPUT_POST, 'researchComments', FILTER_SANITIZE_STRING);
    $researchRating = filter_input(INPUT_POST, 'researchRating', FILTER_VALIDATE_INT);
    $workExperienceComments = filter_input(INPUT_POST, 'workExperienceComments', FILTER_SANITIZE_STRING);
    $workExperienceRating = filter_input(INPUT_POST, 'workExperienceRating', FILTER_VALIDATE_INT);
    $leadershipComments = filter_input(INPUT_POST, 'leadershipComments', FILTER_SANITIZE_STRING);
    $leadershipRating = filter_input(INPUT_POST, 'leadershipRating', FILTER_VALIDATE_INT);
    $additionalComments = filter_input(INPUT_POST, 'additionalComments', FILTER_SANITIZE_STRING);
    $overallRating = filter_input(INPUT_POST, 'overallRating', FILTER_VALIDATE_INT);
    $alternativeProgramComments = filter_input(INPUT_POST, 'alternativeProgramComments', FILTER_SANITIZE_STRING);
    $alternativeProgram = filter_input(INPUT_POST, 'alternativeProgram', FILTER_VALIDATE_INT);   
    
    if (!$technicalComments)
    {
        $saveError .= 'Technical Strengths and/or Weaknesses is required<br>';    
    }
    
    if (!$technicalRating)
    {
        $saveError .= 'Rating for Technical Preparation is required<br>';    
    }
    
    if (!$academicComments)
    {
        $saveError .= 'Academic Strengths and/or Weaknesses is required<br>';    
    }
    
    if (!$ugProgramComments)
    {
        $saveError .= 'UG Academic Program is required<br>';    
    }
    
    if (!$academicRating)
    {
        $saveError .= 'Rating for Academic Preparation is required<br>';    
    }
    
    if (!$overallRating)
    {
        $saveError .= 'Recommendation is required<br>';    
    }
    
    if (!$saveError)
    {
        if ($iniReviewId)
        {
            if (allowUpdate($iniReviewId))
            {
                // Update existing record
                $updateQuery = "UPDATE review_ini SET
                    technical_comments = '" . mysql_real_escape_string($technicalComments) . "',
                    academic_comments = '" . mysql_real_escape_string($academicComments) . "',
                    ug_program_comments = '" . mysql_real_escape_string($ugProgramComments) . "',
                    research_comments = '" . mysql_real_escape_string($researchComments) . "',
                    work_experience_comments = '" . mysql_real_escape_string($workExperienceComments) . "',
                    leadership_comments = '" . mysql_real_escape_string($leadershipComments) . "',
                    additional_comments = '" . mysql_real_escape_string($additionalComments) . "',
                    alternative_program_comments = '" . mysql_real_escape_string($alternativeProgramComments) . "',
                    technical_rating = " . (($technicalRating && ($technicalRating != 6)) ? $technicalRating : 'NULL') . ",
                    academic_rating = " . (($academicRating && ($academicRating != 6)) ? $academicRating : 'NULL') . ",
                    research_rating = " . (($researchRating && ($researchRating != 6)) ? $researchRating : 'NULL') . ",
                    work_experience_rating = " . (($workExperienceRating && ($workExperienceRating != 6)) ? $workExperienceRating : 'NULL') . ",
                    leadership_rating = " . (($leadershipRating && ($leadershipRating != 6)) ? $leadershipRating : 'NULL') . ",
                    overall_rating = " . ($overallRating ? $overallRating : 'NULL') . ",
                    alternative_program = " . (($alternativeProgram && ($alternativeProgram != 6)) ? $alternativeProgram : 'NULL') . "
                    WHERE id = " . intval($iniReviewId);
                mysql_query($updateQuery);
            }
        }
        else
        {
            if (allowInsert())
            {
                // Insert new record
                $insertQuery = "INSERT INTO review_ini (
                    application_id, reviewer_id, department_id, round, 
                    technical_comments, academic_comments, ug_program_comments,
                    research_comments, work_experience_comments, leadership_comments,
                    additional_comments, alternative_program_comments, technical_rating, 
                    academic_rating, research_rating, work_experience_rating,    
                    leadership_rating, overall_rating, alternative_program)
                    VALUES (" 
                    . $applicationId . ","
                    . $reviewerId . ","
                    . $departmentId . "," 
                    . $round . ",'"
                    . mysql_real_escape_string($technicalComments) . "','" 
                    . mysql_real_escape_string($academicComments) . "','" 
                    . mysql_real_escape_string($ugProgramComments) . "','" 
                    . mysql_real_escape_string($researchComments) . "','"
                    . mysql_real_escape_string($workExperienceComments) . "','" 
                    . mysql_real_escape_string($leadershipComments) . "','" 
                    . mysql_real_escape_string($additionalComments) . "','" 
                    . mysql_real_escape_string($alternativeProgramComments) . "'," 
                    . ($technicalRating ? $technicalRating : 'NULL') . ","
                    . ($academicRating ? $academicRating : 'NULL') . ","
                    . ($researchRating ? $researchRating : 'NULL') . ","
                    . ($workExperienceRating ? $workExperienceRating : 'NULL') . ","
                    . ($leadershipRating ? $leadershipRating : 'NULL') . ","
                    . ($overallRating ? $overallRating : 'NULL') . ","
                    . ($alternativeProgram ? $alternativeProgram : 'NULL') . ")";
                mysql_query($insertQuery);
            }
        }
    }
}

function saveAdminReview()
{
    global $iniReviewId;
    global $applicationId;
    global $reviewerId;
    global $departmentId;
    global $round;
    global $adminComments;
    global $saveError; 
    
    $iniReviewId = filter_input(INPUT_POST, 'iniReviewId', FILTER_VALIDATE_INT);
    $applicationId = filter_input(INPUT_POST, 'applicationId', FILTER_VALIDATE_INT);
    $adminComments = filter_input(INPUT_POST, 'adminComments', FILTER_SANITIZE_STRING);

    if (!$adminComments)
    {
        $saveError .= 'Comments is required<br>';    
    }

    if (!$saveError && hasAdminRole())
    {
        if ($iniReviewId)
        {
            // Update existing record
            $updateQuery = "UPDATE review_ini_admin SET
                comments = '" . mysql_real_escape_string($adminComments) . "' 
                WHERE id = " . intval($iniReviewId);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO review_ini_admin (
                application_id, reviewer_id, department_id, round, comments)
                VALUES (" 
                . $applicationId . ","
                . $reviewerId . ","
                . $departmentId . "," 
                . $round . ",'"
                . mysql_real_escape_string($adminComments) . "')";
            mysql_query($insertQuery);
        }
    }
}

function getApplicationPrograms($applicationId)
{
    $applicationPrograms = array();
        
    $query = "SELECT *
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId) . "
        ORDER BY choice";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $applicationPrograms[] = $row;     
    }
    
    return $applicationPrograms;    
}

function getAdmissionStatuses($applicationId)
{
    $admissionStatuses = array();
        
    $query = "SELECT *
        FROM application_decision_ini
        WHERE application_id = " . intval($applicationId) . "
        ORDER BY choice";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $admissionStatuses[] = $row;     
    }
    
    return $admissionStatuses;    
}

function getApplicationPeriodId($applicationId)
{
    $periodId = NULL;
    
    $query = "SELECT period_id FROM period_application
                WHERE application_id = " . intval($applicationId);
    $result = mysql_query($query);
    
    while($row = mysql_fetch_array($result ))
    {
        $periodId = $row["period_id"];
    }    
    
    return $periodId;
}

function saveFinal()
{
    global $applicationId;
    $applicationPrograms = getApplicationPrograms($applicationId);
    $previousAdmissionStatuses = getAdmissionStatuses($applicationId);
    $periodId = getApplicationPeriodId($applicationId);
    
    $comments = filter_input(INPUT_POST, 'comments', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $admissionProgram1 = filter_input(INPUT_POST, 'admissionProgram1', FILTER_VALIDATE_INT);
    $admissionStatus1 = filter_input(INPUT_POST, 'admissionStatus1', FILTER_VALIDATE_INT);
    $admissionProgram2 = filter_input(INPUT_POST, 'admissionProgram2', FILTER_VALIDATE_INT);
    $admissionStatus2 = filter_input(INPUT_POST, 'admissionStatus2', FILTER_VALIDATE_INT);
    $scholarshipAmt = str_replace(array('?', ','), '', $_POST['scholarshipAmt']);
    $scholarshipComments = filter_input(INPUT_POST, 'scholarshipComments', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

    // Update the lu_application_program and admission
    $matchedAdmissionProgramIds = array();
    foreach($applicationPrograms as $applicationProgram)
    {
        // Determine the admission status for the applicant-selected program
        $admissionProgram = $applicationProgram['program_id'];
        $admissionStatus = 'NULL';
        
        if ($applicationProgram['program_id'] == $admissionProgram1)
        {
            $admissionProgram = $admissionProgram1;
            $matchedAdmissionProgramIds[] = $admissionProgram1; 
            if ($admissionStatus1 != 3)
            {
                $admissionStatus = $admissionStatus1; 
            }
        }
        elseif ($applicationProgram['program_id'] == $admissionProgram2)
        {
            $admissionProgram = $admissionProgram2;
            $matchedAdmissionProgramIds[] = $admissionProgram2;
            if ($admissionStatus2 != 3)
            {
                $admissionStatus = $admissionStatus2;
            }  
        }
        else
        {
            if ($applicationProgram['choice'] == 1)
            {
                if ($admissionProgram1)
                {
                    $admissionProgram = $admissionProgram1;
                    $matchedAdmissionProgramIds[] = $admissionProgram1;
                    if ($admissionStatus1 != 3)
                    {
                        $admissionStatus = $admissionStatus1; 
                    }        
                }
                elseif ($admissionProgram2)
                {
                    $admissionProgram = $admissionProgram2;
                    $matchedAdmissionProgramIds[] = $admissionProgram2;
                    if ($admissionStatus2 != 3)
                    {
                        $admissionStatus = $admissionStatus2;
                    }    
                }    
            }
            else
            {
                if ($admissionProgram2 && !in_array($admissionProgram2, $matchedAdmissionProgramIds))
                {
                    $admissionProgram = $admissionProgram2;
                    $matchedAdmissionProgramIds[] = $admissionProgram2;
                    if ($admissionStatus2 != 3)
                    {
                        $admissionStatus = $admissionStatus2;
                    }        
                }
                elseif ($admissionProgram1 && !in_array($admissionProgram1, $matchedAdmissionProgramIds))
                {
                    $admissionProgram = $admissionProgram1;
                    $matchedAdmissionProgramIds[] = $admissionProgram1;
                    if ($admissionStatus1 != 3)
                    {
                        $admissionStatus = $admissionStatus1;
                    }    
                }
            }            
        }

        $luapUpdateQuery = "UPDATE lu_application_programs SET
            admission_status = " . $admissionStatus . ",
            admit_comments = '" . mysql_escape_string($comments) . "',
            scholarship_amt = " . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . ",
            scholarship_comments = '" . mysql_escape_string($scholarshipComments) . "'
            WHERE id = " . $applicationProgram['id'];
            
        mysql_query($luapUpdateQuery);
            
        $adUpdateQuery = "INSERT INTO application_decision
            (application_id, program_id, period_id, admission_program_id, admission_status, comments) 
            VALUES ("
            . $applicationId . ","    
            . $applicationProgram['program_id'] . ","
            . $periodId . ","
            . $admissionProgram . ","
            . $admissionStatus . ",'"
            . mysql_escape_string($comments) . "')
            ON DUPLICATE KEY UPDATE
            admission_status = " . $admissionStatus . ",
            admission_program_id = " . $admissionProgram . ",
            comments = '" . mysql_escape_string($comments) . "'";
            
        mysql_query($adUpdateQuery);
    }
    
    // Insert/update ini admission status records
    if ($admissionProgram1)
    {
        if ($admissionStatus1 == 3)
        {
            $admissionStatus1 = 'NULL';    
        }
        
        $insertQuery1 = "INSERT INTO application_decision_ini
            (application_id, admission_program_id, choice, admission_status, 
            comments, scholarship_amt, scholarship_comments, timestamp) 
            VALUES(" 
            . $applicationId . ","    
            . $admissionProgram1 . ", 1,"
            . $admissionStatus1 . ",'"
            . mysql_escape_string($comments) . "',"
            . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . ",'"
            . mysql_escape_string($scholarshipComments) . "', 
            NOW())
            ON DUPLICATE KEY UPDATE
            choice = 1,
            admission_status = " . $admissionStatus1 . ",
            comments = '" . mysql_escape_string($comments) . "',
            scholarship_amt = " . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . ",
            scholarship_comments = '" . mysql_escape_string($scholarshipComments) . "', 
            timestamp = NOW()";
            
        mysql_query($insertQuery1);
    }
    
    if ($admissionProgram2)
    {
        if ($admissionStatus2 == 3)
        {
            $admissionStatus2 = 'NULL';    
        }
        
        $insertQuery2 = "INSERT INTO application_decision_ini
            (application_id, admission_program_id, choice, admission_status, 
            comments, scholarship_amt, scholarship_comments) 
            VALUES (" 
            . $applicationId . ","    
            . $admissionProgram2 . ", 2,"
            . $admissionStatus2 . ",'"
            . mysql_escape_string($comments) . "',"
            . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . ",'"
            . mysql_escape_string($scholarshipComments) . "')
            ON DUPLICATE KEY UPDATE
            choice = 2,
            admission_status = " . $admissionStatus2 . ",
            comments = '" . mysql_escape_string($comments) . "',
            scholarship_amt = " . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . ",
            scholarship_comments = '" . mysql_escape_string($scholarshipComments) . "'";
            
        mysql_query($insertQuery2);   
    } 
    
    // Delete orphan ini admission status records
    foreach($previousAdmissionStatuses as $previousAdmissionStatus)
    {
        if ($previousAdmissionStatus['admission_program_id'] != $admissionProgram1
            && $previousAdmissionStatus['admission_program_id'] != $admissionProgram2)
        {
            $deleteQuery = "DELETE FROM application_decision_ini
                WHERE application_id = " . intval($applicationId) . "
                AND admission_program_id = " . $previousAdmissionStatus['admission_program_id'];
                
            mysql_query($deleteQuery);
        }    
    }

    return;
    
    
    
    
    
    
    
    $vars = $_POST;
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if(strstr($key, 'comments_') !== false && $key != "comments_msit")
        {
            $itemId = $arr[1];
            $comments = $_POST["comments_".$arr[1]];
            $decision = $_POST["decision_".$arr[1]];
            $faccontact = $_POST["faccontact_".$arr[1]];
            $stucontact = $_POST["stucontact_".$arr[1]];
            $scholarshipAmt = str_replace(array('?', ','), '', $_POST["scholarshipAmt_".$arr[1]]);
            $admissionStatus = $_POST["admissionStatus_".$arr[1]];
            $admitProgramId = $itemId;
            
            /*
            * Handle status "subtypes" for LTI and MSE. 
            */
            $admissionStatusArray = explode('_', $admissionStatus);
            if ( count($admissionStatusArray) == 2 ) {
                $admissionStatus = $admissionStatusArray[0];
                $admitProgramId = $admissionStatusArray[1];    
            }
            
            // Add period code            
            $periodSql = "select period_id from period_application
                where application_id = " . $appid;
            $periodResult = mysql_query($periodSql) or die(mysql_error() . $periodSql);
            while($row = mysql_fetch_array($periodResult ))
            {
                $currentPeriod = $row["period_id"];
            }

            if ($admissionStatus != 3) 
            {
                $sql = "update lu_application_programs set 
                decision = '".$decision."',
                admission_status = ".intval($admissionStatus).",
                admit_comments = '".addslashes($comments)."',
                faccontact = '".addslashes($faccontact)."',
                stucontact='".addslashes($stucontact)."',
                scholarship_amt = " . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . "
                where application_id = ".$appid."
                and program_id = ".$itemId;
                mysql_query($sql) or die(mysql_error().$sql);
                
                // DAS Add code for new application_admission table
                $applicationDecisionSql = "insert into application_decision 
                (application_id, program_id, period_id, admission_program_id, admission_status, comments)
                values (".$appid.",".$itemId.",".$currentPeriod.",".$admitProgramId.",".intval($admissionStatus).",'";
                if ($comments == "") 
                {
                   $comments = "NULL";
                }
                else
                {
                    $comments = addslashes($comments);
                }
                $applicationDecisionSql .= mysql_escape_string($comments)."') 
                on duplicate key update admission_status=".intval($admissionStatus).", 
                admission_program_id = " . $admitProgramId . ",
                comments='".mysql_escape_string($comments)."'";
                mysql_query($applicationDecisionSql) or die(mysql_error().$applicationDecisionSql);
                    
            
            } else {
                $sql = "update lu_application_programs set 
                decision = '".$decision."',
                admission_status = NULL ".",
                admit_comments = '".addslashes($comments)."',
                faccontact = '".addslashes($faccontact)."',
                stucontact='".addslashes($stucontact)."',
                scholarship_amt = " . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . "
                where application_id = ".$appid."
                and program_id = ".$itemId;
                mysql_query($sql) or die(mysql_error().$sql);

                // DAS Add code for new application_admission table
                $applicationDecisionSql = "insert into application_decision 
                (application_id, program_id, period_id, admission_status, comments)
                values (".$appid.",".$itemId.",".$currentPeriod.",".intval($admissionStatus).",";
                if ($comments == "") 
                {
                    $comments = "NULL";
                }
                else
                {
                    $comments = "'" . mysql_escape_string( addslashes($comments) ) . "'";
                }
                $applicationDecisionSql .= $comments . ") on duplicate key update admission_status=NULL, comments=" . $comments;
                mysql_query($applicationDecisionSql) or die(mysql_error().$applicationDecisionSql);
            }
        }
    }
}

function hasCommitteeRole()
{
    $usertypeId = $_SESSION['A_usertypeid'];
    if ($usertypeId == 2)
    {
        return TRUE;
    }
    
    return FALSE;
}

function hasAdminRole()
{
    $usertypeId = $_SESSION['A_usertypeid'];
    if ($usertypeId == 1)
    {
        return TRUE;
    }
    
    return FALSE;
}

function allowUpdate()
{
    if (hasCommitteeRole())
    {
        return TRUE;    
    }
    
    return FALSE;
}

function allowInsert()
{
    if (hasCommitteeRole())
    {
        return TRUE;    
    }
     
    return FALSE;    
}
?>