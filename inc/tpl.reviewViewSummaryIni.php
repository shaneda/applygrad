<?php
$semiblind_review = 0; 
?>

<br/>
<table width="500" border="0"  cellspacing="0px" cellpadding="2px">
  <?php if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Ratings:</strong></td>
    <td valign="top">
    <? 
    foreach ($committeeReviews as $committeeReview)
    {
        echo '<div>';
        echo '<b>' . $committeeReview['lastname'] . '</b>';
        if ($committeeReview['technical_rating'])
        {
            echo ' technical: ' . $committeeReview['technical_rating'] . ';';
        }
        if ($committeeReview['academic_rating'])
        {
            echo ' academic: ' . $committeeReview['academic_rating'] . ';';
        }
        if ($committeeReview['research_rating'])
        {
            echo ' research: ' . $committeeReview['research_rating'] . ';';
        }
        if ($committeeReview['work_experience_rating'])
        {
            echo ' experience: ' . $committeeReview['work_experience_rating'] . ';';
        }
        if ($committeeReview['leadership_rating'])
        {
            echo ' leadership: ' . $committeeReview['leadership_rating'] . ';';
        }
        if ($committeeReview['overall_rating'])
        {
            echo ' overall: ' . $committeeReview['overall_rating'];
        }
        echo '</div><br/>';    
    } 
    ?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" ></td>
  </tr>

  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Comments:</strong>      </td>
    <td valign="top">
    <?     
    foreach ($committeeReviews as $committeeReview)
    {
        echo '<div>';
        echo '<b>' . $committeeReview['lastname'] . '</b>';
        if ($committeeReview['technical_comments'])
        {
            echo ' technical: ' . $committeeReview['technical_comments'];
        }
        if ($committeeReview['academic_comments'])
        {
            echo '; academic: ' . $committeeReview['academic_comments'];
        }
        if ($committeeReview['ug_program_comments'])
        {
            echo '; ug program: ' . $committeeReview['ug_program_comments'];
        }
        if ($committeeReview['research_comments'])
        {
            echo '; research: ' . $committeeReview['research_comments'];
        }
        if ($committeeReview['work_experience_comments'])
        {
            echo '; experience: ' . $committeeReview['work_experience_comments'];
        }
        if ($committeeReview['leadership_comments'])
        {
            echo '; leadership: ' . $committeeReview['leadership_comments'];
        }
        if ($committeeReview['additional_comments'])
        {
            echo '; additional: ' . $committeeReview['additional_comments'];
        }
        echo '</div><br/>';    
    } 
    ?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" ></td>
  </tr>
  
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Other <br>Programs:</strong>      </td>
    <td valign="top">
    <? 
    foreach ($committeeReviews as $committeeReview)
    {                                             
        echo '<div>';
        echo '<b>' . $committeeReview['lastname'] . '</b> ';
        if ($committeeReview['alternative_program_name'])
        {
            echo $committeeReview['alternative_program_name'];    
        }
        if ($committeeReview['alternative_program_comments'])
        {
            if ($committeeReview['alternative_program_name'])
            {
                echo ': ';    
            }
            echo $committeeReview['alternative_program_comments'];    
        }
        echo '</div><br/>';    
    } 
    ?>
    </td>
  </tr>

  <?php
   }//end semiblind reviews
  
  // PLB added coordinator notes 1/8/10
  if ($semiblind_review != 1) { ?>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
  <tr>  
    <td bgcolor="#DDDDDD" ><strong> Coordinator Notes: </strong> </td>
    <td>
    <?php
    foreach ($adminReviews as $adminReview)
    {
        echo '<div>';
        echo '<i>' . $adminReview['lastname'] . ': </i>';
        if ($adminReview['comments'])
        {
            echo $adminReview['comments'];
        }
        echo '</div><br/>';    
    }

    $coordinatorComments = getCoordinatorComments($appid, $thisDept);
    foreach ($coordinatorComments as $coordinatorComment) {
        echo $coordinatorComment . '<br>';    
    }
    
  // Decision comments for admins and committee members.
  if( $_SESSION['A_usertypeid'] == 0
    ||  $_SESSION['A_usertypeid'] == 1
    ||  $_SESSION['A_usertypeid'] == 2
    ||  $_SESSION['A_usertypeid'] == 10
    ) 
    { 
        echo '<i>Final comments:</i> ';
        for($x = 0; $x < count($myPrograms); $x++)
        {
            $aDepts = split(",", $myPrograms[$x][14]);
            for($j = 0; $j < count($aDepts); $j++)
            {
                if($aDepts[$j] == $thisDept)
                {
                    echo $myPrograms[$x][13];
                }
            }
        }
    }   
    ?>    
    </td>
  </tr>
<?php
} // end semiblind check for faculty reviews ?>
          
</table>