<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$ltiSeminars = array(
    array(1, "INFORMATION RETRIEVAL, TEXT MINING"),
    array(2, "MACHINE TRANSLATION"),
    array(3, "SPEECH RECOGNITION, SPEECH SYNTHESIS"),
    array(4, "NATURAL LANGUAGE PROCESSING, COMPUTATIONAL LINGUISTICS"),
    array(5, "BIOLOGICAL LANGUAGE MODELING"),
    array(6, "INTELLIGENT TUTORS"),
    array(7, "MACHINE LEARNING")
);

$ltiVoteVals = array(
    array(1, 1),
    array(1.5, 1.5),
    array(2, 2),
    array(2.5, 2.5),
    array(3, 3)
);

for($i = 0; $i < count($committeeReviews); $i++) {
    if( $committeeReviews[$i][32]== $thisDept
        && $committeeReviews[$i][30] == 0   // not faculty review 
        && $committeeReviews[$i][29] == ""  // not supplemental review
        && $committeeReviews[$i][4] == $reviewerId
        && $committeeReviews[$i][23] == $round) 
    {
        $background = $committeeReviews[$i][6];
        $committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
        $other_interest = $committeeReviews[$i][26];
        $statement = $committeeReviews[$i][8];
        $grades = $committeeReviews[$i][7];
        $brilliance = $committeeReviews[$i][25];
        $comments = $committeeReviews[$i][9];
        $point = $committeeReviews[$i][10];
        $point2 = $committeeReviews[$i][11];
        $publications = $committeeReviews[$i][41];
        $recommendations = $committeeReviews[$i][40];

    }
}

if ($previousRoundData) {
    echo '<span style="font-size: 12px; font-weight: bold; font-style: italic;">';
    echo 'You reviewed this application in round ' . $reviewRound . '</span>';    
    echo '<br/><br/>';
}

include "../inc/special_phone_email.inc.php";
?> 

<table border="0" cellspacing="0" cellpadding="2">
    <tr valign="middle">
        <td class="label">English Ability:</td>
        <td>
        <?php
        ob_start();
        showEditText($background, "textarea", "background", $allowEdit); 
        $backgroundTextarea = ob_get_contents();
        ob_end_clean();
        $backgroundTextarea = str_replace("rows='5'", "rows='6'", $backgroundTextarea);
        echo str_replace("cols='60'", "cols='50'", $backgroundTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Add Interest:</td>
        <td>
        <?php 
        if ($previousRoundData) {
            foreach ($committeeReviewsSeminars as $committeeReviewsSeminar) {
                $committeeReviewsSeminarId = $committeeReviewsSeminar[0];
                foreach ($ltiSeminars as $ltiSeminar) {
                    if ($ltiSeminar[0] == $committeeReviewsSeminarId) {
                        echo $ltiSeminar[1] . ' ';
                    }
                }
            }
        } else {
            showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $ltiSeminars );
        }
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Other Interests:</td>
        <td>
        <?php
        ob_start();
        showEditText($other_interest, "textarea", "other_interest", $allowEdit); 
        $otherInterestTextarea = ob_get_contents();
        ob_end_clean();
        $otherInterestTextarea = str_replace("rows='5'", "rows='6'", $otherInterestTextarea);
        echo str_replace("cols='60'", "cols='50'", $otherInterestTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Academic Quality:</td>
        <td colspan="3">
        <?php
        ob_start();
        showEditText($statement, "textarea", "statement", $allowEdit); 
        $statementTextarea = ob_get_contents();
        ob_end_clean();
        $statementTextarea = str_replace("rows='5'", "rows='6'", $statementTextarea);
        echo str_replace("cols='60'", "cols='50'", $statementTextarea);
        ?>
        </td>
    </tr> 
    <tr valign="middle">
        <td class="label">Research Aptitude:</td>
        <td>
        <?php
        ob_start();
        showEditText($grades, "textarea", "grades", $allowEdit); 
        $gradesTextarea = ob_get_contents();
        ob_end_clean();
        $gradesTextarea = str_replace("rows='5'", "rows='6'", $gradesTextarea);
        echo str_replace("cols='60'", "cols='50'", $gradesTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Industrial Experience:</td>
        <td>
        <?php
        ob_start();
        showEditText($recommendations, "textarea", "recommendations", $allowEdit); 
        $recommendationsTextarea = ob_get_contents();
        ob_end_clean();
        $recommendationsTextarea = str_replace("rows='5'", "rows='6'", $recommendationsTextarea);
        echo str_replace("cols='60'", "cols='50'", $recommendationsTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Fit to LTI:</td>
        <td>
        <?php
        ob_start();
        showEditText($publications, "textarea", "publications", $allowEdit); 
        $publicationsTextarea = ob_get_contents();
        ob_end_clean();
        $publicationsTextarea = str_replace("rows='5'", "rows='6'", $publicationsTextarea);
        echo str_replace("cols='60'", "cols='50'", $publicationsTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Special Consideration:</td>
        <td>
        <?php
        ob_start();
        showEditText($brilliance, "textarea", "brilliance", $allowEdit); 
        $brillianceTextarea = ob_get_contents();
        ob_end_clean();
        $brillianceTextarea = str_replace("rows='5'", "rows='6'", $brillianceTextarea);
        echo str_replace("cols='60'", "cols='50'", $brillianceTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Other:</td>
        <td>
        <?php 
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit); 
        $otherTextarea = ob_get_contents();
        ob_end_clean();
        $otherTextarea = str_replace("rows='5'", "rows='6'", $otherTextarea);
        echo str_replace("cols='60'", "cols='50'", $otherTextarea);
        ?>
        </td>
    </tr>
    <?php 
        if ($deptName != "LTI-IIS") {
            /* hack to stop LTI from changing scores midstream 
            if (2 == 2) {
                $allowEdit = false;
            }
            */
    ?>               
    <tr valign="middle">
        <td class="label">PhD Score:</td>
        <td>
        <?php 
        showEditText($point, "radiogrouphoriz2", "point", $allowEdit, false, $ltiVoteVals); 
        ?>
        </td>
    </tr>
    <?php } ?>
    <tr valign="middle">
        <?php
        if ($deptName != "LTI-IIS") {
        ?> 
            <td class="label">MLT Score:</td>
            <?php } else { ?>
                        <td class="label">MS Score:</td>
                        <?php }  ?>
        <td>
        <?php 
        showEditText($point2, "radiogrouphoriz2", "point2", $allowEdit, false, $ltiVoteVals); 
        ?>
        </td>
    </tr>
 
    <tr>
        <td>
    <!--        
        <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
        &nbsp;&nbsp;
    -->         
        <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>
        </td>
    </tr> 
  
</table>
