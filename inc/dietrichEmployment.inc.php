<table width="650" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td>
      <span class="subtitle"><?=$title;?> Employment</span><br>
      <span class="errorSubtitle">
      <?php 
      if (isset($itemErrors[$myEmploymentId])) {
        echo $itemErrors[$myEmploymentId];    
      }
      ?>
      </span>
    </td>
           <td align="right">
            <? showEditText("Delete", "button", "btnDel_".$myEmployments[$i][0], $_SESSION['allow_edit']); ?>
            <input name="txtId_<?=$myEmployments[$i][0];?>" type="hidden" id="txtId_<?=$myEmployments[$i][0];?>" value="<?=$myEmployments[$i][0];?>">
            </td>
          </tr>
</table>
<table width="650" border="0" cellpadding="3" cellspacing="0" class="tblItem">
  <tr>
    <td  align="right"><strong>Company:</strong></td>
    <td colspan="5">
    <? showEditText(trim($myEmployments[$i][1]), "textbox", 
            "lbName_".$myEmployments[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
    ?>
    </td>
  </tr>
<!-- NEW ROW -->
  <tr>
    <td  align="right"><strong>Address:</strong></td>
    <td colspan="5">
    <? 
    showEditText(trim($myEmployments[$i][5]), "textbox", 
            "lbAddress_".$myEmployments[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
    ?>
    </td>
  </tr>
<!-- NEW ROW -->
  <tr>
    <td  align="right"><strong>Job Description:</strong></td>
    <td colspan="5">
    <? 
    showEditText(trim($myEmployments[$i][7]), "textbox", 
            "lbJobDescription_".$myEmployments[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
    ?>
    </td>
  </tr>
<!-- NEW ROW -->
  <tr>
    <td align="right"><strong>Start Date:</strong></td>
    <td><? showEditText($myEmployments[$i][2], "textbox", "txtStartDate_".$myEmployments[$i][0], 
                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>              
    <td  align="right"><strong>End Date 
    <?  ?></strong><strong>:</strong></td>
    <td><? showEditText($myEmployments[$i][3], "textbox", "txtEndDate_".$myEmployments[$i][0], 
                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY or 'present')</td>
    
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<!-- NEW ROW -->
  <tr>
    <td  align="right"><strong>Years of Experience</strong><strong>:</strong></td>
    <td><? 
    if($_SESSION['allow_edit'] == true)
    {
        showEditText($myEmployments[$i][4], "textbox", "lbYrsExp_".$myEmployments[$i][0], $_SESSION['allow_edit'],true); 
    }
    else
    {
        echo $myInstitutes[$i][21];  //das
    }
    ?></td>
    <td align="right">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr> 
</table>