<?
if( strstr($_SERVER['SCRIPT_NAME'], 'loginConflict.php') === false
    && isset($domainid) && $domainid == 1) 
{ 
    include '../inc/tpl.pageHeaderApplyScs.php';
} 
elseif (strstr($_SERVER['SCRIPT_NAME'], 'loginConflict.php') === false
    && isset($_SESSION['domainname']) && $_SESSION['domainname'] == "SCS") 
{
    include '../inc/tpl.pageHeaderApplyScs.php';    
} 
elseif(strstr($_SERVER['SCRIPT_NAME'], 'loginConflict.php') !== false
    && isset($requestDomainId) && $requestDomainId == 1) 
{ 
    include '../inc/tpl.pageHeaderApplyScs.php';
} 
else 
{

$defaultCssFiles = array(
    );

$defaultJavascriptFiles = array(
    );

$subtitle = '';
if( isset($pageTitle) ) {
    $subtitle = ': ' . $pageTitle;    
}
    
if ( !isset($displayMenu) ) {
    $displayMenu = TRUE;
}

if ( !isset($formAction) ) {
    $formAction = '';
}

if ( !isset($formEnctype) ) {
    $formEnctype = 'application/x-www-form-urlencoded';
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<!---   <meta HTTP-EQUIV="REFRESH" content="0; url=https://applygrad.cs.cmu.edu/under_construction.html">    --->
    <link href="../css/apply.css" rel="stylesheet" type="text/css">
    <link href="../css/SCSStyles_<?php echo $domainname; ?>.css" rel="stylesheet" type="text/css">
    <title><?php echo $HEADER_PREFIX[$domainid]; echo $subtitle; ?>
    </title>
    <?php

    foreach ($defaultCssFiles as $cssFile) {
        echo <<<EOB
        <link type="text/css" href="{$cssFile}" rel="stylesheet">\n   
EOB;
    }
        
    if ( isset($pageCssFiles) ) {
        foreach ($pageCssFiles as $cssFile) {
            echo <<<EOB
            <link type="text/css" href="{$cssFile}" rel="stylesheet">\n   
EOB;
        }
    }

    if ( isset($headJavascriptFiles) ) {
        foreach ($headJavascriptFiles as $headJavascriptFile) {
            echo <<<EOB
            <script type="text/javascript" src="{$headJavascriptFile}"></script>\n   
EOB;
        }
    }

    ?>
</head>
<?php flush(); ?>
<body bgcolor="white">
<form action="<?; echo $formAction; ?>" method="post" enctype="<?; echo $formEnctype; ?>" name="form1" id="form1">
<div id="banner"></div>
<table width="95%" border="0" cellpadding="0" cellspacing="0">    

    <? 
    if( $displayMenu && ( isset($_SESSION['usertypeid']) && $_SESSION['usertypeid'] != 6) 
        && (strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false) ) {
        echo '<tr><td>';
        include '../inc/sideNav.php';
        echo '</td></tr>';
    }
    ?>

    <tr>
        <td valign="top">        
        
        <table>
            <colgroup>
            <col width="80%" style="text-align:left;">
            <col width="20%" style="text-align:right;">
            </colgroup>
            <tr>
            <td class=tblItem>
            <?php
            if ( isset($prevPage) && $prevPage != "" ) 
            {
                echo  "&nbsp;&nbsp;<a href=\"" . $prevPage . "\">< Previous </a>"; 
            }
            if ( isset($nextPage) && $nextPage != "")
            {
                echo "&nbsp; | &nbsp;";
                echo "<a href=\"" . $nextPage . "\">Next ></a>"; 
            }
            ?>
            </td>
        
            <td style="text-align:right; width:130px">    
            <? 
            if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
            ){ 
            ?>
                <a href="../apply/logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout</a>
            <?php
            }
            ?>
            </td>
            </tr>
        </table>
           
        <!--
        <div style="text-align:right; width:680px">
        <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
        && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
        && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
        && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
        ){ 
        ?>
            <a href="../apply/logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout</a>
        <?php
        }
        ?>
        </div>
        -->
        
        <div style="margin:20px;width:660px">
            <span class="title"><?php echo $pageTitle; ?></span>
            <br>
            <br>
            <div class="tblItem" id="contentDiv">
            
<?php
// end super ultra hacky kluge SCS else 
}
?>