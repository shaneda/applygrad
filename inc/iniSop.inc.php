<?php
// Initialize variables
$iniSopObjective = '';
$iniSopBackground = '';
$iniSopResearchExperience = '';
$iniSopLeadershipExperience = '';
$iniSopSfsInterest = '';
$iniSopAdditionalInfo = '';
$iniSopError = '';

// Validate and save posted data
if( isset($_POST['btnSubmit'])) 
{
    saveIniSop();
    checkRequirementsIniSop();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$iniSopError)
{
    $iniSopQuery = "SELECT * FROM ini_sop 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $iniSopResult = mysql_query($iniSopQuery);
    while($row = mysql_fetch_array($iniSopResult))
    {
        $iniSopObjective = $row['objective'];
        $iniSopBackground = $row['background'];
        $iniSopResearchExperience = $row['research_experience'];
        $iniSopLeadershipExperience = $row['leadership_experience'];
        $iniSopSfsInterest = $row['sfs_interest'];  
        $iniSopAdditionalInfo = $row['additional_info'];       
    }    
} 

// Check whether form inputs should be disabled 
$disabled = '';
if (!$_SESSION['allow_edit'])
{
    $disabled = 'disabled="disabled"';    
}
?>

<span class="subtitle">Statement of Purpose</span>
<br>
<span class="errorSubtitle" id="iniSopError"><?php echo $iniSopError; ?></span>

<br/>
Briefly explain your objective in pursuing the master's degree(s) for which you are applying.
<br/>
<textarea name="iniSopObjective" cols="60" rows="5" class="tblItemRequired" 
    onkeyup="countSOPChar(this, 1500, '#iniSopObjectiveCharCount')"
    <?php echo $disabled; ?> ><?php echo $iniSopObjective; ?></textarea>
<br/>
Characters left in your response 
<span id="iniSopObjectiveCharCount"><?php echo 1500 - strlen($iniSopObjective); ?></span> 

<br/>
<br/>
Describe your background in engineering, computer science, and other fields particularly relevant to
your objectives. Include any industrial or commercial work experience.
<br/>
<textarea name="iniSopBackground" cols="60" rows="5" class="tblItemRequired"  
    onkeyup="countSOPChar(this, 1500, '#iniSopBackgroundCharCount')"
    <?php echo $disabled; ?>><?php echo $iniSopBackground; ?></textarea> 
<br/>
Characters left in your response 
<span id="iniSopBackgroundCharCount"><?php echo 1500 - strlen($iniSopBackground); ?></span> 

<br/>
<br/>
Outline your research experience (if applicable).
<br/>
<textarea name="iniSopResearchExperience" cols="60" rows="5" class="tblItemRequired" 
    onkeyup="countSOPChar(this, 1500, '#iniSopResearchExperienceCharCount')"
    <?php echo $disabled; ?>><?php echo $iniSopResearchExperience; ?></textarea> 
<br/>
Characters left in your response 
<span id="iniSopResearchExperienceCharCount"><?php echo 1500 - strlen($iniSopResearchExperience); ?></span> 

<br/>
<br/>
Please describe any experiences where you have demonstrated leadership skills and abilities. These
examples may include academic, professional, extracurricular and/or community activities. Also please
identify any leadership qualities that you would bring to the INI that you believe would enhance and
add to our community and reputation.
<br/>
<textarea name="iniSopLeadershipExperience" cols="60" rows="5"  class="tblItemRequired"  
    onkeyup="countSOPChar(this, 2000, '#iniSopLeadershipExperienceCharCount')"
    <?php echo $disabled; ?>><?php echo $iniSopLeadershipExperience; ?></textarea> 
<br/>
Characters left in your response 
<span id="iniSopLeadershipExperienceCharCount"><?php echo 2000 - strlen($iniSopLeadershipExperience); ?></span>

<?php
if (!$isIniKobeDomain)
{
?>
    <br/>
    <br/>
    U.S. citizens who are admitted to the MSIS are eligible for consideration for a full
    scholarship under the Scholarship for Service (SFS) program. If you are a U.S. citizen applying to 
    this program and interested in the SFS program please describe (1) why you are interested in the
    SFS program, (2) how your background and preparation has prepared you for the SFS program, (3)
    how the SFS program will help you in achieving your career goals.
    <br/>
    <a href="http://www.ini.cmu.edu/prospective_students/financial/sfs.html">Scholarship for Service (SFS) program</a>
    <br/>
    <textarea name="iniSopSfsInterest" cols="60" rows="5" 
        onkeyup="countSOPChar(this, 3000, '#iniSopSfsInterestCharCount')"
        <?php echo $disabled; ?>><?php echo $iniSopSfsInterest; ?></textarea> 
    <br/>
    Characters left in your response 
    <span id="iniSopSfsInterestCharCount"><?php echo 3000 - strlen($iniSopSfsInterest); ?></span>
<?php
}
?>

<br/>
<br/>
Please provide any information you feel would be helpful to the admissions committee in making a decision. 
For exmaple, explain any extenuating circumstances such as an illness that led to a bad semester on your transcript, etc.
<br/>
<textarea name="iniSopAdditionalInfo" cols="60" rows="5" 
    onkeyup="countSOPChar(this, 3000, '#iniSopAdditionalInfoCharCount')"
    <?php echo $disabled; ?>><?php echo $iniSopAdditionalInfo; ?></textarea> 
<br/>
Characters left in your response 
<span id="iniSopAdditionalInfoCharCount"><?php echo 3000 - strlen($iniSopAdditionalInfo); ?></span>

<script src="../javascript/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
function countSOPChar(val, maxsize, textElement) {
    var len = val.value.length;
    if (len > maxsize) {
        val.value = val.value.substring(0, maxsize);
    } else {
        $(textElement).text(maxsize - len + ' characters left');
    }
};

$(document).ready(function() {

    var iniErrorMessage = $('#iniSopError').text();
    if (iniErrorMessage != '')
    {
        var pageErrorMessage = $('#resumePageError').val() + iniErrorMessage;
        $('#resumePageError').text(pageErrorMessage);        
    }
});
</script>

<?php
function saveIniSop()
{
    global $iniSopObjective;
    global $iniSopBackground;
    global $iniSopResearchExperience;
    global $iniSopLeadershipExperience;
    global $iniSopSfsInterest;
    global $iniSopAdditionalInfo;
    global $iniSopError;
    
    $iniSopObjective = filter_input(INPUT_POST, 'iniSopObjective', FILTER_SANITIZE_STRING);
    $iniSopBackground = filter_input(INPUT_POST, 'iniSopBackground', FILTER_SANITIZE_STRING);
    $iniSopResearchExperience = filter_input(INPUT_POST, 'iniSopResearchExperience', FILTER_SANITIZE_STRING);
    $iniSopLeadershipExperience = filter_input(INPUT_POST, 'iniSopLeadershipExperience', FILTER_SANITIZE_STRING);
    $iniSopSfsInterest = filter_input(INPUT_POST, 'iniSopSfsInterest', FILTER_SANITIZE_STRING);
    $iniSopAdditionalInfo = filter_input(INPUT_POST, 'iniSopAdditionalInfo', FILTER_SANITIZE_STRING); 
    
    if (!$iniSopObjective)
    {
        $iniSopError .= 'Statement of purpose objective is required<br>';    
    }
     
    if (!$iniSopBackground)
    {
        $iniSopError .= 'Statement of purpose background is required<br>';    
    }
    
    if (!$iniSopResearchExperience)
    {
        $iniSopError .= 'Statement of purpose research experience is required<br>';    
    }
    
    if (!$iniSopLeadershipExperience)
    {
        $iniSopError .= 'Statement of purpose leadership experience is required<br>';    
    } 
    
    if (!$iniSopError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM ini_sop WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE ini_sop SET
                objective = '" . mysql_real_escape_string($iniSopObjective) . "',
                background = '" . mysql_real_escape_string($iniSopBackground) . "',
                research_experience = '" . mysql_real_escape_string($iniSopResearchExperience) . "',
                leadership_experience = '" . mysql_real_escape_string($iniSopLeadershipExperience) . "',
                sfs_interest = '" . mysql_real_escape_string($iniSopSfsInterest) . "',
                additional_info = '" . mysql_real_escape_string($iniSopAdditionalInfo) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO ini_sop 
                (application_id, objective, background, research_experience, 
                leadership_experience, sfs_interest, additional_info)
                VALUES (" 
                . intval($_SESSION['appid']) . ",'" 
                . mysql_real_escape_string($iniSopObjective) . "','" 
                . mysql_real_escape_string($iniSopBackground) . "','" 
                . mysql_real_escape_string($iniSopResearchExperience) . "','" 
                . mysql_real_escape_string($iniSopLeadershipExperience) . "','"
                . mysql_real_escape_string($iniSopSfsInterest) . "','" 
                . mysql_real_escape_string($iniSopAdditionalInfo) . "')";
            mysql_query($insertQuery);
        }
    } 
}

/*
* Check requirements for resume.php
* (assuming this section comes before INI years experience) 
*/
function checkRequirementsIniSop()
{
    global $iniSopError; 
    global $resumeFileId;    
    
    if (!$iniSopError && ($resumeFileId > 0))
    {               
        // Resume has been uploaded, so page is complete so far.
        updateReqComplete("resume.php", 1);
        return;    
    }
    
    // Page is incomplete.
    updateReqComplete("resume.php", 0);
}
?>