<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';
$allowEdit = TRUE;

$admitVoteValues = array(
    array(1, 1),
    array(1.25, 1.25),
    array(1.5, 1.5),
    array(1.75, 1.75),
    array(2, 2),
    array(2.5, 2.5),
    array(3, 3),
    array(4, 4),
    array(5, 5)
); 

$rankingValues = $arr = array(
    array(0, "This is not a top 5 choice"),
    array(1, "1st Choice"),
    array(2, "2nd Choice"),
    array(3, "3rd Choice"),
    array(4, "4th Choice"),
    array(5, "5th Choice")   
);

$yesNoValues = $arr = array(
    array(0, "No"), 
    array(1, "Yes")
);  

for($i = 0; $i < count($committeeReviews); $i++) {
    if( $committeeReviews[$i][4] == $reviewerId 
        && $committeeReviews[$i][29] == "" 
        && $committeeReviews[$i][23] == $round) 
    {
        $admit_vote = $committeeReviews[$i][15];
        $pertinent_info = $committeeReviews[$i][18];
        $rank = $committeeReviews[$i][31];
        $recruited = $committeeReviews[$i][16];
        $grad_name = $committeeReviews[$i][17];
        $advise_time = $committeeReviews[$i][19];
        $commit_money = $committeeReviews[$i][20];
        $fund_source = $committeeReviews[$i][21];
        
        $background = $committeeReviews[$i][6];
        $grades = $committeeReviews[$i][7];
        $statement = $committeeReviews[$i][8];
        $comments = $committeeReviews[$i][9];
        $point = $committeeReviews[$i][10];
    }
}
?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="comments" type="hidden" value="<?=$comments?>">
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="facVote" type="hidden" value="1">

<table width="500" border="0" cellspacing="0" cellpadding="2">
    <colgroup>
        <col />
    </colgroup>
 
    <tr valign="middle"> 
        <td class="label">
        Ranking (1 is best, 5 is worst)
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        $admitVote = NULL; 
        showEditText($admit_vote, "radiogrouphoriz2", "admit_vote", $allowEdit, false, $admitVoteValues); 
        ?> 
        </td>
    </tr>
    <tr valign="middle"> 
        <td class="label">
        Comments
        </td>
    </tr>
    <tr valign="middle">
        <td>
        <?php 
        showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 60); 
        ?>
        </td>
    </tr> 
    <tr>
        <td colspan="4">
        <!-- 
        <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
        &nbsp;&nbsp;
        -->
        <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>
        </td>
    </tr>
 
</table>
