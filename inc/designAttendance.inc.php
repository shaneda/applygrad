<hr size="1" noshade color="#990000">
<?php
// Initialize variables
$attendanceStatus = NULL;
$attendanceStatusError = '';

// Validate and save posted data
if( isset($_POST['attendanceStatus'])) 
{
    saveAttendanceStatus();
    checkRequirementsAttendanceStatus();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$attendanceStatusError)
{
    $attendanceStatusQuery = "SELECT status FROM attendance 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $attendanceStatusResult = mysql_query($attendanceStatusQuery);
    while($row = mysql_fetch_array($attendanceStatusResult))
    {
        $attendanceStatus = $row['status'];        
    }    
} 
?>

<span class="subtitle">Attendance Status</span>
<br/><br/>
Please indicate whether you are applying for full time or part time 
attendance status.
<br/><br/>
<?php
$attendanceStatuses = array(
    array(1, "Full Time"),
    array(2, "Part Time")
);

ob_start();
showEditText($attendanceStatus, "radiogrouphoriz", "attendanceStatus", $_SESSION['allow_edit'], false, $attendanceStatuses);
$inputElementString = ob_get_contents();
ob_end_clean();

$inputElementReplace = ' onClick="form1.submit();">';
$inputElementString = str_replace('>', $inputElementReplace, $inputElementString);

echo $inputElementString
?>

<hr size="1" noshade color="#990000">

<?php
function saveAttendanceStatus()
{
    global $attendanceStatus;
    global $attendanceStatusError;
    
    $attendanceStatus = filter_input(INPUT_POST, 'attendanceStatus', FILTER_VALIDATE_INT);
    
    if ($attendanceStatus)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM attendance WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE attendance SET
                status = " . intval($attendanceStatus) . "
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO attendance (application_id, status)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($attendanceStatus) . ")";
            mysql_query($insertQuery);
        }
    }
}

function checkRequirementsAttendanceStatus()
{
    global $err;
    global $designRequirementsError;
    global $attendanceStatusError;     
    
    if (!$err && !$designRequirementsError && !$attendanceStatusError)
    {
        updateReqComplete("programs.php", 1);
    }
    else
    {
        updateReqComplete("programs.php", 0);    
    }    
}
?>