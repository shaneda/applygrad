<?PHP
/*
 * Name: class.AdhocGroupsData.php
 * Created by: Paul Worona
 * December 2008
 *
 * Description:
 * class to consolidate functions used to assign applicants to groups
 */

include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once '../classes/class.db_applicant.php';
include_once '../classes/class.db_faculty.php'; 

class AdhocGroupsData
{
	private $application_id;
	private $department;
	private $userid;
	private $assignedFaculty = array();
	private $unassignedFaculty = array();
	
	public function __construct( $application_id, $userid, $department )
	{
		$this->application_id = $application_id;	
		$this->userid = $userid;
		$this->department = $department;
	}
	
	//
	// PRIVATE FUNCTIONS
	//
		
	// remove non-alpha chars
	// and replace spaces with '_'
	private function fromatName( $word )
	{
		$parts = split('\+', $word); // remove '+...'
		$word = trim( $parts[0] );
		
		// remove
		$patterns = array ('/\d+/', '/\'/');  // numbers, apostrophies
		$word = preg_replace( $patterns, '', $word );
		
		// replace
		$patterns = array ('/\s+/');  // spaces with "_"
		$word = preg_replace( $patterns, '_', $word );
		
		return $word;
	}
	
	// get group names for this applicant
	private function getApplicantGroups()
	{
		$application_id = $this->application_id;
		$department = $this->department;
		
		$query = "SELECT rg.name 
			FROM lu_application_groups ag
			LEFT JOIN revgroup rg
			ON ag.group_id = rg.id
			WHERE ag.application_id = $application_id
				AND rg.department_id = $department
				AND ag.round = 1
			";
			
		$result = mysql_query( $query ) or die( "ERROR: Failed to look up groups. " . mysql_error() );
		
		$existingGroups = array();
		while ( $row = mysql_fetch_row($result) )
		{
			array_push( $existingGroups, $row[0] );
		}
		return $existingGroups;
	}
	
	// retun existing group id id
	// insert new group and return id
	private function getGroupID( $groupname )
	{
		$department = $this->department;
		$query = "SELECT id as group_id FROM revgroup 
			WHERE name='$groupname' AND department_id = '$department'";
		$result = mysql_query($query) or die( "ERROR: Failed to verify group. " . mysql_error() );
		
		if ( mysql_num_rows($result) ) 
		{
			$row = mysql_fetch_row($result);	
			$group_id = $row[0];
		} else {
			// insert group
			$query = "INSERT INTO revgroup (name, department_id)
				VALUES ('$groupname', '$department')";
			$result = mysql_query($query) or die( "ERROR: Failed to insert group. " . mysql_error() );
			$group_id = mysql_insert_id();	
		}
		return $group_id;
	}
	
	// insert applicant into a group
	private function addApplicantToGroup($group_id)
	{
		$application_id = $this->application_id;
		$query = "INSERT INTO lu_application_groups (application_id, group_id, round)
			VALUES ('$application_id', '$group_id', '1')";
		$result = mysql_query($query) or die( "ERROR: Failed to add applicant to group. " . 
			mysql_error() );	
	}
	
	public function getApplicantData()
	{
		$application_id = $this->application_id;
		$applicantData = new DB_Applicant();
		return $applicantData->getApplicantInfo($application_id);
	}
	
	//
	// PUBLIC FUNCTIONS
	//
	
	// assign applicant to group
	public function assignApplicant( $groupname )
	{
		$group_id = $this->getGroupID($groupname);
		$this->addApplicantToGroup($group_id);
		// add error checking later if necessary
		return FALSE;
		
	}	
	
	public function setFacultyData( $facultyID = FALSE )
	{
		$application_id = $this->application_id;
		$department = $this->department;
		
		// get existing group assignments
		$existingGroups = $this->getApplicantGroups();
		$data = new DB_Faculty();
		$facultyData = $data->getFacultyInfo($facultyID, $department);
		
		// consolidate faculty and create group name
		$facultyNames = array();
		$assignedFaculty = array();
		$unassignedFaculty = array();
		foreach ( $facultyData as $faculty )
		{
			$firstname = $this->fromatName( $faculty['firstname'] );
			$lastname = $this->fromatName( $faculty['lastname'] );
			
			$groupname = $firstname . "_" . $lastname;
			
			// check for pre-existing name
			$name = $firstname . " " . $lastname;
			if ( ! in_array( $name, $facultyNames ) )
			{
				// add fixed data to faculty data
				$faculty['firstname'] = $firstname;
				$faculty['lastname'] = $lastname;
				
				// create groupname
				$faculty['groupname'] = $groupname;
				
				// add to faculty lists
				if ( in_array( $groupname, $existingGroups ) )
				{
					array_push($assignedFaculty, $faculty);	
				} else {
					array_push($unassignedFaculty, $faculty);
				}
				
				// add to names
				array_push($facultyNames, $name);	
			}
		}
		$this->assignedFaculty = $assignedFaculty;
		$this->unassignedFaculty = $unassignedFaculty;
		
	}	
	
	public function getAssignedFaculty()
	{
		return $this->assignedFaculty;
	}
	
	public function getUnassignedFaculty()
	{
		return $this->unassignedFaculty;
	}
	
}

?>