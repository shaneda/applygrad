<?php
// Initialize variables
$iniYearsExperienceFullTimeProfessional = NULL;
$iniYearsExperienceRelevantIndustry = NULL;
$iniYearsExperienceError = '';

// Validate and save posted data
if( isset($_POST['btnSubmit'])) 
{
    saveIniYearsExperience();
    checkRequirementsIniYearsExperience();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$iniYearsExperienceError)
{
    $iniYearsExperienceQuery = "SELECT * FROM ini_years_experience 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $iniYearsExperienceResult = mysql_query($iniYearsExperienceQuery);
    while($row = mysql_fetch_array($iniYearsExperienceResult))
    {
        $iniYearsExperienceFullTimeProfessional = $row['full_time_professional'];
        $iniYearsExperienceRelevantIndustry = $row['relevant_industry'];      
    }    
}
?>
<span class="errorSubtitle" id="iniSopError"><?php echo $iniYearsExperienceError; ?></span>
<br/>
<span style="display: inline-block; width: 450px;">How many years of full-time professional experience do you have?</span>
<?php 
showEditText($iniYearsExperienceFullTimeProfessional, "textbox", "iniYearsExperienceFullTimeProfessional", $_SESSION['allow_edit'], TRUE, null, true, 2); 
?>
<br>
<span style="display: inline-block; width: 450px;">How many years of relevant industry experience do you have?</span>
<?php 
showEditText($iniYearsExperienceRelevantIndustry, "textbox", "iniYearsExperienceRelevantIndustry", $_SESSION['allow_edit'], TRUE, null, true, 2); 
?>

<?php
function saveIniYearsExperience()
{
    global $iniYearsExperienceFullTimeProfessional;
    global $iniYearsExperienceRelevantIndustry;
    global $iniYearsExperienceError;

    $iniYearsExperienceFullTimeProfessional = filter_input(INPUT_POST, 'iniYearsExperienceFullTimeProfessional', FILTER_VALIDATE_INT);
    $iniYearsExperienceRelevantIndustry = filter_input(INPUT_POST, 'iniYearsExperienceRelevantIndustry', FILTER_VALIDATE_INT);
    
    if ($iniYearsExperienceFullTimeProfessional === NULL || $iniYearsExperienceFullTimeProfessional === FALSE)
    {
        $iniYearsExperienceError .= 'Full-time professional years of experience is required<br>';    
    }
     
    if ($iniYearsExperienceRelevantIndustry === NULL || $iniYearsExperienceRelevantIndustry === FALSE)
    {
        $iniYearsExperienceError .= 'Relevant industry years of experience is required<br>';    
    }
        
    if (!$iniYearsExperienceError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM ini_years_experience WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE ini_years_experience SET
                full_time_professional = " . intval($iniYearsExperienceFullTimeProfessional) . ",
                relevant_industry = " . intval($iniYearsExperienceRelevantIndustry) . "
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO ini_years_experience 
                (application_id, full_time_professional, relevant_industry)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($iniYearsExperienceFullTimeProfessional) . "," 
                . intval($iniYearsExperienceRelevantIndustry) . ")";
            mysql_query($insertQuery);
        }
    }
}
    
/*
* Check requirements for resume.php
* (assuming this section comes after INI text SOP)
*/
function checkRequirementsIniYearsExperience()
{
    global $iniYearsExperienceError;
    global $iniSopError;
    global $resumeFileId;      
    
    if (!$iniYearsExperienceError && !$iniSopError && 
        ($resumeFileId > 0))
    {
        // Resume has been uploaded and statement is valid,
        // so page is complete.
        updateReqComplete("resume.php", 1);
        return;      
    }
    
    // Page is incomplete.
    updateReqComplete("resume.php", 0);
}
?>