<?php
// Initialize variables
$dietrichShareSds = NULL;
$dietrichShareTepper = NULL;
$dietrichShareError = '';
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);

// Validate and save posted data
if (isset($_POST['dietrichShareSds']) || isset($_POST['dietrichShareTepper'])) 
{
    saveDietrichShare();
    checkRequirementsDietrichShare();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$dietrichShareError)
{
    $dietrichShareQuery = "SELECT sds, tepper FROM dietrich_sharing 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $dietrichShareResult = mysql_query($dietrichShareQuery);
    while($row = mysql_fetch_array($dietrichShareResult))
    {
        $dietrichShareSds = $row['sds'];
        $dietrichShareTepper = $row['tepper'];           
    }    
}      
?>
<hr size="1" noshade color="#990000">
If you are interested in the Social Decision Sciences Department at Carnegie Mellon, would you like to have your application shared with them?
<br/>
<br/>
<?php
/* 
* Add onlick event handlers to radio buttons for inclusion on programs.php, 
* because programs has no page-level submit input.
*/
ob_start();
showEditText($dietrichShareSds, "radiogrouphoriz", "dietrichShareSds", $_SESSION['allow_edit'], false, $radioYesNo);
$dietrichShareSdsRadioButtons = ob_get_contents();
ob_end_clean();
$dietrichShareSdsRadioButtons = str_replace('>', ' onClick="form1.submit();">', $dietrichShareSdsRadioButtons);
echo $dietrichShareSdsRadioButtons;
?>

<br/>
<br/>
If you are interested in the Organization Psychology at Carnegie Mellon, would you like to have your application shared with Tepper School of Business?
<br/>
<br/>
<?php
ob_start();
showEditText($dietrichShareTepper, "radiogrouphoriz", "dietrichShareTepper", $_SESSION['allow_edit'], false, $radioYesNo);
$dietrichShareTepperRadioButtons = ob_get_contents();
ob_end_clean();
$dietrichShareTepperRadioButtons = str_replace('>', ' onClick="form1.submit();">', $dietrichShareTepperRadioButtons);
echo $dietrichShareTepperRadioButtons;
?>
<hr size="1" noshade color="#990000">
<br/>

<?php
function saveDietrichShare()
{
    global $dietrichShareSds;
    global $dietrichShareTepper;
    global $dietrichShareError;
    
    $dietrichShareSds = filter_input(INPUT_POST, 'dietrichShareSds', FILTER_VALIDATE_BOOLEAN);
    $dietrichShareTepper = filter_input(INPUT_POST, 'dietrichShareTepper', FILTER_VALIDATE_BOOLEAN);
    
    // Check for existing record
    $existingRecordQuery = "SELECT id FROM dietrich_sharing WHERE application_id = " . intval($_SESSION['appid']);
    $existingRecordResult = mysql_query($existingRecordQuery);
    if (mysql_num_rows($existingRecordResult) > 0)
    {
        // Update existing record
        $updateQuery = "UPDATE dietrich_sharing SET
            sds = " . intval($dietrichShareSds) . ",
            tepper = " . intval($dietrichShareTepper) . "
            WHERE application_id = " . intval($_SESSION['appid']);
        mysql_query($updateQuery);
    }
    else
    {
        // Insert new record
        $insertQuery = "INSERT INTO dietrich_sharing (application_id, sds, tepper)
            VALUES (" 
            . intval($_SESSION['appid']) . ","
            . intval($dietrichShareSds) . "," 
            . intval($dietrichShareTepper) . ")";
        mysql_query($insertQuery);
    }
}

function checkRequirementsDietrichShare()
{
    global $err;
    global $dietrichShareError;     
    
    if (!$err && !$dietrichShareError)
    {
        updateReqComplete("programs.php", 1);
    }
    else
    {
        updateReqComplete("programs.php", 0);    
    }    
}
?>