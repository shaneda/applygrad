</div>

<?php 
$endTime = microtime(TRUE);
if (isset($environment) && $environment == 'development') {
    if ( isset($startTime) ) {
        echo "<br/><br/>execution time: " . number_format( ($endTime - $startTime), 2) . " seconds<br/>"; 
    }
    echo memory_get_peak_usage() / 1000000 . ' MB peak memory used<br/>';
}

if ( isset($defaultJavascriptFiles) ) { 
    foreach ($defaultJavascriptFiles as $javascriptFile) {   
        echo <<<EOB
        <script type="text/javascript" src="{$javascriptFile}"></script>\n    
EOB;
        }
}
 
if ( isset($pageJavascriptFiles) ) { 
    foreach ($pageJavascriptFiles as $javascriptFile) {
        if ($javascriptFile != '../javascript/jquery-1.2.6.min.js') {   
            echo <<<EOB
            <script type="text/javascript" defer="defer" src="{$javascriptFile}"></script>\n    
EOB;
        }
    }
}    
?>
</body>
</html>
