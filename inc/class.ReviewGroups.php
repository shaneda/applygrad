<?PHP
include '../classes/class.ApplicantGroupListData.php';

class ReviewGroups
{
	/* Possible user types:
	 * SCS, -1 ??
	 * CSD, 1
	 * Robotics, 3
	 */	
	
    private $periodId = NULL;
    
    private $deptId = -1;
	private $deptName = "";
	private $round = 1;
	private $sort = "name";
	private $showAllInterests = false;
	private $having = "";
	private $searchString = FALSE;
	private $discuss = "";
	
	private $applicants = array();
	// PLB added $allApplicants to fix member display after a search 12/11/09
    private $allApplicants = array();
    private $groups = array();
	private $applicantGroups = array();
	
	// current group vars
	private $selectedGroupID = 0;
	private $memberCount = 0;
	private $members = array();
	private $nonMembers = array();
	
	public function __construct()
	{

		if ( isset($_REQUEST['period']) ) {
            $this->periodId = $_REQUEST['period'];
        }
        
        // set dept ID
		if(isset($_REQUEST['id']))
			$this->deptId = intval($_REQUEST['id']);
		
		// set round
		if(isset($_GET['r']))
			$this->round = intval($_GET['r']);
		
		// set sort
		if(isset($_GET['sort']))
		{
			$this->sort = $_GET['sort'];
		}
		else
		{
			if(isset($_POST['sort']))
			{
				$this->sort = $_POST['sort'];
			}
		}

		// search
		/*
        if ( isset($_POST['submitSearch']) )
			$this->setSearchString();
		*/
        if ( isset($_POST['searchString']) ) {
            $this->setSearchString();
        }
        
		// set show interests var
		if(isset($_POST['chkShowInt']))
			$this->showAllInterests = true;
		
		if ( isset($_GET['group']) )
			$this->selectedGroupID = $_GET['group'];
		
		if ( isset ($_POST['delete']) )
			$this->deleteMember();
			
		
		if ( isset ($_POST['add']) )
			$this->addMember();			
	}
	
    public function getDeptId() {
        return $this->deptId;
    }
    
    public function getDeptName() {
        return $this->deptName;
    }

	public function getPeriodId() {
        return $this->periodId;
    }
    
    // set search string
	private function setSearchString()
	{
		if ( isset($_POST['searchString']) ) 
		{
					
			$searchString = $_POST['searchString'];
			
			$this->searchString = $searchString;
		}
	}	

	// delete group member
	private function deleteMember()
	{
		if ( isset($_POST['application_id']) AND $this->selectedGroupID != FALSE )
		{
			$application_idList = $_POST['application_id'];
			$group_id = $this->selectedGroupID;
			$round = $this->round;
            
            foreach ($application_idList as $application_id)
			{
				$query = "DELETE FROM lu_application_groups 
                            WHERE application_id='$application_id' 
                            AND group_id = '$group_id'
                            AND round='$round'";
				mysql_query($query)	or die( mysql_error() );
			}
		}
	}
	
	// add group member
	private function addMember()
	{
		if ( isset($_POST['application_id']) AND $this->selectedGroupID != FALSE )
		{
			$application_idList = $_POST['application_id'];
			$group_id = $this->selectedGroupID;
			$round = $this->round;
			
			foreach ($application_idList as $application_id)
			{
				// make sure applicant isn't already in group
				$doUpdate = true;
				$query = "SELECT id FROM lu_application_groups WHERE application_id='$application_id' AND group_id='$group_id' AND round='$round'";
				$result = mysql_query($query) or die(mysql_error());
				while($row = mysql_fetch_array( $result ))
				{
					$doUpdate = false;
				}
				
				// add user
				if($doUpdate == true)
				{
					$query = "INSERT INTO lu_application_groups( application_id, group_id, round ) VALUES( '$application_id', '$group_id', '$round' )";
					mysql_query($query)or die( mysql_error() );
				}
			}
		}
	}
	
	// set available groups
	// department name thrown in for good measure
	public function setGroups()
	{
		$deptId = $this->deptId;
		
		//GET ALL GROUPS AVAILABLE
		$sql = "SELECT revgroup.id as group_id, revgroup.name,department.name as dept
			FROM revgroup
			LEFT JOIN department 
			ON department.id = revgroup.department_id ";
		
		if($deptId > -1)
		{
			$sql .= " WHERE department_id = $deptId";
		}
        else
		{
			if($_SESSION['A_usertypeid'] != 0)
			{
		        $departmentCount = count($_SESSION['A_admin_depts']);
				for($i = 0; $i < count($departmentCount); $i++)
				{
					if($i == 0)
					{
						$sql .= "WHERE (";
					}else
					{
						$sql .= " OR ";
					}
					$sql .= " department_id = ".$_SESSION['A_admin_depts'][$i];
                    if ($i == ($departmentCount - 1)) {
                        $sql .= ")";
                    }
				}
			}
		}
        
        if ($_SESSION['A_usertypeid'] == 3) {
            $sql .= " AND group_type = 2";
        }
        
		$sql .= " ORDER BY name";
		
		$result = mysql_query($sql) or die(mysql_error());
		
		$groups = array();
		$i=0;
		$applicantGroups = $this->applicantGroups;
		$applicants = $this->applicants;
		$applicantKeys = array_keys($applicants);
		$selectedGroupID = $this->selectedGroupID;
		
		while ( $row = mysql_fetch_assoc($result) )
		{
			$group_id = $row['group_id'];
			if ( ! array_key_exists($group_id, $groups) ) $groups[$group_id] = array();
			unset( $row['group_id'] );
			
			$groups[$group_id]['name'] = $row['name'];	
			$groups[$group_id]['dept'] = $row['dept'];	
			
			// get group member count
			if ( array_key_exists($group_id, $applicantGroups) )
			{
				// get member keys
				$memberKeys = $applicantGroups[$group_id];
			
				$count = 0;
				foreach ( $memberKeys as $key )
				{
					//if ( $this->getApplicant($key) ) $count++;
				    $count++;
                }
				$memberCount = $count; //count($applicantGroups[$group_id]);	
			} else {
				$memberCount = 0;
			}
			$groups[$group_id]['memberCount'] = $memberCount;
									
			// add group members and member count to group
			// for selected group
			if ( $group_id == $selectedGroupID )
			{
				if ( $memberCount )
				{
					//DebugBreak();
                    $this->memberCount = count($applicantGroups[$group_id]);	
					
					// add member keys
					//$memberKeys = $applicantGroups[$group_id];
					$this->members = $memberKeys;
					
					// add non members keys
					$nonMemberKeys = array_diff($applicantKeys, $memberKeys);
					$this->nonMembers = $nonMemberKeys;
                    //$this->nonMembers = $applicantKeys;
                    
				}
				else
				{
					$this->nonMembers = $applicantKeys;
				}
			} 
			
			// set selected department name
			if($i == 0) $this->deptName = $row['dept'];
			$i++;			
		}
		$this->groups = $groups;
        
        if (!$this->deptName && $deptId > -1) {
            $sql = "SELECT name FROM department WHERE id=" . $deptId;
            $result = mysql_query($sql) or die(mysql_error());
            while ( $row = mysql_fetch_assoc($result) ) {
                $this->deptName = $row['name'];
            }
        }
	}
	
	public function getGroups()
	{
		return $this->groups;	
	}

	// if round 2, do something ??
	private function setRoundTwo($allApplicants)
	{
		$deptId = $this->deptId;
		$round = $this->round;
		
		//DO SECONDARY QUERY TO WEED OUT NON-ROUND 2 VOTES
		if($round == 2)
		{
			$allApplicants2 = array();
			//for($i = 0; $i < count($allApplicants); $i++)
			foreach ($allApplicants as $application_id => $applicant)
			{
				$sql = "select review.id,
				group_concat(review.round2) as round2,
				lu_user_department.department_id
				from review 
				inner join lu_users_usertypes on lu_users_usertypes.id = review.reviewer_id
				inner join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
				where application_id=" . $application_id . " and review.round=1 ";
				if($deptId > -1)
				{
					$sql .= "  and lu_user_department.department_id = ". $deptId;
				}
				else
				{
					if($_SESSION['A_usertypeid'] != 0)
					{
						if(count($_SESSION['A_admin_depts']) > 0)
						{
							$sql .= " and (";
						}
						for($j = 0; $j < count($_SESSION['A_admin_depts']); $j++)
						{
							if($j > 0)
							{
								$sql .= " or ";
							}
							$sql .= " lu_user_department.department_id = ".$_SESSION['A_admin_depts'][$j];
						}
						if(count($_SESSION['A_admin_depts']) > 0)
						{
							$sql .= " ) ";
						}
					}
				}
				$sql .= " group by review.application_id";
				$result = mysql_query($sql) or die(mysql_error() . $sql);
				//echo $sql;
				while($row = mysql_fetch_array( $result ))
				{
					$display = true;
					$yes = 0;
					$no = 0;
					$arr = split(",", $row['round2']);//THIS IS THE COMMITTEE VOTE, NOT PROMOTION OVERRRIDE
					if(count($arr) == 0)
					{
						$display = false;
					}
					for($j = 0; $j < count($arr); $j++)
					{
						
						if($_SESSION['A_usertypeid'] != 0)
						{
							
								for($q = 0; $q < count($_SESSION['A_admin_depts']); $q++)
								{
									
									if($_SESSION['A_admin_depts'][$q] ==  $row['department_id']  )
									{
										if($arr[$j] == 1)
										{
											$yes++;
										}
										if($arr[$j] == 0)
										{
											$no++;
											$display = false;
											
										}
										//break;
									}
								}
						}
					}
					$applicant['yes'] = $yes;
					$applicant['no'] = $no;
					//CHECK OVERRIDE
					
					$arr = split(",", $applicant['round2']);//SPLIT INTO PROGRAM_ID:ROUND2
					for($q = 0; $q < count($arr); $q++)
					{
						$arr2 = split(":", $arr[$q]);
						if (strpos($arr[$q], ':2') !== false) {
								$display = false;
								$applicant['overridden'] = "DEMOTED ". $arr2[0] ;
						} 
					}
					for($q = 0; $q < count($arr); $q++)
					{
						$arr2 = split(":", $arr[$q]);
						
						if (strpos($arr[$q], ':1') !== false) {
								$display = true;
								$applicant['overridden'] = "Yes";
						} 
					}
					
					// check for adhoc groups
					// if in adhoc group
					if ( $display == false and $applicant['groups2'] != "" )
					{
						$display = true;
						$applicant['adhoc'] = true;
					}
					
					//echo "<br>";
					if( $display == true  )
					{
						//INSERT INTO THE $allApplicants2 ARRAY
						$allApplicants2[$application_id] = $applicant;
					}
				}
		
			}
			$allApplicants = $allApplicants2;
		}
		return $allApplicants;	
	}
	
	public function getApplicants()
	{
		return $this->applicants;	
	}
	
	
	// applicant + groups functions
	public function setApplicantGroups()
	{
	
		$deptId = $this->deptId;
		$round = $this->round;
				
		//GET applicants / GROUPS
		$sql = "select 
        lu_application_groups.id as groupMemberID, 
        lu_application_groups.application_id, 
        lu_application_groups.group_id, 
        firstname, lastname, countries.name as country, 
        groupDept.name as dept
		from lu_application_groups
		left outer join revgroup on revgroup.id = lu_application_groups.group_id
		left outer join application on application.id = lu_application_groups.application_id ";
        
        if ($this->periodId) {
            $sql .= " INNER JOIN period_application on period_application.application_id = application.id";
        }        
        
		$sql .= " left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		
		left outer join department as groupDept on groupDept.id = revgroup.department_id
		
		where round = ".$round . " ";
        
        if ($this->periodId) {
            
            if ( is_array($this->periodId) ) {
                $admissionPeriodIds = '(' . implode(',' , $this->periodId) . ')';
                $sql .= " AND period_application.period_id IN " . $admissionPeriodIds;    
            } else {
                $sql .= " AND period_application.period_id = " . $this->periodId;    
            }
        }
		
		if($deptId > -1)
		{
			$sql .= " and revgroup.department_id = ". $deptId;
		}
		else
		{
			if($_SESSION['A_usertypeid'] != 0)
			{
				if(count($_SESSION['A_admin_depts']) > 0)
				{
					$sql .= " and (";
				}
				for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
				{
					if($i > 0)
					{
						$sql .= " or ";
					}
					$sql .= " revgroup.department_id = ".$_SESSION['A_admin_depts'][$i];
				}
				if(count($_SESSION['A_admin_depts']) > 0)
				{
					$sql .= " ) ";
				}
			}
		}
		$sql .=" group by lu_application_groups.group_id, application.id order by revgroup.id, lastname, firstname";

		//echo $sql;
		$result = mysql_query($sql) or die(mysql_error());
		$applicantGroups = array();
		while( $row = mysql_fetch_assoc($result) )
		{
			$groupID = $row['group_id'];
			$application_id = $row['application_id'];
			//unset($row['group_id']);
			if ( ! array_key_exists($groupID, $applicantGroups) ) $applicantGroups[$groupID] = array();
			if ( ! in_array( $application_id, $applicantGroups[$groupID] ) ) array_push( $applicantGroups[$groupID], $application_id );
		}
		$this->applicantGroups = $applicantGroups;
	}
	
	public function getApplicantGroups()
	{
		return $this->applicantGroups;	
	}
	
	public function setGroupData()
	{
        $this->setApplicants();
        if ( isset($this->searchString) && $this->searchString ) {
            $this->setAllApplicants(); 
        } else { 
            $this->allApplicants = $this->applicants;
        }
        $this->setApplicantGroups();
		$this->setGroups();
	}
	
	public function getMembers()
	{
		return $this->members;	
	}	
	
	public function getNonMembers()
	{
		return $this->nonMembers;	
	}	
	
	public function getSelectedGroupID()
	{
		return $this->selectedGroupID;	
	}
	
	public function getRound()
	{
		return $this->round;	
	}
	
	public function getSearchString()
	{
		return $this->searchString;	
	}	
	
	public function getSort()
	{
		$sort = $this->sort;	
		
		// if "name", the default sort
		// so no sort needs to be set
		if ( $sort == "name" ) $sort = FALSE;
		
		return $sort;
	}	
	
	public function getApplicant( $application_id )
	{
		//$applicants = $this->applicants;
		$applicants = $this->allApplicants;
        if ( array_key_exists($application_id, $applicants) ) 
			return $applicants[$application_id];	
		else 
			return FALSE;
	}
    
    public function getNonmember( $application_id )
    {
        $applicants = $this->applicants;
        if ( array_key_exists($application_id, $applicants) ) 
            return $applicants[$application_id];    
        else 
            return FALSE;
    }



    public function setApplicants() {
        
        if ($this->deptId == -1) {
            if (isset($_SESSION['A_admin_depts'])) {
                foreach ($_SESSION['A_admin_depts'] as $deptId) {
                    $this->deptId = $deptId;
                }
            }
        }
        
        $applicantDataList = new ApplicantGroupListData($this->deptId);
        $applicantData = $applicantDataList->getData($this->periodId, $this->round, NULL, 'allGroups', $this->searchString, $this->sort);
        
        $applicants = array();      
        foreach ( $applicantData as $row )
        {
            // format name
            /*
            $firstname = ucwords( strtolower($row['firstname']) );
            $lastname = ucwords( strtolower($row['lastname']) );
            */
            $nameArray = explode(',', $row['name']);
            $firstname = $nameArray[0];
            $lastname = $nameArray[1];
                        
            $application_id = $row['application_id'];
            $applicants[$application_id]['firstname'] = $firstname;
            $applicants[$application_id]['lastname'] = $lastname;
            $applicants[$application_id]['country'] = $row['cit_country'];
            $applicants[$application_id]['gender'] = $row['gender'];
            $applicants[$application_id]['programs'] = $row['programs'];
            $applicants[$application_id]['countryAbbr'] = $row['cit_country_iso_code'];
            $applicants[$application_id]['nameLink'] = 
                "<a href='../review/userroleEdit_student_print.php?id={$row['lu_users_usertypes_id']}&amp;applicationId={$row['application_id']}' target='_blank'>{$row['name']}</a>";
            $applicants[$application_id]['isoLink'] = "<a href='javascript:;' title='{$row['cit_country']}' >{$row['cit_country_iso_code']}</a>";

            /*
                GROUP_CONCAT(distinct concat('<div class=\'interests\'><em>',degree.name, ' - ' ,fieldsofstudy.name, '</em></div>') SEPARATOR '' ) , 
                GROUP_CONCAT(distinct concat('<div class=\'interests\'><em>',
                    degree.name, ' - ' ,fieldsofstudy.name, ' (', lu_application_interest.choice+1, 
                    ')</em> - <strong>',  interest.name, '</strong></div>') ORDER BY lu_application_interest.choice SEPARATOR '')
                ) as interests,
            */
            /*
            $programsArray = explode('|', $row['programs']);
            $programs = '';
            foreach ($programsArray as $program) {
                $programs .= ''
            }
            */ 
            $applicants[$application_id]['interests'] = str_replace('|', '<br/>', $row['areas_of_interest']);
            
            $applicants[$application_id]['undergradName'] = $row['undergrad_institute_name'];
            $applicants[$application_id]['round2'] = $row['votes_for_round2'];
            $applicants[$application_id]['discuss'] = "";//DISCUSS THIS CANDIDATE...
            $applicants[$application_id]['yes'] = 0;//yes votes
            $applicants[$application_id]['no'] = 0;//no votes
            $applicants[$application_id]['groups'] = str_replace('|', '<br/>', $row['round1_groups']);
            $applicants[$application_id]['groups2'] = str_replace('|', '<br/>', $row['round2_groups']);
            $applicants[$application_id]['groups3'] = str_replace('|', '<br/>', $row['round3_groups']);
            $applicants[$application_id]['groups4'] = str_replace('|', '<br/>', $row['round4_groups']);
            $applicants[$application_id]['overridden'] = "";//overridden vote
            if ( isset($this->searchString) && $this->searchString ) {
                $applicants[$application_id]['searchContext'] = $row['searchContext'];
            } else { 
                $applicants[$application_id]['searchContext'] = "";
            }
            $applicants[$application_id]['adhoc'] = FALSE;
        }

        // This is taken care of with the round param.
        //$applicants = $this->setRoundTwo($applicants);
        
        $this->applicants = $applicants;
    }
    
    public function setAllApplicants() {
        
        if ($this->deptId == -1) {
            if (isset($_SESSION['A_admin_depts'])) {
                foreach ($_SESSION['A_admin_depts'] as $deptId) {
                    $this->deptId = $deptId;
                }
            }
        }
        
        $applicantDataList = new ApplicantGroupListData($this->deptId);
        $applicantData = $applicantDataList->getData($this->periodId, $this->round, NULL, 'allGroups');
        
        $applicants = array();      
        foreach ( $applicantData as $row )
        {
            $nameArray = explode(',', $row['name']);
            $firstname = $nameArray[0];
            $lastname = $nameArray[1];
                        
            $application_id = $row['application_id'];
            $applicants[$application_id]['firstname'] = $firstname;
            $applicants[$application_id]['lastname'] = $lastname;
            $applicants[$application_id]['country'] = $row['cit_country'];
            $applicants[$application_id]['gender'] = $row['gender'];
            $applicants[$application_id]['countryAbbr'] = $row['cit_country_iso_code'];
            $applicants[$application_id]['nameLink'] = 
                "<a href='../review/userroleEdit_student_print.php?id={$row['lu_users_usertypes_id']}&applicationId={$row['application_id']}' target='_blank'>{$row['name']}</a>";
            $applicants[$application_id]['isoLink'] = "<a href='javascript:;' title='{$row['cit_country']}' >{$row['cit_country_iso_code']}</a>";
            $applicants[$application_id]['interests'] = str_replace('|', '<br/>', $row['areas_of_interest']);
            
            $applicants[$application_id]['undergradName'] = $row['undergrad_institute_name'];
            $applicants[$application_id]['round2'] = $row['votes_for_round2'];
            $applicants[$application_id]['discuss'] = "";//DISCUSS THIS CANDIDATE...
            $applicants[$application_id]['yes'] = 0;//yes votes
            $applicants[$application_id]['no'] = 0;//no votes
            $applicants[$application_id]['groups'] = str_replace('|', '<br/>', $row['round1_groups']);
            $applicants[$application_id]['groups2'] = str_replace('|', '<br/>', $row['round2_groups']);
            $applicants[$application_id]['groups3'] = str_replace('|', '<br/>', $row['round3_groups']);
            $applicants[$application_id]['groups4'] = str_replace('|', '<br/>', $row['round4_groups']);
            $applicants[$application_id]['overridden'] = "";//overridden vote
            $applicants[$application_id]['searchContext'] = "";
            $applicants[$application_id]['adhoc'] = FALSE;
        }

        // This is taken care of with the round param.
        //$applicants = $this->setRoundTwo($applicants);
        
        $this->allApplicants = $applicants;
    }
 
/*
    // Applicants functions
    function setApplicants()
    {
        $deptId = $this->deptId;
        
        // full text searching
        $searchString = $this->searchString;
        if ( $searchString )
        {
            // format search string for boolean mode query
            $formattedSearchString = "";
            $ssParts = split( " ", $searchString);
            foreach ( $ssParts as $word )
            {
                $formattedSearchString .= "+" . $word . " ";
            }
            $formattedSearchString = trim($formattedSearchString);
            
            // addition to select parameters
            $stringEnd = strlen($searchString) + 200;
            $searchSelect = ", SUBSTRING( searchMatches.application_text, 
                LOCATE('$searchString', searchMatches.application_text) -100, $stringEnd ) as searchContext";
            // addition to join
            $searchJoin = " INNER JOIN (
                    SELECT application_id, application_text 
                    FROM searchText st 
                    WHERE MATCH (application_text) 
                    AGAINST( '$formattedSearchString' IN BOOLEAN MODE ) 
                ) AS searchMatches 
                ON searchMatches.application_id = application.id ";
        } else {
            $searchJoin = $searchSelect = "";    
        }
        
        $sql = "SET SESSION group_concat_max_len=4096;";
        $result = mysql_query($sql);
        
        //GET ALL applicants AVAILABLE
        $sql = "SELECT application.id,
            lu_users_usertypes.id as userid, 
            firstname, lastname, 
            countries.name as country,
            countries.iso_code,
            users_info.gender, 
            undergradInstitution.undergradName,
            if(lu_application_interest.choice is null, 
                GROUP_CONCAT(distinct concat('<div class=\'interests\'><em>',degree.name, ' - ' ,fieldsofstudy.name, '</em></div>') SEPARATOR '' ) , 
                GROUP_CONCAT(distinct concat('<div class=\'interests\'><em>',
                    degree.name, ' - ' ,fieldsofstudy.name, ' (', lu_application_interest.choice+1, 
                    ')</em> - <strong>',  interest.name, '</strong></div>') ORDER BY lu_application_interest.choice SEPARATOR '')
                ) as interests,
            GROUP_CONCAT(concat(lu_application_programs.program_id,':',lu_application_programs.round2)) as round2,
            GROUP_CONCAT(distinct revgroup.name SEPARATOR '<br>') as groups,
            GROUP_CONCAT(distinct revgroup2.name SEPARATOR '<br>') as groups2
            $searchSelect
            from  application
            left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id
            inner join users on users.id = lu_users_usertypes.user_id
            left outer join users_info on users_info.user_id = lu_users_usertypes.id
            left outer join countries on countries.id = users_info.address_perm_country
            left outer join lu_application_programs on lu_application_programs.application_id = application.id
            left outer join programs on programs.id = lu_application_programs.program_id
            left outer join degree on degree.id = programs.degree_id
            left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
            left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
            left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
            left outer join interest on interest.id = lu_application_interest.interest_id 
            left outer join lu_application_groups on lu_application_groups.application_id = application.id and lu_application_groups.round = 1
            left outer join revgroup on revgroup.id = lu_application_groups.group_id
            left outer join (
                SELECT u.user_id as usersinstUser_id, i.name as undergradName 
                FROM usersinst u, institutes i
                WHERE u.educationtype = 1 AND u.institute_id = i.id
            ) as undergradInstitution on undergradInstitution.usersinstUser_id = lu_users_usertypes.id ";
        
        if($deptId > -1)
        {
            // if department id, use that department
            $sql .= "  AND revgroup.department_id = ". $deptId;
        }
        else
        {
            if($_SESSION['A_usertypeid'] != 0)
            {
                if(count($_SESSION['A_admin_depts']) > 0)
                {
                    $sql .= " AND (";
                }
                for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
                {
                    if($i > 0)
                    {
                        $sql .= " or ";
                    }
                    $sql .= " revgroup.department_id = ".$_SESSION['A_admin_depts'][$i];
                }
                if(count($_SESSION['A_admin_depts']) > 0)
                {
                    $sql .= " ) ";
                }
            }
        }
        $sql .= "LEFT OUTER JOIN lu_application_groups as lu_application_groups2  
                ON lu_application_groups2.application_id = application.id 
                AND lu_application_groups2.round = 2
            LEFT OUTER JOIN revgroup as revgroup2 
                ON revgroup2.id = lu_application_groups2.group_id ";
        
        
        if($deptId > -1)
        {
            $sql .= "  AND revgroup2.department_id = ". $deptId;
        }
        else
        {
            if($_SESSION['A_usertypeid'] != 0)
            {
                if(count($_SESSION['A_admin_depts']) > 0)
                {
                    $sql .= " AND (";
                }
                for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
                {
                    if($i > 0)
                    {
                        $sql .= " or ";
                    }
                    $sql .= " revgroup2.department_id = ".$_SESSION['A_admin_depts'][$i];
                }
                if(count($_SESSION['A_admin_depts']) > 0)
                {
                    $sql .= " ) ";
                }
            }
        }
        
        // include search Join
        $sql .= $searchJoin;
        
        $sql .= " WHERE (lu_users_usertypes.usertype_id > -1 ) and application.submitted=1 and application.id > 0";
        
        if($deptId > -1)
        {
            $sql .= "  AND lu_programs_departments.department_id = ". $deptId;
        }
        else
        {
            if($_SESSION['A_usertypeid'] != 0)
            {
                if(count($_SESSION['A_admin_depts']) > 0)
                {
                    $sql .= " AND (";
                }
                for($i = 0; $i < count($_SESSION['A_admin_depts']); $i++)
                {
                    if($i > 0)
                    {
                        $sql .= " or ";
                    }
                    $sql .= " lu_programs_departments.department_id = ".$_SESSION['A_admin_depts'][$i];
                }
                if(count($_SESSION['A_admin_depts']) > 0)
                {
                    $sql .= " ) ";
                }
            }
        }
        
        
        // SORT
        $sort = $this->sort;
        $showAllInterests = $this->showAllInterests;
        
        switch($sort)
        {
            case "name":
                $sql .= " GROUP BY application.id ";
                $sql .= " ORDER BY lastname,firstname ";
                break;
        
            case "ctzn":
                $sql .= " GROUP BY application.id ";
                $sql .= " ORDER BY countries.iso_code ";
                break;
        
            case "int1":
                if($showAllInterests != true)
                {
                    $sql .= " AND ( lu_application_interest.choice = 0 or lu_application_interest.id IS NULL )
                        GROUP BY lu_application_interest.interest_id, application.id 
                        ORDER BY  lu_programs_departments.department_id, lu_application_programs.program_id ";
                }else
                {
                    $sql .= " GROUP BY application.id
                        ORDER BY  lu_programs_departments.department_id, lu_application_programs.choice, 
                            lu_application_interest.interest_id, lu_application_interest.choice ";
                }
                break;
            
            case "int2":
                if($showAllInterests != true)
                {
                    $sql .= " and ( lu_application_interest.choice = 1 or lu_application_interest.id IS NULL )
                        GROUP BY lu_application_interest.interest_id, application.id
                        ORDER BY  lu_programs_departments.department_id, lu_application_programs.program_id ";
                }else
                {
                    $sql .= " GROUP BY application.id 
                        ORDER BY  lu_programs_departments.department_id, lu_application_programs.choice, 
                            lu_application_interest.interest_id, lu_application_interest.choice ";
                }
                break;
            
            case "int3":
                if($showAllInterests != true)
                {
                    $sql .= " and ( lu_application_interest.choice = 2 or lu_application_interest.id IS NULL )
                        GROUP BY lu_application_interest.interest_id, application.id
                        ORDER BY  lu_programs_departments.department_id, lu_application_programs.program_id ";
                }else
                {
                    $sql .= " GROUP BY application.id
                        ORDER BY  lu_programs_departments.department_id, lu_application_programs.choice, lu_application_interest.interest_id, lu_application_interest.choice ";
                }
                break;
        
            case "grp":
                $sql .= " GROUP BY application.id
                    ORDER BY lu_application_groups.group_id";
                break;
        }
        
        $result = mysql_query($sql) or die(mysql_error() . $sql);
        
        // apply business rules to data
        $applicants = array();
        while ( $row = mysql_fetch_assoc($result) )
        {
            // format name
            $firstname = ucwords( strtolower($row['firstname']) );
            $lastname = ucwords( strtolower($row['lastname']) );
                        
            $application_id = $row['id'];
            $applicants[$application_id]['firstname'] = $firstname;
            $applicants[$application_id]['lastname'] = $lastname;
            $applicants[$application_id]['country'] = $row['country'];
            $applicants[$application_id]['gender'] = $row['gender'];
            $applicants[$application_id]['countryAbbr'] = $row['iso_code'];
            $applicants[$application_id]['nameLink'] = "<a href='userroleEdit_student_formatted.php?v=1&r=1&id={$row['userid']}' target='_blank'>$lastname, $firstname</a>";
            $applicants[$application_id]['isoLink'] = "<a href='javascript:;' title='{$row['country']}' >{$row['iso_code']}</a>";
            $applicants[$application_id]['interests'] = $row['interests'];
            $applicants[$application_id]['undergradName'] = $row['undergradName'];
            $applicants[$application_id]['round2'] = $row['round2'];
            $applicants[$application_id]['discuss'] = "";//DISCUSS THIS CANDIDATE...
            $applicants[$application_id]['yes'] = 0;//yes votes
            $applicants[$application_id]['no'] = 0;//no votes
            $applicants[$application_id]['groups'] = $row['groups'];
            $applicants[$application_id]['groups2'] = $row['groups2'];
            $applicants[$application_id]['overridden'] = "";//overridden vote
            if ( $searchString ) $applicants[$application_id]['searchContext'] = $row['searchContext'];
            else $applicants[$application_id]['searchContext'] = "";
            $applicants[$application_id]['adhoc'] = FALSE;
        }
        $applicants = $this->setRoundTwo($applicants);
        $this->applicants = $applicants;
    }
*/    


}
?>