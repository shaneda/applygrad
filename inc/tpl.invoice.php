<html>
<head>
</head>
<body>

<p>
<?php 
if ($submittedDate) {
    $timestamp = strtotime($submittedDate);    
} else {
    $timestamp = time();
}
echo date('F j, Y', $timestamp); 
?>
</p>

<p>
INVOICE
<br>
<?php echo '#' . $invoiceNumber; ?>
</p>

<p>
<?php
$address = '';
if ( isset($organization) && $organization) {
    $address .= $organization . '<br>';    
}
$address .= $street1;
if ($street2) {
    $address .= '<br>' . $street2;
}
if ($street3) {
    $address .= '<br>' . $street3;
}
if ($street4) {
    $address .= '<br>' . $street4;
}
$address .= '<br>' . $city;
if ($state) {
    $address .= ', ' . $state;
}
$address .= ' ' . $postal;
if ($country) {
    $address .= '<br>' . $country;
}

echo <<<EOB
{$studentName}<br>
{$address}
EOB;
?>
</p>

<p>Please remit the following:</p>

<table border="1" width="80%">
    <tr>
        <th width="70%" align="left">Purpose</th>
        <th  width="30%" align="right">Tuition</th>
    </tr>
<?php
$i = 1;
foreach ($myPrograms as $program) {
    $programName = $program[1] . ' ' . $program[2] . ' ' . $program[3];
?>
    <tr>
        <td align="left"><?php echo $programName; ?></td>
        <td align="right">
        <?php
        if ($i == 1) {
            echo '$' . $program[8];    
        } else {
            echo '$' . $program[6];    
        }
        ?>
        </td>
    </tr>
<?php  
    $i++;
}
?>    
</table>

<br>
<div>
<?php
/* 
* New 5/31/11: Credit card payment confirmation. 
*/
foreach ($payments as $payment) {
    if ($payment['payment_type'] == 2 && $payment['payment_status'] == 'paid') {
        echo '<b>Paid on ' . date( 'F j, Y', strtotime($payment['payment_intent_date']) );
        echo ' by credit card: $' . $payment['payment_amount'] . '</b><br>';
    }   
}
?>
</div>

<?php
$sql = "select content from content where name='Invoice' and domain_id=".$domainid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    //echo html_entity_decode($row["content"]);
    $contentString = html_entity_decode($row["content"]);
    echo parseEmailTemplate2($contentString, $contentVars);
    echo "<br>";
}
?>

</body>
</html>