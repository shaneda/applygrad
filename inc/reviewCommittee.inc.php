<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$certaintyVals = array(
array(5, 5),
array(4, 4),
array(3, 3),
array(2, 2),
array(1, 1)
);

$csdVoteVals = array(
array(1, "Top 10 (1%)"),
array(2, "Top 25 (2.5%)"),
array(3, "Top 50 (5%)"),
array(4, "Top 100 (10%)"),
array(5, "Top 250 (25%)"),
array(6, "Bottom 75%")
);

$compBioVoteVals = array(
array(1.1, 1.1),
array(1.2, 1.2),
array(1.3, 1.3),
array(1.5, 1.5),
array(1.7, 1.7),
array(1.9, 1.9),
array(2, 2),
array(2.5, 2.5),
array(3, 3),
array(4, 4),
array(5, 5)
);

$machineLearningVoteVals = array(
array(1, 1),
array(1.25, 1.25),
array(1.5, 1.5),
array(1.75, 1.75),
array(2, 2),
array(2.5, 2.5),
array(3, 3),
array(4, 4),
array(5, 5)
);

if ($thisDept == 2) {
    $voteVals = $machineLearningVoteVals;
} elseif ($thisDept == 8) {
    $voteVals = $compBioVoteVals; 
} else {
    $voteVals = $csdVoteVals;    
}


$allowEdit = TRUE;
$previousRoundData = FALSE;
for($i = 0; $i < count($committeeReviews); $i++) {
    if( $committeeReviews[$i][4] == $reviewerId 
        && $committeeReviews[$i][29] == "" ) 
    {   
        $comments = $committeeReviews[$i][9];
        $private_comments = $committeeReviews[$i][12];
        $point = $committeeReviews[$i][10];
        $pointCertainty = $committeeReviews[$i][27]; 

        if ($committeeReviews[$i][23] != $round) 
        {
            $previousRoundData = TRUE;
            $reviewRound = $committeeReviews[$i][23];
            $allowEdit = FALSE;
        }
    } 
}  
?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<input name="showDecision" type="hidden" value="<?=$showDecision?>">
<input name="point2" type="hidden" value="<?=$point2?>">
<?php 
if($round == 2){
?>
    <input name="round2" type="hidden" value="<?=$round2?>">
<?php 
} 

if ($previousRoundData) {
    echo '<span style="font-size: 12px; font-weight: bold; font-style: italic;">';
    echo 'You reviewed this application in round ' . $reviewRound . '</span>';    
    echo '<br/><br/>';
}

include "../inc/special_phone_email.inc.php";
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">
    <?php 
        if ($thisDept != 5) {
    ?>
    <tr>
        <td width="50"><strong>Comments:</strong></td>
        <td>
        <?php 
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 80); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea); 
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Personal Comments: </strong> </td>
        <td>
        <?php  
        ob_start();
        showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 40); 
        $personalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $personalCommentsTextarea); 
        ?>    
        </td>
    </tr>
    <?php
       }  
     ?>

    <tr>
        <td width="50"><strong>Ranking:</strong>
        <?php
        if ($thisDept == 2) 
        {
        ?>
            <br>(1 is best, <br>5 is worst)
        <?php
        }
        ?>
        </td>
        <td>
        <?php
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $voteVals);
        ?>
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Certainty:</strong></td>
        <td>
        <?php
        showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals); 
        ?>
        </td>
    </tr>
    
<?php 
if($round  == 1 ) { 
?>
  <tr>
    <td width="50"><strong> Round 2: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $round2 = $committeeReviews[$i][13];
        }
     }

    showEditText($round2, "radiogrouphoriz2", "round2", $allowEdit, false, array(array(1,"Yes"),array(0,"No")) ); ?>    </td>
  </tr>
 
<?php 
} 
?>

</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
