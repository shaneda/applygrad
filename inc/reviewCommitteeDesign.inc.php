<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$certaintyVals = array(
array(5, 5),
array(4, 4),
array(3, 3),
array(2, 2),
array(1, 1)
);

$voteVals = array(
array(5, "5 Strong"),
array(4, "4"),
array(3, "3"),
array(2, "2"),
array(1, "Weak 1"),
);

$mseRiskFactorKey = array(
    1 => 'Language',
    2 => 'Experience',
    3 => 'Academic',
    4 => 'Other',
);

/*
* Recapitulate reviewData.inc.php to handle coordinator difference. 
*/
$mseRiskFactors = array();
// DAS $mseBridgeCourses = array();

for($i = 0; $i < count($committeeReviews); $i++)
{   
    if( $committeeReviews[$i][32]== $thisDept
        && $committeeReviews[$i][30] == 0   // not faculty review 
        && ($committeeReviews[$i][29] == ""  // not supplemental review
            || $committeeReviews[$i][29] == NULL)
        && $committeeReviews[$i][4] == $reviewerId) 
    {   
        $comments = $committeeReviews[$i][9];
        $private_comments = $committeeReviews[$i][12];
        $grades =  $committeeReviews[$i][7];
        $point = $committeeReviews[$i][10];
        $point2 = $committeeReviews[$i][11];
        $mseRiskFactors = $db_risk_factors->getRiskFactors($appid, $committeeReviews[$i][4]);
        $mseBridgeCourses = $db_bridge_courses->getBridgeCourse($appid, $committeeReviews[$i][4]);
        $interviewDate = $committeeReviews[$i][34];
        $interviewType = $committeeReviews[$i][35];
        $interviewTypeText = $committeeReviews[$i][36];
        $interviewComments = $committeeReviews[$i][37];
        $bridgeCourse = $committeeReviews[$i][38];
    
        if ($committeeReviews[$i][23] != $round) 
        {
            $previousRoundData = TRUE;
            $reviewRound = $committeeReviews[$i][23];
            $allowEdit = FALSE;
        } 
    }
}
?>

<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<input name="showDecision" type="hidden" value="<?=$showDecision?>">
<input name="point2" type="hidden" value="<?=$point2?>">
<?php 
if($round == 2){
?>
    <input name="round2" type="hidden" value="<?=$round2?>">
<?php 
} 

if ($previousRoundData) {
    echo '<span style="font-size: 12px; font-weight: bold; font-style: italic;">';
    echo 'You reviewed this application in round ' . $reviewRound . '</span>';    
    echo '<br/><br/>';
}

// include "../inc/special_phone_email.inc.php";
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

    <tr>
        <td width="50"><strong>Comments:</strong></td>
        <td>
        <?php 
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 80); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea); 
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Personal Comments: </strong> </td>
        <td>
        <?php  
        ob_start();
        showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 40); 
        $personalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $personalCommentsTextarea); 
        ?>    
        </td>
    </tr>
    
    <tr>
        <td width="50"><strong>Teaching Potential:</strong></td>
        <td>
        <?php 
        ob_start();
        showEditText($background, "textarea", "background", $allowEdit, false, 80); 
        $teachingPotentialTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $teachingPotentialTextarea); 
        ?>    
        </td>
    </tr>
        
    <tr>
        <td width="50"><strong>Campus Visit:</strong></td>
        <td>
        <?php 
        ob_start();
        showEditText($grades, "textarea", "grades", $allowEdit, false, 80); 
        $camusVisitTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $camusVisitTextarea); 
        ?>    
        </td>
    </tr>
    
    <tr>
        <td width="50"><strong>Risk Factors:</strong></td>
        <td>
        <?php
        $risk_factor_language_checked = "";
        $risk_factor_experience_checked = "";
        $risk_factor_academic_checked = "";
        $risk_factor_other_checked = "";
        $risk_factor_text = "";
        $risk_factor_disabled = "";
         
        foreach ($mseRiskFactors as $risk_factor) {
        
            if ($risk_factor['language'] == 1) {
                $risk_factor_language_checked = "checked";
            }
            if ($risk_factor['experience'] == 1) {
                $risk_factor_experience_checked = "checked";
            }
            if ($risk_factor['academic'] == 1) {
                $risk_factor_academic_checked = "checked";
            }
            if ($risk_factor['other'] == 1) {
                $risk_factor_other_checked = "checked";
            }
            if ($risk_factor['other_text']) {
                $risk_factor_text = $risk_factor['other_text'];    
            }   
        }
        
        if (!$allowEdit) {
            $risk_factor_disabled = "disabled";
        }
        ?> 
        <input name="mse_review" type="hidden" value="true" />    
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="language" <?= $risk_factor_language_checked ?> <?= $risk_factor_disabled ?> />Language
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="experience" <?= $risk_factor_experience_checked ?> <?= $risk_factor_disabled ?> />Experience
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="academic" <?= $risk_factor_academic_checked ?> <?= $risk_factor_disabled ?> />Academic
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="other" <?= $risk_factor_other_checked ?> <?= $risk_factor_disabled ?> />Other
        <div style="margin:5px;">
        Other Risk Factor(s):
        <?php
        showEditText($risk_factor_text, "textbox", "mse_risk_factor_text",
             $allowEdit, false, null, true, 25);
        ?>
        </div>
        </td>
    </tr>
    

    <tr>
        <td width="50"><strong>Ranking:</strong></td>
        <td>
        <?php
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $voteVals);
        ?>
        </td>
    </tr>
    
<?php /*
if($round == 1) { 
?>
  <tr>
    <td width="50"><strong> Round 2: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $round2 = $committeeReviews[$i][13];
        }
     }

    showEditText($round2, "radiogrouphoriz2", "round2", $allowEdit, false, array(array(1,"Yes"),array(0,"No")) ); ?>    </td>
  </tr>
 
<?php 
}   */
?>

</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
