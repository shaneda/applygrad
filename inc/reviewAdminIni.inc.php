<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';
?> 

<input type="hidden" name="departmentId" value="<?php echo $departmentId; ?>" />
<input type="hidden" name="round" value="<?php echo $round; ?>" />
<input type="hidden" name="applicationId" value="<?php echo $applicationId; ?>" />
<input type="hidden" name="iniReviewId" value="<?php echo $iniReviewId; ?>" />

<div style="font-weight: bold; color: red;"><?php echo $saveError; ?></div>
<div style="font-weight: bold; color: green;">
<?php 
if (isset($_POST['btnSubmit']) && !$saveError)
{
    echo 'Review saved';    
}
?>
</div>

<p style="color: red;">
* Required
</p>

<div>
    <div style="padding: 5px;">
        <span style="font-weight: bold;">Comments</span>
        <span style="color: red;">*</span>
        <br/>
        <?php 
        ob_start();
        showEditText($adminComments, "textarea", "adminComments", $allowEdit, false, 80); 
        $adminCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $adminCommentsTextarea); 
        ?>
    </div>    
</div>

<div style="margin-top: 10px;">
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
