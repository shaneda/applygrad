<?php
$applicantInfo = getApplicantInfo($applicationId);
$contentTemplates = getContentTemplates($unit, $unitId);
if ($unit == 'department' && isIniDepartment($unitId))
{
    $admittedPrograms = getAdmittedProgramsIni($applicationId);
}
elseif ($unit == 'department' && isDesignDepartment($unitId))
{
    $admittedPrograms = getAdmittedProgramsDesignMasters($applicationId);
}
else
{
    $admittedPrograms = getAdmittedPrograms($applicationId);    
}
$contentValues = array(
    'Date' => date('F j, Y'),
    'Address' => getAddress($applicantInfo),
    'FirstName' => $applicantInfo['firstname'],
    'LastName' => $applicantInfo['lastname'],
    'AdmissionProgram1' => isset($admittedPrograms[0]) ? $admittedPrograms[0]['program_name'] : '[Program 1]',
    'AdmissionProgram2' => isset($admittedPrograms[1]) ? $admittedPrograms[1]['program_name'] : '[Program 2]',
    'ScholarshipAmount' => isset($admittedPrograms[0]) && isset($admittedPrograms[0]['scholarship_amt']) 
        ? number_format($admittedPrograms[0]['scholarship_amt'], 2) : '[scholarship amount]'
);
?>
<div style="padding: 60px; width: 500px; background-color: white; font-family: sans-serif;">

<?php
if (is_array($selectedLetterSections))
{
    foreach ($selectedLetterSections as $selectedLetterSection => $value)
    {
        if (array_key_exists($selectedLetterSection, $contentTemplates))
        {
            echo parseContentTemplate($contentTemplates[$selectedLetterSection]['content'], $contentValues);
        }
    }
}  
?>

</div>