<?php
$resumeFilepath = getFilePath(2, 1, $uid, $umasterid, $appid);

if ($isDesignPhdDepartment || $isDesignDdesDepartment)
{
    $bioFilepath = getFilePath(29, 1, $uid, $umasterid, $appid);
    $statementFilepath = getFilePath(28, 1, $uid, $umasterid, $appid);    
}
else
{    
    $statementFilepath = getFilePath(4, 1, $uid, $umasterid, $appid);
}
   
$researchFilepath = NULL;
$researchExperience = getExperience($appid, 1);
if ( isset($researchExperience[0]) ) {
    $researchFilepath = getFilePath(5, 1, $uid, $umasterid, $appid, $researchExperience[0][0]);    
}

$industryFilepath = NULL;
$industryExperience = getExperience($appid, 2);
if ( isset($industryExperience[0]) ) {
    $industryFilepath = getFilePath(5, 2, $uid, $umasterid, $appid, $industryExperience[0][0]);    
}

$professionalFilepath = NULL;
$professionalExperience = getExperience($appid, 4);
if ( isset($professionalExperience[0]) ) {
    $professionalFilepath = getFilePath(5, 4, $uid, $umasterid, $appid, $professionalExperience[0][0]);    
}

$academicFilepath = NULL;    
$academicExperience = getExperience($appid, 3);
if ( isset($academicExperience[0]) ) {
    $academicFilepath = getFilePath(5, 3, $uid, $umasterid, $appid, $academicExperience[0][0]);    
}   

$odrIndicator = ' <i><a title="Official Document Received">(ODR)</a></i>';  
?>

<div class="accordionApplication">

    <div class="heading applicantName" tabindex="0">
    <?php
    if (isRiMsRtChinaDepartment($thisDept))
    {
        $lName = strtoupper($lName);    
    }

    echo $lName .", ".$fName ." ". $mName ." ". $title; 
    ?>
    <span class="switch" style="font-size: 10px; color: blue;"></span> 
    </div>
    
    <?php
    if ($allTranscriptsReceived && $allScoresReceived) {
    ?>
        <div class="section">
        <div class="odr">
        <i>(All Official Documents Received)</i>
        </div>
        </div>
    <?php
    }
    ?> 

    <div class="accordion section programs">
    <div id="programs" class="heading" tabindex="0"><span class="switch"></span>Programs</div>
    <div class="content">
    <?php
    foreach ($myPrograms as $myProgram) {
    ?>
        <div class="subsection program">
            <div class="subheading">
            <? echo $myProgram[1] . " ".$myProgram[2] . " ".$myProgram[3]; ?>
            </div>
            
            <?php
            $interests = array();
            foreach (getMyAreas($myProgram[0], $appid) as $myArea) {
                $interests[] = $myArea[1];    
            }
            
            if ( count($interests) ) {
            ?>           
                <div class="element interests">
                    <div class="label">Interests</div>
                    <div class="value">
                    <?php echo implode('; ', $interests); ?>
                    </div>
                </div>
            <?php
            }
            
            $programAdvisors = array();
            foreach ($myAdvisors2 as $myAdvisor) {
                if ($myAdvisor['program_id'] == $myProgram[0]
                    || $myProgram[0] == 27) // CNBC advisors are not identified by program. 
                {
                    if ($myAdvisor['advisor_name']) {
                        $programAdvisors[] = $myAdvisor['advisor_name'];
                    }    
                }
            }
            
            if ( count($programAdvisors) ) {      
            ?>
                <div class="element possibleAdvisors">
                    <div class="label">Possible Advisors</div>
                    <div class="value">
                    <?php echo implode('; ', $programAdvisors); ?>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    <?php
    }
    
    if ($aco)
    {
    ?>
        <div class="subsection program">
            <div class="subheading">ACO</div>
        </div>
    <?php    
    }
    
    if ($pal)
    {
    ?>
        <div class="subsection program">
            <div class="subheading">PAL</div>
        </div>
    <?php    
    }
    
    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
        
    <?php
    if ( $resumeFilepath !== NULL || $statementFilepath !== NULL
        || $researchFilepath !== NULL || $industryFilepath !== NULL 
        || $professionalFilepath !== NULL || $academicFilepath !== NULL ) {
            
        $statementHeading = 'Statements';
        $statementDocName = 'Statement of Purpose';
        if ($isHistoryDepartment)
        {
            $resumeDocName = 'C.V.';
            $bioDocName = '';
        }
        elseif ($isDesignPhdDepartment || $isDesignDdesDepartment)
        {
            $resumeDocName = 'Resume';
            $bioDocName = 'Biographical Essay';
            $statementHeading = $statementDocName = 'Research Proposal';
        }
        else
        {
            $bioDocName = '';
            $resumeDocName = 'Resume';
        }
    ?>
    <div class="accordion supportingDocuments section">
        <div id="supportingDocuments" class="heading" tabindex="0">
            <span class="switch"></span>
            <?php 
                echo $resumeDocName; 
                if ($bioDocName)
                {
                    echo ' / ' . $bioDocName;
                }
            ?> 
            / <?php echo $statementHeading; ?>
        </div>
        <div class="content">
            <?php
            if ($resumeFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $resumeFilepath . '">' . $resumeDocName . '</a></div>';    
            }
            if ($bioFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $bioFilepath . '">' . $bioDocName . '</a></div>';    
            }
            if ($statementFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $statementFilepath . '">' . $statementDocName . '</a></div>';    
            }
            if ($researchFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $researchFilepath . '">Research&nbsp;Experience</a></div>';    
            }
            if ($industryFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $industryFilepath . '">Industry&nbsp;Experience</a></div>';    
            }
            if ($professionalFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $professionalFilepath . '">Professional&nbsp;Experience</a></div>';    
            }
            if ($academicFilepath !== NULL) {
                echo '<div class="statement">
                    <a target="_blank" href="' . $academicFilepath . '">Academic&nbsp;Experience</a></div>';    
            }
            
            if ( isIniDepartment($thisDept) ) 
            {
                echo '<div class="statement">
                    <a target="_blank" href="iniSop.php?appid=' . $appid . '">Statement&nbsp;of&nbsp;Purpose</a></div>';
            }
            
            if ( isEm2Department($thisDept) ) 
            {
                echo '<div class="statement">
                    <a target="_blank" href="em2Sop.php?appid=' . $appid . '">Statement&nbsp;of&nbsp;Purpose</a></div>';
            }
            ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php
    }
    ?>
   
    <?php
    //   Portfolio stuff
        if ( isDesignDepartment($thisDept) || $isDesignPhdDepartment || $isDesignDdesDepartment || isEm2Department($thisDept)) {
    ?>
    <div class="accordion section portfolio">
    <div id="portfolio" class="heading" tabindex="0"><span class="switch"></span>Portfolio</div>
    <div class="content">
        <div class="subsection portfolio">
            <?php
            if ($portLink != NULL || $portLink != "" ) {
                $httpinstring = stripos($portLink, "http");
                    if ($httpinstring === FALSE) {
                        $portLink = "http://" . $portLink;
                    }?>
                
                 <div class="element portfolioUrl">
                    <div class="label">Portfolio URL</div>
                    <div class="value">
                        <a href="<?php echo $portLink; ?>" target="_BLANK"><?php echo $portLink; ?></a>
                    </div>
                </div>
                <?php      
                }
                if ($portfolioPassword != NULL || $portfolioPassword != "" ) { 
                ?>
                 <div class="element portfolioPassword">
                    <div class="label">Portfolio Password</div>
                    <div class="value">
                        <?php echo $portfolioPassword; ?>
                    </div>
                </div>
                <?php      
                }
                
          
            ?>

        </div>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php
        }  // end if design test 
    ?>
    
    
    <div class="accordion section education">
    <div id="education" class="heading" tabindex="0"><span class="switch"></span>Education</div>
    <div class="content">
    <?php
    foreach ($myInstitutes as $myInstitute) {       
        $filePath = getFilePath(1, $myInstitute[0], $uid, $umasterid, $appid, $myInstitute[15]);
        $officialTranscriptReceived = $myInstitute[14];
    ?>
        <div class="subsection institution">
            <div class="subheading">
            <? 
            echo $myInstitute[1]; 
            if (isset($myInstitute[20]) && $myInstitute[20] != '')
            {
                echo ', ' . $myInstitute[20];    
            }
            ?>
            </div>
            
            <?php
            if ($filePath || $officialTranscriptReceived) {
            ?>
            <div class="element"> 
                <div class="label">Transcript</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '">Unofficial Transcript</a>'; 
                }
                if ($officialTranscriptReceived) {
                        echo $odrIndicator;
                } 
                ?>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="element"> 
                <div class="label">Institution</div>
                <?php
                switch($myInstitute[16]){
                    case "1":
                        $institution = "Undergraduate";
                        break;
                    case "2":
                        $institution = "Graduate";
                        break;
                    case "3":
                        $institution = "Additional";
                        break;
                }
                ?>
                <div class="value"><?php echo $institution; ?></div>
            </div>
            
            <div class="element"> 
                <div class="label">Degree</div>
                <div class="value"><?php echo $myInstitute[5]; ?></div>
            </div>

            <div class="element"> 
                <?php 
                if($myInstitute[16] == 3){
                    echo '<div class="label">Date Graduated/Left</div>';
                } else { 
                    echo '<div class="label">Date Graduated</div>';
                }
                ?>
                <div class="value"><?php echo $myInstitute[3]; ?></div>
            </div>
            <div class="element"> 
                <div class="label">GPA Overall</div>
                <div class="value"><?php echo $myInstitute[11] . ' / ' . $myInstitute[13]; ?></div>
            </div>            
             <div class="element"> 
                <div class="label">Major</div>
                <div class="value"><?php echo $myInstitute[6]; ?></div>
            </div>
            <div class="element"> 
                <div class="label">GPA Major</div>
                <div class="value"><?php echo $myInstitute[12] . ' / ' . $myInstitute[13]; ?></div>
            </div>
            <?php
            if ($myInstitute[21])
            {
            ?>
                <div class="element"> 
                    <div class="label">GPA Converted</div>
                    <div class="value"><?php echo $myInstitute[21]; ?></div>
                </div>
            <?php    
            }    
            ?>
        </div>
    <?php
    }
    
    if ((isset($iniKobeCoursework) && $iniKobeCoursework) || (isset($iniCoursework) &&$iniCoursework))
    {
    ?>
        <div class="subsection supportingCoursework">
            <div class="subheading">Supporting Coursework</div>
            <?php
            if ($iniKobeCoursework)
            {
                echo $iniKobeCoursework;   
            }
            
            if ($iniCoursework)
            {
                if ($iniCoursework['data_structures_title'] || $iniCoursework['data_structures_number'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Data Structures</div>
                        <div class="value">
                        <?php 
                        echo htmlspecialchars($iniCoursework['data_structures_title']) . ' ' 
                            . htmlspecialchars($iniCoursework['data_structures_number']); 
                        ?>
                        </div>
                    </div> 
                <?php
                }

                if ($iniCoursework['probability_title'] || $iniCoursework['probability_number'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Probability Theory</div>
                        <div class="value">
                        <?php 
                        echo htmlspecialchars($iniCoursework['probability_title']) . ' ' 
                            . htmlspecialchars($iniCoursework['probability_number']); 
                        ?>
                        </div>
                    </div> 
                <?php
                }
                
                if ($iniCoursework['statistics_title'] || $iniCoursework['statistics_number'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Statistics</div>
                        <div class="value">
                        <?php 
                        echo htmlspecialchars($iniCoursework['statistics_title']) . ' ' 
                            . htmlspecialchars($iniCoursework['statistics_number']); 
                        ?>
                        </div>
                    </div> 
                <?php
                }

                if ($iniCoursework['msit_experience'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Prob/Stats Experience</div>
                        <div class="value"><?php echo htmlspecialchars($iniCoursework['msit_experience']); ?></div>
                    </div>  
                <?php
                }
                
                if ($iniCoursework['programming_description'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">C/Systems-Level Programming</div>
                        <div class="value"><?php echo htmlspecialchars($iniCoursework['programming_description']); ?></div>
                    </div> 
                    <div class="element"> 
                        <div class="label">C++/Java Programming</div>
                        <div class="value"><?php echo htmlspecialchars($iniCoursework['programming_description2']); ?></div>
                    </div>  
                <?php
                }        
            }
            ?>
        </div>
    <?php
    }

    if (isset($em2Coursework) && $em2Coursework)
    {
    ?>
        <div class="subsection supportingCoursework">
            <div class="subheading">Supporting Coursework</div>
            <?php
            if ($em2Coursework)
            {
                if ($em2Coursework['data_structures_title'] || $em2Coursework['data_structures_number'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Data Structures</div>
                        <div class="value">
                        <?php 
                        echo htmlspecialchars($em2Coursework['data_structures_title']) . ' ' 
                            . htmlspecialchars($em2Coursework['data_structures_number']); 
                        ?>
                        </div>
                    </div> 
                <?php
                }

                if ($em2Coursework['probability_title'] || $em2Coursework['probability_number'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Probability Theory</div>
                        <div class="value">
                        <?php 
                        echo htmlspecialchars($em2Coursework['probability_title']) . ' ' 
                            . htmlspecialchars($em2Coursework['probability_number']); 
                        ?>
                        </div>
                    </div> 
                <?php
                }
                
                if ($em2Coursework['statistics_title'] || $em2Coursework['statistics_number'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Statistics</div>
                        <div class="value">
                        <?php 
                        echo htmlspecialchars($em2Coursework['statistics_title']) . ' ' 
                            . htmlspecialchars($em2Coursework['statistics_number']); 
                        ?>
                        </div>
                    </div> 
                <?php
                }

                if ($em2Coursework['msit_experience'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Prob/Stats Experience</div>
                        <div class="value"><?php echo htmlspecialchars($em2Coursework['msit_experience']); ?></div>
                    </div>  
                <?php
                }
                
                if ($em2Coursework['programming_description'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Programming Languages</div>
                        <div class="value"><?php echo htmlspecialchars($em2Coursework['programming_description']); ?></div>
                    </div> 
                    <div class="element"> 
                        <div class="label">Programming Projects</div>
                        <div class="value"><?php echo htmlspecialchars($em2Coursework['programming_description2']); ?></div>
                    </div>  
                <?php
                }  
                
                if ($em2Coursework['makerkits_description'])
                {
                ?>
                    <div class="element"> 
                        <div class="label">Arts-engineering Toolkits</div>
                        <div class="value"><?php echo htmlspecialchars($em2Coursework['makerkits_description']); ?></div>
                    </div>
                    <?php
                }
                
                if ($em2Coursework['makerkits_description2'])
                {  ?> 
                    <div class="element"> 
                        <div class="label">Special Technical Proficiencies</div>
                        <div class="value"><?php echo htmlspecialchars($em2Coursework['makerkits_description2']); ?></div>
                    </div>  
                <?php
                }      
            }
            ?>
        </div>
    <?php
    }
    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->

    <div class="accordion section testScores">
    <div id="testScores" class="heading" tabindex="0"><span class="switch"></span>Test Scores</div>
    <div class="content">
    <?php
    if ($waiveToefl)
    {
    ?>
        <div class="subsection testScore">
            <div class="label">TOEFL Waived</div>
        </div>
    <?php
    }
    
    foreach ($grescores as $grescore) {
    ?>
        <div class="subsection testScore greScore">
            <div class="subheading">GRE General</div>
            
            <?php
            $filePath = getFilePath(6, $grescore[0], $uid, $umasterid, $appid);
            $officialGrescoreReceieved = $grescore[10];
            if ($filePath || $officialGrescoreReceieved) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialGrescoreReceieved) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>
             
            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $grescore[1]; ?></div>
            </div>
            <?php
            if ($grescore[2] || $grescore[3]) {
            ?>
            <div class="element">
                <div class="label">Verbal</div>
                <div class="value"><?php echo $grescore[2] . ' / ' . $grescore[3] . '%'; ?></div>
            </div>
            <?php
            }
            if ($grescore[4] || $grescore[5]) {
            ?>
            <div class="element">
                <div class="label">Quantitative</div>
                <div class="value"><?php echo $grescore[4] . ' / ' . $grescore[5] . '%'; ?></div>
            </div>
            <?php
            }
            if ($grescore[6] || $grescore[7]) {
            ?>
            <div class="element">
                <div class="label">Analytical Writing</div>
                <div class="value"><?php echo $grescore[6] . ' / ' . $grescore[7] . '%'; ?></div>
            </div>
            <?php
            }
            if ($grescore[8] || $grescore[9]) {
            ?>
            <div class="element">
                <div class="label">Analytical</div>
                <div class="value"><?php echo $grescore[8] . ' / ' . $grescore[9] . '%'; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }

    foreach ($gresubjectscores as $gresubjectscore) {
    ?>
        <div class="subsection testScore greScore">
            <div class="subheading">GRE Subject</div>
            
            <?php
            $filePath = getFilePath(8, $gresubjectscore[0], $uid, $umasterid, $appid);
            $officialGresubjectscoreReceieved = $gresubjectscore[5];
            if ($filePath || $officialGresubjectscoreReceieved) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialGresubjectscoreReceieved) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>
             
            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $gresubjectscore[1]; ?></div>
            </div>
            <?php
            if ($gresubjectscore[2] ) {
            ?>
            <div class="element">
                <div class="label">Subject</div>
                <div class="value"><?php echo $gresubjectscore[2]; ?></div>
            </div>
            <?php
            }
            if ($gresubjectscore[3]) {
            ?>
            <div class="element">
                <div class="label">Score</div>
                <div class="value"><?php echo $gresubjectscore[3] . ' / ' . $gresubjectscore[4] . '%'; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }

    foreach ($toefliscores as $toefliscore) {
        if ($toefliscore[1] == "00/0000" || $toefliscore[1] == "") {
            continue;
        }
    ?>
        <div class="subsection testScore toeflScore">
            <div class="subheading">TOEFL IBT</div> 

            <?php
            $filePath = getFilePath(7, $toefliscore[0], $uid, $umasterid, $appid, $toefliscore[11]);
            $officialToefliscoreReceived = $toefliscore[7];
            if ($filePath || $officialToefliscoreReceived) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialToefliscoreReceived) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $toefliscore[1]; ?></div>
            </div>
            <?php
            if ($toefliscore[4]) {
            ?>
            <div class="element">
                <div class="label">Reading</div>
                <div class="value"><?php echo $toefliscore[4]; ?></div>
            </div>
            <?php
            }
            if ($toefliscore[3]) {
            ?>
            <div class="element">
                <div class="label">Listening</div>
                <div class="value"><?php echo $toefliscore[3]; ?></div>
            </div>
            <?php
            }
            if ($toefliscore[2]) {
            ?>
            <div class="element">
                <div class="label">Speaking</div>
                <div class="value"><?php echo $toefliscore[2]; ?></div>
            </div>
            <?php
            }
            if ($toefliscore[5]) {
            ?>
            <div class="element">
                <div class="label">Writing</div>
                <div class="value"><?php echo $toefliscore[5]; ?></div>
            </div>
            <?php
            }
            if ($toefliscore[6]) {
            ?>
            <div class="element">
                <div class="label">Total</div>
                <div class="value"><?php echo $toefliscore[6]; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }

    foreach ($toeflpscores as $toeflpscore) {
        if ($toeflpscore[1] == "00/0000" || $toeflpscore[1] == "") {
            continue;
        }
    ?>
        <div class="subsection testScore toeflScore">
            <div class="subheading">TOEFL PBT</div> 
            
            <?php
            $filePath = getFilePath(10, $toeflpscore[0], $uid, $umasterid, $appid, $toeflpscore[11]);
            $officialToeflpscoreReceived = $toeflpscore[7];
            if ($filePath || $officialToeflpscoreReceived) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialToeflpscoreReceived) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $toeflpscore[1]; ?></div>
            </div>
            <?php
            if ($toeflpscore[2]) {
            ?>
            <div class="element">
                <div class="label">Section 1</div>
                <div class="value"><?php echo $toeflpscore[2]; ?></div>
            </div>
            <?php
            }
            if ($toeflpscore[3]) {
            ?>
            <div class="element">
                <div class="label">Section 2</div>
                <div class="value"><?php echo $toeflpscore[3]; ?></div>
            </div>
            <?php
            }
            if ($toeflpscore[4]) {
            ?>
            <div class="element">
                <div class="label">Section 3</div>
                <div class="value"><?php echo $toeflpscore[4]; ?></div>
            </div>
            <?php
            }
            if ($toeflpscore[5]) {
            ?>
            <div class="element">
                <div class="label">TWE</div>
                <div class="value"><?php echo $toeflpscore[5]; ?></div>
            </div>
            <?php
            }
            if ($toeflpscore[6]) {
            ?>
            <div class="element">
                <div class="label">Total</div>
                <div class="value"><?php echo $toeflpscore[6]; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }

    foreach ($toeflcscores as $toeflCscore) {
        if ($toeflCscore[1] == "00/0000" || $toeflCscore[1] == "") {
            continue;
        }
    ?>
        <div class="subsection testScore toeflScore">
            <div class="subheading">
            TOEFL CBT
            <?php
            $filePath = getFilePath(9, $toeflCscore[0], $uid, $umasterid, $appid, $toeflCscore[11]);
            if ($filePath) {
                echo ' (<a target="_blank" href="' . $filePath . '" >Report</a>)';
            }
            ?>
            </div> 

            <?php
            $filePath = getFilePath(9, $toeflCscore[0], $uid, $umasterid, $appid, $toeflCscore[11]);
            $officialToeflCscoreReceived = $toeflCscore[7];
            if ($filePath || $officialToeflCscoreReceived) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialToeflCscoreReceived) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $toeflCscore[1]; ?></div>
            </div>
            <?php
            if ($toeflCscore[3]) {
            ?>
            <div class="element">
                <div class="label">Listening</div>
                <div class="value"><?php echo $toeflCscore[3]; ?></div>
            </div>
            <?php
            }
            if ($toeflCscore[2]) {
            ?>
            <div class="element">
                <div class="label">Structure / Writing</div>
                <div class="value"><?php echo $toeflCscore[2]; ?></div>
            </div>
            <?php
            }
            if ($toeflCscore[4]) {
            ?>
            <div class="element">
                <div class="label">Reading</div>
                <div class="value"><?php echo $toeflCscore[4]; ?></div>
            </div>
            <?php
            }
            if ($toeflCscore[5]) {
            ?>
            <div class="element">
                <div class="label">Essay</div>
                <div class="value"><?php echo $toeflCscore[5]; ?></div>
            </div>
            <?php
            }
            if ($toeflCscore[6]) {
            ?>
            <div class="element">
                <div class="label">Total</div>
                <div class="value"><?php echo $toeflCscore[6]; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }

    foreach ($ieltsScores as $ieltsScore) {
        if ($ieltsScore[1] == "00/0000") {
            continue;
        }
    ?>
        <div class="subsection testScore ieltsScore">
            <div class="subheading">IELTS</div> 

            <?php
            $filePath = getFilePath(19, $ieltsScore[0], $uid, $umasterid, $appid);
            $officialIeltsScoreReceived = $ieltsScore[7];
            if ($filePath || $officialIeltsScoreReceived) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialIeltsScoreReceived) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>
            
            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $ieltsScore[1]; ?></div>
            </div>
            <?php
            if ($ieltsScore[2]) {
            ?>
            <div class="element">
                <div class="label">Listening</div>
                <div class="value"><?php echo $ieltsScore[2]; ?></div>
            </div>
            <?php
            }
            if ($ieltsScore[3]) {
            ?>
            <div class="element">
                <div class="label">Reading</div>
                <div class="value"><?php echo $ieltsScore[3]; ?></div>
            </div>
            <?php
            }
            if ($ieltsScore[4]) {
            ?>
            <div class="element">
                <div class="label">Writing</div>
                <div class="value"><?php echo $ieltsScore[4]; ?></div>
            </div>
            <?php
            }
            if ($ieltsScore[5]) {
            ?>
            <div class="element">
                <div class="label">Speaking</div>
                <div class="value"><?php echo $ieltsScore[5]; ?></div>
            </div>
            <?php
            }
            if ($ieltsScore[6]) {
            ?>
            <div class="element">
                <div class="label">Total</div>
                <div class="value"><?php echo $ieltsScore[6]; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }

    foreach ($gmatScores as $gmatScore) {
        if ($gmatScore[1] == "00/0000" || $gmatScore[1] == "") {
            continue;
        }
    ?>
        <div class="subsection testScore gmatScore">
            <div class="subheading">GMAT</div> 

            <?php
            $filePath = getFilePath(20, $gmatScore[0], $uid, $umasterid, $appid);
            $officialGmatScoreReceived = $gmatScore[10];
            if ($filePath || $officialGmatScoreReceived) {
            ?>
            <div class="element testDate">
                <div class="label">Report</div>
                <div class="value">
                <?php
                if ($filePath) {
                    echo '<a target="_blank" href="' . $filePath . '" >Unofficial Report</a>';     
                } 
                if ($officialGmatScoreReceived) {
                    echo $odrIndicator;
                }
                ?>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="element testDate">
                <div class="label">Date</div>
                <div class="value"><?php echo $gmatScore[1]; ?></div>
            </div>
            <?php
            if ($gmatScore[2] || $gmatScore[3]) {
            ?>
            <div class="element">
                <div class="label">Verbal</div>
                <div class="value"><?php echo $gmatScore[2] . ' / ' . $gmatScore[3] . '%'; ?></div>
            </div>
            <?php
            }
            if ($gmatScore[4] || $gmatScore[5]) {
            ?>
            <div class="element">
                <div class="label">Quantitative</div>
                <div class="value"><?php echo $gmatScore[4] . ' / ' . $gmatScore[5] . '%'; ?></div>
            </div>
            <?php
            }
            if ($gmatScore[6] || $gmatScore[7]) {
            ?>
            <div class="element">
                <div class="label">Analytical Writing</div>
                <div class="value"><?php echo $gmatScore[6] . ' / ' . $gmatScore[7] . '%'; ?></div>
            </div>
            <?php
            }
            if ($gmatScore[8] || $gmatScore[9]) {
            ?>
            <div class="element">
                <div class="label">Total</div>
                <div class="value"><?php echo $gmatScore[8] . ' / ' . $gmatScore[9] . '%'; ?></div>
            </div>
            <?php
            }
            ?>
        </div>        
    <?php
    }
    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->

    <div class="accordion section recommendations">
    <div id="recommendations" class="heading" tabindex="0"><span class="switch"></span>Recommendations</div>
    <div class="content">
    <?php
    foreach ($myRecommenders as $myRecommender) {
        $recFilePath = getFilePath(3, $myRecommender[0], $uid, $umasterid, $appid, $myRecommender[9]);
        if (!$recFilePath) {
            
            $recommendationContent = $myRecommender[15];
            $recommendationContentLength = strlen($recommendationContent);
            if ($recommendationContentLength > 100) {
                $recFilePath = 'getContentRecommendation.php?recommendation_id=' . $myRecommender[0];
            }
            
        } else {

            /*
            * For MS Word files, give a link to a pdf created 
            * in the merge process, when available. 
            */
            $recFilePathRaw = getFilePathRaw(3, $myRecommender[0], $uid, $umasterid, $appid, $myRecommender[9]);
            $pdfRecFilePathRaw = $pdfRecFilePath = '';
            if ( strpos($recFilePathRaw, '.docx') !== FALSE ) {
                $pdfRecFilePathRaw = str_replace('.docx', '.pdf', $recFilePathRaw); 
                $pdfRecFilePath = str_replace('.docx', '.pdf', $recFilePath);       
            } elseif ( strpos($recFilePathRaw, '.doc') !== FALSE ) {
                $pdfRecFilePathRaw = str_replace('.doc', '.pdf', $recFilePathRaw);
                $pdfRecFilePath = str_replace('.doc', '.pdf', $recFilePath);    
            } elseif ( strpos($recFilePathRaw, '.rtf') !== FALSE ) {
                $pdfRecFilePathRaw = str_replace('.rtf', '.pdf', $recFilePathRaw);
                $pdfRecFilePath = str_replace('.rtf', '.pdf', $recFilePath);    
            }
            
            if ( $pdfRecFilePathRaw && file_exists($pdfRecFilePathRaw) 
                    && filemtime($pdfRecFilePathRaw) >= filemtime($recFilePathRaw) )
            {
                $recFilePath = $pdfRecFilePath;    
            }
        }
    ?>
        <div class="subsection recommender">
            <div class="subheading">
            <?
            if ($recFilePath) {        
                echo '<a target="_blank" href="' . $recFilePath . '">';
            }
            echo $myRecommender[2] . ' ' . $myRecommender[3]; 
            if ($recFilePath) {         
                echo '</a>';
            }
            ?>
            </div>
            
            <?php
            if ($myRecommender[4] || $myRecommender[5]) {
            ?>
            <div class="element"> 
                <div class="label">Title / Affiliation</div>
                <div class="value"><?php echo $myRecommender[4] . ', ' . $myRecommender[5]; ?></div>
            </div>    
            <?php
            }
            
            if ($myRecommender[17])
            {
            ?>
            <div class="element"> 
                <div class="label">Relationship</div>
                <div class="value"><?php echo $myRecommender[17]; ?></div>
            </div>    
            <?php
            }
            
            if ($myRecommender[6] || $myRecommender[7]) {
            ?>
            <div class="element"> 
                <div class="label">E-mail / Phone</div>
                <div class="value"><?php echo $myRecommender[6] . ' / ' . $myRecommender[7]; ?></div>
            </div>    
            <?php
            }
            
            if ($myRecommender[14] != 'Normal') {
            ?>
            <div class="element"> 
                <div class="label">Type</div>
                <div class="value">
                <?php 
                echo $myRecommender[14]; 
                if ( hasRecommendformAnswers($myRecommender[0]) ) {
                    echo ' (<a target="_blank" href="recformResponses.php?applicationId='; 
                    echo $appid . '&recommendId=' . $myRecommender[0];
                    echo '">form responses</a>)';
                }
                ?>
                </div>
            </div>    
            <?php
            }
            elseif (hasRecommendformAnswers($myRecommender[0]))
            {
            ?>
            <div class="element"> 
                <div class="label"></div>
                <div class="value">
                <?php 
                echo '<a target="_blank" href="recformResponses.php?applicationId='; 
                echo $appid . '&recommendId=' . $myRecommender[0];
                echo '">form responses</a>';
                ?>
                </div>
            </div>     
            <?php  
            } 
            ?>
        </div>
    <?php
    }
    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
    
    <?php
    if ( count($pubs) > 0 ) {
        $acceptedPubs = array();
        $otherPubs = array();
        foreach ($pubs as $pub) {
            if ($pub[6] && $pub[6] == 'Accepted') {
                $acceptedPubs[] = $pub;    
            } else {
                $otherPubs[] = $pub;     
            }    
        }
        $sortedPubs = array_merge($acceptedPubs, $otherPubs);
    ?>
    <div class="accordion section publications">
    <div id="publications" class="heading" tabindex="0"><span class="switch"></span>Publications</div>
    <div class="content">
    <?php
    foreach ($sortedPubs as $publication) {
        if ($thisDept == 1 && (!$publication[6] || $publication[6] != 'Accepted')) {
            $publicationTextStyle = 'style="color: #555555; font-style: italic;"';
            $publicationLinkStyle = 'style="color: #555555;"'; 
        } else {
            $publicationTextStyle = '';
            $publicationLinkStyle = '';    
        }
    ?>
        <div class="subsection publication" <?php echo $publicationTextStyle; ?>>
            <div class="subheading" style="height: 1px;"></div>
            <?php
            if ($publication[1]) {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Title</div>
                -->
                <div class="value">
                <?php 
                if($publication[9])
                {
                    $filePath = getFilePath(26, $publication[0], $uid, $umasterid, $appid, $publication[9]);
                    if ($filePath !== null) 
                    {
                        echo '<a href="' . $filePath . '" target="_blank" ' . $publicationLinkStyle . '>' . $publication[1] . '</a>';
                    }
                }
                elseif($publication[5] != "") 
                {
                    $header = "";
                    if(strstr($publication[5], "http") === false) {
                        $header = "http://";
                    }
                    $href = $header . $publication[5];
                    echo '<a href="' . $href . '" target="_blank" ' . $publicationLinkStyle . '>' . $publication[1] . '</a>';
                } 
                else 
                {
                    echo $publication[1];    
                }
                 
                ?>
                </div>
            </div>    
            <?php
            }
            
            if ($publication[2]) {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Author(s)</div>
                -->
                <div class="value"><?php echo $publication[2]; ?></div>
            </div>    
            <?php
            }
            
            if ($publication[3]) {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Journal / Conference</div>
                -->
                <div class="value"><?php echo $publication[3]; ?></div>
            </div>    
            <?php
            } 
            
            if ($publication[4]) {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Citation</div>
                -->
                <div class="value"><?php echo $publication[4]; ?></div>
            </div>    
            <?php
            } 

            if ($publication[6]) {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Status</div>
                -->
                <div class="value"><?php echo $publication[6]; ?></div>
            </div>    
            <?php
            } 

            if ($publication[7] && $publication[7] != 'Other') {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Type</div>
                -->
                <div class="value"><?php echo $publication[7]; ?></div>
            </div>    
            <?php
            } elseif ($publication[8] && (!$publication[7] || $publication[7] == 'Other')) {
            ?>
            <div class="element"> 
                <!--
                <div class="label">Type</div>
                -->
                <div class="value">Other: <?php echo $publication[8]; ?></div>
            </div>  
            <?php
            }
            ?>
            
        </div>
    <?php
    }
    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php
    }

    
    if ( $yearsExperience || $iniYearsExperience || count($employments) > 0 ) {
    ?>
    <div class="accordion section experience">
    <div id="experience" class="heading" tabindex="0"><span class="switch"></span>Experience / Employment</div>
    <div class="content">
    <?php
    if ($yearsExperience) {
    ?>
        <div class="subsection yearsExperience">
        <div class="element"> 
            <div class="label">Years of professional experience</div>
            <div class="value"><?php echo $yearsExperience; ?></div>
        </div>
        </div> 
    <?php    
    }
    /*
    if ($iniYearsExperience) {
    ?>
        <div class="subsection yearsExperience">
        <div class="element"> 
            <div class="label">Years professional</div>
            <div class="value"><?php echo $iniYearsExperience['full_time_professional']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Years relevant industry</div>
            <div class="value"><?php echo $iniYearsExperience['relevant_industry']; ?></div>
        </div>
        </div> 
    <?php    
    } */

    foreach ($employments as $employment) {
    ?>
        <div class="subsection employment">
            <div class="subheading" style="height: 1px;"></div>
            <?php
            if ($employment[1]) {
            ?>
            <div class="element">
                <div class="label">Company</div>
                <div class="value"><?php echo $employment[1]; ?></div>
            </div>    
            <?php
            }
            
            if ($employment[5]) {
            ?>
            <div class="element">
                <div class="label">Address</div>
                <div class="value"><?php echo $employment[5]; ?></div>
            </div>    
            <?php
            }
            
            if ($employment[6]) {
            ?>
            <div class="element">
                <div class="label">Title</div>
                <div class="value"><?php echo $employment[6]; ?></div>
            </div>    
            <?php
            }
            
            if ($employment[7]) {
            ?>
            <div class="element">
                <div class="label">Description</div>
                <div class="value"><?php echo $employment[7]; ?></div>
            </div>    
            <?php
            }
            
            if ($employment[2]) {
            ?>
            <div class="element"> 
                <div class="label">Start Date</div>
                <div class="value"><?php echo $employment[2]; ?></div>
            </div>    
            <?php
            } 

            if ($employment[3]) {
            ?>
            <div class="element"> 
                <div class="label">End Date</div>
                <div class="value"><?php echo $employment[3]; ?></div>
            </div>    
            <?php
            } 

            if ($employment[4]) {
            ?>
            <div class="element"> 
                <div class="label">Years Experience</div>
                <div class="value"><?php echo $employment[4]; ?></div>
            </div>    
            <?php
            } 
            ?>
        </div>
    <?php
    }

    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php
    }

    if (isset ($codility) && count($codility) > 0)
    {
    ?>
    <div class="accordion section experience">
    <div id="experience" class="heading" tabindex="0"><span class="switch"></span>Codility Scores</div>
    <div class="content">
        <div class="subsection codility">
        <div class="element"> 
            <div class="label">Total Score</div>
            <div class="value"><?php echo $codility['total_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Max Score</div>
            <div class="value"><?php echo $codility['max_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">&percnt; Total Score</div>
            <div class="value"><?php echo $codility['percent_total_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">&percnt; Correctness</div>
            <div class="value"><?php echo $codility['percent_correctness']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">&percnt; Performance</div>
            <div class="value"><?php echo $codility['percent_performance']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Similarity Check</div>
            <div class="value"><font color="red"> <?php echo $codility['similarity_check']; ?></font></div>
        </div>
        <div class="element"> 
            <div class="label">---</div>
            <div class="value"></div>
        </div>
        <div class="element"> 
            <div class="label">Task 1</div>
            <div class="value"><?php echo $codility['task1_name']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Score</div>
            <div class="value"><?php echo $codility['task1_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Correctness</div>
            <div class="value"><?php echo $codility['task1_correctness']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Performance</div>
            <div class="value"><?php echo $codility['task1_performance']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">---</div>
            <div class="value"></div>
        </div>
        <div class="element"> 
            <div class="label">Task 2</div>
            <div class="value"><?php echo $codility['task2_name']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Score</div>
            <div class="value"><?php echo $codility['task2_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Correctness</div>
            <div class="value"><?php echo $codility['task2_correctness']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Performance</div>
            <div class="value"><?php echo $codility['task2_performance']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">---</div>
            <div class="value"></div>
        </div>
        <div class="element"> 
            <div class="label">Task 3</div>
            <div class="value"><?php echo $codility['task3_name']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Score</div>
            <div class="value"><?php echo $codility['task3_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Correctness</div>
            <div class="value"><?php echo $codility['task3_correctness']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Performance</div>
            <div class="value"><?php echo $codility['task3_performance']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">---</div>
            <div class="value"></div>
        </div>
        <div class="element"> 
            <div class="label">Task 4</div>
            <div class="value"><?php echo $codility['task4_name']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Score</div>
            <div class="value"><?php echo $codility['task4_score']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Correctness</div>
            <div class="value"><?php echo $codility['task4_correctness']; ?></div>
        </div>
        <div class="element"> 
            <div class="label">Performance</div>
            <div class="value"><?php echo $codility['task4_performance']; ?></div>
        </div>
        </div>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php    
    }
    
    if ( count($fellows) > 0 ) {
    ?>
    <div class="accordion section fellowships">
    <div id="fellowships" class="heading" tabindex="0"><span class="switch"></span>Fellowships</div>
    <div class="content">
    <?php
    foreach ($fellows as $fellowship) {
    ?>
        <div class="subsection fellowship">
            <div class="subheading" style="height: 1px;"></div>
            <?php
            if ($fellowship[1]) {
            ?>
            <div class="element">
                <div class="label">Name</div>
                <div class="value"><?php echo $fellowship[1]; ?></div>
            </div>    
            <?php
            }
            
            if ($fellowship[2]) {
            ?>
            <div class="element"> 
                <div class="label">Amount / Year</div>
                <div class="value">$<?php echo $fellowship[2]; ?></div>
            </div>    
            <?php
            } 

            if ($fellowship[6]) {
            ?>
            <div class="element"> 
                <div class="label">Duration (years)</div>
                <div class="value"><?php echo $fellowship[6]; ?></div>
            </div>    
            <?php
            } 

            if ($fellowship[4]) {
            ?>
            <div class="element"> 
                <div class="label">Applied Date</div>
                <div class="value"><?php echo $fellowship[4]; ?></div>
            </div>    
            <?php
            } 

            if ($fellowship[5]) {
            ?>
            <div class="element"> 
                <div class="label">Award Date</div>
                <div class="value"><?php echo $fellowship[5]; ?></div>
            </div>    
            <?php
            } 

            if ($fellowship[3]) {
            ?>
            <div class="element"> 
                <div class="label">Status</div>
                <div class="value">
                <?php 
                    echo $fellowship[3]; 
                    $awardletterFilePath = getFilePath(22, $fellows[$i][0], $uid, $umasterid, $appid, $fellows[$i][7]);
                    if ($awardletterFilePath !== null)
                    {
                    ?>
                        <a target="_blank" href="<?=$awardletterFilePath?>" >(Letter)</a>
                    <?php    
                    }
                ?>
                </div>
            </div>    
            <?php
            } 
            ?>
        </div>
    <?php
    }
    ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php
    }
    ?>
    
    <?php
    /*
    * Dietrich language proficiency 
    */
    $writingSampleCount = count($writingSamples);
    $languageAssessmentCount = count($languageAssessments);
    if ($writingSampleCount > 0
        || $languageAssessmentCount > 0
        || $speakingSample)
    {
    ?>
    <div class="accordion section proficiency">
    <div id="proficiency" class="heading" tabindex="0"><span class="switch"></span>Writing Samples and Language Proficiency</div>
    <div class="content">
        <?php
        if ($writingSampleCount > 0 || $speakingSample)
        {
        ?>
            <div class="subsection languageSamples">
        <?php
            $i = 1;
            foreach ($writingSamples as $writingSample)
            {
            ?>
                <a target="_blank"  href="<?php echo $writingSample; ?>">Writing Sample<?php echo ($writingSampleCount > 1 ? ' ' . $i : ''); ?></a>&nbsp;&nbsp;
            <?php
                $i++;
            }
            
            if ($speakingSample)
            {
            ?>
                <a target="_blank"  href="<?php echo $speakingSample; ?>">Speaking Sample</a>    
            <?php
            }
        ?>
            </div>
        <?php
        }
        if ($languageAssessmentCount > 0)
        {
            foreach ($languageAssessments as $languageAssessment)
            {
            ?>
                <div class="subsection languageAssessment">
                    <div class="subheading"><?php echo htmlspecialchars($languageAssessment['language']); ?></div>
                    <div class="element">
                        <div class="label">Speaking</div>
                        <div class="value"><?php echo htmlspecialchars($languageAssessment['speaking_rating']); ?></div>
                    </div>
                    <div class="element">
                        <div class="label">Reading</div>
                        <div class="value"><?php echo htmlspecialchars($languageAssessment['reading_rating']); ?></div>
                    </div> 
                    <div class="element">
                        <div class="label">Writing</div>
                        <div class="value"><?php echo htmlspecialchars($languageAssessment['writing_rating']); ?></div>
                    </div>    
                <?php
                if (!isHistoryApplication($appid))
                {
                    if ($languageAssessment['listening_rating'])
                    {
                    ?>
                        <div class="element">
                            <div class="label">Listening</div>
                            <div class="value"><?php echo htmlspecialchars($languageAssessment['listening_rating']); ?></div>
                        </div> 
                    <?php  
                    }
                    
                    if (isset($languageAssessment['native_speaker']))
                    {
                    ?>
                        <div class="element">
                            <div class="label">Native Speaker</div>
                            <div class="value"><?php echo ($languageAssessment['native_speaker'] ? 'Yes' : 'No'); ?></div>
                        </div> 
                    <?php  
                    }
                    
                    if (isset($languageAssessment['years_study']))
                    {
                    ?>
                        <div class="element">
                            <div class="label">Years</div>
                            <div class="value"><?php echo htmlspecialchars($languageAssessment['years_study']); ?></div>
                        </div> 
                    <?php  
                    }
                    
                    if (isset($languageAssessment['study_level']))
                    {
                    ?>
                        <div class="element">
                            <div class="label">Level</div>
                            <div class="value"><?php echo htmlspecialchars($languageAssessment['study_level']); ?></div>
                        </div> 
                    <?php  
                    }
                    
                    if (isset($languageAssessment['competency_evidence']))
                    {
                    ?>
                        <div class="element">
                            <div class="label">Competency Evidence</div>
                            <div class="value"><?php echo htmlspecialchars($languageAssessment['competency_evidence']); ?></div>
                        </div> 
                    <?php  
                    }
                }
                ?>
                </div>
            <?php
            }    
        }
        
    if  ($dietrichLanguageRecommendation) 
    {
        $langRecFilePath = getFilePath(25, $dietrichLanguageRecommendation['id'], $uid, $umasterid, $appid, 
            $dietrichLanguageRecommendation['datafile_id']);
        
        /*
        * For MS Word files, give a link to a pdf created 
        * in the merge process, when available. 
        */
        $langRecFilePathRaw = getFilePathRaw(25, $dietrichLanguageRecommendation['id'], $uid, $umasterid, $appid, 
                $dietrichLanguageRecommendation['datafile_id']);
        $pdfLangRecFilePathRaw = $pdfLangRecFilePath = '';
        if ( strpos($langRecFilePathRaw, '.docx') !== FALSE ) {
            $pdfLangRecFilePathRaw = str_replace('.docx', '.pdf', $langRecFilePathRaw); 
            $pdfLangRecFilePath = str_replace('.docx', '.pdf', $langRecFilePathRaw);       
        } elseif ( strpos($langRecFilePathRaw, '.doc') !== FALSE ) {
            $pdfLangRecFilePathRaw = str_replace('.doc', '.pdf', $langRecFilePathRaw);
            $pdfLangRecFilePath = str_replace('.doc', '.pdf', $langRecFilePathRaw);    
        } elseif ( strpos($langRecFilePathRaw, '.rtf') !== FALSE ) {
            $pdfLangRecFilePathRaw = str_replace('.rtf', '.pdf', $langRecFilePathRaw);
            $pdfLangRecFilePath = str_replace('.rtf', '.pdf', $langRecFilePathRaw);    
        }
        
        if ( $pdfLangRecFilePathRaw && file_exists($pdfLangRecFilePathRaw) 
                && filemtime($pdfLangRecFilePathRaw) >= filemtime($langRecFilePathRaw) )
        {
            $langRecFilePath = $pdfLangRecFilePath;    
        }

    ?>
        <div class="subsection langrecommender">
            <div class="subheading"></div>
            <div class="element"> 
                <div class="label">Recommender</div>
                <div class="value">
                <?
                if ($langRecFilePath) {        
                    echo '<a target="_blank" href="' . $langRecFilePath . '">';
                }
                echo $dietrichLanguageRecommendation['firstname'] . ' ' . $dietrichLanguageRecommendation['lastname']; 
                if ($langRecFilePath) {         
                    echo '</a>';
                }
                ?>
                </div>
            </div>
            
            <?php
            if ($dietrichLanguageRecommendation['title'] || $dietrichLanguageRecommendation['company']) {
            ?>
            <div class="element"> 
                <div class="label">Title / Affiliation</div>
                <div class="value"><?php echo $dietrichLanguageRecommendation['title'] . ', ' . $dietrichLanguageRecommendation['company']; ?></div>
            </div>    
            <?php
            }
            
            if ($dietrichLanguageRecommendation['email'] || $dietrichLanguageRecommendation['address_perm_tel']) {
            ?>
            <div class="element"> 
                <div class="label">E-mail / Phone</div>
                <div class="value"><?php echo $dietrichLanguageRecommendation['email'] . ' / ' . $dietrichLanguageRecommendation['address_perm_tel']; ?></div>
            </div>    
            <?php
            }
            
            if ($dietrichLanguageRecommendation['language_specialization']) {
            ?>
            <div class="element"> 
                <div class="label">Language</div>
                <div class="value">
                <?php 
                echo $dietrichLanguageRecommendation['language_specialization']; 
                ?>
                </div>
            </div>    
            <?php
            } 
            ?>
        </div>
    <?php
    }
        
        
        ?>
    </div> <!-- end content -->
    </div> <!-- end section -->
    <?php
    }
    ?>

    <div class="accordion section supplementals">
    <div id="supplementals" class="heading" tabindex="0"><span class="switch"></span>Supplemental Info</div>
    <div class="content">
        <div class="subsection supplemental">
            <?php
            if ( isScsDepartment($thisDept) ) {
            ?>
            <div class="element womenIT">
                <div class="label">Women@IT Fellowship</div>
                <div class="value"><?php echo $wFellow; ?></div>
            </div>
            <div class="element PIER">
                <div class="label">PIER</div>
                <div class="value"><?php echo $pier; ?></div>
            </div>
            <?php
            }
            
            if ( isMshciiDepartment($thisDept)
                || $thisDept == 5 )  {
            ?>
            <div class="element PIER">
                <div class="label">Sending Portfolio</div>
                <div class="value"><?php echo $port; ?></div>
            </div>
            <?php    
            if ($portLink != NULL || $portLink != "" ) { 
            ?>
             <div class="element portfolioUrl">
                <div class="label">Portfolio URL</div>
                <div class="value">
                    <a href="http://<?php echo $portLink; ?>" target="_BLANK"><?php echo $portLink; ?></a>
                </div>
            </div>
            <?php      
            }
            } // end if hcii test
           
            if($videoEssayLink != ""){
                if (strpos($videoEssayLink, '://') === FALSE) {
                    $videoEssayLink = 'http://' . $videoEssayLink;
                }
            ?>
             <div class="element honors">
                <div class="label">Video Essay URL</div>
                <div class="value">
                <a href="<?=htmlspecialchars($videoEssayLink)?>" target="_BLANK"><?=htmlspecialchars($videoEssayLink)?></a>
                </div>
                <div class="label">Video Essay Access Code</div>
                <div class="value"><?php echo htmlspecialchars($videoEssayCode); ?></div>
            </div>
            <?php  
            }
            
            if($cur_enrolled == "yes"){ 
            ?>
             <div class="element currentlyEnrolled">
                <div class="label">Currently Enrolled</div>
                <div class="value"><?php echo $cur_enrolled; ?></div>
            </div>
            <?php  
            }
            
            if($honors != ""){ 
            ?>
             <div class="element honors">
                <div class="label">Honors</div>
                <div class="value"><?php echo stripslashes($honors); ?></div>
            </div>
            <?php  
            }

            if ($otherUni != "") {
            ?>
            <div class="element otherInstitutes">
                <div class="label">Other Institutes</div>
                <div class="value"><?php echo $otherUni; ?></div>
            </div>
            <?php
            }
            
            if ( !empty($crossDeptProgs) ) {
            ?>
            <div class="element otherPrograms">
                <div class="label">Other Programs</div>
                <div class="value">
                <?php 
                if (!is_array($crossDeptProgs)) {
                    $crossDeptProgs = explode(",",$crossDeptProgs);
                }
                $tmpDisplayItems = array();
                for ($i = 0; $i < count($crossDeptProgs); $i++) {
                    $showItem = true;
                    if ($crossDeptProgs[$i] != "Other") {
                        for ($k = 0; $k < count($tmpDisplayItems); $k++) {
                            if ($tmpDisplayItems[$k] == $crossDeptProgs[$i]) {
                                $showItem = false;
                                break;
                            }
                        }
                        if($showItem == true) {
                            array_push($tmpDisplayItems, $crossDeptProgs[$i]);
                            //echo $crossDeptProgs[$i]. "<br/>";
                        }            
                    }
                }
                echo implode(';', $tmpDisplayItems);
                
                if ($crossDeptProgsOther)
                {
                    echo '; ' . $crossDeptProgsOther;
                }
                ?>
                </div>
            </div>
            <?php
            }
            
            if( marray_search( "CNBC Graduate", $myPrograms) !== false) { 
            ?>
            <div class="element authorizeCollectInfo">
                <div class="label">Authorize to collect info</div>
                <div class="value">
                <?php 
                if ($permission == 1) { 
                    echo "Yes";
                } else {
                    echo "No";
                }
                ?>
                </div>
            </div>
            <?php   
            }
            
            if ($attendanceStatus)
            {
            ?>
            <div class="element attendanceStatus">
                <div class="label">Attendance Status</div>
                <div class="value"><?php echo htmlspecialchars($attendanceStatus); ?></div>
            </div>
            <?php
            }
            
            if ($cmuAffiliation)
            {
            ?>
            <div class="element cmuStudent">
                <div class="label">CMU Student/Alum</div>
                <div class="value"><?php echo ($cmuAffiliation['student_or_alumnus'] ? 'Yes' : 'No'); ?></div>
            </div>
            <div class="element cmuEmployee">
                <div class="label">CMU Employee</div>
                <div class="value"><?php echo ($cmuAffiliation['employee'] ? 'Yes' : 'No'); ?></div>
            </div>
            <?php
            }
 
            if ($dietrichRecognitions)
            {
            ?>
            <div class="element recognitions">
                <div class="label">Recognitions</div>
                <div class="value"><?php echo htmlspecialchars($dietrichRecognitions); ?></div>
            </div>
            <?php
            }
                        
            if (isset($iniDisciplinaryAction) && $iniDisciplinaryAction)
            {
            ?>
            <div class="element iniDisciplinaryAction">
                <div class="label">Disciplinary Action</div>
            </div>
            <?php
            }
            
            if (isset($em2DisciplinaryAction) && $em2DisciplinaryAction)
            {
            ?>
            <div class="element iniDisciplinaryAction">
                <div class="label">Disciplinary Action</div>
            </div>
            <?php
            }
            
            if (isset($iniDisciplinaryAction) && $iniDisciplinaryAction['sanction'])
            {
            ?>
            <div class="element iniDisciplinarySanctions">
                <div class="label"></div>
                <div class="value">--Sanctions Imposed
                <?php
                if ($iniDisciplinaryAction['sanction_description'])
                {
                    echo ': ' . htmlspecialchars($iniDisciplinaryAction['sanction_description']);
                } 
                ?>
                </div>
            </div>
            <?php
            }
            
            if (isset($em2DisciplinaryAction) && $em2DisciplinaryAction['sanction'])
            {
            ?>
            <div class="element iniDisciplinarySanctions">
                <div class="label"></div>
                <div class="value">--Sanctions Imposed
                <?php
                if ($em2DisciplinaryAction['sanction_description'])
                {
                    echo ': ' . htmlspecialchars($em2DisciplinaryAction['sanction_description']);
                } 
                ?>
                </div>
            </div>
            <?php
            }
            
            if (isset($iniDisciplinaryAction) && $iniDisciplinaryAction['retraction'])
            {
            ?>
            <div class="element iniDisciplinaryRetraction">
                <div class="label"></div>
                <div class="value">--Retractions Requested
                <?php
                if ($iniDisciplinaryAction['retraction_description'])
                {
                    echo ': ' . htmlspecialchars($iniDisciplinaryAction['retraction_description']);
                } 
                ?>
                </div>
            </div>
            <?php
            }
            
            if (isset($em2DisciplinaryAction) && $em2DisciplinaryAction['retraction'])
            {
            ?>
            <div class="element iniDisciplinaryRetraction">
                <div class="label"></div>
                <div class="value">--Retractions Requested
                <?php
                if ($em2DisciplinaryAction['retraction_description'])
                {
                    echo ': ' . htmlspecialchars($em2DisciplinaryAction['retraction_description']);
                } 
                ?>
                </div>
            </div>
            <?php
            }
            
            if ((isset($dietrichFinancialSupport) && $dietrichFinancialSupport) || (isset($iniFinancialSupport) && $iniFinancialSupport)
             || (isset($em2FinancialSupport) && $em2FinancialSupport))
            {
            ?>
            <div class="element financialSupport">
                <div class="label">Financial Support</div>
            </div>
            <?php
            }
            
            if (isset($iniFinancialSupport) && $iniFinancialSupport['request_consideration'])
            {
            ?>
            <div class="element iniFinancialRequest">
                <div class="label"></div>
                <div class="value">--Wishes to be considered for financial aid from Carnegie Mellon</div>
            </div>
            <?php
            }
            
            if (isset($em2FinancialSupport) && $em2FinancialSupport['request_consideration'])
            {
            ?>
            <div class="element iniFinancialRequest">
                <div class="label"></div>
                <div class="value">--Wishes to be considered for a fellowship from Carnegie Mellon</div>
            </div>
            <?php
            }
            
            if (isset($em2FinancialSupport) && $em2FinancialSupport['request_assistantship'])
            {
            ?>
            <div class="element iniFinancialRequest">
                <div class="label"></div>
                <div class="value">--Wishes to be considered for a TAship from Carnegie Mellon</div>
            </div>
            <?php
            }
            
            if (isset($iniFinancialSupport) && $iniFinancialSupport['attend_without_support'])
            {
            ?>
            <div class="element iniFinancialAttend">
                <div class="label"></div>
                <div class="value">--Would attend Carnegie Mellon without financial aid from the university</div>
            </div>
            <?php
            }
            
            if (isset($em2FinancialSupport) && $em2FinancialSupport['attend_without_support'])
            {
            ?>
            <div class="element iniFinancialAttend">
                <div class="label"></div>
                <div class="value">--Would attend Carnegie Mellon without financial aid from the university</div>
            </div>
            <?php
            }
            
            if (isset($iniFinancialSupport) && ($iniFinancialSupport['receive_outside_support_type'] || $iniFinancialSupport['receive_outside_support_source']))     
            {
            ?>
                <div class="element iniFinancialReceive">
                    <div class="label"></div>
                    <div class="value">--Receiving outside support: 
                    <?php 
                    echo htmlspecialchars($iniFinancialSupport['receive_outside_support_type']) . ': '; 
                    echo htmlspecialchars($iniFinancialSupport['receive_outside_support_source']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($em2FinancialSupport) && ($em2FinancialSupport['receive_outside_support_type'] || $em2FinancialSupport['receive_outside_support_source']))     
            {
            ?>
                <div class="element iniFinancialReceive">
                    <div class="label"></div>
                    <div class="value">--Receiving outside support: 
                    <?php 
                    echo htmlspecialchars($em2FinancialSupport['receive_outside_support_type']) . ': '; 
                    echo htmlspecialchars($em2FinancialSupport['receive_outside_support_source']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($iniFinancialSupport) && ($iniFinancialSupport['apply_outside_support_type'] || $iniFinancialSupport['apply_outside_support_source']))     
            {
            ?>
                <div class="element iniFinancialApply">
                    <div class="label"></div>
                    <div class="value">--Applying for outside support: 
                    <?php 
                    echo htmlspecialchars($iniFinancialSupport['apply_outside_support_type']) . ': '; 
                    echo htmlspecialchars($iniFinancialSupport['apply_outside_support_source']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($em2FinancialSupport) && ($em2FinancialSupport['apply_outside_support_type'] || $em2FinancialSupport['apply_outside_support_source']))     
            {
            ?>
                <div class="element iniFinancialApply">
                    <div class="label"></div>
                    <div class="value">--Applying for outside support: 
                    <?php 
                    echo htmlspecialchars($em2FinancialSupport['apply_outside_support_type']) . ': '; 
                    echo htmlspecialchars($em2FinancialSupport['apply_outside_support_source']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($iniFinancialSupport) && ($iniFinancialSupport['family_support_type'] || $iniFinancialSupport['family_support_amount']))     
            {
            ?>
                <div class="element iniFinancialApply">
                    <div class="label"></div>
                    <div class="value">--Family/Friends Support: 
                    <?php 
                    echo htmlspecialchars($iniFinancialSupport['family_support_type']) . ': '; 
                    echo htmlspecialchars($iniFinancialSupport['family_support_amount']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($em2FinancialSupport) && ($em2FinancialSupport['family_support_type'] || $em2FinancialSupport['family_support_amount']))    
            {
            ?>
                <div class="element iniFinancialApply">
                    <div class="label"></div>
                    <div class="value">--Family/Friends Support: 
                    <?php 
                    echo htmlspecialchars($em2FinancialSupport['family_support_type']) . ': '; 
                    echo htmlspecialchars($em2FinancialSupport['family_support_amount']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($iniFinancialSupport) && $iniFinancialSupport['other_support_source'])     
            {
            ?>
                <div class="element iniFinancialOther">
                    <div class="label"></div>
                    <div class="value">--Other support source: 
                    <?php 
                    echo htmlspecialchars($iniFinancialSupport['other_support_source']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if (isset($em2FinancialSupport) && $em2FinancialSupport['other_support_source'])     
            {
            ?>
                <div class="element iniFinancialOther">
                    <div class="label"></div>
                    <div class="value">--Other support source: 
                    <?php 
                    echo htmlspecialchars($em2FinancialSupport['other_support_source']);
                    ?>
                    </div>
                </div>
            <?php  
            }
            
            if ($dietrichFinancialSupport['qualified_assistance'])
            {
            ?>
            <div class="element dietrichFinancialQualified">
                <div class="label"></div>
                <div class="value">--Qualified for federal disadvantaged assistance</div>
            </div>
            <?php
            }
            
            if ($dietrichFinancialSupport['received_loans'])
            {
            ?>
            <div class="element dietrichFinancialLoans">
                <div class="label"></div>
                <div class="value">--Received Health Professional Student Loans (HPSL), Loans for Disadvantaged Student Program</div>
            </div>
            <?php
            }
            
            if ($dietrichFinancialSupport['received_scholarships'])
            {
            ?>
            <div class="element dietrichFinancialScholarships">
                <div class="label"></div>
                <div class="value">--Received scholarships from the U.S. Department of Health and Human Services under the Scholarship for Individuals with Exceptional Financial Need</div>
            </div>
            <?php
            }
            
            if ($dietrichFinancialSupport['support_sources'])
            {
            ?>
            <div class="element dietrichFinancialSources">
                <div class="label"></div>
                <div class="value">--Sources: <?php echo htmlspecialchars($dietrichFinancialSupport['support_sources']); ?></div>
            </div>
            <?php
            }
            
            if ($dietrichFinancialSupport['interested_b2_training'])
            {
            ?>
            <div class="element dietrichFinancialScholarships">
                <div class="label"></div>
                <div class="value">--Interested in Behavioral Brain Research Training Program</div>
            </div>
            <?php
            }
            
            if ($dietrichSharing && ($dietrichSharing['sds'] || $dietrichSharing['tepper']))
            {
            ?>
            <div class="element dietrichSharing">
                <div class="label">Application Sharing</div>
            </div>
            <?php
                if ($dietrichSharing['sds'])
                {
                ?>
                    <div class="element dietrichSharingSds">
                        <div class="label"></div>
                        <div class="value">Share application with SDS</div>
                    </div>
                <?php
                }
                if ($dietrichSharing['tepper'])
                {
                ?>
                    <div class="element dietrichSharingTepper">
                        <div class="label"></div>
                        <div class="value">Share application with Tepper</div>
                    </div>
                <?php
                }
            }
            
            if ($referral) {
            ?>
            <div class="element referral">
                <div class="label">Referred to program by</div>
                <div class="value"><?php echo $referral; ?></div>
            </div>
            <?php
            }
            
            if ($prog) {
            ?>
            <div class="element referral">
                <div class="label">
                <br>
                <?php
                if ($thisDept == 58) {
                    echo 'Required Courses';
                } else {
                    echo 'Additional Knowlege, Programming';
                }    
                ?>
                </div>
                <div class="value">
                <br>
                <?php echo $prog; ?>
                </div>
            </div>
            <?php
            }

            if ($design) {
            ?>
            <div class="element referral">
                <div class="label">
                <br>
                Additional Knowlege, Design
                </div>
                <div class="value">
                <br>
                <?php echo $design; ?>
                </div>
            </div>
            <?php
            }
            
            if ($stats) {
            ?>
            <div class="element referral">
                <div class="label">
                <br>
                <?php
                if ($thisDept == 58) {
                    echo 'Experience';
                } else {
                    echo 'Additional Knowlege, Statistics';
                }    
                ?>
                </div>
                <div class="value">
                <br>
                <?php echo $stats; ?>
                </div>
            </div>
            <?php
            }
            ?>

        </div>
    </div> <!-- end content -->
    </div> <!-- end section -->
     
    <div class="accordion section demographics">
    <div id="demographics" class="heading" tabindex="0"><span class="switch"></span>Biographical Info</div>
    <div class="content">
        <div class="subsection demographic">
            <div class="element dob">
                <div class="label">DOB</div>
                <div class="value"><?php echo formatUSDate($dob); ?></div>
            </div>
            <div class="element gender">
                <div class="label">Gender</div>
                <div class="value"><?php echo $gender; ?></div>
            </div>
            <div class="element nativeTongue">
                <div class="label">Native Tongue</div>
                <div class="value"><?php echo $nativeTongue; ?></div>
            </div>
            <div class="element visaStatus">
                <div class="label">Visa Status</div>
                <div class="value"><?php echo $visaNameShort; ?></div>
            </div>
        </div>
        
        <?php
        if ($dietrichDiversity)
        {
        ?>
        <div class="subsection demographic">
        <div class="element dietrichDiversity">
            <div class="label">Diversity Questions</div>
        </div>
        
        <?php        
        if ($dietrichDiversity['background'])
        {
        ?>
        <div class="element dietrichDiversityBackground">
            <div class="label"></div>
            <div class="value">Background: 
            <?php
            echo htmlspecialchars($dietrichDiversity['background']);
            ?>
            </div>
        </div>
        <?php
        }
        
        if ($dietrichDiversity['life_experience'])
        {
        ?>
        <div class="element dietrichDiversityLifeExperience">
            <div class="label"></div>
            <div class="value">Life Experience: 
            <?php
            echo htmlspecialchars($dietrichDiversity['life_experience']);
            ?>
            </div>
        </div>
        <?php
        }
        ?>
        </div>
        <?php
        }
        ?>
        
    </div> <!-- end content -->
    </div> <!-- end section -->

    <div class="accordion section contactInfo">
    <div id="contactInfo" class="heading" tabindex="0"><span class="switch"></span>Contact Info</div>
    <div class="content">
        <div class="subsection current">
            <div class="subheading">Current</div> 
            <div class="element">
            <div class="value address"><?php echo $street1; ?></div>
            <?php
            if ($street2) {
                echo '<div class="value address">' . $street2 . '</div>';
            }
            if ($street3) {
                echo '<div class="value address">' . $street2 . '</div>';
            }
            if ($street4) {
                echo '<div class="value address">' . $street2 . '</div>';
            }
            ?>
            <div class="value address">
            <?php 
            echo $city; 
            if($state != ""){
                echo ", " . $state;
            }
            echo " " . $postal;
            ?>
            </div> 
            <?php
            if ($country) {
                echo '<div class="value address">' . $country . '</div>';
            }
            if($tel != ""){
                echo '<div class="value address">Phone: ' . $tel . '</div>';
            }
            if($telMobile != "")
            {
                echo '<div class="value address">Mobile: ' . $telMobile . '</div>';
            }
            if($homepage != "")
            {
                if (strpos($homepage, '://') === FALSE) {
                    $homepage = 'http://' . $homepage;
                }
                echo '<div class="value address"><a href="'.$homepage.'" target="_blank">' 
                        . $homepage . '</a></div>';
            }
            ?>
            </div>
        </div>
        <div class="subsection permanent">
            <div class="subheading">Permanent</div> 
            <div class="element">
            <div class="value address"><?php echo $streetP1; ?></div>
            <?php
            if ($streetP2) {
                echo '<div class="value address">' . $streetP2 . '</div>';
            }
            if ($streetP3) {
                echo '<div class="value address">' . $streetP3 . '</div>';
            }
            if ($streetP4) {
                echo '<div class="value address">' . $streetP4 . '</div>';
            }
            ?>
            <div class="value address">
            <?php 
            echo $cityP; 
            if($stateP != ""){
                echo ", " . $stateP;
            }
            echo " " . $postalP;
            ?>
            </div> 
            <?php
            if ($country) {
                echo '<div class="value address">' . $country . '</div>';
            }
            ?>
            </div>
        </div>
    </div> <!-- end content -->
    </div> <!-- end section --> 
    
</div> <!-- end application div -->

<div style="clear: left"></div>
<br/>
<br/>

<?php
function hasRecommendformAnswers($recommendId) {
    
    $recommendformQuery = "SELECT * FROM recommendforms
                            WHERE recommend_id = " . $recommendId . "
                            ORDER BY form_id, question_id, question_key";
    $recommendformResult = mysql_query($recommendformQuery);
    
    if ( mysql_num_rows($recommendformResult) > 0 ) {
        return TRUE;   
    } else {
        return FALSE;
    }   
}
?>