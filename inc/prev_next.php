<?php 

    /**
     * Searches haystack for needle and 
     * returns an array of the key path if 
     * it is found in the (multidimensional) 
     * array, FALSE otherwise.
     *
     * @mixed array_searchRecursive ( mixed needle, 
     * array haystack [, bool strict[, array path]] )
     */
 
function array_searchRecursive( $needle, $haystack, $strict=false, $path=array() )
{
    if( !is_array($haystack) ) {
        return false;
    }
 
    foreach( $haystack as $key => $val ) {
        if( is_array($val) && $subPath = array_searchRecursive($needle, $val, $strict, $path) ) {
            $path = array_merge($path, array($key), $subPath);
            return $path;
        } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
            $path[] = $key;
            return $path;
        }
    }
    return false;
}

function getPageList ($applicationId) {

    /*
    $sql = "select distinct lu_programs_applicationreqs.appreq_id, applicationreqs.linkname, applicationreqs.sortorder
    from lu_programs_applicationreqs
    inner join applicationreqs on applicationreqs.id = lu_programs_applicationreqs.appreq_id
    inner join lu_application_programs on lu_application_programs.program_id = lu_programs_applicationreqs.program_id
    where lu_application_programs.application_id = " . $applicationId . " order by sortorder";
    */

    $sql = "select distinct applicationreqs.id, applicationreqs.linkname, applicationreqs.sortorder
    from programs_applicationreqs
    inner join applicationreqs 
        on applicationreqs.id = programs_applicationreqs.applicationreqs_id
    inner join lu_application_programs 
        on lu_application_programs.program_id = programs_applicationreqs.programs_id
    where lu_application_programs.application_id = " . $applicationId . " order by sortorder";
    
    $appPages = array();
    $result = mysql_query($sql)    or die(mysql_error());
    while($row = mysql_fetch_array( $result )) {
        //array_push($appPages, array($row['sortorder'] => $row['linkname']));
        array_push($appPages, $row['linkname']);
    }

    return $appPages;
}


?>
