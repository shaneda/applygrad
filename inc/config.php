<?php
// Set the default timezone to stop php from complaining about it.
date_default_timezone_set('America/New_York');

$prodpattern = "BANSHEE.";
$prodpattern1 = "APPLYGRAD.";
$statspattern = "WEB05.";
$statspattern1 = "APPLY.STAT";
$shibpattern1 = "WEB08";
$shibpattern2 = "WEB24";
$ssnpattern1 = "APPLYGRAD-SSN.";
$dietrichPattern = "APPLYGRAD-DIETRICH";
$designPattern = "APPLYGRAD-DESIGN";
$iniPattern = "APPLYGRAD-INI";
$web28pattern = "WEB28";
$devpattern = "_DEV";
$testpattern = "_TEST";
$hostname = strtoupper ($_SERVER['SERVER_NAME']);
$currentScriptName =  $_SERVER['SCRIPT_NAME'];

$prodtest1 = stristr($hostname, $prodpattern);
$prodtest2 = stristr($hostname, $prodpattern1);
$stattest1 = stristr($hostname, $statspattern);
$stattest2 = stristr($hostname, $statspattern1);
$ssntest1 =  stristr($hostname, $ssnpattern1);
$shibtest = stristr($hostname, $shibpattern1);
$shibtest2 = stristr($hostname, $shibpattern2);
$dietrichTest = stristr($hostname, $dietrichPattern);
$designTest = stristr($hostname, $designPattern);
$iniTest = stristr($hostname, $iniPattern);
$web28Test =  stristr($hostname, $web28pattern);
$scriptname = strtoupper ($_SERVER['SCRIPT_NAME']);

if (!function_exists("isDale")) {
    
    function isDale() {
        if ((isset($_SESSION['userWebIso']) && $_SESSION['userWebIso'] == 'dales@cs.cmu.edu') ||
            (isset($_SESSION['HTTP_EPPN']) && $_SESSION['HTTP_EPPN'] == 'dales@cs.cmu.edu'))  {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
}

if (!function_exists("das_debug")) {
    
    function das_debug() {
        if (isDale())  {
            DebugBreak();
        }
    }
}


// New production test using RELEASE_LEVEL environment variable
$productionRelease = FALSE;
if (isset($_SERVER['RELEASE_LEVEL']) && $_SERVER['RELEASE_LEVEL'] == 'production')
{
    $productionRelease = TRUE;
}
 
if (($prodtest1 == "BANSHEE.SRV.CS.CMU.EDU") ||  ($prodtest2 == "APPLYGRAD.CS.CMU.EDU")) 
{  
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment 
      //  print "test";    
      include 'config_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include 'config_production.php';
    } 
}
elseif  (($stattest1 == "WEB05.SRV.CS.CMU.EDU") ||  ($stattest2 == "APPLY.STAT.CMU.EDU")) {
   if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
      include 'config_stat_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include 'config_stat_production.php';
    } 
}
elseif  ($shibtest == "WEB08.SRV.CS.CMU.EDU")  {
   if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
      include 'config_shib_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include 'config_shib_production.php';
    } 
}
elseif  ($shibtest2 == "WEB24.SRV.CS.CMU.EDU")  {
  if (stristr($scriptname, $testpattern) !== FALSE) {
    // On production box but in test environment
    //  print "test";
    include 'config_shib_test2.php';
  }
   else
     {
       // this is the production box in production mode
       //     print "production";
       include 'config_shib_production2.php';
     }
}
elseif ($ssntest1 == "APPLYGRAD-SSN.CS.CMU.EDU") {
   if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
      include 'config_ssn_test.php';
   }
   else
    {
            // this is the production box in production mode 
       //     print "production"; 
       include 'config_ssn_production.php';
    } 
}
elseif ($dietrichTest == "APPLYGRAD-DIETRICH.CS.CMU.EDU")  
{
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment   
        include 'config_dietrich_test.php';
    }
    else
    {
        // this is the production box in production mode 
        
        include 'config_dietrich_production.php';
        
    } 
}
elseif ($designTest == "APPLYGRAD-DESIGN.CS.CMU.EDU")  
{
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment   
        include 'config_cfa_test.php';
    }
    else
    {
        // this is the production box in production mode 
        
        include 'config_cfa_production.php';
        
    } 
}
elseif ($iniTest == "APPLYGRAD-INI.CS.CMU.EDU")  
{
    if (stristr($scriptname, $testpattern) !== FALSE) 
    {     
        // On production box but in test environment 
          
        include 'config_ini_test.php';
    }
    else
    {
        // this is the production box in production mode
     
        include 'config_ini_production.php';
    } 
}
elseif ($web28Test == "WEB28.SRV.CS.CMU.EDU")  
{   
    if  (stristr($scriptname, $devpattern) === FALSE)
    {
        //include 'config_web28_test.php';
        // include 'config_cfa_test.php';
        // include 'config_cfa_production.php';
        // include 'config_magneto_test.php';
         include 'config_test.php';
        // include 'config_dietrich_test.php';
        //include 'config_ini_test.php';
        // include 'config_dietrich_production.php';
        // include 'config_stat_test.php';
        // include 'config_production.php';
        
    } else {
        // debugbreak();   
        //  applygrad test 
         include 'config_test.php';
        // include 'config_magneto_test.php';
        // applygradINI test
        // include 'config_ini_test.php';
        // include 'config_cfa_test.php';
        // include 'config_ini_production.php';
        // dietrich test
        // include 'config_dietrich_test.php';
        //  include 'config_dietrich_production.php';
        // include 'config_cfa_production.php';
        //  include 'config_production.php';
        // include 'config_stat_test.php';

    }    
}
elseif (stristr($scriptname, $devpattern) === FALSE) 
{
              // this is the test environment on development box
            //  print "dev test";
        include 'config_development_test.php';
    }
    else
        {
        // this is a development environment - _dev directory 
         //   print "dev";     
            include 'config_development.php';
        }
/*
if ((stristr($hostname, $prodpattern) === FALSE) && (stristr($hostname, $prodpattern1) === FALSE)) {   // not production machine
    $scriptname = strtoupper ($_SERVER['SCRIPT_NAME']);
    if (stristr($scriptname, $devpattern) === FALSE) {
              // this is the test environment on development box
            //  print "dev test";
        include 'config_development_test.php';
    }
    else
        {
        // this is a development environment - _dev directory 
         //   print "dev";     
            include 'config_development.php';
        }
    }
    elseif (stristr($hostname, $statspattern) === FALSE) {             // not stats machine
            $scriptname = strtoupper ($_SERVER['SCRIPT_NAME']);
            if (stristr($scriptname, $testpattern) !== FALSE) {     
        // On production box but in test environment 
      //  print "test";    
            include 'config_test.php';
        }
        else
         {
            // this is the production box in production mode 
       //     print "production"; 
             include 'config_production.php';
            }
    }
    else 
        {
            $scriptname = strtoupper ($_SERVER['SCRIPT_NAME']);
            if (stristr($scriptname, $testpattern) !== FALSE) {
                include 'config_stats_test.php'; 
            }
            else
                {
                    include 'config_stats_prod.php';
                }    
        }
        */
?>
