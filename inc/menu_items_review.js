/* Tigra Menu items structure */
var MENU_ITEMS = [
    ['Home', '../admin/home.php'],
    ['Users', '../admin/sysusers.php'],
    ['Data Access', null, null,
        ['Domains', '../admin/domains.php'],
        ['Schools', '../admin/schools.php'],
        ['Departments', '../admin/departments.php'],
        ['Degrees', '../admin/degrees.php'],
        ['Programs', '../admin/programs.php'],
        ['Study Fields', '../admin/fields.php'],
        ['Interests', '../admin/interests.php'],
        ['Page Content', '../admin/content.php'],
        ['--------------------------', null],
        ['Countries', '../admin/countries.php'],
        ['Institutes', '../admin/institutes.php'],
        ['States', '../admin/states.php']
    ],
    ['System Environment', 'config.php']
];
