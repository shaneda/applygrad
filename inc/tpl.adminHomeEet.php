<?php
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Admissions Home';
$pageCssFiles = array(
    '../css/units.css'
    );
$pageJavascriptFiles = array();

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

?>
<br/>

<div id="unitName"><?php echo $_SESSION['roleDepartmentName']; ?></div> 
<div id="pageTitle"><?php echo $pageTitle; ?></div>

<?php
if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 18) {
?>
<div style="float: right; margin-right: 15px;">
<form id="viewPeriodsForm" name="viewPeriodsForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
<b>View Periods:</b>
<input type="radio" name="viewPeriods" value="current" <?php echo $viewPeriodsCurrentChecked; ?> 
    onClick="document.viewPeriodsForm.submit(); return false;" /> Current
<input type="radio" name="viewPeriods" value="all" <?php echo $viewPeriodsAllChecked; ?>
    onClick="document.viewPeriodsForm.submit(); return false;" /> All
</form>
</div>
<?php
}
?>
<br/> 

<div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%">
<?php
 
$periodMenu = makePeriodMenu($adminUnit, $requestPeriodId);
echo $periodMenu;
?>
</div>

<div style="margin:5px; padding: 5px;">

<div id="table" style="margin-left: 20px; float: left; clear: right; width: 75%;">

<?php
if (!$periodMenu) {

    if ($viewPeriods == 'all') {
        $periodTimeframe = 'open or past';        
    } else {
        $periodTimeframe = 'open';     
    }
    
?>
    <div style="margin: 10px 10px 50px 10px; font-weight: bold;">
        There are no <?php echo $periodTimeframe; ?> admission periods.
    </div>

<?php
} else {    
?>

    <div style="margin: 10px;">
        <table width="100%" border="0" cellspacing="2" cellpadding="10">
            <tr valign="top">
                <td width="50%">

                <div class="legend">Administer Applications</div>
                <div class="outer">
                <div class="fieldset">
                <br/> 
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                    <?php
                        $adminHref = "../admin/administerApplications.php?unit=department";
                        $adminHref .= "&unitId=" . $_SESSION['roleDepartmentId'];
                        $adminHref .= "&period=" . $requestPeriodId;
                    ?>
                    <a href="<?php echo $adminHref; ?>">Administer Applications</a>
                </div>             
                <br/><br/>
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                    <?php
                        //$emailHref = "../admin/remindUnpaidApps.php?id=" . $_SESSION['roleDepartmentId'];
                        $emailHref = '../admin/emailApplicants.php?periodId=' . $requestPeriodId;
                        $emailHref .= '&departmentId=' . $_SESSION['roleDepartmentId'];
                    ?>
                    <a href="<?php echo $emailHref; ?>">Email Applicants</a>
                </div>
                <br/>
                <br/> 
                </div>
                </div>           

                <br/>
                <div class="legend">Export Data</div>
                <div class="outer">
                <div class="fieldset">
                <br/> 
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                    <?php
                        $dumpHref = "../admin/getDbDump.php?department_id=" . $_SESSION['roleDepartmentId'];
                        $dumpHref .= "&period=" . $requestPeriodId . "&dump=applications";
                    ?>
                    <a href="<?php echo $dumpHref; ?>">Export Applications</a>
                </div>             
                <br/><br/> 
                </div>
                </div>      

                </td>
            
            <td width="50%">

                <div class="legend">Registered Students</div>            
                <div class="outer">
                <div class="fieldset">
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                    <?php
                        $params = '?id=' . $_SESSION['roleDepartmentId'] . '&period=' . $requestPeriodId;
                    ?>
                    <a href="admitted.php<?php echo $params; ?>">List of Registered Students</a> 
                </div> 
                <br/><br/> 
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                    <a href="shippinglabels-admitted.php<?php echo $params; ?>">Print Mailing Labels</a>  
                </div> 
                <br/><br/>
                </div>
                </div>

            </td>
            </tr>
            </table>
                    
    </div>
<?php
}    
?>
</div> 

</div> 
<div style="clear: both" />

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

function makePeriodMenu($unit, $headingPeriodId = NULL, $displayLinks = TRUE) {
    
    global $viewPeriods;
    
    $unitId = $unit->getId();
    $periods = $unit->getPeriods(1);
    $periodCount = count($periods);
    $menu = '';
    $emptyMenu = TRUE;
    
    if ($periodCount > 0) {
        $menu .= '<ul class="menu" style="list-style-type: none;">';    
    }

    foreach ($periods as $period) {
        
        if ($period->getPeriodTypeId() != 1) {
            continue;
        }
        
        $periodId = $period->getId();
        $periodName = $period->getName();
        $periodDescription = $period->getDescription();
        $startDate = $period->getStartDate('Y-m-d');
        $endDate = $period->getEndDate('Y-m-d');
        $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate;
        $status = $period->getStatus();
        
        if ($status == 'future') {
            continue;
        }
        
        if ($status == 'closed' && $viewPeriods != 'all') {
            continue;
        }        

        if ($periodId == $headingPeriodId) {
            $periodAnchor = '<div class="selected">';    
        } else {
            $periodAnchor = '<div>';    
        }
        
        if ($periodDescription) {
            $anchorTitle = $periodDescription . ', ' . $periodDates . ' (period id: ' . $periodId . ')';    
        } else {
            $anchorTitle = $periodDates . ' (period id: ' . $periodId . ')';   
        }
        
        $periodAnchor .= '<a title="' . $anchorTitle . '"';
        
        if (!$displayLinks || ($periodId == $headingPeriodId) ) {
            $periodAnchor .= '>';
        } else {
            $periodAnchor .= ' href="?unit_id=' . $unitId . '&period_id=' . $periodId . '">';
        }
        
        if ($periodName) {
            $periodAnchor .= $periodName . '</a></div>';     
        } elseif ($periodDescription) {
            $periodAnchor .= $periodDescription . '</a></div>';    
        } else {
            $periodAnchor .= $periodDates . '</a></div>';   
        }

        $menu .= '<li>' . $periodAnchor . '</li>';
    
        $emptyMenu = FALSE;
    }
    
    if ($periodCount > 0) {
        $menu .= '</ul>';    
    }
    
    if ($emptyMenu) {
        $menu = '';    
    }
    
    return $menu;  
}


function displayPeriod($periodId, $rootPeriod = TRUE) {
    
    global $adminUnit;
    
    $period = new Period($periodId);
    
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $status = $period->getStatus();

    $display = '';
    
    if ($rootPeriod) {
        $heading = '<a class="tooltip" title="period id: ';
        $heading .= $period->getId() . '" style="cursor: pointer;">';   
        $heading .= '<b>' . $period->getName() . '</b>';
        $heading .= ' (' . $period->getAdmissionTerm() . ' ' . $period->getAdmissionYear() . ')';  

        if ($adminUnit) {
            $periodUnitId = $period->getUnitId();
            if ( $adminUnit->getId() != $periodUnitId ) {
                $periodUnit = new Unit($periodUnitId);
                $heading .= '<br/>[' . $periodUnit->getName() . ']';  
            }
        }
    
        $heading .= '</a>';
        $display = $heading . '<div class="tooltipContent" style="visibility: hidden; height: 0px;">';
        $display .= $heading . '<br/>';
        $display .= 'start date: ' . $startDate . '<br/>';
    
    } else {
        
        $display = $period->getPeriodType() . ' deadline: ' . $endDate . '<br/>';
    }
    
    $subperiods = $period->getChildPeriods();
    $subperiodCount = count($subperiods);
    if ($subperiodCount > 0) {
        foreach ($subperiods as $subperiod) {
            $display .= displayPeriod($subperiod, FALSE);
        }  
    }
    
    if ($rootPeriod) {
        $display .= '<br/>Application Programs:';
        $programs = $period->getPrograms();
        $programCount = count($programs);
        if ($programCount > 0) {
            $display .= '<ul>';
            foreach ($programs as $programId => $program) {
                $display .= '<li><a title="unit id: ' . $programId . '" style="cursor: pointer;">' . $program .'</a></li>';    
            }
            $display .= '</ul>';
        }
    }
    
    if ($rootPeriod) {
        $display .= '</div><br/>';
    }
    
    return $display;     
}
?>
