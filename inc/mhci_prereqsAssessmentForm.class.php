<?php

class MHCI_PrereqsAssessmentForm
{

    // Fields set in constructor.
    private $studentLuUsersUsertypesId;
    private $view;                      // "student" || "reviewer" 
    private $prereqType;                // "design" || "programming" || "statistics"
    private $prereqName;                // ucfirst($prereqType)
       
    
    // Fields set with form/db data.
    private $prereqId;
    private $prereqStatusId;
    private $studentAssessment;         // "fulfilledTrue" || "fulfilledFalse"
    private $reviewerStatus = array();
    // private $reviewerExplanation = array();
    private $reviewerExplanation = "";
    private $studentStatus;
    private $applicationId;
    private $placeoutPeriodId;
    
    // Fields for rendering form.
    private $formSubmitted = FALSE;
    private $formSaved = FALSE;
     
    private $inProgressLabel = "In progress";
    private $planLabel = "Approved plan:";
    private $fulfilledDegreeLabel = "Fulfilled: undergraduate degree";
    private $fulfilledLabel = "Fulfilled:";
    private $studentSubmitLabel = "Submit Test to Reviewer";
    private $reviewerSubmitLabel = "Change Status";
    private $studentSaveLabel = "Save Without Submit to Reviewer";
    
    private $fulfilledTrueChecked = "";
    private $fulfilledFalseChecked = "";
    private $inProgressSelected = "";
    private $planSelected = "";
    private $fulfilledDegreeSelected = "";
    private $fulfilledSelected = "";
    public $submitButtonDisabled = "disabled";

    
    function __construct($studentLuUsersUsertypesId, $view, $prereqType) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view; 
        $this->prereqType = $prereqType;
        $this->prereqName = ucfirst($prereqType);  
        $this->periodId  = $_SESSION['activePlaceoutPeriod'];
        $this->placeoutPeriodId = $_SESSION['activePlaceoutPeriod'];
    }
    

    public function getStudentAssessment () {
        return $this->studentAssessment;
    }
    
    public function getFormSubmitted () {
        return $this->formSubmitted;
    }
    
    public function setFormSubmitted ($value) {
        $this->formSubmitted = $value;
        return $this->formSubmitted;
    }
    
    public function getFormSaved () {
        return $this->formSaved;
    }
    
    public function setFormSaved ($value) {
        $this->formSaved = $value;
        return $this->formSaved;
    }
    
    public function getApplicationId () {
        return $this->applicationId;
    }
    
    public function setApplicationId ($value) {
        $this->applicationId = $value;
        return $this->applicationId;
    }
    
    public function getStudentStatus () {
        return $this->studentStatus;
    }
    
    public function setStudentStatus ($value) {
        $this->studentStatus = $value;
        return $this->studentStatus;
    }
    
    public function getSaveLabel () {
        return $this->studentSaveLabel;
    }
    
    public function getStudentAssessmentFromDB($AType) {
        $assessment = "";
        $sql = "select student_assessment from mhci_prereqs where prereq_type = '".$AType."' and student_lu_users_usertypes_id = {$this->studentLuUsersUsertypesId}";
        $result = mysql_query($sql);
        // DebugBreak();
        if ($result) {
            while ($row = mysql_fetch_array($result)) {
                $assessment = $row[0];
                 }
        }
        return $assessment;
    }
    
    public function getAssessmentStatusFromDB($AType) {
        $status = "";
        $sql = "select status from mhci_prereqs where prereq_type = '".$AType."' and student_lu_users_usertypes_id = {$this->studentLuUsersUsertypesId}";
        $result = mysql_query($sql);
        if ($result) {
            while ($row = mysql_fetch_array($result)) {
                $status = $row[0];
                 }
        }
        return $status;
    }
    
    public function updateDbStudentStatus () {
        
        $query = "update mhci_prereqs set status = '". $this->getStudentStatus();
        $query .= "' WHERE mhci_prereqs.id = {$this->prereqId} AND mhci_prereqs.prereq_type = '{$this->prereqType}' 
        AND mhci_prereqs.period_id = {$this->placeoutPeriodId}";
        $result = mysql_query($query);
    }
    
    public function updateDbReviewerStatus ($status) {
        
        if ($this->prereqId != NULL) {
            if ($status == TRUE) {
                $safeStatus = 'Submitted';
            } else {
              //  DebugBreak() ;
                $safeStatus = 'Not Submitted';
            }
            
            $query = "INSERT INTO mhci_prereqs_status";
                
                // Handle updates.
            $query .= " (mhci_prereqs_id, reviewer_status,reviewer_timestamp)";
            $query .= sprintf(" VALUES (%d,'%s', NOW()) 
                            ON DUPLICATE KEY UPDATE reviewer_status = '%s'",
                            $this->prereqId,
                            $safeStatus,
                            $safeStatus
                            );            
                
         //   debugBreak();
            $result = mysql_query($query);
            }   
    }
    
    public function getPlaceoutPeriodId () {
        return $this->placeoutPeriodId;
    }
    
    public function setPlaceoutPeriodId ($value) {
        $this->placeoutPeriodId = $value;
        return $this->placeoutPeriodId;
    }
    
    public function getReviewerStatus () {
        return $this->reviewerStatus;
    }
    public function setReviewerStatus ($value) {
        $this->reviewerStatus = $value;
        return $this->reviewerStatus;
    }
    
    public function getAssessmentStatus() {
        $assessmentStatus = "";
        $stuStatus = $this->getStudentStatus();
        if (!isset ($stuStatus) || $stuStatus == "")
        {   
            $assessmentStatus = "Not Started";
        } else
            {
             $assessmentStatus = $stuStatus;  
            }
        return $assessmentStatus;
    }
    
    public function prepareDataForRender() {
      //  DebugBreak();
        $dbAssessmentStatus = $this->getAssessmentStatusFromDB($this->prereqType);
       // if ($dbAssessmentStatus == 'Student Submitted' || $dbAssessmentStatus == 'Student Saved')
        //get data in database if it exists
        $this->getAssessment();
        
        // Get the request data.
        $this->handleRequest();
        
        if ($this->formSubmitted || $this->formSaved) {

            // The form has been submitted.            
            // Insert the data into the database
            $this->addAssessment();
                    
        } else {
             
            // The user is either returning to or starting a new form.
            
            if (!$this->prereqId) {
                
                // The data were not returned in the REQUEST.                
                // Get the data from the db.
                $this->getAssessment();
                   
                if (!$this->prereqId) {
                    
                    // This prereq hasn't been started, so insert an "empty" prereq/assessment record.
                    // The record will be needed to conduct conversations 
                    // when no-one has submitted an assessment yet.
                   /// DAS removed 
                    $this->setReviewerStatus('Not Submitted');
                    $this->addAssessment();
                   
                    

                }
            
            }
                                   
        }
        
        // Get the record from the db (handles problem with reload after submit).
        if (!$this->prereqId) {
        $this->getAssessment(); 
     //   DebugBreak();

        } else {
         //   DebugBreak();
        }
        
        return TRUE;
    }
    
    public function render() {
        /*
        // Get the request data.
        $this->handleRequest();
  
        if ($this->formSubmitted) {

            // The form has been submitted.            
            // Insert the data into the db.
        //    DebugBreak();
            $this->addAssessment();
                    
        } else {
             
            // The user is either returning to or starting a new form.
            
            if (!$this->prereqId) {
                
                // The data were not returned in the REQUEST.                
                // Get the data from the db.
                $this->getAssessment();
                
                if (!$this->prereqId) {
                    
                    // This prereq hasn't been started, so insert an "empty" prereq/assessment record.
                    // The record will be needed to conduct conversations 
                    // when no-one has submitted an assessment yet.
                    $this->addAssessment();
                    

                }
            
            }
                                   
        }
        
        // Get the record from the db (handles problem with reload after submit).
        $this->getAssessment();
        */
        
        // Determine which elements should be checked when rendered.
        $this->setSelectedFields();
        
        // Render the view.                                               ->prereqId
        if ($this->view == "reviewer") {
            
            $this->renderReviewerForm();
            
        } else {
            $testStudentStatus = $this->getStudentStatus();
            if (!isset($testStudentStatus) || $testStudentStatus == NULL)   {
                $this->renderStudentForm(TRUE);
            } else {
          //      DebugBreak();
            
                $this->renderStudentForm(FALSE);
            }
            
        }
        if ($this->formSubmitted != TRUE)
        
        return TRUE;

    }
    
    
    private function handleRequest() {
        
        if ( isset($_REQUEST[$this->prereqType . '_prereqId']) ) {
            
            $this->prereqId = $_REQUEST[$this->prereqType . '_prereqId'];
        
        }
        
        if ( isset($_REQUEST[$this->prereqType . '_studentAssessment']) ) {
            
            $this->studentAssessment = $_REQUEST[$this->prereqType . '_studentAssessment'];
        
        }
        if ($this->view != 'student') {
            if ( isset($_REQUEST[$this->prereqType . '_reviewerStatus']) ) {
            
                $this->reviewerStatus = $_REQUEST[$this->prereqType . '_reviewerStatus']; 
                
            } else {
                // DAS DebugBreak();
                //$this->reviewerStatus = "No Status"; // default 
                
            }
        }
        
        if ( isset($_REQUEST[$this->prereqType . '_statusModifier']) ) {
            
            $this->reviewerExplanation = $_REQUEST[$this->prereqType . '_statusModifier'];
        
        }
        
        if ( isset($_REQUEST[$this->prereqType . '_assessmentSubmit']) ) {
            $this->formSubmitted = TRUE;
            
        }
      //   DebugBreak();
        if (isset($_REQUEST['TestSave'])) {
            $this->formSaved = TRUE;
            $this->setStudentStatus('Student Saved');
            $this->updateDbStudentStatus();
        }
        
        if (isset($_REQUEST['TestSubmit'])) {
          //  DebugBreak();
            $this->formSubmitted = TRUE;
            if ($_REQUEST['TestSubmit'] == 'Submit for Review') {
                $this->setStudentStatus('Student Submitted');
                $this->updateDbStudentStatus();
                $this->setReviewerStatus('Submitted');
                if (isset($this->prereqId)) {
                    $this->updateDbReviewerStatus(TRUE);
                }
            } 
        }
        
        return TRUE;
        
    }
    

    private function getAssessment() {
        
 //       $query = "SELECT * FROM mhci_prereqs
 //                   WHERE student_lu_users_usertypes_id = {$this->studentLuUsersUsertypesId}
//                    AND prereq_type = '{$this->prereqType}' AND period_id = {$this->prereqPeriod}"; 

        $query = "SELECT mhci_prereqs.id, mhci_prereqs.student_lu_users_usertypes_id,
        mhci_prereqs.prereq_type, mhci_prereqs.period_id, mhci_prereqs.student_assessment,
        mhci_prereqs.status as student_status, mhci_prereqs.application_id, mhci_prereqs.timestamp,
        mhci_prereqs_status.id as status_prereqs_id, 
        mhci_prereqs_status.reviewer_lu_users_usertypes_id, mhci_prereqs_status.reviewer_status,
        mhci_prereqs_status.reviewer_explanation, mhci_prereqs_status.reviewer_timestamp,
        mhci_prereqs_status.status as review_status
        from mhci_prereqs
        left outer join mhci_prereqs_status on mhci_prereqs_status.mhci_prereqs_id = mhci_prereqs.id
        WHERE mhci_prereqs.student_lu_users_usertypes_id = {$this->studentLuUsersUsertypesId}
        AND mhci_prereqs.prereq_type = '{$this->prereqType}' 
        AND mhci_prereqs.period_id = {$this->placeoutPeriodId}";
        if (isset($this->prereqId)) {
          $query .= " and mhci_prereqs.id = {$this->prereqId}";
        }     
        $result = mysql_query($query);
        $test = mysql_affected_rows();
        if ((mysql_affected_rows() > 1) || $test == NULL  || $test == -1) {
        } else {
            while ($row = mysql_fetch_array($result)) {
                $this->prereqId = $row['id'];
                $this->studentAssessment = $row['student_assessment'];
      /*          if(isset ($row['program_id'])) {
                $this->reviewerStatus = array_push($this->reviewerStatus[$row['program_id']], $row['reviewer_status']);
                $this->reviewerExplanation = array_push($this->reviewerExplanation[$row['program_id']], $row['reviewer_explanation']);
              }
              */
                $this->reviewerStatus = $row['reviewer_status'];
                $this->reviewerExplanation = $row['reviewer_explanation'];
                $this->studentStatus = $row['student_status'];
                $this->application_id = $row['application_id'];
                $this->placeoutPeriodId = $row['period_id'];
            }
        }
        return TRUE;          
    }
    
    private function addAssessment() {
        
        if ($this->view == "reviewer") {
            
            $insertId = $this->addReviewerAssessment();

        } else {
            
            $insertId = $this->addStudentAssessment();
            
        }
        
        return $insertId;
        
    }


    private function addStudentAssessment() {
        
        // debugBreak();
        $query = "INSERT INTO mhci_prereqs";
        if ($this->prereqId) {
            
            // Handle updates.
            $query .= " (id, student_lu_users_usertypes_id, prereq_type, student_assessment, period_id, status, application_id)"; 
            $query .= sprintf(" VALUES (%d, %d, '%s', '%s', %d, '%s', %d)
                        ON DUPLICATE KEY UPDATE student_assessment = '%s'",
                        $this->prereqId,
                        $this->studentLuUsersUsertypesId,
                        mysql_real_escape_string($this->prereqType),
                        mysql_real_escape_string($this->studentAssessment),
                        $this->placeoutPeriodId,
                        mysql_real_escape_string($this->reviewerStatus),
                        $this->applicationId,
                        mysql_real_escape_string($this->studentAssessment)
                        );            
            
        } else {
            
            $query .= " (student_lu_users_usertypes_id, prereq_type, student_assessment, period_id, status, application_id)";           
            $query .= sprintf(" VALUES (%d, '%s', '%s', %d, '%s', %d)",
                        $this->studentLuUsersUsertypesId,
                        mysql_real_escape_string($this->prereqType),
                        mysql_real_escape_string($this->studentAssessment),
                        $this->placeoutPeriodId,
                        mysql_real_escape_string($this->reviewerStatus),
                        $this->applicationId
                        );
        }
        //echo $query . "<br />";
     //   DebugBreak();
        $result = mysql_query($query);
        
      /*  if ($result) {
            
            $linkname = "mhci_prereqs_" . $this->prereqType . ".php";
            updateReqComplete($linkname, 0);    
            
        }
        */
 
        return mysql_insert_id();     
    }
    
    
    public function addReviewerAssessment() {
        
  //      $query = "INSERT INTO mhci_prereqs";
        $query = "INSERT INTO mhci_prereqs_status";
        
        if ($this->prereqId) {
            
            // Handle updates.
            $query .= " (mhci_prereqs_id, reviewer_status, reviewer_explanation, 
                        reviewer_lu_users_usertypes_id, reviewer_timestamp)";
            $query .= sprintf(" VALUES (%d, '%s', '%s', %d, NOW())
                        ON DUPLICATE KEY UPDATE reviewer_status = '%s',
                        reviewer_explanation = '%s',
                        reviewer_lu_users_usertypes_id = %d,
                        reviewer_timestamp = NOW()",
                        $this->prereqId,
                        mysql_real_escape_string($this->reviewerStatus),
                        mysql_real_escape_string($this->reviewerExplanation),
                        $_SESSION['A_userid'],
                        mysql_real_escape_string($this->reviewerStatus),
                        mysql_real_escape_string($this->reviewerExplanation),
                        $_SESSION['A_userid']
                        ); 

        } else {
            
            $query .= " (student_lu_users_usertypes_id, prereq_type, reviewer_status, reviewer_explanation,
                        reviewer_lu_users_usertypes_id, reviewer_timestamp)";
            $query .= sprintf(" VALUES (%d, '%s', '%s', '%s', %d, NOW())",
                        $this->studentLuUsersUsertypesId,
                        mysql_real_escape_string($this->prereqType),
                        mysql_real_escape_string($this->reviewerStatus),
                        mysql_real_escape_string($this->reviewerExplanation),
                        $_SESSION['A_userid']
                        );             
            
        }
        //echo $query;
       // DebugBreak();
        $result = mysql_query($query);
        
        return mysql_insert_id();     
    }
    
    
    private function setSelectedFields() {
        
        if ($this->studentAssessment == "fulfilledTrue") {
            
            $this->fulfilledTrueChecked = "checked";    
        
        } elseif ($this->studentAssessment == "fulfilledFalse") {
            
            $this->fulfilledFalseChecked = "checked";
            
        }
        
        switch ($this->reviewerStatus) {
            
            case $this->inProgressLabel:
            
                $this->inProgressSelected = "selected";
                break;

            case $this->planLabel:
            
                $this->planSelected = "selected";
                break;
            
            case $this->fulfilledDegreeLabel:
            
                $this->fulfilledDegreeSelected = "selected";
                break;

            case $this->fulfilledLabel:
            
                $this->fulfilledSelected = "selected";
                break;
            
            default:
            
                // Do nothing.   
        }
        
        // Enable the submit button if the student hasn't selected an assessment.
        // Enabling will be handled with javascript in other cases.
        $statusAssessment =  $this->getAssessmentStatus();
        if ( ($this->view == "student") && (!($this->fulfilledTrueChecked || $this->fulfilledFalseChecked) ||
            $statusAssessment != 'Student Submitted')) {
            
            $this->submitButtonDisabled = "";
            
        }  
        if  (($this->view == "student") && ($statusAssessment == 'Student Submitted')) {
            $this->formSubmitted = TRUE;
        }
        
              
        return TRUE;
        
    }
    
    
    private function renderStudentForm($editView=TRUE) {
    //    debugBreak();
        $fulfilledTrueLabel = $this->studentAssessmentTrueVerbose();
        $fulfilledFalseLabel = $this->studentAssessmentFalseVerbose();
        $submitLabel = $this->getSaveLabel();
        if ($editView != TRUE)   {             
            echo <<<EOH
            <div class="bodyText-bold">
            Select {$this->prereqName} Knowledge Status:</div><br/>
EOH;
        } else {
            echo <<<EOS
            <div class="bodyText-bold">First, tell the reviewer where you are in the process:</div><br/>
EOS;
        }
        
        echo <<<EOB
        <input type="hidden" id="{$this->prereqType}_prereqId" name="{$this->prereqType}_prereqId" value="{$this->prereqId}" />
        <input id="{$this->prereqType}_studentAssessmentFulfilledTrue" name="{$this->prereqType}_studentAssessment" type="radio" class="assessmentRadio" value="fulfilledTrue" {$this->fulfilledTrueChecked} {$this->submitButtonDisabled}/>
        <label class="bodyText">&nbsp;{$fulfilledTrueLabel}</label>
        <br/>
        <input id="{$this->prereqType}_studentAssessmentFulfilledFalse" name="{$this->prereqType}_studentAssessment" type="radio" class="assessmentRadio" value="fulfilledFalse" {$this->fulfilledFalseChecked} {$this->submitButtonDisabled}/>
        <label class="bodyText">&nbsp;{$fulfilledFalseLabel}</label><br/>        
EOB;

      
        ///     echo "<button class='bodyButton-right' disabled=".$this->submitButtonDisabled.">Submit to the reviewer</button>";

// do not render the save button
     //  $this->renderSubmit($this->studentSubmitLabel);    

        echo "<br/>";
        
        if ($this->formSubmitted) {
         //   DebugBreak();
            if ($this->getStudentAssessment() == "fulfilledTrue")   {
                $this->renderFeedbackForStudent(TRUE);
            } else {
                $this->renderFeedbackForStudent(FALSE);
            }
        }
        echo "<br/><br/>";
        
    }
    
    
    private function renderReviewerForm() {
        
        $this->prepareDataForRender();
        $this->setSelectedFields();
        $studentAssessment = $this->studentAssessmentVerbose();
        $reviewerAssessment = $this->reviewerStatus . " " . $this->reviewerExplanation;
        $submitLabel = $this->reviewerSubmitLabel; 
        
        echo <<<EOB
        <hr />
        <div class="bodyText-bigger">
            <span class="bodyText-bold-bigger">From the student:</span>&nbsp;{$studentAssessment}
        </div>
        <br/>
        <div class="bodyText-bigger">
            <span class="bodyText-bold-bigger">Current status as viewed by student:</span>&nbsp;{$reviewerAssessment}
        </div>
        <br/><br/>
        <div class="bodyText">
            An explanation is required when changing the status from "In progress" to "Plan" or "Fulfilled". 
            Your explanation will be visible to the student along with your choice from the dropdown.
        </div>
        <br/>
        <input type="hidden" id="{$this->prereqType}_prereqId" name="{$this->prereqType}_prereqId" value="{$this->prereqId}" />
        <input id="{$this->prereqType}_studentAssessment" name="{$this->prereqType}_studentAssessment" type="hidden" value="{$this->studentAssessment}" />
        <select id="{$this->prereqType}_statusSelect" name="{$this->prereqType}_reviewerStatus" class="statusSelect">
            <option value="{$this->inProgressLabel}" {$this->inProgressSelected}>{$this->inProgressLabel}</option>
            <option value="{$this->planLabel}" {$this->planSelected}>{$this->planLabel}</option>
            <option value="{$this->fulfilledDegreeLabel}" {$this->fulfilledDegreeSelected}>{$this->fulfilledDegreeLabel}</option>
            <option value="{$this->fulfilledLabel}" {$this->fulfilledSelected}>{$this->fulfilledLabel}</option>
        </select>
        <input id="{$this->prereqType}_statusModifier" name="{$this->prereqType}_statusModifier" class="statusModifier" type="text" value="{$this->reviewerExplanation}"/>
        <br/><br/>

EOB;

        $this->renderSubmit($submitLabel);
        echo "<br/><br/>";

    }
    
    
    public function renderSubmit($label) {
        echo <<<EOB
               
        <input type="submit" id="{$this->prereqType}_assessmentSubmit" name="{$this->prereqType}_assessmentSubmit" class="bodyButton assessmentSubmit" value="{$label}" {$this->submitButtonDisabled}/>     

EOB;
        
    }    
    
    
    private function studentAssessmentVerbose() {
        
        if ($this->studentAssessment == "fulfilledTrue") {
        
            $assessment = $this->studentAssessmentTrueVerbose();
            
        } elseif ($this->studentAssessment == "fulfilledFalse") {
            
            $assessment = $this->studentAssessmentFalseVerbose();
        
        } else {
            
            $assessment = "No response";
            
        }
        
        return $assessment;
        
    }
    
    
    private function studentAssessmentTrueVerbose() {
        switch ($this->prereqType) {
            case "design":
                $assessment = "I have taken a course or would like to submit an online portfolio for review.";
                break;
            case "statistics":
                $assessment = "I have taken a course.";
                $assessment .= " (i.e., I have taken some Statistics courses and recieved at least a C)";    
                break;
            case "programming":
                $assessment = "I have taken a course or would like to submit a code sample for review.";
                break;
            default:
                DebugBreak();
        }  

        return $assessment;
        
    }
    
    
    private function studentAssessmentFalseVerbose() {
        switch ($this->prereqType) {
            case "design":
                $assessment = "I have not taken a course and do not have an online portfolio for review.";
                break;
            case "statistics":
                $assessment = "I have not taken a course.";
                $assessment .= " (i.e., I currently have no Statistics background)";    
                break;
            case "programming":
                $assessment = "I have not taken a course and do not have an online portfolio for review.";
                break;
            default:
                DebugBreak();
        }
       
       return $assessment;
        
    }
    
    private function renderFeedbackForStudent($PlaceoutEvalRequired) {
        $periodSql = "select * from period where period_id = " . $this->placeoutPeriodId;
        $result = mysql_query($periodSql);
        
        while ($row = mysql_fetch_array($result)) {
            $startDate = date("F j", strtotime($row ['start_date'])) ;
            $endDate = date("F j", strtotime($row ['end_date'])) ;
        }
        if ($PlaceoutEvalRequired == TRUE)

        echo <<<EOB
         <br>      
        You will receive feedback from faculty between {$startDate} and {$endDate}     

EOB;
        else
        switch ($this->prereqType) {
            case 'statistics':
                echo <<<EOB
               
        Nicole Willis will work with you directly for enrollment in the online statistics course offered this summer.     

EOB;
                break;
            case 'design':
                echo <<<EOB
               
        Nicole Willis will work with you directly for enrollment in Communication Design Fundamentals (CDF) offered this fall.       

EOB;
                break;
            case 'programming':
                echo <<<EOB
               
        Nicole Willis will work with you directly for enrollment in the online programming course offered this summer.       

EOB;
                break;
            
        }
        
    }    

}

?>