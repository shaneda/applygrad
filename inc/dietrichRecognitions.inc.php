<?php
// Initialize variables
$dietrichRecognitions = NULL;
$dietrichRecognitionsError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveDietrichRecognitions();
    checkRequirementsDietrichRecognitions();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$dietrichRecognitionsError)
{
    $dietrichRecognitionsQuery = "SELECT description FROM dietrich_recognitions
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $dietrichRecognitionsResult = mysql_query($dietrichRecognitionsQuery);
    while($row = mysql_fetch_array($dietrichRecognitionsResult))
    {
        $dietrichRecognitions = $row['description'];         
    }    
} 
?>

<span class="subtitle">Recognitions</span>
<br/>
<br/>
Please list any recognitions for scholarship (e.g., awards, honorary societies, scholarships), 
memberships in professional societies, research, inventions, or any other creative work.
<br/>
<?php
showEditText($dietrichRecognitions, "textarea", "dietrichRecognitions", $_SESSION['allow_edit']); 
?>
<br/>
<hr size="1" noshade color="#990000">

<?php
function saveDietrichRecognitions()
{
    global $dietrichRecognitions;
    global $dietrichRecognitionsError;

    $dietrichRecognitions = filter_input(INPUT_POST, 'dietrichRecognitions', FILTER_SANITIZE_STRING);
    
    // Check for existing record
    $existingRecordQuery = "SELECT id FROM dietrich_recognitions WHERE application_id = " . intval($_SESSION['appid']);
    $existingRecordResult = mysql_query($existingRecordQuery);
    if (mysql_num_rows($existingRecordResult) > 0)
    {
        // Update existing record
        $updateQuery = "UPDATE dietrich_recognitions SET
            description = '" . mysql_real_escape_string($dietrichRecognitions) . "'
            WHERE application_id = " . intval($_SESSION['appid']);
        mysql_query($updateQuery);
    }
    else
    {
        // Insert new record
        $insertQuery = "INSERT INTO dietrich_recognitions (application_id, description)
            VALUES (" 
            . intval($_SESSION['appid']) . ",'"
            . mysql_real_escape_string($dietrichRecognitions) . "')";
        mysql_query($insertQuery);
    }
}

function checkRequirementsDietrichRecognitions()
{
    global $err;
    global $dietrichRecognitionsError;     
    
    if (!$err && !$dietrichRecognitionsError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>