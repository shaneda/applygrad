<?php
include_once '../classes/class.Department.php';
include_once '../inc/specialCasesAdmin.inc.php';

include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once "../classes/class.db_mse_risk_factors.php";
include_once "../classes/class.db_mse_bridge_course.php";
include_once "../classes/class.db_mse_letter_recs.php";

$db_risk_factors = new DB_MseRiskFactors();
$db_bridge_courses = new DB_MseBridgeCourse();
$db_letter_recs = new DB_MSELetterRecs(); 

// PLB revised csd ranking scale
$csdVoteVals = array(
    array(1, "Top 10 (1%)"),
    array(2, "Top 25 (2.5%)"),
    array(3, "Top 50 (5%)"),
    array(4, "Top 100 (10%)"),
    array(5, "Top 250 (25%)"),
    array(6, "Bottom 75%"),
    array(7, "Bottom 50%")
);

// PLB new ML ranking scale
$machineLearningVoteVals = array(
    array(1, 1),
    array(1.25, 1.25),
    array(1.5, 1.5),
    array(1.75, 1.75),
    array(2, 2),
    array(2.5, 2.5),
    array(3, 3),
    array(4, 4),
    array(5, 5)
);

// PLB new CompBio ranking scale
$compBioVoteVals = array(
    array(1.1, 1.1),
    array(1.2, 1.2),
    array(1.3, 1.3),
    array(1.5, 1.5),
    array(1.7, 1.7),
    array(1.9, 1.9),
    array(2, 2),
    array(2.5, 2.5),
    array(3, 3),
    array(4, 4),
    array(5, 5)
);

// PLB added MSE ranking scales 01/19/09
$mseVoteVals = array(
    array(1, "Definite"),
    array(2, "Admit"),
    array(3, "Marginal"),
    array(4, "Reject")
);

$mseMsitVoteVals = array(
    array(1, "Accept"),
    array(2, "Marginal"),
    array(3, "Reject")
);

$isrseVoteVals = array(
    array(1, 'top 5'),
    array(1.5, ''),
    array(2, 'top 10'),
    array(2.5, ''),
    array(3, 'prob out'),
    array(4, 'out'),
    array(5, 'way out')
);

$csdFacVoteVals = array(
    array(1, 1),
    array(2, 2),
    array(3, 3),
    array(4, 4),
    array(5, 5),
    array(6, 6),
    array(7, 7),
    array(8, 8),
    array(9, 9),
    array(10, 10)
);

$csdAois1 = array(
    array(101, "<span title='Algorithms and Complexity'>AC</span>"),
    array(102, "<span title='Artificial Intelligence'>AI</span>"),
    array(103, "<span title='Computational Biology'>CB</span>"),
    array(104, "<span title='Computational Neuroscience'>CN</span>"),
    array(105, "<span title='Computer Architecture'>CA</span>"),
    array(106, "<span title='Databases'>D</span>")
);

$csdAois2 = array(
    array(107, "<span title='Formal Methods'>FM</span>"),
    array(108, "<span title='Game Theory'>GT</span>"),
    array(109, "<span title='Graphics and Animation'>GA</span>"),
    array(110, "<span title='Human-Computer Interaction'>HCI</span>"),
    array(111, "<span title='Machine Learning'>ML</span>"),
    array(112, "<span title='Mobile and Pervasive Computing'>MPC</span>")
);

$csdAois3 = array(
    array(113, "<span title='Networking'>N</span>"),
    array(114, "<span title='Operating Systems and Distributed Systems'>OS</span>"),
    array(115, "<span title='Programming Languages'>PL</span>"),
    array(116, "<span title='Pure and Applied Logic'>PAL</span>"),
    array(117, "<span title='Robotics'>R</span>"),
    array(118, "<span title='Scientific Computing'>SC</span>")
);

$csdAois4 = array(
    array(119, "<span title='Security and Privacy'>SP</span>"),
    array(120, "<span title='Software Engineering'>SE</span>"),
    array(121, "<span title='Speech and Natural Languages'>SNL</span>"),
    array(122, "<span title='Technology and Society'>TS</span>"),
    array(123, "<span title='Vision'>V</span>")
);

$ltiSeminars = array(
    array(1, "IR"),
    array(2, "MT"),
    array(3, "SR"),
    array(4, "CL"),
    array(5, "BIO"),
    array(6, "CALL"),
    array(7, "ML")
);

$ltiVoteVals = array(
    array(1, 1),
    array(1.5, 1.5),
    array(2, 2),
    array(2.5, 2.5),
    array(3, 3)
);

$certaintyVals = array(
    array(5, 5),
    array(4, 4),
    array(3, 3),
    array(2, 2),
    array(1, 1)
);

$decisionVals = array(
    array('A1', 'A1'),
    array('A2', 'A2'),
    array('B1', 'B1'),
    array('B2', 'B2'),
    array('S', 'S'),
    array('R', 'R')
);

$allRiskFactorVals = array();
$riskFactorValQuery = "SELECT risk_factor_id FROM risk_factor";
$riskFactorValResult = mysql_query($riskFactorValQuery);
while ($row = mysql_fetch_array($riskFactorValResult)) {
    $allRiskFactorVals[] = $row['risk_factor_id'];    
}

$allPositiveFactorVals = array();
$positiveFactorValQuery = "SELECT positive_factor_id FROM positive_factor";
$positiveFactorValResult = mysql_query($positiveFactorValQuery);
while ($row = mysql_fetch_array($positiveFactorValResult)) {
    $allPositiveFactorVals[] = $row['positive_factor_id'];    
}


// This should come from printViewData.inc.php 
/*
$uid = -1;  // of applicant?
if ( isset($luUsersUsertypesId)) {
    $uid = $luUsersUsertypesId;
}
$umasterid = -1;    // of applicant?
$appid = -1;
*/

$view = 1;
$round = -1;
$reviewerId = $_SESSION['A_userid'];
$allowEdit = $_SESSION['A_allow_admin_edit'];
$thisDept = -1;
$semiblind_review = 0;
$semiblindByDefault = 0;
$facVote = 0;
$rank = 0;
$showDecision = false;
$decision = "";
$admit_to = "NULL";
$admissionStatus = "NULL";
$admit_comments = "";
$background = "";
$grades = "";
$statement = "";
$comments = "";
$point = "";
$pointCertainty = "";
$point2 = "";
$point2Certainty = "";
$private_comments = "";
//$round2 = -1;
$round2 = "";
$touched = 0;
$admit_vote = "";
$recruited = "";
$grad_name = "";
$pertinent_info = "";
$advise_time = "";
$commit_money = "";
$fund_source = "";
$interview = "";
$brilliance = "";
$other_interest = "";
$committeeReviews = array();
$committeeReviewsSeminars = array();

$committeeMemberVote = '0';
if ($_SESSION['A_usertypeid'] == 2){
    $committeeMemberVote = '1';    
}

$riskFactors = array();
$positiveFactors = array();
$positiveFactorOther = "";

$bridgeCourse = '';

$interviewDate = '';
$interviewType = '';
$interviewTypeText = '';
$interviewComments = '';
$mseEnglishComments = '';
$mseEnglishRating = NULL;
$mseProgrammingComments = '';
$mseProgrammingRating = NULL;
$mseFoundationalComments = '';
$mseFoundationalRating = NULL;
$mseMaturityComments = '';
$mseMaturityRating = NULL;
$mseUnderstandingComments = '';
$mseUnderstandingRating = NULL;
$mseExperienceComments = '';
$mseExperienceRating = NULL;

$recommendations = '';
$publications = '';

if(isset($_POST['userid']))
{
    $uid = intval($_POST['userid']);
} 
elseif ( isset($_GET['userid']) ) 
{
    $uid = intval($_GET['userid']);        
} 

if(isset($_GET['id']))
{
    $uid = intval($_GET['id']);
}
//VIEW TYPE
if(isset($_POST['v']))
{
    $view = intval($_POST['v']);
}
if(isset($_GET['v']))
{
    $view = intval($_GET['v']);
}
//ROUND NUMBER
if(isset($_POST['r']))
{
    $round = intval($_POST['r']);
}
if(isset($_GET['r']))
{
    $round = intval($_GET['r']);
}
//REVIEWER ID
if(isset($_POST['rid']))
{
    $reviewerId = intval($_POST['rid']);
}
if(isset($_GET['rid']))
{
    $reviewerId = intval($_GET['rid']);
    $allowEdit = false;
    $readOnly = TRUE;
} else {
    $readOnly = FALSE;    
}
//DEPARTMENT ID
if(isset($_POST['d']))
{
    $thisDept = intval($_POST['d']);
}
if(isset($_GET['d']))
{
    $thisDept = intval($_GET['d']);
}
if(isset($_GET['showDecision']))
{
    if($_GET['showDecision'] == 1)
    {
        $showDecision = true;
    }
}
if(isset($_POST['showDecision']))
{
    if($_POST['showDecision'] == 1)
    {
        $showDecision = true;
    }
}

if($showDecision == false)
{
    $sql = "select name, semiblind_review from department where id=".$thisDept;
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        $deptName = $row['name'];
        $semiblindByDefault = intval($row['semiblind_review']);
        if(!isset($_POST['btnSemiblind']) && !isset($_POST['chkSemiblind']))
        {
            $semiblind_review = intval($row['semiblind_review']);
        }
    }
    if(isset($_POST['btnSemiblind']))
    {
        if(intval($_POST['chkSemiblind']) == 1)
        {
            $semiblind_review = 0;
        }
        else
        {
            $semiblind_review = 1;
        }

    }
    else
    {
        if(isset($_POST['chkSemiblind']))
        {
            $semiblind_review = intval($_POST['chkSemiblind']);
        }
    }
}

// PLB added test for application_id 9/28/09
// $appid = -1 from above
if ( isset($_GET['applicationId']) ) {
    $appid = $_GET['applicationId'];
} elseif ( isset($_SESSION['appid']) ) {
    $appid = $_SESSION['appid'];
}

if($uid > -1)
{
    if (isset($_POST["btnSubmitFinal"]) &&
        (isDesignDepartment($thisDept)))
    {
        saveFinalDesignMasters();
    }
    elseif(isset($_POST["btnSubmitFinal"]))
    {
        $vars = $_POST;
        foreach($vars as $key => $value)
        {
            $arr = split("_", $key);
            if(strstr($key, 'comments_') !== false && $key != "comments_msit")
            {
                $itemId = $arr[1];
                $comments = $_POST["comments_".$arr[1]];
                $decision = $_POST["decision_".$arr[1]];
                $faccontact = $_POST["faccontact_".$arr[1]];
                $stucontact = $_POST["stucontact_".$arr[1]];
                $admissionStatus = $_POST["admissionStatus_".$arr[1]];
                $admitProgramId = $itemId;
                
                /*
                * Handle status "subtypes" for LTI and MSE. 
                */
                $admissionStatusArray = explode('_', $admissionStatus);
                if ( count($admissionStatusArray) == 2 ) {
                    $admissionStatus = $admissionStatusArray[0];
                    $admitProgramId = $admissionStatusArray[1];    
                }
                
                // Add period code            
                $periodSql = "select period_id from period_application
                    where application_id = " . $appid;
                $periodResult = mysql_query($periodSql) or die(mysql_error() . $periodSql);
                while($row = mysql_fetch_array($periodResult ))
                {
                    $currentPeriod = $row["period_id"];
                }

                if ($admissionStatus != 3) 
                {
                    $sql = "update lu_application_programs set 
                    decision = '".$decision."',
                    admission_status = ".intval($admissionStatus).",
                    admit_comments = '".addslashes($comments)."',
                    faccontact = '".addslashes($faccontact)."',
                    stucontact='".addslashes($stucontact)."'
                    where application_id = ".$appid."
                    and program_id = ".$itemId;
                    mysql_query($sql) or die(mysql_error().$sql);
                    
                    // DAS Add code for new application_admission table
                    $applicationDecisionSql = "insert into application_decision 
                    (application_id, program_id, period_id, admission_program_id, admission_status, comments)
                    values (".$appid.",".$itemId.",".$currentPeriod.",".$admitProgramId.",".intval($admissionStatus).",'";
                    if ($comments == "") 
                    {
                       $comments = "NULL";
                    }
                    else
                    {
                        $comments = addslashes($comments);
                    }
                    $applicationDecisionSql .= mysql_escape_string($comments)."') 
                    on duplicate key update admission_status=".intval($admissionStatus).", 
                    admission_program_id = " . $admitProgramId . ",
                    comments='".mysql_escape_string($comments)."'";
                    mysql_query($applicationDecisionSql) or die(mysql_error().$applicationDecisionSql);
                        
                
                } else {
                    $sql = "update lu_application_programs set 
                    decision = '".$decision."',
                    admission_status = NULL ".",
                    admit_comments = '".addslashes($comments)."',
                    faccontact = '".addslashes($faccontact)."',
                    stucontact='".addslashes($stucontact)."'
                    where application_id = ".$appid."
                    and program_id = ".$itemId;
                    mysql_query($sql) or die(mysql_error().$sql);

                    // DAS Add code for new application_admission table
                    $applicationDecisionSql = "insert into application_decision 
                    (application_id, program_id, period_id, admission_status, comments)
                    values (".$appid.",".$itemId.",".$currentPeriod.",".intval($admissionStatus).",";
                    if ($comments == "") 
                    {
                        $comments = "NULL";
                    }
                    else
                    {
                        //$comments = addslashes($comments);
                        $comments = "'" . mysql_escape_string( addslashes($comments) ) . "'";
                    }
                    //$applicationDecisionSql .= mysql_escape_string($comments)."') on duplicate key update admission_status=NULL, comments='".mysql_escape_string($comments)."'";
                    $applicationDecisionSql .= $comments . ") on duplicate key update admission_status=NULL, comments=" . $comments;
                    mysql_query($applicationDecisionSql) or die(mysql_error().$applicationDecisionSql);
                }
            }
            elseif ($key == "comments_msit") 
            {
                $arr = split("_", $key);
                $itemId = $arr[1];
                $comments = $_POST["comments_".$arr[1]];
                $admissionStatus = $_POST["admissionStatus_".$arr[1]];
                // Add period code
                $periodSql = "select period_id from period_application
                    where application_id = " . $appid;
                $periodResult = mysql_query($periodSql) or die(mysql_error() . $periodSql);
                while($row = mysql_fetch_array($periodResult ))
                {
                    $currentPeriod = $row["period_id"];
                }
                
                if ($admissionStatus != 3) 
                {
                    //  Check for msit
                    if ($itemId = "msit") 
                    {
                        if ($thisDept == 18) 
                        {
                            $itemId = 29;
                        } 
                        else 
                        {
                            $itemId = 100003;
                        }
                    }
                
                    // DAS add MSIT courtesy score
                    if ($thisDept == 18 || $thisDept == 47) 
                    {
                        $comments_msit = $_POST["comments_msit"];
                        $admissionStatus_msit = $_POST["admissionStatus_msit"];
                        if ($thisDept == 18) 
                        {
                            $currentMsitProgram = 29;
                        } 
                        else
                        {
                            $currentMsitProgram = 10003;
                        }

                        if ($admissionStatus_msit != 3) 
                        {
                            $applicationDecisionSql = "insert into application_decision 
                            (application_id, program_id, period_id, admission_status, comments)
                            values (".$appid.",".$currentMsitProgram.",".$currentPeriod.",".intval($admissionStatus_msit).",";
                            if ($comments_msit == "") 
                            {
                               $comments_msit = NULL;
                            }
                            else
                            {
                                $comments_msit = addslashes($comments_msit);
                            }
                            $applicationDecisionSql .= "'".mysql_escape_string($comments_msit)."') on duplicate key update admission_status=".intval($admissionStatus_msit).", 
                            comments='".mysql_escape_string($comments_msit)."'";
                            mysql_query($applicationDecisionSql) or die(mysql_error().$applicationDecisionSql);
                        }    
                        else 
                        {
                            $applicationDecisionSql = "insert into application_decision 
                            (application_id, program_id, period_id, admission_status, comments)
                            values (".$appid.",".$currentMsitProgram.",".$currentPeriod.",".intval($admissionStatus_msit).", '";
                            if ($comments_msit == "") 
                            {
                                $comments_msit = NULL;
                            }
                            else
                            {
                                $comments_msit = addslashes($comments_msit);
                            }
                            $applicationDecisionSql .= mysql_escape_string($comments)."') on duplicate key update admission_status=NULL, comments='" .mysql_escape_string($comments_msit) . "'";
                            mysql_query($applicationDecisionSql) or die(mysql_error().$applicationDecisionSql);
                        }
                    }
                }
            }  
        }
       
    $includeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');
    
    if (isset($_POST['risk_factors_decision']) 
                || isset($_POST['mse_risk_factor_text_decision'])) 
    {
        // Set the risk factor values to 0 by default
        $risk_factor_language = 0;
        $risk_factor_experience = 0;
        $risk_factor_academic = 0;
        $risk_factor_other = 0;
        $risk_factor_text = "";
        
        // Set the value for each selected risk factor
        if ( isset($_POST['risk_factors_decision']) ) {
            foreach ($_POST['risk_factors_decision'] as $risk_factor) 
            {
                if ($risk_factor == "language") 
                {
                    $risk_factor_language = 1;
                } 
                elseif ($risk_factor == "experience") 
                {
                    $risk_factor_experience = 1;    
                } 
                elseif ($risk_factor == "academic") 
                {
                        $risk_factor_academic = 1;
                } 
                elseif ($risk_factor == "other") 
                {
                    $risk_factor_other = 1;    
                }            
            }
        }
        
        if (isset($_POST['mse_risk_factor_text_decision'])
            && $_POST['mse_risk_factor_text_decision']) {
            $risk_factor_text = $_POST['mse_risk_factor_text_decision'];    
        }
        
        $decisionProgramId = $_POST['mse_decision_program_id']; 
        
        // Include the DB classes
        include_once $includeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
        include_once $includeBase . '../classes/class.db_mse_risk_factors.php';
        
        // Instantiate db class
        $db_risk_factors = new DB_MseRiskFactors();
        
        // Update db        
        $db_risk_factors->updateRiskFactorsDecision($appid, $decisionProgramId, $risk_factor_language, 
            $risk_factor_experience, $risk_factor_academic, 
            $risk_factor_other, $risk_factor_text);
        
    } elseif (isset($_POST['mse_decision'])) {

        $decisionProgramId = $_POST['mse_decision_program_id'];
        
        // Include the DB classes
        include_once $includeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
        include_once $includeBase . '../classes/class.db_mse_risk_factors.php';
        
        // Instantiate db class
        $db_risk_factors = new DB_MseRiskFactors();
        
        // Check whether a risk factors record needs to be updated
        $risk_factors = $db_risk_factors->getRiskFactorsDecision($appid, $decisionProgramId);
        
        if ( count($risk_factors) > 0 ) 
        {
            // Update the record 
            $db_risk_factors->updateRiskFactors($appid, $decisionProgramId, 0, 0, 0, 0);       
        }     
          
    }
    if (isset($_POST['bridge_course_decision'])) {
        $db_bridge_courses->updateBridgeCourseDecision($appid, $decisionProgramId, $_POST['bridge_course_decision']);
    }
    
    // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    if (isset($_POST['letter_recs_decision'])) 
    {
        // Set the letter rec values to 0 by default
        $letter_rec_java = 0;
        $letter_rec_discrete = 0;
        $letter_rec_algorithms = 0;
        $letter_rec_data_structures = 0;
        
        // Set the value for each selected letter rec
        if ( isset($_POST['letter_recs_decision']) ) {
            foreach ($_POST['letter_recs_decision'] as $letter_rec) 
            {
                if ($letter_rec == "java") 
                {
                    $letter_rec_java = 1;
                } 
                elseif ($letter_rec == "discrete") 
                {
                    $letter_rec_discrete = 1;    
                } 
                
                elseif ($letter_rec == "algorithms") 
                {
                        $letter_rec_algorithms = 1;
                } 
                elseif ($letter_rec == "data_structures") 
                {
                        $letter_rec_data_structures = 1;
                }           
            }
        }
        
        
        $decisionProgramId = $_POST['mse_decision_program_id']; 
        
        // Include the DB classes
        include_once $includeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
        include_once $includeBase . '../classes/class.db_mse_letter_recs.php';
        
        // Instantiate db class
        $db_letter_recs = new DB_MseLetterRecs();
        
        // Update db        
        $db_letter_recs->updateLetterRecsDecision($appid, $decisionProgramId, $letter_rec_java, 
            $letter_rec_discrete, $letter_rec_algorithms, 
            $letter_rec_data_structures);
        
    } elseif (isset($_POST['mse_decision'])) {

        $decisionProgramId = $_POST['mse_decision_program_id'];
        
        // Include the DB classes
        include_once $includeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
        include_once $includeBase . '../classes/class.db_mse_letter_recs.php';
        
        // Instantiate db class
        $db_letter_recs = new DB_MseLetterRecs();
        
        // Check whether a risk factors record needs to be updated
        $letter_recs = $db_letter_recs->getLetterRecsDecision($appid, $decisionProgramId);
        
        if ( count($letter_recs) > 0 ) 
        {
            // Update the record 
            $db_letter_recs->updateLetterRecs($appid, $decisionProgramId, 0, 0, 0, 0);       
        }     
          
    }
    //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx    
    
        
    } // END if(isset($_POST["btnSubmitFinal"]))
    
    
} //END IF UID                                               $db_risk_factors->updateRiskFactorsDecision($appid, $decisionProgramId, $risk_factor_language

//POS REVIEW UPDATES HERE
if(isset($_POST['btnSubmit']))
{
    $doUpdate = false;

    //GET POSTED VARS
    /* AVAILABLE FIELDS
    background
    grades
    statement
    comments
    point
    pointCertainty
    point2
    point2Certainty
    private_comments
    round2
    touched
    admit_vote
    recruited
    grad_name
    pertinent_info
    advise_time
    commit_money
    fund_source
    */
    $background = "";
    $grades = "";
    $statement = "";
    $comments = "";
    $point = "NULL";
    $pointCertainty = "NULL";
    $point2 = "NULL";
    $point2Certainty = "NULL";
    $private_comments = "";
    $round2 = "";
    $round3 = "";
    $touched = 0;
    $admit_vote = "";
    $recruited = "";
    $grad_name = "";
    $pertinent_info = "";
    $advise_time = "";
    $commit_money = "";
    $fund_source = "";
    $interview = "";
    $brilliance = "";
    $other_interest = "";
    $facVote = 0;
    $rank = "NULL";
        
    if(isset($_POST['background']))
    {
        $background = addslashes(htmlspecialchars_decode($_POST['background']));
    }    
    if(isset($_POST['grades']))
    {
        $grades = addslashes(htmlspecialchars_decode($_POST['grades']));
    }    
    if(isset($_POST['statement']))
    {
        $statement = addslashes(htmlspecialchars_decode($_POST['statement']));
    }    
    if(isset($_POST['comments']))
    {
        $comments = addslashes(htmlspecialchars_decode($_POST['comments']));
    }    
    if(isset($_POST['point']))
    {
        if($_POST['point'] != "")
        {
            $point = addslashes(htmlspecialchars_decode($_POST['point']));
            if($thisDept == 3)
            {
                if($point != "")
                {
                    $point = number_format(floatval($point), 2);
                    if($point < 5)
                    {
                        $point = 5.00;
                    } 
                    if($point > 10)
                    {
                        $point = 10;
                    } 
                }
            }
            elseif($thisDept == 66)
            {
                if($point != "")
                {
                    $point = number_format(floatval($point), 1);
                    if($point < 5)
                    {
                        $point = 5.0;
                    } 
                    if($point > 10)
                    {
                        $point = 10;
                    } 
                }
            }
            elseif (isRiMscvDepartment($thisDept))
            {
                if($point != "")
                {
                    $point = number_format(floatval($point), 1);
                    if($point < 1)
                    {
                        $point = 1.0;
                    } 
                    if($point > 10)
                    {
                        $point = 10;
                    } 
                }    
            }
        }
    }    
    if(isset($_POST['pointCertainty']))
    {
        $pointCertainty = addslashes(htmlspecialchars_decode($_POST['pointCertainty']));
        $pointCertainty = floatval($pointCertainty);
    }    
    if(isset($_POST['point2']))
    {
        if($_POST['point2'] != "")
        {
            $point2 = addslashes(htmlspecialchars_decode($_POST['point2']));            
            // PLB added test for RI
            if($thisDept == 3)
            {
                if($point2 != "")
                {
                    $point2 = number_format(floatval($point2), 2);
                    if($point2 < 5)
                    {
                        $point2=5.00;
                    }
                    if($point2 > 10)
                    {
                        $point2=10;
                    }
                }
            } 
            elseif(($thisDept == 3 || $thisDept == 66)) 
            {
                if($point2 != "")
                {
                    $point2 = number_format(floatval($point2), 1);
                    if($point2 < 5)
                    {
                        $point2=5.0;
                    }
                    if($point2 > 10)
                    {
                        $point2=10;
                    }
                }
            } 
        }
    }    
    if(isset($_POST['point2Certainty']))
    {
        $point2Certainty = addslashes(htmlspecialchars_decode($_POST['point2Certainty']));
        $point2Certainty = floatval($point2Certainty);
    }    
    if(isset($_POST['private_comments']))
    {
        $private_comments = addslashes(htmlspecialchars_decode($_POST['private_comments']));
    }    
    if(isset($_POST['round2']))
    {
        $round2 = addslashes(htmlspecialchars_decode($_POST['round2']));
    }   
    if(isset($_POST['round3']))
    {
        $round3 = htmlspecialchars_decode($_POST['round3']);
        $round3 = addslashes($round3);
    } 
    if(isset($_POST['touched']))
    {
        $touched = intval($_POST['touched']);
    }    
    if(isset($_POST['admit_vote']))
    {
        $admit_vote = addslashes(htmlspecialchars_decode($_POST['admit_vote']));
    }    
    if(isset($_POST['recruited']))
    {
        $recruited = addslashes(htmlspecialchars_decode($_POST['recruited']));
    }    
    if(isset($_POST['grad_name']))
    {
        $grad_name = addslashes(htmlspecialchars_decode($_POST['grad_name']));
    }    
    if(isset($_POST['pertinent_info']))
    {
        $pertinent_info = addslashes(htmlspecialchars_decode($_POST['pertinent_info']));
    }    
    if(isset($_POST['advise_time']))
    {
        $advise_time = addslashes(htmlspecialchars_decode($_POST['advise_time']));
    }    
    if(isset($_POST['commit_money']))
    {
        $commit_money = addslashes(htmlspecialchars_decode($_POST['commit_money']));
    }    
    if(isset($_POST['fund_source']))
    {
        $fund_source = addslashes(htmlspecialchars_decode($_POST['fund_source']));
    }    
    if(isset($_POST['interview']))
    {
        $interview = addslashes(htmlspecialchars_decode($_POST['interview']));
    }
    if(isset($_POST['recommendations']))
    {
        $recommendations = addslashes(htmlspecialchars_decode($_POST['recommendations']));
    }
    if(isset($_POST['publications']))
    {
        $publications = addslashes(htmlspecialchars_decode($_POST['publications']));
    }
    if(isset($_POST['brilliance']))
    {
        $brilliance = addslashes(htmlspecialchars_decode($_POST['brilliance']));
    }
    if(isset($_POST['other_interest']))
    {
        $other_interest = addslashes(htmlspecialchars_decode($_POST['other_interest']));
    }
    if(isset($_POST['facVote']))
    {
        $facVote = intval($_POST['facVote']);
    }
    if(isset($_POST['rank']))
    {
        $rank = intval($_POST['rank']);
    }
    $vars = $_POST;
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if( strstr($key, 'seminars') !== false )
        {
            array_push($committeeReviewsSeminars, $arr[1]);
        }
    }//END FOR each key

    //first see if a record exists for the reviewer
    $reviewId = -1;
    $sql = "select id from review where reviewer_id = " . $reviewerId . " 
            and application_id = " . $appid . " and round = " . $round;
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        $reviewId = $row['id'];
        $doUpdate = true;
    }
    //if no record, insert one
    if($doUpdate == false)
    {
        $sql = "insert into review(
        application_id,
        reviewer_id,
        background,
        grades,
        statement,
        comments,
        point,
        point_certainty,
        point2,
        point2_certainty,
        private_comments,
        round2,
        round3,
        touched,
        admit_vote,
        recruited,
        grad_name,
        pertinent_info,
        advise_time,
        commit_money,
        fund_source,
        round,
        interview,
        recommendations,
        publications,
        brilliance,
        other_interest,
        fac_vote,
        committee_vote,
        rank,
        department_id
    )values(
        ".$appid.",
        ".$reviewerId.",
        '".$background."',
        '".$grades."',
        '".$statement."',
        '".$comments."',
        ".$point.",
        ".$pointCertainty.",
        ".$point2.",
        ".$point2Certainty.",
        '".$private_comments."',";

        if (isPsychologyDepartment($thisDept))
        {
            if($round2 == "")
            {
                $sql .= "NULL,";
            }
            else
            {
                $sql .= intval($round2).",";
            }
            
            if($round3 == "")
            {
                $sql .= "NULL,";
            }
            else
            {
                $sql .= intval($round3).",";
            }            
        }
        else
        {
            if($round2 == -1 || $round2 == "")
            {
                $sql .= "NULL,";
            }
            else
            {
                $sql .= intval($round2).",";
            }
            
            if($round3 == -1 || $round3 == "")
            {
                $sql .= "NULL,";
            }
            else
            {
                $sql .= intval($round3).",";
            }
        }
        
        $sql .= $touched.",
        '".$admit_vote."',
        ".intval($recruited).",
        '".$grad_name."',
        '".$pertinent_info."',
        '".$advise_time."',
        '".$commit_money."',
        '".$fund_source."',
        ".$round.",
        '".$interview."',
        '".$recommendations."',
        '".$publications."',
        '".$brilliance."',
        '".$other_interest."',
        ".$facVote.",
        ".$committeeMemberVote.",
        ".$rank.",
        ".$thisDept."
        )";
        if(intval($reviewerId) > 0)
        {
            mysql_query($sql) or die(mysql_error().$sql);
        }
        $reviewId = mysql_insert_id();
    }
    else
    {
        //if a record exists, update
        $sql = "update review set
        background='".$background."',
        grades='".$grades."',
        statement='".$statement."',
        comments='".$comments."',
        point=".$point.",
        point_certainty=".$pointCertainty.",
        point2=".$point2.",
        point2_certainty=".$point2Certainty.",
        private_comments='".$private_comments."',";
        
        if (isPsychologyDepartment($thisDept))
        {
            if($round2 == "")
            {
                $sql .= "round2=NULL,";
            }
            else
            {
                $sql .= "round2=".intval($round2).",";
            }
            
            if($round3 == "")
            {
                $sql .= "round3=NULL,";
            }
            else
            {
                $sql .= "round3=".intval($round3).",";
            }            
        }
        else
        {
            if($round2 == -1 || $round2 == "")
            {
                $sql .= "round2=NULL,";
            }
            else
            {
                $sql .= "round2=".intval($round2).",";
            }
            
            if($round3 == -1 || $round3 == "")
            {
                $sql .= "round3=NULL,";
            }
            else
            {
                $sql .= "round3=".intval($round3).",";
            }
        }

        $sql .= "touched=".$touched.",
        admit_vote='".$admit_vote."',
        recruited=".intval($recruited).",
        grad_name='".$grad_name."',
        pertinent_info='".$pertinent_info."',
        advise_time='".$advise_time."',
        commit_money='".$commit_money."',
        fund_source='".$fund_source."',
        interview='".$interview."',
        recommendations='".$recommendations."',
        publications='".$publications."',
        brilliance='".$brilliance."',
        other_interest='".$other_interest."',
        fac_vote=".$facVote.",
        committee_vote=".$committeeMemberVote.",
        rank=".$rank.",
        department_id = ".$thisDept."
        where supplemental_review IS NULL and reviewer_id =".$reviewerId." and application_id=".$appid . " and round=".$round;
        if(intval($reviewerId) > 0)
        {
            mysql_query($sql) or die(mysql_error().$sql);
        }
    }
    
    //HANDLE ADDITIONAL REVIEW ITEMS FOR AOI
    $sql = "delete from review where supplemental_review IS NOT NULL and reviewer_id =".$reviewerId." and application_id=".$appid . " and round=".$round;
    mysql_query($sql) or die(mysql_error().$sql);
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if(strstr($key, 'point_') !== false)
        {
            $tmpid = $arr[1];
            $itemId = $arr[0];
            $pv = $_POST["point_".$arr[1]];
            $pvc = "NULL";
            if(isset($_POST["pointCertainty_".$arr[1]]))
            {
                $pvc = $_POST["pointCertainty_".$arr[1]];
            }
            if(isset($_POST["facVote"]))
            {
                $facVote = $_POST["facVote"];
            }
            if($arr[0] == "point")
            {
                $sql = "insert into review(
                application_id,
                reviewer_id,
                point,
                point_certainty,
                round,
                supplemental_review,
                fac_vote,
                committee_vote,
                department_id
            )values(
                ".$appid.",
                ".$reviewerId.",
                ".floatval($pv).",
                ".floatval($pvc).",
                ".$round.",
                ".intval($arr[1]).",
                ".$facVote.",
                ".$committeeMemberVote.",
                ".$thisDept."
                )";
                if(intval($reviewerId) > 0)
                {
                    mysql_query($sql) or die(mysql_error().$sql);
                }
            }
        }
    }//END FOR

    //HANDLE LTI ADDITIONAL INTERESTS
    if($thisDept == 6 || $thisDept == 1 || $thisDept == 83)
    {
        $lu_review_interest_id = -1;
        $sql = "select id from lu_review_interest where review_id=". $reviewId;

        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result ))
        {
            $lu_review_interest_id = $row['id'];
        }
        if($lu_review_interest_id > -1)
        {
            $sql = "delete from lu_review_interest where review_id=". $reviewId;
            mysql_query($sql) or die(mysql_error());
        }
        for($i = 0; $i < count($committeeReviewsSeminars);$i++)
        {
            /*
            * Guess what? $committeeReviewsSeminars is not a 2D array!!!! 
            */
            //$sql = "insert into lu_review_interest(review_id, program_id) values(".$reviewId.",".$committeeReviewsSeminars[$i][0].")";
            $sql = "insert into lu_review_interest(review_id, program_id) values(".$reviewId.",".$committeeReviewsSeminars[$i].")";
            if(intval($reviewerId) > 0)
            {
                mysql_query($sql) or die(mysql_error().$sql);
            }
        }

    }
    
    // PLB added mse_risk_factors 01/22/09
    $includeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');

    if (isset($_POST['risk_factors']) || isset($_POST['mse_risk_factor_text'])) {
    
        // Set the risk factor values to 0 by default
        $risk_factor_language = 0;
        $risk_factor_experience = 0;
        $risk_factor_academic = 0;
        $risk_factor_other = 0;
        $risk_factor_text = "";
        
        // Set the value for each selected risk factor
        if (isset($_POST['risk_factors'])) {
            foreach ($_POST['risk_factors'] as $risk_factor) 
            {
                if ($risk_factor == "language") 
                {
                    $risk_factor_language = 1;
                } 
                elseif ($risk_factor == "experience") 
                {
                    $risk_factor_experience = 1;    
                } 
                elseif ($risk_factor == "academic") 
                {
                        $risk_factor_academic = 1;
                } 
                elseif ($risk_factor == "other") 
                {
                    $risk_factor_other = 1;    
                }            
            }
        }
        
        if (isset($_POST['mse_risk_factor_text'])
            && $_POST['mse_risk_factor_text']) 
        {
            $risk_factor_text = $_POST['mse_risk_factor_text'];    
        }
        
        // Include the DB classes
        include_once $includeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
        include_once $includeBase . '../classes/class.db_mse_risk_factors.php';
        
        // Instantiate db class
        $db_risk_factors = new DB_MseRiskFactors();
        
        // Update db        
        $db_risk_factors->updateRiskFactors($appid, $reviewerId, $risk_factor_language, 
            $risk_factor_experience, $risk_factor_academic, $risk_factor_other, $risk_factor_text);
            
        if (isMseESE($thisDept)) {       
            if (isset($_POST['bridge_course_rad'])) {
        
            // Set the risk factor values to 0 by default
                $bridge_course_id  = $_POST['bridge_course_rad'];
                    
                include_once $includeBase . '../classes/class.db_mse_bridge_course.php';
            
                // Instantiate db class
                $db_bridge_courses = new DB_MseBridgeCourse();
                
                // Update db        
                $db_bridge_courses->updateBridgeCourse($appid, $reviewerId, $bridge_course_id);
                }
            }
    } elseif (isset($_POST['mse_review'])) {
    
        // Include the DB classes
        include_once $includeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
        include_once $includeBase . '../classes/class.db_mse_risk_factors.php';
        
        // Instantiate db class
        $db_risk_factors = new DB_MseRiskFactors();
        
        // Check whether a risk factors record needs to be updated
        $risk_factors = $db_risk_factors->getRiskFactors($appid, $reviewerId);
        
        if ( count($risk_factors) > 0 ) 
        {
            // Update the record 
            $db_risk_factors->updateRiskFactors($appid, $reviewerId, 0, 0, 0, 0);       
        }  
    }
    
    /*
    * MSE interview, aqa-related fields 
    */
    if (isset($_POST['mse_review'])) {

        $interviewType = filter_input(INPUT_POST, 'interview_type', FILTER_VALIDATE_INT);
        $interviewTypeText = filter_input(INPUT_POST, 'interview_type_text', FILTER_SANITIZE_STRING);
        $interviewDate = filter_input(INPUT_POST, 'interview_date', FILTER_SANITIZE_STRING);
        $interviewComments = filter_input(INPUT_POST, 'interview_comments', FILTER_SANITIZE_STRING);
    
        $mseEnglishComments = filter_input(INPUT_POST, 'mse_english_comments', FILTER_SANITIZE_STRING);
        $mseEnglishRating = filter_input(INPUT_POST, 'mse_english_rating', FILTER_VALIDATE_INT);
    
        $mseProgrammingComments = filter_input(INPUT_POST, 'mse_programming_comments', FILTER_SANITIZE_STRING);
        $mseProgrammingRating = filter_input(INPUT_POST, 'mse_programming_rating', FILTER_VALIDATE_INT);
    
        $mseFoundationalComments = filter_input(INPUT_POST, 'mse_foundational_comments', FILTER_SANITIZE_STRING);
        $mseFoundationalRating = filter_input(INPUT_POST, 'mse_foundational_rating', FILTER_VALIDATE_INT);
        
        $mseMaturityComments = filter_input(INPUT_POST, 'mse_maturity_comments', FILTER_SANITIZE_STRING);
        $mseMaturityRating = filter_input(INPUT_POST, 'mse_maturity_rating', FILTER_VALIDATE_INT);
        
        $mseUnderstandingComments = filter_input(INPUT_POST, 'mse_understanding_comments', FILTER_SANITIZE_STRING);
        $mseUnderstandingRating = filter_input(INPUT_POST, 'mse_understanding_rating', FILTER_VALIDATE_INT);
    
        $mseExperienceComments = filter_input(INPUT_POST, 'mse_experience_comments', FILTER_SANITIZE_STRING);
        $mseExperienceRating = filter_input(INPUT_POST, 'mse_experience_rating', FILTER_VALIDATE_INT);
    
        if ($interviewType || $interviewTypeText || $interviewDate || $interviewComments) {
    
            $interviewQuery = "INSERT INTO mse_interview VALUES (
                " . intval($appid) . ",
                " . intval($reviewerId) . ",
                '" . mysql_real_escape_string($interviewDate) . "',
                " . intval($interviewType) . ",
                '" . mysql_real_escape_string($interviewTypeText) . "',
                '" . mysql_real_escape_string($interviewComments) . "'
                ) ON DUPLICATE KEY UPDATE 
                interview_date = '" . mysql_real_escape_string($interviewDate) . "',
                interview_type = " . intval($interviewType) . ",
                interview_type_other = '" . mysql_real_escape_string($interviewTypeText) . "',
                interview_comments = '" . mysql_real_escape_string($interviewComments) . "'";
                
            mysql_query($interviewQuery);

        }
        
        if ($mseEnglishComments || $mseEnglishRating != NULL
            || $mseProgrammingComments || $mseProgrammingRating != NULL
            || $mseFoundationalComments || $mseFoundationalRating != NULL
            || $mseMaturityComments || $mseMaturityRating != NULL
            || $mseUnderstandingComments || $mseUnderstandingRating != NULL
            || $mseExperienceComments || $mseExperienceRating != NULL) 
        {
            $mseAqaQuery = "INSERT INTO mse_aqa 
                VALUES (
                " . intval($appid) . ",
                " . intval($reviewerId) . ",
                '" . mysql_real_escape_string($mseEnglishComments) . "',
                '" . mysql_real_escape_string($mseProgrammingComments) . "',
                '" . mysql_real_escape_string($mseFoundationalComments) . "',
                '" . mysql_real_escape_string($mseMaturityComments) . "',
                '" . mysql_real_escape_string($mseUnderstandingComments) . "',
                '" . mysql_real_escape_string($mseExperienceComments) . "',
                " . intval($mseEnglishRating) . ",
                " . intval($mseProgrammingRating) . ",
                " . intval($mseFoundationalRating) . ",
                " . intval($mseMaturityRating) . ",
                " . intval($mseUnderstandingRating) . ",
                " . intval($mseExperienceRating) . "
                ) ON DUPLICATE KEY UPDATE 
                english_comments = '" . mysql_real_escape_string($mseEnglishComments) . "',
                programming_comments = '" . mysql_real_escape_string($mseProgrammingComments) . "',
                foundational_comments = '" . mysql_real_escape_string($mseFoundationalComments) . "',
                maturity_comments = '" . mysql_real_escape_string($mseMaturityComments) . "',
                understanding_comments = '" . mysql_real_escape_string($mseUnderstandingComments) . "',
                experience_comments = '" . mysql_real_escape_string($mseExperienceComments) . "',
                english_rating = " . intval($mseEnglishRating) . ",
                programming_rating = " . intval($mseProgrammingRating) . ",
                foundational_rating = " . intval($mseFoundationalRating) . ",
                maturity_rating = " . intval($mseMaturityRating) . ",
                understanding_rating = " . intval($mseUnderstandingRating) . ",
                experience_rating = " . intval($mseExperienceRating);
                
            mysql_query($mseAqaQuery);

        }
    } 
    
    /*
    * Handle risk, positive factors. 
    */
    if ($reviewId != -1) {
        
        $clearRiskFactorQuery = "DELETE FROM review_risk_factor WHERE review_id = " . $reviewId;
        mysql_query($clearRiskFactorQuery); 
        foreach ($allRiskFactorVals as $riskFactorVal) {
            $inputName = 'riskFactors_' . $riskFactorVal;
            if ( isset($_POST[$inputName]) ) {
                $insertRiskFactorQuery = "INSERT INTO review_risk_factor VALUES (
                                            " . $reviewId . ",
                                            " . $riskFactorVal . "
                                            )";    
                mysql_query($insertRiskFactorQuery);
            }
        }
        
        $clearPositiveFactorQuery = "DELETE FROM review_positive_factor WHERE review_id = " . $reviewId;
        mysql_query($clearPositiveFactorQuery); 
        foreach ($allPositiveFactorVals as $positiveFactorVal) {
            $inputName = 'positiveFactors_' . $positiveFactorVal;
            if ( isset($_POST[$inputName]) ) {
                $insertPositiveFactorQuery = "INSERT INTO review_positive_factor VALUES (
                                            " . $reviewId . ",
                                            " . $positiveFactorVal . "
                                            )";    
                mysql_query($insertPositiveFactorQuery);
            }
        }
        
        if ( isset($_POST['positiveFactorOther']) && $_POST['positiveFactorOther'] != '') {
            $positiveFactorOtherQuery = "INSERT INTO review_positive_factor_other VALUES (
                                            " . $reviewId . ",
                                            '" . mysql_real_escape_string($_POST['positiveFactorOther']) . "'
                                            )
                                            ON DUPLICATE KEY UPDATE
                                            positive_factor_other = '" 
                                            . mysql_real_escape_string($_POST['positiveFactorOther']) . "'";
        } else {
            $positiveFactorOtherQuery = "DELETE FROM review_positive_factor_other
                                            WHERE review_id = " . $reviewId;
        }   
        mysql_query($positiveFactorOtherQuery);
    }
}
//END UPDATE REVIEW

//GET REVIEWS
/*
* PLB added test for all comments vs. reviewer's comments only, 12/8/09. 
*/
if ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 10) {
    $allReviewCommentsOn = 1;    
} else {
    $allReviewCommentsOn = 1;     
    /*
    if ($thisDept == 6) {
        $allReviewCommentsOn = 0;
        $reviewCommentsOnQuery = "SELECT all_comments_on
                                FROM application
                                LEFT OUTER JOIN period_application 
                                ON application.id = period_application.application_id
                                LEFT OUTER JOIN department_review_config 
                                ON period_application.period_id = department_review_config.period_id
                                WHERE department_review_config.department_id = " . $thisDept . "
                                AND application.id = " . $appid;
        $reviewCommentsOnResult = mysql_query($reviewCommentsOnQuery) or die(mysql_error());
        while ($row = mysql_fetch_array($reviewCommentsOnResult)) {
            $allReviewCommentsOn = $row['all_comments_on'];        
        }
    }
    */ 
}

$sql = "select
review.id,
review.application_id,
users.firstname,
users.lastname,
review.reviewer_id,
lu_user_department.department_id,
GROUP_CONCAT(revgroup.name) as groups,
review.background,
review.grades,
review.statement,
review.comments,";
if ($thisDept == 66) {
    $sql .= "ROUND(review.point, 1) AS point,
    ROUND(review.point_certainty, 1) AS point_certainty,
    ROUND(review.point2, 1) AS point2,
    ROUND(review.point2_certainty, 1) AS point2_certainty,";    
} elseif ($thisDept == 3) {
    $sql .= "ROUND(review.point, 2) AS point,
    ROUND(review.point_certainty, 1) AS point_certainty,
    ROUND(review.point2, 2) AS point2,
    ROUND(review.point2_certainty, 1) AS point2_certainty,";    
} else {
    $sql .= "review.point,
    review.point_certainty,
    review.point2,
    review.point2_certainty,";
}
if (isPsychologyDepartment($thisDept))
{
    $sql .= "review.private_comments,
    review.round2,
    review.round3,";   
}
else
{
    $sql .= "review.private_comments,
    /* PLB hack for no selection default */
    ifnull(review.round2, -1) AS round2,
    ifnull(review.round3, -1) AS round3,";    
}
$sql .= "review.touched,
review.admit_vote,
review.recruited,
review.grad_name,
review.pertinent_info,
review.advise_time,
review.commit_money,
review.fund_source,
lu_users_usertypes.usertype_id,
review.round,
review.interview,
review.recommendations,
review.publications,
review.brilliance,
review.other_interest,
review.supplemental_review,
review.fac_vote,
review.rank,
review_positive_factor_other.positive_factor_other,
mse_interview.interview_date,
mse_interview.interview_type,
mse_interview.interview_type_other,
mse_interview.interview_comments,
mse_bridge_course.course_id,
mse_aqa.english_comments,
mse_aqa.english_rating,
mse_aqa.programming_comments,
mse_aqa.programming_rating,
mse_aqa.foundational_comments,
mse_aqa.foundational_rating,
mse_aqa.maturity_comments,
mse_aqa.maturity_rating,
mse_aqa.understanding_comments,
mse_aqa.understanding_rating,
mse_aqa.experience_comments,
mse_aqa.experience_rating
from review
left outer join lu_users_usertypes on lu_users_usertypes.id = review.reviewer_id
left outer join users on users.id = lu_users_usertypes.user_id
left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = review.reviewer_id
left outer join revgroup on revgroup.id = lu_reviewer_groups.group_id
left outer join lu_user_department on lu_user_department.user_id = review.reviewer_id
LEFT OUTER JOIN review_positive_factor_other ON review.id = review_positive_factor_other.review_id
LEFT OUTER JOIN mse_interview 
    ON review.application_id = mse_interview.application_id
    AND review.reviewer_id = mse_interview.reviewer_id
LEFT OUTER JOIN mse_bridge_course 
    ON review.application_id = mse_bridge_course.application_id
    AND review.reviewer_id = mse_bridge_course.reviewer_id
LEFT OUTER JOIN mse_aqa 
    ON review.application_id = mse_aqa.application_id
    AND review.reviewer_id = mse_aqa.reviewer_id
where review.application_id=".$appid ;
if($_SESSION['A_usertypeid'] != 0)
{ 
    //$sql .= " and (lu_user_department.department_id = ".$thisDept. "  ) ";
}
if($round == 1)
{
    $sql . " and review.round=".$round;
}
$sql .=" group by review.id";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{    
    // PLB added test to add only reviewer's comments
    // when view all comments is toggled off 12/8/09.
    if ( !$allReviewCommentsOn && ($row['reviewer_id'] != $reviewerId) ) {
        continue;
    }
    
    $arr = array();
    array_push($arr,$row['id']);
    array_push($arr,$row['application_id']);
    array_push($arr,$row['firstname']);
    array_push($arr,$row['lastname']);
    array_push($arr,$row['reviewer_id']);
    array_push($arr,$row['groups']);        // 5
    array_push($arr,$row['background']);
    array_push($arr,$row['grades']);
    array_push($arr,$row['statement']);
    array_push($arr,$row['comments']);
    array_push($arr,$row['point']);         //10
    array_push($arr,$row['point2']);
    array_push($arr,$row['private_comments']);
    array_push($arr,$row['round2']);
    array_push($arr,$row['touched']);
    array_push($arr,$row['admit_vote']);    // 15
    array_push($arr,$row['recruited']);
    array_push($arr,$row['grad_name']);
    array_push($arr,$row['pertinent_info']);
    array_push($arr,$row['advise_time']);
    array_push($arr,$row['commit_money']);  //20
    array_push($arr,$row['fund_source']);
    array_push($arr,$row['usertype_id']);
    array_push($arr,$row['round']);
    array_push($arr,$row['interview']);
    array_push($arr,$row['brilliance']);    //25
    array_push($arr,$row['other_interest']);
    array_push($arr,$row['point_certainty']);
    array_push($arr,$row['point2_certainty']);
    array_push($arr,$row['supplemental_review']);
    array_push($arr,$row['fac_vote']);      //30
    array_push($arr,$row['rank']);
    array_push($arr,$row['department_id']);
    array_push($arr, $row['positive_factor_other']);
    array_push($arr, $row['interview_date']); //34
    array_push($arr, $row['interview_type']);
    array_push($arr, $row['interview_type_other']);
    array_push($arr, $row['interview_comments']);
    array_push($arr, $row['course_id']);
    array_push($arr, $row['round3']);   // 39
    array_push($arr, $row['recommendations']);   // 40
    array_push($arr, $row['publications']);
    array_push($arr, $row['english_comments']);
    array_push($arr, $row['english_rating']);
    array_push($arr, $row['programming_comments']);
    array_push($arr, $row['programming_rating']);   // 45
    array_push($arr, $row['foundational_comments']);
    array_push($arr, $row['foundational_rating']);
    array_push($arr, $row['maturity_comments']);
    array_push($arr, $row['maturity_rating']);
    array_push($arr, $row['understanding_comments']); // 50
    array_push($arr, $row['understanding_rating']);
    array_push($arr, $row['experience_comments']);
    array_push($arr, $row['experience_rating']);
    array_push($committeeReviews, $arr);
}

$riskFactors = getRiskFactors($appid, $_SESSION['A_userid']);
$positiveFactors = getPositiveFactors($appid, $_SESSION['A_userid']);

function getRiskFactors($applicationId, $reviewerLuuId) {

    $riskFactors = array();
    
    $riskFactorQuery = "SELECT review_risk_factor.*
                        FROM review
                        INNER JOIN review_risk_factor
                            ON review.id = review_risk_factor.review_id
                        WHERE review.application_id = " . $applicationId;
    $riskFactorQuery .= " AND review.reviewer_id = " . $reviewerLuuId;
    $riskFactorResult = mysql_query($riskFactorQuery);
    while( $row = mysql_fetch_array($riskFactorResult) ) {
        $riskFactors[] = $row['risk_factor_id']; 
    }
    
    return $riskFactors;
}

function getPositiveFactors($applicationId, $reviewerLuuId) {

    $positiveFactors = array();
    
    $positiveFactorQuery = "SELECT review_positive_factor.*
                        FROM review
                        INNER JOIN review_positive_factor
                            ON review.id = review_positive_factor.review_id
                        WHERE review.application_id = " . $applicationId;
    $positiveFactorQuery .= " AND review.reviewer_id = " . $reviewerLuuId;
    $positiveFactorResult = mysql_query($positiveFactorQuery);
    while( $row = mysql_fetch_array($positiveFactorResult) ) {
        $positiveFactors[] = $row['positive_factor_id']; 
    }
    
    return $positiveFactors;
}
                    
function getReviewSeminars($reviewId)
{
    $ret = array();
    $sql = "select id, program_id from lu_review_interest where review_id=".$reviewId;
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['program_id']);
        array_push($ret, $arr);
    }
    return $ret;
}

function getMyPrograms2($appid) {
    
    $sql = "SELECT programs.id,
    degree.name as degreename,
    fieldsofstudy.name as fieldname,
    choice,
    lu_application_programs.id as itemid,
    programs.programprice,
    programs.baseprice,
    programs.linkword,
    domain.name as domainname,
    lu_application_programs.decision,
    lu_application_programs.admission_status,
    lu_application_programs.admit,
    lu_application_programs.admit_comments,
    group_concat(distinct lu_programs_departments.department_id) as department_id,
    lu_application_programs.faccontact,
    lu_application_programs.stucontact
    FROM lu_application_programs
    inner join programs on programs.id = lu_application_programs.program_id
    inner join degree on degree.id = programs.degree_id
    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
    left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
    left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
    left outer join domain on domain.id = lu_domain_department.domain_id
    where lu_application_programs.application_id = ".$appid." group by programs.id order by choice";
    $result = mysql_query($sql);
    
    $myPrograms2 = array();
    while($row = mysql_fetch_array( $result ))
    {
        $myPrograms2[] = array(
            $row[0],
            $row['degreename'] . " " . $row['linkword'] . " " . $row['fieldname']
        );    
    }    
    
    return $myPrograms2;
}



/*
* PLB added test for robotics display of phd & ms score inputs 11/9/09 
*/
if ($thisDept == 3 || $thisDept == 66 || $thisDept == 92 || $thisDept = 50) 
{
    $myPrograms2 = getMyPrograms2($appid);
    $scorePrograms = array();
    $showPhdInput = $showMsInput = FALSE;
    
    if ($round == 1) {
        foreach($myPrograms2 as $myProgram) {
            if ( Department::hasProgram($thisDept, $myProgram[0]) ) {
                $scorePrograms[$myProgram[0]] = $myProgram[1];    
            }
        }
    } else {
        
        /*
        $promoteQuery = "SELECT program_id FROM lu_application_programs 
                            WHERE round2 = 1 AND application_id = " . $appid;
        */
        $promoteQuery = "SELECT promotion_history.program_id, 
                            promotion_history.round, 
                            promotion_history.promotion_method
                            FROM promotion_history
                            INNER JOIN lu_programs_departments
                                ON promotion_history.program_id = lu_programs_departments.program_id
                            INNER JOIN (
                              SELECT application_id, program_id, 
                              MAX(status_time) AS latest_status
                              FROM promotion_history
                              WHERE application_id = " . $appid . "
                              GROUP BY application_id, program_id
                            ) AS program_status
                                  ON promotion_history.application_id = program_status.application_id
                                  AND promotion_history.program_id = program_status.program_id
                            WHERE promotion_history.application_id = " . $appid . "
                            AND lu_programs_departments.department_id = " . $thisDept . "
                            AND promotion_history.status_time = program_status.latest_status";

        $promoteResult = mysql_query($promoteQuery);
        while ($row = mysql_fetch_array($promoteResult)) {
            if ( $row['round'] != 2 && $row['promotion_method'] != 'promotion' ) {
                // This is not a promotion record.
                continue;
            }
            $scoreProgramId = $row[0];
            foreach($myPrograms2 as $myProgram) {
                if ($myProgram[0] == $scoreProgramId) {
                    $scorePrograms[$myProgram[0]] = $myProgram[1];
                }
            }
        }   
    }
    
    foreach ($scorePrograms as $scoreProgramId => $scoreProgram) {
        if ($scoreProgramId == 4 || $scoreProgramId == 17 || $scoreProgramId == 100029) {
            $showPhdInput = TRUE;    
        } elseif ($scoreProgramId == 12 || 100007) {
            $showMsInput = TRUE;
        }        
    }
   
} else {
    $showPhdInput = $showMsInput = TRUE;    
}

// disable semi-blind control for MSE-MSIT departments
$includeSemiblind = TRUE;
if ( isMseMsitDepartment($thisDept) ) 
{
    if ($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1) {
        // Disable for everyone but su and admin
        $includeSemiblind = FALSE;    
    }          
}

/*
* New: check to disable ability to review same app in multiple rounds.
* DO NOT DISABLE FOR CSD MS 
*/
$previousRoundData = FALSE;
$mseRiskFactors = array();
for($i = 0; $i < count($committeeReviews); $i++) {
    if (($thisDept != 74 // CSD MS must match round
            || ($thisDept == 74 && $committeeReviews[$i][23] == $round) ) 
        && $committeeReviews[$i][32]== $thisDept
        && $committeeReviews[$i][30] == 0   // not faculty review 
        && ($committeeReviews[$i][29] == ""  // not supplemental review
            || $committeeReviews[$i][29] == NULL)
        && $committeeReviews[$i][4] == $reviewerId
        && ( $committeeReviews[$i][22] != 1  // Coordinator review matching round
            ||  ($committeeReviews[$i][22] == 1 && $committeeReviews[$i][23] == $round) ) )
    {
        $comments = $committeeReviews[$i][9];
        $private_comments = $committeeReviews[$i][12];
        $background = $committeeReviews[$i][6];
        $committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
        $other_interest = $committeeReviews[$i][26];
        $statement = $committeeReviews[$i][8];
        $grades = $committeeReviews[$i][7];
        $brilliance = $committeeReviews[$i][25];
        $committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
        $point = $committeeReviews[$i][10];
        $point2 = $committeeReviews[$i][11];
        $pointCertainty = $committeeReviews[$i][27];
        $pertinent_info = $committeeReviews[$i][18];
        $grad_name = $committeeReviews[$i][17];
        $positiveFactorOther = $committeeReviews[$i][33];
        $mseRiskFactors = $db_risk_factors->getRiskFactors($appid, $committeeReviews[$i][4]);
        $recommendations = $committeeReviews[$i][40];
        $publications = $committeeReviews[$i][41];

        if ($thisDept != 74 // not CSD MS
            && $committeeReviews[$i][23] != $round 
            && $committeeReviews[$i][22] != 1 // not a coordinator review
            && !$showDecision) 
        {
            $previousRoundData = TRUE;
            $reviewRound = $committeeReviews[$i][23];
            $allowEdit = FALSE;
        }
    } 
}

function getAdmissionStatusValue($applicationId, $applyProgramId, $multivalue = FALSE) {
    
    $admissionStatusValue = NULL;
       
    $query = "SELECT * FROM application_decision
                WHERE application_id = " . $applicationId . "
                AND program_id = " . $applyProgramId;
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $admissionStatusValue = $row['admission_status']; 
        if ( $multivalue && ($admissionStatusValue > 0) ) {
            $admissionStatusValue .= '_' . $row['admission_program_id'];    
        }       
    }
    
    return $admissionStatusValue;
}

function getReviewerInfo($reviewerId) {
    
    $query = "SELECT CONCAT(users.firstname, ' ', users.lastname) AS reviewer_name,
                department.name AS department_name
                FROM users
                INNER JOIN lu_users_usertypes 
                    ON users.id = lu_users_usertypes.user_id
                LEFT OUTER JOIN lu_user_department 
                    ON lu_users_usertypes.id = lu_user_department.user_id
                LEFT OUTER JOIN department
                    ON lu_user_department.department_id = department.id 
                WHERE lu_users_usertypes.id = " . $reviewerId;
    $result = mysql_query($query);            
    
    $reviewerInfo = array();
    while ($row = mysql_fetch_array($result)) {
        $reviewerInfo[] = $row;    
    }
    
    return $reviewerInfo;
}

function getCoordinatorComments($applicationId, $departmentId) {
    
    $coordinatorComments = array();
    
    $query = "SELECT review.comments 
            FROM review
            INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
            WHERE review.application_id = " . intval($applicationId) . " 
            AND lu_users_usertypes.usertype_id = 1
            AND department_id = " . intval($departmentId);
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $coordinatorComments[] = $row['comments'];
    }
    
    return $coordinatorComments;  
}

function saveFinalDesignMasters()
{
    global $appid;
    $applicationPrograms = getApplicationPrograms($appid);
    $previousAdmissionStatuses = getDesignMastersAdmissionStatuses($appid);
    $periodId = getApplicationPeriodId($appid);
    
    $admissionProgram1 = filter_input(INPUT_POST, 'admissionProgram1', FILTER_VALIDATE_INT);
    $admissionStatus1 = filter_input(INPUT_POST, 'admissionStatus1', FILTER_VALIDATE_INT);
    $admissionProgram2 = filter_input(INPUT_POST, 'admissionProgram2', FILTER_VALIDATE_INT);
    $admissionStatus2 = filter_input(INPUT_POST, 'admissionStatus2', FILTER_VALIDATE_INT);
    $comments = filter_input(INPUT_POST, 'comments', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $faccontact = filter_input(INPUT_POST, 'faccontact', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $stucontact = filter_input(INPUT_POST, 'stucontact', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    
    $scholarshipAmt = NULL;
    $scholarshipComments = NULL;
    
    // Update the lu_application_program and admission
    $matchedAdmissionProgramIds = array();
    foreach($applicationPrograms as $applicationProgram)
    {
        // Determine the admission status for the applicant-selected program
        $admissionProgram = $applicationProgram['program_id'];
        $admissionStatus = 'NULL';
        
        if ($applicationProgram['program_id'] == $admissionProgram1)
        {
            $admissionProgram = $admissionProgram1;
            $matchedAdmissionProgramIds[] = $admissionProgram1; 
            if ($admissionStatus1 != 3)
            {
                $admissionStatus = $admissionStatus1; 
            }
        }
        elseif ($applicationProgram['program_id'] == $admissionProgram2)
        {
            $admissionProgram = $admissionProgram2;
            $matchedAdmissionProgramIds[] = $admissionProgram2;
            if ($admissionStatus2 != 3)
            {
                $admissionStatus = $admissionStatus2;
            }  
        }
        else
        {
            if ($applicationProgram['choice'] == 1)
            {
                if ($admissionProgram1)
                {
                    $admissionProgram = $admissionProgram1;
                    $matchedAdmissionProgramIds[] = $admissionProgram1;
                    if ($admissionStatus1 != 3)
                    {
                        $admissionStatus = $admissionStatus1; 
                    }        
                }
                elseif ($admissionProgram2)
                {
                    $admissionProgram = $admissionProgram2;
                    $matchedAdmissionProgramIds[] = $admissionProgram2;
                    if ($admissionStatus2 != 3)
                    {
                        $admissionStatus = $admissionStatus2;
                    }    
                }    
            }
            else
            {
                if ($admissionProgram2 && !in_array($admissionProgram2, $matchedAdmissionProgramIds))
                {
                    $admissionProgram = $admissionProgram2;
                    $matchedAdmissionProgramIds[] = $admissionProgram2;
                    if ($admissionStatus2 != 3)
                    {
                        $admissionStatus = $admissionStatus2;
                    }        
                }
                elseif ($admissionProgram1 && !in_array($admissionProgram1, $matchedAdmissionProgramIds))
                {
                    $admissionProgram = $admissionProgram1;
                    $matchedAdmissionProgramIds[] = $admissionProgram1;
                    if ($admissionStatus1 != 3)
                    {
                        $admissionStatus = $admissionStatus1;
                    }    
                }
            }            
        }

        $luapUpdateQuery = "UPDATE lu_application_programs SET
            admission_status = " . $admissionStatus . ",
            admit_comments = '" . mysql_escape_string($comments) . "',
            scholarship_amt = " . ($scholarshipAmt ? round(floatval($scholarshipAmt), 2) : 'NULL') . ",
            scholarship_comments = '" . mysql_escape_string($scholarshipComments) . "'
            WHERE id = " . $applicationProgram['id'];
            
        mysql_query($luapUpdateQuery);
            
        $adUpdateQuery = "INSERT INTO application_decision
            (application_id, program_id, period_id, admission_program_id, admission_status, comments) 
            VALUES ("
            . $appid . ","    
            . $applicationProgram['program_id'] . ","
            . $periodId . ","
            . $admissionProgram . ","
            . $admissionStatus . ",'"
            . mysql_escape_string($comments) . "')
            ON DUPLICATE KEY UPDATE
            admission_status = " . $admissionStatus . ",
            admission_program_id = " . $admissionProgram . ",
            comments = '" . mysql_escape_string($comments) . "'";
            
        mysql_query($adUpdateQuery);
    }
    
    // Insert/update design admission status records
    if ($admissionProgram1)
    {
        if ($admissionStatus1 == 3)
        {
            $admissionStatus1 = 'NULL';    
        }
        
        $insertQuery1 = "INSERT INTO application_decision_design
            (application_id, admission_program_id, choice, admission_status, timestamp) 
            VALUES(" 
            . $appid . ","    
            . $admissionProgram1 . ", 1,"
            . $admissionStatus1 . ",
            NOW())
            ON DUPLICATE KEY UPDATE
            choice = 1,
            admission_status = " . $admissionStatus1 . ",
            timestamp = NOW()";
            
        mysql_query($insertQuery1);
    }
    
    if ($admissionProgram2)
    {
        if ($admissionStatus2 == 3)
        {
            $admissionStatus2 = 'NULL';    
        }
        
        $insertQuery2 = "INSERT INTO application_decision_design
            (application_id, admission_program_id, choice, admission_status) 
            VALUES (" 
            . $appid . ","    
            . $admissionProgram2 . ", 2,"
            . $admissionStatus2 . ")
            ON DUPLICATE KEY UPDATE
            choice = 2,
            admission_status = " . $admissionStatus2;;
            
        mysql_query($insertQuery2);   
    } 
    
    // Delete orphan design admission status records
    foreach($previousAdmissionStatuses as $previousAdmissionStatus)
    {
        if ($previousAdmissionStatus['admission_program_id'] != $admissionProgram1
            && $previousAdmissionStatus['admission_program_id'] != $admissionProgram2)
        {
            $deleteQuery = "DELETE FROM application_decision_design
                WHERE application_id = " . intval($appid) . "
                AND admission_program_id = " . $previousAdmissionStatus['admission_program_id'];
                
            mysql_query($deleteQuery);
        }    
    }

    /*
    * Update assistantship
    */
    $assistantshipUpdateQuery = "UPDATE assistantship SET granted = ";
    if (isset($_POST['assistantshipGranted']))
    {
        $assistantshipUpdateQuery .= " 1";    
    }
    else
    {
        $assistantshipUpdateQuery .= " 0";
    }
    $assistantshipUpdateQuery .= " WHERE application_id = " . $appid;
    
    mysql_query($assistantshipUpdateQuery);
}

function getApplicationPeriodId($applicationId)
{
    $periodId = NULL;
    
    $query = "SELECT period_id FROM period_application
                WHERE application_id = " . intval($applicationId);
    $result = mysql_query($query);
    
    while($row = mysql_fetch_array($result ))
    {
        $periodId = $row["period_id"];
    }    
    
    return $periodId;
}

function getApplicationPrograms($applicationId)
{
    $applicationPrograms = array();
        
    $query = "SELECT *
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId) . "
        ORDER BY choice";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $applicationPrograms[] = $row;     
    }
    
    return $applicationPrograms;    
}

function getDesignMastersAdmissionStatuses($applicationId)
{
    $admissionStatuses = array();
        
    $query = "SELECT *
        FROM application_decision_design
        WHERE application_id = " . intval($applicationId) . "
        ORDER BY choice";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $admissionStatuses[] = $row;     
    }
    
    return $admissionStatuses;    
} 
?>