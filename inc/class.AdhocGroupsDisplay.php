<?PHP
/*
 * Name: class.AdhocGroupsData.php
 * Created by: Paul Worona
 * December 2008
 *
 * Description:
 * class to consolidate functions used to assign applicants to groups
 */

class AdhocGroupsDisplay
{
	
	private $application_id;
	private $department;
	private $userid;
	
	public function __construct( $application_id, $userid, $department )
	{
		$this->application_id = $application_id;	
		$this->userid = $userid;
		$this->department = $department;
	}
	
	// applicant info
	public function printApplicantInfo( $applicant )
	{
		// set vars
		$firstname = $applicant['firstname'];
		$lastname = $applicant['lastname'];
	
		print "<div id='applicantInfo'><b>Applicant:</b> " . $firstname . " " . $lastname . "</div>";
	}	
	
	// assigned faculty list
	public function printAssignedFacultyList( $assignedFaculty )
	{
		if ( count($assignedFaculty) )
		{
			$html = "<div id='assignedFaculty'>";			
			$html .= "<b>This applicant has been assigned to:</b>";
			$html .= "<ul>";			
			for ( $i = 0; $i < count($assignedFaculty); $i++ )
			{
				$firstname = $assignedFaculty[$i]['firstname'];
				$lastname = $assignedFaculty[$i]['lastname'];
				$html .= "<li>$firstname $lastname</li>";
			}
			$html .= "</ul>";
			$html .= "</div>";
			print $html;
		} 
	}
	
	// assigned faculty list
	public function printAssignFacultyForm( $unassignedFaculty, $returnURL )
	{
		$application_id = $this->application_id;
		$department = $this->department;
		$userid = $this->userid;
		
		$html = "<div id='unassignedFaculty'>"; 
		if ( count($unassignedFaculty) )
		{
			$options = "";
			foreach ( $unassignedFaculty as $faculty )
			{
				$options .= "<option value='{$faculty['id']}'>{$faculty['lastname']}, {$faculty['firstname']}</option>";	
			}
			
			// print the form
			$html .= "<form action='{$_SERVER['PHP_SELF']}' method='POST'>";
			$html .= "<b>Assign to: </b>";
			$html .= "<select name='FacultyUserID'>$options</select>";
			$html .= "<input type='hidden' name='application_id' value='$application_id'>";
			$html .= "<input type='hidden' name='returnURL' value='$returnURL'>";
			$html .= "<input type='hidden' name='userid' value='$userid'>";
			$html .= "<input type='hidden' name='department' value='$department'> ";
			$html .= "<input type='submit' name='submit' value='Submit'>";
			$html .= "</form>";
			
		} else {
			$html .= "No faculty left to assign";
		}
		$html .= "</div>";
		print $html;
	}
	
	// error message
	public function printErrorMessage( $message )
	{
		print "<div id='errorMessage'>$message</div>";
	}

	// success message
	public function printSuccessMessage( $message )
	{
		print "<div id='successMessage'>$message</div>";
	}
	
	// header
	public function printHeader( $returnURL )
	{
		$html = "
			<html>
			<head>
				<title>Assign Applicant To Faculty</title>
				<style>
					h2 {margin:0}
					#content {padding: 10px}
					#errorMessage {padding: 5px; color:red; font-weight:bold}
					#successMessage {padding: 5px; color:green; font-weight:bold}
					#applicantInfo {padding: 5px}
					#unassignedFaculty {padding: 5px}
					#unassignedFaculty form {margin: 0}
					#assignedFaculty {padding: 5px;}
					#assignedFaculty ul {padding: 0 0 0 25px; margin:0;}
				</style>
			</head>
			<body>
			
			<a href='$returnURL'><< Return To Application</a>
			<h2>Assign Applicant To Faculty</h2>	
			<div id='content'	
			";
		print $html;
	}
	
	// footer
	public function printFooter()
	{
		$html = "
			</div>
			</body>
			</html>
			";
		print $html;
	}	
}

?>



	
