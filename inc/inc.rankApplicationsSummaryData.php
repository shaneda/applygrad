<?php
// Group data
$DB_Applyweb = new DB_Applyweb();
$groupQuery = "SELECT DISTINCT revgroup.id AS group_id,
    revgroup.name AS group_name
    FROM revgroup
    WHERE revgroup.department_id = " . intval($departmentId) . "
    AND revgroup.group_type = 2
    ORDER BY group_name";
$groups = $DB_Applyweb->handleSelectQuery($groupQuery, 'group_id');

// Scores data
$normalizationData = new NormalizationListData($departmentId, $periodId, NULL, $round);
$scores = $normalizationData->getScores(1); // point1 scores

// Application data
foreach ($groups as $groupId => $group) {

    $groups[$groupId]['coordinator_luu_id'] = ''; 
    $groups[$groupId]['coordinator_name'] = '';
    $groups[$groupId]['coordinator_comment'] = '';
    $groups[$groupId]['comment_date'] = '';
    
    // Take the group chair who last saved a ranking for this group/period. 
    $coordinatorQuery = "SELECT 
        group_rank_comment.lu_users_usertypes_id AS coordinator_luu_id, 
        CONCAT(users.firstname, ' ', users.lastname) AS coordinator_name,
        comment AS coordinator_comment,
        comment_date 
        FROM group_rank_comment
        INNER JOIN lu_users_usertypes
            ON group_rank_comment.lu_users_usertypes_id = lu_users_usertypes.id
        INNER JOIN users
            ON lu_users_usertypes.user_id = users.id
        INNER JOIN (
            SELECT 
            IFNULL(group_id, 0) AS group_id,
            IFNULL(round, 0) AS round,
            IFNULL(period_id, 0) AS period_id,
            MAX(timestamp) AS comment_date
            FROM group_rank
            WHERE group_id = " . $groupId . "
            AND round = " . $round . " 
            AND period_id = " . $periodId . "
        ) AS group_rank 
            ON group_rank_comment.group_id = group_rank.group_id
            AND group_rank_comment.round = group_rank.round
            AND group_rank_comment.period_id = group_rank.period_id
        WHERE group_rank_comment.group_id = " . $groupId . "
        AND group_rank_comment.round = " . $round . " 
        AND group_rank_comment.period_id = " . $periodId . "
        LIMIT 1";
    $coordinatorRecord = $DB_Applyweb->handleSelectQuery($coordinatorQuery);
    
    // Add the chair info to the groups array
    if (isset($coordinatorRecord[0])) {
        $groups[$groupId]['coordinator_luu_id'] 
            = $coordinatorRecord[0]['coordinator_luu_id']; 
        $groups[$groupId]['coordinator_name'] 
            = $coordinatorRecord[0]['coordinator_name'];
        $groups[$groupId]['coordinator_comment'] 
            = $coordinatorRecord[0]['coordinator_comment'];
        $groups[$groupId]['comment_date'] 
            = $coordinatorRecord[0]['comment_date'];    
    }
    
    // Get the group applications
    $applicationQuery = "SELECT application.id AS application_id, 
    application.user_id AS luu_id,
    CONCAT(users.lastname, ', ', users.firstname) AS name,
    lu_application_groups.group_id, 
    rank,
    0 AS score_average,
    0 AS score_rank
    FROM application 
    INNER JOIN period_application 
        ON application.id = period_application.application_id
    INNER JOIN lu_application_groups 
        ON application.id = lu_application_groups.application_id
    INNER JOIN lu_users_usertypes
        ON application.user_id = lu_users_usertypes.id 
    INNER JOIN users
        ON lu_users_usertypes.user_id = users.id
    LEFT OUTER JOIN group_rank_member
        ON application.id = group_rank_member.application_id
        AND lu_application_groups.group_id = group_rank_member.group_id
    WHERE lu_application_groups.group_id = " . intval($groupId) . "
    AND lu_application_groups.round = " . intval($round) . "
    AND period_application.period_id = " . intval($periodId) . "
    ORDER BY lu_application_groups.group_id, rank, name";
    $applications = $DB_Applyweb->handleSelectQuery($applicationQuery, 'application_id');
            
    // Add the applications to the groups array
    $groups[$groupId]['applications'] = $applications;
    
    // Add the average scores and score ranks
    $previousScore = 0;
    $scoreCount = 0;
    $scoreRank = 0;

    foreach ($scores as $score) {

        $scoreApplicationId = $score['application_id'];

        if (array_key_exists($scoreApplicationId, $applications)) {

            $scoreCount++;
            if ($score['score_average'] > $previousScore) {
                $scoreRank = $scoreCount;    
            }
            $previousScore = $score['score_average'];
         
            $groups[$groupId]['applications'][$scoreApplicationId]['score_average'] = $score['score_average']; 
            $groups[$groupId]['applications'][$scoreApplicationId]['score_rank'] = $scoreRank;
        }
    }
}
?>