<?
//$expireTime = 60*60*5; // 5 hours
//session_set_cookie_params($expireTime);

// Start a new session only if the including script 
// hasn't already started it. 
if ( !isset($_SESSION) ) {
    session_start();    
}

/*
* Error reporting level now handled in config files.
*/
//error_reporting(E_ALL);
//ini_set('display_errors','On');

include_once '../inc/config.php'; 
include_once '../inc/db_connect.php';

// PLB added redirect to HTTPS 12/9/09
if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' )
{ 
    $srvname = $_SERVER["SERVER_NAME"];
    $scrname = $_SERVER["SCRIPT_NAME"];
    header("Location: https://".$srvname.$scrname);
}


if(!isset($_SESSION['A_SECTION'])){ //STUDENT INTERFACE OR ADMIN
	$_SESSION['A_SECTION']= "1";
}

if(!isset($_SESSION['A_domainid'])){ //POSTED DOMAIN ID
	$_SESSION['A_domainid']= -1;
}
if(!isset($_SESSION['A_usertypeid'])){ //SYSTEM ROLE ID
	$_SESSION['A_usertypeid']= -1;
}
if(!isset($_SESSION['A_usertypename'])){ //SYSTEM ROLE TYPE NAME
	$_SESSION['A_usertypename']= "";
}

if( ((!isset($_SESSION['A_domainid']) || $_SESSION['A_domainid'] == -1) && $_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1) 
&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'launchwebs.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], '/apply/accountCreate.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], '/apply/forgotPassword.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], '/apply/newPassword.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], '/admin/') !== false
)
{ 
	
    if(!isset($_GET['domain']) &&  $_SESSION['A_usertypeid'] != 6)//ALLOW RECOMMENDER TO COME IN
	{
	
		session_unset();
		destroySession();
		echo "redirect";
		
		header("Location: nodomain.php");
	}
}
if(!isset($_SESSION['A_domainname'])){ //POSTED DOMAIN NAME
	$_SESSION['A_domainname']= "";
}
if(!isset($_SESSION['A_level'])){ //DEGREE LEVEL WILL SHOW ONLY PROGRAMS ASSOCIATED WITH DEGREE
	$_SESSION['A_level']= 2;//GRADUATE IS DEFAULT
}
if(!isset($_SESSION['A_allow_edit']))
{
	$_SESSION['A_allow_edit']= false;
	$_SESSION['A_allow_edit_late']= false;
	if($_SESSION['A_domainid'] > -1)
	{
		$sql = "SELECT expdate,expdate2 FROM systemenv where domain_id=".$_SESSION['A_domainid'];
	}
	else
	{
		$sql = "SELECT expdate,expdate2 FROM systemenv where domain_id=1";
	}
	$result = mysql_query($sql) or die(mysql_error());
	$_SESSION['A_expdate'] = "";
	$_SESSION['A_expdate2']  = "";
	while($row = mysql_fetch_array( $result )) 
	{
		$_SESSION['A_expdate']  = $row['expdate'];
		$_SESSION['A_expdate2']  = $row['expdate2'];
	}
	if($_SESSION['A_domainid'] > -1 && $_SESSION['A_domainname'] == "")
	{
		$sql = "SELECT name FROM domain where id=".$_SESSION['A_domainid'];
		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$_SESSION['A_domainname']  = $row['name'];
		}
	}

	if(strtotime(date("Y-m-d h:i:s")) < strtotime($_SESSION['A_expdate']))
	{
		$_SESSION['A_allow_edit']= true;
	}
	if(strtotime(date("Y-m-d h:i:s")) < strtotime($_SESSION['A_expdate2'] ))
	{
		$_SESSION['A_allow_edit_late'] = true;
	}
}
if(!isset($_SESSION['A_allow_admin_edit']))
{
	$_SESSION['A_allow_admin_edit']= true;
}


if(!isset($_SESSION['A_email'])){ //USERS EMAIL
	$_SESSION['A_email']= "";
}
if(!isset($_SESSION['A_firstname'])){ //USERS firstname
	$_SESSION['A_firstname']= "";
}
if(!isset($_SESSION['A_lastname'])){ //USERS lastname
	$_SESSION['A_lastname']= "";
}

if(!isset($_SESSION['A_admin_depts']))//DEPARTMENTS THE ADMIN IS ALLOWED TO SEE
{
	$_SESSION['A_admin_depts']= array();
	
}


if(!isset($_SESSION['A_admin_domains']))//DOMAINS THE ADMIN IS ALLOWED TO SEE
{
	$_SESSION['A_admin_domains']= array();
}
//FOR APPLYWEB
if(!isset($_SESSION['A_usermasterid'])){
	$_SESSION['A_usermasterid']= -1;
}
if(!isset($_SESSION['A_userid'])){
	$_SESSION['A_userid']= -1;
}
if(!isset($_SESSION['A_appid'])){ //STUDENTS APPLICATION NUMBER
	$_SESSION['A_appid']= -1;
}

if(isset($_SESSION['A_domainid']))
{
	switch($_SESSION['A_SECTION'])
	{
		case "1":
		//echo $_SESSION['A_usertypeid'];
			switch($_SESSION['A_usertypeid'])
			{
				case -1:
                    
                    // You don't have a session set, because you didn't go 
                    // through the front door. Go to the front door!
                    $viewApplication = "";
                    if ( isset($_REQUEST['viewApplication']) && isset($_REQUEST['viewDepartment']) ) {
                        $validApplicationId = filter_var($_REQUEST['viewApplication'], FILTER_VALIDATE_INT);
                        $validDepartmentId = filter_var($_REQUEST['viewDepartment'], FILTER_VALIDATE_INT);
                        if ($validApplicationId && $validDepartmentId) {
                            $viewApplication = "?viewApplication=" . $validApplicationId;
                            $viewApplication .= "&viewDepartment=" . $validDepartmentId;
                        }     
                    }
                    header("Location: index.php" . $viewApplication);
                    break;
                
                case 0:
					// no restrictions
					break;
				case 6://RECOMMENDER
					if(strstr($_SERVER['SCRIPT_NAME'], 'recommender.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'recommenderUpload.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false)
					{
						if($_SESSION['A_userid'] != -1)
						{
							header("Location: recommender.php");
						}
					}
				break;
				case 1:
					//NO RESTRICTIONS
					break;

				case 5:
					//STUDENT
					if($_SESSION['A_appid'] == -1 
					&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], '/apply/home.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], '/apply/applications.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], '/apply/applicationEdit.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], '/apply/accountCreate.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') !== false
					)
					{
						if($_SESSION['A_userid'] != -1)
						{
							header("Location: home.php");
							//echo "redirect home";
						}else{
							if( $_SERVER['SCRIPT_NAME'] != '/apply/nodomain.php'){
							header("Location: index.php");
							//echo "redirect index";
							}
						}
					}
					break;
				default:
					if($_SESSION['A_appid'] == -1 
					&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'launchwebs.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/home.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], '/apply/newPassword.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], '/apply/forgotPassword.php') !== false
					)
					{
						
						if($_SESSION['A_userid'] != -1){
							header("Location: home.php");
						}else{
							if(strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') !== false )
							{
								header("Location: index.php");
							}
						}
					}
				break;
			}//SWITCH USERTYPEID
			
			 
		break;
		case "2"://ADMIN SECTION
			switch($_SESSION['A_usertypeid'])
			{
				case 0://SUPERUSER
					//NO RESTRICTIONS
				break;
				case 1:
					//NO RESTRICTIONS
					break;
				case 2:     //committee
					if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'home.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'applicants_adm_committee_reviews.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'sysusersEdit.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') !== false
					)
					{
						if($_SESSION['A_userid'] != -1)
						{
							header("Location: home.php");
						}
					}
					break;	
				case 3:     // faculty
                case 20:    // area chair
					if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'home.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'applicants_adm_committee_reviews.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'sysusersEdit.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') !== false
					)
					{
						if($_SESSION['A_userid'] != -1)
						{
							header("Location: home.php");
						}
					}
					break;
				case 4:
					//NO RESTRICTIONS
					break;
				case 10://admin chair
					//NO RESTRICTIONS
					break;
				case 11://STUDENT CONTACT
					if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'home.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'applicants_adm_committee_reviews.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'sysusersEdit.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') !== false
					)
					{
						if($_SESSION['A_userid'] != -1)
						{
							header("Location: home.php");
						}
					}
					break;
				case 12://FACULTY CONTACT
					if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'admitted.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'sysusersEdit.php') !== false
					&& strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') !== false
					)
					{
						if($_SESSION['A_userid'] != -1)
						{
							header("Location: admitted.php");
						}
					}
					break;
                case 18:    // Statistics read-only role
                    if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
                    && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
                    && strstr($_SERVER['SCRIPT_NAME'], 'admitted.php') !== false
                    )
                    {
                        if($_SESSION['A_userid'] != -1)
                        {
                            header("Location: home.php");
                        }
                    }
                    break;
                case 19:    // placeout reviewer role
                    if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
                    && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
                    && strstr($_SERVER['SCRIPT_NAME'], 'admitted.php') !== false
                    )
                    {
                        if($_SESSION['A_userid'] != -1)
                        {
                            header("Location: home.php");
                        }
                    }
                    break;
                case 21:    // reject reviewer role
                    if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
                    && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
                    && strstr($_SERVER['SCRIPT_NAME'], 'admitted.php') !== false
                    )
                    {
                        if($_SESSION['A_userid'] != -1)
                        {
                            header("Location: home.php");
                        }
                    }
                    break;
				default:
					if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false  
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
					{

					//echo $_SERVER['SCRIPT_NAME'];
						header("Location: index.php");
					}
				break;
			}//SWITCH USERTYPEID
		break;
	}//END SWITCH SECTION
}//end if $_SESSION['A_domainid']
$_SESSION['A_datafileroot']= $datafileroot;
$_SESSION['datafileroot']= $datafileroot;

if(!isset($_SESSION['A_programs'])){
	$_SESSION['A_programs']= array();//USER-SELECTED PROGRAMS
}

if(!isset($_SESSION['A_msgsSent'])){
	$_SESSION['A_msgsSent']= 0;
}


/*
function destroySession()
{
	session_unset();
}
*/




?>
