<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$assistantshipRequested = $aasistantshipGranted = NULL;
$assistanshipStatusQuery = "SELECT * FROM assistantship 
    WHERE application_id = " . $appid . 
    " LIMIT 1";
$assistanshipStatusResult = mysql_query($assistanshipStatusQuery);
while ($row = mysql_fetch_array($assistanshipStatusResult))
{
    $assistantshipRequested = $row['requested']; 
    $assistantshipGranted = $row['granted'];  
}

$admissionProgramOptions = getProgramDepartmentPrograms($myPrograms[0][0]);
$admissionStatuses = getAdmissionStatuses($appid);
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

<?
/*
* Decision form data comes from the lu_application_programs table,
* not from the review table.  Any admin should be able to view and
* modify this data.
*/
for($x = 0; $x < count($myPrograms); $x++)
{
    $aDepts = split(",", $myPrograms[$x][14]);
    //FIRST CHECK IF THE PROGRAM BELONGS TO THIS DEPARTMENT
    for($j = 0; $j < count($aDepts); $j++)
    {
        if($aDepts[$j] == $thisDept)
        {
        //CHECK IF PROGRAM IS ELIGIBLE FOR ROUND2
        ?>
            
            <tr><td colspan="2">
            <hr />
            <strong>Admit Information for program:</strong>  
            <em><?=$myPrograms[$x][1] . " ".$myPrograms[$x][2] . " ".$myPrograms[$x][3]?></em>
            <br />&nbsp;
            </td></tr>

            <tr>
            <td width="50px"><b> Decision: </b> </td>
            <td>
            <? 
            $decision = $myPrograms[$x][10] ;
            showEditText($decision, "listbox", "decision_".$myPrograms[$x][0], $_SESSION['A_allow_admin_edit'], true, $decisionVals); 
            ?>
            </td>
            </tr>
            
            <tr>
            <td width="50px"><b> Comments: </b></td>
            <td>
            <?
            $comments = $myPrograms[$x][13];
            ob_start();
            showEditText($comments, "textarea", "comments_".$myPrograms[$x][0], $allowEdit, false, 60); 
            $commentsTextarea = ob_get_contents();
            ob_end_clean();
            echo str_replace("cols='60'", "cols='50'", $commentsTextarea);  
            ?>
            </td>
            </tr>

            <tr>
            <td><strong>Faculty Contact:</strong></td>
            <td>
            <? 
            $faccontact = $myPrograms[$x][15];
            showEditText($faccontact, "textbox", "faccontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); 
            ?>
            </td>
            </tr>
            
            <tr>
            <td><strong>Student Contact:</font></strong></td>
            <td>
            <? 
            $stucontact = $myPrograms[$x][16];
            showEditText($stucontact, "textbox", "stucontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); 
            ?>
            <br/>&nbsp;
            </td>
            </tr>

            <tr>
            <td colspan="2">
            <h4>Admissions Status</h4>
            <? 
            $admissionStatus = $myPrograms[$x][11];
            showEditText($admissionStatus, "radiogrouphoriz2", "admissionStatus_".$myPrograms[$x][0], $allowEdit, false, array(array(0,"Reject"),array(1,"Waitlist"),array(2,"Admit"),array(3,"Reset")   ) ); 
            ?>
            </td>
            </tr>

<?
        }//END IF DEPTS MATCH
    }//END FOR COUNT aDEPTS        
 }//END FOR EACH PROGRAM?>

 
 <tr>
    <td colspan="2">
        
        <strong></strong>  
    </td>
 </tr>
<tr>
    <td colspan="2">
    <hr />
    <h4>Assistantship Information</h4>
    Assistantship Requested: <?php echo $assistantshipRequested ? 'Yes' : 'No' ; ?>
    <br><br>
    <?
    $assistanshipGranted = NULL; 
    showEditText($assistantshipGranted, "checkbox", "assistantshipGranted", $allowEdit, false, NULL); 
    ?>
    Granted
    </td>
</tr>
            
 
</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmitFinal", $allowEdit); 
?>    
</div>

<?php
function getProgramDepartmentPrograms($programId)
{
    $programs = array();
        
    $query = "SELECT programs.id,
        CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS name
        FROM programs
        INNER JOIN degree ON degree.id = programs.degree_id
        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
        INNER JOIN lu_programs_departments
            ON programs.id = lu_programs_departments.program_id
        WHERE lu_programs_departments.department_id = 
            (SELECT department_id 
            FROM lu_programs_departments 
            WHERE program_id = " . intval($programId) . "
            LIMIT 1)";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $programs[] = array($row['id'], $row['name']);     
    }
    
    return $programs;
}

function getAdmissionStatuses($applicationId)
{
    $admissionStatuses = array();
        
    $query = "SELECT *
        FROM application_decision_design
        WHERE application_id = " . intval($applicationId) . "
        ORDER BY choice";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $admissionStatuses[] = $row;     
    }
    
    return $admissionStatuses;    
}
?>