<?php
// Initialize variables
$rissExternalFunding = NULL;
$rissExternalFundingSource = '';
$rissExternalFundingAmount = '';
$rissReuScholarship = NULL;
$rissDirectFunding = NULL;
$rissFundingError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveRissFunding();
    checkRequirementsRissFunding();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$rissFundingError)
{
    $rissFundingQuery = "SELECT * FROM riss_funding 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $rissFundingResult = mysql_query($rissFundingQuery);
    while($row = mysql_fetch_array($rissFundingResult))
    {
        $rissExternalFunding = $row['external_funding'];
        $rissExternalFundingSource = $row['external_source'];
        $rissExternalFundingAmount = $row['external_amount'];
        $rissReuScholarship = $row['reu_scholarship'];
        $rissDirectFunding = $row['direct_funding'];     
    }    
}  
?>

<span class="subtitle">Scholarships &amp; Funding</span>
<?php
if ($rissFundingError)
{
?>
    <br>
    <span class="errorSubtitle"><?php echo $rissFundingError; ?></span>
<?php  
}
?>
<p>All RI Summer Scholars are considered for funding.</p>

<br/>
<b>External Funding</b>
<br/>
<br/>
I have external funding.
<br/>
<br/>
<?php
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);
showEditText($rissExternalFunding, "radiogrouphoriz", "rissExternalFunding", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
<span style="display: inline-block; width: 200px;">If yes, source of external funding:</span>
<?php
showEditText($rissExternalFundingSource, "textbox", "rissExternalFundingSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
<span style="display: inline-block; width: 200px;">Amount of external funding:</span>
<?php
showEditText($rissExternalFundingAmount, "textbox", "rissExternalFundingAmount", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
<b>NSF REU Scholarship</b>
<br/>
<br/>
I would like to be considered for a U.S. National Science Fellowship Research Experiences for Undergraduates (REU) Scholarship.  
<br/>
<br/>
NSF REU scholarship requirements:
<ul>
  <li>U.S. Citizen, U.S. National, or U.S. Permanent Resident</li>
  <li>Completion of at least sophomore year</li>
  <li>Must still be an undergraduate during BOTH the summer during and fall following the summer scholar program</li>
  <li>GPA of 3.0 or above</li>
</ul>
<?php
showEditText($rissReuScholarship, "radiogrouphoriz", "rissReuScholarship", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
<b>Direct RI Funding</b>
<br/>
<br/>
I would like to be considered for CMU RI lab direct funding.  
<br/>
<br/>
<?php
showEditText($rissDirectFunding, "radiogrouphoriz", "rissDirectFunding", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<hr size="1" noshade color="#990000">

<?php
function saveRissFunding()
{
    global $rissExternalFunding;
    global $rissExternalFundingSource;
    global $rissExternalFundingAmount;
    global $rissReuScholarship;
    global $rissDirectFunding;
    global $rissFundingError;  
    
    $rissExternalFunding = filter_input(INPUT_POST, 'rissExternalFunding', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $rissExternalFundingSource = filter_input(INPUT_POST, 'rissExternalFundingSource', FILTER_SANITIZE_STRING);
    $rissExternalFundingAmount = filter_input(INPUT_POST, 'rissExternalFundingAmount', FILTER_SANITIZE_STRING);
    $rissReuScholarship = filter_input(INPUT_POST, 'rissReuScholarship', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $rissDirectFunding = filter_input(INPUT_POST, 'rissDirectFunding', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    
    if (!$rissFundingError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM riss_funding WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE riss_funding SET
                external_funding = " . intval($rissExternalFunding) . ",
                external_source = '" . mysql_real_escape_string($rissExternalFundingSource) . "',
                external_amount = '" . mysql_real_escape_string($rissExternalFundingAmount) . "',
                reu_scholarship = " . intval($rissReuScholarship) . ",
                direct_funding = " . intval($rissDirectFunding) . "
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO riss_funding 
                (application_id, external_funding, external_source, external_amount, reu_scholarship, direct_funding)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($rissExternalFunding) . ",'" 
                . mysql_real_escape_string($rissExternalFundingSource) . "','" 
                . mysql_real_escape_string($rissExternalFundingAmount) . "'," 
                . intval($rissReuScholarship) . ","
                . intval($rissDirectFunding) . ")";
            mysql_query($insertQuery);
        }
    }
}
    
function checkRequirementsRissFunding()
{
    global $err;
    global $iniFundingError;     
    
    if ($err || $iniFundingError)
    {
        updateReqComplete("suppinfo.php", 0);
    }
}
?>