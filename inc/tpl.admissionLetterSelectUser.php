<?php
include CLASS_DIR . 'DB_Applyweb/class.DB_Domain.php'; 
include CLASS_DIR . 'DB_Applyweb/class.DB_Department.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_VW_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdminList.php';  
include CLASS_DIR . 'DB_Applyweb/class.VW_AdmissionLetterListBase.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdmissionLetterListBaseIni.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_AdmissionLetterListBaseDesignMasters.php';
include CLASS_DIR . 'DB_Applyweb/class.VW_Programs.php';
include CLASS_DIR . 'class.AdmissionLetterApplicantListData.php';
include CLASS_DIR . 'Structures_DataGrid/class.AdminList_DataGrid.php';
include CLASS_DIR . 'Structures_DataGrid/class.AdmissionLetterApplicantList_DataGrid.php';
include CLASS_DIR . 'Structures_DataGrid/class.AdmissionLetterApplicantListIni_DataGrid.php';  
include CLASS_DIR . 'class.Department.php'; 

$datagridDataClass = new AdmissionLetterApplicantListData($unit, $unitId, $applicationId);

$datagridData = $datagridDataClass->getData($periodId, 
    NULL, NULL, NULL, NULL, NULL,
    NULL, NULL, NULL, NULL, NULL);
if ($unit == 'department' && isIniDepartment($unitId))
{
    $datagrid = new AdmissionLetterApplicantListIni_DataGrid($datagridData, NULL, NULL, NULL);    
}
else
{
    $datagrid = new AdmissionLetterApplicantList_DataGrid($datagridData, NULL, NULL, NULL); 
}
?>

<form name="selectionForm" id="selectionForm" action="" method="post">
    <input type="hidden" name="applicationId" id="applicationId" value="" />
    <input type="hidden" name="selectUserSubmit" value="submit" />

<br><br>
<div style="clear: both;">
<?php $datagrid->render(); ?>
</div>

</form>

<script>
$(document).ready(function(){
   $('.selectApplicationId').click(function(){
       var selectedApplicationId = $(this).attr("id").split("_")[1];
       $('#applicationId').val(selectedApplicationId);
       $('#selectionForm').submit();
       return false;
   }); 
});
</script>