<?php
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Statistics';

$pageCssFiles = array(
    '../css/administerApplications.css',
    '../css/statistics.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/common.js',
    '../javascript/statistics.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>

<br/>

<div id="export" style="padding-right: 40px; float: right;">
    <form action="../review/reviewApplicationsExport.php" method="post" target="_blank"
    name="exportForm" id="exportForm" onsubmit="getTable('statisticsTable')">
        <input type="hidden" id="datatodisplay" name="datatodisplay" /> 
        <b>Export current table:</b>
        <input  type="submit" value="Save CSV File">
    </form>
</div>

<div class="title">
<?php 
echo $unit->getName() . '<br/>'; 
echo $period->getDescription();
//echo ' (' . $period->getStartDate('Y-m-d') . '&nbsp;to&nbsp;' . $period->getEndDate('Y-m-d') . ')';
?>
<br/>
<span style="font-size: 18px;">Statistics</span>  
</div>

<div style="margin: 10px 5px; padding: 5px; clear: both;">

<div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%">
<ul id="viewMenu" class="menu">
<?php
foreach ($viewMenuItems as $label => $viewArg) {
    $href = '?periodId=' . $periodId . '&unitId=' . $unitId . '&view=' . $viewArg;
    $class = $divId = $linkStyle = '';
    if ($viewArg == $view) {
        $class = ' class="selectedItem"';
        $divId = ' id="selectedMenuDiv"';
        $linkStyle = ' style="color: #990000;"';     
    }
    echo '<li' . $class . '><div ' . $divId . '><a href="' . $href . '">' . $label . '</a></div></li>';  
}
?>    
</ul>
</div>

<div id="table" style="margin-left: 20px; float: left; clear: right; width: 75%;">

<div id="limitDiv" style="padding-bottom: 10px; float: left;">
<form action="" method="post" name="limitForm" id="limitForm">
<input type="hidden" name="periodId" value="<?php echo $periodId; ?>" />
<input type="hidden" name="unitId" value="<?php echo $unitId; ?>" />
<input type="hidden" name="view" value="<?php echo $view; ?>" />
<?php
if ( isset($limitMenuItems) && count($limitMenuItems) > 0 ) {
    echo $limitMenuLabel . ': ';
    echo '<select id="limit" name="limit">';
    foreach ($limitMenuItems as $label => $limitArg) {
        $option = '<option value="' . $limitArg . '"';
        if ($limitArg == $limit) {
            $option .= ' selected';    
        }
        $option .= '>' . $label . '</option>';
        echo $option;    
    }
    echo '</select>';
    echo ' <input type="submit" value="Re-Calculate">';
}
?>
</form>
</div> 
<br/>

<div id="table" style="float: left; clear: both; width: 100%; background-color: #FFFFFF;">

<?php
$datagrid->render();
?>
</div>

</div> 

</div>

<div style="clear: both;" />


<br/>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';
?>