<?php
  function get_user_selected_programs($applicationID)
{
//get user-selected programs
//GET USERS SELECTED PROGRAMS
  //echo "loading from test directory\n";
  $ret = array();

  $sql = "SELECT programs.id, domain.name
  FROM lu_application_programs 
  inner join programs on programs.id = lu_application_programs.program_id
  inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
  inner join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
  inner join domain on domain.id = lu_domain_department.domain_id
  where lu_application_programs.application_id = ".$applicationID." order by choice";
  $result = mysql_query($sql) or die(mysql_error() . $sql);


  while($row = mysql_fetch_array( $result )) 
  {
    $arr = array();
    array_push($arr, $row[0]);
    array_push($arr, $row['name']);
    array_push($ret, $arr);
  }

  return $ret;

} // END get_user_selected_programs

function getExperience($applicationID, $type=1)
{
    $ret = array();
    $sql = "select datafile_id,experiencetype, moddate,size,extension from experience
    inner join datafileinfo on datafileinfo.id =  experience.datafile_id
    where application_id=".$applicationID." and experiencetype=".$type;
    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['datafile_id']);
        array_push($arr, $row['experiencetype']);
        array_push($arr, $row['moddate']);
        array_push($arr, $row['size']);
        array_push($arr, $row['extension']);
        array_push($ret, $arr);
    }
    return $ret;
}

function getEmployments ($applicationID) {
    $employments = array();
    $sql = "SELECT 
    id, company, start_date, end_date, years_exp, 
    address, job_title, job_description 
    FROM experience
    where application_id=".$applicationID." and experiencetype=5";
    $result = mysql_query($sql) or die(mysql_error());
    $comp = true;
    while($row = mysql_fetch_array( $result )) 
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, trim($row['company']));
        if($row['company'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['start_date'],'-','/')  );
        if($row['start_date'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['end_date'],'-','/') );
        if($row['end_date'] == ""){$comp = false; }
        array_push($arr, $row['years_exp']);
        if($row['years_exp'] == ""){$comp = false; }
        
        // ini/dietrich fields
        array_push($arr, $row['address']);          // 5
        array_push($arr, $row['job_title']);        // 6
        array_push($arr, $row['job_description']);  // 7
                
        array_push($employments, $arr);
    }
    return ($employments);
}

function getTeachingExperiences($applicationID) {
    $employments = array();
    $sql = "SELECT 
    id, institution, start_date, end_date,  
    address, courses_taught
    FROM teaching_experience
    where application_id=".$applicationID;
    $result = mysql_query($sql) or die(mysql_error());
    $comp = true;
    while($row = mysql_fetch_array( $result )) 
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, trim($row['institution']));
        if($row['institution'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['start_date'],'-','/')  );
        if($row['start_date'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['end_date'],'-','/') );
        if($row['end_date'] == ""){$comp = false; }
        array_push($arr, $row['address']);          // 4
        array_push($arr, $row['courses_taught']);        // 5
                
        array_push($employments, $arr);
    }
    return ($employments);
}
?>
