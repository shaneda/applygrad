<?php
if ( isset($recommendationRecords[0]) ) {
    $applicantId = $recommendationRecords[0]['applicant_id'];
    $applicantGuid = $recommendationRecords[0]['guid'];    
} else {
    $applicantId = NULL;
    $applicantGuid = NULL;
}


echo <<<EOB

<br />
<form id="recommendations_{$applicationId}">
<input type="hidden" class="applicantId" id="applicantId_{$applicationId}" value="{$applicantId}" />
<input type="hidden" class="applicantGuid" id="applicantGuid_{$applicationId}" value="{$applicantGuid}" />
<table cellspacing="5px" cellpadding="5px" border="0"> 

EOB;

$isMseMsitDepartment = isMseMsitDepartment($departmentId);
$countRecommendationsSubmitted = $countRecommendationsReceived = 0;
foreach ($recommendationRecords as $record) {
    
    // Skip if the record is full of null values.
    if ( $record['rec_user_id'] != -1 ) {
        
        $countRecommendationsSubmitted++;
        
        $recommendationId = $record['id'];
        $recommenderName = $record['lastname'] . ', ' . $record['firstname'];
        $datafileId = $record['datafile_id'];
        $recSubmitted = $record['submitted'];
        $recommendType = $record['recommendtype'];
                
        echo <<<EOB
        
        <tr valign="top">
            <td><span id="recommenderName_{$applicationId}_{$recommendationId}">{$recommenderName}</span></td>
EOB;
        
        $formMessage = '';
        $formSubmitted = formSubmitted($recommendationId);
        // Form submission is required only for MS-HCII. 
        if ( $recommendType == 2 || $recommendType == 3
            || ($recommendType == 1 && $departmentId == 49) ) {
            if ($formSubmitted) {
                $formMessage = '<br/><span class="confirmComplete">Form Submitted</span>';   
            } else {
                $formMessage = '<br/><span class="confirm">Form Not Submitted</span>';
            }
        }  
        
        if ( $datafileId ) {
            
            if ($recSubmitted) {              

                // Form submission is required only for MS-HCII. 
                if ( $recommendType == 2 || $recommendType == 3 
                    || ($recommendType == 1 && $departmentId == 49) ) {
                    if ( $formSubmitted || $isMseMsitDepartment || $departmentId == 20 ) {
                        // It is not required for MSE-MSIT or VLIS.
                        $countRecommendationsReceived++;    
                    }         
                } else {
                    $countRecommendationsReceived++;    
                }
            }
            
            // The recommendation has been submitted with an uploaded file.
            $fileName = "recommendation_" . $record['userdata'] . "." . $record['extension'];                 
            //$filePath = $datafileroot . "/" . $applicantGuid . "/recommendation/" . $fileName;
            $filePath = '../admin/fileDownload.php?file=' . urlencode($fileName) .'&amp;guid=' . $applicantGuid;
            
            echo <<<EOB

            <td>
            <span class="confirmComplete">Letter Uploaded</span>
            &nbsp;&nbsp;(<a class="file_link" href="{$filePath}" target="_blank">file</a>)
            {$formMessage}
            </td>
            <td>
            <input type="hidden" class="uploadMode" id="uploadMode_{$applicationId}_{$recommendationId}" value="update" />
            <!-- Note: name of file input must be "fileToUpload" for ajax function to work -->
            <input size="30" class="uploadRecommendationFile" 
                id="uploadRecommendationFile_{$applicationId}_{$recommendationId}" name="fileToUpload" type="file" /> 
            <input class="uploadRecommendation" 
                id="uploadRecommendation_{$applicationId}_{$recommendationId}" type="submit" value="Update Letter" />
            </td>                
            
EOB;
        } else {
                          
            // Check whether the recommendation was entered as "content," i.e.,
            // the content field contains more than "&lt;!--tml_entity_decode($content--&gt;"
            $statusMessage = 'Requested';
            $contentLength = strlen($record['content']); 
            if ( $contentLength > 50 ) {
                $statusMessage = '<span class="confirm_complete">Letter Uploaded</span>&nbsp;&nbsp;(text)';

                // Form submission is required only for MS-HCII.
                if ( $recommendType == 2 || $recommendType == 3
                    || ($recommendType == 1 && $departmentId == 49) ) {
                    if ($formSubmitted || $isMseMsitDepartment || $departmentId == 20) {
                        // It is not required for MSE-MSIT or VLIS.
                        $countRecommendationsReceived++;    
                    }         
                } else {
                    $countRecommendationsReceived++;    
                }
            }
                
            echo <<<EOB

            <td>{$statusMessage} {$formMessage}</td>
            <td>
            <input type="hidden" class="uploadMode" id="uploadMode_{$applicationId}_{$recommendationId}" value="new" />
            <!-- Note: name of file input must be "fileToUpload" for ajax function to work -->  
            <input size="30" class="uploadRecommendationFile" 
                id="uploadRecommendationFile_{$applicationId}_{$recommendationId}" name="fileToUpload" type="file" /> 
            <input class="uploadRecommendation" 
                id="uploadRecommendation_{$applicationId}_{$recommendationId}" type="submit" value="Upload Letter" />
            </td>
EOB;
        }
        echo '</tr>';
    } 
}
    
if ( $countRecommendationsSubmitted == 0 ) {
    echo '<td colspan="3"><p>No recommendations found.</p></td>';
}

$message = $countRecommendationsReceived . ' of ' . $countRecommendationsSubmitted;
$messageClass = 'confirm';
if ( $countRecommendationsReceived == $countRecommendationsSubmitted && $countRecommendationsSubmitted > 0 ) {
    $messageClass = 'confirmComplete';     
}
      
echo <<<EOB
      
</table>
</form>

<script>
    $('#recommendationsReceivedMessage_{$applicationId}').html('{$message}');
    $('#recommendationsReceivedMessage_{$applicationId}').attr('class', '{$messageClass}');   
</script>

EOB;

function formSubmitted($recommendationId) {
    
    global $recommendations;    // DB_Recommendations() from administerApplicationsSingle.php
    
    $query = "SELECT DISTINCT recommend_id 
                FROM recommendforms 
                WHERE recommend_id = " . $recommendationId;
    $formRecords = $recommendations->handleSelectQuery($query);            
    
    if (count($formRecords) > 0) {
        return TRUE;    
    } else {
        return FALSE;
    } 
}
?>