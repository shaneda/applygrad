<?php
echo <<<EOB

<form id="transcripts_{$applicationId}">
<table cellspacing="5px" cellpadding="5px" border="0">

EOB;
//DebugBreak();
$countTranscriptsSubmitted = $countTranscriptsReceived = 0;
foreach ($educationRecords as $record) {
    
    $transcriptId = $record['id']; 
    
    // Skip if record is full of null values.
    if ($record['institute_id'] != NULL) {
        
        $countTranscriptsSubmitted++;
        
        // NOTE misspelling of field transcriptreceived with "ss"
        $receivedChecked = "";
        if ($record['transscriptreceived'] == 1) {
            $receivedChecked = "checked";
            $countTranscriptsReceived++;
        }
        
        $institution = $record['name'];
        $degree = $record['degree'];
        $major = $record['major1'];
        $dateEntered = $record['date_entered']; 
        
        echo <<<EOB

        <tr valign="top">
            <td>
            <span id="institution_{$applicationId}_{$transcriptId}">{$institution}</span><br/>
            <span id="degree_{$applicationId}_{$transcriptId}">{$degree}</span>, {$major}<br/>
            {$dateEntered} - 
EOB;
        if ($record['date_grad']) {
            echo $record['date_grad'];    
        } else {
            echo $record['date_left'];
        }       

        echo <<<EOB
            
            </td>
            <td align="center">
                <input type="checkbox" class="transcriptReceived" 
                    id="transcriptReceived_{$applicationId}_{$transcriptId}" {$receivedChecked} /> Received
    
            </td>
EOB;

        if (isIniDepartment($departmentId))
        {
            echo <<<EOB
            <td>
                <form>
                    Converted GPA:
                    <input type="text" size="5" id="convertedGpa_{$applicationId}_{$transcriptId}" 
                        value="{$record['converted_gpa']}"> 
                    <input type="submit" class="submitSmall updateConvertedGpa" 
                        id="updateConvertedGpa_{$applicationId}_{$transcriptId}" value="Update GPA">
                </form>
            </td>
EOB;
        }

        echo <<<EOB
        </tr>
EOB;
    }
}
    
if ( $countTranscriptsSubmitted == 0 ) {
    echo '<td colspan="3"><p>No transcripts found.</p></td>';
}

$message = $countTranscriptsReceived . ' of ' . $countTranscriptsSubmitted;
$messageClass = 'confirm';
if ($countTranscriptsReceived == $countTranscriptsSubmitted && $countTranscriptsSubmitted > 0) {
    $messageClass = 'confirmComplete';     
}

echo <<<EOB
      
</table>
</form>

<script>
    $('#transcriptsReceivedMessage_{$applicationId}').html('{$message}');
    $('#transcriptsReceivedMessage_{$applicationId}').attr('class', '{$messageClass}');
</script>

EOB;
?>