<?php
// Initialize variables
$requestAssistantship = -1;
$requestAssistantshipError = '';

// Validate and save posted data
if(isset($_POST['requestAssistantship'])) 
{
    saveRequestAssistantship();
    checkRequirementsRequestAssistantship();
}                              

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$requestAssistantshipError)
{
    $requestAssistantshipQuery = "SELECT requested FROM assistantship 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $requestAssistantshipResult = mysql_query($requestAssistantshipQuery);
    while($row = mysql_fetch_array($requestAssistantshipResult))
    {
        $requestAssistantship = $row['requested'];        
    }    
}
?>

<span class="subtitle">Assistantship</span>
<br/><br/>
Do you wish to be considered for an assistantship?
<br/><br/>
<?php
$yesNoArray = array(
    array(1, "Yes"),
    array(0, "No")
);

ob_start();
showEditText($requestAssistantship, "radiogrouphoriz", "requestAssistantship", $_SESSION['allow_edit'], false, $yesNoArray);
$inputElementString = ob_get_contents();
ob_end_clean();

$inputElementReplace = ' onClick="form1.submit();">';
$inputElementString = str_replace('>', $inputElementReplace, $inputElementString);

echo $inputElementString
?>

<hr size="1" noshade color="#990000">

<?php
function saveRequestAssistantship()
{
    global $requestAssistantship;
    global $requestAssistantshipError;
    
    $requestAssistantship = filter_input(INPUT_POST, 'requestAssistantship', FILTER_VALIDATE_INT);
    
    if ($requestAssistantship !== -1)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM assistantship WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE assistantship SET
                requested = " . intval($requestAssistantship) . "
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO assistantship (application_id, requested)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($requestAssistantship) . ")";
            mysql_query($insertQuery);
        }
    }
}

function checkRequirementsRequestAssistantship()
{
    global $err;
    global $designRequirementsError;
    global $requestAssistantshipError;
    global $requestAssistantship;
    
    if ($requestAssistantship == -1)
    {
        $requestAssistantshipError = 'You must indicate whether you wish to request an assistantship<br>';    
    }     
    
    if (!$err && !$designRequirementsError && !$requestAssistantshipError)
    {
        updateReqComplete("programs.php", 1);
    }
    else
    {
        updateReqComplete("programs.php", 0);    
    }    
}
?>