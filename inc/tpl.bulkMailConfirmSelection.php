<?php
$emailRecords = getApplicantEmailData($unit, $unitId, $periodId);
$templateContent = getTemplateContent($contentId);
$sampleLuuId = NULL;
if ($allUsersSelected == FALSE && is_array($selectedUsers))
{
    foreach ($selectedUsers as $key => $value)
    {
        $sampleLuuId = $key;
        break;    
    }
}
else
{
    foreach ($emailRecords as $key => $value)
    {
        $sampleLuuId = $key;
        break;
    }
}
$sampleMessage = sendBulkMail($contentId, $admissionsContact, $emailRecords, array($sampleLuuId), TRUE);
$templateVariablesValid = ValidateTemplateVariables($sampleMessage);
$sendMailDisabled = '';
if (!$templateVariablesValid)
{
    $sendMailDisabled = 'disabled';    
}
?>

<br> <br>
<form action="" method="post" name="confirmSelectionSubmit">
    <input type="hidden" name="contentId" value="<?php echo $contentId; ?>" />
    <input type="hidden" name="allUsersSelected" value="<?php echo $allUsersSelected ? '1' : '0'; ?>" />
    <?php
    if (is_array($selectedUsers))
    {
        foreach ($selectedUsers as $luuId => $isSelected)
        {
            echo '<input type="hidden" name="selectedUsers[' . $luuId .']" value="' . $luuId . '" />';    
        }        
    }     
    ?>
    <input type="submit" name="confirmSelectionSubmit" value="Send Mail" <?php echo $sendMailDisabled; ?> />
    <input type="submit" name="selectionSubmit" value="Refresh This Page" />
    <input type="submit" name="cancelSelectionSubmit" value="Return to Selection" />
</form>

<div style="color: red; font-weight: bold;">
<?php
if (!$templateVariablesValid)
{
    echo '<br>This message cannot be sent using the current template format.';    
}     
?>
</div>

<br>
<div style="float: left; width: 40%;">
<p style="font-weight: bold; font-style: italic;">
<?php
$contentName = getContentName($contentId);
if ($allUsersSelected)
{
    echo $contentName . ' email be sent to all users:';
}
elseif (!is_array($selectedUsers) || count($selectedUsers) == 0)
{
    echo 'No users selected';    
}
else
{
    echo $contentName . ' email will be sent to selected users:';
}    
?>
</p>

<ul>
<?php
if ($allUsersSelected)
{
    foreach ($emailRecords as $emailRecord)
    {
        echo '<li>' . $emailRecord['firstname'] . ' ' . $emailRecord['lastname'] . ' - ' . $emailRecord['email'] . '</li>';    
    }
        
}
elseif (is_array($selectedUsers))
{
    foreach ($selectedUsers as $luuId => $isSelected)
    {
        $emailRecord = $emailRecords[$luuId];
        echo '<li>' . $emailRecord['firstname'] . ' ' . $emailRecord['lastname'] . ' - ' . $emailRecord['email'] . '</li>';    
    }   
}
?>
</ul>
</div>

<div style="float: left; width: 50%; border: 1px dotted black;">
    <div style="padding: 10px;">
        <b>SAMPLE</b> (<a href="contentEdit.php?id=<?php echo $contentId; ?>" target="_blank">Edit Template</a>)
    </div>
    <div style="padding: 30px; font-size: smaller;">
    <?php
    echo $sampleMessage;
    ?>
    </div>
</div>

<div style="clear: both;"></div>