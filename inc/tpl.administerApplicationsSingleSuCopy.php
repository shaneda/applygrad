<?php

include '../classes/Application/class.Application.php';
include '../classes/DB_Applyweb/class.DB_Applyweb_Table.php';
include '../classes/DB_Applyweb/Table/class.DB_Application.php'; 

$Application = new Application($applicationId);
$applicationPeriodId = $Application->getPeriodId();

$domainPeriods = array();
$domainPeriodQuery = "SELECT domain_id, domain.name AS domain_name, period_umbrella.*
                        FROM period_umbrella
                        INNER JOIN period
                          ON period_umbrella.period_id = period.period_id
                        INNER JOIN domain_unit
                          ON period.unit_id = domain_unit.unit_id
                        INNER JOIN domain
                          ON domain_unit.domain_id = domain.id
                        ORDER BY domain_id, period_id";
$domainPeriodResult = $DB_Applyweb->handleSelectQuery($domainPeriodQuery);
foreach ($domainPeriodResult as $row) {
    $domainId = $row['domain_id'];
    $domainPeriods[$domainId]['domain_name'] = $row['domain_name'];
    $periodId = $row['period_id'];
    if ($periodId != $applicationPeriodId) {
        $periodName = $periodId . ' - ' . $row['admission_term'] . ' ' . $row['admission_year'];
        $periodName .= ' (' . $row['umbrella_name'] . ')';
        $period = array('id' => $periodId, 'name' => $periodName); 
        $domainPeriods[$domainId]['periods'][] = $period;
    }
}
$domainPeriodsJson = json_encode($domainPeriods);
//DebugBreak();
$periodPrograms = array();
$periodProgramsQuery = "SELECT period_umbrella.period_id, programs.id AS program_id,
                        CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS program_name
                        FROM period_umbrella
                        INNER JOIN period_program
                          ON period_umbrella.period_id = period_program.period_id
                        INNER JOIN programs_unit
                          ON period_program.unit_id = programs_unit.unit_id
                        INNER JOIN programs
                          ON programs_unit.programs_id = programs.id
                        INNER JOIN degree ON degree.id = programs.degree_id
                        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
                        ORDER BY period_umbrella.period_id, program_name";
$periodProgramsResult = $DB_Applyweb->handleSelectQuery($periodProgramsQuery);
foreach ($periodProgramsResult as $row) {
    $periodId = $row['period_id'];
    $programName = $row['program_id'] . ' - ' . $row['program_name'];
    $program = array('id' => $row['program_id'], 'name' => $programName);
    $periodPrograms[$periodId]['programs'][] = $program;        
}
$periodProgramsJson = json_encode($periodPrograms);
?>

<form> 
    <input type="hidden" id="applicationId" name="applicationId" value="<?php echo $applicationId; ?>" />
    
    &nbsp;Domain:&nbsp;<select name="domainId" id="domainSelect" onChange="setPeriodSelect()">
    <?php
    foreach ($domainPeriods as $domainId => $domainPeriodArray) {
        echo '<option value="' . $domainId . '">' . $domainId . ' - ';
        echo $domainPeriodArray['domain_name'] . '</option>';
    }
    ?>
    </select>
    &nbsp;&nbsp;
 
    Period:&nbsp;<select name="periodId" id="periodSelect" onChange="setProgramSelect()">
    <?php
    foreach ($domainPeriods[1]['periods'] as $periodsArray) {
        echo '<option value="' . $periodsArray['id'] . '">' . $periodsArray['name'] . '</option>';
    }
    ?>
    </select>
    <br/><br/>
    
    Program: <select name="programId" id="programSelect">
    <?php
    foreach ($periodPrograms[1]['programs'] as $programArray) {
        echo '<option value="' . $programArray['id'] . '">' . $programArray['name'] . '</option>';
    }
    ?>
    </select>
    <br/><br/>
    
    <input type="submit" value="Copy Application" onClick="copyApplication(); return false;">
</form>

<br/>
<div id="confirmCopy"></div>

<?php
echo <<<EOB
<script type="text/javascript">
    
    var domainPeriods = {$domainPeriodsJson};
    var periodPrograms = {$periodProgramsJson};  

    function setPeriodSelect() {
        $('#periodSelect').empty();
        var domainId = $('#domainSelect').val();
        var periods = domainPeriods[domainId].periods;
        for (var i = 0; i < periods.length; i++) {
            $('#periodSelect').append($('<option></option>').val(periods[i].id).html(periods[i].name));
        }
        setProgramSelect();
    }

    function setProgramSelect() {
        $('#programSelect').empty();
        var periodId = $('#periodSelect').val() + '';
        var programs = periodPrograms[periodId].programs;
        for (var i = 0; i < programs.length; i++) {
            $('#programSelect').append($('<option></option>').val(programs[i].id).html(programs[i].name));
        }
    }
    
    function copyApplication() {
        
        confirm('Confirm Application Copy');
        
        var periodId = $('#periodSelect').val();

        $.getJSON(
        'administerApplicationsSingleUpdate.php', 
        { mode: 'copyApplication', 
            applicationId: $('#applicationId').val(), 
            periodId: periodId,
            programId: $('#programSelect').val() },
        function(data){
            if (data != -1) {
                var newApplicationId = data.msg;
                if (data.error == 'duplicate user application') {
                    var confirmation = 'The applicant already has an application in the ';
                    confirmation += '<a href="administerApplications.php?applicationId=' + newApplicationId;
                    confirmation += '">target period</a>';
                } else {
                    var confirmation = 'The application has been copied to the ';
                    confirmation += '<a href="administerApplications.php?applicationId=' + newApplicationId;
                    confirmation += '">selected period</a>';
                }
                $('#confirmCopy').html(confirmation);    
            } else {
                $('#confirmCopy').html(data.error);    
            }        
        }
        );
    }

</script>
EOB;
?>