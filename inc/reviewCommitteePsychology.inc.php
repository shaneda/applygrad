<?php
/*
* WARNING: Ugly hack! Override values from reviewData.inc.php  
*/
$allowEdit = $_SESSION['A_allow_admin_edit'];
$comments = $private_comments = '';
for($i = 0; $i < count($committeeReviews); $i++)
{
    if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
    {
        $comments = $committeeReviews[$i][9];
        $private_comments = $committeeReviews[$i][12];    
    }
 } 

$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$voteVals = array(
array(5, "Excellent"),
array(4, "Very Good"),
array(3, "Good"),
array(2, "Average"),
array(1, "Poor")
);
 
?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<input name="showDecision" type="hidden" value="<?=$showDecision?>">
<input name="point2" type="hidden" value="<?=$point2?>">
<?php 
if($round == 2){
?>
    <input name="round2" type="hidden" value="<?=$round2?>">
<?php 
} 

include "../inc/special_phone_email.inc.php";
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

    <tr>
        <td width="50"><strong>Comments:</strong></td>
        <td>
        <?php 
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 80); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea); 
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Personal Comments: </strong> </td>
        <td>
        <?php  
        ob_start();
        showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 40); 
        $personalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $personalCommentsTextarea); 
        ?>    
        </td>
    </tr>
<?php 
if($round  == 3) {
?>
    <tr>
        <td width="50"><strong>Ranking:</strong></td>
        <td>
        <?php
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $voteVals);
        ?>
        </td>
    </tr>

<?php 
}
if($round != 3)
{
?>
  <tr>
    <td width="50"><strong> Next Round:</strong> </td>
    <td>
    <?
    if ($round == 2)
    {
        $fieldName = 'round3';   
    }
    else
    {
        $fieldName = 'round2';    
    }
    $voteValue = NULL;
    
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            if ($round == 2)
            {
                $voteValue = $committeeReviews[$i][39];   
            }
            else
            {
                $voteValue = $committeeReviews[$i][13];    
            }
        }
     }
    
    showEditText($voteValue, "radiogrouphoriz2", $fieldName, $allowEdit, false, 
        array(array(1,"Yes"), array(0,"No"), array(-1, 'Possible'))); ?>    </td>
  </tr>
 
<?php   
} 
?>

</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
