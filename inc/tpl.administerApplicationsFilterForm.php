<div style="padding: 2px; width: auto; background: #F7F7F7;">
<form id="filterForm" name="filterForm" action="<?php echo $returnUrl; ?>" method="GET">
<input type="hidden" id="filterUnit" name="unit" value="<?php echo $unit; ?>" />
<input type="hidden" id="filterUnitId" name="unitId" value="<?php echo $unitId; ?>" />
<?php
$isIsreeDepartment = FALSE;
$isMsrtDepartment = FALSE;
if ($unit == 'department') {
    $isIsreeDepartment = isIsreeDepartment($unitId);
    $isMsrtDepartment = isMsrtDepartment($unitId);    
}

if ($periodId) {    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
    if (is_array($periodId)) {
        foreach ($periodId as $id) {
            echo '<input type="hidden" id="filterPeriod" name="period[]" value="' . $id . '"/>';    
        }   
    } else {
        echo '<input type="hidden" id="filterPeriod" name="period" value="' . $periodId . '"/>';     
    }    
}
if ($programId) {
    echo '<input type="hidden" id="filterProgramId" name="programId" value="' . $programId . '" />';    
}
if ($unit == 'department' && $isIsreeDepartment) {
    echo <<<EOB
    <input type="hidden" name="testScoreStatus" value="allTestScore" />
    <input type="hidden" name="completionStatus" value="allCompletion" />
    <input type="hidden" name="transcriptStatus" value="allTranscript" /> 
    <input type="hidden" name="recommendationStatus" value="allRecommendation" />
EOB;
} else {
    echo <<<EOB
    <input type="hidden" name="hiddenStatus" value="allHidden" />
EOB;
}
?>
<input type="hidden" id="filterLimit" name="limit" value="<?php echo $limit; ?>" />
<input type="hidden" name="showReturnLink" value=""/>
<table border="0" cellspacing="0" cellpadding="2">
    <tr valign="middle">
        <td align="right" id="submissionStatusLabel">Submission:</td>
        <td align="left">
            <select class="filter" name="submissionStatus">
                <option value="submitted" <?php echo $submittedSelected; ?>>Submitted</option>
                <option value="unsubmitted" <?php echo $unsubmittedSelected; ?>>Unsubmitted</option> 
                <option value="allSubmission" <?php echo $allSubmissionSelected; ?>>No Filter</option>
            </select>
        </td>
        <?php
        if ( $unit === NULL || $unit == 'domain' || ($unit == 'department' && !$isIsreeDepartment) ) {
        ?>
        <td align="right" id="testScoreStatusLabel">&nbsp;&nbsp;Test Scores:</td>
        <td align="left">
            <select class="filter" name="testScoreStatus">
                <option value="anyTestIncomplete" <?php echo $anyTestIncompleteSelected; ?>>Any Incomplete</option>
                <option value="greIncomplete" <?php echo $greIncompleteSelected; ?>>GRE Incomplete</option>
                <option value="greSubjectIncomplete" <?php echo $greSubjectIncompleteSelected; ?>>GRE Subj Incomplete</option>
                <option value="toeflIncomplete"<?php echo $toeflIncompleteSelected; ?>>TOEFL Incomplete</option>
                <option value="allTestsComplete"<?php echo $allTestsCompleteSelected; ?>>All Complete</option>
                <option value="esl"<?php echo $eslSelected; ?>>ESL</option>
                <option value="allTestScore" <?php echo $allTestScoreSelected; ?>>No Filter</option>
            </select>
        </td>
        <?php
        }
        if ($unit == 'department' && $isIsreeDepartment) {
        ?>
        <td align="right" id="hiddenStatusLabel">&nbsp;&nbsp;Visibility:</td>
        <td align="left">
            <select class="filter" name="hiddenStatus">
                <option value="unhidden" <?php echo $unhiddenSelected; ?>>Not Hidden</option>
                <option value="hidden" <?php echo $hiddenSelected; ?>>Hidden</option>
                <option value="allHidden" <?php echo $allHiddenSelected; ?>>No Filter</option>
            </select>
        </td>
        <?php    
        } 
        ?>  
    </tr>
    <?php 
    if ( $unit === NULL || $unit == 'domain' || ($unit == 'department' && !$isIsreeDepartment) ) {
    ?>
    <tr valign="middle">
        <td align="right"id="completionStatusLabel">Completion:</td>
        <td align="left">
            <select class="filter" name="completionStatus">
                <option value="incomplete" <?php echo $incompleteSelected; ?>>Incomplete</option>
                <option value="complete" <?php echo $completeSelected; ?>>Complete</option> 
                <option value="allCompletion" <?php echo $allCompletionSelected; ?>>No Filter</option>
            </select>
        </td> 
        <td align="right"id="transcriptStatusLabel"> &nbsp;&nbsp;Transcripts:</td>
        <td align="left">
            <select class="filter" name="transcriptStatus">
                <option value="transcriptsIncomplete" <?php echo $transcriptsIncompleteSelected; ?>>Incomplete</option> 
                <option value="transcriptsComplete" <?php echo $transcriptsCompleteSelected; ?>>Complete</option>
                <option value="allTranscript" <?php echo $allTranscriptSelected; ?>>No Filter</option>
            </select>
        </td>  
    </tr>
    <?php
    } 
    ?>
    <tr>
        <td align="right" id="paymentStatusLabel"> &nbsp;&nbsp;Payment:</td>
        <td align="left">
            <select class="filter" name="paymentStatus">
                <option value="unpaidUnwaived" <?php echo $unpaidUnwaivedSelected; ?>>Unpaid / Unwaived</option>
                <option value="paidWaived" <?php echo $paidWaivedSelected; ?>>Paid / Waived</option>
                <option value="paid" <?php echo $paidSelected; ?>>Paid</option>
                <option value="waived" <?php echo $waivedSelected; ?>>Waived</option>
                <option value="allPayment" <?php echo $allPaymentSelected; ?>>No Filter</option>
            </select>
        </td>
        <?php 
        if ( $unit === NULL || $unit == 'domain' || ($unit == 'department' && !$isIsreeDepartment) ) {
        ?>
        <td align="right" id="recommendationStatusLabel"> &nbsp;&nbsp;Recommendations:</td>
        <td align="left">
            <select class="filter" name="recommendationStatus" >
                <option value="recommendationsIncomplete" <?php echo $recommendationsIncompleteSelected; ?>>Incomplete</option> 
                <option value="recommendationsComplete" <?php echo $recommendationsCompleteSelected; ?>>Complete</option>
                <option value="allRecommendation" <?php echo $allRecommendationSelected; ?>>No Filter</option>
            </select>
        </td>
        <?php
        } 
        ?>  
    </tr>
    
    <?php 
    if ( $unit == 'department' && !$isIsreeDepartment && !$isMsrtDepartment ) {
        $hiddenRoundInput = FALSE;
    ?>    
    <tr>
        <td></td><td></td>
        <td align="right" id="roundLabel"> &nbsp;&nbsp;Round:</td> 
        <td align="left">
            <select class="filter" name="round" >
                <option value="2" <?php echo $roundTwoSelected; ?>>Two</option>
                <option value="1" <?php echo $allRoundSelected; ?>>No Filter</option>
            </select>
        </td>
    </tr>
    <?php
    } else {
        $hiddenRoundInput = TRUE;
    } 
    ?> 
    
    <tr>
        <td colspan="4" align="right">
        <a href="<?php echo $_SERVER['REQUEST_URI']?>" onClick="setNoFilter(); return false;">Full&nbsp;List&nbsp;w/&nbsp;Filter&nbsp;Off</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo $_SERVER['REQUEST_URI']?>" onClick="restoreDefaultFilter(); return false;">Full&nbsp;List&nbsp;w/&nbsp;Default&nbsp;Filter</a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" name="submitFilter" value="Filter / Refresh Full List"/>
        </td>
    </tr>
</table>
<?php
    if ($hiddenRoundInput) {
        echo '<input type="hidden" name="round" value="1" />';
    } 
?>
</form>
</div> 