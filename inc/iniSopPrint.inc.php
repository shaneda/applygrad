<?php
$iniSop = NULL;
$iniSopQuery = "SELECT ini_sop.*, users.firstname, users.lastname 
    FROM ini_sop
    INNER JOIN application ON ini_sop.application_id = application.id
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    WHERE application_id = " . intval($appid) . "
    LIMIT 1";
$iniSopResult = mysql_query($iniSopQuery);
while($row = mysql_fetch_array($iniSopResult))
{
    $iniSop = $row;       
}

if ($iniSop)
{
?>
    <div style="font-size: 18px; margin-top: 20px;">Statement of Purpose</div>
    <div style="font-size: 16px;"><?php
        // htmlspecialchars was called on name data before db insert 
        echo $iniSop['lastname'] . ', ' . $iniSop['firstname']; 
    ?></div>
    <br/>
<?php
    if ($iniSop['objective'])
    {
    ?>
    <div style="font-size: 14px;">Briefly explain your objective in pursuing the master's degree(s) for which you are applying.</div>
    <p style="font-size: 12px;"><?php echo htmlspecialchars($iniSop['objective']); ?></p>
    <?php
    }

    if ($iniSop['background'])
    {
    ?>
    <div style="font-size: 14px;">Describe your background in engineering, computer science, and other fields particularly 
    relevant to your objectives. Include any industrial or commercial work experience.</div>
    <p style="font-size: 12px;"><?php echo htmlspecialchars($iniSop['background']); ?></p>
    <?php
    }
    
    if ($iniSop['research_experience'])
    {
    ?>
    <div style="font-size: 14px;">Outline your research experience and list any publications (if applicable).</div>
    <p style="font-size: 12px;"><?php echo htmlspecialchars($iniSop['research_experience']); ?></p>
    <?php
    }
    
    if ($iniSop['leadership_experience'])
    {
    ?>
    <div style="font-size: 14px;">Please describe any experiences where you have demonstrated leadership skills and abilities. 
    These examples may include academic, professional, extracurricular and/or community activities. Also please identify any leadership qualities 
    that you would bring to the INI that you believe would enhance and add to our community and reputation.</div>
    <p style="font-size: 12px;"><?php echo htmlspecialchars($iniSop['leadership_experience']); ?></p>
    <?php
    }
    
    if ($iniSop['sfs_interest'])
    {
    ?>
    <div style="font-size: 14px;">U.S. citizens who are admitted to the MSIS or MSIT-IS are eligible for consideration for a full scholarship 
    under the Scholarship for Service (SFS) program. If you are a U.S. citizen applying to either of these programs and interested in the SFS program 
    please describe (1) why you are interested in the SFS program, (2) how your background and preparation has prepared you for the SFS program, (3) how 
    the SFS program will help you in achieving your career goals.</div>
    <p style="font-size: 12px;"><?php echo htmlspecialchars($iniSop['sfs_interest']); ?></p>
    <?php
    }
    
    if ($iniSop['additional_info'])
    {
    ?>
    <div style="font-size: 14px;">Please provide any information you feel would be helpful to the admissions committee in making a decision. 
    For exmaple, explain any extenuating circumstances such as an illness that led to a bad semester on your transcript, etc.</div>
    <p style="font-size: 12px;"><?php echo htmlspecialchars($iniSop['additional_info']); ?></p>
    <?php
    }
}
?>