<?php

class MHCI_PrereqsMultipleCourseForm {
    
    // Fields set in constructor.
    private $studentLuUsersUsertypesId;
    private $view;                      // "student" || "reviewer" 
    private $courseType;                // "Design" || "Programming" || 
                                        // "Multi-way ANOVA"  || "Single-way ANOVA"  || 
                                        // "Multi-factor regression" || "Single-factor regression" 
    private $renderUploadNote;          // TRUE || FALSE

    // Fields for db/form data.
    private $submittedCourseData = array();    // 2D array filled from db query.
    private $unsubmittedUploadData = array();    // 2D array filled from db query.
    
    // Fields for controller/display.
    private $documentType;
    private $formSubmitted = FALSE;
    private $newCourseInProgress = FALSE;
    private $addCourseButtonLabel = "Add another course";
    private $addCourseButtonDisabled = "disabled";


    public function __construct($studentLuUsersUsertypesId, $view, $courseType, $renderUploadNote=TRUE) {

        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;
        $this->courseType = $courseType;
        $this->renderUploadNote = $renderUploadNote;
        $this->documentType = strtolower( str_replace("-", "", $courseType) ) . "Course";          

    }
    
    
    public function render() {

        $this->handleRequest();
        
        // Get the course/upload data.
        $this->getSubmittedCourseData();
        $this->getUnsubmittedUploadData();
        
        ob_start(); 
        
        $courseCounter = 1;
        foreach ($this->submittedCourseData as $course) {
            
            $courseForm = new MHCI_PrereqsCourseForm($this->studentLuUsersUsertypesId, $this->view, 
                            $this->courseType, "Course" . $courseCounter, $this->renderUploadNote);
            $courseForm->setCourseId($course['id']);
            $courseForm->render();
            echo "<hr/>";
            
            $courseCounter++;
        }
        
        if ($courseCounter == 1 || $this->formSubmitted || $this->newCourseInProgress) {
            
            $courseFormName =  "Course" . $courseCounter;
            
            if ( $this->newCourseInProgress && ($courseFormName != $_SESSION['newUploadFormName']) ) {
            
                // skip
                
            } else {
            
                $courseForm = new MHCI_PrereqsCourseForm($this->studentLuUsersUsertypesId, $this->view, 
                                $this->courseType, $courseFormName, $this->renderUploadNote);  
                $courseForm->setCourseId(-1);
                $courseForm->render();
                echo "<hr/>";
            
            }
            
        }
        
        $out1 = ob_get_contents();
        ob_end_clean();
        
        // Render button for student, enabled if at least one course is complete and none are in progress.
        if ($this->view == "student") {

            $this->getSubmittedCourseData();
            $this->getUnsubmittedUploadData();
            $submittedCourseCount = count($this->submittedCourseData);
            echo "-" . $submittedCourseCount . "-";
            echo $this->formSubmitted . "-"; 
            echo $this->newCourseInProgress . "-"; 
            
            if ( ($submittedCourseCount > 0) && (!$this->formSubmitted) && (!$this->newCourseInProgress) ) {
                
                $this->addCourseButtonDisabled = "";
                    
            }
            
            $this->renderAddCourseButton();

        }
        
        echo $out1;

    }
    

    private function handleRequest() {
    
        if ( isset($_REQUEST[$this->courseType . '_addCourseSubmit'])) {
            
            $this->formSubmitted = TRUE;
            
        }
        
        return TRUE;
        
    }
    
    

    private function renderAddCourseButton() {

        echo <<<EOB

        <input type="submit" id="{$this->courseType}_addCourseSubmit" name="{$this->courseType}_addCourseSubmit" class="bodyButton addCourseSubmit" value="{$this->addCourseButtonLabel}" {$this->addCourseButtonDisabled}/> 
        <hr /> 

EOB;
        
    }
    
    
    private function getSubmittedCourseData() {

        $query = "SELECT * FROM mhci_prereqsCourses";
        $query .= " WHERE student_lu_users_usertypes_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND course_type = '{$this->courseType}'";
        //echo $query;
        $result = mysql_query($query);
        
        $dataArray = array();
        while ($row = mysql_fetch_array($result)) {
            
            $dataArray[] = $row;
            
        }
        $this->submittedCourseData = $dataArray;       
        
    }

    
    private function getUnsubmittedUploadData() {

        $query = "SELECT mhci_prereqsDatafilesStub.*, mhci_prereqsCourseDatafiles.prereq_courses_id";
        $query .= " FROM mhci_prereqsDatafilesStub LEFT OUTER JOIN mhci_prereqsCourseDatafiles";
        $query .= " ON mhci_prereqsDatafilesStub.id = mhci_prereqsCourseDatafiles.datafile_id";
        $query .= " WHERE mhci_prereqsDatafilesStub.user_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND mhci_prereqsDatafilesStub.type = '{$this->documentType}'";
        $query .= " AND mhci_prereqsCourseDatafiles.prereq_courses_id IS NULL";
        //echo $query;
        
        $result = mysql_query($query);   
        
        $dataArray = array();
        $this->newCourseInProgress = FALSE;
        while ($row = mysql_fetch_array($result)) {
            $dataArray[] = $row;
            $this->newCourseInProgress = TRUE;
        }
        $this->unsubmittedUploadData = $dataArray;   
        
    }
    
    
}

?>
