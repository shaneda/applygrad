<?php
$gmatRecord = $gmatRecords[0];
$gmatScoreId = $gmatRecord['id'];

$gmatDate = date("m/Y", strtotime($gmatRecord['testdate']));

$gmatVerbalScore = $gmatRecord['verbalscore'];
$gmatVerbalPercentile = $gmatRecord['verbalpercentile'];

$gmatQuantitativeScore = $gmatRecord['quantitativescore'];
$gmatQuantitativePercentile = $gmatRecord['quantitativepercentile'];

$gmatTotalScore = $gmatRecord['totalscore'];
$gmatTotalPercentile = $gmatRecord['totalpercentile'];

$gmatWritingScore = $gmatRecord['analyticalwritingscore'];
$gmatWritingPercentile = $gmatRecord['analyticalwritingpercentile']; 

$receivedChecked = '';
$receivedMessage = '';
$receivedClass = 'confirm';
if ($gmatRecord['scorereceived'] == 1) {
    $receivedChecked = 'checked';
    $receivedMessage .= ' rcd';
    $receivedClass = 'confirmComplete';
}

echo <<<EOB

<form class="gmatScore" id="gmatScore_{$applicationId}">
<table cellpadding="2px" cellspacing="2px" border="0">
<tr>
    <td colspan="3">GMAT&nbsp;&nbsp;
    <input type="checkbox" class="gmatReceived" 
        id="gmatReceived_{$applicationId}_{$gmatScoreId}" {$receivedChecked} /> Received
    </td>
</tr>
<tr>
    <td colspan="3">Test Date: {$gmatDate}</td>
</tr>
<tr>
    <td></td>
    <td>Score</td>
    <td>Pctile</td>
</tr>
<tr>
    <td>Verbal</td>
    <td>
    <input type="text" size="3" class="gmatVerbalScore" 
        id="gmatVerbalScore_{$applicationId}_{$gmatScoreId}" value="{$gmatVerbalScore}" />
    </td>
    <td>
    <input type="text" size="3" class="gmatVerbalPercentile" 
        id="gmatVerbalPercentile_{$applicationId}_{$gmatScoreId}" value="{$gmatVerbalPercentile}" />
    </td>
</tr>
<tr>
    <td>Quantitative</td>
    <td>
    <input type="text" size="3" class="gmatQuantitativeScore" 
        id="gmatQuantitativeScore_{$applicationId}_{$gmatScoreId}" value="{$gmatQuantitativeScore}">
    </td>
    <td>
    <input type="text" size="3" class="gmatQuantitativePercentile" 
        id="gmatQuantitativePercentile_{$applicationId}_{$gmatScoreId}" value="{$gmatQuantitativePercentile}">
    </td>
</tr>
<tr>
    <td>Writing</td>
    <td>
    <input type="text" size="3" class="gmatWritingScore" 
        id="gmatWritingScore_{$applicationId}_{$gmatScoreId}" value="{$gmatWritingScore}">
    </td>
    <td>
    <input type="text" size="3" class="gmatWritingPercentile" 
        id="gmatWritingPercentile_{$applicationId}_{$gmatScoreId}" value="{$gmatWritingPercentile}">
    </td>
</tr>
<tr>
    <td>Total</td>
    <td>
    <input type="text" size="3" class="gmatTotalScore" 
        id="gmatTotalScore_{$applicationId}_{$gmatScoreId}" value="{$gmatTotalScore}">
    </td>
    <td>
    <input type="text" size="3" class="gmatTotalPercentile" 
        id="gmatTotalPercentile_{$applicationId}_{$gmatScoreId}" value="{$gmatTotalPercentile}">
    </td>
</tr>
<tr>
    <td colspan="3" align="left">
    <input type="submit" class="submitSmall updateGmatScore" 
        id="updateGmatScore_{$applicationId}_{$gmatScoreId}" value="Update GMAT Score"/>
    </td>
</tr>
</table> 
</form>

<script>
    $('#gmatReceivedMessage_{$applicationId}').html('{$receivedMessage}');
    $('#gmatReceivedMessage_{$applicationId}').prev("span").attr('class', '{$receivedClass}');  
</script>

EOB;
?>
