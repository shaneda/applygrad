<?php
    class FolderLabelData {
        
        private $startTime;
        private $lastTimeRan;
        private $startLabelX;
        private $startLabelY;
        private $nextStartTime;
        private $period;
        
        public function __construct() {
            $this->startTime = date("Y-m-d H:i:s");
            $this->startLabelX = 1;
            $this->startLabelY = 1;
        }
        
        public function getLastTimeRan() {
            return $this->lastTimeRan;
        }
        
        public function getStartLabelX() {
            return $this->startLabelX;
        }
        
        public function getStartLabelY() {
            return $this->startLabelY;
        }
        
        public function getStartTime() {
            return $this->startTime;
        }
        
        public function getPeriod() {
            return $this->period;
        }
        
        public function setLastTimeRan($date) {
            $this->lastTimeRan = $date;
        }
        
        public function setStartLabelX($xpos) {
            $this->startLabelX = $xpos;
        }
        
        public function setStartLabelY($ypos) {
            $this->startLabelY = $ypos;
        }
        
        public function getNextStartTime() {
            return $this->nextStartTime;
        }
        
        public function setNextStartTime($date) {
            $this->nextStartTime = $date;
        }
        
        public function setPeriod($period) {
            $this->period = $period;
        }
        
        function getDbData($period) {
        
            $sql = "SELECT * FROM folder_label_info WHERE period_id = " . $period . " ORDER BY id DESC LIMIT 1";
            
            $result = mysql_query($sql);
            if ($result) {
                while ($row = mysql_fetch_array($result)) {
                    $this->setStartLabelX($row['last_label_column']) ;
                    $this->setStartLabelY($row['last_label_row']);
                    $this->setLastTimeRan($row['last_ran_time']);
                }
            }
            //   For debugging purposes
            // $this->setLastTimeRan('2012-12-15 11:18:21');
        }
        
        function getDateTimesForLabels ($period, $end_date = false) {
            
            if (!$end_date) {
                $end_date =  $this->getStartTime();
            }
            $startTime = $this->getLastTimeRan();
            if (is_null($startTime)) {
                 $sql = "select min(a.paymentdate) as start, max(paymentdate) as end, 
                        min(a.waivedate) as startwaive, max(waivedate) as endwaive  
                        from application a
                        inner join period_application pa on pa.application_id = a.id and pa.period_id = " . $period .
                        " where ((a.paymentdate IS NOT NULL and a.paymentdate <= '" . $end_date ."')
                        or (a.waivedate IS NOT NULL and a.waivedate <= '" . $end_date .  "'))";
            } else {
                    $sql =  "select min(a.paymentdate) as start, max(paymentdate) as end, 
                            min(a.waivedate) as startwaive, max(waivedate) as endwaive 
                            from application a
                            inner join period_application pa on pa.application_id = a.id and pa.period_id = " . $period . 
                            " where ((a.paymentdate > '" . $startTime . "' and a.paymentdate <= '" . $end_date . "')
                            or (a.waivedate > '" . $startTime . "' and a.waivedate <= '" . $end_date . "'))";
                    }
            //query  

            $result = mysql_query($sql);
            if ($result) {
                while ($row = mysql_fetch_array($result)) {                
                    if (isset($row['start']) && isset($row['startwaive']))
                    {
                        if ($row['start'] > $row['startwaive'])
                        {
                            if ($row['startwaive'] < $startTime) {
                                 $start = $startTime;
                            } else {
                                  $start = $row['startwaive'];  
                                }
                        } else {
                            $start = $row['start'];
                        }
                        if ($row['end'] > $row['endwaive'])
                        {
                           $end = $row['end'];  
                        } else 
                            {
                               $end = $row['endwaive']; 
                            }
                    } else if (!isset($row['startwaive']))
                        {
                            $start = $row['start'];
                            $end = $row['end'];
                        } else
                            {
                               $start = $row['startwaive'];
                               $end = $row['endwaive'];  
                            }                 
                    // set the last time ran to the first payment date after previous last itme ran
                    $this->setLastTimeRan($start);
                    // set the time for the next start time to be the lastest payment date
                    $this->setNextStartTime($end);
                }
            }
        }
        
        function updateDbData($pdf_info) {
            $currentLabelX = $pdf_info->_COUNTX;
            $currentLabelY = $pdf_info->_COUNTY;
            $maxX = $pdf_info->_X_Number;
            $maxY = $pdf_info->_Y_Number;
            if ($currentLabelY + 1 > $maxY) { // next label moves to beginnning of next column
                $nextY = 1;    
                if ($currentLabelX + 1 > $maxX) {
                    $nextX = 1;
                } else {
                    $nextY = $currentLabelX + 1; 
                }           
            } else {
                $nextY =  $currentLabelY + 1;
                $nextX = $currentLabelX + 1;
            }
    
            $sql = "INSERT INTO folder_label_info (period_id, last_ran_time, last_label_column, last_label_row) VALUES ("
                   . $this->getPeriod() . ", '" . $this->getNextStartTime() . "', " . $nextX . ", " . $nextY . ")";
                 
            // set last run datetime to start datetime
            // update row and column of last label (or next label)
            //update database with changes
            // query 
            //query  debugbreak();
            $result = mysql_query($sql);
            if (!$result) {
                echo "update did not succeed";
            }
        }        
    }
?>
