<?php
// Initialize variables
$cmuStudentOrAlumnus = NULL;
$cmuEmployee = NULL;
$cmuAffiliationError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveCmuAffiliation();
    checkRequirementsCmuAffiliation();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$cmuAffiliationError)
{
    $cmuAffiliationQuery = "SELECT cmu_student_or_alumnus, cmu_employee FROM cmu_affiliation 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $cmuAffiliationResult = mysql_query($cmuAffiliationQuery);
    while($row = mysql_fetch_array($cmuAffiliationResult))
    {
        $cmuStudentOrAlumnus = $row['cmu_student_or_alumnus'];
        $cmuEmployee = $row['cmu_employee'];           
    }    
} 
?>

<span class="subtitle">CMU Affiliation</span>
<br/>
<br/>
Are you a current CMU student or alumnus?
<br/>
<br/>
<?php
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);
showEditText($cmuStudentOrAlumnus, "radiogrouphoriz", "cmuStudentOrAlumnus", $_SESSION['allow_edit'], false, $radioYesNo);
?>

<br/>
<br/>
Are you, your parent, or your spouse a CMU employee?
<br/>
<br/>
<?php
showEditText($cmuEmployee, "radiogrouphoriz", "cmuEmployee", $_SESSION['allow_edit'], false, $radioYesNo);
?>

<hr size="1" noshade color="#990000">

<?php
function saveCmuAffiliation()
{
    global $domainid;
    global $cmuStudentOrAlumnus;
    global $cmuEmployee;
    global $cmuAffiliationError;
    
    $cmuStudentOrAlumnus = filter_input(INPUT_POST, 'cmuStudentOrAlumnus', FILTER_VALIDATE_BOOLEAN);
    $cmuEmployee = filter_input(INPUT_POST, 'cmuEmployee', FILTER_VALIDATE_BOOLEAN);
     
    // Check for existing record
    $existingRecordQuery = "SELECT id FROM cmu_affiliation WHERE application_id = " . intval($_SESSION['appid']);
    $existingRecordResult = mysql_query($existingRecordQuery);
    if (mysql_num_rows($existingRecordResult) > 0)
    {
        // Update existing record
        $updateQuery = "UPDATE cmu_affiliation SET
            cmu_student_or_alumnus = " . intval($cmuStudentOrAlumnus) . ",
            cmu_employee = " . intval($cmuEmployee) . "
            WHERE application_id = " . intval($_SESSION['appid']);
        mysql_query($updateQuery);
    }
    else
    {
        // Insert new record
        $insertQuery = "INSERT INTO cmu_affiliation (application_id, cmu_student_or_alumnus, cmu_employee)
            VALUES (" 
            . intval($_SESSION['appid']) . "," 
            . intval($cmuStudentOrAlumnus) . ","
            . intval($cmuEmployee) . ")";
        mysql_query($insertQuery);
    }
    
    if (isEnglishDomain($domainid))
    {
        if ($cmuStudentOrAlumnus || $cmuEmployee)
        {
            // Waive application fees
            $waiveFeesQuery = "UPDATE application SET waive = 1 WHERE id = " . intval($_SESSION['appid']);    
        }
        else
        {
            // Waive application fees
            $waiveFeesQuery = "UPDATE application SET waive = 0 WHERE id = " . intval($_SESSION['appid']);    
        }
        mysql_query($waiveFeesQuery);
    } 
        

}

function checkRequirementsCmuAffiliation()
{
    global $err;
    global $cmuAffiliationError;     
    
    if (!$err && !$cmuAffiliationError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>