<? 
include_once '../inc/db_connect.php'; 
include_once '../inc/specialCasesApply.inc.php';

if (!class_exists('Domain')) {
    include '../classes/class.Domain.php';
}

$validDomain = FALSE;
if ( isset($_SESSION['domainid']) ) {
    $validDomain = Domain::isValid($_SESSION['domainid']);    
}

//GET REQUIREMENTS
$Requirements = array();
if ( isset($_SESSION['appid']) ) {
    
    $sql = "SELECT applicationreqs.id, applicationreqs.short, applicationreqs.linkname, application.name as appname, 
    lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified, lu_application_appreqs.completed 
    FROM lu_application_programs
    inner join programs on programs.id = lu_application_programs.program_id
    inner join programs_applicationreqs on programs_applicationreqs.programs_id = programs.id
    inner join applicationreqs on applicationreqs.id = programs_applicationreqs.applicationreqs_id
    inner join application on application.id = lu_application_programs.application_id
    left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
    and lu_application_appreqs.req_id = applicationreqs.id
    where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id order by applicationreqs.sortorder";
    
    $result = mysql_query($sql) or die(mysql_error());

    while($row = mysql_fetch_array( $result )) 
    {
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, $row['short']);
    array_push($arr, $row['linkname']);    
    array_push($arr, $row['compreqid']);    
    array_push($arr, formatUSdate($row['last_modified']));    
    array_push($arr, $row['completed']);
    array_push($Requirements, $arr);
    }
}

$showSubmit = true;
if(count($Requirements) == 0)
{
    $showSubmit = false;
}
?>

<div id="sideNav" class="darkbg">

<div id="sideNavLeft">
<?php                    
if ($validDomain) {
    
    if( strstr($_SERVER['SCRIPT_NAME'], "home.php" ) !== false 
        || strstr($_SERVER['SCRIPT_NAME'], "index.php" ) !== false ) {
        $homeClass = "sideNavElement sideNavActive";
    } else {
        $homeClass = "sideNavElement";    
    }
    
    if ( isset($_SESSION['usermasterid']) &&  $_SESSION['usermasterid'] > 0 ) {
        $homeText = "Application<br/>Status";
    } else {
        $homeText = "Log In";
    }
?>
    <div class="<?php echo $homeClass; ?>">
        <a href="home.php"><?php echo $homeText; ?></a>
    </div>
<?php
}

if ($validDomain && Domain::hasContent($_SESSION['domainid'], 'Instructions')) {
    
    if( strstr($_SERVER['SCRIPT_NAME'], "instructions.php" ) !== false ) {
        $instructionsClass = "sideNavElement sideNavActive";
    } else {
        $instructionsClass = "sideNavElement";
    }   
?>
    <div class="<?php echo $instructionsClass; ?>" style="border-left: 1px solid #DDD;">
        <a href="instructions.php">Instructions</a>
    </div>
<?php
}

if ( $validDomain && Domain::hasContent($_SESSION['domainid'], 'FAQ') ) {

    if( strstr($_SERVER['SCRIPT_NAME'], "faq.php" ) !== false ) {
        $faqClass = "sideNavElement sideNavActive";
    } else {
        $faqClass = "sideNavElement";
    }
?>
    <div class="<?php echo $faqClass; ?>" style="border-left: 1px solid #DDD;">
        <a href="faq.php">FAQ</a>
    </div>
    
<?php
}
?>
</div>

<div id="sideNavRight">
<?php
for($i = 0; $i < count($Requirements); $i++) {

    if($Requirements[$i][5] == 0 || $Requirements[$i][5] =="" ) {
        $showSubmit = false;
    }

    $class = "sideNavElement";
    if( strstr($_SERVER['SCRIPT_NAME'], $Requirements[$i][2] ) !== false ) {
        $class = "sideNavElement sideNavActive";
    }
?>
    <div class="<?php echo $class; ?>" style="border-left: 1px solid #DDD;">
    <a href="<?=$Requirements[$i][2]?>" title="<?=str_replace("<br>"," ", $Requirements[$i][1])?>"><?=$Requirements[$i][1]?></a>
    </div>
<?php    
}
?>

</div>

</div>