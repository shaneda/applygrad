<?php
// Initialize variables
$hasDisability = NULL;
$disabilityType = NULL;
$disabilityError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveDisability();
    checkRequirementsDisability();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$disabilityError)
{
    $disabilityQuery = "SELECT has_disability, disability_type FROM disability 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $disabilityResult = mysql_query($disabilityQuery);
    while($row = mysql_fetch_array($disabilityResult))
    {
        $hasDisability = $row['has_disability'];
        $disabilityType = $row['disability_type'];        
    }    
} 
?>

<span class="subtitle">Disabilities (Optional)</span>
<br/>
<br/>
Do you have a disability?
<br/>
<br/>
<?php
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);
showEditText($hasDisability, "radiogrouphoriz", "hasDisability", $_SESSION['allow_edit'], false, $radioYesNo);
?>

<br/>
<br/>
If yes, which of the following categories describe your disability (ies)
<br/>
<br/>
<?php 
$disabilityTypes = array(
    array(1, "Hearing"),
    array(2, "Visual"),
    array(3, "Mobility/Orthopedic Impairment"),
    array(4, "Other (not specified)")
);
showEditText($disabilityType, "listbox", "disabilityType", $_SESSION['allow_edit'], FALSE, $disabilityTypes);
?>
<hr size="1" noshade color="#990000">

<?php
function saveDisability()
{
    global $hasDisability;
    global $disabilityType;
    global $disabilityError;

    $disabilityTypeValidationOptions = array(
        'options' => array(
            'min_range' => 1,
            'max_range' => 4
        )
    );
    
    $hasDisability = filter_input(INPUT_POST, 'hasDisability', FILTER_VALIDATE_BOOLEAN);
    $disabilityType = filter_input(INPUT_POST, 'disabilityType', FILTER_VALIDATE_INT, $disabilityTypeValidationOptions);
    if ($disabilityType === FALSE)
    {
        $disabilityType = 'NULL';    
    }
    
    // Check for existing record
    $existingRecordQuery = "SELECT id FROM disability WHERE application_id = " . intval($_SESSION['appid']);
    $existingRecordResult = mysql_query($existingRecordQuery);
    if (mysql_num_rows($existingRecordResult) > 0)
    {
        // Update existing record
        $updateQuery = "UPDATE disability SET
            has_disability = " . intval($hasDisability) . ",
            disability_type = " . $disabilityType . "
            WHERE application_id = " . intval($_SESSION['appid']);
        mysql_query($updateQuery);
    }
    else
    {
        // Insert new record
        $insertQuery = "INSERT INTO disability (application_id, has_disability, disability_type)
            VALUES (" 
            . intval($_SESSION['appid']) . "," 
            . intval($hasDisability) . ","
            . $disabilityType . ")";
        mysql_query($insertQuery);
    }
}

function checkRequirementsDisability()
{
    global $err;
    global $disabilityError;     
    
    if (!$err && !$disabilityError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>