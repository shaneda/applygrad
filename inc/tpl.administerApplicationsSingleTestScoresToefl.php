<?php
/*
* Template for PBT (default).
*/

echo <<<EOB

<form class="toeflScore" id="toeflScore_{$applicationId}">
<input type="hidden" id="toeflType_{$applicationId}_{$toeflId}" value="{$toeflType}" />
<table cellpadding="2px" cellspacing="2px" border="0">
    <tr>
        <td colspan="3">TOEFL {$toeflType} &nbsp;&nbsp;
        <input type="checkbox" class="toeflReceived" 
            id="toeflReceived_{$applicationId}_{$toeflId}"  {$receivedChecked} /> Received
        </td>
    </tr>
    <tr>
        <td>Test Date</td>
        <td>
        <input type="text" size="10" class="toeflTestDate" 
            id="toeflTestDate_{$applicationId}_{$toeflId}" value="{$toeflDate}" />
        </td>
    </tr>
    <tr>
        <td>{$toeflSectionLabels[$toeflType]['section1']}</td>
        <td>
        <input type="text" size="3" class="toeflSection1Score" 
            id="toeflSection1Score_{$applicationId}_{$toeflId}" value="{$toeflSection1Score}">
        </td>
    </tr>
    <tr>
        <td>{$toeflSectionLabels[$toeflType]['section2']}</td>
        <td>
        <input type="text" size="3" class="toeflSection2Score" 
            id="toeflSection2Score_{$applicationId}_{$toeflId}" value="{$toeflSection2Score}">
        </td>
    </tr>
    <tr>
        <td>{$toeflSectionLabels[$toeflType]['section3']}</td>
        <td>
        <input type="text" size="3" class="toeflSection3Score" 
            id="toeflSection3Score_{$applicationId}_{$toeflId}" value="{$toeflSection3Score}">
        </td>
    </tr>
    <tr>
        <td>Total</td>
        <td>
        <input type="text" size="3" class="toeflTotalScore" 
            id="toeflTotalScore_{$applicationId}_{$toeflId}" value="{$toeflTotalScore}">
        </td>
    </tr>
    <tr>
        <td>{$toeflSectionLabels[$toeflType]['essay']}</td>
        <td>
        <input type="text" size="3" class="toeflEssayScore" 
            id="toeflEssayScore_{$applicationId}_{$toeflId}" value="{$toeflEssayScore}">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <input type="submit" class="submitSmall updateToeflScore" 
            id="updateToeflScore_{$applicationId}_{$toeflId}" value="Update {$toeflType} Score"/>
        </td>
    </tr>
</table> 
</form>

<script>
    $('#toeflReceivedMessage_{$applicationId}').html('{$anyReceivedMessage}');
    $('#toeflReceivedMessage_{$applicationId}').prev("span").attr('class', '{$anyReceivedClass}');  
</script>
    
EOB;
?>