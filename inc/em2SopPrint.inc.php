<?php
$em2Sop = NULL;
$em2SopQuery = "SELECT em2_sop.*, users.firstname, users.lastname 
    FROM em2_sop
    INNER JOIN application ON em2_sop.application_id = application.id
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    WHERE application_id = " . intval($appid) . "
    LIMIT 1";
$em2SopResult = mysql_query($em2SopQuery);
while($row = mysql_fetch_array($em2SopResult))
{
    $em2Sop = $row;       
}

if ($em2Sop)
{
?>
    <div style="font-size: 18px; margin-top: 20px;">Statement of Purpose</div>
    <div style="font-size: 16px;"><?php
        // htmlspecialchars was called on name data before db insert 
        echo $em2Sop['lastname'] . ', ' . $em2Sop['firstname']; 
    ?></div>
    <br/>
<?php
    if ($em2Sop['objective'])
    {
    ?>
    <div style="font-size: 14px;">Briefly explain your objective in pursuing the Emerging Media Program and the specialization paths for which you are applying.</div>
    <p style="font-size: 12px;"><?php echo $em2Sop['objective']; ?></p>
    <?php
    }

    if ($em2Sop['background'])
    {
    ?>
    <div style="font-size: 14px;">Describe your background in engineering, computer science, arts, design ahd architecture and other fields particularly
     relevant to your objectives. Include any industrial, community, or commercial work experience. </div>
    <p style="font-size: 12px;"><?php echo $em2Sop['background']; ?></p>
    <?php
    }
    
    if ($em2Sop['research_experience'])
    {
    ?>
    <div style="font-size: 14px;">Outline your research and creative experience and its relation to Emerging Media. </div>
    <p style="font-size: 12px;"><?php echo $em2Sop['research_experience']; ?></p>
    <?php
    }
    
    if ($em2Sop['ta_interest'])
    {
    ?>
    <div style="font-size: 14px;">If you wish to be considered for a Teaching Assistantship please describe your teaching experience. </div>
    <p style="font-size: 12px;"><?php echo $em2Sop['ta_interest']; ?></p>
    <?php
    }
    
    if ($em2Sop['additional_info'])
    {
    ?>
    <div style="font-size: 14px;">Please provide any information you feel would be helpful to the admissions committee in making a decision. 
    For exmaple, explain any extenuating circumstances such as an illness that led to a bad semester on your transcript, etc.</div>
    <p style="font-size: 12px;"><?php echo $em2Sop['additional_info']; ?></p>
    <?php
    }
}
?>