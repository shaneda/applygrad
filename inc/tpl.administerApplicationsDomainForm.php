<form id="domainForm" name="domainForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
    Limit to domain: <select name="unitId" onChange="document.domainForm.submit();">
    <option value="">All domains</option>
<?php
    foreach ($unitItems as $domain) {
        $selected = '';
        if ($domain['id'] == $unitId) {
            $selected = 'selected';
        }
        echo '<option value="' . $domain['id'] . '" ' . $selected . '>' . $domain['name'] . '</option>';    
    }
?>
    </select>
    <input type="hidden" id="programLimit" name="limit" value="<?php echo $limit; ?>" /> 
    <input type="hidden" id="limitUnit" name="unit" value="domain" />
    <?php
    if ($periodId) {      
        if (is_array($periodId)) {
            foreach ($periodId as $id) {
                echo '<input type="hidden" id="limitPeriod" name="period[]" value="' . $id . '"/>';     
            }   
        } else {
            echo '<input type="hidden" id="limitPeriod" name="period" value="' . $periodId . '"/>';     
        } 
    }
    ?>
    <input type="hidden" id="limitSearchString" name="searchString" value="<?php echo $searchString; ?>" /> 
    <input type="hidden" id="limitApplicationId" name="applicationId" value="<?php echo $applicationId; ?>" /> 
    <input type="hidden" name="submissionStatus" value="<?php echo $submissionStatus; ?>"/>
    <input type="hidden" name="completionStatus" value="<?php echo $completionStatus; ?>"/>
    <input type="hidden" name="paymentStatus" value="<?php echo $paymentStatus; ?>"/>
    <input type="hidden" name="testScoreStatus" value="<?php echo $testScoreStatus; ?>"/>
    <input type="hidden" name="transcriptStatus" value="<?php echo $transcriptStatus; ?>"/>
    <input type="hidden" name="recommendationStatus" value="<?php echo $recommendationStatus; ?>"/>
    <input type="hidden" name="showReturnLink" value="<?php echo $showReturnLink; ?>"/>
</form>