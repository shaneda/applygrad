<?php
    if (isModernLanguageMaDomain($_SESSION['domainid']) || isModernLanguagesPhdDomain($_SESSION['domainid'])) 
    { ?>
        <span class="subtitle">How did you hear about our program?</span>
    <?php
    } 
    else 
    { ?>
        <span class="subtitle">How did you hear about our program(s)?</span>    
    <?php
    } 
    ?>
<br/><br/>
<?php
if (isModernLanguageMaDomain($_SESSION['domainid'])) {
    $dietrichReferrals = array(
    array("Graduate school search site", "Graduate school search site (e.g., petersons.com or gradschools.com)"),
    array("Own internet search", "My own internet search"),
    array("Faculty member or advisor", "Mentioned by a faculty member or advisor"),
    array("Friend or colleague", "Mentioned by a friend or colleague"),
    array("Print materials", "Print materials on the program"),
    array("Electronic Mailing", "Received electronic mail on the program")
);    
} 
else 
{
$dietrichReferrals = array(
    array("Graduate school search site", "Graduate school search site (e.g., petersons.com or gradschools.com)"),
    array("Own internet search", "My own internet search"),
    array("Faculty member or advisor", "Mentioned by a faculty member or advisor"),
    array("Friend or colleague", "Mentioned by a friend or colleague"),
    array("Print materials", "Print materials on the program")
);
}

showEditText($referral, "listbox", "lbReferral", $_SESSION['allow_edit'],false, $dietrichReferrals);
?>
<br/>
<br/>
If a graduate school search site, please specify:
<br/>
<br/>
<?php 
showEditText($referral, "textbox", "txtReferral", $_SESSION['allow_edit'],false,null,true,35,35); 
?>
<hr size="1" noshade color="#990000">