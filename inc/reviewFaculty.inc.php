<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';
$allowEdit = TRUE;

$admitVoteValues = array(
    array("DEFINITELY", "DEFINITELY"),
    array("Probably Yes", "Probably Yes"),
    array("Probably No", "Probably No"),
    array("DEFINITELY NOT", "DEFINITELY NOT")
); 

$rankingValues = $arr = array(
    array(0, "This is not a top 5 choice"),
    array(1, "1st Choice"),
    array(2, "2nd Choice"),
    array(3, "3rd Choice"),
    array(4, "4th Choice"),
    array(5, "5th Choice")   
);

$yesNoValues = $arr = array(
    array(0, "No"), 
    array(1, "Yes")
);  

for($i = 0; $i < count($committeeReviews); $i++) {
    if( $committeeReviews[$i][4] == $reviewerId 
        && $committeeReviews[$i][29] == "" 
        && $committeeReviews[$i][23] == $round) 
    {
        $admit_vote = $committeeReviews[$i][15];
        $pertinent_info = $committeeReviews[$i][18];
        $rank = $committeeReviews[$i][31];
        $recruited = $committeeReviews[$i][16];
        $grad_name = $committeeReviews[$i][17];
        $advise_time = $committeeReviews[$i][19];
        $commit_money = $committeeReviews[$i][20];
        $fund_source = $committeeReviews[$i][21];
        
        $background = $committeeReviews[$i][6];
        $grades = $committeeReviews[$i][7];
        $statement = $committeeReviews[$i][8];
        $comments = $committeeReviews[$i][9];
        $point = $committeeReviews[$i][10];
    }
}
?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="comments" type="hidden" value="<?=$comments?>">
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="facVote" type="hidden" value="1">

<table width="500" border="0" cellspacing="0" cellpadding="2">
    <colgroup>
        <col />
    </colgroup>

    <tr>
        <td class="label">
        PART I. &nbsp;&nbsp; INFORMATION FOR THE ADMISSIONS COMMITTEE.
        <br /><br /> 
        </td>
    </tr>
    
    <tr valign="middle"> 
        <td class="label">
        Do you think we should admit this applicant?  Select one, and explain 
        why by provding other pertinent information that might be useful to the 
        Admissions Committee and your colleagues (e.g. direct knowledge of the 
        student or his/her research, calibration of the recommenders, etc.)
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        $admitVote = NULL; 
        showEditText($admit_vote, "radiogrouphoriz2", "admit_vote", $allowEdit, false, $admitVoteValues); 
        ?>
        <br />
        <?php 
        showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 60); 
        ?> 
        </td>
    </tr>

    <tr valign="middle"> 
        <td class="label">
        Please rank at least your top 3 choices. This applicant is my:
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        showEditText($rank, "radiogrouphoriz2", "rank", $allowEdit, false, $rankingValues); 
        ?>
        </td>
    </tr>    
    
    <tr valign="middle"> 
        <td class="label">
        I would be willing to play an active role in recruiting this student.
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        showEditText($recruited, "radiogrouphoriz2", "recruited", $allowEdit, false, $yesNoValues); 
        ?>
        </td>
    </tr> 

    <tr valign="middle"> 
        <td class="label">
        I suggest the following graduate students as possible recruiting contacts for this student:
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40);
        ?>
        </td>
    </tr>     

    <tr>
        <td class="label">
        <br/>
        PART II. &nbsp;&nbsp; INFORMATION FOR THE DEPARTMENT CHAIR.
        <br /><br /> 
        In the event that this student is considered on the borderline, your answers 
        to the following questions could make a difference in the decision the department 
        chair makes on whether or not to offer admission to the student.
        <br /><br />
        </td>
    </tr>

    <tr valign="middle"> 
        <td class="label">
        I would commit my time and energy to advise this student.
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        showEditText($advise_time, "radiogrouphoriz2", "advise_time", $allowEdit, false, $yesNoValues);
        ?>
        </td>
    </tr>

    <tr valign="middle"> 
        <td class="label">
        I would commit my money to advise this student.
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        showEditText($commit_money, "radiogrouphoriz2", "commit_money", $allowEdit, false, $yesNoValues);
        ?>
        </td>
    </tr>

    <tr valign="middle"> 
        <td class="label">
        If yes, please state your probable source of funding.
        </td>
    </tr> 
    <tr valign="middle">
        <td>
        <?php
        showEditText($fund_source, "textarea", "fund_source", $allowEdit, false, 60);
        ?>
        </td>
    </tr>

<?php
if(count($myAreasAll) > 0){
?>
    <tr>
        <td class="label" colspan="2">
        Please discuss with your colleagues and enter a score that reflects the strength of this applicant 
        within the research areas listed below.
        </td>
    </tr>  
    <?php  
    for($j = 0; $j < count($myAreasAll); $j++){ 
    ?>    
      <tr>
      <td colspan="2">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#eeeeee" width="100px"><strong>
              <?=$myAreasAll[$j][1]?>:</strong>
            </td>
            <td>
            <?
            $val = 0;
            for($i = 0; $i < count($committeeReviews); $i++)
            {
                if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
                {
                    $val = $committeeReviews[$i][10];
                }
             }
            
            if($thisDept == 3 || $thisDept == 66)
            {
                echo "Enter a number from 5.000 to 10: ";
                showEditText($val, "textbox", "point_".$myAreasAll[$j][0], $allowEdit, false);
            }else
            {
                ?><table border='0'>
                <tr>
                  <td>Low-</td>
                  <td>
                <? 
                showEditText($val, "radiogrouphoriz3", "point_".$myAreasAll[$j][0], $allowEdit, false, $csdFacVoteVals);
                ?></td>
                  <td>-High</td>
                </tr></table><?
            }
            ?></td>
            </tr>
        </table>
      </td>
      </tr>
<?php
    }
}
?>
    
    <tr>
        <td colspan="4">
        <!-- 
        <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
        &nbsp;&nbsp;
        -->
        <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>
        </td>
    </tr>
 
</table>
