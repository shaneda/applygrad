<?php
$toeflSectionLabels = array(
    'IBT' => array('section1' => 'Speaking', 'section2' => 'Listening', 'section3' => 'Reading', 'essay' => 'Writing'),
    'PBT' => array('section1' => 'Section 1', 'section2' => 'Section 2', 'section3' => 'Section 3', 'essay' => 'TWE'),
    'CBT' => array('section1' => 'Structure/Writing', 'section2' => 'Listening', 'section3' => 'Reading', 'essay' => 'Essay')
    );

echo <<<EOB
<table cellpadding="12px" border="0">
    <tr valign="top">    
EOB;
    echo '<td>';
    if ( count($greRecords) > 0 && (strtotime($greRecords[0]['testdate']) > 0) ) {
        include '../inc/tpl.administerApplicationsSingleTestScoresGre.php';    
    } else {
        echo 'No GRE General score found.';
    }
     
    echo '</td>';
     
    foreach ($gmatRecords as $gmatRecord) {
        echo '<td style="border-left: 1px dotted #CCCCCC;">';
        include '../inc/tpl.administerApplicationsSingleTestScoresGmat.php';
        echo '</td>';
    }

    foreach ($greSubjectRecords as $greSubjectRecord) {
        echo '<td style="border-left: 1px dotted #CCCCCC;">';
        include '../inc/tpl.administerApplicationsSingleTestScoresGreSubject.php';
        echo '</td>';
    }

    foreach ($toeflRecords as $toeflRecord) {
        if ($toeflRecord['testdate'] != NULL 
            || $toeflRecord['section1'] != NULL
            || $toeflRecord['section2'] != NULL
            || $toeflRecord['section3'] != NULL
            || $toeflRecord['essay'] != NULL
            || $toeflRecord['total'] != NULL) 
        {
            $toeflId = $toeflRecord['id'];
            $toeflType = $toeflRecord['type']; 
            if ($toeflRecord['testdate']) {
                $toeflDate = date("m/Y", strtotime($toeflRecord['testdate']));    
            } else {
                $toeflDate = '';        
            }
            $toeflSection1Score = $toeflRecord['section1'];
            $toeflSection2Score = $toeflRecord['section2'];     
            $toeflSection3Score = $toeflRecord['section3'];
            $toeflEssayScore = $toeflRecord['essay'];
            $toeflTotalScore = $toeflRecord['total'];

            if( !isset($anyReceived) ) {
                $anyReceived = 0;
            }
            $receivedChecked = '';
            if ($toeflRecord['scorereceived'] == 1) {
                $anyReceived = 1;
                $receivedChecked = 'checked';
            }
            $anyReceivedMessage = '';
            $anyReceivedClass = 'confirm';
            if ($anyReceived) {
                $anyReceivedMessage = ' rcd';
                $anyReceivedClass = 'confirmComplete';    
            }
         
            echo '<td style="border-left: 1px dotted #CCCCCC;">'; 
            switch($toeflType) {
                case 'IBT':
                    include '../inc/tpl.administerApplicationsSingleTestScoresToeflIbt.php';
                    break; 
                case 'CBT':
                    include '../inc/tpl.administerApplicationsSingleTestScoresToeflCbt.php';
                    break;
                default:
                    include '../inc/tpl.administerApplicationsSingleTestScoresToefl.php';
            }
            echo '</td>';
        }
    }

    if (count($ieltsRecords) > 0) {
        echo '<td style="border-left: 1px dotted #CCCCCC;">';
        include '../inc/tpl.administerApplicationsSingleTestScoresIelts.php'; 
        echo '</td>';      
    }   
    
    if (isSocialDecisionSciencesDepartment($departmentId)) {
        echo '<td style="border-left: 1px dotted #CCCCCC;">';
        include '../inc/tpl.administerApplicationsSingleTestScoresWaiveToefl.php'; 
        echo '</td>';      
    }         

echo <<<EOB
    </tr>
</table>   
EOB;
 
?>