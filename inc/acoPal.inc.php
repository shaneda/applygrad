<?php
// Initialize variables
$aco = NULL;
$pal = NULL;
$acoPalErr = '';

// Validate and save posted data
if (isset($_POST['acoPalSubmitted']))
{
    saveAcoPal();
    //checkRequirementsAcoPal();
}
// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$acoPalErr)
{
    $acoPalQuery = "SELECT aco, pal FROM aco_pal 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $acoPalResult = mysql_query($acoPalQuery);
    while($row = mysql_fetch_array($acoPalResult))
    {
        $aco = $row['aco'];
        $pal = $row['pal'];           
    }    
}      
?>
<div>
If you would like to be considered for the ACO and/or PAL Ph.D. programs please 
select below. Adding these do not count as an additional Ph.D. selections.
<br>
<?php
/* 
* Add onlick event handlers to checkboxes for inclusion on programs.php, 
* because programs has no page-level submit input.
*/
$acoPalCheckboxReplace = ' onClick="form1.submit();">';
ob_start();
showEditText($aco, "checkbox", "aco", $_SESSION['allow_edit'], false);
$acoCheckbox = ob_get_contents();
ob_end_clean();
$acoCheckbox = str_replace('>', $acoPalCheckboxReplace, $acoCheckbox);    
echo $acoCheckbox . ' ACO';

echo '<br>';

ob_start();
showEditText($pal, "checkbox", "pal", $_SESSION['allow_edit'], false);
$palCheckbox = ob_get_contents();
ob_end_clean();
$palCheckbox = str_replace('>', $acoPalCheckboxReplace, $palCheckbox);    
echo $palCheckbox . ' PAL';
?>

<input type="hidden" name="acoPalSubmitted" value="1">
</div>
<hr size="1" noshade color="#990000">
<br/>

<?php
function saveAcoPal()
{
    global $aco;
    global $pal;
    global $acoPalErr;
    
    $aco = filter_input(INPUT_POST, 'aco', FILTER_VALIDATE_BOOLEAN);
    $pal = filter_input(INPUT_POST, 'pal', FILTER_VALIDATE_BOOLEAN);
    
    $updateQuery = "INSERT INTO aco_pal (application_id, aco, pal)
            VALUES(" 
            . $_SESSION['appid'] . ","
            . intval($aco) . ","
            . intval($pal) . ")
            ON DUPLICATE KEY UPDATE 
            aco = " . intval($aco) . ",
            pal = " . intval($pal);
    mysql_query($updateQuery);
}

//function checkRequirementsAcoPal()
//{
//    global $err;
//    global $acoPalError;     
//    
//    if (!$err && !$acoPalError)
//    {
//        updateReqComplete("programs.php", 1);
//    }
//    else
//    {
//        updateReqComplete("programs.php", 0);    
//    }    
//}
?>