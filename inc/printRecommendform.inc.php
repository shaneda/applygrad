<?php
$recommendformQuestions = array(
    'MSE' => array(                     // index is arbitrary "department key"
        1 => array(                         // index is recommendforms.form_id
            1 => array(                         // index is recommendforms.question_id
                0 => 'Intellectual ability',        //  index is recommendforms.question_key
                1 => 'Breadth of general knowledge',
                2 => 'Depth of knowledge in the undergraduate area',
                3 => 'Motivation &amp; initiative',
                4 => 'Maturity and stability',
                5 => 'Ability to work independently',
                6 => 'Ability to speak and write English',
                7 => 'Creativity',
                8 => 'Clarity of goals for graduate study',
                9 => 'Overall potential for graduate study'
            ),
            2 => array(
                0 => 'In making this evaluation, what group are you using as a comparison?'
            ),
            3 => array(
                0 => 'How long have you known this applicant, and in what capacity?',
            ),
            4 => array(
                0 => 'Please describe the particular strengths and weaknesses of this applicant. Also, describe any special talents or experience. If you can find nothing to say, please, give the applicant&rsquo;s strongest trait and weakest trait.',
            ),
            5 => array(
                0 => 'If you have worked with or supervised this applicant on a project, please describe the project and give an evaluation of the applicant&rsquo;s performance.',
            ),
            6 => array(
                0 => 'Additional Information:'
            )  
        ),
        2 => array(
            1 => array(
                0 => 'Requirements analysis and specification',
                1 => 'Software design',
                2 => 'Software implementation',
                3 => 'Software testing',
                4 => 'Documentation',
                5 => 'Project management',
                6 => 'Reuses previous work products where appropriate',
                7 => 'Learns from experience',
                8 => 'Technical depth/proficiency',
                9 => 'Ability to analyze and solve problems',
                10 => NULL,
                11 => 'Capacity for working with others',
                12 => 'Initiative',
                13 => 'Personal integrity',
                14 => 'Professional commitment',
                15 => 'Taste in design decisions'
            ),
            2 => array(
                0 => 'In making this evaluation, what group are you using as a comparison?'
                ),
            3 => array(
                0 => 'How long have you known this applicant, and in what capacity?',
            ),
            4 => array(
                0 => 'Please describe the particular strengths and weaknesses of this applicant. Also, describe any special talents or experience. If you can find nothing to say, please, give the applicant&rsquo;s strongest trait and weakest trait.',
            ),
            5 => array(
                0 => 'If you have worked with or supervised this applicant on a project, please describe the project and give an evaluation of the applicant&rsquo;s performance.',
            ),
            6 => array(
                0 => 'Additional Information:'
            )  
        )
    ),
    'MS-HCII' => array(
        2 => array(
            1 => array(
                0 => 'Follows established group processes, procedures, and guidelines',
                1 => 'Does his or her fair share of the work',
                2 => 'Does high quality work',
                3 => 'Meets agreed upon task schedules',
                4 => 'Demonstrates initiative within his/her assigned area of responsibility',
                5 => 'Interacts with others in an effective, tactful and pleasant manner',
                6 => 'Expresses ideas in a clear, concise manner',
                7 => 'Regularly asks for, listens to and understands others\' viewpoints'
            )
        )
    ),
    'SDS' => array(
        7 => array(
            1 => array(
                0 => 'Intellectual ability',
                1 => 'Ability to work with others',
                2 => 'Judgment and Maturity',
                3 => 'Motivation to learn',
                4 => 'Originality and creativity',
                5 => 'Persistence in completing complex tasks',
                6 => 'Ability to work independently',
                7 => 'Effectiveness of written communications',
                8 => 'Effectiveness of oral communications',
                9 => 'Willingness to learn from criticism',
                10 => 'Overall potential for graduate study'
            )
        )
    ),
    'History' => array(
        5 => array(
            1 => array(
                0 => 'Effectiveness of Written Communications',
                1 => 'Effectiveness of Oral Communications',
                2 => 'Originality and Creativity',
                3 => 'Motivation to learn',
                4 => 'Willingness to Learn from Criticism',
                5 => 'Judgment and Maturity',
                6 => 'Persistence in Completing Work',
                7 => 'Scholarly Sophistication'
            )
        )
    ),
    'Modern-Languages' => array(
        6 => array(
            1 => array(
                0 => 'Comprehension',
                1 => 'Speaking/Lecturing',
                2 => 'Reading',
                3 => 'Writing'
            )
        ),
        8 => array(
            1 => array(
                0 => 'Intellectual ability',
                1 => 'Scholarly sophistication',
                2 => 'Originality and creativity',
                3 => 'Motivation to learn',
                4 => 'Persistence in completing complex tasks',
                5 => 'Judgement and maturity',
                6 => 'Willingness to learn from criticism',
                7 => 'Effectiveness of written communications',
                8 => 'Effectiveness of oral communications'
            )
        )
    ),
    'INI' => array(
        9 => array(
            1 => array(
                0 => 'Motivation and Initiative',
                1 => 'English Communication Skills',
                2 => 'Creativity',
                3 => 'Intellectual Ability',
                4 => 'Overall Potential for Graduate Study'
            )
        )
    )
);

$recommendformAnswerKeys = array(
    'MSE' => array(         // index is arbitrary "department key"
        1 => array(             // index is recommendforms.form_id
            1 => array(             // index is recommendforms.question_id
                0 => '',                //  index is recommendforms.question_key
                1 => 'Top 1%',
                2 => 'Top 5%',
                3 => 'Top 10%',
                4 => 'Top 20%',
                5 => 'Top 50%',
                6 => 'Below 50%',
                7 => 'Insufficient Information'
            )
        ),
        2 => array(
            1 => array(
                0 => '',
                1 => 'Top 1%',
                2 => 'Top 5%',
                3 => 'Top 10%',
                4 => 'Top 20%',
                5 => 'Top 50%',
                6 => 'Below 50%',
                7 => 'Insufficient Information'
            )
        )
    ),
    'MS-HCII' => array(
        2 => array(
            1 => array(
                0 => '',
                1 => '5',
                2 => '4',
                3 => '3',
                4 => '2',
                5 => '1',
                6 => 'Unknown'
            )
        )
    ),
    'SDS' => array(
        7 => array(
            1 => array(
                0 => '',
                1 => 'Superior - Highest 5%',
                2 => 'Outstanding - Next Highest 10%',
                3 => 'Above Average - Next Highest 15%',
                4 => 'Average - Middle 30%',
                5 => 'Below Average - Lowest 40%',
                6 => 'N/A or Insufficient Information'
            )
        )
    ),
    'History' => array(
        5 => array(
            1 => array(
                0 => '',
                1 => 'Superior - Highest 5%',
                2 => 'Outstanding - Next Highest 10%',
                3 => 'Above Average - Next Highest 15%',
                4 => 'Average - Middle 30%',
                5 => 'Below Average - Lowest 40%',
                6 => 'N/A or Insufficient Information'
            )
        )
    ),
    'Modern-Languages' => array(
        8 => array(
            1 => array(
                0 => '',
                1 => 'Superior - Highest 5%',
                2 => 'Outstanding - Next Highest 10%',
                3 => 'Above Average - Next Highest 15%',
                4 => 'Average - Middle 30%',
                5 => 'Below Average - Lowest 40%',
                6 => 'N/A or Insufficient Information'
            )
        )
    ),
    'INI' => array(
        9 => array(
            1 => array(
                0 => '',
                1 => 'Top 1%',
                2 => 'Top 5%',
                3 => 'Top 10%',
                4 => 'Top 20%',
                5 => 'Top 50%',
                6 => 'Bottom 50%',
                7 => 'Insufficient Information'
            )
        )
    )
);

$modLangAnswerKeys = array(
    6 => array(             // recommendforms.form_id
        1 => array(         // recommendforms.question_id
            0 => array(     // recommendforms.question_key
                0 => '',
                1 => 'No usable proficiency',
                2 => 'Adequate comprehesion for normal daily needs',
                3 => 'Able to comprehend answers in response to questions relating to field of specialization',
                4 => 'Able to understand lectures in field of specialization',
                5 => 'Able to understand group discussions on nontechical subjects',
                6 => 'Able to undertand foreign language news broadcasts',
                7 => 'Comprehehension at level of native speaker'
            ),
            1 => array(
                0 => '',
                1 => 'No usable proficiency',
                2 => 'Able to speak adequately for daily needs',
                3 => 'Able to conduct interview in field of specialization',
                4 => 'Able to deliver lectures form notes or prepared texts but may need assistance of interpreter to engage group discussion that may follow',
                5 => 'Able to deliver lectures form notes or prepared texts in field of specialization and engage in following without assistance',
                6 => 'Able to speak extemporaneously on nontechnical subjects in general and in the area of specialization: able to discuss field of specialization with foreign colleagues',
                7 => 'Speaking ability equivalent to that of educated speaker'
            ),
            2 => array(
                0 => '',
                1 => 'No usable proficiency',
                2 => 'Able to read typed or printed mateiral of a nonspecialized nature, such as simple signs and messages',
                3 => 'Able to read elementary material in own and related fields, though at a slow rate of speed',
                4 => 'Able to read general material in own and related fields, though with some reliance on a dictionary',
                5 => 'Reading ability of educated native speaker'
            ),
            3 => array(
                0 => '',
                1 => 'No practical facility',
                2 => 'Able to write simple messages, nonspecialized letters',
                3 => 'Able to draft academic materials in field of specialization with major editing by native speaker',
                4 => 'Able to prepare written material in field of specialization with minimal editing by native speaker',
                5 => 'Writing ability of educated native speaker'
            )
        )
    )
);

// indices follow pattern above
$recommendformScales = array(
    'MS-HCII' => array(
        2 => array(
            1 => '1 = poor, would not recommend to 5 = excellent, would highly recommend'
        )
    )    
);

$departmentKey = 'MSE';
foreach ($depts as $department) {
    $departmentName = $department[1];
    if ($departmentName == 'MS-HCII' 
        || $departmentName == 'MS-HCII-Portugal'
        || $departmentName == 'METALS') 
    {
        $departmentKey = 'MS-HCII';
        break;    
    }
    if ($departmentName == 'Social and Decision Sciences') {
        $departmentKey = 'SDS';
        break;    
    }
    if ($departmentName == 'History') {
        $departmentKey = 'History';
        break;    
    }
    if ($departmentName == 'Modern-Languages' || $departmentName == 'Modern Language - MA') {
        $departmentKey = 'Modern-Languages';
        break;    
    }
    if ($departmentName == 'INI' || $departmentName == 'INI-Kobe') {
        $departmentKey = 'INI';
        break;    
    }
}

if ( isset($recommendId) && $recommendId ) {

    $recommendForms = getRecommendformAnswers($recommendId);
    
    $recommenders = array();
    foreach ($myRecommenders as $recommender) {
        if ($recommendId == $recommender[0]) {
            $recommenderName = $recommender[2] . ' ' . $recommender[3];
            $recommenderTitle = $recommender[4];
            if ( isset($recommenderTitle) ) {
                if ( isset($recommender[5]) ) {
                    $recommenderTitle .=  ', ' . $recommender[5];    
                }    
            } else {
                $recommenderTitle = $recommender[5];     
            }
            $recommenders[$recommendId] = array(
                    'recommender' => $recommenderName,
                    'title' => $recommenderTitle
                );   
        }
    }    

} else {
    
    $recommenders = array();
    $recommendForms = array();
    foreach ($myRecommenders as $recommender) {
        
        $recUserId = $recommender[1];
        if ($recUserId != -1) {
            
            $recommendId = $recommender[0];
            $recommenderName = $recommender[2] . ' ' . $recommender[3];
            $recommenderTitle = $recommender[4];
            if ( isset($recommenderTitle) ) {
                if ( isset($recommender[5]) ) {
                    $recommenderTitle .=  ', ' . $recommender[5];    
                }    
            } else {
                $recommenderTitle = $recommender[5];     
            }
            $recommenders[$recommendId] = array(
                    'recommender' => $recommenderName,
                    'title' => $recommenderTitle
                );    
        
            $recommendformQuery = "SELECT * FROM recommendforms
                                    WHERE recommend_id = " . $recommendId . "
                                    ORDER BY form_id, question_id, question_key";
            $recommendformResult = mysql_query($recommendformQuery);
            while ( $row = mysql_fetch_array($recommendformResult) ) {
                $recommendId = $row['recommend_id'];
                $formId = $row['form_id'];
                $questionId = $row['question_id'];
                $questionKey = $row['question_key'];
                $response = $row['response'];
                $recommendForms[$recommendId][$formId][$questionId][$questionKey] = $response;
            }
        }
    }
}

if (count($recommendForms) > 0) {

    if ( !isset($mainTableWidth) ) {
        $mainTableWidth = '700px';
    } elseif (strstr($mainTableWidth, '%') === FALSE && strstr($mainTableWidth, 'px') === FALSE) {
        $mainTableWidth .= 'px';
    }
    
    ?>
    <h2 style="page-break-before: always;">Recommendation Form Responses</h2>
    <?php
    
    if ( isset($recommendId) && $recommendId && isset($applicationId) && $applicationId ) {
        
        $progams = array();
        foreach ($myPrograms as $myProgram) {
            $programs[] = $myProgram[1] . " ".$myProgram[2] . " ".$myProgram[3];
        }
        
        echo '<b>Applicant:</b> ' . $lName . ', ' .$fName . ' ' .  $mName . ' ' . $title;
        echo '<br><b>Programs:</b> ' . implode(',', $programs);
        echo '<br><br>';
    }
    
    foreach($recommendForms as $recommendId => $formArray) {
        
        echo '<b>' . $recommenders[$recommendId]['recommender'] . '</b>';
        if ( isset($recommenders[$recommendId]['title']) ) {
            echo '<br/>' . $recommenders[$recommendId]['title'];    
        }
        echo '<ol style="width: ' . $mainTableWidth. ';">';
        foreach ($formArray as $formId => $questionArray) {
            foreach($questionArray as $questionId => $questionKeyArray) {
                foreach ($questionKeyArray as $questionKey => $response) {
                    if (isset($recommendformQuestions[$departmentKey][$formId][$questionId][$questionKey]))
                    {
                        $question = $recommendformQuestions[$departmentKey][$formId][$questionId][$questionKey];
                    }
                    if ( isset($question) ) {
                        echo '<li style="margin-top: 4px;"><i>' . $question . '</i>';
                        if ($response) {  
                            if ($questionId == 1) {
                                settype($response, 'integer');
                            }
                            if ( is_int($response) ) {
                                if ($formId == 6)
                                {
                                    echo ': ' . $modLangAnswerKeys[$formId][$questionId][$questionKey][$response];    
                                }
                                else
                                {
                                    echo ': ' . $recommendformAnswerKeys[$departmentKey][$formId][$questionId][$response];    
                                }
                            } else {
                                echo '<br/>' . $response;    
                            }
                        }
                        echo '</li>';    
                    }        
                } 
            }
        }
        echo '</ol>';    
    }
    ?>
    <?php
}

function getRecommendformAnswers($recommendId) {
    
    $recommendformQuery = "SELECT * FROM recommendforms
                            WHERE recommend_id = " . $recommendId . "
                            ORDER BY form_id, question_id, question_key";
    $recommendformResult = mysql_query($recommendformQuery);
    
    $recommendForms = array();
    while ( $row = mysql_fetch_array($recommendformResult) ) {
        $recommendId = $row['recommend_id'];
        $formId = $row['form_id'];
        $questionId = $row['question_id'];
        $questionKey = $row['question_key'];
        $response = $row['response'];
        $recommendForms[$recommendId][$formId][$questionId][$questionKey] = $response;
    } 
    
    return $recommendForms;   
}

//print_r($recommenders);
//print_r($recommendForms);

/*
$applicationId = NULL;
if ( isset ($appid) ) {
    $applicationId = $appid;
} elseif ( isset($_GET['applicationId']) ) {
    $applicationId = $_GET['applicationId'];
} elseif ( isset($_SESSION['appid']) ) {
    $applicationId = $_SESSION['appid'];
}
if ($applicationId) {

    $recommendformQuery = "SELECT 
        recommend.id as recommend_id,
        recommendforms.*,
        users.title,
        users.firstname,
        users.lastname,
        users_info.company
        FROM application
        INNER JOIN recommend ON application.id = recommend.application_id
        INNER JOIN recommendforms ON recommend.id = recommendforms.recommend_id 
        INNER JOIN lu_users_usertypes ON recommend.rec_user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
        WHERE application.id = " . $applicationId . " 
        AND recommend.rec_user_id != -1
        ORDER BY recommend.id, recommendforms.question_id, recommendforms.question_key";    

    echo $recommendformQuery . '<br/>'; 
    $recommendformResult = mysql_query($recommendformQuery);
    while ( $row = mysql_fetch_array($recommendformResult) ) {
        echo $row['recommend_id'] . ' - ' . $row['question_id'] . ' - ' . $row['question_key'] .  '<br/>';
    }
}
*/
?>