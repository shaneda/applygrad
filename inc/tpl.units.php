<?php
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Administrative Units';

$pageCssFiles = array(
    '../css/units.css',
    '../css/jquery.tooltip.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/ui.core.js',
    '../javascript/ui.datepicker.min.js',
    '../javascript/jquery.dimensions.min.js',
    '../javascript/jquery.tooltip.min.js',
    '../javascript/units.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br/>
<div id="bread" class="menu">
<ul>
    <li class="first"><a href="home.php">Home</a>
    <ul>
        <li>&#187;</li>
    </ul>
    </li>
</ul>
</div>
<span class="title" style="font-size: 18px; margin:5px; padding: 5px;"><?php echo $pageTitle; ?></span>
<br/> 
<div style="margin:5px; padding: 5px;">


<div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%">
<?php 
if ($allowAdmin) {
    $newUnitForm->display();    
}
$unitMenu = '';
foreach ($rootAdminUnitIds as $rootAdminUnitId) {
    $rootUnit = new Unit($rootAdminUnitId);    
    $unitMenu .= makeUnitMenu($rootUnit, $selectedUnitId);
}
echo $unitMenu;
?>
</div>

<div id="table" style="margin-left: 20px; float: left; clear: right; width: 75%;">

        <div style="margin: 10px;">
        <?php
        if (!$allowAdmin) {
        
            // $unitMenu will be empty
            echo '<p>You are not authorized to manage administrative units.</p>';
                
        } elseif ($selectedUnitId && !in_array($selectedUnitId, $allAdminUnitIds)) {
            
            // The user has somehow navigated to a unit for which she doesn't have admin rights.
            echo '<p>You are not authorized to manage ' . $selectedUnit->getName() . '.</p>';
            
        } elseif ($selectedUnitId || $edit == 'newUnit') {

            // Change the renderer templates to make each form have the same column widths.
            $renderer = $unitForm->defaultRenderer(); 
            $renderer->setFormTemplate(
               '<form{attributes}>
                  {hidden}
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                  {content}
                  </table>
               </form>'
            );
            $renderer->setElementTemplate(
            '<tr valign="top">
                <td width="30%" align="right">
                    <!-- BEGIN required --><span style="color: #ff0000;">*</span><!-- END required --><b>{label}</b>
                </td>
                <td width="70%" align="left">
                    <!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->{element}
                </td> 
            </tr>'  
            );
            ?>               
            <h2 style="color: #990000;">
            <?php
            if ($selectedUnitId) {
                echo $selectedUnit->getName(); 
                $isProgram = $selectedUnit->isProgram();
                /*
                if ( $isProgram ) {
                    echo ' (program)';
                }
                */               
            } elseif ($edit == 'newUnit') {
                echo 'Add New Unit';    
            }
            ?>
            </h2>
            
            <table width="100%" border="0" cellspacing="5" cellpadding="10">
            <tr valign="top">
            <td width="50%">
                
                <div class="legend">Unit Details</div>
                <div class="outer">
                <div class="fieldset">
                <?php
                $unitForm->display();
                ?>
                </div>
                </div>           

            <?php
            if ($edit != 'newUnit') {
            ?>
            
                <br/>
                <div class="legend">Old-Model Domain(s)</div>
                <div class="outer">
                <div class="fieldset">
                <?php
                $domainUnitForm->display();
                ?>
                </div>
                </div>
                
                <br/>
                <div class="legend">Old-Model Department</div>
                <div class="outer">
                <div class="fieldset">
                <?php
                $departmentUnitForm->display();
                ?>
                </div>
                </div>

                <br/>
                <div class="legend">Old-Model Program</div>
                <div class="outer">
                <div class="fieldset">
                <?php
                $programUnitForm->display();
                ?>
                </div>
                </div>

            <?php
            }
            ?>                         

            </td>
            <td width="50%">

            <?php
            if ($edit != 'newUnit') {
            ?>

                <div class="legend">Admission Periods</div>
                <div class="outer">
                <div class="fieldset">
                    <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                        <a href="periods.php?unit_id=<?php echo $selectedUnitId; ?>">Manage Admission Periods</a>
                    </div>

                    <br/>

                    <div>
                    <b>Open Periods:</b><br/><br/>
                    <?php
                    //DebugBreak();
                    $activePerdiods = $selectedUnit->getActivePeriods(1);
                    foreach ($activePerdiods as $period) {
                        echo displayPeriod($period);
                    }
                    ?>
                    </div>        
                </div>
                </div>

                <br/>
                <div class="legend">Roles</div>            
                <div class="outer">
                <div class="fieldset">
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                    <a href="unitRoles.php?unit_id=<?php echo $selectedUnitId; ?>">Manage Unit Roles</a>
                </div>
                <br/>
                <?php
                if ( count($admins) > 0 ) {
                    
                    echo '<b>Administrator(s)</b><br/>';
                    // echo '<ul>';
                    $i = 1;
                    foreach ($admins as $admin) {
                        //echo '<li>' . $admin['firstname'] . ' ' . $admin['lastname'] . '</li>';
                        if ($i != 1) {
                            echo ', ';
                        }
                        echo $admin['firstname'] . '&nbsp;' . $admin['lastname'];
                        $i++;
                    }
                    // echo '</ul>';
                }
                
                if ( count($faculty) > 0 ) {
                    
                    echo '<br/><br/><b>Faculty</b><br/>';
                    // echo '<ul>';
                    $i = 1;
                    foreach ($faculty as $admin) {
                        //echo '<li>' . $admin['firstname'] . ' ' . $admin['lastname'] . '</li>';
                        if ($i != 1) {
                            echo ', ';
                        }
                        echo $admin['firstname'] . '&nbsp;' . $admin['lastname'];
                        $i++;
                    }
                    //echo '</ul>';
                }
                ?>               
                <br/><br/> 
                </div>
                </div>
            <?php
            }
            ?> 

            <?php
            $unitDomainIds = $selectedUnit->getUnitDomainIds();
            if ( ($edit != 'newUnit') && (count($unitDomainIds) > 0) ) {
            ?>
                <br/>
                <div class="legend">Page Content</div>
                <div class="outer">
                <div class="fieldset">
                    <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                        <a href="content.php?lbDomain=<?php echo $unitDomainIds[0]; ?>">Manage Page Content</a>
                    </div>    
                <br/><br/>
                </div>
                </div>
                
            <?php    
            }
            ?>            

            </td>
            </tr>
            </table>
        <?
        }        
        ?>        
        </div>

</div>

</div>

<div style="clear: both;"></div>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';


//########################################################################

function makeUnitMenu($unit, $headingUnitId = NULL, $displayLinks = TRUE,
    $includeSelf = TRUE, $selfIsRoot = TRUE) 
{
    $unitId = $unit->getId();
    $unitName = $unit->getName();
    $unitNameShort = $unit->getNameShort();
    if ($unit->isProgram()) {
        $unitName .= ' (program)';
    }
    
    $menu = '';
    if ($includeSelf) {
        
        if ($unitId == $headingUnitId) {
            $unitAnchor = '<div class="selected">';    
        } else {
            $unitAnchor = '<div>';    
        }
        
        $unitAnchor .= '<a title="' . $unitName . ' (unit id: ' . $unitId . ')"';
        
        if (!$displayLinks || $unitId == $headingUnitId) {
            $unitAnchor .= '>';
        } else {
            $unitAnchor .= ' href="?unit_id=' . $unitId . '">';
        }
        
        $unitAnchor .= $unitNameShort . '</a></div>';
        if ($selfIsRoot) {
            $menu .= '<ul class="menu menu-root">';
        }
        $menu .= '<li>' . $unitAnchor;
    }
    
    $childUnits = $unit->getChildUnits();
    if ( count($childUnits) > 0 ) {
        $menu .= '<ul class="menu">';
        foreach ($childUnits as $childUnit) {
            $menu .= makeUnitMenu($childUnit, $headingUnitId, $displayLinks, TRUE, FALSE);
        }
        $menu .= '</ul>';        
    }

    if ($includeSelf) {
        $menu .= '</li>';
        if ($selfIsRoot) {
            $menu .= '</ul>';
        }    
    }
 
    return $menu;  
}

function displayPeriod($period, $rootPeriod = TRUE) {
    
    global $selectedUnit;
    
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $status = $period->getStatus();

    $display = '';
    
    if ($rootPeriod) {
        $heading = '<a class="tooltip" title="period id: ';
        $heading .= $period->getId() . '" style="cursor: pointer;">';   
        $heading .= '<b>' . $period->getName() . '</b>';
        $heading .= ' (' . $period->getAdmissionTerm() . ' ' . $period->getAdmissionYear() . ')';  

        if ($selectedUnit) {
            $periodUnitId = $period->getUnitId();
            if ( $selectedUnit->getId() != $periodUnitId ) {
                $periodUnit = new Unit($periodUnitId);
                $heading .= '<br/>[' . $periodUnit->getName() . ']';  
            }
        }
    
        $heading .= '</a>';
        $display = $heading . '<div class="tooltipContent" style="visibility: hidden; height: 0px;">';
        $display .= $heading . '<br/>';
        $display .= 'start date: ' . $startDate . '<br/>';
    
    } else {
        
        $display = $period->getPeriodType() . ' deadline: ' . $endDate . '<br/>';
    }
    
    $subperiods = $period->getChildPeriods();
    $subperiodCount = count($subperiods);
    if ($subperiodCount > 0) {
        foreach ($subperiods as $subperiod) {
            $display .= displayPeriod($subperiod, FALSE);
        }  
    }
    
    if ($rootPeriod) {
        $display .= '<br/>Application Programs:';
        $programs = $period->getPrograms();
        $programCount = count($programs);
        if ($programCount > 0) {
            $display .= '<ul>';
            foreach ($programs as $programId => $program) {
                $display .= '<li><a title="unit id: ' . $programId . '" style="cursor: pointer;">' . $program .'</a></li>';    
            }
            $display .= '</ul>';
        }
    }
    
    if ($rootPeriod) {
        $display .= '</div><br/>';
    }
    
    return $display;     
}

?>