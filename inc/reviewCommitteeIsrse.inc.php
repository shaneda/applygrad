<?php

$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$riskFactorVals = array(
    array(5, 'Weak undergrad or technical background'),
    array(6, 'Lack of industry experience'),
    array(7, 'Lack of research experience/aptitude'),
    array(8, 'Low GRE'),
    array(9, 'Low TOEFL'),
    array(10, 'Area mismatch')
);

$riskFactorKey = array(
    5 => 'Weak undergrad or technical background',
    6 => 'Lack of industry experience',
    7 => 'Lack of research experience/aptitude',
    8 => 'Low GRE',
    9 => 'Low TOEFL',
    10 => 'Area mismatch'
);

$positiveVals = array(
    array(1, 'Interesting &amp; specific research ideas'),
    array(2, 'Good area match'),
    array(3, 'Research &amp; publishing experience'),
    array(4, 'Strong &amp; specific recommendations'),
    array(5, 'Unusual industry or other experience'),
    array(6, 'Fellowship or other funding')
);

$positiveKey = array(
    1 => 'Interesting &amp; specific research ideas',
    2 => 'Good area match',
    3 => 'Research &amp; publishing experience',
    4 => 'Strong &amp; specific recommendations',
    5 => 'Unusual industry or other experience',
    6 => 'Fellowship or other funding'
);

$isrseVoteVals = array(
    array(1, '1 (top 5)'),
    array(1.5, '1.5'),
    array(2, '2 (top 10)'),
    array(2.5, '2.5'),
    array(3, '3 (prob out)'),
    array(4, '4 (out)'),
    array(5, '5 (way out)')
);

/*
* IMPORTANT: re-using the one-size-fits-all fields of the review table as follows:
* 
* experience => review.background
* research => review.grades
* statement => review.statement
* recoms => review.pertinent_info
* comments => review.comments
* rating => review.point
* mentor => review.grad_name
* 
* risk_factors => review_risk_factor
* positives => review_positive_factor
*/
for($i = 0; $i < count($committeeReviews); $i++) {
    if( $committeeReviews[$i][32]== $thisDept
        && $committeeReviews[$i][30] == 0   // not faculty review 
        && $committeeReviews[$i][29] == ""  // not supplemental review
        && $committeeReviews[$i][4] == $reviewerId
        && $committeeReviews[$i][23] == $round) 
    {
        $background = $committeeReviews[$i][6];
        $grades = $committeeReviews[$i][7];
        $statement = $committeeReviews[$i][8];
        $pertinent_info = $committeeReviews[$i][18];
        $comments = $committeeReviews[$i][9];
        $point = $committeeReviews[$i][10];
        $grad_name = $committeeReviews[$i][17];
        $positiveFactorOther = $committeeReviews[$i][33];
    }
}


if ($previousRoundData) {
    echo '<span style="font-size: 12px; font-weight: bold; font-style: italic;">';
    echo 'You reviewed this application in round ' . $reviewRound . '</span>';    
    echo '<br/><br/>';
}
?> 

<table border="0" cellspacing="0" cellpadding="2">
    <colgroup>
        <col width="10%" />
        <col width="40%" />
        <col width="10%" />
        <col width="40%" />
    </colgroup>
    <tr valign="middle">
        <td class="label">Industry experience:</td>
        <td colspan="3">
        <?php 
        //DebugBreak();
        ob_start();
        showEditText($background, "textarea", "background", $allowEdit); 
        $experienceTextarea = ob_get_contents();
        ob_end_clean();
        $experienceTextarea = str_replace("rows='5'", "rows='3'", $experienceTextarea);
        echo str_replace("cols='60'", "cols='50'", $experienceTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Research involvement &amp; interest:</td>
        <td colspan="3">
        <?php 
        ob_start();
        showEditText($grades, "textarea", "grades", $allowEdit);  
        $researchTextarea = ob_get_contents();
        ob_end_clean();
        $researchTextarea = str_replace("rows='5'", "rows='3'", $researchTextarea);
        echo str_replace("cols='60'", "cols='50'", $researchTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Stmt of purpose:</td>
        <td colspan="3">
        <?php 
        ob_start();
        showEditText($statement, "textarea", "statement", $allowEdit);  
        $statementTextarea = ob_get_contents();
        ob_end_clean();
        $statementTextarea = str_replace("rows='5'", "rows='3'", $statementTextarea);
        echo str_replace("cols='60'", "cols='50'", $statementTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Recoms (who/what):</td>
        <td colspan="3">
        <?php 
        ob_start();
        showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit);  
        $recommendationsTextarea = ob_get_contents();
        ob_end_clean();
        $recommendationsTextarea = str_replace("rows='5'", "rows='3'", $recommendationsTextarea);
        echo str_replace("cols='60'", "cols='50'", $recommendationsTextarea);
        ?>
        </td>
    </tr> 
    <tr valign="middle">
        <td colspan="4" valign="top">
        <div style="width: 50%;  float: left;">
        <span class="label">Risk factors:</span><br/>
        <?php 
        if ($allowEdit) {
            
            showEditText($riskFactors, "checklist", "riskFactors", $allowEdit, false, $riskFactorVals);
        
        } else {
            
            foreach ($riskFactors as $riskFactorIndex) {
                echo $riskFactorKey[$riskFactorIndex] . '<br />';
            }    
        }
        ?>
        </div>
        <div style="width: 50%; float: left;" >
        <span class="label">Positives:</span><br/> 
        <?php
        if ($allowEdit) {
            
            showEditText($positiveFactors, "checklist", "positiveFactors", $allowEdit, false, $positiveVals);
        
        } else {
            
            foreach ($positiveFactors as $positiveFactorIndex) {
                echo $positiveKey[$positiveFactorIndex] . '<br />';
            }    
        }
        echo 'Other: ';
        showEditText($positiveFactorOther, "textbox", "positiveFactorOther", $allowEdit, false,null,true,20);
        ?>
        </div>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Comments:</td>
        <td colspan="3">
        <?php 
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit);  
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        $commentsTextarea = str_replace("rows='5'", "rows='3'", $commentsTextarea);
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea);
        ?>
        </td>
    </tr>
    <tr valign="middle">
        <td class="label">Rating:</td>
        <td colspan="3">
        <?php 
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $isrseVoteVals); 
        ?>
        </td>
    </tr>               
    <tr valign="middle">
        <td colspan="4" class="label">Mentor candidates (if 2.5 or better):
        <span style="font-weight: normal;">
        <?php 
        showEditText($grad_name, "textbox", "grad_name", $allowEdit, false,null,true,25); 
        ?>
        </span>
        </td>
    </tr>


    <tr>
        <td colspan="4">
    <!--         
        <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
        &nbsp;&nbsp;
    -->        
        <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>
        </td>
    </tr> 
   
</table>