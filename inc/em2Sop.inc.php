<?php
// Initialize variables
$em2SopObjective = '';
$em2SopBackground = '';
$em2SopResearchExperience = '';
$em2SopTaInterest = '';
$em2SopAdditionalInfo = '';
$em2SopError = '';

// Validate and save posted data
if( isset($_POST['btnSubmit'])) 
{
    saveEm2Sop();
    checkRequirementsEm2Sop();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$em2SopError)
{
    $em2SopQuery = "SELECT * FROM em2_sop 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $em2SopResult = mysql_query($em2SopQuery);
    while($row = mysql_fetch_array($em2SopResult))
    {
        $em2SopObjective = $row['objective'];
        $em2SopBackground = $row['background'];
        $em2SopResearchExperience = $row['research_experience'];
        $em2SopTaInterest = $row['ta_interest'];  
        $em2SopAdditionalInfo = $row['additional_info'];       
    }    
} 

// Check whether form inputs should be disabled 
$disabled = '';
if (!$_SESSION['allow_edit'])
{
    $disabled = 'disabled="disabled"';    
}
?>

<span class="subtitle">Statement of Purpose</span>
<br>
<span class="errorSubtitle" id="em2SopError"><?php echo $em2SopError; ?></span>

<br/>
Briefly explain your objective in pursuing the Emerging Media Program and the specialization paths for which you are applying. 
<br/>
<textarea name="em2SopObjective" cols="60" rows="5" class="tblItemRequired" 
    onkeyup="countSOPChar(this, 3000, '#em2SopObjectiveCharCount')"
    <?php echo $disabled; ?> ><?php echo $em2SopObjective; ?></textarea>
<br/>
Characters left in your response 
<span id="em2SopObjectiveCharCount"><?php echo 3000 - strlen($em2SopObjective); ?></span> 

<br/>
<br/>
Describe your background in engineering, computer science, arts, design ahd architecture and other fields particularly relevant to
your objectives. Include any industrial, community, or commercial work experience.
<br/>
<textarea name="em2SopBackground" cols="60" rows="5" class="tblItemRequired"  
    onkeyup="countSOPChar(this, 3000, '#em2SopBackgroundCharCount')"
    <?php echo $disabled; ?>><?php echo $em2SopBackground; ?></textarea> 
<br/>
Characters left in your response 
<span id="em2SopBackgroundCharCount"><?php echo 3000 - strlen($em2SopBackground); ?></span> 

<br/>
<br/>
Outline your research and creative experience and its relation to Emerging Media 
<br/>
<textarea name="em2SopResearchExperience" cols="60" rows="5" class="tblItemRequired" 
    onkeyup="countSOPChar(this, 3000, '#em2SopResearchExperienceCharCount')"
    <?php echo $disabled; ?>><?php echo $em2SopResearchExperience; ?></textarea> 
<br/>
Characters left in your response 
<span id="em2SopResearchExperienceCharCount"><?php echo 3000 - strlen($em2SopResearchExperience); ?></span> 

<br/>
<br/>
If you wish to be considered for a Teaching Assistantship please describe your teaching experience.
<br/>
<textarea name="em2SopTaInterest" cols="60" rows="5"  
    onkeyup="countSOPChar(this, 1000, '#em2SopTaInterestCharCount')"
    <?php echo $disabled; ?>><?php echo $em2SopTaInterest; ?></textarea> 
<br/>
Characters left in your response 
<span id="em2SopTaInterestCharCount"><?php echo 1000 - strlen($em2SopTaInterest); ?></span>

<br/>
<br/>
Please provide any information you feel would be helpful to the admissions committee in making a decision.
For example, explain any extenuating circumstances such as an illness that led to a bad semester on your transcript, etc. <br/>
<textarea name="em2SopAdditionalInfo" cols="60" rows="5" 
    onkeyup="countSOPChar(this, 2000, '#em2SopAdditionalInfoCharCount')"
    <?php echo $disabled; ?>><?php echo $em2SopAdditionalInfo; ?></textarea> 
<br/>
Characters left in your response 
<span id="em2SopAdditionalInfoCharCount"><?php echo 2000 - strlen($em2SopAdditionalInfo); ?></span>

<script src="../javascript/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
function countSOPChar(val, maxsize, textElement) {
    var len = val.value.length;
    if (len > maxsize) {
        val.value = val.value.substring(0, maxsize);
    } else {
        $(textElement).text(maxsize - len + ' characters left');
    }
};

$(document).ready(function() {

    var em2ErrorMessage = $('#em2SopError').text();
    if (em2ErrorMessage != '')
    {
        var pageErrorMessage = $('#resumePageError').val() + em2ErrorMessage;
        $('#resumePageError').text(pageErrorMessage);        
    }
});
</script>

<?php
function saveEm2Sop()
{
    global $em2SopObjective;
    global $em2SopBackground;
    global $em2SopResearchExperience;
    global $em2SopLeadershipExperience;
    global $em2SopTaInterest;
    global $em2SopAdditionalInfo;
    global $em2SopError;
    
    $em2SopObjective = filter_input(INPUT_POST, 'em2SopObjective', FILTER_SANITIZE_STRING);
    $em2SopBackground = filter_input(INPUT_POST, 'em2SopBackground', FILTER_SANITIZE_STRING);
    $em2SopResearchExperience = filter_input(INPUT_POST, 'em2SopResearchExperience', FILTER_SANITIZE_STRING);
    $em2SopTaInterest = filter_input(INPUT_POST, 'em2SopTaInterest', FILTER_SANITIZE_STRING);
    $em2SopAdditionalInfo = filter_input(INPUT_POST, 'em2SopAdditionalInfo', FILTER_SANITIZE_STRING); 
    
    if (!$em2SopObjective)
    {
        $em2SopError .= 'Statement of purpose objective is required<br>';    
    }
     
    if (!$em2SopBackground)
    {
        $em2SopError .= 'Statement of purpose background is required<br>';    
    }
    
    if (!$em2SopResearchExperience)
    {
        $em2SopError .= 'Statement of purpose research experience is required<br>';    
    }
     
    
    if (!$em2SopError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM em2_sop WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE em2_sop SET
                objective = '" . mysql_real_escape_string($em2SopObjective) . "',
                background = '" . mysql_real_escape_string($em2SopBackground) . "',
                research_experience = '" . mysql_real_escape_string($em2SopResearchExperience) . "',
                ta_interest = '" . mysql_real_escape_string($em2SopTaInterest) . "',
                additional_info = '" . mysql_real_escape_string($em2SopAdditionalInfo) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO em2_sop 
                (application_id, objective, background, research_experience, 
                 ta_interest, additional_info)
                VALUES (" 
                . intval($_SESSION['appid']) . ",'" 
                . mysql_real_escape_string($em2SopObjective) . "','" 
                . mysql_real_escape_string($em2SopBackground) . "','" 
                . mysql_real_escape_string($em2SopResearchExperience) . "','"
                . mysql_real_escape_string($em2SopTaInterest) . "','" 
                . mysql_real_escape_string($em2SopAdditionalInfo) . "')";
            mysql_query($insertQuery);
        }
    } 
}

/*
* Check requirements for resume.php
* 
*/
function checkRequirementsEm2Sop()
{
    global $em2SopError; 
    global $resumeFileId;    
    
    if (!$em2SopError && ($resumeFileId > 0))
    {               
        // Resume has been uploaded, so page is complete so far.
        updateReqComplete("resume.php", 1);
        return;    
    }
    
    // Page is incomplete.
    updateReqComplete("resume.php", 0);
}
?>