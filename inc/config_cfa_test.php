<?php
if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
{
    //header("Location: ../apply/offline.php");
}

/*
CONSTANT VALUES FOR THE APPLICATION
*/
//set_time_limit(0);
$environment="test";

$db="awCfaTest";
$db_username="awCfaTestAdmin";
$db_password="de3u5u68La5e";
$db_host="webuild-db.srv.cs.cmu.edu";

$datafileroot = "../test-data";

$dontsendemail = TRUE;
$admissionsContact = "scscfa+admissions@cs.cmu.edu"; 
$paymentEmail = "scscfa+payment@cs.cmu.edu";
$supportEmail = "scscfa+technical@cs.cmu.edu";

// $paymentProcessor = "https://ccard-submit-test.as.cmu.edu/cgi-bin/gather_info.cgi"; //TESTING USING GENERIC COLLECTOR
$paymentProcessor = "https://train.cashnet.com/";

if ( strpos( phpversion(),'ubuntu' ) !== FALSE)
{
    // System magic file doesn't seem to work
    // $magic_location = "/usr/share/file/magic"; 
    $magic_location = "/usr0/apache2/inc/magic-php-5.3";
    //$magic_location = "/usr0/apache2/webapps/ApplygradCfa/www/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE ^ E_STRICT);    
}
elseif ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED);    
} else {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic";
    error_reporting(E_ALL);    
}

ini_set('display_errors', 1);                                                                                                                                                                                   
?>