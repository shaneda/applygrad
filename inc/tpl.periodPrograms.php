<?php
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Program Groups and Roles';

$pageCssFiles = array(
    '../css/units.css',
    '../css/ui.datepicker.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/ui.core.js',
    '../javascript/ui.datepicker.min.js',
    '../javascript/units.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br/>
<div id="bread" class="menu">
<ul>
    <li class="first"><a href="home.php">Home</a>
    <ul>
        <li>&#187; 
        <a href="units.php?unit_id=<?php echo $unitId; ?>"><?php echo $unit->getName(); ?></a> 
        <ul>
            <li>&#187; 
            <a href="periods.php?unit_id=<?php echo $unitId; ?>&period_id=<?php echo $periodId; ?>">Admission Periods</a>
            <ul>
                <li>&#187;</li>
            </ul>
            </li>
        </ul>
        </li>
    </ul>
    </li>
</ul>
</div>
<div id="unitName"><?php echo $period->getDescription(); ?></div>
<div id="pageTitle"><?php echo $pageTitle; ?></div>


<div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%">
<?php 
if ($allowAdmin) {
    $newProgramGroupForm->display();    
}  
$groupMenu = makeGroupMenu($periodId, $programGroupId);
echo $groupMenu;
?>
</div>

<div id="table" style="margin: -5px 0px 0px 20px; float: left; clear: right; width: 75%;">

        <div style="margin: 10px;">
        <?php
        if (!$allowAdmin) {
        
            echo '<p>You are not authorized to manage admission periods.</p>';
            
        } elseif ($programGroupId || $edit == 'newProgramGroup') {

            // Change the renderer templates to make each form have the same column widths.
            $renderer = $programGroupForm->defaultRenderer(); 
            $renderer->setFormTemplate(
               '<form{attributes}>
                  {hidden}
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                  {content}
                  </table>
               </form>'
            );
            $renderer->setElementTemplate(
            '<tr valign="top">
                <td width="30%" align="right">
                    <!-- BEGIN required --><span style="color: #ff0000;">*</span><!-- END required --><b>{label}</b>
                </td>
                <td width="70%" align="left">
                    <!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->{element}
                </td> 
            </tr>'  
            );
            ?> 
            
            <table width="100%" border="0" cellspacing="5" cellpadding="10">
            <tr valign="top">
            <td width="50%">
                
                <div class="legend">Program Group</div>
                <div class="outer">
                <div class="fieldset">
                <?php
                $programGroupForm->display();
                ?>
                </div>
                </div>           

            <?php
            if ($edit != 'newProgramGroup') {    
            ?>
                <br/>
                <div class="legend">Programs</div>            
                <div class="outer">
                <div class="fieldset">  
                <?php
                $programGroupProgramForm->display();
                ?>
                </div>
                </div>
            <?php
            }
            ?>
            </td>

            <td width="50%">
            <?php
            if ($edit != 'newProgramGroup') {    
            ?>
            
                <div class="legend">Roles</div>            
                <div class="outer">
                <div class="fieldset">  
                <div class="menu" style="float: right; text-align: right; padding-right: 20px;"> 
                    <a href="periodProgramRoles.php?role_id=1&program_group_id=<?php echo $programGroupId; ?>">Manage Program Group Roles</a>
                </div>
                <br/>
                <?php
                if ( count($admins) > 0 ) {
                    
                    echo '<b>Administrator(s)</b><br/><ul>';
                    foreach ($admins as $admin) {
                        echo '<li>' . $admin['firstname'] . ' ' . $admin['lastname'] . '</li>';
                    }
                    echo '</ul>';
                }
                
                if ($programGroup->isReviewGroup()) {
                ?> 
                <br/> 
                <?php
                if ( count($chairs) > 0 ) {
                    
                    echo '<b>Admission Chair(s)</b><br/><ul>';
                    foreach ($chairs as $chair) {
                        echo '<li>' . $chair['firstname'] . ' ' . $chair['lastname'] . '</li>';
                    }
                    echo '</ul>';
                }
                ?> 
                <br/>
                <?php
                if ( count($committee) > 0 ) {
                    
                    echo '<b>Admission Committee</b><br/><ul>';
                    foreach ($committee as $member) {
                        echo '<li>' . $member['firstname'] . ' ' . $member['lastname'] . '</li>';
                    }
                    echo '</ul>';
                }
                
                }
                ?> 
                
                <br/>
                </div>
                </div>
            
            <?php
            }
            ?>  
            </td>
            </tr>
            </table>
        <?
        }        
        ?>        
        </div>

</div>

<div style="clear: both;"></div>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';


//########################################################################

function makeGroupMenu($periodId, $programGroupId = NULL, $displayLinks = TRUE) {

    //DebugBreak();
    $periodProgramGroups = array();
    $query = "SELECT * FROM program_group WHERE period_id = " . $periodId;
    $query .= " ORDER BY program_group_id";
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $periodProgramGroups[] = $row;    
    }
    
    $groupCount = count($periodProgramGroups);
    $menu = ''; 
    if ($groupCount > 0) {
        $menu .= '<ul class="menu menu-root" style="list-style-type: none;">';    
    }
    $i = 1;
    foreach ($periodProgramGroups as $periodProgramGroup) {

        $groupId = $periodProgramGroup['program_group_id'];
        $groupName = $periodProgramGroup['program_group_name'];
        $groupDescription = $periodProgramGroup['program_group_description'];
        
        if ($groupId == $programGroupId) {
            $groupAnchor = '<div class="selected">';    
        } else {
            $groupAnchor = '<div>';    
        }
        
        $groupAnchor .= '<a title="' . $groupDescription . ' (program group id: ' . $groupId . ')"';
        
        if (!$displayLinks || ($groupId == $programGroupId) ) {
            $groupAnchor .= '>';
        } else {
            $groupAnchor .= ' href="?period_id=' . $periodId . '&program_group_id=' . $groupId . '">';
        }
        
        $groupAnchor .= $groupName . '</a></div>';
        $menu .= '<li>' . $groupAnchor . '</li>';
    
        $i++;
    }
    if ($groupCount > 0) {
        $menu .= '</ul>';    
    }
    
    return $menu;  
}

?>
