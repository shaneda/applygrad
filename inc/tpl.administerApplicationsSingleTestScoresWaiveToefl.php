<?php
$waiveToeflChecked = '';
$eslMessage = 'ESL';
$eslClass = 'confirm';
if ($waiveToefl == 1) {
    $waiveToeflChecked = 'checked';
    $eslMessage = '';
    $eslClass = 'confirmComplete';
}

echo <<<EOB
<form class="waiveToefl" id="waiveToefl_{$applicationId}" action="">
<table cellpadding="2px" cellspacing="2px" border="0">
<tr>
    <td colspan="3">Waive TOEFL&nbsp;&nbsp;
    <input type="checkbox" class="waiveToefl" 
        id="waiveToefl_{$applicationId}_1" {$waiveToeflChecked} />
    </td>
</tr>
</table> 
</form>

<script type="text/javascript">
        
</script>

<script type="text/javascript">
    $('#eslMessage_{$applicationId}').html('{$eslMessage}');
    $('#eslMessage_{$applicationId}').prev('span').attr('class', '{$eslClass}');
</script>

EOB;
?>