<?php
include '../inc/specialCasesDomains.inc.php';

function isProgram($programId, $programNames)
{
    $programNames = array_map('normalize', $programNames);
  
    $query = "SELECT programs.id 
        FROM programs
        INNER JOIN degree ON programs.degree_id = degree.id
        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id  
        WHERE LOWER(TRIM(CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name))) 
        IN ('" . implode("','", $programNames) . "')
        AND programs.id = " . intval($programId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;    
}

function isDepartmentProgram($programId, $departmentName)
{  
    $query = "SELECT department.id 
        FROM lu_programs_departments 
        INNER JOIN department ON lu_programs_departments.department_id = department.id
        WHERE LOWER(TRIM(department.name)) = '" . normalize($departmentName) . "'
        AND lu_programs_departments.program_id = " . intval($programId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;    
}


/*
* Domain tests 
*/
function isScsDomain($domainId) 
{ 
    global $scsDomains;
    return isDomain($domainId, $scsDomains);
}

function isRiFifthYearMastersDomain($domainId) 
{ 
    global $riFifthYearMastersDomains;
    return isDomain($domainId, $riFifthYearMastersDomains);
}

function isIsreeDomain($domainId) 
{ 
    global $isreeDomains;
    /*
    ****************************************************************************************
    
    I know this is brain dead, but I need to allow the domain names for exec ed to change 
        "domain_id","department_id","domain_name","department_name"
        "42","55","Enterprise-Architecture-Fundamentals","Enterprise Architecture Fundamentals"
        "45","59","Advanced-Enterprise-Architecture","Advanced Enterprise Architecture"
        "46","60","COTS-Based-Integration","COTS Based Integration"
        "47","61","Managing-Software-Outsourcing","Managing Software Outsourcing"
        "48","62","Systems-Integration","Systems Integration"
        "50","63","Challenge-Exam-EAF","Challenge Exam - EAF"
        "53","70","EA-Security","EA Security"
        "54","69","Requirements-Engineering","Requirements Engineering"
        "55","68","Software-Architecture","Software Architecture"
        "56","67","Software-Project-Management","Software Project Management"
        "58","72","Security-for-Software-Engineers","Security-for-Software-Engineers"
        "61","77","Systems-and-Software-Security","Systems-and-Software-Security"
        "64","80","Leadership-Cyber-Enabled-World","Leadership Cyber-Enabled World"
        "68","84","Cybersecurity-Architectures","Cybersecurity Architectures"
        "71","86","Cybersecurity-Fundamentals","Cybersecurity Fundamentals"
        "74","89","Big-Data-Systems","Big Data Systems"
        "75","90","Architectures-for-Mergers","Architecture for Mergers"
        "76","91","Advanced-Big-Data","Advanced Big Data"
        "78","96","Principles-of-Architecture-Design","Principles of Architecture Design"
        "79","95","Applied-Data-Analytics","Applied Data Analytics "
 */
 
    $isreeDomainIds = array();
    $query = "select ldd.domain_id
                from program p
                inner join programs_unit pu on pu.unit_id = p.unit_id 
                inner join lu_programs_departments lpd on lpd.program_id = pu.programs_id and lpd.department_id NOT IN (51, 98, 87)
                inner join lu_domain_department ldd on ldd.department_id = lpd.department_id
                where p.program_type_id = 4";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        array_push($isreeDomainIds, $row['domain_id']);
    }

    return in_array($domainId, $isreeDomainIds);
}

function isMseMsitDomain($domainId) 
{ 
    global $mseMsitDomains;
    return isDomain($domainId, $mseMsitDomains);
}

function isMseMsitESEDomain($domainId) 
{ 
    global $mseEseDomains;
    return isDomain($domainId, $mseEseDomains);
}

function isMseMsitEbizDomain($domainId) 
{ 
    global $mseMsitEbizDomains;
    return isDomain($domainId, $mseMsitEbizDomains);
}

function isCnbcDomain($domainId) 
{
    global $cnbcDomains;
    return isDomain($domainId, $cnbcDomains);
}

function isStatisticsDomain($domainId) 
{
    global $statisticsDomains;
    return isDomain($domainId, $statisticsDomains);
}

function isMshciiDomain($domainId)
{
    global $mshciiDomains;
    return isDomain($domainId, $mshciiDomains);
}

function isMetalsDomain($domainId)
{
    global $metalsDomains;
    return isDomain($domainId, $metalsDomains);
}

function isMsrtDomain($domainId) 
{
    global $msrtDomains;
    return isDomain($domainId, $msrtDomains);
}

function isBICDomain($domainId) 
{
    global $bicDomains;
    return isDomain($domainId, $bicDomains);
}

function isPrivacyDomain($domainId) 
{
    global $privacyDomains;
    return isDomain($domainId, $privacyDomains);
}

function isMsLSEDomain($domainId) 
{
    global $metalsDomains;
    return isDomain($domainId, $metalsDomains);
}

function isMITSDomain($domainId) 
{
    global $mitsDomains;
    return isDomain($domainId, $mitsDomains);   
}

function isDietrichDomain($domainId) 
{
    global $dietrichDomains;
    return isDomain($domainId, $dietrichDomains); 
}

function isEnglishDomain($domainId) 
{
    global $englishDomains;
    return isDomain($domainId, $englishDomains);
}

function isHistoryDomain($domainId) 
{
    global $historyDomains;
    return isDomain($domainId, $historyDomains);
}

function isPsychologyDomain($domainId) 
{
    global $psychologyDomains;
    return isDomain($domainId, $psychologyDomains);
}

function isPhilosophyDomain($domainId) 
{
    global $philosophyDomains;
    return isDomain($domainId, $philosophyDomains);
}

function isModernLanguagesDomain($domainId) 
{
    global $modernLanguagesDomains;
    return isDomain($domainId, $modernLanguagesDomains);
}

function isModernLanguagesPhdDomain($domainId) 
{
    global $modernLanguagesPhdDomains;
    return isDomain($domainId, $modernLanguagesPhdDomains);
}

function isModernLanguageMaDomain($domainId) 
{
    global $modernLanguagesMaDomains;
    return isDomain($domainId, $modernLanguagesMaDomains);
}

function isSdsDomain($domainId) 
{
    global $sdsDomains;
    return isDomain($domainId, $sdsDomains);   
}

function isIniDomain ($domainId) 
{
    global $iniDomains;
    return isDomain($domainId, $iniDomains);  
}

function isIniKobeDomain($domainId) 
{
    global $iniKobeDomains;
    return isDomain($domainId, $iniKobeDomains);
}

function isDesignDomain($domainId) 
{
    global $designDomains;
    return isDomain($domainId, $designDomains);   
}

function isDesignPhdDomain($domainId) 
{
    global $designPhdDomains;
    return isDomain($domainId, $designPhdDomains);   
}

function isRiMsRtChinaDomain($domainId) 
{
    global $riMsRtChinaDomains;
    return isDomain($domainId, $riMsRtChinaDomains);   
}

function isEM2Domain($domainId) 
{
    global $EM2Domains;
    return isDomain($domainId, $EM2Domains);   
}

function isREUSEDomain($domainId) 
{
    global $REUSEDomains;
    return isDomain($domainId, $REUSEDomains);   
}

function isEngineeringDomain($domainId) 
{
    global $EngineeringDomains;
    return isDomain($domainId, $EngineeringDomains);   
}


/*
* Program tests 
*/
function isCsdProgram($programId) 
{
    return isDepartmentProgram($programId, 'Computer Science Department'); 
}

function isMLProgram($programId) 
{
    return (isDepartmentProgram($programId, 'Machine Learning') ||
            isDepartmentProgram($programId, 'ML-Masters')); 
}

function isRiProgram($programId) 
{
    return isDepartmentProgram($programId, 'Robotics Institute');
}

function isMsCsProgram($programId) 
{
    return isDepartmentProgram($programId, 'Computer Science - MS');
}

function isVlisProgram($programId) 
{   
    $vlisPrograms = array(
        'Master of Computational Data Science'
    );
    return isProgram($programId, $vlisPrograms);
}

function isLtiProgram($programId) 
{
    return isDepartmentProgram($programId, 'Language Technologies Institute');
}

function isPhilosophyProgram($programId) 
{
    return isDepartmentProgram($programId, 'Philosophy');
}

function isHistoryProgram($programId) 
{
    return isDepartmentProgram($programId, 'History');
}

function isEnglishProgram($programId) 
{
    return isDepartmentProgram($programId, 'English');
}

function isModernLanguageProgram ($programId) 
{
    return isDepartmentProgram($programId, 'Modern-Languages');
}

function isPsychologyProgram($programId) 
{
    return isDepartmentProgram($programId, 'Psychology');
}

function isSDSProgram($programId) 
{
    return isDepartmentProgram($programId, 'Social and Decision Sciences');
}

function isDdesProgram($programId) 
{
    $ddesPrograms = array(
        'Doctorate of Design'
    );
    return isProgram($programId, $ddesPrograms);
}


/*
* Application tests 
*/
function isMsCsApplication($applicationId) 
{
    // NOTE: this test is to determine that MS in CS is the only
    // program the applicant has selected.
    
    $programQuery = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);

    $programResult = mysql_query($programQuery);

    if (mysql_num_rows($programResult) == 1) 
    {    
        while ($row = mysql_fetch_array($programResult)) 
        {
            if (isMsCsProgram($row['program_id'])) 
            { 
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

function isCsdPhdApplication($applicationId) 
{
    $programQuery = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);

    $programResult = mysql_query($programQuery);

    while ($row = mysql_fetch_array($programResult)) 
    {
        if (isCsdProgram($row['program_id'])) 
        { 
            return TRUE;
        }
    }
    
    return FALSE;
}

// Not used?
function isPhilosophyApplication($applicationId) 
{
    $programQuery = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);

    $programResult = mysql_query($programQuery);

    if (mysql_num_rows($programResult) == 1) 
    {    
        while ($row = mysql_fetch_array($programResult)) {
        
            if (isPhilosophyProgram($row['program_id'])) {
             
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

// Not used?
/*
function isHistoryApplication($applicationId) {
    
    $programQuery = "SELECT program_id FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    $programResult = mysql_query($programQuery);

    if (mysql_num_rows($programResult) == 1) {
        
        while ($row = mysql_fetch_array($programResult)) {
        
            if (isHistoryProgram($row['program_id'])) {
             
                return TRUE;
            }
        }
    }
    
    return FALSE;
}
*/

// Not used?
function isEnglishApplication($applicationId) {
    
    $programQuery = "SELECT program_id FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    $programResult = mysql_query($programQuery);

    if (mysql_num_rows($programResult) == 1) {
        
        while ($row = mysql_fetch_array($programResult)) {
        
            if (isEnglishProgram($row['program_id'])) {
             
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

// Not used?
function isModernLanguageApplication($applicationId) {
    
    $programQuery = "SELECT program_id FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    $programResult = mysql_query($programQuery);

    if (mysql_num_rows($programResult) == 1) {
        
        while ($row = mysql_fetch_array($programResult)) {
        
            if (isModernLanguageProgram($row['program_id'])) {
             
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

// Not used?
function isPsychologyApplication($applicationId) {
    
    $programQuery = "SELECT program_id FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    $programResult = mysql_query($programQuery);

    if (mysql_num_rows($programResult) == 1) {
        
        while ($row = mysql_fetch_array($programResult)) {
        
            if (isPsychologyProgram($row['program_id'])) {
             
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

function isIniMsitApplication($applicationId) 
{
    $iniMsitPrograms = array(
        'M.S. in Information Technology - Mobility',
        'M.S. in Information Technology - Information Security',
        'M.S. in Information Technology - Software Management',
    );
    
    $query = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) 
    {
        if (isProgram($row['program_id'], $iniMsitPrograms))
        {
            return TRUE;
        }
    }
    
    return FALSE;
}

function isIniMsistmApplication($applicationId) 
{
    $iniMsistmPrograms = array(
        'M.S. in Information Security'
    );
    
    $query = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) 
    {
        if (isProgram($row['program_id'], $iniMsistmPrograms))
        {
            return TRUE;
        }
    }
    
    return FALSE;
}

function isIniMsinApplication($applicationId) 
{
    $iniMsinPrograms = array(
        'M.S. in Information Networking'
    );
        
    $query = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) 
    {
        if (isProgram($row['program_id'], $iniMsinPrograms))
        {
            return TRUE;
        }
    }
        
    return FALSE;
}

function isEm2CfaApplication($applicationId) 
{   
    $em2CfaPrograms = array(
        'Master of Arts in Narrative, Composition and Experiential Material (CFA)',
        'Master of Arts in Participatory Culture and Digital Humanities (CFA)',
        'Master of Arts in Enactive Technologies and Interfaces (CFA)',
        'Master of Arts in Emerging Media (CFA)'
    );
    
    $query = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) 
    {
        if (isProgram($row['program_id'], $em2CfaPrograms))
        {
            return TRUE;
        }
    }
    
    return FALSE;
}

function isEm2MobilityApplication($applicationId) 
{
    $em2MobilityPrograms = array(
        'Master of Science in Mobility (CIT)'
    );
    
    $query = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result)) 
    {
        if (isProgram($row['program_id'], $em2MobilityPrograms))
        {
            return TRUE;
        }
    }
    
    return FALSE;
}

/*
* "Utility" functions 
*/
function greComplete($applicationId) {
    
    $greQuery = "SELECT testdate FROM grescore
        WHERE application_id = " . intval($applicationId);
    $greResult = mysql_query($greQuery);    
    
    $greComplete = FALSE;
    while ($row = mysql_fetch_array($greResult)) {
        
        if ($row['testdate'] != NULL 
            && $row['testdate'] != '0000-00-00'
            && $row['testdate'] != '0000-00-01') {
         
            $greComplete = TRUE;
        }
    } 
    
    return $greComplete;   
}

function cmuUndergrad($applicationId) {
    
    $cmuUndergradQuery = "SELECT date_grad, degree FROM usersinst
        WHERE institute_id = 261
        AND application_id = " . intval($applicationId);
    $cmuUndergradResult = mysql_query($cmuUndergradQuery);
    
    $cmuUndergrad = FALSE;
    while ($row = mysql_fetch_array($cmuUndergradResult)) {
        
        if ($row['date_grad'] != NULL 
            && $row['degree'] != NULL
            && $row['degree'] != '-1') {
         
            $cmuUndergrad = TRUE;
        }
    } 
    
    return $cmuUndergrad;   
}

function cmuMscsGreWaiver($applicationId) {
    
    $waiverQuery = "SELECT waiver_agree FROM grescore_mscs_waiver
        WHERE application_id = " . intval($applicationId);
    $waiverResult = mysql_query($waiverQuery);    
    
    $greWaiver = FALSE;
    while ($row = mysql_fetch_array($waiverResult)) {
        
        if ($row['waiver_agree'] == 1) {
         
            $greWaiver = TRUE;
        }
    } 
    
    return $greWaiver;   
}

function iniSupportingCourseworkComplete($applicationId)
{
    $isIniMsinApplication = isIniMsinApplication($applicationId);
    $isIniMsistmApplication = isIniMsistmApplication($applicationId);
    $isIniMsitApplication = isIniMsitApplication($applicationId);
    
    $query = "SELECT * FROM ini_supporting_coursework 
        WHERE application_id = " . intval($applicationId);
    $result = mysql_query($query);
    
    if (mysql_num_rows($result) == 0)
    {
        return false;
    }
    
    while ($row = mysql_fetch_array($result))
    {
        if (!$row['data_structures_title'] ||
            (!$row['data_structures_number'] && trim($row['data_structures_number']) !== '0')  || 
            !$row['programming_description'])
        {
            return FALSE;
        }
        
        if ($isIniMsitApplication 
            && (!$row['statistics_title'] || 
            (!$row['statistics_number'] && trim($row['statistics_number']) !== '0')))
        {
            return FALSE;    
        }
        
        if (($isIniMsinApplication || $isIniMsistmApplication)
            && (!$row['probability_title'] || 
                (!$row['probability_number'] && trim($row['probability_number']) !== '0')))
        {
            return FALSE;    
        }    
    }    
    
    return TRUE;
}

function em2SupportingCourseworkComplete($applicationId)
{
    /*
    $isIniMsinApplication = isIniMsinApplication($applicationId);
    $isIniMsistmApplication = isIniMsistmApplication($applicationId);
    $isIniMsitApplication = isIniMsitApplication($applicationId);
    */
    $query = "SELECT * FROM em2_supporting_coursework 
        WHERE application_id = " . intval($applicationId);
    $result = mysql_query($query);
    
    if (mysql_num_rows($result) == 0)
    {
        return false;
    }
    
    while ($row = mysql_fetch_array($result))
    {
        if (!$row['programming_description2'] || 
            !$row['programming_description'])
        {
            return FALSE;
        }
        /*
        if (isEm2CfaApplication(intval($applicationId)) 
            && (!$row['makerkits_description'] || !$row['statistics_number']))
        {
            return FALSE;    
        }
        
        if (($isIniMsinApplication || $isIniMsistmApplication)
            && (!$row['probability_title'] || !$row['probability_number']) )
        {
            return FALSE;    
        } 
        */   
    }    
    
    return TRUE;
}

function getStoreNumber($domainId)
{
    global $environment;
    
    if (isIsreeDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu147test';
        } else {
            $storeNumber = 'cmu147';
        }    
    }
    elseif (isStatisticsDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu162test';
        } else {
            $storeNumber = 'cmu162';
        }     
    }
    elseif (isEnglishDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu138test';
        } else {
            $storeNumber = 'cmu138';
        }    
    }
    elseif (isHistoryDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu157test';
        } else {
            $storeNumber = 'cmu157';
        }    
    }
    elseif (isModernLanguagesDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu112test';
        } else {
            $storeNumber = 'cmu112';
        }    
    }
    elseif (isPhilosophyDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu136test';
        } else {
            $storeNumber = 'cmu136';
        }    
    }
    elseif (isPsychologyDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu141test';
        } else {
            $storeNumber = 'cmu141';
        }    
    }
    elseif (isSdsDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu139test';
        } else {
            $storeNumber = 'cmu139';
        }    
    }
    elseif (isIniDomain($domainId))
    {
        $storeNumber = '20470';    
    }
    elseif (isDesignDomain($domainId) || isDesignPhdDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'cmu158test';
        } else {
            $storeNumber = 'cmu158';
        }    
    }
    elseif (isEM2Domain($domainId))
    {
        $storeNumber = '21880';    
    }
    elseif (isEngineeringDomain($domainId))
    {
        if ($environment == "test") {
            $storeNumber = 'CMU175test';
        } else {
            $storeNumber = 'CMU175';
        }    
    }
    else
    {
        if ($environment == "test") {
            $storeNumber = 'cmu101test';
        } else {
            $storeNumber = 'cmu101';
        }
    }
    
    return $storeNumber;    
}

function getAppFeeStoreItem($domainId)
{
    
    if (isIsreeDomain($domainId))
    {
        $appFeeStoreItem = '147-REG FEE';    
    }
    elseif (isStatisticsDomain($domainId))
    {
        $appFeeStoreItem = '162-APP FEE';    
    }
    elseif (isEnglishDomain($domainId))
    {
        $appFeeStoreItem = '138-APP FEE';    
    }
    elseif (isHistoryDomain($domainId))
    {
        $appFeeStoreItem = '157-APP FEE';    
    }
    elseif (isModernLanguagesDomain($domainId))
    {
        $appFeeStoreItem = '112-APP FEE';    
    }
    elseif (isPhilosophyDomain($domainId))
    {
        $appFeeStoreItem = '136-APP FEE';    
    }
    elseif (isPsychologyDomain($domainId))
    {
        $appFeeStoreItem = '141-APP FEE';    
    }
    elseif (isSdsDomain($domainId))
    {
        $appFeeStoreItem = '139-APP FEE';    
    }
    elseif (isIniDomain($domainId))
    {
        $appFeeStoreItem = '20470';    
    }
    elseif (isDesignDomain($domainId) || isDesignPhdDomain($domainId))
    {
        $appFeeStoreItem = '158-APP FEE';    
    }
    elseif (isEM2Domain($domainId))
    {
        $appFeeStoreItem = '21880';    
    }
    elseif (isEngineeringDomain($domainId)) {
        $appFeeStoreItem = '175-REG FEE';
    }
    else
    {   
        $appFeeStoreItem = '101-APP FEE';
    }

    return $appFeeStoreItem;    
}

function getRegFeeStoreItem($domainId)
{
    
    if (isIsreeDomain($domainId))
    {
        $regFeeStoreItem = '16040';    
    }
    elseif (isStatisticsDomain($domainId))
    {
        $regFeeStoreItem = '162-REG FEE';    
    }
    elseif (isEnglishDomain($domainId))
    {
        $regFeeStoreItem = '138-REG FEE';    
    }
    elseif (isHistoryDomain($domainId))
    {
        $regFeeStoreItem = '157-REG FEE';    
    }
    elseif (isModernLanguagesDomain($domainId))
    {
        $regFeeStoreItem = '112-REG FEE';    
    }
    elseif (isPhilosophyDomain($domainId))
    {
        $regFeeStoreItem = '136-REG FEE';    
    }
    elseif (isPsychologyDomain($domainId))
    {
        $regFeeStoreItem = '141-REG FEE';    
    }
    elseif (isSdsDomain($domainId))
    {
        $regFeeStoreItem = '139-REG FEE';    
    }
    elseif (isIniDomain($domainId))
    {
        $regFeeStoreItem = '20470';    
    }
    elseif (isDesignDomain($domainId) || isDesignPhdDomain($domainId))
    {
        $regFeeStoreItem = '158-REG FEE';    
    }
    elseif (isEM2Domain($domainId))
    {
        $regFeeStoreItem = '21880';    
    }
    else
    {
        $regFeeStoreItem = '101-REG FEE';
    }

    return $regFeeStoreItem;    
}

function designPhdTranscriptOptional($applicationId)
{
    $ddesApplicationProgram = FALSE;
    $otherApplicationProgram = FALSE;

    $query = "SELECT program_id 
        FROM lu_application_programs
        WHERE application_id = " . intval($applicationId);
    $result = mysql_query($query);

    while ($row = mysql_fetch_array($result))
    {
        if (isDdesProgram($row['program_id']))
        {
            $ddesApplicationProgram = TRUE;    
        }
        else
        {
            $otherApplicationProgram = TRUE;    
        }    
    }
    
    if ($ddesApplicationProgram && !$otherApplicationProgram)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
?>