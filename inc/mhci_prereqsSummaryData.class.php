<?php

include_once "mhci_prereqsCourseTypes.config.php";

class MHCI_PrereqsSummaryData {
        
    
    public function getStatus($studentLuUsersUsertypesId=NULL) {
        
     //   $query = "SELECT * FROM mhci_prereqs";
        $query = "SELECT mhci_prereqs.id, mhci_prereqs.student_lu_users_usertypes_id,
        mhci_prereqs.prereq_type, mhci_prereqs.period_id, mhci_prereqs.student_assessment,
        mhci_prereqs.status as student_status, mhci_prereqs.application_id, mhci_prereqs.timestamp as student_timestamp,
        mhci_prereqs_status.id as status_prereqs_id, 
        mhci_prereqs_status.reviewer_lu_users_usertypes_id, mhci_prereqs_status.reviewer_status,
        mhci_prereqs_status.reviewer_explanation, mhci_prereqs_status.reviewer_timestamp,
        mhci_prereqs_status.status as review_status
        from mhci_prereqs
        left outer join mhci_prereqs_status on mhci_prereqs_status.mhci_prereqs_id = mhci_prereqs.id";
        
        if ($studentLuUsersUsertypesId) {
            
            $query .= " WHERE mhci_prereqs.student_lu_users_usertypes_id = " . $studentLuUsersUsertypesId;
            
        }
        $result = mysql_query($query);
        
        $statusArray = array();
        while ($row = mysql_fetch_array($result)) {
        
            $studentLuUsersUsertypesId = $row['student_lu_users_usertypes_id'];
            $prereqType = $row['prereq_type'];
            $statusArray[$studentLuUsersUsertypesId][$prereqType] = $row;
        
        }
        
        return $statusArray;    // 3D array
        
    }
    
    
    public function getStatusFlat() {
        
        $query = <<<EOB
        
        SELECT lu_users_usertypes.id AS lu_users_usertypes_id,
        application.id AS application_id,
        concat(users.lastname, ', ',users.firstname) AS name,
        users.email
        FROM lu_users_usertypes
        INNER JOIN users ON users.id = lu_users_usertypes.user_id
        INNER JOIN users_info ON users_info.user_id = lu_users_usertypes.id
        INNER JOIN application ON application.user_id = lu_users_usertypes.id
        INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
        INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id    
        WHERE lu_application_programs.admission_status = 2
        AND lu_programs_departments.department_id = 49 
        GROUP BY lu_users_usertypes.id    
        ORDER BY users.lastname, users.firstname

EOB;
        
        $result = mysql_query($query);
        
        $studentArray = array();
        $statusArray = $this->getStatus();
        
        while ($row = mysql_fetch_array($result)) {

            $lu_users_usertypes_id = $row['lu_users_usertypes_id'];
            $studentArray[$lu_users_usertypes_id] = $row;

            $designStatus = "Not started";
            $programmingStatus = "Not started";
            $statisticsStatus = "Not started";
            
            if ( isset($statusArray[$lu_users_usertypes_id]) ) {
                
                if ( isset ($statusArray[$lu_users_usertypes_id]['design']) ) {
                    
                    $designStatusArray = $statusArray[$lu_users_usertypes_id]['design'];    
                    $designReviewerStatus = $designStatusArray['reviewer_status'];
                    $designReviewerExplanation = $designStatusArray['reviewer_explanation'];
                    $designStatus = $designReviewerStatus . " " .  $designReviewerExplanation;
                    
                }

                if ( isset ($statusArray[$lu_users_usertypes_id]['programming']) ) {
                    
                    $programmingStatusArray = $statusArray[$lu_users_usertypes_id]['programming'];    
                    $programmingReviewerStatus = $programmingStatusArray['reviewer_status'];
                    $programmingReviewerExplanation = $programmingStatusArray['reviewer_explanation'];
                    $programmingStatus = $programmingReviewerStatus . " " .  $programmingReviewerExplanation;
                    
                }
                
                if ( isset ($statusArray[$lu_users_usertypes_id]['statistics']) ) {
                    
                    $statisticsStatusArray = $statusArray[$lu_users_usertypes_id]['statistics'];    
                    $statisticsReviewerStatus = $statisticsStatusArray['reviewer_status'];
                    $statisticsReviewerExplanation = $statisticsStatusArray['reviewer_explanation'];
                    $statisticsStatus = $statisticsReviewerStatus . " " .  $statisticsReviewerExplanation;
                    
                }
            
            }
            
            $studentArray[$lu_users_usertypes_id]['design_status'] = $designStatus;
            $studentArray[$lu_users_usertypes_id]['programming_status'] = $programmingStatus;
            $studentArray[$lu_users_usertypes_id]['statistics_status'] = $statisticsStatus; 
        
        }
        
        return $studentArray;    // 2D array
        
    }
    
    
    
    public function getCourses($studentLuUsersUsertypesId=NULL) {
        
        $query = "SELECT * FROM mhci_prereqsCourses";
        
        if ($studentLuUsersUsertypesId) {
            
            $query .= " WHERE student_lu_users_usertypes_id = " . $studentLuUsersUsertypesId;
            
        }

        $result = mysql_query($query);
        
        global $courseTypes;  // from mhci_prereqsCourseTypes.config.php
        $courseArray = array();
        while ($row = mysql_fetch_array($result)) {
        
            $studentLuUsersUsertypesId = $row['student_lu_users_usertypes_id'];
            $courseType = $row['course_type'];
            $prereqType = $courseTypes[$courseType];
            $courseArray[$studentLuUsersUsertypesId][$prereqType][] = $row;
        
        }
        
        return $courseArray;    // 4D array
        
    }
    
    
    
}

?>
