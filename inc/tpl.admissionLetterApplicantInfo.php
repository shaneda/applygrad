<?php
$applicantInfo = getApplicantInfo($applicationId);
if ($unit == 'department' && isIniDepartment($unitId))
{
    $admittedPrograms = getAdmittedProgramsIni($applicationId);
}
elseif ($unit == 'department' && isDesignDepartment($unitId))
{
    $admittedPrograms = getAdmittedProgramsDesignMasters($applicationId);
}
else
{
    $admittedPrograms = getAdmittedPrograms($applicationId);    
}
$fileLink = getFileLink($applicantInfo, $unitId)
?>

<div>
<span style="display: inline-block; width: 150px; font-weight: bold">Name:</span> 
<?php echo $applicantInfo['firstname'] . ' ' . $applicantInfo['lastname']; ?>
<br>
<span style="display: inline-block; width: 150px; font-weight: bold">Admitted Program(s):</span> 
<?php 
echo $admittedPrograms[0]['program_name'];
if (isset($admittedPrograms[1]))
{
    echo ', ' . $admittedPrograms[1]['program_name'];    
} 

if (isset($admittedPrograms[0]['scholarship_amt'])
    && $admittedPrograms[0]['scholarship_amt'])
{
?>
    <br>
    <span style="display: inline-block; width: 150px; font-weight: bold">Scholarship Amt:</span>
    <?php echo number_format($admittedPrograms[0]['scholarship_amt'], 2); ?> 
<?php
}    

if ($fileLink)
{
?>
    <br>
    <span style="display: inline-block; width: 150px; font-weight: bold">Current Letter:</span>
    <?php echo $fileLink; ?>  
<?php
}    
?>
</div>

