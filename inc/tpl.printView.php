<?php
include_once '../inc/specialCasesAdmin.inc.php';

if ( !isset($mainTableWidth) ) {
    $mainTableWidth = '700';
}
?>
<!-- START BIG TABLE -->
<table width="<?php echo $mainTableWidth; ?>" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;">

<!-- NAME ROW/TABLE -->
<?php
if (isset($thisDept) && isRiMsRtChinaDepartment($thisDept))
{
    $lName = strtoupper($lName);    
}
?>
<tr>
<td colspan="3">
<table border="0"  cellpadding="0" cellspacing="0" width="100%" bgcolor="#C5C5C5">
  <tr>
    <td rowspan="3"><font size="+1">&nbsp;<?=$lName .", ".$fName ." ". $mName ." ". $title ?></font>
      <input type="hidden" name="userid" value="<?=$uid?>" />
	   <input type="hidden" name="appid" value="<?=$appid?>" />
    </td>
    <td valign="middle"><strong>
        <?
        $resumeFilePath=getFilePath(2, 1, $uid, $umasterid, $appid);
        if ($resumeFilePath !== null) {?>
            <a target="_blank" href="<?=$resumeFilePath?>" ><?php echo ($isHistoryDepartment ? 'C.V.' : 'Resume')?></a>
        <? 
        }
        
        if ($isDesignPhdDepartment || $isDesignDdesDepartment)
        {
            $bioFilePath=getFilePath(29, 1, $uid, $umasterid, $appid);
            if ($bioFilePath !== null) {?>
                &nbsp;
                <a target="_blank" href="<?=$bioFilePath?>">Biographical Essay</a>
            <?   
            }
        }
      ?>
    </strong></td>

    <td valign="middle"><strong>
    <?
    if ($isDesignPhdDepartment)
    {
        $statementTitle = 'Research Proposal';
        $statementFilePath=getFilePath(28, 1, $uid, $umasterid, $appid);
    }
    else
    {
        $statementTitle = 'Statement of Purpose';
        $statementFilePath=getFilePath(4, 1, $uid, $umasterid, $appid);    
    }
    
      if ($statementFilePath !== null) {?>
        <a target="_blank" href="<?=$statementFilePath?>"><?=$statementTitle?></a>
      <? } ?>
    </strong></td>
  </tr>
  <tr>
    <td valign="middle"><strong><?
    $ex = getExperience($appid, 1);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href="<?=getFilePath(5, 1, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Research Experience</a><?
    }
   ?></strong>&nbsp;</td>

    <td valign="middle"><strong><?
    $ex = getExperience($appid, 2);
    for($i = 0; $i < count($ex); $i++)
    {
     ?><a href="<?=getFilePath(5, 2, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Industry Experience</a><?
    }
    ?></strong>&nbsp;</td>
  </tr>
  <tr>
    <td valign="middle"><strong><?
    $ex = getExperience($appid, 4);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href ="<?=getFilePath(5, 4, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Professional Experience</a><?
    }
   ?></strong>&nbsp;</td>
    <td valign="middle"><strong><?
    $ex = getExperience($appid, 3);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href ="<?=getFilePath(5, 3, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Academic Experience</a><?
    }
   ?></strong>&nbsp;</td>
  </tr>
</table>
</td>
</tr>
<!-- END NAME ROW/TABLE --> 


<!-- PROGRAMS ROW/TABLE -->  
<tr>
<td colspan="3">
<table border="1" cellpadding="0" cellspacing="0" width="100%">
<?
for($i = 0; $i < count($myPrograms); $i++)
{
	?>
	<tr>
	<td width="40%"><strong><font size="-1"><?=$myPrograms[$i][1] . " ".$myPrograms[$i][2] . " ".$myPrograms[$i][3] ?></font></strong></td>
	<td>&nbsp;
	<?
	$myAreas = getMyAreas($myPrograms[$i][0],$appid);
	for($j = 0; $j< count($myAreas); $j++)
	{
		echo $myAreas[$j][1];
		//DebugBreak();
        if($j < count($myAreas)-1 )
		{
			if ( isset($_SESSION['A_admin_depts']) && in_array(6, $_SESSION['A_admin_depts']) ) {
                echo '<br/>&nbsp;&nbsp;';
            } else {
                echo ', ';
            }
		}
	}
	?>
	</td>
	</tr>
	<?  
    }
    
    if ($aco)
    {
    ?>
    <tr><td><strong><font size="-1">ACO</font></strong></td><td></td></tr>
    <?php
    }
    
    if ($pal)
    {
    ?>
    <tr><td><strong><font size="-1">PAL</font></strong></td><td></td></tr>
    <?php
    }
    
    /*
    if ( isset($mastersReviewWaiver) && $mastersReviewWaiver ) {
    ?>
        <tr>
            <td colspan="2">Allow review by masters programs</td>
        </tr>
    <?php  
    }
    */
?>
</table>
</td>
</tr>
<!-- END PROGRAMS ROW/TABLE -->


<!-- ADDRESS ROW/TABLE -->
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
<tr>
<td width="50%" bgcolor="#c5c5c5" align="center"><strong>Current Address</strong></td>
<td width="50%" bgcolor="#c5c5c5" align="center"><strong>Permanent Address</strong></td>
</tr>
<tr>
<td width="50%" align="left" valign="top">
<?
echo $street1;
if($street2 != "")
{
	echo "<br/>".$street2;
}
if($street3 != "")
{
	echo "<br/>".$street3;
}
if($street4 != "")
{
	echo "<br/>".$street4;
}
echo "<br/>".$city;
if($state != "")
{
	echo ", ".$state;
}
echo " ".$postal;
if($country != "")
{
	echo "<br/>".$country;
}
if($tel != "")
{
	echo "<br/>Tel: ".$tel;
}
if($telMobile != "")
{
	echo "<br/>Mobile: ".$telMobile;
}
if($homepage != "")
{
    if (strpos($homepage, '://') === FALSE) {
        $homepage = 'http://' . $homepage;
    }
    echo '<br/><a href="'.$homepage.'" target="_blank">'.$homepage . '</a>';
}
if($curResident != NULL && $curResident != "")
{
	echo "<br/>PA Resident: " . $curResident;
}
?></td>
<td width="50%" align="left" valign="top">
<?
echo "&nbsp;".$streetP1;
if($streetP2 != "")
{
	echo "<br/>&nbsp;".$streetP2;
}
if($streetP3 != "")
{
	echo "<br/>&nbsp;".$streetP3;
}
if($streetP4 != "")
{
	echo "<br/>&nbsp;".$streetP4;
}
echo "<br/>&nbsp;".$cityP;
if($stateP != "")
{
	echo ", ".$stateP;
}
echo " ".$postalP;
if($countryP != "")
{
	echo "<br/>&nbsp;".$countryP;
}
?></td>
</tr>
</table>
</td>
</tr>
<!-- END ADDRESS ROW/TABLE --> 


<!-- BIO ROW/TABLE -->
<tr>
<td colspan="3">
<table border="1"   cellpadding="0" cellspacing="0" width="100%">  
  <tr bgcolor="#c5c5c5">
    <td align="center" width="40%"><strong>Biographical Info</strong></td>
    <td align="center" width="60%"><strong>Immigration and Ethnicity Info</strong></td>
  </tr>
  <tr valign="top">
    <td align="center" width="40%">
      <table width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr>
        <td width="30%" align="right"><strong>Gender &nbsp;</strong></td>
        <td width="70%">&nbsp;
              <?=$gender?></td>
      </tr>
      <tr>
        <td width="30%" align="right"><strong>Date of Birth &nbsp;</strong></td>
        <td width="70%">&nbsp;
              <?=formatUSDate($dob)?></td>
      </tr>

      <tr>
        <td width="30%" align="right"><strong>Email &nbsp;</strong></td>
        <td width="70%">&nbsp;
              <?=$email?></td>
      </tr>
      <?php
      if ($qq_number) {
      ?>
      <tr>
        <td width="30%" align="right"><strong>QQ Number &nbsp;</strong></td>
        <td width="70%">&nbsp;
              <?=$qq_number?></td>
      </tr>
      <?php
      }
      
      if ($organization) {
      ?>
      <tr>
        <td width="30%" align="right"><strong>Organization &nbsp;</strong></td>
        <td width="70%">&nbsp;
              <?=$organization?></td>
      </tr>
      <?php
      }
      ?>
    </table></td>
    <td width="65%" align="left" valign="top">
    <table width="100%" border="1" cellpadding="0" cellspacing="0">
      <?php
      $usertypeId = intval($_SESSION['A_usertypeid']);
      settype($usertypeId, 'integer');
      if ($usertypeId === 0 || $usertypeId === 1)
      {
        // Only show race/ethnicity and citizenship to SU and admins
        if ($usCitizenOrPermanentResident !== NULL)
        {
        ?>
      <tr>
        <td width="30%" align="right"><strong>US Citizen or Permanent Resident</strong></td>
        <td width="70%" >&nbsp;
              <?= $usCitizenOrPermanentResident
              ?></td>
      </tr>
        <?php
        }
        
      ?>
      <tr>
        <td width="30%" align="right"><strong>Citizenship &nbsp;</strong></td>
        <td width="70%" >&nbsp;
              <?= ""//  $countryCit
              ?></td>
      </tr>
      <tr valign="top">
        <td width="30%" align="right"><strong>Ethnicity &nbsp;</strong></td>
        <td>
          <?php 
         // echo $ethnicity;
        //  if ($ethnicity && $race) {
        //      echo "; ";
       //   }
        //  echo $race;
          ?>
         </td>
      </tr>
      <?php
      }
      ?>
      <tr>
        <td width="100" align="right"><strong>Native Tongue &nbsp;</strong></td>
        <td>&nbsp;
        <?php // echo $nativeTongue; ?>
        </td>  
      </tr> 
    </table>
    </td>
  </tr>
</table>
</td>
</tr>
<!-- END BIO ROW/TABLE --> 

<? 
//HIDE FROM STUDENT CONTACT
if($_SESSION['A_usertypeid'] != 11){ 

/*
* Dietrich Diversity Questions 
*/
if ($dietrichDiversity) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Diversity Questions</strong></td></tr>
  <tr>
      <td>
      <?php
      if ($dietrichDiversity['background'])
      {
      ?>
        <b>Background:</b> <?php echo htmlspecialchars($dietrichDiversity['background']);?>
        <br>
      <?php
      }
      
      if ($dietrichDiversity['life_experience'])
      {
      ?>
        <b>Life Experience:</b> <?php echo htmlspecialchars($dietrichDiversity['life_experience']);?>
      <?php
      }
      ?>
      </td>
  </tr>
</table>
</td>
</tr>
<?php
}    
?>

<!-- Official document status table -->
<!--
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td><b>All Official Documents Received:</b>
  <?php
  if ($allTranscriptsReceived && $allScoresReceived) {
    echo 'Yes';
  } else {
    echo  'No';
  }
  ?> 
  </td>
</tr>
</table>
</td>
</tr>
-->

<!-- EDUCATION ROW/TABLE -->
<tr>
<td colspan="3">
<table border="1" cellpadding="0" cellspacing="0" width="100%">
<? 
for($i = 0; $i < count($myInstitutes); $i++){
    if($myInstitutes[$i][1] != ""){
	    $title = "";
	    switch($myInstitutes[$i][16])
	    {
		    case "1":
			    $title = "Undergraduate";
		    break;
		    case "2":
			    $title = "Graduate";
		    break;
		    case "3":
			    $title = "Additional";
		    break;

	    }//end switch
?>
	<tr>
	<td bgcolor="#c5c5c5" width="30%"><strong><?=$title?> College/University</strong></td>
	<td width="70%"><strong>&nbsp;
        <?php
        echo $myInstitutes[$i][1];
        if (isset($myInstitutes[$i][20]) && $myInstitutes[$i][20] != '')
        {
            echo ', ' . $myInstitutes[$i][20];    
        }
        
          $filePath=getFilePath(1, $myInstitutes[$i][0], $uid, $umasterid, $appid, $myInstitutes[$i][15]);
          if ($filePath !== null) {
          ?>
          <a target="_blank" href="<?=$filePath?>" >(Transcript)</a>
          <?php 
          } 
          if ($myInstitutes[$i][14]) {
            // Official transcript has been received.
            echo ' <i>ODR</i> ';    
          }
          ?>
        </strong>
        </td>
	</tr>
	 <tr>
	  <td colspan="2">
	  <table border="1" cellpadding="0" cellspacing="0" width="100%">
	   <tr>
	    <td width="12%" align="right"><strong>Degree</strong>&nbsp;</td>
	    <td width="28%">&nbsp;<?=$myInstitutes[$i][5]?></td>
	    <td align="right"><strong><? if($myInstitutes[$i][16] == 3){ ?>
		Date Graduated/Left
		<? } else { ?> Date Graduated <? } ?></strong>&nbsp;</td>
	    <td>&nbsp;<?=$myInstitutes[$i][3]?></td>
	    <td width="12%" align="right"><strong>GPA Overall&nbsp;</strong> </td>
	    <td width="12%">&nbsp;<?=$myInstitutes[$i][11]?> / <?=$myInstitutes[$i][13]?>		</td>
	   </tr>
	   <tr>
	    <td width="12%" align="right"><strong>Major</strong>&nbsp;</td>
	    <td colspan="3">&nbsp;<?=$myInstitutes[$i][6]?></td>
	    <td width="12%" align="right">&nbsp;<strong>GPA Major</strong>&nbsp;</td>
	    <td width="12%">&nbsp;<?=$myInstitutes[$i][12]?>	/ <?=$myInstitutes[$i][13]?>                </td>
       </tr>
       <?php
       if ($myInstitutes[$i][21])
       {
       ?>
        <tr>
        <td width="12%" align="right"><strong>Converted GPA</strong>&nbsp;</td>
        <td colspan="5">&nbsp;<?=$myInstitutes[$i][21]?></td>
        </tr>
       <?php  
       }
       ?>
	  </table>
      </td>
	 </tr>
	<?
    } 
}//end for myInstitutes
?>
</table>
</td>
</tr>
<!-- END EDUCATION ROW/TABLE -->


<?php
/*
* INI Supporting Coursework 
*/
if ($iniCoursework || $iniKobeCoursework) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Supporting Coursework</strong></td></tr>
  <tr>
    <?php 
      if ($iniKobeCoursework)
      {
        echo '<td>' . htmlspecialchars($iniKobeCoursework) . '</td>';
      }
      
      if ($iniCoursework)
      {
      ?>
        <td>
        <table border="1" width="100%">
        <tr>
            <td>
                <b>Data Structures:</b> 
                <?php 
                echo htmlspecialchars($iniCoursework['data_structures_title']) . ' ' 
                . htmlspecialchars($iniCoursework['data_structures_number']); 
                ?>
            </td>
            <td>
                <b>Probability Theory:</b> 
                <?php 
                echo htmlspecialchars($iniCoursework['probability_title']) . ' ' 
                . htmlspecialchars($iniCoursework['probability_number']); 
                ?>
            </td>
            <td>
                <b>Statistics:</b> 
                <?php 
                echo htmlspecialchars($iniCoursework['statistics_title']) . ' ' 
                . htmlspecialchars($iniCoursework['statistics_number']); 
                ?>
            </td>
        </tr>
        <?php
        if ($iniCoursework['msit_experience'])
        {
        ?>
        <tr>
            <td colspan="3">
            <b>Probability/Statistics Experience:</b>
            <?php echo htmlspecialchars($iniCoursework['msit_experience']); ?> 
            </td>
        </tr>
        <?php
        }
        
        if ($iniCoursework['programming_description'])
        {
            ?>
        <tr>
            <td colspan="3">
            <b>C/Systems-Level Programming:</b>
            <?php echo htmlspecialchars($iniCoursework['programming_description']); ?> 
            </td>
        </tr>
        <tr>
            <td colspan="3">
            <b>C++/Java Programming:</b>
            <?php echo htmlspecialchars($iniCoursework['programming_description2']); ?> 
            </td>
        </tr>
        <?php
        }
        ?>
        </table>
        </td>
      <?php  
      }
      ?>
  </tr>
<?php
} else if ($em2Coursework) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Supporting Coursework</strong></td></tr>
  <tr>
    <?php 
      
      if ($em2Coursework)
      {
      ?>
        <td>
        <table border="1" width="100%">
        <tr>
            <td>
                <b>Data Structures:</b> 
                <?php 
                echo htmlspecialchars($em2Coursework['data_structures_title']) . ' ' 
                . htmlspecialchars($em2Coursework['data_structures_number']); 
                ?>
            </td>
            <td>
                <b>Statistics:</b> 
                <?php 
                echo htmlspecialchars($em2Coursework['statistics_title']) . ' ' 
                . htmlspecialchars($em2Coursework['statistics_number']); 
                ?>
            </td>
        </tr>
        <?php
        if ($em2Coursework['msit_experience'])
        {
        ?>
        <tr>
            <td colspan="3">
            <b>Probability/Statistics Experience:</b>
            <?php echo htmlspecialchars($em2Coursework['msit_experience']); ?> 
            </td>
        </tr>
        <?php
        }
        
        if ($em2Coursework['programming_description'])
        {
            ?>
        <tr>
            <td colspan="3">
            <b>Programming Languages:</b>
            <?php echo htmlspecialchars($em2Coursework['programming_description']); ?> 
            </td>
        </tr>
        <tr>
            <td colspan="3">
            <b>Programming Project Description:</b>
            <?php echo htmlspecialchars($em2Coursework['programming_description2']); ?> 
            </td>
        </tr>
        <?php
        }
        if ($em2Coursework['makerkits_description'] || $em2Coursework['makerkits_description2'])
        {
            ?>
        <tr>
            <td colspan="3">
            <b>Arts-engineering Toolkits Experience:</b>
            <?php echo htmlspecialchars($em2Coursework['makerkits_description']); ?> 
            </td>
        </tr>
        <tr>
            <td colspan="3">
            <b>Special Technical Proficiencies:</b>
            <?php echo htmlspecialchars($em2Coursework['makerkits_description2']); ?> 
            </td>
        </tr>
        <?php
        }
        ?>
        </table>
        </td>
      <?php  
      }
      ?>
  </tr>
<?php
}       
?>


<!-- SCORES ROW/TABLE -->
<?php
$greScoreCount = count($grescores);
$gresubjectScoreCount = count($gresubjectscores);
$toefliScoreCount =  count($toefliscores);
$sumScores1 = $greScoreCount + $gresubjectScoreCount + $toefliScoreCount;
if ($sumScores1 > 2) {
    $testTableWidth1 = round( (100/$sumScores1) ) . '%';
} else {
    $testTableWidth1 = '50%';    
}
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <? for($i = 0; $i < $greScoreCount; $i++){ ?>
        <td valign="top"  width="<?php echo $testTableWidth1; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="3" align="center" bgcolor="#c5c5c5"><strong>GRE General Test</strong><?
              $filePath=getFilePath(6, $grescores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($grescores[$i][10]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Test Date&nbsp;</strong></td>
            <td colspan="2">&nbsp;<?=$grescores[$i][1] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Verbal&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$grescores[$i][2] ?></td>
            <td width="20%">&nbsp;<?=$grescores[$i][3] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Quantitative&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$grescores[$i][4] ?></td>
            <td width="20%">&nbsp;<?=$grescores[$i][5] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Analytical Writing&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$grescores[$i][6] ?></td>
            <td width="20%">&nbsp;<?=$grescores[$i][7] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Analytical&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$grescores[$i][8] ?></td>
            <td width="20%">&nbsp;<?=$grescores[$i][9] ?></td>
          </tr>
        </table>
        </td>
        <? } ?>

      <? for($i = 0; $i < $gresubjectScoreCount; $i++){
	 ?>
        <td valign="top" width="<?php echo $testTableWidth1; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="3" align="center" bgcolor="#c5c5c5"><strong>GRE Subject Test</strong><?
              $filePath=getFilePath(8, $gresubjectscores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($gresubjectscores[$i][5]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Test Date</strong></td>
            <td colspan="2">&nbsp;<?=$gresubjectscores[$i][1] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right">
                <strong>&nbsp;<?=$gresubjectscores[$i][2] ?></strong>
            </td>
            <td width="20%">&nbsp;<?=$gresubjectscores[$i][3] ?></td>
            <td width="20%">&nbsp;<?=$gresubjectscores[$i][4] ?></td>
          </tr>
        </table>
        </td>
        <?  } ?>

      <? for($i = 0; $i < $toefliScoreCount; $i++){
 if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
	{?>

        <td valign="top" width="<?php echo $testTableWidth1; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="4" align="center" bgcolor="#c5c5c5"><strong>TOEFL IBT</strong><?
              $filePath=getFilePath(7, $toefliscores[$i][0], $uid, $umasterid, $appid, $toefliscores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($toefliscores[$i][7]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>

          </tr>
          <tr>
            <td width="30%" align="right"><strong>Test Date&nbsp;</strong></td>
            <td width="70%" colspan="3">&nbsp;<?=$toefliscores[$i][1]?></td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Reading&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toefliscores[$i][4]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Listening&nbsp;</strong></td>
            <td width="20%" valign="middle">&nbsp;<?=$toefliscores[$i][3]?></td>
            <td width="30%" align="right"><strong>Total </strong> &nbsp;</td>
            <td width="20%" align="left">&nbsp;<?=$toefliscores[$i][6]?></td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Speaking&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toefliscores[$i][2]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Writing&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toefliscores[$i][5]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
        </table>
        </td>
        <? } 
      }
      
if ($sumScores1 == 1) {
    echo '<td width="50%"></td>';
}      
?>
    </tr>    
</table>    
</td>
</tr>        
<?php
$gmatScoreCount = count($gmatScores);
$toeflcScoreCount = count($toeflcscores);
$toeflpScoreCount = count($toeflpscores);
$ieltsScoreCount =  count($ieltsScores);
$sumScores2 = $gmatScoreCount + $toeflcScoreCount + $toeflpScoreCount + $ieltsScoreCount;
if ($sumScores2 > 2) {
    $testTableWidth2 = round( (100/$sumScores2) ) . '%';
} else {
    $testTableWidth2 = '50%';    
}
    
if ( $gmatScoreCount > 0 || $toeflcScoreCount > 0 || $toeflpScoreCount > 0 || $ieltsScoreCount > 0 ) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="0" cellspacing="0" width="100%">
    <tr>     
<?
    for($i = 0; $i < $gmatScoreCount; $i++){
    if($gmatScores[$i][1] != "00/0000" && $gmatScores[$i][1] != "")
    { ?>
        <td valign="top" width="<?php echo $testTableWidth2; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="4" align="center" bgcolor="#c5c5c5"><strong>GMAT</strong><?
              $filePath=getFilePath(20, $gmatScores[$i][0], $uid, $umasterid, $appid, $gmatScores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($gmatScores[$i][10]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Test Date&nbsp;</strong></td>
            <td width="70%" colspan="3">&nbsp;<?=$gmatScores[$i][1]?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Verbal&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][2] ?></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][3] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Quantitative&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][4] ?></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][5] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Analytical Writing&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][6] ?></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][7] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Total&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][8] ?></td>
            <td width="20%">&nbsp;<?=$gmatScores[$i][9] ?></td>
          </tr>
        </table>
        </td>
        <? } }

    for($i = 0; $i < $toeflcScoreCount; $i++){
	if($toeflcscores[$i][1] != "00/0000" && $toeflcscores[$i][1] != "")
	{ ?>
        <td valign="top" width="<?php echo $testTableWidth2; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="4" align="center" bgcolor="#c5c5c5"><strong>TOEFL CBT</strong><?
              $filePath=getFilePath(9, $toeflcscores[$i][0], $uid, $umasterid, $appid, $toeflcscores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($toeflcscores[$i][7]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Test Date&nbsp;</strong></td>
            <td width="70%" colspan="3">&nbsp;<?=$toeflcscores[$i][1]?></td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Listening&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toeflcscores[$i][3]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Structure / Writing&nbsp;</strong></td>
            <td width="20%" valign="middle"><?=$toeflcscores[$i][2]?></td>
            <td width="30%" align="right"><strong>Total </strong> &nbsp;</td>
            <td width="20%" align="left">&nbsp;<?=$toeflcscores[$i][6]?></td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Reading&nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toeflcscores[$i][4]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Essay &nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toeflcscores[$i][5]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
        </table>
        </td>
        <? } } ?>
        <? for($i = 0; $i < $toeflpScoreCount; $i++){
	if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
	{  ?>
        <td valign="top" width="<?php echo $testTableWidth2; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="4" align="center" bgcolor="#c5c5c5"><strong>TOEFL PBT</strong><?
              $filePath=getFilePath(10, $toeflpscores[$i][0], $uid, $umasterid, $appid, $toeflpscores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($toeflpscores[$i][7]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Test Date&nbsp;</strong></td>
            <td width="70%" colspan="3">&nbsp;<?=$toeflpscores[$i][1]?></td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Section 1 &nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toeflpscores[$i][2]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Section 2 &nbsp;</strong></td>
            <td width="20%" valign="middle"><?=$toeflpscores[$i][3]?></td>
            <td width="30%" align="right"><strong>Total </strong> &nbsp;</td>
            <td width="20%" align="left">&nbsp;<?=$toeflpscores[$i][6]?></td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>Section 3 &nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toeflpscores[$i][4]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td width="30%" align="right"><strong>TWE &nbsp;</strong></td>
            <td width="20%">&nbsp;<?=$toeflpscores[$i][5]?></td>
            <td width="50%" colspan="2">&nbsp;</td>
          </tr>
        </table>
        </td>
    <? 
        } 
    } 
?> 

      <? for($i = 0; $i < $ieltsScoreCount; $i++){ 
          if ($ieltsScores[$i][1] != '00/0000') {
          ?>
        <td valign="top" width="<?php echo $testTableWidth2; ?>">
        <table width="100%" border="1" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="2" align="center" bgcolor="#c5c5c5"><strong>IELTS</strong><?
              $filePath=getFilePath(19, $ieltsScores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target="_blank" href="<?=$filePath?>" >(Scores)</a>
              <? } 
              if ($ieltsScores[$i][7]) {
                // Official report has been received.
                echo ' <i>ODR</i> ';    
              }
              ?>
              </td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Test Date&nbsp;</strong></td>
            <td>&nbsp;<?=$ieltsScores[$i][1] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Listening&nbsp;</strong></td>
            <td>&nbsp;<?=$ieltsScores[$i][2] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Reading&nbsp;</strong></td>
            <td>&nbsp;<?=$ieltsScores[$i][3] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Writing&nbsp;</strong></td>
            <td>&nbsp;<?=$ieltsScores[$i][4] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Speaking&nbsp;</strong></td>
            <td>&nbsp;<?=$ieltsScores[$i][5] ?></td>
          </tr>
          <tr>
            <td width="60%" align="right"><strong>Total&nbsp;</strong></td>
            <td>&nbsp;<?=$ieltsScores[$i][6] ?></td>
          </tr>
        </table>
        </td>
        <?php 
            }
          } 
if ($sumScores2 == 1) {
    echo '<td width="50%"></td>';
}           
?>
    </tr>    
</table>    
</td>
</tr>
<?php
}  
?>

<?php
if ($waiveToefl)
{
?>
    <tr>
        <td colspan="3">
            <table border="1"  cellpadding="1" cellspacing="0" width="100%">
                <tr><td><b>TOEFL Waived</b></td></tr>
            </table>
        </td>
    </tr>
<?php
}  
?>
<!-- END SCORES ROW/TABLE(S) --> 


<!-- RECOMMEND TABLE -->
<?
if(count($myRecommenders) > 0){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="#c5c5c5"><strong>Recommenders</strong></td>
        <td bgcolor="#c5c5c5"><strong>Type</strong></td>
        <td bgcolor="#c5c5c5">
            <strong>Title / Affiliation 
            <?php 
            if ($showRecommenderRelationship) 
            { 
                echo '/ Relationship'; 
            } 
            ?> 
            </strong>
        </td>
        <td bgcolor="#c5c5c5"><strong>Email / Phone </strong></td>
        <td bgcolor="#c5c5c5"><strong>Waive<br/>Buckley</strong></td>
      </tr>
      <?
      for($i = 0; $i < count($myRecommenders); $i++){
	  if($myRecommenders[$i][2] != ""){
	  ?>
	  <tr>
        <td>
		<?
        // PLB added following variables for ease of reference
        $recommendation_id = $myRecommenders[$i][0];
        $recommendation_content = $myRecommenders[$i][15];
        $recommendation_content_length = strlen($recommendation_content);
         
        if($myRecommenders[$i][9] != ""){

			$recFilePath = getFilePath(3, $myRecommenders[$i][0], $uid, $umasterid, $appid, $myRecommenders[$i][9]);

            /*
            * For MS Word files, give a link to a pdf created 
            * in the merge process, when available. 
            */
            $recFilePathRaw = getFilePathRaw(3, $myRecommenders[$i][0], $uid, $umasterid, $appid, $myRecommenders[$i][9]);
            $pdfRecFilePathRaw = $pdfRecFilePath = '';
            if ( strpos($recFilePathRaw, '.docx') !== FALSE ) {
                $pdfRecFilePathRaw = str_replace('.docx', '.pdf', $recFilePathRaw); 
                $pdfRecFilePath = str_replace('.docx', '.pdf', $recFilePath);       
            } elseif ( strpos($recFilePathRaw, '.doc') !== FALSE ) {
                $pdfRecFilePathRaw = str_replace('.doc', '.pdf', $recFilePathRaw);
                $pdfRecFilePath = str_replace('.doc', '.pdf', $recFilePath);    
            } elseif ( strpos($recFilePathRaw, '.rtf') !== FALSE ) {
                $pdfRecFilePathRaw = str_replace('.rtf', '.pdf', $recFilePathRaw);
                $pdfRecFilePath = str_replace('.rtf', '.pdf', $recFilePath);    
            }
            
            if ( $pdfRecFilePathRaw && file_exists($pdfRecFilePathRaw) 
                    && filemtime($pdfRecFilePathRaw) >= filemtime($recFilePathRaw) )
            {
                $recFilePath = $pdfRecFilePath;    
            }
			?>
			<input name="txtRecId" type="hidden" value="<?=$myRecommenders[$i][9]?>" />
			<? if($recFilePath != ""){ ?>
				<a href="<?=$recFilePath?>" target="_blank">
					<?=$myRecommenders[$i][2]?> <?=$myRecommenders[$i][3]?>
				</a><?           
            } else
			{
				echo $myRecommenders[$i][2]. " " . $myRecommenders[$i][3];
			}
        } elseif ($recommendation_content_length > 100) {   // PLB added elseif to test for "content" recommendation
           
           $recommendation_file = NULL;
           if ( isset($applicant_guid) ) {
            // Use variables set in pdf_link.inc.php
            $recommedation_filename = "recommendation_" . $appid . "_" . $myRecommenders[$i][0] . ".pdf";
            $recommendation_file = $datafileroot . "/" . $applicant_guid . "/" . $recommedation_filename;
            $recommendation_href = '../admin/fileDownload.php?file=';
            $recommendation_href .= urlencode($recommedation_filename) .'&amp;guid=' . $applicant_guid;              
           }

           if ( isset($applicant_guid) && file_exists($recommendation_file) ) {
                ?> 
                <a href="<?= $recommendation_href ?>" target="_blank">
                <?=$myRecommenders[$i][2]?> <?=$myRecommenders[$i][3]?>
                </a>
                <?    
           } else {
           ?>
            <a href="getContentRecommendation.php?recommendation_id=<?= $recommendation_id ?>" target="_blank">
                <?=$myRecommenders[$i][2]?> <?=$myRecommenders[$i][3]?>
            </a>
           <?
           }
        }
        // PLB - just print the name
        else
		{
			echo $myRecommenders[$i][2]." " .$myRecommenders[$i][3];
		} ?>&nbsp;		</td>
        <td><?=$myRecommenders[$i][14]?>&nbsp;</td>
        <td><?php echo htmlspecialchars($myRecommenders[$i][4], ENT_COMPAT, 'UTF-8', FALSE); ?> - 
            <?php echo htmlspecialchars($myRecommenders[$i][5], ENT_COMPAT, 'UTF-8', FALSE); ?>&nbsp;
            <?php
            if ($myRecommenders[$i][17])
            {
                echo '- ' . htmlspecialchars($myRecommenders[$i][17], ENT_COMPAT, 'UTF-8', FALSE);    
            }
            ?>
        </td>
        <td><?=$myRecommenders[$i][6]?> - <?=$myRecommenders[$i][7]?>&nbsp;</td>
        <td><? if($buckley == 1){ echo "Yes";}else{ echo "No";} ?></td>
      </tr>
	  <? } } ?>

<!-- END RECOMMEND TABLE -->
</table>
</td>
</tr>
<? } ?>


<!--  BEGIN EMPLOYMENT TABLE  -->
<? 
/*
* NOTE: marray_search doesn't work here, because it expects a 1D array,
* and $myPrograms is a 2D array. Upshot: this test always evaluates as true.
*/
if (marray_search( "MSE-Dist", $myPrograms) == 0 
        || marray_search( "MSE-Campus", $myPrograms) == 0
        || marray_search( "MSIT-ESE", $myPrograms) == 0
        || marray_search( "MSE-Portugal", $myPrograms) == 0
        || marray_search( "MSE-MSIT-Korea", $myPrograms) == 0
         )
{ 
    //$employments = getEmployments($appid);
    /*
    // MOVED this to printViewData
    $experienceRecords = getEmployments($appid);
    $yearsExperience = NULL;
    $employments = array();
    foreach ($experienceRecords as $experienceRecord) {
        if ( !$experienceRecord[1] && $experienceRecord[4] ) {
            // This is an MS-HCII summary record.
            $yearsExperience = $experienceRecord[4];  
        } else {
            // This is an MSE-MSIT experience record.
            $employments[] = $experienceRecord;    
        }    
    } */

    if ($yearsExperience) {
    ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td><b>Years of professional experience:</b> <?php echo $yearsExperience; ?>
  </td>
</tr>
</table>
</td>
</tr>
    <?php    
    }
/*    
if ($iniYearsExperience) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td>
  <b>Years of full-time professional experience:</b> <?php echo $iniYearsExperience['full_time_professional']; ?>
  <br>
  <b>Years of relevant industry experience:</b> <?php echo $iniYearsExperience['relevant_industry']; ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php    
} */

if (isset($em2YearsExperience) && $em2YearsExperience) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td>
  <b>Years of full-time professional experience:</b> <?php echo $em2YearsExperience['full_time_professional']; ?>
  <br>
  <b>Years of relevant industry experience:</b> <?php echo $em2YearsExperience['relevant_industry']; ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php    
}
    
    if(count($employments) > 0){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
<!-- EMPLOYMENT TABLE -->

  <tr><td colspan="7" bgcolor="#c5c5c5" align="center"><strong>Employment History</strong></td></tr>
  <tr>
    <td valign="top" bgcolor="#C5C5C5"><strong>Company:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Address:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Title:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Description:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Start Date:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>End Date:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Years Exp.:</strong></td>
  </tr>
  <? for($i = 0; $i < count($employments); $i++){ ?>
  <tr>
    <td valign="top">
            <?=$employments[$i][1] ?>&nbsp;</td>
    <td valign="top">
            <?=$employments[$i][5] ?>&nbsp;</td>
    <td valign="top">
            <?=$employments[$i][6] ?>&nbsp;</td>
    <td valign="top">
            <?=$employments[$i][7] ?>&nbsp;</td>
    <td valign="top">
            <?=$employments[$i][2] ?>&nbsp;</td>
    <td valign="top">
            <?= $employments[$i][3] ?>&nbsp;</td>
    <td valign="top">
            <?= $employments[$i][4] ?>&nbsp;</td>
  </tr>

  <? }//END PUBS LOOP ?>

<!-- END EMPLOYMENT TABLE -->
</table>
</td>
</tr>
<? }

if(count($codility) > 0)
{
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
<!-- CODILITY TABLE -->

  <tr><td colspan="6" bgcolor="#c5c5c5" align="center"><strong>Codility Scores</strong></td></tr>
  <tr>
    <td valign="top" bgcolor="#C5C5C5"><strong>Total Score</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Max Score</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>&percnt; Total Score</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>&percnt; Correctness</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>&percnt; Performance</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Similarity Check</strong></td>
  </tr>
  <tr>
    <td valign="top"><?= $codility['total_score'] ?></td>
    <td valign="top"><?= $codility['max_score'] ?></td>
    <td valign="top"><?= $codility['percent_total_score'] ?></td>
    <td valign="top"><?= $codility['percent_correctness'] ?></td>
    <td valign="top"><?= $codility['percent_performance'] ?></td>
    <td valign="top"><font color="red"><?= $codility['similarity_check'] ?></font></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#C5C5C5"><strong>Task</strong></td>
    <td valign="top" bgcolor="#C5C5C5" colspan="2"><strong>Score</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Correctness</strong></td>
    <td valign="top" bgcolor="#C5C5C5" colspan="2"><strong>Performance</strong></td>
  </tr>
  <tr>
    <td valign="top">1. <?= $codility['task1_name'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task1_score'] ?></td>
    <td valign="top"><?= $codility['task1_correctness'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task1_performance'] ?></td>
  </tr>
  <tr>
    <td valign="top">2. <?= $codility['task2_name'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task2_score'] ?></td>
    <td valign="top"><?= $codility['task2_correctness'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task2_performance'] ?></td>
  </tr>
  <tr>
    <td valign="top">3. <?= $codility['task3_name'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task3_score'] ?></td>
    <td valign="top"><?= $codility['task3_correctness'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task3_performance'] ?></td>
  </tr>
  <tr>
    <td valign="top">4. <?= $codility['task4_name'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task4_score'] ?></td>
    <td valign="top"><?= $codility['task4_correctness'] ?></td>
    <td valign="top" colspan="2"><?= $codility['task4_performance'] ?></td>
  </tr>
<!-- END CODILITY TABLE -->
</table>
</td>
</tr>
<? } ?>

<? } ?>


<!-- PUBLICATION ROW/TABLE -->
<? if(count($pubs) > 0){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">

  <tr><td colspan="5" bgcolor="#c5c5c5" align="center"><strong>Publications</strong></td></tr>
  <tr>
    <td valign="top" bgcolor="#C5C5C5"><strong>Title:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Author(s):</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Name of Journal or Conference:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Status:</strong></td>
    <td valign="top" bgcolor="#C5C5C5"><strong>Type:</strong></td>
  </tr>
  <? for($i = 0; $i < count($pubs); $i++){ ?>
  <tr>
    <td valign="top">
    <? 
    if($pubs[$i][9])
    {
        $filePath = getFilePath(26, $pubs[$i][0], $uid, $umasterid, $appid, $pubs[$i][9]);
        if ($filePath !== null) 
        {
        ?>
        <a target="_blank" href="<?=$filePath?>"><?=$pubs[$i][1]?></a>
        <?php
        }
    }
    elseif($pubs[$i][5] != "") 
    {
		$header = "";
		if(strstr($pubs[$i][5], "http") === false)
		{
			$header = "http://";
		}
		?>
		<a href="<?=$header.$pubs[$i][5]?>" target="_blank"><?=$pubs[$i][1]?></a>
	<? 
    }
    else
    { 
    ?>
	    <?=$pubs[$i][1] ?>
	<? 
    } 
    ?>		  
    </td>
    <td valign="top">
            <?=$pubs[$i][2] ?>&nbsp;</td>
    <td valign="top">
            <?= $pubs[$i][3] ?>&nbsp;</td>
    <td valign="top">
            <?= $pubs[$i][6] ?>&nbsp;</td>
    <td valign="top">
        <?php
        if ($pubs[$i][7] && $pubs[$i][7] != 'Other') {
            echo $pubs[$i][7];  // Type not other
        } elseif ($pubs[$i][8] && (!$pubs[$i][7] || $pubs[$i][7] == 'Other')) {
            echo 'Other: ' . $pubs[$i][8];  // Type other  
        }
        ?>
    </td>
  </tr>

  <? }//END PUBS LOOP ?>


</table>
</td>
</tr>
<? } ?>
<!-- END PUBS ROW/TABLE -->


<!-- FELLOWSHIP ROW/TABLE -->
<? if(count($fellows) > 0){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
  <tr><td colspan="6" bgcolor="#c5c5c5" align="center"><strong>Fellowships</strong></td></tr>
  <tr>
   <td valign="top" bgcolor="#C5C5C5"><strong>Name</strong></td>
   <td valign="top" bgcolor="#C5C5C5"><strong>Amount/Year</strong></td>
   <td valign="top" bgcolor="#C5C5C5"><strong>Duration (years)</strong></td>
   <td valign="top" bgcolor="#C5C5C5"><strong>Applied Date </strong></td>
   <td valign="top" bgcolor="#C5C5C5"><strong>Award Date</strong></td>
   <td valign="top" bgcolor="#C5C5C5"><strong>Status:</strong></td>
  </tr>
  <? for($i = 0; $i < count($fellows); $i++){ ?>
  <tr>
   <td valign="top"><?= $fellows[$i][1]?>
    <br/>
   </td>
   <td>$
     <?= $fellows[$i][2] ?></td>
   <td valign="top">
     <?= $fellows[$i][6] ?>
     <br/>
   </td>
   <td valign="top"><?= $fellows[$i][4] ?>&nbsp;</td>
   <td valign="top"><?= $fellows[$i][5] ?>&nbsp;</td>
   <td valign="top">
   <?php 
        echo $fellows[$i][3];    
        $awardletterFilePath = getFilePath(22, $fellows[$i][0], $uid, $umasterid, $appid, $fellows[$i][7]);
        if ($awardletterFilePath !== null)
        {
        ?>
            <a target="_blank" href="<?=$awardletterFilePath?>" >(Letter)</a>
        <?php    
        }
   ?>
   </td>
  </tr>
  <? } //END FELLOWS LOOP ?>
</table>
</td>
</tr>
<? } ?>
<!-- END FELLOW ROW/TABLE -->

<!-- VIDEO ESSAY ROW/TABLE --> 
<? if($videoEssayLink != ''){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
  <tr><td colspan="2" bgcolor="#c5c5c5" align="center"><strong>Video Essay</strong></td></tr>
  <tr>
   <td valign="top" bgcolor="#C5C5C5"><strong>Url</strong></td>
   <td valign="top" bgcolor="#C5C5C5"><strong>Access Code</strong></td>
  </tr>
  <tr>
   <td valign="top">
   <?php 
    if (strpos($videoEssayLink, '://') === FALSE) {
        $videoEssayLink = 'http://' . $videoEssayLink;
    }
   ?>
    <a href="<?=htmlspecialchars($videoEssayLink)?>" target="_BLANK"><?=htmlspecialchars($videoEssayLink)?></a>
   </td>
   <td valign="top"><?php echo htmlspecialchars($videoEssayCode); ?></td>
  </tr>
</table>
</td>
</tr>
<? } ?>
<!-- END VIDEO ESSAY ROW/TABLE --> 

<!-- WOMEN@IT/PIER/CUPS ROW/TABLE --> 
<?
 if( marray_search( "SCS", $myPrograms) !== false
|| marray_search( "CSD", $myPrograms) !== false
|| marray_search( "RI", $myPrograms) !== false
){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr>
    <!--<td  valign="top"><strong>CUPS:</strong> <?=$cups?></td>-->
    <td  valign="top"><strong>Women@IT Fellowship:</strong> <?=$wFellow?></td>
    <td  valign="top"><strong>PIER:</strong> <?=$pier?></td>
  </tr>
</table>
</td>
</tr>
<? } ?>
<!-- END MISC/SUPP ROW/TABLE -->


<!-- HCII PORTFOLIO TABLE -->
<?php
// MS-HCII "additional knowledge" and Stats-MS prereqs
if ($prog || $design || $stats) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <?php
  $isStatsMsApplication = FALSE;
  for ($i=0; $i < count($myPrograms); $i++) {
    if ($myPrograms[$i][0] == 100014) {
        $isStatsMsApplication = TRUE;
        continue;   
    }
  }
  
  if ($prog) {  
  ?>
  <tr>
    <td valign="top"><strong>
    <?php
    if ($isStatsMsApplication) {
        echo 'Required Courses:';
    } else {
        echo 'Additional Knowlege, Programming:';    
    }
    ?>
    </strong></td>
    <td><?php echo $prog; ?></td>
  </tr>
  <?php
  }
  
  if ($design) {  
  ?>
  <tr>
    <td valign="top"><strong>Additional Knowlege, Design:</strong></td>
    <td><?php echo $design; ?></td>
  </tr>
  <?php
  }  
  
  if ($stats) {
  ?>
  <tr>
    <td valign="top"><strong>
    <?php
    if ($isStatsMsApplication) {
        echo 'Experience:';
    } else {
        echo 'Additional Knowlege, Statistics:';    
    }
    ?>
    </strong></td>
    <td><?php echo $stats; ?></td>
  </tr>
  <?php
  }  
  ?>  
  
</table>
</td>
</tr>
<?php    
}

// PLB moved this to its own table 9/29/09
if( marray_search( "MS-HCII", $myPrograms) !== false
    || marray_search( "Human-Computer Interaction", $myPrograms) !== false
    || $isDesignDepartment || $isDesignPhdDepartment || $isDesignDdesDepartment)
{ 
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr>
    <td  valign="top">
    <?php
    if (!$isDesignDepartment && !$isDesignPhdDepartment && !$isDesignDdesDepartment)
    {
    ?>
    <strong>Sending Portfolio (HCII):</strong> <?=$port?>&nbsp;&nbsp;
    <?php
    }
    ?>
    <strong>Portfolio URL:</strong>&nbsp; 
    <?
    if ($portLink != NULL || $portLink != "" ) 
    { 
        if (strpos($portLink, '://') === FALSE) {
            $portLink = 'http://' . $portLink;
        }
    ?>
      <a href="<?=$portLink?>" target="_BLANK"><?=$portLink?></a>
    <?}?>
    <?php
    if ($portfolioPassword != NULL || $portfolioPassword != "" ) 
    { 
    ?>
        &nbsp;&nbsp;<strong>Portfolio Password:</strong>&nbsp;
        <?php echo $portfolioPassword; ?>
    <?php
    }
    ?>
    </td>
  </tr>
</table>
</td>
</tr>
<? } ?> 
<!-- END HCII PORTFOLIO TABLE --> 


<!-- POSSIBLE ADVISORS ROW/TABLE --> 
<?
// PLB replaced test to use myAdvisors array 
// if($advisor1 != "" || $advisor2 != "" || $advisor3 != "" || $advisor4 != ""){ 

if ( count($myAdvisors) > 0 ) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<!-- PLB changed "advisors" to "possible advisors" -->
<tr><td colspan="3" align="center" bgcolor="#C5C5C5"><strong> Possible Advisor(s) </strong></td></tr>
<tr>
  <td colspan="3" width="100%">
  <?
  // PLB replaced logic below to use myAdvisors array
  for ( $i = 0; $i < count($myAdvisors); $i++ ) {  
        echo $myAdvisors[$i] . "<br />";      
  }
  /*
    if($advisor1 != ""){echo $advisor1."<br/>";}
    if($advisor1 != ""){echo $advisor2."<br/>";}
    if($advisor1 != ""){echo $advisor3."<br/>";}
    if($advisor1 != ""){echo $advisor4;}
  */
  ?>
  </td>
</tr>
</table>
</td>
</tr>
<? } ?>
<!-- END ADVISORS ROW/TABLE --> 


<!-- CURRENTLY ENROLLED/HONORS ROW/TABLE --> 
<? if( $cur_enrolled == "yes" || $honors != "" ){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
  <tr>
    <td bgcolor="#c5c5c5"><strong>Currently Enrolled </strong></td>
    <td bgcolor="#c5c5c5"><strong>Honors</strong></td>
  </tr>
  <tr>
    <td><?=$cur_enrolled?></td>
    <td><?=stripslashes($honors)?></td>
  </tr>
</table>
</td>
</tr>
<? } ?>
<!-- END CURRENTLY ENROLLED/HONORS ROW/TABLE --> 


<!-- OTHER UNIVERSITIES/PROGRAMS ROW/TABLE -->
<? // PLB changed test 09/29/09
if($otherUni != "" || !empty($crossDeptProgs) ){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">

<tr>
  <td bgcolor="#c5c5c5"><strong>Other Institutes </strong></td>
  <td bgcolor="#c5c5c5"><strong>Other Programs </strong></td>
</tr>

<tr>
  <td><?=$otherUni?>&nbsp;</td>
  <td>
    <?
	if(!is_array($crossDeptProgs))
	{
		$crossDeptProgs = explode(",",$crossDeptProgs);
	}

	$tmpDisplayItems = array();
	
	for($i = 0; $i < count($crossDeptProgs); $i++)
	{
		$showItem = true;
		if($crossDeptProgs[$i] != "Other")
		{
			for($k = 0; $k < count($tmpDisplayItems); $k++)
			{
				if($tmpDisplayItems[$k] == $crossDeptProgs[$i])
				{
					$showItem = false;
					break;
				}
			}
			if($showItem == true)
			{
				array_push($tmpDisplayItems, $crossDeptProgs[$i]);
				echo $crossDeptProgs[$i]. "<br/>";
			}			
		}
	}
    
    if ($crossDeptProgsOther)
    {
        echo $crossDeptProgsOther;
    }
    {
        
    }
	?> &nbsp;</td>
  </tr>
</table>
</td>
</tr>
<? } ?>
<!-- END OTHER UNIVERSITIES/PROGRAMS ROW/TABLE --> 


<?php
/*
* INI Disciplinary Action 
*/
if ($iniDisciplinaryAction) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Disciplinary Action</strong></td></tr>
  <tr>
      <td>
          <b>Sanctions Imposed:</b>
          <?php 
          echo ($iniDisciplinaryAction['sanction'] ? 'Yes' : 'No'); 
          if ($iniDisciplinaryAction['sanction_description'])
          {
              echo ': ' . htmlspecialchars($iniDisciplinaryAction['sanction_description']);
          }
          ?>
          <br>
          <b>Retraction Requested:</b>
          <?php 
          echo ($iniDisciplinaryAction['retraction'] ? 'Yes' : 'No'); 
          if ($iniDisciplinaryAction['retraction_description'])
          {
              echo ': ' . htmlspecialchars($iniDisciplinaryAction['retraction_description']);
          }
          ?>
      </td>
  </tr>
</table>
</td>
</tr>
<?php
}    
?>
<?php
if (isset($em2DisciplinaryAction) && $em2DisciplinaryAction) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Disciplinary Action</strong></td></tr>
  <tr>
      <td>
          <b>Sanctions Imposed:</b>
          <?php 
          echo ($em2DisciplinaryAction['sanction'] ? 'Yes' : 'No'); 
          if ($em2DisciplinaryAction['sanction_description'])
          {
              echo ': ' . htmlspecialchars($em2DisciplinaryAction['sanction_description']);
          }
          ?>
          <br>
          <b>Retraction Requested:</b>
          <?php 
          echo ($em2DisciplinaryAction['retraction'] ? 'Yes' : 'No'); 
          if ($em2DisciplinaryAction['retraction_description'])
          {
              echo ': ' . htmlspecialchars($em2DisciplinaryAction['retraction_description']);
          }
          ?>
      </td>
  </tr>
</table>
</td>
</tr>
<?php
}    
?>


<?php
/*
* INI Financial Support 
*/
if ($iniFinancialSupport) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Financial Support</strong></td></tr>
  <tr>
      <td>
          <b>Wishes to be considered for financial support from Carnegie Mellon:</b>
          <?php echo ($iniFinancialSupport['request_consideration'] ? 'Yes' : 'No'); ?>
          <br>
          <b>Would attend Carnegie Mellon without financial aid from the university:</b>
          <?php echo ($iniFinancialSupport['attend_without_support'] ? 'Yes' : 'No'); ?>
          <br>
          <?php
          if ($iniFinancialSupport['receive_outside_support_type'] || $iniFinancialSupport['receive_outside_support_source'])     
          {
          ?>
            <b>Receiving outside support:</b>
            <?php 
            echo htmlspecialchars($iniFinancialSupport['receive_outside_support_type']) . ': '; 
            echo htmlspecialchars($iniFinancialSupport['receive_outside_support_source']);
            ?>
            <br>
          <?php  
          }

          if ($iniFinancialSupport['apply_outside_support_type'] || $iniFinancialSupport['apply_outside_support_source'])     
          {
          ?>
            <b>Applying for outside support:</b>
            <?php 
            echo htmlspecialchars($iniFinancialSupport['apply_outside_support_type']) . ': '; 
            echo htmlspecialchars($iniFinancialSupport['apply_outside_support_source']);
            ?>
            <br>
          <?php  
          }
          
          if ($iniFinancialSupport['family_support_type'] || $iniFinancialSupport['family_support_amount'])     
          {
          ?>
            <b>Family/Friends Support:</b>
            <?php 
            echo htmlspecialchars($iniFinancialSupport['family_support_type']) . ': '; 
            echo htmlspecialchars($iniFinancialSupport['family_support_amount']);
            ?>
            <br>
          <?php  
          }
          
          if ($iniFinancialSupport['other_support_source'])     
          {
          ?>
            <b>Other support source:</b>
            <?php 
            echo htmlspecialchars($iniFinancialSupport['other_support_source']);
            ?>
          <?php  
          }
          
          if (count($iniFinancialSupportDocuments) > 0)
          {
          ?>
            <br>
            <b>Documents:</b>
            <?php 
            $financialDocCounter = 1;
            foreach($iniFinancialSupportDocuments as $financialSupportDocument)
            {
                $filePath=getFilePath(27, $financialSupportDocument['document_id'], $uid, $umasterid, $appid);
                if ($filePath !== null) 
                {
                ?>
                <a target="_blank" href="<?=$filePath?>">Financial Support <?=$financialDocCounter?></a>
                <?php
                }
                $financialDocCounter++;
            }
            ?>  
          <?php
          }
          ?>   
      </td>
  </tr>
</table>
</td>
</tr>
<?php
}    
?>

<?php
if (isset($em2FinancialSupport) && $em2FinancialSupport) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Financial Support</strong></td></tr>
  <tr>
      <td>
          <b>Wishes to be considered for financial support from Carnegie Mellon:</b>
          <?php echo ($em2FinancialSupport['request_consideration'] ? 'Yes' : 'No'); ?>
          <br>
          <b>Would attend Carnegie Mellon without financial aid from the university:</b>
          <?php echo ($em2FinancialSupport['attend_without_support'] ? 'Yes' : 'No'); ?>
          <br>
          <?php
          if ($em2FinancialSupport['receive_outside_support_type'] || $em2FinancialSupport['receive_outside_support_source'])     
          {
          ?>
            <b>Receiving outside support:</b>
            <?php 
            echo htmlspecialchars($em2FinancialSupport['receive_outside_support_type']) . ': '; 
            echo htmlspecialchars($em2FinancialSupport['receive_outside_support_source']);
            ?>
            <br>
          <?php  
          }

          if ($em2FinancialSupport['apply_outside_support_type'] || $em2FinancialSupport['apply_outside_support_source'])     
          {
          ?>
            <b>Applying for outside support:</b>
            <?php 
            echo htmlspecialchars($em2FinancialSupport['apply_outside_support_type']) . ': '; 
            echo htmlspecialchars($em2FinancialSupport['apply_outside_support_source']);
            ?>
            <br>
          <?php  
          }
          
          if ($em2FinancialSupport['family_support_type'] || $em2FinancialSupport['family_support_amount'])     
          {
          ?>
            <b>Family/Friends Support:</b>
            <?php 
            echo htmlspecialchars($em2FinancialSupport['family_support_type']) . ': '; 
            echo htmlspecialchars($em2FinancialSupport['family_support_amount']);
            ?>
            <br>
          <?php  
          }
          
          if ($em2FinancialSupport['other_support_source'])     
          {
          ?>
            <b>Other support source:</b>
            <?php 
            echo htmlspecialchars($em2FinancialSupport['other_support_source']);
            ?>
          <?php  
          }
          
          if (count($em2FinancialSupportDocuments) > 0)
          {
          ?>
            <br>
            <b>Documents:</b>
            <?php 
            $financialDocCounter = 1;
            foreach($em2FinancialSupportDocuments as $financialSupportDocument)
            {
                $filePath=getFilePath(27, $financialSupportDocument['document_id'], $uid, $umasterid, $appid);
                if ($filePath !== null) 
                {
                ?>
                <a target="_blank" href="<?=$filePath?>">Financial Support <?=$financialDocCounter?></a>
                <?php
                }
                $financialDocCounter++;
            }
            ?>  
          <?php
          }
          ?>   
      </td>
  </tr>
</table>
</td>
</tr>
<?php
}    
?>


<?php
/*
* Dietrich recognitions
*/
if ($dietrichRecognitions) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td>
  <b>Recognitions:</b> <?php echo htmlspecialchars($dietrichRecognitions); ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>


<?php
/*
* Dietrich language proficiency 
*/
$writingSampleCount = count($writingSamples);
$languageAssessmentCount = count($languageAssessments);
if ($writingSampleCount > 0
    || $languageAssessmentCount > 0
    || $speakingSample) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="4" bgcolor="#c5c5c5" align="center"><strong>Writing Samples and Language Proficiency</strong></td></tr>
  <?php 
  if ($writingSampleCount > 0 || $speakingSample)
  {
  ?>
    <tr><td><b>
      <?php
      $i = 1;
      foreach ($writingSamples as $writingSample)
      {
      ?>
        &nbsp;&nbsp;<a target="_blank" href="<?php echo $writingSample; ?>">Writing Sample<?php echo ($writingSampleCount > 1 ? ' ' . $i : ''); ?></a>
      <?php
        $i++;
      }
    
      if ($speakingSample)
      {
      ?>
        &nbsp;&nbsp;<a target="_blank" href="<?php echo $speakingSample; ?>">Speaking Sample</a>
      <?php
      }
      ?>
    </b></td></tr>
  <?php
  }

  if ($languageAssessmentCount > 0)
  {
  ?>
    <tr><td>
    <table border="1">
        <tr>
            <th>Language</th>
            <th>Speaking</th>
            <th>Reading</th>
            <th>Writing</th>
            <?php
            if (!isHistoryApplication($appid))
            {  
            ?>
            <th>Listening</th>
            <th>Native Speaker</th>
            <th>Years</th>
            <th>Level</th>
            <th>Competency Evidence</th>
            <?php       
            }    
            ?>
        </tr>
        <?php
        foreach ($languageAssessments as $languageAssessment)
        {
        ?>
        <tr>
            <td><?php echo htmlspecialchars($languageAssessment['language']); ?></td>
            <td><?php echo htmlspecialchars($languageAssessment['speaking_rating']); ?></td>
            <td><?php echo htmlspecialchars($languageAssessment['reading_rating']); ?></td>
            <td><?php echo htmlspecialchars($languageAssessment['writing_rating']); ?></td>
            <?php
            if (!isHistoryApplication($appid))
            {  
            ?>
            <td><?php echo htmlspecialchars($languageAssessment['listening_rating']); ?></td>
            <td><?php echo ($languageAssessment['native_speaker'] ? 'Yes' : 'No'); ?></td>
            <td><?php echo htmlspecialchars($languageAssessment['years_study']); ?></td>
            <td><?php echo htmlspecialchars($languageAssessment['study_level']); ?></td>
            <td><?php echo htmlspecialchars($languageAssessment['competency_evidence']); ?></td>
            <?php       
            }    
            ?>
        </tr>
        <?php    
        }
        ?>
    </table>
    </td></tr>
  <?php
  }
  ?>
</table>
</td>
</tr>
<?php
}  

if($dietrichLanguageRecommendation){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
      <tr>
        <td bgcolor="#c5c5c5"><strong>Language Recommender</strong></td>
        <td bgcolor="#c5c5c5"><strong>Title / Affiliation </strong></td>
        <td bgcolor="#c5c5c5"><strong>Email / Phone </strong></td>
        <td bgcolor="#c5c5c5"><strong>Language Specialization</strong></td>
      </tr>
      <tr>
        <td>
        <?php
        if($dietrichLanguageRecommendation['datafile_id'])
        {
            $langRecFilePath = getFilePath(25, $dietrichLanguageRecommendation['id'], $uid, $umasterid, $appid, 
                $dietrichLanguageRecommendation['datafile_id']);

            /*
            * For MS Word files, give a link to a pdf created 
            * in the merge process, when available. 
            */
            $langRecFilePathRaw = getFilePathRaw(25, $dietrichLanguageRecommendation['id'], $uid, $umasterid, $appid, 
                $dietrichLanguageRecommendation['datafile_id']);
            $pdfLangRecFilePathRaw = $pdfLangRecFilePath = '';
            if ( strpos($langRecFilePathRaw, '.docx') !== FALSE ) {
                $pdfLangRecFilePathRaw = str_replace('.docx', '.pdf', $langRecFilePathRaw); 
                $pdfLangRecFilePath = str_replace('.docx', '.pdf', $langRecFilePathRaw);       
            } elseif ( strpos($langRecFilePathRaw, '.doc') !== FALSE ) {
                $pdfLangRecFilePathRaw = str_replace('.doc', '.pdf', $langRecFilePathRaw);
                $pdfLangRecFilePath = str_replace('.doc', '.pdf', $langRecFilePathRaw);    
            } elseif ( strpos($langRecFilePathRaw, '.rtf') !== FALSE ) {
                $pdfLangRecFilePathRaw = str_replace('.rtf', '.pdf', $langRecFilePathRaw);
                $pdfLangRecFilePath = str_replace('.rtf', '.pdf', $langRecFilePathRaw);    
            }
            
            if ( $pdfLangRecFilePathRaw && file_exists($pdfLangRecFilePathRaw) 
                    && filemtime($pdfLangRecFilePathRaw) >= filemtime($langRecFilePathRaw) )
            {
                $langRecFilePath = $pdfLangRecFilePath;    
            }
            if($langRecFilePath != "")
            { 
            ?>
                <a href="<?=$langRecFilePath?>" target="_blank">
                    <?=$dietrichLanguageRecommendation['firstname']?> <?=$dietrichLanguageRecommendation['lastname']?>
                </a>
            <?           
            } 
            else
            {
                echo $dietrichLanguageRecommendation['firstname'] . " " . $dietrichLanguageRecommendation['lastname'];
            }
        }
        else
        {
            echo $dietrichLanguageRecommendation['firstname'] . " " . $dietrichLanguageRecommendation['lastname'];
        } 
        ?>
        &nbsp;        
        </td>
        <td><?php echo htmlspecialchars($dietrichLanguageRecommendation['title'], ENT_COMPAT, 'UTF-8', FALSE); ?> - 
            <?php echo htmlspecialchars($dietrichLanguageRecommendation['company'], ENT_COMPAT, 'UTF-8', FALSE); ?>&nbsp;</td>
        <td><?=$dietrichLanguageRecommendation['email']?> - <?=$dietrichLanguageRecommendation['address_perm_tel']?>&nbsp;</td>
        <td><?=$dietrichLanguageRecommendation['language_specialization']?></td>
      </tr>
</table>
</td>
</tr>

<?php 
}

/*
* Dietrich attendance status 
*/
if ($attendanceStatus) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td><b>Attendance Status:</b> <?php echo htmlspecialchars($attendanceStatus); ?></td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>

<?php
/*
* Dietrich cmu affiliation
*/
if ($cmuAffiliation) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td width="50%">
  <b>CMU student or alumnus:</b> <?php echo ($cmuAffiliation['student_or_alumnus'] ? 'Yes' : 'No'); ?>
  </td>
  <td>
  <b>CMU employee:</b> <?php echo ($cmuAffiliation['employee'] ? 'Yes' : 'No'); ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>

<?php
/*
* Dietrich disability
*/
if ($disability && ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1)) 
{
    // Only show disability to SU and admins
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td>
  <b>Disability:</b> <?php echo htmlspecialchars($disability); ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>

<?php
/*
* Dietrich financial support
*/
if ($dietrichFinancialSupport) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td>
  <?php
  if ($dietrichFinancialSupport['qualified_assistance'] !== NULL)
  {
  ?>
    <b>Qualified for federal disadvantaged assistance:</b> <?php echo ($dietrichFinancialSupport['qualified_assistance'] ? 'Yes' : 'No'); ?><br>
  <?php
  }
  
  if ($dietrichFinancialSupport['received_loans'] !== NULL)
  {
  ?>
    <b>Received Health Professional Student Loans (HPSL), Loans for Disadvantaged Student Program:</b> <?php echo ($dietrichFinancialSupport['received_loans'] ? 'Yes' : 'No'); ?><br>
  <?php
  }
  
  if ($dietrichFinancialSupport['received_scholarships'] !== NULL)
  {
  ?>
    <b>Received scholarships from the U.S. Department of Health and Human Services under the Scholarship for Individuals with Exceptional Financial Need:</b> <?php echo ($dietrichFinancialSupport['received_scholarships'] ? 'Yes' : 'No'); ?><br>
  <?php
  }

  if ($dietrichFinancialSupport['support_sources'])
  {
  ?>
    <b>Sources of Support:</b> <?php echo htmlspecialchars($dietrichFinancialSupport['support_sources']); ?><br>
  <?php
  }
  
  if ($dietrichFinancialSupport['interested_b2_training'] !== NULL)
  {
  ?>
    <b>Interested in Behavioral Brain Research Training Program:</b> <?php echo ($dietrichFinancialSupport['interested_b2_training'] ? 'Yes' : 'No'); ?><br>
  <?php
  }
  ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>


<?php
/*
* Dietrich application sharing
*/
if ($dietrichSharing) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td>
  <b>Share application with SDS:</b> <?php echo ($dietrichSharing['sds'] ? 'Yes' : 'No'); ?><br>
  <b>Share application with Tepper:</b> <?php echo ($dietrichSharing['tepper'] ? 'Yes' : 'No'); ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>

<?php
/*
* Design assistantship
*/
if ($requestAssistantship !== NULL) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td width="50%">
  <b>Request assistantship:</b> <?php echo ($requestAssistantship ? 'Yes' : 'No'); ?>
  </td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>

<!-- REFERRAL -->
<?php
if ($referral) {
?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td><b>Referred to program by:</b> <?php echo $referral; ?></td>
</tr>
</table>
</td>
</tr>
<?php
}    
?>


<!-- CNBC/AUTHORIZE ROW/TABLE --> 
<?
// PLB moved this to its own test/table 9/30/09 
 if( marray_search( "CNBC Graduate", $myPrograms) !== false){ ?>
<tr>
<td colspan="3">
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<tr>
  <td><b>Authorize to collect information:</b> <? if($permission == 1){ echo "Yes";}else{echo "No";}?></td>
</tr>
</table>
</td>
</tr>
<? } ?>
<!-- END CNBC/AUTHORIZE ROW/TABLE --> 

<? }//end student contact HIDE ?>

</table>
<!-- END BIG TABLE -->
