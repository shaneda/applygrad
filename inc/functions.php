<?
if (isset($_SERVER['HTTP_USER_AGENT'])) {
    $BROWSER = $_SERVER['HTTP_USER_AGENT'];
}
//echo $BROWSER;
$CONFIG['security']['password_generator'] = array(
	"C" => array('characters' => 'abcdefghjkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ', 'minimum' => 3, 'maximum' => 4),
	"S" => array('characters' => "!-", 'minimum' => 1, 'maximum' => 2),
	"N" => array('characters' => '23456789', 'minimum' => 3, 'maximum' => 4)
);

// Enable option to exclude scriptaculous by setting $exclude_scriptaculous
// before including this file. - PLB 01/22/09
if ( !isset($exclude_scriptaculous) ) {
    echo "<script src='../inc/prototype.js' type='text/javascript'></script><script src='../inc/scriptaculous.js' type='text/javascript'></script>";
}

function showEditText($strVal, $type, $name, $allowEdit, $required=false, $list = array(array()), $enabled = true, $size=10, $maxlen=255)
{
	
	$class = "tblItem";
	$validate = "";
	global $BROWSER;
	if($required == true)
	{
		$class = "tblItemRequired";
		//$validate = "onblur=\"validate(this.id , this.value)\"";
	}

	if($allowEdit == true)
	{
		switch($type)
		{
            case "inst-textbox":
                
                if(is_array($list) && count($list) > 1)
                {
                    echo "\n<script language='javascript' charset='UTF-8'>\nvar arr".$name." = new Array(";
                    for($i = 0; $i < count($list); $i++)
                    {
                       $val = str_replace('"',"&#34;",$list[$i][1]);
                       $val = str_replace("'","\'",$val);
                        echo "\"".$val."\"";
                        if($i < count($list)-1)
                        {
                            echo ",\n";
                        }
                    }

                    echo ");\n</script>\n";
                    ?>
                    <input class="<?=$class?>" autocomplete="off" id="<?=$name?>" name="<?=$name?>" maxlength="<?=$maxlen?>" size="<?=$size?>" type="text" value="<?=$strVal?>" />
                    <div id="ac_<?=$name?>" name="ac_<?=$name?>" class="dropdown"></div>
                    <script type="text/javascript">new Autocompleter.Local('<?=$name?>', 'ac_<?=$name?>', arr<?=$name?>, {partialSearch: true, fullSearch:true})</script>
                    <?
                }else
                {
                    echo "<input name='".$name."' id='".$name."' type='text' class='".$class."' maxlength='".$maxlen."' size='".$size."' value='" . htmlspecialchars($strVal, ENT_QUOTES) ."' ".$validate." >\n";
                }

                break;
			case "textbox":
            
				if(is_array($list) && count($list) > 1)
				{
					echo "\n<script language='javascript'>\nvar arr".$name." = new Array(";
					for($i = 0; $i < count($list); $i++)
					{
					    $val = str_replace('"',"&#34;",$list[$i][1]);
						$val = str_replace("'","\'",$val);
						echo "\"".$val."\"";
						if($i < count($list)-1)
						{
							echo ",\n";
						}
					}

					echo ");\n</script>\n";
					?>
					<input class="<?=$class?>" autocomplete="off" id="<?=$name?>" name="<?=$name?>" maxlength="<?=$maxlen?>" size="<?=$size?>" type="text" value="<?=$strVal?>" />
					<div id="ac_<?=$name?>" name="ac_<?=$name?>" class="dropdown"></div>
					<script type="text/javascript">new Autocompleter.Local('<?=$name?>', 'ac_<?=$name?>', arr<?=$name?>, {partialSearch: true, fullSearch:true})</script>
					<?
				}else
				{
					echo "<input name='".$name."' id='".$name."' type='text' class='".$class."' maxlength='".$maxlen."' size='".$size."' value='" . htmlspecialchars($strVal, ENT_QUOTES) ."' ".$validate." >\n";
				}

				break;
			case "password":
				echo "<input name='".$name."' id='".$name."' type='password' class='".$class."' maxlength='".$maxlen."' value='".htmlspecialchars($strVal)."' ".$validate.">\n";
				break;
			case "hidden":
				echo "<input name='".$name."' id='".$name."' type='hidden' value='".htmlspecialchars($strVal)."'>\n";
				break;
			case "textarea":
				echo "<textarea name='".$name."' id='".$name."' cols='60' rows='5' class='".$class."'>".htmlspecialchars($strVal)."</textarea>";
				break;
			case "listbox":
                if ((strpos($_SERVER['PHP_SELF'],'interestsAdvisorsWithOthers') !== false)
                        && (strpos($name, 'lbAdvisor') !== false)) {
                    echo '<select name="'.$name.'" class="'.$class.'" id="'.$name.'" onChange="ShowHideOther(this);"'.'>\n';
                } else {
				    echo "<select name='".$name."' class='".$class."' id='".$name."' ".$validate.">\n";
                }
				echo "<option value=''>Choose One</option>\n";
				for($i = 0; $i < count($list); $i++)
				{
				
					$selected = "";
					if(is_array($strVal))
					{
						if( count($strVal)>0)
						{                              
							$strVal = $strVal[0];
						}
					}
					
					if($list[$i][0] == htmlspecialchars($strVal) && $strVal != "")
					{
						$selected = "selected";
					}
					echo "<option value='".$list[$i][0]."' ".$selected.">".$list[$i][1]."</option>\n";
				}
				echo "</select>\n";   
				break;
			case "checkbox":
				$checked = "";
				if(htmlspecialchars($strVal) == "1" || htmlspecialchars($strVal) == 1)
				{
					$checked = "checked";
				}
				if(htmlspecialchars($enabled) == false || htmlspecialchars($enabled) == "false")
				{
					$enabled = "disabled='disabled'";
				}
				else
				{
					$enabled = "";
					//echo "check if yes:";
				}
				echo "<input name='".$name."' type='checkbox' class='".$class."' id='".$name."' ".$checked." ".$validate." ".$enabled.">\n";
				break;

			case "checklist":


					if(htmlspecialchars($enabled) == false || htmlspecialchars($enabled) == "false")
					{
						$enabled = "disabled='disabled'";
					}
                    else
                    {
                        $enabled = "";
                    }

					for($i = 0; $i < count($list); $i++)
					{
						$checked = "";
						if(is_array($strVal))
						{
							for($j = 0; $j < count($strVal); $j++)
							{
								//echo $strVal[$j][0] ."|". $list[$i][0]. ".<br>";
								
								if(!is_array($strVal[$j])){
									if($strVal[$j] == $list[$i][0])
									{
	
										$checked = "checked";
										break;
									}
								}else
								{
									if($strVal[$j][0] == $list[$i][0])
									{
	
										$checked = "checked";
										break;
									}
								}
							}
						}

						echo "<input name='".$name."_".$list[$i][0]."' type='checkbox' class='".$class."' id='".$name."_".$list[$i][0]."' ".$checked." ".$validate." ".$enabled.">".$list[$i][1]."<br>\n";
					}

				break;
				case "radiogrouphoriz":
					for($i = 0; $i < count($list); $i++)
					{
						$selected = "";
						if(is_array($strVal))
						{
							if( count($strVal)>0)
							{
								$strVal = $strVal[0];
							}
						}
						
						if($list[$i][0] == htmlspecialchars($strVal))
						{
							$selected = "checked";
						}
						echo "<input name='".$name."' type='radio' value='".$list[$i][0]."' ".$selected.">".$list[$i][1];
					}
				break;
                case "radiogrouphoriz2":
                    for($i = 0; $i < count($list); $i++)
                    {
                        $selected = "";
                        if(is_array($strVal))
                        {
                            if( count($strVal)>0)
                            {
                                $strVal = $strVal[0];
                            }
                        }

                        if($list[$i][0] == htmlspecialchars($strVal))
                        {
                            $selected = "checked";
                        }
                        echo "<input name='".$name."' type='radio' value='".$list[$i][0]."' ".$selected.">".$list[$i][1]. "&nbsp;";
                    }
                break;
                case "radiogrouphoriz3":
                    ?><table class="tblItem" border="0" cellpadding="2" cellspacing="0"><tr><?
                    //build table header
                    for($i = 0; $i < count($list); $i++)
                    {
                        ?><td align="center"><?=$list[$i][1]?></td><?
                    }
                    ?></tr><tr><?
                    for($i = 0; $i < count($list); $i++)
                    {
                        $selected = "";
                        if(is_array($strVal))
                        {
                            if( count($strVal)>0)
                            {
                                $strVal = $strVal[0];
                            }
                        }

                        if($list[$i][0] == htmlspecialchars($strVal))
                        {
                            $selected = "checked";
                        }
                        echo "<td align='center'><input name='".$name."' type='radio' value='".$list[$i][0]."' ".$selected."></td>";
                    }
                    ?></tr></table><?
                break;
                case "radiogrouphoriz4":
                    for($i = 0; $i < count($list); $i++)
                    {
                        $selected = "";
                        if (isset($strVal)) {
                            if(is_array($strVal))
                            {
                                if( count($strVal)>0)
                                {
                                    $strVal = $strVal[0];
                                }
                            }
                           
                            if($list[$i][0] == htmlspecialchars($strVal))
                            {
                                $selected = "checked";
                            }
                        }
                        echo "<input name='".$name."' type='radio' value='".$list[$i][0]."' required ".$selected.">".$list[$i][1];
                    }
                break;
				case "button":
					echo "<input name='".$name."' type='submit' class='tblItem' id='".$name."' value='".htmlspecialchars($strVal)."'>\n";
				break;
				case "button2":
					echo "<input name='".$name."' type='button' class='tblItem' id='".$name."' value='".htmlspecialchars($strVal)."'>\n";
				break;
				case "file":
					echo "<input name='".$name."' id='".$name."' type='file' class='tblItem' maxlength='255' ".$validate.">\n";
				break;
				case "linkbutton":
				
					if(strstr($BROWSER, 'MSIE') !== false)
					{
						echo "<a href='#' onclick=\"document.getElementById('sender').value='".$name."'; document.forms[0].submit()\">".htmlspecialchars($strVal)."</a>";

					}else
					{
						
						echo "<a href='javascript:;' onclick=\"document.form1.sender.value='".$name."'; document.form1.submit()\">".htmlspecialchars($strVal)."</a>";

					}

				break;



			}
	}
	else
	{
		

		switch($type)
		{
			case "checkbox":
				echo "<input name='".$name."' id='".$name."' type='hidden' value='".htmlspecialchars($strVal)."' >\n";
				if($strVal == "1" || $strVal == 1)
				{
					$strVal = "yes ";
				}else{
					$strVal = "no ";
				}
				break;
			case "button":
				echo "<input name='".$name."' type='button' disabled='true' class='tblItem' id='".$name."' value='".htmlspecialchars($strVal)."'>\n";
				$strVal = "";
				break;
            case "hidden":
                echo "<input name='".$name."' type='hidden' id='".$name."' value='".htmlspecialchars($strVal)."'>\n";
                $strVal = "";
                break;
			default:
				echo "<input name='".$name."' id='".$name."' type='hidden' value='".htmlspecialchars($strVal)."' >\n";
				break;
		}
		echo "<span class='tblItem'>".htmlspecialchars($strVal)."</span>";
	}
}
//UPDATE REQUIREMENTS COMPLETION
//UPDATE lu_application_appreqs TABLE
function updateReqComplete($linkname, $complete = 0, $bypassEmail = false, $saveDate = true)
{
	$doAdd = true;
	$reqid = "";
    
    if ((isset($_SESSION['domainid']) 
                && function_exists('isIniDomain') 
                && isIniDomain($_SESSION['domainid'])) || 
        (isset($_SESSION['roleDepartmentId']) 
            && function_exists('isIniDepartment') 
            && isIniDepartment($_SESSION['roleDepartmentId'])))
    {
        logAccessIni($linkname);
    }
    
    if ($linkname == 'resume.php') 
    {      
        if ( isset($_SESSION['domainid']) 
            && $_SESSION['domainid'] == 44 ) 
        {
            // Stats
            $sql = "select * from applicationreqs where id = 13";    
        }
        elseif ( isset($_SESSION['domainid']) 
            && isEM2Domain($_SESSION['domainid'])) {
                $sql = "select * from applicationreqs where id = 5";    
            } 
            
        elseif ( isset($_SESSION['domainid']) && 
                ($_SESSION['domainid'] == 41     // RI-MS-RT
                    || $_SESSION['domainid'] == 74 // History
                    || $_SESSION['domainid'] == 77 // Psychology
                    || $_SESSION['domainid'] == 76 // Philosophy
                    || isPhilosophyDomain($_SESSION['domainid'])
                    )
                ) 
        {
            $sql = "select * from applicationreqs where id = 14";
        }
        else
        {
            // Everybody else.
            $sql = "select * from applicationreqs where id = 5";
        }    
    } 
    else 
    {
        $sql = "select * from applicationreqs where linkname ='".$linkname."'";    
    }
    
	$result = mysql_query($sql) or die(mysql_error(). $sql);
	while($row = mysql_fetch_array( $result ))
	{
		$reqid = $row['id'];
		$linkname = str_replace("<br>"," ", $row['short']) ;
	}
	if($reqid != "")
	{
		$sql = "select * from lu_application_appreqs where application_id =".$_SESSION['appid']." and req_id=".$reqid;
		$result = mysql_query($sql) or die(mysql_error(). $sql);
		while($row = mysql_fetch_array( $result ))
		{
			$doAdd = false;
		}
			if($complete == "")
			{
				$complete = 0;
			}
		if($doAdd == true )
		{
			$sql = "";
        
			if($saveDate != true)
			{
				$sql = "insert into lu_application_appreqs(application_id, req_id, completed)values(".$_SESSION['appid'].", ".$reqid.", ".$complete.")";
			}else
			{
				$sql = "insert into lu_application_appreqs(application_id, req_id, last_modified, completed)values(".$_SESSION['appid'].", ".$reqid.", '".date("Y-m-d h:i:s")."', ".$complete.")";
			}
			$result = mysql_query($sql) or die(mysql_error(). $sql);
		}else
		{
			$sql = "";
			if($saveDate != true)
			{
				$sql = "update lu_application_appreqs set completed=".$complete." where application_id=".$_SESSION['appid']." and req_id=".$reqid;
			}else
			{
				$sql = "update lu_application_appreqs set last_modified ='".date("Y-m-d h:i:s")."', completed=".$complete." where application_id=".$_SESSION['appid']." and req_id=".$reqid;
			}
			$result = mysql_query($sql) or die(mysql_error(). $sql);

		}
		if( $bypassEmail != true 
            && ( !isset($_SESSION['aliasLogin']) || !$_SESSION['aliasLogin']) )
		{
			handlePageUpdate($linkname);
		}
        
	}//END REQID
}
//CALCULATE AMOUNT OWED
function totalCost($appid)
{
	$ret = 0.0;
	//GET USERS SELECTED PROGRAMS
	$sql = "SELECT programs.id, 
	degree.name as degreename,
	fieldsofstudy.name as fieldname, 
	choice, 
	lu_application_programs.id as itemid,
	programs.programprice,
	programs.baseprice
	FROM lu_application_programs 
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	where lu_application_programs.application_id = ".$appid." order by choice";
	$result = mysql_query($sql) or die(mysql_error() . $sql);
	
	$i = 0;
	while($row = mysql_fetch_array( $result )) 
	{
		
		if($i == 0)
		{
			$ret =$row['baseprice'];
		}else
		{
		 	$ret += $row['programprice'];
			//echo $row['programprice']."<br>";
		}
		$i++;
	}
	return $ret;
}


//CHECK DATE FORMAT
function check_Date($date)
{
	$ret = false;
	$arr = $arr = split("/", $date);
	if(count($arr) == 3)
	{
		$matched = ereg ("(0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[01])/(19|20)([0-9]{2})", $date);
		if($date != "" &&  $matched != false)
		{
			$ret = true;
		}
	}
	return $ret;
}
function check_Date2($date)
{
	$ret = false;
	$arr = $arr = split("/", $date);
	if(count($arr) == 2)
	{
		$matched = ereg ("(0[1-9]|1[012])/(19|20)([0-9]{2})", $date);
		if($date != "" &&  $matched != false)
		{
			$ret = true;
		}
	}
	return $ret;
}

function check_Date3($date)
{
    $ret = false;
    $arr = $arr = split("/", $date);
    if(count($arr) == 2)
    {
        $matched = ereg ("^(0[1-9]|1[012])/(19|20)([0-9]{2})$", $date);
        if($date != "" &&  $matched != false)
        {
            $ret = true;
        }
    }
    return $ret;
}

//CHECK EMAIL FUNCTION
function check_email_address($email) {
	// First, we check that there's one @ symbol, and that the lengths are right
	if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
		// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
		return false;
	}
	// Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for ($i = 0; $i < sizeof($local_array); $i++) {
	if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
	return false;
	}
	}
	if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
	$domain_array = explode(".", $email_array[1]);
	if (sizeof($domain_array) < 2) {
	return false; // Not enough parts to domain
	}
	for ($i = 0; $i < sizeof($domain_array); $i++) {
	if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
	return false;
	}
	}
	}
	return true;
}

function strToNum($val)
{
	$val = ereg_replace("[^[:digit:]]","", $val);
	return intval($val);
}

function formatMySQLdate($dt, $seperator_in = '/', $seperator_out = '-')
{
	//IN: MM/DD/YYYY 00:00:00
	//OUT: YYYY-MM-DD
	$time = "";
	$dt = explode(' ', $dt);
	if(count($dt)> 1){
		$time = " " . $dt[count($dt)-1] ;
	}
	$dt = $dt[0];
	
	$arr = explode($seperator_in, $dt);
	$arrOut = array();
	if(count($arr) > 2)
	{
		array_push($arrOut,$arr[2]);
		array_push($arrOut,$arr[0]);
		array_push($arrOut,$arr[1]);
	}
	return implode($seperator_out, $arrOut).$time;

}

function formatMySQLdate2($dt, $seperator_in = '/', $seperator_out = '-')
{
	//IN: MM/YYYY
	//OUT: YYYY-MM-DD
	$time = "";
	$dt = explode(' ', $dt);
	if(count($dt)> 1){
		$time = " " . $dt[count($dt)-1] ;
	}
	$dt = $dt[0];
	
	$arr = explode($seperator_in, $dt);
	$arrOut = array();
	if(count($arr) > 1)
	{
		array_push($arrOut,$arr[1]);
		array_push($arrOut,$arr[0]);
		array_push($arrOut,"01");
	}
	return implode($seperator_out, $arrOut).$time;

}

function formatUSdate($dt, $seperator_in = '-', $seperator_out = '/')
{
	//IN: YYYY-MM-DD
	//OUT: MM/DD/YYYY
	$time = "";
	$dt = explode(' ', $dt);
	if(count($dt)> 1){
		$time = " " . $dt[count($dt)-1] ;
	}
	$dt = $dt[0];

	$arr = explode($seperator_in, $dt);
	$arrOut = array();
	if(count($arr) > 2)
	{
		array_push($arrOut,$arr[1]);
		array_push($arrOut,$arr[2]);
		array_push($arrOut,$arr[0]);
	}else{
		for($i = 0; $i < count($arr); $i++)
		{
			array_push($arrOut,$arr[$i]);
		}
	}
	return implode($seperator_out, $arrOut).$time;

}

function formatUSdate2($dt, $seperator_in = '-', $seperator_out = '/')
{
	//IN: YYYY-MM-DD
	//OUT: MM/YYYY
	$time = "";
	$dt = explode(' ', $dt);
	if(count($dt)> 1){
		$time = " " . $dt[count($dt)-1] ;
	}
	$dt = $dt[0];

	$arr = explode($seperator_in, $dt);
	$arrOut = array();
	if(count($arr) > 2)
	{
		array_push($arrOut,$arr[1]);
		//array_push($arrOut,$arr[2]);
		array_push($arrOut,$arr[0]);
	}else{
		for($i = 0; $i < count($arr); $i++)
		{
			array_push($arrOut,$arr[$i]);
		}
	}
	return implode($seperator_out, $arrOut);

}

function formatUSdate3($dt, $seperator_in = '-', $seperator_out = '/')
{
	//IN: YYYY-MM-DD
	//OUT: MM/DD/YYYY
	$time = "";
	$dt = explode(' ', $dt);
	if(count($dt)> 1){
		$time = " " . $dt[count($dt)-1] ;
	}
	$dt = $dt[0];

	$arr = explode($seperator_in, $dt);
	$arrOut = array();
	if(count($arr) > 2)
	{
		array_push($arrOut,$arr[1]);
		array_push($arrOut,$arr[2]);
		array_push($arrOut,$arr[0]);
	}else{
		for($i = 0; $i < count($arr); $i++)
		{
			array_push($arrOut,$arr[$i]);
		}
	}
	return implode($seperator_out, $arrOut);

}

function flipdate($dt, $seperator_in = '/', $seperator_out = '/')
{
	return implode($seperator_out, array_reverse(explode($seperator_in, $dt)));
}

function arrayshift($array,$value,$direction = 'up')
{
    $temp = $array;

    /* search for the value */
    while ($array_value = current($array))
    {
        if ($array_value == $value)
        {
            $key = key($array);
            break;
        }
        next($array);
    }
    /* no key */
    if ( !isset($key) )
    {
        return false;
    }

    /* check if we are at the top or bottom of the array and want to do an invalid move
       ie: top element up, bottom element down
       - return the original array... */
    if ( $key == 0 && $direction == 'up' )
    {
        return $array;
    }
    if ( $key == (count($array)-1) && $direction == 'down' )
    {
        return $array;
    }

    /* reorder the elements */
    switch ($direction)
    {
        case 'up':
            $factor = -1;
            break;
        case 'down':
            $factor = 1;
            break;
    }

    $temp[$key+$factor] = $array[$key];
    $temp[$key] = $array[$key+$factor];

    /* return the ordered array */
    return $temp;
}

function marray_search($needle, $haystack)
{
	$ret = false;
	foreach ($haystack as $key => $row)
	{
		
		
		
		 foreach($row as $cell)
		 {
		 
			  if ($cell == $needle)
				   $ret = $key;
		 }
	}
	return $ret;
}

function showFileInfo($name, $size, $moddate, $link)
{
	if($link != null)
	{
	?>
	<table width="450" border="0" cellspacing="2" cellpadding="2" class="tblItem">
	  <tr>
		<td><strong>File Name</strong></td>
		<td><strong>File Size</strong></td>
		<td><strong>Last Modified</strong></td>
        <td></td>
	  </tr>
	  <tr>
		<td><a href="<?=$link?>" target="_blank"><em><u><?=$name?></u></em></a></td>
		<td><em><? printf("%.1f",$size/1024);?>Kb</em></td>
		<td><em><?=$moddate?></em></td>
        <td>
        <?php
        if ( ($_SESSION['domainid'] == 1 || $_SESSION['domainid'] == 3)
            && ($name == 'resume.pdf' || $name == 'statement.pdf') ) 
        {
            if (strstr($link, '?') !== FALSE) {
                
                // This is a link to fileDownload.php
                $linkArray = explode('?', $link);
                $linkParams = $linkArray[1];
                
            } else {
            
                // This is the standard file path.
                $linkArray = array_reverse(explode('/', $link));
                $fileName = $linkArray[0];
                $guid = $linkArray[2];
                $linkParams = 'file=' . $fileName . '&amp;guid=' . $guid;
            }
        ?>
            <a href="previewPdfToText.php?<?php echo $linkParams; ?>" target="_blank">Review search text</a>
        <?php
        }
        ?>
        </td>
	  </tr>
	</table>
	<strong>(Please review your uploaded file by clicking the link above)</strong>	
	<? 
	}
}

function getFilePathRaw($sectionid, $userdata, $userid, $umasterid, $appid, $id=-1)
{
    $filename = "";
    $extension = "";
    $sql = "select id, extension from datafileinfo where user_id=".$userid." and section=".$sectionid;
    $sql .= " AND userdata LIKE '" . $appid . "_%'";
    if($id > -1)
    {
        $sql .=" and id = ".$id;
    }
    $result = mysql_query($sql)
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);

    while($row = mysql_fetch_array( $result ))
    {
        $itemId = $row['id'];
        $extension = $row['extension'];
    }

    switch( $sectionid )
    {
        case 1:
            $filename = "transcript";
            break;
        case 2:
            $filename = "resume";
            break;
        case 3:
            $filename = "recommendation";
            break;
        case 4:
            $filename = "statement";
            break;
        case 5:
            $filename = "experience";
            break;
        case 6:
            $filename = "grescore";
            break;
        case 7:
            $filename = "toefliscore";
            break;
        case 8:
            $filename = "gresubjectscore";
            break;
        case 9:
            $filename = "toeflcscore";
            break;
        case 10:
            $filename = "toeflpscore";
            break;
        case 19:
            $filename = "ieltsscore";
            break;
        case 20:
            $filename = "gmatscore";
            break;
        case 21:
            $filename = "PaymentVoucher";
            break;
        case 22:
            $filename = "awardletter";
            break;
        case 23:
            $filename = "writingsample";
            break;
        case 24:
            $filename = "speakingsample";
            break;
        case 25:
            $filename = "langprofrecommendation";
            break;
        case 26:
            $filename = "publication";
            break;
        case 27:
            $filename = "financialsupport";
            break;
        case 28:
            $filename = "researchproposal";
            break;
        case 29:
            $filename = "biographicalessay";
            break;
        default:
            //DebugBreak();
    }//END SWITCH
 //   debugbreak();
    $path = $_SESSION['datafileroot'] . "/".getGuid($umasterid)."/".$filename."/".$filename."_".$appid."_".$userdata.".".$extension;
    //DO FINAL CHECK IF FILE EXISTS
    if(file_exists($path)) 
    {
        return $path;  
    } 
    else 
    {
        return null;
    }
}

function getFilePath($sectionid, $userdata, $userid, $umasterid, $appid, $id=-1)
{
    $filename = "";
	$extension = "";
	$sql = "select id, extension from datafileinfo where user_id=".$userid." and section=".$sectionid;
    if ($sectionid == 23) {  // allow for multiple writing samples
        $sql .= " AND userdata LIKE '" . $appid . "_" . $userdata ."%'";
    } else {
	    $sql .= " AND userdata LIKE '" . $appid . "_%'";
    }
	if($id > -1)
	{
		$sql .=" and id = ".$id;
	}
    
	$result = mysql_query($sql)
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	while($row = mysql_fetch_array( $result ))
	{
		$itemId = $row['id'];
		$extension = $row['extension'];
	}

	switch( $sectionid )
	{
		case 1:
			$filename = "transcript";
			break;
		case 2:
			$filename = "resume";
			break;
		case 3:
			$filename = "recommendation";
			break;
		case 4:
			$filename = "statement";
			break;
		case 5:
			$filename = "experience";
			break;
		case 6:
			$filename = "grescore";
			break;
		case 7:
			$filename = "toefliscore";
			break;
		case 8:
			$filename = "gresubjectscore";
			break;
		case 9:
			$filename = "toeflcscore";
			break;
		case 10:
			$filename = "toeflpscore";
			break;
        case 19:
            $filename = "ieltsscore";
            break;
        case 20:
            $filename = "gmatscore";
            break;
        case 21:
            $filename = "PaymentVoucher";
            break;
        case 22:
            $filename = "awardletter";
            break;
        case 23:
            $filename = "writingsample";
            break;
        case 24:
            $filename = "speakingsample";
            break;
        case 25:
            $filename = "langprofrecommendation";
        case 26:
            $filename = "publication";
            break;
        case 27:
            $filename = "financialsupport";
            break;
        case 28:
            $filename = "researchproposal";
            break;
        case 29:
            $filename = "biographicalessay";
            break;
    }//END SWITCH
    
	$path = $_SESSION['datafileroot'] . "/".getGuid($umasterid)."/".$filename."/".$filename."_".$appid."_".$userdata.".".$extension;
    
    //DO FINAL CHECK IF FILE EXISTS
	if(file_exists($path)) 
	{
   		//return $path;
        
        $file = $filename . "_" . $appid . "_" . $userdata . "." . $extension;
        $guid = getGuid($umasterid);
        $scriptPathArray = array_reverse(explode('/', $_SERVER['SCRIPT_NAME']));
        if ($scriptPathArray[1] == 'apply') {
            $scriptDirectory = '../apply/';    
        } else {
            $scriptDirectory = '../admin/';
        }
        return $scriptDirectory . 'fileDownload.php?file=' . urlencode($file) .'&amp;guid=' . $guid;    
	} 
    else 
	{
		return null;
	}
}

function handle_upload_file($sectionid, $userid, $usermasterid, $file, $uservar=NULL)
{
  
$err = "";
$filename = "";
$ext = "";
$ret = "";
$mime_type = "";

// moved to configuration files  ---   $magic_location = "/usr0/wwwsrv/htdocs/inc/magic";
$file_name = $file["tmp_name"];

global $magic_location;

$info = '';
$mime_type = '';
if ($file_name) 
{
    if ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) 
    {
        if (strnatcmp(phpversion(), "5.5.9-1ubuntu4" >= 0) || phpversion() == "5.3.10-1ubuntu3.25")  {
            $info = finfo_open(FILEINFO_MIME); 
        } else {
            $info = finfo_open(FILEINFO_MIME_TYPE, $magic_location);
        }    
    }  
    else 
    {
        $info = finfo_open(FILEINFO_MIME, $magic_location);    
    }
    
    $mime_type = finfo_file($info, $file_name);
    
}
 
$allowed_ext = "jpg, jpeg, gif, png, pdf, doc, rtf, txt, docx, zip, mp3, wav";
    
	if (
	(      /*   application/vnd.openxmlformats-officedocument.wordprocessingml.document   
                application/vnd.ms-word.document.macroEnabled.12     */
           strcmp( $mime_type, "image/gif") == 0  ||
           strcmp( $mime_type, "image/pjpeg")== 0 ||
           strcmp( $mime_type, "image/jpeg") == 0 ||
           strcmp( $mime_type, "application/pdf") == 0 ||
           strcmp( $mime_type, "application/msword")== 0 ||
           strcmp( $mime_type, "application/rtf") == 0 ||
           strstr( $mime_type, "text/plain") == 0 ||
		   strcmp( $mime_type, "application/octet-stream") == 0 ||
           ( (strcmp( $mime_type, "application/zip") == 0) 
            && ((strtolower($ext['extension']) == 'doc') 
                || (strtolower($ext['extension']) == 'docx')) )  ||
            (strcmp( $mime_type, "application/x-gzip") == 0) ||
            (strcmp( $mime_type, "application/zip") == 0) ||
            (strcmp( $mime_type, "application/x-zip") == 0) ||
            (strcmp( $mime_type, "audio/mpeg") == 0) ||
           (strcmp( $mime_type, "audio/wav") == 0) ||
           (strcmp( $mime_type, "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=binary") == 0) ||
           (strcmp( $mime_type, "audio/x-wav") == 0) ||
           (strcmp ( $mime_type, "audio/mpeg; charset=binary") == 0)
	)
	&& (($file["size"] > 0) && ($file["size"] < 7000000))//7MB
	)
	{

		if ($file["error"] > 0)
		{
			$ret .= "Error: " . $file["error"] . "<br />";
		}
		else
		{   
			$ext = pathinfo($file['name']);
			$ext = strtolower($ext['extension']);
			$allowed_paths = explode(", ", $allowed_ext);
			$err = "Invalid file extension";
			for($i = 0; $i < count($allowed_paths); $i++)
			{
				if ($allowed_paths[$i] == $ext)
				{
					$err = "";
				}
			}
			if($err != "")
			{
				$ret .= $err."<br>";
			}
			//data/year/guid/transscript/trans.xxx
            
			switch( $sectionid )
			{
				case 1:
                    $pdffiletest = strcmp($mime_type, "application/octet-stream") && $file['type'] == 'application/pdf';
					$filename = "transcript";
					if(
						strcmp($mime_type, "image/gif") != 0 
						&& strcmp($mime_type, "image/pjpeg") != 0
						&& strcmp($mime_type, "image/jpeg") != 0
						&& strcmp($mime_type, "application/pdf") != 0
                        && !$pdffiletest
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
					)
					{
						$ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
					}
					break;
				case 2:
				    $filename = "resume";
					if ((strcmp($mime_type, "application/pdf") != 0) && (strcmp($mime_type, "application/pdf; charset=binary") != 0))
					{
						$ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                        logAccess($mime_type. " " . $ret);
					}
				    break;
			    case 3:
				    $filename = "recommendation";
                    $testoctet =  strcmp($mime_type, "application/octet-stream");
                    if (
					    strcmp($mime_type, "application/pdf") != 0              // not pdf
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
					    && strcmp($mime_type, "application/msword") != 0        // not word
                        && strcmp($mime_type, "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=binary") != 0
                        && ( !( strcmp($mime_type, "application/octet-stream") >= 0
                            &&  strcmp($mime_type, "application/octet-stream; charset=binary") == 0
                            // && strcmp($mime_type, "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=binary") == 0     // not word (docx)
                            && ($ext == 'doc' || $ext == 'docx') ) 
                            )
                        && ( !( strcmp($mime_type, "application/octet-stream") >= 0
                            &&  strcmp($mime_type, "application/octet-stream; charset=binary") == 0
                            // && strcmp($mime_type, "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=binary") == 0     // not word (docx)
                            && $ext == 'pdf' ) 
                            )
					    && $testoctet != 0  // not word?
                        && ( !( strcmp($mime_type, "application/zip") >= 0
                            &&  strcmp($mime_type, "application/zip; charset=binary") == 0
                            // && strcmp($mime_type, "application/vnd.openxmlformats-officedocument.wordprocessingml.document; charset=binary") == 0     // not word (docx)
                            && ($ext == 'doc' || $ext == 'docx') ) 
                            )
                    )
					{
						$ret .= "Invalid file type for this section. Files must be in PDF or MS Word format.<br>";
                        logAccess($mime_type. " " . $ret);
					}
				    break;
			    case 4:
				    $filename = "statement";
					if ((strcmp($mime_type, "application/pdf") != 0) 
                    && (strcmp($mime_type, "application/pdf; charset=binary") != 0))
					{
						$ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                        logAccess($mime_type. " " . $ret);
					}
				    break;
			    case 5:
				    $filename = "experience";
					if (strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0) 
                        {
						$ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                        logAccess($mime_type. " " . $ret);
					}
				    break;
			    case 6:
				    $filename = "grescore";
				    if(
					    strcmp($mime_type, "image/gif") != 0 
					    && strcmp($mime_type, "image/pjpeg") != 0
					    && strcmp($mime_type, "image/jpeg") != 0
					    && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
					)
					{
						$ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                        logAccess($mime_type. " " . $ret);
					}
				    break;
			    case 7:
				    $filename = "toefliscore";
				    if(
					    strcmp($mime_type, "image/gif") != 0 
					    && strcmp($mime_type, "image/pjpeg") != 0
					    && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
					    && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") !=0
					)
					{
						$ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
					}
				    break;
			    case 8:
				    $filename = "gresubjectscore";
				    if(
					    strcmp($mime_type, "image/gif") != 0 
					    && strcmp($mime_type, "image/pjpeg") != 0
					    && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
					    && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                        
					)
					{
						$ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
					}
				    break;
			    case 9:
				    $filename = "toeflcscore";
				    if(
					    strcmp($mime_type, "image/gif") != 0 
					    && strcmp($mime_type, "image/pjpeg") != 0
					    && strcmp($mime_type, "image/jpeg") != 0
					    && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
					)
					{
						$ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
					}
				    break;
			    case 10:
				    $filename = "toeflpscore";
				    if(
					    strcmp($mime_type, "image/gif") != 0 
					    && strcmp($mime_type, "image/pjpeg") != 0
					    && strcmp($mime_type, "image/jpeg") != 0
					    && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
					)
					{
						$ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
					}
				    break;
                                
                // PLB added new cases from MCHII 03/12/09
                case 11:
                    $filename = "Multi-wayANOVACourse";
                    if(
                        strcmp($mime_type, "image/gif") != 0
                        && strcmp($mime_type, "image/pjpeg") != 0
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }
                    break;
                case 12:
                    $filename = "Multi-factorregressionCourse";
                    if(
                        strcmp($mime_type, "image/gif") != 0
                        && strcmp($mime_type, "image/pjpeg") != 0
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }
                    break;
                case 13:
                    $filename = "Single-wayANOVACourse";
                    if(
                        strcmp($mime_type, "image/gif") != 0
                        && strcmp($mime_type, "image/pjpeg") != 0
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }
                    break;
                case 14:
                    $filename = "Single-factorregressionCourse";
                    if(
                        strcmp($mime_type, "image/gif") != 0
                        && strcmp($mime_type, "image/pjpeg") != 0
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }
                    break;
                case 15:
                    $filename = "DesignCourse";
                    if(
                        strcmp($mime_type, "image/gif") != 0
                        && strcmp($mime_type, "image/pjpeg") != 0
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }
                    break;
                case 16:
                    $filename = "ProgrammingCourse";
                    if(
                        strcmp($mime_type, "image/gif") != 0
                        && strcmp($mime_type, "image/pjpeg") != 0
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }
                    break;
                case 17:
	                $filename = "CodeSample";
                    /*
                    if(
                        strcmp($mime_type, "text/plain") != 0
                        && strcmp($mime_type, "application/x-gzip") != 0
                        && strcmp($mime_type, "application/x-zip") != 0
                        && strcmp($mime_type, "application/zip") != 0
                        && strcmp($mime_type, "application/x-zip-compressed") != 0
                        && strcmp($mime_type, "application/x-compress") != 0
                        && strcmp($mime_type, "application/x-compressed") != 0
                        && strcmp($mime_type, "multipart/x-zip") != 0
                        && !(strcmp( $mime_type, "application/octet-stream") == 0 && $file['type'] ==  "application/x-zip-compressed")
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in either text or zip format.<br>";
                    }
                    */
                    break;
                case 18:
                    $filename = "ProgrammingTest";
                    if(
                        strstr( $mime_type, "text/plain") != 0
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in text format.<br>";
                    }
                    break;
			     case 19:
                    $filename = "ieltsscore";                
                    if(
                        strcmp($mime_type, "image/gif") != 0 
                        && strcmp($mime_type, "image/pjpeg") != 0 
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0                    
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                        )                    
                    {                        
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }                
                    break;  
                 case 20:
                    $filename = "gmatscore";                
                    if(
                        strcmp($mime_type, "image/gif") != 0 
                        && strcmp($mime_type, "image/pjpeg") != 0 
                        && strcmp($mime_type, "image/jpeg") != 0
                        && strcmp($mime_type, "image/jpeg; charset=binary") != 0                    
                        && strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                    )                    
                    {                        
                        $ret .= "Invalid file type for this section. Files must be in GIF, JPG or PDF format.<br>";
                    }                
                    break; 
                 case 21:
                    $filename = "PaymentVoucher";                
                    if( strcmp($mime_type, "application/pdf") != 0 
                         && strcmp($mime_type, "application/pdf; charset=binary") != 0)                    
                    {                        
                        $ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                    }                
                    break;
                 case 22:
                    $filename = "awardletter";                
                    if( strcmp($mime_type, "application/pdf") != 0 
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0)                    
                    {                        
                        $ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                    }                
                    break;
                 case 23:
                    $filename = "writingsample";                
                    if (
                        strcmp($mime_type, "application/pdf") != 0              // not pdf
                        && strcmp($mime_type, "application/msword") != 0        // not word
                        && strcmp($mime_type, "application/octet-stream") != 0  // not word?
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                        && ( !( strcmp($mime_type, "application/zip") == 0       // not word (docx)
                            && ($ext == 'doc' || $ext == 'docx') ) 
                            )
                    )                    
                    {                        
                        $ret .= "Invalid file type for this section. Files must be in PDF or MS Word format.<br>";
                    }                
                    break;
                 case 24:
                    $filename = "speakingsample";                
                    if (
                        strcmp($mime_type, "audio/mpeg") < 0           // not mp3
                        && strcmp($mime_type, "audio/wav") < 0         // not wav
                        && strcmp($mime_type, "audio/x-wav") < 0       // not wav
                    )                    
                    {                        
                        $ret .= "Invalid file type for this section. Files must be in MP3 or WAV format.<br>";
                    }                
                    break;
                 case 25:
                    $filename = "langprofrecommendation";
                    $testoctet =  strcmp($mime_type, "application/octet-stream");
                    if (
                        strcmp($mime_type, "application/pdf") != 0              // not pdf
                        && strcmp($mime_type, "application/msword") != 0        // not word
                        && $testoctet != 0  // not word?
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0
                        && ( !( strcmp($mime_type, "application/zip") == 0       // not word (docx)
                            && ($ext == 'doc' || $ext == 'docx') ) 
                            )
                    )
                    {
                        $ret .= "Invalid file type for this section. Files must be in PDF or MS Word format.<br>";
                        logAccess($ret);
                    }
                    break;
                 case 26:
                    $filename = "publication";
                    if (strcmp($mime_type, "application/pdf") != 0 
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0)
                    {
                        $ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                    }
                    break;
                 case 27:
                    $filename = "financialsupport";
                    if (strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0)
                    {
                        $ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                    }
                    break;
                 case 28:
                    $filename = "researchproposal";
                    if (strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0)
                    {
                        $ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                    }
                    break;
                 case 29:
                    $filename = "biographicalessay";
                    if (strcmp($mime_type, "application/pdf") != 0
                        && strcmp($mime_type, "application/pdf; charset=binary") != 0)
                    {
                        $ret .= "Invalid file type for this section. Files must be in PDF format.<br>";
                    }
                    break;
                default:
                    //DebugBreak();  						
			}//END SWITCH

			$path = $_SESSION['datafileroot'] . "/".getGuid($usermasterid)."/".$filename;
			$i = "";
			if(!is_dir($path))
			{
				$i = mk_dir($path."/".$filename);
			}                                                                                                
			if($i == "" && $ret == "")
			{
				if( $uservar != "" && $uservar != NULL )
				{
					$path = $path . "/" .$filename ."_". $uservar ."." . $ext;
				}else
				{
					$path = $path . "/" .$filename . "." . $ext;
				}
				move_uploaded_file($file["tmp_name"],	$path );
				$ret = 0;
				//update db
				$itemId = -1;
				$sql = "select id from datafileinfo  
                    where user_id=" . intval($userid) . " and section=" . intval($sectionid);
				if($uservar != NULL && $uservar != "")
				{
					$sql .= " and userdata='" . mysql_real_escape_string($uservar) . "'";
				}
				$result = mysql_query($sql)
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);

				while($row = mysql_fetch_array( $result ))
				{
					$itemId = $row['id'];
				}

				if($itemId == -1)
				{
					$sql = "insert into datafileinfo(type,extension, size, user_id, section, moddate, userdata)
                        values('" . mysql_real_escape_string(htmlspecialchars($file["type"])) . "','"
                        . mysql_real_escape_string(htmlspecialchars($ext)) . "',"
                        . intval($file["size"]) . "," . intval($userid) . "," . intval($sectionid) . ",'"
                        . date("Y-m-d h:i:s") . "','" . mysql_real_escape_string($uservar) . "')";
                    $result = mysql_query($sql) 
                        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
					$itemId = mysql_insert_id();

				}else
				{
					$sql = "update datafileinfo set
					type='" . mysql_real_escape_string(htmlspecialchars($file["type"])) ."',
					extension='" . mysql_real_escape_string(htmlspecialchars($ext)) . "',
					size=" . intval($file["size"]) . ",
					section=" . intval($sectionid) . ",
					moddate='" . date("Y-m-d h:i:s") . "',
					userdata='" . mysql_real_escape_string($uservar) . "'
					where id = " . intval($itemId);
					$result = mysql_query($sql) 
                        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
				}
				$ret = $itemId;

			} else {
				//$ret .= $i."<br>";
                $ret .= "Unable to upload file<br>";
                
                // Log upload errors.
                $errorLogMessage = $_SERVER['PHP_SELF'] . ': ' . $ret;
                $errorLogMessage .= ': ' . htmlspecialchars($file["name"]);
                $errorLogMessage .= ', ' . htmlspecialchars($file["type"]);
                $errorLogMessage .= ':: ' . htmlspecialchars($mime_type);
                
                if ($i != '') {
                    $errorLogMessage .= '; ' . $i;    
                }
                logError($errorLogMessage);
			}
		}
	}
	else
	{
		$ret .= $mime_type . "  Invalid file " . $file["type"]. " " . $file["size"]. "bytes ". $mime_type;
        logAccess($ret);
	}
	return $ret;
}//END UPLOAD FILE

function mk_dir($path, $rights = 0777)
{
	$ret = "";
	$folder_path = array(strstr($path, '.') ? dirname($path) : $path);

	while(!@is_dir(dirname(end($folder_path)))
	&& dirname(end($folder_path)) != '/'
	&& dirname(end($folder_path)) != '.'
	&& dirname(end($folder_path)) != '')
	{
		array_push($folder_path, dirname(end($folder_path)));
	}

	while($parent_folder_path = array_pop($folder_path))
	{
		if(!@mkdir($parent_folder_path, $rights))
		{
			$ret = "Can't create folder \"$parent_folder_path\".";
            //$errorLogMessage = $_SERVER['PHP_SELF'] . ': ' . $ret;
            //logError($errorLogMessage);
		}
	}
    
	return $ret;
}

function getGuid($userid)
{
	$ret = "";
	$result = mysql_query("SELECT guid FROM users where id=".$userid) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$ret = $row['guid'];
	}
	return $ret;
}

function makeGuid()
{
    $guid =  uniqid(md5(rand()), true);
    list($firstWord, $secondpart) = explode('.', $guid);
    $guid = $firstWord . $secondpart;
	return $guid;
}

function STEM_GeneratePassword()
{
	// Create the meta-password
	$sMetaPassword = "";

	global $CONFIG;
	$ahPasswordGenerator = $CONFIG['security']['password_generator'];
	foreach ($ahPasswordGenerator as $cToken => $ahPasswordSeed)
		$sMetaPassword .= str_repeat($cToken, rand($ahPasswordSeed['minimum'], $ahPasswordSeed['maximum']));

	$sMetaPassword = str_shuffle($sMetaPassword);

	// Create the real password
	$arBuffer = array();
	for ($i = 0; $i < strlen($sMetaPassword); $i ++)
		$arBuffer[] = $ahPasswordGenerator[(string)$sMetaPassword[$i]]['characters'][rand(0, strlen($ahPasswordGenerator[$sMetaPassword[$i]]['characters']) - 1)];

	return implode("", $arBuffer);
}

function parseEmailTemplate($fileName, $vars=array(array()) )
{
	//echo "file ".$fileName;
	$fh = fopen($fileName, 'r');
	$str = fread($fh, filesize($fileName));//read file here
	fclose($fh);
	if($str != "")
	{
		for($i = 0; $i < count($vars); $i++)
		{
			$str = str_replace("##".$vars[$i][0]."##",$vars[$i][1],$str);
		}
	}
	return $str;
}
function parseEmailTemplate2($str, $vars=array(array()) )
{
	if($str != "")
	{
		for($i = 0; $i < count($vars); $i++)
		{
			if ($vars[$i][0] == 'replyemail') {
                $newString = '<a href="mailto:' . $vars[$i][1];
                $newString .= '?subject=' . getMailSieveString() . ' Online Admissions Inquiry';
                $newString .= '">' . $vars[$i][1] . '</a>'; 
                $str = str_replace("##".$vars[$i][0]."##", $newString, $str);    
            } else {
                $str = str_replace("##".$vars[$i][0]."##", $vars[$i][1], $str);    
		}
	}
	}
    return $str;
}

function printHtmlMail($sender, $recipient, $subject, $content, $section, $domain = "")
{
    
    global $hostname;
    
        //SEND EMAIL
        $headers = "";
//echo "isset A_:".isset($_SESSION['A_domainname'])."<BR>";
//echo "isset ?_:".isset($_SESSION['domainname'])."<BR>";
        if(isset($_SESSION['A_domainname']) && $domain == "")
        {
            $domain = $_SESSION['A_domainname'];
//echo "A<br>";
        }
        else
        {
//echo "B<br>";
            if(isset($_SESSION['domainname']))
            {
                $domain = $_SESSION['domainname'];
//echo "C<br>";
            }
        }
        
//echo "A_domainname: ".$_SESSION['A_domainname']." domainname: ".$_SESSION['A_domainname']." domain: ".$domain."<BR>";
        

        $content = html_entity_decode($content);
        if(strcmp  ( $section , "recommend" )== 0 || strcmp  ( $section , "reminder" ) == 0   || strcmp  ( $section , "recreminder" ) == 0 || strcmp  ( $section , "rejection" ) == 0)
        {   
            switch ($hostname)
            {
                case "APPLY.STAT.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsstats@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsstats@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsstats@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                case "APPLYGRAD-INI.CS.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsini@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsini@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsini@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdiet@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdiet@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdiet@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                 case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdesgn@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdesgn@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdesgn@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                default:
                    if ($_SESSION['roleDepartmentId'] == 1) {
                        $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <csd-phd-admissions@cs.cmu.edu>\r\n");
                        $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <csd-phd-admissions@cs.cmu.edu>\r\n");
                        $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <csd-phd-admissions@cs.cmu.edu>\r\n");
                        $headers .= "Content-type: text/html; charset=us-ascii";
                    } else {
                        $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <applygrad@cs.cmu.edu>\r\n");
                        $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <applygrad@cs.cmu.edu>\r\n");
                        $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <applygrad@cs.cmu.edu>\r\n");
                        $headers .= "Content-type: text/html; charset=us-ascii";
                    }
             }
        } else
        {   
            switch ($hostname)
            {   
                case "APPLY.STAT.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsstats@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsstats@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsstats@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                case "APPLYGRAD-INI.CS.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsini@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsini@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsini@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdiet@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdiet@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdiet@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdesgn@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdesgn@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <scsdesgn@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
                    break;
                default:
                    $headers =      "From: Carnegie Mellon Admissions - ".$domain. htmlentities(" <applygrad@cs.cmu.edu>\r\n");
                    $headers .=       "Cc: Carnegie Mellon Admissions - ".$domain. htmlentities(" <applygrad@cs.cmu.edu>\r\n");
                    $headers .= "Reply-to: Carnegie Mellon Admissions - ".$domain. htmlentities(" <applygrad@cs.cmu.edu>\r\n");
                    $headers .= "Content-type: text/html; charset=us-ascii";
             }
        }
        return $headers."<br>".$recipient."<br>".$subject."<br>".$content."<br>";

}

function getMailSieveString() {
    $domain = "";
    if(isset($_SESSION['domainname']))
    {
        $domain = $_SESSION['domainname'];
    }
    else
    {
        if((isset($_SESSION['A_domainname'])) && ($domain == ""))
        {
            $domain = $_SESSION['A_domainname'];
        }
    }
    
    if ( isset($_SESSION['programs']) ) {
        $sessionProgramIds = array();
        foreach ($_SESSION['programs'] as $sessionProgram) {
            $sessionProgramIds[] = $sessionProgram[0];
        }
        $programIds = implode('-', $sessionProgramIds);
    } else {
        $programIds = '';    
    }
    
    if ( isset($_SESSION['appid']) && $_SESSION['appid'] > 0 ) {
        $applicationId = $_SESSION['appid'];    
    } else {
        $applicationId = '';    
    }
    
    $mailSieveString = ' [' . $domain . ':';
    $mailSieveString .= $programIds . ':';
    $mailSieveString .= $applicationId . ']';
    
    return $mailSieveString;
}


function sendHtmlMail($sender, $recipient, $subject, $content, $section, $domain = "", $mailSieveString = "")
{
        include '../inc/config.php';
        
        if(isset($_SESSION['domainname']))
        {
            $domain = $_SESSION['domainname'];
        }
        else
        {
            if((isset($_SESSION['A_domainname'])) && ($domain == ""))
            {
                $domain = $_SESSION['A_domainname'];
            }
        }
        
        /*
        if ( isset($_SESSION['programs']) ) {
            $sessionProgramIds = array();
            foreach ($_SESSION['programs'] as $sessionProgram) {
                $sessionProgramIds[] = $sessionProgram[0];
            }
            $programIds = implode('-', $sessionProgramIds);
        } else {
            $programIds = '';    
        }
        
        if ( isset($_SESSION['appid']) ) {
            $applicationId = $_SESSION['appid'];    
        } else {
            $applicationId = '';    
        }
        
        $mailFilterIds = ' [' . $domain . ':';
        $mailFilterIds .= $programIds . ':';
        $mailFilterIds .= $applicationId . ']';
        */
        
        if ($mailSieveString) {
            $mailFilterIds = $mailSieveString;    
        } else {
            $mailFilterIds = getMailSieveString();    
        }
        
        //SEND EMAIL
        $content = html_entity_decode($content);
        //$headers = "From: Carnegie Mellon Admissions <applyweb+".$domain."@cs.cmu.edu>\r\n";
        
        switch ($hostname)
        {
            case "APPLY.STAT.CMU.EDU":  
                $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsstats@cs.cmu.edu>\r\n";
                break;
            case "APPLYGRAD-INI.CS.CMU.EDU":  
                $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsini@cs.cmu.edu>\r\n";
                break;
            case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsdiet@cs.cmu.edu>\r\n";
                break;
            case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsdesgn@cs.cmu.edu>\r\n";
                break;
            default:
                if (isset($_SESSION['roleDepartmentId']) && $_SESSION['roleDepartmentId'] == 1) {
                        $headers =      "From: Carnegie Mellon Admissions " . $mailFilterIds . " <csd-phd-admissions@cs.cmu.edu>\r\n";
                    } else {
                        $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <applygrad@cs.cmu.edu>\r\n";
                    }
         }
        if ($dontsendemail !== TRUE) {
        switch ($hostname)
        {
            case "APPLY.STAT.CMU.EDU":  
                $headers .= "Cc: scsstats@cs.cmu.edu" . "\r\n";
                break;
            case "APPLYGRAD-INI.CS.CMU.EDU":  
                $headers .= "Cc: scsini@cs.cmu.edu" . "\r\n";
                break;
            case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                $headers .= "Cc: scsdiet@cs.cmu.edu" . "\r\n";
                break;
            case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                $headers .= "Cc: scsdesgn@cs.cmu.edu" . "\r\n";
                break;
            default:
                if (isset($_SESSION['roleDepartmentId']) && $_SESSION['roleDepartmentId'] == 1) {
                    $headers .= "Cc: csd-phd-admissions@cs.cmu.edu" . "\r\n";
                } else {
                    $headers .= "Cc: applygrad@cs.cmu.edu" . "\r\n";
                }
         }
                //$headers .= "Cc: applyweb+".$domain."@cs.cmu.edu" . "\r\n";
        }
        //$headers .= "Reply-to: Carnegie Mellon Admissions <applyweb+".$domain."@cs.cmu.edu>\r\n";
        switch ($hostname)
        {
            case "APPLY.STAT.CMU.EDU":  
                $headers .= "Reply-to: Carnegie Mellon Admissions " . $mailFilterIds . " <scsstats@cs.cmu.edu>\r\n";
                break;
            case "APPLYGRAD-INI.CS.CMU.EDU":  
                $headers .= "Reply-to: Carnegie Mellon Admissions " . $mailFilterIds . " <scsini@cs.cmu.edu>\r\n";
                break;
            case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                $headers .= "Reply-to: Carnegie Mellon Admissions " . $mailFilterIds . " <scsdiet@cs.cmu.edu>\r\n";
                break;
            case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                $headers .= "Reply-to: Carnegie Mellon Admissions " . $mailFilterIds . " <scsdesgn@cs.cmu.edu>\r\n";
                break;
            default:    
                if (isset($_SESSION['roleDepartmentId']) && $_SESSION['roleDepartmentId'] == 1) {
                    $headers .= "Reply-to: Carnegie Mellon Admissions " . $mailFilterIds . " <csd-phd-admissions@cs.cmu.edu>\r\n";
                } else {
                    $headers .= "Reply-to: Carnegie Mellon Admissions " . $mailFilterIds . " <applygrad@cs.cmu.edu>\r\n";
                }
         }
        
        $headers .= "Content-type: text/html; charset=us-ascii";
        
        $subject = $mailFilterIds . " " . $subject; 
        
        switch ($hostname)
        {
            case "APPLY.STAT.CMU.EDU":  
                $envelopeFrom = '-f scsstats@cs.cmu.edu';
                break;
            case "APPLYGRAD-INI.CS.CMU.EDU":  
                $envelopeFrom = '-f scsini@cs.cmu.edu';
                break;
            case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                $envelopeFrom = '-f scsdiet@cs.cmu.edu';
                break;
            case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                $envelopeFrom = '-f scsdesgn@cs.cmu.edu';
                break;
            default:
                if (isset($_SESSION['roleDepartmentId']) && $_SESSION['roleDepartmentId'] == 1) {
                    $envelopeFrom = '-f csd-phd-admissions@cs.cmu.edu';
                } else {
                    $envelopeFrom = '-f applygrad@cs.cmu.edu';
                }
         }   
 
        if ($dontsendemail === TRUE) 
        {
            if ($hostname == "APPLY.STAT.CMU.EDU")  
            {
                $developer = "dales+scsstats".$domain."@cs.cmu.edu";
            } 
            else 
            {
                if ( ($hostname == "BANSHEE.SRV.CS.CMU.EDU") 
                        ||  ($hostname == "APPLYWEB.CS.CMU.EDU") 
                        ||  ($hostname == "APPLYGRAD.CS.CMU.EDU"))
                { 
                    $developer = "dales+applygrad".$domain."@cs.cmu.edu";
                } 
                else 
                {
                    $developer = "dales+applygrad".$domain."@cs.cmu.edu";    
                }
            }
            mail($developer, $subject, $content, $headers, $envelopeFrom);
        }
        else 
        {
            mail($recipient, $subject, $content, $headers, $envelopeFrom);
        }
}

function handlePageUpdate($pagename)
{
    global $hostname;
    
	$submitted = 0;
	//GET SUBMITTED STATUS
	$sql = "select submitted from application where id=".$_SESSION['appid'];
	$result = mysql_query($sql)	or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{ 
		$submitted = $row['submitted'];
	}
	if($submitted == 1)
	{
		$str = "";
        //$sysemail = "applyweb+@cs.cmu.edu";
		//GET SYSTEM EMAIL
		$domainid = 1;
		if(isset($_SESSION['domainid']))
		{
			if($_SESSION['domainid'] > -1)
			{
				$domainid = $_SESSION['domainid'];
			}
		}
		/*
		$sql = "select sysemail from systemenv where domain_id=".$domainid;
		$result = mysql_query($sql)	or die(mysql_error());
		while($row = mysql_fetch_array( $result ))
		{ 
			$sysemail = $row['sysemail'];
		}
        */
        switch ($hostname)
        {
            case "APPLY.STAT.CMU.EDU":  
                $sysemail = "scsstats@cs.cmu.edu";
                break;
            case "APPLYGRAD-INI.CS.CMU.EDU":  
                $sysemail = "scsini@cs.cmu.edu";
                break;
            case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                $sysemail = "scsdiet@cs.cmu.edu";
                break;
            case "APPLYGRAD-DESIGN.CS.CMU.EDU":  
                $sysemail = "scsdesgn@cs.cmu.edu";
                break;
            default:
                $sysemail = "applygrad@cs.cmu.edu";
         }
        
		//GET TEMPLATE CONTENT
		$sql = "select content from content where name='Page Update Letter' and domain_id=".$domainid;
		$result = mysql_query($sql)	or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$str = $row["content"];
		}
		$vars = array(	
		array('date', date("Y-m-d")),
		array('firstname',$_SESSION['firstname']),
		array('lastname',$_SESSION['lastname']),
		array('email',$_SESSION['email']),
		array('page',$pagename),
		array('replyemail',$sysemail)
		);
		$str = parseEmailTemplate2($str, $vars );
		//SEND EMAIL
		sendHtmlMail($sysemail, $_SESSION['email'], "Online Application Edit Confirmation", $str, "update");//to user
		//sendHtmlMail($sysemail, $sysemail, "Online Application Edit Confirmation", $str);//to applyweb
	}
}//end handlepageupdate

function do_post_request($url, $data, $optional_headers = null)
{
$php_errormsg = "";
 $params = array('http' => array(
			  'method' => 'POST',
			  'content' => $data
		   ));
 if ($optional_headers !== null) {
	$params['http']['header'] = $optional_headers;
 }
 $ctx = stream_context_create($params);
 //$fp = @fopen($url, 'rb', false, $ctx);
 $fp = file_get_contents($url, false, $ctx);
 if (!$fp) {
	throw new Exception("Problem with $url, $php_errormsg");
 }
 $response = @stream_get_contents($fp);
 if ($response === false) {
	throw new Exception("Problem reading data from $url, $php_errormsg");
 }
 return $response;
}//end do_post_request

function trimall($str, $charlist = " \t\n\r\0\x0B")
{
  return str_replace(str_split($charlist), '', $str);
}

 function truncate($substring, $max = 25, $rep = '...')
 {
   if(strlen($substring) < 1){
       $string = $rep;
   }else{
       $string = $substring;
   }

   $leave = $max - strlen ($rep);

   if(strlen($string) > $max){
       return substr_replace($string, $rep, $leave);
   }else{
       return $string;
   }

}

function toChecked($var)
{
    if($var == 1)
    {
        return "checked";
    }else
    {
        return "";
    }
}

function getApplicantProgramIds($applicationId) {
    $query = "SELECT program_id FROM lu_application_programs
                WHERE application_id = " . $applicationId;
    $result = mysql_query($query);
    $programIds = array();
    while ($row = mysql_fetch_array($result)) {
        $programIds[] = $row['program_id'];    
    }   
    return $programIds;
}

function logError($errorLogMessage) {
      
    $applicationId = NULL;
    if (isset($_SESSION['appid'])) {
        $applicationId = intval($_SESSION['appid']);    
    }
    
    $usertypeId = NULL;
    $usersId = NULL;
    $luuId = NULL;
    if (isset($_SESSION['usertypeid'])) {
    
        $usertypeId = intval($_SESSION['usertypeid']);    

        if (isset($_SESSION['usermasterid'])) {
            $usersId = intval($_SESSION['usermasterid']);
        }
        
        if (isset($_SESSION['userid'])) {
            $luuId = intval($_SESSION['userid']);
        }
    } 
    elseif (isset($_SESSION['A_usertypeid'])) 
    {
        $usertypeId = intval($_SESSION['A_usertypeid']);
        
        if (isset($_SESSION['A_usermasterid'])) {
            $usersId = intval($_SESSION['A_usermasterid']);
        }
        
        if (isset($_SESSION['A_userid'])) {
            $luuId = intval($_SESSION['A_userid']);
        }
    }
    
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $client = $_SERVER['REMOTE_ADDR'];    
    } 
    
    if ($usertypeId  == 6)  {
        logAccess(@mysql_real_escape_string($errorLogMessage));
    }                                      
    
    $errorLogQuery = "INSERT INTO errorlog 
                (users_id, lu_users_usertypes_id, usertype_id, 
                application_id, client, message)
                VALUES (" 
                . $usersId . ","
                . $luuId . ","
                . $usertypeId . ","
                . $applicationId . ",'"
                . @mysql_real_escape_string($client) . "','"
                . @mysql_real_escape_string($errorLogMessage) . "'"
                .")";
    
    @mysql_query($errorLogQuery);   
}

function logAccess($accessLogMessage) {
      
    $applicationId = NULL;
    if (isset($_SESSION['appid'])) {
        $applicationId = intval($_SESSION['appid']);    
    }
    
    $usertypeId = NULL;
    $usersId = NULL;
    $luuId = NULL;
    if (isset($_SESSION['usertypeid'])) {
    
        $usertypeId = intval($_SESSION['usertypeid']);    

        if (isset($_SESSION['usermasterid'])) {
            $usersId = intval($_SESSION['usermasterid']);
        }
        
        if (isset($_SESSION['userid'])) {
            $luuId = intval($_SESSION['userid']);
        }
    } 
    elseif (isset($_SESSION['A_usertypeid'])) 
    {
        $usertypeId = intval($_SESSION['A_usertypeid']);
        
        if (isset($_SESSION['A_usermasterid'])) {
            $usersId = intval($_SESSION['A_usermasterid']);
        }
        
        if (isset($_SESSION['A_userid'])) {
            $luuId = intval($_SESSION['A_userid']);
        }
    }
    
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $client = $_SERVER['REMOTE_ADDR'];    
    }

    $accessLogQuery = "INSERT INTO accesslog 
                (users_id, lu_users_usertypes_id, usertype_id, 
                application_id, client, activity)
                VALUES (" 
                . $usersId . ","
                . $luuId . ","
                . $usertypeId . ","
                . $applicationId . ",'"
                . @mysql_real_escape_string($client) . "','"
                . @mysql_real_escape_string($accessLogMessage) . "'"
                .")";
    
    @mysql_query($accessLogQuery);   
}

function logReplyformAccess($accessLogMessage) {
      
    $applicationId = NULL;
    if (isset($_SESSION['appid'])) {
        $applicationId = intval($_SESSION['appid']);    
    }
    
    $usertypeId = NULL;
    $usersId = NULL;
    $luuId = NULL;
    if (isset($_SESSION['usertypeid'])) {
    
        $usertypeId = intval($_SESSION['usertypeid']);    

        if (isset($_SESSION['usermasterid'])) {
            $usersId = intval($_SESSION['usermasterid']);
        }
        
        if (isset($_SESSION['userid'])) {
            $luuId = intval($_SESSION['userid']);
        }
    } 
    elseif (isset($_SESSION['A_usertypeid'])) 
    {
        $usertypeId = intval($_SESSION['A_usertypeid']);
        
        if (isset($_SESSION['A_usermasterid'])) {
            $usersId = intval($_SESSION['A_usermasterid']);
        }
        
        if (isset($_SESSION['A_userid'])) {
            $luuId = intval($_SESSION['A_userid']);
        }
    }
    
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $client = $_SERVER['REMOTE_ADDR'];    
    }

    $accessLogQuery = "INSERT INTO accesslogreplyform 
                (users_id, lu_users_usertypes_id, usertype_id, 
                application_id, client, activity)
                VALUES (" 
                . $usersId . ","
                . $luuId . ","
                . $usertypeId . ","
                . $applicationId . ",'"
                . @mysql_real_escape_string($client) . "','"
                . @mysql_real_escape_string($accessLogMessage) . "'"
                .")";
    
    @mysql_query($accessLogQuery);   
}

function logAccessIni($accessLogMessage, $applicationId = NULL) 
{
    if (isset($_SESSION['appid'])) {
        $applicationId = intval($_SESSION['appid']);    
    }
    
    $usertypeId = NULL;
    $usersId = NULL;
    $luuId = NULL;
    if (isset($_SESSION['A_usertypeid'])) 
    {
        $usertypeId = intval($_SESSION['A_usertypeid']);
        
        if (isset($_SESSION['A_usermasterid'])) {
            $usersId = intval($_SESSION['A_usermasterid']);
        }
        
        if (isset($_SESSION['A_userid'])) {
            $luuId = intval($_SESSION['A_userid']);
        }
    }
    elseif (isset($_SESSION['usertypeid'])) {
    
        $usertypeId = intval($_SESSION['usertypeid']);    

        if (isset($_SESSION['usermasterid'])) {
            $usersId = intval($_SESSION['usermasterid']);
        }
        
        if (isset($_SESSION['userid'])) {
            $luuId = intval($_SESSION['userid']);
        }
    } 

    if (isset($_SERVER['REMOTE_ADDR'])) {
        $client = $_SERVER['REMOTE_ADDR'];    
    }

    $accessLogQuery = "INSERT INTO accesslog 
                (users_id, lu_users_usertypes_id, usertype_id, 
                application_id, client, activity)
                VALUES (" 
                . $usersId . ","
                . $luuId . ","
                . $usertypeId . ","
                . $applicationId . ",'"
                . @mysql_real_escape_string($client) . "','"
                . @mysql_real_escape_string($accessLogMessage) . "'"
                .")";
    
    @mysql_query($accessLogQuery);   
}

function diePretty($errorMessage, $errorCode = 1) {
    
    $errorResponses = array(
        1 => 'Error: unable to process request.' 
        );
    
    logError($errorMessage);
    
    if (array_key_exists($errorCode, $errorResponses)) {
        $response = $errorResponses[$errorCode];
    } else {
        $response = 'Unknown error.';
    } 
    $response .= '<br><a href="home.php">Home</a>';
    
    exit($response);    
}
?>