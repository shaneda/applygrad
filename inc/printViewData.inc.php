<?php
include_once '../inc/applicationFunctions.php';

$err = "";

// PLB enabled override of $uid 10/19/09
$uid = -1;
if ( isset($luUsersUsertypesId)) {
    $uid = $luUsersUsertypesId;
}

$appid = -1;
if ( isset($applicationId)) {
    $appid = $applicationId;
    $uidQuery = "SELECT user_id FROM application WHERE application.id = " . $applicationId;
    $uidResult = mysql_query($uidQuery);
    while ($row = mysql_fetch_array($uidResult)) {
        $uid = $row['user_id'];
    }
}

$umasterid = -1;    // of applicant?

$submittedDate = NULL;
$myPrograms = array();
$myPrograms2 = array();

$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
$ethnicities = array();
$titles = array(array('Mr.', 'Mr.'), array('Ms.','Ms.'));

//$mode = "edit";
$title = "";
$applicantTitle = ""; 
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$ethnicity = "";
$pass = "";
$passC = "";
$dob = "";
$visaStatus = -1;
$visaNameShort = '';
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$street4 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$telMobile = "";
$telWork = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$streetP4 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";
$telPMobile = "";
$telPWork = "";
$countryCit = "";
$curResident = "";
$usCitizenOrPermanentResident = NULL;
$nativeTongue = "";
$organization = "";
$qq_number = "";

$degrees = array();
$uniTypes = array();
$institutes = array();
$myInstitutes = array();
$scales = array();
$transUploaded = true;
$comp = false;

$arr = array(2, "Graduate");
array_push($uniTypes, $arr);
$arr = array(3, "Additional College/University");
array_push($uniTypes, $arr);

$sectioncomplete = false;
$grecomplete = false;
$grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflcscores = array();
$toeflpscores = array();
$greScoreRange1 = array();
$greScoreRange2 = array();
$grePercRange1 = array();

$yearsExperience = NULL;
$iniYearsExperience = NULL;
$employments = array();
$pubs = array();
$fellows = array();
$crossDeptProgs = array();
$crossDeptProgsOther = "";
$advisors = array();//FACULTY DESIGNATED AS ADVISORS
$advisor1 = "";
$advisor2 = "";
$advisor3 = "";
$advisor4 = "";
$expResearch = "";
$expInd = "";
$exFileId = "";
$exFileDate = "";
$exFileSize = "";
$exFileExt = "";
$cups = "No";
$pier = "No";
$wFellow = "No";
$port = "No";
$portLink = "";
$portfolioPassword = "";
$videoEssayLink = "";
$videoEssayCode = "";
$prog = "";
$design = "";
$stats = "";
$enrolled = 0;
$referral = "";
$honors = "";
$otherUni = "";
$tmpCrossDeptProgsId = "";
$permission= 0;
$sent_to_program = "";
$cur_enrolled = "";
$mastersReviewWaiver = 0;
$waiveToefl = 0;
$aco = 0;
$pal = 0;

$myRecommenders = array();
$buckley = false;
$showRecommenderRelationship = FALSE;

$myAreas = array();
$myAreasAll = array();

//GET ID OF APPLICATION
// PLB added test for GET request
if ( isset($_POST['userid']) )
{
    $uid = intval($_POST['userid']);
} 
elseif ( isset($_GET['userid']) ) 
{
    $uid = intval($_GET['userid']);   
} 

if(isset($_GET['id']))
{
    $uid = intval($_GET['id']);
}
//VIEW TYPE
if(isset($_POST['v']))
{
    $view = intval($_POST['v']);
}
if(isset($_GET['v']))
{
    $view = intval($_GET['v']);
}
//ROUND NUMBER
if(isset($_POST['r']))
{
    $round = intval($_POST['r']);
}
if(isset($_GET['r']))
{
    $round = intval($_GET['r']);
}
//REVIEWER ID
if(isset($_POST['rid']))
{
    $reviewerId = intval($_POST['rid']);
}
if(isset($_GET['rid']))
{
    $reviewerId = intval($_GET['rid']);
    $allowEdit = false;
}
//DEPARTMENT ID
if(isset($_POST['d']))
{
    $thisDept = intval($_POST['d']);
}
if(isset($_GET['d']))
{
    $thisDept = intval($_GET['d']);
}

// PLB added test for application_id 9/28/09
// $appid = -1 from above
if ( isset($_GET['applicationId']) ) {
    $appid = intval($_GET['applicationId']);
} elseif ( isset($_POST['applicationId']) ) {
    $appid = intval($_POST['applicationId']);
} elseif ( isset($_SESSION['appid']) ) {
    $appid = $_SESSION['appid'];
}

if($uid > -1)
{
    //GET USER INFO
    $sql = "SELECT
    users.id as usermasterid,
    users.email,
    users.title,
    users.firstname,
    users.middlename,
    users.lastname,
    users.initials,
    users_info.gender,
    users_info.dob,
    users_info.address_cur_street1,
    users_info.address_cur_street2,
    users_info.address_cur_street3,
    users_info.address_cur_street4,
    users_info.address_cur_city,
    cur_states.name as currentstate,
    users_info.address_cur_pcode,
    cur_countries.name as currentcountry,
    users_info.address_cur_tel,
    users_info.address_cur_tel_mobile,
    users_info.address_cur_tel_work,
    users_info.address_perm_street1,
    users_info.address_perm_street2,
    users_info.address_perm_street3,
    users_info.address_perm_street4,
    users_info.address_perm_city,
    perm_states.name as permstate,
    users_info.address_perm_pcode,
    perm_countries.name as permcountry,
    users_info.address_perm_tel,
    users_info.address_perm_tel_mobile,
    users_info.address_perm_tel_work,
    ethnicity.name as ethnicity,
    /* PLB added ipeds race 10/19/09 */
    GROUP_CONCAT(ipeds_race) AS race,
    users_info.visastatus,
    visatypes.short AS visa_name_short,
    users_info.company,
    users_info.homepage,
    cit_countries.name as citcountry,
    users_info.native_tongue,
    users_info.cur_pa_res,
    users_info.us_citizen_or_permanent_resident,
    application.id as appid,
    application.submitted_date,
    application.waive_toefl,
    users_info.qq_number
    
    FROM lu_users_usertypes
    inner join users on users.id = lu_users_usertypes.user_id
    left outer join users_info on users_info.user_id = lu_users_usertypes.id
    left outer join states as cur_states on cur_states.id = users_info.address_cur_state
    left outer join states as perm_states on perm_states.id = users_info.address_perm_state
    left outer join countries as cur_countries on cur_countries.id = users_info.address_cur_country
    left outer join countries as perm_countries on perm_countries.id = users_info.address_perm_country
    left outer join countries as cit_countries on cit_countries.id = users_info.cit_country
    LEFT OUTER JOIN visatypes ON visatypes.id = users_info.visastatus
    left outer join application on application.user_id = lu_users_usertypes.id
    left outer join ethnicity on ethnicity.id = users_info.ethnicity";
    
    // PLB added ipeds race 10/19/09
    $sql .= " LEFT OUTER JOIN applicant_ipeds_race ON lu_users_usertypes.id = applicant_ipeds_race.lu_users_usertypes_id
                LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id";
    
    //PLB added test for application_id 9/28/09
    if ($appid != -1) {
        $sql .= " WHERE application.id = " . $appid;   
    } else {
        $sql .= " where lu_users_usertypes.id=".$uid;    
    }
    
    $sql .= " GROUP BY application.id";
    
    $result = mysql_query($sql)
    or die(mysql_error());

    while($row = mysql_fetch_array( $result ))
    {
        $umasterid = $row['usermasterid'];
        $email = $row['email'];
        $title = $row['title'];
        $applicantTitle = $row['title'];
        $fName = $row['firstname'];
        $mName = $row['middlename'];
        $lName = $row['lastname'];
        $initials = $row['initials'];
        $gender = $row['gender'];
        $dob = formatUSdate($row['dob'],'-','/');
        $street1 = $row['address_cur_street1'];
        $street2 = $row['address_cur_street2'];
        $street3 = $row['address_cur_street3'];
        $street4 = $row['address_cur_street4'];
        $city = $row['address_cur_city'];
        $state = $row['currentstate'];
        $postal = $row['address_cur_pcode'];
        $country = $row['currentcountry'];
        $tel = $row['address_cur_tel'];
        $telMobile = $row['address_cur_tel_mobile'];
        $telWork = $row['address_cur_tel_work'];
        $streetP1 = $row['address_perm_street1'];
        $streetP2 = $row['address_perm_street2'];
        $streetP3 = $row['address_perm_street3'];
        $streetP4 = $row['address_perm_street4'];
        $cityP = $row['address_perm_city'];
        $stateP = $row['permstate'];
        $postalP = $row['address_perm_pcode'];
        $countryP = $row['permcountry'];
        // PLB added ipeds race 10/19/09
        $race = $row['race'];
        $ethnicity = $row['ethnicity'];
        $visaStatus = $row['visastatus'];
        $visaNameShort = $row['visa_name_short'];
        $organization = $row['company'];
        $homepage = $row['homepage'];
        $countryCit = $row['citcountry'];
        $nativeTongue = $row['native_tongue'];
        if($row['cur_pa_res'] == 1)
        {
            $curResident = "Yes";
        }
        
        if($row['us_citizen_or_permanent_resident'] === "0")
        {
            $usCitizenOrPermanentResident = "No";
        }
        if($row['us_citizen_or_permanent_resident'] === "1")
        {
            $usCitizenOrPermanentResident = "Yes";
        }
        
        $appid = $row['appid'];
        $submittedDate = $row['submitted_date'];
        $waiveToefl = $row['waive_toefl'];
        $qq_number = $row['qq_number'];
    }
    
    //GET USER SELECTED PROGRAMS
    $sql = "SELECT programs.id,
    degree.name as degreename,
    fieldsofstudy.name as fieldname,
    choice,
    lu_application_programs.id as itemid,
    programs.programprice,
    programs.baseprice,
    programs.linkword,
    domain.name as domainname,
    lu_application_programs.decision,
    lu_application_programs.admission_status,
    lu_application_programs.admit,
    lu_application_programs.admit_comments,
    group_concat(distinct lu_programs_departments.department_id) as department_id,
    lu_application_programs.faccontact,
    lu_application_programs.stucontact,
    lu_application_programs.scholarship_amt,
    lu_application_programs.scholarship_comments
    FROM lu_application_programs
    inner join programs on programs.id = lu_application_programs.program_id
    inner join degree on degree.id = programs.degree_id
    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
    left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
    left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
    left outer join domain on domain.id = lu_domain_department.domain_id
    where lu_application_programs.application_id = ".$appid." group by programs.id order by choice";

    $result = mysql_query($sql) or die(mysql_error() . $sql);

    while($row = mysql_fetch_array( $result ))
    {
        $dept = "";
        $baseprice = 0.0;
        $oracle = "";
        $depts = getDepartments($row[0]);
        $tmpDept = "";

        if(count($depts) > 1)
        {

            $dept = " - ";
            for($i = 0; $i < count($depts); $i++)
            {
                $dept .= $depts[$i][1];
                $oracle = $depts[$i][2];
                // PLB commented out $baseprice to fix error 10/1/09
                // baseprice is not selected in dept query
                // $baseprice = $depts[$i][3];
                if($i < count($depts)-1)
                {
                    $dept .= "/";
                }

            }
        }

        $arr = array();
        array_push($arr, $row[0]);
        array_push($arr, $row['degreename']);
        array_push($arr, $row['linkword']);
        array_push($arr, $row['fieldname'].$dept);
        array_push($arr, $row['choice']);
        array_push($arr, $row['itemid']);
        array_push($arr, $row['programprice']);
        array_push($arr, $oracle);
        array_push($arr, $row['baseprice']);
        array_push($arr, $row['domainname']);
        array_push($arr, $row['decision']);
        array_push($arr, $row['admission_status']);
        array_push($arr, $row['admit']);
        array_push($arr, $row['admit_comments']);
        array_push($arr, $row['department_id']); // $myPrograms[n][14]
        array_push($arr, $row['faccontact']);
        array_push($arr, $row['stucontact']);
        array_push($arr, $row['scholarship_amt']); // $myPrograms[n][17]
        array_push($arr, $row['scholarship_comments']);
        
        array_push($myPrograms, $arr);
        
        
        $arr = array();
        array_push($arr, $row[0]);
        array_push($arr, $row['degreename'] . " " . $row['linkword']. " ". $row['fieldname'].$dept);
        array_push($myPrograms2, $arr);
    }

}//END IF UID



function getDepartments($itemId)
{
    $ret = array();
    $sql = "SELECT
    department.id,
    department.name,
    department.oraclestring
    FROM lu_programs_departments
    inner join department on department.id = lu_programs_departments.department_id
    where lu_programs_departments.program_id = ". $itemId;

    $result = mysql_query($sql)
    or die(mysql_error());

    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row["id"]);
        array_push($arr, $row["name"]);
        array_push($arr, $row["oraclestring"]);
        array_push($ret, $arr);
    }
    return $ret;
}

/*
* Check for special cases 
*/
$isHistoryDepartment = FALSE;
$isDesignDepartment = FALSE;
$isDesignPhdDepartment = FALSE;
$isDesignDdesDepartment = FALSE;
$isEm2Department = FALSE;
$isIniDepartment = FALSE;
foreach($myPrograms as $myProgramSpecialCase)
{
    if (isHistoryDepartment($myProgramSpecialCase[14]))
    {
        $isHistoryDepartment = TRUE;    
    }
    
    if (isDesignDepartment($myProgramSpecialCase[14]))
    {
        $isDesignDepartment = TRUE;    
    }
    
    if (isDesignPhdDepartment($myProgramSpecialCase[14]))
    {
        $isDesignPhdDepartment = TRUE;    
    }
    
    if (isDesignDdesDepartment($myProgramSpecialCase[14]))
    {
        $isDesignDdesDepartment = TRUE;    
    }
    if (isEm2Department($myProgramSpecialCase[14]))
    {
        $isEm2Department = TRUE;    
    }
    if (isIniDepartment($myProgramSpecialCase[14]))
    {
        $isIniDepartment = TRUE;    
    }
    
}




//GET EDUCATION
$sql = "SELECT
    usersinst.id,institutes.name,college_name, date_entered,
    date_grad,date_left,degreesall.name as degree,
    major1,major2,major3,minor1,minor2,gpa,gpa_major,
    gpascales.name as gpa_scale,transscriptreceived,converted_gpa,
    datafile_id,educationtype,
    datafileinfo.moddate,
    datafileinfo.size,
    datafileinfo.extension
    FROM usersinst
    left outer join institutes on institutes.id = usersinst.institute_id
    left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
    left outer join gpascales on gpascales.id = usersinst.gpa_scale
    left outer join degreesall on degreesall.id = usersinst.degree
    where usersinst.application_id=".$appid. " order by usersinst.educationtype";
    $result = mysql_query($sql) or die(mysql_error());
    $comp = true;
    if (mysql_numrows($result) == 0) {
        $allTranscriptsReceived = FALSE;    
    } else {
        $allTranscriptsReceived = TRUE;    
    }
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['name']);
        if($row['name'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['date_entered'],'-','/')  );
        if($row['date_entered'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['date_grad'],'-','/') );
        if($row['date_grad'] == ""){$comp = false; }
        array_push($arr, formatUSdate2($row['date_left'],'-','/') );
        array_push($arr, $row['degree']);
        if($row['degree'] == ""){$comp = false; }
        array_push($arr, $row['major1']);
        array_push($arr, $row['major2']);
        array_push($arr, $row['major3']);
        array_push($arr, $row['minor1']);
        array_push($arr, $row['minor2']);
        array_push($arr, $row['gpa']);
        if($row['gpa'] == ""){$comp = false; }
        array_push($arr, $row['gpa_major']);
        if($row['gpa_major'] == ""){$comp = false; }
        array_push($arr, $row['gpa_scale']);
        if($row['gpa_scale'] == ""){$comp = false; }
        array_push($arr, $row['transscriptreceived']); // $myInstitutes[14]
        if (!$row['transscriptreceived']) {
            $allTranscriptsReceived = FALSE;
        }
        array_push($arr, $row['datafile_id']);
        if($row['datafile_id'] == ""){$comp = false; }
        array_push($arr, $row['educationtype']);
        array_push($arr, $row['moddate']);
        array_push($arr, $row['size']);
        array_push($arr, $row['extension']);
        array_push($arr, $row['college_name']);     // $myInstitutes[20]
        array_push($arr, $row['converted_gpa']);
        array_push($myInstitutes, $arr);
    }
//GET SCORES

//LOOK FOR GRE GENERAL SCORE
$sql = "select grescore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from grescore
left outer join datafileinfo on datafileinfo.id = grescore.datafile_id
where grescore.application_id = " .$appid;
$result = mysql_query($sql)    or die(mysql_error());
if (mysql_numrows($result) == 0) {
    $allScoresReceived = FALSE;    
} else {
    $allScoresReceived = TRUE;    
}
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['verbalscore']);
    array_push($arr, $row['verbalpercentile']);
    array_push($arr, $row['quantitativescore']);
    array_push($arr, $row['quantitativepercentile']);
    array_push($arr, $row['analyticalwritingscore']);
    array_push($arr, $row['analyticalwritingpercentile']);
    array_push($arr, $row['analyticalscore']);
    array_push($arr, $row['analyticalpercentile']);
    array_push($arr, $row['scorereceived']);  // $grescores[10]
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($grescores,$arr);

}
//LOOK FOR GRE SUBJECT SCORE
$sql = "select gresubjectscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from gresubjectscore
left outer join datafileinfo on datafileinfo.id = gresubjectscore.datafile_id
where 
(
    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
    OR (gresubjectscore.name != '' AND gresubjectscore.name IS NOT NULL)
    OR (score !=0 AND score IS NOT NULL)
    OR (percentile != 0 AND percentile IS NOT NULL)
)
AND gresubjectscore.application_id = " .$appid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['name']);
    array_push($arr, $row['score']);
    array_push($arr, $row['percentile']);
    array_push($arr, $row['scorereceived']);  // $gresubjectscores[5]
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($gresubjectscores,$arr);
}
//LOOK FOR TOEFL SCORE

//LOOK FOR TOEFL IBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where
(
    (toefl.testdate != '0000-00-00' AND toefl.testdate NOT LIKE '0000-00-01%' AND toefl.testdate IS NOT NULL)
    OR (toefl.section1 != 0 AND toefl.section1 IS NOT NULL)
    OR (toefl.section2 != 0 AND toefl.section2 IS NOT NULL)
    OR (toefl.section3 != 0 AND toefl.section3 IS NOT NULL)
    OR (toefl.essay != 0 AND toefl.essay IS NOT NULL)
    OR (toefl.total != 0 AND toefl.total IS NOT NULL)
) 
AND toefl.application_id = " .$appid . " and toefl.type = 'IBT'";
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    //echo $row['essay'];
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']); // $toefliscores[7]
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toefliscores,$arr);
}
//LOOK FOR TOEFL CBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where
(
    (toefl.testdate != '0000-00-00' AND toefl.testdate NOT LIKE '0000-00-01%' AND toefl.testdate IS NOT NULL)
    OR (toefl.section1 != 0 AND toefl.section1 IS NOT NULL)
    OR (toefl.section2 != 0 AND toefl.section2 IS NOT NULL)
    OR (toefl.section3 != 0 AND toefl.section3 IS NOT NULL)
    OR (toefl.essay != 0 AND toefl.essay IS NOT NULL)
    OR (toefl.total != 0 AND toefl.total IS NOT NULL)
) 
AND toefl.application_id = " .$appid . " and toefl.type = 'CBT'";
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']); // $toeflcscores[7] 
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflcscores,$arr);
}
//LOOK FOR TOEFL PBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where
(
    (toefl.testdate != '0000-00-00' AND toefl.testdate NOT LIKE '0000-00-01%' AND toefl.testdate IS NOT NULL)
    OR (toefl.section1 != 0 AND toefl.section1 IS NOT NULL)
    OR (toefl.section2 != 0 AND toefl.section2 IS NOT NULL)
    OR (toefl.section3 != 0 AND toefl.section3 IS NOT NULL)
    OR (toefl.essay != 0 AND toefl.essay IS NOT NULL)
    OR (toefl.total != 0 AND toefl.total IS NOT NULL)
) 
AND application_id = " .$appid . " and toefl.type = 'PBT'";
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']); // $toeflpscores[7] 
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflpscores,$arr);
}

/*
* IELTS Score(s)
*/
$ieltsQuery = "SELECT ieltsscore.*, datafileinfo.moddate, datafileinfo.size, datafileinfo.extension
                FROM ieltsscore
                LEFT OUTER JOIN datafileinfo on datafileinfo.id = ieltsscore.datafile_id
                WHERE
                (
                    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
                    OR (listeningscore != 0 AND listeningscore IS NOT NULL)
                    OR (readingscore != 0 AND readingscore IS NOT NULL)
                    OR (writingscore != 0 AND writingscore IS NOT NULL)
                    OR (speakingscore != 0 AND speakingscore IS NOT NULL)
                    OR (overallscore != 0 AND overallscore IS NOT NULL)
                ) 
                AND ieltsscore.application_id = " .$appid;
$ieltsResult = mysql_query($ieltsQuery)    or die(mysql_error());
$ieltsScores = array();
while($row = mysql_fetch_array( $ieltsResult )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['listeningscore']);
    array_push($arr, $row['readingscore']);
    array_push($arr, $row['writingscore']);
    array_push($arr, $row['speakingscore']);
    array_push($arr, $row['overallscore']);
    array_push($arr, $row['scorereceived']); // $ieltsScores[7]
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($ieltsScores,$arr);    
}

/*
* GMAT Score(s)
*/
$gmatQuery = "SELECT gmatscore.*, datafileinfo.moddate, datafileinfo.size, datafileinfo.extension
                FROM gmatscore
                LEFT OUTER JOIN datafileinfo on datafileinfo.id = gmatscore.datafile_id
                WHERE
                (
                    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
                    OR (verbalscore IS NOT NULL)
                    OR (verbalpercentile IS NOT NULL)
                    OR (quantitativescore IS NOT NULL)
                    OR (quantitativepercentile IS NOT NULL)
                    OR (totalscore IS NOT NULL)
                    OR (totalpercentile IS NOT NULL) 
                    OR (analyticalwritingscore IS NOT NULL) 
                    OR (analyticalwritingpercentile IS NOT NULL)
                ) 
                AND gmatscore.application_id = " .$appid; 
$gmatResult = mysql_query($gmatQuery)    or die(mysql_error());
$gmatScores = array();
while($row = mysql_fetch_array( $gmatResult ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['verbalscore']);
    array_push($arr, $row['verbalpercentile']);
    array_push($arr, $row['quantitativescore']);
    array_push($arr, $row['quantitativepercentile']);
    array_push($arr, $row['analyticalwritingscore']);
    array_push($arr, $row['analyticalwritingpercentile']);
    array_push($arr, $row['totalscore']);
    array_push($arr, $row['totalpercentile']);
    array_push($arr, $row['scorereceived']);  // $gmatScores[10] 
    if (!$row['scorereceived']) {
        $allScoresReceived = FALSE;
    }
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($gmatScores,$arr);
}

//SUPPLEMENTAL INFO
$sql = "select cups, pier, womenfellowship, 
        portfoliosubmitted, portfolio_link, portfolio_password, 
        area1, area2, area3,
        referral_to_program, other_inst, 
        cross_dept_progs, cross_dept_progs_other, 
        records_permission, masters_review_waiver,
        if(sent_to_program=1,'yes','no') as sent_to_program,
        if(cur_enrolled=1,'yes','no') as cur_enrolled,
        honors,
        video_essay.url AS video_essay_url, video_essay.access_code AS video_essay_access_code,
        aco,
        pal
        from application 
        LEFT OUTER JOIN video_essay ON application.id = video_essay.application_id
        LEFT OUTER JOIN aco_pal ON application.id = aco_pal.application_id
        where application.id=".$appid;
$result = mysql_query($sql) or die(mysql_error());
$tmpProgs = "";
while($row = mysql_fetch_array( $result ))
{
    if($row['cups'] == 1)
    {
        $cups = "Yes";
    }
    if($row['womenfellowship'] == 1)
    {
        $wFellow = "Yes";
    }
    if($row['pier'] == 1)
    {
        $pier = "Yes";
    }
    if($row['portfoliosubmitted'] == 1)
    {
        $port = "Yes";
    }
    $prog = $row['area1'];
    $design = $row['area2'];
    $stats = $row['area3'];
    $portLink = stripslashes($row['portfolio_link']);
    $portfolioPassword = stripslashes($row['portfolio_password']);
    $videoEssayLink = $row['video_essay_url'];
    $videoEssayCode = $row['video_essay_access_code'];
    $referral = stripslashes($row['referral_to_program']);
    $otherUni = stripslashes($row['other_inst']);
    $crossDeptProgs = $row['cross_dept_progs'];
    $crossDeptProgsOther = $row['cross_dept_progs_other'];
    $permission = $row['records_permission'];
    $mastersReviewWaiver = $row['masters_review_waiver'];
    $sent_to_program = $row['sent_to_program'];
    $cur_enrolled = $row['cur_enrolled'];
    $honors = $row['honors'];
    $aco = $row['aco'];
    $pal = $row['pal'];
}

$sql = "select
publication.id,
title,
author,
forum,
citation,
url,
status,
publication.type,
type_other,
datafile_id,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from publication 
LEFT OUTER JOIN datafileinfo ON datafileinfo.id = publication.datafile_id
where application_id=".$appid . " order by id";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, $row['title']);
    array_push($arr, $row['author']);
    array_push($arr, $row['forum']);
    array_push($arr, $row['citation']);
    array_push($arr, $row['url']);
    array_push($arr, $row['status']);
    array_push($arr, $row['type']);
    array_push($arr, $row['type_other']);
    array_push($arr, $row['datafile_id']);  // $arr[9]
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($pubs, $arr);
}
$sql = "select fellowships.id,
name,
amount,
status,
applied_date,
award_date,
duration,
datafile_id,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from fellowships 
LEFT OUTER JOIN datafileinfo ON datafileinfo.id = fellowships.datafile_id
where application_id=".$appid;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, $row['name']);
    array_push($arr, $row['amount']);
    array_push($arr, $row['status']);
    array_push($arr, formatUSdate2($row['applied_date'],'-','/'));
    array_push($arr, formatUSdate2($row['award_date'],'-','/'));
    array_push($arr, $row['duration']);
    array_push($arr, $row['datafile_id']); // 7
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($fellows, $arr);
}

// PLB edited query to add "content" field
function getRecommenders($appid)
{
    global $buckley;
    global $showRecommenderRelationship;
    $ret = array();
    $letter = "";
    
    //RETRIEVE USER INFORMATION
    $sql = "SELECT
    recommend.id,
    recommend.rec_user_id,
    recommend.datafile_id,
    recommend.submitted,
    recommend.recommendtype,
    recommend.content,
    users.title,
    users.firstname,
    users.lastname,
    users.email,
    users_info.address_perm_tel,
    lu_users_usertypes.id as uid,
    users_info.company,
    reminder_sent_count,
    datafileinfo.moddate,
    datafileinfo.size,
    datafileinfo.extension,
    recommendationtypes.name,
    recommender_info.relationship_to_applicant
    FROM recommend
    LEFT OUTER JOIN recommender_info ON recommend.id = recommender_info.recommend_id
    left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
    left outer join users on users.id = lu_users_usertypes.user_id
    left join users_info on users_info.user_id = lu_users_usertypes.id
    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
    left outer join recommendationtypes on recommendationtypes.id = recommend.recommendtype
    where recommend.application_id=".$appid. " order by recommend.id";
    $result = mysql_query($sql) or die(mysql_error().$sql);
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['rec_user_id']);
        array_push($arr, $row['firstname']);
        array_push($arr, $row['lastname']);
        array_push($arr, $row['title']);
        array_push($arr, $row['company']);
        array_push($arr, $row['email']);
        array_push($arr, $row['address_perm_tel']);
        array_push($arr, $row['recommendtype']);
        array_push($arr, $row['datafile_id']);
        array_push($arr, $row['moddate']);
        array_push($arr, $row['size']);
        array_push($arr, $row['extension']);
        array_push($arr,$row['reminder_sent_count']);
        array_push($arr,$row['name']);
        // PLB added content field to index 15
        array_push($arr,$row['content']);
        array_push($arr, 0);//ARRAY INFO
        array_push($arr, $row['relationship_to_applicant']);
        array_push($ret, $arr);
        
        if ($row['relationship_to_applicant'])
        {
            $showRecommenderRelationship = TRUE;
        }
    }
    
    $sql2 = "SELECT
    lang_prof_recommend.id,
    lang_prof_recommend.rec_user_id,
    lang_prof_recommend.datafile_id,
    lang_prof_recommend.submitted,
    NULL AS recommendtype,
    'Language Proficiency' AS name,
    lang_prof_recommend.content,
    users.title,
    users.firstname,
    users.lastname,
    users.email,
    users_info.address_perm_tel,
    lu_users_usertypes.id as uid,
    users_info.company,
    reminder_sent_count,
    datafileinfo.moddate,
    datafileinfo.size,
    datafileinfo.extension
    FROM lang_prof_recommend
    left outer join lu_users_usertypes on lu_users_usertypes.id = lang_prof_recommend.rec_user_id
    left outer join users on users.id = lu_users_usertypes.user_id
    left join users_info on users_info.user_id = lu_users_usertypes.id
    left outer join datafileinfo on datafileinfo.id = lang_prof_recommend.datafile_id
    where lang_prof_recommend.application_id=".$appid. " order by lang_prof_recommend.id";
    $result = mysql_query($sql2) or die(mysql_error().$sql2);
    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['rec_user_id']);
        array_push($arr, $row['firstname']);
        array_push($arr, $row['lastname']);
        array_push($arr, $row['title']);
        array_push($arr, $row['company']);
        array_push($arr, $row['email']);
        array_push($arr, $row['address_perm_tel']);
        array_push($arr, $row['recommendtype']);
        array_push($arr, $row['datafile_id']);
        array_push($arr, $row['moddate']);
        array_push($arr, $row['size']);
        array_push($arr, $row['extension']);
        array_push($arr,$row['reminder_sent_count']);
        array_push($arr,$row['name']);
        // PLB added content field to index 15
        array_push($arr,$row['content']);
        array_push($arr, 0);//ARRAY INFO
        array_push($ret, $arr);
    }
    
    $sql = "select buckleywaive from application where id=".$appid;
    $result = mysql_query($sql) or die(mysql_error().$sql);
    while($row = mysql_fetch_array( $result ))
    {
        $buckley = $row['buckleywaive'];
    }
    return $ret;
}
$myRecommenders = getRecommenders($appid);

function getMyAreas($id, $appid)
{
    global $myAreasAll;
    $ret = array();
    $sql = "SELECT
    interest.id,
    interest.name
    FROM
    lu_application_programs
    inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
    inner join interest on interest.id = lu_application_interest.interest_id
    where lu_application_programs.program_id=".$id." and lu_application_programs.application_id=".$appid . " order by lu_application_interest.choice" ;

    $result = mysql_query($sql)    or die(mysql_error());

    while($row = mysql_fetch_array( $result ))
    {
        $arr = array();
        array_push($arr, $row["id"]);
        array_push($arr, $row["name"]);
        array_push($ret, $arr);
        $doAdd = true;
        for($j = 0; $j < count($myAreasAll); $j++)
        {
            if($myAreasAll[$j][0] == $row["id"])
            {
                $doAdd = false;
            }
        }
        if($doAdd == true)
        {
            array_push($myAreasAll, $arr);
        }
    }
    return $ret;
}

//ADVISORS
$sql = "select
lu_application_advisor.program_id,
concat(users.firstname, ' ',users.lastname) as name2,
name
from
lu_application_advisor
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_application_advisor.advisor_user_id
left outer join users on users.id = lu_users_usertypes.user_id
where application_id=".$appid . " order by lu_application_advisor.id" ;
$result = mysql_query($sql) or die(mysql_error());
$myAdvisors = array();
$myAdvisors2 = array();
$k=0;
while($row = mysql_fetch_array( $result ))
{ 
    // PLB replaced logic below to fill/use myAdvisors array
    // Get up to 12 possible advisors
    if ($k < 12) 
    {        
        if($row['name2'] == "") 
        {
            $advisorName = $row['name'];
            $myAdvisors[$k] = $row['name'];
        } 
        else 
        {
            $advisorName = $row['name2']; 
            $myAdvisors[$k] = $row['name2'];
        }
        $myAdvisors2[] = array(
            'program_id' => $row['program_id'],
            'advisor_name' => $advisorName
        );        
    }

    $k++;
}

// Employment / Experience
$experienceRecords = getEmployments($appid);
$yearsExperience = NULL;
$employments = array();
foreach ($experienceRecords as $experienceRecord) {
    if ( !$experienceRecord[1] && $experienceRecord[4] ) {
        // This is an MS-HCII summary record.
        $yearsExperience = $experienceRecord[4];  
    } else {
        // This is an MSE-MSIT experience record.
        $employments[] = $experienceRecord;    
    }    
}


/*
* Dietrich language proficiency 
*/
$writingSamples = getWritingSamples();
$languageAssessments = getLanguageAssessments();
$speakingSample = getSpeakingSample();

function getWritingSamples()
{
    global $uid;
    global $umasterid;
    global $appid;
    
    $writingSamples = array();
    
    $query = "SELECT id, moddate, size, extension, userdata 
        FROM datafileinfo 
        WHERE user_id = " . $uid . " 
        AND section = 23
        AND userdata LIKE '" . $appid . "_%'";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result)) 
    {
        $userdataArray = explode('_', $row['userdata']);
        $writingSampleId = $userdataArray[1];
        $writingSamples[] = getFilePath(23, $writingSampleId, $uid, $umasterid, $appid);
    } 
    
    return $writingSamples;    
}

function getLanguageAssessments()
{
    global $appid;
    
    $languageAssessments = array();
    
    $query = "SELECT language,
        listening_rating.value AS listening_rating, 
        speaking_rating.value AS speaking_rating,
        reading_rating.value AS reading_rating,
        writing_rating.value AS writing_rating,
        native_speaker,
        years_study,
        language_assessment_study_level.value AS study_level,
        competency_evidence
        FROM language_assessment
        LEFT OUTER JOIN language_assessment_rating AS listening_rating
            ON language_assessment.listening = listening_rating.id
        LEFT OUTER JOIN language_assessment_rating AS speaking_rating
            ON language_assessment.speaking = speaking_rating.id
        LEFT OUTER JOIN language_assessment_rating AS reading_rating
            ON language_assessment.reading = reading_rating.id
        LEFT OUTER JOIN language_assessment_rating AS writing_rating
            ON language_assessment.writing = writing_rating.id
        LEFT OUTER JOIN  language_assessment_study_level
            ON language_assessment.study_level = language_assessment_study_level.id
        WHERE application_id = " . $appid . " ORDER BY language_assessment.id";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result)) 
    {
        $languageAssessments[] = $row;
    } 
    
    return $languageAssessments;  
}

function getSpeakingSample()
{
    global $uid;
    global $umasterid;
    global $appid;
    
    $speakingSample = NULL;
    
    $query = "SELECT id, moddate, size, extension, userdata 
    FROM datafileinfo 
    WHERE user_id = " . $uid . " 
    AND section = 24
    AND userdata LIKE '" . $appid . "_%'";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result)) 
    {
        $speakingSample = getFilePath(24, 1, $uid, $umasterid, $appid);
    } 
    
    return $speakingSample;
}

/*
* Dietrich misc. 
*/
$attendanceStatus = getAttendanceStatus();
$cmuAffiliation = getCmuAffiliation();
$disability = getDisability();
$dietrichFinancialSupport = getDietrichFinancialSupport();
$dietrichSharing = getDietrichSharing();
$dietrichRecognitions = getDietrichRecognitions();
$dietrichDiversity = getDietrichDiversity();
$dietrichLanguageRecommendation = getDietrichLanguageRecommendation();


function getAttendanceStatus()
{
    global $appid;
    
    $attendanceStatus = NULL;
    
    $query = "SELECT attendance_status.value AS status 
        FROM attendance
        INNER JOIN attendance_status 
            ON attendance.status = attendance_status.id
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $attendanceStatus = $row['status'];        
    }
    
    return $attendanceStatus;   
}

function getCmuAffiliation()
{
    global $appid;
    
    $cmuAffiliation = NULL;    
    
    $query = "SELECT cmu_student_or_alumnus, cmu_employee 
        FROM cmu_affiliation 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $cmuAffiliation['student_or_alumnus'] = $row['cmu_student_or_alumnus'];
        $cmuAffiliation['employee'] = $row['cmu_employee'];           
    } 
    
    return $cmuAffiliation;
}

function getDisability()
{
    global $appid;
    
    $disability = NULL;    
    
    $query = "SELECT has_disability, disability_type.value AS disability_type 
        FROM disability
        LEFT OUTER JOIN disability_type 
            ON disability.disability_type = disability_type.id 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        if ($row['has_disability'])
        {
            $disability = 'Yes';
            if($row['disability_type'])
            {
                $disability .= ': ' . $row['disability_type'];    
            }        
        }        
        else
        {
            $disability = 'No';    
        }
    } 
    
    return $disability;
}

function getDietrichFinancialSupport()
{
    global $appid;
    
    $dietrichFinancialSupport = NULL;    
    
    $query = "SELECT qualified_assistance, received_loans, 
        received_scholarships, support_sources, interested_b2_training 
        FROM dietrich_financial_support
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $dietrichFinancialSupport = $row;           
    } 
    
    return $dietrichFinancialSupport;
}

function getDietrichSharing()
{
    global $appid;
    
    $dietrichSharing = NULL;    
    
    $query = "SELECT sds, tepper FROM dietrich_sharing 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $dietrichSharing = $row;           
    } 
    
    return $dietrichSharing;
}

function getDietrichRecognitions()
{
    global $appid;
    
    $dietrichRecognitions = NULL;    
    
    $query = "SELECT description FROM dietrich_recognitions
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $dietrichRecognitions = $row['description'];           
    } 
    
    return $dietrichRecognitions;
}

function getDietrichDiversity()
{
    global $appid;
    
    $dietrichDiversity = NULL;    
    
    $query = "SELECT background, life_experience 
        FROM dietrich_diversity
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $dietrichDiversity = $row;           
    } 
    
    return $dietrichDiversity;
}

function getDietrichLanguageRecommendation()
{
    global $appid;
    
    $dietrichLanguageRecommendation = NULL;    
    
    $query = "SELECT
        lang_prof_recommend.id,
        lang_prof_recommend.rec_user_id,
        lang_prof_recommender_info.language_specialization,
        users.firstname,
        users.lastname,
        users.title,
        users_info.company,
        users.email,
        users_info.address_perm_tel,
        lang_prof_recommend.datafile_id,
        datafileinfo.moddate,
        datafileinfo.size,
        datafileinfo.extension
        FROM lang_prof_recommend
        INNER JOIN lang_prof_recommender_info 
            ON lang_prof_recommend.id = lang_prof_recommender_info.recommend_id
        INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = lang_prof_recommend.rec_user_id
        INNER JOIN users ON users.id = lu_users_usertypes.user_id
        INNER JOIN users_info ON users_info.user_id = lu_users_usertypes.id
        LEFT OUTER JOIN datafileinfo ON datafileinfo.id = lang_prof_recommend.datafile_id
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
 
    $result = mysql_query($query);
    if ($result) {
        while($row = mysql_fetch_array($result))
        {
            $dietrichLanguageRecommendation = $row;           
        } 
    }
    return $dietrichLanguageRecommendation;    
}

/*
* INI Misc 
*/
 
if ($isIniDepartment) {
    $iniCoursework = getIniCoursework();
    $iniKobeCoursework = getIniKobeCoursework();
    $iniFinancialSupport = getIniFinancialSupport();
    $iniFinancialSupportDocuments = getIniFinancialSupportDocuments();
    $iniDisciplinaryAction =  getIniDisciplinaryAction();
    $iniYearsExperience = getIniYearsExperience();
} else if ($isEm2Department) {
    $em2Coursework = getEm2Coursework(); 
    $em2FinancialSupport = getEm2FinancialSupport();
    $em2FinancialSupportDocuments = getEm2FinancialSupportDocuments();
    $em2DisciplinaryAction =  getEm2DisciplinaryAction();
    $em2YearsExperience = getEm2YearsExperience();
}
 

function getIniCoursework()
{
    global $appid;
    
    $iniCoursework = NULL;    
    
    $query = "SELECT * FROM ini_supporting_coursework 
            WHERE application_id = " . intval($appid) . " LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniCoursework = $row;           
    } 
    
    return $iniCoursework;
}

function getIniKobeCoursework()
{
    global $appid;
    
    $iniKobeCoursework = NULL;    
    
    $query = "SELECT * FROM ini_supporting_coursework_kobe 
            WHERE application_id = " . intval($appid) . " LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniKobeCoursework = $row['description'];           
    } 
    
    return $iniKobeCoursework;
}

function getIniFinancialSupport()
{
    global $appid;
    
    $iniFinancialSupport = NULL;    
    
    $query = "SELECT * FROM ini_financial_support 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniFinancialSupport = $row;           
    } 
    
    return $iniFinancialSupport;
}

function getIniFinancialSupportDocuments()
{
    global $uid;
    global $appid;
    
    $financialSupportDocumentsQuery = "SELECT id, moddate, size, extension, userdata 
    FROM datafileinfo 
    WHERE user_id = " . $uid . " 
    AND section = 27
    AND userdata LIKE '" . $appid . "_%'";
    $financialSupportDocumentsResult = mysql_query($financialSupportDocumentsQuery);

    $financialSupportDocuments = array();
    while($row = mysql_fetch_array($financialSupportDocumentsResult)) 
    {
        $userdataArray = split('_', $row['userdata']);
        $documentId = $userdataArray[1];
        $row['document_id'] = $documentId;
        $financialSupportDocuments[] = $row;    
    }
    
    return $financialSupportDocuments;
}

function getIniDisciplinaryAction()
{
    global $appid;
    
    $iniDisciplinaryAction = NULL;    
    
    $query = "SELECT * FROM ini_disciplinary_action 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniDisciplinaryAction = $row;           
    } 
    
    return $iniDisciplinaryAction;
}

function getIniYearsExperience()
{
    global $appid;
    
    $iniYearsExperience = NULL;    
    
    $query = "SELECT * FROM ini_years_experience 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $iniYearsExperience = $row;           
    } 
    
    return $iniYearsExperience;
}

function getEm2Coursework()
{
    global $appid;
    
    $em2Coursework = NULL;    
    
    $query = "SELECT * FROM em2_supporting_coursework 
            WHERE application_id = " . intval($appid) . " LIMIT 1";
    $result = mysql_query($query);
    
    while($row = mysql_fetch_array($result))
    {
        $em2Coursework = $row;           
    } 
    
    return $em2Coursework;
}

function getEm2FinancialSupport()
{
    global $appid;
    
    $em2FinancialSupport = NULL;    
    
    $query = "SELECT * FROM em2_financial_support 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $em2FinancialSupport = $row;           
    } 
    
    return $em2FinancialSupport;
}

function getEm2FinancialSupportDocuments()
{
    global $uid;
    global $appid;
    
    $financialSupportDocumentsQuery = "SELECT id, moddate, size, extension, userdata 
    FROM datafileinfo 
    WHERE user_id = " . $uid . " 
    AND section = 27
    AND userdata LIKE '" . $appid . "_%'";
    $financialSupportDocumentsResult = mysql_query($financialSupportDocumentsQuery);

    $financialSupportDocuments = array();
    while($row = mysql_fetch_array($financialSupportDocumentsResult)) 
    {
        $userdataArray = split('_', $row['userdata']);
        $documentId = $userdataArray[1];
        $row['document_id'] = $documentId;
        $financialSupportDocuments[] = $row;    
    }
    
    return $financialSupportDocuments;
}

function getEm2DisciplinaryAction()
{
    global $appid;
    
    $em2DisciplinaryAction = NULL;    
    
    $query = "SELECT * FROM em2_disciplinary_action 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    if ($result) {
        while($row = mysql_fetch_array($result))
        {
            $em2DisciplinaryAction = $row;           
        }
    } 
    
    return $em2DisciplinaryAction;
}

function getEm2YearsExperience()
{
    global $appid;
    
    $em2YearsExperience = NULL;    
    
    $query = "SELECT * FROM em2_years_experience 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    if ($result) {
        while($row = mysql_fetch_array($result))
        {
            $em2YearsExperience = $row;           
        } 
    }
    return $em2YearsExperience;
}

/*
* Design misc.
*/
$requestAssistantship = getRequestAssistantship();

function getRequestAssistantship()
{
    global $appid;
    
    $requestAssistantship = NULL;
    
    $query = "SELECT assistantship.requested 
        FROM assistantship
        WHERE application_id = " . intval($appid) . "
        LIMIT 1";
    $result = mysql_query($query);
    while($row = mysql_fetch_array($result))
    {
        $requestAssistantship = $row['requested'];        
    }
    
    return $requestAssistantship;   
}

/*
* MSE-MSIT misc.
*/
$codility = getCodility();

function getCodility()
{
    global $appid;
    
    $codility = array();
    
    $query = "SELECT * FROM mse_codility 
        WHERE application_id = " . intval($appid) . "
        LIMIT 1"; 
        
    $result = mysql_query($query);
    if ($result) {
        while ($row = mysql_fetch_array($result))
        {
            $codility = $row;    
        }
    }    
    return $codility;
}
?>