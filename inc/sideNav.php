<? 
include_once '../inc/db_connect.php'; 
include_once '../inc/specialCasesApply.inc.php';

if (!class_exists('Domain')) {
    include '../classes/class.Domain.php';
}

$validDomain = FALSE;
if ( isset($_SESSION['domainid']) ) {
    $validDomain = Domain::isValid($_SESSION['domainid']);    
}

/*
* Check for special cases; 
*/
$isIsreeDomain = isIsreeDomain($_SESSION['domainid']);
$isHistoryDomain = isHistoryDomain($_SESSION['domainid']);
$isEnglishDomain = isEnglishDomain($_SESSION['domainid']);
$isPhilosophyDomain = isPhilosophyDomain($_SESSION['domainid']);
$isModLangPhdDomain = isModernLanguagesDomain($_SESSION['domainid']);
$isModLangMaDomain = isModernLanguageMaDomain($_SESSION['domainid']);
$isDesignPhdDomain = isDesignPhdDomain($_SESSION['domainid']);

$showSubmit = true;
?>
<table width="740" border="0" cellpadding="0" cellspacing="0">
  <tr>
<?php
if ($validDomain) {

    if ( isset($_SESSION['usermasterid']) &&  $_SESSION['usermasterid'] > 0 ) {
    ?>
    <td>
    <?
    $class = "rollbutton1";
    if( strstr($_SERVER['SCRIPT_NAME'], "home.php" ) !== false )
    {
    $class = "rollbutton1active";
    } ?>
    <DIV class="<?=$class?>">
    <DIV style="text-align: center;"><A href="home.php" title="Home" onfocus="this.blur()">
    <?php 
    echo ($isIsreeDomain) ? 'Registration' : 'Application';
    ?>
    <br>Status</A>
    </DIV>
    </DIV>
    </td>
    <?php
     } else {
    $class = "rollbutton1";
    if( strstr($_SERVER['SCRIPT_NAME'], "index.php" ) !== false )
    {
    $class = "rollbutton1active";
    }
    ?>    
    <td>
    <DIV class="<?php echo $class; ?>">
    <DIV style="text-align: center;"><A href="index.php" title="Home" onfocus="this.blur()" target="_parent">Log In</A></DIV>
    </DIV>
    </td>   
    <?php    
    }
} 

if ( $validDomain && Domain::hasContent($_SESSION['domainid'], 'Instructions') ) {
?>
<td>
<?
$class = "rollbutton1";
if( strstr($_SERVER['SCRIPT_NAME'], "instructions.php" ) !== false )
{
$class = "rollbutton1active";
} ?>
<DIV class="<?=$class?>">
<DIV style="text-align: center;"><A href="instructions.php" title="Instructions" onfocus="this.blur()">Instructions</A></DIV>
</DIV>
</td>
<?php
}

if ( $validDomain && Domain::hasContent($_SESSION['domainid'], 'FAQ') ) {
?>
<td>
<? $class = "rollbutton1";
if( strstr($_SERVER['SCRIPT_NAME'], "faq.php" ) !== false )
{
$class = "rollbutton1active";
} ?>
<DIV class="<?=$class?>">
<DIV style="text-align: center;"><A href="faq.php" title="FAQ" onfocus="this.blur()">FAQ</A></DIV>
</DIV>
</td>
<?php
}

$Requirements = array();
//GET REQUIREMENTS
if ( isset($_SESSION['appid']) ) {
    
    /*
    $sql = "SELECT applicationreqs.id, applicationreqs.short, applicationreqs.linkname, application.name as appname, 
    lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified, lu_application_appreqs.completed 
    FROM `lu_application_programs`
    inner join programs on programs.id = lu_application_programs.`program_id`
    inner join degree on degree.id = programs.degree_id
    inner join lu_degrees_applicationreqs on lu_degrees_applicationreqs.degree_id = programs.degree_id
    inner join applicationreqs on applicationreqs.id = lu_degrees_applicationreqs.appreq_id
    inner join application on application.id = lu_application_programs.application_id
    left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
    and lu_application_appreqs.req_id = applicationreqs.id
    where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id order by applicationreqs.sortorder";
    */
    
    $sql = "SELECT applicationreqs.id, applicationreqs.short, applicationreqs.linkname, application.name as appname, 
    lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified, lu_application_appreqs.completed 
    FROM lu_application_programs
    inner join programs on programs.id = lu_application_programs.program_id
    inner join programs_applicationreqs on programs_applicationreqs.programs_id = programs.id
    inner join applicationreqs on applicationreqs.id = programs_applicationreqs.applicationreqs_id
    inner join application on application.id = lu_application_programs.application_id
    left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
    and lu_application_appreqs.req_id = applicationreqs.id
    where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id order by applicationreqs.sortorder";
    
    $result = mysql_query($sql) or die(mysql_error());

    while($row = mysql_fetch_array( $result )) 
    {
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, $row['short']);
    array_push($arr, $row['linkname']);	
    array_push($arr, $row['compreqid']);	
    array_push($arr, formatUSdate($row['last_modified']));	
    array_push($arr, $row['completed']);
    array_push($Requirements, $arr);
    }
}

if(count($Requirements) == 0)
{
	$showSubmit = false;
} else {
/* add seperator if listing all menu items */
if (!isHistoryDomain($domainid)) {
?>
   <td>
   <? $class = "rollbutton2";?>
   <DIV class="<?=$class?>">
   <div>&nbsp</div>
   </DIV>
   </td>

<?
  }
}

for($i = 0; $i < count($Requirements); $i++)
{
	if($Requirements[$i][5] == 0 || $Requirements[$i][5] =="" )
	{
		$showSubmit = false;
	}

	$class = "rollbutton1";
	if( strstr($_SERVER['SCRIPT_NAME'], $Requirements[$i][2] ) !== false )
	{
	$class = "rollbutton1active";
	}

    if ($Requirements[$i][1] == 'Programs' && $isIsreeDomain) 
    {
        $Requirements[$i][1] = 'Course Description';
    }
    
    if ($Requirements[$i][1] == 'Programs' && $isHistoryDomain) 
    {
        $Requirements[$i][1] = 'Program';
    }
    
    if ($Requirements[$i][2] == 'resume.php' && $isHistoryDomain) 
    {
        $Requirements[$i][1] = 'Statement<br>and C.V.';
    }
    
    if ($Requirements[$i][2] == 'proficiency.php' && ($isHistoryDomain || $isModLangPhdDomain)) 
    {
        $Requirements[$i][1] = 'Writing Samples and<br>Language Proficiency';
    }
    
    if ($Requirements[$i][2] == 'proficiency.php' && ($isModLangMaDomain)) 
    {
        $Requirements[$i][1] = 'Writing and Spoken<br />Language Samples';
    }
    
    if ($Requirements[$i][2] == 'proficiency.php' 
        && ($isEnglishDomain || $isPhilosophyDomain)) 
    {
        $Requirements[$i][1] = 'Writing Samples';
    }
    
        if ($Requirements[$i][2] == 'resume.php' && ($isDesignPhdDomain)) 
    {
        $Requirements[$i][1] = 'Experience';
    }
    
    if ($Requirements[$i][2] == 'portfolio.php' && ($isDesignPhdDomain)) 
    {
        $Requirements[$i][1] = 'Portfolio of<br />Expertise';
    }
?>
<td>
<DIV class="<?=$class?>">
<DIV style="text-align: center;"><A href="<?=$Requirements[$i][2]?>" title="<?=str_replace("<br>"," ", $Requirements[$i][1])?>" onfocus="this.blur()"><?=$Requirements[$i][1]?></A></DIV>
</DIV>
</td>
<?
}

?>
<?
/* if(isset($_SESSION['appid'])){ 

if($_SESSION['appid'] > -1 && $showSubmit == true){?>
<td>
<? $class = "rollbutton1";
if("/apply/submit.php" == $_SERVER['SCRIPT_NAME'])
{
$class = "rollbutton3active2";
} ?>
<DIV id=<?=$class?>>
<DIV><A href="submit.php" title="Submit" onfocus="this.blur()"><center>Submit</center></A> </DIV>
</DIV>
</td>
<? }} 
*/
?>
</tr></table>