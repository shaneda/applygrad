<?php
$semiblind_review = 0; 

$interviewTypes = array(
    1 => 'other',
    2 => 'phone',
    3 => 'skype'
); 
?>

<br/>
<table width="500" border="0"  cellspacing="0px" cellpadding="2px">
   <? if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Committee <br>
      Scores:</strong>
      <br>
      (1 is best, <br>5 is worst)
      </td>
    <td valign="top"><strong>
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" 
            && $committeeReviews[$i][30]==0 
            && $committeeReviews[$i][22] != 1 // Not a admin/coordinator score
            && $committeeReviews[$i][32]== $thisDept )//NOT A SUPPLEMENTAL OR FAC VOTE
        {
            ?><input name="revdept" type="hidden" value="<?=$committeeReviews[$i][32]?>"><?
            echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3];
            if ($committeeReviews[$i][5])
            {
                echo ", " . $committeeReviews[$i][5];    
            } 
            if(isPsychologyDepartment($thisDept) || $thisDept == 74 || $round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            
            echo ' - ';
            
            //1st score
            ?>
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][10]?>">
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][11]?>">
            <?
            if($thisDept == 3)
            {
                // PLB added program test 11/10/09.
                if ($showPhdInput) {
                    echo " PhD Score: ". $committeeReviews[$i][10];    
                }
                if ($showMsInput) {
                    echo " MS Score: ".$committeeReviews[$i][11];//show point 2     
                }    
            }
            elseif ( isMseMsitDepartment($thisDept) )
            {
                echo " Score: ". $committeeReviews[$i][10];
                if ($committeeReviews[$i][11]) {
                    echo ", MSIT Score: ". $committeeReviews[$i][11];    
                }
            } 
            elseif ( isPsychologyDepartment($thisDept) )
            {
                if ($committeeReviews[$i][23] == 3)
                {
                    echo " Score: ". $committeeReviews[$i][10];
                }
            }    
            else
            {
                echo " Score: ". $committeeReviews[$i][10];//show point 1
            }
            /*
            //2nd score
            if($thisDept == 3 && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][11];
                }
            }
            */
            //1st score
            if($thisDept == 3 || isPsychologyDepartment($thisDept))
            {
                /*
                But RI doesn't use certainty!!! 
                // PLB added program test 11/10/09.
                if ($showPhdInput) {
                    echo ", PhD Certainty: ". $committeeReviews[$i][28];    
                }
                if ($showMsInput) {
                    echo " MS Certainty: ".$committeeReviews[$i][28] ;//show certainty 2    
                }
                */
            }
            else
            {
                // PLB fixed - should be point1_certainty - 2/3/10
                //echo ", Certainty: ". $committeeReviews[$i][28];//show certainty 2 
                echo ", Certainty: ". $committeeReviews[$i][27];//show certainty 2
            }
            /*
            //2nd score
            
            if($thisDept == 3 && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][28];
                }
            }
            */
            
            if($committeeReviews[$i][13] == 1)
            {
                echo ", Round 2: Yes";
            }
            elseif ($committeeReviews[$i][13] == 0)
            {
                echo ", Round 2: No";
            }
            
            if($committeeReviews[$i][39] == 1)
            {
                echo ", Round 3: Yes";
            }
            elseif ($committeeReviews[$i][39] == 0)
            {
                echo ", Round 3: No";
            }
            
            echo "<br>";
        }
     } ?>
    </strong></td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" >    </td>
  </tr>

  <? if($semiblind_review != 1 && !isPsychologyDepartment($thisDept)) { ?>
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top">
    <? 
    for($i = 0; $i < count($committeeReviews); $i++)
    {
    
        if ( $committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" 
            && $committeeReviews[$i][32] == $thisDept )
        {
            echo '<strong>' . $committeeReviews[$i][2] . ' ' . $committeeReviews[$i][3];
            echo ' (round ' . $committeeReviews[$i][23] . ')</strong>'; 
            echo ' - Ranking: ' . $committeeReviews[$i][15];
            echo '; Comments: ' . $committeeReviews[$i][18] . '<br>';
        }
     } ?></tr>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
     <? } // end semiblind check for faculty reviews
     ?>
     
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Committee <br>Comments:</strong>      </td>
    <td valign="top">
    <? 
    for($i = 0; $i < count($committeeReviews); $i++){
        $reviewerReviewsSeminars = array();
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 
            && $committeeReviews[$i][32]== $thisDept && $committeeReviews[$i][22] != 1)
        {
            echo "<input name='txtReviewerId' type='hidden' value='".$committeeReviews[$i][4]."'><strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            echo "</strong><br>";
            if ($thisDept == 6 || $thisDept == 83) {
                // LTI fields
                if ($committeeReviews[$i][6]) {
                    echo '<i>Background:</i> ' . $committeeReviews[$i][6] . '<br>';
                }
                $reviewerReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
                if ( count($reviewerReviewsSeminars) > 0) {
                    echo '<i>Add Interest:</i> ';
                    $reviewerReviewsSeminarsFull = array();
                    foreach ($reviewerReviewsSeminars as $reviewerReviewsSeminar) {
                        $reviewsSeminarId = $reviewerReviewsSeminar[0];
                        foreach ($ltiSeminars as $ltiSeminar) {
                            if ($ltiSeminar[0] == $reviewsSeminarId) {
                                $reviewerReviewsSeminarsFull[] = $ltiSeminar[1];
                            }
                        }
                    } 
                    echo implode(', ', $reviewerReviewsSeminarsFull) . '<br>';     
                }
                if ($committeeReviews[$i][26]) {
                    echo '<i>Other Interests:</i> ' . $committeeReviews[$i][26] . '<br>';
                }
                if ($committeeReviews[$i][8]) {
                    echo '<i>Statement:</i> ' . $committeeReviews[$i][8] . '<br>';
                }
                if ($committeeReviews[$i][7]) {
                    echo '<i>Grades:</i> ' . $committeeReviews[$i][7] . '<br>';
                }
                if ($committeeReviews[$i][25]) {
                    echo '<i>Special Consideration:</i> ' . $committeeReviews[$i][25] . '<br>';
                }
                if ($committeeReviews[$i][9]) {
                    echo '<i>Other:</i> ' . $committeeReviews[$i][9] . '<br>';
                }
            } elseif ($thisDept == 25) {
                // ISR-SE fields
                if ($committeeReviews[$i][6]) {
                    echo '<i>Industry experience:</i> ' . $committeeReviews[$i][6] . '<br>';
                }
               if ($committeeReviews[$i][7]) {
                    echo '<i>Research involvement &amp; interest:</i> ' . $committeeReviews[$i][7] . '<br>';
                }
                if ($committeeReviews[$i][8]) {
                    echo '<i>Stmt of purpose:</i> ' . $committeeReviews[$i][8] . '<br>';
                }
                if ($committeeReviews[$i][18]) {
                    echo '<i>Recoms:</i> ' . $committeeReviews[$i][18] . '<br>';
                }
                $reviewerLuuId = $committeeReviews[$i][4];
                $reviewerRiskFactors = getRiskFactors($appid, $reviewerLuuId);
                $reviewerPosistiveFactors = getPositiveFactors($appid, $reviewerLuuId);
                if ( count($reviewerRiskFactors > 0 )) {
                    echo '<i>Risk factors:</i> ';
                    $isrRiskFactorsFull = array();
                    foreach ($reviewerRiskFactors as $riskFactorIndex) {
                        $isrRiskFactorsFull[] = $riskFactorKey[$riskFactorIndex];
                    } 
                    echo implode(', ', $isrRiskFactorsFull) . '<br>';
                }
                if ( count($reviewerPosistiveFactors > 0) || $committeeReviews[$i][33]) {
                    echo '<i>Positive factors:</i> ';
                    $reviewerPositiveFactorsFull = array();
                    foreach ($reviewerPosistiveFactors as $positiveFactorIndex) {
                        $reviewerPositiveFactorsFull[] = $positiveKey[$positiveFactorIndex];
                    } 
                    echo implode(', ', $reviewerPositiveFactorsFull); 
                    if ($committeeReviews[$i][33]) {
                        echo ', ' . $committeeReviews[$i][33];
                    }
                    echo '<br>';
                }
                if ($committeeReviews[$i][9]) {
                    echo '<i>Comments:</i> ' . $committeeReviews[$i][9] . '<br>';
                }
                if ($committeeReviews[$i][17]) {
                    echo '<i>Mentor candidates:</i> ' . $committeeReviews[$i][17] . '<br>';
                }
            } elseif ( isMseMsitDepartment($thisDept) ) {
                
                if ($committeeReviews[$i][9]) {
                    echo $committeeReviews[$i][9]."<br>";   
                }
                
                $reviewerLuuId = $committeeReviews[$i][4];
                $reviewerMseRiskFactors = 
                    $db_risk_factors->getRiskFactors($appid, $reviewerLuuId);

                if ( count($reviewerMseRiskFactors) > 0 ) {
                    echo '<i>Risk factors:</i> ';
                    $mseRiskFactorsFull = array();
                    foreach ($reviewerMseRiskFactors as $reviewerMseRiskFactor) {
                        if ($reviewerMseRiskFactor['language']) {
                            $mseRiskFactorsFull[] = 'Language';    
                        }
                        if ($reviewerMseRiskFactor['experience']) {
                            $mseRiskFactorsFull[] = 'Experience';    
                        }                        
                        if ($reviewerMseRiskFactor['academic']) {
                            $mseRiskFactorsFull[] = 'Academic';    
                        }
                        if ($reviewerMseRiskFactor['other']) {
                            $mseRiskFactorsFull[] = 'Other';    
                        }
                        if ($reviewerMseRiskFactor['other_text']) {
                            $mseRiskFactorsFull[] = $reviewerMseRiskFactor['other_text'];    
                        }
                    } 
                    echo implode(', ', $mseRiskFactorsFull) . '<br>';
                }
                
                $reviewerInterviewType = $committeeReviews[$i][35];
                if ($reviewerInterviewType) {
                    echo '<i>Interview ' . $committeeReviews[$i][34] . ', ';
                    if ($reviewerInterviewType == 1) {
                        echo $committeeReviews[$i][36];        
                    } else {
                        echo $interviewTypes[$reviewerInterviewType];
                    }    
                    echo ':</i> ' . $committeeReviews[$i][37] . '<br>';
                }
                
            } else {
                // Just show "comments" field.
                echo $committeeReviews[$i][9]."<br>";    
            }
        }
     } ?>    </td>
  </tr>
  
    <?php
    // Let CSD MS see CSD PhD Comments
    if ($thisDept == 74)
    {
        // Check whether this candidate has applied to any CSD PhD programs
        $phdQuery = "SELECT review.*, users.firstname, users.lastname 
            FROM review
            INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
            INNER JOIN users ON lu_users_usertypes.user_id = users.id
            WHERE review.application_id = " . $appid . " 
            AND review.department_id = 1";
        $phdResult = mysql_query($phdQuery);
        
        if ($phdResult)
        {        
    ?>
    <tr>
      <td colspan="2"><hr align="left"></td>
    </tr>
    <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>CSD PhD <br>Comments:</strong>      </td>
    <td valign="top">
    <?
        while($row = mysql_fetch_array($phdResult))
        {
            echo '<strong>' . $row['firstname'] . ' ' . $row['lastname'] . '</strong><br>';
            echo $row['comments'] . '<br>';
        }
     ?>    
     </td>
  </tr>
  <? 
        } // end if $phdResult
    } // end if CSD MS
   
   }//end semiblind reviews
  
  if (isset($showSemiblindFull) && $showSemiblindFull) { 
  ?>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr> 
  <tr>
    <td bgcolor="#DDDDDD" valign="top" width="50px"><strong>Personal <br> Comments:</strong></td>
    <td valign="top"><?php echo $private_comments; ?></td>
  </tr>  
  <?php
  } // end semiblind check for faculty reviews 
  
  // PLB added coordinator notes 1/8/10
  if ($semiblind_review != 1) { ?>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
  <tr>  
    <td bgcolor="#DDDDDD" ><strong> Coordinator Notes: </strong> </td>
    <td>
    <?php

    $coordinatorComments = getCoordinatorComments($appid, $thisDept);
    foreach ($coordinatorComments as $coordinatorComment) {
        echo $coordinatorComment . '<br>';    
    }
    
  // Decision comments for admins and committee members.
  if( $_SESSION['A_usertypeid'] == 0
    ||  $_SESSION['A_usertypeid'] == 1
    ||  $_SESSION['A_usertypeid'] == 2
    ||  $_SESSION['A_usertypeid'] == 10
    ) 
    { 
        echo '<i>Final comments:</i> ';
        for($x = 0; $x < count($myPrograms); $x++)
        {
            $aDepts = split(",", $myPrograms[$x][14]);
            for($j = 0; $j < count($aDepts); $j++)
            {
                if($aDepts[$j] == $thisDept)
                {
                    echo $myPrograms[$x][13];
                }
            }
        }
    }   
    ?>    
    </td>
  </tr>
<?php
} // end semiblind check for faculty reviews ?>
          
</table>