<?php
// Initialize variables
$em2AssistantshipRequest = NULL; 
$em2FinancialSupportRequest = NULL;
$em2FinancialSupportAttendWithout = NULL;
$em2FinancialSupportReceiveOutsideType = '';
$em2FinancialSupportReceiveOutsideSource = '';
$em2FinancialSupportApplyOutsideType = '';
$em2FinancialSupportApplyOutsideSource = '';
$em2FinancialSupportFamilyType = '';
$em2FinancialSupportFamilyAmount = '';
$em2FinancialSupportOtherSource = '';
$em2FinancialSupportError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveEm2FinancialSupport();
    checkRequirementsEm2FinancialSupport();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$em2FinancialSupportError)
{
    $em2FinancialQuery = "SELECT * FROM em2_financial_support 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $em2FinancialResult = mysql_query($em2FinancialQuery);
    while($row = mysql_fetch_array($em2FinancialResult))
    {   
        $em2AssistantshipRequest = $row['request_assistantship'];
        $em2FinancialSupportRequest = $row['request_consideration'];
        $em2FinancialSupportAttendWithout = $row['attend_without_support'];
        $em2FinancialSupportReceiveOutsideType = $row['receive_outside_support_type'];
        $em2FinancialSupportReceiveOutsideSource = $row['receive_outside_support_source'];
        $em2FinancialSupportApplyOutsideType = $row['apply_outside_support_type'];
        $em2FinancialSupportApplyOutsideSource = $row['apply_outside_support_source'];
        $em2FinancialSupportFamilyType = $row['family_support_type'];
        $em2FinancialSupportFamilyAmount = $row['family_support_amount']; 
        $em2FinancialSupportOtherSource = $row['other_support_source'];        
    }    
}  
?>

<span class="subtitle">Financial Support</span>
<?php
if ($em2FinancialSupportError)
{
?>
    <br>
    <span class="errorSubtitle"><?php echo $em2FinancialSupportError; ?></span>
<?php  
}
?>
<br/>
<br/>
Do you wish to be considered for a fellowship from Carnegie Mellon?
<br/>
<br/>
<?php
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);
showEditText($em2FinancialSupportRequest, "radiogrouphoriz", "em2FinancialSupportRequest", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
Do you wish to be considered for a TAship from Carnegie Mellon? 
<br/>
<br/>
<?php
showEditText($em2AssistantshipRequest, "radiogrouphoriz", "em2AssistantshipRequest", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
Would you attend Carnegie Mellon without financial aid from the university?
<br/>
<br/>
<?php
showEditText($em2FinancialSupportAttendWithout, "radiogrouphoriz", "em2FinancialSupportAttendWithout", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>

<br/>
<br/>
If you will be receiving financial support from an outside agency, please specifiy:
<br/>
<br/>
<span style="display: inline-block; width: 50px;">Type</span>
<?php
$em2FinancialSupportTypes = array(
    array('Employer', 'Employer'),
    array('Government Agency', 'Government Agency'),
    array('Private Foundation', 'Private Foundation')
);
showEditText($em2FinancialSupportReceiveOutsideType, "listbox", "em2FinancialSupportReceiveOutsideType", $_SESSION['allow_edit'], FALSE, $em2FinancialSupportTypes); 
?>
<br/>
<span style="display: inline-block; width: 50px;">Source</span>
<?php
showEditText($em2FinancialSupportReceiveOutsideSource, "textbox", "em2FinancialSupportReceiveOutsideSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
If you will be applying for financial support from an outside agency, please specifiy:
<br/>
<br/>
<span style="display: inline-block; width: 50px;">Type</span>
<?php
showEditText($em2FinancialSupportApplyOutsideType, "listbox", "em2FinancialSupportApplyOutsideType", $_SESSION['allow_edit'], FALSE, $em2FinancialSupportTypes);
?>
<br/>
<span style="display: inline-block; width: 50px;">Source</span>
<?php
showEditText($em2FinancialSupportApplyOutsideSource, "textbox", "em2FinancialSupportApplyOutsideSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
If you will be receiving financial support from family or friends, please specifiy:
<br/>
<br/>
<span style="display: inline-block; width: 50px;">Type</span>
<?php
$em2FamilySupportTypes = array(
    array('Parents', 'Parents'),
    array('Other Relatives', 'Other Relatives'),
    array('Friends', 'Friends')
);
showEditText($em2FinancialSupportFamilyType, "listbox", "em2FinancialSupportFamilyType", $_SESSION['allow_edit'], FALSE, $em2FamilySupportTypes); 
?>
<br/>
<span style="display: inline-block; width: 50px;">Amount</span>
<?php
showEditText($em2FinancialSupportFamilyAmount, "textbox", "em2FinancialSupportFamilyAmount", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<br/>
<br/>
If your source of funding will be different from above, please specify:
<br/>
<?php
showEditText($em2FinancialSupportOtherSource, "textbox", "em2FinancialSupportInternationalSource", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 50);
?>

<hr size="1" noshade color="#990000">

<?php
function saveEm2FinancialSupport()
{
    global $em2FinancialSupportRequest;
    global $em2AssistantshipRequest;
    global $em2FinancialSupportAttendWithout;
    global $em2FinancialSupportReceiveOutsideType;
    global $em2FinancialSupportReceiveOutsideSource;
    global $em2FinancialSupportApplyOutsideType;
    global $em2FinancialSupportApplyOutsideSource;
    global $em2FinancialSupportFamilyType;
    global $em2FinancialSupportFamilyAmount;
    global $em2FinancialSupportOtherSource;
    global $em2FinancialSupportError;  
    
    $em2FinancialSupportRequest = filter_input(INPUT_POST, 'em2FinancialSupportRequest', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $em2AssistantshipRequest = filter_input(INPUT_POST, 'em2AssistantshipRequest', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $em2FinancialSupportAttendWithout = filter_input(INPUT_POST, 'em2FinancialSupportAttendWithout', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $em2FinancialSupportReceiveOutsideType = filter_input(INPUT_POST, 'em2FinancialSupportReceiveOutsideType', FILTER_SANITIZE_STRING);
    $em2FinancialSupportReceiveOutsideSource = filter_input(INPUT_POST, 'em2FinancialSupportReceiveOutsideSource', FILTER_SANITIZE_STRING);
    $em2FinancialSupportApplyOutsideType = filter_input(INPUT_POST, 'em2FinancialSupportApplyOutsideType', FILTER_SANITIZE_STRING);
    $em2FinancialSupportApplyOutsideSource = filter_input(INPUT_POST, 'em2FinancialSupportApplyOutsideSource', FILTER_SANITIZE_STRING);
    $em2FinancialSupportFamilyType = filter_input(INPUT_POST, 'em2FinancialSupportFamilyType', FILTER_SANITIZE_STRING);
    $em2FinancialSupportFamilyAmount = filter_input(INPUT_POST, 'em2FinancialSupportFamilyAmount', FILTER_SANITIZE_STRING);
    $em2FinancialSupportOtherSource = filter_input(INPUT_POST, 'em2FinancialSupportInternationalSource', FILTER_SANITIZE_STRING);
    
    if ($em2FinancialSupportRequest === NULL)
    {
        $em2FinancialSupportError .= 'Financial support consideration is required<br>';    
    }
    
    if ($em2AssistantshipRequest === NULL)
    {
        $em2FinancialSupportError .= 'TAship consideration is required<br>';    
    }
     
    if ($em2FinancialSupportAttendWithout === NULL)
    {
        $em2FinancialSupportError .= 'Attend without financial support is required<br>';    
    }
        
    if (!$em2FinancialSupportError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM em2_financial_support WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE em2_financial_support SET
                request_consideration = " . intval($em2FinancialSupportRequest) . ",
                request_assistantship = " . intval($em2AssistantshipRequest) . ",
                attend_without_support = " . intval($em2FinancialSupportAttendWithout) . ",
                receive_outside_support_type = '" . mysql_real_escape_string($em2FinancialSupportReceiveOutsideType) . "',
                receive_outside_support_source = '" . mysql_real_escape_string($em2FinancialSupportReceiveOutsideSource) . "',
                apply_outside_support_type = '" . mysql_real_escape_string($em2FinancialSupportApplyOutsideType) . "',
                apply_outside_support_source = '" . mysql_real_escape_string($em2FinancialSupportApplyOutsideSource) . "',
                family_support_type = '" . mysql_real_escape_string($em2FinancialSupportFamilyType) . "',
                family_support_amount = '" . mysql_real_escape_string($em2FinancialSupportFamilyAmount) . "',
                other_support_source = '" . mysql_real_escape_string($em2FinancialSupportOtherSource) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO em2_financial_support 
                (application_id, request_consideration, request_assistantship, attend_without_support, 
                    receive_outside_support_type, receive_outside_support_source, 
                    apply_outside_support_type, apply_outside_support_source, 
                    family_support_type, family_support_amount, other_support_source)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($em2FinancialSupportRequest) . ","
                . intval($em2AssistantshipRequest) . ","  
                . intval($em2FinancialSupportAttendWithout) . ",'" 
                . mysql_real_escape_string($em2FinancialSupportReceiveOutsideType) . "','" 
                . mysql_real_escape_string($em2FinancialSupportReceiveOutsideSource) . "','" 
                . mysql_real_escape_string($em2FinancialSupportApplyOutsideType) . "','" 
                . mysql_real_escape_string($em2FinancialSupportApplyOutsideSource) . "','"
                . mysql_real_escape_string($em2FinancialSupportFamilyType) . "','"
                . mysql_real_escape_string($em2FinancialSupportFamilyAmount) . "','"
                . mysql_real_escape_string($em2FinancialSupportOtherSource) . "')";
            mysql_query($insertQuery);
        }
    }
}
    
function checkRequirementsEm2FinancialSupport()
{
    global $err;
    global $em2FinancialSupportError;     
    
    if (!$err && !$em2FinancialSupportError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>