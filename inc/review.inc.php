<?
if($_SESSION['A_usertypeid'] != 11){//HID FROM STUDENT CONTACT ?>

<? if($_SESSION['A_usertypeid'] != 12){//HIDE FROM FAC CONTATCS ?>
<!-- REVIEW STUFF GOES HERE -->

<!-- PLB added div -->
    <div style="margin-bottom: 5px;">

<!-- COMMITTEE REVIEW ROUNDS 1 AND 2 (CSD, RI) -->
<? if($view == 2 && ($thisDept !=6 )){?>
<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<!--<input name="private_comments" type="hidden" value="<?=$private_comments?>">-->
<? if($round == 2){?>
<input name="round2" type="hidden" value="<?=$round2?>">
<? } ?>
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<?php  
// PLB added test to disable semi-blind control for MSE-MSIT departments 1/21/10.
$semiblindDisabled = '';
if ($thisDept == 18 || $thisDept == 19 ||
    $thisDept == 47 || $thisDept == 48 || $thisDept == 52) 
{
    if ($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1) {
        // Disable for everyone but su and admin
        $semiblindDisabled = 'disabled';    
    }          
}
?>
<input name='btnSemiblind' id='btnSemiblind' type='Submit' value='Toggle Semi-blind Review' <?php echo $semiblindDisabled; ?> >
<input name="showDecision" type="hidden" value="<?=$showDecision?>">

<? 
// PLB added save button here
print "&nbsp;&nbsp;";
showEditText("Save", "button", "btnSubmit", $allowEdit); 

// also added close div
?>
</div> 

<table width="700" border="0"  cellspacing="0" cellpadding="0">
   <? if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Committee <br>
      Scores:</strong></td>
    <td valign="top"><strong>
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept )//NOT A SUPPLEMENTAL OR FAC VOTE
        {
            ?><input name="revdept" type="hidden" value="<?=$committeeReviews[$i][32]?>"><?
            echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            //1st score
            ?>
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][10]?>">
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][11]?>">
            <?
            if(($thisDept == 3 || $thisDept == 66))
            {
                // PLB added program test 11/10/09.
                if ($showPhdInput) {
                    echo " PhD Score: ". $committeeReviews[$i][10];    
                }
                if ($showMsInput) {
                    echo " MS Score: ".$committeeReviews[$i][11];//show point 2     
                }    
            }else
            {
                echo " Score: ". $committeeReviews[$i][10];//show point 1
            }
            /*
            //2nd score
            if(($thisDept == 3 || $thisDept == 66) && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][11];
                }
            }
            */
            //1st score
            if(($thisDept == 3 || $thisDept == 66) )
            {
                /*
                But RI doesn't use certainty!!! 
                // PLB added program test 11/10/09.
                if ($showPhdInput) {
                    echo ", PhD Certainty: ". $committeeReviews[$i][28];    
                }
                if ($showMsInput) {
                    echo " MS Certainty: ".$committeeReviews[$i][28] ;//show certainty 2    
                }
                */
            }else
            {
                // PLB fixed - should be point1_certainty - 2/3/10
                //echo ", Certainty: ". $committeeReviews[$i][28];//show certainty 2 
                echo ", Certainty: ". $committeeReviews[$i][27];//show certainty 2
            }
            /*
            //2nd score
            
            if(($thisDept == 3 || $thisDept == 66) && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][28];
                }
            }
            */
            
            if($committeeReviews[$i][13] == 1)
            {
                echo ", Round 2: Yes";
            }else
            {
                echo ", Round 2: No";
            }
            
            echo "<br>";
        }
     } ?>
    </strong></td>
  </tr>

  <? if($semiblind_review != 1){ ?>
  <tr>
    <td colspan="2"><hr align="left">    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top">
    <? for($i = 0; $i < count($committeeReviews); $i++)
    {
    
        if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" )
        {

                ?>
                <?php 
                //PLB changed link here
                /*
                <a linkindex="1" href="userroleEdit_student_formatted.php?v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                */
                ?>
                <a linkindex="1" href="userroleEdit_student_review.php?d=<?=$thisDept?>&v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                <?=$committeeReviews[$i][2] . " ". $committeeReviews[$i][3]?></a> (<?=$committeeReviews[$i][15]?>)<br>
              <?
    
        }
     } ?></tr>
     <? } // end semiblind check for faculty reviews
     ?>
  <tr>
    <td colspan="2"><hr align="left" >    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Committee <br>Comments:</strong>      </td>
    <td valign="top">
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept)
        {
            echo "<input name='txtReviewerId' type='hidden' value='".$committeeReviews[$i][4]."'><strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            echo "</strong><br>";
            echo $committeeReviews[$i][9]."<br>";
        }
     } ?>    </td>
  </tr>

  <? }//end semiblind reviews ?>
  
  <!-- PLB commented out "Areas of Interest" 
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Areas of <br> Interest:</strong></td>
    <td valign="top">    </td>
  </tr>  
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
    --> 

<?php
  // PLB added coordinator notes 1/8/10
  if($semiblind_review != 1){ ?>
  <tr>
    <td colspan="2"><hr align="left"></td>
  </tr>
  <tr>  
    <td bgcolor="#c5c5c5" ><strong> Coordinator Notes: </strong> </td>
    <td><?
    $sql = "SELECT review.comments 
            FROM review
            INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
            WHERE review.application_id = " . $appid . " 
            AND lu_users_usertypes.usertype_id = 1";
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        echo $row['comments'] . '<br/>';
    }
    ?>    </td>
  </tr>
     <? } // end semiblind check for faculty reviews ?>


     <? if($showDecision == false){ 
         
         
            if ($thisDept == 25) {
                include "../inc/reviewCommitteeIsrse.inc.php";
            } else {   
     ?>

  <tr>
    <td colspan="2"><hr align="left" ></td>
  </tr>
  
  <!-- this is where user inputs start in every case? -->
  <?php
  if (($thisDept == 3 || $thisDept == 66)) {
  ?>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><b> Evaluate for Program(s): </b></td>
    <td>
    <?php
    foreach ($scorePrograms as $scoreProgramId => $scoreProgram) {
        echo $scoreProgram . ' ';
    }
    ?>
    </td>
  </tr>
  <?php
  }
  ?>
  
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Comments: </strong> </td>
    <td>
    <?

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        // PLB added round test to keep round2 data from showing up in round1 form 2/3/10.
        //if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" )
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round) 
        {
            $comments = $committeeReviews[$i][9];
        }
     }

     showEditText($comments, "textarea", "comments", $allowEdit, false, 80); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong>Personal<br>
      Comments: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $private_comments = $committeeReviews[$i][12];
        }
     }
    ?>
    <? showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 40); ?>    </td>
  </tr>
  <tr>
  <?php

  // PLB added test to display "Score" instead of "Ranking" for RI
  if(($thisDept == 3 || $thisDept == 66)){
      $ranking_label = "Score";
  } else {
    $ranking_label = "Ranking";
  }
  
  ?>
    <td bgcolor="#c5c5c5" width="50">
    <?php
    if ($showPhdInput) {
    ?>
    <strong><?= $ranking_label ?><? if(($thisDept == 3 || $thisDept == 66)){echo " (Phd)";} ?>: </strong> 
    <?php
    }
    ?>
    </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $point = $committeeReviews[$i][10];
        }
     }
    /* PLB - original test to determine ranking scale
    if(($thisDept == 3 || $thisDept == 66))
    {
        echo "Enter a number from 5.000 to 10: ";
        showEditText($point, "textbox", "point", $allowEdit, false);
    }else
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
    }    
    */
    // PLB new test to include different scales for RI and machine learning 
    //DebugBreak();
    if(($thisDept == 3 || $thisDept == 66)) // robotics institute
    {
        if ($showPhdInput) {
            // PLB edit here to change scale.
            echo "Enter a number from 5.0 to 10: ";
            showEditText($point, "textbox", "point", $allowEdit, false);
        }
    } elseif ($thisDept == 2) // machine learning
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $machineLearningVoteVals);
    } elseif ($thisDept == 8) // PLB added comp bio 01/05/09
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $compBioVoteVals);
    }
    else
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
    }
    ?></td>
  </tr>

  <?php
  // PLB added test to display "Score" instead of "Ranking" for RI
  if($thisDept != 3 && $thisDept != 66) { ?>
  <tr>
    <!-- PLB edit here -->
    <td bgcolor="#c5c5c5" width="50"><strong>Confidence<? if(($thisDept == 3 || $thisDept == 66)){echo " (PhD)";} ?>:</strong></td>
    <td><table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">High-</td>
          <td align="center"><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $pointCertainty = $committeeReviews[$i][27];
        }
     }

    showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals);
    ?></td>
          <td align="center">-Low </td>
        </tr>
      </table>
    </td>
  </tr>
  <?
  } // end RI if 
  /*
  DISPLAY 2ND SCORE FOR RI
  */
  if(($thisDept == 3 || $thisDept == 66) && $showMsInput){
  ?>
  <tr>
  <!-- PLB changed "Ranking" to "Score" for RI -->
    <td bgcolor="#c5c5c5" width="50"><strong>Score<? if(($thisDept == 3 || $thisDept == 66)){echo " (M.S.)";} ?>: </strong></td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $point2 = $committeeReviews[$i][11];
        }
     }
    if(($thisDept == 3 || $thisDept == 66))
    {
        // PLB edit here to change scale.
        echo "Enter a number from 5.0 to 10: ";
        showEditText($point2, "textbox", "point2", $allowEdit, false);
    }else
    {
        showEditText($point2, "radiogrouphoriz3", "point2", $allowEdit, false, $csdVoteVals);
    }
    ?></td>
  </tr>
  <!-- PLB commented out "Certainty" section
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong>Certainty<? if(($thisDept == 3 || $thisDept == 66)){echo " (M.S.)";} ?>:</strong></td>
    <td><table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">High-</td>
        <td align="center"><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $point2Certainty = $committeeReviews[$i][28];
        }
     }

    showEditText($point2Certainty, "radiogrouphoriz3", "point2Certainty", $allowEdit, false, $certaintyVals);
    ?></td>
        <td align="center">-Low </td>
      </tr>
    </table></td>
  </tr>
  
  -->
  
   <? }else{?>
      <input name="point2" type="hidden" value="<?=$point2?>">
  <? }//end if $doDisplay ?>
  <? if($round  == 1 ){ ?>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Round 2: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $round2 = $committeeReviews[$i][13];
        }
     }

    showEditText($round2, "radiogrouphoriz2", "round2", $allowEdit, false, array(array(1,"yes"),array(0,"No")) ); ?>    </td>
  </tr>
 
 <? }//end if round1 ?>
  <tr>
    <td colspan="2">
    <!-- PLB added div -->
    <div style="margin: 5px;">
    <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />

<!-- PLB commented out br, added spaces
     <br>
     <br>
     -->
     &nbsp;&nbsp;   
    <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>    
    </div>
    </td>
  </tr>
  <? 
     } // end if isr-se
     
     }//end not admin ?>
</table>
    
    
    <!-- FINAL ROUND DECISION STUFF -->
    <? } ?>
    <? if($_SESSION["A_usertypeid"] == 1 && $showDecision == true){ ?>
    
<table border =0 cellpadding="0" cellspacing=2 cellpading=2>
<? 
for($x = 0; $x < count($myPrograms); $x++)
{
    $aDepts = split(",", $myPrograms[$x][14]);
    //FIRST CHECK IF THE PROGRAM BELONGS TO THIS DEPARTMENT
    for($j = 0; $j < count($aDepts); $j++)
    {
        if($aDepts[$j] == $thisDept)
        {
            //CHECK IF PROGRAM IS ELIGIBLE FOR ROUND2
            ?>
            
            <tr><td >
<hr  align=left>
<strong>Admit Information for program:</strong> <em><?=$myPrograms[$x][1] . " ".$myPrograms[$x][2] . " ".$myPrograms[$x][3]?> </em>
</td>
</tr>


<tr>
<td>
<table border="0" cellpadding="0" cellspacing="2">
<tr>

<td bgcolor="c5c5c5" width="50"> <font color="000099"><b> Comments: </b> </td>
<td><?
    $comments = $myPrograms[$x][13];
    showEditText($comments, "textarea", "comments_".$myPrograms[$x][0], $allowEdit, false, 60); ?></td>
<td valign="top" align="right" bgcolor="c5c5c5" width="50"> <font color="000099"><b> Decision: </b> </td>
<td valign="top">
<? 
$decision = $myPrograms[$x][10] ;
showEditText($decision, "listbox", "decision_".$myPrograms[$x][0], $_SESSION['A_allow_admin_edit'], true, $decisionVals); 
?></td>
</tr>
</table></td>
</tr>

<tr>
<td>
<table border="0" cellpadding="0" cellspacing="2">
<tr><td width="50%"><font color="000099"><strong>Faculty Contact:&nbsp;</strong></font>
<? 
$faccontact = $myPrograms[$x][15];
showEditText($faccontact, "textbox", "faccontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); ?>
</td>
<td width="50%"><strong><font color="000099">Student Contact:&nbsp;</font></strong>
<? 
$stucontact = $myPrograms[$x][16];
showEditText($stucontact, "textbox", "stucontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); ?></td>
</tr>
</table></td>
</tr>
<tr>
<td>
<table border="0"  bgcolor="c5c5c5" cellpadding="0" cellspacing="2">
<tr><td><font color="000099"><h4>Admissions Status
</td></tr>
<tr>
<td  colspan="2"><font color="000099">
<? 
$admissionStatus = $myPrograms[$x][11];
showEditText($admissionStatus, "radiogrouphoriz2", "admissionStatus_".$myPrograms[$x][0], $allowEdit, false, array(array(0,"Reject"),array(1,"Waitlist"),array(2,"Admit"),array(3,"Reset")   ) ); ?></tr>
<!--
<tr>
<td >
<font color="000099"> &nbsp;&nbsp;&nbsp;<b>Admit to: </td>
<td width="270" align="left">
<?  showEditText($admit_to, "listbox", "admitto_".$myPrograms[$x][0], $allowEdit, false,  $myPrograms2); ?>
</td>
</tr>
-->
</table></td>
</tr>

            
            
            <?
        }//END IF DEPTS MATCH
    }//END FOR COUNT aDEPTS    
    
 }//END FOR EACH PROGRAM?>
 <tr>
  <td><? showEditText("Save", "button", "btnSubmitFinal", $allowEdit); ?></td>
</tr>
</table>
    
    
<? }//end show decision ?>

<!-- LTI REVIEWS -->
<? if($thisDept == 6 && (($round ==1 && $view == 2) || $view == 3) ){ ?>

<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>"> 
<input name='btnSemiblind' id='btnSemiblind' type='Submit' value='Toggle Semi-blind Review' >

<?php
// PLB added save button here
print "&nbsp;&nbsp;";
showEditText("Save", "button", "btnSubmit", $allowEdit);
?>

<table cellpadding="0" cellspacing="2">
<? if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Coordinator Notes: </strong> </td>
    <td bgcolor="#dedede"><?
    //GET LTI COORDINATOR ID
    $coordId = -1;
    $sql = "select lu_users_usertypes.id from lu_users_usertypes
    left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
    where lu_user_department.department_id=".$thisDept." and lu_users_usertypes.usertype_id=1";
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        $coordId = $row['id'];
    }

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $coordId)
        {
            $comments = $committeeReviews[$i][9];
            echo $comments;
        }
    }
    // PLB moved into if block
    //echo $comments;
    ?>    </td>
  </tr>
  <tr>
    <td colspan="2"><hr />
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Background: </strong> </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        
        if($committeeReviews[$i][29] == "")
        {
            
            echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
            echo $committeeReviews[$i][6] . "<br>";
        }
     }
     ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5" ><strong> Area(s) of Interest</strong>: </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Add Interest: </strong> </td>
    <td bgcolor="#dedede">
    
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>". $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            $committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
            for($j = 0; $j < count($committeeReviewsSeminars); $j++)
            {
                $idx = $committeeReviewsSeminars[$j][0];
                if($idx == "")
                {
                    $idx = 0;
                }else
                {
                    $idx = $idx  -1;
                }
                //echo $committeeReviewsSeminars[$j][0]. " ";
                if(marray_search($idx, $ltiSeminars) !== false)
                {
                    echo $ltiSeminars[ $idx ][1]. " ";
                }
                
            }
            
            echo "<br>";
        }
     } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Other Interests: </strong> </td>
    <td>
     
     <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            echo $committeeReviews[$i][26]."<br>";
        }
     } ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Statement: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            echo $committeeReviews[$i][8]."<br>";
        }
     } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Grades: </strong> </td>
    <td><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            echo $committeeReviews[$i][7]."<br>";
        }
     } ?>
    </td>
  </tr>
<!--
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Interview: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            echo $committeeReviews[$i][24]."<br>";
        }
     } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Refer to: </strong> </td>
    <td><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            echo $committeeReviews[$i][17]."<br>";
        }
     } ?>
    </td>
  </tr>
-->
  <tr>
    <!--
    <td bgcolor="#c5c5c5" ><strong> Brilliance: </strong> </td>
    -->
    <td bgcolor="#c5c5c5" ><strong> Special Consideration: </strong> </td>  
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            // PLB fixed array index problem
            // echo $committeeReviews[$i][24]."<br>";
            echo $committeeReviews[$i][25]."<br>";
        }
     } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Finished:</strong> </td>
    <td> Yes </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> Other: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
            echo $committeeReviews[$i][9]."<br>";
        }
     } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> PhD Score: </strong> </td>
    <td><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
//            echo $committeeReviews[$i][10]."<br>";
            echo number_format($committeeReviews[$i][10],2) ."<br>";
        }
     } ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong> MLT Score: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (".$committeeReviews[$i][23].")";
            }
            echo "</strong>";
//            echo $committeeReviews[$i][11]."<br>";
            echo number_format($committeeReviews[$i][11],2)."<br>";
        }
     } ?></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong>Avgs:</strong></td>
    <td>
    <? 
    $totalP = 0;
    $totalM = 0;
    for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "")
        {
            if($committeeReviews[$i][10] > 0)
            {
                $totalP += $committeeReviews[$i][10];
            }
            if($committeeReviews[$i][11] > 0)
            {
                $totalM += $committeeReviews[$i][11];
            }
        }
     } 
     
     if($totalP > 0)
     {
         $totalP = $totalP/count($committeeReviews);
     }
     if($totalM > 0)
     {
         $totalM = $totalM/count($committeeReviews);
     }
//     echo "PhD: ".$totalP . " MS: ".$totalM;
     echo "PhD: ".number_format($totalP,2) . " MS: ".number_format($totalM,2);

     ?>
    </td>
  </tr>

  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top">
    <? for($i = 0; $i < count($committeeReviews); $i++)
    {
    
        if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" )
        {

                ?>
                <?php 
                //PLB changed link here
                /*
                <a linkindex="1" href="userroleEdit_student_formatted.php?v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                */
                ?>
                <a linkindex="1" href="userroleEdit_student_review.php?d=<?=$thisDept?>&v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                <?=$committeeReviews[$i][2] . " ". $committeeReviews[$i][3]?></a> (<?=$committeeReviews[$i][15]?>)<br>
              <?
    
        }
     } ?></tr>
  
  
<?php } //end semiblind reviews ?> 
</table>
<!-- END LTI REVIEWS -->
<? }//END LTI REVIEWS ?>
<!-- LTI COORDINATOR -->
<? if($thisDept == 6 && $round ==1 && $view == 2 && ($_SESSION['A_usertypeid']==0 || $_SESSION['A_usertypeid']==1 )){ ?>
<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<input name="point" type="hidden" value="<?=$point?>">
<input name="point2" type="hidden" value="<?=$point2?>">
<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">
<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<!--<input name="touched" type="hidden" value="<?=$touched?>">-->
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<table width="100%" cellpadding="0" cellspacing="0">

  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Note: </strong> </td>
    <td><?

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $comments = $committeeReviews[$i][9];
        }
     }

     showEditText($comments, "textarea", "comments", $allowEdit, false, 60); ?>    </td>
  </tr>

  <tr>
    <td bgcolor="#c5c5c5"><strong>Refer to: </strong></td>
    <td bgcolor="#dedede"><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $grad_name = $committeeReviews[$i][17];
        }
     }
    showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Finished: </strong> </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $touched = $committeeReviews[$i][14];
        }
     }

    showEditText($touched, "radiogrouphoriz2", "touched", $allowEdit, false, array(array(1,"yes"), array(0,"No")) ); ?></td>
  </tr>
  <? if($round == 1){ ?>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Round 2: </strong> </td>
    <td bgcolor="#dedede">&nbsp;</td>
  </tr>
  <? } ?>
  <tr>
    <td colspan="2">
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />        
        <br><br>    
    <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>    </td>
  </tr>
</table>
<? }//end LTI COORDINATOR ?>


<!-- LTI COMMITTEE -->
<? if($thisDept == 6 && $round ==1 && $view == 2 && ($_SESSION['A_usertypeid']== 2 )){ ?>
<!--<input name="background" type="hidden" value="<?=$background?>">-->
<!--<input name="grades" type="hidden" value="<?=$grades?>">-->
<!--<input name="statement" type="hidden" value="<?=$statement?>">-->
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<!--<input name="touched" type="hidden" value="<?=$touched?>">-->
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<table width="100%" cellpadding="0" cellspacing="2">
<!--
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Coordinator Notes: </strong> </td>
    <td bgcolor="#dedede">
    <?
    //GET LTI COORDINATOR ID
    $coordId = -1;
    $sql = "select lu_users_usertypes.id from lu_users_usertypes
    left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
    where lu_user_department.department_id=".$thisDept." and lu_users_usertypes.usertype_id=1";
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
        $coordId = $row['id'];
    }

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $coordId)
        {
            $comments = $committeeReviews[$i][9];
            echo $comments;
        }
    }
    // PLB moved to if block
    //echo $comments;
    ?>    </td>
  </tr>
-->
  <tr>
    <td colspan="2"><hr />
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Background: </strong> </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $background = $committeeReviews[$i][6];
        }
     }
     showEditText($background, "textarea", "background", $allowEdit, false, 60); ?>
     </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5" width="50"><strong> Area(s) of Interest</strong>: </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Add Interest: </strong> </td>
    <td bgcolor="#dedede">
    <?

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
        }
     }
    showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $ltiSeminars ); ?> </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Other Interests: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $other_interest = $committeeReviews[$i][26];
        }
     }
    showEditText($other_interest, "textbox", "other_interest", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Statement: </strong> </td>
    <td bgcolor="#dedede">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $statement = $committeeReviews[$i][8];
        }
     }
     showEditText($statement, "textbox", "statement", $allowEdit, false, null,true,40); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Grades: </strong> </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $grades = $committeeReviews[$i][7];
        }
     }
     showEditText($grades, "textbox", "grades", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
<!--
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Interview: </strong> </td>
    <td bgcolor="#dedede"><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $interview = $committeeReviews[$i][24];
        }
     }
    showEditText($interview, "textbox", "interview", $allowEdit, false, null,true,40); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Refer to: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $grad_name = $committeeReviews[$i][17];
        }
     }
    showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
-->
  <tr>
    <!--
    <td bgcolor="#c5c5c5" width="50"><strong> Brilliance: </strong> </td>
    -->
    <td bgcolor="#c5c5c5" width="50"><strong> Special Consideration: </strong> </td>
    <td bgcolor="#dedede"><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            // PLB fixed array index problem
            //$brilliance = $committeeReviews[$i][24];
            $brilliance = $committeeReviews[$i][25];
        }
     }
    showEditText($brilliance, "textbox", "brilliance", $allowEdit, false, null,true,40); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Other: </strong> </td>
    <td><?

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $comments = $committeeReviews[$i][9];
        }
     }

     showEditText($comments, "textarea", "comments", $allowEdit, false, 60); ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> PhD Score: </strong> </td>
    <td bgcolor="#dedede">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $point = $committeeReviews[$i][10];
        }
     }

    showEditText($point, "radiogrouphoriz2", "point", $allowEdit, false, $ltiVoteVals);

    ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> MLT Score: </strong> </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $point2 = $committeeReviews[$i][11];
        }
     }

    showEditText($point2, "radiogrouphoriz2", "point2", $allowEdit, false, $ltiVoteVals);

    ?></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
</table>
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
<br><br>
<? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>
<? }//END LTI COMMITTEE ?>



<!-- FACULTY REVIEW FOR ROUND 2 -->
<? if($view == 3 && ($round == 2 || $round == 3) ){ ?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="comments" type="hidden" value="<?=$comments?>">
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="facVote" type="hidden" value="1">
<!--<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">-->
<!--<input name="recruited" type="hidden" value="<?=$recruited?>">-->
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<!--<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">-->
<!--<input name="advise_time" type="hidden" value="<?=$advise_time?>">-->
<!--<input name="commit_money" type="hidden" value="<?=$commit_money?>">-->
<!--<input name="fund_source" type="hidden" value="<?=$fund_source?>">-->
<?php
// PLB added semiblind, view all comments for HCII faculty 2/2/10
// WAIT: misunderstood request? - backing off for now.
/*
//DebugBreak();
if ($thisDept == 5){
    ?>
    <input name='btnSemiblind' id='btnSemiblind' type='Submit' value='Toggle Semi-blind Review' />
    <input name="showDecision" type="hidden" value="<?=$showDecision?>" />
    <input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>" />
    <?php
    if($semiblind_review != 1){ ?>

  <table>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Committee <br>
      Scores:</strong></td>
    <td valign="top"><strong>
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept )//NOT A SUPPLEMENTAL OR FAC VOTE
        {
            ?><input name="revdept" type="hidden" value="<?=$committeeReviews[$i][32]?>"><?
            echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            //1st score
            ?>
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][10]?>">
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][11]?>">
            <?

                echo " Score: ". $committeeReviews[$i][10];//show point 1
                echo ", Certainty: ". $committeeReviews[$i][28];//show certainty 2
            
            if($committeeReviews[$i][13] == 1)
            {
                echo ", Round 2: Yes";
            }else
            {
                echo ", Round 2: No";
            }
            
            echo "<br>";
        }
     } ?>
    </strong></td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" >    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Committee <br>Comments:</strong>      </td>
    <td valign="top">
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept)
        {
            echo "<input name='txtReviewerId' type='hidden' value='".$committeeReviews[$i][4]."'><strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            echo "</strong><br>";
            echo $committeeReviews[$i][9]."<br>";
        }
     } ?>    </td>
  </tr>
  
  <tr>
    <td colspan="2"><hr align="left">    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top">
    <? for($i = 0; $i < count($committeeReviews); $i++)
    {
    
        if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" )
        {
                ?>
                <a linkindex="1" href="userroleEdit_student_review.php?d=<?=$thisDept?>&v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                <?=$committeeReviews[$i][2] . " ". $committeeReviews[$i][3]?></a> (<?=$committeeReviews[$i][15]?>)<br>
              <?
    
        }
     } ?>
     </tr>
     </table>
     <? } 
    
}
*/
?>
<table cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>PART I.  INFORMATION FOR THE ADMISSIONS COMMITTEE. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Do you think we should admit this applicant?  Select one, and explain why by provding other pertinent information that might be useful to the Admissions Committee and your colleagues (e.g. direct knowledge of the student or his/her research, calibration of the recommenders, etc.) </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $admit_vote = $committeeReviews[$i][15];
        }
     }

    showEditText($admit_vote, "radiogrouphoriz2", "admit_vote", $allowEdit, false,
    array(
    array("DEFINITELY","DEFINITELY"),
    array("Probably Yes","Probably Yes"),
    array("Probably No","Probably No"),
    array("DEFINITELY NOT","DEFINITELY NOT")
    )
    ); ?>
    <br>

<?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $pertinent_info = $committeeReviews[$i][18];
        }
     }
     showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 60); ?>
  </td>
  </tr>





<!--OTHER FAC COMMENTS -->  
<tr>
    <td colspan="2" bgcolor="#c5c5c5">
    <strong>Other faculty comments    
    </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" 
            && $committeeReviews[$i][32] == $thisDept && $committeeReviews[$i][4] != $reviewerId )
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] ."</strong><br>".$committeeReviews[$i][18]."<br>";
        }
    }
    ?>
    </td>
  </tr> 



<!-- END OTHER FAC COMMENTS -->
<tr>
    <td colspan="2" bgcolor="#c5c5c5">
    <strong>Please rank at least your top 3 choices. This applicant is my:    
    </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    $rank = 0;
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && 
        $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $rank = $committeeReviews[$i][31];
        }
     }
    $arr = array(array(0,"This is not a top 5 choice"),    array(1,"1st Choice"),array(2,"2nd Choice") ,array(3,"3rd Choice"),array(4,"4th Choice"),array(5,"5th Choice")   );
    if($rank == "")
    {
        $rank = 0;
    }
    if($allowEdit == true)
    {
        
        showEditText($rank, "radiogrouphoriz2", "rank", $allowEdit, false, $arr     ); 
    }else
    {
        
        echo $arr[$rank][1];
        
        
    }    
    ?>
    </td>
  </tr>  
  
  
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would be willing to play an active role in recruiting this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $recruited = $committeeReviews[$i][16];
        }
     }

     $arr = array(    array(0, "No"), array(1,"Yes"));
    if($allowEdit == true)
    {
        showEditText($recruited, "radiogrouphoriz2", "recruited", $allowEdit, false, $arr      ); 
    }else
    {
        if($recruited != "")
        {
            echo $arr[$recruited][1];
        }
    }    
    ?>

    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I suggest the following graduate students as possible recruiting contacts for this student: </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $grad_name = $committeeReviews[$i][17];
        }
     }
    showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <!--
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Other pertinent information that might be useful to the Admissions Committee (e.g. direct       knowledge of the student or his/her research, calibration of the recommenders, etc.) </strong></td>
  </tr>
 
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $pertinent_info = $committeeReviews[$i][18];
        }
     }
     showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 60); ?>
    </td>
  </tr>
   -->
  <tr>
    <td colspan="2"><hr align="left" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>PART II.  INFORMATION FOR THE DEPARTMENT CHAIR. <br />
      In the event that this student is considered on the borderline, your  answers to the following questions could make a difference in the  decision the department chair makes on whether or not to offer  admission to the student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would commit my time and energy to advise this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
        <?
        for($i = 0; $i < count($committeeReviews); $i++)
        {
            if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
            {
                $advise_time = $committeeReviews[$i][19];
            }
         }
//         $arr =  array(    array(1,"Yes"),    array(0,"No") );
         $arr =  array(    array(0, "No"), array(1,"Yes") );
        if($allowEdit == true)
        {
         
            showEditText($advise_time, "radiogrouphoriz2", "advise_time", $allowEdit, false,$arr     ); 
        }else
        {
            if($recruited != "")
            {
                echo $arr[$advise_time][1];
            }
        }        
        ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would commit my money to advise this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $commit_money = $committeeReviews[$i][20];
        }
     }
//     $arr = array(    array(1,"Yes"),    array(0,"No") ) ;
     $arr = array(    array(0,"No"), array(1,"Yes") ) ;
     if( $allowEdit == true)
     {
     
        showEditText($commit_money, "radiogrouphoriz2", "commit_money", $allowEdit, false, $arr    ); 
    }else
    {
        if($recruited != "")
        {
            echo $arr[$commit_money][1];
        }
    }
    ?>
     <br />
      <strong> If yes, please state your probable source of funding.</strong> <br />
      <?
          for($i = 0; $i < count($committeeReviews); $i++)
        {
            if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
            {
                $fund_source = $committeeReviews[$i][21];
            }
         }
       showEditText($fund_source, "textarea", "fund_source", $allowEdit, false, 60); ?><br>
<br>

      </td>
  </tr>
   <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
     <!-- REMOVE OVERALL RANKING  
      <tr>
        <td bgcolor="#c5c5c5"><strong>Ranking - Overall:</strong></td>
        <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
        {
            $point = $committeeReviews[$i][10];
        }
     }
    if(($thisDept == 3 || $thisDept == 66))
    {
        echo "Enter a number from 5.000 to 10: ";
        showEditText($point, "textbox", "point", $allowEdit, false);
    }else
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
    }
    ?></td>
        </tr>
   
        
      <tr>
        <td width="50" bgcolor="#c5c5c5"><strong>Certainty:</strong></td>
        <td><table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center">High-</td>
            <td align="center"><?
                for($i = 0; $i < count($committeeReviews); $i++)
                {
                    if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" && $committeeReviews[$i][23] == $round)
                    {
                        $pointCertainty = $committeeReviews[$i][27];
                    }
                 }
            
                showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals);
            
                ?></td>
            <td align="center">-Low </td>
          </tr>
        </table>
                  </td>
        </tr>
    </table></td>
  </tr>
     -->
   <? 
   if(count($myAreasAll) > 0){  ?>
       <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Please discuss with your colleagues and enter a score that reflects the strength of this applicant within the research areas listed below.</strong></td>
  </tr>
  
  <? 
  }
      for($j = 0; $j < count($myAreasAll); $j++){ ?>
      <tr>
      <td colspan="2">
    
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#c5c5c5" WIDTH="50"><strong>
              <?=$myAreasAll[$j][1]?>
              :</strong></td>
            <td><?
            $val = 0;
            for($i = 0; $i < count($committeeReviews); $i++)
            {
                if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
                {
                    $val = $committeeReviews[$i][10];
                }
             }
            
            if(($thisDept == 3 || $thisDept == 66))
            {
                echo "Enter a number from 5.000 to 10: ";
                showEditText($val, "textbox", "point_".$myAreasAll[$j][0], $allowEdit, false);
            }else
            {
                ?><TABLE BORDER=0>
                <tr>
                  <td>Low-</td>
                  <td>
                <? 
                showEditText($val, "radiogrouphoriz3", "point_".$myAreasAll[$j][0], $allowEdit, false, $csdFacVoteVals);
                ?></td>
                  <td>-High</td>
                </tr></table><?
            }
            
            ?></td>
            </tr>
                  <!-- DONT SHOW CERTAINTY
                        <tr>
                          <td width="50" bgcolor="#c5c5c5"><strong>Certainty:</strong></td>
                          <td><table border="0" cellspacing="0" cellpadding="0">
                         <tr>
                        <td align="center">High-</td>
                        <td align="center"><?
                        $val = 0;
                        for($i = 0; $i < count($committeeReviews); $i++)
                        {
                        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
                        {
                        $val = $committeeReviews[$i][27];
                        }
                         }
                        
                        showEditText($val, "radiogrouphoriz3", "pointCertainty_".$myAreasAll[$j][0], $allowEdit, false, $certaintyVals);
                        
                        ?></td>
                        <td align="center">-Low</td>
                         </tr>
                          </table>
                        </td>
                        </tr>
                -->
        </table>
    
    
    
    
      </td>
      </tr>
  <? }//END AOI REVIEWS ?>
  <tr>
    <td colspan="2">
    <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />

<!-- PLB commented out line breaks, added spaces
    <br>
    <br>
-->
&nbsp;&nbsp;
    <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?></td>
  </tr>
</table>


<? }//end if view ?>
<!-- END REVIEW STUFF -->


<? 
}//END FAC CONTACT HIDE
}//end student contact HIDE
?>
