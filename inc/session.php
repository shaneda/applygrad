<?
//$expireTime = 60*60*5; // 5 hours
//session_set_cookie_params($expireTime);

/*
* Redirect from http to https if necessary.
*/
if( !isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off' ) { 
    $serverName = $_SERVER["SERVER_NAME"];
    $scriptName = $_SERVER["SCRIPT_NAME"];
    header("Location: https://" . $serverName . $scriptName);
    exit;
}

/*
* Set the environment.
*/
include_once '../inc/config.php'; 
include_once '../inc/db_connect.php';

function logSessionAccess($accessLogMessage) {
      
    $applicationId = NULL;
    if (isset($_SESSION['appid'])) {
        $applicationId = intval($_SESSION['appid']);    
    }
    
    $usertypeId = NULL;
    $usersId = NULL;
    $luuId = NULL;
    if (isset($_SESSION['usertypeid'])) {
    
        $usertypeId = intval($_SESSION['usertypeid']);    

        if (isset($_SESSION['usermasterid'])) {
            $usersId = intval($_SESSION['usermasterid']);
        }
        
        if (isset($_SESSION['userid'])) {
            $luuId = intval($_SESSION['userid']);
        }
    } 
    elseif (isset($_SESSION['A_usertypeid'])) 
    {
        $usertypeId = intval($_SESSION['A_usertypeid']);
        
        if (isset($_SESSION['A_usermasterid'])) {
            $usersId = intval($_SESSION['A_usermasterid']);
        }
        
        if (isset($_SESSION['A_userid'])) {
            $luuId = intval($_SESSION['A_userid']);
        }
    }
    
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $client = $_SERVER['REMOTE_ADDR'];    
    }

    $accessLogQuery = "INSERT INTO accesslogreplyform 
                (users_id, lu_users_usertypes_id, usertype_id, 
                application_id, client, activity)
                VALUES (" 
                . $usersId . ","
                . $luuId . ","
                . $usertypeId . ","
                . $applicationId . ",'"
                . @mysql_real_escape_string($client) . "','"
                . @mysql_real_escape_string($accessLogMessage) . "'"
                .")";
    
    @mysql_query($accessLogQuery);   
}

/*
* Error reporting level now handled in config files.
*/
/*
if ($environment == 'development' || $environment == 'test') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);    
} else {
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors', 0);
}
*/

/*
* Start a new session only if the including script 
* hasn't already started it. 
*/
if ( !isset($_SESSION) ) {
    session_start();    
}

if ((isset ($_SESSION['A_usertypeid'])) && $_SESSION['A_usertypeid'] == 19) {
    $_SESSION['domainid'] = 37;
    
} else {


// REMOVE THIS?????????????????????????
// Set usertypeid for recommender. 
$test = strstr($_SERVER['SCRIPT_NAME'], 'rec.php');
if ($test) {
    $_SESSION['usertypeid']= 6;
}

// REMOVE THIS ?????????????????
if(!isset($_SESSION['SECTION'])){ //STUDENT INTERFACE OR ADMIN
	$_SESSION['SECTION']= "1";
}


if(!isset($_SESSION['domainid'])){ //POSTED DOMAIN ID
	    $_SESSION['domainid']= -1;
}

if(!isset($_SESSION['usertypeid'])){ //SYSTEM ROLE ID
	$_SESSION['usertypeid']= -1;
}

// Unless you are a recommender, the domain param must be set.
// Otherwise, redirect to "nodomain." 
if( ((!isset($_SESSION['domainid']) || $_SESSION['domainid'] == -1) && $_SESSION['usertypeid'] != 0 && $_SESSION['usertypeid'] != 1) 
&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'launchwebs.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'apply/accountCreate.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'apply/forgotPassword.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'apply/newPassword.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'admin/') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'faq.php') !== false
&& strstr($_SERVER['SCRIPT_NAME'], 'instructions.php') !== false
)
{ 
	if(!isset($_GET['domain']) &&  $_SESSION['usertypeid'] != 6)//ALLOW RECOMMENDER TO COME IN
	{
		//echo $_SESSION['SECTION'];
		session_unset();
		destroySession();  // Is session_unset() called twice for a reason?!
		//echo "redirect";
		//DebugBreak();
		header("Location: nodomain.php");
        exit;
	}
}

if(!isset($_SESSION['domainname'])){ //POSTED DOMAIN NAME
	$_SESSION['domainname']= "";
}

if(!isset($_SESSION['level'])){ //DEGREE LEVEL WILL SHOW ONLY PROGRAMS ASSOCIATED WITH DEGREE
	$_SESSION['level']= 2;//GRADUATE IS DEFAULT
}

if(!isset($_SESSION['allow_edit']))
{
	$_SESSION['allow_edit']= false;
	$_SESSION['allow_edit_late']= false;
	
	if($_SESSION['domainid'] > -1 && $_SESSION['domainname'] == "")
	{
		$sql = "SELECT name FROM domain where id=".$_SESSION['domainid'];
		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$_SESSION['domainname']  = $row['name'];
		}
	}	
}

if( !( isset($_SESSION['expdate']) && isset($_SESSION['expdate2']) ) ) {
    
    if($_SESSION['domainid'] > -1)
    {
	    $sql = "SELECT expdate,expdate2 FROM systemenv where domain_id=".$_SESSION['domainid'];
    }
    else
    {
	    $sql = "SELECT expdate,expdate2 FROM systemenv where domain_id=1";
    }
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['expdate'] = "";
    $_SESSION['expdate2']  = "";
    while($row = mysql_fetch_array( $result )) 
    {
	    $_SESSION['expdate']  = $row['expdate'];
	    $_SESSION['expdate2']  = $row['expdate2'];
    }
}
 

 
if (strtotime( date("Y-m-d h:i:s")) < strtotime($_SESSION['expdate'])+86400)
{
	$_SESSION['allow_edit']= true;
}
elseif (isset($_SESSION['allow_edit']) && $_SESSION['allow_edit'] == FALSE)
{
	if(isset($_SESSION['A_usertypeid']))
	{
		if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1)
		{
			$_SESSION['allow_edit']= true;
		}
	}else
	{
		$_SESSION['allow_edit'] = false;
	}
	
}

 
//if(strtotime(date("Y-m-d h:i:s")) < strtotime($_SESSION['expdate2'])+86400  && $_SESSION['expdate2'] != "0000-00-00 00:00:00")
das_debug();
if (strncmp($_SESSION['expdate2'],"0000-00-00 00:00:00", 4)!= 0) {
	if (strtotime(date("Y-m-d h:i:s")) < strtotime($_SESSION['expdate2'])+86400)
	{	
		//$_SESSION['allow_edit'] = true;
		$_SESSION['allow_edit_late']= true;
	}else
		{

			if(isset($_SESSION['A_usertypeid']))
			{
				if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 19)
				{
					$_SESSION['allow_edit_late']= true;
				}else
				{
					$_SESSION['allow_edit_late']= false;
					if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
					{
					if ($_SESSION['allow_edit'] == false) {
                        logSessionAccess("offline 1");
						header("Location: offline.php");
                        exit;
					}
					}
				}
			} else
                {   
                    if (strstr($_SERVER['SCRIPT_NAME'], 'placeout') !== false ||
                    (isset ($_SESSION['placeout_enabled']) && $_SESSION['placeout_enabled'] == TRUE)) 
                    {
                        //handle placeout session
                        $_SESSION['allow_edit_late']= true; 
                        // debugBreak();
                         // exit;
                    } else if (strstr($_SERVER['SCRIPT_NAME'], 'replyform') !== false ||
                    (isset ($_SESSION['replyform_enabled']) && $_SESSION['replyform_enabled'] == TRUE)) 
                    {
                        //handle placeout session
                        $_SESSION['allow_edit_late']= true; 
                        // debugBreak();
                         // exit;
                    } else
                    
                    {   
                        if (strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false) {
                            if ($_SESSION['allow_edit_late'] == false) {
                            logSessionAccess("offline 2");  
                             header("Location: offline.php");
                             exit;
                             }
                             }
                    }
                }
		}
	}
	else
		{

		   $_SESSION['allow_late_edit'] = false;
		   if (($_SESSION['allow_edit'] == false) && (strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)) {
               logSessionAccess("offline 3");
			header("Location: offline.php");
            exit;
			} 
		}

if(!isset($_SESSION['allow_admin_edit']))
{
	$_SESSION['allow_admin_edit']= true;
}


if(!isset($_SESSION['usertypename'])){ //SYSTEM ROLE TYPE NAME
	$_SESSION['usertypename']= "";
}
if(!isset($_SESSION['email'])){ //USERS EMAIL
	$_SESSION['email']= "";
}
if(!isset($_SESSION['firstname'])){ //USERS firstname
	$_SESSION['firstname']= "";
}
if(!isset($_SESSION['lastname'])){ //USERS lastname
	$_SESSION['lastname']= "";
}

if(!isset($_SESSION['admin_depts']))//DOMAINS THE ADMIN IS ALLOWED TO SEE
{
	$_SESSION['admin_depts']= array();
}
//FOR APPLYWEB
if(!isset($_SESSION['usermasterid'])){
	$_SESSION['usermasterid']= -1;
}
if(!isset($_SESSION['userid'])){
    if (isset($_SESSION['A_userid']) && isset($SESSION['A_usertypeid']) &&  $SESSION['A_usertypeid'] == 19) {
        $_SESSION['userid'] = $_SESSION['A_userid'];
    }
    else {
	    $_SESSION['userid']= -1;
    }
}
if(!isset($_SESSION['appid'])){ //STUDENTS APPLICATION NUMBER
	$_SESSION['appid']= -1;
}

if(isset($_SESSION['domainid']))
{

    //echo $_SESSION['usertypeid'];
	switch($_SESSION['SECTION'])
	{
		case "1":
				//echo $_SESSION['userid'];

			switch($_SESSION['usertypeid'])
			{
				case 0:
					// no restrictions
					break;
				case 6://RECOMMENDER
	
					if($_SESSION['userid'] == -1)
					{
						//redirect
						if( $_SERVER['SCRIPT_NAME'] != '/apply/nodomain.php')
						{
							header("Location: index.php");
                            exit;
							//echo "redirect index";
						}
					}
					if($_SESSION['userid'] != -1
					&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'recommender.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'recommenderUpload.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'faq.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'instructions.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'recform_academic.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'recform_industrial.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false
                    && strstr($_SERVER['SCRIPT_NAME'], 'fileDownload.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/accountCreate.php') === false)
					{
						//redirect
						if( $_SERVER['SCRIPT_NAME'] != '/apply/nodomain.php')
						{
							header("Location: recommender.php");
                            exit;
							//echo "redirect index";
						}
					}else
					{
						if($_SESSION['domainid'] == -1)
						{
							if(strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false 
							&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
							{
                             //   DebugBreak();
								header("Location: nodomain.php");
                                exit;
								//echo "nodomain index";
							}
						}
					}
				break;
				case 1:
					//NO RESTRICTIONS
					break;
				case 5:
					//STUDENT
					if($_SESSION['userid'] == -1 
					&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/home.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/applications.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/applicationEdit.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/accountCreate.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'faq.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'instructions.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false
					)
					{
                        if  (!strstr($_SERVER['SCRIPT_NAME'], "/replyform/home.php")) {
						//redirect
						if( $_SERVER['SCRIPT_NAME'] != '/apply/nodomain.php'
						&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
						{              
							header("Location: index.php");
                            exit;
							//echo "redirect index";
						}
                    }
                    }
                        else
					{
						if($_SESSION['domainid'] == -1)
						{
							if(strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false
							&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
							{
                             //   DebugBreak();
								header("Location: nodomain.php");
                                exit;
								//echo "nodomain index";
							}
						}
					}
                    
					break;
        /*        case -1:
                
                if (isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] == 19) {
                    if (isset($_REQUEST['studentLuId'])) {
                       $placeoutAppId = $_REQUEST['studentLuId'];
                       $placeoutType = $_REQUEST['type'];
                       $sql = "SELECT id FROM mhci_prereqs WHERE prereq_type = '" . $placeoutType . "' AND application_id = " . $placeoutAppId;
                       $result = mysql_query($sql) or die(mysql_error());
                       while($row = mysql_fetch_array( $result )) {
                           $placeoutId = $row['id'];
                       }
                       header("Location: mhci_prereqs_" . $placeoutType ."?id=" . $placeoutId . "&t=2&role=reviewer"); 
                    }
                    break;
                }
               */ 
				default:
                    // If no app id and NOT one of these files....  
                    if($_SESSION['appid'] == -1 
					&& strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'launchwebs.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
					//&& strstr($_SERVER['SCRIPT_NAME'], 'apply/home.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/newPassword.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'apply/forgotPassword.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'faq.php') === false
					&& strstr($_SERVER['SCRIPT_NAME'], 'instructions.php') === false
					)
					{
						//echo "redirect";
					//	DebugBreak();				
						if ($_SESSION['userid'] != -1) {
							header("Location: home.php");
                            exit;
						}else{
							// Redirect if NOT one of these files. (redundant??????)
                            if(strstr($_SERVER['SCRIPT_NAME'], 'nodomain.php') === false
							&& strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'faq.php') === false
							&& strstr($_SERVER['SCRIPT_NAME'], 'instructions.php') === false
							&& strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'reply') === false
							)
							{            
                                header("Location: index.php");
                                exit;
							}
						}
						
					} else {
                        $test = strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php');
                        if ($_SESSION['domainid'] == -1 &&
                            (strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') !== false
                            || strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') !== false
                            || strstr($_SERVER['SCRIPT_NAME'], 'faq.php') !== false
                            || strstr($_SERVER['SCRIPT_NAME'], 'instructions.php') !== false)
                            )
                        {
                        //    DebugBreak();
                            header("Location: nodomain.php");
                            exit;   
                        }
                    }
				break;
			}//SWITCH USERTYPEID
					 
		break;
		case "2"://ADMIN SECTION
			switch($_SESSION['usertypeid'])
			{
				case 0://SUPERUSER
					//NO RESTRICTIONS
				break;
				case 1:
					//NO RESTRICTIONS
					break;
				default:
					if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false  
					&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false)
					{     
						header("Location: index.php");
                        exit;
					}
				break;
			}//SWITCH USERTYPEID
		break;
	}//END SWITCH SECTION
}//end if $_SESSION['domainid']

$_SESSION['datafileroot']= $datafileroot;

if(!isset($_SESSION['programs'])){
	$_SESSION['programs']= array();//USER-SELECTED PROGRAMS
}

if(!isset($_SESSION['msgsSent'])){
	$_SESSION['msgsSent']= 0;
}

//FOR ADMIN
if(isset($_SERVER['REMOTE_USER'])){
	//$_SESSION['email']= $_SERVER['REMOTE_USER'];
}


function destroySession()
{
	session_unset();
}
}
?>