<div style="padding: 2px; width: auto; background: #F7F7F7;">
<form id="filterForm" name="filterForm" action="<?php echo $returnUrl; ?>" method="GET">
<input type="hidden" id="filterUnit" name="unit" value="<?php echo $unit; ?>" />
<input type="hidden" id="filterUnitId" name="unitId" value="<?php echo $unitId; ?>" />
<?php
$isIsreeDepartment = FALSE;
$isMsrtDepartment = FALSE;
if ($unit == 'department') {
    $isIsreeDepartment = isIsreeDepartment($unitId);
    $isMsrtDepartment = isMsrtDepartment($unitId);    
}

if ($periodId) {    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids
    if (is_array($periodId)) {
        foreach ($periodId as $id) {
            echo '<input type="hidden" id="filterPeriod" name="period[]" value="' . $id . '"/>';    
        }   
    } else {
        echo '<input type="hidden" id="filterPeriod" name="period" value="' . $periodId . '"/>';     
    }    
}
if ($programId) {
    echo '<input type="hidden" id="filterProgramId" name="programId" value="' . $programId . '" />';    
}
?>
<input type="hidden" id="filterLimit" name="limit" value="<?php echo $limit; ?>" />
<input type="hidden" name="showReturnLink" value=""/>
<table border="0" cellspacing="0" cellpadding="2">
    <tr>
        <td align="right" id="paymentStatusLabel"> &nbsp;&nbsp;Payment:</td>
        <td align="left">
            <select class="filter" name="paymentStatus">
                <option value="unpaidUnwaived" <?php echo $unpaidUnwaivedSelected; ?>>Unpaid / Unwaived</option>
                <option value="paidWaived" <?php echo $paidWaivedSelected; ?>>Paid / Waived</option>
                <option value="paid" <?php echo $paidSelected; ?>>Paid</option>
                <option value="waived" <?php echo $waivedSelected; ?>>Waived</option>
                <option value="allPayment" <?php echo $allPaymentSelected; ?>>No Filter</option>
            </select>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><input type="submit" name="submitFilter" value="Filter / Refresh Full List"/></td>
    </tr>
    
    <tr>
        <td colspan="4" align="right">
        <a href="<?php echo $_SERVER['REQUEST_URI']?>" onClick="setNoFilter(); return false;">Full&nbsp;List&nbsp;w/&nbsp;Filter&nbsp;Off</a>
        <br>
        <a href="<?php echo $_SERVER['REQUEST_URI']?>" onClick="restoreDefaultFilter(); return false;">Full&nbsp;List&nbsp;w/&nbsp;Default&nbsp;Filter</a>
        
        
        </td>
    </tr>
</table>
</form>
</div> 