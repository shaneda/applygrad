<?php
$greRecord = $greRecords[0];
$greScoreId = $greRecord['id'];

$greDate = date("m/Y", strtotime($greRecord['testdate']));

$greVerbalScore = $greRecord['verbalscore'];
$greVerbalPercentile = $greRecord['verbalpercentile'];

$greQuantitativeScore = $greRecord['quantitativescore'];
$greQuantitativePercentile = $greRecord['quantitativepercentile'];

$greAnalyticalScore = $greRecord['analyticalscore'];
$greAnalyticalPercentile = $greRecord['analyticalpercentile'];

$greWritingScore = $greRecord['analyticalwritingscore'];
$greWritingPercentile = $greRecord['analyticalwritingpercentile']; 

$receivedChecked = '';
$receivedMessage = '';
$receivedClass = 'confirm';
if ($greRecord['scorereceived'] == 1) {
    $receivedChecked = 'checked="checked"';
    $receivedMessage .= ' rcd';
    $receivedClass = 'confirmComplete';
}

echo <<<EOB
<form class="greScore" id="greScore_{$applicationId}" action="">
<table cellpadding="2px" cellspacing="2px" border="0">
<tr>
    <td colspan="3">GRE General&nbsp;&nbsp;
    <input type="checkbox" class="greReceived" 
        id="greReceived_{$applicationId}_{$greScoreId}" {$receivedChecked} /> Received
    </td>
</tr>
<tr>
    <td>Test Date</td>
    <td colspan="2">
    <input type="text" size="10" class="greTestDate" 
        id="greTestDate_{$applicationId}_{$greScoreId}" value="{$greDate}" />
    </td>
</tr>
<tr>
    <td></td>
    <td>Score</td>
    <td>Pctile</td>
</tr>
<tr>
    <td>Verbal</td>
    <td>
    <input type="text" size="3" class="greVerbalScore" 
        id="greVerbalScore_{$applicationId}_{$greScoreId}" value="{$greVerbalScore}" />
    </td>
    <td>
    <input type="text" size="3" class="greVerbalPercentile" 
        id="greVerbalPercentile_{$applicationId}_{$greScoreId}" value="{$greVerbalPercentile}" />
    </td>
</tr>
<tr>
    <td>Quantitative</td>
    <td>
    <input type="text" size="3" class="greQuantitativeScore" 
        id="greQuantitativeScore_{$applicationId}_{$greScoreId}" value="{$greQuantitativeScore}" />
    </td>
    <td>
    <input type="text" size="3" class="greQuantitativePercentile" 
        id="greQuantitativePercentile_{$applicationId}_{$greScoreId}" value="{$greQuantitativePercentile}" />
    </td>
</tr>
<tr>
    <td>Analytical</td>
    <td>
    <input type="text" size="3" class="gre_general_analytical_score" 
        id="greAnalyticalScore_{$applicationId}_{$greScoreId}" value="{$greAnalyticalScore}" />
    </td>
    <td>
    <input type="text" size="3" class="gre_general_analytical_percentile" 
        id="greAnalyticalPercentile_{$applicationId}_{$greScoreId}" value="{$greAnalyticalPercentile}" />
    </td>
</tr>
<tr>
    <td>Writing</td>
    <td>
    <input type="text" size="3" class="gre_general_writing_score" 
        id="greWritingScore_{$applicationId}_{$greScoreId}" value="{$greWritingScore}" />
    </td>
    <td>
    <input type="text" size="3" class="gre_general_writing_percentile" 
        id="greWritingPercentile_{$applicationId}_{$greScoreId}" value="{$greWritingPercentile}" />
    </td>
</tr>
<tr>
    <td colspan="3" align="left">
    <input type="submit" class="submitSmall updateGreScore" 
        id="updateGreScore_{$applicationId}_{$greScoreId}" value="Update GRE Score"/>
    </td>
</tr>
</table> 
</form>

<script type="text/javascript">
        
</script>

<script type="text/javascript">
    $('#greReceivedMessage_{$applicationId}').html('{$receivedMessage}');
    $('#greReceivedMessage_{$applicationId}').prev('span').attr('class', '{$receivedClass}');
</script>

EOB;
?>