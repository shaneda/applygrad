<?php
/*
* "Private" functions 
*/
function normalize($string) 
{
    return strtolower(trim($string));
};

function isDomain($domainId, $domainNames)
{    
    $domainNames = array_map('normalize', $domainNames);
  
    $query = "SELECT domain.id 
        FROM domain 
        WHERE LOWER(TRIM(domain.name)) 
        IN ('" . implode("','", $domainNames) . "')
        AND domain.id = " . intval($domainId);  
    
    $result = mysql_query($query);        
    
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;    
}

$scsDomains = array(
    'SCS',
    'RI'
);

$riFifthYearMastersDomains = array(
    'RI-5YR'
);

$isreeDomains = array(
    'Advanced-Big-Data',
    'Advanced-Enterprise-Architecture',
    'Applied-Data-Analytics',
    'Architectures-for-Mergers',
    'Big-Data-Systems',
    'Capstone-Project',
    'Challenge-Exam-EAF',
    'COTS-Based-Integration',
    'Cybersecurity-Architectures',
    'Cybersecurity-Fundamentals',
    'EA-Security',
    'Enterprise-Architecture-Fundamentals',
    'Leadership-Cyber-Enabled-World',
    'Managing-Software-Outsourcing',
    'Principles-of-Architecture-Design',
    'Requirements-Engineering',
    'Security-for-Software-Engineers',
    'Software-Architecture',
    'Software-Project-Management',
    'Systems-and-Software-Security',
    'Systems-Integration' 
);

$mseMsitDomains = array(
    'MITS',
    'MSE-Campus',
    'MSE-Dist',
    'MSE-MSIT-Korea',
    'MSE-Portugal',
    'MSE-SEM',
    'MSIT-eBiz',
    'MSIT-eBiz-US',
    'MSIT-GM-Dist-INTL',
    'MSIT-GM-Dist-NATL',
    'MSIT-ITSM',
    'MSIT-SASE',
    'MSIT-SE-Russia',
    'MSIT-SRM'
);

$mseMsitEbizDomains = array(
    'MSIT-eBiz',
    'MSIT-eBiz-US'
);

$mitsDomains = array(
    'MITS'
);

$mseEseDomains = array(
    'MSIT-ESE'
);

$cnbcDomains = array(
//    'CNBC',
    'CNBC-Grad-Training'
);

$statisticsDomains = array(
    'Statistics',
    'Statistics-MS'
);

$mshciiDomains = array(
    'MS-HCII'
);

$msrtDomains = array(
    'RI-MS-RT-China',
    'RI-MS-RT-Dominican',
    'RI-MS-RT-Mexico',
    'RI-MS-RT-UK' 
);

$bicDomains = array(
    'BIC'
);

$privacyDomains = array(
    'MSIT-Privacy'
);

$metalsDomains = array(
    'METALS',
    'MS-LSE'
);

$dietrichDomains = array(
    'English-MA',
    'History',
    'Philosophy',
    'Psychology',
    'Social-and-Decision-Sciences',
    'English-PhD',
    'Modern-Languages',
    'ModLang-MA',
    'ModLang-PhD'
);

$englishDomains = array(
    'English-MA',
    'English-PhD'
);

$historyDomains = array(
    'History'
);

$psychologyDomains = array(
    'Psychology'
);

$philosophyDomains = array(
    'Philosophy',
    'Philosophy-Summer'
);

$modernLanguagesDomains = array(
    'Modern-Languages',
    'ModLang-MA',
    'ModLang-PhD'
);

$modernLanguagesPhdDomains = array(
    'ModLang-PhD'
);


$modernLanguagesMaDomains = array(
    'ModLang-MA'
);

$sdsDomains = array(
    'Social-and-Decision-Sciences'
);

$iniDomains = array(
    'INI',
    'INI-Kobe' 
);

$iniKobeDomains = array(
    'INI-Kobe' 
);

$designDomains = array(
    'Design'
);

$designPhdDomains = array(
    'Design-PhD',
    'Design-DDes'
);

$riMsRtChinaDomains = array(
    'RI-MS-RT-China'
);

$EM2Domains = array(
    'EM2'
);

$REUSEDomains = array(
   'REU-SE' 
);

$EngineeringDomains = array(
   'CMU-Engineering-Workshop' 
);
?>