<?php

include_once "mhci_prereqsCourseTypes.config.php";

class MHCI_PrereqsCourseForm
{

    // Constructor parameters.
    private $studentLuUsersUsertypesId;
    private $view;                          // "student" || "reviewer"   
    private $courseType;                    // "Design" || "Programming" || 
                                            // "Multi-way ANOVA"  || "Single-way ANOVA"  || 
                                            // "Multi-factor regression" || "Single-factor regression" 
    private $formName;                      // Must be unique, usually the same as courseType; will be 
                                            // converted into the form element prefix
    
    // Fields set in constructor
    private $formElementPrefix;             // str_replace(" ", "", $formName);
    private $renderUploadNote;              // TRUE || FALSE
    private $documentType;                  // str_replace(" ", "", $courseType) . "Course"
    private $datafileinfoSectionCode;       // Section code for the courseType in the datafileinfo table
    
    // Fields for form/db data.
    private $courseId;
    private $courseName;
    private $courseTime;
    private $courseInstitution;
    private $courseGrade;
    private $courseSubmittedToReviewer;
    private $placeoutPeriodId;
    private $applicationId;
    
    private $newUploadCourseDatafileId;
    private $newUploadDatafileinfoId;
    private $newUploadNote;
    private $submittedUploadInfo = array();         // 2D array filled from db query.
    private $newUploadInfo = array();               // 2D array filled from db query.
    
    // Fields for handling display.
    private $formSubmitted = FALSE;   
    private $formHeading;                           // set with setHeading() 
    private $formIntro;                             // set with setIntro()
    private $courseNameLabel = "Course name and/or number";
    private $courseTimeLabel = "When taken (or to be taken)";
    private $courseInstitutionLabel = "Institution";
    private $courseGradeLabel = "Grade";
    private $uploadFirstDocLabel = "Upload syllabus or other course document";
    private $uploadAnotherDocLabel = "Upload a different document";
    private $uploadButtonLabel;
    private $docNoteLabelIntro = "Where does this document show that your course covered";
    private $submitLabel = "Submit to the reviewer";    
    private $uploadButtonDisabled = "";
    private $submitButtonDisabled = "disabled";     // Enabling is handled with renderSubmitButton() and javascript.

       
    public function __construct($studentLuUsersUsertypesId, $view, $courseType, $formName) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;
        $this->courseType = $courseType;
        $this->formName = $formName;
        $this->formElementPrefix = str_replace(" ", "", $formName);
        $this->documentType = str_replace(" ", "", $courseType) . "Course";
        $this->applicationId = $_SESSION['application_id'];
        $this->placeoutPeriodId = $_SESSION['activePlaceoutPeriod'];
        
        global $courseTypes;  // from mhci_prereqsCourseTypes.config.php
        $this->datafileinfoSectionCode = $courseTypes[$courseType]['datafileinfoSectionCode'];
        $this->renderUploadNote = $courseTypes[$courseType]['renderUploadNote'];         
    }
    
    public function getFormSubmitted () {
        return $this->formSubmitted;
    }
    
    public function setFormSubmitted ($value) {
        $this->formSubmitted = $value;
        return $this->formSubmitted;
    }
    
    public function setplaceoutPeriodId ($value) {
        $this->placeoutPeriodId = $value;
        return $this->placeoutPeriodId;
    }
    
    public function setApplicationId ($value) {
        $this->applicationId = $value;
        return $this->applicationId;
    }
    
    public function getNewUploadInfoFromObject() {
        return $this->newUploadCourseDatafileId;
    } 
    
    public function getSubmittedUploadNote () {
        $latestUpload = end($this->submittedUploadInfo);
        return $latestUpload['note'];
    }
    
    public function getNewUploadNote () {
        $latestUpload = end($this->newUploadInfo);
        return $latestUpload['note'];
    }
    
    public function getSubmittedUploadID () {
        $latestUpload = end($this->submittedUploadInfo);
        return $latestUpload['id'];
    }
    
    public function getNewUploadID () {
        $latestUpload = end($this->newUploadInfo);
        return $latestUpload['id'];
    }
    
    public function getCourseName() {
        return $this->courseName;
    }
    
    public function getCourseSubmittedToReviewer() {
        return $this->courseSubmittedToReviewer;
    }
    
    public function render($disable) {
        // DebugBreak();
        $this->renderHeading(); 

        // IF REVIEWER view, get the data and return a summary.
        if ($this->view == "reviewer" || $disable == 'bogus') {
            
            $this->getCourseData();
            // DebugBreak();
            if ($this->courseId) {
                
                $this->renderCourseDataSummary();
                $this->getSubmittedUploadInfo();
                $this->renderSubmittedUploadSummary(); 
                
            }
            
            return TRUE;
            
        }
        
        // OTHERWISE, render the STUDENT view.
     //    DebugBreak();
        $this->renderIntro();
      //  DebugBreak();
       $this->getCourseData();
   //     DebugBreak();
        if (isset($this->courseName) && $this->courseName != "") {
         //   $this->getCourseData();
            $this->getSubmittedUploadInfo();
            $this->getNewUploadInfo();
        }
        // Get request data.
        $this->handleRequest();
// DAS removed
/*        if (addCourseData {
            $this->setFormSubmitted(TRUE);
        }
        */
        // IF the form has been SUBMITTED
//        DebugBreak();
        if ($this->formSubmitted  || isset($_REQUEST['TestSave']) || isset($_REQUEST['TestSubmit'])) {
            // echo "submission complete<br />";
        //    DebugBreak();
            // Insert/update the course data.
            if (isset($_REQUEST['TestSave']) && (isset($this->courseName) && $this->courseName != "")) {
                $this->courseId = $this->addCourseData(FALSE);
             //   $this->newUploadCourseDatafileId = $this->addUploadInfo(FALSE);
            } else {
                if ($this->formSubmitted || isset($_REQUEST['TestSubmit']))  {
                   $this->courseId = $this->addCourseData(TRUE);
             //      $this->newUploadCourseDatafileId = $this->addUploadInfo(TRUE);
                }
            }
           // DebugBreak();
            // Insert/update the latest upload data into db.
                if (isset($_REQUEST['TestSave'])) {
                    $this->newUploadCourseDatafileId = $this->addUploadInfo(FALSE);
                } else {
                     $this->newUploadCourseDatafileId = $this->addUploadInfo(TRUE);
                }
 /*           
            if ($this->formSubmitted || isset($_REQUEST['TestSubmit'])) {
                // Render the course data summary.
                $this->renderCourseDataSummary();
            }
            */
            
//            if (isset($_REQUEST['TestSave'])) {
                $this->renderCourseDataEdit();
 //           }
//             DebugBreak();
            // Get the upload info.
            $this->getSubmittedUploadInfo();
 //                   DebugBreak();
            // Render the upload button and upload summary.
            if (count($this->newUploadInfo) > 0) {
                $this->uploadButtonLabel = $this->uploadAnotherDocLabel;
            } else {
                $this->uploadButtonLabel = $this->uploadFirstDocLabel;
            }
//            DebugBreak 
            if ($this->getCourseName() == ""  //   || isset($_REQUEST['TestSubmit'])
            ) {
                $this->uploadButtonDisabled = 'disabled="true"';
            }
                $this->renderUploadButton();
//                DebugBreak();
/*                if ($this->getCourseSubmittedToReviewer() == 1) {
                    $this->renderSubmittedUploadSummary();
                } else {
                */
                    /// DALE check this
                    $this->renderNewUpload();
      //          }
                $testNew = count($this->newUploadInfo);
                $testSubmit = count($this->submittedUploadInfo);
//                DebugBreak();
               if ($this->renderUploadNote && (($testNew > 0) || ($testSubmit > 0))) {
          //          if ($this->courseSubmittedToReviewer == 1) {
          //                $this->renderUploadNoteSummary();
          //          } else {

                        $this->renderUploadNoteEdit();                        
         //           }
                }
            
            // DAS removed
            // Render the submit button.
      //      $this->renderSubmitButton();
            
            return TRUE;
        
        }
            
        // OTHERWISE, check whether it's a post-upload request.
        
        // Check DB for course and upload data.
        // DAS Can this be killed
    //    $this->getCourseData();
     //   $this->getSubmittedUploadInfo();
     //   $this->getNewUploadInfo();
  //       DebugBreak();
        // Get the last upload record.
        $lastUploadInfo = end($this->newUploadInfo);
        
        if ( ($lastUploadInfo['new_file_uploaded'] == 1) && ($lastUploadInfo['prereq_courses_id'] == $this->courseId) ) {
            
            // A new file has been uploaded.
            echo "new file has been uploaded<br /><br />";
            
            // Set the new upload fields.
            $this->newUploadCourseDatafileId = $lastUploadInfo['prereq_course_datafile_id'];
            $this->newUploadDatafileinfoId = $lastUploadInfo['id'];
            $this->newUploadNote = $lastUploadInfo['note'];
            
 //           if (!$this->courseSubmittedToReviewer) {
                
                // The course data have not been submitted yet.
                
                // Render the course data for editing.
                $this->renderCourseDataEdit();
 //                DebugBreak();
                // Set the upload button label for first upload.
                $this->uploadButtonLabel = $this->uploadFirstDocLabel; 
                
                
      /*      } else {
                
                // The course data have been submitted previously.
                
                // Render the course data summary.
                $this->renderCourseDataSummary();
                 // DebugBreak();
                // Set the upload button label for additional upload.
                $this->uploadButtonLabel = $this->uploadAnotherDocLabel;
                
            }   
            */             
            
            // Render the upload button and summary.
            // debugbreak();
  //          $this->uploadButtonDisabled = 'disabled="true"';
            $this->renderUploadButton();
            $this->renderSubmittedUploadSummary();
             
            $this->renderNewUpload(); 
            // DebugBreak();
            if ($this->renderUploadNote && ((isset($this->courseName) && $this->courseName != "")) && (count($this->newUploadInfo) > 0)) {
                    
                // Render the upload note for editing.
                $this->renderUploadNoteEdit();
                    
            }
             
            
        } else {
            
//             DebugBreak();
            // Check DB for course data.
            // DAS removed
            // $this->getCourseData();
                  //  DALE check this**********************  does this just mean saved or submitted
            if ($this->courseSubmittedToReviewer) {
                
                // The form subset was submitted previously and is complete for the time being.
               // echo "returning to course<br /><br />";
                
                // Render the course data summary.
                $this->renderCourseDataSummary();
                $this->uploadButtonDisabled = 'disabled="true"';
                // Render the upload button and summary. 
                $this->uploadButtonLabel = $this->uploadAnotherDocLabel;
           //     DebugBreak();
                $this->renderUploadButton();
                $this->renderSubmittedUploadSummary();
                if ($this->renderUploadNote) {
                $this->renderUploadNoteEdit();
            }
                
            } else {
                
                // This form subset has not been started.
                // echo "new course<br /><br />";
                
                // Render the course data for editing.
                $this->renderCourseDataEdit();
                 if (!isset($this->courseName) || $this->courseName == "") {
                     $this->uploadButtonDisabled = "disabled = true";
                 }
                // Render the upload button and summary.
                $this->uploadButtonLabel = $this->uploadFirstDocLabel;
   //                 DebugBreak();
                $this->renderUploadButton();
                $this->renderSubmittedUploadSummary();
                
                $this->renderNewUpload();
                
                if ($this->renderUploadNote && ((isset($this->courseName) && $this->courseName != "")) && (count($this->newUploadInfo) > 0)) {
                     // DebugBreak();
                    // Render the upload note for editing.
                    $this->renderUploadNoteEdit();
                    
                }
                   
            }     
            
        }
           

        // Render the submit button.
      //  $this->renderSubmitButton();
        
        return TRUE;  
        
    }
    
    
    private function setUploadTypeId() {
        
        switch ($this->courseType) {
        
            case "Multi-way ANOVA":
                $this->uploadTypeId = 11;
                $uploadId = 1;
                break;
            case "Single-way ANOVA":
                $this->uploadTypeId = 12;
                $uploadId = 1;
                break;
            case "Multi-factor regression":
                $this->uploadTypeId = 13;
                $uploadId = 1;
                break;
            case "Single-factor regression":
                $this->uploadTypeId = 14;
                $uploadId = 1;
                break;
            case "Design":
                $this->uploadTypeId = 15;
                $uploadId = 1;
                break;  
            case "Programming":
                $this->uploadTypeId = 16;
                $uploadId = 1;
                break;   

        }
        
        
    }
    
     public function getCourseId() {
            
           return $this->courseId;       
    }
    
    public function setCourseId($courseId) {
        
        if ($courseId) {
            
            $this->courseId = $courseId;   
            
        }    
        
    }
    
    public function setCourseName($name) {
            
            $this->courseName = $name;   
            
        }    
        
    public function setCourseTime($time) {
            
            $this->courseTime = $time;   
            
        } 
        
    public function setCourseInstitution($name) {
            
            $this->courseInstitution = $name;   
            
        }
        
    public function setCourseGrade($grade) {
            
            $this->CourseGrade = $grade;   
            
        }
    
    

    public function setHeading($headingString="") {
    
        if ($headingString) {
            
            $this->formHeading = $headingString;
            
        } else {
            
            $this->formHeading = ucfirst($this->courseType);
            
        }
        
        return TRUE;
        
    }
    
    
    public function setIntro($introString="") {
        
        $this->formIntro = $introString;
        
        return TRUE;
        
    }
    
    
    private function handleRequest() {
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_courseId'])) {
            
            $this->courseId = $_REQUEST[$this->formElementPrefix . '_courseId'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_courseName'])) {
            
            $this->courseName = $_REQUEST[$this->formElementPrefix . '_courseName'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_courseTime'])) {
            
            $this->courseTime = $_REQUEST[$this->formElementPrefix . '_courseTime'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_courseInstitution'])) {
            
            $this->courseInstitution = $_REQUEST[$this->formElementPrefix . '_courseInstitution']; 
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_courseGrade'])) {
            
            $this->courseGrade = $_REQUEST[$this->formElementPrefix . '_courseGrade'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_newCourseDatafileId'])) {
            
            $this->newUploadCourseDatafileId = $_REQUEST[$this->formElementPrefix . '_newCourseDatafileId'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_newDatafileinfoId'])) {
            
            $this->newUploadDatafileinfoId = $_REQUEST[$this->formElementPrefix . '_newDatafileinfoId'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_keywordTextarea'])) {
            
            $this->newUploadNote = $_REQUEST[$this->formElementPrefix . '_keywordTextarea'];
            
        }
        
        if ( isset($_REQUEST[$this->formElementPrefix . '_courseSubmit'])) {
            
            $this->formSubmitted = TRUE;
            
        }
        
        return TRUE;
        
    }
    
    
    private function getCourseData() {
        
        $query = "SELECT * FROM mhci_prereqsCourses";
        $query .= " WHERE student_lu_users_usertypes_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND course_type = '{$this->courseType}' AND application_id = " . $_SESSION['application_id'];
        $query .= " AND period_id = " . $_SESSION['activePlaceoutPeriod']; 
        // THIS IS A KLUGE to deal with calls from the course form controller class
        // when there is more than one course for the course type.
        if ($this->courseId) {
            if ($this->courseId < 0) {
                // debugBreak();
                $query .= " AND id NOT IN (SELECT prereq_courses_id FROM mhci_prereqsCourseDatafiles)";    
            } else {
                $query .= " AND id = '{$this->courseId}'";
            }
        }
        //echo $query . "<br />";
         //   DebugBreak();
        $result = mysql_query($query);
        if ($this->courseId > 0 || $this->courseType == "Multi-way ANOVA" || $this->courseType == "Multi-factor regression"
            || $this->courseType == "Single-way ANOVA" || $this->courseType == "Single-factor regression")  {
            while ($row = mysql_fetch_array($result)) {
                
                $this->courseId = $row['id'];
                $this->courseName = $row['student_course_name'];
                $this->courseTime = $row['student_course_time'];
                $this->courseInstitution = $row['student_course_institution'];
                $this->courseGrade = $row['student_course_grade'];
                $this->courseSubmittedToReviewer = $row['submitted_to_reviewer'];
            }  
        }  
        
    }


    private function getSubmittedUploadInfo() {
    
        $query = "SELECT datafileinfo.*, lu_users_usertypes.user_id AS usermasterid, 
                    mhci_prereqsCourseDatafiles.id AS prereq_course_datafile_id, 
                    mhci_prereqsCourseDatafiles.prereq_courses_id, mhci_prereqsCourseDatafiles.note, 
                    mhci_prereqsCourseDatafiles.submitted_to_reviewer, mhci_prereqsCourseDatafiles.new_file_uploaded
                    FROM datafileinfo 
                    INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
                    LEFT OUTER JOIN mhci_prereqsCourseDatafiles
                    ON datafileinfo.id = mhci_prereqsCourseDatafiles.datafileinfo_id";
        $query .= " WHERE datafileinfo.user_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND datafileinfo.section = '{$this->datafileinfoSectionCode}'";
        $query .= " AND mhci_prereqsCourseDatafiles.prereq_courses_id = '{$this->courseId}'";
        $query .= " AND mhci_prereqsCourseDatafiles.submitted_to_reviewer = 1
                    ORDER BY datafileinfo.id";
        //echo $query . "<br/>";
        
        $result = mysql_query($query);   
        
        while ($row = mysql_fetch_array($result)) {
            
            $this->submittedUploadInfo[] = $row;
            
        }
        
    }
    
    
    private function getNewUploadInfo() {
        $query = "SELECT datafileinfo.*, lu_users_usertypes.user_id AS usermasterid,
                    mhci_prereqsCourseDatafiles.id AS prereq_course_datafile_id,  
                    mhci_prereqsCourseDatafiles.prereq_courses_id, mhci_prereqsCourseDatafiles.note, 
                    mhci_prereqsCourseDatafiles.submitted_to_reviewer, mhci_prereqsCourseDatafiles.new_file_uploaded
                    FROM datafileinfo 
                    INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
                    LEFT OUTER JOIN mhci_prereqsCourseDatafiles
                    ON datafileinfo.id = mhci_prereqsCourseDatafiles.datafileinfo_id";
        $query .= " WHERE datafileinfo.user_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND datafileinfo.section = '{$this->datafileinfoSectionCode}'";
        $query .= " AND mhci_prereqsCourseDatafiles.prereq_courses_id = '{$this->courseId}'";    
        $query .= " AND mhci_prereqsCourseDatafiles.new_file_uploaded = 1
                    ORDER BY datafileinfo.id";
        //echo $query . "<br/>";
        
        $result = mysql_query($query);   
        
        while ($row = mysql_fetch_array($result)) {
            
            $this->newUploadInfo[] = $row;
            
        }
        
    }

    
    public function addCourseData($submitNotSave) {
        
        $query = "INSERT INTO mhci_prereqsCourses";
        
        if ($this->courseId && ($this->courseId != -1)) {
        
            $query .= " (id, student_lu_users_usertypes_id, course_type, 
                        student_course_name, student_course_time, 
                        student_course_institution, student_course_grade,
                        application_id, period_id)";
            if ($submitNotSave)  {
                $query .= sprintf(" VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', %d, %d)
                                ON DUPLICATE KEY UPDATE submitted_to_reviewer = 1,
                                student_course_name = '%s',
                                student_course_time = '%s',
                                student_course_institution = '%s',
                                student_course_grade = '%s'",
                                $this->courseId,
                                $this->studentLuUsersUsertypesId,
                                mysql_real_escape_string($this->courseType),
                                mysql_real_escape_string($this->courseName),
                                mysql_real_escape_string($this->courseTime),
                                mysql_real_escape_string($this->courseInstitution),
                                mysql_real_escape_string($this->courseGrade),
                                $this->applicationId,
                                $this->placeoutPeriodId,
                                mysql_real_escape_string($this->courseName),
                                mysql_real_escape_string($this->courseTime),
                                mysql_real_escape_string($this->courseInstitution),
                                mysql_real_escape_string($this->courseGrade)
                                ); 
            } else {
                $query .= sprintf(" VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', %d, %d)
                            ON DUPLICATE KEY UPDATE submitted_to_reviewer = 0,
                            student_course_name = '%s',
                            student_course_time = '%s',
                            student_course_institution = '%s',
                            student_course_grade = '%s'",
                            $this->courseId,
                            $this->studentLuUsersUsertypesId,
                            mysql_real_escape_string($this->courseType),
                            mysql_real_escape_string($this->courseName),
                            mysql_real_escape_string($this->courseTime),
                            mysql_real_escape_string($this->courseInstitution),
                            mysql_real_escape_string($this->courseGrade),
                            $this->applicationId,
                            $this->placeoutPeriodId,
                            mysql_real_escape_string($this->courseName),
                            mysql_real_escape_string($this->courseTime),
                            mysql_real_escape_string($this->courseInstitution),
                            mysql_real_escape_string($this->courseGrade)
                            );
            }
            // DebugBreak();               
            $result = mysql_query($query);
            
            $courseId = $this->courseId;
        
        } else {

            $query .= " (student_lu_users_usertypes_id, course_type, student_course_name, 
                        student_course_time, student_course_institution, student_course_grade,
                        application_id, period_id)";
            $query .= sprintf(" VALUES (%d, '%s', '%s', '%s', '%s', '%s', %d, %d)",
                            $this->studentLuUsersUsertypesId,
                            mysql_real_escape_string($this->courseType),
                            mysql_real_escape_string($this->courseName),
                            mysql_real_escape_string($this->courseTime),
                            mysql_real_escape_string($this->courseInstitution),
                            mysql_real_escape_string($this->courseGrade),
                            $this->applicationId,
                            $this->placeoutPeriodId
                            ); 
 //           DebugBreak();                
            $result = mysql_query($query);
            
            $courseId = mysql_insert_id();
            
        }
        //echo $query . "<br />";
        
        return $courseId;
        
    }
    
    
    private function addUploadInfo($submitNotSave) {
        $query = "INSERT INTO mhci_prereqsCourseDatafiles ";
        if ($this->newUploadCourseDatafileId) {
            
            $query .= " (id, prereq_courses_id, datafileinfo_id, note)";
            if ($submitNotSave) {
                $query .= sprintf(" VALUES (%d, %d, %d, '%s')
                    ON DUPLICATE KEY UPDATE submitted_to_reviewer = 1,
                    new_file_uploaded = 0,
                    note = '%s'",
                    $this->newUploadCourseDatafileId,
                    $this->courseId,
                    $this->newUploadDatafileinfoId,
                    mysql_real_escape_string($this->newUploadNote),
                    mysql_real_escape_string($this->newUploadNote)
                    );
            } else {
                    $query .= sprintf(" VALUES (%d, %d, %d, '%s')
                        ON DUPLICATE KEY UPDATE submitted_to_reviewer = 0,
                        new_file_uploaded = 1,
                        note = '%s'",
                        $this->newUploadCourseDatafileId,
                        $this->courseId,
                        $this->newUploadDatafileinfoId,
                        mysql_real_escape_string($this->newUploadNote),
                        mysql_real_escape_string($this->newUploadNote)
                        );
            }
        
            $result = mysql_query($query);
            
            $courseDatafileId = $this->newUploadCourseDatafileId;
            
        } else {
            
            $query .= " (prereq_courses_id, datafileinfo_id, note)"; 
            $query .= sprintf(" VALUES (%d, %d, '%s')",
                    $this->courseId,
                    $this->newUploadDatafileinfoId,
                    mysql_real_escape_string($this->newUploadNote)
                    );
        
            $result = mysql_query($query);
            
            $courseDatafileId = mysql_insert_id();
        }
        //echo $query . "<br />"; 
        
        if ($result) {
            
            global $courseTypes;
            $linkname = "mhci_prereqs_" . $courseTypes[$this->courseType]['prereqType'] . ".php";
            updateReqComplete($linkname, 0);    
            
        }
        
        return $courseDatafileId;        
           
    }
    

    private function renderHeading() {
        
        echo '<div class="sectionTitle">' . $this->formHeading . '</div><br/><br/>'; 

    }
    
    
    private function renderIntro() {
        
        echo '<div class="bodyText">' . $this->formIntro . '</div><br/>'; 

    }
    
    
    private function renderCourseDataEdit() {
        echo <<<EOB
        <input type="hidden" id="{$this->formElementPrefix}_periodId" name="{$this->formElementPrefix}_periodId" value="{$this->placeoutPeriodId}" />
        <input type="hidden" id="{$this->formElementPrefix}_applicationId" name="{$this->formElementPrefix}_applicationId" value="{$this->applicationId}" />
        <input type="hidden" id="{$this->formElementPrefix}_studentId" name="{$this->formElementPrefix}_studentId" value="{$this->studentLuUsersUsertypesId}" />
        <input type="hidden" id="{$this->formElementPrefix}_courseId" name="{$this->formElementPrefix}_courseId" value="{$this->courseId}" />
        <input type="hidden" id="{$this->formElementPrefix}_courseType" name="{$this->formElementPrefix}_courseType" value="{$this->courseType}" />
        <div id="{$this->formElementPrefix}_courseNameLabel" class="bodyText courseNameLabel">
            <strong>{$this->courseNameLabel}</strong><br/>
            (as written on your transcript)<br/>
            <input id="{$this->formElementPrefix}_courseNameInput" name="{$this->formElementPrefix}_courseName" class="textInput-short courseNameInput" type="text" value="{$this->courseName}" />
        </div>
        <div id="{$this->formElementPrefix}_courseTimeLabel" class="bodyText courseTimeLabel">
            <strong>{$this->courseTimeLabel}</strong><br/>
            (as written on your transcript)<br/>
            <input id="{$this->formElementPrefix}_courseTimeInput" name="{$this->formElementPrefix}_courseTime" class="textInput-short courseTimeInput" type="text" value="{$this->courseTime}" />
        </div>
        <div id="{$this->formElementPrefix}_courseInstitutionLabel" class="bodyText courseInstitutionLabel">
            <strong>{$this->courseInstitutionLabel}</strong><br/>
            (e.g. Berkeley)<br/>
            <input id="{$this->formElementPrefix}_courseInstitutionInput" name="{$this->formElementPrefix}_courseInstitution" class="textInput-short courseInstitutionInput" type="text" value="{$this->courseInstitution}" />
        </div>
        <div id="{$this->formElementPrefix}_courseGradeLabel" class="bodyText">
            <strong>{$this->courseGradeLabel}</strong><br/>
            (e.g. A)<br/>
            <input id="{$this->formElementPrefix}_courseGradeInput" name="{$this->formElementPrefix}_courseGrade" class="textInput-xshort" type="text" value="{$this->courseGrade}" />
        </div>
        <br/><br/><br/>

EOB;
        
    }
    
    
    private function renderCourseDataSummary() {
        
        echo <<<EOB

        <input type="hidden" id="{$this->formElementPrefix}_studentId" name="{$this->formElementPrefix}_studentId" value="{$this->studentLuUsersUsertypesId}" />
        <input type="hidden" id="{$this->formElementPrefix}_courseId" name="{$this->formElementPrefix}_courseId" value="{$this->courseId}" />
        <input type="hidden" id="{$this->formElementPrefix}_courseType" name="{$this->formElementPrefix}_courseType" value="{$this->courseType}" />
        <div id="{$this->formElementPrefix}_courseNameLabel" class="bodyText courseNameLabel">
            <strong>{$this->courseNameLabel}</strong><br/>
            {$this->courseName}
            <input id="{$this->formElementPrefix}_courseNameInput" name="{$this->formElementPrefix}_courseName" class="requiredData" type="hidden" value="{$this->courseName}"/>
        </div>
        <div id="{$this->formElementPrefix}_courseTimeLabel" class="bodyText courseTimeLabel">
            <strong>{$this->courseTimeLabel}</strong><br/>
            {$this->courseTime}
            <input id="{$this->formElementPrefix}_courseTimeInput" name="{$this->formElementPrefix}_courseTime" class="requiredData" type="hidden" value="{$this->courseTime}"/>
        </div>
        <div id="{$this->formElementPrefix}_courseInstitutionLabel" class="bodyText courseInstitutionLabel">
            <strong>{$this->courseInstitutionLabel}</strong><br/>
            {$this->courseInstitution}
            <input id="{$this->formElementPrefix}_courseInstitutionInput" name="{$this->formElementPrefix}_courseInstitution" class="requiredData" type="hidden" value="{$this->courseInstitution}"/>
        </div>
        <div id="{$this->formElementPrefix}_courseGradeLabel" class="bodyText">
            <strong>{$this->courseGradeLabel}</strong><br/>
            {$this->courseGrade}
            <input id="{$this->formElementPrefix}_courseGradeInput" name="{$this->formElementPrefix}_courseGrade" type="hidden" value="{$this->courseGrade}"/>
        </div>
        <br/><br/>  
    
EOB;
        
    }
    
    
    private function renderUploadButton() {
        // <button id="{$this->formElementPrefix}_uploadDoc" class="bodyButton docUploadButton" onClick="uploadCourseDocument(this);return false;" {$this->uploadButtonDisabled} >{$this->uploadButtonLabel}</button>
        echo <<<EOB
        <button id="{$this->formElementPrefix}_uploadDoc" class="bodyButton docUploadButton" onClick="uploadCourseDocument(this);return false;" {$this->uploadButtonDisabled} />{$this->uploadButtonLabel}</button> 
        <br/><br/> 

EOB;
        
    }
    
    
    private function renderUpload($uploadArray) {
        
        $studentGuid = getGuid($uploadArray['usermasterid']);
        $pathFilename = $this->documentType . "_" . $uploadArray['userdata'] . "." . $uploadArray['extension'];
        $displayFilename = $this->documentType . "." . $uploadArray['extension'];
        //$path = $_SESSION['datafileroot'] . "/" . getGuid($_SESSION['usermasterid']) . "/" . $this->documentType . "/" . $pathFilename;
        $path = $_SESSION['datafileroot'] . "/" . $studentGuid . "/" . $this->documentType . "/" . $pathFilename;
        if (isset($_REQUEST['role']) && $_REQUEST['role'] == "reviewer") {   
echo <<<EOB

        <div class="bodyText-bold fileFirstLine">File Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Size&nbsp;&nbsp;&nbsp;Last Modified</div>
        <div class="bodyText fileSecondLine"><a href="" onClick="window.open('{$path}'); return false;">{$displayFilename}</a> &nbsp;&nbsp;&nbsp;{$uploadArray['size']}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$uploadArray['moddate']}</div>
        <input type="hidden" id="{$this->formElementPrefix}_courseDatafileId" value="{$uploadArray['prereq_course_datafile_id']}" />
        <br/>

EOB;
        }   else {
        echo <<<EOB

        <div class="bodyText-bold fileFirstLine">File Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Size&nbsp;&nbsp;&nbsp;Last Modified</div>
        <div class="bodyText fileSecondLine"><a href="" onClick="window.open('{$path}'); return false;">{$displayFilename}</a> &nbsp;&nbsp;&nbsp;{$uploadArray['size']}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$uploadArray['moddate']}</div>
        <div class="bodyText fileThirdLine">(Please review your uploaded file by clicking the link above.)</div>
        <input type="hidden" id="{$this->formElementPrefix}_courseDatafileId" value="{$uploadArray['prereq_course_datafile_id']}" />
        <br/>

EOB;
        }

        if ( isset($uploadArray['note']) && ($uploadArray['note'] != "") && $uploadArray['submitted_to_reviewer']) {
            echo "<strong>" . $this->courseType . "</strong>: " . $uploadArray['note'];
        }
        
        echo "<br/><br/>";
        
    }
    
    
    private function renderSubmittedUploadSummary() {
        global $assessmentForm;
        //  DebugBreak();
        if (isset ($assessmentForm) || $this->courseType == "Multi-way ANOVA" || $this->courseType == "Multi-factor regression"
            || $this->courseType == "Single-way ANOVA" || $this->courseType == "Single-factor regression" ) {
              //  DebugBreak();
                if (isset($_REQUEST['role']) && ($_REQUEST['role'] == "reviewer" ) && sizeof($this->submittedUploadInfo) == 0 && isset($assessmentForm) && ($assessmentForm->getStudentAssessment() == "fulfilledTrue")) {
        echo <<<EOB
                <div class="bodyText fileThirdLine">No Document Uploaded</div>
                <br/>

EOB;
                }
                foreach ($this->submittedUploadInfo as $upload) {
                
                    $this->renderUpload($upload);

                }
                
            }
    }
    
    
    private function renderNewUpload() {
        //  DebugBreak();
        foreach ($this->newUploadInfo as $upload) {
            
                $this->renderUpload($upload);
                
        }
        
        echo <<<EOB
                
            <input type="hidden" id="{$this->formElementPrefix}_newCourseDatafileId" name="{$this->formElementPrefix}_newCourseDatafileId" class="requiredData" value="{$this->newUploadCourseDatafileId}" />
            <input type="hidden" id="{$this->formElementPrefix}_newDatafileinfoId" name="{$this->formElementPrefix}_newDatafileinfoId" class="requiredData" value="{$this->newUploadDatafileinfoId}" />

EOB;
    
    }
    
    
    
    private function renderUploadNoteEdit() {
        
        echo <<<EOB

        <div class="bodyText">{$this->docNoteLabelIntro} <strong>{$this->courseType}</strong>?</div>
        <textarea id="{$this->formElementPrefix}_keywordTextarea" name="{$this->formElementPrefix}_keywordTextarea" class="keywordArea requiredData">{$this->newUploadNote}</textarea><br/><br/>
    
EOB;
        
    }
    
    private function renderUploadNoteSummary() {
        /*
        echo <<<EOB

        <div class="bodyText">Where to look for for coverage of <strong>{$this->courseType}</strong> in uploaded document</div>
        <br />
        <div class="bodyText">{$this->newUploadNote}</div>
        <br/><br/>
    
EOB;
*/
        
    }
    
    
    private function renderSubmitButton() {
        
        // Enable the button if all required field values are filled in the REQUEST.
        // Activating/deactivating the button in other circumstances is handled by mhci_prereqs.js
        if ($this->newUploadDatafileinfoId && !$this->formSubmitted) {
       //     debugBreak();
            if ( !$this->renderUploadNote || ($this->renderUploadNote && $this->newUploadNote) ) {
                
                if ($this->courseName && $this->courseTime && $this->courseInstitution) {
                    
                    $this->submitButtonDisabled = "";    
                    
                }
                
            }
            
            
        }
        
        echo <<<EOB

        <input type="submit" id="{$this->formElementPrefix}_courseSubmit" name="{$this->formElementPrefix}_courseSubmit" class="bodyButton-right courseSubmit" value="{$this->submitLabel}" {$this->submitButtonDisabled}/>
        <br/><br/>

EOB;
        
    }

    
    private function handleSession() {
        
        $this->courseName = $_SESSION['courseName'];
        $this->courseTime = $_SESSION['courseTime'];
        $this->courseInstitution = $_SESSION['courseInstitution'];
        $this->courseGrade = $_SESSION['courseGrade'];
        $this->newUploadNote = $_SESSION['keywordTextarea'];
        $this->newUploadFormName = $_SESSION['newUploadFormName']; 
        
    } 
    
} 

?>
