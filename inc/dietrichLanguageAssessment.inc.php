<table border="0" cellpadding="2" cellspacing="2" class="tblItem">
<tr>
    <td>
    <?php
    echo "Language:  "; 
    showEditText($languageAssessment['language'], "textbox", 
        "language_assessment[" . $languageAssessment['id'] . "][language]", 
        $_SESSION['allow_edit'], true, null, true, 30); 
    ?>
    </td>
    <td colspan="5" align="right">
    <?php
    if (!$isModernLanguagesDomain || ($isModernLanguagesDomain && count($languageAssessments) > 1))
    {
    showEditText("Delete", "button", "deleteLanguageAssessment[" . $languageAssessment['id'] . "]", $_SESSION['allow_edit']);
    }
    ?>
    </td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td><strong>Fluent</strong></td>
  <td><strong>Somewhat Fluent</strong></td>
  <td><strong>Passable</strong></td>
  <td><strong>With Difficulty</strong></td>
</tr>

<?php
if (isModernLanguagesDomain($domainid)) 
{
?>
    <tr>
        <td class="tblItemRequired">Listening</td>
        <?php 
        for($i = 0; $i < 4; $i++)
        {
            $inputValue = $i + 1;
            $checked = $languageAssessment['listening'] == $inputValue ? 'checked' : ''; 
        ?>
            <td align="middle">
                <input name="language_assessment[<?php echo $languageAssessment['id'] ?>][listening]" 
                    type="radio" 
                    value="<?php echo $inputValue; ?>" 
                    <?php echo $checked; ?> />
            </td>
        <?php 
        }
        ?>
    </tr>
<?php
} 
?>

<tr class="tblItemAlt">
    <td class="tblItemRequired">Speaking</td>
    <?php 
    for($i = 0; $i < 4; $i++)
    {
        $inputValue = $i + 1;
        $checked = $languageAssessment['speaking'] == $inputValue ? 'checked' : ''; 
    ?>
    <td align="middle">
        <input name="language_assessment[<?php echo $languageAssessment['id'] ?>][speaking]" 
            type="radio" 
            value="<?php echo $inputValue; ?>" 
            <?php echo $checked; ?>  />
    </td>
    <?php 
    }
    ?>
</tr>

<tr>
    <td class="tblItemRequired">Reading</td> 
    <?php 
    for($i = 0; $i < 4; $i++)
    {
        $inputValue = $i + 1;
        $checked = $languageAssessment['reading'] == $inputValue ? 'checked' : ''; 
    ?>
    <td align="middle">
        <input name="language_assessment[<?php echo $languageAssessment['id'] ?>][reading]" 
            type="radio" 
            value="<?php echo $inputValue; ?>" 
            <?php echo $checked; ?>  />
    </td>
    <?php 
    }
    ?>
</tr>

<tr class="tblItemAlt">
    <td class="tblItemRequired">Writing</td>
    <?php 
    for($i = 0; $i < 4; $i++)
    {
        $inputValue = $i + 1;
        $checked = $languageAssessment['writing'] == $inputValue ? 'checked' : ''; 
    ?>
    <td align="middle">
        <input name="language_assessment[<?php echo $languageAssessment['id'] ?>][writing]" 
            type="radio" 
            value="<?php echo $inputValue; ?>" 
            <?php echo $checked; ?>  />
    </td>
    <?php 
    }
    ?>
  </tr>        
</table>

<?php
if (isModernLanguagesDomain($domainid)) 
{
?>
    <br/>
    Are you a native speaker?
    <br/>
    <?php
    $radioYesNo = array(
        array(1, "Yes"),
        array(0, "No")
    );
    showEditText($languageAssessment['native_speaker'], "radiogrouphoriz", 
        "language_assessment[" . $languageAssessment['id'] . "][native_speaker]", 
        $_SESSION['allow_edit'], false, $radioYesNo);
    ?>

    <br/>
    <br/>
    <span style="display: inline-block; width: 80px;">Years of Study</span>
    <?php
    showEditText($languageAssessment['years_study'], "textbox", 
        "language_assessment[" . $languageAssessment['id'] . "][years_study]", 
        $_SESSION['allow_edit'], TRUE, NULL, TRUE, 10);    
    ?>

    <br/>
    <br/>
    <span style="display: inline-block; width: 80px;">Level of Study</span>
    <?php
    $languageAssessmentStudyLevels = array(
        array(1, "High School"),
        array(2, "Undergraduate"),
        array(3, "Graduate")
    );
    showEditText($languageAssessment['study_level'], "listbox", 
        "language_assessment[" . $languageAssessment['id'] . "][study_level]", 
        $_SESSION['allow_edit'], true, $languageAssessmentStudyLevels);    
    ?>

    <br/>
    <br/>
    Please provide any evidence you may have which demonstrates competency/proficiency in this language. 
    These include standardized test scores, proficiency ratings, language honor society memberships, advanced 
    placement credit, overseas residency and/or study, etc.
    <br/>
<?php 
    showEditText($languageAssessment['competency_evidence'], "textarea", 
        "language_assessment[" . $languageAssessment['id'] . "][competency_evidence]", $_SESSION['allow_edit']);
}
?>