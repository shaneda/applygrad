<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

<?
/*
* Decision form data comes from the lu_application_programs table,
* not from the review table.  Any admin should be able to view and
* modify this data.
*/
for($x = 0; $x < count($myPrograms); $x++)
{
    $aDepts = split(",", $myPrograms[$x][14]);
    //FIRST CHECK IF THE PROGRAM BELONGS TO THIS DEPARTMENT
    for($j = 0; $j < count($aDepts); $j++)
    {
        if($aDepts[$j] == $thisDept)
        {
        //CHECK IF PROGRAM IS ELIGIBLE FOR ROUND2
        ?>
            
            <tr><td colspan="2">
            <hr />
            <strong>Admit Information for program:</strong>  
            <em><?=$myPrograms[$x][1] . " ".$myPrograms[$x][2] . " ".$myPrograms[$x][3]?></em>
            <br />&nbsp;
            </td></tr>

       <!--     <tr>
            <td width="50px"><b> Decision: </b> </td>
            <td>
            <? 
            $decision = $myPrograms[$x][10] ;
            showEditText($decision, "listbox", "decision_".$myPrograms[$x][0], $_SESSION['A_allow_admin_edit'], true, $decisionVals); 
            ?>
            </td>
            </tr>
            --->
            
            <tr>
            <td width="50px"><b> Comments: </b></td>
            <td>
            <?
            $comments = $myPrograms[$x][13]; 
            ob_start();
            showEditText($comments, "textarea", "comments_".$myPrograms[$x][0], $allowEdit, false, 60); 
            $commentsTextarea = ob_get_contents();
            ob_end_clean();
            echo str_replace("cols='60'", "cols='50'", $commentsTextarea); 
            ?>
            </td>
            </tr>

            <tr>
                <td width="50"><strong>Risk Factors:</strong></td>
                <td>
                <?php
                $risk_factor_language_checked = "";
                $risk_factor_experience_checked = "";
                $risk_factor_academic_checked = "";
                $risk_factor_other_checked = "";
                $risk_factor_text = "";
                $risk_factor_disabled = "";
                
                $mseDecisionProgramId = $myPrograms[$x][0];
                $mseRiskFactors = $db_risk_factors->getRiskFactorsDecision($appid, $mseDecisionProgramId);
                
                foreach ($mseRiskFactors as $risk_factor) {
                
                    if ($risk_factor['language'] == 1) {
                        $risk_factor_language_checked = "checked";
                    }
                    if ($risk_factor['experience'] == 1) {
                        $risk_factor_experience_checked = "checked";
                    }
                    if ($risk_factor['academic'] == 1) {
                        $risk_factor_academic_checked = "checked";
                    }
                    if ($risk_factor['other'] == 1) {
                        $risk_factor_other_checked = "checked";
                    }
                    if ($risk_factor['other_text']) {
                        $risk_factor_text = $risk_factor['other_text'];    
                    }   
                }
                
                if (!$allowEdit) {
                    $risk_factor_disabled = "disabled";
                }
                ?> 
                <input name="mse_decision" type="hidden" value="true" />    
                <input name="mse_decision_program_id" type="hidden" value="<?php echo $mseDecisionProgramId; ?>" />
                <input name="risk_factors_decision[]" type="checkbox" class="tblItem" 
                    value="language" <?= $risk_factor_language_checked ?> <?= $risk_factor_disabled ?> />Language
                <input name="risk_factors_decision[]" type="checkbox" class="tblItem" 
                    value="experience" <?= $risk_factor_experience_checked ?> <?= $risk_factor_disabled ?> />Experience
                <input name="risk_factors_decision[]" type="checkbox" class="tblItem" 
                    value="academic" <?= $risk_factor_academic_checked ?> <?= $risk_factor_disabled ?> />Academic
                <input name="risk_factors_decision[]" type="checkbox" class="tblItem" 
                    value="other" <?= $risk_factor_other_checked ?> <?= $risk_factor_disabled ?> />Other
                <div style="margin:5px;">
                Other Risk Factor(s):
                <?php
                showEditText($risk_factor_text, "textbox", "mse_risk_factor_text_decision",
                     $allowEdit, false, null, true, 25);
                ?>
                </div>
                </td>
            </tr>
            <?php
            if (isMseESE($thisDept)) {
                $mseBridgeCourses = $db_bridge_courses->getBridgeCourseDecision($appid, $mseDecisionProgramId);
                ?>
                 <tr>
                    <td width="50"><strong>Bridge Course:</strong></td>
                    <td>
                         <div class="mse_msit" id="ese_bridge_course">    
                            <?php
                            $bridge_course_nobridgecourse_checked = "";
                            $bridge_course_hw4sw_checked = "";
                            $bridge_course_cs4pe_checked = "";
                            $bridge_course_tbd_checked = "";
                            $bridge_course_disabled = '';
                            foreach ($mseBridgeCourses as $bridgeCourse) {
                                if ($bridgeCourse['course_id']== 0) {
                                    $bridge_course_nobridgecourse_checked = "checked";    
                                }
                                
                                if ($bridgeCourse['course_id'] == 1) {
                                    $bridge_course_hw4sw_checked = "checked";
                                }
                                if ($bridgeCourse['course_id'] == 2) {
                                    $bridge_course_cs4pe_checked = "checked";
                                }
                                if ($bridgeCourse['course_id'] == 3) {
                                    $bridge_course_tbd_checked = "checked";
                                }
                            }
                            
                          // DAS  $bridge_course = '';
                            if (!$allowEdit) {
                                $bridge_course_disabled = "disabled";
                            }
                            
                            // class="tblItem interviewType"  removed from below
                            ?>
                            <input name="bridge_course_decision" id="bridge_course_hw4sw" type="radio"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         bridge_course_decision" id="bridge_course_none" type="radio"  
                                value="0" <?= $bridge_course_nobridgecourse_checked ?> <?= $bridge_course_disabled ?> />No bridge course Needed
                            <input name="bridge_course_decision" id="bridge_course_hw4sw" type="radio" 
                                value="1" <?= $bridge_course_hw4sw_checked ?> <?= $bridge_course_disabled ?> />HW4SW
                            <input name="bridge_course_decision" id="bridge_course_cs4pe" type="radio"  
                                value="2" <?= $bridge_course_cs4pe_checked ?> <?= $bridge_course_disabled ?> />CS4PE
                            <input name="bridge_course_decision" id="bridge_course_tbd" type="radio" 
                                value="3" <?= $bridge_course_tbd_checked ?> <?= $bridge_course_disabled ?> />TBD
                        </div>
                    </td>
                 </tr>
            <?php } ?>
            <tr>
            <td><strong>Evaluator Phone Contact:</font></strong></td>
            <td>
            <? 
            $stucontact = $myPrograms[$x][16];
            showEditText($stucontact, "textbox", "stucontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); 
            ?>
            <br/>&nbsp;
            </td>
            </tr>
            <tr>
            <td><strong>Academic Advisor Assignment:</strong></td>
            <td>
            <? 
            $faccontact = $myPrograms[$x][15];
            showEditText($faccontact, "textbox", "faccontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); 
            ?>
            </td>
            </tr>
            
            
            <tr>
                <td width="50"><strong>Letter Recommendations:</strong></td>
                <td>
                <?php
                $letter_rec_java_checked = "";
                $letter_rec_discrete_checked = "";
                $letter_rec_algorithms_checked = "";
                $letter_rec_data_structures_checked = "";
                $letter_rec_disabled = "";
                
                $mseDecisionProgramId = $myPrograms[$x][0];
                $mseLetterRecs = $db_letter_recs->getLetterRecsDecision($appid, $mseDecisionProgramId);
                
                foreach ($mseLetterRecs as $letter_rec) {
                
                    if ($letter_rec['java'] == 1) {
                        $letter_rec_java_checked = "checked";
                    }
                    if ($letter_rec['discrete'] == 1) {
                        $letter_rec_discrete_checked = "checked";
                    }
                    if ($letter_rec['algorithms'] == 1) {
                        $letter_rec_algorithms_checked = "checked";
                    }
                    if ($letter_rec['data_structures'] == 1) {
                        $letter_rec_data_structures_checked = "checked";
                    }
  
                }
                
                if (!$allowEdit) {
                    $letter_recs_disabled = "disabled";
                }
                ?> 
                <input name="mse_decision" type="hidden" value="true" />    
                <input name="mse_decision_program_id" type="hidden" value="<?php echo $mseDecisionProgramId; ?>" />
                <input name="letter_recs_decision[]" type="checkbox" class="tblItem" 
                    value="java" <?= $letter_rec_java_checked ?> <?= $letter_rec_disabled ?> />Java
                <input name="letter_recs_decision[]" type="checkbox" class="tblItem" 
                    value="discrete" <?= $letter_rec_discrete_checked ?> <?= $letter_rec_disabled ?> />Discrete Math
                <input name="letter_recs_decision[]" type="checkbox" class="tblItem" 
                    value="algorithms" <?= $letter_rec_algorithms_checked ?> <?= $letter_rec_disabled ?> />Algorithms
                <input name="letter_recs_decision[]" type="checkbox" class="tblItem" 
                    value="data_structures" <?= $letter_rec_data_structures_checked ?> <?= $letter_rec_disabled ?> />DataStructures
                
                </td>
            </tr>
             <tr>
                <td>
                    <br />
                </td>
             </tr>
            <tr>
            <td colspan="2">
            <h4>Admissions Status</h4>
            <?
            $admissionStatusValue = $myPrograms[$x][11];
            $applyProgramId = $myPrograms[$x][0];
            $inputName = "admissionStatus_" . $applyProgramId;
            
            if ($applyProgramId == 21 || $applyProgramId == 30)  {

                // MSE Campus
                
                $admissionStatusValue = getAdmissionStatusValue($appid, $applyProgramId, TRUE);
                
                $statusOptions = array(
                    array(0, "Reject"),
                    array('1_21', "Waitlist MSE"),
                    array('2_21', "Admit MSE"),
                    array('1_29', "Waitlist MSIT"),
                    array('2_29', "Admit MSIT"),
                    array(3, "Reset")    
                );

            } elseif ($applyProgramId == 37)  {

                // MSE Portugal
                
                $admissionStatusValue = getAdmissionStatusValue($appid, $applyProgramId, TRUE);
                
                $statusOptions = array(
                    array(0, "Reject"),
                    array('1_37', "Waitlist MSE"),
                    array('2_37', "Admit MSE"),
                    array('1_100003', "Waitlist MSIT"),
                    array('2_100003', "Admit MSIT"),
                    array(3, "Reset")    
                );
                                
            } elseif ($applyProgramId == 46)  {

                // MSE Korea
                
                $admissionStatusValue = getAdmissionStatusValue($appid, $applyProgramId, TRUE);
                
                $statusOptions = array(
                    array(0, "Reject"),
                    array('1_46', "Waitlist MSE"),
                    array('2_46', "Admit MSE"),
                    array('1_47', "Waitlist MSIT"),
                    array('2_47', "Admit MSIT"),
                    array(3, "Reset")    
                );
                
            } else {
                
                // All MSIT
                
                $statusOptions = array(
                    array(0, "Reject"),
                    array(1, "Waitlist"),
                    array(2, "Admit"),
                    array(3, "Reset")    
                );
            }
            
            showEditText($admissionStatusValue, "radiogrouphoriz2", $inputName, $allowEdit, false, $statusOptions); 
            ?>
            </td>
            </tr>

<?
        }//END IF DEPTS MATCH
    }//END FOR COUNT aDEPTS        
 }//END FOR EACH PROGRAM?>

</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmitFinal", $allowEdit); 
?>    
</div>
