<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$doctoralVoteVals = array(
    array(5, "5 Strong"),
    array(4, "4"),
    array(3, "3"),
    array(2, "2"),
    array(1, "Weak 1"),
);

$mseRiskFactorKey = array(
    1 => 'Language',
    2 => 'Experience',
    3 => 'Academic',
    4 => 'Other',
);


/*
* Recapitulate reviewData.inc.php to handle coordinator difference. 
*/
$mseRiskFactors = array();
// DAS $mseBridgeCourses = array();

for($i = 0; $i < count($committeeReviews); $i++){
    if( $committeeReviews[$i][32]== $thisDept
        && $committeeReviews[$i][30] == 0   // not faculty review 
        && ($committeeReviews[$i][29] == ""  // not supplemental review
            || $committeeReviews[$i][29] == NULL)
        && $committeeReviews[$i][4] == $reviewerId) 
    {
        $comments = $committeeReviews[$i][9];
        $point = $committeeReviews[$i][10];
        $point2 = $committeeReviews[$i][11];
        $mseRiskFactors = $db_risk_factors->getRiskFactors($appid, $committeeReviews[$i][4]);
        $mseBridgeCourses = $db_bridge_courses->getBridgeCourse($appid, $committeeReviews[$i][4]);
        $interviewDate = $committeeReviews[$i][34];
        $interviewType = $committeeReviews[$i][35];
        $interviewTypeText = $committeeReviews[$i][36];
        $interviewComments = $committeeReviews[$i][37];
        $bridgeCourse = $committeeReviews[$i][38];
        $mseEnglishComments = $committeeReviews[$i][42];
        $mseEnglishRating = $committeeReviews[$i][43];
        $mseProgrammingComments = $committeeReviews[$i][44];
        $mseProgrammingRating = $committeeReviews[$i][45];
        $mseFoundationalComments = $committeeReviews[$i][46];
        $mseFoundationalRating = $committeeReviews[$i][47];
        $mseMaturityComments = $committeeReviews[$i][48];
        $mseMaturityRating = $committeeReviews[$i][49];
        $mseUnderstandingComments = $committeeReviews[$i][50];
        $mseUnderstandingRating = $committeeReviews[$i][51];
        $mseExperienceComments = $committeeReviews[$i][52];
        $mseExperienceRating = $committeeReviews[$i][53];
    
        if ($committeeReviews[$i][23] != $round) 
        {
            $previousRoundData = TRUE;
            $reviewRound = $committeeReviews[$i][23];
            $allowEdit = FALSE;
        } 
    }
}


if ($previousRoundData) {
    echo '<span style="font-size: 12px; font-weight: bold; font-style: italic;">';
    echo 'You reviewed this application in round ' . $reviewRound . '</span>';    
    echo '<br/><br/>';
}

?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">
    <tr>
        <td width="50"><strong>Academics:</strong></td>
        <td>  
        <?php  
        ob_start();
        showEditText($mseEnglishComments, "textarea", "mse_english_comments", $allowEdit, false, 80); 
        $mseEnglishCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $mseEnglishCommentsTextarea); 
        ?> 
        </td>
    </tr>
    
    <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
    <tr>
        <td width="50"><strong>Design:</strong></td>
        <td>  
        <?php  
        ob_start();
        showEditText($mseProgrammingComments, "textarea", "mse_programming_comments", $allowEdit, false, 80); 
        $mseProgrammingCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $mseProgrammingCommentsTextarea); 
        ?> 
        </td>
    </tr>
    
    <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
    <tr>
        <td width="50"><strong>Research Ready:</strong></td>
        <td>  
        <?php  
        ob_start();
        showEditText($mseFoundationalComments, "textarea", "mse_foundational_comments", $allowEdit, false, 80); 
        $mseFoundationalCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $mseFoundationalCommentsTextarea); 
        ?>  
        </td>
    </tr>
    
    <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
    <tr>
        <td width="50"><strong>Faculty Fit:</strong></td>
        <td>  
        <?php  
        ob_start();
        showEditText($mseMaturityComments, "textarea", "mse_maturity_comments", $allowEdit, false, 80); 
        $mseMaturityCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $mseMaturityCommentsTextarea); 
        ?> 
        </td>
    </tr>
        
    <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
    <tr>
        <td width="50"><strong>Focus:</strong></td>
        <td>  
        <?php  
        ob_start();
        showEditText($mseUnderstandingComments, "textarea", "mse_understanding_comments", $allowEdit, false, 80); 
        $mseUnderstandingCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $mseUnderstandingCommentsTextarea); 
        ?> 
        </td>
    </tr>
       
    <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
    <tr>
        <td width="50"><strong>Teaching Ready:</strong></td>
        <td>  
        <?php  
        ob_start();
        showEditText($mseExperienceComments, "textarea", "mse_experience_comments", $allowEdit, false, 80); 
        $mseExperienceCommentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $mseExperienceCommentsTextarea); 
        ?> 
        </td>
    </tr>
    
     <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
     
        <tr>
        <td width="50"><strong>Overall Comments:</strong></td>
        <td>
        <?php  
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 80); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea); 
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Risk Factors:</strong></td>
        <td>
        <?php
        $risk_factor_language_checked = "";
        $risk_factor_experience_checked = "";
        $risk_factor_academic_checked = "";
        $risk_factor_other_checked = "";
        $risk_factor_text = "";
        $risk_factor_disabled = "";
         
        foreach ($mseRiskFactors as $risk_factor) {
        
            if ($risk_factor['language'] == 1) {
                $risk_factor_language_checked = "checked";
            }
            if ($risk_factor['experience'] == 1) {
                $risk_factor_experience_checked = "checked";
            }
            if ($risk_factor['academic'] == 1) {
                $risk_factor_academic_checked = "checked";
            }
            if ($risk_factor['other'] == 1) {
                $risk_factor_other_checked = "checked";
            }
            if ($risk_factor['other_text']) {
                $risk_factor_text = $risk_factor['other_text'];    
            }   
        }
        
        if (!$allowEdit) {
            $risk_factor_disabled = "disabled";
        }
        ?> 
        <input name="mse_review" type="hidden" value="true" />    
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="language" <?= $risk_factor_language_checked ?> <?= $risk_factor_disabled ?> />Language
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="experience" <?= $risk_factor_experience_checked ?> <?= $risk_factor_disabled ?> />Experience
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="academic" <?= $risk_factor_academic_checked ?> <?= $risk_factor_disabled ?> />Academic
        <input name="risk_factors[]" type="checkbox" class="tblItem" 
            value="other" <?= $risk_factor_other_checked ?> <?= $risk_factor_disabled ?> />Other
        <div style="margin:5px;">
        Other Risk Factor(s):
        <?php
        showEditText($risk_factor_text, "textbox", "mse_risk_factor_text",
             $allowEdit, false, null, true, 25);
        ?>
        </div>
        </td>
    </tr>
    
    <tr>
        <td width="50"><strong>Evaluation:</strong></td>
        <td>
        <div id="mse_score">
        <?php
        
        $programId = $myPrograms[0][0];
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $doctoralVoteVals);
        ?>
        </div>
        </td>
    </tr>
    
<?php
$programId = $myPrograms[0][0];
if ($programId == 21 || $programId == 37) {
?>
    <tr>
        <td width="50"><div class="mse_msit" id="mse_msit_label"><strong>MSIT Evaluation:</strong></div></td>
        <td>
        <script language="javascript">
        $(document).ready(function () {
            
            $("#mse_score input").click(function (event) {
            
                var $target = $(event.target);               
                if($target.attr("value") == "4") {
                
                    alert("Please evaluate this candidate for MSIT admission.");
                    $("#mse_msit_label").css("visibility", "visible"); 
                    $("#mse_msit_score").css("visibility", "visible");
                    <?php
                    // Keep the "Accept" selection from the db if user is
                    // changing selection without saving. 
                    if($point2 == 1) { 
                    ?>
                    $("#mse_msit_score input[value='1']").attr("checked", "checked");
                    <?php
                    } elseif($point2 == 2) { 
                    ?>
                    $("#mse_msit_score input[value='2']").attr("checked", "checked");
                    <?php
                    } elseif($point2 == 3) { 
                    ?>
                    $("#mse_msit_score input[value='3']").attr("checked", "checked");
                    <?php
                    } else { 
                    ?>
                    $("#mse_msit_score input[value='4']").attr("checked", "checked");
                    <?php
                    }
                    ?>
                
                } else {
                
                    $("#mse_msit_label").css("visibility", "hidden"); 
                    $("#mse_msit_score").css("visibility", "hidden");
                    $("#mse_msit_score input").removeAttr("checked");   
                }     
            });            
        });
        </script>
        <div class="mse_msit" id="mse_msit_score">    
        <?php
        showEditText($point2, "radiogrouphoriz3", "point2", $allowEdit, false, $mseVoteVals);
        ?>
        </div>
        <?php
        // Hide the MSE-MSIT score form on page load unless the MSE score is "marginal"
        if ($point == 4) {
            // Do nothing
        } else {           
            // Hide the form
        ?>
            <script language="javascript">
                $(document).ready(function () {
                    $("#mse_msit_label").css("visibility", "hidden"); 
                    $("#mse_msit_score").css("visibility", "hidden");
                });
            </script>       
        <?
        }
        ?>
        </td>
    </tr>
<?php
} else {
?>
    <tr><td colspan="2">&nbsp;</td></tr>
<?php
}
    ?>

   
    <tr><td colspan="2" align="center"><hr width="95%" style="color: #eee;"></td></tr>
    
</table>


<div style="margin-top: 10px;">
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>

<script type="text/javascript">
$(document).ready(function(){
    
    $("#interview_date").datepicker({ dateFormat: 'yy-mm-dd', constrainInput: true });
    
    $("#form1").submit(function() {
    
        var interviewType = $(".interviewType:checked").val();
        var interviewTypeText = $("#interview_type_text").val();
        var interviewDate = $("#interview_date").val();
        var interviewComments = $("#interview_comments").val();
        
        if (interviewType != 0
            || interviewTypeText != ''
            || interviewDate != ''
            || interviewComments != '') 
        {        
            if (interviewType == 0
                || interviewDate == ''
                || interviewComments == '') 
            {
                alert('Please complete all interview-related fields.');                
                return false;
            }        
        }    
        
        if (interviewType == 1 && interviewTypeText == '') {
            alert('Please enter a value for interview type "other."');                
            return false;    
        }
            
    });
    
});

</script>