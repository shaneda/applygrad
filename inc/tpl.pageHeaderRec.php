<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
 <!--- comment  --->
 <!--   <meta HTTP-EQUIV="REFRESH" content="0; url=https://applygrad.cs.cmu.edu/under_construction.html">      --->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $HEADER_PREFIX[$domainid]; ?></title>

    <?php
    /* print some header content to handle the results from the request */
    if ($includeAjax) {
    ?>
        <script type="text/javascript" src="../phplib/AjaxCore/prototype.js"></script>
        <script type="text/javascript" src="../phplib/AjaxCore/AjaxCore.js"></script>    
    <?php
        if (isset($ajax)) {
            echo $ajax->getJSCode();
        }
    }
    
    // $stattest1 and $stattest2 come from config.php
    if (isset($domainname)) {
        if ($domainname == 'SCS') {
            $scsStyle = TRUE;
        } else {
            $scsStyle = FALSE;
        }
    ?>
        <link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
    <?php    
    } 
    elseif (($stattest1 == "WEB05.SRV.CS.CMU.EDU") ||  ($stattest2 == "APPLY.STAT.CMU.EDU"))
    {
        $scsStyle = FALSE;
    ?>
        <link href="../css/SCSStyles_Statistics.css" rel="stylesheet" type="text/css">
    <?    
    } 
    else 
    {
        $scsStyle = TRUE;
    ?>
        <link href="../css/SCSStyles_SCS.css" rel="stylesheet" type="text/css">
    <?    
    }
    ?>
    
    <SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT> 
</head>
<?php flush(); ?>
<body bgcolor="white">
<?php
if ($scsStyle) {
?>
<div id="banner">
    <img src="../apply/images/CSD/appl.jpg" width="753" height="127" border="0" usemap="#Map" />
    <map name="Map" id="Map">
        <area shape="rect" coords="490,23,742,38" href="http://www.cs.cmu.edu/" />
        <area shape="rect" coords="633,5,742,21" href="http://www.cmu.edu/index.shtml" />
    </map>
</div>
<?php
} else {
?>
    <div id="banner"></div>
<?php
}
?>
<table width="753px" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top">
        <div class="content">