<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

$assistantshipRequested = $aasistantshipGranted = NULL;
$assistanshipStatusQuery = "SELECT * FROM assistantship 
    WHERE application_id = " . $appid . 
    " LIMIT 1";
$assistanshipStatusResult = mysql_query($assistanshipStatusQuery);
while ($row = mysql_fetch_array($assistanshipStatusResult))
{
    $assistantshipRequested = $row['requested']; 
    $assistantshipGranted = $row['granted'];  
}

$admissionProgramOptions = getProgramDepartmentPrograms($myPrograms[0][0]);
$admissionStatuses = getDesignMastersAdmissionStatuses($appid);

$admissionStatusValues = array(
    array(0, "Reject"),
    array(1, "Waitlist"),
    array(4, "Strong Waitlist"),
    array(2, "Admit"),
    array(3, "Reset")
);
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

    <tr>
        <td width="50px"><b>Program 1:</b></td>
        <td>
        <?
        if (isset($admissionStatuses[0]))
        {
            $admissionProgram1 = $admissionStatuses[0]['admission_program_id'];    
        }
        else
        {
            if (isset($myPrograms[0]))
            {
                $admissionProgram1 = $myPrograms[0][0];    
            }
            else
            {
                $admissionProgram1 = NULL;
            } 
        }
        showEditText($admissionProgram1, "listbox", "admissionProgram1", $_SESSION['A_allow_admin_edit'], 
            true, $admissionProgramOptions); 
        ?>
        </td>
    </tr>
        
    <tr>
        <td width="50px"><b>&nbsp;</b></td>
        <td colspan="2">
        <? 
        if (isset($admissionStatuses[0]))
        {
            $admissionStatus1 = $admissionStatuses[0]['admission_status'];    
        }
        else
        {
            if (isset($myPrograms[0]))
            {
                $admissionStatus1 = $myPrograms[0][11];    
            }
            else
            {
                $admissionStatus1 = NULL;
            }  
        }
        showEditText($admissionStatus1, "radiogrouphoriz2", "admissionStatus1", $allowEdit, false, $admissionStatusValues); 
        ?>
        </td>
    </tr>

    <tr>
        <td colspan="2"><hr style="color: #eee;"></td>
    </tr>
    
    <tr>
        <td width="50px"><b>Program 2:</b></td>
        <td>
        <?
        if (isset($admissionStatuses[1]))
        {
            $admissionProgram2 = $admissionStatuses[1]['admission_program_id'];    
        }
        else
        {
            if (!isset($admissionStatuses[0]) && isset($myPrograms[1]))
            {
                $admissionProgram2 = $myPrograms[1][0];    
            }
            else
            {
                $admissionProgram2 = NULL;
            }
        }
        showEditText($admissionProgram2, "listbox", "admissionProgram2", $_SESSION['A_allow_admin_edit'], 
            true, $admissionProgramOptions); 
        ?>
        </td>
    </tr>
        
    <tr>
        <td width="50px"><b>&nbsp;</b></td>
        <td colspan="2">
        <? 
        if (isset($admissionStatuses[1]))
        {
            $admissionStatus2 = $admissionStatuses[1]['admission_status'];    
        }
        else
        {
            if (!isset($admissionStatuses[0]) && isset($myPrograms[0]) && isset($myPrograms[1]))
            {
                $admissionStatus2 = $myPrograms[1][11];    
            }
            else
            {
                $admissionStatus2 = NULL;
            } 
        }
        showEditText($admissionStatus2, "radiogrouphoriz2", "admissionStatus2", $allowEdit, false, $admissionStatusValues); 
        ?>
        </td>
    </tr>
    
    <tr>
        <td colspan="2"><hr style="color: #eee;"></td>
    </tr>
    
    <tr>
        <td width="50px"><b>Comments:</b></td>
        <td>
        <?
        if (isset($myPrograms[0]))
        {
            $comments = $myPrograms[0][13];    
        }
        else
        {
            $comments = '';
        }    
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 60); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea);  
        ?>
        </td>
    </tr>
    
    <tr>
        <?php
        if (isset($myPrograms[0]))
        {
            $faccontact = $myPrograms[0][15];
            $stucontact = $myPrograms[0][16];    
        }
        else
        {
            $faccontact = '';
            $stucontact = '';
        }
        ?>
    
        <td><strong>Faculty Contact:</strong></td>
        <td>
        <?
        showEditText($faccontact, "textbox", "faccontact", $allowEdit,false,null,true,30); 
        ?>
        </td>
        </tr>
        
        <tr>
        <td><strong>Student Contact:</font></strong></td>
        <td>
        <?
        showEditText($stucontact, "textbox", "stucontact", $allowEdit,false,null,true,30); 
        ?>
        <br/>&nbsp;
        </td>
    </tr>

 
 <tr>
    <td colspan="2">
        
        <strong></strong>  
    </td>
 </tr>
<tr>
    <td colspan="2">
    <hr />
    <h4>Assistantship Information</h4>
    Assistantship Requested: <?php echo $assistantshipRequested ? 'Yes' : 'No' ; ?>
    <br><br>
    <?
    $assistanshipGranted = NULL; 
    showEditText($assistantshipGranted, "checkbox", "assistantshipGranted", $allowEdit, false, NULL); 
    ?>
    Granted
    </td>
</tr>
            
 
</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmitFinal", $allowEdit); 
?>    
</div>

<?php
function getProgramDepartmentPrograms($programId)
{
    $programs = array();
        
    $query = "SELECT programs.id,
        CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS name
        FROM programs
        INNER JOIN degree ON degree.id = programs.degree_id
        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
        INNER JOIN lu_programs_departments
            ON programs.id = lu_programs_departments.program_id
        WHERE lu_programs_departments.department_id = 
            (SELECT department_id 
            FROM lu_programs_departments 
            WHERE program_id = " . intval($programId) . "
            LIMIT 1)";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        $programs[] = array($row['id'], $row['name']);     
    }
    
    return $programs;
}


?>