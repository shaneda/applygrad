<?php
define("CLASS_DIR", "../classes/");
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb.php';
include CLASS_DIR . 'DB_Applyweb/class.DB_Applyweb_Table.php';
include '../inc/unitPeriodIncludes.inc.php';

$DB_DomainUnit = new DB_DomainUnit();
$domainUnitRecord = $DB_DomainUnit->find($domainid);
$unitId = $domainUnitRecord[0]['unit_id'];
$unit = new Unit($unitId);

// Disable login/editing by default.
$_SESSION['allow_login'] = FALSE;
$_SESSION['allow_new_application'] = FALSE; 
$_SESSION['allow_edit'] = FALSE;
$_SESSION['allow_edit_late'] = FALSE;
$_SESSION['expdate'] = '';
$_SESSION['expdate2']  = '';

// Check for umbrella periods with active submission periods.
$activeSubmitUmbrellas = $unit->getActivePeriods();
if ( count($activeSubmitUmbrellas) > 0) {
     
    $_SESSION['allow_login'] = TRUE;
    $_SESSION['allow_new_application'] = TRUE;
    $_SESSION['allow_edit'] = TRUE;
    $_SESSION['allow_edit_late'] = TRUE;
    
    foreach ($activeSubmitUmbrellas as $activeSubmitUmbrella) {
    
        $submitPeriods = $activeSubmitUmbrella->getChildPeriods(2);
        // There should be one (and only one) submission period.
        if ( count($submitPeriods) > 0 ) {       
            $_SESSION['expdate'] = $submitPeriods[0]->getEndDate();    
        }    
        
        $editPeriods = $activeSubmitUmbrella->getChildPeriods(3);
        // There should be one (and only one) editing period.
        if ( count($editPeriods) > 0 ) {       
            $_SESSION['expdate2'] = $editPeriods[0]->getEndDate();;    
        } 
    }
  
} else {
    
    // Check for umbrella periods with active editing periods.
    $activeEditUmbrellas = $unit->getActivePeriods(3);
    if ( count($activeEditUmbrellas) > 0) { 
        
        $_SESSION['allow_login'] = TRUE;
        $_SESSION['allow_edit_late'] = TRUE;    

        foreach ($activeEditUmbrellas as $activeEditUmbrella) {
        
            $submitPeriods = $activeEditUmbrella->getChildPeriods(2);
            // There should be one (and only one) submission period.
            if ( count($submitPeriods) > 0 ) {       
                $_SESSION['expdate'] = $submitPeriods[0]->getEndDate();    
            }    
            
            $editPeriods = $activeEditUmbrella->getChildPeriods(3);
            // There should be one (and only one) editing period.
            if ( count($editPeriods) > 0 ) {       
                $_SESSION['expdate2'] = $editPeriods[0]->getEndDate();;    
            } 
        }
    }    
}

/*
// There should only be one active period per domain.
$activePeriods = $unit->getActivePeriods();
$activePeriod = NULL;
$activePeriodId = NULL;
foreach ($activePeriods as $activePeriod) {
    $activePeriodId = $activePeriod->getId();
}

$activeUmbrellaPeriod = NULL;
if ($activePeriodId) {

    $_SESSION['allow_edit'] = TRUE;
    $activeUmbrellaPeriod = $activePeriod;
    
} else {

    // Check for 0 || 1 umbrella period with an active editing period.
    $activePeriods = $unit->getActivePeriods(3);
    if ( count($activePeriods) > 0 ) {
        $activeUmbrellaPeriod = $activePeriods[0];    
    }    
}

if ($activeUmbrellaPeriod) {
    
    $_SESSION['allow_edit_late'] = TRUE;

    $submitPeriods = $activeUmbrellaPeriod->getChildPeriods(2);
    // There should be one (and only one) submission period.
    if ( count($submitPeriods) > 0 ) {       
        $_SESSION['expdate'] = $submitPeriods[0]->getEndDate();    
    }
    
    $editPeriods = $activeUmbrellaPeriod->getChildPeriods(3);
    // There should be one (and only one) editing period.
    if ( count($editPeriods) > 0 ) {       
        $_SESSION['expdate2'] = $editPeriods[0]->getEndDate();;    
    }    
}
*/
?>