<?php
// Initialize variables
$dietrichPier = NULL;
$dietrichCnbc = NULL;
$dietrichPierCnbcError = '';

// Validate and save posted data
if (isset($_POST['dietrichPierCnbcSubmitted']))
{
    saveDietrichPierCnbc();
    checkRequirementsDietrichPierCnbc();
}
// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$dietrichPierCnbcError)
{
    $dietrichPierCnbcQuery = "SELECT pier, cnbc FROM application 
        WHERE id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $dietrichPierCnbcResult = mysql_query($dietrichPierCnbcQuery);
    while($row = mysql_fetch_array($dietrichPierCnbcResult))
    {
        $dietrichPier = $row['pier'];
        $dietrichCnbc = $row['cnbc'];           
    }    
}      
?>
<hr size="1" noshade color="#990000">

The Department uses a variety of sources to provide a stipend and tuition for all students in good standing 
during the first four years of their graduate training. Departmental sources include departmental funds, 
faculty research grants, and training programs and grants. Participation in the  PIER Program extends the 
support period by an additional year.
<br>
<?php
/* 
* Add onlick event handlers to checkboxes for inclusion on programs.php, 
* because programs has no page-level submit input.
*/
$dietrichPierCnbcCheckboxReplace = ' onClick="form1.submit();">';
ob_start();
showEditText($dietrichPier, "checkbox", "dietrichPier", $_SESSION['allow_edit'], false);
$dietrichPierCheckbox = ob_get_contents();
ob_end_clean();
$dietrichPierCheckbox = str_replace('>', $dietrichPierCnbcCheckboxReplace, $dietrichPierCheckbox);    
echo $dietrichPierCheckbox . ' Program in Interdisciplinary Educational Research (PIER) (supplemental application required)
    <br>URL: <a href="http://www.cmu.edu/pier/">http://www.cmu.edu/pier/</a>';

echo '<br>';

ob_start();
showEditText($dietrichCnbc, "checkbox", "dietrichCnbc", $_SESSION['allow_edit'], false);
$dietrichCnbcCheckbox = ob_get_contents();
ob_end_clean();
$dietrichCnbcCheckbox = str_replace('>', $dietrichPierCnbcCheckboxReplace, $dietrichCnbcCheckbox);    
echo $dietrichCnbcCheckbox . ' Program in The Center of the Neutral Basis of Cognition (CNBC) (supplemental application required)
    <br>URL: <a href="http://www.cnbc.cmu.edu/">http://www.cnbc.cmu.edu/</a>';
?>

<input type="hidden" name="dietrichPierCnbcSubmitted" value="1">
<hr size="1" noshade color="#990000">
<br/>

<?php
function saveDietrichPierCnbc()
{
    global $dietrichPier;
    global $dietrichCnbc;
    global $dietrichPierCnbcError;
    
    $dietrichPier = filter_input(INPUT_POST, 'dietrichPier', FILTER_VALIDATE_BOOLEAN);
    $dietrichCnbc = filter_input(INPUT_POST, 'dietrichCnbc', FILTER_VALIDATE_BOOLEAN);
    
    $updateQuery = "UPDATE application SET
            pier = " . intval($dietrichPier) . ",
            cnbc = " . intval($dietrichCnbc) . "
            WHERE id = " . intval($_SESSION['appid']);
    mysql_query($updateQuery);
}

function checkRequirementsDietrichPierCnbc()
{
    global $err;
    global $dietrichPierCnbcError;     
    
    if (!$err && !$dietrichPierCnbcError)
    {
        updateReqComplete("programs.php", 1);
    }
    else
    {
        updateReqComplete("programs.php", 0);    
    }    
}
?>