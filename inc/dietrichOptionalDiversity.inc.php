<?php
// Initialize variables
$dietrichDiversityBackground = NULL;
$dietrichDiversityLifeExperience = NULL;
$dietrichDiversityError = '';

// Validate and save posted data
if( isset($_POST['btnSubmit'])) 
{
    saveDietrichDiversity();
    checkRequirementsDietrichDiversity();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$dietrichDiversityError)
{
    $dietrichDiversityQuery = "SELECT background, life_experience 
        FROM dietrich_diversity
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $dietrichDiversityResult = mysql_query($dietrichDiversityQuery);
    while($row = mysql_fetch_array($dietrichDiversityResult))
    {
        $dietrichDiversityBackground = $row['background'];
        $dietrichDiversityLifeExperience = $row['life_experience'];         
    }    
}

// Check whether form inputs should be disabled 
$disabled = '';
if (!$_SESSION['allow_edit'])
{
    $disabled = 'disabled="disabled"';    
} 
?>

<span class="subtitle">Optional Diversity Questions</span>
<br/>
<br/>
<b>Please describe any aspects of your personal background, accomplishments, or achievements that you feel are 
important in evaluating your application for graduate study.</b>  For example, economic challenges in higher 
education, such as financial responsibility for family members, having to work significant hours during 
undergraduate school, or a family background of limited income. (Limit 1200 characters.)
<br/>
<?php
/*
showEditText($dietrichDiversityBackground, "textarea", "dietrichDiversityBackground", $_SESSION['allow_edit']); 
*/
?>
<textarea name="dietrichDiversityBackground" cols="60" rows="5" 
    maxlength="1200" onkeyup="countChar(this, 1200)"
    <?php echo $disabled; ?> ><?php echo $dietrichDiversityBackground; ?></textarea>

<br/>
<br/>
<b>Please describe any unusual or varied life experiences that might contribute to the diversity of the graduate group.</b>  
For examples, fluency in other languages, experience living in bicultural communities, or significant experience working in underserved 
or disadvantaged communities. (Limit 1200 characters.)
<br/>
<?php
/*
showEditText($dietrichDiversityLifeExperience, "textarea", "dietrichDiversityLifeExperience", $_SESSION['allow_edit']); 
*/
?>
<textarea name="dietrichDiversityLifeExperience" cols="60" rows="5" 
    maxlength="1200" onkeyup="countChar(this, 1200)"
    <?php echo $disabled; ?> ><?php echo $dietrichDiversityLifeExperience; ?></textarea>
<br/>
<hr size="1" noshade color="#990000">

<script type="text/javascript">
function countChar(val, maxsize) {
    var len = val.value.length;
    if (len > maxsize) {
        val.value = val.value.substring(0, maxsize);
    }
};
</script>

<?php
function saveDietrichDiversity()
{
    global $dietrichDiversityBackground;
    global $dietrichDiversityLifeExperience;
    global $dietrichDiversityError;

    $dietrichDiversityBackground = filter_input(INPUT_POST, 'dietrichDiversityBackground', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $dietrichDiversityLifeExperience = filter_input(INPUT_POST, 'dietrichDiversityLifeExperience', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    
    // Check for existing record
    $existingRecordQuery = "SELECT id FROM dietrich_diversity WHERE application_id = " . intval($_SESSION['appid']);
    $existingRecordResult = mysql_query($existingRecordQuery);
    if (mysql_num_rows($existingRecordResult) > 0)
    {
        // Update existing record
        $updateQuery = "UPDATE dietrich_diversity SET
            background = '" . mysql_real_escape_string($dietrichDiversityBackground) . "',
            life_experience = '" . mysql_real_escape_string($dietrichDiversityLifeExperience) . "'
            WHERE application_id = " . intval($_SESSION['appid']);
        mysql_query($updateQuery);
    }
    else
    {
        // Insert new record
        $insertQuery = "INSERT INTO dietrich_diversity 
            (application_id, background, life_experience)
            VALUES (" 
            . intval($_SESSION['appid']) . ",'"
            . mysql_real_escape_string($dietrichDiversityBackground) . "','"
            . mysql_real_escape_string($dietrichDiversityLifeExperience) . "')";
        mysql_query($insertQuery);
    }
}

function checkRequirementsDietrichDiversity()
{
    global $err;
    global $dietrichDiversityError;     
    
    if (!$err && !$dietrichDiversityError)
    {
        updateReqComplete("bio.php", 1);
    }
    else
    {
        updateReqComplete("bio.php", 0);    
    }    
}
?>