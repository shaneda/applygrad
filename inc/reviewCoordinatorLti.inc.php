<?php
$exclude_scriptaculous = TRUE; 
include_once '../inc/functions.php';

/*
$allowEdit = TRUE;
$previousRoundData = FALSE;
for($i = 0; $i < count($committeeReviews); $i++) {
    if( $committeeReviews[$i][4] == $reviewerId 
        && $committeeReviews[$i][29] == "" ) 
    {
        $comments = $committeeReviews[$i][9];
        $private_comments = $committeeReviews[$i][12];
        $point = $committeeReviews[$i][10]; 

        if ($committeeReviews[$i][23] != $round) 
        {
            $previousRoundData = TRUE;
            $reviewRound = $committeeReviews[$i][23];
            $allowEdit = FALSE;
        }
    } 
}
*/ 

$allowEdit = TRUE;
$comments = '';
$grad_name = '';
$touched = 0;
for($i = 0; $i < count($committeeReviews); $i++)
{
    if($committeeReviews[$i][4] == $reviewerId 
        && $committeeReviews[$i][29] == "" 
        && $committeeReviews[$i][23] == $round)
    {
        $comments = $committeeReviews[$i][9];
        $grad_name = $committeeReviews[$i][17];
        $touched = $committeeReviews[$i][14];
    }
} 
?> 

<table width="500" border="0" cellspacing="0" cellpadding="2">

    <tr>
        <td width="50"><strong>Note:</strong></td>
        <td>
        <?php 
        ob_start();
        showEditText($comments, "textarea", "comments", $allowEdit, false, 80); 
        $commentsTextarea = ob_get_contents();
        ob_end_clean();
        echo str_replace("cols='60'", "cols='50'", $commentsTextarea);
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Refer to:</strong> </td>
        <td>
        <?php 
        showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); 
        ?>    
        </td>
    </tr>

    <tr>
        <td width="50"><strong>Finished:</strong></td>
        <td>
        <?php
        showEditText($touched, "radiogrouphoriz2", "touched", $allowEdit, false, array(array(1,"yes"), array(0,"No")) );
        ?>
        </td>
    </tr>

</table>


<div style="margin-top: 10px;">
<!-- 
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" 
    onClick="resetForm()" <?php if (!$allowEdit) { echo 'disabled'; } ?> />   
-->
<?php 
showEditText("Save", "button", "btnSubmit", $allowEdit); 
?>    
</div>
