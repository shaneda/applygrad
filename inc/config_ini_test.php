<?php
if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
{
    //header("Location: ../apply/offline.php");
}

/*
CONSTANT VALUES FOR THE APPLICATION
*/
//set_time_limit(0);
$environment="test";

$db="awIniTest";
$db_username="awIniTestAdmin";
$db_password="8uHuVos1Rebu";
$db_host="webuild-db.srv.cs.cmu.edu";

$datafileroot = "../test-data";

$dontsendemail = TRUE;
$admissionsContact = "scsini+admissions@cs.cmu.edu"; 
$paymentEmail = "scsini+payment@cs.cmu.edu";
$supportEmail = "scsini+technical@cs.cmu.edu";

$paymentProcessor = "https://ccard-submit-test.as.cmu.edu/cgi-bin/gather_info.cgi"; //TESTING USING GENERIC COLLECTOR

if ( strpos( phpversion(),'ubuntu' ) !== FALSE)
{
    // System magic file doesn't seem to work
    // $magic_location = "/usr/share/file/magic"; 
    $magic_location = "/usr0/apache2/webapps/ApplygradIni/www/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_STRICT);    
}
elseif ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic-php-5.3"; 
    error_reporting(E_ALL ^ E_DEPRECATED);    
} else {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic";
    error_reporting(E_ALL);    
}

ini_set('display_errors', 1);                                                                                                                                                                                   
?>