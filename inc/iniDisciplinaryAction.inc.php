<?php
// Initialize variables
$iniDisciplinaryActionSanction = NULL;
$iniDisciplinaryActionSanctionDescription = '';
$iniDisciplinaryActionRetraction = NULL;
$iniDisciplinaryActionRetractionDescription = '';
$iniDisciplinaryActionError = '';

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveIniDisciplinaryAction();
    checkRequirementsIniDisciplinaryAction();
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$iniDisciplinaryActionError)
{
    $iniFinancialQuery = "SELECT * FROM ini_disciplinary_action 
        WHERE application_id = " . intval($_SESSION['appid']) . "
        LIMIT 1";
    $iniFinancialResult = mysql_query($iniFinancialQuery);
    while($row = mysql_fetch_array($iniFinancialResult))
    {
        $iniDisciplinaryActionSanction = $row['sanction'];
        $iniDisciplinaryActionSanctionDescription = $row['sanction_description'];
        $iniDisciplinaryActionRetraction = $row['retraction'];
        $iniDisciplinaryActionRetractionDescription = $row['retraction_description'];        
    }    
}  
?>

<span class="subtitle">Disciplinary Action</span>
<?php
if ($iniDisciplinaryActionError)
{
?>
    <br>
    <span class="errorSubtitle"><?php echo $iniDisciplinaryActionError; ?></span>
<?php  
}
?>
<br/>
<br/>
<strong style="color: red;">(Mandatory)</strong> Has an institution of higher education ever imposed disciplinary sanctions
against you for academic misconduct (e.g., cheating or plagiarism) or non-academic
misconduct (e.g., violations of institutional behavioral standards)? For purposes
of this question, disciplinary sanctions include any form of probation, suspension,
or expulsion.
<br/><br/>
<?php
$radioYesNo = array(
    array(1, "Yes"),
    array(0, "No")
);
showEditText($iniDisciplinaryActionSanction, "radiogrouphoriz4", "iniDisciplinaryActionSanction", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>
<br/><br/>
If yes, please describe.
<br/>
<?php
showEditText($iniDisciplinaryActionSanctionDescription, "textbox", "iniDisciplinaryActionSanctionDescription", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 100);
?>
<br/> <br/>
<strong style="color: red;">(Mandatory)</strong> Have you ever been asked or required to retract a paper, report, or other publication 
due to misconduct by you?
<br/><br/>
<?php
showEditText($iniDisciplinaryActionRetraction, "radiogrouphoriz4", "iniDisciplinaryActionRetraction", $_SESSION['allow_edit'], TRUE, $radioYesNo);
?>
<br/><br/>
If yes, please describe.
<br/>
<?php
showEditText($iniDisciplinaryActionRetractionDescription, "textbox", "iniDisciplinaryActionRetractionDescription", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 100);
?>

<hr size="1" noshade color="#990000">

<?php
function saveIniDisciplinaryAction()
{
    global $iniDisciplinaryActionSanction;
    global $iniDisciplinaryActionSanctionDescription;
    global $iniDisciplinaryActionRetraction;
    global $iniDisciplinaryActionRetractionDescription;
    global $iniDisciplinaryActionError;
    
    $iniDisciplinaryActionSanction = filter_input(INPUT_POST, 'iniDisciplinaryActionSanction', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $iniDisciplinaryActionSanctionDescription = filter_input(INPUT_POST, 'iniDisciplinaryActionSanctionDescription', FILTER_SANITIZE_STRING);
    $iniDisciplinaryActionRetraction = filter_input(INPUT_POST, 'iniDisciplinaryActionRetraction', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $iniDisciplinaryActionRetractionDescription = filter_input(INPUT_POST, 'iniDisciplinaryActionRetractionDescription', FILTER_SANITIZE_STRING);

    if ($iniDisciplinaryActionSanction === NULL)
    {
        $iniDisciplinaryActionError .= 'Disciplinary sanction answer is required<br>';    
    }
     
    if ($iniDisciplinaryActionRetraction === NULL)
    {
        $iniDisciplinaryActionError .= 'Paper retraction answer is required<br>';    
    }
        
    if (!$iniDisciplinaryActionError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM ini_disciplinary_action WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE ini_disciplinary_action SET
                sanction = " . intval($iniDisciplinaryActionSanction) . ",
                sanction_description = '" . mysql_real_escape_string($iniDisciplinaryActionSanctionDescription) . "',
                retraction = " . intval($iniDisciplinaryActionRetraction) . ",
                retraction_description = '" . mysql_real_escape_string($iniDisciplinaryActionRetractionDescription) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO ini_disciplinary_action 
                (application_id, sanction, sanction_description, retraction, retraction_description)
                VALUES (" 
                . intval($_SESSION['appid']) . "," 
                . intval($iniDisciplinaryActionSanction) . ",'" 
                . mysql_real_escape_string($iniDisciplinaryActionSanctionDescription) . "'," 
                . intval($iniDisciplinaryActionRetraction) . ",'" 
                . mysql_real_escape_string($iniDisciplinaryActionRetractionDescription) . "')";
            mysql_query($insertQuery);
        }
    }
}

function checkRequirementsIniDisciplinaryAction()
{
    global $err;
    global $iniDisciplinaryActionError;   
 
    if (!$err && !$iniDisciplinaryActionError)
    {
        updateReqComplete("suppinfo.php", 1);
    }
    else
    {
        updateReqComplete("suppinfo.php", 0);    
    }    
}
?>