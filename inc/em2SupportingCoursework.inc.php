<?php
// initialize variables
$em2CourseworkKobeDescription = '';
$em2CourseworkDataStructuresTitle = '';
$em2CourseworkDataStructuresNumber = '';
$em2CourseworkStatisticsTitle = '';
$em2CourseworkStatisticsNumber = '';
$em2CourseworkMsitExperience = '';
$em2CourseworkProgrammingDescription = '';
$em2CourseworkProgrammingDescription2 = '';
$em2CfaMakerKitsDescription = '';
$em2CfaMakerKitsDescription2 = '';
$em2CourseworkError = '';

// Special cases
$isEm2MobilityApplication = isEm2MobilityApplication($_SESSION['appid']);
$isEm2CfaApplication = isEm2CfaApplication($_SESSION['appid']);
$em2MobilityRequired = FALSE;


// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    saveEM2Coursework();
    
    checkEM2CourseworkRequirements();  
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$em2CourseworkError)
{
    
        $em2SupportingCourseworkQuery = "SELECT * FROM em2_supporting_coursework 
            WHERE application_id = " . intval($_SESSION['appid']) . " LIMIT 1";
        $em2SupportingCourseworkResult = mysql_query($em2SupportingCourseworkQuery);        
        while($row = mysql_fetch_array($em2SupportingCourseworkResult))
        {
            $em2CourseworkDataStructuresTitle = $row['data_structures_title'];
            $em2CourseworkDataStructuresNumber = $row['data_structures_number'];      
            $em2CourseworkStatisticsTitle = $row['statistics_title'];
            $em2CourseworkStatisticsNumber = $row['statistics_number'];
            $em2CourseworkMsitExperience = $row['msit_experience'];
            $em2CourseworkProgrammingDescription = $row['programming_description'];
            $em2CourseworkProgrammingDescription2 = $row['programming_description2'];
            $em2CfaMakerKitsDescription = $row['makerkits_description'];
            $em2CfaMakerKitsDescription2 = $row['makerkits_description2'];   
        }
    
}
?>

<span class="subtitle">Supporting Educational Data</span>
<?php
if ($em2CourseworkError)
{
?>
    <br>
    <span class="errorSubtitle"><?php echo $em2CourseworkError; ?></span>
<?php  
}
?>
<br/>
<br/>

    <u>Programming</u>
    <p>For students admitted to Emerging Media Program, programming experience is required.  Expectations 
    vary per specialization path. Knowledge of C is recommended for all SCS and CIT path applicants. 
    Proficiency in Java or C++ is required for the Mobility path applicants. </p>
    
    <p>Please indicate in the text box below your programming experience.  In Box 1 please indicate languages
    you know and how long you have used each of these programming languages.  Please categorize each 
    experience into course work, independent work, employment work, academic research work or other work. 
    In Box 2 describe your practical experience on one or two sizable projects using these languages. 
    Include indicators of the project size, complexity, team size and your responsibility level. Descriptions 
    not providing adequate detail may be marked as having insufficient experience. If you do not have 
    experience with a particular language, please indicate this. </p>
    
    Describe programming languages you know
    <br/>
    
    <?php
    $disabled = '';
    if (!$_SESSION['allow_edit'])
    {
        $disabled = 'disabled="disabled"';    
    }
    ?>
    <textarea name="em2CourseworkProgrammingDescription" id="em2CourseworkProgrammingDescription" class="tblItemRequired"
        cols="60" rows="5" <?php echo $disabled; ?> ><?php echo $em2CourseworkProgrammingDescription; ?></textarea>
    <br/>
    Characters left in your response 
    <span id="em2CourseworkProgrammingDescriptionCharCount"><?php echo 3000 - strlen($em2CourseworkProgrammingDescription); ?></span>
    
    <br/>
    <br/>
    Detailed Programming Project Description: 
    <br/>
    <textarea name="em2CourseworkProgrammingDescription2" id="em2CourseworkProgrammingDescription2" class="tblItemRequired"
        cols="60" rows="5" <?php echo $disabled; ?> ><?php echo $em2CourseworkProgrammingDescription2; ?></textarea>
    <br/>
    Characters left in your response 
    <span id="em2CourseworkProgrammingDescription2CharCount"><?php echo 3000 - strlen($em2CourseworkProgrammingDescription2); ?></span>
    <br/>
    <br/>
    <u>CFA paths additional  requirement: experience with maker kits</u>
    <p>1. Please describe your familiarity and number of years' experience developing software and/or hardware
    with specific "arts-engineering toolkits" (e.g visualization programming languages, physical computing 
    platforms, game development environments, audiovisual patching tools, and/or web development libraries). 
    Examples might include (but are not limited to) Arduino, Cinder, Cocoa, D3, DirectX, Actionscript, GLSL, 
    Grasshopper, HTML Canvas, Isadora, JavaScript/DOM, Max/MSP/Jitter, MEL Script, Node.js, OpenGL, 
    openFrameworks, Processing, p5.js, Pure Data, Quartz Composer, RhinoScript, SuperCollider, Touch Designer, 
    Unity3D, VVVV, etc.  </p>
    
    <?php
    $cfadisabled = '';
    if (!$isEm2CfaApplication)
    {
        $cfadisabled = 'disabled="disabled"';    
    }
    ?>
    <textarea name="em2CfaMakerKitsDescription" id="em2CfaMakerKitsDescription" class="tblItemRequired"
        cols="60" rows="5" <?php echo $cfadisabled; ?> ><?php echo $em2CfaMakerKitsDescription; ?></textarea>
    <br/>
    Characters left in your response 
    <span id="em2CfaMakerKitsDescriptionCharCount"><?php echo 3000 - strlen($em2CfaMakerKitsDescription); ?></span>
    
    <br/>
    <p>2. Please describe your special technical proficiencies (apart from programming and physical computing). 
    Include expertise you may have in areas such as (but not limited to) CAD software, animation and 3D software, 
    show control systems, lighting control systems, projection mapping, digital fabrication, rapid prototyping 
    tools, rigging and/or construction, audio or video production, anthropological research methods, etc. </p>
 
    
    <textarea name="em2CfaMakerKitsDescription2" id="em2CfaMakerKitsDescription2" class="tblItemRequired"
        cols="60" rows="5" <?php echo $cfadisabled; ?> ><?php echo $em2CfaMakerKitsDescription2; ?></textarea>
    <br/>
    Characters left in your response 
    <span id="em2CfaMakerKitsDescription2CharCount"><?php echo 3000 - strlen($em2CfaMakerKitsDescription2); ?></span>
    <br/>
    <br/>
 
    <u>Mobility path additional requirements: </u>
    <p>To meet the minimum qualifications necessary to be considered for the Mobility path you must have taken the courses listed below
    in your undergraduate program. If no course information is provided, it will be assumed the applicant does not have the required course
    and the application will not be reviewed.  These courses are pre-requisites to graduate-level courses Mobility students are required to take.</p> 

    <p>Please provide both the course titles and numbers in your undergraduate curriculum that covered the following topics. If your university 
    does not provide course numbers, please enter a 0 in the appropriate space. You must provide the information required. If courses cannot be 
    located on your transcript the review of your application may be delayed. </p> 

    Data Structures:
    <br/>
    <span style="display: inline-block; width: 120px;">Course Title</span>
    <?php
    showEditText($em2CourseworkDataStructuresTitle, "textbox", "em2CourseworkDataStructuresTitle", $_SESSION['allow_edit'], $em2MobilityRequired, NULL, TRUE, 50);     
    ?>
    <br/>
    <span style="display: inline-block; width: 120px;">Course Number</span>
    <?php
    showEditText($em2CourseworkDataStructuresNumber, "textbox", "em2CourseworkDataStructuresNumber", $_SESSION['allow_edit'], $em2MobilityRequired, NULL, TRUE, 50);     
    ?>
    <br/>
    Statistics:
    
    <br/>
    <br/>
    <span style="display: inline-block; width: 120px;">Course Title</span>
    <?php
    showEditText($em2CourseworkStatisticsTitle, "textbox", "em2CourseworkStatisticsTitle", 
        $_SESSION['allow_edit'], $em2MobilityRequired, NULL, TRUE, 50);     
    ?>
    <br/>
    <span style="display: inline-block; width: 120px;">Course Number</span>
    <?php
    showEditText($em2CourseworkStatisticsNumber, "textbox", "em2CourseworkStatisticsNumber", 
        $_SESSION['allow_edit'], $em2MobilityRequired, NULL, TRUE, 50)
    ?>
    
    <br/>
    <br/>
    If no courses have been taken in either data structures or statistics,
    these qualifications may be fulfilled by appropriate professional/industrial experience to be judged by
    the admissions committee. Please explain your experience with either data structures or statistics
    or both:
    <br/>
    <br/>
    <?php
    $mobdisabled = '';
    if (!$isEm2MobilityApplication)
    {
        $mobdisabled = 'disabled="disabled"';    
    }
    ?>
     <textarea name="em2CourseworkMsitExperience" id="em2CourseworkMsitExperience" class="tblItemRequired"
        cols="60" rows="5" <?php echo $mobdisabled; ?> ><?php echo $em2CourseworkMsitExperience; ?></textarea>
    
    <br/>
    Characters left in your response 
    <span id="em2CourseworkMsitExperienceCharCount"><?php echo 3000 - strlen($em2CourseworkMsitExperience); ?></span>
    
    <br/>
    <br/>
    
    <u>SCS paths additional requirements:</u>
    <p>All students in the SCS paths must take the preparation prerequisite 15-513: Introduction to Computer Systems 
    (the summer before admission). Carnegie Mellon offers a distance education course 15-513 Introduction to Computer 
    Systems. A grade of "B" or higher in 15-513 is required. Admitted students take this course via distance education 
    during the summer before attending CMU. Final exam testing for the class occurs after the student arrives on campus 
    in August. Attempting to test out of this course is not encouraged as students who understand this material typically 
    find the exercise of review beneficial. </p>

    
<?php

?>
<br/>
<hr size="1" noshade color="#990000">

<?php
// http://codeimpossible.com/2010/01/13/solving-document-ready-is-not-a-function-and-other-problems/ 
// http://learn.jquery.com/using-jquery-core/avoid-conflicts-other-libraries/  
?>
<script src="../javascript/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$.noConflict();
(function() {
jQuery(document).ready(function() {
    jQuery('#em2CourseworkProgrammingDescription').keyup(function (event) {
        var maxsize = 3000;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#em2CourseworkProgrammingDescriptionCharCount').text(newCharCount);
        }
    });
    
    jQuery('#em2CourseworkProgrammingDescription2').keyup(function (event) {
        var maxsize = 3000;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#em2CourseworkProgrammingDescription2CharCount').text(newCharCount);
        }
    });
    
    jQuery('#em2CfaMakerKitsDescription').keyup(function (event) {
        var maxsize = 3000;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#em2CfaMakerKitsDescriptionCharCount').text(newCharCount);
        }
    });
    
    jQuery('#em2CfaMakerKitsDescription2').keyup(function (event) {
        var maxsize = 3000;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#em2CfaMakerKitsDescription2CharCount').text(newCharCount);
        }
    });
    
    jQuery('#em2CourseworkMsitExperience').keyup(function (event) {
        var maxsize = 3000;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#em2CourseworkMsitExperienceCharCount').text(newCharCount);
        }
    });
});
})(jQuery);
</script>

<?php
/*
* FUNCTIONS 
*/ 

    
function saveEM2Coursework()
{
    global $em2CourseworkDataStructuresTitle;
    global $em2CourseworkDataStructuresNumber;
    global $em2CourseworkStatisticsTitle;
    global $em2CourseworkStatisticsNumber;
    global $em2CourseworkMsitExperience;
    global $em2CourseworkProgrammingDescription;
    global $em2CourseworkProgrammingDescription2;
    global $em2CfaMakerKitsDescription;
    global $em2CfaMakerKitsDescription2;
    global $em2CourseworkError; 
    
    $em2CourseworkDataStructuresTitle = filter_input(INPUT_POST, 'em2CourseworkDataStructuresTitle', FILTER_SANITIZE_STRING);
    $em2CourseworkDataStructuresNumber = filter_input(INPUT_POST, 'em2CourseworkDataStructuresNumber', FILTER_SANITIZE_STRING); 
    $em2CourseworkStatisticsTitle = filter_input(INPUT_POST, 'em2CourseworkStatisticsTitle', FILTER_SANITIZE_STRING); 
    $em2CourseworkStatisticsNumber = filter_input(INPUT_POST, 'em2CourseworkStatisticsNumber', FILTER_SANITIZE_STRING);
    $em2CourseworkMsitExperience = filter_input(INPUT_POST, 'em2CourseworkMsitExperience', FILTER_SANITIZE_STRING);
    $em2CourseworkProgrammingDescription = filter_input(INPUT_POST, 'em2CourseworkProgrammingDescription', FILTER_SANITIZE_STRING);  
    $em2CourseworkProgrammingDescription2 = filter_input(INPUT_POST, 'em2CourseworkProgrammingDescription2', FILTER_SANITIZE_STRING);
    $em2CfaMakerKitsDescription = filter_input(INPUT_POST, 'em2CfaMakerKitsDescription', FILTER_SANITIZE_STRING);  
    $em2CfaMakerKitsDescription2 = filter_input(INPUT_POST, 'em2CfaMakerKitsDescription2', FILTER_SANITIZE_STRING);
    
    if (!$em2CourseworkProgrammingDescription)
    {
        $em2CourseworkError .= 'C/system-level programming project description is required<br>';    
    }
    
    if (!$em2CourseworkProgrammingDescription2)
    {
        $em2CourseworkError .= 'C++/Java programming project description is required<br>';    
    }
    
    if (!$em2CourseworkError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM em2_supporting_coursework 
            WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE em2_supporting_coursework SET
                data_structures_title = '" . mysql_real_escape_string($em2CourseworkDataStructuresTitle) . "',
                data_structures_number = '" . mysql_real_escape_string($em2CourseworkDataStructuresNumber) . "',
                statistics_title = '" . mysql_real_escape_string($em2CourseworkStatisticsTitle) . "',
                statistics_number = '" . mysql_real_escape_string($em2CourseworkStatisticsNumber) . "',
                msit_experience = '" . mysql_real_escape_string($em2CourseworkMsitExperience) . "',
                programming_description = '" . mysql_real_escape_string($em2CourseworkProgrammingDescription) . "',
                programming_description2 = '" . mysql_real_escape_string($em2CourseworkProgrammingDescription2) . "',
                makerkits_description = '" . mysql_real_escape_string($em2CfaMakerKitsDescription) . "',
                makerkits_description2 = '" . mysql_real_escape_string($em2CfaMakerKitsDescription2) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO em2_supporting_coursework 
                (application_id, data_structures_title, data_structures_number, 
                    statistics_title, statistics_number, msit_experience, programming_description, 
                    programming_description2, makerkits_description, makerkits_description2)
                VALUES (" 
                . intval($_SESSION['appid']) . ",'" 
                . mysql_real_escape_string($em2CourseworkDataStructuresTitle) . "','" 
                . mysql_real_escape_string($em2CourseworkDataStructuresNumber) . "','" 
                . mysql_real_escape_string($em2CourseworkStatisticsTitle) . "','" 
                . mysql_real_escape_string($em2CourseworkStatisticsNumber) . "','" 
                . mysql_real_escape_string($em2CourseworkMsitExperience) . "','"
                . mysql_real_escape_string($em2CourseworkProgrammingDescription) . "','" 
                . mysql_real_escape_string($em2CourseworkProgrammingDescription2) . "','"
                . mysql_real_escape_string($em2CfaMakerKitsDescription) . "','" 
                . mysql_real_escape_string($em2CfaMakerKitsDescription2) . "')";
            mysql_query($insertQuery);
        }
    }
    else
    {
        $em2CourseworkError .= ' Changes will not be saved until all required fields are completed.';
    } 
}

function checkEM2CourseworkRequirements()
{
    global $em2CourseworkError;     
    
    if (!$em2CourseworkError)
    {
        // Check for transcripts
        $transcriptQuery = "SELECT id, datafile_id FROM usersinst 
            WHERE application_id = " . $_SESSION['appid'];     
        $transcriptResult = mysql_query($transcriptQuery);                
        while ($row = mysql_fetch_array($transcriptResult))
        {
            if (!$row['datafile_id'])
            {
                // There is an institute without a transcript, so page is incomplete.
                updateReqComplete("uni.php", 0);
                return;
            }    
        }
        
        // All institutes have a transcript, so page is complete.
        updateReqComplete("uni.php", 1);
        return;   
    }
    
    // Page is incomplete.
    updateReqComplete("uni.php", 0);
}
?>
