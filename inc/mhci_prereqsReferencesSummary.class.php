<?php

class MHCI_PrereqsReferencesSummary 
{

    private $studentLuUsersUsertypesId;
    private $referenceData;
    
    private $anovaValues = array(
                    "No knowldege" => 
                        "I have no direct knowledge of this applicant's understanding of 
                        multi-way ANOVA.",
                    "Not confident" => 
                        "I am not confident that the applicant understands multi-way ANOVA 
                        in sufficient detail to knowledgeably read research literature.",
                    "Direct knowledge" => 
                        "I have direct knowledge of this applicant's understanding of 
                        multi-way ANOVA. He or she understands when it is appropriate 
                        to use this test as opposed to other tests and understands how 
                        to appropriately interpret this test, including an understanding 
                        of interactions."
                    );
                
    private $regressionValues = array(
                    "No knowldege" => 
                        "I have no direct knowledge of this applicant's understanding of 
                        multi-factor regression.",
                    "Not confident" => 
                        "I am not confident that the applicant understands multi-factor regression 
                        in sufficient detail to knowledgeably read research literature.",
                    "Direct knowledge" => 
                        "I have direct knowledge of this applicant's understanding of 
                        multi-factor regression. He or she understands when it is appropriate 
                        to use this test as opposed to other tests and understands how 
                        to appropriately interpret this test, including an understanding 
                        of interactions."
                    );
    
    
    public function __construct($studentLuUsersUsertypesId, $view) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;
        
    }
    
    
    public function render($disable) { 
    
        $this->getReferences();
        
        $i = 1;
        foreach($this->referenceData as $referenceArray) {
            
            echo <<<EOB

            <div class="sectionTitle">Reference {$i} </div>
                <br/><br/>
                <div id="referenceFirst" class="bodyText">First Name</div>
                <div id="referenceLast" class="bodyText">Last Name</div>

                <div id="referenceJob" class="bodyText">Job Title</div>
                <br/>
                    <div id="referenceFirstText" class="bodyText">{$referenceArray['firstname']}</div>
                    <div id="referenceLastText" class="bodyText">{$referenceArray['lastname']}</div>
                    <div id="referenceJobText" class="bodyText">{$referenceArray['title']}</div>
                <br/><br/>
                <div id="referenceComp" class="bodyText">Institution</div>

                <div id="referenceEmail" class="bodyText">Email</div>
                <div id="referencePhone" class="bodyText">Phone</div>
                <br/>
                    <div id="referenceCompText" class="bodyText">{$referenceArray['company']}</div>
                    <div id="referenceEmailText" class="bodyText">{$referenceArray['email']}</div>
                    <div id="referencePhoneText" class="bodyText">{$referenceArray['address_cur_tel']}</div>

                <br/><br/>
EOB;

            if ( $referenceArray['submitted'] == 1 ) {
                
                echo <<<EOB
                
                <br/>
                <div class="bodyText-bold">This reference has responded:</div> 
                <br/>
                <div class="bodyText">
                    <b>Multi-way ANOVA</b>: 
                    {$this->anovaValues[$referenceArray['anova_knowledge']]}
                    <br>
                    <b>Comment</b>: 
                    {$referenceArray['anova_comment']}
                </div>
                <br/>
                <div class="bodyText">
                    <b>Multi-factor regression</b>: 
                    {$this->regressionValues[$referenceArray['regression_knowledge']]}
                    <br />
                    <b>Comment</b>: 
                    {$referenceArray['regression_comment']}
                </div>
                <br/>
                
EOB;
            
            } else {
                
                if ( $referenceArray['reminder_sent_count'] > 0 ) {

                    echo <<<EOB
                    
                    <div class="bodyText-bold">An email has been sent to this reference.</div>

EOB;
                    
                } else {
                    
                    echo <<<EOB
                    
                    <div class="bodyText-bold">An email has not been sent to this reference.</div>

EOB;
                    
                }
                
                
                
            }
                

                
                
            echo '<hr class="hr"/>';
            
            $i++;
            
        }
        
        return TRUE;   
    
    }
    
    
    private function getReferences() {
        
        $query = <<<EOB
        
        SELECT mhci_prereqsReferences.*,
        users.email,
        users.firstname,
        users.lastname,
        users.title,
        users_info.company,
        users_info.address_cur_tel,
        datafileinfo.moddate
        FROM mhci_prereqsReferences
        inner join application on application.id = mhci_prereqsReferences.application_id
        inner join lu_users_usertypes on lu_users_usertypes.id = mhci_prereqsReferences.ref_user_id
        inner join users on users.id = lu_users_usertypes.user_id
        INNER JOIN users_info ON users_info.user_id = lu_users_usertypes.id
        left outer join datafileinfo on datafileinfo.id = mhci_prereqsReferences.datafile_id

EOB;

    $query .= "WHERE application.user_id=". $this->studentLuUsersUsertypesId;    
    //echo $query . "<br />";
    
    $result = mysql_query($query);
    
    $this->referenceData = array();
    while ($row = mysql_fetch_array($result)) {
        
        $this->referenceData[] = $row;
        
    }
    
    return TRUE;
    
    }
    
    
}
  
?>
