<?php

class MHCI_PrereqsCodeSampleForm
{

    // Constructor parameters.
    private $studentLuUsersUsertypesId;
    private $view;                      // "student" || "reviewer"
    
    // Fields set in constructor
    private $prereqPrefix;
    private $prereqName;
    
    // Fields for form/db data.
    private $sampleId;
    private $documentType = "CodeSample";
    private $newUploadDatafileinfoId;
    private $newUploadNote;
    private $uploadInfo = array();    // 2D array filled from db query.

    // Fields for handling display.
    private $formSubmitted = FALSE;
    
    private $uploadButtonLabel = "Upload code";
    private $formHeading = "Code Sample";
    private $docNoteLabel = "Code Description for the faculty reviewer:";
    private $submitLabel = "Submit this code to the reviewer";
    private $uploadButtonDisabled = "";
    private $submitButtonDisabled = "disabled";     // Enabling is handled with renderSubmitButton() and javascript.

       
    function __construct($studentLuUsersUsertypesId, $view) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;  
        $this->prereqPrefix = "codeSample";
        $this->prereqName = "Code sample";
        
    } 
    
    public function getUploadInfo() {
         return $this->uploadInfo;
    }
    
    function render() {
        global $assessmentForm;
              
        $this->renderHeading();
        
        // If reviewer view, get data and return summary.
        if ($this->view == "reviewer") {
            
            $this->getSample();
            
            if ($this->sampleId) {
                
                $this->renderUploadSummary(); 
                
            }
            
            return TRUE;
            
        }
        
        // OTHERWISE, render the student view.
        
        $this->renderIntro();
         // DebugBreak();
          $this->getSample();
        // Get request data.
        $this->handleRequest();
        // IF the form has been SUBMITTED 
        
        if ($this->formSubmitted || isset($_REQUEST['TestSave'])){
            
            // The complete form subset has been submitted.
       //     echo "submission complete<br /><br />";
   //         debugBreak();
            // Insert the new data into db.
            $this->addSample();
            
            // Get the complete data from the db.
            $this->getSample();
            
            // Render the summary.
      //      $this->uploadButtonDisabled = 'disabled="disabled"';
            $this->renderUploadButton();
            $this->renderUploadSummary();
            
            return TRUE;
            
        }
            
        // OTHERWISE, Check whether it's a post-upload request.
        
        
        // Get the last sample record.
        $this->getSample();
        $lastUploadInfo = end($this->uploadInfo);
        
        if ( ($lastUploadInfo['new_file_uploaded'] == 1) && ($lastUploadInfo['sample_id'] == $this->sampleId) ) {
            
            // A new file has been uploaded.
            echo "new upload<br /><br />";
            
            // Get any form data that was entered before the upload from the SESSION
            //$this->courseId = $_SESSION[$this->prereqPrefix . '_sampleId'];
            //$this->newUploadNote = $_SESSION[$this->prereqPrefix . '_codeDescription']; 
                    
            // Set the new upload fields.
            $this->newUploadDatafileinfoId = $lastUploadInfo['id'];
            $this->newUploadNote = $lastUploadInfo['note'];
            
            // Render the summary.
            $this->uploadButtonDisabled = 'disabled="disabled"';
            $this->renderUploadButton();
            $this->renderUploadSummary(); 
            
            // Render the upload note for editing.
            $this->renderUploadNoteEdit();     
            
        } else {
            
            // Check DB for data.
            $this->getSample();
            
            if ($this->sampleId) {
                
                // The form subset was submitted previously and is complete for the time being.
             //   echo "returning to code sample<br /><br />";
                
                // Render the summary.
                $this->renderUploadSummary();
                
            } else {
                
                // This form subset has not been started.
              //  echo "new code sample<br /><br />";
                                   
                // Render the upload summary.
                $this->renderUploadButton();
                $this->renderUploadSummary();
                
                // Render the upload note for editing.
                $this->renderUploadNoteEdit();    
                   
            }     
            
        }        
            

    
        // Always render the submit button for the form subset.
        // Logic for activating/deactivating the button is handled with javascript.
     //   DebugBreak();
       // $this->renderSubmitButton();
        
        return TRUE;  
        
    }

    
    private function handleRequest() {
        // DebugBreak();
        if ( isset($_REQUEST[$this->prereqPrefix . '_sampleId']) ) {
            
            $this->sampleId = $_REQUEST[$this->prereqPrefix . '_sampleId']; 
            
        }
                
        if ( isset($_REQUEST[$this->prereqPrefix . '_newDatafileId']) ) {
            
            $this->newUploadDatafileinfoId = $_REQUEST[$this->prereqPrefix . '_newDatafileId']; 
            
        }
        
        if ( isset($_REQUEST[$this->prereqPrefix . '_codeDescription']) ) {
            
            $this->newUploadNote = $_REQUEST[$this->prereqPrefix . '_codeDescription']; 
            
        }
        
        
        if ( isset($_REQUEST[$this->prereqPrefix . '_codeSampleSubmit']) ) {
            
            $this->formSubmitted = TRUE;
            
        }
        
        return TRUE;
        
    }
    

    private function getSample() {
        // DebugBreak();
        $query = "SELECT datafileinfo.*, lu_users_usertypes.user_id AS usermasterid, 
                    mhci_prereqsProgrammingSamples.id as sample_id,
                    mhci_prereqsProgrammingSamples.note, mhci_prereqsProgrammingSamples.submitted_to_reviewer,
                    mhci_prereqsProgrammingSamples.new_file_uploaded  
                    FROM datafileinfo 
                    INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
                    LEFT OUTER JOIN mhci_prereqsProgrammingSamples
                    ON datafileinfo.id = mhci_prereqsProgrammingSamples.datafileinfo_id";
        $query .= " WHERE datafileinfo.user_id = " . $this->studentLuUsersUsertypesId;
        $query .= " AND datafileinfo.section = 17
                    ORDER BY datafileinfo.id";
        //echo $query . "<br />";
        //  DebugBreak();
        $result = mysql_query($query);   
        
        $dataArray = array();
        while ($row = mysql_fetch_array($result)) {
            
            $this->sampleId = $row['sample_id'];
            $dataArray[] = $row;
            
        }
        $this->uploadInfo = $dataArray;
        
    }

    
    private function addSample() {
        $query = "INSERT INTO mhci_prereqsProgrammingSamples";
        
        if ($this->sampleId) {
            
            $query .= " (id, student_lu_users_usertypes_id, datafileinfo_id, note)";
            $query .= sprintf(" VALUES (%d, %d, %d, '%s')
                        ON DUPLICATE KEY UPDATE submitted_to_reviewer = 1, 
                        new_file_uploaded = 0, 
                        note = '%s'",
                        $this->sampleId,
                        $this->studentLuUsersUsertypesId,
                        $this->newUploadDatafileinfoId, 
                        mysql_real_escape_string($this->newUploadNote),
                        mysql_real_escape_string($this->newUploadNote)
                        );     
            
        } else {

            $query .= " (student_lu_user_id, datafileinfo_id, note)";
            $query .= sprintf(" VALUES (%d, %d, '%s')",
                        $this->studentLuUsersUsertypesId,
                        $this->newUploadDatafileinfoId,
                        mysql_real_escape_string($this->newUploadNote)
                        );    
            
        }
        //echo $query . "<br />";
        
        $result = mysql_query($query);
        
        if ($result) {
            
            $linkname = "mhci_prereqs_programming.php";
            updateReqComplete($linkname, 0);    
            
        }
        
        return mysql_insert_id();        
           
    }
    
    
    private function renderHeading() {
        
        echo '<div class="sectionTitle">' . $this->formHeading . '</div><br/><br/>'; 

    }


    private function renderIntro() {
        
        echo <<<EOB

        <div class="bodyText">
        If you have independently written a program of over 300 lines in a high-level 
        language such as Java or C++ in the last two years, you can upload that code 
        here to fulfill the programming placeout. If your program includes more 
        than one file, please upload a zip file. Be sure that your code is documented 
        and include a description of your program, a trace of the code running, and 
        instructions for running it.
        </div><br/>

EOB;
 
    }
    
    
    private function renderUploadButton() {

        echo <<<EOB

        <button id="{$this->prereqPrefix}_uploadDoc" class="bodyButton docUploadButton" onClick="uploadCodeSample(this);return false;" 
        {$this->uploadButtonDisabled} >{$this->uploadButtonLabel}</button> 
        <br/><br/> 

EOB;
        
    }
    
    
    private function renderUploadSummary() {
        
        foreach ($this->uploadInfo as $uploadArray) {
            
            $studentGuid = getGuid($uploadArray['usermasterid']);
            $pathFilename = $this->documentType . "_" . $uploadArray['userdata'] . "." . $uploadArray['extension'];
            $displayFilename = $this->documentType . "." . $uploadArray['extension'];
            //$path = $_SESSION['datafileroot'] . "/" . getGuid($_SESSION['usermasterid']) . "/" . $this->documentType . "/" . $pathFilename;
            $path = $_SESSION['datafileroot'] . "/" . $studentGuid . "/" . $this->documentType . "/" . $pathFilename;
    
            echo <<<EOB

            <div class="bodyText-bold fileFirstLine">File Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File Size&nbsp;&nbsp;&nbsp;Last Modified</div>
            <div class="bodyText fileSecondLine"><a href="" onClick="window.open('{$path}'); return false;">{$displayFilename}</a> 
            &nbsp;&nbsp;&nbsp;{$uploadArray['size']}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$uploadArray['moddate']}</div>
            <div class="bodyText fileThirdLine">(Please review your uploaded file by clicking the link above.)</div>
            <br/>

EOB;

        if ( isset($uploadArray['note']) && ($uploadArray['note'] != "") && $uploadArray['submitted_to_reviewer']) {
            echo "<strong>" . $this->prereqName . "</strong>: " . $uploadArray['note'];
        }
        
        echo "<br/><br/>";

}

        
        
        
    }
    
    
    private function renderUploadNoteEdit() {
        
        echo <<<EOB

        <div class="bodyText"><strong>{$this->docNoteLabel}</strong></div>
        <input type="hidden" id="{$this->prereqPrefix}_studentId" name="{$this->prereqPrefix}_studentId" value="{$this->studentLuUsersUsertypesId}" />
        <input type="hidden" id="{$this->prereqPrefix}_sampleId" name="{$this->prereqPrefix}_sampleId" value="{$this->sampleId}" />
        <input type="hidden" id="{$this->prereqPrefix}_newDatafileId" name="{$this->prereqPrefix}_newDatafileId" class="requiredData" value="{$this->newUploadDatafileinfoId}" />
        <textarea id="{$this->prereqPrefix}_codeDescriptionInput" name="{$this->prereqPrefix}_codeDescription" class="commentArea requiredData">{$this->newUploadNote}</textarea>
        <br/><br/>
    
EOB;
        
    }
    
    
    private function renderSubmitButton() {

        // Enable the button if all required field values are filled in the REQUEST.
        // Activating/deactivating the button in other circumstances is handled by mhci_prereqs.js
        if ($this->newUploadDatafileinfoId && $this->newUploadNote && !$this->formSubmitted) {
                    
                    $this->submitButtonDisabled = "";    
            
        }

        echo <<<EOB

        <input type="submit" id="{$this->prereqPrefix}_codeSampleSubmit" name="{$this->prereqPrefix}_codeSampleSubmit" 
        class="bodyButton-right" value="{$this->submitLabel}" {$this->submitButtonDisabled}/>
        <br /><br />

EOB;
        
    }
 
    
} 

?>