<?php

class MHCI_PrereqsConversationForm
{

    // Fields set in constructor.
    private $studentLuUsersUsertypesId;
    private $commenterLuUsersUsertypesId;
    private $view;                          // "student" || "reviewer"
    private $prereqType;                    // "design" || "programming" || "statistics"
    private $prereqName;                    // ucfirst($prereqType)    
    
    // Fields set with form/db data.
    private $prereqId;
    private $newComment;
    private $comments = array();      // 2D array filled from db
    
    // Fields for rendering form.
    private $formSubmitted = FALSE;
    
    private $studentHeading = "Send Comments to Faculty Reviewer: (optional)";
    private $reviewerHeading = "Conversation History";
    private $studentSubmitButtonLabel = "Send to reviewer";
    private $reviewerSubmitButtonLabel = "Send to student";
    private $submitButtonLabel = "Send";
    private $studentCommentLabel = "Student wrote:";
    private $reviewerCommentLabel = "Faculty wrote:";
    
    
    function __construct($studentLuUsersUsertypesId, $commenterLuUserUsertypesId, $view, $prereqType) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->commenterLuUsersUsertypesId = $commenterLuUserUsertypesId;
        $this->view = $view;
        $this->prereqType = $prereqType;
        $this->prereqName = ucfirst($prereqType);      
        
    }
    
    public function setFormSubmitted ($value) {
        $this->formSubmitted = $value;
        return $this->formSubmitted;
    }
    
    function render($disabled) {
        // DAS DebugBreak();
        $this->getComments();
        $this->renderHeading();
        $this->renderIntro();
        
        // Get the request data.
        $this->handleRequest();

        
        if (!$this->prereqId) {
            
            // The id wasn't sent in the request.
            // Get it from the db.
            $this->getPrereqId();
            
        }
        
        
        if (($this->formSubmitted || (isset($_REQUEST['TestSave']) || isset($_REQUEST['TestSubmit']))) && $this->newComment != "") {
        
            // A comment has been submitted.
            //echo "new comment<br /><br />";
      //      debugbreak();
            // Insert the data into the db.
            $this->addComment();
                    
        }
                            
        // Get the comments data from the db.
        $this->getComments();
        
        // Render the form.
        $this->renderForm();
        
        // Render the summary
        $this->renderSummary();
    }
    
    
    private function handleRequest() {
  //      DebugBreak();
        
        if ( isset($_REQUEST[$this->prereqType . '_conversationPrereqId']) ) {
        
            $this->prereqId = $_REQUEST[$this->prereqType . '_conversationPrereqId'];    
            
        }
        
        if ( isset($_REQUEST[$this->prereqType . '_commentTextarea']) ) {
        
            $this->newComment = $_REQUEST[$this->prereqType . '_commentTextarea'];    
            
        }
        
        if ( isset($_REQUEST[$this->prereqType . '_submitComment']) ) {
        
            $this->formSubmitted = TRUE;    
            
        }
        
        return TRUE;
        
    }
    
    
    
    
    private function getPrereqId() {
        
        $query = "SELECT id from mhci_prereqs
                    WHERE student_lu_users_usertypes_id = {$this->studentLuUsersUsertypesId} 
                    AND prereq_type = '{$this->prereqType}'";

        $result = mysql_query($query);
        
        while ($row = mysql_fetch_array($result)) {
            
            $this->prereqId = $row['id'];

        }  
        
    }


    private function getComments() {
        
        $query = "SELECT mhci_prereqsConversationComments.* FROM mhci_prereqsConversationComments
                    INNER JOIN mhci_prereqs ON
                    mhci_prereqsConversationComments.prereq_id = mhci_prereqs.id
                    WHERE mhci_prereqs.id = {$this->prereqId}
                    AND mhci_prereqs.prereq_type = '{$this->prereqType}'
                    ORDER BY timestamp DESC";
        

        $result = mysql_query($query);
        if ($result != FALSE) {

            while ($row = mysql_fetch_array($result)) {
            
                $this->comments[] = $row;

                }
        }
          
    }


    private function addComment() { 
        
        $query = "INSERT INTO mhci_prereqsConversationComments            
                    (prereq_id, lu_users_usertypes_id, comment, timestamp)";
        if ($this->view == "reviewer") {
            $query .= sprintf(" VALUES (%d, %d,  '%s', NOW())",
                        $this->prereqId,
                        $_SESSION['A_userid'],
                        mysql_real_escape_string($this->newComment)
                        );
            } else {           
                $query .= sprintf(" VALUES (%d, %d,  '%s', NOW())",
                                $this->prereqId,
                                $this->commenterLuUsersUsertypesId,
                                mysql_real_escape_string($this->newComment)
                                );
            }
        //VALUES ('{$this->prereqId}', {$this->commenterLuUsersUsertypesId}, '{$this->newComment}', NOW())"; 

        $result = mysql_query($query);
        
        if ($result && ($this->view == "student")) {
            
            $linkname = "mhci_prereqs_" . $this->prereqType . ".php";
            updateReqComplete($linkname, 0);    
            
        }
        
        return mysql_insert_id();     
    }
    
    
    private function renderHeading() {
        
        if ($this->view == "reviewer") {
            
            $heading = $this->reviewerHeading;
            
        } else {
            
            $heading = $this->studentHeading;
            
        }
        
        echo <<<EOB
        
        <div id="feedbackTitle" class="sectionTitle">
        {$heading}
        </div><br/><br/>

EOB;
            
    }
    
    
    private function renderIntro() {
        
        if ($this->view == "student") {
            
            echo <<<EOB

            <div class="bodyText">
            Use this area to start a conversation with the faculty member responsible for evaluating your {$this->prereqType} placeout.
            </div>
            <br/>
            
EOB;
        }
        
    }
    
    
    private function renderForm() {  
        
        echo <<<EOB

        <input type="hidden" id="{$this->prereqType}_conversationPrereqId" name="{$this->prereqType}_conversationPrereqId" value="{$this->prereqId}" />
        <textarea name="{$this->prereqType}_commentTextarea" class="commentArea"></textarea><br/>
<!--       <input type="submit" id="{$this->prereqType}_submitComment" name="{$this->prereqType}_submitComment" class="bodyButton-right" value="{$this->submitButtonLabel}" />
         
        <button class="bodyButton-right">Send</button>
        -->
        <br/><br/>

EOB;
        
    }    

    
    private function renderSummary() {
    
        foreach($this->comments as $comment) {
            if ($comment['lu_users_usertypes_id'] == $this->studentLuUsersUsertypesId) {
                
                $commentLabel = $this->studentCommentLabel;
                
            } else {
                
                $commentLabel = $this->reviewerCommentLabel;
                
            }
            
            $commentText = htmlentities($comment['comment']); 
            
            echo <<<EOB
            
            <div class="bodyText feedbackLabel1">
                {$comment['timestamp']}
            </div>
            <div class="bodyText">
                <strong>{$commentLabel}</strong>&nbsp;{$commentText}
            </div>
            <br/>
            
EOB;
            
        }
        
        
        
    }

}

  
?>
