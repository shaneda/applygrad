<?php
/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Admission Periods';

$pageCssFiles = array(
    '../css/units.css',
    '../css/ui.datepicker.css',
    '../css/jquery.tooltip.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/ui.core.js',
    '../javascript/ui.datepicker.min.js',
    '../javascript/jquery.tooltip.min.js',
    '../javascript/units.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';
?>
<br/>
<div id="bread" class="menu">
<ul>
    <li class="first"><a href="home.php">Home</a>
    <ul>
        <li>&#187; <a href="units.php?unit_id=<?php echo $unitId; ?>">Units</a> 
        <ul>
            <li>&#187;</li>
        </ul>
        </li>
    </ul>
    </li>
</ul>
</div>
<div id="unitName"><?php echo $unit->getName(); ?></div>
<div id="pageTitle"><?php echo $pageTitle; ?></div>


<div style="margin: 0px; border-right: 1px solid black; float: left; clear: right; width: 20%">
<?php 
if ($allowAdmin) {
    $newPeriodForm->display();    
}  
$periodMenu = makePeriodMenu($unit, $umbrellaPeriodId);
echo $periodMenu;
?>
</div>

<div id="table" style="margin: -5px 0px 0px 20px; float: left; clear: right; width: 75%;">

        <div style="margin: 10px;">
        <?php
        if (!$allowAdmin) {
        
            echo '<p>You are not authorized to admission periods.</p>';
            
        } elseif ($umbrellaPeriodId || $edit == 'newPeriod') {

            // Change the renderer templates to make each form have the same column widths.
            $renderer = $umbrellaForm->defaultRenderer(); 
            $renderer->setFormTemplate(
               '<form{attributes}>
                  {hidden}
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                  {content}
                  </table>
               </form>'
            );
            $renderer->setElementTemplate(
            '<tr valign="top">
                <td width="30%" align="right">
                    <!-- BEGIN required --><span style="color: #ff0000;">*</span><!-- END required --><b>{label}</b>
                </td>
                <td width="70%" align="left">
                    <!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->{element}
                </td> 
            </tr>'  
            );
            ?> 
                          
            <!--
            <div id="sectionHeading">
            <?php
            if ($umbrellaPeriodId) {
                $startDate = $umbrellaPeriod->getStartDate('Y-m-d');
                $endDate = $umbrellaPeriod->getEndDate('Y-m-d');
                echo $umbrellaPeriod->getDescription() . ' (' . $startDate . ' to ' . $endDate . ')';
                      
            } elseif ($edit == 'newPeriod') {
                echo 'Add New Period';    
            }
            ?>
            </div>
            -->
            
            <table width="100%" border="0" cellspacing="5" cellpadding="10">
            <tr valign="top">
            <td width="50%">
                
                <div class="legend">Admission Period</div>
                <div class="outer">
                <div class="fieldset">
                <?php
                $umbrellaForm->display();
                ?>
                </div>
                </div>           

            <?php
            
            if ($edit != 'newPeriod') {
            ?>

                <br/>
                <div class="legend">Submit Applications</div>
                <div class="outer">
                <div class="fieldset">
                    <!--
                    <div style="float: right;">
                        <button>Manage Periods</button>
                    </div>
                    -->
                    <div>
                    <?php
                    $submissionPeriodForm->display();
                    ?>
                    </div>        
                </div>
                </div>
            <?php
            }
            ?>

            <?php
            if ($edit != 'newPeriod' && isset($editingPeriodForm)) {
            ?>

                <br/>
                <div class="legend">Edit Applications</div>
                <div class="outer">
                <div class="fieldset">
                    <div>
                    <?php
                    $editingPeriodForm->display();
                    ?>
                    </div>        
                </div>
                </div>
            <?php
            }
            
            // Viewing subperiod not currently used.
            /*
            if ($edit != 'newPeriod' && isset($viewingPeriodForm)) {
            ?>

                <br/>
                <div class="legend">View Applications</div>
                <div class="outer">
                <div class="fieldset">
                    <div>
                    <?php
                    $viewingPeriodForm->display();
                    ?>
                    </div>        
                </div>
                </div>
            <?php
            }
            */
            
            
            if ($edit != 'newPeriod' && isset($placeoutPeriodForm)) {
            ?>

                <br/>
                <div class="legend">Placeout Period</div>
                <div class="outer">
                <div class="fieldset">
                    <div>
                    <?php
                    $placeoutPeriodForm->display();
                    ?>
                    </div>        
                </div>
                </div>
            <?php
            }
            ?> 
                                     

            </td>
            <td width="50%">

            <?php
            // Program groups/roles not currently used.
            /*
            if ($edit != 'newPeriod') {
            ?>
                <div class="legend">Program Groups and Roles</div>            
                <div class="outer">
                <div class="fieldset" style="text-align: right; padding: 10px; padding-right: 20px;">
                    <div class="menu" style="float: right; text-align: right; padding-right: 20px;">
                        <a href="periodPrograms.php?period_id=<?php echo $umbrellaPeriodId; ?>">Manage Program Groups and Roles</a> 
                    </div>
                    <br/>
                </div>
                </div>
            <?php
            }
            */
            ?>
            
            <?php
            if ($edit != 'newPeriod') {
            ?>
                <!--
                <br/>
                -->
                <div class="legend">Application Programs</div>            
                <div class="outer">
                <div class="fieldset">
                <?php
                $periodProgramForm->display();
                ?>
                </div>
                </div>
            <?php
            }
            ?>
            
              
            </td>
            </tr>
            </table>
        <?
        }        
        ?>        
        </div>

</div>

<div style="clear: both;"></div>
<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';


//########################################################################

function makePeriodMenu($unit, $headingPeriodId = NULL, $displayLinks = TRUE) {
    
    $unitId = $unit->getId();
    $periods = $unit->getPeriods(1);
    $periodCount = count($periods);
    $menu = '';
    
    if ($periodCount > 0) {
        $menu .= '<ul class="menu menu-root" style="list-style-type: none;">';    
    }
    $i = 1;
    foreach ($periods as $period) {
        
        if ($period->getPeriodTypeId() != 1) {
            continue;
        }
        
        $periodId = $period->getId();
        $periodDescription = $period->getDescription();
        $startDate = $period->getStartDate('Y-m-d');
        $endDate = $period->getEndDate('Y-m-d');
        $status = $period->getStatus();
        
        if ($periodId == $headingPeriodId) {
            $periodAnchor = '<div class="selected">';    
        } else {
            $periodAnchor = '<div>';    
        }
        
        $periodAnchor .= '<a title="' . $startDate . ' to ' . $endDate . ' (period id: ' . $periodId . ')"';
        
        if (!$displayLinks || ($periodId == $headingPeriodId) ) {
            $periodAnchor .= '>';
        } else {
            $periodAnchor .= ' href="?unit_id=' . $unitId . '&period_id=' . $periodId . '">';
        }
        
        $periodAnchor .= $periodDescription . '</a></div>';
        $menu .= '<li>' . $periodAnchor . '</li>';
    
        $i++;
    }
    if ($periodCount > 0) {
        $menu .= '</ul>';    
    }
    
    return $menu;  
}

?>