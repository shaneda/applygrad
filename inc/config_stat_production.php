<?php
if(strstr($_SERVER['SCRIPT_NAME'], 'offline.php') === false)
{
//header("Location: ../apply/offline.php");
}

/*
CONSTANT VALUES FOR THE APPLICATION
*/
//set_time_limit(0);
$environment="production";

$db_host="localhost";
$db_username="phdapp";
$db_password="phd321app";
$db="gradAdmissions2009New";

//$datafileroot = "../../data/".date("Y");
$datafileroot = "../data/2009";

$dontsendemail = FALSE;
$admissionsContact = "scsstats+admissions@cs.cmu.edu"; 
$paymentEmail = "scsstats+payment@cs.cmu.edu";
$supportEmail = "scsstats+technical@cs.cmu.edu";

// $paymentProcessor = "https://ccard-submit.as.cmu.edu/cgi-bin/gather_info.cgi"; //PRODUCTION USING GENERIC COLLECTOR
//$paymentProcessor = "https://ccard-test.as.cmu.edu/cgi-bin/gather_store.cgi"; //TEST
//$paymentProcessor = "https://acis.as.cmu.edu:4443/cc/gather_info.cgi"; //TESTING USING GENERIC COLLECTOR
$paymentProcessor = "https://commerce.cashnet.com/"; //PRODUCTION

if ( strnatcmp( phpversion(),'5.3.1' ) >= 0 ) {
 //   $magic_location = "/usr0/wwwsrv/htdocs/inc/magic-php-5.3"; 
    $magic_location = "/usr/share/file/magic";
    error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);    
} else {
    $magic_location = "/usr0/wwwsrv/htdocs/inc/magic";
    error_reporting(E_ALL ^ E_NOTICE);    
}

ini_set('display_errors', 0);                                                                                                                                                                                   
?>