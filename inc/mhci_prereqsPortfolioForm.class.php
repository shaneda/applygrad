<?php

class MHCI_PrereqsPortfolioForm
{

    // Fields set in constructor.
    private $studentLuUsersUsertypesId;
    private $view;                          // "student" || "reviewer"
    private $prereqPrefix;                  // "designPortfolio"
    private $prereqName;                    // "Website or online portfolio"
    private $applicationId;
    private $placeoutPeriodId;
                           
    
    // Fields for form/db data.
    private $portfolioId;
    private $portfolioUrl;
    private $portfolioDescription;
    
    // Fields for display elements.
    private $formSubmitted = FALSE;
    
    private $studentIntro = "If you have an online portfolio or website, please list the URL here:";
    private $portfolioUrlLabel = "Portfolio URL";
    private $portfolioDescriptionLabel = "Description";
    // private $submitLabel = "Submit to the reviewer";
    
    
    public function __construct($studentLuUsersUsertypesId, $view) {
    
        $this->studentLuUsersUsertypesId = $studentLuUsersUsertypesId;
        $this->view = $view;
        $this->prereqPrefix = "designPortfolio";
        $this->prereqName = "Website or Online Portfolio";
        $this->applicationId = $_SESSION['application_id'];
        $this->placeoutPeriodId = $_SESSION['activePlaceoutPeriod'];
    }
    
    public function getPortfolioURL() {
        return $this->portfolioUrl;
    }
    
    public function setFormSubmitted ($value) {
        $this->formSubmitted = $value;
        return $this->formSubmitted;
    }

    
    public function render($disable) {
        
        // Render heading for both views.
        $this->renderHeading();
        $this->getPortfolio();
        // Get the request data.
        $this->handleRequest();
        
        // If reviewer view, get data and return summary.
        if ($this->view == "reviewer" || $disable == 'bogus') {
            
            $this->getPortfolio();
            
            if ($this->portfolioId) {
                
                $this->renderSummary(); 
                
            }
            
            return TRUE;
            
        }
        
        // Otherwise, render the student view.
        
        $this->renderStudentIntro();
//        DebugBreak();

        if ((isset($_REQUEST['TestSave']) && $this->portfolioUrl != "") || ($this->formSubmitted && $this->portfolioUrl != "")) {
            // debugBreak();
            // The complete form subset has been submitted.
            // Insert the data into the db.
            $this->portfolioId = $this->addPortfolio();
            
            // Render as a summary.
            $this->renderSummary();
            
                    
        } else {
            
            // The student is either returning to or starting a new form subset.
            
            if (!$this->portfolioId) {
                
                // Data are not available from REQUEST
                // Check the db for the data.
                $this->getPortfolio();
                
                if (!$this->portfolioId) {
                
                    // Data not in db; the student is starting a new form subset
                    // Render form for editing.
                    $this->renderForm(); 
                
                } else {
                    
                    // The data are in the db; the student is returning.
                    // Render as a summary;
                        $this->renderSummary();
                    
                } 
                
            } else {
                if (isset($_REQUEST['designPortfolio_changePortfolio'])) {
                      //  DebugBreak();
                          $this->renderForm();
                    } else {
                   //     DebugBreak();
                        if ($this->formSubmitted)  {
                          // The data are in the db; the student is returning.
                    // Render as a summary;
                        $this->renderSummary();
                        } else {
                            $this->renderForm();
                        }
                    }
            }
        
        }
        
        return TRUE;
 
    }
    

    private function handleRequest() {
        
        if ( isset($_REQUEST[$this->prereqPrefix . '_portfolioId']) ) {
            
            $this->assessmentId = $_REQUEST[$this->prereqPrefix . '_portfolioId'];    
            
        }
        
        if ( isset($_REQUEST[$this->prereqPrefix . '_portfolioUrl']) ) {
            
            $this->portfolioUrl = $_REQUEST[$this->prereqPrefix . '_portfolioUrl'];    
            
        }
        
        if ( isset($_REQUEST[$this->prereqPrefix . '_portfolioDescription']) ) {
            
            $this->portfolioDescription = $_REQUEST[$this->prereqPrefix . '_portfolioDescription'];    
            
        }

        
        if ( isset($_REQUEST[$this->prereqPrefix . '_submitPortfolio']) ) {
            
            $this->formSubmitted = TRUE;
            
        }
        
        return TRUE;
        
    }
    
    
    private function getPortfolio() {
        
        $query = "SELECT id, url /* , description */ FROM mhci_prereqsDesignPortfolios";
        $query .= " WHERE student_lu_users_usertypes_id = " . $this->studentLuUsersUsertypesId;

        $result = mysql_query($query);
        
        while ($row = mysql_fetch_array($result)) {
            
            $this->portfolioId = $row['id'];
            $this->portfolioUrl = $row['url'];
         //   $this->portfolioDescription = $row['description'];

        }          
    }


    private function addPortfolio() {
        
        $query = "INSERT INTO mhci_prereqsDesignPortfolios";
        // DebugBreak();
        if (isset($this->portfolioId) && $this->portfolioId > 0) {
            
            // Handle updates.
            $query .= sprintf(" VALUES (%d, '%s', '%s')
                        ON DUPLICATE KEY UPDATE url = '%s',
                        description = '%s'",
                    //    $this->prereqId,
                        $this->studentLuUsersUsertypesId,
                        mysql_real_escape_string($this->portfolioUrl),
                        mysql_real_escape_string($this->portfolioDescription),
                        mysql_real_escape_string($this->portfolioUrl),
                        mysql_real_escape_string($this->portfolioDescription)
                        );
            /*
            $query .= " VALUES ({$this->portfolioId}, {$_SESSION['userId']}, '{$this->portfolioUrl}', '{$this->portfolioDescription}')";
            $query .= " ON DUPLICATE KEY UPDATE";
            $query .= " url = '{$this->portfolioUrl}',";
            $query .= " description = '{$this->portfolioDescription}'";             
            */
        } else {

            $query .= " (student_lu_users_usertypes_id, url, description)";           
            $query .= sprintf(" VALUES (%d, '%s', '%s')",
                        $this->studentLuUsersUsertypesId,
                        mysql_real_escape_string($this->portfolioUrl),
                        mysql_real_escape_string($this->portfolioDescription)
                        );
            /*
            $query .= " (student_lu_users_usertypes_id, url, description)";
            $query .= " VALUES ({$_SESSION['userId']}, '{$this->portfolioUrl}', '{$this->portfolioDescription}')";
            */
        }
        
        $result = mysql_query($query);
        
        if ($result) {
            
            $linkname = "mhci_prereqs_design.php";
            updateReqComplete($linkname, 0);    
            
        }
        
        return mysql_insert_id();     
    }
    
    
    private function renderHeading() {

        echo '<div class="sectionTitle">' . $this->prereqName . '</div><br/><br/>';
        
    }
    
    
    private function renderStudentIntro() {

        echo '<div class="bodyText">' . $this->studentIntro . '</div><br/>';
        
    }
    
    
    private function renderSummary() {
        global $assessmentForm;
        
        $portfolioUrl =  htmlentities($this->portfolioUrl);
        echo '<div class="bodyText">';
        echo "<b>" . $this->portfolioUrlLabel .  "</b>: ";
        echo '<a href="' . $portfolioUrl . '">' . $portfolioUrl . '</a>';
   //     echo "<b>" . $this->portfolioDescriptionLabel . "</b>: " . htmlentities($this->portfolioDescription) . "<br/>";
        if (!$assessmentForm->getFormSubmitted()) {
             echo '<input type="submit" id="'. $this->prereqPrefix.'_changePortfolio" name="'. $this->prereqPrefix.'_changePortfolio" class="bodyButton-right" value="Change" /><br />'; 
        }
        echo "</div>"; 
         // DebugBreak(); 
        
        
    }
    
    
    private function renderForm() {
        
        echo <<<EOB
            
        <input type="hidden" id="{$this->prereqPrefix}_portfolioId" name="{$this->prereqPrefix}_portfolioId" value="{$this->portfolioId}" />
        <div class="bodyText"><strong>{$this->portfolioUrlLabel}</strong>
            <br/><input id="{$this->prereqPrefix}_portfolioUrlInput" name="{$this->prereqPrefix}_portfolioUrl" class="textInput-long" type="text" value="{$this->portfolioUrl}" />
        </div>
        <br/>
 
       
EOB;
/*       <div class="bodyText"><strong>{$this->portfolioDescriptionLabel}</strong>
            <textarea id="{$this->prereqPrefix}_portfolioDescriptionInput" name="{$this->prereqPrefix}_portfolioDescription" class="commentArea"></textarea>
        </div>
         
        
        <br/>
        <input type="submit" id="{$this->prereqPrefix}_submitPortfolio" name="{$this->prereqPrefix}_submitPortfolio" class="bodyButton-right" value="{$this->submitLabel}" />
        <br /><br />
        */
    }    


}

  
?>
