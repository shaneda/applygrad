<?php
function getApplicantEmailData($unit, $unitId, $periodId, $luuIds = NULL)
{
    $query = "SELECT DISTINCT 
        application.id AS application_id, application.user_id AS luu_id, 
        users.id AS users_id, users.title, users.firstname, users.lastname, users.email
        FROM application
        INNER JOIN period_application 
            ON application.id = period_application.application_id
            AND period_application.period_id = " . intval($periodId) . "
        INNER JOIN lu_users_usertypes 
            ON application.user_id = lu_users_usertypes.id
        INNER JOIN users 
            ON lu_users_usertypes.user_id = users.id
        INNER JOIN lu_application_programs 
            ON application.id = lu_application_programs.application_id
        ";

    if ($unit == 'program')
    {
        $query .= " AND lu_application_programs.program_id = " . intval($unitId);    
    }
    elseif ($unit == 'department' || $unit == 'domain')
    {
        $query .= " INNER JOIN lu_programs_departments 
            ON lu_application_programs.program_id = lu_programs_departments.program_id";
            
        if ($unit == 'department')
        {
            $query .= " AND lu_programs_departments.department_id = " . intval($unitId);    
        }
        
        if ($unit == 'domain')
        {
            $query .= " INNER JOIN lu_domain_department
                ON lu_programs_departments.department_id = lu_domain_department.department_id 
                AND lu_domain_department.domain_id " . intval($unitId);   
        }    
    }

    if ($luuIds != NULL && is_array($luuIds))
    {
        $inString = explode(',', array_map('intval', $luuIds));
        $query .= " WHERE application.user_id IN (" . $inString . ")";
    }
    
    $query .= " ORDER BY users.lastname, users.firstname";
    
    $db = new DB_Applyweb();
    
    return $db->handleSelectQuery($query, 'luu_id');
}

function getApplicantMailingAddressData($unit, $unitId, $periodId, $luuIds = NULL)
{
    $query = "SELECT DISTINCT 
        application.id AS application_id, application.user_id AS luu_id, 
        users_info.address_cur_street1 AS current_street1, 
        users_info.address_cur_street2 AS current_street2,
        users_info.address_cur_street3 AS current_street3,
        users_info.address_cur_street4 AS current_street4,
        users_info.address_cur_city AS current_city,
        users_info.address_cur_state AS current_state,
        users_info.address_cur_pcode AS current_postal_code,
        users_info.address_cur_country AS current_country,
        users_info.address_perm_street1 AS permanent_street1, 
        users_info.address_perm_street2 AS permanent_street2,
        users_info.address_perm_street3 AS permanent_street3,
        users_info.address_perm_street4 AS permanent_street4,
        users_info.address_perm_city AS permanent_city,
        users_info.address_perm_state AS permanent_state,
        users_info.address_perm_pcode AS permanent_postal_code,
        users_info.address_perm_country AS permanent_country
        FROM application
        INNER JOIN period_application 
            ON application.id = period_application.application_id
            AND period_application.period_id = " . intval($periodId) . "
        INNER JOIN users_info
            ON application.user_id = users_info.user_id
        INNER JOIN lu_application_programs 
            ON application.id = lu_application_programs.application_id
        ";

    if ($unit == 'program')
    {
        $query .= " AND lu_application_programs.program_id = " . intval($unitId);    
    }
    elseif ($unit == 'department' || $unit == 'domain')
    {
        $query .= " INNER JOIN lu_programs_departments 
            ON lu_application_programs.program_id = lu_programs_departments.department_id";
            
        if ($unit == 'department')
        {
            $query .= " AND lu_programs_departments.department_id = " . intval($unitId);    
        }
        
        if ($unit == 'domain')
        {
            $query .= " INNER JOIN lu_domain_department
                ON lu_programs_departments.department_id = lu_domain_department.department_id 
                AND lu_domain_department.domain_id " . intval($unitId);   
        }    
    }

    if ($luuIds != NULL && is_array($luuIds))
    {
        $inString = explode(',', array_map('intval', $luuIds));
        $query .= " WHERE application.user_id IN (" . $inString . ")";
    }
    
    $db = new DB_Applyweb();
    
    return $db->handleSelectQuery($query, 'luu_id');
}

function getMailTemplates($unit, $unitId)
{
    $query = "SELECT DISTINCT content.id, content.name FROM content
        INNER JOIN contenttypes ON content.contenttype_id = contenttypes.id
            AND contenttypes.name LIKE 'Mail Template%'";
    if ($unit == 'department')
    {
        $query .= " LEFT OUTER JOIN lu_domain_department ON content.domain_id = lu_domain_department.domain_id
            WHERE lu_domain_department.department_id = " . intval($unitId);   
    }
    elseif ($unit == 'domain')
    {
        $query .= " WHERE content.domain_id = " . intval($unitId);
    }
    $query .= " ORDER BY content.name";   
    $result = mysql_query($query);

    $templates = array();
    while($row = mysql_fetch_array($result))
    {
        $templates[$row['id']] = $row;
    }    
    
    return $templates;
}

function getTemplateContent($contentId)
{
    $query = "SELECT content 
        FROM content 
        WHERE content.id = " . intval($contentId) . "
        LIMIT 1";
    $result = mysql_query($query);

    $content = '';
    while($row = mysql_fetch_array($result))
    {
        $content = str_replace('&amp;nbsp;' , ' ', $row['content']);
    }    
    
    return $content;
}

function getContentName($contentId)
{
    $query = "SELECT name FROM content 
        WHERE id = " . intval($contentId) . " LIMIT 1";
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result))
    {
        return $row['name'];        
    }
    
    return NULL;    
}

function getFunctionName($contentId)
{
    $contentName = getContentName($contentId);
    
    $contentNameArray = explode(' ', $contentName);
    $contentNameArray = array_map('trim', $contentNameArray);
    $contentNameArray = array_map('ucfirst', $contentNameArray);
    
    $functionName = implode('', $contentNameArray);
    
    if (function_exists($functionName))
    {
        return $functionName;    
    }
    
    return 'DefaultTemplateFunction';
}

function sendBulkMail($contentId, $systemEmail, $contacts, $selectedLuuIds = NULL, $preview = FALSE)
{
    $functionName = getFunctionName($contentId);
    $output = '';
    
    if ($functionName && function_exists($functionName))
    {
        if ($selectedLuuIds != NULL)
        {
            foreach ($selectedLuuIds as $selectedLuuId)
            {
                if (isset($contacts[$selectedLuuId]))
                {
                    $output .= $functionName($contentId, $systemEmail, $contacts[$selectedLuuId], $preview);
                }
            }
        }
        else
        {
            foreach ($contacts as $luuId => $contact)
            {
                $output .= $functionName($contentId, $systemEmail, $contact, $preview);    
            }    
        }   
    }
    
    return $output;    
}

/*
* Content-specific functions 
*/
function ValidateTemplateVariables($parsedTemplateContent)
{
    if (strstr($parsedTemplateContent, '##'))
    {
        return FALSE;    
    }
    
    return TRUE;
}

function DefaultTemplateFunction($contentId, $fromEmail, $applicantContact, $preview = FALSE)
{
    $templateContent = str_replace('&amp;nbsp;' , ' ', getTemplateContent($contentId));
    if (!$templateContent)
    {
        return NULL;    
    }
    
    $mailSieveString = getMailSieveString();
    $recipient = $applicantContact['email'];
    $subject = getContentName($contentId);
    $data = array();
    $emailBody = parseEmailTemplate2(html_entity_decode($templateContent), $data);
    
    if ($preview)
    {
        $printedEmail = printHtmlMail($fromEmail, $applicantContact['email'], $subject, $emailBody, NULL, NULL);
        return str_replace("\r\n", '<br>', $printedEmail); 
    }
    else
    {
        // return sendHtmlMail($systemEmail, $recipient, "Applicant Submission Reminder", $emailBody, "reminder", "", $mailSieveString); 
    }      
}

function ApplicantPaymentReminder($contentId, $fromEmail, $applicantContact, $preview = FALSE)
{
    $templateContent = str_replace('&amp;nbsp;' , ' ', getTemplateContent($contentId));
    if (!$templateContent)
    {
        return NULL;    
    }
    
    $mailSieveString = getMailSieveString();
    $recipient = $applicantContact['email'];
    $subject = getContentName($contentId);
    $data = array();
    $emailBody = parseEmailTemplate2(html_entity_decode($templateContent), $data);
    
    if ($preview)
    {
        $printedEmail = printHtmlMail($fromEmail, $applicantContact['email'], $subject, $emailBody, NULL, NULL);
        return str_replace("\r\n", '<br>', $printedEmail); 
    }
    else
    {
        // return sendHtmlMail($systemEmail, $recipient, "Applicant Submission Reminder", $emailBody, "reminder", "", $mailSieveString); 
    }    
}

function ApplicantSubmissionReminder($contentId, $fromEmail, $applicantContact, $preview = FALSE)
{
    $templateContent = str_replace('&amp;nbsp;' , ' ', getTemplateContent($contentId));
    if (!$templateContent)
    {
        return NULL;    
    }
    
    $mailSieveString = getMailSieveString();
    $recipient = $applicantContact['email'];
    $subject = getContentName($contentId);
    $data = array(array('userid', $recipient));
    $emailBody = parseEmailTemplate2(html_entity_decode($templateContent), $data);
    
    if ($preview)
    {
        $printedEmail = printHtmlMail($fromEmail, $applicantContact['email'], $subject, $emailBody, NULL, NULL);
        return str_replace("\r\n", '<br>', $printedEmail); 
    }
    else
    {
        if (ValidateTemplateVariables($emailBody))
        {
            // return sendHtmlMail($systemEmail, $recipient, "Applicant Submission Reminder", $emailBody, "reminder", "", $mailSieveString); 
        }
    }    
}



?>