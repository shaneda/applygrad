<?php
// Initialize variables
$iniCourseworkKobeDescription = '';
$iniCourseworkDataStructuresTitle = '';
$iniCourseworkDataStructuresNumber = '';
$iniCourseworkProbabilityTitle = '';
$iniCourseworkProbabilityNumber = '';
$iniCourseworkStatisticsTitle = '';
$iniCourseworkStatisticsNumber = '';
$iniCourseworkMsitExperience = '';
$iniCourseworkProgrammingDescription = '';
$iniCourseworkProgrammingDescription2 = '';
$iniCourseworkError = '';

// Special cases
$isIniMsinApplication = isIniMsinApplication($_SESSION['appid']);
$isIniMsistmApplication = isIniMsistmApplication($_SESSION['appid']);
$isIniMsitApplication = isIniMsitApplication($_SESSION['appid']);

// Validate and save posted data
if( isset($_POST['btnSave'])) 
{
    if (isIniKobeDomain($domainid))
    {
        saveIniCourseworkKobe();
    }
    else
    {
        saveIniCoursework();
    }
    
    checkIniCourseworkRequirements();  
}

// Get data from db
// (Posted data will be used if there has been a validation error)
if (!$iniCourseworkError)
{
    if (isIniKobeDomain($domainid))
    {
        $iniSupportingCourseworkQuery = "SELECT * FROM ini_supporting_coursework_kobe 
            WHERE application_id = " . intval($_SESSION['appid']) . " LIMIT 1";
        $iniSupportingCourseworkResult = mysql_query($iniSupportingCourseworkQuery);
        while($row = mysql_fetch_array($iniSupportingCourseworkResult))
        {
            $iniCourseworkKobeDescription = $row['description'];       
        } 
    }
    else
    {
        $iniSupportingCourseworkQuery = "SELECT * FROM ini_supporting_coursework 
            WHERE application_id = " . intval($_SESSION['appid']) . " LIMIT 1";
        $iniSupportingCourseworkResult = mysql_query($iniSupportingCourseworkQuery);        
        while($row = mysql_fetch_array($iniSupportingCourseworkResult))
        {
            $iniCourseworkDataStructuresTitle = $row['data_structures_title'];
            $iniCourseworkDataStructuresNumber = $row['data_structures_number'];
            $iniCourseworkProbabilityTitle = $row['probability_title'];
            $iniCourseworkProbabilityNumber = $row['probability_number'];
            $iniCourseworkStatisticsTitle = $row['statistics_title'];
            $iniCourseworkStatisticsNumber = $row['statistics_number'];
            $iniCourseworkMsitExperience = $row['msit_experience'];
            $iniCourseworkProgrammingDescription = $row['programming_description'];
            $iniCourseworkProgrammingDescription2 = $row['programming_description2'];   
        }
    }
}
?>

<span class="subtitle">Supporting Educational Data</span>
<?php
if ($iniCourseworkError)
{
?>
    <br>
    <span class="errorSubtitle"><?php echo $iniCourseworkError; ?></span>
<?php  
}
?>
<br/>
<br/>
<?php
if (isIniKobeDomain($domainid))
{
?>
    To meet the minimum qualifications necessary to be considered for the program
    you must meet the following: Successful completion of introductory-level
    undergraduate computer science or information technology course and
    demonstrated analytical ability, including successful completion of undergraduate
    probability or statistics course. These requirements may also be fulfilled by
    appropriate professional/industrial experience to be judged by the admissions
    committee.
    <br/>
    <br/>
    Please list the title and course number (if your university provides course numbers) of the
    course(s) taken in probability or statistics and the course(s) taken in computer science or
    information technology or describe your professional/industrial experience in these areas.
    <br/>
    <br/>
<?php
    showEditText($iniCourseworkKobeDescription, "textarea", "iniCourseworkKobeDescription", $_SESSION['allow_edit'], TRUE);   
}
else
{
    $IniMsinMsistmRequired = FALSE;
    if ($isIniMsinApplication || $isIniMsistmApplication)
    {
        $IniMsinMsistmRequired = TRUE;    
    }
    
    $iniMsitRequired = FALSE;
    if ($isIniMsitApplication)
    {
        $iniMsitRequired = TRUE;    
    }
    
?>
    To meet the minimum qualifications necessary to be considered for the MSIN/MSIS/MSIT programs
    of the Information Networking Institute (INI), you must have taken the courses listed below in 
    your undergraduate program. If no course information is provided, it will be assumed the applicant
    does not have the required course and the application will not be reviewed.
    <br/>
    <br/>
    Applicants who identify courses that are not specifically listed as "data structures" and "probablity
    theory" on their transcripts must provide a university syllabus of the courses they are identifying
    as covering these two subjects.
    <br/>
    <br/>
    These courses are pre-requisites to graduate-level courses INI students are required to take.
    <br/>
    <br/>
    Please provide both the course titles and numbers in your undergraduate curriculum that covered
    the following topics. If your university does not provide course numbers, please enter a 0 in the
    appropriate space. You must provide the information required. If courses cannot be located on your 
    transcript the review of your application may be delayed.
    
    <br/>
    <br/>
    Data Structures:
    <br/>
    <span style="display: inline-block; width: 120px;">Course Title</span>
    <?php
    showEditText($iniCourseworkDataStructuresTitle, "textbox", "iniCourseworkDataStructuresTitle", $_SESSION['allow_edit'], TRUE, NULL, TRUE, 50);     
    ?>
    <br/>
    <span style="display: inline-block; width: 120px;">Course Number</span>
    <?php
    showEditText($iniCourseworkDataStructuresNumber, "textbox", "iniCourseworkDataStructuresNumber", $_SESSION['allow_edit'], TRUE, NULL, TRUE, 50);     
    ?>
    
    <br/>
    <br/>
    Probability Theory (required for MSIS/MSIN applicants only):
    <br/>
    <span style="display: inline-block; width: 120px;">Course Title</span>
    <?php
    showEditText($iniCourseworkProbabilityTitle, "textbox", "iniCourseworkProbabilityTitle", 
        $_SESSION['allow_edit'], $IniMsinMsistmRequired, NULL, TRUE, 50);     
    ?>
    <br/>
    <span style="display: inline-block; width: 120px;">Course Number</span>
    <?php
    showEditText($iniCourseworkProbabilityNumber, "textbox", "iniCourseworkProbabilityNumber", 
        $_SESSION['allow_edit'], $IniMsinMsistmRequired, NULL, TRUE, 50)
    ?>
    
    <br/>
    <br/>
    Statistics (required for MSIT applicants only):
    <br/>
    <span style="display: inline-block; width: 120px;">Course Title</span>
    <?php
    showEditText($iniCourseworkStatisticsTitle, "textbox", "iniCourseworkStatisticsTitle", 
        $_SESSION['allow_edit'], $iniMsitRequired, NULL, TRUE, 50);     
    ?>
    <br/>
    <span style="display: inline-block; width: 120px;">Course Number</span>
    <?php
    showEditText($iniCourseworkStatisticsNumber, "textbox", "iniCourseworkStatisticsNumber", 
        $_SESSION['allow_edit'], $iniMsitRequired, NULL, TRUE, 50)
    ?>
    
    <br/>
    <br/>
    For MSIT applicants only, if no course has been taken in either probablity/statistics or data structures,
    these qualifications may be fulfilled by appropriate professional/industrial experience to be judged by
    the admissions committee. Please explain your experience with either data structures or probablity/statistics
    or both:
    <br/>
    <br/>
    <?php
    showEditText($iniCourseworkMsitExperience, "textbox", "iniCourseworkMsitExperience", $_SESSION['allow_edit'], FALSE, NULL, TRUE, 100) 
    ?>
    
    <br/>
    <br/>
    Students admitted to the MSIN/MSIS/MSIT programs must be proficient in programming. Fluency in C
    programming is required for MSIN/MSIS applicants. Proficiency in Java or C++ is required for MSIT
    applicants. Additionally, knowledge of C is recommended for all MSIT applicants. Please indicate
    in the text box below (1) how long you have used each of these programming languages, and (2)
    describe your practical experience on one or two sizable projects using these languages. We are
    particularly interested in your systems level programming experience, so highlight any such work.
    Projects may be from courses, research projects, or on the job - let us know their source. Include
    indicators of the project size, complexity, team size and your responsibility level. Descriptions
    not providing adequate detail will be marked as having insufficient experience. If you do not have
    experience with a particular language, please indicate this.
    <br/>
    <br/>
    Detailed C programming and systems-level project Description:
    <br/>
    <br/>
    <?php
    $disabled = '';
    if (!$_SESSION['allow_edit'])
    {
        $disabled = 'disabled="disabled"';    
    }
    ?>
    <textarea name="iniCourseworkProgrammingDescription" id="iniCourseworkProgrammingDescription" class="tblItemRequired"
        cols="60" rows="5" <?php echo $disabled; ?> ><?php echo $iniCourseworkProgrammingDescription; ?></textarea>
    <br/>
    Characters left in your response 
    <span id="iniCourseworkProgrammingDescriptionCharCount"><?php echo 7500 - strlen($iniCourseworkProgrammingDescription); ?></span>
    
    <br/>
    <br/>
    Detailed C++/Java Project Description:
    <br/>
    <br/>
    <textarea name="iniCourseworkProgrammingDescription2" id="iniCourseworkProgrammingDescription2" class="tblItemRequired"
        cols="60" rows="5" <?php echo $disabled; ?> ><?php echo $iniCourseworkProgrammingDescription2; ?></textarea>
    <br/>
    Characters left in your response 
    <span id="iniCourseworkProgrammingDescription2CharCount"><?php echo 7500 - strlen($iniCourseworkProgrammingDescription2); ?></span>
<?php
} // end else
?>
<br/>
<hr size="1" noshade color="#990000">

<?php
// http://codeimpossible.com/2010/01/13/solving-document-ready-is-not-a-function-and-other-problems/ 
// http://learn.jquery.com/using-jquery-core/avoid-conflicts-other-libraries/  
?>
<script src="../javascript/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$.noConflict();
(function() {
jQuery(document).ready(function() {
    jQuery('#iniCourseworkProgrammingDescription').keyup(function (event) {
        var maxsize = 7500;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#iniCourseworkProgrammingDescriptionCharCount').text(newCharCount);
        }
    });
    
    jQuery('#iniCourseworkProgrammingDescription2').keyup(function (event) {
        var maxsize = 7500;
        var len = jQuery(this).val().length;
        if (len > maxsize) {
            var newVal = jQuery(this).val().substring(0, maxsize);
            jQuery(this).val(newVal);
        } else {
            var newCharCount = (maxsize - len) + ' characters left';
            jQuery('#iniCourseworkProgrammingDescription2CharCount').text(newCharCount);
        }
    });
});
})(jQuery);
</script>

<?php
/*
* FUNCTIONS 
*/ 
function saveIniCourseworkKobe()
{
    global $iniCourseworkKobeDescription;
    global $iniCourseworkError;
    
    $iniCourseworkKobeDescription = filter_input(INPUT_POST, 'iniCourseworkKobeDescription', FILTER_SANITIZE_STRING);
    
    if (!$iniCourseworkKobeDescription)
    {
        $iniCourseworkError .= 'Supporting coursework description is required<br>';    
    } 
    
    if (!$iniCourseworkError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM ini_supporting_coursework_kobe 
            WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE ini_supporting_coursework_kobe SET
                description = '" . mysql_real_escape_string($iniCourseworkKobeDescription) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO ini_supporting_coursework_kobe (application_id, description)
                VALUES (" 
                . intval($_SESSION['appid']) . ",'"
                . mysql_real_escape_string($iniCourseworkKobeDescription) . "')";
            mysql_query($insertQuery);
        }
    }
}
    
function saveIniCoursework()
{
    global $iniCourseworkDataStructuresTitle;
    global $iniCourseworkDataStructuresNumber;
    global $iniCourseworkProbabilityTitle;
    global $iniCourseworkProbabilityNumber;
    global $iniCourseworkStatisticsTitle;
    global $iniCourseworkStatisticsNumber;
    global $iniCourseworkMsitExperience;
    global $iniCourseworkProgrammingDescription;
    global $iniCourseworkProgrammingDescription2;
    global $isIniMsinApplication;
    global $isIniMsistmApplication;
    global $isIniMsitApplication;
    global $iniCourseworkError; 
    
    $iniCourseworkDataStructuresTitle = filter_input(INPUT_POST, 'iniCourseworkDataStructuresTitle', FILTER_SANITIZE_STRING);
    $iniCourseworkDataStructuresNumber = filter_input(INPUT_POST, 'iniCourseworkDataStructuresNumber', FILTER_SANITIZE_STRING);
    $iniCourseworkProbabilityTitle = filter_input(INPUT_POST, 'iniCourseworkProbabilityTitle', FILTER_SANITIZE_STRING); 
    $iniCourseworkProbabilityNumber = filter_input(INPUT_POST, 'iniCourseworkProbabilityNumber', FILTER_SANITIZE_STRING); 
    $iniCourseworkStatisticsTitle = filter_input(INPUT_POST, 'iniCourseworkStatisticsTitle', FILTER_SANITIZE_STRING); 
    $iniCourseworkStatisticsNumber = filter_input(INPUT_POST, 'iniCourseworkStatisticsNumber', FILTER_SANITIZE_STRING);
    $iniCourseworkMsitExperience = filter_input(INPUT_POST, 'iniCourseworkMsitExperience', FILTER_SANITIZE_STRING);
    $iniCourseworkProgrammingDescription = filter_input(INPUT_POST, 'iniCourseworkProgrammingDescription', FILTER_SANITIZE_STRING);  
    $iniCourseworkProgrammingDescription2 = filter_input(INPUT_POST, 'iniCourseworkProgrammingDescription2', FILTER_SANITIZE_STRING);
    
    if (!$iniCourseworkDataStructuresTitle)
    {
        $iniCourseworkError .= 'Data structures course title is required<br>';    
    }
    
    if ($iniCourseworkDataStructuresNumber == '' || $iniCourseworkDataStructuresNumber == NULL)
    {
        $iniCourseworkError .= 'Data structures course number is required<br>';    
    }
    
    if (($isIniMsinApplication || $isIniMsistmApplication) && !$iniCourseworkProbabilityTitle)
    {
        $iniCourseworkError .= 'Probability course title is required<br>';    
    }
     
    if (($isIniMsinApplication || $isIniMsistmApplication) 
        && ($iniCourseworkProbabilityNumber == '' || $iniCourseworkProbabilityNumber == NULL))
    {
        $iniCourseworkError .= 'Probability course number is required<br>';    
    }
    
    if ($isIniMsitApplication && !$iniCourseworkStatisticsTitle)
    {
        $iniCourseworkError .= 'Statistics course title is required<br>';    
    }
     
    if ($isIniMsitApplication 
        && ($iniCourseworkStatisticsNumber == '' || $iniCourseworkStatisticsNumber == NULL))
    {
        $iniCourseworkError .= 'Statistics course number is required<br>';    
    }
    
    if (!$iniCourseworkProgrammingDescription)
    {
        $iniCourseworkError .= 'C/system-level programming project description is required<br>';    
    }
    
    if (!$iniCourseworkProgrammingDescription2)
    {
        $iniCourseworkError .= 'C++/Java programming project description is required<br>';    
    }
    
    if (!$iniCourseworkError)
    {
        // Check for existing record
        $existingRecordQuery = "SELECT id FROM ini_supporting_coursework 
            WHERE application_id = " . intval($_SESSION['appid']);
        $existingRecordResult = mysql_query($existingRecordQuery);
        if (mysql_num_rows($existingRecordResult) > 0)
        {
            // Update existing record
            $updateQuery = "UPDATE ini_supporting_coursework SET
                data_structures_title = '" . mysql_real_escape_string($iniCourseworkDataStructuresTitle) . "',
                data_structures_number = '" . mysql_real_escape_string($iniCourseworkDataStructuresNumber) . "',
                probability_title = '" . mysql_real_escape_string($iniCourseworkProbabilityTitle) . "',
                probability_number = '" . mysql_real_escape_string($iniCourseworkProbabilityNumber) . "',
                statistics_title = '" . mysql_real_escape_string($iniCourseworkStatisticsTitle) . "',
                statistics_number = '" . mysql_real_escape_string($iniCourseworkStatisticsNumber) . "',
                msit_experience = '" . mysql_real_escape_string($iniCourseworkMsitExperience) . "',
                programming_description = '" . mysql_real_escape_string($iniCourseworkProgrammingDescription) . "',
                programming_description2 = '" . mysql_real_escape_string($iniCourseworkProgrammingDescription2) . "'
                WHERE application_id = " . intval($_SESSION['appid']);
            mysql_query($updateQuery);
        }
        else
        {
            // Insert new record
            $insertQuery = "INSERT INTO ini_supporting_coursework 
                (application_id, data_structures_title, data_structures_number, probability_title, probability_number, 
                    statistics_title, statistics_number, msit_experience, programming_description, programming_description2)
                VALUES (" 
                . intval($_SESSION['appid']) . ",'" 
                . mysql_real_escape_string($iniCourseworkDataStructuresTitle) . "','" 
                . mysql_real_escape_string($iniCourseworkDataStructuresNumber) . "','" 
                . mysql_real_escape_string($iniCourseworkProbabilityTitle) . "','" 
                . mysql_real_escape_string($iniCourseworkProbabilityNumber) . "','" 
                . mysql_real_escape_string($iniCourseworkStatisticsTitle) . "','" 
                . mysql_real_escape_string($iniCourseworkStatisticsNumber) . "','" 
                . mysql_real_escape_string($iniCourseworkMsitExperience) . "','"
                . mysql_real_escape_string($iniCourseworkProgrammingDescription) . "','" 
                . mysql_real_escape_string($iniCourseworkProgrammingDescription2) . "')";
            mysql_query($insertQuery);
        }
    }
    else
    {
        $iniCourseworkError .= ' Changes will not be saved until all required fields are completed.';
    } 
}

function checkIniCourseworkRequirements()
{
    global $iniCourseworkError;     
    
    if (!$iniCourseworkError)
    {
        // Check for transcripts
        $transcriptQuery = "SELECT id, datafile_id FROM usersinst 
            WHERE application_id = " . $_SESSION['appid'];     
        $transcriptResult = mysql_query($transcriptQuery);                
        while ($row = mysql_fetch_array($transcriptResult))
        {
            if (!$row['datafile_id'])
            {
                // There is an institute without a transcript, so page is incomplete.
                updateReqComplete("uni.php", 0);
                return;
            }    
        }
        
        // All institutes have a transcript, so page is complete.
        updateReqComplete("uni.php", 1);
        return;   
    }
    
    // Page is incomplete.
    updateReqComplete("uni.php", 0);
}
?>
