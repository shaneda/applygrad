<?php
/*
* Functions for working with users_remote_auth_string data.
*/

function getRemoteAuthStrings($usersId) {
    
    $query = "SELECT remote_auth_string
        FROM users_remote_auth_string
        WHERE users_id = " . $usersId;
    $result = mysql_query($query);
    
    $remoteAuthStrings = array();
    while ($row = mysql_fetch_array($result)) {
        $remoteAuthStrings[] = $row['remote_auth_string'];
    }
    
    return $remoteAuthStrings;    
}

function explodeNewline($string) {
    
    $stringArray = explode("\n", trim($string)); 
    $stringArray = array_map('trim', $stringArray);
    
    return $stringArray;     
}

function saveRemoteAuthStrings($usersId, $remoteAuthStrings) {
    
    foreach ($remoteAuthStrings as $remoteAuthString) {
    
        // Don't insert empty strings.
        if ($remoteAuthString) {
            $query = "INSERT INTO users_remote_auth_string VALUES (
                " . intval($usersId) . ",
                '" . mysql_real_escape_string($remoteAuthString) . "'
                ) 
                ON DUPLICATE KEY UPDATE users_id = users_id";
            $result = mysql_query($query);
        }
    }
    
    return TRUE;    
}

function deleteRemoteAuthStringOrphans($usersId, $remoteAuthStrings) {

    if (count($remoteAuthStrings) > 0) {
    
        $remoteAuthStrings = 
            array_map('mysql_real_escape_string', $remoteAuthStrings); 
        
        $query = "DELETE FROM users_remote_auth_string
            WHERE users_id = " . intval($usersId) . "
            AND remote_auth_string NOT IN 
            ('" . implode("','", $remoteAuthStrings) . "')";
        
        $result = mysql_query($query);
    }
    
    return TRUE;
}

function deleteRemoteAuthStrings($usersId) {

    $query = "DELETE FROM users_remote_auth_string
        WHERE users_id = " . intval($usersId);
    
    $result = mysql_query($query);
    
    return TRUE;
}


/*
* Functions for working with scsUser and WebISO data.
*/

function cleanWebIsoId($webIsoId) {
    $webIsoArray = split('@', $webIsoId);
    $id = $webIsoArray[0];
    return $id;
}

function getScsUserDataByUsersId($usersId) {
    
    $query = "
        SELECT scs_user.*
        FROM scs_user
        WHERE scs_user.users_id = " . $usersId;
    $queryResult = mysql_query($query);
    $resultArray = array();
    while ($row = mysql_fetch_array($queryResult)) {
        $resultArray[] = $row;
    }
    
    return $resultArray;
}

function saveScsUserData($scsUserId, $usersId, $csId, $andrewId, $eceId, $qatarId) {
    
    // Insert/upadate the record accordingly.
    if ($scsUserId) {
        
        $query = "UPDATE scs_user";
        $query .= sprintf( 
                    " SET cs_id = '%s', 
                    andrew_id = '%s', 
                    ece_id = '%s', 
                    qatar_id = '%s' 
                    WHERE scs_user_id = %d",
                    mysql_real_escape_string($csId),
                    mysql_real_escape_string($andrewId),
                    mysql_real_escape_string($eceId),
                    mysql_real_escape_string($qatarId),
                    intval($scsUserId)
                    );
        
        $result = mysql_query($query);
                 
    } else {
        
        // Don't add a record unless at least one of the id's is set.
        if (!$csId && !$andrewId && !$eceId && !$qatarId) {
            return $scsUserId;
        }
        
        // Make a new record; include the name.
        $firstname = $_POST['txtFirstname'];
        $lastname = $_POST['txtLastname'];
    
        $query = "INSERT INTO scs_user (users_id, cs_id, andrew_id, ece_id, qatar_id)";
        $query .= sprintf( " VALUES (%d, '%s', '%s', '%s', '%s')",
            intval($usersId),
            mysql_real_escape_string($csId),
            mysql_real_escape_string($andrewId),
            mysql_real_escape_string($eceId),
            mysql_real_escape_string($qatarId)
            );
        
        $result = mysql_query($query);
        $scsUserId = mysql_insert_id();  
        
    }
    
    return $scsUserId;
}

function saveScsUserWebIsoData($scsUserId, $webIso) {
    
    if (!$scsUserId) {
        return FALSE;
    }
    
    $query = "INSERT INTO scs_user_webiso (scs_user_id, webiso)";
    $query .= sprintf( " VALUES (%d, '%s')
                ON DUPLICATE KEY UPDATE webiso = '%s'",
                intval($scsUserId),
                mysql_real_escape_string($webIso),
                mysql_real_escape_string($webIso)
                );
    
    $result = mysql_query($query);
    
    return TRUE;    
}

function deleteScsUserWebIsoOrphans($scsUserId, $csId, $andrewId, $eceId, $qatarId) {
    
    $query = "DELETE FROM scs_user_webiso";
    $query .= sprintf(
            " WHERE scs_user_id = %d
            AND webiso NOT IN ('%s', '%s', '%s', '%s')",
            intval($scsUserId),
            mysql_real_escape_string($csId . '@CS.CMU.EDU'),
            mysql_real_escape_string($andrewId . '@ANDREW.CMU.EDU'),
            mysql_real_escape_string($eceId . '@ECE.CMU.EDU'),
            mysql_real_escape_string($qatarId . '@QATAR.CMU.EDU')
            );    

    $result = mysql_query($query);
    
    return TRUE;
}

function deleteScsUserData($scsUserId) {

    $query = sprintf("DELETE FROM scs_user WHERE scs_user_id = %d", 
                    intval($scsUserId));
    
    $result = mysql_query($query);
    
    return TRUE;    
}

function deleteScsUserWebIsoData($scsUserId) {
    
    $query = sprintf("DELETE FROM scs_user_webiso WHERE scs_user_id = %d",
            intval($scsUserId));    

    $result = mysql_query($query);
    
    return TRUE;
}

?>