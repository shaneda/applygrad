<?php
//DebugBreak();

include_once './inc/config.php';
include_once './inc/db_connect.php';
include './classes/DB_Applyweb/class.DB_Applyweb.php';

$activePrograms = array();

include './classes/DB_Applyweb/class.DB_Programs.php'; 
$DB_Programs = new DB_Programs();
$allPrograms = $DB_Programs->getWithLookup();
$domainQuery = "SELECT DISTINCT lu_programs_departments.program_id, lu_domain_department.domain_id
                FROM lu_programs_departments
                INNER JOIN lu_domain_department 
                    ON lu_programs_departments.department_id = lu_domain_department.department_id
                INNER JOIN systemenv 
                    ON lu_domain_department.domain_id = systemenv.domain_id
                WHERE systemenv.expdate > NOW()
                OR systemenv.expdate2 > NOW()  
                ";
$activeDomains = $DB_Programs->handleSelectQuery($domainQuery, 'program_id');
$activePrograms = array_keys($activeDomains);

$domainname = '';
include './inc/tpl.pageHeaderApply.php';

?>
<br/>
<br/>
<div style="margin: 10px;">
<span class="title">Carnegie Mellon University School of Computer Science: Online Admissions</span>
<br/>
<?php
if ( count($activePrograms) > 0) {
?>

<p>Applications are currently being accepted for the following programs:</p>

<ul>
<?php
foreach ($allPrograms as $program) {
    $programId = $program['id'];
    if ( in_array($programId, $activePrograms) ) {
        $domainId = $activeDomains[$programId]['domain_id'];
        echo <<<EOB
        <li style="margin-top: 4px;">
        <a href="apply/index.php?domain={$domainId}">{$program['name']}</a>
        </li>    
EOB;
    }
}
?>
</ul>
</div>
<br/>

<?php
} else {
?>
    <p>Applications are not being accepted at this time.</p>
<?php 
}
include './inc/tpl.pageFooterApply.php';  
?>