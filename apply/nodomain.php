<?php
include_once '../inc/config.php'; 
/*
* The session include is unnecessary here and will redirect to offline.php.
*/
//include_once '../inc/session.php'; 
session_start();
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

// Include the shared page header elements.
$pageTitle = 'Application Error';
$displayMenu = FALSE;
include '../inc/tpl.pageHeaderApply.php';
?>

<p class="tblItem">
We are sorry, but you have reached this site in error. 
</p>
<p class="tblItem">
<?php  if ($hostname == 'APPLY.STAT.CMU.EDU') {?>
Please launch the site from the <a href="http://www.stat.cmu.edu/">corresponding departmental home page</a>.
    <?php } else {  ?> 
Please launch the site from the <a href="http://www.cs.cmu.edu/">corresponding departmental home page</a>. 
                <?php } ?>
</p>

<?php
include '../inc/tpl.pageFooterApply.php';
?>