<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/AdvisorsData.class.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$advisors = array();
$MyAdvisors = array();
$id = -1;
$progid = -1;
$choices = array("Faculty of Interest", "Faculty of Interest", "Faculty of Interest");

if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
}

if(isset($_GET['p']))
{
	$progid = intval($_GET['p']);
}

$deptNumber = getDeptFromProgram($progid);
$advSelType = getDeptAdvisorSelectionType($deptNumber);

if(isset($_POST['btnSubmit']))
{
	$hasAdvisors = false;
	$vars = $_POST;
	$advisorsCount = 0;
	$itemId = -1;
	
	foreach($vars as $key1 => $value1)
	{
		if( strstr($key1, 'lbAdvisor') !== false )
		{
			$hasAdvisors = true;
			$advisorsCount++;
			$arr = split("_", $key1);
			$itemId = $arr[2];
            
			$appProgId = $arr[1];
			if( $itemId > -1 && $value1 != "")
			{
				if($value1 < 1)
				{
					//$err .= "Please select an interest from each list<br>";
				}
				//echo $value1 . " ";
				array_push($MyAdvisors, $value1);
			}//END IF
		}//END IF ADD
	}//END FOR

	$matched = false;
	for($i = 0; $i<count($MyAdvisors); $i++)
	{
		for($j = 0; $j<count($MyAdvisors); $j++)
		{
			if(($j != $i) && ($MyAdvisors[$j] == $MyAdvisors[$i]))
			{
				$matched = true;
			}
		}
	}
	if($matched == true)
	{
		$err .= "Error: Please change your faculty of interest selections. They must be unique.<br>";
	}
	if($err == "")
	{   
        switch ($advSelType) {
            case 1:
                $originalAdvisors = getMyAdvisors($progid);
                $originalAdvisorsIDs = array();
                for ($a = 0; $a<count($originalAdvisors); $a++) {
                    array_push($originalAdvisorsIDs, $originalAdvisors[$a][0]);
                    if (in_array($originalAdvisors[$a][0], $MyAdvisors) == FALSE) {
                        $sql = "DELETE FROM lu_application_advisor WHERE program_id=".$progid." and advisor_user_id = ".$originalAdvisors[$a][0]." and application_id = ".$_SESSION['appid'];
                        mysql_query($sql); 
                        } 
                    }
                for ($a = 0; $a<count($MyAdvisors); $a++) {
                    if (in_array($MyAdvisors[$a], $originalAdvisorsIDs) == FALSE) {
                        if($MyAdvisors[$a] != "")
                            {
                               $sql = "insert into lu_application_advisor(program_id, advisor_user_id, application_id)values( ".$progid." , '".addslashes($MyAdvisors[$a])."', ".$_SESSION['appid'].")";
                               $result = mysql_query($sql) or die(mysql_error().$sql);
                            }
                    }
                }
                break;
                
            case 2:
                $originalAdvisors = getMyAdvisorsWithAppId($_SESSION['appid'], $progid);
                for ($a = 0; $a<count($originalAdvisors); $a++) {
                    if (in_array($originalAdvisors[$a], $MyAdvisors) == FALSE) {
                        if($originalAdvisors[$a] != "") {
                            $sql = "DELETE FROM lu_application_advisor WHERE program_id=".$progid." and name = '".addslashes($originalAdvisors[$a])."' and application_id = ".$_SESSION['appid'];
                             mysql_query($sql) or die(mysql_error().$sql); 
                             }
                    }
                }
                for ($a = 0; $a<count($MyAdvisors); $a++) {
                    if (in_array($MyAdvisors[$a], $originalAdvisors) == FALSE) { 
                        if($MyAdvisors[$a] != "")
                            {
                               $sql = "insert into lu_application_advisor(program_id, name, application_id)values( ".$progid." , '".addslashes($MyAdvisors[$a])."', ".$_SESSION['appid'].")";
                               $result = mysql_query($sql) or die(mysql_error().$sql);
                            }
                    }
                } 
                break;
        } 
	//	updateReqComplete("programs.php",1);
	}  
}//end if submit

// Include the shared page header elements.
$pageTitle = 'Faculty of Interest';
include '../inc/tpl.pageHeaderApply.php';
?>

<span class="errorSubTitle"><?=$err?></span>
<span class="tblItem">
<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
    if($_SESSION['domainid'] > -1)
    {
        $domainid = $_SESSION['domainid'];
    }
}
$sql = "select content from content where name='Faculty of Interest' and domain_id=".$domainid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}
?>

<table border="0" cellspacing="2" cellpadding="4">

<?php
$isCsdProgram = isCsdProgram($progid);
$isRiProgram = isRiProgram($progid);
if ($isCsdProgram || $isRiProgram) {

    if ($isCsdProgram) {
        $facultyUrl = 'http://www.csd.cs.cmu.edu/research/faculty_research/';
        $facultyTitle = 'Faculty Research Guide';   
    } 
    if ($isRiProgram) {
        // $facultyUrl = 'http://www.ri.cmu.edu/research_keyword.html?menu_id=260';
        $facultyUrl = 'http://ri.cmu.edu/people/faculty/';
        $facultyTitle = 'Faculty Research Interests guide';    
    }
?>
    <tr>
    <td colspan="2">
    <a href="<?php echo $facultyUrl; ?>" target="_blank">Click here for the <?php echo $facultyTitle; ?></a>
    <br><br>
    </td>
    </tr>
<?php 
}
?>
    <tr>            
	  <? 
      if ($advSelType ==  1) {
	    $advisors = getProgramAdvisors($progid);
        $myAdvisors = getMyAdvisors($progid);
      } else  if ($advSelType ==  2) {
        	    $myAdvisors = getMyAdvisorsWithAppId($_SESSION['appid'], $progid);
      }

	  $choicesCount = 3;
/*	  if(count($advisors) < 3)
	  {
		$choicesCount = count($advisors);
	  }
      */
	  for($j=0; $j< $choicesCount;$j++) { ?>
		
		<td width=225px>
		<span class="tblItem">
		  <? 
		  echo "<strong>".$choices[$j]."</strong><br>";
		  $myAdvisor = "";
		  if(count($myAdvisors) > $j)
		  {
			$myAdvisor = $myAdvisors[$j];
		  }
		  switch ($advSelType) {
              case 1:
		        showEditText( $myAdvisor, "listbox", "lbAdvisor_".$id."_".$j, $_SESSION['allow_edit'],false, $advisors);
                break;
              case 2: 
                showEditText( $myAdvisor, "textbox", "lbAdvisor_".$id."_".$j, $_SESSION['allow_edit'],false, null, true, 30);
                break;
          }?>
		</span></td>
	  
	  <? }//END FOR CHOICESCOUNT ?>
	</tr>
<?php 
//DebugBreak();
/*
if ($advSelType == 2) {
?>
    <tr>
    <td>
    <a href="http://www.csd.cs.cmu.edu/people/faculty.html" target="_blank">Click here for the faculty directory </a>
    </td>
    </tr>
<?php 
} 
*/

?>
</table>

<br>
<? showEditText("  Save  ", "button", "btnSubmit", $_SESSION['allow_edit']); ?> 			 
<input class="tblItem" name="btnAreas_" type="button" id="btnBack" value="Return to Programs" onClick="document.location='programs.php'">
</span>
<br>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>                        