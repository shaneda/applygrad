
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$appid = -1;
$appName = "";

if(isset($_GET))
{
	if(isset($_GET['id']) && $_SESSION['userid'] != -1)
	{
		
		//VERIFY USER/APP COMBO
		$sql = "select id from application where user_id = ".$_SESSION['userid']." and id = ". intval($_GET['id']);
		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$appid = $row['id'];
		}
	}
}
if(isset($_POST['btnSubmit'] ))
{
	
	$appName = $_POST['txtName'];
	$appid = $_POST['txtAppId'];
	if($appName == "")
	{
		$err .= "You must supply a name for your application.";
	}
	if($_SESSION['userid'] > -1 && $appName != "" && $err == "")
	{
		if($appid == -1)
		{
			//INSERT
			$sql = "insert into application (name, user_id, created_date)values('".addslashes($appName)."', ".$_SESSION['userid'].",'".date("Y-m-d h:i:s")."')";
			mysql_query( $sql) or die(mysql_error());
			$appid = mysql_insert_id();
		}else
		{
			$sql = "update application set name = '".addslashes($appName)."', created_date='".date("Y-m-d h:i:s")."' where id = ".$appid;
			mysql_query( $sql) or die(mysql_error());
		}
		echo $sql;
		header("Location: home.php");
	}
}
//GET APP INFO
$sql = "select id, name from application where user_id = ".$_SESSION['userid']." and id = ". $appid ;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	//$_SESSION['appid'] = $row['id'];<br>
	$appName = stripslashes($row['name']);
}

?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Add/Edit Application </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<span class="errorSubtitle"><?=$err?></span>
            <table width="100%"  border="0" cellpadding="4" cellspacing="2" class="tblItem">
              <tr>
                <td width="100" align="right"><strong>Name of your application:</strong></td>
                <td><input name="txtAppId" type="hidden" id="txtAppId" value="<?=$appid?>">
                <? showEditText($appName, "textbox", "txtName", $_SESSION['allow_edit'], false,null,true,20); ?></td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td>Since you can create multiple applications, giving each one a unique name will help you to organize your applications more effectively </td>
              </tr>
              <tr>
                <td width="100" align="right">&nbsp;</td>
                <td><input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="Save"></td>
              </tr>
            </table>
            <!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
