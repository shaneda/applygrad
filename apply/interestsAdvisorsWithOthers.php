<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../apply/AdvisorsData.class.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$areas = array();
$myAreas = array();
$advisors = array();
$MyAdvisors = array();
$id = -1; // lu_application_programs.id
$progid = -1;
$interestChoices = array("1st Choice", "2nd Choice", "3rd Choice");
$advisorChoices = array("Faculty of Interest", "Faculty of Interest", "Faculty of Interest");

if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
}

if(isset($_GET['p']))
{
	$progid = intval($_GET['p']);
}

// For advisors
$deptNumber = getDeptFromProgram($progid);
$advSelType = getDeptAdvisorSelectionType($deptNumber);


if(isset($_POST['btnSubmit']))
{
	$hasAreas = false;
    $hasAdvisors = false;
	$vars = $_POST;
	$areasCount = 0;
    $advisorsCount = 0;
	$itemId = -1;
    $tempAdvisors = array();
    foreach($vars as $key1 => $value1)
    {
        if( strstr($key1, 'lbAdvisor') !== false )
        {
            $arr = split("_", $key1);
            $itemId = $arr[2];
            if( $itemId > -1 && $value1 != "")
            {
                if($value1 < 1)
                {
                    //$err .= "Please select an interest from each list<br>";
                }
                //echo $value1 . " ";
                array_push($tempAdvisors, $value1);
            }//END IF
            
        }
    }
    for($i = 0; $i<count($tempAdvisors); $i++)
    {
        if (intval($tempAdvisors[$i]) != 0) {
            // either id = 1 or a true id
            if (intval($tempAdvisors[$i]) == 1) {
                array_push($MyAdvisors, $tempAdvisors[$i]);
                $i++;
                array_push($MyAdvisors, $tempAdvisors[$i]);
            } else {
                array_push($MyAdvisors, $tempAdvisors[$i]);
            }
            
        } else {
            // skip the bogus value
        }
    }

	foreach($vars as $key1 => $value1)
	{
		if( strstr($key1, 'lbArea') !== false )
		{
			$hasAreas = true;
			$areasCount++;
			$arr = split("_", $key1);
			$itemId = $arr[2];
			$appProgId = $arr[1];
			if( $itemId > -1 && $value1 != "")
			{
				if($value1 < 1)
				{
					//$err .= "Please select an interest from each list<br>";
				}
				//echo $value1 . " ";
				array_push($myAreas, $value1);
			}//END IF
		}//END IF ADD
        
        /*
        if( strstr($key1, 'lbAdvisor') !== false )
        {
            $hasAdvisors = true;
            $advisorsCount++;
            $arr = split("_", $key1);
            $itemId = $arr[2];
            
            $appProgId = $arr[1];
            DebugBreak();
            if( $itemId > -1 && $value1 != "")
            {
                if($value1 < 1)
                {
                    //$err .= "Please select an interest from each list<br>";
                }
                //echo $value1 . " ";
                array_push($MyAdvisors, $value1);
            }//END IF
        }//END IF ADD
        */
        
        
	}//END FOR
	
    if($hasAreas == true && count($myAreas) == 0)
	{
        // Enforce AOI selection for SCS/RI.
        if ($domainid == 1 || $domainid == 3) {
            $err .= "Please select at least one area of interest for this program<br>";    
        }
		
	}
	$areasMatched = false;
	for($i = 0; $i<count($myAreas); $i++)
	{
		for($j = 0; $j<count($myAreas); $j++)
		{
			if(($j != $i) && ($myAreas[$j] == $myAreas[$i]))
			{
				$areasMatched = true;
			}
		}
	}
	if($areasMatched == true)
	{
		$err .= "Error: Please change your areas of interest selections. They must be unique.<br>";
	}
    
    $advisorsMatched = false;
      // add logic for advisors matching
    for($i = 0; $i<count($MyAdvisors); $i++)
    {
        for($j = 0; $j<count($MyAdvisors); $j++)
        {
            if(($j != $i) && ($MyAdvisors[$j] == $MyAdvisors[$i]) && $MyAdvisors[$j] != 1)
            {
                $advisorsMatched = true;
            }
        }
    }
    if($advisorsMatched == true)
    {
        $err .= "Error: Please change your faculty of interest selections. They must be unique.<br>";
    }
    
	if($err == "")
	{
		$sql = "DELETE FROM lu_application_interest WHERE app_program_id=".$appProgId;
		mysql_query($sql)or die(mysql_error().$sql);
		
		for($i = 0; $i<count($myAreas); $i++)
		{
			if($myAreas[$i] != "")
			{
				$sql = "insert into lu_application_interest(app_program_id, interest_id, choice)values( ".$appProgId." ,".$myAreas[$i].", ".$i.")";
				$result = mysql_query($sql) or die(mysql_error().$sql);
			}
		}
        
        // Huh?? Always update program requirement as complete on save??
        // Maybe because you can't select an AOI without having selected a program??
        updateReqComplete("programs.php",1);
        
        
        // Enforce AOI selection for SCS/RI
        if ($domainid == 1 || $domainid == 3) {
            
            foreach ($_SESSION['programs'] as $program) {
                
                $programId = $program[0];
                $programAreas = getAreas($programId);
                $myProgramAreas = getMyAreas($programId);
                
                // Set incomplete if the user doesn't have at least one AOI selected
                // for each program that has AOIs. 
                if (count($programAreas) > 0 && count($myProgramAreas) < 1) {
                    updateReqComplete("programs.php",0);
                    break;    
                }    
            }
        }

        switch ($advSelType) {
            case 1:
                $originalAdvisors = getMyAdvisors($progid);
                $originalAdvisorsIDs = array();
                for ($a = 0; $a<count($originalAdvisors); $a++) {
                    array_push($originalAdvisorsIDs, $originalAdvisors[$a][0]);
                    if (in_array($originalAdvisors[$a][0], $MyAdvisors) == FALSE) {
                        $sql = "DELETE FROM lu_application_advisor WHERE program_id=".$progid." and advisor_user_id = ".$originalAdvisors[$a][0]." and application_id = ".$_SESSION['appid'];
                        mysql_query($sql); 
                        } 
                    }
                for ($a = 0; $a<count($MyAdvisors); $a++) {
                    if (in_array($MyAdvisors[$a], $originalAdvisorsIDs) == FALSE) {
                        if($MyAdvisors[$a] != "")
                            {
                               $sql = "insert into lu_application_advisor(program_id, advisor_user_id, application_id)values( ".$progid." , '".addslashes($MyAdvisors[$a])."', ".$_SESSION['appid'].")";
                               $result = mysql_query($sql) or die(mysql_error().$sql);
                            }
                    }
                }
                break;
                
            case 2:
                $originalAdvisors = getMyAdvisorsWithAppId($_SESSION['appid'], $progid);
                for ($a = 0; $a<count($originalAdvisors); $a++) {
                    if (in_array($originalAdvisors[$a], $MyAdvisors) == FALSE) {
                        if($originalAdvisors[$a] != "") {
                            $sql = "DELETE FROM lu_application_advisor WHERE program_id=".$progid." and name = '".addslashes($originalAdvisors[$a])."' and application_id = ".$_SESSION['appid'];
                             mysql_query($sql) or die(mysql_error().$sql); 
                             }
                    }
                }
                for ($a = 0; $a<count($MyAdvisors); $a++) {
                    if (in_array($MyAdvisors[$a], $originalAdvisors) == FALSE) { 
                        if($MyAdvisors[$a] != "")
                            {
                               $sql = "insert into lu_application_advisor(program_id, name, application_id)values( ".$progid." , '".addslashes($MyAdvisors[$a])."', ".$_SESSION['appid'].")";
                               $result = mysql_query($sql) or die(mysql_error().$sql);
                            }
                    }
                } 
                break;
            case 3:
                $originalAdvisors = getMyAdvisorsWithOthers($progid);
                // need to handle removal and building lists
                $originalAdvisorsIDs = array();
                $originalAdvisorsText = array();
                for ($a = 0; $a<count($originalAdvisors); $a++) {
                    if ($originalAdvisors[$a][0] == 1) {
                       array_push($originalAdvisorsText, $originalAdvisors[$a][2]); 
                    } else {
                        array_push($originalAdvisorsIDs, $originalAdvisors[$a][0]);
                    }
                    
                    if ($originalAdvisors[$a][0] == 1) {
                        // handle the check by name
                        if (in_array($originalAdvisors[$a][2], $MyAdvisors) == FALSE) {
                            $sql = "DELETE FROM lu_application_advisor WHERE program_id=".$progid." and name = '".addslashes($originalAdvisors[$a][2])."' and application_id = ".$_SESSION['appid'];
                             mysql_query($sql) or die(mysql_error().$sql);
                        }
                    } else if (in_array($originalAdvisors[$a][0], $MyAdvisors) == FALSE) {
                        $sql = "DELETE FROM lu_application_advisor WHERE program_id=".$progid." and advisor_user_id = ".$originalAdvisors[$a][0]." and application_id = ".$_SESSION['appid'];
                        mysql_query($sql); 
                        } 
                }
                
                for ($a = 0; $a<count($MyAdvisors); $a++) {
                    if ($MyAdvisors[$a] == 1) {
                            $a++;
                            if($MyAdvisors[$a] != "" && $MyAdvisors[$a] != 1 && (in_array($MyAdvisors[$a], $originalAdvisorsText) == FALSE)) {
                                $sql = "insert into lu_application_advisor(program_id, name, application_id, advisor_user_id)values( ".$progid." , '".addslashes($MyAdvisors[$a])."', ".$_SESSION['appid'].", 1)";
                                $result = mysql_query($sql) or die(mysql_error().$sql);
                            }
                    } else {
                        if (intval($MyAdvisors[$a]) !=0 && (in_array($MyAdvisors[$a], $originalAdvisorsIDs) == FALSE)) {
                            if($MyAdvisors[$a] != "" &&  $MyAdvisors[$a] != 1)
                                {
                                   $sql = "insert into lu_application_advisor(program_id, advisor_user_id, application_id)values( ".$progid." , '".addslashes($MyAdvisors[$a])."', ".$_SESSION['appid'].")";
                                   $result = mysql_query($sql) or die(mysql_error().$sql);
                                }
                        }
                    }
                } 
                break;
        }  
	}

}//end if submit

function getAreas($programId = NULL)
{
    global $progid;
    
    if ($programId) {
        $aoiProgramId = $programId;
    } else {
        $aoiProgramId = $progid;    
    }
    
	$ret = array();
	//GET AREAS OF INTEREST
	$sql = "select interest.id,
	interest.name from
	lu_programs_interests 
	inner join interest on interest.id = lu_programs_interests.interest_id 
	where lu_programs_interests.program_id=" . intval($aoiProgramId). " order by name";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row[1]);
		array_push($ret, $arr);
	}
	return $ret;
}

function getMyAreas($id)
{
	$ret = array();
	$sql = "SELECT 
	interest.id,
	interest.name
	FROM 
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.program_id=".$id." and lu_application_programs.application_id=".$_SESSION['appid'] . " order by lu_application_interest.choice" ;

	$result = mysql_query($sql)	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

// Include the shared page header elements.
$headJavascriptFiles = array(
    '../inc/interests.js'
);
$pageTitle = 'Areas and Faculty of Interest';
include '../inc/tpl.pageHeaderApply.php';
?>

<p>
<b>You must select at least one</b>, but no more than three, areas of interest that are most closely related to your current research interests. 
This does not limit your application to any specific area, but rather gives the Admissions Committee a general idea of your interests.
</p>

<?
// For advisors
$domainid = 1;
if(isset($_SESSION['domainid']))
{
    if($_SESSION['domainid'] > -1)
    {
        $domainid = $_SESSION['domainid'];
    }
}
$sql = "select content from content where name='Faculty of Interest' and domain_id=".$domainid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}
?>

<span class="errorSubtitle"><?=$err?></span>
<!--- <br> ---> 
<table border="0" cellpadding="10" cellspacing="10">
<tr><td valign="top">
<h3>Areas of Interest</h3>

<table border="0" cellspacing="2" cellpadding="4">

<?php
$isCsdProgram = isCsdProgram($progid);
$isRiProgram = isRiProgram($progid);
$isPsychologyProgram = isPsychologyProgram($progid);
if ($isCsdProgram || $isRiProgram) {
    
    if ($isCsdProgram) {
        $researchUrl = 'http://www.csd.cs.cmu.edu/research/index.html';
        $researchTitle = 'Areas of Research guide';   
    } 
    if ($isRiProgram) {
        $researchUrl = 'http://www.ri.cmu.edu/research_guide/index.html';
        $researchTitle = 'Robotics Institute Research Guide';    
    }
    
?>
    <tr>
    <td colspan="2">
    <a href="<?php echo $researchUrl; ?>" target="_blank">Click here for the <?php echo $researchTitle; ?></a>
    <br><br>
    </td>
    </tr>
<?php 
}
?>
    <tr><td>
	  <? 
	  $areas = getAreas();
  	  $myAreas = getMyAreas($progid);

	  $choicesCount = 3;
	  if(count($areas) < 3)
	  {
		$choicesCount = count($areas);
	  }
	  for($j=0; $j< $choicesCount;$j++) {?>
		
		
		<div class="tblItem">
		  <? 
		  echo "<strong>".$interestChoices[$j]."</strong><br>";
		  $myArea = "";
		  if(count($myAreas) > $j)
		  {
			$myArea = $myAreas[$j];
		  }
		//  echo "myArea ".$myArea[0]."<br>";
            if ($j == 0) {
                showEditText( $myArea, "listbox", "lbArea_".$id."_".$j, $_SESSION['allow_edit'],true, $areas);    
            } else {
		        showEditText( $myArea, "listbox", "lbArea_".$id."_".$j, $_SESSION['allow_edit'],false, $areas); 
            }?>
		</div>
        <br/><br/>
	  
	  <? }//END FOR CHOICESCOUNT ?>
	</td></tr>
</table>

</td>
<td valign="top">
<h3>Faculty of Interest</h3>

<table border="0" cellspacing="2" cellpadding="4">

<?php
if ($isCsdProgram || $isRiProgram || $isPsychologyProgram) {

    if ($isCsdProgram) {
        $facultyUrl = 'http://www.csd.cs.cmu.edu/research/faculty_research/';
        $facultyTitle = 'Faculty Research Guide';   
    } 
    if ($isRiProgram) {
        $facultyUrl = 'http://www.ri.cmu.edu/research_keyword.html?menu_id=260';
        $facultyTitle = 'Faculty Research Interests guide';    
    }
    if ($isPsychologyProgram) {
        $facultyUrl = 'http://www.psy.cmu.edu/people/faculty.html';
        $facultyTitle = 'Psychology Faculty Guide';    
    }
?>
    <tr>
    <td>
    <a href="<?php echo $facultyUrl; ?>" target="_blank">Click here for the <?php echo $facultyTitle; ?></a>
    <br><br>
    </td>
    </tr>
<?php 
}
?>
    <tr><td>            
      <? 
      if ($advSelType ==  1) {
        $advisors = getProgramAdvisors($progid);
        $myAdvisors = getMyAdvisors($progid);
      } else  if ($advSelType ==  2) {
                $myAdvisors = getMyAdvisorsWithAppId($_SESSION['appid'], $progid);
      } else if ($advSelType ==  3) {
        $advisors = getProgramAdvisors($progid);
        $advisors[] = array(1, "Other");
        $myAdvisors = getMyAdvisorsWithOthers($progid);
        $myAdvisors2 = getMyAdvisorsWithAppId($_SESSION['appid'], $progid);
      }

      $choicesCount = 3;
/*      if(count($advisors) < 3)
      {
        $choicesCount = count($advisors);
      }
      */
      if ($advSelType == 3) {
            for($j=0; $j< $choicesCount;$j++) { ?>
        
                <div class="tblItem">
                  <? 
                  echo "<strong>".$advisorChoices[$j]."</strong><br>";
                  $myAdvisor = "";
                  if(count($myAdvisors) > $j)
                  {
                    $myAdvisor = $myAdvisors[$j];
                  }
                  showEditText( $myAdvisor, "listbox", "lbAdvisor_".$id."_".$j, $_SESSION['allow_edit'],false, $advisors);
                  ?>
                </div>
                <?php   
                            if (is_array($myAdvisor) &&  $myAdvisor[0] == 1) {
                                echo  '<span id="spanOthers' . $j . '">Other : <input type="text" name="lbAdvisor_' . $id . '_other_' . $j .'" 
                                   id="lbAdvisor_' . $id . '_other_' . $j . '" value= "' . $myAdvisor[2] . '" /></span>'; 
                            }
                            else
                                echo  '<span id="spanOthers' . $j . '" style="display:none;">Other : <input type="text" name="lbAdvisor_' . $id . '_other_' . $j .'" id="lbAdvisor_' . $id . '_other_' . $j . '" /></span>';

                    //    }
                    ?>
                <br/><br/>
              
              <? } 
      } else {
      for($j=0; $j< $choicesCount;$j++) { ?>
        
        <div class="tblItem">
          <? 
          echo "<strong>".$advisorChoices[$j]."</strong><br>";
          $myAdvisor = "";
          if(count($myAdvisors) > $j)
          {
            $myAdvisor = $myAdvisors[$j];
          }
          switch ($advSelType) {
              case 1:
                showEditText( $myAdvisor, "listbox", "lbAdvisor_".$id."_".$j, $_SESSION['allow_edit'],false, $advisors);
                break;
              case 2: 
                showEditText( $myAdvisor, "textbox", "lbAdvisor_".$id."_".$j, $_SESSION['allow_edit'],false, null, true, 30);
                break;
              case 3:
                showEditText( $myAdvisor, "listbox", "lbAdvisor_".$id."_".$j, $_SESSION['allow_edit'],false, $advisors);
                break;
          }?>
        </div>
        <br/><br/>
      
      <? }
      }//END FOR CHOICESCOUNT ?>
    </td></tr>

</table>

</td>
</tr>
</table>

<br>
<? showEditText("  Save  ", "button", "btnSubmit", $_SESSION['allow_edit']); ?>              
<input class="tblItem" name="btnAreas_" type="button" id="btnBack" value="Return to Programs" onClick="document.location='programs.php'">

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>