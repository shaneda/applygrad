
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$NUMRECS = 0; //DEFAULT NUMBER OF RECOMMENDERS
$buckley = "";
$buckleys = array(array(1, 'I waive'), array(0,'I do not waive'));
$myRecommenders = array();
$types = array();
$myPrograms = array();

$applastname = "";
$appfirstname = "";
$appemail = "";
$appdegree = "";
$appfield = "";
$appschool = "";
$enddate = "";
$err = "";
$err_2 = "";
$comp = true;
$sysemail = "applyweb@cs.cmu.edu";

//GET SYSTEM EMAIL
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select sysemail from systemenv where domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{ 
	$sysemail = $row['sysemail'];
}

//GET USER INFO THAT WE'LL NEED
$sql = "SELECT programs.id, 
degree.name as degreename,
fieldsofstudy.name as fieldname, 
choice, 
lu_application_programs.id as itemid,
users.firstname,
users.lastname,
users.email,
department.name as departmentname,
schools.name as schoolname,
programs.linkword
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
inner join application on application.id = lu_application_programs.application_id
inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
inner join users on users.id = lu_users_usertypes.user_id

inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
inner join department on department.id = lu_programs_departments.department_id
inner join schools on schools.id = department.parent_school_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice limit 1";
//echo $sql."<br>";
//echo $_SESSION['userid']."<br>";

$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result )) 
{
	$applastname = stripslashes($row['lastname']);
	$appfirstname = stripslashes($row['firstname']);
	$appemail = stripslashes($row['email']);
	$appdegree = $row['degreename'];
	$appfield = $row['fieldname'];
	$appschool = $row['schoolname'];
	$arr = array();
	array_push($arr, $row['id']);
	array_push($myPrograms, $arr);
	
}

//GET REQUIRED RECOMMENDATION TYPES
$sql = "select recommendationtypes.id, 
recommendationtypes.name,
numrequired, 
lu_programs_recommendations.program_id
from
	lu_programs_recommendations 
	inner join recommendationtypes on recommendationtypes.id = lu_programs_recommendations.recommendationtype_id 
	inner join lu_application_programs on lu_application_programs.program_id = lu_programs_recommendations.program_id
	where lu_application_programs.application_id=".$_SESSION['appid']."
	order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$matched = marray_search( $row["name"], $types);
	//echo "matched".$matched."<br>";
	if($matched  === false)
	{
	//echo $row["name"] . " ". $row["numrequired"]. " ". $row['program_id'] ."<br>";
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($arr, $row["numrequired"]);
		array_push($types, $arr);
	}
	
}

//RETRIEVE USER INFORMATION
//USER MUST HAVE AT LEAST X RECOMMENDES SO ADD IT IF IT DOESN'T EXIST
$tmpNumRecs = 0;
$tmpType = -1;
for($i = 0; $i < count($types); $i++)
{
//echo $types[$i][1]." ".$types[$i][2]. "<br>";
	if($tmpType != $types[$i][1])
	{
		$tmpNumRecs += $types[$i][2];
		$tmpType = $types[$i][1];
	}
	
	
}
//echo $tmpNumRecs;
if($tmpNumRecs > $NUMRECS)
{
	$NUMRECS = $tmpNumRecs;
}

function addRec($allowDup, $type)
{
	global $err;
	
	$sql = "insert into recommend(application_id, rec_user_id, recommendtype)values(".$_SESSION['appid'].",-1, ".$type.")";
	mysql_query($sql) or die(mysql_error().$sql);
	
}

//INSERT RECORD TABLE FOR USER 
if(isset($_POST['btnAdd']))
{
	$allowdups = true;
	addRec($allowdups, 3);
}//END POST BTNADD



if( isset( $_POST ) && !isset($_POST['btnAdd']) )
{
	$vars = $_POST;
	$itemId = -1;
	$uid = -1;
	if(isset( $_POST["lbBuckley"]))
	{
		$buckley = $_POST["lbBuckley"];
	}
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnSendmail') !== false ) 
		{
			$arr = split("_", $key);
			$email = "";
			$record = intval($_POST["txtRecord_".$arr[1]]);
			if($_POST["txtUid_".$arr[1]] != "")
			{
				$uid = $_POST["txtUid_".$arr[1]];
				$itemId = $_POST["txtRecord_".$arr[1]];
				$email = $_POST["txtEmail_".$arr[1]];
				$reminderSentCount = intval($_POST["txtRecSent_".$arr[1]]);
			}
			if($reminderSentCount < 3)
			{
				$pass = "";
				$sql = "select users.guid, lu_users_usertypes.user_id, lu_users_usertypes.id from users  
                inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
                where lu_users_usertypes.id=".intval($_POST["txtUid_".$arr[1]]);
				$result = mysql_query($sql)or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					$pass = $row['guid'];
                    $accesscode = $row['user_id']."-".$_SESSION['domainid']."-".$row['id'];
				}
				$vars = array(
				array('email',$email), 
				array('user',str_replace("+","%2b",$email)),	
				array('pass',$pass),	
				array('enddate',formatUSdate3($_SESSION['expdate']) ),
				array('replyemail',$sysemail),
				array('lastname',$applastname),
				array('firstname',$appfirstname),
				array('appemail',$appemail),
				array('degree',$appdegree),
				array('field',$appfield),
				array('school',$appschool),
				array('domainid',$_SESSION['domainid']),
                array('accesscode',$accesscode)
				);
				sendMail(1,$vars, $email);
				$reminderSentCount++;
				$sql = "update recommend set reminder_sent_count=".$reminderSentCount. " where id=".$itemId;
				mysql_query($sql)	or die(mysql_error().$sql);
			}else
			{
				$err .= "You cannot send more than 3 recommendation reminders.<br>";
				$err_2 .= "You cannot send more than 3 recommendation reminders.<br>";
			}
		}
		if( strstr($key, 'btnSendReminder') !== false ) 
		{
			$arr = split("_", $key);
			$email = "";
			$record = intval($_POST["txtRecord_".$arr[1]]);
			if($_POST["txtUid_".$arr[1]] != "")
			{
				$uid = $_POST["txtUid_".$arr[1]];
				$itemId = $_POST["txtRecord_".$arr[1]];
				$email = $_POST["txtEmail_".$arr[1]];
				$reminderSentCount = intval($_POST["txtRecSent_".$arr[1]]);
			}
			if($reminderSentCount < 3)
			{
				$pass = "";
				$sql = "select guid, lu_users_usertypes.user_id, lu_users_usertypes.id from users 
				inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
				where lu_users_usertypes.id=".intval($_POST["txtUid_".$arr[1]]);
				$result = mysql_query($sql)or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					$pass = $row['guid'];
                    $accesscode = $row['user_id']."-".$_SESSION['domainid']."-".$row['id'];
				}
				$vars = array(
				array('email', $email), 
				array('user',str_replace("+","%2b",$email)),
				array('pass',$pass),
				array('enddate',formatUSdate3($_SESSION['expdate']) ),
				array('replyemail',$sysemail),
				array('lastname',$applastname),
				array('firstname',$appfirstname),
				array('appemail',$appemail),
				array('degree',$appdegree),
				array('field',$appfield),
				array('school',$appschool),
				array('username',$email),
				array('domainid',$_SESSION['domainid']),
                array('accesscode',$accesscode)
				);
				sendMail(2,$vars, $email);
				$reminderSentCount++;
				$sql = "update recommend set reminder_sent_count=".$reminderSentCount. " where id=".$itemId;
				mysql_query($sql)	or die(mysql_error().$sql);
			}else
			{
				$err .= "You cannot send more than 3 recommendation reminders.<br>";
				$err_2 .= "You cannot send more than 3 recommendation reminders.<br>";
			}
		}
		if( strstr($key, 'btnDel') !== false ) 
		{
			$arr = split("_", $key);
			$umasterid = -1;
			if($_POST["txtUid_".$arr[1]] != "")
			{
				$uid = $_POST["txtUid_".$arr[1]]; 
				$umasterid = $_POST["txtUMasterid_".$arr[1]]; 
			}else
			{
				$err .= "record not found.";
			}
			/*
			//GET USER'S RECOMMENDER IDENTITY ID
			$sql = "select id from lu_users_usertypes where id=".$itemId. " and usertype_id = 6 ";
			$result = mysql_query($sql)	or die(mysql_error().$sql);
			while($row = mysql_fetch_array( $result )) 
			{
				$uid = $row['id'];
				$identityCounts++;
			}
			*/
			$sql = "DELETE FROM recommend WHERE application_id=".$_SESSION['appid']. " and rec_user_id=" . $uid;
			mysql_query($sql) or die(mysql_error().$sql );
			
			//CHECK TO SEE IF THIS RECOMMENDER IS ASSIGNED TO ANYONE ELSE. IF NOT, REMOVE RECOMMENDER IDENTITY ONLY
			$sql = "select id from recommend where rec_user_id=".$uid;
			$result = mysql_query($sql)	or die(mysql_error());
			$doDelete = true;
			while($row = mysql_fetch_array( $result )) 
			{
				$doDelete = false;
			}
			if($doDelete == true)
			{
				$sql = "DELETE FROM  lu_users_usertypes WHERE id=".$uid. " and usertype_id=6";
				mysql_query($sql) or die(mysql_error().$sql );
				if(mysql_affected_rows() > 0)
				{
					$sql = "DELETE FROM usersinst WHERE user_id=".$uid;
					mysql_query($sql) or die(mysql_error().$sql );
					
					$sql = "DELETE FROM  users_info WHERE user_id=".$uid;
					mysql_query($sql) or die(mysql_error().$sql );
					$updateApp = true;
					
					//SEE IF THERE ARE OTHER ACCOUNT TYPES ASSIGNED TO THIS USER. IF NOT, DELETE USER.
					$sql = "select id from lu_users_usertypes where user_id=".$umasterid;
					$result = mysql_query($sql)	or die(mysql_error());
					if(mysql_num_rows($result) == 0)
					{
						$sql = "DELETE FROM  users WHERE id=".$umasterid;
						mysql_query($sql) or die(mysql_error().$sql );
					}else
					{
						while($row = mysql_fetch_array( $result )) 
						{
							//echo $row['id'. "<br>"];
						}
					}
				}
			}//end if dodelete
		}//end btnDel
	}//END FOR
}//END IF

//UPDATE
if( isset($_POST['btnSave'])) 
{
	$vars = $_POST;
	$itemId = -1;
	$tmpid = -1;
	$arrItemId = array();
        $err_2 = "";
	
	$buckley = $_POST["lbBuckley"];
	if($buckley == "")
	{
		$err .= "Buckley choice is Required<br><br>";
                $err_2 .= "ERROR: Buckley choice is Required<br>";
	}
	if($err == "")
	{
		$sql = "update application set buckleywaive=".$buckley." where id=".$_SESSION['appid'];
		mysql_query( $sql) or die(mysql_error());
	}
	
	//HERE WE GET THE RECORD IDS FOR THE UPDATED RECOMMENDERS
	foreach($vars as $key => $value)
	{
		$arr = split("_", $key);
		if($arr[0] == "lbType")
		{
			$tmpid = $arr[1];
			if($itemId != $tmpid)
			{
				$itemId = $tmpid;
				array_push($arrItemId,$itemId);
			}
		}
	}//END FOR
	//ITERATE THROUGH IDS AND DO UPDATES
	for($i = 0; $i < count($arrItemId); $i++)
	{
		$type = 1;
		$sendmail = 0;
		$reminderCount = 0;
		$pass = "";
		$guid = "";
		
		$umasterid = $_POST["txtUMasterid_".$arrItemId[$i]];
		$uid = $_POST["txtUid_".$arrItemId[$i]];
		$firstname= htmlspecialchars($_POST["txtFname_".$arrItemId[$i]]);
		$lastname= htmlspecialchars($_POST["txtLname_".$arrItemId[$i]]);
		$title= $_POST["txtTitle_".$arrItemId[$i]];
		$affiliation= $_POST["txtAffiliation_".$arrItemId[$i]];
		$email= stripslashes($_POST["txtEmail_".$arrItemId[$i]]);
		$phone= htmlspecialchars($_POST["txtPhone_".$arrItemId[$i]]);
		if(isset($_POST["lbType_".$arrItemId[$i]]))
		{
			$type = intval($_POST["lbType_".$arrItemId[$i]]);
		}
		if(isset($_POST["chkSendmail_".$arrItemId[$i]]))
		{
			$sendmail = intval($_POST["chkSendmail_".$arrItemId[$i]]);
		}
		if(isset($_POST["txtRemSent_".$arrItemId[$i]]))
		{
			$reminderCount = intval($_POST["txtRemSent_".$arrItemId[$i]]);
		}
		
		//DO VERIFICATION
		if($firstname == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. First name is required<br>";
		}
		if($lastname == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. Last name is required<br>";
		}
		if($title == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. Title is required<br>";
		}
		if($email == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. Email is required<br>";
		}
		if($affiliation == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. Company is required<br>";
		}
		if($phone == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. Phone is required<br>";
		}
		
		if($email == "")
		{
			$err = "Recommender ".($i+1)." is incomplete. A valid email is required<br>";
		}else
		{
			if(check_email_address($email)===false)
			{
				$err = "Recommender ".($i+1)." is incomplete. A valid email is required<br>";
			}
		}
		

		//POPULATE ARRAY FOR POSTBACK
		$arr = array();
		
		array_push($arr, $arrItemId[$i]);
		array_push($arr, $_POST["txtUid_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtFname_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtLname_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtTitle_".$arrItemId[$i]] );
		array_push($arr, $_POST["txtAffiliation_".$arrItemId[$i]] );
		array_push($arr, $_POST["txtEmail_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtPhone_".$arrItemId[$i]]);
		array_push($arr, $_POST["lbType_".$arrItemId[$i]]);
		if(isset($_POST["txtDatafile_".$arrItemId[$i]])){array_push($arr, $_POST["txtDatafile_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtModdate_".$arrItemId[$i]])){array_push($arr, $_POST["txtModdate_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtSize_".$arrItemId[$i]])){array_push($arr, $_POST["txtSize_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtExtension_".$arrItemId[$i]])){array_push($arr, $_POST["txtExtension_".$arrItemId[$i]]);}else{array_push($arr,"");}
		array_push($arr, $_POST["txtRecSent_".$arrItemId[$i]]);
		if($err == "")
		{
			array_push($arr, 0);//NO ERROR
		}else
		{
			array_push($arr, 1);//HAS ERROR
		}
		array_push($arr, $_POST["txtUMasterid_".$arrItemId[$i]]);
		array_push($myRecommenders, $arr);


		// New ERROR Messaages

		if ($err != "")
		{
		  $err_2 .= "<BR>ERROR: Recommender ".($i+1). " information is incomplete or invalid. Please verify all data has been provided and is correct.<br>";
		}

		if ( $myRecommenders[$i][13] == 0) 
		{
		   if($myRecommenders[$i][6] != "") // Is there an email address?
		   {
				 $err_2 .= "NOTE: Email request has NOT been sent to Recommender ".($i+1)."<BR>";
		   }
		}


		/////////////
		//INSERT DATA
		/////////////
		if($err == "")
		{
			$umasterid = -1;
			//LOOKUP USER
			//echo "looking up user ";
			$sql = "select id from users where users.firstname='".addslashes($firstname)."' and users.lastname='".addslashes($lastname)."' and users.email='".addslashes($email)."'";
			$result = mysql_query($sql)or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$umasterid = $row['id'];
				//echo "record found " . $row['id'];
			}
			//IF USER DOESN'T EXIST, ADD
			if($umasterid == -1)
			{
				$pass = STEM_GeneratePassword();
				$guid = makeGuid();
				$sql = "insert into users(email, password,title,firstname,lastname, signup_date, verified, guid) 
				values( '".addslashes($email)."', '".$pass."', '".addslashes($title)."', '".addslashes($firstname)."', '".addslashes($lastname)."', '".date("Y-m-d h:i:s")."', 0, '".$guid."')";
				$result = mysql_query($sql)or die(mysql_error());
				$umasterid = mysql_insert_id();
			}
			//FIND EXISTING RECOMMENDATION ACCOUNT FOR USER (IF EXISTS)
			$sql = "select lu_users_usertypes.id, users.id as umasterid from users 
				inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
				where lu_users_usertypes.user_id=".$umasterid . " and lu_users_usertypes.usertype_id=6";
			$result = mysql_query($sql) or die(mysql_error(). $sql);
			while($row = mysql_fetch_array( $result )) 
			{
				$uid = $row['id'];
				//echo "found user type ";
			}
			//INSERT RECOMMENDATION RECORD IF IT DOESN'T EXIST
			//  DebugBreak();
			if($uid == -1)
			{
				$sql = "insert into lu_users_usertypes(user_id, usertype_id,domain)values( ".$umasterid.", 6, NULL)";
				mysql_query( $sql) or die(mysql_error());
				$uid = mysql_insert_id();
				//echo "inserting recommend id ". $uid. "<br>";
			}
			//FIND USERINFO FOR USER
			$doInsert = true;
			$sql = "select id from users_info where user_id=".$uid;
			$result = mysql_query($sql)or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$doInsert = false;
			}
			if($doInsert == true)
			{
				$sql = "insert into users_info(user_id,company, address_cur_tel,address_perm_tel)values(".$uid.",'".$affiliation."'	,'".addslashes($phone)."',	'".addslashes($phone)."')";
				mysql_query( $sql) or die(mysql_error());
				//echo "inserting userinfo<br>";
			}
			//UPDATE RECOMMENDATION RECORD
			$sql = "update recommend 
			set application_id = ".$_SESSION['appid'].", 
			rec_user_id =".$uid.", 
			recommendtype = ".$type." 
			where id=".$arrItemId[$i];
			mysql_query( $sql) or die(mysql_error());
			
			//GET DATAFILE INFO FOR RECOMMENDATION
			$sql = "select datafile_id from recommend where id=".$arrItemId[$i];
			$result = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows( $result ) == 0)
			{
				//$comp = false;
			}
			while($row = mysql_fetch_array( $result )) 
			{
				if($row['datafile_id'] > 0)
				{
					$letter = "(Letter of Recommendation Received)";
				}
			}
			updateReqComplete("recommendations.php", 1, 1);
			//header("Location: home.php");
		}
	}//end for
	$tmpRecs = getInfo();
	for($i = 0; $i < count($tmpRecs); $i++)
	{
		for($j = 0; $j < count($myRecommenders); $j++)
		{
			if($tmpRecs[$i][0] == $myRecommenders[$j][0])
			{
				if($myRecommenders[$j][14] == 0)
				{
					$myRecommenders[$j] = $tmpRecs[$i];
				}
			}
		}
		
	}
		
	
}//end if btnsave


$doAdd = true;
$sql = "select id from recommend where application_id = ".$_SESSION['appid'] ;
$result = mysql_query($sql)	or die(mysql_error());
$count = mysql_num_rows( $result );
for($i = $count; $i < $NUMRECS; $i++)
{
	addRec(false, 1);
}


if(!isset($_POST['btnSave']))
{
	$myRecommenders = getInfo();
}//end if btnSave

for($i = 0; $i < count($myRecommenders); $i++)
{
	if($myRecommenders[$i][2] == "")
	{
		$comp = false;
	}
}
if(count($myRecommenders) >=  $NUMRECS && $comp == true && $err == "")
{
	
	if($comp == true)
	{
		if(isset($_POST['btnSave']))
		{
			updateReqComplete("recommendations.php", 1);
		}else
		{
			updateReqComplete("recommendations.php", 1, 1);
		}
	}
	
}else
{
	updateReqComplete("recommendations.php", 0,1, 0);
}

function getInfo()
{
	global $buckley;
	$ret = array();
	$letter = "";
	//RETRIEVE USER INFORMATION
	$sql = "SELECT 
	recommend.id,
	recommend.rec_user_id,
	recommend.datafile_id,
	recommend.submitted,
	recommend.recommendtype,
	users.id as usermasterid,
	users.title,
	users.firstname,
	users.lastname,
	users.email,
	users_info.address_perm_tel, 
	lu_users_usertypes.id as uid,
	users_info.company,
	reminder_sent_count,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM recommend
	left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
	left outer join users on users.id = lu_users_usertypes.user_id
	left join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
	where application_id=".$_SESSION['appid']. " order by recommend.id";
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['rec_user_id']);
		array_push($arr, stripslashes($row['firstname']));
		array_push($arr, stripslashes($row['lastname']));
		array_push($arr, stripslashes($row['title']));
		array_push($arr, stripslashes($row['company']));
		array_push($arr, stripslashes($row['email']));
		array_push($arr, stripslashes($row['address_perm_tel']));
		array_push($arr, $row['recommendtype']);
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($arr,$row['reminder_sent_count']);
		array_push($arr, 0);//ARRAY INFO
		array_push($arr, $row['usermasterid']);
		array_push($ret, $arr);
	}
	
	$sql = "select buckleywaive from application where id=".$_SESSION['appid'];
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$buckley = $row['buckleywaive'];
	}
	return $ret;
}

function sendMail($type,$vars, $email)
{
	$str = "";
        $clean_str = "";
	global $domainid;
	global $sysemail;
	global $appfirstname;
	global $applastname;
	switch($type)
	{
		case 1:
			$sql = "select content from content where name='Recommendation Letter' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$str = $row["content"];
			}
			$str = parseEmailTemplate2($str, $vars );
                        /* This cleans the string of the html representation of & */
                        /* which is causing problems for recommenders accessing the site */
                        /* needs to be fixed in the mail template tinyMCE tool */
                        $clean_str = str_replace("&amp;", "&", $str);
			sendHtmlMail($sysemail, $email, "Recommendation requested for ".$appfirstname." " . $applastname, $clean_str, "recommend");
		break;
		case 2:
			//GET TEMPLATE CONTENT
			$sql = "select content from content where name='Recommendation Reminder Letter' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$str = $row["content"];
			}
			$str = parseEmailTemplate2($str, $vars );
                        /* This cleans the string of the html representation of & */
                        /* which is causing problems for recommenders accessing the site */
                        /* needs to be fixed in the mail template tinyMCE tool */
                        $clean_str = str_replace("&amp;", "&", $str);
			sendHtmlMail($sysemail, $email, "Reminder: Recommendation reminder for ". $appfirstname." ".$applastname, $clean_str, "recreminder");
		break;
	}
}


?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			<!-- InstanceBeginEditable name="EditNav" -->
            <tr>
            <td>
                <table>
                <colgroup>
                <col width=80% style=text-align:left;>
                <col width=20% style=:right;>
                </colgroup>
                <tr>
            <td class=tblItem>
                <?php
                   echo  "&nbsp;&nbsp;<a href=\"resume.php\">< Previous </a>
                &nbsp; | &nbsp;<a href=\"suppinfo.php\">Next ></a>";
             ?>
            </td>
            
            <td style="text-align:right; width:130px">    
         
                <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                ){ ?>
                    <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                    <!-- DAS Removed - <? echo $sesEmail;?>   -->
                    <? }else
                        {
                            if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                            && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                            {
                            //session_unset();
                            //destroySession();
                            //header("Location: index.php");
                        ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                }
            } ?>
            </td>
            <tr>
            </table>
            </td>
            </tr>
            <!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Recommenders</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<br>
			<span class="tblItem">
			<?
			$domainid = 1;
			if(isset($_SESSION['domainid']))
			{
				if($_SESSION['domainid'] > -1)
				{
					$domainid = $_SESSION['domainid'];
				}
			}
			$sql = "select content from content where name='Recommenders' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				echo html_entity_decode($row["content"]);
			}
			?>
			</span><br><br>
<?                      // show status if entering page without result of a SAVE                         
                        if( !isset($_POST['btnSave'])) 
                        {
  			  for($i = 0; $i < count($myRecommenders); $i++)
                          {
                            // has a recommendation been sent and email address present
                            if($myRecommenders[$i][13] == 0 && $myRecommenders[$i][6] != "" ) 
                            {
                                $err_2 .= "NOTE: Email request has NOT been sent to Recommender ".($i+1)."<BR>";
                            }
                          }
                        }
?>

<!--			<span class="subtitle"><?=$err." ";?></span> -->
			<span class="subtitle"><?=$err_2." ";?></span>
            
			<br>

			<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
			<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
			</div><span class="tblItem">= required</span><br>
			<br>

            <span class="subtitle">
            <? if ($buckley == NULL) {
                    showEditText("Save Buckley", "button", "btnSave", $_SESSION['allow_edit']);
                    } else {
                                showEditText("Save Recommenders", "button", "btnSave", $_SESSION['allow_edit']);
                                } ?>
            </span>
			<? if ($buckley != NULL) {
                    showEditText("Add Recommender", "button", "btnAdd", $_SESSION['allow_edit']);} ?>
            <br>
            <br>
            <hr size="1" noshade color="#990000">
            <span class="subTitle"> <b>Family Education Rights and Privacy Act(FERPA) (Buckley Amendment):</b></span><br>
			Under the provisions of this act you have the right, if you enroll at Carnegie Mellon University, to review your educational records. The act further provides that you may waive your right to see recommendations for admission. Please indicate in the box below whether you wish to waive or not waive that right. This information will be provided to each recommender listed.<br>
			<br>
			<?
			if($_SESSION['allow_edit'] == true)
			{
				showEditText($buckley, "listbox", "lbBuckley", $_SESSION['allow_edit'],true, $buckleys); 
			}else
			{
				if($buckley == 1)
				{
					echo "I waive";
				}else
				{
					echo "I do not waive";
				}
			}
			?>
			<br>
	   		<hr size="1" noshade color="#990000">
            
			<?
            DebugBreak();
            if ($buckley != NULL)  {
			for($i = 0; $i < count($myRecommenders); $i++)
			{
				$allowEdit = $_SESSION['allow_edit'];
				$submitted = "";
				if($myRecommenders[$i][1] > -1)
				{
					$allowEdit = false;
				}
				$title = "";
				switch($myRecommenders[$i][8])
				{
					case "1":
						$title = "Undergraduate";
					break;
					case "2":
						$title = "Graduate";
					break;
					case "3":
						$title = "Additional";
					break;
				
				}//end switch
				if($myRecommenders[$i][13] > 0)
				{
					$submitted = "(Letter of Recommendation Requested)";
				} else {

                                  if($myRecommenders[$i][6] != "") // Is there an email address?
                                  {
			              $submitted = "(NOTE: Email Request to this recommender has NOT been sent)";
                                  }
                                }

				if($myRecommenders[$i][9] > 0)
				{
					$submitted = "(Letter of Recommendation Received)";
				}
				
				?><!--BEGIN MAIN TABLE -->
            <!-- REC TABLE -->
<table width="650" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td><span class="subtitle">Recommender <?=($i+1)?> <?=$submitted?></span> </td>
    <td align="right">
<!-- Display DEL Button only after this recommender has been saved -->
<?      // If the recommender in question has been created, that means its been saved
//echo $i . " " . $NUMRECS;
        if ( $allowEdit == false || $i > $NUMRECS-1)
        {
		showEditText("Delete", "button", "btnDelete_".$myRecommenders[$i][0], $_SESSION['allow_edit']); 
        }
?>
	<input name="txtRecord_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtRecord_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][0]?>" />
	<input name="txtUid_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtUid_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][1]?>" />
	<input name="txtUMasterid_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtUMasterid_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][15];?>">

	<input name="txtDatafile_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtDatafile_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][9];?>">
	<input name="txtModdate_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtModdate_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][10];?>">
	<input name="txtSize_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtSize_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][11];?>">
	<input name="txtExtension_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtExtension_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][12];?>">
	<input name="txtRecSent_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtExtension_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][13];?>">

</td>
  </tr>
</table>


<table border=0 cellpadding=1 cellspacing=1 class="tblItem" width="650">
<? debugbreak();   if(count($types) > 1){ ?>
<tr>
<td>Recommendation Type: <? showEditText($myRecommenders[$i][8], "listbox", "lbType_".$myRecommenders[$i][0], $_SESSION['allow_edit'],true, $types); ?></td>
<td></td>
<td></td>
</tr>
<? }else
{
	showEditText("1", "hidden", "lbType_".$myRecommenders[$i][0], $_SESSION['allow_edit'],true, null);
} ?>
<tr>
<td width="216"><strong>First Name:</strong><br>
<? showEditText($myRecommenders[$i][2], "textbox", "txtFname_".$myRecommenders[$i][0], $allowEdit,true,null,true,20); ?></td>
<td width="216"><strong>Last Name:</strong><br>
<? showEditText($myRecommenders[$i][3], "textbox", "txtLname_".$myRecommenders[$i][0], $allowEdit,true,null,true,20); ?></td>
<td width="218"><strong>Job Title:</strong><br>
<? showEditText($myRecommenders[$i][4], "textbox", "txtTitle_".$myRecommenders[$i][0], $allowEdit,true,null,true,20); ?></td>
</tr>
<tr>
<td><strong>Company/Institution:</strong><br>
<? showEditText($myRecommenders[$i][5], "textbox", "txtAffiliation_".$myRecommenders[$i][0], $allowEdit,true,null,true,40); ?></td>
<td><strong>Email:</strong><br>
<? showEditText($myRecommenders[$i][6], "textbox", "txtEmail_".$myRecommenders[$i][0], $allowEdit,true,null,true,30); ?> </td>
<td><strong>Phone:</strong><br>
<? showEditText($myRecommenders[$i][7], "textbox", "txtPhone_".$myRecommenders[$i][0], $allowEdit,true,null,true,20,20); ?></td>
</tr>
<tr>
<td colspan=3>
<? 
// If recommendation already received, do not show any buttons 
if($myRecommenders[$i][9] == 0)
{
if($myRecommenders[$i][2] != "" && $allowEdit == false)
{
	if($myRecommenders[$i][1] != "" && $myRecommenders[$i][13] > 0){ ?>
	<p><b>
	 Send email reminder to  recommender.</b> 
	<? showEditText("Send Reminder", "button", "btnSendReminder_".$myRecommenders[$i][0], $_SESSION['allow_edit_late'],false); 
	}else{
	
	?>
	<p><b>
	 Send email request to recommender.</b> <? showEditText("Send Request", "button", "btnSendmail_".$myRecommenders[$i][0], $_SESSION['allow_edit_late'],false); 
	 
	} 
}
}
?>
</td>
</tr>
</table>
<!--END MAIN TABLE -->
			    <hr size="1" noshade color="#990000">
		    <? 
				
			}//end for 
         }  //  end the buckley test    
			?>
            <span class="tblItem"><span class="subtitle">
            <? if ($buckley == NULL) {
                    showEditText("Save Buckley", "button", "btnSave", $_SESSION['allow_edit']);
                    } else {
                                showEditText("Save Recommenders", "button", "btnSave", $_SESSION['allow_edit']);
                                } ?>
            </span></span><br>
            <br>
            <br>
<!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<div style="float:left;"> &nbsp;&nbsp;<a href="resume.php">< Previous </a>
                &nbsp; | &nbsp;<a href="suppinfo.php">Next ></a></div>
            <br style="clear:left">
            <!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
