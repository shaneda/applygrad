<?
include_once '../inc/config.php'; 
/*
* The session include is unnecessary here and will redirect to offline.php.
*/
//include_once '../inc/session.php'; 
session_start();
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";

if( isset($_SESSION['domainid']) && $_SESSION['domainid'] != -1 )
{
    $domainid = $_SESSION['domainid'];
}
elseif ( !isset($_SESSION['usertypeid']) 
    || ( isset($_SESSION['usertypeid']) && $_SESSION['usertypeid'] != 6  ) )
{
    header("Location: nodomain.php");
    exit; 
}

if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$name = "";
$email = "";
$msg = "";
$allowSubmit = TRUE;
$msgSent = filter_input(INPUT_GET, 'msgSent', FILTER_SANITIZE_STRING);
if ($msgSent == 'true') {
    $msgSent = TRUE;    
} else {
    $msgSent = FALSE;
}

$showEmailFrom = FALSE;
if( isset($_SESSION['userid']) && $_SESSION['userid'] > 0) 
{
    $sql = "SELECT id, firstname, lastname, email FROM users 
        where users.id = " . intval($_SESSION['usermasterid']);
    $result = mysql_query($sql)
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    
    while($row = mysql_fetch_array( $result )) 
    {
        $name = $row['firstname']. " " . $row['lastname'];
        $email = $row['email'];
    }
    if($name == "" || $email == "")
    {
        $err .= "You must complete your biographical profile before you can use this feature<br>";
    }

}
else
{
    $showEmailFrom = TRUE;
}

if ( !isset($_SESSION['msgsSent']) || $_SESSION['msgsSent'] < 2)
{
    $allowSubmit = true;
    if(isset($_POST["btnSubmit"]) && $err == "")
    {
        $name = filter_input(INPUT_POST, 'txtName', FILTER_SANITIZE_STRING);
        $email = filter_input(INPUT_POST, 'txtEmail', FILTER_SANITIZE_EMAIL); 
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $message = filter_input(INPUT_POST, 'txtMsg', FILTER_SANITIZE_STRING);
            
        if ($email) {
        
            $mailFilterIds = getMailSieveString();

            $msgBody = htmlspecialchars($message) . "\r\n<br>\r\n<br>";
            $msgBody .= htmlspecialchars($name) . "\r\n<br>" . htmlspecialchars($email);
            
            switch ($hostname)
            {
                case "APPLY.STAT.CMU.EDU":  
                    $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsstats@cs.cmu.edu>\r\n";
                    break;
                case "APPLYGRAD-INI.CS.CMU.EDU":  
                    $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsini@cs.cmu.edu>\r\n";
                    break;
                case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                    $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <scsdiet@cs.cmu.edu>\r\n";
                    break;
                default:
                    $headers = "From: Carnegie Mellon Admissions " . $mailFilterIds . " <applygrad@cs.cmu.edu>\r\n";
             }

            $headers .= "Cc:  ".$name."<".$email.">\r\n";
            $headers .= "Reply-to: ".$name."<".$email.">\r\n";
            $headers .= "Content-type: text/html; charset=us-ascii";

            $msgSubject = $mailFilterIds . ' Online Admissions Inquiry';
            
            switch ($hostname)
            {
                case "APPLY.STAT.CMU.EDU":  
                    $envelopeFrom = '-f scsstats@cs.cmu.edu';
                    break;
                case "APPLYGRAD-INI.CS.CMU.EDU":  
                    $envelopeFrom = '-f scsini@cs.cmu.edu';
                    break;
                case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                    $envelopeFrom = '-f scsdiet@cs.cmu.edu';
                    break;
                default:
                    $envelopeFrom = '-f applygrad@cs.cmu.edu';
             }
             mail($supportEmail, $msgSubject, $msgBody, $headers, $envelopeFrom); 
                /*
                if ($hostname == "APPLY.STAT.CMU.EDU")  {
                    mail("dales+scsstats@cs.cmu.edu", "Online Admissions Inquiry", $msgBody, $headers, $envelopeFrom);
                } else {
                    mail("pbergen+applyweb@cs.cmu.edu", $msgSubject, $msgBody, $headers, $envelopeFrom);
                }
                */
            }
            else
            {
                 mail($supportEmail, $msgSubject, $msgBody, $headers, $envelopeFrom); 
            }
            
            if ( isset($_SESSION['msgsSent']) ) {
                $_SESSION['msgsSent'] = $_SESSION['msgsSent']+1;    
            } else {
                $_SESSION['msgsSent'] = 1;    
            } 
            
            $msgSent = true;
            
            header('Location: contact.php?msgSent=true');
            exit;
        }
        else 
        {
            $err = "Invalid e-mail address<br/>";
        }
    }
else
{
    $err .= "You can only send 2 messages per session.<br>";
}

$pageTitle = 'Report a Technical Problem';
include '../inc/tpl.pageHeaderApply.php'; 
?>

    <span class="errorSubtitle"><?=$err?></span>
	
	<? if($msgSent == true){ ?>
	<span class="tblItem">
	Thank you for your inquiry. While we will make every attempt to answer every inquiry personally, 
    please be patient as we receive a large amount of email during the admissions process.
    </span>
	<? 
    } else { 
    ?>
    <span class="tblItem"> 
    This page is to report technical problems only. If you have questions about the application process, 
    please refer to the FAQ page for more information.
    </span>

    <table width="600" border="0" cellspacing="2" cellpadding="4">
      <tr class="tblItem">
        <td width="200" align="right">
        <?php
        if ($showEmailFrom) {
            echo 'Name:';    
        }
        ?>
        </td>
        <td>
        <?php
        if ($showEmailFrom) {
            showEditText("", "textbox", "txtName", $allowSubmit, true,null,true,20);    
        } else {
            showEditText($name, "hidden", "txtName", false, true,null,true,20);    
        }
        ?>        
        <?  ?>
        </td>
      </tr>            
      <tr class="tblItem">
        <td width="200" align="right">
        <?php
        if ($showEmailFrom) {
            echo 'Email address:';    
        }
        ?>
        </td>
        <td>
        <?php
        if ($showEmailFrom) {
            showEditText("", "textbox", "txtEmail", $allowSubmit, true,null,true,20);    
        } else {
            showEditText($email, "hidden", "txtEmail", false, true,null,true,20);    
        }
        ?>
        </td>
      </tr>
      <tr class="tblItem">
        <td width="200" align="right">Message:</td>
        <td><? showEditText("", "textarea", "txtMsg", $allowSubmit, true); ?>
        </td>
      </tr>
      <tr class="tblItem">
        <td width="200" align="right">&nbsp;</td>
        <td><? showEditText("Submit", "button", "btnSubmit", $allowSubmit, true); ?></td>
      </tr>
    </table>
	<? 
    }
            
// Include the shared page footer elements.
include '../inc/tpl.pageFooterApply.php';
?>