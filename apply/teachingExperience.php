<?
$myTeachingExperiences = array();
$err = "";
$itemErrors = array();
$comp = false;

function addTeachingExperience($allowDup)
{
    $doAdd = true;
    global $err;

    $sql = "select * from teaching_experience where application_id = " . intval($_SESSION['appid']);
    $result = mysql_query($sql) 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    $numTeachingExperiences = mysql_affected_rows();
    if ($numTeachingExperiences > 4) {
        $doAdd = false;
    }
    if($doAdd == true)
    {
        $sql = "insert into teaching_experience (application_id)
                values(" . intval($_SESSION['appid']) . ")";
        mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
    else
    {
        $err .= "You cannot add more than 5 teaching experience entries.";
    }
}

function saveTeachingExperiences($vars) 
{
    global $myTeachingExperiences;
    global $err;
    global $itemErrors;
    
    $itemId = -1;
    $tmpid = -1;
    $arrItemId = array();
    
    //HERE WE GET THE RECORD IDS FOR THE UPDATED teaching_experiences
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if($arr[0] == "txtTeachingId")
        {
            $tmpid = $arr[1];
            if($itemId != $tmpid)
            {
                $itemId = $tmpid;
                array_push($arrItemId, $itemId);
            }
        }
    }//END FOR
    
     //ITERATE THROUGH IDS AND DO UPDATES
    $localErr = "";
    for($i = 0; $i < count($arrItemId); $i++)
    {
        $itemError = '';
        $teachingExperienceId = $arrItemId[$i];
        $doUpdate = true;
        $institution = trim(htmlspecialchars($_POST["lbTeachingInstitution_".$arrItemId[$i]]));
        $address = trim(htmlspecialchars($_POST["lbTeachingAddress_".$arrItemId[$i]]));
        $coursesTaught = trim(htmlspecialchars($_POST["lbTeachingCoursesTaught_".$arrItemId[$i]]));
        $start_date = $_POST["txtTeachingStartDate_".$arrItemId[$i]];
        $end_date = $_POST["txtTeachingEndDate_".$arrItemId[$i]];

        //POPULATE ARRAY FOR POSTBACK
        $arr = array();
        array_push($arr, $teachingExperienceId);
        if(isset($_POST["lbTeachingInstitution_".$arrItemId[$i]])){array_push($arr, $_POST["lbTeachingInstitution_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["txtTeachingStartDate_".$arrItemId[$i]])){array_push($arr, $_POST["txtTeachingStartDate_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["txtTeachingEndDate_".$arrItemId[$i]])){array_push($arr, $_POST["txtTeachingEndDate_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["lbTeachingAddress_".$arrItemId[$i]])){array_push($arr, $_POST["lbTeachingAddress_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["lbTeachingCoursesTaught_".$arrItemId[$i]])){array_push($arr, $_POST["lbTeachingCoursesTaught_".$arrItemId[$i]]);}else{array_push($arr,"");}
        
        array_push($myTeachingExperiences, $arr);
        
        //DO VERIFICATION
        if(checkTeachingExperienceDate($start_date) == false)
        {
            $itemError .= "Start date is invalid<br>";
        }
        if(strtolower(trim($end_date)) != 'present'
            && checkTeachingExperienceDate($end_date) == false)
        {
            $itemError .= "End Date is invalid<br>";
        }
        
        if ($institution == '')
        {
            $itemError .= "You must enter an institution.<br>";
        }

        if ($address == '')
        {
            $itemError .= "You must enter an address.<br>";
        }
        
        if ($coursesTaught == '')
        {
            $itemError .= "You must enter courses taught.<br>";
        }

        if($itemError != "")
        {
            $itemErrors[$teachingExperienceId] = $itemError;
        } 
        elseif($doUpdate == true)
        {
            if (strtolower(trim($end_date)) == 'present') {
                $endDateString = 'present';
            } else {
                $endDateString = formatMySQLdate2($end_date,'/','-');    
            }
            
            $sql = "update teaching_experience set
            institution = '" . mysql_real_escape_string($institution) . " ', ";
            $sql .= "start_date= '" . formatMySQLdate2($start_date,'/','-') . "',
            end_date = '" . mysql_real_escape_string($endDateString) . "',
            address = '" . mysql_real_escape_string($address) . "',
            courses_taught = '" . mysql_real_escape_string($coursesTaught) . "'
            where id = " . $arrItemId[$i];
            mysql_query($sql) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        }
    }
}

function checkTeachingExperienceDate($date)
{
    $ret = false;
    $dateArray = explode('/', $date);
    if(count($dateArray) == 2)
    {
        $matched = preg_match("#^(0[1-9]|1[012])/(19|20)([0-9]{2})$#", $date);
        if($date != "" &&  $matched != false)
        {
            $ret = true;
        }
    }
    return $ret;
}
    

//INSERT RECORD TABLE FOR USER 
if(isset($_POST['btnAddTeachingExperience']))
{
    $allowdups = true;
    saveTeachingExperiences($_POST);
    addTeachingExperience($allowdups);
}//END POST BTNADD

function deleteTeachingExperience ($vars) {

    $itemId = -1;
    
    foreach($vars as $key => $value)
    {
        if( strstr($key, 'btnTeachingDel') !== false ) 
        {
            $arr = split("_", $key);
            $itemId = $arr[1];
            
            $datafileid = -1;
            $type = -1;
            if($itemId != "")
            {   
                $sql = "select * from teaching_experience 
                    where application_id = " . intval($_SESSION['appid']) . " 
                    and id = " . intval($itemId);
                $result = mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
                
                $sql = "DELETE FROM teaching_experience WHERE id = " . intval($itemId);
                mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            }
        }
    }//END FOR
}

// Delete
$teachingExperienceDataSavedAfterDeletions = FALSE;
if( isset($_POST) && !isset($_POST['btnAddTeachingExperience']) )
{
    saveTeachingExperiences($_POST);
    deleteTeachingExperience($_POST);
    $teachingExperienceDataSavedAfterDeletions = TRUE;
}//END IF

//UPDATE
if( isset($_POST['btnSubmit']) && !$teachingExperienceDataSavedAfterDeletions) {
    saveTeachingExperiences($_POST);
}

if(!isset($_POST['btnSubmit']))
{
  $myTeachingExperiences = getTeachingExperiences($_SESSION['appid']);  
    
} 
//end if not btnSave
if (isset($_POST['btnAddTeachingExperience'])) {
  $myTeachingExperiences = getTeachingExperiences($_SESSION['appid']); 
}
?>

<span class="subtitle">
Teaching Experience
</span>
<br>
<br>
<? 
if(count($myTeachingExperiences) < 5){ 
showEditText("Add Teaching Experience", "button", "btnAddTeachingExperience", $_SESSION['allow_edit']);
} ?>
<br>            
<?
for($i = 0; $i < count($myTeachingExperiences); $i++)
{
    $title = "";
    $myTeachingExperiencetId = $myTeachingExperiences[$i][0];
?>
    <hr size="1"> 
    <table width="650" border="0" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <span class="subtitle"><?=$title;?> Experience</span><br>
          <span class="errorSubtitle itemError">
          <?php 
          if (isset($itemErrors[$myTeachingExperiencetId])) {
            echo $itemErrors[$myTeachingExperiencetId];    
          }
          ?>
          </span>
        </td>
               <td align="right">
                <? showEditText("Delete", "button", "btnTeachingDel_".$myTeachingExperiences[$i][0], $_SESSION['allow_edit']); ?>
                <input name="txtTeachingId_<?=$myTeachingExperiences[$i][0];?>" type="hidden" id="txtTeachingId_<?=$myTeachingExperiences[$i][0];?>" value="<?=$myTeachingExperiences[$i][0];?>">
                </td>
              </tr>
                </table>
            <table width="650" border="0" cellpadding="3" cellspacing="0" class="tblItem">
                  <tr>
                    <td  align="right"><strong>Institution:</strong></td>
                    <td colspan="5">
                    <? showEditText(trim($myTeachingExperiences[$i][1]), "textbox", 
                            "lbTeachingInstitution_".$myTeachingExperiences[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
                    ?>
                    </td>
                  </tr>
                  
                  <tr>
                    <td  align="right"><strong>Address:</strong></td>
                    <td colspan="5">
                    <? 
                    showEditText(trim($myTeachingExperiences[$i][4]), "textbox", 
                            "lbTeachingAddress_".$myTeachingExperiences[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
                    ?>
                    </td>
                  </tr>
                  
                  <tr>
                    <td  align="right"><strong>Courses Taught:</strong></td>
                    <td colspan="5">
                    <? 
                    showEditText(trim($myTeachingExperiences[$i][5]), "textbox", 
                            "lbTeachingCoursesTaught_".$myTeachingExperiences[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
                    ?>
                    </td>
                  </tr>
                  
                  <tr>
                    <td align="right"><strong>Start Date:</strong></td>
                    <td><? showEditText($myTeachingExperiences[$i][2], "textbox", "txtTeachingStartDate_".$myTeachingExperiences[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>              
                    <td  align="right"><strong>End Date 
                    <?  ?></strong><strong>:</strong></td>
                    <td><? showEditText($myTeachingExperiences[$i][3], "textbox", "txtTeachingEndDate_".$myTeachingExperiences[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY or 'present')</td>
                    
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
            </table>   
    <?
    }//end for loop 
?>