<?php

include_once '../inc/config.php';
include_once '../inc/session.php';
include_once '../inc/db_connect.php';

$testId = getTest();

if (!$testId) {
    
    addTest();
    
}

function getTest() {

    $query = "SELECT * FROM mhci_prereqsProgrammingTests
                WHERE mhci_prereqsProgrammingTests.student_lu_users_usertypes_id = " 
                . $_SESSION['userid'];
    //echo $query;
    $result = mysql_query($query);   
    
    $testId = NULL;
    while ($row = mysql_fetch_array($result)) {
        
        $testId = $row['id'];
        
    }
    
    return $testId;
    
}


function addTest() {

    $query = "INSERT INTO mhci_prereqsProgrammingTests
                (student_lu_users_usertypes_id, download_timestamp)
                VALUES ({$_SESSION['userid']}, NOW())";

    $result = mysql_query($query);
    
    return mysql_insert_id();        
       
}

?>

<html>
<head>
<title>MHCI Prerequisites: Programming Test</title>
</head>
<body>
<pre>

<?php
include "../inc/mhci_prereqsProgrammingTest.txt";
?>

</pre>
</body>
</html>