
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$err= "";
$formNum = 1;
$id = -1 ;
$responses = array();

if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
}
if($id  < 1)
{
	$err .= "Invalid id number<br>";
}

if(isset($_POST['btnSubmit']))
{
	//$sql = "delete from recommendforms where form_id=1 and recommend_id=".$id;
	//$result = mysql_query($sql) or die(mysql_error());
	$vars = $_POST;
	$itemId = -1;
	$response = "";
	$responses = array();
	
	$tmpItem = -1;
	$i = 0;
	
	$sql = "delete from recommendforms where recommend_id=".$id." and form_id=1";
	mysql_query($sql) or die(mysql_error());
			
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'ques_') !== false ) 
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			
			if(count($arr) == 3)//QUESTION IS A MATRIX
			{
				$i = $arr[2];
				$response .= htmlspecialchars($value);
				
			}
			else
			{
				$response = htmlspecialchars($value);
			}
			if($itemId != $tmpItem || count($arr) == 3)
			{
				$arr1 = array();
				array_push($arr1, $itemId);
				array_push($arr1, $i);
				array_push($arr1, $response);
				array_push($responses, $arr1);
				$response = "";
				$i = 0;
			}
			
			
			$tmpItem = $itemId;
		}
	}//END FOR
	for($i = 0; $i < count($responses); $i++)
	{
		$sql = "INSERT INTO recommendforms(recommend_id,form_id,question_id, question_key, response) values(".$id.",1, ".$responses[$i][0].",".$responses[$i][1].", '".addslashes($responses[$i][2])."')";
		mysql_query($sql) or die(mysql_error());
		//echo $responses[$i][1]." ".$responses[$i][2]."<br>";
	}
	
	
}//END IF SUBMIT

//GET DATA
$sql = "select question_id, question_key, response from recommendforms where recommend_id =".$id." and form_id=1 order by question_id, question_key";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr,$row['question_id']);
	array_push($arr,$row['question_key']);
	array_push($arr,stripslashes($row['response']));
	array_push($responses,$arr);
	
}

function isSelected($qNum, $qKey, $fieldVal, $idx)
{
	global $responses;
	$ret = "";
	
	if(count($responses) > $idx)
	{
		if($fieldVal == $responses[$idx][2])
		{
			$ret = "checked";
		}
	}else
	{
		//DEFAULT TO LOWEST VALUE
		if($fieldVal == 7)
		{
			$ret = "checked";
		}
	}
	
	return $ret;
}

function getValue($qNum, $qKey, $fieldVal, $idx)
{
	global $responses;
	$ret = "";
	if(count($responses) > $idx)
	{
		$ret= $responses[$idx][2];
	}
	
	return $ret;
}
?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Academic Recommendation Form</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" --><span class="tblItem">
            <input name="txtId" type="hidden" id="txtId" value="<?=$id?>">
            <input class="tblItem" name="btnBack" type="button" id="btnBack" value="Return to Recommendations" onClick="document.location='recommender.php'">
            <br>
            <br>
            Please rank the applicant as well as you can along the listed dimensions. Feel free to check &ldquo;Insufficient Information&rdquo; if you cannot make a judgement. 
            <table  border="0" cellpadding="2" cellspacing="2" class="tblItem">
              <tr>
                <td>&nbsp;</td>
                <td><strong>Top 1% </strong></td>
                <td><strong>Top 5% </strong></td>
                <td><strong>Top 10% </strong></td>
                <td><strong>Top 20% </strong></td>
                <td><strong>Top 50% </strong></td>
                <td><strong>Below 50%</strong></td>
                <td><strong>Insufficient Information </strong></td>
              </tr>
              <tr>
                <td>Intellectual ability</td>
				<? 
				$j = 0;//row index
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Breadth of general knowledge</td>
                <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr>
                <td>Depth of knowledge in the undergraduate area</td>
                <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Motivation &amp; initiative</td>
                <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr>
                <td>Maturity and stability</td>
                 <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_4" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Ability to work independently</td>
                 <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_5" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr>
                <td>Ability to speak and write English</td>
                 <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_6" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Creativity</td>
                 <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_7" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr>
                <td>Clarity of goals for graduate study</td>
                 <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_8" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Overall potential for graduate study</td>
                 <? 
				for($i = 0; $i < 7; $i++){ ?>
                <td><input name="ques_1_9" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
            </table>
            <input name="ques1count" type="hidden" id="ques1count" value="10">
            In making this evaluation, what group are you using as a comparison? <br>
            <? showEditText(getValue(2,0,0,$j), "textarea", "ques_2", $_SESSION['allow_edit']); 
			$j++;
			?>
            <br>
              <br>
            How long have you known this applicant, and in what capacity?<br>
            <? showEditText(getValue(3,0,0,$j), "textarea", "ques_3", $_SESSION['allow_edit']); 
			$j++;
			?>
            <br>
            <br>
            Please describe the particular strengths and weaknesses of this applicant. Also, describe any special talents or experience. If you can find nothing to say, please, give the applicant&rsquo;s strongest trait and weakest trait.<br>
            <? showEditText(getValue(4,0,0,$j), "textarea", "ques_4", $_SESSION['allow_edit']); 
			$j++;
			?>
            <br>
            <br>
            If you have worked with or supervised this applicant on a project, please describe the project and give an evaluation of the applicant&rsquo;s performance.<br>
            <? showEditText(getValue(5,0,0,$j), "textarea", "ques_5", $_SESSION['allow_edit']); 
			$j++;
			?>
            <br>
            <br>
            Additional Information:<br>
            <? showEditText(getValue(6,0,0,$j), "textarea", "ques_6", $_SESSION['allow_edit']); 
			$j++;
			?>
            <br>
            <br>
            <span class="subtitle">
            <? showEditText("Save Info", "button", "btnSubmit", $_SESSION['allow_edit']); ?>
            </span>            </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
