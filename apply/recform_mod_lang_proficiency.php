<?php
/*
* Use minimal session handling logic instead of session.php.
*/
session_start();
if ( !( isset($_SESSION['usermasterid']) && $_SESSION['usermasterid'] > 1 )
    && !( isset($_SESSION['userid']) && $_SESSION['userid'] > 1) ) 
{   
    unset($_SESSION['userid']);
    unset($_SESSION['usermasterid']);
    header('Location: langProfRec.php');
    exit;
}
$_SESSION['SECTION']= "1"; 

include_once '../inc/config.php'; 
//include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php';

$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}
if (!isset ($_SESSION['usertypeid'])) {
    $_SESSION['usertypeid'] = 6;
}
if (!isset ($_SESSION['datafileroot'])) {
    $_SESSION['datafileroot']= $datafileroot;
}

$err= "";
$formNum = 6;
$id = -1 ;
$responses = array();

$sender = NULL;
if(isset($_GET['sender']))
{
    $sender = $_GET['sender'];
    $studentIDArray = explode('_', $_GET['sender']);
    if (isset($studentIDArray[1])) {
        $id = intval($studentIDArray[1]);    
    }
}
if($id  < 1)
{
	$err .= "Applicant cannot be identified.<br>"; 
}

function getLanguageOfInterest ($recommenderId) {
    $language = "";
    $query = "SELECT language_specialization as lang FROM lang_prof_recommender_info
                WHERE recommend_id = " . $recommenderId;
    $result = mysql_query($query) 
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $query);
    while($row = mysql_fetch_array( $result ))
    {
        $language = $row['lang'];        
         }
    return $language;
}

$languageOfInterest = getLanguageOfInterest($id);

if(isset($_POST['btnSubmit']))
{
    $vars = $_POST;
	$itemId = -1;
	$response = "";
	$responses = array();
	
	$tmpItem = -1;
	$i = 0;
	
	$sql = "delete from recommendforms where recommend_id=". intval($id) ." and form_id=". intval($formNum);
	mysql_query($sql) 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'ques_') !== false ) 
		{
			$arr = split("_", $key);
			$itemId = intval($arr[1]);
			
			if(count($arr) == 3)//QUESTION IS A MATRIX
			{
				$i = $arr[2];
			}

            $response = htmlspecialchars($value);
            
			if($itemId != $tmpItem || count($arr) == 3)
			{
				$arr1 = array();
				array_push($arr1, $itemId);
				array_push($arr1, $i);
				array_push($arr1, $response);
				array_push($responses, $arr1);
				$response = "";
				$i = 0;
			}

			$tmpItem = $itemId;
		}
	}//END FOR
	for($i = 0; $i < count($responses); $i++)
	{
		$sql = "INSERT INTO recommendforms(recommend_id, form_id, question_id, question_key, response) 
            values(" . intval($id) ."," . intval($formNum) . "," . intval($responses[$i][0]) . ","
            . intval($responses[$i][1]) . ",'" . mysql_real_escape_string($responses[$i][2]) . "')";
		mysql_query($sql) 
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	}

    $sender = filter_input(INPUT_GET, 'sender', FILTER_SANITIZE_STRING);
    // Where do I go
    echo "<meta http-equiv='refresh' content='0;url=../apply/langProfRecommenderUpload.php?id=$sender'>\n";	
	
}//END IF SUBMIT

//GET DATA
$sql = "select question_id, question_key, response from recommendforms 
    where recommend_id =". intval($id) . " and form_id=". intval($formNum) ." order by question_id, question_key";
$result = mysql_query($sql)	
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr,$row['question_id']);
	array_push($arr,$row['question_key']);
	array_push($arr,stripslashes($row['response']));
	array_push($responses,$arr);	
}

function isSelected($qNum, $qKey, $fieldVal, $idx)
{
	global $responses;
	$ret = "";
	if(count($responses) > $idx)
	{
		if($fieldVal == $responses[$idx][2])
		{
			$ret = "checked";
		}
	}

	return $ret;
}

function getValue($qNum, $qKey, $fieldVal, $idx)
{
	global $responses;
	$ret = "";
	if(count($responses) > $idx)
	{
		$ret= $responses[$idx][2];
	}
	
	return $ret;
}

?>
<html>
<head>
<title><?=$HEADER_PREFIX[$domainid]?></title>
<link href="../css/SCSStyles_ModLang-PhD.css" rel="stylesheet" type="text/css">
</head>
<body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
<div id="banner"></div>
<div class="tblItem" id="contentDiv">

            <span class="tblItem">
            <a href="langProficiencyRecommender.php"></a><em>
            <input name="btnRecs" type="button" id="btnBack" value="Return to Recommender Home" onClick="document.location='langProficiencyRecommender.php'">
            </em><br><br>
            
            <?php
$sql = "select content from content where name='Language Proficiency Recommender Survey' and (domain_id=" . $domainid . ")";
                $result = mysql_query($sql)    
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
                while($row = mysql_fetch_array( $result )) 
                {
                    echo '<div style="margin:20px;width:660px">';
                    echo html_entity_decode($row["content"]);
                    echo "</div>";
                }
?>
<form action="" method="post" name="form1" id="form1">
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            
          <tr>
            <td valign="top">			
			
			<div style="margin:20px;width:660px">
            <div class="tblItem" id="contentDiv">
            <span class="tblItem">
            
            <br>
            <br> 
<?php
if ($err) {
    echo '<span class="errorSubtitle">' . $err . '</span>';    
} else {  
?>           
<br>        In regards to the following language: <b><?php echo $languageOfInterest; ?> </b><br /> Please select the statement that best describes the candidate's degree of competence in each category:
            <table  border="0" cellpadding="2" cellspacing="2" class="tblItem">
              <tr class="tblItemAlt">
              <tr>
                <td><br /><b>Comprehension</b></td>
              </tr>
                <? 
				$j = 0;//row index
			//	for($i = 0; $i < 6; $i++){ ?>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,1,$j) ?> value="<?=1?>">No usable proficiency</td>
              </tr>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,2,$j) ?> value="<?=2?>">Adequate comprehesion for normal daily needs</td>
              </tr>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,3,$j) ?> value="<?=3?>">Able to comprehend answers in response to questions relating to field of specialization</td>
              </tr>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,4,$j) ?> value="<?=4?>">Able to understand lectures in field of specialization</td>
              </tr>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,5,$j) ?> value="<?=5?>">Able to understand group discussions on nontechical subjects</td>
              </tr>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,6,$j) ?> value="<?=6?>">Able to undertand foreign language news broadcasts</td>
              </tr>
              <tr>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,7,$j) ?> value="<?=7?>">Comprehehension at level of native speaker</td>
              </tr>
				<? 
			//	}//end for 
				$j++;
				?>
                
              <tr>
                <td><br /><b>Speaking/Lecturing</b></td>
              </tr>
              
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,1,$j) ?> value="<?=1?>">No usable proficiency</td>
              </tr>
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,2,$j) ?> value="<?=2?>">Able to speak adequately for daily needs</td>
              </tr>
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,3,$j) ?> value="<?=3?>">Able to conduct interview in field of specialization</td>
              </tr>
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,4,$j) ?> value="<?=4?>">Able to deliver lectures form notes or prepared texts but may need <br />assistance of interpreter to engage group discussion that may follow</td>
              </tr>
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,5,$j) ?> value="<?=5?>">Able to deliver lectures form notes or prepared texts in field of specialization and engage in following without assistance</td>
              </tr>
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,6,$j) ?> value="<?=6?>">Able to speak extemporaneously on nontechnical subjects in general and in the area of <br />specialization: able to discuss field of specialization with foreign colleagues</td>
              </tr>
              <tr>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,1,7,$j) ?> value="<?=7?>">Speaking ability equivalent to that of educated speaker</td>
              </tr>
                <?  
                $j++;
                ?>
              
              <tr>
                <td><b>Reading</b></td>
              </tr>
               
              <tr>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,2,1,$j) ?> value="<?=1?>">No usable proficiency</td>
              </tr>
              <tr>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,2,2,$j) ?> value="<?=2?>">Able to read typed or printed mateiral of a nonspecialized nature, such as simple signs and messages</td>
              </tr>
              <tr>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,2,3,$j) ?> value="<?=3?>">Able to read elementary material in own and related fields, though at a slow rate of speed</td>
              </tr>
              <tr>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,2,4,$j) ?> value="<?=4?>">Able to read general material in own and related fields, though with some reliance on a dictionary</td>
              </tr>
              <tr>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,2,5,$j) ?> value="<?=5?>">Reading ability of educated native speaker</td>
              </tr>
                 <?  
				$j++;
				?>
              
              <tr>
                <td><br /><b>Writing</b></td>
              </tr>
              <tr>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,3,1,$j) ?> value="<?=1?>">No practical facility</td>
              </tr>
              <tr>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,3,2,$j) ?> value="<?=2?>">Able to write simple messages, nonspecialized letters</td>
              </tr>
              <tr>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,3,3,$j) ?> value="<?=3?>">Able to draft academic materials in field of specialization with major editing by native speaker</td>
              </tr>
              <tr>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,3,4,$j) ?> value="<?=4?>">Able to prepare written material in field of specialization with minimal editing by native speaker</td>
              </tr>
              <tr>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,3,5,$j) ?> value="<?=5?>">Writing ability of educated native speaker</td>
              </tr>
            </table>
            <br>
            <span class="subtitle">
            <?php 
            if ($sender) {
                echo '<input name="sender" type="hidden" id="sender" value="' . $sender . '">'; 
            }
            showEditText("Save Info", "button", "btnSubmit", TRUE); 
            ?>
            </span>            
            
            <?php
            }
            ?>  
            </span>
            </div>
			</div>
			</td>
          </tr>
        </table>
        </form>
   </body>
</html>