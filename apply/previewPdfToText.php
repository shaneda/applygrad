<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
include '../classes/class.FileConverter.php';
include '../classes/class.FileDownload.php';

// Determine the name of the datafile being requested.
$file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

// Determine the guid of the datafile being requested.
$guid = filter_input(INPUT_GET, 'guid', FILTER_SANITIZE_STRING);

// Determine the requester's luuId. 
$requesterLuuId = NULL;
if ( isset($_SESSION['userid']) &&  $_SESSION['userid'] > 0) {
    $requesterLuuId = $_SESSION['userid'];
}

// Determine the the requester's current applicationId.
$requesterApplicationId = NULL;
if ( isset($_SESSION['appid']) &&  $_SESSION['appid'] > 0) {
    $requesterApplicationId = $_SESSION['appid'];
}

// Exit if datafileId, luuId, applicationId, and usertype cannot be determined.
if ( !($file && $guid && $requesterLuuId && $requesterApplicationId) ) {
    exit;
}

// Instantiate a new file download with the requested datafile id.
try {
    $download = new FileDownload(urldecode($file), $guid);    
} catch (Exception $e) {
    if ($e->getCode() == 404) {
        header("HTTP/1.0 404 Not Found");
        echo "File not found.";
        exit;
    }    
}

// Don't permit applicant to see recommendations, merge files.
$fileLuuId = $download->fileLuuId;
$fileApplicationId = $download->fileApplicationId;
$fileType = $download->fileType;

$doPreview = FALSE;
if ($fileLuuId == $requesterLuuId 
    && $fileType != 'recommendation' && $fileType != 'merged') {                          
    
    // Applicant requesting own file, luuId match.
    $doPreview = TRUE;
        
} elseif ($fileApplicationId == $requesterApplicationId 
            && $fileType != 'recommendation' && $fileType != 'merged') {

    // Applicant requesting own file, applicationId match.
    // (User ID may not match if an admin uploaded the file.)
    $doPreview = TRUE;

}

// Get the path of the file to preview.
if ($doPreview) {
    $filePath = realpath($download->filePath);
    if (!$filePath) {
        exit;
    }
} else {
    exit;
}

// Do conversion only if there is no text file 
// or the pdf is newer than the text file.
$textFile = str_replace(".pdf", ".txt", $filePath);

if (file_exists($textFile) && (filemtime($textFile) >= filemtime($filePath)) ) {    
    $usePreviousTextFile = TRUE;
    $newConversion = FALSE;
} else {
    $usePreviousTextFile = FALSE;
    $newConversion = TRUE;
    $conversionStatus = FileConverter::pdf2text($filePath);    
}

if ($usePreviousTextFile || ($newConversion && !$conversionStatus['error'])) { 
    echo shell_exec('strings ' . $textFile);
}

?>