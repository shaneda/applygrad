
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$reqs = array();
$compReqs = array();
$apps = array();
$progs = array();
$appName = "";
$firstname = "";
$appid = -1;

//GET USER INFO
$sql = "SELECT id, name, paid, submitted, submitted_date from application where user_id = ". $_SESSION['userid'];
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['paid']);
	array_push($arr, $row['submitted']);
	array_push($arr, formatUSdate($row['submitted_date']));
	array_push($apps, $arr);
}

//GET REQUIREMENTS
$sql = "SELECT applicationreqs.id, 
applicationreqs.name, 
applicationreqs.linkname, 
application.name as appname, 
lu_application_appreqs.id as compreqid, 
lu_application_appreqs.last_modified 
FROM `lu_application_programs`
inner join programs on programs.id = lu_application_programs.`program_id`
inner join degree on degree.id = programs.degree_id
inner join lu_degrees_applicationreqs on lu_degrees_applicationreqs.degree_id = programs.degree_id
inner join applicationreqs on applicationreqs.id = lu_degrees_applicationreqs.appreq_id
inner join application on application.id = lu_application_programs.application_id
left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
	and lu_application_appreqs.req_id = applicationreqs.id
where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['linkname']);	
	array_push($arr, $row['compreqid']);	
	array_push($arr, formatUSdate($row['last_modified']));	
	array_push($compReqs, $arr);
}

function getProgs($appid)
{
	$ret = array();
	$sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname  from lu_application_programs
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	where lu_application_programs.application_id = ". $appid;
	$result = mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['fieldname']);
		array_push($ret, $arr);
	}
	return $ret;
}

?>



<script language="JavaScript" type="text/JavaScript">
<!--
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to delete? Note that you will lose all of your data if you do this.");
if (agree)
	return true ;
else
	return false ;
}
//-->
</script>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Applications</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" --><span class="tblItem">
			 <fieldset><legend class="subtitle">Tools</legend>
			 <a href="applicationEdit.php">Add a new application</a>			 </fieldset><br>
			 <? 
				$paid = "not paid";
				$submitted = "Not Submitted";
				$lastMod = "Never";
				$tmpLastMod = "";
				for($i = 0; $i < count($compReqs); $i++)
				{
					if(strtotime($compReqs[$i][4]) > strtotime($tmpLastMod))
					{
						$tmpLastMod = $compReqs[$i][4];
						$lastMod = $tmpLastMod;
					}
				}
				
				for($i = 0; $i < count($apps); $i++)
				{ 
					if( $apps[$i][4] == 1)
					{
						$submitted = "Submitted on ". $apps[$i][5];
					}
					echo "</span><span class='subtitle'>".$apps[$i][1]."<em> (".$submitted.")</em></span>";
					echo "<span class='tblItem'><em><br>Last modified:(".$lastMod.")<br></em>";
					if($apps[$i][2] == 1)
					{
						$paid = "paid";
					}
					echo "<strong>Total Cost:</strong> $".totalCost($apps[$i][0])." (".$paid.")<br><br>";
					echo "<strong>Programs Selected:</strong><br>";
					
					//PROGRAMS
					$progs = getProgs($apps[$i][0]);
					for($j = 0; $j < count($progs); $j++)
					{ 
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$progs[$j][1]." in " .$progs[$j][2] ;
					}
					echo "<br><br>";
				}
			?>
			 
			 
			 <br>
			 </span><span class="tblItem">			 </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
