<?php
include_once '../inc/config.php'; 
/*
* The session include is unnecessary here and will redirect to offline.php.
*/
//include_once '../inc/session.php'; 
session_start();
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

include '../classes/class.Domain.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
$newDomainname = NULL;

//DebugBreak();
$requestDomainId = filter_input(INPUT_GET, 'domain', FILTER_VALIDATE_INT);

if (Domain::isValid($requestDomainId)) {
    $domainid = $requestDomainId;     
    $domainname = Domain::getName($requestDomainId);
    $newDomainname = $domainname;
} else {
    if(isset($_SESSION['domainid'])) {
        $domainid = $_SESSION['domainid'];
    }
    if(isset($_SESSION['domainname'])) {
	    $domainname = $_SESSION['domainname'];
    }
}



// Include the shared page header elements.
$pageTitle = 'Login Conflict';
$displayMenu = FALSE;
include '../inc/tpl.pageHeaderApply.php';
?>

<br/><br/>  <br/><br/>
<p>

<?php
if($newDomainname) {
    if (isset($_SESSION['domainname'])) {
?>
    You are currently logged in to the <?php echo $_SESSION['domainname'] ?> online application.
    Please log out before logging in to the <?php echo $newDomainname; ?> online application. 
<?php 
    } else {
?>
    You are currently logged into a different online application.  
    Please log out before logging in to the <?php echo $newDomainname; ?> online application.
<?php        
    }
} else {
    if (isset($_SESSION['domainname'])) {
?>
    You are currently logged in to the <?php echo $_SESSION['domainname'] ?> online application. 
    Please log out before logging in to a new online application.
<?php 
    } else {
?>
    You are currently logged in to a different online application.  
    Please log out before logging in to a new online application.
<?php        
    }   
}
?>


</p>

<?php
include '../inc/tpl.pageFooterApply.php';
?>