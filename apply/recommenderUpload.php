<?php
/*
* Use minimal session handling logic instead of session.php.
*/
session_start();
if ( !( isset($_SESSION['usermasterid']) && $_SESSION['usermasterid'] > 1 )
    && !( isset($_SESSION['userid']) && $_SESSION['userid'] > 1) ) 
{   
    unset($_SESSION['userid']);
    unset($_SESSION['usermasterid']);
    header('Location: rec.php');
    exit;
}
$_SESSION['SECTION']= "1";

include_once '../inc/config.php'; 
// include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}
if (!isset ($_SESSION['usertypeid'])) {
    $_SESSION['usertypeid'] = 6;
}
if (!isset ($_SESSION['datafileroot'])) {
    $_SESSION['datafileroot']= $datafileroot;
}

$id = -1;
$uid = -1;
$umasterid = -1;
$appid = -1;
$err = "";
$uName = "";
$recText = "";
$modDate = "";
$fileid = -1;
$buckley = 0;
$size = 0;
$extension = "";
$allowEdit = true;
$recType = -1;

if(isset($_POST['uid']))
{
    $uid = $_POST['uid'];
}
if(isset($_POST['id']))
{
    $id = $_POST['id'];
}

//HANDLE VARS COMING IN FROM RECOMMENDER.PHP
$applicationId = NULL;
$sender = NULL;
if(isset($_POST) || isset($_GET))
{
    if (isset($_POST) && !empty($_POST)) {
       $vars = $_POST;
    } else {
        $vars = $_GET;
    }
    foreach($vars as $key => $value)
    {
        if(( strstr($key, 'sender') == true || strstr($key, 'id')) == true ) 
        {
            $sender = $value;
            $arr = split("_", $value);
            if (sizeof($arr) > 1) {
                $id = $arr[1];
                $uid = $arr[2];
                $applicationId = $arr[3];
            }
        }
    }//END FOR
}

if($id > 0 && $uid > 0)
{
    //RETRIEVE USER INFORMATION
    $sql = "SELECT firstname, lastname, recommend.submitted, 
    recommend.content,
    datafileinfo.id as datafileid,
    datafileinfo.moddate,
    users.id as umasterid,application.id as appid,
    datafileinfo.extension,
    datafileinfo.size,
    recommendtype
    FROM recommend
    inner join application on application.id = recommend.application_id
    inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
    inner join users on users.id = lu_users_usertypes.user_id
    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
    where recommend.id=".$id;
    $result = mysql_query($sql) or die(mysql_error());
    //echo $sql;
    while($row = mysql_fetch_array( $result ))
    {
        $umasterid = $row['umasterid'];
        $appid = $row['appid'];
        
        if($row['submitted'] >= 2 )
        {
            $allowEdit = false;
        } else {
                    $allowEdit = true;
        }
        if ((isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] != -1))
        {
            if($_SESSION['A_usertypeid']!=0 && $_SESSION['A_usertypeid']!=1)
            {    
                $allowEdit = false;
            }else
            {
                $allowEdit = true;
            }
        }
        $uName = $row['firstname']. " " . $row['lastname'];
        $recText = $row['content'];
        $modDate = $row['moddate'];
        $fileid = $row['datafileid'];
        $extension = $row['extension'];
        $size = $row['size'];
        $recType = $row['recommendtype'];
        //echo $modDate;
    }

    if(isset($_POST["btnSubmit"]) )
    {   
        $logMessage = 'attempt upload: ' . $appid . " " . $_FILES["file1"]['name'] . " " . $_SERVER['REMOTE_ADDR'];
        logAccess($logMessage);
        if($appid > -1)
        {
            if((isset($_FILES["file1"]['name'])) && ($_FILES["file1"]['name'] != ""))
            {
                $ret = handle_upload_file(3, $uid,$umasterid, $_FILES["file1"],$appid."_".$id );
                logAccess($ret);
                $err = "";
                if(intval($ret) < 1)
                {
                    $err = $ret."<br>";
                }else
                {   $sql = "select submitted from recommend where id = ".$id;
                    $result = mysql_query($sql) or die(mysql_error());
                    while($row = mysql_fetch_array( $result )) {
                        $numSubmissions = $row['submitted'];
                    }
                    $nextSubmission = 1 +  $numSubmissions;
                    
                    $sql = "update recommend set datafile_id =".intval($ret).", submitted=" . $nextSubmission . " where id = ".$id;
                    $result = mysql_query($sql) or die(mysql_error());
                    $_SESSION["recAllow_".$id."_".$uid] = true;
                    //echo "ret ". $ret . " uid " . $uid . " appid ". $appid . " id " . $id ;
                    //echo $_SESSION["recAllow_".$id."_".$uid];
                    // Log successful uploads
                    $filePath = getFilePath(3, $id, $uid, $umasterid, $appid, $ret);
                    $logMessage = 'upload: ' . strrchr($filePath, '?'); 
                    logAccess($logMessage);
                }
        
            }
            /*
            if ((isset($_POST['txtContent'])) && ($_POST['txtContent'] != "")) {
                $content = addslashes(htmlspecialchars($_POST['txtContent']));
                $txtsql = "update recommend set content ='".$content."', submitted=1 where id = ".$id;
                $txtresult = mysql_query($txtsql) or die(mysql_error());
                $err = "";
                $_SESSION["recAllow_".$id."_".$uid] = true;
            }
            */
            if($err == "")
            {
                //header("Location: recommender.php");
                $err = "Letter of Recommendation uploaded and submitted successfully.";
            }
        }else
        {
            $err .= "Invalid Application";
        }
    }
    
    //RETRIEVE USER INFORMATION
    $sql = "SELECT firstname, lastname, recommend.submitted, 
    recommend.content,
    datafileinfo.id as datafileid,
    datafileinfo.moddate,
    users.id as umasterid,application.id as appid,
    datafileinfo.extension,
    datafileinfo.size,
    recommendtype
    FROM recommend
    inner join application on application.id = recommend.application_id
    inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
    inner join users on users.id = lu_users_usertypes.user_id
    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
    where recommend.id=".$id;
    $result = mysql_query($sql) or die(mysql_error());
    //echo $sql;
    while($row = mysql_fetch_array( $result ))
    {
        $umasterid = $row['umasterid'];
        $appid = $row['appid'];
        if($row['submitted'] >= 2)
        {
            $allowEdit = false;
            
        }
        if ((isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] != -1))
        {
            if($_SESSION['A_usertypeid']!=0 && $_SESSION['A_usertypeid']!=1)
            {    
                $allowEdit = false;
            }else
            {
                $allowEdit = true;
            }
        }
        
        $uName = $row['firstname']. " " . $row['lastname'];
        $recText = $row['content'];
        $modDate = $row['moddate'];
        $fileid = $row['datafileid'];
        $extension = $row['extension'];
        $size = $row['size'];
        $recType = $row['recommendtype'];
    }
    
}else
{
    $err .= "Invalid request<br>";
}

function getProgs($appid)
{
    $ret = array();
    //GET USERS SELECTED PROGRAMS
    $sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname, choice, 
    lu_application_programs.id as itemid,
    application.buckleywaive
    FROM lu_application_programs 
    inner join programs on programs.id = lu_application_programs.program_id
    inner join degree on degree.id = programs.degree_id
    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
    inner join application on application.id = lu_application_programs.application_id
    where lu_application_programs.application_id = ".$appid." order by choice";
    $result = mysql_query($sql) or die(mysql_error() . $sql);
    
    while($row = mysql_fetch_array( $result )) 
    {
        $dept = "";
        $depts = getDepartments($row[0]);
        if(count($depts) > 1)
        {
            $dept = " - ";
            for($i = 0; $i < count($depts); $i++)
            {
                $dept .= $depts[$i][1];
                if($i < count($depts)-1)
                {
                    $dept .= "/";
                }
                
            }
        }
        $arr = array();
        array_push($arr, $row[0]);
        array_push($arr, $row['degreename']);
        array_push($arr, $row['fieldname'].$dept);
        array_push($arr, $row['choice']);
        array_push($arr, $row['itemid']);
        array_push($arr, $row['buckleywaive']);    
        array_push($ret, $arr);
    }

    
    return $ret;
}

function getDepartments($itemId)
{
    $ret = array();
    $sql = "SELECT 
    department.id,
    department.name
    FROM lu_programs_departments 
    inner join department on department.id = lu_programs_departments.department_id
    where lu_programs_departments.program_id = ". $itemId;

    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $arr = array();
        array_push($arr, $row["id"]);
        array_push($arr, $row["name"]);
        array_push($ret, $arr);
    }
    return $ret;
}

function getAreas($id, $appid)
{
    $ret = array();
    $sql = "SELECT 
    interest.id,
    interest.name
    FROM 
    lu_application_programs
    inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
    inner join interest on interest.id = lu_application_interest.interest_id
    where lu_application_programs.id=".$id. " and lu_application_programs.application_id=".$appid . " order by         lu_application_interest.choice";

    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $arr = array();
        array_push($arr, $row["id"]);
        array_push($arr, $row["name"]);
        array_push($ret, $arr);
    }
    return $ret;
}
 

?>
<html>
<head>

<title><?=$HEADER_PREFIX[$domainid]?></title>
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">


<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>


</head>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
        <!-- InstanceBeginEditable name="EditRegion5" -->
        <form action="recommenderUpload.php#1" method="post" name="form1" id="form1" enctype="multipart/form-data" ><!-- InstanceEndEditable -->

<?php
if (isset($domainname) && $domainname == 'SCS') {
?>
<div id="banner">
    <img src="../apply/images/CSD/appl.jpg" width="753" height="127" border="0" usemap="#Map" />
    <map name="Map" id="Map">
        <area shape="rect" coords="490,23,742,38" href="http://www.cs.cmu.edu/" />
        <area shape="rect" coords="633,5,742,21" href="http://www.cmu.edu/index.shtml" />
    </map>
</div>
<?php
} else {
?>
    <div id="banner"></div>
<?php
}
?>
        
        <table width="753px" height="400" border="0" cellpadding="0" cellspacing="0" class="content" style="width: 753;">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
              <? 
            /*
            if($_SESSION['usertypeid'] != 6)
            {
                include '../inc/sideNav.php'; 
            }
            */
            ?>
            <!-- InstanceEndEditable -->
            
          <tr>
            <td valign="top">            
            <div style="text-align:right; width:680px">
            <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
            ){ ?>
                <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                <!-- DAS Removed - <? echo $sesEmail;?>   -->
            <? }else
            {
                if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                {
                    //session_unset();
                    //destroySession();
                    //header("Location: index.php");
                    ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                }
            } 
            
            $backLocation = 'recommender.php';
            if ($applicationId) {
                $backLocation .= '?appid=' . $applicationId;
            }
            ?>
            </div>
            <div style="margin:20px;width:660px""><span class="title">Upload Recommendation </span>
            <br>
            <br>
            <div class="tblItem" id="contentDiv">
            <span class="tblItem">
            <a href="recommender.php"></a><em>
            <input name="btnRecs" type="button" id="btnBack" 
                value="Return to Recommender Home" onClick="document.location='<?php echo $backLocation; ?>'">
            </em><br><br>

            <? 
            if (isset($_SESSION["recAllow_".$id."_".$uid] ))
            {
                $allowEdit = true;
            }
            
            if($allowEdit == true){ 
            
                $programs = getProgs($appid);
                
                $domainid = 1;
                if(isset($_SESSION['domainid']))
                {
                    if($_SESSION['domainid'] > -1)
                    {
                        $domainid = $_SESSION['domainid'];
                    }
                }
                $sql = "select content from content where name='Recommendation Upload Page' and domain_id=".$domainid;
                $result = mysql_query($sql)    or die(mysql_error());
                while($row = mysql_fetch_array( $result )) 
                {
                    echo html_entity_decode($row["content"]);
                }
            
            ?>
            <br>
            <?php
       //     debugbreak();
            ?>
            Name of applicant: <strong><?=$uName?></strong><br>
            <br>
            Programs applied to:<br>
            </span>
            <div id="list" class="tblItem">
            <ul>
            <? for($i = 0; $i < count($programs); $i++)
            { 
                $buckley = $programs[$i][5];
                $areas = getAreas($programs[$i][4], $appid);
                ?>
                <li><strong><?=$programs[$i][1]?> in <?=$programs[$i][2]?></strong>
                <ul>
                <? for($j = 0; $j < count($areas); $j++){ ?>
                    <li><?=$areas[$j][1]?></li>
            <? } ?>
            </ul>
            </li>
            <? } ?>
            </ul>
            </div><br>
            <span class="tblItem">Under the <strong>Buckley Amendment</strong> this applicant 
                                              has <strong><? if($buckley!=1){echo "NOT ";} ?>waived</strong> 
                                              the right to see this recommendation for admission. <br>
            <br>
            
            You are about to upload a recommendation for the  applicant <strong><?=$uName?></strong> 
                        to Carnegie Mellon University. 
                        <strong>Once your recommendation has been submitted, you will no longer be able to update it.                        </strong>
            <br>
            <br>
            Acceptable file formats are PDF or MS Word format. 
                        Any other format will not be accepted. The maximum file size is 7MB.<br>
            <? }else{ ?>
            You have submitted the following information for the applicant <strong><?=$uName?></strong>. You may not change this data once it is saved.<br>
            <? }//end allowedit ?>
            <br>
            <span class="subtitle"><?
            if($err != "")
            {
                echo "<a name='1'></a>". $err;
                
            }
            
            ?></span>
            <br>
            
            <input name="uid" type="hidden" id="uid" value="<?=$uid?>">
            <input name="id" type="hidden" id="id" value="<?=$id?>">
            
            <?
            if ($sender) {
                echo '<input name="sender" type="hidden" id="sender" value="' . $sender . '">'; 
            }
             
            if($allowEdit == true)
            {
                echo "<strong>Attach Letter of Recommendation</strong><br>";
                //showEditText("", "file", "file1", $allowEdit); 
                showEditText("", "file", "file1", true); 
                showEditText("Submit", "button", "btnSubmit", true); 
            }
            ?>
            <br><br>
            <?
            
            if($fileid != "")
            {
                //echo "id:". $id . " uid:".$uid . " umasterid:". $umasterid . " appid:" . $appid;
                $path = getFilePath(3, $id, $uid, $umasterid, $appid, $fileid);
                //echo "path:" . $path;
                showFileInfo("recommendation.".$extension, $size, formatUSdate($modDate), $path );            
                
            }//end moddate
            if($recType == 2 || $recType == 3)
            {
                ?>
                </span><br>
                
                <span class="subTitle">Additional Information requested</span><br>
                <span class="tblItem">
                <?
                $link = "#";
                switch($recType)
                {
                    case 2:
                        $link = "recform_academic.php?id=".$id;
                        break;
                    case 3:
                        $link = "recform_industrial.php?id=".$id;
                        break;
                }
                ?>
                <a href="<?=$link?>"><strong>Click here to continue</strong></a>    
                <br> <hr>            </span>
            
            <? }//END IF recType 
            
            // Begin test for MSHCII to include survey
            
          //  if ($domainid == "37") {
          //    include_once './recform_mshcii.php'; 
          //  }
            if($err != "")
            {
                
            }
            
            ?>
      <tr>
        <td>
            <em> <br>
            <br>
            <input name="btnRecs2" type="button" id="btnRecs" 
                value="Return to Recommender Home" onClick="document.location='<?php echo $backLocation; ?>'">
            </em>            <br>
</div>
            <br>
            <br>
        </td>
      </tr>
      <tr>
        <td>
            <span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
            <? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
            &middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
            <? } ?>
            </span>
            </div>
            </td>
          </tr>
        </table>
      
        </form>
        </body>
</html>
