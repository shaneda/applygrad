
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>CMU Online Admissions - University Experience</title>
<!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$degrees = array();
$uniTypes = array();
$institutes = array();
$myInstitutes = array();
$scales = array();
$err = "";
$transUploaded = true;

$arr = array(2, "Graduate");
array_push($uniTypes, $arr);
$arr = array(3, "Additional College/University");
array_push($uniTypes, $arr);
	
/*
foreach($_POST as $key=>$val)
		{
			echo "key: ".$key . " val: ".$val."<br>";
		}
*/

// DEGREES
$result = mysql_query("SELECT * FROM degreesall order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);

	array_push($degrees, $arr);
}
// GPA SCALES

$result = mysql_query("SELECT * FROM gpascales order by name") or die(mysql_error());


while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);

	array_push($scales, $arr);
}

// INSTITUTES
$sql = "SELECT * FROM institutes order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, trim($row['name']));

	array_push($institutes, $arr);
}



function addUni($allowDup, $type)
{
	$doAdd = true;
	global $err;
	if($allowDup == false)
	{
		$sql = "select id from usersinst where user_id = ".$_SESSION['userid'] . " and educationtype=".$type;
		$result = mysql_query($sql)	or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$doAdd = false;
		}
	}
	if($doAdd == true)
	{
		$sql = "insert into usersinst(user_id, educationtype)values(".$_SESSION['userid'].", ".$type.")";
		mysql_query($sql)	or die(mysql_error().$sql);
	}else
	{
		$err .= "You cannot add more than 1 school of this type.";
	}
}

//INSERT RECORD TABLE FOR USER 
if(isset($_POST['btnAdd']))
{
	$allowdups = true;
	
	
	addUni($allowdups, 3);
}//END POST BTNADD



if( isset( $_POST ) && !isset($_POST['btnAdd']) )
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDel') !== false ) 
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			
			$datafileid = -1;
			$type = -1;
			if($itemId != "")
			{
				$sql = "select datafile_id, educationtype from usersinst where user_id=".$_SESSION['userid']. " and id=" . $itemId;
				$result = mysql_query($sql) or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					$datafileid = $row['datafile_id'];
					$type = $row['educationtype'];
				}
				if($datafileid != "")
				{
					$sql = "delete from datafileinfo where id=".$datafileid;
					mysql_query($sql) or die(mysql_error().$sql);
				}
	
				$sql = "DELETE FROM usersinst WHERE user_id=".$_SESSION['userid']. " and id=" . $itemId;
				mysql_query($sql) or die(mysql_error().$sql);
				if($type == 1)
				{
					updateReqComplete("uni.php", 0);
				}
			}
		}
	}//END FOR
}//END IF

//UPDATE
if( isset($_POST['btnSave'])) 
{
	$vars = $_POST;
	$itemId = -1;
	$tmpid = -1;
	$arrItemId = array();
	//HERE WE GET THE RECORD IDS FOR THE UPDATED SCHOOLS
	foreach($vars as $key => $value)
	{
		$arr = split("_", $key);
		if($arr[0] == "txtType")
		{
			$tmpid = $arr[1];
			if($itemId != $tmpid)
			{
				$itemId = $tmpid;
				array_push($arrItemId,$itemId);
			}
		}
	}//END FOR
	//ITERATE THROUGH IDS AND DO UPDATES
	for($i = 0; $i < count($arrItemId); $i++)
	{
		$major2 = NULL;
		$minor1 = NULL;
		$minor2 = NULL;
		$type = "";

		$institute= htmlspecialchars($_POST["lbName_".$arrItemId[$i]]);
		$date_entered= $_POST["txtDateE_".$arrItemId[$i]];
		$date_grad= $_POST["txtDateG_".$arrItemId[$i]];
		//$date_left= $_POST["txtDateL_".$arrItemId[$i]];
		$degree= intval($_POST["lbDegree_".$arrItemId[$i]]);
		$major1= htmlspecialchars($_POST["txtMajor1_".$arrItemId[$i]]);
		if(isset($_POST["txtMajor2_".$arrItemId[$i]])){
		$major2= htmlspecialchars($_POST["txtMajor2_".$arrItemId[$i]]);
		}
		$major3= NULL;
		if(isset($_POST["txtMinor1_".$arrItemId[$i]])){
		$minor1= htmlspecialchars($_POST["txtMinor1_".$arrItemId[$i]]);
		}
		if(isset($_POST["txtMinor2_".$arrItemId[$i]])){
		$minor2= htmlspecialchars($_POST["txtMinor2_".$arrItemId[$i]]);
		}
		$gpa= htmlspecialchars($_POST["txtGPA_".$arrItemId[$i]]);
		$gpa_major= htmlspecialchars($_POST["txtGPAMajor_".$arrItemId[$i]]);
		$gpa_scale= intval($_POST["lbScale_".$arrItemId[$i]]);
		if(isset($_POST["lbType_".$arrItemId[$i]]))
		{
			$type = intval($_POST["lbType_".$arrItemId[$i]]);
		}
		
		$key = marray_search($institute,$institutes);
		
		if($key === false )
		{
			$err .="You have entered an invalid university name (".$institute.")<br>";
		}else
		{
			$institute = $institutes[$key][0];
		}
		if(check_Date2($date_entered) == false)
		{
			$err .= "Date Entered is invalid (".$date_entered.").<br>";
		}
		if(check_Date2($date_grad) == false)
		{
			$err .= "Date of Graduation is invalid (".$date_grad.").<br>";
		}
		/*
		if(check_Date2($date_left) == false)
		{
			//$err .= "Date Leaving is invalid (".$date_left.").<br>";
		}
		*/
		if($degree == 0)
		{
			$err .= "You must select a degree.<br>";
		}
		if($major1 == "")
		{
			$err .= "You must list your major.<br>";
		}
		if($gpa == "")
		{
			$err .= "You must list your GPA.<br>";
		}
		if($gpa_major == "")
		{
			$err .= "You must list your major GPA.<br>";
		}
		if($gpa_scale == 0)
		{
			$err .= "You must select a GPA scale.<br>";
		}
		if($err == "")
		{
		$sql = "update usersinst set
			institute_id= ".$institute.", ";
			if($type != "")
			{
				$sql .= "educationtype=".$type.", ";
			}
			$sql .="date_entered= '".formatMySQLdate2($date_entered,'/','-')."',
			date_grad= '".formatMySQLdate2($date_grad,'/','-')."',
			degree= '".$degree."',
			major1= '".$major1."',
			major2= '".$major2."',
			major3= '".$major3."',
			minor1= '".$minor1."',
			minor2= '".$minor2."',
			gpa= '".$gpa."',
			gpa_major= '".$gpa_major."',
			gpa_scale=".$gpa_scale."
			where id = ".$arrItemId[$i]." and user_id=".$_SESSION['userid'];
			mysql_query($sql) or die(mysql_error());
			
			$sql = "select datafile_id from usersinst where id=".$arrItemId[$i];
			$result = mysql_query($sql) or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				if($row['datafile_id'] == "")
				{
					$transUploaded = false;
				}
			}
			if($transUploaded == true)
			{
				updateReqComplete("uni.php", 1);
			}else
			{
				updateReqComplete("uni.php", 0);
			}
			//header("Location: home.php");
		}
	}


	

}

//RETRIEVE USER INFORMATION
//USER MUST HAVE AT LEAST ONE UNDERGRAD SCHOOL SO ADD IT IF IT DOESN'T EXIST
$doAdd = true;
$sql = "select id from usersinst where user_id = ".$_SESSION['userid'] . " and educationtype=1";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$doAdd = false;
}
if($doAdd == true)
{
	addUni(false, 1);
}


$sql = "SELECT 
usersinst.id,institutes.name,date_entered,
date_grad,date_left,degree,
major1,major2,major3,minor1,minor2,gpa,gpa_major,
gpa_scale,transscriptreceived,datafile_id,educationtype,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
FROM usersinst 
left outer join institutes on institutes.id = usersinst.institute_id
left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
where usersinst.user_id=".$_SESSION['userid']. " order by usersinst.educationtype";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, formatUSdate($row['date_entered'],'-','/')  );
	array_push($arr, formatUSdate($row['date_grad'],'-','/') );
	array_push($arr, formatUSdate($row['date_left'],'-','/') );
	array_push($arr, $row['degree']);
	array_push($arr, $row['major1']);
	array_push($arr, $row['major2']);
	array_push($arr, $row['major3']);
	array_push($arr, $row['minor1']);
	array_push($arr, $row['minor2']);
	array_push($arr, $row['gpa']);
	array_push($arr, $row['gpa_major']);
	array_push($arr, $row['gpa_scale']);
	array_push($arr, $row['transscriptreceived']);
	array_push($arr, $row['datafile_id']);
	array_push($arr, $row['educationtype']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($myInstitutes, $arr);
	
}

?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Education </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<br>
			<span class="tblItem">
			<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='College/University' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?><br>
			</span>
            <br>

<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
				<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
			</div><span class="tblItem">= required</span><br>
<br>

            <span class="subtitle">
            <? showEditText("Save College/University Info", "button", "btnSave", $_SESSION['allow_edit']); ?>
            </span>
            <input name="btnAdd" type="submit" class="tblItem" id="btnAdd" value="Add College/University">
            <br>
	    <hr size="1" noshade color="#990000">
            <span class="errorSubtitle"><?=$err." ";?></span>
			<?
			for($i = 0; $i < count($myInstitutes); $i++)
			{
				$title = "";
				switch($myInstitutes[$i][16])
				{
					case "1":
						$title = "Undergraduate";
					break;
					case "2":
						$title = "Graduate";
					break;
					case "3":
						$title = "Additional";
					break;
				
				}//end switch
				
				?>
			<table width="650" border="0" cellpadding="1" cellspacing="1">
              <tr>
                <td><span class="subtitle">
                  <?=$title;?>
 College/University <em><strong>(Official Transcripts 
<?
					if($myInstitutes[$i][14] > 0)
					{
						echo "Received.";
					}else
					{
						echo "Not Received.";
					}
					?>
)</strong> </em></span><br>
<span class="tblItem">
<? 
			  if($myInstitutes[$i][16] != 1)
			  {
				echo "<strong>Type:</strong>";
				showEditText($myInstitutes[$i][16], "radiogrouphoriz", "lbType_".$myInstitutes[$i][0], 
                                             $_SESSION['allow_edit'],false, $uniTypes); 
			  }
			  ?></span></td>
                          <td align="right"><span class="subtitle">
                          <? showEditText("Delete", "button", "btnDel_".$myInstitutes[$i][0], $_SESSION['allow_edit']); ?>
                          </span>
                          <input name="txtType_<?=$myInstitutes[$i][0];?>" 
                                    type="hidden" id="txtType_<?=$myInstitutes[$i][0];?>" 
                                    value="<?=$myInstitutes[$i][16];?>"></td>
                        </tr>
                        </table>
			<table width="650" border="0" cellpadding="3" cellspacing="0" class="tblItem">
				  <tr>
				    <td  align="right"><strong>College/University:</strong></td>
				    <td colspan="5"><? showEditText($myInstitutes[$i][1], "textbox", 
                                                                    "lbName_".$myInstitutes[$i][0], $_SESSION['allow_edit'],
                                                                    true, $institutes,true,80); ?></td>
                                  </tr>
<!-- NEW ROW -->
                                  <tr>
				    <td  align="right"><strong>Graduation Date</strong><strong>:</strong></td>
				    <td><? showEditText($myInstitutes[$i][3], "textbox", "txtDateG_".$myInstitutes[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>
				    <td align="right"><strong>Date Entered:</strong></td>
				    <td><? showEditText($myInstitutes[$i][2], "textbox", "txtDateE_".$myInstitutes[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
<!-- NEW ROW -->
				  <tr>
				    <td  align="right"><strong>Degree Earned</strong><strong>:</strong></td>
				    <td><? showEditText($myInstitutes[$i][5], "listbox", "lbDegree_".$myInstitutes[$i][0], 
                                                        $_SESSION['allow_edit'],true, $degrees); ?></td>
				    <td align="right">&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
<!-- NEW ROW -->				 
				  <tr>
				    <td align="right"><strong>Major:</strong></td>
				    <td><? showEditText($myInstitutes[$i][6], "textbox", "txtMajor1_".$myInstitutes[$i][0],
                                                        $_SESSION['allow_edit'], true, null,true,20); ?></td>
				    <td align="right"><strong>Minor:</strong></td>
				    <td><? showEditText($myInstitutes[$i][9], "textbox", "txtMinor1_".$myInstitutes[$i][0],
                                                        $_SESSION['allow_edit'], false, null,true,20); ?></td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
 			          </tr>
<!-- NEW ROW -->
				   <? if($myInstitutes[$i][16] == "1"){ ?>
				  <tr>
				    <td  align="right"><strong>2nd Major:</strong></td>
				    <td><? showEditText($myInstitutes[$i][7], "textbox", "txtMajor2_".$myInstitutes[$i][0],
                                                        $_SESSION['allow_edit'], false, null,true,20); ?></td>
				    <td align="right"><strong>2nd minor:</strong></td>
				    <td><? showEditText($myInstitutes[$i][10], "textbox", "txtMinor2_".$myInstitutes[$i][0],
                                                        $_SESSION['allow_edit'], false, null,true,20); ?></td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				  </tr>
				  <? } ?>
<!-- NEW ROW -->

				  <tr>
				    <td align="right"><strong>GPA Major:</strong></td>
                                    <td><? showEditText($myInstitutes[$i][12], "textbox", 
                                                        "txtGPAMajor_".$myInstitutes[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?></td>
				    <td align="right"><strong>GPA Overall:</strong></td>
		                    <td><? showEditText($myInstitutes[$i][11], "textbox", "txtGPA_".$myInstitutes[$i][0],
                                                        $_SESSION['allow_edit'], true); ?></td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
                                 </tr>
<!-- NEW ROW -->
                                 <tr>
                              
                                   <td align="right"><strong>GPA Scale: </strong></td>
           	                   <td><? showEditText($myInstitutes[$i][13], "listbox", "lbScale_".$myInstitutes[$i][0], 
                                                       $_SESSION['allow_edit'],true, $scales); ?></td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
 			          </tr>
<!-- NEW ROW -->
				  <tr>
				    <td colspan="4">
				    </td></tr>
				  <tr>
				   <?
					$received = "Not Received";
					$recDate = "";
					$qs = "?id=".$myInstitutes[$i][0];
					if($myInstitutes[$i][15] > 0)
					{
						$received = "Received.";
						$recDate = "Last updated: ". $myInstitutes[$i][17];
					}
					//echo $received;
					?>
				    <td colspan="4" valign="middle">
					<? if($_SESSION['allow_edit'] == true){ ?>
				      <input class="tblItem" name="btnUpload" value="Upload Unofficial Transcript" type="button" onClick="parent.location='transcripts.php<?=$qs;?>'">
			        <? } ?>	
						<? if($recDate!= "") {
						showFileInfo("transcript.".$myInstitutes[$i][19], $myInstitutes[$i][18], formatUSdate($recDate), getFilePath(1, $myInstitutes[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
						?>
				      <? } ?>				      				</td>
			      </tr>
			</table>
			    <hr size="1" noshade color="#990000">
		    <? 
				
			}//end for 
			
			?>
            <span class="tblItem"><span class="subtitle">
            <? showEditText("Save College/University Info", "button", "btnSave", $_SESSION['allow_edit']); ?>
            </span></span><br>
            <br>
            <br>
<!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
