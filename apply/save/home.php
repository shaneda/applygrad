
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>CMU Online Admissions - Home</title>
<!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$reqs = array();
$compReqs = array();
$apps = array();
$progs = array();
$appName = "";
$firstname = "";
$appid = -1;
$totalcost = 0.0;
$amountPaid = 0.0;
$balance = 0.0;
$submitted = "";


$submissionDate = "";
$paid = "Unpaid";
$paymentdate = "";
$paymenttype = "";
$reviewed = false;
$notified = "";
$appComp = false;
$_SESSION['paid'] = false;

if(isset($_POST['btnNewApp']))
{
	if($appid == -1)
	{
		//INSERT
		$sql = "insert into application (name, user_id, created_date)values('".$_SESSION['firstname']."s Application', ".$_SESSION['userid'].",'".date("Y-m-d h:i:s")."')";
		mysql_query( $sql) or die(mysql_error());
		//$appid = mysql_insert_id();
	}
}
//GET APPLICATIONS
$sql = "SELECT id, name, paid, paymentamount, submitted, submitted_date,created_date,waive, paymenttype, paymentdate   from application where user_id = ". $_SESSION['userid'] . " order by created_date limit 1";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['paid']);
	array_push($arr, $row['paymentamount']);
	array_push($arr, $row['submitted']);
	array_push($arr, formatUSdate3($row['submitted_date']));	
	array_push($arr, $row['created_date']);
	array_push($arr, $row['waive']);
	array_push($arr, $row['paymenttype']);
	array_push($arr, formatUSdate3($row['paymentdate']));	
	array_push($apps, $arr);
	//echo $row['id']."<br>";
}

//GET USER INFO
$sql = "SELECT firstname from lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id where lu_users_usertypes.id = ". $_SESSION['userid'];
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$firstname = $row['firstname'];
}

if($_SESSION['allow_edit'] == true)
{

	if(count($apps) > 0 )
	{
		$_SESSION['appid'] = $apps[0][0];
	}
		
	if((isset($_GET['id']) || $_SESSION['appid'] != -1) && $_SESSION['userid'] != -1 )
	{
		if(isset($_GET['id']))
		{
			$_SESSION['appid'] = $_GET['id'];
		}
		
		//VERIFY USER/APP COMBO
		$sql = "select id, name from application where user_id = ".$_SESSION['userid']." and id = ". intval($_SESSION['appid']);
		$result = mysql_query($sql) or die(mysql_error());
		
		while($row = mysql_fetch_array( $result )) 
		{
			$_SESSION['appid'] = $row['id'];
			$appName = $row['name'];
		}
		//GET REQUIREMENTS
		$sql = "SELECT applicationreqs.id, applicationreqs.short, applicationreqs.linkname, application.name as appname, 
		lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified,
		lu_application_appreqs.completed
		FROM lu_application_programs
		inner join programs on programs.id = lu_application_programs.program_id
		inner join degree on degree.id = programs.degree_id
		inner join lu_degrees_applicationreqs on lu_degrees_applicationreqs.degree_id = programs.degree_id
		inner join applicationreqs on applicationreqs.id = lu_degrees_applicationreqs.appreq_id
		inner join application on application.id = lu_application_programs.application_id
		left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
		and lu_application_appreqs.req_id = applicationreqs.id
		where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id order by applicationreqs.sortorder";
		//echo $sql;
		$result = mysql_query($sql) or die(mysql_error());

		while($row = mysql_fetch_array( $result )) 
		{
			$arr = array();
			array_push($arr, $row['id']);
			array_push($arr, $row['short']);
			array_push($arr, $row['linkname']);	
			array_push($arr, $row['compreqid']);	
			array_push($arr, formatUSdate($row['last_modified']));	
			array_push($arr, $row['completed']);	
			array_push($compReqs, $arr);
		}
	}
}//END ALLOW EDIT

function getProgress($appid)
{
	global $submitted;
	global $paid;
	global $submissionDate;
	global $paymentdate;
	global $notified;
	global $reviewed;
	$sql = "SELECT 
	submitted, submitted_date, paid, paymentamount, paymentdate, notificationsent,waive
	FROM application
	where id=".$appid;
	//echo $sql;
	$result = mysql_query($sql) or die(mysql_error().$sql);
	
	while($row = mysql_fetch_array( $result )) 
	{
		if($row['submitted'] == "1")
		{
			$submitted = "Submitted";
		}
		if($row['waive'] == "1")
		{
			$submitted = "Payment Waived";
		}else
		{
			if($row['paid'] == "1")
			{
				$submitted = "Paid";
			}
			$paymentdate = $row['paymentdate'];
		}
		$submissionDate = $row['submitted_date'];
		
		$notified = $row['notificationsent'];
		$reviewed = false;
	}
}

function getProgs($appid)
{
	$ret = array();
	$sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname,programs.linkword  from lu_application_programs
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	where lu_application_programs.application_id = ". $appid;
	$result = mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['fieldname']);
		array_push($arr, $row['linkword']);
		array_push($ret, $arr);
	}
	
	return $ret;
}
?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			 <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
			<!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Welcome, <?=$firstname?>!  </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<? if(count($apps) > 0)
			{
				//do nothing
			}else{
				showEditText("Start a new application", "button", "btnNewApp", $_SESSION['allow_edit']); 
			}//end count apps ?>
			<? 
			if(count($apps) > 0)
			{ 
			$class = "tblItem";
			$paid = "not paid";
			
			?>
            <table width="660" height="480"  border="0" cellpadding="4" cellspacing="6">
              <tr>
                <td valign="top" bgcolor="#FFFFFF" class="tblItem">			   
                  
			
			
			<?
			for($i = 0; $i < count($apps); $i++)
			{
				$submissionDate = $apps[$i][5];
				$paymentdate = $apps[$i][9];
				echo "<input name='appid' type='hidden' value='".$apps[$i][0]."'>";
/*
				if($i == 0)
				{
				?><span class="subTitle">Your application </span><br><?
				}
*/
				if($i == 1)
				{
					?>
					<br><span class="subTitle">Your previous applications</span><br>
					<?
				}
				if($i > 0)
				{
					echo "<em>".$i.") - ".formatUSDate($apps[$i][6])."</em><br>";
				}
				if($apps[$i][4] == 1)
				{
					$submitted = "checked";
				}
				if($i %2 == 0)
				{
					$class = "tblItemAlt";
				}
				if($apps[$i][8] > 0 && $apps[$i][2] != 1)
				{
					$paid = "Pending Receipt of Payment";
					switch ($apps[$i][8])
					{
						case 1:
							$paid .=" - Mailed in Payment";
						break;
						case 2:
						$paid .=" - Paid by Credit Card";
						break;
					}
					$balance = "";
					$totalcost = "";
				}
				if($apps[$i][7] == 1)
				{
					$paid = "Payment Waived";
					$balance = "";
					$totalcost = "";
				}else
				{
					if($i ==0)
					{
						if($apps[$i][2] == 1)
						{
							$paid = "paid";
						}
						$amountPaid = $apps[$i][3];
						$totalcost = totalCost($apps[$i][0]);
						$balance = $totalcost - $amountPaid;
					}
				}//end if waived
				
				
				$progs = getProgs($apps[$i][0]); 
				if(count($progs)>0){
				?>
				 <br>
				 <strong>Program(s) selected in order of preference</strong> <br>


				  <div id="list">
					  <ul>
						<?
						for($j = 0; $j < count($progs); $j++)
						{ 
							echo "<li>".$progs[$j][1]." ".$progs[$j][3]." " .$progs[$j][2]. "</li>" ;
						}
					  ?>
					  </ul>
				  </div>
				<? 
				}else{
					header("Location: programs.php");
				}//end if count progs
			}//end for apps 
		?>
                 <br>
				  <?
				  if($_SESSION['appid'] > -1)
				  {
				  if(count($compReqs) == 0 )
				  {
				  	echo "<strong>You need to apply to at least 1 program.</strong><br><br><a href='programs.php'><span class='subtitle'>Click here to view available programs</span></a>.<br>";
				  }
				  else
				  {
				  ?>
				  <span><strong>Application Checklist</strong></span><br>
                                  The following sections of the application must be completed before your 
                                  application can be submitted. 
                                  A check will appear in the box when the section is complete. 
                                  You must submit your completed application by the deadline. 
                                  Once the application has been submitted, you may return to the corresponding 
                                  section to track receipt of test score reports, official transcripts,
                                  and letters of recommendation.
                                  <br>                                 
                                  <br>
				  <table width='100%'  border='0' cellspacing='0' cellpadding='0'>
				  <tr>
				  <td class="tblItem"><strong> </strong></td>
				  <td class="tblItem"><i>Last Modified</i></td>
				  </tr>
				  <?
				 $appComp = true;
				 if(count($compReqs) == 0)
				 {
				  $appComp = false;
				 }
					  for($i = 0; $i < count($compReqs); $i++)
					  {
					  	if($compReqs[$i][5] != 1)
						{
							$appComp = false;
						}
					  	$date = "";
						$comp = "0";
						if($compReqs[$i][4] != "")
						{
							$date = "(".$compReqs[$i][4].")";
							
						}
						//$comp = "1";
						echo "<tr><td>";
						 showEditText($compReqs[$i][5], "checkbox", "checkbox",true,false, null,false);
						 echo "<a href='".$compReqs[$i][2]."'>".str_replace("<br>"," ",$compReqs[$i][1])."</a></td><td class='tblItem'><em>".$date."</em></td></tr>";
					  }
					   $submitted = "";
						for($i = 0; $i < count($apps); $i++)
						{ 
							if($apps[$i][4] == 1 && $apps[$i][0] == $_SESSION['appid'])
							{
								$submitted = "checked";
							}
						
						}
					  if($appComp == true && $submitted == "checked")
					  {
					  ?>
					  <tr class = "tblItem">
					  <td><? showEditText(true, "checkbox", "checkbox",true,false, null,false); ?>
					  Application Submitted 
					  </td>
					  <td><em>(<?= $submissionDate ?>)</em></td>
					  </tr>
					  <?
					  }//end checked submit
					  
					  if($appComp == true && $paymentdate != "" && $paymentdate != "00/00/0000 00:00:00")
					  {
					  ?>
					  <tr class = "tblItem">
					  <td><? showEditText(true, "checkbox", "checkbox",true,false, null,false); ?>
					  Application Fee <?
					  if($paid == "paid")
					  {
					  echo "Received";
					  }else
					  {
					  	echo "Submitted";
					  }
					  ?>
					   
					  </td>
					  <td><em>(<?= $paymentdate ?>)</em></td>
					  </tr>
					  <?
					  }
					  
					  
					  
					  ?>
					  
			  	  </table>
					
					<?
					if($appComp == true && $submitted != "checked"){
					?>
                                        <br>
                                        You have provided the required information for all sections of this application. 
                                        The final step of the process is to submit the completed application and pay your application fee of $<?= $totalcost ?>.
					<br>
					<br>
					<input class="tblItem" name="btnSubmit" type="button" id="btnSubmit" value="Proceed to Submission & Payment" onClick="document.location='submit.php'"><br>

					<? } ?>   
				
				 
				  				  
				  <? if($submitted == "checked"){?>
				  <br>
					<div id="list">
                          <? if($paid == "Payment Waived"){ ?>
						  Payment Status: Waived
						  <? }else{ 
						  if($paymentdate != "" && $paymentdate != "00/00/0000 00:00:00")
						  {
						  	//$paid == "Pending Receipt of Payment"
						  }
						 
						  ?>
						  <ul>
                            <li><strong>Application fee :</strong> $<?= $totalcost?><em> (<?=$paid;?>)</em>							</li>
                            <? 
						 $balance = $totalcost - $amountPaid;
						 if($appComp == true && ($paymentdate == "" || $balance > 0)){ ?>
							<li>You have a balance due of $<?=$balance?></li>
							<li >							</li>
                            <li >
                              <input class="tblItem" name="btnPay" type="button" id="btnBack" value="Pay Application Fee Now" onClick="document.location='pay.php'">
                            </li>
                            <li></li>
				<li><strong>Note:</strong> Your application will not be considered unless the required fee is paid in full.
                                          Please allow 2-3 business days for your credit card payment to be recorded as 
                                          <em>Paid</em> on your submitted application. 
                                </li>
							<? }//end if not submitted payment ?>
                          </ul>
						  <? }//end if paid ?>
                  </div>				
					<span class="subtitle">
					<? }//end if submitted ?>
			      <? }//end if compreqs  
				  }//end appid
				  ?>                  
                </td>
				<? if($_SESSION['appid'] != -1 ){ ?>
                <? }//END IF APPID ?>
              </tr>
            </table>
      <?  }//end count apps ?>
			<!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
