
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>SCS Graduate Online Application</title>
<!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?

$err = "";
$myPrograms = array();
$cardTypes = array(array('Visa', 'Visa'), array('MasterCard','MasterCard'), array('American Express','American Express'));
$paymentTypes = array(array('Mail Payment', 'Mail Payment'), array('Credit Card','Credit Card'));
$states = array();
$countries = array();
$months = array();
$years = array();
$amt = totalCost($_SESSION['appid']);
$ptype = "";
$customer_firstname = "";
$customer_lastname = "";
$customer_email = "";
$bill_address1 = "";
$bill_city = "";
$bill_state = "";
$bill_zip = "";
$bill_country = "";
$customer_phone = "";
$card_type = "";
$customer_cc_number = "";
$customer_cc_expmo = "";
$customer_cc_expyr = "";
$customer_cc_code = "";
$paid = false;

$thankyouurl = "https://".$_SERVER['HTTP_HOST'] . "/apply/payccthanks.php";
$gLstr = "6160200000100002127012101";

$poststr = "";

for($i = 1; $i < 13; $i++)
{
	$arr = array($i, $i);
	array_push($months, $arr);
}
for($i = 0; $i < 10; $i++)
{
	$k = date("Y")+$i;
	$arr = array($k, $k);
	array_push($years, $arr);
}

if(isset($_GET['btnCancel']))
{
	header("Location: home.php");
}


if(isset($_POST['btnCancel']))
{
	header("Location: home.php");
}
//STATES

$result = mysql_query("SELECT * FROM states order by country_id,name")or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['abbrev']);
	array_push($arr, $row['name']);
	array_push($states, $arr);
}
$result = mysql_query("SELECT * FROM countries order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['iso_code']);
	array_push($arr, $row['name']);
	array_push($countries, $arr);
}

//GET USERS SELECTED PROGRAMS
$sql = "SELECT programs.id, 
degree.name as degreename,
fieldsofstudy.name as fieldname, 
choice, 
lu_application_programs.id as itemid,
programs.programprice,
programs.baseprice
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice";
$result = mysql_query($sql) or die(mysql_error() . $sql);


while($row = mysql_fetch_array( $result )) 
{
	$dept = "";
	$baseprice = 0.0;
	$oracle = "";
	$depts = getDepartments($row[0]);
	$tmpDept = "";
	if(count($depts) > 1)
	{
		
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$dept .= $depts[$i][1];
			$oracle = $depts[$i][2];
			$baseprice = $depts[$i][3];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
			
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row['degreename']);
	array_push($arr, $row['fieldname'].$dept);
	array_push($arr, $row['choice']);
	array_push($arr, $row['itemid']);
	array_push($arr, $row['programprice']);
	array_push($arr, $oracle);
	array_push($arr, $row['baseprice']);

	array_push($myPrograms, $arr);
}

	

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT 
	department.id,
	department.name,
	department.oraclestring,
	systemenv.appbaseprice
	FROM lu_programs_departments 
	inner join department on department.id = lu_programs_departments.department_id
	inner join lu_domain_department on lu_domain_department.department_id
	inner join systemenv on systemenv.domain_id = lu_domain_department.domain_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($arr, $row["oraclestring"]);
		array_push($arr, $row["appbaseprice"]);
		array_push($ret, $arr);
	}
	return $ret;
}


?>

<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Payment</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
<!--
			<span class="tblItem"><strong><a href="home.php"></a></strong></span>
			<input class="tblItem" name="btnBack" type="button" id="btnBack" value="Return to Homepage" onClick="document.location='home.php'">
-->

			<span class="errorSubtitle"><?=$err?></span>
			
			<?

//   Show the page content prior to submission of credit card payment
//   Once credit card payment made and end up back at this page
//   only need to display the message that its pending and
//   the return to home button

			if(isset($_SESSION['paid']))
			{
				if($_SESSION['paid'] == true)
				{
					$paid = true;
				}
			}
			if($paid != true)
			{


			$domainid = 1;
			if(isset($_SESSION['domainid']))
			{
				if($_SESSION['domainid'] > -1)
				{
					$domainid = $_SESSION['domainid'];
				}
			}

			$sql = "select content from content where name='Payment Credit Page' and domain_id=".$domainid;
			
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				echo html_entity_decode($row["content"]);
			}
			?>
			<br><br>


			
			<input name="txtCredit" type="hidden" value="1">
			<? showEditText($amt, "hidden", "amt", $_SESSION['allow_edit'],true); ?>
			<? showEditText("10530", "hidden", "store_no", $_SESSION['allow_edit']); ?>
			<? showEditText($thankyouurl, "hidden", "return_url", $_SESSION['allow_edit']); ?>
			<? showEditText("Return to Online Admissions", "hidden", "return_url_text", $_SESSION['allow_edit']); ?>
			<? showEditText($_SESSION['appid'], "hidden", "merchant_ref_no", $_SESSION['allow_edit']); ?>
			<? showEditText("Y", "hidden", "settle_now", $_SESSION['allow_edit']); ?>
			<? showEditText($paymentEmail, "hidden", "notify_email", $_SESSION['allow_edit']); ?>
			<? showEditText("Graduate Online Application Fee", "hidden", "item1_name", $_SESSION['allow_edit']); ?>
			<? 
			
			for($i = 0; $i < count($myPrograms); $i++)
			{
			//echo $myPrograms[$i][2]."<br>";
			$amt = 0.0;
			if($i == 0)
			{
			$amt = $myPrograms[$i][7];
			}else
			{
			$amt = $myPrograms[$i][5];
			}
			showEditText($myPrograms[$i][1]. "_".($i+1), "hidden", "item".($i+1)."_name", $_SESSION['allow_edit']); 
			showEditText(1, "hidden", "item".($i+1)."_qty", $_SESSION['allow_edit']); 
			showEditText($amt, "hidden", "item".($i+1)."_price_each", $_SESSION['allow_edit']); 
			showEditText($myPrograms[$i][6], "hidden", "item".($i+1)."_gl_str", $_SESSION['allow_edit']); 
			echo "\n";
			}
			
			
			$poststr = "<script language='javascript'>document.form1.action='".$paymentProcessor."'; document.form1.submit(); </script>";

				showEditText("Proceed To Pay By Credit Card", "button", "btnSubmit", $_SESSION['allow_edit']);
				showEditText("Cancel/Return to Homepage", "button", "btnCancel", $_SESSION['allow_edit']);
			}else
			{
			  echo "Your payment is now pending. Please allow 2-3 business days for your credit card payment to be 
                                recorded as <em>Paid</em> on your submitted application.";
?>
                          <BR><BR>
 			  <span class="tblItem"><strong><a href="home.php"></a></strong></span>
			  <input class="tblItem" name="btnBack" type="button" id="btnBack" value="Return to Homepage" onClick="document.location='home.php'">
<?
			}
			  
			if(isset($_POST['btnSubmit']) )
			{
				
				if($paid == false)
				{
					$sql = "update application set paymenttype = 2, paymentdate ='".date("Y-m-d h:i:s")."' where id=".$_SESSION['appid'];
					mysql_query($sql)or die(mysql_error());
					$_SESSION['paid'] = true;
					echo $poststr;
				}
			}
			
			?>
			
		
			
            <!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
