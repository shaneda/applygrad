<?
/* Web page header information specific School-programs */
/* can probably put this in the database, but will wait */
/* till later to do that  */ 

/* Indexed by Domain */

$GRE_SCORES_REQUIRED[-1] = true;
$GRE_SCORES_REQUIRED[0] = true;                             
$GRE_SCORES_REQUIRED[1] = true; 
$GRE_SCORES_REQUIRED[2] = true;
$GRE_SCORES_REQUIRED[3] = true;
$GRE_SCORES_REQUIRED[4] = true;
$GRE_SCORES_REQUIRED[5] = true;
$GRE_SCORES_REQUIRED[6] = true;
$GRE_SCORES_REQUIRED[7] = true;
$GRE_SCORES_REQUIRED[8] = true;
$GRE_SCORES_REQUIRED[9] = true;
$GRE_SCORES_REQUIRED[10] = true;
$GRE_SCORES_REQUIRED[11] = true;
$GRE_SCORES_REQUIRED[12] = true;
$GRE_SCORES_REQUIRED[13] = true;
$GRE_SCORES_REQUIRED[14] = true;
$GRE_SCORES_REQUIRED[15] = true;
$GRE_SCORES_REQUIRED[16] = false; /* MSE-Dist */
$GRE_SCORES_REQUIRED[17] = false; /* MSE-Campus */
$GRE_SCORES_REQUIRED[18] = true;
$GRE_SCORES_REQUIRED[19] = true;
$GRE_SCORES_REQUIRED[20] = false; /* MSIT-GM-Dist */
$GRE_SCORES_REQUIRED[21] = true;
$GRE_SCORES_REQUIRED[22] = false; /* MSIT-eBiz */
$GRE_SCORES_REQUIRED[23] = true;
$GRE_SCORES_REQUIRED[24] = true;
$GRE_SCORES_REQUIRED[25] = true           ; 

/* 
   Used for determining whether or not the supplemental
   information section has tests to ensure section is
   filled out by the applicant. On entry to the page
   it automatically updates as complete. Currently only
   the CNBC Training Program has a requirement to provide
   permission to obtain records
*/
   
$SUPPINFO_REQUIRED[-1] = false;
$SUPPINFO_REQUIRED[0] = false;                             
$SUPPINFO_REQUIRED[1] = false; 
$SUPPINFO_REQUIRED[2] = false;
$SUPPINFO_REQUIRED[3] = false;
$SUPPINFO_REQUIRED[4] = false;
$SUPPINFO_REQUIRED[5] = false;
$SUPPINFO_REQUIRED[6] = false;
$SUPPINFO_REQUIRED[7] = false;
$SUPPINFO_REQUIRED[8] = false;
$SUPPINFO_REQUIRED[9] = false;
$SUPPINFO_REQUIRED[10] = false;
$SUPPINFO_REQUIRED[11] = true; /* CNBC */
$SUPPINFO_REQUIRED[12] = false;
$SUPPINFO_REQUIRED[13] = false;
$SUPPINFO_REQUIRED[14] = false;
$SUPPINFO_REQUIRED[15] = false;
$SUPPINFO_REQUIRED[16] = false;
$SUPPINFO_REQUIRED[17] = false;
$SUPPINFO_REQUIRED[18] = false;
$SUPPINFO_REQUIRED[19] = false;
$SUPPINFO_REQUIRED[20] = false;
$SUPPINFO_REQUIRED[21] = false;
$SUPPINFO_REQUIRED[22] = false;
$SUPPINFO_REQUIRED[23] = false;
$SUPPINFO_REQUIRED[24] = false;
$SUPPINFO_REQUIRED[25] = false; 
?>