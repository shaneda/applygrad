
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>CMU Online Admissions - Resume</title>
<!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$id = -1;
$err = "";
$resName = "";
$resSize = 0;
$resModDate = "";

$stateName = "";
$stateSize = "";
$stateModDate = "";
$resumeFileId = -1;
$statementFileId = -1;

if(isset($_FILES["fileResume"]) )
{
	$ret = handle_upload_file(2, $_SESSION['userid'],$_SESSION['usermasterid'], $_FILES["fileResume"],$_SESSION['appid']."_1" );
	//echo $ret;
	$err = "";
	if(intval($ret) < 1)
	{
		$err = $ret."<br>";
	}else
	{
		
		//header("Location: home.php");
		
	} 

}
if(isset($_FILES["fileStatement"]) )
{
	$ret = handle_upload_file(4, $_SESSION['userid'],$_SESSION['usermasterid'], $_FILES["fileStatement"],$_SESSION['appid']."_1" );
	echo $ret;
	$err = "";
	if(intval($ret) < 1)
	{
		$err = $ret."<br>";
	}else
	{
		//updateReqComplete("resume.php");
		//header("Location: home.php");
		
	} 

}
  
 //RETRIEVE USER INFORMATION
$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=2";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$resName  = "resume.".$row['extension'];
	$resSize  = $row['size'];
	$resumeFileId = $row['id'];
	$resModDate = $row['moddate'];
} 

$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=4";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$stateName  = "statement.".$row['extension'];
	$stateSize  = $row['size'];
	$statementFileId = $row['id'];
	$stateModDate = $row['moddate'];
} 


if($resModDate != "" && $stateModDate != "")
{
	updateReqComplete("resume.php", 1);
}else
{
	if(!isset($_FILES["fileResume"]) || !isset($_FILES["fileStatement"]))
	{
		updateReqComplete("resume.php", 0);
	}
}
  
?>





<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data" ><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Resume and Statement</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" --><?=$err;?><span class="tblItem">
			<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='Resume and Statement' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?>
            The Resume and Statement of Purpose must be submitted in PDF, MS Word or text format. 
            Any other format will not be accepted. The maximum file size is 3MB.<br>

            <br>
            <? showEditText("Save Resume & Statement", "button", "btnSubmit", $_SESSION['allow_edit']); ?>
            <br>

            <hr size="1" noshade color="#990000">
            </span><span class="subtitle">Resume</span><span class="tblItem"><br>
			<? 
			$qs = "?id=1&t=2";
			?>
			<input class="tblItem" name="btnUpload" value="Upload Resume" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
			<?
			if($resModDate != "")
			{
			showFileInfo($resName, $resSize, formatUSdate($resModDate), getFilePath(2, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
			}//end if ?>
            
            <br><br>
            <hr size="1" noshade color="#990000">
            </span><span class="subtitle">Statement of Purpose</span><span class="tblItem"><br>
			<? $qs = "?id=1&t=4"; ?>
			<input class="tblItem" name="btnUpload" value="Upload Statement of Purpose" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
			<? if($stateModDate != "")
			{
				showFileInfo($stateName, $stateSize, formatUSdate($stateModDate), getFilePath(4, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
			}//end if ?>
            <br><br>
            <hr size="1" noshade color="#990000">

            <? showEditText("Save Resume & Statement", "button", "btnSubmit", $_SESSION['allow_edit']); ?>
            <br>
            </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
