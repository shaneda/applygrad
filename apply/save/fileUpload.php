
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>SCS Graduate Online Application</title>
<!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$id = -1;
$type = -1;
$err = "";
$isvalid = false;
$returnurl = "";
$title = "";
$returnurl = "";

if(isset($_GET['id']))
{
	if($_GET['id'] != "")
	{
		$id = intval($_GET['id']);
	}
}
if(isset($_GET['t']))
{
	if($_GET['t'] != "")
	{
		$type = intval($_GET['t']);
	}
}


switch($type)
{
	case 1:
		//transcript
		$returnurl = "uni.php";
		$title = "Colleges/Universities";
		break;
	case 2:
		//resume
		$title = "Resume";
		$returnurl = "resume.php";
		$title = "Resume";
		break;
	case 3:
		//recommendation
		$title = "Recommendation";
		break;
	case 4:
		$returnurl = "resume.php";
		$title = "Statement of Purpose";
		//statement
		break;
	case 5:
		//experience
		$returnurl = "suppinfo.php";
                $title = "Experience";
		break;
	case 6:
		//gre score
		$returnurl = "scores.php";
		$title = "Test Scores";
		break;
	case 7:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		break;
	case 8:
		$returnurl = "scores.php";
		$title = "Test Scores";
		//toefl score
		break;
	case 9:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		break;
	case 10:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		break;
}//end switch

if(isset($_FILES["file"]))
{
	$ret = handle_upload_file($type, $_SESSION['userid'],$_SESSION['usermasterid'], &$_FILES["file"],$_SESSION['appid']."_".$id );
	//echo $ret;
	$err = "";
	if(intval($ret) < 1)
	{
		$err = $ret."<br>";
	}else
	{
		$sql = "";
		switch($type)
		{
			case 1:
				//transcript
				$sql = "update usersinst set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 2:
				//resume
				break;
			case 3:
				//recommendation

				break;
			case 4:
				//statement
				break;
			case 5:
				//experience
				$sql = "delete from experience where application_id=".intval($_SESSION['appid'])." and experiencetype=".intval($id);
				$result = mysql_query($sql) or die(mysql_error());
				$sql = "insert into experience(application_id, datafile_id, experiencetype )values(".intval($_SESSION['appid']).", ".intval($ret).", ".intval($id).")" ;
				break;
			case 6:
				//gre score
				$sql = "update grescore set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 7:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 8:
				$sql = "update gresubjectscore set datafile_id =".intval($ret)." where id = ".$id;
				//toefl score
				break;
			case 9:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 10:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
		}//end switch
		
	
		if($sql != "")
		{
			$result = mysql_query($sql) or die(mysql_error());
		}
		//echo $sql;
		header("Location: ".$returnurl);
		
	} 

}
  
  
  
  
?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" enctype="multipart/form-data" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title"><?=$title ?> File Upload </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" --><span class="errorSubtitle"><?=$err;?></span><br>
<span class="tblItem">Your <?=$title ?> file must be submitted in PDF text format. Any other format will not be accepted. The maximum file size for your transcripts is 3MB.</span><br>
<!-- <span class="tblItem">Your file can be in image or pdf format. The maximum file size for your transcripts is 3MB</span><br> -->
            <br>
            <input name="file" type="file" class="tblItem" maxlength="255">
            <? showEditText("Upload", "button", "btnSubmit", $_SESSION['allow_edit']); ?>
            <!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
