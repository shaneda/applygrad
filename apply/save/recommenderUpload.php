
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" -->
<title>SCS Graduate Online Application</title>
<!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->

<?
$id = -1;
$uid = -1;
$umasterid = -1;
$appid = -1;
$err = "";
$uName = "";
$recText = "";
$modDate = "";
$fileid = -1;
$buckley = 0;
$size = 0;
$extension = "";
$allowEdit = $_SESSION['allow_edit'];
$recType = -1;

if(isset($_POST['uid']))
{
	$uid = $_POST['uid'];
}
if(isset($_POST['id']))
{
	$id = $_POST['id'];
}
//HANDLE VARS COMING IN FROM RECOMMENDER.PHP
if(isset($_POST))
{
	$vars = $_POST;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'sender') !== false ) 
		{
			$arr = split("_", $value);
			$id = $arr[1];
			$uid = $arr[2];
		}
		
	}//END FOR
}
if($id > 0 && $uid > 0)
{
	//RETRIEVE USER INFORMATION
	$sql = "SELECT firstname, lastname, recommend.submitted, 
	recommend.content,
	datafileinfo.id as datafileid,
	datafileinfo.moddate,
	users.id as umasterid,application.id as appid,
	datafileinfo.extension,
	datafileinfo.size,
	recommendtype
	FROM recommend
	inner join application on application.id = recommend.application_id
	inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
	inner join users on users.id = lu_users_usertypes.user_id
	left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
	where recommend.id=".$id;
	$result = mysql_query($sql) or die(mysql_error());
	//echo $sql;
	while($row = mysql_fetch_array( $result ))
	{
		$umasterid = $row['umasterid'];
		$appid = $row['appid'];
		if($row['submitted'] == 1)
		{
			$allowEdit = false;
		}
		$uName = $row['firstname']. " " . $row['lastname'];
		$recText = $row['content'];
		$modDate = $row['moddate'];
		$fileid = $row['datafileid'];
		$extension = $row['extension'];
		$size = $row['size'];
		$recType = $row['recommendtype'];
	}
	
	if(isset($_POST["btnSubmit"]) )
	{
	
		if(isset($_FILES["file1"]) )
		{
			
			$ret = handle_upload_file(3, $uid,$umasterid, $_FILES["file1"],$appid."_".$id );
			
			$err = "";
			if(intval($ret) < 1)
			{
				$err = $ret."<br>";
			}else
			{
				$sql = "update recommend set datafile_id =".intval($ret).", submitted=1 where id = ".$id;
				$result = mysql_query($sql) or die(mysql_error());
				$_SESSION["recAllow_".$id."_".$uid] = true;
				//echo "ret ". $ret . " uid " . $uid . " appid ". $appid . " id " . $id ;
				//echo $_SESSION["recAllow_".$id."_".$uid];
			}
	
		}
		if($err == "")
		{
			//header("Location: recommender.php");
			
		}
	}
	
}else
{
	$err .= "Invalid request<br>";
}

function getProgs($appid)
{
	$ret = array();
	//GET USERS SELECTED PROGRAMS
	$sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname, choice, 
	lu_application_programs.id as itemid,
	application.buckleywaive
	FROM lu_application_programs 
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	inner join application on application.id = lu_application_programs.application_id
	where lu_application_programs.application_id = ".$appid." order by choice";
	$result = mysql_query($sql) or die(mysql_error() . $sql);
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$dept = "";
		$depts = getDepartments($row[0]);
		if(count($depts) > 1)
		{
			$dept = " - ";
			for($i = 0; $i < count($depts); $i++)
			{
				$dept .= $depts[$i][1];
				if($i < count($depts)-1)
				{
					$dept .= "/";
				}
				
			}
		}
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['fieldname'].$dept);
		array_push($arr, $row['choice']);
		array_push($arr, $row['itemid']);
		array_push($arr, $row['buckleywaive']);
	
		array_push($ret, $arr);
	}

	
	return $ret;
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT 
	department.id,
	department.name
	FROM lu_programs_departments 
	
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

function getAreas($id, $appid)
{
	$ret = array();
	$sql = "SELECT 
	interest.id,
	interest.name
	FROM 
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.id=".$id. " and lu_application_programs.application_id=".$appid . " order by 		lu_application_interest.choice";

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}
 

?>

<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data" ><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Upload Recommendation </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<span class="errorSubtitle"><?=$err;?></span>
			<span class="tblItem">
			<a href="recommender.php"></a><em>
			<input name="btnRecs" type="button" id="btnBack" value="Back to Recommendations" onClick="document.location='recommender.php'">
			</em><br><br>

			<? 
			
			if( isset($_SESSION["recAllow_".$id."_".$uid] ))
			{
				$allowEdit = true;
			}
			if($allowEdit == true){ 
			
				$programs = getProgs($appid);
				
				$domainid = 1;
				if(isset($_SESSION['domainid']))
				{
					if($_SESSION['domainid'] > -1)
					{
						$domainid = $_SESSION['domainid'];
					}
				}
				$sql = "select content from content where name='Recommendation Upload Page' and domain_id=".$domainid;
				$result = mysql_query($sql)	or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					echo html_entity_decode($row["content"]);
				}
			
			?>
			<br>
			Name of applicant: <strong><?=$uName?></strong><br>
			<br>
			Programs applied to:<br>
			</span>
			<div id="list" class="tblItem">
			<ul>
			<? for($i = 0; $i < count($programs); $i++)
			{ 
				$buckley = $programs[$i][5];
				$areas = getAreas($programs[$i][4], $appid);
				?>
				<li><strong><?=$programs[$i][1]?> in <?=$programs[$i][2]?></strong>
				<ul>
				<? for($j = 0; $j < count($areas); $j++){ ?>
					<li><?=$areas[$j][1]?></li>
			<? } ?>
			</ul>
			</li>
			<? } ?>
			</ul>
			</div><br>
			<span class="tblItem">Under the <strong>Buckley Amendment</strong> this applicant 
                                              has <strong><? if($buckley!=1){echo "NOT ";} ?>waived</strong> 
                                              the right to see this recommendation for admission. <br>
			<br>
			
			You are about to upload a recommendation for the  applicant <strong><?=$uName?></strong> 
                        to Carnegie Mellon University. 
                        <strong>Once your recommendation has been submitted, you will no longer be able to update it.
                        </strong> 
            <br>
            <br>
            You may paste the text of the transcript into the field below, or upload a file below.
			Acceptable file formats are PDF, MS Word or text format. 
                        Any other format will not be accepted. The maximum file size is 3MB.<br>
			<? }else{ ?>
			You have submitted the following information for the applicant <strong><?=$uName?></strong>. You may not change this data once it is saved.<br>
			<? }//end allowedit ?>
			<br>
            <br>
            <strong>
			<input name="uid" type="hidden" id="uid" value="<?=$uid?>">
			<input name="id" type="hidden" id="id" value="<?=$id?>">
            <? 
			if($allowEdit == true)
			{
				//showEditText("", "file", "file1", $allowEdit); 
				showEditText("", "file", "file1", true); 
				showEditText("Submit", "button", "btnSubmit", true); 
			}
			?>
            </strong><br>
            <? 
			if($modDate != "")
			{
				showFileInfo("recommendation.".$extension, $size, formatUSdate($modDate), getFilePath(3, $id, $uid, $umasterid, $appid));			
				
			}//end moddate
			?>
			</span><br>
			<hr>
			<span class="subTitle">Additional Information requested</span><br>
			<span class="tblItem">
			<?
			$link = "#";
			switch($recType)
			{
				case 2:
					$link = "recform_mse_academic.php?id=".$id;
					break;
				case 3:
					$link = "recform_mse_industrial.php?id=".$id;
					break;
			}
			?>
			<a href="<?=$link?>"><strong>Click here to continue</strong></a>
			</span>
			
			
			<!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
