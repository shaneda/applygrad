
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$appcomp = true;
$firstname = "";
$lastname = "";
$email = "";
$submitted = "Unsubmitted";
$submissionDate = "";
$paid = "Unpaid";
$totalcost = 0.0;
$amountPaid = 0.0;
$balance = 0.0;
$reqs = array();
$compReqs = array();
$myPrograms = array();


//RETRIEVE USER INFORMATION
$sql = "SELECT 
users.firstname,
users.lastname,
users.email
FROM lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
where lu_users_usertypes.id=".$_SESSION['userid'];
//echo $sql;
$result = mysql_query($sql) or die(mysql_error().$sql);

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	$firstname = $row['firstname'];
	$lastname = $row['lastname'];
	$email = $row['email'];
}




//GET REQUIREMENTS
$sql = "SELECT applicationreqs.id, applicationreqs.name, applicationreqs.linkname, application.name as appname, 
lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified,
lu_application_appreqs.completed
FROM `lu_application_programs`
inner join programs on programs.id = lu_application_programs.`program_id`
inner join degree on degree.id = programs.degree_id
inner join lu_degrees_applicationreqs on lu_degrees_applicationreqs.degree_id = programs.degree_id
inner join applicationreqs on applicationreqs.id = lu_degrees_applicationreqs.appreq_id
inner join application on application.id = lu_application_programs.application_id
left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
	and lu_application_appreqs.req_id = applicationreqs.id
where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['linkname']);	
	array_push($arr, $row['compreqid']);	
	array_push($arr, formatUSdate($row['last_modified']));	
	array_push($arr, $row['completed']);	
	array_push($compReqs, $arr);
}

$totalcost = totalCost($_SESSION['appid']);

//GET USERS SELECTED PROGRAMS
$sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname, choice, lu_application_programs.id as itemid, programs.programprice 
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice";
$result = mysql_query($sql) or die(mysql_error() . $sql);


while($row = mysql_fetch_array( $result )) 
{
	$dept = "";
	$depts = getDepartments($row[0]);
	if(count($depts) > 1)
	{
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$dept .= $depts[$i][1];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
			
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row['degreename']);
	array_push($arr, $row['fieldname'].$dept);
	array_push($arr, $row['choice']);
	array_push($arr, $row['itemid']);
	
	array_push($myPrograms, $arr);
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT 
	department.id,
	department.name
	FROM lu_programs_departments 
	
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}


if(isset($_POST['btnSubmit']))
{
	if($submitted == "Unsubmitted")
	{
		$sql = "update application set submitted=1, submitted_date='".date("Y-m-d h:i:s")."' where id=".$_SESSION['appid'];
		mysql_query($sql) or die(mysql_error());
		$submitted = "Submitted";
		$replyemail = "applyweb+@cs.cmu.edu";
		
		
		//GET MAIL TEMPLATE
		$str = "";
		$domain = 1;
		if($_SESSION['domainid'] > -1)
		{
			$domain = $_SESSION['domainid'];
		}
		$sql = "select content from content where name='Submission Letter' and domain_id=".$domain;
		$result = mysql_query($sql)	or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$str = $row["content"];
		}
		
		$degrees = "";
		for($i = 0; $i < count($myPrograms); $i++)
		{
			$degrees .= $myPrograms[$i][1] . " in ". $myPrograms[$i][2]."<br>";
		}
		
		$vars = array(	
		array('enddate',formatUSdate($_SESSION['expdate']) ),
		array('replyemail',$replyemail),
		array('lastname',$lastname),
		array('firstname',$firstname),
		array('email',$email),
		array('degrees',$degrees)
		);
		$str = parseEmailTemplate2($str, $vars );
		sendHtmlMail($replyemail, $email, "Application Submission to Carnegie Mellon University", $str);
	}else
	{
		$sql = "update application set submitted=0, submitted_date=NULL where id=".$_SESSION['appid'];
		mysql_query($sql) or die(mysql_error());
		$submitted = "Unsubmitted";
	}

}
$sql = "SELECT 
submitted, submitted_date, paid, paymentamount
FROM application
where id=".$_SESSION['appid'];
//echo $sql;
$result = mysql_query($sql) or die(mysql_error().$sql);

while($row = mysql_fetch_array( $result )) 
{
	if($row['submitted'] == "1")
	{
		$submitted = "Submitted";
	}
	if($row['paymentamount'] != "" && $row['paymentamount'] != 0.0)
	{
		$paid = "Paid";
	}
	$amountPaid = $row['paymentamount'];
	$submissionDate = $row['submitted_date'];
}

?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Submit Application </span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->Please review each section of your application 
                 before you submit it to our Admissions Committee. You may continue to 
                 make changes to your submitted application until the <? echo  formatUSDate3($_SESSION['expdate'])  ?> 
                 deadline. No changes will be allowed after this date.
<?               // Display message for total fee owed for all applications applied to
                 // no message if the fee is 0; effectively no applicaiton fee required.
                 if ($totalcost > 0)
                 {
?>
                   Once you submit your application, you will be prompted for payment of your application fee of $<?= $totalcost ?>.
<?
                 } else {
                  $paid="No Application Fee Required";
                 }
?>
                 <BR><BR>
                 <strong>By selecting the Submit Application Now button, you are also verifying that the information and supporting 
                         documents submitted in this application are accurate, authentic, and completed to the best of your ability.  
                         If the information provided has been falsified in any manner, then your application will be disqualified.
                 </strong>

            <span class="tblItem"><br>
            </span>
            <?
			if(count($compReqs) == 0)
			{
				$appcomp = false;
			}
			for($i = 0; $i < count($compReqs); $i++)
			{
				if($compReqs[$i][5]  != 1)
				{
					$appcomp = false;
					break;
				}
			}
			
			?>
			<span class="tblItem"><br>
			<? if($appcomp == true){?>
            <strong>Submission Status:</strong> <em>
			<?
			
			echo $submitted;
			if($submissionDate != "" && $submissionDate != "0000-00-00 00:00:00")
			{
				echo " on " . formatUSdate($submissionDate,'-','/');
			}
			}//end if appcomp
			?>
                        </em>
			<br>
			<? if($appcomp == false){ ?>
			<br>
            </span><span class="subtitle"><strong>You cannot submit your application because you have not completed the following steps</strong>:</span>
			<span class="tblItem">
			<div id="List">
			  <ul>
			  	<? for($i = 0; $i < count($compReqs); $i++){ 
					if($compReqs[$i][5] != 1){?>
	    		<li><a href="<?=$compReqs[$i][2]?>"><em><?=$compReqs[$i][1]?></em></a></li>
		        <? }//end if
				}//end for ?>
			  </ul>
			</div>
			<? }else 
			{
				$txt = "Submit";
				if($submitted == "Submitted")
				{
					//$txt = "Unsubmit";
				}else
				{
					showEditText($txt." Application Now", "button", "btnSubmit", $_SESSION['allow_edit']);
				}
				echo "<br>";
			}//end if 


			if($submitted == "Submitted" && $appcomp == 1)
			{
			?>
			<br>
			</em><strong>Application Fee Status: </strong><em><?=$paid?> </em>
			<? 
                        // This should be 0 when there is no application fee for a program
                        // i.e. totalcost = 0; amountPaid = 0, there fore balance = 0;
                        // it should not prompt with payment button 
			$balance = $totalcost - $amountPaid;
			if($balance > 0){ ?>
			</span><span class="tblItem">
			<div id="List">
			  <ul>
			    <li>Your application fee is $<?=$balance?>. 
			      <input class="tblItem" name="btnPay" type="button" id="btnBack" value="Pay Application Fee Now" onClick="document.location='pay.php'">
		        .</li>
		        <li></li>
			    <li><strong>Note:</strong> Your application will not be considered unless the required fee is paid in full.
			    Please allow 2-3 business days for your credit card payment to be recorded as <em>Paid</em> on your submitted application. </li>
			  </ul>
			</div>
			<? }//end if balance 
			}//end if submitted?>
			</span><span class="tblItem"><br>
            <br> 
            </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
