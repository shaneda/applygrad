
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
if(!isset($_POST['btnSave']))
{
   if ($SUPPINFO_REQUIRED[$domainid] == false ) {
	updateReqComplete("suppinfo.php", 1, 1, 0);
   } else {
     $myPrograms = get_user_selected_programs();
     /* 
        if the program is NOT required, then go ahead and
        save i.e. requirements complete.
     */
     if( marray_search( 39, $myPrograms) == false ){ 
	updateReqComplete("suppinfo.php", 1, 1, 0);
     }
   }

}


$err = "";
$myPrograms = array();
$pubStatus = array(array('In Progress', 'In Progress'), array('Submitted','Submitted'), array('Accepted','Accepted'),
	array('Not Accepted','Not Accepted'), array('No Plans to Submit','No Plans to Submit'));
$fellowshipStatus = array(array('Applied For', 'Applied For'), array('Awarded','Awarded'));
$referrals = array(
array("Website","Website"),
array("Alumni","Alumni"),
array("Friend","Friend"),
array("Current Student","Current Student"),
array("Former Faculty","Former Faculty"),
array("Former Faculty/Educational Institution","Former Faculty/Educational Institution"),
array("Advertisement","Advertisement-Please explain"),
array("Other","Other-Please explain")
);
$arrCrossDeptProgs = array(
array("0","Biological Sciences at CMU"),
array("1","Biomedical Engineering at CMU"),
array("2","Computer Science at CMU"),
array("3","Machine Learning at CMU"),
array("4","Psychology at CMU"),
array("5","Robotics at CMU"),
array("6","Statistics at CMU"),

array("7","Bioengineering at Pitt"),
array("8","Center for Neuroscience at Pitt"),
array("9","Mathematics at Pitt"),
array("10","Psychology at Pitt"),
array("11","Other")
);
$crossDeptProgs = array();
$crossDeptProgsOther = "";

$CrossDeptProgList = false;
$RecPermission = false;

$pubs = array();
$fellows = array();
$advisors = array();//FACULTY DESIGNATED AS ADVISORS
$advisor1 = "";
$advisor2 = "";
$advisor3 = "";
$advisor4 = "";
$expResearch = "";
$expInd = "";
$exFileId = "";
$exFileDate = "";
$exFileSize = "";
$exFileExt = "";
$pier = 0;
$wFellow = 0;
$port = "NULL";
$portLink = "";
$prog = "";
$design = "";
$stats = "";
$enrolled = 0;
$referral = "";
$honors = "";
$otherUni = "";
$tmpCrossDeptProgsId = "";
$permission= 0;

//GET ADVISORS
$sql = "select lu_users_usertypes.id as uid, concat(users.lastname , ', ' , users.firstname) as name,
users_info.additionalinfo
from lu_users_usertypes
left outer join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id 
where lu_users_usertypes.usertype_id=8 and lu_domain_department.domain_id=".$_SESSION['domainid']." group by users.id
order by lastname, firstname";
$result = mysql_query($sql) or die(mysql_error());
//echo $sql;
$tmpTitle = "";
$title = "";
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	/*
	$tmpTitle = $row['additionalinfo'];
	if($tmpTitle == $title)
	{
		array_push($arr, $row['uid']);
		array_push($arr, $row['name']);
	}else
	{
		array_push($arr, -1);
		array_push($arr, "-- ".$row['additionalinfo']." --");
		$title = $tmpTitle;
	}
	*/
	array_push($arr, $row['uid']);
	array_push($arr, $row['name']);
	

	array_push($advisors, $arr);
}




//HANDLE DELETIONS
if( isset( $_POST['txtPost'] ) && !isset($_POST['btnAddPub']) && !isset($_POST['btnAddFellow']) )
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelPub') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];

			$sql = "DELETE FROM publication WHERE application_id=".$_SESSION['appid']. " and id=" . $itemId;
			mysql_query($sql) or die(mysql_error());
		}
		if( strstr($key, 'btnDelFellow') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];

			$sql = "DELETE FROM fellowships WHERE application_id=".$_SESSION['appid']. " and id=" . $itemId;
			mysql_query($sql) or die(mysql_error());
		}
	}//END FOR
	saveData();
}//END DELETIONS


if(isset($_POST['btnAddPub']))
{
	saveData();
	$sql = "insert into publication(application_id)values(".$_SESSION['appid'].")";
	$result = mysql_query($sql) or die(mysql_error());
}//END ADD PUB
if(isset($_POST['btnAddFellow']))
{
	saveData();
	$sql = "insert into fellowships(application_id)values(".$_SESSION['appid'].")";
	$result = mysql_query($sql) or die(mysql_error());
}//END ADD FELLOW

if(isset($_POST['btnSave']))
{
	saveData();
}
function saveData()
{
	/*
	foreach($_POST as $key=>$val)
	{
		echo "key: ".$key . " val: ".$val."<br>";
	}
	*/
global $err;
global $myPrograms;
global $pubStatus;
global $fellowshipStatus;
global $referrals;
global $pubs;
global $fellows;
global $advisors;//FACULTY DESIGNATED AS ADVISORS
global $advisor1;
global $advisor2;
global $advisor3;
global $advisor4;
global $expResearch;
global $expInd;
global $exFileId;
global $exFileDate;
global $exFileSize;
global $exFileExt;
global $pier;
global $wFellow;
global $port;
global $portLink;
global $prog;
global $design;
global $stats;
global $enrolled;
global $referral;
global $honors;
global $otherUni;
global $arrCrossDeptProgs;
global $crossDeptProgs;
global $permission;
global $CrossDeptProgList;
global $RecPermission ;
global $crossDeptProgsOther;
global $tmpCrossDeptProgsId;
       $tmpCrossDeptProgsId = "";
global $tmpCrossDeptProgs;
       $tmpCrossDeptProgs = "";
	
	//VERIFY DATA
	$arrPubId = array();
	$arrFellowId = array();
	$tmpType="";
	$itemId = -1;
	$vars = $_POST;
	foreach($vars as $key => $value)
	{
		//echo $key ." ". $value."<br>";
		$arr = split("_", $key);
		if($arr[0] == "txtPub" || $arr[0] == "txtFellow")
		{
			$tmpid = $arr[1];
			if($itemId != $tmpid || $tmpType != $arr[0])
			{
				$itemId = $tmpid;
				$tmpType=$arr[0];
				
				if($arr[0] == "txtPub")
				{
					array_push($arrPubId,$itemId);
				}else{
					array_push($arrFellowId,$itemId);
				}
			}
		}
		if( strstr($key, 'chkCrossDeptProgs') !== false )
		{
			$k = $arr[1];
			$arr = array();

                        /* There are cross department & programs chosen */
                        $CrossDeptProgList = true;

			array_push($arr, $arrCrossDeptProgs[$k][0]);
			array_push($arr, $arrCrossDeptProgs[$k][1]);
			array_push($crossDeptProgs, $arr);
			/*
			if($tmpCrossDeptProgs != "")
			{
				$tmpCrossDeptProgs .= ",";
				$tmpCrossDeptProgsId .= ",";
			}
			$tmpCrossDeptProgsId .= $arrCrossDeptProgs[$k][0];
			$tmpCrossDeptProgs .= $arrCrossDeptProgs[$k][1];
			*/
		}
	}//END FOR each key
	if($tmpCrossDeptProgs != "")
	{
		//$crossDeptProgs = $tmpCrossDeptProgs;
                $CrossDeptProgList = true;

	}
	//ITERATE THROUGH IDS AND DO UPDATES
	for($i = 0; $i < count($arrPubId); $i++)
	{
		$title= htmlspecialchars($_POST["txtTitle_".$arrPubId[$i]]);
		$author= $_POST["txtAuthor_".$arrPubId[$i]];
		$forum= $_POST["txtForum_".$arrPubId[$i]];
		$citation= $_POST["txtCitation_".$arrPubId[$i]];
		$url= $_POST["txtUrl_".$arrPubId[$i]];
		$status= htmlspecialchars($_POST["lbPubStatus_".$arrPubId[$i]]);

		if($title == "")
		{
			$err .= "ERROR: Title ". ($i+1) ." is required.<br>";
		}
		if($author == "")
		{
			$err .= "ERROR: Author ". ($i+1) ." is required.<br>";
		}

		if($err == "")
		{
			$sql = "update publication set
			title= '".$title."',
			author= '".$author."',
			forum= '".$forum."',
			citation= '".$citation."',
			url= '".$url."',
			status= '".$status."'
			where id = ".$arrPubId[$i]." and application_id=".$_SESSION['appid'];
			mysql_query($sql) or die(mysql_error());
		}
	}//END PUBS UPDATE

	for($i = 0; $i < count($arrFellowId); $i++)
	{
		$name= htmlspecialchars($_POST["txtName_".$arrFellowId[$i]]);
		$amount= $_POST["txtAmount_".$arrFellowId[$i]];
		$status= htmlspecialchars($_POST["lbFellowStatus_".$arrFellowId[$i]]);
		$awDate = htmlspecialchars($_POST["txtAwDate_".$arrFellowId[$i]]);
		$appDate = htmlspecialchars($_POST["txtAppDate_".$arrFellowId[$i]]);
		$duration = intval($_POST["txtfellowLen_".$arrFellowId[$i]]);

		if($name == "")
		{
			$err .= "ERROR: Name ". ($i+1) ." is required.<br>";
		}
		if($status == "")
		{
			//$err .= "ERROR: Status ". ($i+1) ." is required.<br>";
		}
		if($appDate != "" && $appDate != "00/0000" && check_Date2($appDate) === false)
		{
			$err .= "ERROR: Application date is invalid.<br>";
		}
		if($awDate != "" && $awDate != "00/0000" && check_Date2($awDate) === false)
		{
			$err .= "ERROR: Award date is invalid.<br>";
		}

		if($err == "")
		{ }
			$sql = "update fellowships set
			name= '".$name."',
			amount= '".$amount."',
			status= '".$status."',
			duration=".$duration. " ";
			if($appDate != "" && $appDate != "00/0000" && check_Date2($appDate) !== false)
			{
				$sql .= ", applied_date = '".formatMySQLdate2($appDate,'/','-')."' ";
			}
			if($awDate != "" && $awDate != "00/0000" && check_Date2($awDate) !== false)
			{
				$sql .= ", award_date='".formatMySQLdate2($awDate,'/','-')."' ";
			}
			$sql .= " where id = ".$arrFellowId[$i]." and application_id=".$_SESSION['appid'];
			//echo $sql."<br>";
			mysql_query($sql) or die(mysql_error());
		
	}//END FELLOWSHIPS UPDATE

	$wFellow = 0;
	$pier = 0;

	if(isset($_POST['chkWomen']))
	{
		$wFellow = 1;
	}
	if(isset($_POST['chkPier']))
	{
		$pier = 1;
	}
	if(isset($_POST['chkPort']))
	{
		$port = 1;
	}
	if(isset($_POST['txtPort']))
	{
		$portLink = htmlspecialchars(addslashes($_POST['txtPort']));
	}
	if(isset($_POST['txtProg']))
	{
		$prog = htmlspecialchars($_POST['txtProg']);
	}
	if(isset($_POST['txtDesign']))
	{
		$design = htmlspecialchars($_POST['txtDesign']);
	}
	if(isset($_POST['txtStats']))
	{
		$stats = htmlspecialchars($_POST['txtStats']);
	}
	if(isset($_POST['lbReferral']))
	{
		if($_POST['lbReferral'] != "Other-Please explain" && $_POST['lbReferral'] != "" && $_POST['lbReferral'] != "Other")
		{
			$referral = addslashes($_POST['lbReferral']);
		}else
		{	
			if(isset( $_POST['txtReferral']))
			{
				$referral = addslashes(substr ( $_POST['txtReferral'], 0 ,255 ) );
			}
		}
	}
	else
	{
		if(isset( $_POST['txtReferral']))
		{
			$referral = addslashes(substr ( $_POST['txtReferral'], 0 ,255 ) );
		}
	}
	
	if(isset( $_POST['lbEnrolled']))
	{
		$enrolled = intval($_POST['lbEnrolled']);
	}
	
	if(isset( $_POST['txtHonors']))
	{
		$honors = addslashes($_POST['txtHonors']);
	}
	
	if(isset($_POST['txtOtherUni']))
	{
		$otherUni = htmlspecialchars(addslashes($_POST['txtOtherUni']));
	}
	if(isset($_POST['chkPermission']))
	{
		$permission = 1;
                /* permission to get records has been given by user */
                $RecPermission = true;

	}

	if(isset($_POST['txtCrossDeptProgs']))
	{

		$crossDeptProgsOther = addslashes(substr ( $_POST['txtCrossDeptProgs'], 0 ,50 ) );
		//echo $crossDeptProgsOther;
	}
	
	
	if($_SESSION['domainname'] == "MCHI")
	{
		if($prog == "")
		{
			$err .= "ERROR: Programming knowledge is required<br>";
		}
		if($design == "")
		{
			$err .= "ERROR: Design knowledge is required<br>";
		}
		if($stats == "")
		{
			$err .= "ERROR: Statistics knowledge is required<br>";
		}
	}
	
	$progs = "";
	for($i = 0; $i < count($crossDeptProgs); $i++)
	{
	
		if($i > 0 && $i < count($crossDeptProgs))
		{
			$progs .= ",";
		}
		$progs .= $crossDeptProgs[$i][1];
	}
	$sql = "update application set
			womenfellowship= ".$wFellow.",
			pier = ".$pier.",
			portfoliosubmitted = ".$port.",
			portfolio_link = '".$portLink."',
			area1 = '".$prog."',
			area2 = '".$design."',
			area3 = '".$stats."',
			referral_to_program = '".$referral."',
			other_inst = '".$otherUni."',
			cross_dept_progs = '".$progs."',
			cross_dept_progs_other = '".$crossDeptProgsOther."',
			records_permission = ".$permission.",
			cur_enrolled = ".$enrolled.",
			honors = '".$honors."'
			where id=".$_SESSION['appid'];
			mysql_query($sql) or die(mysql_error().$sql);

	$sql = "delete from lu_application_advisor where application_id=".$_SESSION['appid'];
	mysql_query($sql) or die(mysql_error());

	if(isset($_POST['lbAdvisor_1']))
	{
		$advisor1 = intval($_POST['lbAdvisor_1']);
		$type = intval($_POST['txtAdvisorType_1']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor1.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['lbAdvisor_2']))
	{
		$advisor2 = intval($_POST['lbAdvisor_2']);
		$type = intval($_POST['txtAdvisorType_2']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor2.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['lbAdvisor_3']))
	{
		$advisor3 = intval($_POST['lbAdvisor_3']);
		$type = intval($_POST['txtAdvisorType_3']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor3.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['lbAdvisor_4']))
	{
		$advisor4 = stripslashes($_POST['lbAdvisor_4']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor4.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	
	//HANDLE ADVISOR TEXT BOXES
	if(isset($_POST['txtAdvisor_1']))
	{
		$advisor1 = stripslashes($_POST['txtAdvisor_1']);
		$type = intval($_POST['txtAdvisorType_1']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, name, advisor_type)values(".$_SESSION['appid'].", null, '".$advisor1."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['txtAdvisor_2']))
	{
		$advisor2 = stripslashes($_POST['txtAdvisor_2']);
		$type = intval($_POST['txtAdvisorType_2']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id,  name, advisor_type)values(".$_SESSION['appid'].",null, '".$advisor2."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['txtAdvisor_3']))
	{
		$advisor3 = stripslashes($_POST['txtAdvisor_3']);
		$type = intval($_POST['txtAdvisorType_3']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id,  name, advisor_type)values(".$_SESSION['appid'].",null, '".$advisor3."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['txtAdvisor_4']))
	{
		$advisor4 = stripslashes($_POST['txtAdvisor_4']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id,  name, advisor_type)values(".$_SESSION['appid'].",null, '".$advisor4."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	
	
	//EXPERIENCE
	/*
	if(isset($_POST['txtExpResearch']))
	{
		$expResearch = htmlspecialchars($_POST['txtExpResearch']);
	}
	if(isset($_POST['txtExpInd']))
	{
		$expInd = htmlspecialchars($_POST['txtExpInd']);
	}
	*/
	$doInsert = true;
	$sql = "select id from experience where application_id = ".$_SESSION['appid'];
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$doInsert = false;
	}
	if($doInsert == true)
	{
		$sql = "insert into experience(application_id)values(".$_SESSION['appid'].")";
		$result = mysql_query($sql) or die(mysql_error());
	}else{
		/*
		$sql = "update experience set
		research='".$expResearch."',
		industry='".$expInd."'
		where application_id=".$_SESSION['appid'];
		$result = mysql_query($sql) or die(mysql_error());
		*/
	}
	
	if(isset($_FILES["file"]))
	{
		if($_FILES["file"]["size"] > 0)
		{
			$ret = handle_upload_file(5, $_SESSION['userid'],$_SESSION['usermasterid'], &$_FILES["file"],$_SESSION['appid']."_1" );
			echo $ret;
	
			if(intval($ret) < 1)
			{
				$err .= $ret."<br>";
			}else
			{
				$sql = "update experience set datafile_id =".intval($ret)." where application_id = ".$_SESSION['appid'];
				$result = mysql_query($sql) or die(mysql_error());
	
			}
		}
	}
	if($err == "")
	{
		updateReqComplete("suppinfo.php", 1);


               /* This is ID for CNBC Graduate Training Program  */
               /* The applicants to this program are required to */
               /* give permission to obtain supprting docs       */
               /* if not given, the application is NOT complete  */

                $myPrograms = get_user_selected_programs();
                
		if( marray_search( 39, $myPrograms) != false ){ 
                  /* program applied too provided? */
                  /* permission to get records given? */
                  if ($CrossDeptProgList == false || $RecPermission == false) {
                    /* reset supplemental section to incomplete */
 		    updateReqComplete("suppinfo.php", 0);
                  }
                }

		//header("Location: home.php");
	}


}//END SAVE

//GET USER INFO

$sql = "select
id,
title,
author,
forum,
citation,
url,
status
from publication where application_id=".$_SESSION['appid'] . " order by id";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['title']);
	array_push($arr, $row['author']);
	array_push($arr, $row['forum']);
	array_push($arr, $row['citation']);
	array_push($arr, $row['url']);
	array_push($arr, $row['status']);
	array_push($pubs, $arr);
}
$sql = "select id,
name,
amount,
status,
applied_date,
award_date,
duration
from fellowships where application_id=".$_SESSION['appid'];
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['amount']);
	array_push($arr, $row['status']);
	array_push($arr, formatUSdate2($row['applied_date'],'-','/'));
	array_push($arr, formatUSdate2($row['award_date'],'-','/'));
	array_push($arr, $row['duration']);
	array_push($fellows, $arr);
}

$sql = "select pier, womenfellowship, portfoliosubmitted,portfolio_link, area1,area2,area3,
referral_to_program, other_inst, cross_dept_progs,cross_dept_progs_other, records_permission, 
cur_enrolled, honors
from application where id=".$_SESSION['appid'];
$result = mysql_query($sql) or die(mysql_error());
$tmpProgs = "";
while($row = mysql_fetch_array( $result ))
{
	$wFellow = $row['womenfellowship'];
	$pier = $row['pier'];
	$port = $row['portfoliosubmitted'];
	$prog = $row['area1'];
	$design = $row['area2'];
	$stats = $row['area3'];
	$portLink = stripslashes($row['portfolio_link']);
	$referral = stripslashes($row['referral_to_program']);
	$otherUni = stripslashes($row['other_inst']);
	$tmpProgs = $row['cross_dept_progs'];
	$crossDeptProgsOther = $row['cross_dept_progs_other'];
	$permission = $row['records_permission'];
	$enrolled = $row['cur_enrolled'];
	$honors = stripslashes($row['honors']);
	
}
if($tmpProgs != "")
{
	$crossDeptProgs = explode(",",$tmpProgs);
}
for($i = 0; $i < count($crossDeptProgs); $i++)
{
	$crossDeptProgs[$i] = marray_search( $crossDeptProgs[$i], $arrCrossDeptProgs);
}

$sql = "select advisor_user_id, name from lu_application_advisor where application_id=".$_SESSION['appid'] . " order by id" ;
$result = mysql_query($sql) or die(mysql_error());
$myAdvisors = array();
$k=0;
while($row = mysql_fetch_array( $result ))
{
	if($k == 0)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor1 = $row['name'];
		}else
		{
			$advisor1 = $row['advisor_user_id'];
		}
	}
	if($k == 1)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor2 = $row['name'];
		}else
		{
			$advisor2 = $row['advisor_user_id'];
		}
	}
	if($k == 2)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor3 = $row['name'];
		}else
		{
			$advisor3 = $row['advisor_user_id'];
		}
	}
	if($k == 3)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor4 = $row['name'];
		}else
		{
			$advisor4 = $row['advisor_user_id'];
		}
	}
	$k++;
}

function getExperience($type=1)
{
	$ret = array();
	$sql = "select datafile_id,experiencetype, moddate,size,extension from experience
	inner join datafileinfo on datafileinfo.id =  experience.datafile_id
	where application_id=".$_SESSION['appid']." and experiencetype=".$type;
	$result = mysql_query($sql) or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['experiencetype']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($ret, $arr);
	}
	return $ret;
}


function get_user_selected_programs()
{
//get user-selected programs
//GET USERS SELECTED PROGRAMS

  $ret = array();

  $sql = "SELECT programs.id, domain.name
  FROM lu_application_programs 
  inner join programs on programs.id = lu_application_programs.program_id
  inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
  inner join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
  inner join domain on domain.id = lu_domain_department.domain_id
  where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice";
  $result = mysql_query($sql) or die(mysql_error() . $sql);


  while($row = mysql_fetch_array( $result )) 
  {
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row['name']);
	array_push($ret, $arr);
  }

  return $ret;

} // END get_user_selected_programs

$myPrograms = get_user_selected_programs();

?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="../logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="../index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>
			<!-- InstanceBeginEditable name="EditNav" -->
			
				
			
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Supplemental Information</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->

			
		
<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
$sql = "select content from content where name='Supplemental Information' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?><br>
<span class="errorSubtitle"><?=$err?></span><br>
<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
				<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
			</div> = required<br>
<br>


            <? showEditText("Save Supplemental Info", "button", "btnSave", $_SESSION['allow_edit']); ?>
			<input name="txtPost" type="hidden" id="txtPost" value="1">
            <br>
            <br>
            <hr size="1" noshade color="#990000">

            <span class="subTitle">Publications</span><br>
            <br>
            List relevant publications, including journal or conference papers (submitted, accepted, or published), thesis, or technical reports. If you would like for the admissions committee to have access to a copy of the paper, please provide the URL.<br> 
<BR>
            <? showEditText("Add Publication", "button", "btnAddPub", $_SESSION['allow_edit']); ?>
            <? for($i = 0; $i < count($pubs); $i++){ ?>
			<input name="txtPub_<?=$pubs[$i][0];?>" type="hidden" id="txtPub_<?=$pubs[$i][0];?>" value="Pub">

            <hr size="1">
            <table width="650" border=0 cellpadding=2 cellspacing=2 class="tblItem">
              <tr>
                <td valign="top"><?=$i+1?></td>
                <td valign="top"><p>Author(s):<br>
                  <? showEditText($pubs[$i][2], "textbox", "txtAuthor_".$pubs[$i][0], $_SESSION['allow_edit'], true,null,true,30); ?>
                  <br>
                </p></td>
                <td valign="top"><p>Title:<br>
                  <? showEditText($pubs[$i][1], "textbox", "txtTitle_".$pubs[$i][0], $_SESSION['allow_edit'], true,null,true,20); ?>
                  <br>
                </td>
                <td valign="top">Name of Journal or Conference:<br>
                <? showEditText($pubs[$i][3], "textbox", "txtForum_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,20); ?></td>
                <td rowspan="2" valign="top" align="right"><? showEditText("Delete", "button", "btnDelPub_".$pubs[$i][0], $_SESSION['allow_edit']); ?></td>
              </tr>
              <tr>
                <td valign="top">&nbsp;</td>
                <td valign="top"><p>Citation: (vol., pp., year)<br>
                    <? showEditText($pubs[$i][4], "textbox", "txtCitation_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,30); ?>
            
                </td>
                <td valign="top"><p>URL (If available):<br>
                    <? showEditText($pubs[$i][5], "textbox", "txtUrl_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,20); ?>
                </td>
                <td valign="top">Status:<br>
                <? showEditText($pubs[$i][6], "listbox", "lbPubStatus_".$pubs[$i][0], $_SESSION['allow_edit'],false, $pubStatus); ?></td>
              </tr>
            </table>

			<? }//END PUBS LOOP ?>

            <hr size="1" noshade color="#990000">
            <span class="subtitle">Fellowships</span><br>
            
            <br>
            List only those fellowships which you are applying for or have been awarded for your doctoral studies. <br>
            Do not list fellowships that were awarded for undergraduate studies.<br>
            <BR>
			<? showEditText("Add Fellowship", "button", "btnAddFellow", $_SESSION['allow_edit']); ?><br> 
            
            <? for($i = 0; $i < count($fellows); $i++){ ?>
            <input name="txtFellow_<?=$fellows[$i][0];?>" type="hidden" id="txtFellow_<?=$fellows[$i][0];?>" value="Fellow">
            <hr size="1">
			<!--BEGIN FELLOWSHIPS TABLE-->
            <table width="650" border=0 cellpadding=2 cellspacing=2 class="tblItem">
              <tr>
                <td valign="top"><?=$i+1?></td>
                <td valign="top"><p>Fellowship Name:<br>
                  <? showEditText($fellows[$i][1], "textbox", "txtName_".$fellows[$i][0], $_SESSION['allow_edit'],true,null,true,30); ?>
                  <br>
                </td>
                <td valign="bottom">$</td>
                <td valign="top"><p>Amount/Year (If available):<br>
        		<? showEditText($fellows[$i][2], "textbox", "txtAmount_".$fellows[$i][0], $_SESSION['allow_edit']); ?>
                </td>
                  <td valign="top">Duration of Award (years): <br>
					<? showEditText($fellows[$i][6], "textbox", "txtfellowLen_".$fellows[$i][0], $_SESSION['allow_edit']); ?>                  </td>
                <td rowspan="2" valign="top" align="right"><? showEditText("Delete", "button", "btnDelFellow_".$fellows[$i][0], $_SESSION['allow_edit']); ?></td>
              </tr>
              <tr>
                <td valign="top">&nbsp;</td>
                <td valign="top"><p>Status:<br>
                  <? showEditText($fellows[$i][3], "listbox", "lbFellowStatus_".$fellows[$i][0], $_SESSION['allow_edit'],false, $fellowshipStatus); ?></td>
                <td valign="top">&nbsp;</td>
                <td valign="top">Applied Date <font size="1">(MM/YYYY)</font>: <br>
                  <? showEditText($fellows[$i][4], "textbox", "txtAppDate_".$fellows[$i][0], $_SESSION['allow_edit']); ?></td>
                  <td valign="top">Award Date <font size="1">(MM/YYYY)</font>: <br>
                  <? showEditText($fellows[$i][5], "textbox", "txtAwDate_".$fellows[$i][0], $_SESSION['allow_edit']); ?></td>
              </tr>
            </table>
			<!-- END FELLOWSHIPS TABLE-->
			<? } //END FELLOWS LOOP ?>
			<hr size="1" noshade color="#990000">
            <br>
			<? if($_SESSION['domainname'] == "SCS" 
			|| marray_search( "SCS", $myPrograms) != false 
			|| $_SESSION['domainname'] == "RI"
			|| marray_search( "RI", $myPrograms) != false 
			){ ?>
            <span class="subtitle">Barbara Lazarus Women@IT Fellowship</span><br>
            <br>The Barbara Lazarus Women@IT Fellowships are aimed primarily at students with 
                nontraditional backgrounds to provide an initial buffer year that includes a tailored 
                menu of courses and appropriate advising. 
                While the Fellowship is targeted to those whose background may not be in computer science, applicants 
                with computer science backgrounds may also be considered.
                For more information on the fellowship see: <a href="http://women.cs.cmu.edu/" target="_blank">http://women.cs.cmu.edu/</a>.
            <br><br>
            Would you like to be considered for a Barbara Lazarus Women@IT Fellowship?
            <br><br>
            <? showEditText($wFellow, "checkbox", "chkWomen", $_SESSION['allow_edit'],false); 
			if($_SESSION['allow_edit'] == true)
			{
				echo "Check if yes";
			}
			?>
			
            <br>
            <hr size="1" noshade color="#990000">
           <br>

            <span class="subtitle">Program in Interdisciplinary Educational Research (PIER)<br>
            </span><br>
            PIER is a new CMU program to train scientists to do rigorous research needed for 
            evidence-based educational practice and policy. Note that a Supplementary 
            Application for Admission is required. Further information can be found at
            <a href="http://www.cmu.edu/pier/" target="_blank">http://www.cmu.edu/pier/</a>. 
            <br><br>
            Do you plan to apply to the Program in Interdisciplinary Educational Research (PIER)?            
            <br><br>            
            <? showEditText($pier, "checkbox", "chkPier", $_SESSION['allow_edit'],false); 
			if($_SESSION['allow_edit'] == true)
			{
				echo "Check if yes";
			}?>

            <br>
            <hr size="1" noshade color="#990000">
           <br>

            <span class="subtitle">Portfolio <br>
            </span><br>
            <strong>Human Computer Interaction (HCI)</strong> applicants: 
            If appropriate to your background you may include a link to your portfolio. 
            If you are unable to provide a link, mail a CD of your portfolio to Martha Clarke, Admissions Coordinator. Include your User ID on all materials submitted. Further information can be found at 
            <a href="http://www.hcii.cs.cmu.edu/Academics/PhD/Applying/howtoapply.html" target="_blank">
                     http://www.hcii.cs.cmu.edu/Academics/PhD/Applying/howtoapply.html</a>

            <br>
            <br>
            Do you plan to submit a portfolio?<br>            
            <br>
            <? showEditText($port, "checkbox", "chkPort", $_SESSION['allow_edit'],false); 
			if($_SESSION['allow_edit'] == true)
			{
				echo "Check if yes";
			}?>
            <br>
            <br>
            Please provide a link to your portfolio (optional)<br>
            <? showEditText($portLink, "textbox", "txtPort", $_SESSION['allow_edit'],false, null, true, 40); ?>
            <br>

	   <? }//end scs ?>




            <? if($_SESSION['domainname'] == "SCS" 
			|| marray_search( "SCS", $myPrograms) != false  
			|| $_SESSION['domainname'] == "RI"
			|| marray_search( "RI", $myPrograms) != false ){ ?>
            <hr size="1" noshade color="#990000">			
			<span class="subtitle">Experience</span>
			<br>
            <br>
            Only for <strong>Software Engineering</strong> and/or 
            <strong>Computation, Organizations and Society</strong> Applicants.
            Details related to your Experience must be submitted in PDF, MS Word or text format. 
            Any other format will not be accepted. The maximum file size is 3MB.
            <br>
            <br>
            Research Experience<br>
            <? $qs = "?id=1&t=5"; ?>
			<input class="tblItem" name="btnUpload" value="Upload Research Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
			<?
			$ex = getExperience(1);
			for($i = 0; $i < count($ex); $i++)
			{
				
				showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
			}?>
			<br>
                       <hr size="1">
                       
            Industry / Government Experience
            <br>
			<? $qs = "?id=2&t=5"; ?>
			<input class="tblItem" name="btnUpload" value="Upload Industry / Government Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
			<?
			$ex = getExperience(2);
			for($i = 0; $i < count($ex); $i++)
			{
				showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 2, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
			}?>
           <br>
			<? }//end scs ?>

			<? if($_SESSION['domainname'] == "MSE-Dist" 
			|| marray_search( "MSE-Dist", $myPrograms) != false 
			|| $_SESSION['domainname'] == "MSE-Campus"
			|| marray_search( "MSE-Campus", $myPrograms) != false 
			|| $_SESSION['domainname'] == "VLIS"
			|| marray_search( "VLIS", $myPrograms) != false ){ ?>
			<br>
<strong>Where did you learn about the Software Engineering @ Carnegie Mellon program?</strong><br>
		
			<? showEditText($referral, "listbox", "lbReferral", $_SESSION['allow_edit'],false, $referrals); ?><br>
			<br>
			<? showEditText($referral, "textbox", "txtReferral", $_SESSION['allow_edit'],false,null,true,35,35); ?>
			If Other, please explain: (35 characters max)<br>
			
			
			<br>
			<br>
			<strong>Are you currently enrolled in Carnegie Mellon's (CSE) Certificate in Software Engineering Program?</strong><br>
			<? 
			showEditText($enrolled, "listbox", "lbEnrolled", $_SESSION['allow_edit'],false, array(array(1,"Yes"), array(0,"No")) ); ?>
			
			
            <span class="subtitle"><br>
            <br>
            Professional Experience</span><br>
<!--        Only for Software Engineering Applicants and/or Computation, Organizations and Society Applicants. -->
            Details related to your Professional Experience must be submitted in PDF, MS Word or text format. 
            Any other format will not be accepted. The maximum file size is 3MB.
            <br>
            <? $qs = "?id=4&t=5"; ?>
			<input class="tblItem" name="btnUpload" value="Upload Professional Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
			<?
			$ex = getExperience(4);
			for($i = 0; $i < count($ex); $i++)
			{
				showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 4, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
			}
			?>
			<br>
			
			<span class="subtitle"><br>
			<br>
			Honors and Awards</span><br>
			<? showEditText($honors, "textarea", "txtHonors", $_SESSION['allow_edit'],false); ?>
			
			<? }//end MSE/VLIS

			?>
            </span>


		<? if($_SESSION['domainname'] == "CNBC" || marray_search( "CNBC", $myPrograms) != false ){ ?>
            <span class="subtitle"><u>For candidates applying to the Ph.D. Program in Neural Computation only</u></span><br><br>
            <span class="subtitle">Advisors</span><br>
            Please list the names of up to 4 potential research advisors you might want to work with if admitted. 
            For a list of PNC training faculty, please visit this page: 
            <a href="http://www.cnbc.cmu.edu/GradTrain/pnc_trainingfaculty.shtml" target="_blank">
                     http://www.cnbc.cmu.edu/GradTrain/pnc_trainingfaculty.shtml</a> . 
            This list is not exclusive; students may also work with faculty not listed here if the research is
            appropriate for the Program in Neural Computation. 
            <BR><BR>

            1. <? showEditText($advisor1, "textbox", "txtAdvisor_1", $_SESSION['allow_edit'],true, null, true, 30); ?>
			<? showEditText(2, "hidden", "txtAdvisorType_1", $_SESSION['allow_edit']); ?>
            <br>
            2.
            <? showEditText($advisor2, "textbox", "txtAdvisor_2", $_SESSION['allow_edit'],true,  null, true, 30); ?>
			<? showEditText(2, "hidden", "txtAdvisorType_2", $_SESSION['allow_edit']); ?>
            <br>
            3.
            <? showEditText($advisor3, "textbox", "txtAdvisor_3", $_SESSION['allow_edit'],true, null, true, 30); ?>
			<? showEditText(2, "hidden", "txtAdvisorType_3", $_SESSION['allow_edit']); ?>
			<br>
			4.
            <? showEditText($advisor4, "textbox", "txtAdvisor_4", $_SESSION['allow_edit'],true, null, true, 30); ?>
			<? showEditText(2, "hidden", "txtAdvisorType_4", $_SESSION['allow_edit']); ?>
            <br>
            <br>
			<span class="subtitle">Other Institutions/Programs</span>
			<br>
			Please tell us the names of other institutions/programs you are applying to in addition to 
                        Carnegie Mellon and the University of Pittsburgh.
			<br> 
			<? showEditText($otherUni, "textarea", "txtOtherUni", $_SESSION['allow_edit'], true); ?>
            <br><br>
            <hr size="1" noshade color="#990000"><BR>
            <span class="subtitle"><u>For candidates applying to the CNBC Graduate Training Program only</u></span><br><br>
            Please indicate the affiliated Ph.D. programs to which you have applied.<br>
            For information about these programs, click
            <a href="http://www.cnbc.cmu.edu/Affiliated" target=_blank>here</a>.

            <BR><BR>
            <? 
			/*
			if($tmpCrossDeptProgsId != "")
			{
				$crossDeptProgs = $tmpCrossDeptProgsId;
			}
			if(!is_array($crossDeptProgs))
			{
				$crossDeptProgs = explode(",",$crossDeptProgs);
			}
			*/
			showEditText($crossDeptProgs, "checklist", "chkCrossDeptProgs", $_SESSION['allow_edit'], false, $arrCrossDeptProgs); 
			?>
            <br>
            <? showEditText($crossDeptProgsOther, "textbox", "txtCrossDeptProgs", $_SESSION['allow_edit'],false,null,true,35,35); ?>
			If Other, please explain: (35 characters max)			
                        <br><br>
                        <span class="subtitle">Authorization</span>
			<br>
			<? showEditText($permission, "checkbox", "chkPermission", $_SESSION['allow_edit'], false); ?>
			
			I authorize the CNBC to obtain copies of my  transcripts, GRE scores, and letters of reference from the  affiliated PhD program(s) to which I am applying.
			<br>
         <br>
         <? }//end if cnbc ?>

	    <? if($_SESSION['domainname'] == "CompBio" || marray_search( "CompBio", $myPrograms) != false ){ ?>
            <span class="subtitle">Advisors</span><br>
            The following drop down menus contain the list of training faculty associated with this program at Carnegie Melon University and the University of Pittsburgh. If you are interested in working with a specific faculty member please select her / his name from the lists below. You may select up to three potential advisors.<br>

            1. <? showEditText($advisor1, "listbox", "lbAdvisor_1", $_SESSION['allow_edit'],true, $advisors); ?>
			<? showEditText(1, "hidden", "txtAdvisorType_1", $_SESSION['allow_edit']); ?>
            <br>
            2.
            <? showEditText($advisor2, "listbox", "lbAdvisor_2", $_SESSION['allow_edit'],true, $advisors); ?>
			<? showEditText(1, "hidden", "txtAdvisorType_2", $_SESSION['allow_edit']); ?>
            <br>
            3.
            <? showEditText($advisor3, "listbox", "lbAdvisor_3", $_SESSION['allow_edit'],true, $advisors); ?>
			<? showEditText(1, "hidden", "txtAdvisorType_3", $_SESSION['allow_edit']); ?>
            <br>
            <br>
         <? }//end if compbio ?>

			<? if($_SESSION['domainname'] == "HCII" || marray_search( "HCII", $myPrograms) != false ){ ?>
			<span class="subtitle">Portfolio</span><br>
			Have you submitted a portfolio?
			<? 
			$vars = array(array('0', 'No'), array('1','Yes'));
			showEditText($port, "listbox", "lbPort", $_SESSION['allow_edit'],true, $vars); 
			?>
			<br>
			<br>
			<? }//end if HCII ?>


			<? if($_SESSION['domainname'] == "MHCI" || marray_search( "MHCI", $myPrograms) != false ){ ?>
			<span class="subtitle">Additional Knowledge</span>
			<br>
			Please give a detailed description of your experience in the field of <strong>programming</strong>.<br> 
			<? showEditText($prog, "textarea", "txtProg", $_SESSION['allow_edit'], true); ?>
			<br>
			<br>
			Please give a detailed description of your experience in the field of <strong>design</strong>.<br>
            <? showEditText($design, "textarea", "txtDesign", $_SESSION['allow_edit'], true); ?>
            <br>
            <br>
            Please give a detailed description of your experience in the field of <strong>statistics</strong>.<br>
            <? showEditText($stats, "textarea", "txtstats", $_SESSION['allow_edit'], true); ?>
            <br>
			<? }//end if MHCI ?>
			
           
            <hr size="1" noshade color="#990000">
            <span class="subtitle">
            <? showEditText("Save Supplemental Info", "button", "btnSave", $_SESSION['allow_edit']); ?>
            </span><br>
            <!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			<!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="../contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
