
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? $exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";

if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/MHCI_style.css"/>
<script language="javascript" src="../inc/jquery-1.2.6.min.js"></script>
<script language="javascript" src="../inc/mhci_prereqs.js"></script>
<!-- InstanceBeginEditable name="head" -->
<?
/*
$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
$ethnicities = array();
$titles = array(array('Mr.', 'Mr.'), array('Ms.','Ms.'));
$err = "";
$mode = "edit";
$title = "";
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$ethnicity = -1;
$pass = "";
$passC = "";
$dob = "";
$visaStatus = -1;
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$street4 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$telMobile = "";
$telWork = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$streetP4 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";
$telPMobile = "";
$telPWork = "";
$countryCit = "";
$curResident = "NULL";

if(isset($_GET['mode']))
{
	if($_GET['mode'] != "")
	{
		$mode = $_GET['mode'];
	}
}



	// COUNTRIES
	$result = mysql_query("SELECT * FROM countries order by name") or die(mysql_error());
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($arr, $row['iso_code']);
		array_push($countries, $arr);
	}
	
	
	
	//STATES
	
	$result = mysql_query("SELECT * FROM states order by country_id,name")
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($states, $arr);
	}
	
	// STATII
	$result = mysql_query("SELECT * FROM visatypes order by short DESC") or die(mysql_error());
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name'] . " (".$row['short'].")");
		array_push($arr, $row['short']);
		array_push($statii, $arr);
	}
	
	// ETHNICITIES
	$result = mysql_query("SELECT * FROM ethnicity order by sortorder") or die(mysql_error());
	
	
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($ethnicities, $arr);
	}
	


if(isset($_POST['btnSubmit'] ))
{
	$sql = "";
	$visaStatus = intval($_POST['lbStatus']);
	
	$mode = $_POST['txtMode'];
	
	$fName = $_POST['txtFName'];
	$mName = $_POST['txtMName'];
	$lName = $_POST['txtLName'];
	$title = $_POST['txtTitle'];	
	$dob = $_POST['txtDob'];
	$email = $_POST['txtEmail'];
	$gender = $_POST['lbGender'];
	if(isset($_POST['lbEthnicity']))
	{
		$ethnicity = intval($_POST['lbEthnicity']);
	}
	if(isset($_POST['lbCurResident']))
	{
		$curResident = intval($_POST['lbCurResident']);
	}
	$pass = $_POST['txtPass'];
	$passC = $_POST['txtPassC'];
	$street1 = $_POST['txtStreet1'];
	$street2 = $_POST['txtStreet2'];
	$street3 = $_POST['txtStreet3'];
	$street4 = $_POST['txtStreet4'];
	$city = $_POST['txtCity'];
	$postal = $_POST['txtPostal'];
	$country = intval($_POST['lbCountry']);
	$state = intval($_POST['lbState']);
	$streetP1 = $_POST['txtStreetP1'];
	$streetP2 = $_POST['txtStreetP2'];
	$streetP3 = $_POST['txtStreetP3'];
	$streetP4 = $_POST['txtStreetP4'];
	$cityP = $_POST['txtCityP'];
	$postalP = $_POST['txtPostalP'];
	$countryP = intval($_POST['lbCountryP']);
	$stateP = intval($_POST['lbStateP']);
	$tel = $_POST['txtTel'];
	$telMobile = $_POST['txtTelMobile'];
	$telWork = $_POST['txtTelWork'];
	//$telP = $_POST['txtTelP']);
	//$telPMobile = $_POST['txtTelPMobile']);
	//$telPWork = $_POST['txtTelPWork']);
	$initals = substr($fName,0,1) . substr($_POST["txtMName"],0,1) . substr($lName,0,1);
	$homepage = $_POST['txtHomepage'];
	$countryCit = intval($_POST['lbCountryCit']);
	//VERIFY FORM VARS HERE
	if($fName == "")
	{
		$err .= "First Name is Required<br>";
	}
	if($lName == "")
	{
		$err .= "Last Name is Required<br>";
	}
	if(check_Date($dob) == false)
	{
		$err .= "Date of birth is invalid.<br>";
	}
	if($email == "")
	{
		$err .= "Email is required.<br>";
	}
	if($email != "" && check_email_address($email)===false )
	{
		$err .= "Email is invalid.<br>";
	}
	if($gender == "")
	{
		$err .= "Gender is required.<br>";
	}
	if($ethnicity == "" && $_SESSION['domainname'] != "CNBC")
	{
		$err .= "Ethnicity is required. (An option is provided to decline)<br>";
	}
	if($pass == "" && $_SESSION['usermasterid'] == -1)
	{
		$err .= "Password is required.<br>";
	}
	if($pass != $_POST['txtPassC'] && $_SESSION['usermasterid'] == -1)
	{
		$err .= "Passwords do not match.<br>";
	}
	if($visaStatus == "")
	{
		$err .= "Visa status is required.<br>";
	}
	//CURRENT ADDRESS VERIFY
	if($street1 == "")
	{
		$err .= "Street1 is required.<br>";
	}
	if($city == "")
	{
		$err .= "City is required.<br>";
	}
	if($postal == "")
	{
		$err .= "Postal code is required.<br>";
	}
	if($country == "")
	{
		$err .= "Country is required.<br>";
	}
	if($country == "231" || $country == "142" || $country == "39")
	{
		if($state == "")
		{
			$err .= "State is required.<br>";
		}
	}
	if($tel == "")
	{
		$err .= "Telephone is required.<br>";
	}
	//PERMANENT ADDRESS VERIFY
	if($streetP1 == "")
	{
		//$err .= "Permanent street1 is required.<br>";
	}
	if($cityP == "")
	{
		//$err .= "Permanent city is required.<br>";
	}
	if($postalP == "")
	{
		//$err .= "Permanent postal code is required.<br>";
	}
	if($countryP == "")
	{
		//$err .= "Permanent country is required.<br>";
	}
	if($countryP == "231" || $countryP == "142" || $countryP == "39")
	{
		if($stateP == "")
		{
			//$err .= "Permanent state is required.<br>";
		}
	}
	if($telP == "")
	{
		//$err .= "Permanent Telephone is required.<br>";
	}
	
	if($countryCit == "" && $_SESSION['domainname'] != "CNBC")
	{
		$err .= "Country of citizenship is required.<br>";
	}
	
	
	//DO INSERT/UPDATE
	if($email != "")
	{
		$sql = "select users.id from users 
		where id=".$_SESSION['usermasterid'];
		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$_SESSION['usermasterid'] = $row['id'];
		}
		if($_SESSION['usermasterid'] == -1)
		{
			$guid = makeGuid();
			$sql = "insert into users(email, password,title,firstname,middlename,lastname,initials, signup_date, verified, guid) 
			values( '".$email."', '".sha1($pass)."', '".addslashes($title)."', '".addslashes($fName)."', '".addslashes($mName)."', '".addslashes($lName)."', '".addslashes($initials)."', '".date("Y-m-d h:i:s")."', 0, '".$guid."')";
			mysql_query( $sql) or die(mysql_error());
			$_SESSION['usermasterid'] = mysql_insert_id();
		}
		else
		{
			$sql = "UPDATE users SET 
			email ='".$email."', ";
			if($pass != ""){
				$sql .=	"password = '".sha1($pass)."', ";
			}
			$sql .=	"title = '".$title."',
			firstname = '".addslashes($fName)."',
			middlename = '".addslashes($mName)."',
			lastname = '".addslashes($lName)."',
			initials = '".addslashes($initials)."'
			WHERE id = ".$_SESSION['usermasterid'];
			mysql_query( $sql) or die(mysql_error());
		}//END IF USERS
		
		$sql = "SELECT * FROM lu_users_usertypes where user_id=".$_SESSION['usermasterid'] . "  and usertype_id=5";
		$result = mysql_query($sql)
		or die(mysql_error().$sql);
		$doInsert = true;
		while($row = mysql_fetch_array( $result )) 
		{
			$doInsert = false;
			$_SESSION['userid'] = $row['id'];
		}
		if($doInsert == true)
		{
			$sql = "insert into lu_users_usertypes(user_id, usertype_id,domain) 
			values( ".$_SESSION['usermasterid'].", 5, NULL)";
			mysql_query( $sql) or die(mysql_error());
			$_SESSION['userid'] = mysql_insert_id();
		}
		
		$sql = "SELECT * FROM users_info where user_id=".$_SESSION['userid'];
		$result = mysql_query($sql)
		or die(mysql_error());
		$doInsert = true;
		while($row = mysql_fetch_array( $result )) 
		{
			$doInsert = false;
		}
		if($doInsert == true && $err == "")
		{
			//INSERT USER INFO
			$sql = "insert into users_info(
			user_id,
			gender,
			dob,
			address_cur_street1,
			address_cur_street2,
			address_cur_street3,
			address_cur_street4,
			address_cur_city,
			address_cur_state,
			address_cur_pcode,
			address_cur_country,
			address_cur_tel,
			address_cur_tel_mobile,
			address_cur_tel_work,
			address_perm_street1,
			address_perm_street2,
			address_perm_street3,
			address_perm_street4,
			address_perm_city,
			address_perm_state,
			address_perm_pcode,
			address_perm_country,
			ethnicity,
			visastatus,
			homepage,
			cit_country,
			cur_pa_res
			)values(
			".$_SESSION['userid'].",
			'".$gender."',
			'".flipdate($dob,'/','-')."',
			'".addslashes($street1)."',
			'".addslashes($street2)."',
			'".addslashes($street3)."',
			'".addslashes($street4)."',
			'".addslashes($city)."',
			".$state.",
			'".$postal."',
			".$country.",
			'".addslashes($tel)."',
			'".addslashes($telMobile)."',
			'".addslashes($telWork)."',
			'".addslashes($streetP1)."',
			'".addslashes($streetP2)."',
			'".addslashes($streetP3)."',
			'".addslashes($streetP4)."',
			'".addslashes($cityP)."',
			".$stateP.",
			'".addslashes($postalP)."',
			".$countryP.",
			".$ethnicity.",
			".$visaStatus.",
			'".addslashes($homepage)."',
			".$countryCit.",
			".$curResident."
			)";
			mysql_query( $sql) or die(mysql_error() . "<>". $sql);
			//echo $sql;
		}else
		{
			$sql = "UPDATE users SET 
			email ='".$email."', ";
			if($pass != ""){
				$sql .=	"password = '".sha1($pass)."', ";
			}
			$sql .=	"title = '".addslashes($title)."',
			firstname = '".addslashes($fName)."',
			middlename = '".addslashes($mName)."',
			lastname = '".addslashes($lName)."',
			initials = '".addslashes($initials)."'
			WHERE id = ".$_SESSION['usermasterid'];
			mysql_query( $sql) or die(mysql_error());
			
			$sql = "update users_info set
			gender = '".$gender."',
			dob = '".formatMySQLdate($dob,'/','-')."',
			address_cur_street1 = '".addslashes($street1)."',
			address_cur_street2 = '".addslashes($street2)."',
			address_cur_street3 = '".addslashes($street3)."',
			address_cur_street4 = '".addslashes($street4)."',
			address_cur_city = '".addslashes($city)."',
			address_cur_state = ".$state.",
			address_cur_pcode = '".addslashes($postal)."',
			address_cur_country = ".$country.",
			address_cur_tel = '".addslashes($tel)."',
			address_cur_tel_mobile = '".addslashes($telMobile)."',
			address_cur_tel_work = '".addslashes($telWork)."',
			address_perm_street1 = '".addslashes($streetP1)."',
			address_perm_street2 = '".addslashes($streetP2)."',
			address_perm_street3 = '".addslashes($streetP3)."',
			address_perm_street4 = '".addslashes($streetP4)."',
			address_perm_city = '".addslashes($cityP)."',
			address_perm_state = ".$stateP.",
			address_perm_pcode = '".addslashes($postalP)."',
			address_perm_country = ".$countryP.",
			ethnicity = ".$ethnicity.",
			visastatus = ".$visaStatus.",
			homepage='".addslashes($homepage)."',
			cit_country=".$countryCit." ,
			cur_pa_res=".$curResident."
			where user_id=".$_SESSION['userid'];
			mysql_query( $sql) or die(mysql_error()."<br>".$sql);
			//echo $sql;
		}//END USER INFO

		if($err == "")
		{
			$_SESSION['firstname'] = $fName;
			$_SESSION['lastname'] = $lName;
			$_SESSION['email'] = $email;
			updateReqComplete("bio.php",1);
		}else
		{
			updateReqComplete("bio.php",0);
		}
		//header("Location: home.php");
	}//end if email
}
else{


//GET USER INFO
$sql = "SELECT 
users.email,
users.title,
users.firstname,
users.middlename,
users.lastname,
users.initials,
users_info.gender,
users_info.dob,
users_info.address_cur_street1,
users_info.address_cur_street2,
users_info.address_cur_street3,
users_info.address_cur_street4,
users_info.address_cur_city,
users_info.address_cur_state,
users_info.address_cur_pcode,
users_info.address_cur_country,
users_info.address_cur_tel,
users_info.address_cur_tel_mobile,
users_info.address_cur_tel_work,
users_info.address_perm_street1,
users_info.address_perm_street2,
users_info.address_perm_street3,
users_info.address_perm_street4,
users_info.address_perm_city,
users_info.address_perm_state,
users_info.address_perm_pcode,
users_info.address_perm_country,
users_info.address_perm_tel,
users_info.address_perm_tel_mobile,
users_info.address_perm_tel_work,
users_info.ethnicity,
users_info.visastatus,
users_info.homepage,
users_info.cit_country,
users_info.cur_pa_res
FROM lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id 
left outer join users_info on users_info.user_id = lu_users_usertypes.id  
where lu_users_usertypes.id=".$_SESSION['userid'];
//echo $sql;
$result = mysql_query($sql)
or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$email = stripslashes($row['email']);
	$title = stripslashes($row['title']);
	$fName = stripslashes($row['firstname']);
	$mName = stripslashes($row['middlename']);
	$lName = stripslashes($row['lastname']);
	$initials = stripslashes($row['initials']);
	$gender = $row['gender'];
	$dob = formatUSdate($row['dob'],'-','/');
	$street1 = stripslashes($row['address_cur_street1']);
	$street2 = stripslashes($row['address_cur_street2']);
	$street3 = stripslashes($row['address_cur_street3']);
	$street4 = stripslashes($row['address_cur_street4']);
	$city = stripslashes($row['address_cur_city']);
	$state = $row['address_cur_state'];
	$postal = stripslashes($row['address_cur_pcode']);
	$country = $row['address_cur_country'];
	$tel = stripslashes($row['address_cur_tel']);
	$telMobile = stripslashes($row['address_cur_tel_mobile']);
	$telWork = stripslashes($row['address_cur_tel_work']);
	$streetP1 = stripslashes($row['address_perm_street1']);
	$streetP2 = stripslashes($row['address_perm_street2']);
	$streetP3 = stripslashes($row['address_perm_street3']);
	$streetP4 = stripslashes($row['address_perm_street4']);
	$cityP = stripslashes($row['address_perm_city']);
	$stateP = $row['address_perm_state'];
	$postalP = stripslashes($row['address_perm_pcode']);
	$countryP = $row['address_perm_country'];
	$ethnicity = $row['ethnicity'];
	$visaStatus = $row['visastatus'];
	$homepage = stripslashes($row['homepage']);
	$countryCit = $row['cit_country'];
	$curResident = $row['cur_pa_res'];
}
}
?>
<script language="javascript">
function copyAddress()
{
	document.form1.elements['txtStreetP1'].value = document.form1.elements['txtStreet1'].value;
	document.form1.elements['txtStreetP2'].value = document.form1.elements['txtStreet2'].value;
	document.form1.elements['txtStreetP3'].value = document.form1.elements['txtStreet3'].value;
	document.form1.elements['txtStreetP4'].value = document.form1.elements['txtStreet4'].value;
	document.form1.elements['txtCityP'].value = document.form1.elements['txtCity'].value;
	document.form1.elements['txtPostalP'].value = document.form1.elements['txtPostal'].value;
	document.forms[0].lbStateP.selectedIndex = document.forms[0].lbState.selectedIndex;
	document.forms[0].lbCountryP.selectedIndex = document.forms[0].lbCountry.selectedIndex;
	//document.form1.elements['txtTelP'].value = document.form1.elements['txtTel'].value;
	//document.form1.elements['txtTelPMobile'].value = document.form1.elements['txtTelMobile'].value;
	//document.form1.elements['txtTelPWork'].value = document.form1.elements['txtTelWork'].value;
}

</script>

<!-- InstanceEndEditable -->
*/
?>
</head>
<link rel="stylesheet" href="../css/app2.css" type="text/css">
<link rel="stylesheet" href="../css/MHCI_style.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" onSubmit="return verifyForm()">
        <!-- InstanceEndEditable -->
        <div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
              <? 
            if($_SESSION['usertypeid'] != 6)
            {
                include '../inc/sideNav.php'; 
            }
            ?>
            <!-- InstanceEndEditable -->
			<!-- InstanceBeginEditable name="EditNav" -->
            <tr>
            <td>
                <table>
                <colgroup>
                <col width=80% style=text-align:left;>
                <col width=20% style=:right;>
                </colgroup>
                <tr>
            
            <td style="text-align:right; width:130px">    
         
                <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                ){ ?>
                    <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                    <!-- DAS Removed - <? echo $sesEmail;?>   -->
                    <? }else
                        {
                            if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                            && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                            {
                            //session_unset();
                            //destroySession();
                            //header("Location: index.php");
                        ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                }
            } ?>
            </td>
            <tr>
            </table>
            </td>
            </tr>
			<!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" -->
            <div class="title">Programming Prerequisite</div>
            <button class="bodyButton-right" onClick="parent.location='mhci_prereqs_intro.php';return false;">
            Go back and view requirements and status
            </button> 
            <!-- InstanceEndEditable -->
			<br/><br/>

<?
include "../inc/mhci_prereqsAssessmentForm.class.php";
include "../inc/mhci_prereqsCourseForm.class.php";
include "../inc/mhci_prereqsCourseFormController.class.php";
include "../inc/mhci_prereqsCodeSampleForm.class.php";
include "../inc/mhci_prereqsProgrammingTestForm.class.php";
include "../inc/mhci_prereqsConversationForm.class.php";
            
$studentId = $_SESSION['userid'];
$view = "student";
if( isset($_REQUEST['role']) ) {
    if ($_REQUEST['role'] == "reviewer") {
        $userId = $reviewerId;
        $view = $_REQUEST['role'];
    }
} 
?>

<br />
<div class="bodyText">
Use this page to work with the Programming faculty reviewer on a plan for fulfilling this prerequisite. Feel free to ask the reviewer a question at any point by starting a conversation at the bottom of the page. 
</div>
<br />

<div id="assessmentForm">

<?php

$assessmentForm = new MHCI_PrereqsAssessmentForm($studentId, $view, "programming");
$assessmentForm->render();

// New content piece goes here.
echo '<div class="bodyText">new content type here: DASProgramming</div>';
$sql = "select content from content where name='DASProgramming' and domain_id=".$domainid;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}


?>

</div> 
<hr/>

<div id="courseForms">

<?php

$courseFormController = new MHCI_PrereqsCourseFormController($studentId, $view, "programming", TRUE);
$courseFormController->render();

?>

</div>

 
<div id="codeSample">

<?php 

$codeSampleForm = new MHCI_PrereqsCodeSampleForm($studentId, $view);
$codeSampleForm->render();

?>

</div>
<hr/>

<div id="placementTest">

<?php 

$codeSampleForm = new MHCI_PrereqsProgrammingTestForm($studentId, $view);
$codeSampleForm->render();

?>

</div>
<hr/>


<div id="conversationForm">

<?php 

$conversationForm = new MHCI_PrereqsConversationForm($studentId, $studentId, $view, "programming");
$conversationForm->render();

?>

</div>
<hr/>


<!--
<br/><br/>
<div class="bodyText">
Use this page to work with the Statistics faculty reviewer on a plan for fulfilling this prerequisite. Feel free to ask the reviewer a question at any point by starting a conversation at the bottom of the page. 
</div>

<br/>
<div class="bodyText-bold">First, tell the reviewer where you are in the process:</div><br/>
<input type="radio" /><label class="bodyText">&nbsp;I think I have fulfilled all or part of the Statistics prerequsite. (i.e., I have taken some Statistics courses and recieved at least a C) </label>
<br/>
<input type="radio" /><label class="bodyText">&nbsp;I have not fulfilled the Statistics prerequsite. (i.e., I currently have no Statistics background)</label><br/>
<button class="bodyButton">Save</button>
<br/><br/>
<br>
<br/>

<hr class="hr"/>
<div class="sectionTitle">Course that covers multi-way ANOVA</div>
<br/><br/>
    <div class="bodyText">If you have taken a course that covers multi-way ANOVA, please list it here and upload a syllabus. Otherwise, you can skip this section.</div><br/>
    <div id="courseNameLabel" class="bodyText"><strong>Course name and/or number</strong>

        <br/>(as written on your transcript)
           <br/><input id="courseNameInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseTimeLabel" class="bodyText"><strong>When taken (or to be taken)</strong>
        <br/>(as written on your transcript)
           <br/><input id="courseTimeInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseInstitutionLabel" class="bodyText"><strong>Institution</strong>
        <br/>(e.g. Carnegie Mellon University)
           <br/><input id="courseInstitutionInput" class="textInput-short" type="text"/>

       </div>
    <div id="courseGradeLabel" class="bodyText"><strong>Grade</strong>
        <br/>(e.g. A)
           <br/><input id="courseGradeInput" class="textInput-xshort" type="text"/>
       </div>
    <br/><br/><br/><br/>
    <button id="uploadDoc" class="bodyButton">Upload syllabus or other course document</button>
    <br/><br/>

    <div class="bodyText">Where does this document show that your course covered <strong>multi-way ANOVA</strong>?</div>
    <textarea name="keywordTextarea" class="keywordArea" onkeyup="checkKeywordFields();"></textarea><br/><br/>
    <button class="bodyButton-right" disabled=true>Submit to the reviewer</button>
    <br/>
<hr class="hr"/>
<br/><br/>

<hr class="hr"/>

<div class="sectionTitle">Course that covers multi-factor regression</div>
    
    <br/><br/>
    <div class="bodyText">If you have taken a course that covers multi-factor regression, please list it here and upload a syllabus. Otherwise, you can skip this section.</div><br/>
    <div id="courseNameLabel" class="bodyText"><strong>Course name and/or number</strong>
        <br/>(as written on your transcript)
           <br/><input id="courseNameInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseTimeLabel" class="bodyText"><strong>When taken (or to be taken)</strong>

        <br/>(as written on your transcript)
           <br/><input id="courseTimeInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseInstitutionLabel" class="bodyText"><strong>Institution</strong>
        <br/>(e.g. Carnegie Mellon University)
           <br/><input id="courseInstitutionInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseGradeLabel" class="bodyText"><strong>Grade</strong>
        <br/>(e.g. A)
           <br/><input id="courseGradeInput" class="textInput-xshort" type="text"/>

       </div>
    <br/><br/><br/><br/>
    <button id="uploadDoc" class="bodyButton">Upload syllabus or other course document</button>
    <br/><br/>
    <div class="bodyText">Where does this document show that your course covered <strong>multi-factor regression</strong>?</div>
    <textarea name="keywordTextarea" class="keywordArea" onkeyup="checkKeywordFields();"></textarea><br/><br/>

    <button class="bodyButton-right" disabled=true>Submit to the reviewer</button>
    <br/>
<hr class="hr"/>
<br/><br/>

<hr class="hr"/>
<div class="sectionTitle">Course that covers single-way ANOVA</div>
<br/><br/>
    <div class="bodyText">If you have taken a course that covers single-way ANOVA, please list it here and upload a syllabus. This section is not necessary if you have already listed a course that covers multi-way ANOVA. </div><br/>
    <div id="courseNameLabel" class="bodyText"><strong>Course name and/or number</strong>

        <br/>(as written on your transcript)
           <br/><input id="courseNameInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseTimeLabel" class="bodyText"><strong>When taken (or to be taken)</strong>
        <br/>(as written on your transcript)
           <br/><input id="courseTimeInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseInstitutionLabel" class="bodyText"><strong>Institution</strong>
        <br/>(e.g. Carnegie Mellon University)
           <br/><input id="courseInstitutionInput" class="textInput-short" type="text"/>

       </div>
    <div id="courseGradeLabel" class="bodyText"><strong>Grade</strong>
        <br/>(e.g. A)
           <br/><input id="courseGradeInput" class="textInput-xshort" type="text"/>
       </div>
    <br/><br/><br/><br/>
    <button id="uploadDoc" class="bodyButton">Upload syllabus or other course document</button>
    <br/><br/>

    <div class="bodyText">Where does this document show that your course covered <strong>single-way ANOVA</strong>?</div>
    <textarea name="keywordTextarea" class="keywordArea" onkeyup="checkKeywordFields();"></textarea><br/><br/>
    <button class="bodyButton-right" disabled=true>Submit to the reviewer</button>
    <br/>
<hr class="hr"/>
<br/><br/>

<hr class="hr"/>

<div class="sectionTitle">Course that covers single-factor regression</div>
       <br/><br/>
    <div class="bodyText">If you have taken a course that covers single-factor regression, please list it here and upload a syllabus. This section is not necessary if you have already listed a course that covers multi-factor regression. </div><br/>
    <div id="courseNameLabel" class="bodyText"><strong>Course name and/or number</strong>
        <br/>(as written on your transcript)
           <br/><input id="courseNameInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseTimeLabel" class="bodyText"><strong>When taken (or to be taken)</strong>

        <br/>(as written on your transcript)
           <br/><input id="courseTimeInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseInstitutionLabel" class="bodyText"><strong>Institution</strong>
        <br/>(e.g. Carnegie Mellon University)
           <br/><input id="courseInstitutionInput" class="textInput-short" type="text"/>
       </div>
    <div id="courseGradeLabel" class="bodyText"><strong>Grade</strong>
        <br/>(e.g. A)
           <br/><input id="courseGradeInput" class="textInput-xshort" type="text"/>

       </div>
    <br/><br/><br/><br/>
    <button id="uploadDoc" class="bodyButton">Upload syllabus or other course document</button>
    <br/><br/>
    <div class="bodyText">Where does this document show that your course covered <strong>single-factor regression</strong>?</div>
    <textarea name="keywordTextarea" class="keywordArea" onkeyup="checkKeywordFields();"></textarea><br/><br/>

    <button class="bodyButton-right" disabled=true>Submit to the reviewer</button>
    <br/>
<hr class="hr"/>
<br/><br/>

<button class="bodyButton">Add another reference</button>
<hr class="hr"/>
<div class="sectionTitle">Reference 1</div>
    <button class="bodyButton-right" id="deleteReferenceButton" >Delete</button>
    <br/><br/>

<div class="bodyText">In very rare cases, a student may have no coursework in Statistics, but have on-the-job training. If you would like to submit a reference to can attest to your knowledge of multi-way ANOVA and/or multi-factor regression, you may add that person's information here.</div><br/>
    <div id="referenceFirst" class="bodyText">First Name</div>
    <div id="referenceLast" class="bodyText">Last Name</div>
    <div id="referenceJob" class="bodyText">Job Title</div>
    <br/>
        <div id="referenceFirstText" class="bodyText">John</div>
        <div id="referenceLastText" class="bodyText">Doe</div>

        <div id="referenceJobText" class="bodyText">Head researcher</div>
    <br/><br/>
    <div id="referenceComp" class="bodyText">Institution</div>
    <div id="referenceEmail" class="bodyText">Email</div>
    <div id="referencePhone" class="bodyText">Phone</div>
    <br/>
        <div id="referenceCompText" class="bodyText">MIT</div>

        <div id="referenceEmailText" class="bodyText">john.doe@mit.edu</div>
        <div id="referencePhoneText" class="bodyText">384-285-2998</div>
    <br/><br/>
    <div id="sendRequest" class="bodyText-bold">Send email request to reference.</div>
    <button class="bodyButton">Send request</button>
    <br/>
<hr class="hr"/>

<br/><br/>

<hr class="hr"/>
<div id="feedbackTitle" class="sectionHeaderText">
    Conversation with faculty reviewer
</div>
<br/><br/>
    <div class="bodyText">Use this area to start a conversation with the faculty member responsible for evaluating your statistics prerequisite.</div>
    <br/>
    <textarea class="commentArea"></textarea>
    <br/>

    <button class="bodyButton-right">Send to reviewer</button>
    <br/>
<hr class="hr"/>
-->
        </form>
        </body>
<!-- InstanceEnd --></html>
