<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';  

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$id = -1;
$type = -1;
$err = "";
$isvalid = false;
$returnurl = "";
$title = "";
$returnurl = "";
$filetypes = "";

if(isset($_GET['id']))
{
	if($_GET['id'] != "")
	{
		$id = intval($_GET['id']);
	}
}
if(isset($_GET['t']))
{
	if($_GET['t'] != "")
	{
		$type = intval($_GET['t']);
	}
}

switch($type)
{
	case 1:
		//transcript
		$returnurl = "uni.php";
		$title = "Colleges/Universities";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 2:
		//resume
		if (isHistoryDomain($_SESSION['domainid']))
        {
            $title = "C.V.";
        }
        else
        {
            $title = "Resume";
        }
        if ($domainid == 44) {
            $returnurl = "suppinfo.php";
        } else {
		    $returnurl = "resume.php";
        }
		$filetypes = "PDF";
		break;
	case 3:
		//recommendation
		$title = "Recommendation";
		$filetypes = "PDF or MS Word";
		break;
	case 4:
        if ($domainid == 44 || isRiFifthYearMastersDomain($domainid)) {
            $returnurl = "statement.php";
        } else {
		    $returnurl = "resume.php";
        }
		$title = "Statement of Purpose";
		$filetypes = "PDF";
		//statement
		break;
	case 5:
		//experience
        /*
        if($_SESSION['domainname'] == "MSE-Dist" 
            || $_SESSION['domainname'] == "MSE-Campus"
            || $_SESSION['domainname'] == "MSIT-ESE"
            || $_SESSION['domainname'] == "MSE-Portugal"
            || $_SESSION['domainname'] == "MSE-MSIT-Korea"
            || $_SESSION['domainname'] == "MSIT-GM-Dist"
            || $_SESSION['domainname'] == "MSIT-GM-Dist-INTL"
            || $_SESSION['domainname'] == "MSIT-GM-Dist-NATL"
            || $_SESSION['domainname'] == "GM-CSE"
            || $_SESSION['domainname'] == "MS-HCII") {
		*/
        if ( isMseMsitDomain($_SESSION['domainid']) 
            || isMshciiDomain($_SESSION['domainid'])
            || isMsrtDomain($_SESSION['domainid'])
            || isMsLSEDomain($_SESSION['domainid']) ) 
        {
		    $returnurl = "resume.php";
        } else {
            $returnurl = "suppinfo.php";
        };
        $title = "Experience";
		$filetypes = "PDF";
		break;
	case 6:
		//gre score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 7:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 8:
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		//toefl score
		break;
	case 9:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
	case 10:
		//toefl score
		$returnurl = "scores.php";
		$title = "Test Scores";
		$filetypes = "GIF, JPG or PDF";
		break;
    
    // PLB added new cases 03/12/09 
    case 11:
        $returnurl = "../placeout/mhci_prereqs_statistics.php";
        $title = "Multi-way ANOVA course";
        $filetypes = "GIF, JPG or PDF";
        break;
    case 12:
        $returnurl = "../placeout/mhci_prereqs_statistics.php";
        $title = "Multi-factor regression course";
        $filetypes = "GIF, JPG or PDF";
        break;
    case 13:
        $returnurl = "../placeout/mhci_prereqs_statistics.php";
        $title = "Single-way ANOVA course";
        $filetypes = "GIF, JPG or PDF";
        break; 
    case 14:
        $returnurl = "../placeout/mhci_prereqs_statistics.php";
        $title = "Single-factor regression course";
        $filetypes = "GIF, JPG or PDF";
        break;
    case 15:
        $returnurl = "../placeout/mhci_prereqs_design.php";
        $title = "Design course";
        $filetypes = "GIF, JPG or PDF";
        break;
    case 16:
        $returnurl = "../placeout/mhci_prereqs_programming.php";
        $title = "Programming course";
        $filetypes = "GIF, JPG or PDF";
        break;
    case 17:
        $returnurl = "../placeout/mhci_prereqs_programming.php";
        $title = "Code sample";
        $filetypes = "Text or Zip";
        break;
    case 18:
        $returnurl = "../placeout/mhci_prereqs_programming.php";
        $title = "Programming Test";
        $filetypes = "Text";
        break;
    case 19:
        //elts score
        $returnurl = "scores.php";
        $title = "Test Scores";
        $filetypes = "GIF, JPG or PDF";
        break;
    case 20:
        //gmat score
        $returnurl = "scores.php";
        $title = "Test Scores";
        $filetypes = "GIF, JPG or PDF";
        break;
     case 21:
        //ISRET voucher
        $returnurl = "pay.php";
        $title = "Payment Voucher";
        $filetypes = "PDF";
        break;     
    case 22:
        // funding award letter
        $title = "Award Letter";
        $returnurl = "suppinfo.php";
        $filetypes = "PDF";
        break;
    case 23:
        // writing sample
        $title = "Writing Sample";
        $returnurl = "proficiency.php";
        $filetypes = "PDF or MS Word";
        break;
    case 24:
        // speaking sample
        $title = "Speaking Sample";
        $returnurl = "proficiency.php";
        $filetypes = "MP3 or WAV";
        break;
    case 26:
        // publication
        $title = "Publicaton";
        $returnurl = "suppinfo.php";
        $filetypes = "PDF";
        break;
    case 27:
        // financial support document
        $title = "Financial Support Letter";
        $returnurl = "suppinfo.php";
        $filetypes = "PDF";
        break;
    case 28:
        // research proposal document
        $title = "Research Topic Proposal";
        $returnurl = "research_proposal.php";
        $filetypes = "PDF";
        break;
    case 29:
        // biographical essay document
        $title = "Biographical Essay";
        $returnurl = "biographical_essay.php";
        $filetypes = "PDF";
        break;
     default:
}//end switch

if(isset($_FILES["file"]))
{  
	$ret = handle_upload_file($type, $_SESSION['userid'],$_SESSION['usermasterid'], $_FILES["file"],$_SESSION['appid']."_".$id );
	//echo $ret;
	$err = "";
	if(intval($ret) < 1)
	{
		$err = $ret."<br>";
	}else
	{
		$sql = "";
        
		switch($type)
		{
			case 1:
				//transcript
				$sql = "update usersinst set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 2:
				//resume;
                /*  For domains with professional experience uploads need to check for statement and profesional experience to mark section complete
                when a resume is uploaded
                */
                if (isIniDomain($_SESSION['domainid']))
                {
                    // Check for textual SOP
                    $sopQuery = "SELECT id FROM ini_sop WHERE application_id = " . $_SESSION['appid'];
                    $sopResult = mysql_query($sopQuery); 
                    if (mysql_numrows($sopResult) > 0)
                    {
                        // SOP has been saved, page is complete.
                        updateReqComplete("resume.php", 1);        
                    }
                    else
                    {
                        // SOP has not been saved, page is incomplete.
                        updateReqComplete("resume.php", 0);    
                    }    
                }
                elseif (isEM2Domain($_SESSION['domainid']))
                {
                    // Check for textual SOP
                    $sopQuery = "SELECT id FROM em2_sop WHERE application_id = " . $_SESSION['appid'];
                    $sopResult = mysql_query($sopQuery); 
                    if (mysql_numrows($sopResult) > 0)
                    {
                        // SOP has been saved, page is complete.
                        updateReqComplete("resume.php", 1);        
                    }
                    else
                    {
                        // SOP has not been saved, page is incomplete.
                        updateReqComplete("resume.php", 0);    
                    }    
                }
                elseif (isDesignPhdDomain($_SESSION['domainid']))
                {
                    // SOP is not required
                    updateReqComplete("resume.php", 1);        
                }
                elseif (isMsLSEDomain($_SESSION['domainid'])) {
                        // Check for statement.
                            $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                            $statementQuery .= $_SESSION['userid'] . " AND section = 4 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                            $result = mysql_query($statementQuery) or die(mysql_error());                
                            $doUpdate = false;
                            while($row = mysql_fetch_array( $result )) 
                            {
                                $statementUploaded= true;
                            }
                        // Remove check for professional experience
                        // Check for professional experience
                        /*    $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                            $statementQuery .= $_SESSION['userid'] . " AND section = 5 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                            $result = mysql_query($statementQuery) or die(mysql_error());                
                            $doUpdate = false;
                            while($row = mysql_fetch_array( $result )) 
                            {
                                $profExpUploaded= true;
                            }
                        */
                            if ($statementUploaded) {  // && $profExpUploaded
                                updateReqComplete("resume.php", 1);
                                }
                                else {
                                    updateReqComplete("resume.php", 0);
                                }
                } else {
                    //Check for statement
                    $sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=";
                    $sql .= $_SESSION['userid'] . " and section=4 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";
				    $result = mysql_query($sql) or die(mysql_error());				
				    $doUpdate = false;
				    while($row = mysql_fetch_array( $result )) 
				    {
					    $doUpdate= true;
				    } 
				    if($doUpdate== true)
				    {
                        if ($domainid != 44)  {
                            updateReqComplete("resume.php", 1);
                        } else {
                            updateReqComplete("suppinfo.php", 1);
                        }
                    }else
                    {   if ($domainid != 44)  {
                            updateReqComplete("resume.php", 0);
                            } else {
                                updateReqComplete("suppinfo.php", 0);
                        }
                    }
                }
				break;
			case 3:
				//recommendation

				break;
			case 4:
				//statement 
                /*  For domains with professional experience uploads need to check for resume and profesional experience to mark section complete
                when a statement is uploaded
                */
                if (isMsLSEDomain($_SESSION['domainid'])) {
                        // Check for statement.
                            $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                            $statementQuery .= $_SESSION['userid'] . " AND section = 2 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                            $result = mysql_query($statementQuery) or die(mysql_error());                
                            $doUpdate = false;
                            while($row = mysql_fetch_array( $result )) 
                            {
                                $resumeUploaded= true;
                            }
                        // remove experience requirement
                        // Check for professional experience
                      /*
                            $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                            $statementQuery .= $_SESSION['userid'] . " AND section = 5 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                            $result = mysql_query($statementQuery) or die(mysql_error());                
                            $doUpdate = false;
                            while($row = mysql_fetch_array( $result )) 
                            {
                                $profExpUploaded= true;
                            }
                      */
                            if ($resumeUploaded ) { // && $profExpUploaded
                                updateReqComplete("resume.php", 1);
                                }
                                else {
                                    updateReqComplete("resume.php", 0);
                                }
                } 
                else 
                {
                    if ($domainid == 44 || isPhilosophyDomain($_SESSION['domainid'])
                        || isRiFifthYearMastersDomain($_SESSION['domainid']))  
                    {         
                        // skip check for resume for statistics, philosophy domains                                    
                        $doUpdate = true;
                    } 
                    else 
                    {
                        if ( isMsrtDomain($_SESSION['domainid']) )
                       {                        
                           // Check for professional experience.                        
                           $sql = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";                        
                           $sql .= $_SESSION['userid'] . " AND section = 5 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";                         
                       }                    
                        else                    
                        {                        // check for resume already existing after upload of the statement                        
                            $sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=";                        
                            $sql .= $_SESSION['userid'] . " and section=2 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";                                             
                        }				
                        $result = mysql_query($sql) or die(mysql_error());				
				        $doUpdate = false;
				        while($row = mysql_fetch_array( $result )) 
				        {
					        $doUpdate= true;
				        } 
                    }				
                    
                    if($doUpdate== true)
				    {
                        if ($domainid != 44 && !isRiFifthYearMastersDomain($domainid))  {
					        updateReqComplete("resume.php", 1);
                        } 
                        else 
                        {                        
                            updateReqComplete("statement.php", 1);                    
                        }				
                    }
                    else				
                    {   
                        if ($domainid != 44)  
                        {
					        updateReqComplete("resume.php", 0);
                        } 
                        else 
                        {
                                updateReqComplete("statement.php", 0);
                        }  
				    }
                }
				break;
			case 5:
				//experience
				$sql = "delete from experience where application_id=".intval($_SESSION['appid'])." and experiencetype=".intval($id);
				$result = mysql_query($sql) or die(mysql_error());
				$sql = "insert into experience(application_id, datafile_id, experiencetype )values(".intval($_SESSION['appid']).", ".intval($ret).", ".intval($id).")" ;

                if ( isMsrtDomain($_SESSION['domainid']) )  
                {
                    // Check for statement.
                    $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                    $statementQuery .= $_SESSION['userid'] . " AND section = 4 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                    $result = mysql_query($statementQuery) or die(mysql_error());                
                    $doUpdate = false;
                    while($row = mysql_fetch_array( $result )) 
                    {
                        $doUpdate= true;
                    } 
                    if($doUpdate== true)
                    {
                        updateReqComplete("resume.php", 1);
                    }
                    else
                    {
                        updateReqComplete("resume.php", 0);
                    }
                } else if (isMsLSEDomain($_SESSION['domainid'])) {
                        // Check for statement.
                            $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                            $statementQuery .= $_SESSION['userid'] . " AND section = 4 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                            $result = mysql_query($statementQuery) or die(mysql_error());                
                            $doUpdate = false;
                            while($row = mysql_fetch_array( $result )) 
                            {
                                $statementUploaded= true;
                            }
                        // Check for resume
                            $statementQuery = "SELECT id, moddate, size, extension FROM datafileinfo WHERE user_id = ";
                            $statementQuery .= $_SESSION['userid'] . " AND section = 2 AND userdata LIKE '" . $_SESSION['appid'] . "_%'";     
                            $result = mysql_query($statementQuery) or die(mysql_error());                
                            $doUpdate = false;
                            while($row = mysql_fetch_array( $result )) 
                            {
                                $resumeUploaded= true;
                            }
                            if ($statementUploaded && $resumeUploaded) {
                                updateReqComplete("resume.php", 1);
                                }
                                else {
                                    updateReqComplete("resume.php", 0);
                                }
                }
                break;
			case 6:
				//gre score
				$sql = "update grescore set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 7:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 8:
				$sql = "update gresubjectscore set datafile_id =".intval($ret)." where id = ".$id;
				//toefl score
				break;
			case 9:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
			case 10:
				//toefl score
				$sql = "update toefl set datafile_id =".intval($ret)." where id = ".$id;
				break;
            
            // PLB added new cases 03/12/09
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                $sql = "UPDATE mhci_prereqsCourseDatafiles SET
                        submitted_to_reviewer = 0,
                        new_file_uploaded = 1,
                        datafileinfo_id = " . intval($ret);
                $sql .= " WHERE mhci_prereqsCourseDatafiles.id = " . $id; 
                break;    
            case 17:
                $sql = "UPDATE mhci_prereqsProgrammingSamples SET
                        submitted_to_reviewer = 0,
                        new_file_uploaded = 1,
                        datafileinfo_id = " . intval($ret);
                $sql .= " WHERE mhci_prereqsProgrammingSamples.id = " . $id; 
                break; 
            case 18:
                $sql = "UPDATE mhci_prereqsProgrammingTests SET
                        upload_datafileinfo_id = " . intval($ret);
                $sql .= " WHERE mhci_prereqsProgrammingTests.id = " . $id; 
                break;  
            case 19:
                // ielts score
                $sql = "update ieltsscore set datafile_id =".intval($ret)." where id = ".$id;
                break;           
            case 20:
                // gmat score
                $sql = "update gmatscore set datafile_id =".intval($ret)." where id = ".$id;
                break; 
            case 21:
                // ISRET payment voucher
                $sql = "INSERT INTO payment_voucher VALUES (" . intval($id) . "," . intval($ret) . ") 
                ON DUPLICATE KEY UPDATE datafileinfo_id = " . intval($ret);
                break;
            case 22:
                // funding award letter
                $sql = "UPDATE fellowships SET datafile_id = " . intval($ret) . " where id = " . $id;
                break;
            case 23:
                // Writing sample
                // Treat upload like a resume or statement.
                // Check whether requirements are complete in proficiency.php
                break;
            case 24:
                // Speaking sample
                // Treat upload like a resume or statement.
                // Check whether requirements are complete in proficiency.php
                break;
            case 26:
                // publication
                $sql = "UPDATE publication SET datafile_id = " . intval($ret) . " where id = " . $id;
                break;
            case 27:
                // financial support
                // Treat upload like a resume or statement.
                // Check whether requirements are complete in suppinfo.php
                break;
            case 28:
                // research proposal
                // Treat upload like a resume, but with no interdependencies. 
                updateReqComplete("research_proposal.php", 1);
                break;
            case 29:
                // biographical essay
                // Treat upload like a resume, but with no interdependencies. 
                updateReqComplete("biographical_essay.php", 1);
                break;
                
        }//end switch
		
		if($sql != "")
		{
			$result = mysql_query($sql) or die(mysql_error());
            
            // PLB: Record/email notice on programming test uploads.
            if ($result && ($type == 18)) {
            
                $linkname = "mhci_prereqs_programming.php";
                updateReqComplete($linkname, 0);        
            }   
		}
		//echo $sql;
		header("Location: ".$returnurl);
	} 
}

// Include the shared page header elements.
$pageTitle = $title . ' File Upload';
$formEnctype = 'multipart/form-data';
include '../inc/tpl.pageHeaderApply.php'; 
?>

<br>
<span class="errorSubtitle"><?=$err;?></span><br>
<span class="tblItem">Your <?=$title ?> file must be submitted in <?=$filetypes?> format. 
Any other format will not be accepted. The maximum file size is 7MB.</span>
<br>
<!-- <span class="tblItem">Your file can be in image or pdf format. The maximum file size for is 7MB</span><br> -->
<br>
<input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="30000000">
<input type="hidden" name="post_max_size" id="post_max_size" value="30000000">
<input type="hidden" name="upload_max_filesize" id="upload_max_filesize" value="30000000">
<input name="file" type="file" class="tblItem" maxlength="255" >
<?php
if (isset($_SESSION['placeout_enabled']))
{
    showEditText("Upload", "button", "btnSubmit", $_SESSION['placeout_enabled']);
} else 
    {
        showEditText("Upload", "button", "btnSubmit", $_SESSION['allow_edit']);
        }
?>
<br>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>
