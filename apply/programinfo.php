<?php 
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php  
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$id = -1;
$programname = "";
$department = "";
$school = "";
$description = "";
$url = "";

if(isset($_GET['id']))
{
	$id = intval(htmlspecialchars($_GET['id']));
}

$sql = "select programs.id, 
programs.description,
programs.url,
degree.name as degreename, 
fieldsofstudy.name as fieldname,
department.name as departmentname,
schools.name as schoolname,
programs.linkword
from programs
left outer join degree on degree.id = programs.degree_id
left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
left outer join department on department.id = lu_programs_departments.department_id
left outer join schools on schools.id = department.parent_school_id 
where programs.id=".$id;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$programname = $row['degreename'] . " ".$row['linkword']." " . $row['fieldname'];
	$department = $row['departmentname'];
	$school = $row['schoolname'];
	$description = $row['description'];
	$url = $row['url'];
}

// Include the shared page header elements.
$pageTitle = 'Program Information';
include '../inc/tpl.pageHeaderApply.php';
?>

<span class="subtitle"><?=$programname?></span><br>
<span class="tblItem"><br>
<!-- <strong>Department:</strong> <?=$department?><br>  -->
<!-- <strong>School:</strong> <?=$school?><br>          -->
<strong>Description:</strong><br>
<?=html_entity_decode($description)?><br>
<?
if($url != "")
{
echo "<strong>Additional Information:</strong> <a href='".$url."' target='_blank'>" . $url . "</a>";
}
?> 
 <br><br>
 <input name="btnInterests_" type="button" id="btnBack" value="Return to Programs" onClick="document.location='programs.php'">
 <br>
</span>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>