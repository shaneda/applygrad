<?php 
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
$ethnicities = array();
$err = "";
$mode = "edit";
$title = "";
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$ethnicity = "";
$pass = "";
$passC = "";
$dob = "";
$visaStatus = "";
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";
$umasterid = -1;

// This is obsolete??????
/*
if(isset($_GET['mode']))
{
	if($_GET['mode'] != "")
	{
		$mode = $_GET['mode'];
	}
}
*/

if(!isset($_POST['btnSubmit']))
{
	$_SESSION['firstname'] = "";
	$_SESSION['lastname'] = "";
	$_SESSION['userid'] = -1;
	$_SESSION['usermasterid'] = -1;
	$_SESSION['email'] = "";
	$_SESSION['appid'] = -1;
	
}

if($_SESSION['allow_edit'] == true)
{
	// COUNTRIES
	$result = mysql_query("SELECT * FROM countries order by name") 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($arr, $row['iso_code']);
		array_push($countries, $arr);
	}
		
	//STATES
	$result = mysql_query("SELECT * FROM states order by country_id,name")
	    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		array_push($states, $arr);
	}	
}//END IF

if(isset($_POST['btnSubmit'] ))
{
	$_SESSION['usermasterid'] == -1;
	$_SESSION['userid'] = -1;
	$sql = "";	
	
    // $mode is obsolete?????????
    /*
    $mode = filter_input(INPUT_POST, 'txtMode', FILTER_SANITIZE_STRING);
    $mode = htmlspecialchars($mode)''
    */

	$fName = filter_input(INPUT_POST, 'txtFName', FILTER_SANITIZE_STRING);
    $fName = htmlspecialchars($fName);
    
	$mName = filter_input(INPUT_POST, 'txtMName', FILTER_SANITIZE_STRING);
    $mName = htmlspecialchars($mName);
	 
    $lName = filter_input(INPUT_POST, 'txtLName', FILTER_SANITIZE_STRING);
    $lName = addslashes($lName);
    
	$email = filter_input(INPUT_POST, 'txtE', FILTER_VALIDATE_EMAIL);
	$email = htmlspecialchars($email);
    
    $pass = filter_input(INPUT_POST, 'txtP', FILTER_UNSAFE_RAW);
	$pass = htmlspecialchars($pass);
	
    $passC = filter_input(INPUT_POST, 'txtPC', FILTER_UNSAFE_RAW);
    $passC = htmlspecialchars($passC);
    
    //VERIFY FORM VARS HERE
	if($fName == "")
	{
		$err .= "First Name is Required<br>";
	}
	if($lName == "")
	{
		$err .= "Last Name is Required<br>";
	}
	if($email == "" || check_email_address($email)===false )
	{
		$err .= "Email is invalid.<br>";
	}
	
	if($pass == "" && $_SESSION['userid'] == -1)
	{
		$err .= "Password is required.<br>";
	}
    
	if(strlen($pass) < 6)   // || strlen($pass) > 8
	{
		$err .= "Password must be at least 6 characters.<br>";
	}else
	{
		if($pass != $passC && $_SESSION['userid'] == -1)
		{
			$err .= "Passwords do not match.<br>";
		}
	}
	$matched = ereg ("[0-9]+", $pass);
	if($matched != false      )
	{
	
	}else
	{
		$err .= "Password must contain at least one (1) number.<br>";
	}

	//DO INSERT/UPDATE
	if($err == "")
	{
	//FIRST LOOKUP USER INFO BASED ON FIRSTNAME, LASTNAME
	//IF MATCHED, UPDATE USER
	$sql = "select users.id
		from users
		where email = '". mysql_real_escape_string($email) ."' ";
	$result = mysql_query($sql) 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$err = "ERROR: ACCOUNT ALREADY EXISTS<br>";
		$umasterid = $row['id'];
	}
	
	//THEN DO LOOKUP ON USERTYPE - IF ONE EXISTS FOR USER, UPDATE, IF NOT, INSERT
	if( strcmp($err, "") == 0 )
	{
		if($umasterid == -1)
		{
			$guid = makeGuid();
            $password = sha1($pass);
            
			$insertQueryFormat = "INSERT INTO users
                (email, username, password, title, firstname, middlename, lastname, initials, 
                signup_date, verified, guid)
                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', 0, '%s')";
            $insertQuery = sprintf($insertQueryFormat,
                mysql_real_escape_string($email),
                mysql_real_escape_string($email),
                mysql_real_escape_string($password),
                mysql_real_escape_string($title),
                mysql_real_escape_string($fName),
                mysql_real_escape_string($mName),
                mysql_real_escape_string($lName),
                mysql_real_escape_string($initials),
                mysql_real_escape_string(date("Y-m-d")),
                mysql_real_escape_string($guid)
                );

            mysql_query($insertQuery) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $insertQuery);
			$umasterid = mysql_insert_id();
		}
		
		$sql = "SELECT * FROM lu_users_usertypes where user_id=". intval($umasterid) . "  and usertype_id=5";
		$result = mysql_query($sql)
		    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
		$doInsert = true;
		while($row = mysql_fetch_array( $result )) 
		{
			$doInsert = false;
		}
		if($doInsert == true)
		{
			$sql = "insert into lu_users_usertypes(user_id, usertype_id,domain) 
                values( ". intval($umasterid) .", 5, NULL)";
			mysql_query($sql) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			$_SESSION['userid'] = mysql_insert_id();
			$_SESSION['usertypeid'] = 5;
			
			//USER INFO
			$sql = "SELECT * FROM users_info where user_id=". intval($_SESSION['userid']);
			$result = mysql_query($sql)
			    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			$doInsert = true;
			while($row = mysql_fetch_array( $result )) 
			{
				$doInsert = false;
			}
			if($doInsert == true)
			{
				$sql = "insert into users_info(user_id)values( ". intval($_SESSION['userid']) .")";
				mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			}
			$_SESSION['firstname'] = $fName;
			$_SESSION['lastname'] = $lName;
			$_SESSION['email'] = $email;
			$_SESSION['usermasterid'] = intval($umasterid);
						
			//SEND OUT EMAIL HERE
			$str = "";
       
            switch ($hostname)
            {
                case "APPLY.STAT.CMU.EDU":  
                    $sysemail = "scsstats@cs.cmu.edu";
                    break;
                case "APPLYGRAD-INI.CS.CMU.EDU":  
                    $sysemail = "scsini@cs.cmu.edu";
                    break;
                case "APPLYGRAD-DIETRICH.CS.CMU.EDU":  
                    $sysemail = "scsdiet@cs.cmu.edu";
                    break;
                default:
                    $sysemail = "applygrad@cs.cmu.edu";
             }
			//GET SYSTEM EMAIL
			$domainid = 1;
			if(isset($_SESSION['domainid']))
			{
				if($_SESSION['domainid'] > -1)
				{
					$domainid = $_SESSION['domainid'];
				}
			}
			$sql = "select sysemail from systemenv where domain_id=". intval($domainid);
			$result = mysql_query($sql)	
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			while($row = mysql_fetch_array( $result ))
			{ 
				$sysemail = $row['sysemail'];
			}
			//GET TEMPLATE CONTENT
			$sql = "select content from content where name='New Student Account Confirmation' 
                and domain_id=". intval($domainid);
			$result = mysql_query($sql)	
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			while($row = mysql_fetch_array( $result )) 
			{
				$str = $row["content"];
			}
			$date = strtotime($_SESSION['expdate']);
			$date =  date("F d",$date);
			$vars = array(	
			array('enddate', $date),
			array('firstname',$_SESSION['firstname']),
			array('lastname',$_SESSION['lastname'])
			);
			$str = parseEmailTemplate2($str, $vars );
			//SEND EMAIL
			sendHtmlMail($sysemail, $_SESSION['email'], "New Student Account Confirmation", $str, "confirm");//to user
			header("Location: home.php");
		}//end if doinsert

		}//end if err
	}//end if err
}

// Include the shared page header elements.
$pageTitle = 'Register as New Applicant';
include '../inc/tpl.pageHeaderApply.php';
?>
<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
	<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
</div>
<span class="tblItem">&nbsp;= required</span>
<span class="errorSubtitle"><br><br><?=$err;?></span>            
<table width="100%" border="0" cellspacing="2" cellpadding="4">
  <tr>
        <td class="tblItem">
        Complete all required fields. Create a password that you can remember. 
        The password must be at least 6 characters and contain at least one (1) 
        number.
        <BR><BR>
        <strong>PLEASE NOTE: 
        <?php
        if ($_SESSION['domainid'] == 1) { 
        ?>
        Your email address is your User ID.&nbsp;&nbsp;Please use an email address that will be valid through March.
        <?php    
        } else {
        ?>
        Your email address is your User ID.&nbsp;&nbsp;Please use an email address that will be valid for the next several months.
        <?php
        }
        ?>
        <br>Please do NOT type your name in all uppercase letters.</strong>
        </td>
  </tr>
  <tr>
    <td valign="top" class="tblItem"><table width="100%" border="0" cellspacing="0" cellpadding="4">
      <tr>
        <td width="100" align="right" class="tblItem"><strong>First Name:</strong></td>
        <td class="tblItem"><? showEditText($fName, "textbox", "txtFName", $_SESSION['allow_edit'], true,null,true,40); ?></td>
      </tr>
      <tr>
        <td width="100" align="right" class="tblItem"><strong>Middle Name:</strong></td>
        <td class="tblItem"><? showEditText($mName, "textbox", "txtMName", $_SESSION['allow_edit'],false,null,true,40); ?></td>
      </tr>
      <tr>
        <td width="100" align="right" class="tblItem"><strong>Last Name:</strong></td>
        <td class="tblItem"><? showEditText($lName, "textbox", "txtLName", $_SESSION['allow_edit'], true,null,true,40); ?></td>
      </tr>

      <tr>
        <td width="100" align="right" class="tblItem"><strong>Email</strong>:</td>
        <td class="tblItem"><? showEditText($email, "textbox", "txtE", $_SESSION['allow_edit'], true,null,true,40); ?></td>
      </tr>
      <tr>
        <td width="100" align="right" class="tblItem"><strong>Password:</strong></td>
        <td class="tblItem"><? 
		$req = false;
		if($pass == "" && $_SESSION['userid'] == -1)
		{
			$req = true;
		}
		showEditText($pass, "password", "txtP", $_SESSION['allow_edit'], $req,null,true,10); ?></td>
      </tr>
      <tr>
        <td width="100" align="right" class="tblItem"><strong>Confirm Password:</strong> </td>
        <td class="tblItem"><?
		$req = false;
		if($pass == ""  && $_SESSION['userid'] == -1)
		{
			$req = true;
		}
		showEditText($passC, "password", "txtPC", $_SESSION['allow_edit'], $req,null,true,10); ?></td>
      </tr>
      <tr>
        <td align="right" class="tblItem">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="tblItem"><input name="txtMode" type="hidden" id="txtMode" value="<?=$mode;?>">
      <input name="btnSubmit" type="submit" class="tblItem" id="btnSubmit" value="  Register  " ></td>
  </tr>
</table>

<script language="javascript" type="text/javascript">
    function validate(key,val)
    {
        var err = '';
        if(val == '' || val.length == 0)
        {
            err += key + ' is required.';
        }
        if(err != '')
        {
            alert(err + ' ' + val);
            return false;
        }
    } 
</script>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>