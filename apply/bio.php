<?php 
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../apply/header_prefix.php';
include '../inc/specialCasesApply.inc.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

/*
* Check for special cases; 
*/
$isIsreeDomain = isIsreeDomain($_SESSION['domainid']);
$isCnbcDomain = isCnbcDomain($_SESSION['domainid']);
$isIniDomain = isIniDomain($_SESSION['domainid']);
$isEM2Domain = isEM2Domain($_SESSION['domainid']); 
$isDesignDomain = isDesignDomain($domainid);
$isDesignPhdDomain = isDesignPhdDomain($domainid);
$isRiMsRtChinaDomain = isRiMsRtChinaDomain($_SESSION['domainid']); 
$isPhilosopyDomain = isPhilosophyDomain($_SESSION['domainid']);

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
        {
            $prevPage = 'programs.php';
            //$prevPage = "";
        }
if (sizeof ($appPageList) > $curPageIndex + 1)
{
    $nextPage =  $appPageList[$curPageIndex + 1]; 
} 
else
{
    if ( !isSubmitted($_SESSION['appid']) ) {
        $nextPage = 'submit.php';    
    } elseif ( !isPaid($_SESSION['appid']) ) { 
        $nextPage = 'pay.php';
    } else {
        $nextPage = "";    
    }  
}
        
$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
// PLB changed to legacyEthnicities 7/23/09
//$ethnicities = array();
$legacyEthnicities = array();
$titles = array(array('Mr.', 'Mr.'), array('Ms.','Ms.'));
$err = "";
$confirmationMessage = '';
$mode = "edit";
$title = "";
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$qq_number = "";

$ethnicity = NULL;
// PLB added ipedsRace 7/23/09
$ipedsRaces = array();

$pass = "";
$passC = "";
$dob = "";
$visaStatus = -1;
$company = '';
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$street4 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$telMobile = "";
$telWork = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$streetP4 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";
$telPMobile = "";
$telPWork = "";
$countryCit = "";
$nativeTongue = "";
$curResident = "NULL";
$usCitizenOrPermanentResident = NULL;

if(isset($_GET['mode']))
{
	if($_GET['mode'] != "")
	{
		$mode = $_GET['mode'];
	}
}

// COUNTRIES
$result = mysql_query("SELECT * FROM countries order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['iso_code']);
	array_push($countries, $arr);
}
		
//STATES	
$result = mysql_query("SELECT * FROM states order by country_id,name")
or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($states, $arr);
}
	
// STATII
$result = mysql_query("SELECT * FROM visatypes order by short DESC") or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name'] . " (".$row['short'].")");
	array_push($arr, $row['short']);
	array_push($statii, $arr);
}
	
// PLB changed to legacy_ethnicity 7/23/09
// ETHNICITIES
//$result = mysql_query("SELECT * FROM ethnicity order by sortorder") or die(mysql_error());
$result = mysql_query("SELECT * FROM legacy_ethnicity order by sortorder") or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	//array_push($ethnicities, $arr);
    array_push($legacyEthnicities, $arr);
}
	    
/*
* Handle the form submission. 
*/    
if( isset($_POST['btnSubmit']) )
{
	$sql = "";
	if ( isset($_POST['lbStatus'])) 
    {
        $visaStatus = intval($_POST['lbStatus']);    
    }
    if ( isset($_POST['txtCompany'])) 
    {
        $company = $_POST['txtCompany'];    
    }
	$mode = $_POST['txtMode'];
	$fName = $_POST['txtFName'];
	$mName = $_POST['txtMName'];
	$lName = $_POST['txtLName'];
	$title = $_POST['txtTitle'];	
    if ( isset($_POST['txtDob']) ) 
    {
        $dob = $_POST['txtDob'];     
    }
	$email = $_POST['txtEmail'];
    if ( isset($_POST['txtQqNumber']) ) 
    {
        $qq_number = $_POST['txtQqNumber'];     
    }
	if ( isset($_POST['lbGender']) ) 
    {
        $gender = $_POST['lbGender'];     
    }
	// PLB changed ethnicity, added race  7/23/09
    /*
    if(isset($_POST['lbEthnicity']))
	{
		$ethnicity = intval($_POST['lbEthnicity']);
	}
    */
    if ( isset($_POST['ipedsEthnicity']) ) 
    {
        $ethnicity = intval($_POST['ipedsEthnicity']);
    } 
    elseif ( isset($_POST['lbEthnicity']) ) 
    {
        $ethnicity = intval($_POST['lbEthnicity']);
    }    
    if ( isset($_POST['ipedsRaces']) ) 
    {
        $ipedsRaces = $_POST['ipedsRaces'];   
    }
    if(isset($_POST['lbCurResident']))
	{
		$curResident = intval($_POST['lbCurResident']);
	}
    if(isset($_POST['usCitizenOrPermanentResident']))
    {
        $usCitizenOrPermanentResident = intval($_POST['usCitizenOrPermanentResident']);
    }
	$pass = $_POST['txtPass'];
	$passC = $_POST['txtPassC'];
	$street1 = $_POST['txtStreet1'];
	$street2 = $_POST['txtStreet2'];
	$street3 = $_POST['txtStreet3'];
	$street4 = $_POST['txtStreet4'];
	$city = $_POST['txtCity'];
	$postal = $_POST['txtPostal'];
	$country = intval($_POST['lbCountry']);
	$state = intval($_POST['lbState']);
	$streetP1 = $_POST['txtStreetP1'];
	$streetP2 = $_POST['txtStreetP2'];
	$streetP3 = $_POST['txtStreetP3'];
	$streetP4 = $_POST['txtStreetP4'];
	$cityP = $_POST['txtCityP'];
	$postalP = $_POST['txtPostalP'];
	$countryP = intval($_POST['lbCountryP']);
	$stateP = intval($_POST['lbStateP']);
	$tel = $_POST['txtTel'];
	$telMobile = $_POST['txtTelMobile'];
	$telWork = $_POST['txtTelWork'];
	//$telP = $_POST['txtTelP']);
	//$telPMobile = $_POST['txtTelPMobile']);
	//$telPWork = $_POST['txtTelPWork']);
	$initals = substr($fName,0,1) . substr($_POST["txtMName"],0,1) . substr($lName,0,1);
	$homepage = $_POST['txtHomepage'];
	if ( isset($_POST['lbCountryCit']) )
    {
        $countryCit = intval($_POST['lbCountryCit']);    
    }
    if ( isset($_POST['txtNativeTongue']) )
    {    
        $nativeTongue = $_POST['txtNativeTongue']; 
	}
    //VERIFY FORM VARS HERE
    if(($isDesignDomain || $isDesignPhdDomain) && $usCitizenOrPermanentResident === NULL)
    {
        $err .= "Citizen or permanent resident is required<br>";
    }
    
    if($fName == "")
    {
        $err .= "First Name is Required<br>";
    }
    
	if($fName == "")
	{
		$err .= "First Name is Required<br>";
	}
	if($lName == "")
	{
		$err .= "Last Name is Required<br>";
	}
    //if( $_SESSION['domainid'] != 42 && check_Date($dob) == false )
    if( !$isIsreeDomain && check_Date($dob) == false )
	{
		$err .= "Date of birth is invalid.<br>";
	}
	if($email == "")
	{
		$err .= "Email is required.<br>";
	}
	if($email != "" && check_email_address($email)===false )
	{
		$err .= "Email is invalid.<br>";
	}
	//if( $_SESSION['domainid'] != 42 && $gender == "" )
    if( !$isPhilosopyDomain && !$isIsreeDomain && $gender == "" )
	{
		$err .= "Sex is required.<br>";
	}
    // PLB eliminated ethnicity requirement 7/23/09
    /*
    if($ethnicity == "" && $_SESSION['domainname'] != "CNBC")
	{
		$err .= "Ethnicity is required. (An option is provided to decline)<br>";
	}
    */
	if($pass == "" && $_SESSION['usermasterid'] == -1)
	{
		$err .= "Password is required.<br>";
	}
	if($pass != $_POST['txtPassC'] && $_SESSION['usermasterid'] == -1)
	{
		$err .= "Passwords do not match.<br>";
	}
	//if( $_SESSION['domainid'] != 42 && $visaStatus == "" )
    if( !$isIsreeDomain && $visaStatus == "" ) 
	{
		$err .= "Visa status is required.<br>";
	}
    //if( $_SESSION['domainid'] == 42 && $company == "" )
    if( $isIsreeDomain && $company == "" )
    {
        $err .= "Organization is required.<br>";
    }
	//CURRENT ADDRESS VERIFY
	if($street1 == "")
	{
		$err .= "Street1 is required.<br>";
	}
	if($city == "")
	{
		$err .= "City is required.<br>";
	}
	if($postal == "")
	{
		$err .= "Postal code is required.<br>";
	}
	if($country == "")
	{
		$err .= "Country is required.<br>";
	}
	if($country == "231" || $country == "142" || $country == "39")
	{
		if($state == "")
		{
			$err .= "State is required.<br>";
		}
	}
	if($tel == "")
	{
		$err .= "Telephone is required.<br>";
	}
	//PERMANENT ADDRESS VERIFY
	if($streetP1 == "")
	{
		//$err .= "Permanent street1 is required.<br>";
	}
	if($cityP == "")
	{
		//$err .= "Permanent city is required.<br>";
	}
	if($postalP == "")
	{
		//$err .= "Permanent postal code is required.<br>";
	}
	if($countryP == "")
	{
		//$err .= "Permanent country is required.<br>";
	}
	if($countryP == "231" || $countryP == "142" || $countryP == "39")
	{
		if($stateP == "")
		{
			//$err .= "Permanent state is required.<br>";
		}
	}
	if($telP == "")
	{
		//$err .= "Permanent Telephone is required.<br>";
	}
	
	// PLB added cnbc grad training exemption 8/31/09
    //if( $countryCit == "" && $_SESSION['domainid'] != 42 
    //    && $_SESSION['domainname'] != "CNBC" && $_SESSION['domainname'] != "CNBC-Grad-Training")
    if( $countryCit == "" && !$isIsreeDomain && !$isCnbcDomain )
	{
		$err .= "Country of citizenship is required.<br>";
	}
	
    //if( $_SESSION['domainid'] != 42 && $nativeTongue == "" )
    if( !$isIsreeDomain && $nativeTongue == "" ) 
    {
        $err .= "Native language is required.<br>";
    }

	//DO INSERT/UPDATE
	if($email != "")
	{
		$sql = "select users.id from users 
		where id=".$_SESSION['usermasterid'];
		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$_SESSION['usermasterid'] = $row['id'];
		}
		if($_SESSION['usermasterid'] == -1)
		{
			$guid = makeGuid();
			$sql = "insert into users(email, password,title,firstname,middlename,lastname,initials, signup_date, verified, guid) 
			values( '".$email."', '".sha1($pass)."', '".addslashes($title)."', '".addslashes($fName)."', '".addslashes($mName)."', '".addslashes($lName)."', '".addslashes($initials)."', '".date("Y-m-d h:i:s")."', 0, '".$guid."')";
			mysql_query( $sql) or die(mysql_error());
			$_SESSION['usermasterid'] = mysql_insert_id();
		}
		else
		{
			$sql = "UPDATE users SET 
			email ='".$email."', ";
			if($pass != ""){
				$sql .=	"password = '".sha1($pass)."', ";
			}
			$sql .=	"title = '".$title."',
			firstname = '".addslashes($fName)."',
			middlename = '".addslashes($mName)."',
			lastname = '".addslashes($lName)."',
			initials = '".addslashes($initials)."'
			WHERE id = ".$_SESSION['usermasterid'];
			mysql_query( $sql) or die(mysql_error());
		}//END IF USERS
		
		$sql = "SELECT * FROM lu_users_usertypes where user_id=".$_SESSION['usermasterid'] . "  and usertype_id=5";
		$result = mysql_query($sql)
		or die(mysql_error().$sql);
		$doInsert = true;
		while($row = mysql_fetch_array( $result )) 
		{
			$doInsert = false;
			$_SESSION['userid'] = $row['id'];
		}
		if($doInsert == true)
		{
			$sql = "insert into lu_users_usertypes(user_id, usertype_id,domain) 
			values( ".$_SESSION['usermasterid'].", 5, NULL)";
			mysql_query( $sql) or die(mysql_error());
			$_SESSION['userid'] = mysql_insert_id();
		}
		
		$sql = "SELECT * FROM users_info where user_id=".$_SESSION['userid'];
		$result = mysql_query($sql)
		or die(mysql_error());
		$doInsert = true;
		while($row = mysql_fetch_array( $result )) 
		{
			$doInsert = false;
		}
		if($doInsert == true && $err == "")
		{
			//INSERT USER INFO
			$sql = "insert into users_info(
			user_id,
			gender,
			dob,
			address_cur_street1,
			address_cur_street2,
			address_cur_street3,
			address_cur_street4,
			address_cur_city,
			address_cur_state,
			address_cur_pcode,
			address_cur_country,
			address_cur_tel,
			address_cur_tel_mobile,
			address_cur_tel_work,
			address_perm_street1,
			address_perm_street2,
			address_perm_street3,
			address_perm_street4,
			address_perm_city,
			address_perm_state,
			address_perm_pcode,
			address_perm_country,
			ethnicity,
			visastatus,
			homepage,";
            if ($company) {
                $sql .=  "company,";
            }
            if ($countryCit) {
                $sql .=  "cit_country,";
            }
            if ($nativeTongue) {
                $sql .=  "native_tongue,";
            }
            if ($usCitizenOrPermanentResident != null) {
                $sql .=  "us_citizen_or_permanent_resident,";
            }
            if ($qq_number) {
                $sql .=  "qq_number,";
            }
			$sql .= " cur_pa_res
			)values(
			".$_SESSION['userid'].",
			'".$gender."',
			'".flipdate($dob,'/','-')."',
			'".addslashes($street1)."',
			'".addslashes($street2)."',
			'".addslashes($street3)."',
			'".addslashes($street4)."',
			'".addslashes($city)."',
			".$state.",
			'".$postal."',
			".$country.",
			'".addslashes($tel)."',
			'".addslashes($telMobile)."',
			'".addslashes($telWork)."',
			'".addslashes($streetP1)."',
			'".addslashes($streetP2)."',
			'".addslashes($streetP3)."',
			'".addslashes($streetP4)."',
			'".addslashes($cityP)."',
			".$stateP.",
			'".addslashes($postalP)."',
			".$countryP.",
			".$ethnicity.",
			".$visaStatus.",
			'".addslashes($homepage)."',";
            if ($company) {
                $sql .= "'" . addslashes($company) . "',";
            }
            if ($countryCit) {
                $sql .= $countryCit . "',";
            }
            if ($nativeTongue) {
                $sql .= "'" . addslashes($nativeTongue) . "',";
            }
            if ($usCitizenOrPermanentResident !== NULL) {
                $sql .= $usCitizenOrPermanentResident . "',";
            }
            if ($qq_number) {
                $sql .= "'" . addslashes($qq_number) . "',";
            }
			$sql .= $curResident."
			)";
			mysql_query( $sql) or die(mysql_error() . "<>". $sql);
			//echo $sql;
		} elseif ($err == "")
		{
			$sql = "UPDATE users SET 
			email ='".$email."', ";
			if($pass != ""){
				$sql .=	"password = '".sha1($pass)."', ";
			}
			$sql .=	"title = '".addslashes($title)."',
			firstname = '".addslashes($fName)."',
			middlename = '".addslashes($mName)."',
			lastname = '".addslashes($lName)."',
			initials = '".addslashes($initials)."'
			WHERE id = ".$_SESSION['usermasterid'];
			mysql_query( $sql) or die(mysql_error());
			
            
			$sql = "update users_info set
			gender = '".$gender."',
			dob = '".formatMySQLdate($dob,'/','-')."',
			address_cur_street1 = '".addslashes($street1)."',
			address_cur_street2 = '".addslashes($street2)."',
			address_cur_street3 = '".addslashes($street3)."',
			address_cur_street4 = '".addslashes($street4)."',
			address_cur_city = '".addslashes($city)."',
			address_cur_state = ".$state.",
			address_cur_pcode = '".addslashes($postal)."',
			address_cur_country = ".$country.",
			address_cur_tel = '".addslashes($tel)."',
			address_cur_tel_mobile = '".addslashes($telMobile)."',
			address_cur_tel_work = '".addslashes($telWork)."',
			address_perm_street1 = '".addslashes($streetP1)."',
			address_perm_street2 = '".addslashes($streetP2)."',
			address_perm_street3 = '".addslashes($streetP3)."',
			address_perm_street4 = '".addslashes($streetP4)."',
			address_perm_city = '".addslashes($cityP)."',
			address_perm_state = ".$stateP.",
			address_perm_pcode = '".addslashes($postalP)."',
			address_perm_country = ".$countryP.",";
            if (isset ($ethnicity)) {
                $sql .= "ethnicity = ".$ethnicity.",";
            }
			$sql .= "visastatus = ".$visaStatus.",";
            if (isset ($company)) {
                $sql .= "company='" . addslashes($company) . "',";
            }
            $sql .= "homepage='".addslashes($homepage)."',";
            if ($countryCit) {
                $sql .=  "cit_country =" . $countryCit . ",";
            }
            if ($nativeTongue) {
                $sql .=  "native_tongue = '" . addslashes($nativeTongue) . "',";
            } else {
                $sql .=  "native_tongue = NULL,";    
            }
            if ($usCitizenOrPermanentResident !== NULL) {
                $sql .=  "us_citizen_or_permanent_resident = " . $usCitizenOrPermanentResident . ",";
            } else {
                $sql .=  "us_citizen_or_permanent_resident = NULL,";    
            }
            if ($qq_number) {
                $sql .=  "qq_number = '" . addslashes($qq_number) . "',";
            } else {
                $sql .=  "qq_number = NULL,";    
            }
			$sql .= "cur_pa_res=".$curResident."
			where user_id=".$_SESSION['userid'];
			mysql_query( $sql) or die(mysql_error()."<br>".$sql);
			//echo $sql;
		}//END USER INFO\
        
        /*
        * PLB added applicant race 7/23/09
        */
        // Insert/upate records.
        $insertRaceIds = array();
        foreach ($ipedsRaces as $ipedsRaceId) {
            $insertRaceQuery = sprintf("REPLACE INTO applicant_ipeds_race VALUES(%d, %d)",
                                $_SESSION['userid'],
                                $ipedsRaceId);
            $insertRaceResult = mysql_query($insertRaceQuery);
            $insertRaceIds[] = sprintf("%d", $ipedsRaceId); 
        }
        // Clean up orphans.
        $deleteRaceQuery = "DELETE FROM applicant_ipeds_race
                            WHERE lu_users_usertypes_id = " . $_SESSION['userid'];
        if ( !empty($insertRaceIds) ) {
            $deleteRaceQuery .= " AND ipeds_race_id NOT IN (" . implode(',', $insertRaceIds) . ")";    
        }                   
        $deleteRaceResult = mysql_query($deleteRaceQuery);
        
		if($err == "")
		{
			$_SESSION['firstname'] = $fName;
			$_SESSION['lastname'] = $lName;
			$_SESSION['email'] = $email;
			updateReqComplete("bio.php",1);
		}else
		{
			updateReqComplete("bio.php",0);
		}
		//header("Location: home.php");
	}//end if email
    
    if (!$err) {
        $confirmationMessage = 'Your biographical information has been updated successfully.';    
    }
}
else
{

//GET USER INFO
$sql = "SELECT 
users.email,
users.title,
users.firstname,
users.middlename,
users.lastname,
users.initials,
users_info.gender,
users_info.dob,
users_info.address_cur_street1,
users_info.address_cur_street2,
users_info.address_cur_street3,
users_info.address_cur_street4,
users_info.address_cur_city,
users_info.address_cur_state,
users_info.address_cur_pcode,
users_info.address_cur_country,
users_info.address_cur_tel,
users_info.address_cur_tel_mobile,
users_info.address_cur_tel_work,
users_info.address_perm_street1,
users_info.address_perm_street2,
users_info.address_perm_street3,
users_info.address_perm_street4,
users_info.address_perm_city,
users_info.address_perm_state,
users_info.address_perm_pcode,
users_info.address_perm_country,
users_info.address_perm_tel,
users_info.address_perm_tel_mobile,
users_info.address_perm_tel_work,
users_info.ethnicity,
users_info.visastatus,
users_info.company,
users_info.homepage,
users_info.cit_country,
users_info.native_tongue,
users_info.cur_pa_res,
users_info.us_citizen_or_permanent_resident,
users_info.qq_number
FROM lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id 
left outer join users_info on users_info.user_id = lu_users_usertypes.id  
where lu_users_usertypes.id=".$_SESSION['userid'];
//echo $sql;
$result = mysql_query($sql)
or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$email = stripslashes($row['email']);
	$title = stripslashes($row['title']);
	$fName = stripslashes($row['firstname']);
	$mName = stripslashes($row['middlename']);
	$lName = stripslashes($row['lastname']);
	$initials = stripslashes($row['initials']);
	$gender = $row['gender'];
	$dob = formatUSdate($row['dob'],'-','/');
	$street1 = stripslashes($row['address_cur_street1']);
	$street2 = stripslashes($row['address_cur_street2']);
	$street3 = stripslashes($row['address_cur_street3']);
	$street4 = stripslashes($row['address_cur_street4']);
	$city = stripslashes($row['address_cur_city']);
	$state = $row['address_cur_state'];
	$postal = stripslashes($row['address_cur_pcode']);
	$country = $row['address_cur_country'];
	$tel = stripslashes($row['address_cur_tel']);
	$telMobile = stripslashes($row['address_cur_tel_mobile']);
	$telWork = stripslashes($row['address_cur_tel_work']);
	$streetP1 = stripslashes($row['address_perm_street1']);
	$streetP2 = stripslashes($row['address_perm_street2']);
	$streetP3 = stripslashes($row['address_perm_street3']);
	$streetP4 = stripslashes($row['address_perm_street4']);
	$cityP = stripslashes($row['address_perm_city']);
	$stateP = $row['address_perm_state'];
	$postalP = stripslashes($row['address_perm_pcode']);
	$countryP = $row['address_perm_country'];
	$ethnicity = $row['ethnicity'];
	$visaStatus = $row['visastatus'];
    $company = $row['company'];
	$homepage = stripslashes($row['homepage']);
	$countryCit = $row['cit_country'];
    $nativeTongue = $row['native_tongue']; 
	$curResident = $row['cur_pa_res'];
    $usCitizenOrPermanentResident = $row['us_citizen_or_permanent_resident'];
    $qq_number = $row['qq_number'];
}
}

// Include the shared page header elements.
$pageTitle = 'Biographical Information';
$footJavascriptFiles = array('../javascript/applyBio.js');
include '../inc/tpl.pageHeaderApply.php';

$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='Biographical Information' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?>
			

<?php
if ($err) {
    echo '<span class="errorSubtitle">';
    echo $err;    
} elseif ($_SESSION['domainid'] == 42 && $confirmationMessage) {
    echo '<span class="subtitle">';
    echo $confirmationMessage;
}    
?>
</span><br><br>

<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
	<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
</div>
<span class="tblItem">= required</span><br>

<br>
<? showEditText("Save Biographical Info", "button", "btnSubmit", $_SESSION['allow_edit']); ?>

<br>
<br>
<hr size="1" noshade color="#990000">
<table width="100%" border="0" cellspacing="2" cellpadding="2">
    <tr>
      <td colspan="2" class="tblHead2">Personal</td>
    </tr>
    <tr>
      <td colspan="2" valign="top" class="tblItem"><strong>Note: Your name should appear as on official documents (VISA, Passport, Drivers License, etc).<br>Please do NOT type your name in all uppercase letters.<BR><BR></strong>
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <?php
                if (!isPhilosophyDomain($_SESSION['domainid'])) {
            ?>
            <td class="tblItem"><strong>Title:<br>
                <? showEditText($title, "listbox", "txtTitle", $_SESSION['allow_edit'],false,$titles,true,20); ?>
            </strong></td>
            <?php }
            ?>
            <td class="tblItem"><strong><?php echo ($isIniDomain || $isEM2Domain ? 'Given Name:' : 'First Name:'); ?><br>
                <? showEditText($fName, "textbox", "txtFName", $_SESSION['allow_edit'], true,null,true,20); ?>
            </strong></td>
            <td class="tblItem"><strong>Middle Name:
                  <br>
                <? showEditText($mName, "textbox", "txtMName", $_SESSION['allow_edit'],false,null,true,20); ?>
            </strong></td>
            <td class="tblItem"><strong><?php echo ($isIniDomain || $isEM2Domain ? 'Family Name:' : 'Last Name:'); ?><br>
                <? showEditText($lName, "textbox", "txtLName", $_SESSION['allow_edit'], true,null,true,20); ?>
            </strong></td>
          </tr>
<?php
//if ($_SESSION['domainid'] != 42) {
if ($domainname == "REU-SE") { ?>
    <tr>
            <td class="tblItem"><strong>Date of Birth </strong>(MM/DD/YYYY): <br>
                      <? showEditText($dob, "textbox", "txtDob", $_SESSION['allow_edit'], true,null,true,20); ?>                    </td>
            <td class="tblItem"><strong>Sex: </strong><br>
                    <? showEditText($gender, "listbox", "lbGender", $_SESSION['allow_edit'],false, $genders); ?>                    </td>
            <td class="tblItem">&nbsp;</td>
            <td class="tblItem">&nbsp;</td>
          </tr>
<?php }
else if (!$isIsreeDomain) {
?>
          <tr>
            <td class="tblItem"><strong>Date of Birth </strong>(MM/DD/YYYY): <br>
                      <? showEditText($dob, "textbox", "txtDob", $_SESSION['allow_edit'], true,null,true,20); ?>                    </td>
            <td class="tblItem"><strong>Sex: </strong><br>
                    <? 
                        if (isPhilosophyDomain($_SESSION['domainid'])) {
                            showEditText($gender, "listbox", "lbGender", $_SESSION['allow_edit'],false, $genders);
                            echo "<br />You are not required to respond to this question.  We recognize that applicants may not identify with either male or female as a sex.  We sorry that we are unable to present other specific options at this time.  Choosing not to respond to this question will not negatively impact your application."; 
                        } else {
                             showEditText($gender, "listbox", "lbGender", $_SESSION['allow_edit'],true, $genders);   
                            }
                            ?>                    </td>
            <td class="tblItem">&nbsp;</td>
            <td class="tblItem">&nbsp;</td>
          </tr>
<?php
} else {
    echo '<tr><td colspan="4">';
    showEditText($dob, "hidden", "txtDob", $_SESSION['allow_edit'], FALSE,null,true,20);
    showEditText($gender, "hidden", "lbGender", $_SESSION['allow_edit'], FALSE,null,true,20);
    echo '<tr><td>';    
}
?>
          <tr>
            <td class="tblItem"><strong>Email / User ID</strong>:<br>
              <? 
              showEditText($email, "textbox", "txtEmail", false, true,null,false,20); 
              ?>
            </td>
            
            <td class="tblItem">
             <?php
             if ($isIsreeDomain) { 
             ?>
                <strong>Organization </strong>: <br>
             <?php 
                showEditText($company, "textbox", "txtCompany", $_SESSION['allow_edit'], true,null,true,20); 
            } else {
                if ($isRiMsRtChinaDomain)
                {
                ?>
                    <strong>QQ Number</strong>: 
                    <br>    
                <?php
                    showEditText($qq_number, "textbox", "txtQqNumber", $_SESSION['allow_edit'], FALSE, null, true, 20);
                }
                       
                showEditText($company, "hidden", "txtCompany", $_SESSION['allow_edit'], FALSE,null,true,20); 
            }
            ?>
            </td>

            <td class="tblItem">
            <?php showEditText($pass, "hidden", "txtPass", $_SESSION['allow_edit'], FALSE,null,true,20); ?>
            </td>
            <td class="tblItem">
            <?php showEditText($passC, "hidden", "txtPassC", $_SESSION['allow_edit'], FALSE,null,true,20); ?>
            </td>
          </tr>
          </table></td>
    </tr>
    
    <tr>
        <td colspan="2"><hr size="1" noshade color="#990000"></td>
    </tr>
    
    <?php

//if ($_SESSION['domainid'] != 42) {
if (!$isIsreeDomain) { 

    if ($isDesignDomain || $isDesignPhdDomain)
    {
    ?>
        <tr>
            <td colspan="2">
            <strong>Are you a citizen or permanent resident of the United States?</strong>
            </td>
        </tr> 
        <tr>
            <td colspan="2">
            <?php
            $yesNoArray = array(
                array(1, "Yes"),
                array(0, "No")
            );
            showEditText($usCitizenOrPermanentResident, "radiogrouphoriz", "usCitizenOrPermanentResident", $_SESSION['allow_edit'], false, $yesNoArray);
            ?>
            </td>
        </tr> 
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    <?php
    }
    ?>
  
    <tr>           
        <td valign="top" class="tblHead2">Citizenship</td>
        <td valign="top" class="tblHead2">
        <? 
        // PLB added cnbc grad training exemption 8/31/09
        //if($_SESSION['domainname'] != "CNBC" && $_SESSION['domainname'] != "CNBC-Grad-Training")
        if (!$isCnbcDomain)
        { 
        ?>
            Ethnicity/Race
        <? 
        } 
        ?>			  
        </td>
    </tr>
        
    <tr>
        <td colspan="2" valign="top" class="tblItem">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">        
            <tr valign="top">
                <td class="tblItem">
                <strong>Visa Status: <a class="addButton" href="./faq.php" target="_blank">?</a><br>
                  <? 
					if($_SESSION['allow_edit'] == true)
					{
						showEditText($visaStatus, "listbox", "lbStatus", $_SESSION['allow_edit'],true, $statii); 
					}else
					{
						for($i = 0; $i < count($statii); $i++)
						{
							if($visaStatus == $statii[$i][0])
							{
								echo $statii[$i][1];
								break;
							}
						}
					}
				  ?>    
                </strong>
                </td>
                <td class="tblItem" rowspan="3">
                <? 
                // PLB added cnbc grad training exemption 8/31/09
                //if($_SESSION['domainname'] != "CNBC" && $_SESSION['domainname'] != "CNBC-Grad-Training")
                if (!$isCnbcDomain)
                { 
                  
                /*
                * PLB changed ethnicity/race to comply with IPEDs guidelines 7/22/09 
                * see http://nces.ed.gov/statprog/2002/std1_5.asp for more info
                */

                // There is only one ipeds ethnicity category, so just hard code for now. 
                $hispanicYesChecked = '';
                $hispanicNoChecked = '';
                $hispanicDeclineChecked = '';
                if ($ethnicity == 21) {
                    $hispanicYesChecked = "checked";        
                } elseif ($ethnicity == 22) {
                    $hispanicNoChecked = "checked";    
                } elseif ($ethnicity == 1) {
                    $hispanicDeclineChecked = "checked";    
                }
                ?> 
                <div style="margin-left: 5px;">
                <b>Are you Hispanic/Latino?</b><br/>
                <input type="radio" name="ipedsEthnicity" value="21" <?php echo $hispanicYesChecked; ?>> Yes
                <input type="radio" name="ipedsEthnicity" value="22" <?php echo $hispanicNoChecked; ?>> No
                <input type="radio" name="ipedsEthnicity" value="1" <?php echo $hispanicDeclineChecked; ?>> Decline to answer
                <br/><br/>
                <b>You may select one or more races:</b>
                <br/>
                <?
                // Get any previously selected race ids for the applicant.
                $selectedRaceIds = array();
                // Check the db first.
                $applicantIpedsRaceQuery = "SELECT ipeds_race_id FROM applicant_ipeds_race
                                            WHERE lu_users_usertypes_id = " . $_SESSION['userid'];
                $ipedsRaceResult = mysql_query($applicantIpedsRaceQuery);
                while ($row = mysql_fetch_array($ipedsRaceResult)) {
                    $selectedRaceIds[] = $row['ipeds_race_id'];
                }
                // Try the request if there are no db records.
                if (empty($selectedRaceIds)) {
                    if(!empty($ipedsRaces)) {
                        $selectedRaceIds = $ipedsRaces;    
                    }    
                }
                // Create the check boxes using the lookup table.
                $ipedsRacesQuery = "SELECT * FROM ipeds_race ORDER BY sort_order";
                $ipedsRacesResult = mysql_query($ipedsRacesQuery);
                while ($row = mysql_fetch_array($ipedsRacesResult)) {
                    $raceId = $row['ipeds_race_id'];
                    $race = $row['ipeds_race'];
                    $raceChecked = "";
                    if (in_array($raceId, $selectedRaceIds)) {
                        $raceChecked = "checked";
                    }
                    echo '<input type="checkbox" name="ipedsRaces[]" value="' . $raceId . '"';
                    echo ' ' . $raceChecked . '> ' . $race . '<br/>';
                }
                 
                // legacy ethnicity codes are between 0 and 11
                // 1 = "Decline to answer"
                if ($ethnicity > 1 && $ethnicity <= 11) {

                    echo '<br/><br/><strong>Ethnicity (pre-IPEDS):</strong> <br>';
                    /*
                    if($_SESSION['allow_edit'] == true)
	                {
		                showEditText($ethnicity, "listbox", "lbEthnicity", $_SESSION['allow_edit'], true, $ethnicities); 
                    }else
	                { for($i = 0; $i < count($ethnicities); $i++)
                        {
                            if($ethnicity == $ethnicities[$i][0])
                            {
                                echo '<input type="hidden" name="lbEthnicity" value="' . $ethnicities[$i][0] . '">';
                                echo $ethnicities[$i][1];
                                break;
                            }
                        }
                    }
                    */ 
                    // just print out any legacy info
	                for($i = 0; $i < count($legacyEthnicities); $i++)
	                {
		                if($ethnicity == $legacyEthnicities[$i][0])
		                {
			                echo '<input type="hidden" name="lbEthnicity" value="' . $legacyEthnicities[$i][0] . '">';
                            echo $legacyEthnicities[$i][1];
			                break;
		                }
	                }

                }

                } else {
                    if ($ethnicity) {
                        showEditText($ethnicity, "hidden", "ipedsEthnicity", $_SESSION['allow_edit'], FALSE,null,true,20);
                    }
                    $applicantIpedsRaceQuery = "SELECT ipeds_race_id FROM applicant_ipeds_race
                                                WHERE lu_users_usertypes_id = " . $_SESSION['userid'];
                    $ipedsRaceResult = mysql_query($applicantIpedsRaceQuery);
                    while ($row = mysql_fetch_array($ipedsRaceResult)) {
                        echo '<input type="hidden" name="ipedsRaces[]" value="' . $row['ipeds_race_id'] . '">';
                    }
                } 
                ?>
                </div>
                </td>
            </tr>
            <tr valign="top">
                <td >
                <span class="tblItem"><strong>Country of Citizenship:<br>
                <? 
                if($_SESSION['allow_edit'] == true)
                {
                    showEditText($countryCit, "listbox", "lbCountryCit", $_SESSION['allow_edit'],true,$countries); 
                }else
                {
                    for($i = 0; $i < count($countries); $i++)
                    {
                        if($countryCit == $countries[$i][0])
                        {
                            echo $countries[$i][1];
                            break;
                        }
                    }
                }
                ?>
                </strong></span>
                </td>
            </tr>

            <tr valign="top">
                <td >
                <span class="tblItem">
                <a name="native_language"></a>
                <strong>
                <?php
                if ($isDesignDomain || $isDesignPhdDomain)
                {
                ?>
                    Native Language/Mother Tongue:
                <?php
                }
                else
                {
                ?>
                    Native Language:
                <?php    
                }
                ?>
                </strong>
                <br>
                <? showEditText($nativeTongue, "textbox", "txtNativeTongue", 
                    $_SESSION['allow_edit'], true, null, true, 20); ?>
                </span>
                </td>
            </tr>
        </table>
        </td>
    </tr>
            
    <tr>
        <td colspan=2><hr size="1" noshade color="#990000"></td>
    </tr>  

<?php
} else {
    echo '<tr><td colspan="2">';
    showEditText($visaStatus, "hidden", "lbStatus", $_SESSION['allow_edit'], FALSE,null,true,20);
    showEditText($countryCit, "hidden", "lbCountryCit", $_SESSION['allow_edit'], FALSE,null,true,20);
    showEditText($nativeTongue, "hidden", "txtNativeTongue", $_SESSION['allow_edit'], FALSE,null,true,20);
    if ($ethnicity) {
        showEditText($ethnicity, "hidden", "ipedsEthnicity", $_SESSION['allow_edit'], FALSE,null,true,20);
    }
    $applicantIpedsRaceQuery = "SELECT ipeds_race_id FROM applicant_ipeds_race
                                WHERE lu_users_usertypes_id = " . $_SESSION['userid'];
    $ipedsRaceResult = mysql_query($applicantIpedsRaceQuery);
    while ($row = mysql_fetch_array($ipedsRaceResult)) {
        echo '<input type="hidden" name="ipedsRaces[]" value="' . $row['ipeds_race_id'] . '">';
    }
    echo '<tr><td>';    
}


/*
* DIETRICH/INI
* Optional diversity section for english
*/
if (isEnglishDomain($_SESSION['domainid']))
{
?>
    <tr><td colspan="2">
<?php
    include('../inc/dietrichOptionalDiversity.inc.php');
?>    
    </td></tr>
<?php  
}
?>          


    <tr>
        <td class="tblItem">
        <? if($_SESSION['domainname'] == "CompBio"){ ?>
        Are you a Pennsylvania resident? 
        <? showEditText($curResident, "listbox", "lbCurResident", $_SESSION['allow_edit'],true, array(array('0','No'), array('1','Yes')) ); ?>
        <? } ?>			  </td>
        <td ><span class="tblItem"><strong>Use Current Address as Permanent Address :
            <input name="Button" type="button" class="tblItem" value="Yes" onClick="copyAddress();">
        </strong></span>
        </td>
    </tr>
    <tr>
        <td class="tblHead2">Current Address </td> 
        <td class="tblHead2">Permanent Address </td>
    </tr>
    <tr>
        <td width="50%" class="tblItem">
        <table width="100%" border="0" cellpadding="1" cellspacing="0" class="tblItem">
          <tr>
            <td width="100" align="right"><strong>Street 1:</strong></td>
            <td><? showEditText($street1, "textbox", "txtStreet1", $_SESSION['allow_edit'], true,null,true,20,40);
            echo "(No P.O. Box)" ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Street 2:</strong> </td>                                                    
            <td><? showEditText($street2, "textbox", "txtStreet2", $_SESSION['allow_edit'],false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Street 3:</strong> </td>
            <td><? showEditText($street3, "textbox", "txtStreet3", $_SESSION['allow_edit'],false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Street 4:</strong> </td>
            <td><? showEditText($street4, "textbox", "txtStreet4", $_SESSION['allow_edit'],false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>City:</strong></td>
            <td><? showEditText($city, "textbox", "txtCity", $_SESSION['allow_edit'], true,null,true,20); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>State:</strong></td>
            <td><? 
	        if($_SESSION['allow_edit'] == true)
	        {
		        showEditText($state, "listbox", "lbState", $_SESSION['allow_edit'],false, $states); 
		        echo "(If applicable)";
	        }else
	        {
		        for($i = 0; $i < count($states); $i++)
		        {
			        if($state == $states[$i][0])
			        {
				        echo $states[$i][1];
				        break;
			        }
		        }
	        }
	        ?>
            </td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Postal Code: </strong></td>
            <td><? showEditText($postal, "textbox", "txtPostal", $_SESSION['allow_edit'], true,null,true,20); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Country:</strong></td>
            <td><? 
	        if($_SESSION['allow_edit'] == true)
	        {
		        showEditText($country, "listbox", "lbCountry", $_SESSION['allow_edit'],true,$countries); 
	        }
	        else
	        {
		        for($i = 0; $i < count($countries); $i++)
		        {
			        if($country == $countries[$i][0])
			        {
				        echo $countries[$i][1];
				        break;
			        }
		        }
	        }
	        ?>                    
            </td>
          </tr>
          <?php
          if (!$isIniDomain && !$isEM2Domain)
          {
          ?>
          <tr>
            <td width="100" align="right"><strong>Home page:</strong></td>
            <td><? showEditText($homepage, "textbox", "txtHomepage", $_SESSION['allow_edit'],false,null,true,20); ?></td>
          </tr>
          <?php
          }     
          ?>
        </table>
        </td>
        <td valign="top" class="tblItem">
        <table width="100%" border="0" cellpadding="1" cellspacing="0" class="tblItem">
          <tr>
            <td width="100" align="right"><strong>Street 1:</strong></td>
            <td><? showEditText($streetP1, "textbox", "txtStreetP1", $_SESSION['allow_edit'], false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Street 2:</strong> </td>
            <td><? showEditText($streetP2, "textbox", "txtStreetP2", $_SESSION['allow_edit'],false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Street 3:</strong> </td>
            <td><? showEditText($streetP3, "textbox", "txtStreetP3", $_SESSION['allow_edit'],false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Street 4:</strong> </td>
            <td><? showEditText($streetP4, "textbox", "txtStreetP4", $_SESSION['allow_edit'],false,null,true,20,40); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>City:</strong></td>
            <td><? showEditText($cityP, "textbox", "txtCityP", $_SESSION['allow_edit'], false,null,true,20); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>State:</strong></td>
            <td><? 
		    if($_SESSION['allow_edit'] ==  true)
		    {
			    showEditText($stateP, "listbox", "lbStateP", $_SESSION['allow_edit'],false, $states); 
			    echo "(If applicable)";
		    }else
		    {
			    for($i = 0; $i < count($states); $i++)
			    {
				    if($stateP == $states[$i][0])
				    {
					    echo $states[$i][1];
					    break;
				    }
			    }
		    }
		    ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Postal Code: </strong></td>
            <td><? showEditText($postalP, "textbox", "txtPostalP", $_SESSION['allow_edit'], false,null,true,20); ?></td>
          </tr>
          <tr>
            <td width="100" align="right"><strong>Country:</strong></td>
            <td><? 
		    if($_SESSION['allow_edit'] == true)
		    {
			    showEditText($countryP, "listbox", "lbCountryP", $_SESSION['allow_edit'], false, $countries); 
		    }else
		    {
			    for($i = 0; $i < count($countries); $i++)
			    {
				    if($countryP == $countries[$i][0])
				    {
					    echo $countries[$i][1];
					    break;
				    }
			    }
		    }
		    ?>                    
            </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
      <td colspan="2" class="tblItem"><table width="100%"  border="0" cellspacing="0" cellpadding="0" class="tblItem">
        <tr valign="top">
          <td><strong>Preferred Phone:</strong>                    <br>
            <? showEditText($tel, "textbox", "txtTel", $_SESSION['allow_edit'], true,null,true,20); ?>                    <br></td>
          <td><strong>Alternate Phone: 
            </strong><br>
            <? showEditText($telWork, "textbox", "txtTelWork", $_SESSION['allow_edit'], false,null,true,20); ?></td>
          <td><strong>Mobile Phone: 
            </strong><br>
            <? showEditText($telMobile, "textbox", "txtTelMobile", $_SESSION['allow_edit'], false,null,true,20); ?></td>
        </tr>
      </table></td>
    </tr>
           
    <tr>
        <td colspan=2><hr size="1" noshade color="#990000"></td>
    </tr>            
    <tr>
        <td colspan="2" class="tblItem">                  
        <span class="subtitle">
        <? showEditText("Save Biographical Info", "button", "btnSubmit", $_SESSION['allow_edit']); ?>
        </span>                
        <input name="txtMode" type="hidden" id="txtMode" value="<?=$mode;?>">              
        </td>
    </tr>
</table>

<?php 
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';


function isSubmitted($applicationId) {
    $query = "SELECT submitted FROM application WHERE id = " . $applicationId;
    $query .= " LIMIT 1";
    $result = mysql_query($query);
    $isSubmitted = FALSE;
    while ($row = mysql_fetch_array($result)) {
        if ($row['submitted']) {
            $isSubmitted = TRUE;
        }    
    }
    if ($isSubmitted) {
        return TRUE;
    } else {
        return FALSE;    
    }
}

function isPaid($applicationId) {
    $query = "SELECT paid, waive FROM application WHERE id = " . $applicationId;
    $query .= " LIMIT 1";
    $result = mysql_query($query);
    $isPaid = FALSE;
    while ($row = mysql_fetch_array($result)) {
        if ($row['paid'] || $row['waive']) {
            $isPaid = TRUE;
        }    
    }
    if ($isPaid) {
        return TRUE;
    } else {
        return FALSE;    
    }    
}
?>