
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../css/MHCI_style.css" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$NUMRECS = 0; //DEFAULT NUMBER OF REFERENCES
$myReferences = array();
$types = array();
$myPrograms = array();

$applastname = "";
$appfirstname = "";
$appemail = "";
$appdegree = "";
$appfield = "";
$appschool = "";
$enddate = "";
$err = "";
$err_2 = "";
$comp = true;
$sysemail = "applygrad@cs.cmu.edu";

//GET SYSTEM EMAIL
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select sysemail from systemenv where domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{ 
	$sysemail = $row['sysemail'];
}

//GET USER INFO THAT WE'LL NEED
$sql = "SELECT programs.id, 
degree.name as degreename,
fieldsofstudy.name as fieldname, 
choice, 
lu_application_programs.id as itemid,
users.firstname,
users.lastname,
users.email,
department.name as departmentname,
schools.name as schoolname,
programs.linkword
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
inner join application on application.id = lu_application_programs.application_id
inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
inner join users on users.id = lu_users_usertypes.user_id

inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
inner join department on department.id = lu_programs_departments.department_id
inner join schools on schools.id = department.parent_school_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice limit 1";
//echo $sql."<br>";
//echo $_SESSION['userid']."<br>";
$result = mysql_query($sql) or die(mysql_error().$sql);
while($row = mysql_fetch_array( $result )) 
{
	$applastname = stripslashes($row['lastname']);
	$appfirstname = stripslashes($row['firstname']);
	$appemail = stripslashes($row['email']);
	$appdegree = $row['degreename'];
	$appfield = $row['fieldname'];
	$appschool = $row['schoolname'];
	$arr = array();
	array_push($arr, $row['id']);
	array_push($myPrograms, $arr);
	
}

//RETRIEVE USER INFORMATION
//USER MUST HAVE AT LEAST X RECOMMENDES SO ADD IT IF IT DOESN'T EXIST
$tmpNumRecs = 0;
$tmpType = -1;
for($i = 0; $i < count($types); $i++)
{
//echo $types[$i][1]." ".$types[$i][2]. "<br>";
	if($tmpType != $types[$i][1])
	{
		$tmpNumRecs += $types[$i][2];
		$tmpType = $types[$i][1];
	}
	
	
}
//echo $tmpNumRecs;
if($tmpNumRecs > $NUMRECS)
{
	$NUMRECS = $tmpNumRecs;
}

function addRef($allowDup, $type)
{
	global $err;
	
	$sql = "insert into mhci_prereqsReferences(application_id, ref_user_id)values(".$_SESSION['appid'].",-1 ".")";
	mysql_query($sql) or die(mysql_error().$sql);
	
}

//INSERT RECORD TABLE FOR USER 
if(isset($_POST['btnAdd']))
{
	$allowdups = true;
	addRef($allowdups, 3);
}//END POST BTNADD



if( isset( $_POST ) && !isset($_POST['btnAdd']) )
{
	$vars = $_POST;
	$itemId = -1;
	$uid = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnSendmail') !== false ) 
		{
			$arr = split("_", $key);
			$email = "";
			$record = intval($_POST["txtRecord_".$arr[1]]);
			if($_POST["txtUid_".$arr[1]] != "")
			{
				$uid = $_POST["txtUid_".$arr[1]];
				$itemId = $_POST["txtRecord_".$arr[1]];
				$email = $_POST["txtEmail_".$arr[1]];
				$reminderSentCount = intval($_POST["txtRecSent_".$arr[1]]);
			}
			if($reminderSentCount < 3)
			{
				$pass = "";
				$sql = "select users.guid, lu_users_usertypes.user_id, lu_users_usertypes.id from users  
                inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
                where lu_users_usertypes.id=".intval($_POST["txtUid_".$arr[1]]);
				$result = mysql_query($sql)or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					$pass = $row['guid'];
                    $accesscode = $row['user_id']."-".$_SESSION['domainid']."-".$row['id'];
				}
				$vars = array(
				array('email',$email), 
				array('user',str_replace("+","%2b",$email)),	
				array('pass',$pass),	
				array('enddate',formatUSdate3($_SESSION['expdate']) ),
				array('replyemail',$sysemail),
				array('lastname',$applastname),
				array('firstname',$appfirstname),
				array('appemail',$appemail),
				array('degree',$appdegree),
				array('field',$appfield),
				array('school',$appschool),
				array('domainid',$_SESSION['domainid']),
                array('accesscode',$accesscode)
				);
				sendRefMail(1,$vars, $email);
				$reminderSentCount++;
				$sql = "update mhci_prereqsReferences set reminder_sent_count=".$reminderSentCount. " where id=".$itemId;
				mysql_query($sql)	or die(mysql_error().$sql);
			}else
			{
				$err .= "You cannot send more than 3 reference reminders.<br>";
				$err_2 .= "You cannot send more than 3 reference reminders.<br>";
			}
		}
		if( strstr($key, 'btnSendReminder') !== false ) 
		{
			$arr = split("_", $key);
			$email = "";
			$record = intval($_POST["txtRecord_".$arr[1]]);
			if($_POST["txtUid_".$arr[1]] != "")
			{
				$uid = $_POST["txtUid_".$arr[1]];
				$itemId = $_POST["txtRecord_".$arr[1]];
				$email = $_POST["txtEmail_".$arr[1]];
				$reminderSentCount = intval($_POST["txtRecSent_".$arr[1]]);
			}
			if($reminderSentCount < 3)
			{
				$pass = "";
				$sql = "select guid, lu_users_usertypes.user_id, lu_users_usertypes.id from users 
				inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
				where lu_users_usertypes.id=".intval($_POST["txtUid_".$arr[1]]);
				$result = mysql_query($sql)or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					$pass = $row['guid'];
                    $accesscode = $row['user_id']."-".$_SESSION['domainid']."-".$row['id'];
				}
				$vars = array(
				array('email', $email), 
				array('user',str_replace("+","%2b",$email)),
				array('pass',$pass),
				array('enddate',formatUSdate3($_SESSION['expdate']) ),
				array('replyemail',$sysemail),
				array('lastname',$applastname),
				array('firstname',$appfirstname),
				array('appemail',$appemail),
				array('degree',$appdegree),
				array('field',$appfield),
				array('school',$appschool),
				array('username',$email),
				array('domainid',$_SESSION['domainid']),
                array('accesscode',$accesscode)
				);
				sendRefMail(2,$vars, $email);
				$reminderSentCount++;
				$sql = "update mhci_prereqsReferences set reminder_sent_count=".$reminderSentCount. " where id=".$itemId;
				mysql_query($sql)	or die(mysql_error().$sql);
			}else
			{
				$err .= "You cannot send more than 3 reference reminders.<br>";
				$err_2 .= "You cannot send more than 3 reference reminders.<br>";
			}
		}
		if( strstr($key, 'btnDel') !== false ) 
		{
			$arr = split("_", $key);
			$umasterid = -1;
			if($_POST["txtUid_".$arr[1]] != "")
			{
				$uid = $_POST["txtUid_".$arr[1]]; 
				$umasterid = $_POST["txtUMasterid_".$arr[1]]; 
			}else
			{
				$err .= "record not found.";
			}
			
			$sql = "DELETE FROM mhci_prereqsReferences WHERE application_id=".$_SESSION['appid']. " and ref_user_id=" . $uid;
			mysql_query($sql) or die(mysql_error().$sql );
			
			//CHECK TO SEE IF THIS RECOMMENDER IS ASSIGNED TO ANYONE ELSE. IF NOT, REMOVE RECOMMENDER IDENTITY ONLY
			$sql = "select id from mhci_prereqsReferences where ref_user_id=".$uid;
			$result = mysql_query($sql)	or die(mysql_error());
			$doDelete = true;
			while($row = mysql_fetch_array( $result )) 
			{
				$doDelete = false;
			}
            $sql = "select id from recommend where rec_user_id=".$uid;
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $doDelete = false;
            }
			if($doDelete == true)
			{
				$sql = "DELETE FROM  lu_users_usertypes WHERE id=".$uid. " and usertype_id=6";
				mysql_query($sql) or die(mysql_error().$sql );
				if(mysql_affected_rows() > 0)
				{
					$sql = "DELETE FROM usersinst WHERE user_id=".$uid;
					mysql_query($sql) or die(mysql_error().$sql );
					
					$sql = "DELETE FROM  users_info WHERE user_id=".$uid;
					mysql_query($sql) or die(mysql_error().$sql );
					$updateApp = true;
					
					//SEE IF THERE ARE OTHER ACCOUNT TYPES ASSIGNED TO THIS USER. IF NOT, DELETE USER.
					$sql = "select id from lu_users_usertypes where user_id=".$_SESSION['usermasterid'];
					$result = mysql_query($sql)	or die(mysql_error());
					if(mysql_num_rows($result) == 0)
					{
						$sql = "DELETE FROM  users WHERE id=".$umasterid;
						mysql_query($sql) or die(mysql_error().$sql );
					}else
					{
						while($row = mysql_fetch_array( $result )) 
						{
							//echo $row['id'. "<br>"];
						}
					}
				}
			}//end if dodelete
		}//end btnDel
	}//END FOR
}//END IF


//UPDATE
if( isset($_POST['btnSave'])) 
{
	$vars = $_POST;
	$itemId = -1;
	$tmpid = -1;
	$arrItemId = array();
        $err_2 = "";
	
	
	//HERE WE GET THE RECORD IDS FOR THE UPDATED RECOMMENDERS
	foreach($vars as $key => $value)
	{
		$arr = split("_", $key);
		if($arr[0] == "lbType")
		{
			$tmpid = $arr[1];
			if($itemId != $tmpid)
			{
				$itemId = $tmpid;
				array_push($arrItemId,$itemId);
			}
		}
	}//END FOR
	//ITERATE THROUGH IDS AND DO UPDATES
	for($i = 0; $i < count($arrItemId); $i++)
	{
		$type = 1;
		$sendmail = 0;
		$reminderCount = 0;
		$pass = "";
		$guid = "";
		
		$umasterid = $_POST["txtUMasterid_".$arrItemId[$i]];
		$uid = $_POST["txtUid_".$arrItemId[$i]];
		$firstname= htmlspecialchars($_POST["txtFname_".$arrItemId[$i]]);
		$lastname= htmlspecialchars($_POST["txtLname_".$arrItemId[$i]]);
		$title= $_POST["txtTitle_".$arrItemId[$i]];
		$affiliation= $_POST["txtAffiliation_".$arrItemId[$i]];
		$email= stripslashes($_POST["txtEmail_".$arrItemId[$i]]);
		$phone= htmlspecialchars($_POST["txtPhone_".$arrItemId[$i]]);
		if(isset($_POST["lbType_".$arrItemId[$i]]))
		{
			$type = intval($_POST["lbType_".$arrItemId[$i]]);
		}
		if(isset($_POST["chkSendmail_".$arrItemId[$i]]))
		{
			$sendmail = intval($_POST["chkSendmail_".$arrItemId[$i]]);
		}
		if(isset($_POST["txtRemSent_".$arrItemId[$i]]))
		{
			$reminderCount = intval($_POST["txtRemSent_".$arrItemId[$i]]);
		}
		
		//DO VERIFICATION
        
		if($firstname == "")
		{
			$err = "Reference ".($i+1)." is incomplete. First name is required<br>";
		}
		if($lastname == "")
		{
			$err = "Reference ".($i+1)." is incomplete. Last name is required<br>";
		}
	//	if($title == "")
	//	{
	//		$err = "Recommender ".($i+1)." is incomplete. Title is required<br>";
	//	}
		if($email == "")
		{
			$err = "Reference ".($i+1)." is incomplete. Email is required<br>";
		}
		if($affiliation == "")
		{
			$err = "Reference ".($i+1)." is incomplete. Company is required<br>";
		}
		if($phone == "")
		{
			$err = "Reference ".($i+1)." is incomplete. Phone is required<br>";
		}
		
		if($email == "")
		{
			$err = "Reference ".($i+1)." is incomplete. A valid email is required<br>";
		}else
		{
			if(check_email_address($email)===false)
			{
				$err = "Reference ".($i+1)." is incomplete. A valid email is required<br>";
			}
		}
		

		//POPULATE ARRAY FOR POSTBACK
		$arr = array();
		array_push($arr, $arrItemId[$i]);
		array_push($arr, $_POST["txtUid_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtFname_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtLname_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtTitle_".$arrItemId[$i]] );
		array_push($arr, $_POST["txtAffiliation_".$arrItemId[$i]] );
		array_push($arr, $_POST["txtEmail_".$arrItemId[$i]]);
		array_push($arr, $_POST["txtPhone_".$arrItemId[$i]]);
		//  array_push($arr, $_POST["lbType_".$arrItemId[$i]]);
		if(isset($_POST["txtDatafile_".$arrItemId[$i]])){array_push($arr, $_POST["txtDatafile_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtModdate_".$arrItemId[$i]])){array_push($arr, $_POST["txtModdate_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtSize_".$arrItemId[$i]])){array_push($arr, $_POST["txtSize_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtExtension_".$arrItemId[$i]])){array_push($arr, $_POST["txtExtension_".$arrItemId[$i]]);}else{array_push($arr,"");}
		array_push($arr, $_POST["txtRecSent_".$arrItemId[$i]]);
		if($err == "")
		{
			array_push($arr, 0);//NO ERROR
		}else
		{
			array_push($arr, 1);//HAS ERROR
		}
		array_push($arr, $_POST["txtUMasterid_".$arrItemId[$i]]);
    //    array_push($arr, $row['content']);
		array_push($myReferences, $arr);


		// New ERROR Messaages
		if ($err != "")
		{
		  $err_2 .= "<BR>ERROR: Reference ".($i+1). " information is incomplete or invalid. Please verify all data has been provided and is correct.<br>";
		}
		if ( $myReferences[$i][12] == 0) 
		{  
		   if($myReferences[$i][6] != "") // Is there an email address?
		   {
				 $err_2 .= "NOTE: Email request has NOT been sent to Reference ".($i+1)."<BR>";
		   }
		}


		/////////////
		//INSERT DATA
		/////////////
		if($err == "")
		{
			$umasterid = -1;
			//LOOKUP USER
			//echo "looking up user ";
			$sql = "select id from users where users.firstname='".addslashes($firstname)."' and users.lastname='".addslashes($lastname)."' and users.email='".addslashes($email)."'";
			$result = mysql_query($sql)or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$umasterid = $row['id'];
				//echo "record found " . $row['id'];
			}
			//IF USER DOESN'T EXIST, ADD
			if($umasterid == -1)
			{
				$pass = STEM_GeneratePassword();
				$guid = makeGuid();
				$sql = "insert into users(email, password,title,firstname,lastname, signup_date, verified, guid) 
				values( '".addslashes($email)."', '".$pass."', '".addslashes($title)."', '".addslashes($firstname)."', '".addslashes($lastname)."', '".date("Y-m-d h:i:s")."', 0, '".$guid."')";
				$result = mysql_query($sql)or die(mysql_error());
				$umasterid = mysql_insert_id();
			}
			//FIND EXISTING RECOMMENDATION ACCOUNT FOR USER (IF EXISTS)
			$sql = "select lu_users_usertypes.id, users.id as umasterid from users 
				inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
				where lu_users_usertypes.user_id=".$umasterid . " and lu_users_usertypes.usertype_id=6";
			$result = mysql_query($sql) or die(mysql_error(). $sql);
			while($row = mysql_fetch_array( $result )) 
			{
				$uid = $row['id'];
				//echo "found user type ";
			}
			//INSERT RECOMMENDATION RECORD IF IT DOESN'T EXIST 
			if($uid == -1)
			{
				$sql = "insert into lu_users_usertypes(user_id, usertype_id,domain)values( ".$umasterid.", 6, NULL)";
				mysql_query( $sql) or die(mysql_error());
				$uid = mysql_insert_id();
				//echo "inserting reference id ". $uid. "<br>";
			}
			//FIND USERINFO FOR USER
			$doInsert = true;
			$sql = "select id from users_info where user_id=".$uid;
			$result = mysql_query($sql)or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$doInsert = false;
			}
			if($doInsert == true)
			{
				$sql = "insert into users_info(user_id,company, address_cur_tel,address_perm_tel)values(".$uid.",'".addslashes($affiliation)."'	,'".addslashes($phone)."',	'".addslashes($phone)."')";
				mysql_query( $sql) or die(mysql_error());
				//echo "inserting userinfo<br>";
			}
			//UPDATE RECOMMENDATION RECORD
			$sql = "update mhci_prereqsReferences 
			set application_id = ".$_SESSION['appid'].", 
			ref_user_id =".$uid." 
			where id=".$arrItemId[$i];
			mysql_query( $sql) or die(mysql_error());
			
			//GET DATAFILE INFO FOR RECOMMENDATION
			$sql = "select datafile_id from mhci_prereqsReferences where id=".$arrItemId[$i];
			$result = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows( $result ) == 0)
			{
				//$comp = false;
			}
			while($row = mysql_fetch_array( $result )) 
			{
				if($row['datafile_id'] > 0)
				{
					$letter = "(Reference Received)";
				}
			}
		//	updateReqComplete("recommendations.php", 1, 1);
			//header("Location: home.php");
		}
	}//end for
	$tmpRecs = getRefInfo();
	for($i = 0; $i < count($tmpRecs); $i++)
	{
		for($j = 0; $j < count($myReferences); $j++)
		{
			if($tmpRecs[$i][0] == $myReferences[$j][0])
			{
				if($myReferences[$j][14] == 0)
				{
					$myReferences[$j] = $tmpRecs[$i];
				}
			}
		}
		
	}
		
	
}//end if btnsave


$doAdd = true;
$sql = "select id from mhci_prereqsReferences where application_id = ".$_SESSION['appid'] ;
$result = mysql_query($sql)	or die(mysql_error());
$count = mysql_num_rows( $result );
for($i = $count; $i < $NUMRECS; $i++)
{
	addRef(false, 1);
}


if(!isset($_POST['btnSave']))
{
	$myReferences = getRefInfo();
}//end if btnSave

for($i = 0; $i < count($myReferences); $i++)
{
	if($myReferences[$i][2] == "")
	{
		$comp = false;
	}
}
if(count($myReferences) >=  $NUMRECS && $comp == true && $err == "")
{
	
	if($comp == true)
	{
		if(isset($_POST['btnSave']))
		{
		//	updateReqComplete("recommendations.php", 1);
		}else
		{
		//	updateReqComplete("recommendations.php", 1, 1);
		}
	}
	
}else
{
//	updateReqComplete("recommendations.php", 0,1, 0);
}

function getRefInfo()
{
	
	$ret = array();
	$letter = "";
	//RETRIEVE USER INFORMATION
	$sql = "SELECT 
	mhci_prereqsReferences.id,
	mhci_prereqsReferences.ref_user_id,
	mhci_prereqsReferences.datafile_id,
	mhci_prereqsReferences.submitted,
    mhci_prereqsReferences.content,
	users.id as usermasterid,
	users.title,
	users.firstname,
	users.lastname,
	users.email,
	users_info.address_perm_tel, 
	lu_users_usertypes.id as uid,
	users_info.company,
	reminder_sent_count,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM mhci_prereqsReferences
	left outer join lu_users_usertypes on lu_users_usertypes.id = mhci_prereqsReferences.ref_user_id
	left outer join users on users.id = lu_users_usertypes.user_id
	left join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join datafileinfo on datafileinfo.id = mhci_prereqsReferences.datafile_id
	where application_id=".$_SESSION['appid']. " order by mhci_prereqsReferences.id";
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['ref_user_id']);
		array_push($arr, stripslashes($row['firstname']));
		array_push($arr, stripslashes($row['lastname']));
		array_push($arr, stripslashes($row['title']));
		array_push($arr, stripslashes($row['company']));
		array_push($arr, stripslashes($row['email']));
		array_push($arr, stripslashes($row['address_perm_tel']));
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($arr,$row['reminder_sent_count']);
		array_push($arr, 0);//ARRAY INFO
		array_push($arr, $row['usermasterid']);
        array_push($arr, $row['content']);
		array_push($ret, $arr);
	}
	
	return $ret;
}

function sendRefMail($type,$vars, $email)
{
	$str = "";
        $clean_str = "";
	global $domainid;
	global $sysemail;
	global $appfirstname;
	global $applastname;
	switch($type)
	{
		case 1:
			$sql = "select content from content where name='Reference Letter' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$str = $row["content"];
			}
			$str = parseEmailTemplate2($str, $vars );
                        /* This cleans the string of the html representation of & */
                        /* which is causing problems for recommenders accessing the site */
                        /* needs to be fixed in the mail template tinyMCE tool */
                        $clean_str = str_replace("&amp;", "&", $str);
			sendHtmlMail($sysemail, $email, "Reference requested for ".$appfirstname." " . $applastname, $clean_str, "reference");
		break;
		case 2:
			//GET TEMPLATE CONTENT
			$sql = "select content from content where name='Reference Reminder Letter' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				$str = $row["content"];
			}
			$str = parseEmailTemplate2($str, $vars );
                        /* This cleans the string of the html representation of & */
                        /* which is causing problems for recommenders accessing the site */
                        /* needs to be fixed in the mail template tinyMCE tool */
                        $clean_str = str_replace("&amp;", "&", $str);
			sendHtmlMail($sysemail, $email, "Reference reminder for ". $appfirstname." ".$applastname, $clean_str, "refreminder");
		break;
	}
}


?>
<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			<!-- InstanceBeginEditable name="EditNav" -->
            <tr>
            <td>
                <table>
                <colgroup>
                <col width=80% style=text-align:left;>
                <col width=20% style=:right;>
                </colgroup>
                <tr>
            <td class=tblItem>
            
            </td>
            
            <td style="text-align:right; width:130px">    
         
                <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                ){ ?>
                    <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                    <!-- DAS Removed - <? echo $sesEmail;?>   -->
                    <? }else
                        {
                            if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                            && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                            {
                            //session_unset();
                            //destroySession();
                            //header("Location: index.php");
                        ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                }
            } ?>
            </td>
            <tr>
            </table>
            </td>
            </tr>
            <!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" -->
            <div class="title">Statistics References</div>
            
            <?php makeBackButton(); ?>
            
            <br /><br /><br />
            <div class="bodyText">
            In very rare cases, a student may have no coursework in Statistics, 
            but have on-the-job training. If you would like to submit a reference 
            to someone who can attest to your knowledge of multi-way ANOVA and/or 
            multi-factor regression, you may add that person's information here.
            </div>
            <br />
            <!-- InstanceEndEditable -->
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<span class="tblItem">
			<?
			$domainid = 1;
			if(isset($_SESSION['domainid']))
			{
				if($_SESSION['domainid'] > -1)
				{
					$domainid = $_SESSION['domainid'];
				}
			}
			$sql = "select content from content where name='References' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				echo html_entity_decode($row["content"]);
			}
			?>
			</span><br><br>
<?                      // show status if entering page without result of a SAVE                         
                        if( !isset($_POST['btnSave'])) 
                        {
  			  for($i = 0; $i < count($myReferences); $i++)
                          {
                            // has a recommendation been sent and email address present
                            if($myReferences[$i][12] == 0 && $myReferences[$i][6] != "" ) 
                            {
                                $err_2 .= "NOTE: Email request has NOT been sent to Reference ".($i+1)."<BR>";
                            }
                          }
                        }
?>

<!--			<span class="subtitle"><?=$err." ";?></span> -->
			<span class="errorSubtitle"><?=$err_2." ";?></span>
            
			<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
			<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
			</div><span class="tblItem">= required</span><br>
			<br>

            <span class="subtitle">
            <? 
                showEditText("Save References", "button", "btnSave", $_SESSION['allow_edit']);
                                 ?>
            </span>
			<? 
                    showEditText("Add Reference", "button", "btnAdd", $_SESSION['allow_edit']); ?>
            <br>
            <br>
            <hr size="1" noshade color="#990000">

			
			
            
			<?
                
			include_once '../apply/mhci_referencesDisplay.php'; 
           //  end the buckley test    
			?>
            <span class="tblItem"><span class="subtitle">
            <? 
                showEditText("Save References", "button", "btnSave", $_SESSION['allow_edit']);
                                ?>
            </span></span><br>
            <br>
            <br>
<!-- InstanceEndEditable --></div>
            
            <!-- InstanceBeginEditable name="EditNav2" -->
			
            <?php makeBackButton(); ?>
            
            <br style="clear:left">
            <!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>

<?php

function makeBackButton() {
    
    echo <<<EOB

    <button class="bodyButton-right" onClick="parent.location='mhci_prereqs_statistics.php';return false;">
    Return to statistics prerequisite
    </button>
    
EOB;
    
}


?>
