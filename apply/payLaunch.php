
<html>
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$email = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['email']))
{
	$email = $_SESSION['email'];
}
?>
<head>
<title>SCS Graduate Online Application</title>

<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<?

$err = "";
$states = array();
$countries = array();
$months = array();
$years = array();
$amt = totalCost($_SESSION['appid']);
$ptype = "";
$customer_firstname = "";
$customer_lastname = "";
$customer_email = "";
$bill_address1 = "";
$bill_city = "";
$bill_state = "";
$bill_zip = "";
$bill_country = "";
$customer_phone = "";
$card_type = "";
$customer_cc_number = "";
$customer_cc_expmo = "";
$customer_cc_expyr = "";
$customer_cc_code = "";

$thankyouurl = "https://".$_SERVER['HTTP_HOST'] . "/apply/payccthanks.php";
$gLstr = "";

$poststr = "";

for($i = 1; $i < 13; $i++)
{
	$arr = array($i, $i);
	array_push($months, $arr);
}
for($i = 0; $i < 10; $i++)
{
	$k = date("Y")+$i;
	$arr = array($k, $k);
	array_push($years, $arr);
}

if(isset($_GET['btnCancel']))
{
	header("Location: home.php");
}
if(isset($_GET['customer_firstname']))
{
		$customer_firstname = htmlspecialchars($_GET['customer_firstname']);
		$customer_lastname = htmlspecialchars($_GET['customer_lastname']);
		$customer_email = htmlspecialchars($_GET['customer_email']);
		$bill_address1 = htmlspecialchars($_GET['bill_address1']);
		$bill_city = htmlspecialchars($_GET['bill_city']);
		$bill_state = htmlspecialchars($_GET['bill_state']);
		$bill_zip = htmlspecialchars($_GET['bill_zip']);
		$bill_country = htmlspecialchars($_GET['bill_country']);
		$customer_phone = htmlspecialchars($_GET['customer_phone']);
		$card_type = htmlspecialchars($_GET['card_type']);
		$customer_cc_number = htmlspecialchars($_GET['customer_cc_number']);
		$customer_cc_expmo = htmlspecialchars($_GET['customer_cc_expmo']);
		$customer_cc_expyr = htmlspecialchars($_GET['customer_cc_expyr']);
		$customer_cc_code = htmlspecialchars($_GET['customer_cc_code']);
		$gLstr = htmlspecialchars($_GET['item1_gl_str']);
}
if(isset($_GET['txtCredit']))
{
	if(isset($_GET['txtCredit']) && $ptype == "Credit Card")
	{

		if($customer_firstname == "")
		{
			$err .= "First name on card is required<br>";
		}
		if($customer_lastname == "")
		{
			$err .= "Last name on card is required<br>";
		}
		if($bill_address1 == "")
		{
			$err .= "Address is required<br>";
		}
		if($bill_city == "")
		{
			$err .= "City is required<br>";
		}
		if($bill_state == "")
		{
			$err .= "State is required<br>";
		}
		if($bill_zip == "")
		{
			$err .= "Postal Code is required<br>";
		}
		if($bill_country == "")
		{
			$err .= "Country is required<br>";
		}
		if($customer_phone == "")
		{
			$err .= "Phone number is required<br>";
		}
		if($card_type == "")
		{
			$err .= "Card type is required<br>";
		}
		if($customer_cc_number == "")
		{
			$err .= "Card number is required<br>";
		}
		if($customer_cc_expmo == "")
		{
			$err .= "Expiration month is required.<br>";
		}
		if($customer_cc_expyr == "")
		{
			$err .= "Expiration year is required.<br>";
		}
		if($customer_cc_code == "")
		{
			$err .= "Security code is required<br>";
		}

		if($err == "")
		{
			$poststr = "<script language='javascript'>document.form1.action='payccconfirm.php'; document.form1.submit(); </script>";
		}
	}

}


?>


</head>
<link rel="stylesheet" href="../app.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
        <form name="form1" method="post" action="">
          <span class="errorSubtitle">
		  <?=$err?>
          <? showEditText($amt, "hidden", "amt", $_SESSION['allow_edit'],true); ?>
          <? showEditText("10530", "hidden", "store_no", $_SESSION['allow_edit']); ?>
          <? showEditText($thankyouurl, "hidden", "return_url", $_SESSION['allow_edit']); ?>
          <? showEditText("Return to Online Admissions", "hidden", "return_url_text", $_SESSION['allow_edit']); ?>
          <? showEditText($_SESSION['appid'], "hidden", "merchant_ref_no", $_SESSION['allow_edit']); ?>
          <? showEditText("Y", "hidden", "settle_now", $_SESSION['allow_edit']); ?>
          <? showEditText($paymentEmail, "hidden", "notify_email", $_SESSION['allow_edit']); ?>
          <? showEditText("Graduate Online Application Fee", "hidden", "item1_name", $_SESSION['allow_edit']); ?>
          <? showEditText(1, "hidden", "item1_qty", $_SESSION['allow_edit']); ?>
          <? showEditText($amt, "hidden", "item1_price_each", $_SESSION['allow_edit']); ?>
          <? showEditText($gLstr, "hidden", "item1_gl_str", $_SESSION['allow_edit']); ?>
          <? showEditText($customer_firstname, "textbox", "customer_firstname", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($customer_lastname, "textbox", "customer_lastname", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($customer_email, "textbox", "customer_email", $_SESSION['allow_edit'],false,null,true,20); ?>
          <? showEditText($bill_address1, "textbox", "bill_address1", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($bill_city, "textbox", "bill_city", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($bill_state, "listbox", "bill_state", $_SESSION['allow_edit'],true, $states); ?>
          <? showEditText($bill_zip, "textbox", "bill_zip", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($bill_country, "listbox", "bill_country", $_SESSION['allow_edit'],true,$countries); ?>
          <? showEditText($customer_phone, "textbox", "customer_phone", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($card_type, "listbox", "card_type", $_SESSION['allow_edit'],true, $cardTypes); ?>
          <? showEditText($customer_cc_number, "textbox", "customer_cc_number", $_SESSION['allow_edit'], true,null,true,20); ?>
          <? showEditText($customer_cc_expmo, "textbox", "customer_cc_expmo", $_SESSION['allow_edit'], true); ?>
          <? showEditText($customer_cc_expyr, "textbox", "customer_cc_expyr", $_SESSION['allow_edit'], true); ?>
          <? showEditText($customer_cc_code, "textbox", "customer_cc_code", $_SESSION['allow_edit'], true); ?>
          <?=$poststr?>
     </form>
        </body>
</html>
