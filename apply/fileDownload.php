<?php
// Standard includes.
include_once '../inc/session.php';
/* session.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/

// Include file download handler.
include '../classes/class.FileDownload.php';

// Determine the name of the datafile being requested.
$file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

// Determine the guid of the datafile being requested.
$guid = filter_input(INPUT_GET, 'guid', FILTER_SANITIZE_STRING);

// Determine the requester's luuId. 
$requesterLuuId = NULL;
if ( isset($_SESSION['userid']) &&  $_SESSION['userid'] > 0) {
    $requesterLuuId = $_SESSION['userid'];
}

// Determine the the requester's current applicationId.
$requesterApplicationId = NULL;
if ( isset($_SESSION['appid']) &&  $_SESSION['appid'] > 0) {
    $requesterApplicationId = $_SESSION['appid'];
}

// Determine the requester's usertype.
$requesterUsertypeId = NULL;
if ( isset($_SESSION['usertypeid']) && $_SESSION['usertypeid'] > 0 ) {
    $requesterUsertypeId = $_SESSION['usertypeid'];
}

// Exit if fileName, guid, luuId, and usertype cannot be determined.
if ( !($file && $guid && $requesterLuuId && $requesterUsertypeId) ) {
    exit;
}

// Instantiate a new file download with the requested datafile id.
try {
    $download = new FileDownload(urldecode($file), $guid);    
} catch (Exception $e) {
    if ($e->getCode() == 404) {
        header('HTTP/1.0 404 Not Found');
        echo 'File ' . $file . ' not found.';
        exit;
    }    
}

// Only serve files to their owners.
// Don't permit applicant to see recommendations, merge files. 
$fileLuuId = $download->fileLuuId;
$fileApplicationId = $download->fileApplicationId;
$fileType = $download->fileType; 

$doDownload = FALSE;
if ($fileLuuId == $requesterLuuId 
    && $fileType != 'recommendation' && $fileType != 'merged') {                          
    
    // Applicant requesting own file, luuId match.
    $doDownload = TRUE;
        
} elseif ($fileApplicationId == $requesterApplicationId 
            && $fileType != 'recommendation' && $fileType != 'merged') {

    // Applicant requesting own file, applicationId match.
    // (User ID may not match if an admin uploaded the file.)
    $doDownload = TRUE;

} elseif($fileType == 'recommendation' && $requesterUsertypeId == 6) {    

    // Recommender requesting own file
    $datafileinfoId = $download->datafileinfo['id'];
    $fileRecommenderLuuId = NULL;
    $recommendQuery = "SELECT rec_user_id FROM recommend WHERE datafile_id = " . $datafileinfoId;
    $recommendResult = mysql_query($recommendQuery);
    while ( $row = mysql_fetch_array($recommendResult) ) {
        $fileRecommenderLuuId = $row['rec_user_id'];    
    }
    
    if ($fileRecommenderLuuId == $requesterLuuId) {
        $doDownload = TRUE;    
    }
}

// Download or return 403/404 error.
if ($doDownload) {
    
    echo $download->serve();
    
} else {
    
    /*
    header("HTTP/1.0 403 Forbidden");
    echo "You are not permitted to view this file.";
    */
    header("HTTP/1.0 404 Not Found");
    echo "File not found.";
    exit;    
}

?>