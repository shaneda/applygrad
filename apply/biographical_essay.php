<?php
    include_once '../inc/config.php'; 
    include_once '../inc/session.php'; 
    include_once '../inc/db_connect.php';
    $exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
    include_once '../inc/functions.php';
    include_once '../inc/prev_next.php';
    include_once '../inc/applicationFunctions.php';
    include_once '../inc/specialCasesApply.inc.php';
    include_once '../apply/header_prefix.php'; 
    $_SESSION['SECTION']= "1";
    $domainname = "";
    $domainid = -1;
    $sesEmail = "";
    if(isset($_SESSION['domainname']))
    {
        $domainname = $_SESSION['domainname'];
    }
    if(isset($_SESSION['domainid']))
    {
        $domainid = $_SESSION['domainid'];
    }
    if(isset($_SESSION['email']))
    {
        $sesEmail = $_SESSION['email'];
    }

    $id = -1;
    $err = "";
    $bioEssayName = "";
    $bioEssaySize = "";
    $bioEssayModDate = "";
    $bioEssayModFileId = -1;

    //RETRIEVE USER INFORMATION
    $sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=29";
    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result )) 
    {
        $bioEssayName  = "biographicalessay.".$row['extension'];
        $bioEssaySize  = $row['size'];
        $bioEssayModFileId = $row['id'];
        $bioEssayModDate = $row['moddate'];
    } 

    // used for finding the previous and next pages
    $appPageList = getPagelist($_SESSION['appid']);
    $curLocationArray = explode ("/", $currentScriptName);
    $curPageIndex = array_search (end($curLocationArray), $appPageList);
    if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
    {
        $prevPage = "";
    }
    if (sizeof ($appPageList) > $curPageIndex + 1)
    {
        $nextPage =  $appPageList[$curPageIndex + 1]; 
    } 
    else
    {
        $nextPage = "";
    }

    // Include the shared page header elements.
    $pageTitle = 'Biographical Essay';
    include '../inc/tpl.pageHeaderApply.php';         
?>

<span class="tblItem">

    <?
        $domainid = 1;
        if(isset($_SESSION['domainid']))
        {
            if($_SESSION['domainid'] > -1)
            {
                $domainid = $_SESSION['domainid'];
            }
        }

        $sql = "select content from content where name='Biographical Essay' and domain_id=".$domainid;
            
        $result = mysql_query($sql)	or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            echo html_entity_decode($row["content"]);
        }
    ?>
    <br>
    <span class="errorSubTitle"><?=$err;?></span>
    <hr size="1" noshade color="#990000">
</span>

<!--
<span class="subtitle">Biographical Essay</span>
-->
<span class="tblItem"><br>
    <? $qs = "?id=1&t=29"; ?>
    <input class="tblItem" name="btnUpload" value="Upload Biographical Essay" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <? if($bioEssayModDate != "")
        {
            showFileInfo($bioEssayName, $bioEssaySize, formatUSdate($bioEssayModDate), getFilePath(29, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
        }//end if ?>
</span>
<br>
<br>


<?php
    // Include the shared page footer elements. 
    include '../inc/tpl.pageFooterApply.php';
?>