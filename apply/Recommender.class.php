<?php
if ( !isset($_SESSION['usertypeid']) ) {
    session_start();    
}
require_once("../phplib/AjaxCore/AjaxCore.class.php");  // first we include the AjaxCore class
require_once("RecommenderData.class.php");

class Recommender extends AjaxCore
{        
     function Recommender()
     {
        $this->setup();
        parent::AjaxCore();
     }

     function setup()
     {
        $this->setMethod("POST");
        $this->setCurrentFile("Recommender.class.php");
        $this->setCache(false);
        $this->setDebug(false);
        $this->setUpdating("Updating");
        $this->setPlaceHolder("resultsDiv");
     }
     
     function checkUsercode()
     {
         $user=$this->request['usercode'];
         $code=array(); // define an array to store our JavaScript code;
         $ucode = splitUserCode($user);       // $this->splitUserCode($this->getUCode());
         $validCode = usercodeValidP($ucode);
         
         if ($validCode) {
                    $code[]=$this->htmlEnable("button1"); // disable register button 
                    $code[]=$this->htmlInner("resultsDiv","");  
            }
            else {
                $code[]=$this->htmlDisable("button1"); // disable register button
                $code[]=$this->htmlInner("resultsDiv","<font color='red'>Invalid User Code</font>");  // sets some text on the on div "resultsDiv" 
            }
         echo $this->arrayToString($code); // converts our array into a printable JavaScript string 
     }

     function checkUsername()
     {
         $user=$this->request['username'];
         
         $code=array(); // define an array to store our JavaScript code;
         $validUser = usernameValidP($user);
         if($validUser == FALSE) 
            { 
                $code[]=$this->htmlDisable("button1"); // disable register button 
                $code[]=$this->htmlInner("resultsDiv","<font color='green'>Username Invalid</font>");   
            }
            else {
                    $code[]=$this->htmlEnable("button1"); // disable register button 
                    $code[]=$this->htmlInner("resultsDiv","");  
            }
         echo $this->arrayToString($code); // converts our array into a printable JavaScript string
     }
}

new Recommender();

?>
