<?php 
/*
* Use minimal session handling logic instead of session.php.
*/
session_start();
$_SESSION['SECTION']= "1"; 

include_once '../inc/config.php';  
// PLB: it appears that session.php causes redirect loop.
//include_once '../inc/session.php';
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$domainname = "";
$domainid = -1;
$sesEmail = "";

if ((isset ($_POST) && (sizeof($_POST) > 0))) {
    
    $usercodeArray = explode("-", $_POST['usercode']);
    list($left, $domain, $right) = $usercodeArray;
    $_SESSION['userid'] = trim($right);
    $_SESSION['email'] = trim($_POST['username']);
    $_SESSION['domainid'] = trim($domain);
    $sql = "SELECT name
         FROM domain
         where id=". $_SESSION['domainid'];
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result ))
    {
            $_SESSION['domainname'] = $row['name'];
    }
    
    if ( isset($usercodeArray[3]) ) {
        $applicationId = trim($usercodeArray[3]);
        $_SESSION['appid'] = $applicationId;
    }
    
    if (isset($_POST['aliasLogin'])) {
        $_SESSION['aliasLogin'] = TRUE;
    }
}

//$applicationId = filter_input(INPUT_GET, 'appid', FILTER_VALIDATE_INT); 
    
// PLB added usertypeid, usermasterid to session to fix redirect loop 
// and authorization problems with contact.php 10/16/09
$_SESSION['usertypeid'] = 6;
$_SESSION['usermasterid'] = -1;
if ( isset($_SESSION['userid']) ) {
    $usermasteridQuery = "SELECT user_id FROM lu_users_usertypes WHERE id = " . $_SESSION['userid'];
    $usermasteridResult = mysql_query($usermasteridQuery) or die(mysql_error());
    while ($row = mysql_fetch_array($usermasteridResult)) {
        $_SESSION['usermasterid'] = $row['user_id'];    
    }   
}

if ( $_SESSION['usermasterid'] > 1 ) {   
    
    $_SESSION['allow_edit'] = TRUE;
    
    // Successful login (?) add access log record
    if (!isset($_SESSION['loginRecorded']) || !$_SESSION['loginRecorded']) {
        logAccess('login');
    }

} else {
    
    unset($_SESSION['userid']);
    unset($_SESSION['usermasterid']);
    header('Location: rec.php');
    exit;
}
        
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
if ( isset($_SESSION['appid']) ) {
    $applicationId = $_SESSION['appid'];    
}

/*
if (($_POST["usercode"] == "") or ($_POST["username"] == "")) {
    session_unset();
    header("Location: logout.php?r=1");
}
*/

/*
 if($_SESSION['userid']== -1)
{
	session_unset();
	header("Location: logout.php?r=1");
}
*/

$users = array();

$sql = "SELECT lang_prof_recommend.id,
application.user_id,
users.firstname,
users.lastname,
datafileinfo.moddate,
lang_prof_recommend.submitted
FROM lang_prof_recommend
inner join application on application.id = lang_prof_recommend.application_id
inner join lu_users_usertypes on lu_users_usertypes.id = application.user_id
inner join users on users.id = lu_users_usertypes.user_id
left outer join datafileinfo on datafileinfo.id = lang_prof_recommend.datafile_id
where rec_user_id=". $_SESSION['userid'];
if ($applicationId) {
    $sql .= " AND application.id = " . $applicationId;
}

$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr,  $row['id']);      // recommend.id
	array_push($arr,  $row['user_id']);
	array_push($arr,  $row['firstname']. " " . $row['lastname']);
	array_push($arr,  formatUSdate($row['moddate']));
    array_push($arr, $row['submitted']);
	array_push($users, $arr);
}
?>
<html>
<head>
<title><?=$HEADER_PREFIX[$domainid]  //1 is $domainid ?></title>
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
        <?php
            switch ($_SESSION['domainid']) {
                case 37:
                case 65:
                    if ( isset($_POST['sender']) ) {
                        $studentIDArray = explode("_", $_POST['sender']);
                        $studentID = $studentIDArray[1];                
                    } else {
                        $studentID = NULL;    
                        }
                        ?>
                    <form action="recform_mshcii.php" method="get" name="form1" id="form1">
        <?php
                    break;
                 case 75:
                 case 81:
                 case 82:
                    if ( isset($_POST['sender']) ) {
                        $studentIDArray = explode("_", $_POST['sender']);
                        $studentID = $studentIDArray[1];                
                    } else {
                        $studentID = NULL;    
                        }
                        ?>
                    <form action="recform_mod_lang_proficiency.php" method="get" name="form1" id="form1">
                    <?php
                    break;
                 case 74:
                    if ( isset($_POST['sender']) ) {
                        $studentIDArray = explode("_", $_POST['sender']);
                        $studentID = $studentIDArray[1];                
                    } else {
                        $studentID = NULL;    
                        }
                        ?>
                    <form action="recform_history.php" method="get" name="form1" id="form1">
                    <?php
                    break;
                 case 78:
                    if ( isset($_POST['sender']) ) {
                        $studentIDArray = explode("_", $_POST['sender']);
                        $studentID = $studentIDArray[1];                
                    } else {
                        $studentID = NULL;    
                        }
                        ?>
                    <form action="recform_sds.php" method="get" name="form1" id="form1">
                    <?php
                    break;
                 default:
                 ?>
                    <form action="recommenderUpload.php" method="post" name="form1" id="form1">
        <?php 
            }
        ?>
        <?php /*
        if ($_SESSION['domainid'] != 37 && $_SESSION['domainid'] != 65) {
            ?>
		<form action="recommenderUpload.php" method="post" name="form1" id="form1">
        <?php } else {
            if ( isset($_POST['sender']) ) {
                $studentIDArray = explode("_", $_POST['sender']);
                $studentID = $studentIDArray[1];                
            } else {
                $studentID = NULL;    
            }
            ?>
            <form action="recform_mshcii.php" method="get" name="form1" id="form1">
        <?php  } */ ?>
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
			 <? 
              /*
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
            */
			?>
			
          <tr>
            <td valign="top">			
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			){ ?>
				<a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
				<!-- DAS Removed - <? echo $sesEmail;?>   -->
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
				}
			} ?>
			</div>

			<div style="margin:20px;width:660px""><span class="title">Language Proficiency Recommender Home</span>
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <span class="tblItem">
			<?
			
			if(isset($_SESSION['domainid']))
			{
				if($_SESSION['domainid'] > -1)
				{
					$domainid = $_SESSION['domainid'];
				}
			}
			$sql = "select content from content where name='Lang Prof Recommendation Main Page' and domain_id=".$domainid;
			$result = mysql_query($sql)	or die(mysql_error());
			while($row = mysql_fetch_array( $result )) 
			{
				echo html_entity_decode($row["content"]);
			}
			?>
                        <input name="sender" type="hidden" value="">
			<? 
            for ($i = 0; $i< count($users); $i++){ 
			$allowEdit = true;
			if($users[$i][3] != "" || $_SESSION['allow_edit'] == false)
			{
				//$allowEdit = false;
			}
			?>
			 <strong><? 
             if( $users[$i][3] == "" ||  ($users[$i][3] != "" && $users[$i][4] < 2) )
                {
                    $sender = "btnUser_" . $users[$i][0] . "_" . $users[$i][1];
                    if ($applicationId) {
                        $sender .= "_" . $applicationId;    
                    }
                    showEditText($users[$i][2], "linkbutton", $sender, $allowEdit);
                }
                else {
                        echo $users[$i][2];
                } ?>
			 </strong>
			<em>
			<?
			$date = "";
			if($users[$i][3] != "" && $users[$i][4] > 0)
			{
				$date = " - Submitted ".$users[$i][3];
			}
			echo $date;
			?> 
			</em><br> 
			<? } ?>
            </span>
            </div>

            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
</html>