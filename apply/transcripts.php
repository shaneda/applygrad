<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php  
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$id = -1;
$err = "";
$isvalid = false;  

if(isset($_GET['id']))
{
	if($_GET['id'] != "")
	{
		$id = intval($_GET['id']);
		//DO SECURITY MATCHING
		$sql = "select id from usersinst where user_id=".$_SESSION['userid']." and id=".$id;
		$result = mysql_query($sql) or die(mysql_error());

		while($row = mysql_fetch_array( $result )) 
		{
			$isvalid = true;
		}
	}
}

if(isset($_FILES["file"]) && $isvalid == true)
{   
	$ret = handle_upload_file(1, $_SESSION['userid'],$_SESSION['usermasterid'], $_FILES["file"],$_SESSION['appid']."_".$id );
	//echo $ret;
	$err = "";
	if(intval($ret) < 1)
	{
		$err = $ret."<br>";
	}else
	{
		$sql = "update usersinst set datafile_id =".intval($ret)." where id = ".$id;
		//echo $sql;
		$result = mysql_query($sql) or die(mysql_error());
		header("Location: uni.php");
		
	} 

}
  
// Include the shared page header elements.
$pageTitle = 'Transcripts';
$formEnctype = 'multipart/form-data';
include '../inc/tpl.pageHeaderApply.php'; 
?>

<span class="errorSubtitle"><?=$err;?></span><br>
<span class="tblItem">Your unofficial transcript must be submitted in GIF, JPG or PDF format. Any other format will not be accepted. The maximum file size for your transcripts is 7MB.</span><br>
<br>
<input name="file" type="file" class="tblItem" maxlength="255">
<input name="Submit" type="submit" class="tblItem" value="Upload">
<br>
<br>
            
<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>