<?
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../apply/header_prefix.php';
include_once '../apply/priority_display.php';
include '../inc/specialCasesApply.inc.php';

include_once '../apply/AdvisorsData.class.php';

// Payment manager includes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

/* 
* PLB added unit/period check 8/26/09.
* The include sets $activePeriod (a period object) and $activePeriodId
*/
include '../inc/applyPeriod.inc.php';

function isAlreadyPaid($applicationId) {
    $query = "SELECT paid, waive FROM application WHERE id = " . $applicationId;
    $query .= " LIMIT 1";
    $result = mysql_query($query);
    $isPaid = FALSE;
    while ($row = mysql_fetch_array($result)) {
        if ($row['paid'] || $row['waive']) {
            $isPaid = TRUE;
        }    
    }
    if ($isPaid) {
        return TRUE;
    } else {
        return FALSE;    
    }    
}

// Get the unit ids of the programs that are active for this period.
$activeUmbrellaPeriod = NULL;
$activeProgramIds = array();

if ($activePeriodId) {
    $activeUmbrellaPeriod = $activePeriod;
} else {
    // Check to see if an umbrella period has an active editing period.
    $activePeriods = $unit->getActivePeriods(3);
    if ( count($activePeriods) > 0 ) {
        // There should be max one active period.
        $activeUmbrellaPeriod = $activePeriods[0];    
    } 
}
if ($activeUmbrellaPeriod) {
    $periodPrograms = $activeUmbrellaPeriod->getPrograms();
    foreach ($periodPrograms as $unitId => $unitName) {
        $programUnit = new Unit($unitId);
        $programUnitProgramsIds = $programUnit->getUnitProgramIds(); 
        foreach ($programUnitProgramsIds as $programsId) {
            $activeProgramIds[] = $programsId;    
        }
    }
}

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
        {
            $prevPage = "";
        }
if (sizeof ($appPageList) > $curPageIndex + 1)
    {
        $nextPage =  $appPageList[$curPageIndex + 1]; 
    } 
    else
        {
            $nextPage = "";
        }

$err = "";
$degrees = array();
$programs = array();
$myPrograms = array();
$areas = array();
$myAreas = array();
$depts = array();
$wasSubmitted = false;
$degree = "";
$program = "";
$programName = "";
$programDegree = "";
$area1 = 0;
$area2 = 0;
$area3 = 0;
$linkword = "in";
$advisorCount = 1;
$myAdvisors = array();
$deptsWithAdvisorReqs = getDeptsAcceptingAdvisorRequest();
$mastersReviewWaiver = 0;

// Design special cases
$isDesignDomain = isDesignDomain($domainid);
$isDesignPhdDomain = isDesignPhdDomain($domainid);
        

if($_SESSION['allow_edit'] == true && $_SESSION['appid'] != -1)
{
	//POST PROCESSING
	if(isset($_POST))
	{
        if( isset($_POST['chkMastersReviewWaiver']) )
        {
            $mastersReviewWaiverQuery = "UPDATE application 
                                        SET masters_review_waiver = 1
                                        WHERE application.id = " . intval($_SESSION['appid']); 
            $mastersReviewWaiverResult = mysql_query($mastersReviewWaiverQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $mastersReviewWaiverQuery);
        }
        elseif ( isset($_POST['txtMastersReviewWaiver']) ) 
        {
            $mastersReviewWaiverQuery = "UPDATE application 
                                        SET masters_review_waiver = " . intval( $_POST['txtMastersReviewWaiver'] ); 
            $mastersReviewWaiverQuery .= " WHERE application.id = " . intval($_SESSION['appid']);
            $mastersReviewWaiverResult = mysql_query($mastersReviewWaiverQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $mastersReviewWaiverQuery);        
        }
        elseif ( isset($_POST['formSubmitted']) ) 
        {
            $mastersReviewWaiverQuery = "UPDATE application 
                                        SET masters_review_waiver = 0
                                        WHERE application.id = " . intval($_SESSION['appid']); 
            $mastersReviewWaiverResult = mysql_query($mastersReviewWaiverQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $mastersReviewWaiverQuery);
        }
        
        // RI Summer Scholars re-using masters_review_waiver as agree_program_dates
        if( isset($_POST['chkProgramDates']) )
        {
            $programDatesQuery = "UPDATE application 
                                        SET masters_review_waiver = 1
                                        WHERE application.id = " . intval($_SESSION['appid']); 
            $programDatesResult = mysql_query($programDatesQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $programDatesQuery);
                
            // Update requirements complete if program selected.
            if ($domainid == 38) {
                
                $programSelectedQuery = "SELECT program_id FROM lu_application_programs
                    WHERE application_id = " . intval($_SESSION['appid']);
                $programSelectedResult = mysql_query($programSelectedQuery)
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $programSelectedQuery);
                
                if (mysql_num_rows($programSelectedResult) > 0) {
                    updateReqComplete("programs.php",1);    
                } 
            }  
        }
        elseif ( isset($_POST['txtProgramDates']) ) 
        {
            $programDatesQuery = "UPDATE application 
                                        SET masters_review_waiver = " . intval( $_POST['txtProgramDates'] ); 
            $programDatesQuery .= " WHERE application.id = " . intval($_SESSION['appid']);
            $programDatesResult = mysql_query($programDatesQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $programDatesQuery);      
        }
        elseif ( isset($_POST['programDatesSubmitted']) ) 
        {
            $programDatesQuery = "UPDATE application 
                                        SET masters_review_waiver = 0
                                        WHERE application.id = " . intval($_SESSION['appid']); 
            $programDatesResult = mysql_query($programDatesQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $programDatesQuery);
                
            updateReqComplete("programs.php",1); 
        }

        $vars = $_POST;
		$itemId = -1;
        foreach($vars as $key => $value)
		{
            if( strstr($key, 'btnAddProg') !== false )
			{
				$arr = split("_", $key);
				$itemId = $arr[1];
				if( $itemId > -1 )
				{
                    $program = intval($itemId);
                    $programName = filter_input(INPUT_POST, "txtProgName_".$itemId, FILTER_SANITIZE_STRING);
                    $programDegree = filter_input(INPUT_POST, "txtProgDegree_".$itemId, FILTER_SANITIZE_STRING);
				}

			$myAreas = array();
			$areasCount = 0;
			
			if($program < 0)
			{
				$err .= "You must select a program first.<br>";
			}
			
			if($err == "")
			{
				$doAdd = true;
				$degreename = "";
				$maxnum = 0;
				$phdCount = 0;
                // Changed to unlimited masters 10/3/11
				//$mastersCount = 0;
				$sql = "select program_id, choice,degree.name 
				from lu_application_programs
				inner join programs on programs.id = lu_application_programs.program_id
				inner join degree on degree.id = programs.degree_id
				where application_id =". intval($_SESSION['appid']);
				$result = mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
				$itemid = -1;
				$progCount = 0;
				while($row = mysql_fetch_array( $result )) 
				{
					$degreename = $row['name'];
					$progCount++;
					if($row['choice'] > $maxnum)
					{
						$maxnum = $row['choice'];
					}
					if($row['program_id']==$program)
					{
						$doAdd == false;
						$err .= "PROGRAM ALREADY EXISTS IN SELECTION<br>";
					}
					if( strstr($degreename, 'Ph.D.') !== false 
					||  strstr($degreename, 'Ph.D. Dual Degree') !== false
					||  strstr($degreename, 'Ph.D./M.S.') !== false
                    ||  strstr($degreename, 'Joint Ph.D') !== false
                    ||  strstr($degreename, 'Joint CMU-Pitt Ph.D.') !== false) 
					{
						$phdCount++;
					}
                    
                    // Changed to unlimited masters 10/3/11
                    /*
					if( strstr($degreename, 'M.S.') !== false 
						|| strstr($degreename, 'M.S./M.B.A.') !== false
						|| strstr($degreename, 'M.B.A.') !== false
						||  strstr($degreename, 'Ph.D./M.S.') !== false ) 
					{
						$mastersCount++;
					}
                    */
				}
				if ($maxnum > $progCount)
				{
					$maxnum = $progCount;
				}
				$maxnum = $maxnum+1;
				
                if(isScsDomain($domainid))
				{
                    if($phdCount > 2 && strstr($programDegree, 'Ph.D.') !== false )
                    {
					    $err .= "ERROR: You can only apply to 3 Doctoral programs.<br>";
                    }
				}
                elseif(!isSdsDomain($domainid) && $phdCount > 1 
                    && strstr($programDegree, 'Ph.D.') !== false )
                {
                    $err .= "ERROR: You can only apply to 2 Doctoral programs.<br>";
                }
                
                /*
                * Limit INI to two programs 
                */
                if($progCount > 1 && (isIniDomain($domainid) || isIniKobeDomain($domainid) || isEM2Domain($domainid)))
                {
                    $err .= "ERROR: You can only apply to 2 programs.<br>";
                }
				
                // Changed to unlimited masters 10/3/11
                /*
                if($mastersCount > 1 &&  strstr($programDegree, 'M.S.') !== false )
				{
                    $err .= "ERROR: You can only apply to 2 Masters programs.<br>";
				}
                */
			
				if($doAdd == true && $err == "")
				{
					$sql = "insert into lu_application_programs(application_id, program_id, choice)
                        values( ". intval($_SESSION['appid']) . ", "
                        . intval($program) . " , " . intval($maxnum) . ") ";
					$result = mysql_query($sql) 
                        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
					$itemid = mysql_insert_id();
                    
                    // Update application as "unpaid" on every program addition.
                    $unpaidQuery = "UPDATE application SET paid = 0 WHERE id = " . intval($_SESSION['appid']);
                    $unpaidQueryresult = mysql_query($unpaidQuery)
                        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $unpaidQuery);
                    
                    // PLB added period_application insert/update 8/26/09
                    if ($activePeriodId) {
                        $activePeriod->addApplication($_SESSION['appid']);
                    }
					
					for($i = 0; $i<count($myAreas); $i++)
					{
						$sql = "insert into lu_application_interest (app_program_id, interest_id, choice)
                            values (" . intval($itemid) . ", " . intval($myAreas[$i]) . ", " 
                            . intval($i) . ")";
						$result = mysql_query($sql) 
                            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
					}
                    
                    // Kluge for AOI test
                    $insertedProgramId = $program;
					
                    // These are being unset why??? Re-used below, maybe????
                    $degree = "";
					$program = "";
					$area1 = 0;
					$area2 = 0;
					$area3 = 0;
                    
                    // Update scores requirement for MS CS
                    $isMsCsApplication = isMsCsApplication($_SESSION['appid']);
                    if (!$isMsCsApplication) {
                        $greComplete = greComplete($_SESSION['appid']);
                        if (!$greComplete) {
                            updateReqComplete('scores.php', 0, 1);
                        }    
                    }
                    
				}//END IF doadd
                
				if($err == "")
				{
					//Enforce AOI selection for SCS/RI
                    if ($domainid == 1 || $domainid == 3 || $domainid == 18) {
                    
                        $insertedProgramAreas = getAreas($insertedProgramId);
                        $insertedProgramAois = getMyAreas($insertedProgramId);
                            
                        if (count($insertedProgramAreas) == 0) {
                            
                            updateReqComplete("programs.php",1);
                            
                        } elseif (count($insertedProgramAois) > 0) {
                            
                            updateReqComplete("programs.php",1);
                            
                        } else {
                       
                            // Basically, make the requirement unfulfilled every time a
                            // new program that has AOIs is added.
                            updateReqComplete("programs.php",0);
                        }   
                       
                    } else {
                    
                        if ($domainid == 38) {
                            
                            if (isset($_POST['txtProgramDates']) && intval($_POST['txtProgramDates']) == 1) {
                            
                                updateReqComplete("programs.php",1);    
                            }
                            
                        } else {
                        
                            updateReqComplete("programs.php",1);
                        }
                    }
                    
                    // Enforce fellowship award letter upload for LTI
                    if (isLtiProgram($insertedProgramId))
                    {
                        $awardLetterQuery = "SELECT id FROM fellowships 
                            WHERE application_id = " . intval($_SESSION['appid']) .
                            " AND datafile_id IS NULL";
                        $awardLetterResult = mysql_query($awardLetterQuery);
                        if (mysql_num_rows($awardLetterResult) > 0)
                        {
                            updateReqComplete("suppinfo.php", 0);   
                        }
                    }
                    
                    // Enforce supporting educational data for INI
                    if (isIniDomain($domainid))
                    {
                        if (!iniSupportingCourseworkComplete($_SESSION['appid']))
                        {
                            updateReqComplete("uni.php", 0);    
                        }
                    }
                    // Enforce supporting educational data for EM2
                    if (isEM2Domain($domainid))
                    {
                        if (!em2SupportingCourseworkComplete($_SESSION['appid']))
                        {
                            updateReqComplete("uni.php", 0);    
                        }
                    }
				}
			}//END IF ERR
				
        }//END IF ADD
			
        if( strstr($key, 'btnDelete') !== false )
        {
	        $arr = split("_", $key);
	        $itemId = $arr[1];
	        if( $itemId > -1 )
	        {
		        
		        mysql_query("DELETE FROM lu_application_programs WHERE id=". intval($itemId))
		            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error());
		        
		        mysql_query("DELETE FROM lu_application_interest WHERE app_program_id=". intval($itemId))
		            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error());
		        
		        //REORDER CHOICES
		        $myProgs = array();
		        $sql = "select id,program_id from lu_application_programs 
                    where application_id = " . intval($_SESSION['appid']) . " order by choice";
		        $result = mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
		        $myProgs = array();
		        while($row = mysql_fetch_array( $result )) 
		        {
			        $arr = array();
			        array_push($arr, $row['id']);
			        array_push($arr, $row['program_id']);
			        array_push($myProgs, $arr);
		        }
		        for($i = 0; $i < count($myProgs); $i++)
		        {
			        $sql = "update lu_application_programs set choice=".($i+1)." 
                        where id=" . intval($myProgs[$i][0]);
			        mysql_query($sql) 
                        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
		        }
		        if(count($myProgs) == 0)
		        {
			        updateReqComplete("programs.php",0);
		        }
                
                // Enforce AOI selection for SCS/RI
                if (count($myProgs) > 0 && ($domainid == 1 || $domainid == 3 || $domainid == 18)) {
                    
                    // Kluge: go ahead and set requirement as complete
                    updateReqComplete("programs.php",1);

                    // Now, set incomplete if any program doesn't have an AOI selected.
                    foreach ($myProgs as $myProg) {
                    
                        $myProgramId = $myProg[1];
                        $programAois = getAreas($myProgramId);
                        $myLuApplicationProgramId = $myProg[0];
                        $myProgramAois = getMyAreas($myLuApplicationProgramId);
                        
                        if (count($programAois) > 0 && count($myProgramAois) < 1) {
                            updateReqComplete("programs.php",0);
                            break;    
                        }
                    }   
                }
                
                // Update the payment status.
                $paymentManager = new PaymentManager($_SESSION['appid']);
                $balanceUnpaid = $paymentManager->getBalanceUnpaid();
                if ($balanceUnpaid > 0) {
                    $isPaid = 0; 
                } else {
                    $isPaid = 1;
                }
                $unpaidQuery = "UPDATE application SET paid = " . $isPaid;  
                $unpaidQuery .= " WHERE id = " . intval($_SESSION['appid']);
                $unpaidQueryresult = mysql_query($unpaidQuery)
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $unpaidQuery);

                // Update scores requirement for MS CS
                $isMsCsApplication = isMsCsApplication($_SESSION['appid']);
                if ($isMsCsApplication) {
                    $cmuMscsGreWaiver = cmuMscsGreWaiver($_SESSION['appid']);
                    if ($cmuMscsGreWaiver) {
                        updateReqComplete('scores.php', 1);
                    }    
                }
                	        
	        }//END IF
        }//END IF DELETE
			
			//HANDLE REORDERING
			if( strstr($key, 'btnDn') !== false ||  strstr($key, 'btnUp') !== false)
			{
				$direction = "down";
				$arr = split("_", $key);
				$itemId = $arr[1];
				
				if($arr[0] == "btnUp"){
					$direction = "up";
				}
				if( $itemId > -1 )
				{
					$myProgs = array();
                    $myProgramIds = array();    // Kluge for AOI test
					$sql = "select * from lu_application_programs 
                        where application_id = ". intval($_SESSION['appid']) . " order by choice";
					$result = mysql_query($sql) 
                        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
					$i = 0;
					while($row = mysql_fetch_array( $result )) 
					{
						$i++;
						$arr = array();
						array_push($arr, $row[0]);
						array_push($arr, $row[1]);
						array_push($arr, $i);				
						array_push($myProgs, $arr);
                        
                        // add program id, indexed by lu_application_program_id,
                        // to use in check for AOIs
                        $myProgramIds[$row[0]] = $row[2];  
					}
					$val = -1;
					for($i = 0; $i < count($myProgs); $i++)
					{
						if($myProgs[$i][0] ==  $itemId)
						{
							$val = $myProgs[$i];
						}
					}
					
					$myProgsSorted = arrayshift($myProgs,$val,$direction );
					for($i = 0; $i < count($myProgs); $i++)
					{
						$sql = "update lu_application_programs 
                            set choice = " . intval($myProgsSorted[$i][2]) . " 
                            where application_id = " . intval($_SESSION['appid']) . " 
                            and id =" . intval($myProgs[$i][0]);
						$result = mysql_query($sql) 
                            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
					}
					if($err == "")
					{
                        // Apparently the requirement has always been marked
                        // as complete upon reordering, so go ahead and do it.
                        updateReqComplete("programs.php",1);

						// Enforce AOI selection for SCS/RI and VLIS
                        if ($domainid == 1 || $domainid == 3 || $domainid == 18) {

                            // Now, set incomplete if any program doesn't have an AOI selected.
                            foreach ($myProgramIds as $myLuApplicationProgramId => $myProgramId) {
                            
                                $programAois = getAreas($myProgramId);
                                $myProgramAois = getMyAreas($myLuApplicationProgramId);
                                
                                if (count($programAois) > 0 && count($myProgramAois) < 1) {
                                    updateReqComplete("programs.php",0);
                                    break;    
                                }
                            }   
                        }
					}
					break;
					
				}//END IF
			}//END IF
		}//END FOR

        // Enforce attendance, assistantship for Design
        $designRequirementsError = '';
        if (count($_POST) > 0 && ($isDesignDomain || $isDesignPhdDomain))
        {
            if (!isset($_POST['attendanceStatus']) ||
                !isset($_POST['requestAssistantship']))
            {
                $designRequirementsQuery = "SELECT 
                    application.id AS application_id,
                    attendance.status AS attendance_status,
                    assistantship.requested AS assistantship_requested
                    FROM application
                    LEFT OUTER JOIN attendance ON application.id = attendance.application_id
                    LEFT OUTER JOIN assistantship ON application.id = assistantship.application_id
                    WHERE application.id = " . intval($_SESSION['appid']) . 
                    " LIMIT 1";
                    
                $designRequirementsResult = mysql_query($designRequirementsQuery);
                    
                while ($row = mysql_fetch_array($designRequirementsResult))
                {
                    if (!isset($_POST['attendanceStatus']) && $row['attendance_status'] == NULL)
                    {
                        $designRequirementsError .= 'You must indicate your attendance status<br>';        
                    }
                    
                    if (!isset($_POST['requestAssistantship']) && $row['assistantship_requested'] == NULL)
                    {
                        $designRequirementsError .= 'You must indicate whether you are requesting an assistantship<br>';        
                    }
                }
                
                if ($designRequirementsError)
                {
                    //$err .= $designRequirementsError;
                    updateReqComplete("programs.php", 0);    
                }
            }    
        } 
        
        
		// PROGRAMS
		$sql = "SELECT 
		programs.id,
		fieldsofstudy.name,
		degree.name as degreename,
		department.rank,
		programs.rank,
		programs.linkword,
		programs.url, 
		CASE programs.description WHEN '' THEN false WHEN NULL THEN false ELSE true END  as description
		FROM programs 
		inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		inner join degree on degree.id = programs.degree_id 
		inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
		inner join department on department.id = lu_programs_departments.department_id
		";
		if($_SESSION['domainid'] > -1)
		{
			$sql .= "
				inner join lu_domain_department 
                on lu_domain_department.department_id = lu_programs_departments.department_id
				where lu_domain_department.domain_id=" . intval($_SESSION['domainid']);
		}
		switch($_SESSION['level'])
		{
			case 1:
			$sql .= " and degree.id=1 ";
			break;
			case 2:
			$sql .= " and degree.id>1 ";
			break;
			
		}
		$sql .= " and programs.enabled=1 ";
		if($_SESSION['domainid'] > -1)
		{
			$sql .= " order by lu_domain_department.rank, programs.rank, degree.name, fieldsofstudy.name";
		}else
		{
			$sql .= " order by department.rank, programs.rank, degree.name, fieldsofstudy.name";
		}
		$result = mysql_query($sql) 
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
		while($row = mysql_fetch_array( $result )) 
		{
			$dept = "";
			$depts = getDepts($row[0]);
			if(count($depts) > 1)
			{
				$dept = " - ";
				for($i = 0; $i < count($depts); $i++)
				{
					$dept .= $depts[$i][1];
					if($i < count($depts)-1)
					{
						$dept .= "/";
					}
				}
			}
			$arr = array();
			array_push($arr, $row[0]);
			array_push($arr, $row[2] . " ".$row['linkword']." ". $row[1].$dept);
			array_push($arr, $row[2]);
			array_push($arr, $row['url']);
			array_push($arr, $row['description']);
			array_push($programs, $arr);
		}
        
        
		
	}//END IF POST
	
	// DEGREES
	$result = mysql_query("SELECT * FROM degree order by name") 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name'] .' ('.$row['short'].')');
		array_push($degrees, $arr);
	}
	
}//END IF ALLOW EDIT

//GET USERS SELECTED PROGRAMS
$sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname, choice, lu_application_programs.id as itemid,
programs.linkword, programs.url,
CASE programs.description WHEN '' THEN false WHEN NULL THEN false ELSE true END  as description,
application.masters_review_waiver
FROM lu_application_programs
INNER JOIN application ON lu_application_programs.application_id = application.id 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
where lu_application_programs.application_id = " . intval($_SESSION['appid']) . " 
order by choice";
$result = mysql_query($sql) 
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);

$enableMastersReviewWaiver = FALSE; 
$enableProgramDatesAgreement = FALSE;
while($row = mysql_fetch_array( $result )) 
{
	$mastersReviewWaiver = $row['masters_review_waiver'];
    $enableProgramDatesAgreement = TRUE;
    
    $dept = "";
	$depts = getDepts($row[0]);
	if(count($depts) > 1)
	{
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$dept .= $depts[$i][1];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row['degreename']);
    if ( stripos($row['degreename'], 'Ph.D') !== FALSE ) 
    {
        $enableMastersReviewWaiver = TRUE;
    }
	array_push($arr, $row['fieldname'].$dept);
	array_push($arr, $row['choice']);
	array_push($arr, $row['itemid']);
	array_push($arr, $row['linkword']);
	array_push($arr, $row['url']);
    array_push($arr, $row['description']);
	array_push($myPrograms, $arr);
}
$_SESSION['programs'] = $myPrograms;


function getAreas($id)
{
	$ret = array();
	//GET AREAS OF INTEREST
	$sql = "select interest.id,
	interest.name from
	lu_programs_interests 
	inner join interest on interest.id = lu_programs_interests.interest_id 
	where lu_programs_interests.program_id=".intval($id) ;
	$result = mysql_query($sql) 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row[1]);
		array_push($ret, $arr);
	}
	return $ret;
}


function getMyAreas($id)
{
	$ret = array();
	$sql = "SELECT 
	interest.id,
	interest.name
	FROM 
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.id=" . intval($id) . " 
    and lu_application_programs.application_id=" . intval($_SESSION['appid']) . " 
    order by lu_application_interest.choice";

	$result = mysql_query($sql)
	    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}


/*
* Check for special cases; 
*/
$isIsreeDomain = isIsreeDomain($_SESSION['domainid']);
$isHistoryDomain = isHistoryDomain($_SESSION['domainid']);

// Include the shared page header elements.
if ( count($myPrograms) > 0) {
    $nextPage = 'bio.php';
} 

if ($isIsreeDomain) 
{
    $pageTitle = 'Course Description';
} 
elseif ($isHistoryDomain)
{
    $pageTitle = 'Program';    
}
else 
{
    $pageTitle = 'Programs';    
}
include '../inc/tpl.pageHeaderApply.php';
?>

<p class="tblItem">
<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='Programs' and domain_id=". intval($domainid);
$result = mysql_query($sql)	
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}

/*
* Don't show program selections/choices for ISR-EE, History. 
*/
if ($isIsreeDomain || $isHistoryDomain) 
{
    include '../inc/tpl.pageFooterApply.php'; 
    exit;
}

?>
<br>
<span class="errorSubtitle">
<?=$err;?></span><br>

<? 
/*
* DIETRICH/INI
* Pier/cnbc, application sharing sections for psychology
*/
if (isPsychologyDomain($domainid))
{
    include('../inc/dietrichPierCnbc.inc.php');
    include('../inc/dietrichSharing.inc.php');
}

$count = count($myPrograms);
if(count($myPrograms) > 0){ ?>

 Click on a section in the top menu bar to continue filling out your application.

 <hr size="1" noshade="noshade" color="#990000"> 
  <table width="96%" border="0" cellpadding="4" cellspacing="1" class="tblItem">
    <tr>


<? if ($DISPLAY_PRIORITY[$domainid] == $TRUE ) { ?>
      <td width="70" class="tblHead2">Priority</td>
      <td class="tblHead2">Program(s) selected in order of preference </td>
<? } else { ?>
      <td width="70" &nbsp; </td>      
      <td class="tblHead2">Program Selection(s)</td>
<? } ?>

        <td width="70" align="right" class="tblHead2">Remove</td>
    </tr>

	<?
    $advisors = getAdvisors($domainid);
	for($i = 0; $i < count($myPrograms); $i++)
	{ 
    /*
    * PROGRAMS LOOP BEGINS HERE !!!!!!!!!!!!!!!
    */
    $areas = getAreas($myPrograms[$i][0]);
	$myAreas = getMyAreas($myPrograms[$i][4]);
    
    // Checks for specific programs
    $isCsdProgram = isCsdProgram($myPrograms[$i][0]);
    $isMLProgram = isMLProgram($myPrograms[$i][0]);
    $isMsCsProgram = isMsCsProgram($myPrograms[$i][0]);
    $isVlisProgram = isVlisProgram($myPrograms[$i][0]);
    $isSdsProgram = isSDSProgram($myPrograms[$i][0]);
    $isPsychologyProgram = isPsychologyProgram($myPrograms[$i][0]);
	?>
	<tr>

<? if ($DISPLAY_PRIORITY[$domainid] == $TRUE ) { ?>
	  <td width="70" class="menuItemDesc">
	   <? if($myPrograms[$i][7] == true) { ?>
		 <input name="btnUp_<?=$myPrograms[$i][4];?>" type="image" class="tblItem" id="btnUp_<?=$myPrograms[$i][4];?>" value="UP" src="../images/arrowup.gif" title="Move Up">
	  <? }//END IF MYPROGRAMS ?>
		<? if($myPrograms[$i][3] <= $count-1) { ?>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="btnDn_<?=$myPrograms[$i][4];?>" type="image" class="tblItem" id="btnDn_<?=$myPrograms[$i][4];?>" value="DN" src="../images/arrowdn.gif" title="Move Down">
		<? }//END IF MYPROGRAMS ?>	  </td>
<? } else { ?>
      <td width="70" &nbsp; </td>      
<? } ?>

	  <td class="menuItemDesc"><strong><?=$i+1?> - <?
	  if($myPrograms[$i][6] != ""){ ?>
	  	<a href="<?="programinfo.php?id=".$myPrograms[$i][0];?>" title="Click for more information"><?=$myPrograms[$i][1]." ".$myPrograms[$i][5]." ".$myPrograms[$i][2];?></a>
	  <? }else {
	  	echo $myPrograms[$i][1]." ".$myPrograms[$i][5]." ".$myPrograms[$i][2];
	  } ?>
	  </strong><br>
	   <?
       // Does the program have AOIs
       if (count($areas) > 0) {
           $programHasAois = TRUE;
       } else {
           $programHasAois = FALSE;
       }
       
       // Has an AOI been selected for this program?
       if (count($myAreas) > 0) {
           $programAoiSelected = TRUE;
       } else {
           $programAoiSelected = FALSE;
       }       
       
	   if($programHasAois && !$programAoiSelected){
	   
           // The program has AOIs and none has been selected
           // so show the button.
       ?>
	    <div style="margin-left:7px; "><em>
		<? if($_SESSION['allow_edit'] == true){ 
            
            // Enforce AOI selection for SCS/RI
            if ($domainid == 1 || $domainid == 3) {
                if (!$isVlisProgram) {
            ?>    
                You must select at least one Area of Interest for this program <br>
            <?php
                    } else {
            ?>    
                You must select a preferred track for this program <br>
            <?php    
                    }
            }  
            if ($isCsdProgram || $isMLProgram) {
                // Point to combined AOI/FOI page.
            ?>
                <input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Add Areas/Faculty of Interest" onClick="document.location='interestsAdvisorsWithOthers.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
            <?php    
            } elseif ($isSdsProgram || $isPsychologyProgram) {
                // Point to combined AOI/FOI page.
            ?>
                <input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Add Areas/Faculty of Interest" onClick="document.location='interestsAdvisors.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
            <?php    
            } elseif ($isVlisProgram) {
            ?>
                <input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Select Preferred Track" onClick="document.location='interests.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
            <?
            } else {
            ?>
		        <input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Add Areas of Interest" onClick="document.location='interests.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
		    <?
            } 
        } 
        ?>
		 </em>		
         </div>
         
         
		<?
	   }
       ?>
	   
       <div style="margin-left:7px; ">
       <?php
	   if($programAoiSelected){ 
           
           // At least one AOI has been selected, so show the link instead of the button.
           if ($isVlisProgram) {
           ?>
               Preferred Track:<em>
           <?php    
           } else {    
           ?>
	           Areas of Interest:<em>
           <?php
           }
           
        if ($isSdsProgram || $isPsychologyProgram) {
            
            // CSD programs should link to combined AOI/FOI page, except for MS in CS,
            // which should not have faculty of interest selection.
        ?>
            <a href="interestsAdvisors.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change areas of interest">
        <?php
        // $isMsCsProgram set line 714    
        } elseif (($isCsdProgram && !$isMsCsProgram) || $isMLProgram) {
            
            // CSD programs should link to combined AOI/FOI page, except for MS in CS,
            // which should not have faculty of interest selection.
        ?>
            <a href="interestsAdvisorsWithOthers.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change areas of interest">
        <?php    
        } else {
        ?>
		    <a href="interests.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change areas of interest">
        <?php
        }
		
		for($j = 0; $j < count($myAreas); $j++)
		{
			echo $myAreas[$j][1];
			if($j < count($myAreas)-1)
			{
				echo ", ";
			}
		}//END FOR MYAREAS
		?></a> </em>		
		<? } //end if myareas ?>	
        </div>
        </td>
	  
		<td width="70" align="right">
		<? // DAS keep PhD Statistics in the list all the time
           if (($domainid == 44) && $myPrograms[$i][0] == 100013) {
             // do not show delete button
        } else { 
            showEditText("X", "button", "btnDelete_".$myPrograms[$i][4], $_SESSION['allow_edit']); } ?>
<span class="subtitle">		</span>	  </td>
	</tr>
    <?php 
        $dpt = getDepts($myPrograms[$i][0]);
       if (in_array($dpt[0][0], $deptsWithAdvisorReqs)) {
        ?>
    <tr>
        <?
        $deptNumber = getDeptFromProgram($myPrograms[$i][0]);
        $advSelType = getDeptAdvisorSelectionType($deptNumber);
        if ($advSelType ==  1) {
            $advisors = getProgramAdvisors($myPrograms[$i][0]);
            $myAdvisors = getMyAdvisors($myPrograms[$i][0]);
            } else  if ($advSelType ==  2) {
                $myAdvisors = getMyAdvisorsWithAppId($_SESSION['appid'], $myPrograms[$i][0]);
                } else if ($advSelType ==  3) {
                     $myAdvisors = getMyAdvisorsWithOthers($myPrograms[$i][0]);
                }

       /*
       * Don't show faculty of interest for MRSD or MS in CS.
       * Show combined AOI/FOI for other CSD programs.
       * 
       * $isCsdProgram, $isMsCsProgram set line 714
       */
       
       // Have FOI been selected for this program?
       if (count($myAdvisors) > 0) {
           $programFoiSelected = TRUE;
       } else {
           $programFoiSelected = FALSE;
       }
       
       if( !$programFoiSelected && $myPrograms[$i][0] != 100007 && !$isMsCsProgram
            && (!$isSdsProgram || ($isSdsProgram && !$programHasAois))
            && (!$isPsychologyProgram || ($isPsychologyProgram && !$programHasAois))
            && (!$isCsdProgram || ($isCsdProgram && !$programHasAois))
            && !$isMLProgram ){
       
            // No FOIs have been selected;
            // This is not MRSD or MS in CS; 
            // This is not a CSD program OR it is a (non MS) CSD program that has no AOIs (and
            // thus should not use the combined AOI/FOI page),
            // so show the FOI button
       ?>
        <td></td><td>
        <div style="margin-left:7px; ">
            <em>
                <? if($_SESSION['allow_edit'] == true){ ?>
                <input name="btnAdvisors_" type="button" id="btnAdvisors_" class="tblItem" value="Faculty of Interest" onClick="document.location='advisors.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
        <? } ?>
            </em>        
        </div>
        </td>
        <?
       } 
       elseif (( $isSdsProgram || $isPsychologyProgram) && $programAoiSelected && !$programFoiSelected )
       {
           // This is a CSD program (not MS) for which at least one AOI has been selected
           // so the combined AOI/FOI link, and not the button, has been displayed,
           // and no FOI has been selected, so show the combined AOI/FOI button.
       ?>
        <td></td><td>
        <div style="margin-left:7px; ">
            <em>
                <? if($_SESSION['allow_edit'] == true){ ?>
                <input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Add Areas/Faculty of Interest" onClick="document.location='interestsAdvisors.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
        <? } ?>
            </em>        
        </div>
        </td>
        <?   
       }
       elseif ((($isCsdProgram && !$isMsCsProgram) || $isMLProgram) && $programAoiSelected && !$programFoiSelected )
       {
           // This is a CSD program (not MS) for which at least one AOI has been selected
           // so the combined AOI/FOI link, and not the button, has been displayed,
           // and no FOI has been selected, so show the combined AOI/FOI button.
       ?>
        <td></td><td>
        <div style="margin-left:7px; ">
            <em>
                <? if($_SESSION['allow_edit'] == true){ ?>
                <input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Add Areas/Faculty of Interest" onClick="document.location='interestsAdvisorsWithOthers.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
        <? } ?>
            </em>        
        </div>
        </td>
        <?   
       }
       
       if($programFoiSelected){  
       
            // At least one FOI has been selected, so show the FOI link
            // (the button will not have been displayed)    
       ?>
        <td></td><td>
        <div style="margin-left:7px; ">Faculty of Interest:<em>
        <?php
        if ($isSdsProgram || $isPsychologyProgram) {
        ?>
            <a href="interestsAdvisors.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change requested advisors">
        <?php    
        } else if (($isCsdProgram && !$isMsCsProgram) || $isMLProgram) {
        ?>
            <a href="interestsAdvisorsWithOthers.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change requested advisors">
        <?php    
        } else {
        ?>
            <a href="advisors.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change requested advisors">
        <?php
        }
         
        for($j = 0; $j < count($myAdvisors); $j++)
        {
            if ($advSelType == 1) {
                echo $myAdvisors[$j][2];
            } else if ($advSelType == 3) {
                echo $myAdvisors[$j][2];
            } else{
                echo $myAdvisors[$j];
            }
            
            if($j < count($myAdvisors)-1)
            {
                echo ", ";
            }
        } ?> </a>
        </em></div></td>
        <? } ?>
        </tr> <?php }  // end list of advisors ?>    
	<?
    $progid = $myPrograms[$i][0];
    $deptArray = getDepts($progid);
    $deptNo = $deptArray[0][0];
	}   //END FOR MYPROGRAMS
	?>
 </table>
<BR>


<? if ($DISPLAY_PRIORITY[$domainid] == $TRUE ) { ?>
     Click the 
     <img src="../images/arrowup.gif" alt="Up" width="11" height="6"> or 
     <img src="../images/arrowdn.gif" alt="down" width="11" height="6"> arrows to change your order of preference.<br>
<? } ?>

    <br>
     <hr size="1" noshade="noshade" color="#990000"> 

 <br>
 
 <? }//END IF COUNT 
// stop paid applicants from changing programs
if (!(isAlreadyPaid($_SESSION['appid']) && !$_SESSION['A_usertypeid'] == 0)) {  
         ?>
 <span class="subtitle">Select a program to add to your application </span>
 <div class="tblItem">
 <table  border="0" cellpadding="4" cellspacing="2" class="tblItem">
<?
$class = "tblItem";
$allowAdd = $_SESSION['allow_edit'];

if(count($programs) > 0)
{
for($i= 0; $i<count($programs); $i++){

/*
* Only show programs that are active for this period.
*/
if ( !in_array($programs[$i][0], $activeProgramIds) ) {
    continue;
}
 
if($i % 2 == 0){
	$class = "tblItem";
}else
{
	$class = "tblItemAlt";
}
?>
  <tr class="<?=$class?>">
    <td><?
	if($programs[$i][4] == true){ ?>
	<a href="<?="programinfo.php?id=".$programs[$i][0];?>" title="Click for more information on this program"><?=$programs[$i][1]?></a>
	<? }else{ ?>
	<?=$programs[$i][1]?>
	<? } ?>
      <input name="txtProgName_<?=$programs[$i][0]?>" type="hidden" id="txtProgName_<?=$programs[$i][0]?>" value="<?=$programs[$i][1]?>">
      <input name="txtProgDegree_<?=$programs[$i][0]?>" type="hidden" id="txtProgDegree_<?=$programs[$i][0]?>" value="<?=$programs[$i][2]?>"></td>
    <td>
      <?
	  for($j = 0; $j < count($myPrograms); $j++)
	  {
	  	$allowAdd = $_SESSION['allow_edit'];
	  	if($myPrograms[$j][0]==$programs[$i][0])
		{
			$allowAdd = false;
		}
        
        /*
        * Limit INI to two programs 
        */
        if(count($myPrograms) >= 2 && (isIniDomain($domainid) || isIniKobeDomain($domainid) || isEM2Domain($domainid)))
        {
            $allowAdd = false;
        }
        
        if($allowAdd == false)
        {
            break;
        }
        
		if($allowAdd == true)
		{
			//LOOKUP ANY LOCKOUTS BETWEEN PROGRAMS APPLIED FOR AND THIS PROGRAM
			$sql = "select id from multiprogramlockouts 
			where(program_id1=" . intval($myPrograms[$j][0]) . " 
                and program_id2=" . intval($programs[$i][0]) . ") 
			or (program_id1=" . intval($programs[$i][0]) . " 
                and program_id2=" . intval($myPrograms[$j][0]) . ")";   
			$result2 = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);

			while($row2 = mysql_fetch_array( $result2 ))
			{ 
				$allowAdd = false;
				break;
			}
			if($allowAdd == false)
			{
				break;
			}
		}//end if allowAdd
	  }//end for myprograms
	  showEditText("Add", "button", "btnAddProg_".$programs[$i][0], $allowAdd);  ?>    </td>
  </tr>
  <? }//end for programs
  }else
  {
  	echo "No programs available.";
  }//end if count programs
   ?>
</table>
<?php } ?>
<hr size="1" noshade="noshade" >

<? 
if($program != ""){ ?>
	<br>You have chosen the program: <strong><?=$programName?></strong>
	<input name="txtProgId" type="hidden" value="<?=$program?>">
<? } ?>
 <div class="tblItem"><strong></strong> <span class="errorSubtitle">
    <?=$err;?>
    <?php
    if (isset($designRequirementsError) && $designRequirementsError)
    {
        if ($err)
        {
            echo '<br>';    
        }
        echo $designRequirementsError;
    }
    ?>
  </span>
 </div> 

<?php
// Display ACO/PAL form if any CSD PhD program has been selected.
if (isCsdPhdApplication($_SESSION['appid']))
{
    include('../inc/acoPal.inc.php');
}
  
// change test for displaying waiver for other programs    
//  if ($enableMastersReviewWaiver) {
if (isset($domainid)) {
    switch ($domainid)  {
        case 1:
        case 3:
        case 11:
        case 15:
        case 16:
        case 17:
        case 18:
        case 22:
        case 32:
        case 34:
        case 35:
        case 36:
        case 37:
        case 39:
        case 40:
        case 41:
        case 43:
        case 51:
        case 52:
        case 57:
        case 59:
        case 60:
        case 62:
        case 63:
        case 65:
        case 66:
        case 67:
        case 69:
        case 70:
        case 73:
        case 74:
        ?>
        
        <p style="color: #840000;">
There are additional M.S. programs in SCS with different deadlines and requirements. 
Information regarding these programs can be found at 
<a href="http://www.cs.cmu.edu/prospectivestudents/masters/index.html" 
style="text-decoration: underline;" target="_blank">SCS Masters Programs</a>.
</p>

 <div style="width: 100%;">    
    <input type="hidden" name="formSubmitted" value="1">    
    <?php    
    if ($err != "") {        
        echo"<br>";    
    }    

        echo <<<EOB
        <p>
        Occasionally, some graduate programs may be looking for a very limited number of 
        additional well-qualified applicants. In the event that your application is not accepted to a
        program you selected above, do you give permission for your application to be reviewed by 
        other interested SCS graduate programs? 
        </p>
EOB;

        ob_start();
        showEditText($mastersReviewWaiver, "checkbox", "chkMastersReviewWaiver", 
            $_SESSION['allow_edit'], false);
        $inputElementString = ob_get_contents();
        ob_end_clean();
        
        $inputElementReplace = ' onClick="form1.submit();">';
        $inputElementString = str_replace('>', $inputElementReplace, $inputElementString);
        
        echo $inputElementString . ' I give permission for my application to be reviewed by other interested 
            SCS graduate programs. However, I understand that if I am interested in a particular graduate program,
            then I should apply directly to that program.';
        break;
    
      default:
        showEditText($mastersReviewWaiver, "hidden", "txtMastersReviewWaiver", $_SESSION['allow_edit']);
    }    
      
    ?>
    </div>
 
<?php      
} elseif ($domainid == 38 && $enableProgramDatesAgreement) {
?>    

<div style="width: 100%;">    
    <input type="hidden" name="programDatesSubmitted" value="1">
    
    <p>
    The Robotics Institute Summer Scholar Program runs June 1st through August 15th. 
    Scholars must be available for the entire length of the program. Only applicants available for these core 
    dates will be considered for the program.
    </p>
     
<?php  
    ob_start();
    showEditText($mastersReviewWaiver, "checkbox", "chkProgramDates", 
        $_SESSION['allow_edit'], false);
    $inputElementString = ob_get_contents();
    ob_end_clean();
    
    $inputElementReplace = ' onClick="form1.submit();">';
    $inputElementString = str_replace('>', $inputElementReplace, $inputElementString);    

    echo $inputElementString . ' I understand that the Summer Scholar Program runs June 1st through August 15th. 
        I confirm that I am available for these dates.';    
?>
</div>      
<?php
} elseif ($domainid == 38) {
    
    $programDatesQuery = "SELECT masters_review_waiver 
        FROM application WHERE id = " . intval($_SESSION['appid']);
    $programDatesResult = mysql_query($programDatesQuery);
    
    $mastersReviewWaiver = 0;
    while($row = mysql_fetch_array($programDatesResult)) {
        $mastersReviewWaiver = $row['masters_review_waiver'];    
    }
    
    showEditText($mastersReviewWaiver, "hidden", "txtProgramDates", $_SESSION['allow_edit']);
}

/*
* Design: attendance, assistantship
*/
if ($isDesignDomain || $isDesignPhdDomain)
{
    include('../inc/designAttendance.inc.php');
    include('../inc/designAssistantship.inc.php');
}
?>


<?php    
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?> 