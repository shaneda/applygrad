
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
include_once '../apply/priority_display.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>
<title><?=$HEADER_PREFIX[$domainid]?></title>
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$err = "";
$degrees = array();
$programs = array();
$myPrograms = array();
$areas = array();
$myAreas = array();
$depts = array();
$wasSubmitted = false;
$degree = "";
$program = "";
$programName = "";
$programDegree = "";
$area1 = 0;
$area2 = 0;
$area3 = 0;
$linkword = "in";

if($_SESSION['allow_edit'] == true && $_SESSION['appid'] != -1)
{
	//POST PROCESSING
	/*
	foreach($_POST as $key=>$val)
	{
		echo "key: ".$key . " val: ".$val."<br>";
	}
	*/
	if(isset($_POST))
	{
		$vars = $_POST;
		$itemId = -1;
		foreach($vars as $key => $value)
		{
			if( strstr($key, 'btnAddProg') !== false )
			{
				$arr = split("_", $key);
				$itemId = $arr[1];
				if( $itemId > -1 )
				{
					$program = $itemId;
					$programName = $_POST["txtProgName_".$itemId];
					$programDegree = $_POST["txtProgDegree_".$itemId];
				}//END IF
				
			
			//$program = intval($_POST['txtProgId']);
			$myAreas = array();
			$areasCount = 0;
			
			if($program < 0)
			{
				$err .= "You must select a program first.<br>";
			}//END PROGID
			
			if($err == "")
			{
				$doAdd = true;
				$degreename = "";
				$maxnum = 0;
				$phdCount = 0;
				$mastersCount = 0;
				$sql = "select program_id, choice,degree.name 
				from lu_application_programs
				inner join programs on programs.id = lu_application_programs.program_id
				inner join degree on degree.id = programs.degree_id
				where application_id =".$_SESSION['appid'];
				$result = mysql_query($sql) or die(mysql_error(). $sql);
				$itemid = -1;
				$progCount = 0;
				while($row = mysql_fetch_array( $result )) 
				{
					$degreename = $row['name'];
					$progCount++;
					if($row['choice'] > $maxnum)
					{
						$maxnum = $row['choice'];
					}
					if($row['program_id']==$program)
					{
						$doAdd == false;
						$err .= "PROGRAM ALREADY EXISTS IN SELECTION<br>";
					}
					if( strstr($degreename, 'Ph.D.') !== false 
					||  strstr($degreename, 'Ph.D. Dual Degree') !== false
					||  strstr($degreename, 'Ph.D./M.S.') !== false) 
					{
						$phdCount++;
					}
					if( strstr($degreename, 'M.S.') !== false 
						|| strstr($degreename, 'M.S./M.B.A.') !== false
						|| strstr($degreename, 'M.B.A.') !== false
						||  strstr($degreename, 'Ph.D./M.S.') !== false ) 
					{
						$mastersCount++;
					}
				}
				if($maxnum > $progCount )
				{
					$maxnum = $progCount;
				}
				$maxnum = $maxnum+1;
				
				//echo $degreename;
				if($phdCount > 1 && strstr($programDegree, 'Ph.D.') !== false )
				{
					$err .= "ERROR: You can only apply to 2 Doctoral programs.<br>";
				}
				if($mastersCount > 1 &&  strstr($programDegree, 'M.S.') !== false )
				{
					$err .= "ERROR: You can only apply to 2 Masters programs.<br>";
				}
			
				if($doAdd == true && $err == "")
				{
					$sql = "insert into lu_application_programs(application_id, program_id, choice)values( ".$_SESSION['appid'].", ".$program." , ".$maxnum.") ";
					$result = mysql_query($sql) or die(mysql_error());
					$itemid = mysql_insert_id();
					
					for($i = 0; $i<count($myAreas); $i++)
					{
						$sql = "insert into lu_application_interest(app_program_id, interest_id, choice)values( ".$itemid." ,".$myAreas[$i].", ".$i.")";
						$result = mysql_query($sql) or die(mysql_error());
					}
					$degree = "";
					$program = "";
					$area1 = 0;
					$area2 = 0;
					$area3 = 0;
					
				}//END IF doadd
				if($err == "")
				{
					//echo "updated";
					updateReqComplete("programs.php",1);
				}
			}//END IF ERR
				
				
				
				
				
			}//END IF ADD
			
			if( strstr($key, 'btnDelete') !== false )
			{
				$arr = split("_", $key);
				$itemId = $arr[1];
				if( $itemId > -1 )
				{
					
					mysql_query("DELETE FROM lu_application_programs WHERE id=".$itemId)
					or die(mysql_error());
					
					mysql_query("DELETE FROM lu_application_interest WHERE app_program_id=".$itemId)
					or die(mysql_error());
					
					//REORDER CHOICES
					$myProgs = array();
					$sql = "select id,program_id from lu_application_programs where application_id = ".$_SESSION['appid']. " order by choice";
					$result = mysql_query($sql) or die(mysql_error());
					$myProgs = array();
					while($row = mysql_fetch_array( $result )) 
					{
						$arr = array();
						array_push($arr, $row['id']);
						array_push($arr, $row['program_id']);				
						array_push($myProgs, $arr);
					}
					for($i = 0; $i < count($myProgs); $i++)
					{
					
						$sql = "update lu_application_programs set choice=".($i+1)." where id=".$myProgs[$i][0];
						mysql_query($sql) or die(mysql_error());
					}
					if(count($myProgs) == 0)
					{
						updateReqComplete("programs.php",0);
					}
			
				
				}//END IF
			}//END IF DELETE
			
			//HANDLE REORDERING
			if( strstr($key, 'btnDn') !== false ||  strstr($key, 'btnUp') !== false)
			{
				$direction = "down";
				$arr = split("_", $key);
				$itemId = $arr[1];
				
				if($arr[0] == "btnUp"){
					$direction = "up";
				}
				if( $itemId > -1 )
				{
					$myProgs = array();
					$sql = "select * from lu_application_programs where application_id = ".$_SESSION['appid']. " order by choice";
					$result = mysql_query($sql) or die(mysql_error());
					$i = 0;
					while($row = mysql_fetch_array( $result )) 
					{
						$i++;
						$arr = array();
						array_push($arr, $row[0]);
						array_push($arr, $row[1]);
						array_push($arr, $i);				
						array_push($myProgs, $arr);
					}
					$val = -1;
					for($i = 0; $i < count($myProgs); $i++)
					{
					
						if($myProgs[$i][0] ==  $itemId)
						{
							$val = $myProgs[$i];
						}
					}
					
					$myProgsSorted = arrayshift($myProgs,$val,$direction );
					for($i = 0; $i < count($myProgs); $i++)
					{
						$sql = "update lu_application_programs set choice = ".$myProgsSorted[$i][2]." where application_id = ".$_SESSION['appid']." and id =".$myProgs[$i][0];
						$result = mysql_query($sql) or die(mysql_error());
						
					}
					if($err == "")
					{
						updateReqComplete("programs.php",1);
					}
					break;
					
				}//END IF
			}//END IF
		
		}//END FOR

		
		// PROGRAMS
		
		$sql = "SELECT 
		programs.id,
		fieldsofstudy.name,
		degree.name as degreename,
		department.rank,
		programs.rank,
		programs.linkword,
		programs.url, 
		CASE programs.description WHEN '' THEN false WHEN NULL THEN false ELSE true END  as description
		FROM programs 
		inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		inner join degree on degree.id = programs.degree_id 
		inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
		inner join department on department.id = lu_programs_departments.department_id
		";
		if($_SESSION['domainid'] > -1)
		{
			$sql .= "
				inner join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
				where lu_domain_department.domain_id=".$_SESSION['domainid'];
		}
		switch($_SESSION['level'])
		{
			case 1:
			$sql .= " and degree.id=1 ";
			break;
			case 2:
			$sql .= " and degree.id>1 ";
			break;
			
		}
		if($_SESSION['domainid'] > -1)
		{
			$sql .= " order by lu_domain_department.rank, programs.rank, degree.name, fieldsofstudy.name";
		}else
		{
			$sql .= " order by department.rank, programs.rank, degree.name, fieldsofstudy.name";
		}
		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$dept = "";
			$depts = getDepts($row[0]);
			if(count($depts) > 1)
			{
				$dept = " - ";
				for($i = 0; $i < count($depts); $i++)
				{
					$dept .= $depts[$i][1];
					if($i < count($depts)-1)
					{
						$dept .= "/";
					}
					
				}
			}
			$arr = array();
			array_push($arr, $row[0]);
			array_push($arr, $row[2] . " ".$row['linkword']." ". $row[1].$dept);
			array_push($arr, $row[2]);
			array_push($arr, $row['url']);
			array_push($arr, $row['description']);
			array_push($programs, $arr);
		}
		
		
	}//END IF POST
	
	
	// DEGREES
	$result = mysql_query("SELECT * FROM degree order by name") or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name'] .' ('.$row['short'].')');

		array_push($degrees, $arr);
	}
	
}//END IF ALLOW EDIT

//GET USERS SELECTED PROGRAMS
$sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname, choice, lu_application_programs.id as itemid,
programs.linkword, programs.url,
CASE programs.description WHEN '' THEN false WHEN NULL THEN false ELSE true END  as description
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice";
$result = mysql_query($sql) or die(mysql_error() . $sql);
//echo $sql;

while($row = mysql_fetch_array( $result )) 
{
	$dept = "";
	$depts = getDepts($row[0]);
	if(count($depts) > 1)
	{
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$dept .= $depts[$i][1];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
			
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row['degreename']);
	array_push($arr, $row['fieldname'].$dept);
	array_push($arr, $row['choice']);
	array_push($arr, $row['itemid']);
	array_push($arr, $row['linkword']);
	array_push($arr, $row['url']);
array_push($arr, $row['description']);
	array_push($myPrograms, $arr);
}

function getDepts($itemId)
{
	$ret = array();
	$sql = "SELECT 
	department.id,
	department.name
	FROM lu_programs_departments 
	
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

function getAreas($id)
{
	$ret = array();
	//GET AREAS OF INTEREST
	$sql = "select interest.id,
	interest.name from
	lu_programs_interests 
	inner join interest on interest.id = lu_programs_interests.interest_id 
	where lu_programs_interests.program_id=".intval($id) ;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row[1]);

		array_push($ret, $arr);
	}
	return $ret;
}


function getMyAreas($id)
{
	$ret = array();
	$sql = "SELECT 
	interest.id,
	interest.name
	FROM 
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.id=".$id. " and lu_application_programs.application_id=".$_SESSION['appid'] . " order by lu_application_interest.choice";

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}



?>
<script language="javascript">
function loadState()
{

}
</script>

<!-- InstanceEndEditable -->

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1"><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
          <tr>
            <td valign="top">
			<div style="text-align:right; width:680px">
			<? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false
			){ ?>
				<a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout <? echo $sesEmail;?></a>
			<? }else
			{
				if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
				&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
				&& strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false)
				{
					//session_unset();
					//destroySession();
					//header("Location: index.php");
					?><a href="index.php" class="subtitle">Session expired - Login Again</a><?
				}
			} ?>
			</div>
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">Programs</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" -->
			<p class="tblItem">
<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='Programs' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?>
<br>

<span class="subtitle">
<?=$err;?></span><br>

<? 
$count = count($myPrograms);
if(count($myPrograms) > 0){ ?>

 Click on a section in the top menu bar to continue filling out your application.

 <hr size="1" noshade color="#990000"> 
  <table width="96%" border="0" cellpadding="4" cellspacing="1" class="tblItem">
    <tr>


<? if ($DISPLAY_PRIORITY[$domainid] == $TRUE ) { ?>
      <td width="70" class="tblHead2">Priority</td>
      <td class="tblHead2">Program(s) selected in order of preference </td>
<? } else { ?>
      <td width="70" &nbsp; </td>      
      <td class="tblHead2">Program Selection(s)</td>
<? } ?>



      <td width="70" align="right" class="tblHead2">Remove</td>
    </tr>
	<?
	for($i = 0; $i < count($myPrograms); $i++)
	{ 
	$areas = getAreas($myPrograms[$i][0]);
	$myAreas = getMyAreas($myPrograms[$i][4]);
	?>
	<tr>


<? if ($DISPLAY_PRIORITY[$domainid] == $TRUE ) { ?>
	  <td width="70" class="menuItemDesc">
	   <? if($myPrograms[$i][7] == true) { ?>
		 <input name="btnUp_<?=$myPrograms[$i][4];?>" type="image" class="tblItem" id="btnUp_<?=$myPrograms[$i][4];?>" value="UP" src="../images/arrowup.gif" title="Move Up">
	  <? }//END IF MYPROGRAMS ?>
		<? if($myPrograms[$i][3] <= $count-1) { ?>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input name="btnDn_<?=$myPrograms[$i][4];?>" type="image" class="tblItem" id="btnDn_<?=$myPrograms[$i][4];?>" value="DN" src="../images/arrowdn.gif" title="Move Down">
		<? }//END IF MYPROGRAMS ?>	  </td>
<? } else { ?>
      <td width="70" &nbsp; </td>      
<? } ?>


	  <td class="menuItemDesc"><strong><?=$i+1?> - <?
	  if($myPrograms[$i][6] != ""){ ?>
	  	<a href="<?="programinfo.php?id=".$myPrograms[$i][0];?>" title="Click for more information"><?=$myPrograms[$i][1]." ".$myPrograms[$i][5]." ".$myPrograms[$i][2];?></a>
	  <? }else {
	  	echo $myPrograms[$i][1]." ".$myPrograms[$i][5]." ".$myPrograms[$i][2];
	  } ?>
	  </strong><br>
	   <? 
	   if(count($areas)> 0 && count($myAreas) == 0){
	   ?>
	    <div style="margin-left:7px; "><em>
		<? if($_SESSION['allow_edit'] == true){ ?>
		<input name="btnInterests_" type="button" id="btnInterests_" class="tblItem" value="Add Areas of Interest" onClick="document.location='interests.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>'">
		<? } ?>
		 </em>		</div>
		<?
	   }
	   
	   if(count($myAreas)> 0){ ?>
	    <div style="margin-left:7px; ">Areas of Interest:<em>
		<a href="interests.php?id=<?=$myPrograms[$i][4]?>&p=<?=$myPrograms[$i][0]?>" title="Click to change areas of interest"><?
		
		for($j = 0; $j < count($myAreas); $j++)
		{
			echo $myAreas[$j][1];
			if($j < count($myAreas)-1)
			{
				echo ", ";
			}
		}//END FOR MYAREAS
		?></a> </em>		</div>
		<? }//end if myareas ?>	</td>
	  
		<td width="70" align="right">
		<? showEditText("X", "button", "btnDelete_".$myPrograms[$i][4], $_SESSION['allow_edit']); ?>
<span class="subtitle">		</span>	  </td>
	</tr>
	<?
	}//END FOR MYPROGRAMS
	?>
 </table>
<BR>


<? if ($DISPLAY_PRIORITY[$domainid] == $TRUE ) { ?>
     Click the 
     <img src="../images/arrowup.gif" alt="Up" width="11" height="6"> or 
     <img src="../images/arrowdn.gif" alt="down" width="11" height="6"> arrows to change your order of preference.<br>
<? } ?>
     <hr size="1" noshade color="#990000"> 

 <br>
 <br>


 <? }//END IF COUNT ?>


 <span class="subtitle">Select a program to add to your application </span><span class="tblItem">
 <table  border="0" cellpadding="4" cellspacing="2" class="tblItem">
<? 
$class = "tblItem";
$allowAdd = $_SESSION['allow_edit'];
if(count($programs) > 0)
{
for($i= 0; $i<count($programs); $i++){ 
if($i % 2 == 0){
	$class = "tblItem";
}else
{
	$class = "tblItemAlt";
}
?>
  <tr class="<?=$class?>">
    <td><?
	if($programs[$i][4] == true){ ?>
	<a href="<?="programinfo.php?id=".$programs[$i][0];?>" title="Click for more information on this program"><?=$programs[$i][1]?></a>
	<? }else{ ?>
	<?=$programs[$i][1]?>
	<? } ?>
      <input name="txtProgName_<?=$programs[$i][0]?>" type="hidden" id="txtProgName_<?=$programs[$i][0]?>" value="<?=$programs[$i][1]?>">
      <input name="txtProgDegree_<?=$programs[$i][0]?>" type="hidden" id="txtProgDegree_<?=$programs[$i][0]?>" value="<?=$programs[$i][2]?>"></td>
    <td>
      <? 
	  
	  for($j = 0; $j < count($myPrograms); $j++)
	  {
	  	$allowAdd = $_SESSION['allow_edit'];
	  	if($myPrograms[$j][0]==$programs[$i][0])
		{
			$allowAdd = false;
			//echo "match ". $myPrograms[$j][0]."<br>";
			break;
		}
		if($allowAdd == true)
		{
			//LOOKUP ANY LOCKOUTS BETWEEN PROGRAMS APPLIED FOR AND THIS PROGRAM
			$sql = "select id from multiprogramlockouts 
			where(program_id1=".$myPrograms[$j][0]." and program_id2=".$programs[$i][0].") 
			or (program_id1=".$programs[$i][0]." and program_id2=".$myPrograms[$j][0].")";
			$result2 = mysql_query($sql)or die(mysql_error());
			//echo $sql;
			while($row2 = mysql_fetch_array( $result2 ))
			{ 
				$allowAdd = false;
				break;
			}
			if($allowAdd == false)
			{
				break;
			}
		}//end if allowAdd
	  }//end for myprograms
	  showEditText("Add", "button", "btnAddProg_".$programs[$i][0], $allowAdd); ?>    </td>
  </tr>
  <? }//end for programs
  }else
  {
  	echo "No programs available.";
  }//end if count programs
   ?>
</table>
<hr size="1" noshade >
<? if($program != ""){ ?>
	<br>You have chosen the program: <strong><?=$programName?></strong>
	<input name="txtProgId" type="hidden" value="<?=$program?>">
<? } ?>
 </span>
 <p class="tblItem"><strong></strong> <span class="subtitle">
    <?=$err;?>
  </span><br>
        <br>
<!-- InstanceEndEditable --></div>
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false
			&& strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
