<?
include_once '../inc/config.php'; 
include_once '../inc/db_connect.php';
include_once '../apply/header_prefix.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';

include '../classes/class.Domain.php';

// Check for alias login.
session_start();
$aliasLogin = FALSE;
if(isset($_GET['a']) && isset($_GET['uid'])
    && isset($_SESSION['A_usertypeid']) 
    && ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1))
{
    $aliasLogin = TRUE;
}

/*
* Handle the session logic for login.  
*/
include '../classes/Session/class.SessionManagerApply.php';
$sessionManager = new SessionManagerApply($aliasLogin);

// If the domain id is in the request
$requestDomainId = filter_input(INPUT_GET, 'domain', FILTER_VALIDATE_INT); 
if ($requestDomainId) {

    if ( !Domain::isValid($requestDomainId) ) {
        $_SESSION['domainid'] = $requestDomainId;
        $_SESSION['domainname'] = 'SCS';
        header("Location: nodomain.php");
        exit;        
    }
    
    // If the request domain id != the session domain id
    if ( !isset($_SESSION['domainid']) || ( $requestDomainId != $_SESSION['domainid'] ) ) {

        // Redirect to error if the user is already logged in to a different domain.
        if (isset($_SESSION['domainid']) && $requestDomainId != $_SESSION['domainid']) {
            if (isset($_SESSION['usermasterid']) && $_SESSION['usermasterid'] > 0) {
                header("Location: loginConflict.php?domain=" . $requestDomainId);
                exit;    
            }
        }
        
        // Start a new session
        $sessionManager->newSession();

        // Set session domain id = request domain id
        $_SESSION['domainid'] = $requestDomainId;
        $params = session_get_cookie_params();
        setcookie('aw_domainid', $requestDomainId, 0,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
            );
        // Set the domain name
        $_SESSION['domainname'] = Domain::getName($_SESSION['domainid']); 
    }
    
    // If user is logged in already
    if ( $sessionManager->userLoggedIn() ) {
    
        // Redirect/forward to home
        header("Location: home.php");
        exit;
        
    } else {
       // Continue: prompt for login  
    } 

} else {
    
    // If domain id is not in the request but is set in the session
    if ( isset($_SESSION['domainid']) && Domain::isValid($_SESSION['domainid']) ) {
        
        // If unset, set the domain name
        if ( !isset($_SESSION['domainname']) || $_SESSION['domainname'] == '' ) {
            $_SESSION['domainname'] = Domain::getName($_SESSION['domainid']);    
        }
        
        // If user is logged in already
        if ( $sessionManager->userLoggedIn() ) {
        
            // Redirect/forward to home
            header("Location: home.php");
            exit;
            
        } else {
           // Continue: prompt for login  
        }        
        
    } else {
        
        // Domain id is not in the request or the session: redirect to "No domain"    
        header("Location: nodomain.php");
        exit; 
    }
}

/*
* Now start the original controller logic. 
*/
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if( isset($_SESSION['domainname']) && $domainname != '' )
{
    $domainname = $_SESSION['domainname'];
}
if( isset($_SESSION['domainid']) )                                      
{
    $domainid = $_SESSION['domainid'];
    if ($domainname == '') {
        $domainname = Domain::getName($domainid); 
        $_SESSION['domainname'] = $domainname;
    }
}
if( isset($_SESSION['email']) )
{
    $sesEmail = $_SESSION['email'];
}

/*
* Redirect stats domains from applyweb to stats and vice versa. 
*/
if ($hostname == "APPLY.STAT.CMU.EDU") {
    
    if ( $domainid != 44 && $domainid != 49 ) {
        header("Location: https://applygrad.cs.cmu.edu/apply/index.php?domain=" . $domainid);
        exit;         
    }
    
} else {
    
    if ( $hostname == "BANSHEE.CS.CMU.EDU" && ($domainid == 44 || $domainid == 49) ) {
        header("Location: https://apply.stat.cmu.edu/apply/index.php?domain=" . $domainid);
        exit;         
    }
}

/*
* Now check for domain periods and set allow_edit, etc. accordingly.
* Note: this depends on having $domainid set properly.
*/
include_once '../inc/checkPeriod.inc.php'; 

// Redirect to "offline" if no editing is allowed.
if ( !$_SESSION['allow_edit'] && !$_SESSION['allow_edit_late']
    && !$_SESSION['A_allow_admin_edit'] && !$_SESSION['A_allow_admin_edit_late'] 
    && !$aliasLogin ) 
{
        header("Location: offline.php");
        exit;     
}  
                                                                      
/*
* Now finish the original controller logic. 
*/
$err = "";

if(!isset($_POST['btnSubmit']) && !isset($_POST['btnLogin']))
{
    $_SESSION['firstname'] = "";
    $_SESSION['lastname'] = "";
    $_SESSION['userid'] = -1;
    $_SESSION['usermasterid'] = -1;
    $_SESSION['appid'] = -1;
    $_SESSION['email'] = "";    
}

$accounts = array();

//ALLOW ADMIN TO ASSUME ROLE OF APPLICANT
if($aliasLogin) {

    $_SESSION['appid'] = -1;
    $_SESSION['usermasterid'] = -1;
    $_SESSION['userid'] = -1;
    $_SESSION['firstname'] = "";
    $_SESSION['lastname'] = "";
    $_SESSION['email'] = "";
    $_SESSION['usertypeid'] = -1;
    
    $guid = filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
    
    //LOOKUP USER BY GUID
    $sql = "select users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email
        from users 
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        where guid = '" . mysql_real_escape_string($guid) . "' 
        and usertypes.id=5";

        $result = mysql_query($sql) 
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' . $sql);
        $doLogin = false;
        while($row = mysql_fetch_array( $result )) 
        {        
            // PLB added check for applicationId param 
            // to enable multiple applications per user 9/22/09.
            if ( isset($_GET['applicationId']) ) {
                $_SESSION['appid'] = $_GET['applicationId'];    
            }
            
            $usersId = $row['id'];
            $_SESSION['usermasterid'] = $usersId;
            $_SESSION['userid'] = $row['uid'];
            $_SESSION['firstname'] = $row['firstname'];
            $_SESSION['lastname'] = $row['lastname'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['usertypeid'] = $row['typeid'];
            $arr = array();
            //array_push($arr,$row['uid']);
            array_push($arr,$row['typeid']);
            array_push($arr,$row['typename']);
            array_push($accounts, $arr);
            $doLogin = true;
        }
        if($doLogin == true)
        {
            $_SESSION['aliasLogin'] = TRUE;
            $sessionManager->setLoggedIn($usersId);
            header("Location: home.php");
            exit;
        }
}

if(isset($_POST['btnLogin']))
{
    if(isset($_POST['txtEmail']) && isset($_POST['txtPass']) )
    {
        
        $email = filter_input(INPUT_POST, 'txtEmail', FILTER_VALIDATE_EMAIL);
        $email = htmlspecialchars($email);
        
        $rawPassword = filter_input(INPUT_POST, 'txtPass', FILTER_UNSAFE_RAW);
        $rawPassword = htmlspecialchars($rawPassword);
        $passwordHash = sha1($rawPassword);

        $sql = "select users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email
        from users 
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        where email = '" . mysql_real_escape_string($email) . "' 
        and password = '" . mysql_real_escape_string($passwordHash) . "'";
        // PLB added test to restrict login to student accounts.
        $sql .= " AND lu_users_usertypes.usertype_id = 5";
        $result = mysql_query($sql) 
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' . $sql);

        while($row = mysql_fetch_array( $result )) 
        {
            $usersId = $row['id'];
            $_SESSION['usermasterid'] = $usersId;
            $_SESSION['userid'] = $row['uid'];
            $_SESSION['firstname'] = $row['firstname'];
            $_SESSION['lastname'] = $row['lastname'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['usertypeid'] = $row['typeid'];
            $arr = array();
            //array_push($arr,$row['uid']);
            array_push($arr,$row['typeid']);
            array_push($arr,$row['typename']);
            array_push($accounts, $arr);
            //echo $row['email'];
            //echo $_SESSION['email'];
        }

        if($_SESSION['userid'] == -1 || $_SESSION['usermasterid'] == -1)
        {
            $err .= "Invalid username or password.<br>";
        }else
        {
            if(count($accounts) > 1)
            {
            
            }
            else{
                if(count($accounts) == 1)
                {
                    $sessionManager->setLoggedIn($usersId);
                    header("Location: home.php");
                }
            }
        }
    }
    else{
        $err .= "Please enter both your email address and password.<br>";
        $_SESSION['userid'] = -1;
    }
} 
else
{
    // Obsolete???? Remove????
    if(isset($_POST['btnSubmit']))
    {
        if(intval($_POST['lbUserType']) > -1)
        {
            $sql = "select lu_users_usertypes.id as uid
            from users 
            inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
            inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
            where users.id =" . intval($_SESSION['usermasterid']) . " 
            and lu_users_usertypes.usertype_id=". intval($_POST['lbUserType']);
            $result = mysql_query($sql) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' . $sql);
            while($row = mysql_fetch_array( $result )) 
            {
                $_SESSION['userid'] = $row['uid'];
                
            }
            $_SESSION['usertypeid'] = intval($_POST['lbUserType']);
            header("Location: home.php");
        }else
        {
            $err .="Please select a user type to log in as.<br>";
        }
    }
    
}
//GET ACCOUNT TYPES
$sesEmail = $_SESSION['email'];

// Include the shared page header elements.
$pageTitle = '';
include '../inc/tpl.pageHeaderApply.php';
?>

    <span class="errorSubtitle"><?=$err;?></span>            
    <div style="width:100%" class="tblItem">
    <?
    if(isset($_SESSION['domainid']))
    {
        if($_SESSION['domainid'] > -1)
        {
            $domainid = $_SESSION['domainid'];
        }
    }
    if ($domainid == -1) {
        header("Location: nodomain.php");
    }

    $sql = "select content from content where name='Index Page' and domain_id=" . intval($domainid);
    $result = mysql_query($sql)    
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' . $sql);
    while($row = mysql_fetch_array( $result )) 
    {
        echo html_entity_decode($row["content"]);
    }
    ?>
    
      <div align="center">
      <? 
      /*
      if(count($accounts) > 1){ ?>
      <br>
      You are authorized to log in under more than one account type.<br>
    Please choose the account that you wish to use from the list below:
    <br>
    <? showEditText("", "listbox", "lbUserType", true,true, $accounts); ?>
    <? showEditText("Login", "button", "btnSubmit", true); ?>
    <span class="subtitle">
    
    </span><br>
    <? } else { 
    */
    ?>
     <table width="150" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td colspan="2" class="subTitle">
            <? echo "Returning Applicant"; ?>
            </td>
          </tr>
          <tr>
            <td class="tblItem">User ID :</td>
            <td class="tblItem"><input name="txtEmail" type="text" class="tblItem" id="txtEmail" maxlength="100" tabindex="1" ></td>
          </tr>
          <tr>
            <td class="tblItem">Password:</td>
            <td class="tblItem"><input name="txtPass" type="password" class="tblItem" maxlength="50" tabindex="2"></td>
          </tr>
          <tr>
            <td class="tblItem"><input type="submit" name="btnLogin" value="Login" class="tblItem" tabindex="3" alt="Login"></td>
            <td class="tblItem"><a href="forgotPassword.php">Forgot Password?</a> </td>
          </tr>
          <tr>
            <td colspan="2" class="tblItem">
            <br>
            <br>
                <?
                /* 
                * If the submission deadline is changed to 12/17, applicants
                * will be able to log in and submit, but they will not be given 
                * a "register as new" button. 
                */
                /*
                if ($_SESSION['expdate'] == "2010-12-17 23:59:59")
                { 
                    $_SESSION['allow_edit'] = true; 
                }
                */ 
                $currentTimestamp = time();
                $submissionDeadlineTimestamp = strtotime($_SESSION['expdate']); 
                $gracePeriodTimestamp = $submissionDeadlineTimestamp; // + 172800;
                //$gracePeriodDeadline = date('Y-m-d H:i:s', $gracePeriodTimestamp);
                if ( ($_SESSION['domainid'] == 1 || $_SESSION['domainid'] == 3)
                    && ($currentTimestamp > $submissionDeadlineTimestamp)
                    && ($currentTimestamp <= $gracePeriodTimestamp) ) 
                {
                    $_SESSION['allow_edit'] = true;    
                }
                else 
                {                              
                    if($_SESSION['allow_edit'] == true ){
                    ?>
                    <input type="button" name="btnNew" value="Register as New Applicant" class="tblItem" tabindex="3" alt="Register as New User" onClick="document.location='accountCreate.php'">
                    <? 
                    } // end allow_edit if 
                } // end expdate if 
                ?>
            </td>
          </tr>
        </table>
      <a href="accountCreate.php" ></a> <br>
    <? // } //end if accounts ?>
    </div> <!-- end align center -->
    </div> <!-- end width 100% -->

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>
