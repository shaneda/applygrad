<?php
// Include the payment manager classes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
include '../classes/class.Payment.php';

include_once '../inc/config.php'; 
include_once '../inc/session.php';
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php';
include '../inc/specialCasesApply.inc.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
} 

/* 
* Include applyPeriod.inc.php to make $activePeriod available
* when checking whether to use the higher fee.
* NOTE: the include sets 
* $unit (a unit object),
* $activePeriod (a period object) and 
* $activePeriodId
*/
include '../inc/applyPeriod.inc.php';

$err = "";
$myPrograms = array();
$cardTypes = array(array('Visa', 'Visa'), array('MasterCard','MasterCard'), array('American Express','American Express'));
$paymentTypes = array(array('Mail Payment', 'Mail Payment'), array('Credit Card','Credit Card'));
$states = array();
$countries = array();
$months = array();
$years = array();
// Set below by payment manager.
//$amt = totalCost($_SESSION['appid']);
$total = 0.0;
$ptype = "";
$customer_firstname = "";
$customer_lastname = "";
$customer_email = "";
$bill_address1 = "";
$bill_city = "";
$bill_state = "";
$bill_zip = "";
$bill_country = "";
$customer_phone = "";
$card_type = "";
$customer_cc_number = "";
$customer_cc_expmo = "";
$customer_cc_expyr = "";
$customer_cc_code = "";
$paid = false;

$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}

/*
* Set store number 
*/
$storeNumber = getStoreNumber($_SESSION['domainid']);

// PLB added server, environment test 7/31/09
switch($_SERVER['SERVER_NAME']) {
    case 'rogue.fac.cs.cmu.edu':
        $docrootExtra = '/htdocs';    
    default:
        $docrootExtra = '';    
}
switch($environment) {
    case 'development':
        $docrootExtra .= '/_dev';
        break;
    case 'test':
        $docrootExtra .= '/_test';
        break;
    default:
        $docrootExtra .= '';
}
$thankyouurl = "https://".$_SERVER['HTTP_HOST'] . $docrootExtra . "/apply/success.php";
$gLstr = "6160200000100002127012101";

$poststr = "";

for($i = 1; $i < 13; $i++)
{
	$arr = array($i, $i);
	array_push($months, $arr);
}
for($i = 0; $i < 10; $i++)
{
	$k = date("Y")+$i;
	$arr = array($k, $k);
	array_push($years, $arr);
}

if(isset($_GET['btnCancel']))
{
	header("Location: home.php");
}


if(isset($_POST['btnCancel']))
{
	header("Location: home.php");
}

//STATES
$result = mysql_query("SELECT * FROM states order by country_id,name")or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['abbrev']);
	array_push($arr, $row['name']);
	array_push($states, $arr);
}
$result = mysql_query("SELECT * FROM countries order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['iso_code']);
	array_push($arr, $row['name']);
	array_push($countries, $arr);
}

//GET USERS SELECTED PROGRAMS
$sql = "SELECT 
programs.id, 
degree.name as degreename,
fieldsofstudy.name as fieldname, 
choice, 
lu_application_programs.id as itemid,
programs.oraclestring,
programs.programprice,
programs.baseprice, 
programs.linkword
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result )) 
{
	$deptId = -1;
	$dept = "";
	$baseprice = 0.0;
	$oracle = $row['oraclestring'];
	$depts = getDepartments($row[0]);
	$tmpDept = "";
	$deptEmail = "";
	if(count($depts) > 0)
	{
		
		$dept = " - ";
		for($i = 0; $i < count($depts); $i++)
		{
			$deptId = $depts[$i][0];
			$dept .= $depts[$i][1];
			if (!$oracle)
            {
                $oracle = $depts[$i][2];
            }
			$baseprice = $depts[$i][3];
			$deptEmail = $depts[$i][4];
			if($i < count($depts)-1)
			{
				$dept .= "/";
			}
			
		}
	}
	$arr = array();
	array_push($arr, $row[0]);
	array_push($arr, $row['degreename']);
	array_push($arr, $row['fieldname'].$dept);
	array_push($arr, $row['choice']);
	array_push($arr, $row['itemid']);
	array_push($arr, $row['programprice']);
	array_push($arr, $oracle);
	array_push($arr, $row['baseprice']);
	array_push($arr, $deptEmail);
	array_push($arr, $row['linkword']);
	array_push($arr, $dept);
	array_push($myPrograms, $arr);
}

function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT 
	department.id,
	department.name,
	department.oraclestring,
	cc_email
	FROM lu_programs_departments 
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($arr, $row["oraclestring"]);
		array_push($arr, "");
		array_push($arr, $row["cc_email"]);
		array_push($ret, $arr);
	}
	return $ret;
}

// Instantiate the payment manager so its available for the remaining logic.
if(isset($activePeriod)) {
    $higherFeeDate = $activePeriod->getHigherFeeDate();    
} else {
    $higherFeeDate = NULL;
}
$paymentManager = new PaymentManager($_SESSION['appid'], $higherFeeDate);

// Don't allow cc payment if no balance due.
if ($paymentManager->getBalanceDue() == 0) {
    header('Location: pay.php');
    exit;
}

// Include the shared page header elements.
$pageTitle = 'Credit Card Payment';
include '../inc/tpl.pageHeaderApply.php';
?>

<!--
<span class="tblItem"><strong><a href="home.php"></a></strong></span>
<input class="tblItem" name="btnBack" type="button" id="btnBack" value="Return to Homepage" onClick="document.location='home.php'">
-->

<span class="errorSubtitle"><?=$err?></span>

<?
//   Show the page content prior to submission of credit card payment
//   Once credit card payment made and end up back at this page
//   only need to display the message that its pending and
//   the return to home button

if(isset($_SESSION['paid']))
{
	if($_SESSION['paid'] == true)
	{
		$paid = true;
	}
}
if($paid != true)
{
	$domainid = 1;
	if(isset($_SESSION['domainid']))
	{
		if($_SESSION['domainid'] > -1)
		{
			$domainid = $_SESSION['domainid'];
		}
	}

	$sql = "select content from content where name='Payment Credit Page' and domain_id=".$domainid;
	
	$result = mysql_query($sql)	or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		echo html_entity_decode($row["content"]);
	}

    // PLB changed amt to be set by payment manager 8/6/09
    $totalBalanceDue = $paymentManager->getBalanceDue();           
            
    // Buffer the output so the merchant_ref_no can be handled properly
    ob_start(); 
    $programBalancesDue = $paymentManager->getProgramBalancesDue();
    $newPaymentItems = array();     // PLB added newPaymentItems 8/6/09 
    $paymentItemCount = 1;
    $myProgramsCount = count($myPrograms);
    for($i = 0; $i < $myProgramsCount; $i++)
    {
        if($i == 0) {
            if($myPrograms[$i][8] != "") {
                $paymentEmail = $myPrograms[$i][8];
            }
        }
        
        $programId = $myPrograms[$i][0];
        
        $programBalanceDue = 0;
        if (isIniDomain($domainid) || isEM2Domain($domainid))
        {
            $programBalanceDue = $totalBalanceDue / $myProgramsCount;   
        }
        else
        {
            if (array_key_exists($programId, $programBalancesDue)) 
            {
                $programBalanceDue = $programBalancesDue[$programId];    
            }    
        }
        
        if($programBalanceDue > 0) {
            $newPaymentItems[] = array('program_id' => $programId, 'payment_item_amount' => $programBalanceDue);
            if ($paymentItemCount > 1) {
                $itemCode = getAppFeeStoreItem($domainid);
                showEditText($itemCode, "hidden", "itemcode".($paymentItemCount), $_SESSION['allow_edit_late']); 
                showEditText("App Fee ".$myPrograms[$i][10], "hidden", "desc".($paymentItemCount), $_SESSION['allow_edit_late']);
                showEditText(1, "hidden", "qty".($paymentItemCount), $_SESSION['allow_edit_late']); 
                showEditText($programBalanceDue, "hidden", "amount".($paymentItemCount), $_SESSION['allow_edit_late']); 
                showEditText($myPrograms[$i][6], "hidden", "gl".($paymentItemCount), $_SESSION['allow_edit_late']);
            } else {
                $itemCode = getAppFeeStoreItem($domainid);
                showEditText($itemCode, "hidden", "itemcode", $_SESSION['allow_edit_late']);
                showEditText("App Fee ".$myPrograms[$i][10], "hidden", "desc".($paymentItemCount), $_SESSION['allow_edit_late']); 
                showEditText(1, "hidden", "qty", $_SESSION['allow_edit_late']); 
                showEditText($programBalanceDue, "hidden", "amount", $_SESSION['allow_edit_late']); 
                showEditText($myPrograms[$i][6], "hidden", "gl", $_SESSION['allow_edit_late']);
                
            }
    
            $paymentItemCount++;
        }
        echo "\n";
    }
    // Put the buffer contents in a string for display below.
    $itemInputs = ob_get_contents();
    ob_end_clean();
            
    // Go ahead and handle any new payment.
    $merchantRefNo = $_SESSION['appid'];
    if(isset($_POST['btnSubmit']) )
    {   
        if($paid == false)
        {
            // PLB added new payment tracking 8/4/09
            $newPayment = array(
                'payment_type' => 2, 
                'payment_amount' => $totalBalanceDue,
                'payment_intent_date' => date('Y-m-d H:i:s')
                );
            $paymentManager->savePayment($newPayment, $newPaymentItems);
            $newPaymentId = $paymentManager->getNewPaymentId();
            $merchantRefNo .= '_' . $newPaymentId;
        }
    }       
    ?>
	<br><br>

	<!-- <input name="txtCredit" type="hidden" value="1">  -->
	<?
    showEditText($storeNumber, "hidden", "virtual", $_SESSION['allow_edit_late']); 
    // showEditText($totalBalanceDue, "hidden", "amt", $_SESSION['allow_edit_late'],true);
    // showEditText($storeNumber, "hidden", "store_no", $_SESSION['allow_edit_late']);
    // showEditText($thankyouurl, "hidden", "signout_url", $_SESSION['allow_edit_late']);
	//showEditText("Return to Online Admissions", "hidden", "return_url_text", $_SESSION['allow_edit_late']);
	// showEditText($merchantRefNo, "hidden", "merchant_ref_no", $_SESSION['allow_edit_late']); 
	// showEditText("Y", "hidden", "settle_now", $_SESSION['allow_edit_late']); 
	
	echo $itemInputs;

	// showEditText($paymentEmail, "hidden", "notify_email", $_SESSION['allow_edit_late']);
	showEditText($_SESSION['firstname'] . " ".$_SESSION['lastname'], "hidden", "Flex_Field1", $_SESSION['allow_edit_late']);
	showEditText($_SESSION['email'], "hidden", "Flex_Field2", $_SESSION['allow_edit_late']);
	showEditText($_SESSION['appid'], "hidden", "Flex_Field3", $_SESSION['allow_edit_late']);
	showEditText($_SESSION['domainname'], "hidden", "Flex_Field4", $_SESSION['allow_edit_late']);
	showEditText('Application Fee', "hidden", "Flex_Field5", $_SESSION['allow_edit_late']);	
    /* if ($storeNumber == "CMU175" || $storeNumber == "CMU175test") {
             $poststr = "<script language='javascript'>document.form1.action='"."https://train.cashnet.com/CMU175test"."'; document.form1.submit(); </script>";
    } else { 
    */
	$poststr = "<script language='javascript'>document.form1.action='".$paymentProcessor.$storeNumber."'; document.form1.submit(); </script>";
/*    }       */

	showEditText("Proceed To Pay By Credit Card", "button", "btnSubmit", $_SESSION['allow_edit_late']);
	showEditText("Cancel/Return to Homepage", "button", "btnCancel", $_SESSION['allow_edit_late']);
            
}else
{
			  echo "Your payment is now pending. Please allow 2-3 business days for your credit card payment to be 
                                recorded as <em>Paid</em> on your submitted application.";
?>
    <BR><BR>
    <span class="tblItem"><strong><a href="home.php"></a></strong></span>
    <input class="tblItem" name="btnBack" type="button" id="btnBack" value="Return to Homepage" onClick="document.location='home.php'">
<?
}
			  
if(isset($_POST['btnSubmit']) )
{
	
	if($paid == false)
	{
		/*
        $sql = "update application set paymenttype = 2, paymentdate ='".date("Y-m-d h:i:s")."', paymentamount=".$total." where id=".$_SESSION['appid'];
		mysql_query($sql)or die(mysql_error());
		$_SESSION['paid'] = true;
		*/
        // PLB added new payment tracking 8/4/09
        /*
        $newPayment = array('payment_type' => 2, 'payment_amount' => $totalBalanceDue);
        $paymentManager->savePayment($newPayment, $newPaymentItems);
        $newPaymentId = $paymentManager->getNewPaymentId();
        */

        echo $poststr;
	}
}
			

// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>  