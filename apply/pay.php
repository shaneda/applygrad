<?php                     
// Include the payment manager classes.
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
include '../classes/class.Payment.php';

include_once '../inc/config.php';
include_once '../inc/session.php';
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php';
include '../inc/specialCasesApply.inc.php'; 

/* 
* Include applyPeriod.inc.php to make $activePeriod available
* when checking whether to use the higher fee.
* NOTE: the include sets 
* $unit (a unit object),
* $activePeriod (a period object) and 
* $activePeriodId
*/
$domainid = -1;
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
include '../inc/applyPeriod.inc.php';

if(isset($_POST['btnCancel'])) {
    header("Location: home.php");
}

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname'])) {
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid'])) {
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email'])) {
	$sesEmail = $_SESSION['email'];
}

$err = "";

//GET USERS SELECTED PROGRAMS
// (PLB moved up so array would be set for use in getPaymentItems())
$myPrograms = array();

$sql = "SELECT programs.id, 
degree.name as degreename,
fieldsofstudy.name as fieldname, 
choice, 
lu_application_programs.id as itemid,
programs.oraclestring,
programs.programprice,
programs.baseprice
FROM lu_application_programs 
inner join programs on programs.id = lu_application_programs.program_id
inner join degree on degree.id = programs.degree_id
inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
where lu_application_programs.application_id = ".$_SESSION['appid']." order by choice";
$result = mysql_query($sql) or die(mysql_error() . $sql);

while($row = mysql_fetch_array( $result )) 
{
    $dept = "";
    $baseprice = 0.0;
    $oracle = $row['oraclestring'];
    $depts = getDepartments($row[0]);
    $tmpDept = "";
    if(count($depts) > 1)
    {        
        $dept = " - ";
        for($i = 0; $i < count($depts); $i++)
        {
            $dept .= $depts[$i][1];
            if (!$oracle)
            {
                $oracle = $depts[$i][2];
            }
            $baseprice = $depts[$i][3];
            if($i < count($depts)-1)
            {
                $dept .= "/";
            }        
        }
    }
    $arr = array();
    array_push($arr, $row[0]);
    array_push($arr, $row['degreename']);
    array_push($arr, $row['fieldname'].$dept);
    array_push($arr, $row['choice']);
    array_push($arr, $row['itemid']);
    array_push($arr, $row['programprice']);
    array_push($arr, $oracle);
    array_push($arr, $row['baseprice']);

    array_push($myPrograms, $arr);
}


/*
* Set special cases & store number 
*/
$isIsreeDomain = isIsreeDomain($_SESSION['domainid']);
$isDesignDomain = isDesignDomain($_SESSION['domainid']);
$isDesignPhdDomain = isDesignPhdDomain($_SESSION['domainid']);
$isSdsDomain = isSdsDomain($_SESSION['domainid']);
$isEngineeringDomain = isEngineeringDomain($_SESSION['domainid']);
$storeNumber = getStoreNumber($_SESSION['domainid']);

$cardTypes = array(
    array('Visa', 'Visa'), 
    array('MasterCard','MasterCard'), 
    array('American Express','American Express')
);

$paymentTypes = array(
    array('Mail Payment', 'Mail Payment'), 
    array('Credit Card','Credit Card')
);

if ($isIsreeDomain) {
    if ($_SESSION['domainid'] != 50) {
        $paymentTypes[] = array('Voucher', 'Voucher'); 
    }
    $paymentTypes[] = array('Wire Transfer', 'Wire Transfer');
    $paymentTypes[] = array('Other', 'Other'); 
}

if ($isDesignDomain || $isDesignPhdDomain)
{
    $paymentTypes = array(
        array('Credit Card','Credit Card')
    );    
}

$paymentTypeIndex = array(
    1 => 'Check / Money Order',
    2 => 'CC',
    3 => 'Voucher',
    4 => 'Other',
    5 => 'Wire Transfer'
);

// PLB added server, environment test 7/31/09
switch($_SERVER['SERVER_NAME']) {
    case 'rogue.fac.cs.cmu.edu':
        $docrootExtra = '/htdocs';    
    default:
        $docrootExtra = '';    
}
switch($environment) {
    case 'development':
        $docrootExtra .= '/_dev';
        break;
    case 'test':
        $docrootExtra .= '/_test';
        break;
    default:
        $docrootExtra .= '';
}
$thankyouurl = "https://".$_SERVER['HTTP_HOST'] . $docrootExtra . "/apply/payccthanks.php";
//$thankyouurl = "https://".$_SERVER['HTTP_HOST'] . "/apply/payccthanks.php";

// Instantiate payment manager with higherFeeDate
if(isset($activePeriod)) {
    $higherFeeDate = $activePeriod->getHigherFeeDate();    
} else {
    $higherFeeDate = NULL;
}
$paymentManager = new PaymentManager($_SESSION['appid'], $higherFeeDate);

//$amt = totalCost($_SESSION['appid']);
$amt = $paymentManager->getTotalFee();

// PLB: start inserting payment_item records for non-CC payments 8/26/11            
$totalCost = $amt;
$balanceDue = $paymentManager->getBalanceDue();
$ptype = NULL;
$paymentId = NULL;
if(isset($_POST['btnSubmit']))
{
	$ptype = $_POST['lbType'];

	if($ptype == "Mail Payment" || $ptype == "Check") {
		
        // Prevent insertion of new payment on reload.
        if ($paymentManager->getBalanceDue() > 0) {
        
            $sql = "update application set paymenttype = 1, paymentdate ='";
            $sql .= date("Y-m-d h:i:s")."', paymentamount=".$amt." where id=".$_SESSION['appid'];
		    mysql_query($sql)or die(mysql_error());
            
            $newPayment = array(
                            'payment_type' => 1, 
                            'payment_amount' => $balanceDue,
                            'payment_intent_date' => date('Y-m-d H:i:s')
                            );
            $paymentManager->savePayment($newPayment, getPaymentItems());
            $paymentId = $paymentManager->getNewPaymentId();
        
            // Redirect to prevent re-submission of form.
            header('Location: pay.php');
        }
    }

    if($ptype == "Voucher") {
        
        $sql = "update application set paymenttype = 3, paymentdate ='";
        $sql .= date("Y-m-d h:i:s")."', paymentamount=".$amt." where id=".$_SESSION['appid'];
        mysql_query($sql)or die(mysql_error());

        $newPayment = array(
                        'payment_type' => 3, 
                        'payment_amount' => $balanceDue,
                        'payment_intent_date' => date('Y-m-d H:i:s')
                        );
        $paymentManager->savePayment($newPayment, getPaymentItems());
        $paymentId = $paymentManager->getNewPaymentId();
        
        // Redirect to file upload.
        header('Location: fileUpload.php?id=' . $paymentId . '&t=21');        
    }

    if($ptype == "Other") {
        
        $sql = "update application set paymenttype = 4, paymentdate ='";
        $sql .= date("Y-m-d h:i:s")."', paymentamount=".$amt." where id=".$_SESSION['appid'];
        mysql_query($sql)or die(mysql_error());

        $newPayment = array(
                        'payment_type' => 4, 
                        'payment_amount' => $balanceDue,
                        'payment_intent_date' => date('Y-m-d H:i:s')
                        );
        $paymentManager->savePayment($newPayment, getPaymentItems());
        $paymentId = $paymentManager->getNewPaymentId();
        
        // Redirect to prevent re-submission of form.
        header('Location: pay.php');       
    }

    if($ptype == "Wire Transfer") {
        
        $sql = "update application set paymenttype = 5, paymentdate ='";
        $sql .= date("Y-m-d h:i:s")."', paymentamount=".$amt." where id=".$_SESSION['appid'];
        mysql_query($sql)or die(mysql_error());

        $newPayment = array(
                        'payment_type' => 5, 
                        'payment_amount' => $balanceDue,
                        'payment_intent_date' => date('Y-m-d H:i:s')
                        );
        $paymentManager->savePayment($newPayment, getPaymentItems());
        $paymentId = $paymentManager->getNewPaymentId();
        
        // Redirect to prevent re-submission of form.
        header('Location: pay.php');       
    }
    
}
 
if( isset($_POST['btnCancelPayment']) ) {
    $paymentType = filter_input(INPUT_POST, 'payment_type', FILTER_VALIDATE_INT);
    if ($paymentType && $paymentType != 2) {
        //DebugBreak();
        foreach ($paymentManager->getPayments() as $payment) {
            if ( $payment['payment_type'] == $paymentType
                && $payment['payment_status'] == 'pending' ) 
            {
                $paymentRecord = array(
                    'payment_id' => $payment['payment_id'],
                    'payment_status' => 'void'
                );
                $paymentManager->savePayment($paymentRecord);
            }    
        }
    }
}

// Get together balances and payment types.
$payments = $paymentManager->getPayments();
$balanceDue = $paymentManager->getBalanceDue(); 
$balanceUnpaid = $paymentManager->getBalanceUnpaid();
$feeWaived = $paymentManager->getStatusFeeWaived();
$paymentType = NULL;
$nonvoidPayments = array();
foreach ($payments as $payment) {
    // Assumption here is that multiple payments will be of the same type,
    // or that the last array element will be the latest payment.
    if ($payment['payment_status'] != 'void' 
        && $payment['payment_status'] != 'refund') 
    {
        $paymentType = $payment['payment_type'];
        $nonvoidPayments[] = $payment;        
    }
}

// Include the shared page header elements.
$pageTitle = 'Payment';
include '../inc/tpl.pageHeaderApply.php';

//if ($_SESSION['domainid'] == 42) {
if ($isIsreeDomain) {
    $documentName = 'registration';    
} else {
    $documentName = 'application';    
}
?>

<span class="tblItem"><strong><a href="home.php"></a></strong></span>
<input class="tblItem" name="btnBack" type="button" id="btnBack" value="Return to Homepage" onClick="document.location='home.php'">
<br><br>

<span class="errorSubtitle"><?=$err?></span>
<table width="100%"  border="0" cellpadding="4" cellspacing="2" class="tblItem">
    <tr>
    <td width="200" align="right"><strong>Total <?php echo ucfirst($documentName); ?> Fee: </strong></td>
    <?php // PLB added balance, changed payment amount from total to balance 8/5/09 ?>  
    <td>$<?php echo $amt; ?></td>
    </tr>
    <?php
    if ($nonvoidPayments && $balanceDue > 0) {
    ?>
        <tr valign="top">
        <td width="200" align="right"><strong>Prior Payments: </strong></td>
        <?php // PLB added balance, changed payment amount from total to balance 8/5/09 ?>  
        <td>
        <?php 
        foreach ($nonvoidPayments as $priorPayment) {
            if ($priorPayment['payment_status'] == 'pending' || $priorPayment['payment_status'] == 'paid') {
                $priorPaymentType = $paymentTypeIndex[$priorPayment['payment_type']];
                echo '<i>$' . $priorPayment['payment_amount'];
                echo ' (' . $priorPaymentType . ', ' . $priorPayment['payment_status'] . ')</i><br>';
            }
        }
        ?>
        </td>
        </tr>
    <?    
    }
    ?>
    <tr>
        <td width="200" align="right"><strong>Balance Due: </strong></td>
        <td>$<?php 
        /*
        * All these hidden inputs are for submission to payccconfirm.php 
        */
        if ($feeWaived) {
            echo '0';
        } else {
            //echo $balanceUnpaid;
            echo $balanceDue; 
        }
        // showEditText($amt, "hidden", "amt", true,true);
        // showEditText($storeNumber, "hidden", "store_no",true);
        // showEditText($thankyouurl, "hidden", "return_url", true);
        // showEditText("Return to Online Admissions", "hidden", "return_url_text", true);
        // showEditText($_SESSION['appid'], "hidden", "merchant_ref_no", true);
        // showEditText("Y", "hidden", "settle_now", true);
        // showEditText($paymentEmail, "hidden", "notify_email", true);
        showEditText("Graduate Online Application Fee", "hidden", "item1_name", true);       
        $myProgramCount = count($myPrograms);
        for($i = 0; $i < $myProgramCount; $i++)
        {
          $amt = 0.0;
          if (isIniDomain($domainid) || isEM2Domain($domainid))
          {
            $amt = $balanceDue / $myProgramCount;    
          }
          else
          {
              if($i == 0)
              {
                $amt = $myPrograms[$i][7];
              }
              else
              {
                $amt = $myPrograms[$i][5];
              }   
          }
          showEditText($myPrograms[$i][1]. "_".($i+1), "hidden", "item".($i+1)."_name", true); 
          showEditText(1, "hidden", "item".($i+1)."_qty", true); 
          showEditText($amt, "hidden", "item".($i+1)."_price_each", true); 
          showEditText($myPrograms[$i][6], "hidden", "item".($i+1)."_gl_str", true); 
          echo "\n";
        }
        ?>
        </td>
    </tr>
    <tr valign="top">
            <?php
            if ($balanceDue <= 0 || $feeWaived) {
            ?>
                <td width="200" align="right"><strong>Status: </strong></td>
                <td>
            <?php
                if ($feeWaived) {
                
                    echo 'Fee Waived';
                    
                } else {
                
                    if ($balanceUnpaid > 0) {
                        echo "Pending receipt of payment(s):<br>";
                        foreach ($nonvoidPayments as $nonvoidPayment) {
                            $nonvoidPaymentType = $paymentTypeIndex[$nonvoidPayment['payment_type']];                            
                            if ($nonvoidPayment['payment_status'] == 'paid') {    
                                echo '<i>$' . $nonvoidPayment['payment_amount'];
                                echo ' (' . $nonvoidPaymentType . ' , paid)</i>';
                            } else {
                                echo '$' . $nonvoidPayment['payment_amount'];
                                echo ' (' . $nonvoidPaymentType . ')';     
                            }
                            echo '<br>';
                        }
                    } else {
                        echo "$" . $totalCost . " paid in full<br>";
                        foreach ($nonvoidPayments as $nonvoidPayment) {
                            $nonvoidPaymentType = $paymentTypeIndex[$nonvoidPayment['payment_type']];
                            if ($nonvoidPayment['payment_status'] == 'paid') {
                                echo '<i>$' . $nonvoidPayment['payment_amount'];
                                echo ' (' . $nonvoidPaymentType . ')</i><br>';                                
                            }
                        }  
                    }
                    
                    switch ($paymentType) {
                        
                        case 1:
                        
                            if ($balanceUnpaid > 0) {
                                //echo "Pending receipt of payment of $" . $balanceUnpaid . " by check or money order.";
                                ?>
                                <br><br>
                                <input type="hidden" name="payment_type" value="<?php echo $paymentType; ?>">
                                <input class="tblItem" name="btnCancelPayment" type="submit" id="btnCancelPayment" value="Cancel Check Payment" onClick="return confirm('Cancel Payment?');">
                                <?php     
                            } else {
                                //echo "$" . $totalCost . " paid in full by check or money order.";    
                            } 
                            break;
                        
                        case 2:
                        
                            if ($balanceUnpaid > 0) {
                                //echo "Pending confirmation of $" . $balanceUnpaid . " credit card payment.";     
                            } else {
                                //echo "$" . $totalCost . " paid in full by credit card.";    
                            } 
                            break;
                        
                        case 3:

                            if ($balanceUnpaid > 0) {
                                //echo "Pending confirmation of upload of $" . $balanceUnpaid . " voucher.";     
                                ?>
                                <br><br>
                                <input type="hidden" name="payment_type" value="<?php echo $paymentType; ?>">
                                <input class="tblItem" name="btnCancelPayment" type="submit" id="btnCancelPayment" value="Cancel Voucher Payment" onClick="confirm('Cancel Payment?');">
                                <?php 
                            } else {
                                //echo "$" . $totalCost . " paid in full by voucher.";    
                            }
                            break;

                        case 4:

                            if ($balanceUnpaid > 0) {
                                //echo "Pending confirmation of $" . $balanceUnpaid . " payment (other).";     
                                ?>
                                <br><br>
                                <input type="hidden" name="payment_type" value="<?php echo $paymentType; ?>">
                                <input class="tblItem" name="btnCancelPayment" type="submit" id="btnCancelPayment" value="Cancel Payment (Other)" onClick="confirm('Cancel Payment?');">
                                <?php
                            } else {
                                //echo "$" . $totalCost . " paid in full (other).";    
                            }
                            break;

                        case 5:

                            if ($balanceUnpaid > 0) {
                                //echo "Pending confirmation of $" . $balanceUnpaid . " payment (other).";     
                                ?>
                                <br><br>
                                <input type="hidden" name="payment_type" value="<?php echo $paymentType; ?>">
                                <input class="tblItem" name="btnCancelPayment" type="submit" id="btnCancelPayment" value="Cancel Wire Transfer Payment" onClick="confirm('Cancel Payment?');">
                                <?php
                            } else {
                                //echo "$" . $totalCost . " paid in full (other).";    
                            }
                            break;                            

                        default:
                            // do nothing
                    }
                }               
            ?>
                </td>        
            <?php
            } else {
                //DebugBreak();
                ?>
            <td width="200" align="right"><strong>Payment Method: </strong></td>
            <td>
            <? 
                if ($isDesignDomain || $isSdsDomain || $isEngineeringDomain)
                {
                ?>
                    Credit Card
                    <input type="hidden" name="lbType" value="Credit Card">
                <?php    
                }
                else
                {
                    showEditText($ptype, "listbox", "lbType", $_SESSION['allow_edit_late'],true, $paymentTypes);
                } 
            ?>
            </td>
            <?php
            }
            ?>
          </tr>
          
          <?php
          if ($ptype == "Credit Card") { 
          ?>
            <script language='javascript'>document.location='payccconfirm.php';</script> 
          <?php
          } else {
          ?>
            <tr>
            <td width="200" align="right">&nbsp;</td> 
            <td>
          <?php    
               switch ($paymentType) {
                   
                   case 1:
                   
                        // check
                        if ($balanceUnpaid > 0) {
                            echo getContent("Payment Mail Page", $domainid);
                        }
                        break;  
                   
                   case 2:
                   
                        // credit card - do nothing;
                        break;
                   
                   case 3:
                                         
                        // Show info for previous vouchers.
                        // $payments is from line 129.
                        //DebugBreak();
                        foreach ($payments as $payment) {

                            if ( $payment['payment_type'] == 3 
                                && $payment['payment_status'] == 'pending' 
                                ) {

                                // Display the voucher file upload controls.
                                if ($balanceUnpaid > 0) {
                                ?>
                                    <br/>
                                    <input class="tblItem" name="btnUpload" value="Upload New Voucher" type="button" 
                                        onClick="parent.location='fileUpload.php?id=<?php echo $payment['payment_id']; ?>&t=21'" />                        
                                <?php
                                }

                                $voucherQuery = "SELECT payment.application_id, 
                                    lu_users_usertypes.user_id AS users_id, datafileinfo.* 
                                    FROM payment
                                    INNER JOIN payment_voucher ON payment.payment_id = payment_voucher.payment_id 
                                    INNER JOIN datafileinfo ON payment_voucher.datafileinfo_id = datafileinfo.id
                                    INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
                                    WHERE payment.payment_id = " . $payment['payment_id'];
                                $voucherResult = mysql_query($voucherQuery) or die(mysql_error());
                                while ($row = mysql_fetch_array($voucherResult)) {
                                    showFileInfo("voucher." . $row['extension'], $row['size'], 
                                        formatUSdate2($row['moddate']), 
                                        getFilePath(21, $payment['payment_id'], 
                                        $row['user_id'], $row['users_id'], $row['application_id'])
                                        );    
                                } 
                            }
                        }
                                   
                        break;
                   
                   case 4:
                   
                        //echo '--"Other" payment content here.--';
                        if ($balanceUnpaid > 0) {
                            echo getContent("Payment Other Page", $domainid);
                        }
                        break;
                   
                   case 5:
                   
                        if ($balanceUnpaid > 0) {
                            echo getContent("Payment Wire Transfer Page", $domainid);
                        }
                        break;

                   default:
                        // do nothing         
               }
               ?>
               </td></tr>
          <?php
          } 
          ?>
			<tr>
            <td align="right">&nbsp;</td>
            <td>
              <?php 
              if ($balanceDue > 0 && !$feeWaived) {
                showEditText("Submit Payment Method", "button", "btnSubmit", $_SESSION['allow_edit_late']); 
                showEditText("Cancel/Return to Homepage", "button", "btnCancel", true); 
              }
              ?>
			</td>
          </tr>	 
        </table>
        </div>

        <br/><br/>
		<span class="tblItem">
        <?=date("Y")?> Carnegie Mellon University 
		<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
		&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
		<? } ?>
		</span>
		</div>
		</td>
      </tr>
    </table>
  
    </form>
    
</body>
</html>

<?php
function getDepartments($itemId)
{
    $ret = array();
    $sql = "SELECT 
    department.id,
    department.name,
    department.oraclestring,
    systemenv.appbaseprice
    FROM lu_programs_departments 
    inner join department on department.id = lu_programs_departments.department_id
    inner join lu_domain_department on lu_domain_department.department_id
    inner join systemenv on systemenv.domain_id = lu_domain_department.domain_id
    where lu_programs_departments.program_id = ". $itemId;

    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $arr = array();
        array_push($arr, $row["id"]);
        array_push($arr, $row["name"]);
        array_push($arr, $row["oraclestring"]);
        array_push($arr, $row["appbaseprice"]);
        array_push($ret, $arr);
    }
    return $ret;
}

function getContent($contentName, $domainid) {

    $content = '';
    $sql = "select content from content where name='";
    $sql .= $contentName . "' and domain_id=".$domainid;
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array($result) ) {
        $content = html_entity_decode($row["content"]);
    }
    return $content;      
}

function getPaymentItems() {

    global $paymentManager;
    global $myPrograms; 

    $programBalancesDue = $paymentManager->getProgramBalancesDue();
    $paymentItems = array();
    $paymentItemCount = 1;
    for($i = 0; $i < count($myPrograms); $i++) {
        
        $programId = $myPrograms[$i][0];
        
        if (array_key_exists($programId, $programBalancesDue)) {
        
            $programBalanceDue = $programBalancesDue[$programId];
            if($programBalanceDue > 0) {
                
                $paymentItems[] = array('program_id' => $programId, 'payment_item_amount' => $programBalanceDue);
                $paymentItemCount++;
            }
        }
    }
    
    return $paymentItems;
}
?>