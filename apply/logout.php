<?
include_once '../inc/config.php'; 
//include_once '../inc/session.php'; 
include_once '../inc/db_connect.php'; 
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

include '../classes/Session/class.SessionManagerApply.php';

session_start();
$_SESSION['initiated'] = TRUE;
$aliasLogin = FALSE;
if ( isset($_SESSION['aliasLogin']) ) {
    $aliasLogin = $_SESSION['aliasLogin'];
}

$sessionManager = new SessionManagerApply($aliasLogin);

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";

if( isset($_SESSION['domainid']) && $_SESSION['domainid'] != -1)
{
    $domainid = $_SESSION['domainid'];
} 

if (!$domainid || $domainid == -1) 
{
    header("Location: nodomain.php");
    exit;     
}

if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}

if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$domainid = -1;
$recommender = false;
if(isset($_GET['domain']))
{
    $domainid = intval($_GET['domain']);
}

if(isset($_GET['r']))
{
    $recommender = true;
}

if(isset($_SESSION['usertypeid']))
{
    if($_SESSION['usertypeid'] == 6)
    {
        $recommender = true;
    }
}



if ($recommender && !$aliasLogin) {

    $sessionManager->destroySession();
    
} else {

    $sessionManager->newSession($aliasLogin);
    $_SESSION['domainid'] = $domainid;
    $_SESSION['domainname'] = $domainname; 
}

/*
unset($_SESSION["usertypeid"]);
unset($_SESSION["usermasterid"]);
unset($_SESSION["userid"]);
unset($_SESSION["firstname"]);
unset($_SESSION["lastname"]);
unset($_SESSION["email"]);
unset($_SESSION["appid"]);
unset($_SESSION["usertypeid"]);
unset($_SESSION["usertypeid"]);
*/

// Include the shared page header elements.
$pageTitle = 'Logout';
include '../inc/tpl.pageHeaderApply.php';

if($recommender == true) {
 
	$domainid = 1;
	if(isset($_SESSION['domainid']))
	{
		if($_SESSION['domainid'] > -1)
		{
			$domainid = $_SESSION['domainid'];
		}
	}
	
	$sql = "select content from content where name='Logout Page for Recommenders' and domain_id=".$domainid;
	
	$result = mysql_query($sql)	or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		echo html_entity_decode($row["content"]);
	}

} else { 
?> 
	<span class="tblItem">You have successfully logged off the Applygrad system.<br>
    <br>
    <?php
    if (isset ($_SERVER['HTTP_REFERER']) && strstr($_SERVER['HTTP_REFERER'], 'replyform') === FALSE) {
        ?>
    <a href="index.php?domain=<?=$domainid?><? if($recommender==true){echo "&r=1";}?>"><strong>Click here</strong></a> to log in again.
    <?php } ?>
    </span>
<?php 
} 

// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>