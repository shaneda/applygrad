<?php
  //GET ADVISORS
  function getAdvisors ($domainid) {
      $advisors = array (array ());
          // testing purposes  $domainid = 2;
      
      $sql = "select lu_users_usertypes.id as uid, concat(users.lastname , ', ' , users.firstname) as name,
      users_info.additionalinfo
      from lu_users_usertypes
      left outer join users on users.id = lu_users_usertypes.user_id
      left outer join users_info on users_info.user_id = lu_users_usertypes.id
      left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
      left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id 
      where lu_users_usertypes.usertype_id=8 and lu_domain_department.domain_id=".$domainid." group by users.id
      order by lastname, firstname";
      $result = mysql_query($sql) or die(mysql_error());
      //echo $sql;
      while($row = mysql_fetch_array( $result ))
      {
        $arr = array();
    
        array_push($arr, $row['uid']);
        array_push($arr, $row['name']);
    
        array_push($advisors, $arr);
        }
    return $advisors;
  }
  
  function getAdvisorsByProgram ($progid, $domainid) {
      $advisors = array (array ());
          // testing purposes  $domainid = 2;
      
      $sql = "select lu_users_usertypes.id as uid, concat(users.lastname , ', ' , users.firstname) as name,
      users_info.additionalinfo
      from lu_users_usertypes
      left outer join users on users.id = lu_users_usertypes.user_id
      left outer join users_info on users_info.user_id = lu_users_usertypes.id
      left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
      left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id 
      inner join lu_programs_departments on lu_programs_departments.department_id = lu_domain_department.department_id
      and lu_programs_departments.program_id = " . $progid .
      " where lu_users_usertypes.usertype_id=8 and lu_domain_department.domain_id= " . $domainid . " group by users.id
      order by users.lastname, users.firstname";
      
      $result = mysql_query($sql) or die(mysql_error());
      //echo $sql;
      while($row = mysql_fetch_array( $result ))
      {
        $arr = array();
    
        array_push($arr, $row['uid']);
        array_push($arr, $row['name']);
                                                      
        array_push($advisors, $arr);
        }
    return $advisors;
  }
  
  
  function getDeptsAcceptingAdvisorRequest () {
      global $domainid;
      $depts = array();
      $sql = "select department.id from department, lu_domain_department
      where lu_domain_department.domain_id = ".$domainid. 
      " and department.allowRequestAdvisors > 0 and lu_domain_department.department_id = department.id";
      $result = mysql_query($sql) or die(mysql_error());
      while($row = mysql_fetch_array( $result ))
      {
        array_push($depts, $row['id']);
      }
      return $depts;
  }
  
  function getDeptAdvisorSelectionType ($deptId) {
      $retVal = 0;
      $sql = "select department.allowRequestAdvisors from department
      where department.id = ".$deptId;
      $result = mysql_query($sql) or die(mysql_error());
      if($row = mysql_fetch_array( $result )) {
          $retVal = $row['allowRequestAdvisors'];
      }
      return ($retVal);
        
  }
 
  function getProgramAdvisors($progid)
{
    global $domainid;
    $ret = getAdvisorsByProgram($progid, $domainid);
    return $ret;
}


/*
select * from lu_users_usertypes, lu_user_department where lu_users_usertypes.usertype_id = 8 
and lu_users_usertypes.id = lu_user_department.user_id
and lu_user_department.department_id = 8;
*/
function getDepts($itemId)
{
    $ret = array();
    $sql = "SELECT 
    department.id,
    department.name
    FROM lu_programs_departments 
    
    inner join department on department.id = lu_programs_departments.department_id
    where lu_programs_departments.program_id = ". $itemId;

    $result = mysql_query($sql)
    or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $arr = array();
        array_push($arr, $row["id"]);
        array_push($arr, $row["name"]);
        array_push($ret, $arr);
    }
    return $ret;
}

function getMyAdvisors($id)
{
    $ret = array();
    $sql = "select application_id,advisor_user_id,advisor_type,concat(users.lastname , ', ' , users.firstname) as name,program_id
from lu_application_advisor
inner join lu_users_usertypes on lu_application_advisor.advisor_user_id = lu_users_usertypes.id
inner join users on lu_users_usertypes.user_id = users.id
where lu_application_advisor.application_id = ".$_SESSION['appid']." and lu_application_advisor.program_id = ".$id;


    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $arr = array();
        array_push($arr, $row["advisor_user_id"]);
        array_push($arr, $row["application_id"]);
        array_push($arr, $row["name"]);
        array_push($arr, $row["program_id"]);
        array_push($ret, $arr);
    }
    return $ret;
}

function getMyAdvisorsWithOthers($id)
{
    $ret = array();
    $sql = "select application_id,advisor_user_id,advisor_type,concat(users.lastname , ', ' , users.firstname) as name,program_id
from lu_application_advisor
inner join lu_users_usertypes on lu_application_advisor.advisor_user_id = lu_users_usertypes.id
inner join users on lu_users_usertypes.user_id = users.id
where lu_application_advisor.application_id = ".$_SESSION['appid']." and lu_application_advisor.program_id = ".$id.
" union select application_id,advisor_user_id,advisor_type,name,program_id
from lu_application_advisor
where lu_application_advisor.application_id = ".$_SESSION['appid']." and lu_application_advisor.advisor_user_id = 1 and lu_application_advisor.program_id = ".$id;

    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $arr = array();
        array_push($arr, $row["advisor_user_id"]);
        array_push($arr, $row["application_id"]);
        array_push($arr, $row["name"]);
        array_push($arr, $row["program_id"]);
        array_push($ret, $arr);
    }
    return $ret;
}

function getMyAdvisorsWithAppId($appid, $id) {
    $ret = array();
    $sql = "select application_id,advisor_user_id,advisor_type,name,program_id
    from lu_application_advisor
    where application_id = ".$appid." and program_id = ".$id;

    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result ))
    { 
        $name = $row["name"];
        array_push($ret, $name);
        
    }
    return $ret;
}

function getDeptFromProgram($programId)
{
    $ret = 0;
    $sql = "SELECT 
    department_id
    FROM lu_programs_departments
    where program_id = ". $programId;

    $result = mysql_query($sql)
    or die(mysql_error());
    
    if ($row = mysql_fetch_array( $result ))
    { 
        $ret = $row["department_id"];
    }
    return $ret;
}
?>
