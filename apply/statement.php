<?php
    include_once '../inc/config.php'; 
    include_once '../inc/session.php'; 
    include_once '../inc/db_connect.php';
    $exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
    include_once '../inc/functions.php';
    include_once '../inc/prev_next.php';
    include_once '../inc/applicationFunctions.php';
    include_once '../inc/specialCasesApply.inc.php';
    include_once '../apply/header_prefix.php'; 
    $_SESSION['SECTION']= "1";
    $domainname = "";
    $domainid = -1;
    $sesEmail = "";
    if(isset($_SESSION['domainname']))
    {
        $domainname = $_SESSION['domainname'];
    }
    if(isset($_SESSION['domainid']))
    {
        $domainid = $_SESSION['domainid'];
    }
    if(isset($_SESSION['email']))
    {
        $sesEmail = $_SESSION['email'];
    }

    $id = -1;
    $err = "";
    $resName = "";
    $resSize = 0;
    $resModDate = "";

    $stateName = "";
    $stateSize = "";
    $stateModDate = "";
    $resumeFileId = -1;
    $statementFileId = -1;

    $yearsExperience = '';

    //RETRIEVE USER INFORMATION
    $sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=2";
    $result = mysql_query($sql) or die(mysql_error());

    while($row = mysql_fetch_array( $result )) 
    {
        $resName  = "resume.".$row['extension'];
        $resSize  = $row['size'];
        $resumeFileId = $row['id'];
        $resModDate = $row['moddate'];
    } 

    $sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=4";
    $result = mysql_query($sql) or die(mysql_error());

    while($row = mysql_fetch_array( $result )) 
    {
        $stateName  = "statement.".$row['extension'];
        $stateSize  = $row['size'];
        $statementFileId = $row['id'];
        $stateModDate = $row['moddate'];
    } 

    if($resModDate == "" || $stateModDate == "")
    {
        //updateReqComplete("resume.php", 0,1);
    }

    // used for finding the previous and next pages
    $appPageList = getPagelist($_SESSION['appid']);
    $curLocationArray = explode ("/", $currentScriptName);
    $curPageIndex = array_search (end($curLocationArray), $appPageList);
    if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
    {
        $prevPage = "";
    }
    if (sizeof ($appPageList) > $curPageIndex + 1)
    {
        $nextPage =  $appPageList[$curPageIndex + 1]; 
    } 
    else
    {
        $nextPage = "";
    }

    // Include the shared page header elements.
    $isRiFifthYearMastersDomain = isRiFifthYearMastersDomain($domainid);
    if ($domainid == 44 || $isRiFifthYearMastersDomain) {
        $pageTitle = 'Statement of Purpose';
    } else {
        $pageTitle = 'Resume and Statement';}
    include '../inc/tpl.pageHeaderApply.php';         
?>

<span class="tblItem">

    <?
        $domainid = 1;
        if(isset($_SESSION['domainid']))
        {
            if($_SESSION['domainid'] > -1)
            {
                $domainid = $_SESSION['domainid'];
            }
        }
        if ($domainid == 44) {
            $sql = "select content from content where name='Statement of Purpose' and domain_id=".$domainid;
        } else {
            $sql = "select content from content where name='Resume and Statement' and domain_id=".$domainid;
        }
        $result = mysql_query($sql)	or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            echo html_entity_decode($row["content"]);
        }
    ?>
    <br>
    <span class="errorSubTitle"><?=$err;?></span>

    <br>
    <? 
    if (!$isRiFifthYearMastersDomain)
    {
        showEditText("Save", "button", "btnSubmit", $_SESSION['allow_edit']); 
    }
    ?>
    <br>

    <hr size="1" noshade color="#990000">
</span>

<span class="subtitle">Statement of Purpose</span><span class="tblItem"><br>
    <? $qs = "?id=1&t=4"; ?>
    <input class="tblItem" name="btnUpload" value="Upload Statement of Purpose" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <? if($stateModDate != "")
        {
            showFileInfo($stateName, $stateSize, formatUSdate($stateModDate), getFilePath(4, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
        }//end if ?>
    <br><br>
    
    <hr size="1" noshade color="#990000">
    <? 
    if (!$isRiFifthYearMastersDomain)
    {
        showEditText("Save", "button", "btnSubmit", $_SESSION['allow_edit']); 
    }
    ?>
    <br>

</span>

<?php
    // Include the shared page footer elements. 
    include '../inc/tpl.pageFooterApply.php';
?>