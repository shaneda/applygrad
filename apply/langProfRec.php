<?php
/*
* Use minimal session handling logic instead of session.php.
*/
session_start();
$_SESSION['usertypeid'] = 22;

require_once("LangProfRecommender.class.php"); // include our class
$ajax=new LangProfRecommender();

include_once '../inc/config.php';
// include_once '../inc/session.php';
include_once '../inc/db_connect.php';
include_once '../apply/header_prefix.php'; 

$err = ""; 
if (isset($_SESSION['domainid'])) {
    $domainid = $_SESSION['domainid'];
    } else
        {
            $domainid = 1;
        }

$applicationId = filter_input(INPUT_GET, 'appid', FILTER_VALIDATE_INT);        
$formAction = 'langProficiencyRecommender.php';
if ($applicationId) {
    $formAction .= '?appid=' . $applicationId;
}

$includeAjax = TRUE;
include '../inc/tpl.pageHeaderRec.php';
?>

        <form method="post" name="form1" id="form1" action="<?php echo $formAction; ?>">
        <div style="margin:20px;width:660px;">
            <br>
            <br>
            <div class="tblItem" id="contentDiv">
            <span class="errorSubtitle"><?=$err;?></span>            
            <div style="width:100%" class="tblItem">
            <?
            $sql = "select content from content 
                where name='Index Page for Language Proficiency Recommenders' and domain_id=". intval($domainid);
            $result = mysql_query($sql) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            while($row = mysql_fetch_array( $result )) 
            {
                echo html_entity_decode($row["content"]);
            }
            ?>
             <div align="center">
                  
  <table border="0" style="font-size: 10pt; font-family: Verdana">
    <tr>
        <td style="width: 100px; height: 50px;">
            Username:
        </td>
        <td style="width: 400px; height: 50px;">
            <input type="text" id="username" name="username" style="width: 200px" />
            <?  // echo $ajax->bind("username", "onchange","checkUsername","username"); /* Bind an HTML object to a JavaScript event to call a PHP function  */  ?>
        </td>
    </tr>
    <tr>
        <td style="width: 100px; height: 50px;">
            Usercode:
        </td>
        <td style="width: 400px; height: 50px;">
            <input type="text" id="usercode" name="usercode" style="width: 200px" />
            <? // echo $ajax->bind("usercode", "onchange","checkUsercode","usercode"); /* Bind an HTML object to a JavaScript event to call a PHP function  */  ?>
            <div id="resultsDiv">&nbsp;</div>
        </td>
    </tr>
    <tr>
        <td colspan="2"  align="center">
            <input type="submit" id="button1" name="checkButton" value="Login" /> 
        </td>
    </tr>
</table>
</div></div></div>
<br><br>  
<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
            <? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
            &middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
            <? } ?>
            </span>
</div>
</form>

<!-- footer -->
</div>
</td></tr></table>
</body>
</html>