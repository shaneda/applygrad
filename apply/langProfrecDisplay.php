<?php
    $allowEdit = $_SESSION['allow_edit'];
    $submitted = "";
    if($myLangProfRec[1] > -1)
    {
        $allowEdit = false;
    }
    $title = "";
    switch($myLangProfRec[8])
    {
        case "1":
            $title = "Undergraduate";
        break;
        case "2":
            $title = "Graduate";
        break;
        case "3":
            $title = "Additional";
        break;

    }//end switch
       
    if($myLangProfRec[13] > 0)
    {
        $submitted = "(Language Proficiency Recommendation Requested)";
    } else {
        if( $myLangProfRec[6] != "" && !$isMsrtDomain )
        //if( $myLangProfRec[6] != "" && $_SESSION['domainname'] != 'RI-MS-RT' ) // Is there an email address?
        {
            $submitted = "(NOTE: Email Request for language proficiency recommendation has NOT been sent)";
        }
    }
    $attCount = sizeof($myLangProfRec);
    if ($attCount < 17) {
        if ($myLangProfRec[9] > 0) 
        {
         $submitted = "(Language Proficiency Report Received)";
         }
    } else {
        if (($myLangProfRec[9] > 0) || ($myLangProfRec[16] != NULL))
        {
         $submitted = "(Language Proficiency Report Received)";
         }
    }
    ?>
<!--BEGIN MAIN TABLE -->
<!-- REC TABLE -->
<table width="650" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td><span class="subtitle">Language Proficiency Recommender <?=($i+1)?> <?=$submitted?></span> </td>
    <td align="right">
<!-- Display DEL Button only after this recommender has been saved -->
<?      // If the recommender in question has been created, that means its been saved
//echo $i . " " . $NUMRECS;
        if ( $allowEdit == false || $i > $NUMRECS-1)
        {
        showEditText("Delete", "button", "btnDelete_".$myLangProfRec[0], $_SESSION['allow_edit']); 
        }
?>
    <input name="txtRecord_<?=$myLangProfRec[0];?>" type="hidden" id="txtRecord_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[0]?>" />
    <input name="txtUid_<?=$myLangProfRec[0];?>" type="hidden" id="txtUid_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[1]?>" />
    <input name="txtUMasterid_<?=$myLangProfRec[0];?>" type="hidden" id="txtUMasterid_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[15];?>">

    <input name="txtDatafile_<?=$myLangProfRec[0];?>" type="hidden" id="txtDatafile_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[9];?>">
    <input name="txtModdate_<?=$myLangProfRec[0];?>" type="hidden" id="txtModdate_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[10];?>">
    <input name="txtSize_<?=$myLangProfRec[0];?>" type="hidden" id="txtSize_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[11];?>">
    <input name="txtExtension_<?=$myLangProfRec[0];?>" type="hidden" id="txtExtension_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[12];?>">
    <input name="txtRecSent_<?=$myLangProfRec[0];?>" type="hidden" id="txtExtension_<?=$myLangProfRec[0];?>" value="<?=$myLangProfRec[13];?>">

</td>
  </tr>
</table>


<table border=0 cellpadding=1 cellspacing=1 class="tblItem" width="650">
<?  if(count($types) > 1){ ?>
<tr>
<td>Recommendation Type: <? showEditText($myLangProfRec[8], "listbox", "lbType_".$myLangProfRec[0], $_SESSION['allow_edit'],true, $types); ?></td>
<td></td>
<td></td>
</tr>
<? }else
{
    echo '<tr><td colspan="3">';
    showEditText("1", "hidden", "lbType_".$myLangProfRec[0], $_SESSION['allow_edit'],true, null);
    echo '</td></tr>';
} 
?>
<tr>
<td width="216"><strong>First Name:</strong><br>
<? showEditText($myLangProfRec[2], "textbox", "txtFname_".$myLangProfRec[0], $allowEdit,true,null,true,20); ?></td>
<td width="216"><strong>Last Name:</strong><br>
<? showEditText($myLangProfRec[3], "textbox", "txtLname_".$myLangProfRec[0], $allowEdit,true,null,true,20); ?></td>
<td width="218"><strong>Job Title:</strong><br>
<? 
if (isset($myLangProfRec[17]) && $myLangProfRec[17] != ''){
    $langProfRecTitle = $myLangProfRec[17];
} else {
    $langProfRecTitle = $myLangProfRec[4];    
}
showEditText($langProfRecTitle, "textbox", "txtTitle_".$myLangProfRec[0], $allowEdit,true,null,true,20); 
?>
</td>
</tr>
<tr>
<td><strong>Company/Institution:</strong><br>
<? 
if (isset($myLangProfRec[18]) && $myLangProfRec[18] != ''){
    $langProfRecAffiliation = $myLangProfRec[18];
} else {
    $langProfRecAffiliation = $myLangProfRec[5];    
}
showEditText($langProfRecAffiliation, "textbox", "txtAffiliation_".$myLangProfRec[0], $allowEdit,true,null,true,40); 
?>
</td>
<td><strong>Email:</strong><br>
<? showEditText($myLangProfRec[6], "textbox", "txtEmail_".$myLangProfRec[0], $allowEdit,true,null,true,30); ?> </td>
<td><strong>Phone:</strong><br>
<? 
if (isset($myLangProfRec[19]) && $myLangProfRec[19] != ''){
    $langProfRecPhone = $myLangProfRec[19];
} else {
    $langProfRecPhone = $myLangProfRec[7];    
}
showEditText($langProfRecPhone, "textbox", "txtPhone_".$myLangProfRec[0], $allowEdit,true,null,true,20,20); 
?>
</td>
</tr>
<tr>
<td width="216" colspan=3><strong>Language for recommender to evaluate your skills:</strong><br>
<? debugbreak(); showEditText($myLangProfRec[10], "textbox", "txtLangOfInterest_".$myLangProfRec[0], $allowEdit,true,null,true,30); ?></td>
</tr>
<tr>
<td colspan=3>
<? 
// If recommendation already received, do not show any buttons
 
if($myLangProfRec[9] == 0)
{   
if ($myLangProfRec[2] != "" && $allowEdit == false  
    && ($submitted != "(Language Proficiency Report Received)"))
{    

    if (!$isMsrtDomain) {
    //if ( $_SESSION['domainname'] != 'RI-MS-RT' ) {
    
        if($myLangProfRec[1] != "" && $myLangProfRec[13] > 0)  { ?>
        <p><b>
         Send email reminder to language proficiency recommender.</b> 
        <? showEditText("Send Reminder", "button", "btnSendReminder_".$myLangProfRec[0], $_SESSION['allow_edit'],false); 
        }else{
        
        ?>
        <p><b>
         Send email request to language proficiency recommender.</b> <? showEditText("Send Request", "button", "btnSendmail_".$myLangProfRec[0], $_SESSION['allow_edit'],false); 
         
        }
        
    } 
}
}
?>
</td>
</tr>
</table>
<!--END MAIN TABLE -->
<hr size="1" noshade color="#990000">
