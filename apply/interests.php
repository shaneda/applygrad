<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$areas = array();
$myAreas = array();
$id = -1;
$progid = -1;
$choices = array("1st Choice", "2nd Choice", "3rd Choice");

if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
}

if(isset($_GET['p']))
{
	$progid = intval($_GET['p']);
}

$isVlisProgram = isVlisProgram($progid);

if(isset($_POST['btnSubmit']))
{

	$hasAreas = false;
	$vars = $_POST;
	$areasCount = 0;
	$itemId = -1;
	
	foreach($vars as $key1 => $value1)
	{
		if( strstr($key1, 'lbArea') !== false )
		{
			$hasAreas = true;
			$areasCount++;
			$arr = split("_", $key1);
			$itemId = $arr[2];
			$appProgId = $arr[1];
			if( $itemId > -1 && $value1 != "")
			{
				if($value1 < 1)
				{
					//$err .= "Please select an interest from each list<br>";
				}
				//echo $value1 . " ";
				array_push($myAreas, $value1);
			}//END IF
		}//END IF ADD
	}//END FOR
	if($hasAreas == true && count($myAreas) == 0)
	{   
        // Enforce AOI selection for SCS/RI.
        if ($domainid == 1 || $domainid == 3) {
            if (!$isVlisProgram) {
                $err .= "Please select at least one area of interest for this program<br>";    
            } else {
                $err .= "Please select a preferred track for this program<br>";
            }
        }
		
	}
	$matched = false;
	for($i = 0; $i<count($myAreas); $i++)
	{
		for($j = 0; $j<count($myAreas); $j++)
		{
			if(($j != $i) && ($myAreas[$j] == $myAreas[$i]))
			{
				$matched = true;
			}
		}
	}
	if($matched == true)
	{
		$err .= "Error: Please change your areas of interest selections. They must be unique.<br>";
	}
	if($err == "")
	{
		$sql = "DELETE FROM lu_application_interest WHERE app_program_id=".$appProgId;
		mysql_query($sql)or die(mysql_error().$sql);
		
		for($i = 0; $i<count($myAreas); $i++)
		{
			if($myAreas[$i] != "")
			{
				$sql = "insert into lu_application_interest(app_program_id, interest_id, choice)values( ".$appProgId." ,".$myAreas[$i].", ".$i.")";
				$result = mysql_query($sql) or die(mysql_error().$sql);
			}
		}
        
        // Huh?? Always update program requirement as complete on save??
        // Maybe because you can't select an AOI without having selected a program??
        updateReqComplete("programs.php",1);
        
        // Enforce AOI selection for SCS/RI
        if ($domainid == 1 || $domainid == 3 || $domainid == 18) {
            
            foreach ($_SESSION['programs'] as $program) {
                
                $programId = $program[0];
                $programAreas = getAreas($programId);
                $myProgramAreas = getMyAreas($programId);
                
                // Set incomplete if the user doesn't have at least one AOI selected
                // for each program that has AOIs. 
                if (count($programAreas) > 0 && count($myProgramAreas) < 1) {
                    updateReqComplete("programs.php",0);
                    break;    
                }    
            }
        }
        
        
	}

}//end if submit

function getAreas($programId = NULL)
{
	global $progid;
    
    if ($programId) {
        $aoiProgramId = $programId;
    } else {
        $aoiProgramId = $progid;    
    }
    
	$ret = array();
	//GET AREAS OF INTEREST
	$sql = "select interest.id,
	interest.name from
	lu_programs_interests 
	inner join interest on interest.id = lu_programs_interests.interest_id 
	where lu_programs_interests.program_id=".intval($aoiProgramId). " order by name";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row[1]);

		array_push($ret, $arr);
	}
	return $ret;
}

function getMyAreas($id)
{
	$ret = array();
	$sql = "SELECT 
	interest.id,
	interest.name
	FROM 
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.program_id=".$id." and lu_application_programs.application_id=".$_SESSION['appid'] . " order by lu_application_interest.choice" ;

	$result = mysql_query($sql)	or die(mysql_error());
	
	while($row = mysql_fetch_array( $result ))
	{ 
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
	}
	return $ret;
}

// Include the shared page header elements.

if ($isVlisProgram) {
    $pageTitle = 'Preferred Track(s)';    
} else {
    $pageTitle = 'Areas of Interest';    
}
include '../inc/tpl.pageHeaderApply.php';

if ($isVlisProgram) {
?>

<p>
Please select your preferred track for the degree program. 
This preference helps us evaluate your application and manage enrollment numbers. 
Your preferred track may be changed if you are admitted to the degree program.
</p>

<?php  
} else {
?>

<p>
You must select at least one but no more than three areas of interest that most closely related to your current interests. 
This does not limit your application to any specific area, but rather gives the Admissions Committee a general idea of your interests.
</p>

<?php
}
?>

<span class="errorSubtitle"><?=$err?></span>
<br> 
<table border="0" cellspacing="2" cellpadding="4">

<?php
$isCsdProgram = isCsdProgram($progid);
$isRiProgram = isRiProgram($progid);
if ($isCsdProgram || $isRiProgram) {
    
    if ($isCsdProgram) {
        $researchUrl = 'http://www.csd.cs.cmu.edu/research/index.html';
        $researchTitle = 'Areas of Research guide';   
    } 
    if ($isRiProgram) {
        $researchUrl = 'http://www.ri.cmu.edu/research_guide/index.html';
        $researchTitle = 'Robotics Institute Research Guide';    
    }
?>
    <tr>
    <td colspan="2">
    <a href="<?php echo $researchUrl; ?>" target="_blank">Click here for the <?php echo $researchTitle; ?></a>
    <br><br>
    </td>
    </tr>
<?php 
}
?>
    <tr><td>
	  <? 
	  $areas = getAreas();
  	  $myAreas = getMyAreas($progid);

	  $choicesCount = 3;
	  if(count($areas) < 3)
	  {
		$choicesCount = count($areas);
	  }
      
	  for($j=0; $j< $choicesCount;$j++) {
      ?>

		<div class="tblItem">
		  <?
           
		  echo "<strong>".$choices[$j]."</strong><br>";
		  
          $myArea = "";
		  if(count($myAreas) > $j)
		  {
			$myArea = $myAreas[$j];
		  }
		//  echo "myArea ".$myArea[0]."<br>";
            if ($j == 0) {
                showEditText( $myArea, "listbox", "lbArea_".$id."_".$j, $_SESSION['allow_edit'],true, $areas);    
            } else {
		                showEditText( $myArea, "listbox", "lbArea_".$id."_".$j, $_SESSION['allow_edit'],false, $areas); 
            }?>
		</div>
        <br/><br/>
	  
	  <? }//END FOR CHOICESCOUNT ?>
	</td></tr>
</table>
<br>
<? showEditText("  Save  ", "button", "btnSubmit", $_SESSION['allow_edit']); ?> 			 
<input class="tblItem" name="btnInterests_" type="button" id="btnBack" value="Return to Programs" onClick="document.location='programs.php'">
<br>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>