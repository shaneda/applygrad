<?php
for($i = 0; $i < count($myRecommenders); $i++)
{
    $allowEdit = $_SESSION['allow_edit'];
    $submitted = "";
    if($myRecommenders[$i][1] > -1)
    {
        $allowEdit = false;
    }
    $title = "";
    switch($myRecommenders[$i][8])
    {
        case "1":
            $title = "Undergraduate";
        break;
        case "2":
            $title = "Graduate";
        break;
        case "3":
            $title = "Additional";
        break;

    }//end switch
       
    if($myRecommenders[$i][13] > 0)
    {
        $submitted = "(Letter of Recommendation Requested)";
    } else {
        if( $myRecommenders[$i][6] != "" && !$isMsrtDomain )
        //if( $myRecommenders[$i][6] != "" && $_SESSION['domainname'] != 'RI-MS-RT' ) // Is there an email address?
        {
            $submitted = "(NOTE: Email Request to this recommender has NOT been sent)";
        }
    }
    $attCount = sizeof($myRecommenders[$i]);
    if ($attCount < 17) {
        if ($myRecommenders[$i][9] > 0) 
        {
         $submitted = "(Letter of Recommendation Received)";
         }
    } else {
        if (($myRecommenders[$i][9] > 0) || 
            (isset($myRecommenders[$i][16]) && $myRecommenders[$i][16] != NULL))
        {
         $submitted = "(Letter of Recommendation Received)";
         }
    }
    ?>
<!--BEGIN MAIN TABLE -->
<!-- REC TABLE -->
<table width="650" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td><span class="subtitle">Recommender <?=($i+1)?> <?=$submitted?></span> </td>
    <td align="right">
<!-- Display DEL Button only after this recommender has been saved -->
<?      // If the recommender in question has been created, that means its been saved
//echo $i . " " . $NUMRECS;
        if ( $allowEdit == false || $i > $NUMRECS-1)
        {
        showEditText("Delete", "button", "btnDelete_".$myRecommenders[$i][0], $_SESSION['allow_edit']); 
        }
?>
    <input name="txtRecord_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtRecord_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][0]?>" />
    <input name="txtUid_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtUid_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][1]?>" />
    <input name="txtUMasterid_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtUMasterid_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][15];?>">

    <input name="txtDatafile_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtDatafile_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][9];?>">
    <input name="txtModdate_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtModdate_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][10];?>">
    <input name="txtSize_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtSize_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][11];?>">
    <input name="txtExtension_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtExtension_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][12];?>">
    <input name="txtRecSent_<?=$myRecommenders[$i][0];?>" type="hidden" id="txtExtension_<?=$myRecommenders[$i][0];?>" value="<?=$myRecommenders[$i][13];?>">

</td>
  </tr>
</table>


<table border=0 cellpadding=1 cellspacing=1 class="tblItem" width="650">
<?  if(count($types) > 1){ ?>
<tr>
<td>Recommendation Type: <? showEditText($myRecommenders[$i][8], "listbox", "lbType_".$myRecommenders[$i][0], $_SESSION['allow_edit'],true, $types); ?></td>
<td></td>
<td></td>
</tr>
<? }else
{
    echo '<tr><td colspan="3">';
    showEditText("1", "hidden", "lbType_".$myRecommenders[$i][0], $_SESSION['allow_edit'],true, null);
    echo '</td></tr>';
} 
?>
<tr>
<td width="216"><strong>First Name:</strong><br>
<? showEditText($myRecommenders[$i][2], "textbox", "txtFname_".$myRecommenders[$i][0], $allowEdit,true,null,true,20); ?></td>
<td width="216"><strong>Last Name:</strong><br>
<? showEditText($myRecommenders[$i][3], "textbox", "txtLname_".$myRecommenders[$i][0], $allowEdit,true,null,true,20); ?></td>
<td width="218"><strong>Job Title:</strong><br>
<? 
if (isset($myRecommenders[$i][17]) && $myRecommenders[$i][17] != ''){
    $recommenderTitle = $myRecommenders[$i][17];
} else {
    $recommenderTitle = $myRecommenders[$i][4];    
}
showEditText($recommenderTitle, "textbox", "txtTitle_".$myRecommenders[$i][0], $allowEdit,true,null,true,20); 
?>
</td>
</tr>
<tr>
<td><strong>Company/Institution:</strong><br>
<? 
if (isset($myRecommenders[$i][18]) && $myRecommenders[$i][18] != ''){
    $recommenderAffiliation = $myRecommenders[$i][18];
} else {
    $recommenderAffiliation = $myRecommenders[$i][5];    
}
showEditText($recommenderAffiliation, "textbox", "txtAffiliation_".$myRecommenders[$i][0], $allowEdit,true,null,true,40); 
?>
</td>
<td><strong>Email:</strong><br>
<? showEditText($myRecommenders[$i][6], "textbox", "txtEmail_".$myRecommenders[$i][0], $allowEdit,true,null,true,30); ?> </td>
<td><strong>Phone:</strong><br>
<? 
if (isset($myRecommenders[$i][19]) && $myRecommenders[$i][19] != ''){
    $recommenderPhone = $myRecommenders[$i][19];
} else {
    $recommenderPhone = $myRecommenders[$i][7];    
}
showEditText($recommenderPhone, "textbox", "txtPhone_".$myRecommenders[$i][0], $allowEdit,true,null,true,20,20); 
?>
</td>
</tr>

<?php
if ($isDesignDomain || $isDesignPhdDomain)
{
?>
<tr>
<td><strong>Relationship to Applicant:</strong><br>
<? 
showEditText($myRecommenders[$i][20], "textbox", "txtRelationshipToApplicant_".$myRecommenders[$i][0], $allowEdit,true,null,true,40); 
?>
</td>
<td></td>
<td></td>
</tr>
<?php
}
?>

<tr>
<td colspan=3>
<? 
// If recommendation already received, do not show any buttons
 
if($myRecommenders[$i][9] == 0)
{   
if ($myRecommenders[$i][2] != "" && $allowEdit == false  
    && ($submitted != "(Letter of Recommendation Received)"))
{    

    if (!$isMsrtDomain) {
    //if ( $_SESSION['domainname'] != 'RI-MS-RT' ) {
    
        if($myRecommenders[$i][1] != "" && $myRecommenders[$i][13] > 0)  { ?>
        <p><b>
         Send email reminder to  recommender.</b> 
        <? showEditText("Send Reminder", "button", "btnSendReminder_".$myRecommenders[$i][0], $_SESSION['allow_edit'],false); 
        }else{
        
        ?>
        <p><b>
         Send email request to recommender.</b> <? showEditText("Send Request", "button", "btnSendmail_".$myRecommenders[$i][0], $_SESSION['allow_edit'],false); 
         
        }
        
    } 
}
}
?>
</td>
</tr>
</table>
<!--END MAIN TABLE -->
<hr size="1" noshade color="#990000">
<?      
}//end for
?>