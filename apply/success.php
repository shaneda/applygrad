<?php
include "../inc/db_connect.php";
include "../inc/tpl.pageHeaderApply.php";
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
 include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
 include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
 include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
 include  '../classes/class.Payment.php';
 include '../classes/class.PaymentManager.php';
 include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
include '../classes/class.RegistrationFeePaymentManager.php';
include '../classes/class.RegistrationFeePayment.php';


function getLastPaymentRecordId ($applicationId)
{
    $id = 0;
    $sql = "SELECT payment_id FROM payment WHERE application_id = " . $applicationId . ' AND payment_status = "pending"';
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $query;
        die($message);
    }
    $id = mysql_result($result, 0);
    return $id;
}

function updatePaymentTablePaid($rowId)
{
    $sql = "UPDATE payment SET payment_status = 'paid', last_mod_time = NOW() WHERE payment_id = " . $rowId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}

function updateApplicationPaid($appId)
{
    $sql = "UPDATE application SET paid = 1, paymentdate = NOW() WHERE id = " . $appId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}
    

function paidPaymentTransaction ($appId)
{
   $paymentManager = new PaymentManager($appId); 
   $rowId = getLastPaymentRecordId($appId);
   $payment = array(
            'payment_id' => $rowId, 
            'payment_type' => 2, 
            'payment_amount' => NULL, 
            'payment_status' => 'paid',
            'last_mod_user_id' => 0
            );
   if ($rowId != 0) {
     $status = $paymentManager->savePayment($payment); 
   }                        
}

function getLastRegistrationPaymentRecordId ($applicationId)
{
    $id = 0;
    $sql = "SELECT id FROM registration_fee_payment WHERE application_id = " . $applicationId . ' AND payment_status = "pending"';
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $query;
        die($message);
    }
    $id = mysql_result($result, 0);
    return $id;
}

function updateRegistrationPaymentTablePaid($rowId)
{
    $sql = "UPDATE registration_fee_payment SET payment_status = 'paid', last_mod_time = NOW() WHERE id = " . $rowId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}

function updateRegistrationPaid($appId)
{
    $sql = "UPDATE registration_fee_status SET paid = 1 WHERE application_id = " . $appId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}

    

function paidRegistrationTransaction ($appId)
{
   $rowId = getLastRegistrationPaymentRecordId($appId);
   if ($rowId != 0) {
      updateRegistrationPaymentTablePaid($rowId);
      updateRegistrationPaid($appId);
       
   }
    
}

function addSuccessTransaction ($app_id, $tx, $name, $email, $merchant, $status)
{
    $sql = 'INSERT INTO cashnet_payment (app_id, transaction_id, applicant_name, applicant_email, merchant, status) VALUES ('
    . $app_id . ', ' . $tx . ', "' . $name . '", "' . $email . '", ' . $merchant . ', "' . $status . '")';
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }    
}

    echo '<span class="title">Transaction Successful</span>'."<BR /><br />";
    $paymentCategory = $_POST['FLEX_FIELD5'];
    $paymentAppId = $_POST['FLEX_FIELD3'];
    
    addSuccessTransaction($paymentAppId, $_POST['tx'], $_POST['FLEX_FIELD1'], $_POST['FLEX_FIELD2'], $_POST['merchant'], $_POST['respmessage']);
    if ($paymentCategory == 'Registration Fee') {
        paidRegistrationTransaction($paymentAppId);
        echo "<p>Your payment has been successful.  Please use the link below to return to your reply form. </p>";
        if ($_SERVER['SERVER_NAME'] == "web28.srv.cs.cmu.edu") {
                    echo '<br /><a href="https://web28.srv.cs.cmu.edu:446/replyform/admitted_applicant.php" > Back to Reply Form </a>';
                } else {
                     echo '<br /><a href="https://' .$_SERVER['SERVER_NAME'] . '/admitted_applicant.php" > Back to Reply Form </a>';
                }
        
    } else {
                paidPaymentTransaction($paymentAppId);
                echo "<p>Your payment has been successful.  Please use the link below to return to your application. </p>";
                if ($_SERVER['SERVER_NAME'] == "web28.srv.cs.cmu.edu") {
                    echo '<br /><a href="https://web28.srv.cs.cmu.edu:446/apply/home.php" > Back to Application </a>';
                } else {
                     echo '<br /><a href="https://' .$_SERVER['SERVER_NAME'] . '/apply/home.php" > Back to Application </a>';
                }
    }

?>
