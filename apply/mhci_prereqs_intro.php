
<html><!-- InstanceBegin template="/Templates/templateApp.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?> 
<? include_once '../inc/session.php'; ?> 
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/header_prefix.php'; 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}
?>
<head>

<!-- InstanceBeginEditable name="doctitle" --><title><?=$HEADER_PREFIX[$domainid]?></title><!-- InstanceEndEditable -->
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<?
$id = -1;
$err = "";
$resName = "";
$resSize = 0;
$resModDate = "";

$stateName = "";
$stateSize = "";
$stateModDate = "";
$resumeFileId = -1;
$statementFileId = -1;
  
 //RETRIEVE USER INFORMATION
$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=2";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$resName  = "resume.".$row['extension'];
	$resSize  = $row['size'];
	$resumeFileId = $row['id'];
	$resModDate = $row['moddate'];
} 

$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=4";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
	$stateName  = "statement.".$row['extension'];
	$stateSize  = $row['size'];
	$statementFileId = $row['id'];
	$stateModDate = $row['moddate'];
} 

if($resModDate == "" || $stateModDate == "")
{
	//updateReqComplete("resume.php", 0,1);
}
  
?>





<!-- InstanceEndEditable -->

</head>
<link rel="stylesheet" href="../app2.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
        <body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
		<!-- InstanceBeginEditable name="EditRegion5" -->
		<form action="" method="post" name="form1" id="form1" enctype="multipart/form-data" ><!-- InstanceEndEditable -->
		<div id="banner"></div>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            <!-- InstanceBeginEditable name="LeftMenuRegion" -->
			  <? 
			if($_SESSION['usertypeid'] != 6)
			{
				include '../inc/sideNav.php'; 
			}
			?>
		    <!-- InstanceEndEditable -->
			<!-- InstanceBeginEditable name="EditNav" -->
            <tr>
            <td>
                <table>
                <colgroup>
                <col width=80% style=text-align:left;>
                <col width=20% style=:right;>
                </colgroup>
                <tr>
            <td class=tblItem>
                
            </td>
            
            <td style="text-align:right; width:130px">    
         
                <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                ){ ?>
                    <a href="logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                    <!-- DAS Removed - <? echo $sesEmail;?>   -->
                    <? }else
                        {
                            if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                            && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                            {
                            //session_unset();
                            //destroySession();
                            //header("Location: index.php");
                        ?><a href="index.php" class="subtitle">Session expired - Please Login Again</a><?
                }
            } ?>
            </td>
            <tr>
            </table>
            </td>
            </tr>
            <!-- InstanceEndEditable -->
			<div style="margin:20px;width:660px""><!-- InstanceBeginEditable name="EditRegion3" --><span class="title">MHCI Prerequisites</span><!-- InstanceEndEditable -->
			<br>
            <br>
            <div class="tblItem" id="contentDiv">
            <!-- InstanceBeginEditable name="EditRegion4" --><span class="tblItem">
			<?
$domainid = 37;
$qs = "?id=1&t=2";

if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}
$sql = "select content from content where name='DASIntro' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}


// Function, variables, calls for displaying summary info.
function getStatusMessage($studentId, $summaryDataStatusArray, $prereqType) {

    $statusMessage = "Not yet begun, please read definition below";    
    if ( isset($summaryDataStatusArray[$studentId][$prereqType]) ) {

        $reviewerStatus = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_status'];
        $reviewerExplanation = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_explanation'];   
        $statusMessage = $reviewerStatus . " " . $reviewerExplanation;        
            
    }
    
    return $statusMessage;
}

include "../inc/mhci_prereqsSummaryData.class.php";

$studentId = $_SESSION['userid'];
$summaryData = new MHCI_PrereqsSummaryData();
$statusArray = $summaryData->getStatus($studentId);

?>


<br>
<span class="errorSubTitle"><?=$err;?></span>

<hr size="1" noshade color="#990000">

            
            The current status of your prerequisites is:
            <br><br>
            <table>
                <tr>
                    <td class="tblItem" style="text-align:left; width:370px">
                        <b>Design</b>: <?php echo getStatusMessage($studentId, $statusArray, "design"); ?>
                    </td>
                    <td style="text-align:right; width:130px">
                        <input class="tblItem" name="btnUpload" value="Discuss Design" type="button" onClick="parent.location='mhci_prereqs_design.php<?=$qs;?>'"> 
                    </td>
                </tr>
            <tr>
                <td class="tblItem" style="text-align:left; width:370px">
                    <b>Programming</b>: <?php echo getStatusMessage($studentId, $statusArray, "programming"); ?>
                </td>
                <td style="text-align:right; width:130px">
                    <input align="right" class="tblItem" name="btnUpload" value="Discuss Programming" type="button" onClick="parent.location='mhci_prereqs_programming.php<?=$qs;?>'">
                </td>
            </tr>
            <tr>
                <td class="tblItem" style="text-align:left; width:370px">
                    <b>Statistics</b>: <?php echo getStatusMessage($studentId, $statusArray, "statistics"); ?>
                </td>
                <td style="text-align:right; width:130px">
                    <input align="right" class="tblItem" name="btnUpload" value="Discuss Statistics" type="button" onClick="parent.location='mhci_prereqs_statistics.php<?=$qs;?>'">
                </td>
            </tr>
            </table>
            <hr size="1" noshade color="#990000">
            
			<? 
			$qs = "?id=1&t=2";
			?>

            
            
            </span>
 <?           $sql = "select content from content where name='DASIntro2' and domain_id=".$domainid;
$result = mysql_query($sql)    or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}
?>

                
            <hr size="1" noshade color="#990000">
            
            </span><!-- InstanceEndEditable --></div>
            <!-- InstanceBeginEditable name="EditNav2" -->
			
            <!-- InstanceEndEditable -->
            <br>
            <br>
			<span class="tblItem">
            <?=date("Y")?> Carnegie Mellon University 
			<? if(strstr($_SERVER['SCRIPT_NAME'], 'contact.php') === false){ ?>
			&middot; <a href="contact.php" target="_blank">Report a Technical Problem </a>
			<? } ?>
			</span>
			</div>
			</td>
          </tr>
        </table>
      
        </form>
        </body>
<!-- InstanceEnd --></html>
