<?php     
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/section_required.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}

/*
* Check for special cases; 
*/
$isHistoryDomain = isHistoryDomain($domainid);
$isModernLanguagesDomain = isModernLanguagesDomain($domainid);
$isModernLanguagesPhdDomain = isModernLanguagesPhdDomain($domainid);
$isModernLanguageMaDomain = isModernLanguageMaDomain($domainid);
$isEnglishDomain = isEnglishDomain($domainid);
$isPhilosophyDomain = isPhilosophyDomain($domainid);


/*
* Initialize error variables
*/


if (!isset($_POST['btnSave'])) {
    $err = '';    
}
$errRecordIds['language_assessment'] = array();
$errRecordIds['language_prof_recomender'] = array();

/*
* Handle language assessment add. 
* Insert new record on first page visit.
*/
if(isset($_POST['addLanguageAssessment']) || !proficiencyRequirementSet())
{
    saveData();
    $addLanguageAssessmentQuery = "INSERT INTO language_assessment (application_id) VALUES (" . $_SESSION['appid'] . ")";
    $addLanguageAssessmentResult = mysql_query($addLanguageAssessmentQuery);
}

/*
* Handle language assessment delete 
*/
if(isset($_POST['deleteLanguageAssessment']))
{
    foreach ($_POST['deleteLanguageAssessment'] as $deleteLanguageAssessmentId => $deleteLanguageAssessmentButtonValue)
    {
        $deleteLanguageAssessmentQuery = "DELETE FROM language_assessment 
            WHERE id = " . intval($deleteLanguageAssessmentId) . 
            " AND application_id = " . $_SESSION['appid'];
        mysql_query($deleteLanguageAssessmentQuery);
    }
    saveData();
}

/*
* Handle writing sample delete 
*/
if(isset($_POST['deleteWritingSample']))
{
    foreach ($_POST['deleteWritingSample'] as $deleteWritingSampleId => $deleteWritingSampleButtonValue)
    {
        $deleteWritingSampleQuery = "DELETE FROM datafileinfo 
            WHERE id = " . intval($deleteWritingSampleId) . 
            " AND section = 23";
        mysql_query($deleteWritingSampleQuery);
    }
    saveData();
}

/*
* Handle save 
*/        
if (isset($_POST['btnSave']))
{
    saveData();
}

/*
* Set writing sample data 
*/
$writingSamples = array();
$writingSampleQuery = "SELECT id, moddate, size, extension, userdata 
    FROM datafileinfo 
    WHERE user_id = " . $_SESSION['userid'] . " 
    AND section = 23
    AND userdata LIKE '" . $_SESSION['appid'] . "_%'";
$writingSampleResult = mysql_query($writingSampleQuery);
while($row = mysql_fetch_array($writingSampleResult)) 
{
    $writingSamples[] = $row;
}

/*
* Set language assessment data 
*/
$languageAssessments = array();
$languageAssessmentsQuery = "SELECT * FROM language_assessment WHERE application_id = " . $_SESSION['appid'] . " ORDER BY id";
$languageAssessmentsResult = mysql_query($languageAssessmentsQuery) or die(mysql_error());
while($languageAssessment = mysql_fetch_array($languageAssessmentsResult))
{
    if (in_array($languageAssessment['id'], $errRecordIds['language_assessment']))
    {
        $languageAssessments[] = languageAssessmentWithPostedValues($languageAssessment);    
    }
    else
    {
        $languageAssessments[] = $languageAssessment;
    }
}

/*
* Set speaking sample data 
*/       
$speakingSampleFileId = NULL;
$speakingSampleName = NULL;
$speakingSampleSize = NULL;
$speakingSampleModDate = NULL;
$speakingSampleQuery = "SELECT id, moddate, size, extension 
    FROM datafileinfo 
    WHERE user_id = " . $_SESSION['userid'] . " 
    AND section = 24
    AND userdata LIKE '" . $_SESSION['appid'] . "_%'";
$speakingSampleResult = mysql_query($speakingSampleQuery);
while($row = mysql_fetch_array($speakingSampleResult)) 
{
    $speakingSampleFileId = $row['id'];
    $speakingSampleName  = "speakingsample." . $row['extension'];
    $speakingSampleSize  = $row['size'];
    $speakingSampleModDate = $row['moddate'];
}

/*
* Handle language recommendation data 
*/
if ($isModernLanguagesPhdDomain) 
{
    include('../inc/dietrichLangProfRecData.inc.php');  
}


/*
* Check/update requirements status 
*/
if ($isModernLanguageMaDomain)
{
    if (writingSampleRequirementMet()
        && $speakingSampleModDate != "")
    { 
        updateReqComplete("proficiency.php", 1);
    } 
    else 
    {
        updateReqComplete("proficiency.php", 0, 1, 1);
    }
}
else if ($isModernLanguagesPhdDomain)
{
    if (writingSampleRequirementMet() 
        && languageAssessmentRequirementMet()
        && languageRecommendationRequirementMet())
    { 
        updateReqComplete("proficiency.php", 1);
    } 
    else 
    {
        updateReqComplete("proficiency.php", 0, 1, 1);
    }
} 
else if ($isHistoryDomain)
{
    if (writingSampleRequirementMet() 
        && languageAssessmentRequirementMet())
    { 
        updateReqComplete("proficiency.php", 1);
    } 
    else 
    {
        updateReqComplete("proficiency.php", 0, 1, 1);
    }
} 
else
{
    if (writingSampleRequirementMet())
    { 
        updateReqComplete("proficiency.php", 1);
    } 
    else 
    {
        updateReqComplete("proficiency.php", 0, 1, 1);
    }
} 

/*
* Set previous and next pages data
*/
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
{
    $prevPage = $appPageList[$curPageIndex - 1]; 
}
else
{
    $prevPage = "";
}
if (sizeof ($appPageList) > $curPageIndex + 1)
{
    $nextPage =  $appPageList[$curPageIndex + 1]; 
} 
else
{
    $nextPage = "";
}

/*
* Start page display 
*/

// Include the shared page header elements.
if ($isHistoryDomain || $isModernLanguagesPhdDomain)
{
    $pageTitle = 'Writing Samples and Language Proficiency';   
}
elseif ($isModernLanguageMaDomain) 
{
    $pageTitle = 'Writing and Spoken Language Samples';
}
elseif ($isEnglishDomain || $isPhilosophyDomain)
{
    $pageTitle = 'Writing Samples';   
}
else
{
    $pageTitle = 'Language Proficiency';    
}
include '../inc/tpl.pageHeaderApply.php';

// Add the page content
$contentDomainid = 1;
if(isset($_SESSION['domainid']))
{
    $contentDomainid = $_SESSION['domainid'];
}
$contentQuery = "select content from content where name='Writing Sample and Language Proficiency' and domain_id=".$contentDomainid;
$contentResult = mysql_query($contentQuery);
while($row = mysql_fetch_array($contentResult)) 
{
    echo html_entity_decode($row["content"]);
}

?>

<br>                            
<span class="errorSubtitle"><?=$err?></span><br>
<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
</div> = required<br>
<br>
<?
if (!$isEnglishDomain && !$isPhilosophyDomain)
{ 
    if ($isHistoryDomain || $isModernLanguagesPhdDomain)
    {
        $saveButtonText = 'Save Writing Samples and Language Proficiency';    
    }
    else if ($isModernLanguageMaDomain)
    {
        $saveButtonText = 'Save Writing and Spoken Language Samples';    
    }
    else
    {
        $saveButtonText = 'Save Language Proficiency';
    }
    showEditText($saveButtonText, "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
    <input name="txtPost" type="hidden" id="txtPost" value="1">
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<?php
}
if($isPhilosophyDomain || $isEnglishDomain || $isHistoryDomain || $isModernLanguagesPhdDomain || $isModernLanguageMaDomain) 
{ 
    $newWritingSampleId = getNewWritingSampleId();
    $newWritingSampleQs = '?id=' . $newWritingSampleId . '&t=23';
?>             
<span class="subtitle">Writing Samples</span>
<br>
<br>
<?php 
if ($isModernLanguageMaDomain) 
{  
?>
Upload two academic writing samples (2-3 pages in length) one in English and one in the language in which you intend to teach (if not English)
<?php  
}
else 
{  ?>
Please upload your writing samples here.  You can upload multiple samples.
<?php
}
?>
<br> 
<br>
<input class="tblItem" name="btnUpload" value="Upload Writing Sample" type="button" onClick="parent.location='fileUpload.php<?php echo $newWritingSampleQs; ?>'">    
<?php
$writingSampleNameCount = 1;
foreach($writingSamples as $writingSample)
{
    $writingSampleFileId = $writingSample['id'];
    $writingSampleName  = "writingsample" . $writingSampleNameCount  . "." . $writingSample['extension'];
    $writingSampleSize  = $writingSample['size'];
    $writingSampleModDate = $writingSample['moddate'];
    $writingSampleId = getWritingSampleId($writingSample['userdata']);
    $writingSampleQs = '?id=' . $writingSampleId . '&t=23';
?>

    <hr>
    <input class="tblItem" name="btnUpload" value="Change" type="button" onClick="parent.location='fileUpload.php<?php echo $writingSampleQs; ?>'">
<?php
    if (count($writingSamples) > 1)
    {
        showEditText("Delete", "button", "deleteWritingSample[" . $writingSampleFileId . "]", $_SESSION['allow_edit']);
    }

    $fileDate = formatUSdate($writingSampleModDate);
    $filePath = getFilePath(23, $writingSampleId, $_SESSION['userid'], $_SESSION['usermasterid'], 
        $_SESSION['appid'], $writingSampleFileId);
    showFileInfo($writingSampleName, $writingSampleSize, $fileDate, $filePath);
        
    $writingSampleNameCount++;
}
?>
<br>
<br>
<hr size="1" noshade color="#990000">
    
<?php
}

if ($isHistoryDomain || $isModernLanguagesPhdDomain)
{
?>
    <span class="subtitle">Language Proficiency Self Evaluations</span>
    <br>
    <br>
    Please assess your proficiency in various languages. 
    <br> 
    <br>
    <?php
    showEditText("Add Language", "button", "addLanguageAssessment", $_SESSION['allow_edit']);    
    
    foreach ($languageAssessments as $languageAssessment)
    {
        echo '<hr size="1">';
        include('../inc/dietrichLanguageAssessment.inc.php');  
    }
    ?>
    <br>
    <hr size="1" noshade color="#990000">
<?php    
}

if ($isModernLanguagesPhdDomain) 
{
    // $speakingSampleQs = '?id=1&t=24';  
?>
    <span class="subtitle">Language Proficiency Recommendation</span>
    <br>
    <br>
    Please enter the information for someone who can provide an evaluation for the language you are interested in studying. 
    <br> 
    <br>
    <?php
        include('../inc/dietrichLangProfRec.inc.php');
}  
    
if ($isModernLanguageMaDomain)
{
    $speakingSampleQs = '?id=1&t=24';
?>
    <br>
    <span class="subtitle">Language of Interest Speaking Sample</span>
    <br>
    <br>
    Upload a three minute <b>unscripted</b> audio recording in the language in which you intend to teach describing
    both your language experience and goals.  Recordings should be in MP3 or WAV format, labeled with your name.
    <br> 
    <br>
    <input class="tblItem" name="btnUpload" value="Upload Speaking Sample" type="button" onClick="parent.location='fileUpload.php<?php echo $speakingSampleQs; ?>'"> 
    <?php
    if($speakingSampleModDate != "")
    {
        showFileInfo($speakingSampleName, $speakingSampleSize, formatUSdate($speakingSampleModDate), 
            getFilePath(24, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }
    ?>   
    <br>
    <hr size="1" noshade color="#990000">
<?php
} 
if (!$isEnglishDomain && !$isPhilosophyDomain)
{ 
    if ($isHistoryDomain || $isModernLanguagesPhdDomain)
    {
        $saveButtonText = 'Save Writing Samples and Language Proficiency';    
    }
    else if ($isModernLanguageMaDomain)
    {
        $saveButtonText = 'Save Writing and Spoken Language Samples';    
    }
    else
    {
        $saveButtonText = 'Save Language Proficiency';
    }
    showEditText($saveButtonText, "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
    <input name="txtPost" type="hidden" id="txtPost" value="1">
    <br>

<?php
} 
?>

<?php
// Include the shared page footer elements. 
 include '../inc/tpl.pageFooterApply.php';

/*
* FUNCTIONS 
*/
function saveData()
{
    saveLanguageAssessments();
}

function saveLanguageAssessments()
{
    global $isModernLanguagesPhdDomain;
    global $err;  
    global $errRecordIds;
    
    $ratingValidationOptions = array(
        'options' => array(
            'min_range' => 1,
            'max_range' => 4
        )
    ); 
    
    $levelValidationOptions = array(
        'options' => array(
            'min_range' => 1,
            'max_range' => 3
        )
    );
    
    $boolValidationOptions = array(
        'options' => array(
            'min_range' => 0,
            'max_range' => 1
        )
    );
    
    if (isset($_POST['language_assessment']))
    {
        foreach($_POST['language_assessment'] as $languageAssessmentId => $languageAssessment)
        {
            // Don't validate a record that was just deleted
            if (isset($_POST['deleteLanguageAssessment'][$languageAssessmentId]))
            {
                continue;
            }
            
            $language = isset($languageAssessment['language']) ? 
                filter_var($languageAssessment['language'], FILTER_SANITIZE_STRING) : NULL;
            $listening = isset($languageAssessment['listening']) ?
                filter_var($languageAssessment['listening'], FILTER_VALIDATE_INT, $ratingValidationOptions) : NULL; 
            $speaking = isset($languageAssessment['speaking']) ?
                filter_var($languageAssessment['speaking'], FILTER_VALIDATE_INT, $ratingValidationOptions) : NULL;
            $reading = isset($languageAssessment['reading']) ?
                filter_var($languageAssessment['reading'], FILTER_VALIDATE_INT, $ratingValidationOptions) : NULL;
            $writing = isset($languageAssessment['writing']) ?
                filter_var($languageAssessment['writing'], FILTER_VALIDATE_INT, $ratingValidationOptions) : NULL;
            $nativeSpeaker = isset($languageAssessment['native_speaker']) ?
                filter_var($languageAssessment['native_speaker'], FILTER_VALIDATE_INT, $boolValidationOptions) : NULL;
            $yearsStudy = isset($languageAssessment['years_study']) ?
                filter_var($languageAssessment['years_study'], FILTER_VALIDATE_INT) : NULL;
            $studyLevel = isset($languageAssessment['study_level']) ?
                filter_var($languageAssessment['study_level'], FILTER_VALIDATE_INT, $levelValidationOptions) : NULL;
            $competencyEvidence = isset($languageAssessment['competency_evidence']) ?
                filter_var($languageAssessment['competency_evidence'], FILTER_SANITIZE_STRING) : NULL;

            if (!$language)
            {
                $err .= 'Language proficiency self evaluation: language is required.<br>';
                $errRecordIds['language_assessment'][] = $languageAssessmentId;    
            }
            
            if (!$speaking)
            {
                $err .= 'Language proficiency self evaluation: speaking rating is required.<br>';
                $errRecordIds['language_assessment'][] = $languageAssessmentId;    
            }
            
            if (!$reading)
            {
                $err .= 'Language proficiency self evaluation: reading rating is required.<br>';
                $errRecordIds['language_assessment'][] = $languageAssessmentId;    
            }
            
            if (!$writing)
            {
                $err .= 'Language proficiency self evaluation: writing rating is required.<br>';
                $errRecordIds['language_assessment'][] = $languageAssessmentId;    
            }
            
            if ($isModernLanguagesPhdDomain)
            {
                if (!$listening)
                {
                    $err .= 'Language proficiency self evaluation: listening rating is required.<br>';
                    $errRecordIds['language_assessment'][] = $languageAssessmentId;    
                } 
                
                if ($nativeSpeaker === FALSE || $nativeSpeaker === NULL)
                {
                    $err .= 'Language proficiency self evaluation: native speaker is required.<br>';
                    $errRecordIds['language_assessment'][] = $languageAssessmentId;    
                }
                
                if (!$yearsStudy)
                {
                    $err .= 'Language proficiency self evaluation: years of study must be a number.<br>';
                    $errRecordIds['language_assessment'][] = $languageAssessmentId;    
                }
                
                if (!$studyLevel)
                {
                    $err .= 'Language proficiency self evaluation: level of study is required.<br>';
                    $errRecordIds['language_assessment'][] = $languageAssessmentId;    
                }   
            }  
            
            if (!$err)
            {
                // Update record
                $updateQuery = "UPDATE language_assessment SET ";
                $updateQuery .= " language = '" . mysql_real_escape_string($language) . "',";
                $updateQuery .= " listening = " . intval($listening) . ",";
                $updateQuery .= " speaking = " . intval($speaking) . ",";
                $updateQuery .= " reading = " . intval($reading) . ",";
                $updateQuery .= " writing = " . intval($writing) . ",";
                $updateQuery .= " native_speaker = " . intval($nativeSpeaker) . ",";
                $updateQuery .= " years_study = " . intval($yearsStudy) . ",";
                $updateQuery .= " study_level = " . intval($studyLevel) . ",";
                $updateQuery .= " competency_evidence = '" . mysql_real_escape_string($competencyEvidence) . "'"; 
                $updateQuery .= " WHERE id = " . intval($languageAssessmentId);
                $updateQuery .= " AND application_id = " . $_SESSION['appid'];
                
                mysql_query($updateQuery); 
            }
        }  
    }
}

function languageAssessmentWithPostedValues($languageAssessment)
{
    if (isset($_POST['language_assessment'][$languageAssessment['id']]))
    {
        $postedFields = array_keys($_POST['language_assessment'][$languageAssessment['id']]);
        foreach ($postedFields as $field)
        {
            $languageAssessment[$field] = 
                $_POST['language_assessment'][$languageAssessment['id']][$field];      
        }      
    }
    
    return $languageAssessment;       
}

function getNewWritingSampleId()
{
    global $writingSamples;
    
    $uploadNumber = 0;
    
    foreach($writingSamples as $writingSample)
    {
        // Split writing sample userdata into application id, upload number
        $userdataArray = explode('_', $writingSample['userdata']);
        
        // Use the max upload number as the upload
        if ($userdataArray[1] > $uploadNumber)
        {
            $uploadNumber = $userdataArray[1];    
        } 
    }
    
    // Increment upload number by 1
    return $uploadNumber + 1;
}

function getWritingSampleId($userdata)
{
    $userdataArray = explode('_', $userdata);
    return $userdataArray[1];
}

function writingSampleRequirementMet()
{
    global $isPhilosophyDomain;
    global $isEnglishDomain; 
    global $isHistoryDomain;
    global $isModernLanguageMaDomain;
    global $isModernLanguagesPhdDomain;
    global $writingSamples;
    
    if ($isPhilosophyDomain || $isEnglishDomain || $isHistoryDomain || $isModernLanguagesPhdDomain || $isModernLanguageMaDomain)
    {
        if (!isset($writingSamples) || count($writingSamples) == 0)
        {
            return FALSE;    
        }    
    }
    
    return TRUE;
}

function languageAssessmentRequirementMet()
{
    global $languageAssessments;
    global $err;
    
    if ($err || !isset($languageAssessments) || count($languageAssessments) == 0)
    {
        return FALSE;    
    }
    
    foreach ($languageAssessments as $languageAssessment)
    {
        if (!isset($languageAssessment['language']))
        {
            return FALSE;
        }
    }   
    
    return TRUE;
}

function languageRecommendationRequirementMet()
{
    global $isModernLanguagesDomain;
    global $myLangProfRecommender;
    global $err;
    
    if ($isModernLanguagesDomain)
    {
        if ($err || !(count($myLangProfRecommender) >=  1))
        {
            return FALSE;
        }
        
        if ($err || (count($myLangProfRecommender) >=  1 
            && $myLangProfRecommender[8] == 0))
        {
            // No email sent.
            $err .= 'Language proficiency recommendation request must be sent.';
            return FALSE;
        }
    }
    
    return TRUE;
}

function proficiencyRequirementSet()
{
    // The record will not be inserted into the DB until the first time
    // updateReqComplete() is called for this page (ca. line 153)
    
    $query = "SELECT id  FROM lu_application_appreqs 
        WHERE req_id = 15 
        AND application_id = " . intval($_SESSION['appid']);
    $result = mysql_query($query);
    if (mysql_num_rows($result) > 0)
    {
        return TRUE;
    }
    
    return FALSE;
}
?>