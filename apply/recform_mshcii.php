<?php
/*
* Use minimal session handling logic instead of session.php.
*/
session_start();
if ( !( isset($_SESSION['usermasterid']) && $_SESSION['usermasterid'] > 1 )
    && !( isset($_SESSION['userid']) && $_SESSION['userid'] > 1) ) 
{   
    unset($_SESSION['userid']);
    unset($_SESSION['usermasterid']);
    header('Location: rec.php');
    exit;
}
$_SESSION['SECTION']= "1"; 

include_once '../inc/config.php'; 
//include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php';

$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}
if (!isset ($_SESSION['usertypeid'])) {
    $_SESSION['usertypeid'] = 6;
}
if (!isset ($_SESSION['datafileroot'])) {
    $_SESSION['datafileroot']= $datafileroot;
}

$err= "";
$formNum = 2;
$id = -1 ;
$responses = array();

$sender = NULL;
if(isset($_GET['sender']))
{
    $sender = $_GET['sender'];
    $studentIDArray = explode('_', $_GET['sender']);
    if (isset($studentIDArray[1])) {
        $id = intval($studentIDArray[1]);    
    }
}
if($id  < 1)
{
	$err .= "Applicant cannot be identified.<br>"; 
}

if(isset($_POST['btnSubmit']))
{
    $vars = $_POST;
	$itemId = -1;
	$response = "";
	$responses = array();
	
	$tmpItem = -1;
	$i = 0;
	
	$sql = "delete from recommendforms where recommend_id=". intval($id) ." and form_id=". intval($formNum);
	mysql_query($sql) 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
			
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'ques_') !== false ) 
		{
			$arr = split("_", $key);
			$itemId = intval($arr[1]);
			
			if(count($arr) == 3)//QUESTION IS A MATRIX
			{
				$i = $arr[2];
			}

            $response = htmlspecialchars($value);
            
			if($itemId != $tmpItem || count($arr) == 3)
			{
				$arr1 = array();
				array_push($arr1, $itemId);
				array_push($arr1, $i);
				array_push($arr1, $response);
				array_push($responses, $arr1);
				$response = "";
				$i = 0;
			}

			$tmpItem = $itemId;
		}
	}//END FOR
	for($i = 0; $i < count($responses); $i++)
	{
		$sql = "INSERT INTO recommendforms(recommend_id, form_id, question_id, question_key, response) 
            values(" . intval($id) ."," . intval($formNum) . "," . intval($responses[$i][0]) . ","
            . intval($responses[$i][1]) . ",'" . mysql_real_escape_string($responses[$i][2]) . "')";
		mysql_query($sql) 
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	}

    $sender = filter_input(INPUT_GET, 'sender', FILTER_SANITIZE_STRING);
    echo "<meta http-equiv='refresh' content='0;url=../apply/recommenderUpload.php?id=$sender'>\n";	
	
}//END IF SUBMIT

//GET DATA
$sql = "select question_id, question_key, response from recommendforms 
    where recommend_id =". intval($id) . " and form_id=". intval($formNum) ." order by question_id, question_key";
$result = mysql_query($sql)	
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr,$row['question_id']);
	array_push($arr,$row['question_key']);
	array_push($arr,stripslashes($row['response']));
	array_push($responses,$arr);	
}

function isSelected($qNum, $qKey, $fieldVal, $idx)
{
	global $responses;
	$ret = "";
	if(count($responses) > $idx)
	{
		if($fieldVal == $responses[$idx][2])
		{
			$ret = "checked";
		}
	}else
	{
		//DEFAULT TO LOWEST VALUE
		if($fieldVal == 7)
		{
			$ret = "checked";
		}
	}
	
	return $ret;
}

function getValue($qNum, $qKey, $fieldVal, $idx)
{
	global $responses;
	$ret = "";
	if(count($responses) > $idx)
	{
		$ret= $responses[$idx][2];
	}
	
	return $ret;
}

?>
<html>
<head>
<title><?=$HEADER_PREFIX[$domainid]?></title>
<link href="../css/SCSStyles_MS-HCII.css" rel="stylesheet" type="text/css">
</head>
<body marginwidth="0" leftmargin="0" marginheight="0" topmargin="0" bgcolor="white">
<div id="banner"></div>
<div class="tblItem" id="contentDiv">

            <span class="tblItem">
            <a href="recommender.php"></a><em>
            <input name="btnRecs" type="button" id="btnBack" value="Return to Recommender Home" onClick="document.location='recommender.php'">
            </em><br><br>
            
            <?php
$sql = "select content from content where name='Recommender Survey' and domain_id=37";
                $result = mysql_query($sql)    
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
                while($row = mysql_fetch_array( $result )) 
                {
                    echo '<div style="margin:20px;width:660px">';
                    echo html_entity_decode($row["content"]);
                    echo "</div>";
                }
?>
<form action="" method="post" name="form1" id="form1">
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            
          <tr>
            <td valign="top">			
			
			<div style="margin:20px;width:660px"><span class="title">Part 1: Group work/Interpersonal Evaluation</span>
            <div class="tblItem" id="contentDiv">
            <span class="tblItem">
            
            <br>
            <br> 
<?php
if ($err) {
    echo '<span class="errorSubtitle">' . $err . '</span>';    
} else {  
?>           
On a scale from 1 to 5, please fill out the following evaluation in regards to the applicant's group work capabilities.
<br>
1 =  poor, would not recommend       to    5 = excellent, would highly recommend
  <br><br>
            <table  border="0" cellpadding="2" cellspacing="2" class="tblItem">
              <tr>
                <td>&nbsp;</td>
                <td align="center"><strong> 5 </strong></td>
                <td align="center"><strong> 4 </strong></td>
                <td align="center"><strong> 3 </strong></td>
                <td align="center"><strong> 2 </strong></td>
                <td align="center"><strong> 1 </strong></td>
                <td align="center"><strong> Unknown </strong></td>
              </tr>
              <tr class="tblItemAlt">
              <tr>
                <td>Follows established group processes, procedures, and guidelines </td>
                <? 
				$j = 0;//row index
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_0" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
              <td>Does his or her fair share of the work </td>
                 <? 
                for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_1" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
                <? 
                }//end for 
                $j++;
                ?>
              </tr>
              <tr>
                <td>Does high quality work</td>
                 <? 
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_2" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Meets agreed upon task schedules</td>
                 <? 
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_3" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr>
                <td>Demonstrates initiative within his/her assigned area of responsibility </td>
                 <? 
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_4" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Interacts with others in an effective, tactful and pleasant manner </td>
                <? 
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_5" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr>
                <td>Expresses ideas in a clear, concise manner </td>
                 <? 
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_6" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
              <tr class="tblItemAlt">
                <td>Regularly asks for, listens to and understands others' viewpoints </td>
                <? 
				for($i = 0; $i < 6; $i++){ ?>
                <td><input name="ques_1_7" type="radio" <?=isSelected(1,0,$i+1,$j) ?> value="<?=$i+1?>"></td>
				<? 
				}//end for 
				$j++;
				?>
              </tr>
            </table>
            <br>
            <span class="subtitle">
            <?php 
            if ($sender) {
                echo '<input name="sender" type="hidden" id="sender" value="' . $sender . '">'; 
            }
            showEditText("Save Info", "button", "btnSubmit", TRUE); 
            ?>
            </span>            
            
            <?php
            }
            ?>  
            </span>
            </div>
			</div>
			</td>
          </tr>
        </table>
        </form>
   </body>
</html>