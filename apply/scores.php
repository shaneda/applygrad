<? 
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../apply/header_prefix.php'; 
include_once '../apply/section_required.php';
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
        {
            $prevPage = "";
        }
if (sizeof ($appPageList) > $curPageIndex + 1)
    {
        $nextPage =  $appPageList[$curPageIndex + 1]; 
    } 
    else
        {
            $nextPage = "";
        }

$sectioncomplete = false;
$grecomplete = false;
$grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflpscores = array();

// Dales - IELTS - International English Language Testing System
$ieltsscores = array();

$greScoreRange1 = array();
$greScoreRange2 = array();
$grePercRange1 = array();
$greScoreRange2011 = array();

$greSubs = array(
array("Computer Science","Computer Science"),
array("Mathematics","Mathematics"),
array("Physics","Physics"),
array("Chemistry","Chemistry"),
array("Biochemistry Cell and Molecular Biology","Biochemistry Cell and Molecular Biology"),
array("Biology","Biology"),
array("Psychology","Psychology"),
array("Other","Other, Please Specify")
);
$greSubRange = array();
$greSubPercRange = array();

$toeflRange1 = array(); // 0-30
$toeflRange2 = array(); // 0-300
$toeflRange3 = array(); // 0, 1-6 in .5 incre
$toeflRange4= array();  // 31-68
$toeflRange5 = array(); // 310-677
$toeflRange6 = array(); // 0-120
$toeflRange7 = array(); // 31-67

$ieltsRange = array();  // 1 - 9 by .5

for($i = 200; $i < 801; $i += 10)
{
    $arr = array($i, $i);
    array_push($greScoreRange1, $arr);
}

for($i = 0; $i < 6.5; $i += 0.5)
{
    $arr = array($i, $i);
    array_push($greScoreRange2, $arr);
}

for($i = 1; $i < 100; $i += 1)
{
    $arr = array($i, $i);
    array_push($grePercRange1, $arr);
}

$greScoreRange2011[] = array('NS', 'No Score (NS)');
for($i = 130; $i <= 170; $i++) {
    $greScoreRange2011[] = array($i, $i);    
}
for($i = 200; $i <= 800; $i += 10)
{
    $greScoreRange2011[] = array($i, $i);
}

for($i = 200; $i < 1001; $i += 10)
{
    $arr = array($i, $i);
    array_push($greSubRange, $arr);
}
for($i = 1; $i < 100; $i += 1)
{
    $arr = array($i, $i);
    array_push($greSubPercRange, $arr);
}
// Range 0-30 
for($i = 0; $i < 31; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange1, $arr);
}
// range 0 to 300
for($i = 0; $i < 301; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange2, $arr);
}
/// range 0 , 1 to 6.5 in .5 incremements
$arr = array(0, 0);
array_push($toeflRange3, $arr);
for($i = 1; $i < 6.5; $i += 0.5)
{
    $arr = array($i, $i);
    array_push($toeflRange3, $arr);
}

// Range 31 to 68
for($i = 31; $i < 69; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange4, $arr);
}
// Range 310 to 677
for($i = 310; $i < 678; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange5, $arr);
}
// Range 0-120
for($i = 0; $i < 121; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange6, $arr);
}
// Range 31 to 67
for($i = 31; $i < 68; $i += 1)
{
    $arr = array($i, $i);
    array_push($toeflRange7, $arr);
}
for($i = 1; $i <= 9; $i += 0.5)
{
    $arr = array($i, $i);
    array_push($ieltsRange, $arr);
}


// PLB - GMAT for SEM - 2/12/10
$gmatscores = array();
$gmatScoreRange1 = array();
$gmatScoreRange2 = array();
$gmatScoreRange3 = array();
$gmatPercRange1 = array();
for($i = 0; $i < 61; $i++) {
    $arr = array($i, $i);
    array_push($gmatScoreRange1, $arr);
}
for($i = 200; $i < 801; $i += 10) {
    $arr = array($i, $i);
    array_push($gmatScoreRange2, $arr);
}
for($i = 0; $i < 6.5; $i += 0.5) {
    $arr = array($i, $i);
    array_push($gmatScoreRange3, $arr);
}
for($i = 1; $i < 100; $i += 1) {
    $arr = array($i, $i);
    array_push($gmatPercRange1, $arr);
}

$err = "";
//FIRST CHECK TO SEE IF RECORDS EXIST. IF NOT, ADD THEM
addGRE(false);
addGRESub(false);
addToefl(false, "IBT");    
addToefl(false, "PBT");
//ielts
addIELTS(false);
// GMAT
addGMAT(false);
 
if( isset( $_POST ) )
{
    $vars = $_POST;
    $itemId = -1;
    foreach($vars as $key => $value)
    {
        if( strstr($key, 'btnDel') !== false ) 
        {
            $arr = split("_", $key);
            $item = $arr[1];
            $itemId = $arr[2];
            $sql = "";
            switch( $item)
            {
                case "1":
                    $sql = "update grescore set
                    testdate=NULL,    verbalscore=NULL,verbalpercentile=NULL,quantitativescore=NULL,
                    quantitativepercentile=NULL,analyticalwritingscore=NULL,analyticalwritingpercentile=NULL,
                    analyticalscore=NULL,analyticalpercentile=NULL,
                    scorereceived=NULL,datafile_id=NULL where id=" . intval($itemId);
                    break;
                case "2":
                    $sql = "update gresubjectscore set testdate=NULL,name=NULL,score=NULL,
                    percentile=NULL, scorereceived=NULL, datafile_id=NULL WHERE id=" . intval($itemId);
                    break;
                // ielts
                case "3":
                    $sql = "update ieltsscore set
                    testdate=NULL,listeningscore=NULL,readingscore=NULL,
                    writingscore=NULL,speakingscore=NULL,overallscore=NULL,
                    scorereceived=NULL,datafile_id=NULL where id=" . intval($itemId);
                    break;
                // GMAT
                case "7":
                    $sql = "update gmatscore set
                    testdate=NULL, verbalscore=NULL, verbalpercentile=NULL, quantitativescore=NULL,
                    quantitativepercentile=NULL, analyticalwritingscore=NULL, analyticalwritingpercentile=NULL,
                    totalscore=NULL, totalpercentile=NULL,
                    scorereceived=NULL, datafile_id=NULL where id = " . intval($itemId);
                    break;
                default:
                    $sql = "update toefl set testdate=NULL,section1=NULL,section2=NULL,section3=NULL,essay=NULL,
                    total=NULL,scorereceived=NULL,datafile_id=NULL WHERE id=" . intval($itemId);
                    break;
            }

            mysql_query($sql) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        }
    }
}

function addGRE($allowDup)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from grescore where application_id = " . intval($_SESSION['appid']);
        $result = mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    if($doAdd == true)
    {
        $sql = "insert into grescore(application_id)
            values(" . intval($_SESSION['appid']) . ")";
        mysql_query($sql)    
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
}

function addGRESub($allowDup)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from gresubjectscore where application_id = " . intval($_SESSION['appid']);
        $result = mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    if($doAdd == true)
    {
        $sql = "insert into gresubjectscore(application_id)
            values(" . intval($_SESSION['appid']) . ")";
        mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
}

function addToefl($allowDup, $type)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from toefl where application_id = " . intval($_SESSION['appid']) . " 
            and type='" . mysql_real_escape_string($type) . "'";
        $result = mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }

    }
    if($doAdd == true)
    {
        $sql = "insert into toefl(application_id,type)
            values(" . intval($_SESSION['appid']) . ",'" . mysql_real_escape_string($type) . "')";
        mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
}
function addIELTS($allowDup)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from ieltsscore where application_id = " . intval($_SESSION['appid']);
        $result = mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    if($doAdd == true)
    {
        $sql = "insert into ieltsscore(application_id)
            values(" . intval($_SESSION['appid']) . ")";
        mysql_query($sql)    
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
}

function addGMAT($allowDup)
{
    $doAdd = true;
    $allowDup = false;
    if($allowDup == false)
    {
        $sql = "select id from gmatscore where application_id = " . intval($_SESSION['appid']);
        $result = mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    if($doAdd == true)
    {
        $sql = "insert into gmatscore(application_id)
            values(" . intval($_SESSION['appid']) . ")";
        mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
}


/*
* Design: check for ESL requirement
*/
$isDesignDomain = isDesignDomain($domainid);
$isDesignPhdDomain = isDesignPhdDomain($domainid);
$nativeTongue = '';
$eslRequired = FALSE;
if ($isDesignDomain || $isDesignPhdDomain)
{
    $nativeTongueQuery = "SELECT users_info.native_tongue
        FROM application
        INNER JOIN users_info ON application.user_id = users_info.user_id
        WHERE application.id = " . intval($_SESSION['appid']) . " LIMIT 1";
    $nativeTongueResult = mysql_query($nativeTongueQuery);
    while ($row = mysql_fetch_array($nativeTongueResult))
    {
        $nativeTongue = $row['native_tongue'];        
    }
    
    if (trim(strtolower($nativeTongue)) != 'english')
    {
        $eslRequired = TRUE;    
    }
}

//INSERT RECORD INTO CORRESPONDING TEST TABLE FOR USER 
if(isset($_POST['btnAdd']))
{
    $sql = ""; 
    switch($_POST['lbType'])
    {
        case "1":
            addGRE(false);
            break;
        case "2":
            addGRESub(false);
            break;
        case "3":
            addToefl(false, "IBT");                    
            break;
        case "5":
            addToefl(false, "PBT");    
            break;
        case "6":
            addIELTS(false);
            break;
        case "7":
            addGMAT(false);
            break;
    }//END SWITCH
}//END POST BTNADD

if(isset($_POST['btnSave']) || isset($_POST['chkCmuMscsWaiver']))
{   
    $greId = array();
    $greSubId = array();
    $toefliId = array();
    $toeflpId = array();
    $ieltsId = array();
    $gmatId = array();
    $tmpType="";
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if($arr[0] == "txtGreGenId" || $arr[0] == "txtGreSubId" 
            || $arr[0] == "txtToeflIntId" || $arr[0] == "txtToeflCompId" 
            || $arr[0] == "txtToeflPaperId" || $arr[0] == "txtIeltsId" || $arr[0] == "txtGmatId")
        {
            $tmpid = $arr[1];
            if($itemId != $tmpid || $tmpType != $arr[0])
            {
                $itemId = $tmpid;
                $tmpType=$arr[0];
                switch($arr[0])
                {
                    case "txtGreGenId":
                        array_push($greId, intval($itemId));
                        break;
                    case "txtGreSubId":
                        array_push($greSubId, intval($itemId));
                        break;
                    case "txtToeflIntId":
                        array_push($toefliId, intval($itemId));
                        break;
                    case "txtToeflPaperId":
                        array_push($toeflpId, intval($itemId));
                        break;
                    case "txtIeltsId":
                        array_push($ieltsId, intval($itemId));
                        break;
                    case "txtGmatId":
                        array_push($gmatId, intval($itemId));
                        break;
                }//END SWITCH
            }//END IF
        }//END IF
    }//END FOR
    
    //ITERATE THROUGH IDS AND DO UPDATES
    for($i = 0; $i < count($greId); $i++)
    {
        //SET VARS
        $greGenDate = htmlspecialchars( $_POST["txtGreGenDate_".$greId[$i]] );
        $greGenVerScore = "NULL";
        $greGenVerPer = "NULL";
        $greGenQuanScore = "NULL";
        $greGenQuanPer = "NULL";
        $greGenWrScore = "NULL";
        $greGenWrPer = "NULL";
        $greGenAnScore = "NULL";
        $greGenAnPer = "NULL";
        $greCmuMscsWaiver = 0;
        
        if($_POST["txtGreGenVerScore_".$greId[$i]]  != "")
        {
            if ($_POST["txtGreGenVerScore_".$greId[$i]] == 'NS') {
                $greGenVerScore = "'NS'";    
            } else {
                $greGenVerScore = strToNum($_POST["txtGreGenVerScore_".$greId[$i]]);    
            }
        }
        if($_POST["txtGreGenVerPer_".$greId[$i]]  != "")
        {
            $greGenVerPer = strToNum($_POST["txtGreGenVerPer_".$greId[$i]]);
        }
        if($_POST["txtGreGenQuanScore_".$greId[$i]]  != "")
        {
            if($_POST["txtGreGenQuanScore_".$greId[$i]]  == 'NS') {
                $greGenQuanScore = "'NS'";    
            } else {
                $greGenQuanScore = strToNum($_POST["txtGreGenQuanScore_".$greId[$i]]);
            }
        }
        if($_POST["txtGreGenQuanPer_".$greId[$i]]  != "")
        {
            $greGenQuanPer = strToNum($_POST["txtGreGenQuanPer_".$greId[$i]]);
        }
        if($_POST["txtGreGenWrScore_".$greId[$i]]  != "")
        {
            $greGenWrScore = floatval($_POST["txtGreGenWrScore_".$greId[$i]]);
        }
        if( $_POST["txtGreGenWrPer_".$greId[$i]] != "")
        {
            $greGenWrPer = strToNum($_POST["txtGreGenWrPer_".$greId[$i]]);
        }
        if($_POST["txtGreGenAnScore_".$greId[$i]]  != "")
        {
            $greGenAnScore = strToNum($_POST["txtGreGenAnScore_".$greId[$i]]);
        }
        if($_POST["txtGreGenAnPer_".$greId[$i]]  != "")
        {
            $greGenAnPer = strToNum($_POST["txtGreGenAnPer_".$greId[$i]]);
        }
        
        if(isset($_POST["chkCmuMscsWaiver_".$greId[$i]]) && $_POST["chkCmuMscsWaiver_".$greId[$i]] != "")
        {
            if ($_POST["chkCmuMscsWaiver_".$greId[$i]] == 'on') 
            {
                $greCmuMscsWaiver = 1;   
            }
        }
        
        $localErr = "";
        //DO VALIDATION
        if (check_Date2($greGenDate) == false && $greCmuMscsWaiver == 0 && !$isDesignPhdDomain)
        {
            $localErr .= "GRE General Score date is invalid<br>";
        }
        else
        {
            $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-5);
            $arr = split("/", $greGenDate);
            $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
            if($thisdate < $pastdate )
            {
                //$localErr .= "GRE General Score date is too old. GRE scores are only valid for 5 years<br>";
            }
        }
        //DO UPDATE
        if($localErr == "" || (isset($_POST['chkCmuMscsWaiver']) && $_POST['chkCmuMscsWaiver'] == 1))
        {
            $mscsWaiverUpdateQuery = "INSERT INTO grescore_mscs_waiver (application_id, waiver_agree)
                VALUES (" . intval($_SESSION['appid']) . ", " . intval($greCmuMscsWaiver) . ")
                ON DUPLICATE KEY UPDATE waiver_agree = " . intval($greCmuMscsWaiver);
            $mscsWaiverUpdateResult = mysql_query($mscsWaiverUpdateQuery)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $mscsWaiverUpdateQuery); 
            
            $sql = "update grescore set
            testdate='" . mysql_real_escape_string( formatMySQLdate2($greGenDate,'/','-') ) . "',
            verbalscore=" . $greGenVerScore . ",
            verbalpercentile=" . $greGenVerPer . ",
            quantitativescore=" . $greGenQuanScore . ",
            quantitativepercentile=" . $greGenQuanPer . ",
            analyticalwritingscore=" . $greGenWrScore .",
            analyticalwritingpercentile=" . $greGenWrPer . ",
            analyticalscore=" . $greGenAnScore . ",
            analyticalpercentile= " . $greGenAnPer . "
            where id = " . intval($greId[$i]) . " 
            and application_id= " . intval($_SESSION['appid']);
            $result = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            
            if ($localErr != '' && $greCmuMscsWaiver == 0 && !$isDesignPhdDomain) 
            {
                $sectioncomplete = FALSE;
                $grecomplete = FALSE;    
            } 
            else
            {
                $sectioncomplete = true;
                $grecomplete = true;                  
            } 
        }
        else
        {
            if ($greCmuMscsWaiver == 1) {
                $sectioncomplete = true;
                $grecomplete = true;  
            }
            
            $err .= $localErr;
        }
    }//END GRE UPDATE
    
    for($i = 0; $i < count($greSubId); $i++)
    {
        //SET VARS
        $greSubDate = htmlspecialchars($_POST["txtGreSubDate_".$greSubId[$i]]);
        $greSubName = "NULL";
        if($_POST["lbGreSubName_".$greSubId[$i]] == "Other" || $_POST["lbGreSubName_".$greSubId[$i]] == "")
        {
            $greSubName = htmlspecialchars($_POST["txtGreSubName_".$greSubId[$i]]);
        }else
        {
            $greSubName = htmlspecialchars($_POST["lbGreSubName_".$greSubId[$i]]);
        }
        $greSubScore = "NULL";
        $greSubPer = "NULL";
        
        if($_POST["txtGreSubScore_".$greSubId[$i]] != "")
        {
            $greSubScore = strToNum($_POST["txtGreSubScore_".$greSubId[$i]]);
        }
        if($_POST["txtGreSubPer_".$greSubId[$i]] != "")
        {
            $greSubPer = strToNum($_POST["txtGreSubPer_".$greSubId[$i]]);
        }
        
        $localErr = "";
        //DO VALIDATION
        $doUpdate = false;
        if($greSubDate != "" && $greSubDate != "00/0000")
        {
            if(check_Date2($greSubDate) == false)
            {
                $localErr .= "GRE Subject Score date is invalid<br>";
            }else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $greSubDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "GRE Subject Score date is too old. Scores are only valid for 2 years<br>";
                }
                if($greSubName == "")
                {
                    $localErr .= "GRE Subject Score name is invalid<br>";
                    
                }
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update gresubjectscore set
            testdate='" . mysql_real_escape_string( formatMySQLdate2($greSubDate,'/','-') ) . "',
            name='" . mysql_real_escape_string($greSubName) . "',
            score=" . $greSubScore . ",
            percentile=" . $greSubPer . "
            where id = ". intval($greSubId[$i]) . " 
            and application_id= " . intval($_SESSION['appid']);
            $result = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            $sectioncomplete = true;
        }else
        {
            $err .= $localErr;
        }
    }//END SUBJECT UPDATE
    
    for($i = 0; $i < count($toefliId); $i++)
    {
        //SET VARS
        $toeflIntDate = htmlspecialchars($_POST["txtToeflIntDate_".$toefliId[$i]]);
        $toeflIntSect1 = "NULL";
        $toeflIntSect2 = "NULL";
        $toeflIntSect3 = "NULL";
        $toeflIntEssay = "NULL";
        $toeflIntTotal = "NULL";
        
        if($_POST["txtToeflIntSect1_".$toefliId[$i]] != "")
        {
            $toeflIntSect1 = strToNum($_POST["txtToeflIntSect1_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntSect2_".$toefliId[$i]] != "")
        {
            $toeflIntSect2 = strToNum($_POST["txtToeflIntSect2_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntSect3_".$toefliId[$i]] != "")
        {
            $toeflIntSect3 = strToNum($_POST["txtToeflIntSect3_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntEssay_".$toefliId[$i]] != "")
        {
            $toeflIntEssay = floatval($_POST["txtToeflIntEssay_".$toefliId[$i]]);
        }
        if($_POST["txtToeflIntTotal_".$toefliId[$i]] != "")
        {
            $toeflIntTotal = strToNum($_POST["txtToeflIntTotal_".$toefliId[$i]]);
        }

        $toeflIntErr = '';
        $localErr = "";
        //DO VALIDATION
        if($toeflIntDate != "" && $toeflIntDate != "00/0000")
        {
            if(check_Date2($toeflIntDate) == false)
            {
                $toeflIntErr = "TOEFL IBT date is invalid<br>";
                $localErr .= $toeflIntErr;
            }
            else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $toeflIntDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "TOEFL IBT date is too old. Scores are only valid for 2 years<br>";
                }
            }
        }
        //DO UPDATE
        if($localErr == "" )
        {
            $sql = "update toefl set";
            if( $toeflIntDate != "" && $toeflIntDate != "00/0000" ) {
                $sql .= " testdate='" . mysql_real_escape_string( formatMySQLdate2($toeflIntDate,'/','-') ) . "',";    
            }
            $sql .= " section1=" . $toeflIntSect1 . ",
            section2=" . $toeflIntSect2 . ",
            section3=" . $toeflIntSect3 . ",
            essay=" . $toeflIntEssay . ",
            total=" . $toeflIntTotal . "
            where id = " . intval($toefliId[$i]) . " 
            and application_id= " . intval($_SESSION['appid']);
            $result = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            $sectioncomplete = true;
            
        }else
        {
            $err .= $localErr;
        }
    }//END TOEFL IBT UPDATE
        
    for($i = 0; $i < count($toeflpId); $i++)
    {
        //SET VARS
        $toeflPaperDate = htmlspecialchars($_POST["txtToeflPaperDate_".$toeflpId[$i]]);
        $toeflPaperSect1 = "NULL";
        $toeflPaperSect2 = "NULL";
        $toeflPaperSect3 = "NULL";
        $toeflPaperEssay = "NULL";
        $toeflPaperTotal = "NULL";
        
        if($_POST["txtToeflPaperSect1_".$toeflpId[$i]] != "")
        {
            $toeflPaperSect1 = strToNum($_POST["txtToeflPaperSect1_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperSect2_".$toeflpId[$i]] != "")
        {
            $toeflPaperSect2 = strToNum($_POST["txtToeflPaperSect2_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperSect3_".$toeflpId[$i]] != "")
        {
            $toeflPaperSect3 = strToNum($_POST["txtToeflPaperSect3_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperEssay_".$toeflpId[$i]] != "")
        {
            $toeflPaperEssay = floatval($_POST["txtToeflPaperEssay_".$toeflpId[$i]]);
        }
        if($_POST["txtToeflPaperTotal_".$toeflpId[$i]] != "")
        {
            $toeflPaperTotal = strToNum($_POST["txtToeflPaperTotal_".$toeflpId[$i]]);
        }

        //DO VALIDATION
        $localErr = "";
        if($toeflPaperDate != "" && $toeflPaperDate != "00/0000")
        {
            if(check_Date2($toeflPaperDate) == false)
            {
                $localErr .= "TOEFL PBT date is invalid<br>";
            }
            else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-2);
                $arr = split("/", $toeflPaperDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "TOEFL PBT date is too old. Scores are only valid for 2 years<br>";
                }
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update toefl set";
            if( $toeflPaperDate != "" && $toeflPaperDate != "00/0000" ) {
                $sql .= " testdate='" . mysql_real_escape_string( formatMySQLdate2($toeflPaperDate,'/','-') ) . "',";    
            }
            $sql .= " section1=".$toeflPaperSect1.",
            section2=".$toeflPaperSect2.",
            section3=".$toeflPaperSect3.",
            essay=".$toeflPaperEssay.",
            total=".$toeflPaperTotal."
            where id = " . intval($toeflpId[$i]) . " 
            and application_id= " . intval($_SESSION['appid']);
            $result = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            $sectioncomplete = true;
            
        }else
        {
            $err .= $localErr;
        
        }
    }//END TOEFL PBT UPDATE
    
    // ielts
    for($i = 0; $i < count($ieltsId); $i++)
    {
        //SET VARS
        $ieltsDate = htmlspecialchars( $_POST["txtIeltsDate_".$ieltsId[$i]] );
        $ieltsListenScore = "NULL";
        $ieltsReadScore = "NULL";
        $ieltsWriteScore = "NULL";
        $ieltsSpeakScore = "NULL";
        $ieltsOverallScore = "NULL";
                
        if($_POST["txtIeltsListenScore_".$ieltsId[$i]]  != "")
        {
            $ieltsListenScore = number_format (floatval ($_POST["txtIeltsListenScore_".$ieltsId[$i]]) ,1);
        }
        if($_POST["txtIeltsReadScore_".$ieltsId[$i]]  != "")
        {
            $ieltsReadScore = number_format (floatval ($_POST["txtIeltsReadScore_".$ieltsId[$i]]), 1);
        }
        if($_POST["txtIeltsWriteScore_".$ieltsId[$i]]  != "")
        {
            $ieltsWriteScore = number_format (floatval ($_POST["txtIeltsWriteScore_".$ieltsId[$i]]) , 1);
        }
        if($_POST["txtIeltsSpeakScore_".$ieltsId[$i]]  != "")
        {
            $ieltsSpeakScore = number_format (floatval ($_POST["txtIeltsSpeakScore_".$ieltsId[$i]]) , 1);
        }
        if($_POST["txtIeltsOverallScore_".$ieltsId[$i]]  != "")
        {
            $ieltsOverallScore = number_format (floatval ($_POST["txtIeltsOverallScore_".$ieltsId[$i]]) , 1);
        }
        
        $ieltsErr = '';
        $localErr = "";
        
        if($ieltsDate != "" && $ieltsDate != "00/0000")
        {
        //DO VALIDATION
            if(check_Date2($ieltsDate) == false)
            {
                $ieltsErr = "IELTS Score date is invalid<br>";
                $localErr .= $ieltsErr;
            }
            else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-5);
                $arr = split("/", $greGenDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                //$localErr .= "GRE General Score date is too old. GRE scores are only valid for 5 years<br>";
                }
                }
        }
        //DO UPDATE  
        if($localErr == "")
        {
            $sql = "update ieltsscore set";
            if( $ieltsDate != "" && $ieltsDate != "00/0000" ) {
                $sql .= " testdate='" . mysql_real_escape_string( formatMySQLdate2($ieltsDate,'/','-') ) . "',";    
            }
            $sql .= " listeningscore=".$ieltsListenScore.",
            readingscore=".$ieltsReadScore.",
            writingscore=".$ieltsWriteScore.",
            speakingscore=".$ieltsSpeakScore.",
            overallscore=".$ieltsOverallScore. "
            where id = ". intval($ieltsId[$i]) . " 
            and application_id= " . intval($_SESSION['appid']);
            $result = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        }
        else
        {
            $err .= $localErr;
        }
    }   // End ielts update
    
    // GMAT
    for($i = 0; $i < count($gmatId); $i++)
    {
        //SET VARS
        $gmatDate = htmlspecialchars( $_POST["txtGmatDate_".$gmatId[$i]] );
        $gmatVerScore = "NULL";
        $gmatVerPer = "NULL";
        $gmatQuanScore = "NULL";
        $gmatQuanPer = "NULL";
        $gmatWrScore = "NULL";
        $gmatWrPer = "NULL";
        $gmatTotalScore = "NULL";
        $gmatTotalPer = "NULL";
        
        if($_POST["txtGmatVerScore_".$gmatId[$i]]  != "")
        {
            $gmatVerScore = strToNum($_POST["txtGmatVerScore_".$gmatId[$i]]);
        }
        if($_POST["txtGmatVerPer_".$gmatId[$i]]  != "")
        {
            $gmatVerPer = strToNum($_POST["txtGmatVerPer_".$gmatId[$i]]);
        }
        if($_POST["txtGmatQuanScore_".$gmatId[$i]]  != "")
        {
            $gmatQuanScore = strToNum($_POST["txtGmatQuanScore_".$gmatId[$i]]);
        }
        if($_POST["txtGmatQuanPer_".$gmatId[$i]]  != "")
        {
            $gmatQuanPer = strToNum($_POST["txtGmatQuanPer_".$gmatId[$i]]);
        }
        if($_POST["txtGmatWrScore_".$gmatId[$i]]  != "")
        {
            $gmatWrScore = floatval($_POST["txtGmatWrScore_".$gmatId[$i]]);
        }
        if( $_POST["txtGmatWrPer_".$gmatId[$i]] != "")
        {
            $gmatWrPer = strToNum($_POST["txtGmatWrPer_".$gmatId[$i]]);
        }
        if($_POST["txtGmatTotalScore_".$gmatId[$i]]  != "")
        {
            $gmatTotalScore = strToNum($_POST["txtGmatTotalScore_".$gmatId[$i]]);
        }
        if($_POST["txtGmatTotalPer_".$gmatId[$i]]  != "")
        {
            $gmatTotalPer = strToNum($_POST["txtGmatTotalPer_".$gmatId[$i]]);
        }
        
        $localErr = "";
        //DO VALIDATION
        if($gmatDate != "" && $gmatDate != "00/0000")
        {
            if(check_Date2($gmatDate) == false)
            { 
                $localErr .= "GMAT Score date is invalid<br>";
            } else
            {
                $pastdate = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-5);
                $arr = split("/", $gmatDate);
                $thisdate = mktime(0, 0, 0, date($arr[0]),   date(1),   date($arr[1]));
                if($thisdate < $pastdate )
                {
                    //$localErr .= "GRE General Score date is too old. GRE scores are only valid for 5 years<br>";
                }
            }
        }
        //DO UPDATE
        if($localErr == "")
        {
            $sql = "update gmatscore set";
            if( $gmatDate != "" && $gmatDate != "00/0000" ) {
                $sql .= " testdate='" . mysql_real_escape_string( formatMySQLdate2($gmatDate,'/','-') ) . "',";    
            }
            $sql .= " verbalscore=".$gmatVerScore.",
            verbalpercentile=".$gmatVerPer.",
            quantitativescore=".$gmatQuanScore.",
            quantitativepercentile=".$gmatQuanPer.",
            analyticalwritingscore=".$gmatWrScore.",
            analyticalwritingpercentile=".$gmatWrPer.",
            totalscore=".$gmatTotalScore.",
            totalpercentile=".$gmatTotalPer."
            where id = ". intval($gmatId[$i]) . " 
            and application_id= " . intval($_SESSION['appid']);
            $result = mysql_query($sql)
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        }
        else
        {
            $err .= $localErr;
        }
    }//END GMAT UPDATE

    if ($isDesignDomain || $isDesignPhdDomain)
    {
        if ($isDesignPhdDomain)
        {
            $greComplete = TRUE;    
        }
        
        if($eslRequired)
        {
            $toeflComplete = $ieltsComplete = $eslComplete = FALSE;
            
            if (!($toeflIntErr || $toeflIntDate == "" || $toeflIntDate == "00/0000"))
            {
                $toeflComplete = TRUE;    
            } 
            
            if (!($ieltsErr || $ieltsDate == "" || $ieltsDate == "00/0000"))
            {
                $ieltsComplete = TRUE;
            }

            if ($toeflComplete || $ieltsComplete)
            {
                $eslComplete = TRUE;    
            }
            else
            {
                if (!$toeflIntErr && !$ieltsErr)
                {
                    $err .= 'You must provide a TOEFL or IELTS test date<br>';    
                }
            }
            
            if ($sectioncomplete == true && $grecomplete == true && $eslComplete)
            {
                updateReqComplete("scores.php", 1);    
            }
            else
            {
                updateReqComplete("scores.php", 0);    
            }
        }
        else
        {
            if($sectioncomplete == true && $grecomplete == true)
            {
                updateReqComplete("scores.php", 1);
            }
            else
            {
                updateReqComplete("scores.php", 0);    
            }    
        }
    }
    else
    {
        if($sectioncomplete == true && $grecomplete == true)
        {
            updateReqComplete("scores.php", 1);
        }
        else
        {
            /* if the GRE scores are required, then don't mark as complete */
            /* if they are not required, then its ok to mark as complete   */
            /* there is really no way to tell if the applicant MUST submit */
            /* TOEFL scores. This has to be documented in the preamble text*/
            /* of this section, (supplied by the user via the admin i/f)   */
            if ($GRE_SCORES_REQUIRED[$domainid] == true ) {
                updateReqComplete("scores.php", 0);
            } else {
                updateReqComplete("scores.php", 1);                
            }
        }
    }
}

//LOOK FOR GRE GENERAL SCORE
$sql = "select grescore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from grescore 
left outer join datafileinfo on datafileinfo.id = grescore.datafile_id
where application_id = " . intval($_SESSION['appid']);
$result = mysql_query($sql)
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['verbalscore']);
    array_push($arr, $row['verbalpercentile']);
    array_push($arr, $row['quantitativescore']);
    array_push($arr, $row['quantitativepercentile']);
    array_push($arr, $row['analyticalwritingscore']);
    array_push($arr, $row['analyticalwritingpercentile']);
    array_push($arr, $row['analyticalscore']);
    array_push($arr, $row['analyticalpercentile']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($grescores,$arr); 
}

//LOOK FOR GRE SUBJECT SCORE
$sql = "select gresubjectscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from gresubjectscore
left outer join datafileinfo on datafileinfo.id = gresubjectscore.datafile_id
where application_id = " . intval($_SESSION['appid']);
$result = mysql_query($sql)
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['name']);
    array_push($arr, $row['score']);
    array_push($arr, $row['percentile']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($gresubjectscores,$arr); 
}

//LOOK FOR TOEFL IBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " . intval($_SESSION['appid']) . " 
and toefl.type = 'IBT'";
$result = mysql_query($sql)
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toefliscores,$arr); 
}

//LOOK FOR TOEFL PBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl  
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " . intval($_SESSION['appid']) . " 
and toefl.type = 'PBT'";
$result = mysql_query($sql)
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['section1']);
    array_push($arr, $row['section2']);
    array_push($arr, $row['section3']);
    array_push($arr, $row['essay']);
    array_push($arr, $row['total']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($arr, $row['datafile_id']);
    array_push($toeflpscores,$arr); 
}

//LOOK FOR IELTS SCORE
$sql = "select ieltsscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from ieltsscore 
left outer join datafileinfo on datafileinfo.id = ieltsscore.datafile_id
where application_id = " . intval($_SESSION['appid']);
$result = mysql_query($sql)
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['listeningscore']);
    array_push($arr, $row['readingscore']);
    array_push($arr, $row['writingscore']);
    array_push($arr, $row['speakingscore']);
    array_push($arr, $row['overallscore']);
    array_push($arr, $row['scorereceived']);
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($ieltsscores,$arr); 
}

//LOOK FOR GMAT SCORE
$sql = "select gmatscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from gmatscore 
left outer join datafileinfo on datafileinfo.id = gmatscore.datafile_id
where application_id = " . intval($_SESSION['appid']);
$result = mysql_query($sql) 
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, formatUSdate2($row['testdate'],'-','/'));
    array_push($arr, $row['verbalscore']);
    array_push($arr, $row['verbalpercentile']);
    array_push($arr, $row['quantitativescore']);
    array_push($arr, $row['quantitativepercentile']);
    array_push($arr, $row['analyticalwritingscore']);
    array_push($arr, $row['analyticalwritingpercentile']);
    array_push($arr, $row['totalscore']);
    array_push($arr, $row['totalpercentile']);
    array_push($arr, $row['scorereceived']); // 10
    array_push($arr, $row['datafile_id']);
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
    array_push($gmatscores,$arr); 
}

// update design phd requirements based on optional gre & esl
function designPhdEslComplete()
{
    global $toefliscores;
    global $ieltsscores;

    foreach ($toefliscores AS $toefliscore)
    {
        if ($toefliscore[1] != '' && $toefliscore[1] != '00/0000')
        {
            return true;    
        }    
    }
    
    foreach ($ieltsscores AS $ieltsscore)
    {
        if ($ieltsscore[1] != '' && $ieltsscore[1] != '00/0000')
        {
            return true;    
        }    
    }
    
    return false;    
}
 
if($isDesignPhdDomain && !isset($_POST['btnSave']))
{
    $designPhdEslComplete = designPhdEslComplete();
    
    if ($eslRequired && !$designPhdEslComplete)
    {
        updateReqComplete("scores.php", 0);   
    }
    else
    {
        updateReqComplete("scores.php", 1);    
    }
}

// Test for RI-MS-RT special cases
$isMsrtDomain = isMsrtDomain($_SESSION['domainid']);
$isScsDomain = isScsDomain($_SESSION['domainid']);

// Include the shared page header elements.
$pageTitle = 'Test Scores';
include '../inc/tpl.pageHeaderApply.php';
?>

<span class="tblItem">
<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
    if($_SESSION['domainid'] > -1)
    {
        $domainid = $_SESSION['domainid'];
    }
}
$sql = "select content from content where name='Test Scores' and domain_id=". intval($domainid);
$result = mysql_query($sql)
    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}
?>
</span>

<?php
if ($isDesignDomain || $isDesignPhdDomain)
{
?>
    <br>
    <div style="border: 1px dotted black; padding: 10px;">
    <b>School of Design ESL Requirement</b>
    <br><br>
    
    Your native language/mother tongue:
    <a href="bio.php#native_language"> 
    <?php
        if ($nativeTongue)
        { 
            echo $nativeTongue;
        }
        else
        {
            echo '[N/A]';
        } 
    ?>
    </a>
    <br><br>
    
    <?php
    if ($eslRequired)
    {
        echo '<span class="tblItemRequired">TOEFL or IELTS test date is required</span>';
    }
    else
    {
        echo '<span>You are not required to submit a TOEFL or IELTS score.</span>';
    }
    ?>
    </div>
<?php   
}
?>
 <br><span class="errorSubtitle"><?=$err;?></span>
<br>

<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
</div><span class="tblItem">= required</span><br>
<br>
<? showEditText("Save Scores", "button", "btnSave", $_SESSION['allow_edit']); ?>
<hr size="1" noshade color="#990000">
<? for($i = 0; $i < count($grescores); $i++){ ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><span class="subtitle">GRE General <? 
        if (!$isMsrtDomain) {
            if($grescores[$i][10] == 1)
            {
                echo "<em>(Official Score Report Received)</em>";
            }else
            {
                if($grescores[$i][1] != "00/0000" && $grescores[$i][1] != ""  && !$isScsDomain)
                {
                    echo "<em>(Official Score Report Not Received)</em>";
                }
            }
        }
         ?></span></td>
        <td align="right"><span class="tblItem">
          <? showEditText("Clear Scores", "button", "btnDel_1_".$grescores[$i][0], $_SESSION['allow_edit']); ?>
          <input name="txtGreGenId_<?=$grescores[$i][0]?>" type="hidden" id="txtGreGenId_<?=$grescores[$i][0]?>" value="<?=$grescores[$i][0];?>">
</span></td>
      </tr>
    </table>
            
    <table border="0" cellspacing="2" cellpadding="2" class="tblItem">
    <?php
    $isMsCsApplication = isMsCsApplication($_SESSION['appid']);
    $cmuUndergrad = cmuUndergrad($_SESSION['appid']);
    if ($isMsCsApplication && $cmuUndergrad) {
    ?>
        <tr>
            <td colspan="3">
            <input type="hidden" name="chkCmuMscsWaiver" id="chkCmuMscsWaiver">
    <?php
    /*
    * 
        CREATE TABLE IF NOT EXISTS `grescore_mscs_waiver` (
          `application_id` int(11) NOT NULL,
          `waiver_agree` tinyint(4) NOT NULL default '0',
          `waiver_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
          PRIMARY KEY  (`application_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
    * 
    */
        ?>
        <p style="font-weight: bold;">
        You may choose to have your GRE score requirement waived.
        </p>
        <p>
        By checking “Agree�? below, you hereby authorize your current and former instructors at 
        Carnegie Mellon University to discuss your academic performance with the admissions committee 
        (or those acting on behalf of the committee) for the Master of Science in Computer Science Program.  
        You acknowledge that such discussions may include the disclosure of information or documents relating 
        to your status and performance as a student at Carnegie Mellon, including but not limited to information 
        which may be deemed to be personally identifiable information from your student education records pursuant 
        to the Family Educational Rights and Privacy Act of 1974 or which may otherwise be protected under other 
        applicable privacy laws.
        </p>
        <p>
        By checking “AGREE�?, you consent to disclosure of information as described above. 
        </p>
        
        <?php
        $mscsWaiver = cmuMscsGreWaiver($_SESSION['appid']);

        ob_start();
        showEditText(intval($mscsWaiver), "checkbox", "chkCmuMscsWaiver_" . $grescores[$i][0], 
            $_SESSION['allow_edit'], false);
        $inputElementString = ob_get_contents();
        ob_end_clean();
        
        $inputElementReplace = ' onClick="form1.elements[\'chkCmuMscsWaiver\'].value=\'1\';form1.submit();">';
        $inputElementString = str_replace('>', $inputElementReplace, $inputElementString);

        echo $inputElementString . 
        ' AGREE: I acknowledge that I have read, understand, and agree to this authorization.<br><br>'
    ?>
            </td>
        </tr>
    <?php    
    }
    ?>
      <tr>
        <td><strong>Test date (MM/YYYY): </strong></td>
        <td><span class="tblItem">
              <? 
              if ($isDesignPhdDomain)
              {
                $greRequired = FALSE;    
              }
              else
              {
                  $greRequired = $GRE_SCORES_REQUIRED[$domainid];
              }
              showEditText($grescores[$i][1], "textbox", "txtGreGenDate_".$grescores[$i][0], 
                              $_SESSION['allow_edit'], $greRequired); 
              ?>
        </span></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><strong>Score:</strong></td>
        <td><strong>Percentile:</strong></td>
      </tr>
      <tr>
        <td><strong>Verbal:</strong></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][2], "listbox", "txtGreGenVerScore_".$grescores[$i][0], $_SESSION['allow_edit'], false, $greScoreRange2011); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][3], "listbox", "txtGreGenVerPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%                
          </span></td>
      </tr>
      <tr>
        <td><strong>Quantitative:</strong></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][4], "listbox", "txtGreGenQuanScore_".$grescores[$i][0], $_SESSION['allow_edit'], false,$greScoreRange2011); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][5], "listbox", "txtGreGenQuanPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%
        </span></td>
      </tr>
      <tr>
        <td><strong>Analytical Writing: </strong></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][6], "listbox", "txtGreGenWrScore_".$grescores[$i][0], $_SESSION['allow_edit'], false,$greScoreRange2); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][7], "listbox", "txtGreGenWrPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%
        </span></td>
      </tr>
      <?php
      /*
      if (!isIniDomain($_SESSION['domainid'])) 
      {   
      ?>
      <tr>
        <td><strong>Analytical:</strong></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][8], "listbox", "txtGreGenAnScore_".$grescores[$i][0], $_SESSION['allow_edit'], false,$greScoreRange1); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($grescores[$i][9], "listbox", "txtGreGenAnPer_".$grescores[$i][0], $_SESSION['allow_edit'], false, $grePercRange1); ?>%
        </span></td>
      </tr>
      <?php
      } */
      ?>
    </table>
            
    <? 
    if($grescores[$i][1] != "00/0000")
    {
    $qs = "?id=".$grescores[$i][0]."&t=6"; ?>
    <br>
    <?php
    if (!$isScsDomain) {
        ?>
    <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php 
        } else {
        ?>
        <input  class="tblItem" name="btnUpload" value="Upload Scores" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">    
        <?php
        }
    if (!$isScsDomain) {
        echo "(if available)";
    }
    ?>
    <?
    if($grescores[$i][12] != "")
    {
        showFileInfo("grescore.".$grescores[$i][14], $grescores[$i][13], formatUSdate2($grescores[$i][12]), getFilePath(6, $grescores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }
    
    
    if($_SESSION['allow_edit'] == true){ ?>
        <br>
    <? }//end if allowedit 
    }//end if date
    ?>
            
    <hr size="1" noshade color="#990000">
    <? } ?>
    <?  if (($domainid == 44) || ($domainid == 49) || !displayGreSubject()) {
        // do not show gre subject for stats, dietrich
    } else {
        for($i = 0; $i < count($gresubjectscores); $i++){ ?>
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><span class="subtitle">GRE Subject <? 
        if (!$isMsrtDomain) {  
            if($gresubjectscores[$i][5] == 1)
            {
                echo "<em>(Official Score Report Received)</em>";
            }else
            {
                if($gresubjectscores[$i][1] != "00/0000" && $gresubjectscores[$i][1] != "" && !$isScsDomain)
                {
                    echo "<em>(Official Score Report Not Received)</em>";
                }
            }
        }
         ?></span></td>
        <td align="right"><span class="tblItem">
          <? showEditText("Clear Scores", "button", "btnDel_2_".$gresubjectscores[$i][0], $_SESSION['allow_edit']); ?>
          <input name="txtGreSubId_<?=$gresubjectscores[$i][0]?>" type="hidden" id="txtGreSubId_<?=$gresubjectscores[$i][0]?>" value="<?=$gresubjectscores[$i][0]?>">
                    </span></td>
      </tr>
    </table>
    <span class="tblItem">            </span>
    <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
      <tr>
        <td colspan="3"><strong>Test date (MM/YYYY): </strong>  
        <? showEditText($gresubjectscores[$i][1], "textbox", "txtGreSubDate_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
      </tr>
      <tr>
        <td><strong>Subject:</strong></td>
        <td><strong>Score:</strong></td>
        <td><strong>Percentile:</strong></td>
      </tr>
      <tr>
        <td><? showEditText($gresubjectscores[$i][2], "listbox", "lbGreSubName_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false,$greSubs); ?>
        <br></td>
        <td valign="top"><? showEditText($gresubjectscores[$i][3], "listbox", "txtGreSubScore_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false,$greSubRange); ?></td>
        <td valign="top">      <? showEditText($gresubjectscores[$i][4], "listbox", "txtGreSubPer_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false, $greSubPercRange); ?>
        % </td>
      </tr>
      <tr>
        <td colspan="3"><? showEditText($gresubjectscores[$i][2], "textbox", "txtGreSubName_".$gresubjectscores[$i][0], $_SESSION['allow_edit'], false, null, true, 30); ?> If other please specify </td>
      </tr>
    </table>
    <? 
    if($gresubjectscores[$i][1] != "00/0000" && $gresubjectscores[$i][1] != "")
    {
    $qs = "?id=".$gresubjectscores[$i][0]."&t=8"; ?>
    <br>
    <?php
        if ($isScsDomain) {
    ?>
    <input  class="tblItem" name="btnUpload" value="Upload Scores" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php
        } else {
            ?>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php        
        }
    if (!$isScsDomain) {
        echo '(if available)';
    }
    ?>
    
    <?

    if($gresubjectscores[$i][7] != "")
    {
        showFileInfo("gresubjectscore.".$gresubjectscores[$i][9], $gresubjectscores[$i][8], formatUSdate2($gresubjectscores[$i][7]), getFilePath(8, $gresubjectscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }
    if($_SESSION['allow_edit'] == true){ ?>
        <br>
    <? }//end if allowedit 
    }//end if date ?>
    <hr size="1" noshade color="#990000">
        <? }} ?>
                       
<?php
/*
* Show GMAT form for MSIT-SEM (program id 100004), 
* MBA-MSE (program id 30),
* and BIC (program id 100012). 
*/
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_ApplicationProgram.php';
$DB_ApplicationProgram = new DB_ApplicationProgram;

$showGmatForm = FALSE;
$applicationPrograms = $DB_ApplicationProgram->find($_SESSION['appid']);
foreach($applicationPrograms as $applicationProgram) {
    $applicationProgramId = $applicationProgram['program_id'];
    if ($applicationProgramId == 100004 
        || $applicationProgramId == 30
        || $applicationProgramId == 100012) {
        $showGmatForm = TRUE;
        break;    
    }
}

//if ($domainid == 40 || $domainid == 17) {
if ($showGmatForm) {
    for($i = 0; $i < count($gmatscores); $i++){ ?>
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><span class="subtitle">GMAT <? 
        if (!$isMsrtDomain) { 
            if($gmatscores[$i][10] == 1)
            {
                echo "<em>(Official Score Report Received)</em>";
            }else
            {
                if($gmatscores[$i][1] != "00/0000" && $gmatscores[$i][1] != "" && !$isScsDomain)
                {
                    echo "<em>(Official Score Report Not Received)</em>";
                }
            }
        }
         ?></span></td>
        <td align="right"><span class="tblItem">
          <? showEditText("Clear Scores", "button", "btnDel_7_".$gmatscores[$i][0], $_SESSION['allow_edit']); ?>
          <input name="txtGmatId_<?=$gmatscores[$i][0]?>" type="hidden" id="txtGreGenId_<?=$gmatscores[$i][0]?>" value="<?=$gmatscores[$i][0];?>">
</span></td>
      </tr>
    </table>
    
    <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
      <tr>
        <td><strong>Test date (MM/YYYY): </strong></td>
        <td><span class="tblItem">
            <? showEditText($gmatscores[$i][1], "textbox", "txtGmatDate_".$gmatscores[$i][0], $_SESSION['allow_edit'], false); ?>
        </span></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><strong>Score:</strong></td>
        <td><strong>Percentile:</strong></td>
      </tr>
      <tr>
        <td><strong>Verbal:</strong></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][2], "listbox", "txtGmatVerScore_".$gmatscores[$i][0], $_SESSION['allow_edit'], false, $gmatScoreRange1); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][3], "listbox", "txtGmatVerPer_".$gmatscores[$i][0], $_SESSION['allow_edit'], false, $gmatPercRange1); ?>%                
          </span></td>
      </tr>
      <tr>
        <td><strong>Quantitative:</strong></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][4], "listbox", "txtGmatQuanScore_".$gmatscores[$i][0], $_SESSION['allow_edit'], false,$gmatScoreRange1); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][5], "listbox", "txtGmatQuanPer_".$gmatscores[$i][0], $_SESSION['allow_edit'], false, $gmatPercRange1); ?>%
        </span></td>
      </tr>
      <tr>
        <td><strong>Analytical Writing: </strong></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][6], "listbox", "txtGmatWrScore_".$gmatscores[$i][0], $_SESSION['allow_edit'], false,$gmatScoreRange3); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][7], "listbox", "txtGmatWrPer_".$gmatscores[$i][0], $_SESSION['allow_edit'], false, $gmatPercRange1); ?>%
        </span></td>
      </tr>
      <tr>
        <td><strong>Total:</strong></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][8], "listbox", "txtGmatTotalScore_".$gmatscores[$i][0], $_SESSION['allow_edit'], false,$gmatScoreRange2); ?>
        </span></td>
        <td><span class="tblItem">
          <? showEditText($gmatscores[$i][9], "listbox", "txtGmatTotalPer_".$gmatscores[$i][0], $_SESSION['allow_edit'], false, $gmatPercRange1); ?>%
        </span></td>
      </tr>
    </table>
    <?
    if($gmatscores[$i][1] != "00/0000" && $gmatscores[$i][1] != "")
    {
    $qs = "?id=".$gmatscores[$i][0]."&t=20"; ?>
    <br>
    <?php
        if ($isScsDomain) {
    ?>
    <input  class="tblItem" name="btnUpload" value="Upload Scores" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php
        } else {
    ?>
        <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php        
        }
      if (!$isScsDomain) {
         echo "(if available)";
      }
    ?>
    <?
    if($gmatscores[$i][12] != "")
    {
        showFileInfo("gmatscore.".$gmatscores[$i][14], $gmatscores[$i][13], formatUSdate2($gmatscores[$i][12]), getFilePath(20, $gmatscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }
    
    if($_SESSION['allow_edit'] == true){ ?>
        <br>
    <? }//end if allowedit 
    }//end if date
    ?>
    
    <hr size="1" noshade color="#990000">
    <? }
}  // end GMAT for SEM domainId == 40        
?>

<? for($i = 0; $i < count($toefliscores); $i++){ ?>
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><span class="subtitle">TOEFL Internet-Based <? 
        if (!$isMsrtDomain) { 
            if($toefliscores[$i][7] == 1)
            {
                echo "<em>(Official Score Report Received)</em>";
            }else
            {
                if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "" && !$isScsDomain)
                {
                    echo "<em>(Official Score Report Not Received)</em>";
                }
            }
        }
         ?></span></td>
        <td align="right"><span class="tblItem">
          <? showEditText("Clear Scores", "button", "btnDel_6_".$toefliscores[$i][0], $_SESSION['allow_edit']); ?>
          <input name="txtToeflIntId_<?=$toefliscores[$i][0]?>" type="hidden" id="txtToeflIntId_<?=$toefliscores[$i][0]?>" value="<?=$toefliscores[$i][0]?>">
        </span></td>
      </tr>
    </table>
    <span class="tblItem">            </span>
    <table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
      <tr>
        <td><strong>Test date (MM/YYYY): </strong></td>
        <td><? showEditText($toefliscores[$i][1], "textbox", "txtToeflIntDate_".$toefliscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><strong>Score:</strong></td>
      </tr>
      <tr>
        <td><strong>Reading: </strong></td>
        <td><? showEditText($toefliscores[$i][4], "listbox", "txtToeflIntSect3_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
      </tr>
      
      <tr>
        <td><strong>Listening:</strong></td>
        <td><? showEditText($toefliscores[$i][3], "listbox", "txtToeflIntSect2_".$toefliscores[$i][0], $_SESSION['allow_edit'], false,$toeflRange1); ?></td>
      </tr>
      <tr>
        <td><strong>Speaking:</strong></td>
        <td><? showEditText($toefliscores[$i][2], "listbox", "txtToeflIntSect1_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
      </tr>
      <tr>
        <td><strong>Writing:</strong></td>
        <td><? showEditText($toefliscores[$i][5], "listbox", "txtToeflIntEssay_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange1); ?></td>
      </tr>
      <tr>
        <td><strong>Total Score:</strong></td>
        <td><? showEditText($toefliscores[$i][6], "listbox", "txtToeflIntTotal_".$toefliscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange6); ?></td>
      </tr>
    </table>
    <? 
    if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
    {
    $qs = "?id=".$toefliscores[$i][0]."&t=7"; ?>
    <br>
    <?php 
    if ($isScsDomain) {
        ?>
    <input  class="tblItem" name="btnUpload" value="Upload Scores" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php
    } else {
    ?>
            <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
    <?php
    }
    if (!$isScsDomain) {
        echo "(if available)";
    }
    ?>
    
    <?
    if($toefliscores[$i][8] != "")
    {
        showFileInfo("toeflscore.".$toefliscores[$i][10], $toefliscores[$i][9], formatUSdate2($toefliscores[$i][8]), getFilePath(7, $toefliscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $toefliscores[$i][11]));
    }
    if($_SESSION['allow_edit'] == true){ ?>
        <br>
    <? }//end if allowedit 
    }//end if date ?>
    <hr size="1" noshade color="#990000">
    <? 
    }

if (displayToeflPbt())
{
    
for($i = 0; $i < count($toeflpscores); $i++){ 
?>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><span class="subtitle">TOEFL Paper-Based <? 
    if (!$isMsrtDomain) { 
        if($toeflpscores[$i][7] == 1)
        {
            echo "<em>(Official Score Report Received)</em>";
        }else
        {
            if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "" && !$isScsDomain)
            {
                echo "<em>(Official Score Report Not Received)</em>";
            }
        }
    }
     ?></span></td>
    <td align="right"><span class="tblItem">
      <? showEditText("Clear Scores", "button", "btnDel_5_".$toeflpscores[$i][0], $_SESSION['allow_edit']); ?>
      <input name="txtToeflPaperId_<?=$toeflpscores[$i][0]?>" type="hidden" id="txtToeflPaperId_<?=$toeflpscores[$i][0]?>" value="<?=$toeflpscores[$i][0]?>">
</span></td>
  </tr>
</table>
<span class="tblItem">            </span>
<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
  <tr>
    <td><strong>Test date (MM/YYYY): </strong></td>
    <td><? showEditText($toeflpscores[$i][1], "textbox", "txtToeflPaperDate_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Score:</strong></td>
  </tr>
  <tr>
    <td><strong>Section 1 :</strong></td>
    <td><? showEditText($toeflpscores[$i][2], "listbox", "txtToeflPaperSect1_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange4); ?></td>
  </tr>
  <tr>
    <td><strong>Section 2 :</strong></td>
    <td><? showEditText($toeflpscores[$i][3], "listbox", "txtToeflPaperSect2_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange4); ?></td>
  </tr>
  <tr>
    <td><strong>Section 3 : </strong></td>
    <td><? showEditText($toeflpscores[$i][4], "listbox", "txtToeflPaperSect3_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange7); ?></td>
  </tr>
  <tr>
    <td><strong>Total Score :</strong></td>
    <td><? showEditText($toeflpscores[$i][6], "listbox", "txtToeflPaperTotal_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange5); ?></td>
  </tr>
  <tr>
    <td><strong>TWE Score :</strong></td>
    <td><? showEditText($toeflpscores[$i][5], "listbox", "txtToeflPaperEssay_".$toeflpscores[$i][0], $_SESSION['allow_edit'], false, $toeflRange3); ?></td>
  </tr>
</table>
<? 
if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
{
$qs = "?id=".$toeflpscores[$i][0]."&t=10"; ?>
<br>
<input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
(if available)
<?
if($toeflpscores[$i][8] != "")
{
    showFileInfo("toeflscore.".$toeflpscores[$i][10], $toeflpscores[$i][9], formatUSdate2($toeflpscores[$i][8]), getFilePath(10, $toeflpscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $toeflpscores[$i][11]));
}
if($_SESSION['allow_edit'] == true){ ?>
    <br>
<? }//end if allowedit 
}//end if date ?>
<hr size="1" noshade color="#990000">
<? } 
}
?>

<?
if (displayIelts())
{ 
for($i = 0; $i < count($ieltsscores); $i++)
{ 
?>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><span class="subtitle">IELTS <?
    if (!$isMsrtDomain) { 
        if($ieltsscores[$i][7] == 1)
        {
            echo "<em>(Official Score Report Received)</em>";
        }else
        {
            if($ieltsscores[$i][1] != "00/0000" && $ieltsscores[$i][1] != "" && !$isScsDomain)
            {
                echo "<em>(Official Score Report Not Received)</em>";
            }
        }
    }
     ?></span></td>
    <td align="right"><span class="tblItem">
      <? showEditText("Clear Scores", "button", "btnDel_3_".$ieltsscores[$i][0], $_SESSION['allow_edit']); ?>
      <input name="txtIeltsId_<?=$ieltsscores[$i][0]?>" type="hidden" id="txtIeltsId_<?=$ieltsscores[$i][0]?>" value="<?=$ieltsscores[$i][0];?>">
</span></td>
  </tr>
</table>
            
<table  border="0" cellspacing="2" cellpadding="2" class="tblItem">
  <tr>
    <td><strong>Test date (MM/YYYY): </strong></td>
    <td><span class="tblItem">
          <? showEditText($ieltsscores[$i][1], "textbox", "txtIeltsDate_".$ieltsscores[$i][0], 
                          $_SESSION['allow_edit'], false); ?>
    </span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><strong>Score:</strong></td>
  </tr>
  <tr>
    <td><strong>Listening:</strong></td>
    <td><span class="tblItem">
      <? showEditText($ieltsscores[$i][2], "listbox", "txtIeltsListenScore_".$ieltsscores[$i][0], $_SESSION['allow_edit'], false, $ieltsRange); ?>
    </span></td>
  </tr>
  <tr>
    <td><strong>Reading:</strong></td>
    <td><span class="tblItem">
      <? showEditText($ieltsscores[$i][3], "listbox", "txtIeltsReadScore_".$ieltsscores[$i][0], $_SESSION['allow_edit'], false, $ieltsRange); ?>
    </span></td>
  </tr>
  <tr>
    <td><strong>Writing:</strong></td>
    <td><span class="tblItem">
      <? showEditText($ieltsscores[$i][4], "listbox", "txtIeltsWriteScore_".$ieltsscores[$i][0], $_SESSION['allow_edit'], false, $ieltsRange); ?>
    </span></td>
  </tr>
  <tr>
    <td><strong>Speaking:</strong></td>
    <td><span class="tblItem">
      <? showEditText($ieltsscores[$i][5], "listbox", "txtIeltsSpeakScore_".$ieltsscores[$i][0], $_SESSION['allow_edit'], false, $ieltsRange); ?>
    </span></td>
  </tr>
  <tr>
    <td><strong>Overall:</strong></td>
    <td><span class="tblItem">
      <? showEditText($ieltsscores[$i][6], "listbox", "txtIeltsOverallScore_".$ieltsscores[$i][0], $_SESSION['allow_edit'], false, $ieltsRange); ?>
    </span></td>
  </tr>
</table>

<? 
if($ieltsscores[$i][1] != "" && $ieltsscores[$i][1] != "00/0000")
{
$qs = "?id=".$ieltsscores[$i][0]."&t=19"; ?>
<br>
<?php
    if ($isScsDomain) {
?>
<input  class="tblItem" name="btnUpload" value="Upload Scores" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
<?php
    } else {
        ?>
        <input  class="tblItem" name="btnUpload" value="Upload Unofficial Copy" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
<?php
    }
if (!$isScsDomain) {                  
   echo "(if available)";
}           ?>
<?
if($ieltsscores[$i][8] != "")
{
    showFileInfo("ieltsscore.".$ieltsscores[$i][11], $ieltsscores[$i][10], formatUSdate2($ieltsscores[$i][9]), getFilePath(19, $ieltsscores[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
}
                        
if($_SESSION['allow_edit'] == true){ ?>
    <br>
<? }//end if allowedit 
}//end if date
?>
<hr size="1" noshade color="#990000">
<? 
} 
} // end if displayIelts
?>            
<br>
<? showEditText("Save Scores", "button", "btnSave", $_SESSION['allow_edit']); ?>
<br>

<script language="JavaScript" type="text/JavaScript">
<!--
function confirmSubmit()
{
var agree=confirm("Are you sure you wish to delete?");
if (agree)
    return true ;
else
    return false ;
}
//-->
</script>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';

function displayIelts()
{
    global $domainid;
    
    if (isHistoryDomain($domainid) || isSdsDomain($domainid) || isPsychologyDomain($domainid))
    {
        return FALSE;
    }
    
    return TRUE;
}

function displayGreSubject()
{
    global $domainid;
    global $isDesignDomain;
    global $isDesignPhdDomain;
    
    if ((isDietrichDomain($domainid) && !isEnglishDomain($domainid))
        || isIniDomain($domainid) || $isDesignDomain || $isDesignPhdDomain)
    {
        return FALSE;
    }
    
    return TRUE;
}

function displayToeflPbt()
{
    global $isDesignDomain;
    global $isDesignPhdDomain;
    
    if ($isDesignDomain || $isDesignPhdDomain)
    {
        return FALSE;
    }
    
    return TRUE;
}
?>