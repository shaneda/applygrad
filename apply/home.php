<?php
/*
*Standard (old) includes.
*/
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
include_once '../apply/header_prefix.php';
include '../inc/specialCasesApply.inc.php'; 

$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php
include_once '../inc/functions.php';

/*
* New includes for payment manager. 
*/
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
//include '../classes/class.Payment.php';

/*
* New includes for showing status of each requirement.
*/
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminList.php";  
include "../classes/DB_Applyweb/class.VW_AdminListBase.php";
include "../classes/DB_Applyweb/class.VW_AdminListRecommendationCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListGreSubjectCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListToeflCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListIeltsCount.php";
include "../classes/DB_Applyweb/class.VW_AdminListGmatCount.php";

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}

$reqs = array();
$compReqs = array();
$apps = array();
$progs = array();
$appName = "";
$firstname = "";
$appid = -1;
$totalcost = 0.0;
$amountPaid = 0.0;
$balance = 0.0;
$submitted = "";

$submissionDate = "";
$paid = "Unpaid";
$paymentdate = "";
$paymenttype = "";
$reviewed = false;
$notified = "";
$appComp = false;
$_SESSION['paid'] = false;

/*
* Check for special cases; 
*/
$isIsreeDomain = isIsreeDomain($_SESSION['domainid']);
$isMsrtDomain = isMsrtDomain($_SESSION['domainid']);
$isHistoryDomain = isHistoryDomain($_SESSION['domainid']);
$isEnglishDomain = isEnglishDomain($_SESSION['domainid']);
$isPhilosophyDomain = isPhilosophyDomain($_SESSION['domainid']);
$isModLangPhdDomain = isModernLanguagesPhdDomain($_SESSION['domainid']);
$isModLangMaDomain = isModernLanguageMaDomain($_SESSION['domainid']);
$isDesignPhdDomain = isDesignPhdDomain($_SESSION['domainid']);
$isScsDomain = isScsDomain($_SESSION['domainid']);
$isREUSEDomain = isREUSEDomain($_SESSION['domainid']);
$isEngineeringDomain = isEngineeringDomain($_SESSION['domainid']);


/* 
* PLB added unit/period check 8/26/09.
* NOTE: the include sets 
* $unit (a unit object),
* $activePeriod (a period object) and 
* $activePeriodId
*/
include '../inc/applyPeriod.inc.php';
if(isset($_POST['btnNewApp']))
{
    if($appid == -1)
    {
        //INSERT
        $sql = "insert into application (name, user_id, created_date) values ('"
        . mysql_real_escape_string($_SESSION['firstname']) ."s Application', "
        . $_SESSION['userid'] .",'" .date("Y-m-d h:i:s")."')";
        mysql_query( $sql) or die(mysql_error());

        // PLB added period_application insert 8/26/09
        if ($activePeriodId) {
            
            $applicationId = mysql_insert_id();
            if ($applicationId) {
                $activePeriod->addApplication($applicationId);    
            }
            
            if (($isIsreeDomain || $isHistoryDomain || $isEngineeringDomain) && $applicationId) {
                
                $_SESSION['appid'] = $applicationId;
                
                $applicationPrograms = $activePeriod->getPrograms();
                $i = 1;
                foreach ($applicationPrograms as $unitId => $programName) {
                    
                    $programId = NULL;
                    $programIdQuery = "SELECT programs_id FROM programs_unit
                                        WHERE unit_id = " . $unitId;
                    $result = mysql_query($programIdQuery) or die(mysql_error());
                    while ( $row = mysql_fetch_array($result) ) {
                        $programId = $row['programs_id'];
                    }

                    if ($programId) {
                        $applicationProgramQuery = "INSERT INTO lu_application_programs
                                                    (application_id, program_id, choice)
                                                    VALUES(" . $applicationId . ","
                                                    . $programId . "," . $i . ")";   
                        mysql_query($applicationProgramQuery) or die(mysql_error()); 
                        
                        updateReqComplete("programs.php", 1);
                        
                        $i++;                        
                    }
                }                            
            }

            // DAS added automatic selection of PhD for Statistics
            if ($_SESSION['domainid'] == 44 )  {
                $_SESSION['appid'] = $applicationId;
                
                $applicationPrograms = $activePeriod->getPrograms();
                $i = 1;
                foreach ($applicationPrograms as $unitId => $programName) {
                    
                    $programId = NULL;
                    $programIdQuery = "SELECT programs_id FROM programs_unit
                                        WHERE unit_id = " . $unitId;
                    $result = mysql_query($programIdQuery) or die(mysql_error());
                    while ( $row = mysql_fetch_array($result) ) {
                        $programId = $row['programs_id'];
        }
        
                    if ($programId == 100013) {
                        $applicationProgramQuery = "INSERT INTO lu_application_programs
                                                    (application_id, program_id, choice)
                                                    VALUES(" . $applicationId . ","
                                                    . $programId . "," . $i . ")";   
                        mysql_query($applicationProgramQuery) or die(mysql_error());
                        updateReqComplete("programs.php", 1);
                        $i++; 
                    }   
                }                    
    }
        }
        
    }
}
//GET APPLICATIONS
/*
$sql = "SELECT id, name, paid, paymentamount, submitted, submitted_date,created_date,waive, paymenttype, paymentdate   
        from application where user_id = ". $_SESSION['userid'] . " order by created_date limit 1";
*/
 
/*
* PLB added 9/18/09
* Get only applications that are not associated with a closed period
* and, if they are not associated with a period, were not created before the 
* start date of the active period.
* 
* PLB added check for $_SESSION['appid'] set by autologin 9/22/09. 
*/
if ( isset($_SESSION['appid']) && $_SESSION['appid'] != -1 ) {

    $sql = "SELECT id, name, paid, paymentamount, submitted, submitted_date, 
            created_date, waive, paymenttype, paymentdate, sent_to_program   
            FROM application WHERE id = " . $_SESSION['appid'];
    
} else {

    /*
    $activePeriodStartDate = NULL;
    if ($activePeriod) {
        $activePeriodStartDate = $activePeriod->getStartDate();    
    }
    $sql = "SELECT id, name, paid, paymentamount, submitted, submitted_date, 
            created_date, waive, paymenttype, paymentdate   
            FROM application 
            WHERE user_id = ". $_SESSION['userid']; 
    if ($activePeriodStartDate) {
        $sql .= " AND created_date > '" . $activePeriodStartDate ."'";   
    } 
    */
    //$sql .= " AND id NOT IN 
            /* for applications associated with closed periods */
    /*        (SELECT application_id FROM period_application
            INNER JOIN period ON period_application.period_id = period.period_id
            WHERE period.period_type_id = 1 
            AND period.end_date <= NOW() 
            )
            ORDER BY created_date DESC LIMIT 1";
    */
    $sql = "SELECT id, name, paid, paymentamount, submitted, submitted_date, 
            created_date, waive, paymenttype, paymentdate, sent_to_program   
            FROM application 
            INNER JOIN period_application ON application.id = period_application.application_id
            WHERE user_id = ". $_SESSION['userid'];
    $sql .= " AND period_application.period_id = " . $activePeriodId;  
}

$result = mysql_query($sql) or die(mysql_error());


while($row = mysql_fetch_array( $result )) 
{
    $submittedDate = formatUSdate3($row['submitted_date']);
    $paymentDate = formatUSdate3($row['paymentdate']);
    $arr = array();
    array_push($arr, $row['id']);
    array_push($arr, $row['name']);
    array_push($arr, $row['paid']);
    array_push($arr, $row['paymentamount']);
    array_push($arr, $row['submitted']);
    array_push($arr, $submittedDate);    
    array_push($arr, $row['created_date']);
    array_push($arr, $row['waive']);
    array_push($arr, $row['paymenttype']);
    array_push($arr, $paymentDate);
    array_push($arr, $row['sent_to_program']);  // $apps[10] 
    array_push($apps, $arr);
    //echo $row['id']."<br>";
}

//GET USER INFO
$sql = "SELECT firstname from lu_users_usertypes
inner join users on users.id = lu_users_usertypes.user_id where lu_users_usertypes.id = ". $_SESSION['userid'];
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
    $firstname = $row['firstname'];
}

if(isset($_SESSION['allow_edit']))
{

    if(count($apps) > 0 )
    {
        $_SESSION['appid'] = $apps[0][0];
    }
        
    if((isset($_GET['id']) || $_SESSION['appid'] != -1) && $_SESSION['userid'] != -1 )
    {
        if(isset($_GET['id']))
        {
            $_SESSION['appid'] = $_GET['id'];
        }
        
        //VERIFY USER/APP COMBO
        $sql = "select id, name from application where user_id = ".$_SESSION['userid']." and id = ". intval($_SESSION['appid']);
        $result = mysql_query($sql) or die(mysql_error());
        
        while($row = mysql_fetch_array( $result )) 
        {
            $_SESSION['appid'] = $row['id'];
            $appName = $row['name'];
        }
        //GET REQUIREMENTS
        /*
        $sql = "SELECT applicationreqs.id, applicationreqs.short, applicationreqs.linkname, application.name as appname, 
        lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified,
        lu_application_appreqs.completed
        FROM lu_application_programs
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join lu_degrees_applicationreqs on lu_degrees_applicationreqs.degree_id = programs.degree_id
        inner join applicationreqs on applicationreqs.id = lu_degrees_applicationreqs.appreq_id
        inner join application on application.id = lu_application_programs.application_id
        left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
        and lu_application_appreqs.req_id = applicationreqs.id
        where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id order by applicationreqs.sortorder";
        */
        $sql = "SELECT applicationreqs.id, applicationreqs.short, applicationreqs.linkname, application.name as appname, 
        lu_application_appreqs.id as compreqid, lu_application_appreqs.last_modified,
        lu_application_appreqs.completed
        FROM lu_application_programs
        inner join programs on programs.id = lu_application_programs.program_id
        inner join programs_applicationreqs on programs_applicationreqs.programs_id = programs.id
        inner join applicationreqs on applicationreqs.id = programs_applicationreqs.applicationreqs_id
        inner join application on application.id = lu_application_programs.application_id
        left outer join lu_application_appreqs on lu_application_appreqs.application_id = lu_application_programs.application_id
        and lu_application_appreqs.req_id = applicationreqs.id
        where lu_application_programs.application_id = " . $_SESSION['appid'] . " group by applicationreqs.id order by applicationreqs.sortorder";
        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            $arr = array();
            array_push($arr, $row['id']);
            
            // Requirement text
            if ($row['short'] == 'Programs' && $isIsreeDomain)
            {
                array_push($arr, 'Course Selection');    
            }
            elseif ($row['short'] == 'Programs' && $isHistoryDomain)
            {
                array_push($arr, 'Program');
            }
            elseif ($row['linkname'] == 'proficiency.php' && ($isHistoryDomain || $isModLangPhdDomain))
            {
                array_push($arr, 'Writing Samples and Language Proficiency');
            }
            elseif ($row['linkname'] == 'proficiency.php' && ($isModLangMaDomain))
            {
                array_push($arr, 'Writing and Spoken Language Samples');
            }
            elseif ($row['linkname'] == 'proficiency.php' 
                && ($isEnglishDomain || $isPhilosophyDomain ))
            {
                array_push($arr, 'Writing Samples');
            }
            elseif ($row['linkname'] == 'resume.php' && $isHistoryDomain)
            {
                array_push($arr, 'Statement and C.V.');
            }
            else 
            {
                array_push($arr, $row['short']);
            }
            
            array_push($arr, $row['linkname']);    
            array_push($arr, $row['compreqid']);    
            array_push($arr, formatUSdate($row['last_modified']));    
            array_push($arr, $row['completed']);    
            array_push($compReqs, $arr);
        }
    }
}//END ALLOW EDIT

/*
* new functions for showing status of each requirement.
*/ 
function getTestScoreStatus($applicationId) {

    $countSubmitted = 0;
    $countReceived = 0;
    
    $baseView = new VW_AdminListBase();
    $baseArray = $baseView->find(NULL, NULL, NULL, $applicationId);

    if ( !empty($baseArray) && $baseArray[0]['gre_submitted']) {    // base array *not* indexed by applicationId
        $countSubmitted++;
    }
    if ( !empty($baseArray) && $baseArray[0]['gre_rcvd']) {    // base array *not* indexed by applicationId
        $countReceived++;
    }
    
    $greSubjectView = new VW_AdminListGreSubjectCount();
    $greSubjectArray = $greSubjectView->find(NULL, NULL, NULL, $applicationId); 
    if ( isset($greSubjectArray[$applicationId]) && $greSubjectArray[$applicationId]['ct_gresubject_entered']) {
        $countSubmitted += $greSubjectArray[$applicationId]['ct_gresubject_entered'];
        $countReceived += $greSubjectArray[$applicationId]['ct_gresubject_received'];
    }
    
    $toeflView = new VW_AdminListToeflCount();
    $toeflArray = $toeflView->find(NULL, NULL, NULL, $applicationId);
    if ( isset($toeflArray[$applicationId]) && $toeflArray[$applicationId]['toefl_entered']) {
        $countSubmitted += $toeflArray[$applicationId]['toefl_entered'];
        $countReceived += $toeflArray[$applicationId]['toefl_submitted'];
    }
    
    $ieltsView = new VW_AdminListIeltsCount();
    $ieltsArray = $ieltsView->find(NULL, NULL, NULL, $applicationId);
    if ( isset($ieltsArray[$applicationId]) && $ieltsArray[$applicationId]['ct_ielts_entered']) {
        $countSubmitted += $ieltsArray[$applicationId]['ct_ielts_entered'];
        $countReceived += $ieltsArray[$applicationId]['ct_ielts_received'];
    }
    
    $gmatView = new VW_AdminListGmatCount();
    $gmatArray = $gmatView->find(NULL, NULL, NULL, $applicationId);
    if ( isset($gmatArray[$applicationId]) && $gmatArray[$applicationId]['ct_gmat_entered']) {
        $countSubmitted += $gmatArray[$applicationId]['ct_gmat_entered'];
        $countReceived += $gmatArray[$applicationId]['ct_gmat_received'];
    }

    $statusMessage = $countReceived . ' of ' . $countSubmitted;
    $statusMessage .= ' official score reports received';
    return $statusMessage;
}

function getTranscriptStatus($applicationId) {

    $statusMessage = '';
    $baseView = new VW_AdminListBase();
    $baseArray = $baseView->find(NULL, NULL, NULL, $applicationId);
    if ( !empty($baseArray) ) {     // base array *not* indexed by applicationId
        $statusMessage = $baseArray[0]['ct_transcripts_received'] . ' of ';
        $statusMessage .= $baseArray[0]['ct_transcripts_submitted'];
        $statusMessage .= ' official transcripts received';     
    }
    return $statusMessage;
}

function getRecommendationStatus($applicationId) {
    
    $statusMessage = '';
    $recommendationView = new VW_AdminListRecommendationCount();
    $recommendationArray = $recommendationView->find(NULL, NULL, NULL, $applicationId);
    
    global $domainid;
    
    if ( isMseMsitDomain($domainid) ) {
        if (isset($recommendationArray[$applicationId]['ct_recommendation_files_submitted'])) {
            $recomendationsSubmitted = $recommendationArray[$applicationId]['ct_recommendation_files_submitted'];    
        } else {
            $recomendationsSubmitted = 0;    
        }    
    } else {
        if (isset($recommendationArray[$applicationId]['ct_recommendations_submitted'])) {
            $recomendationsSubmitted = $recommendationArray[$applicationId]['ct_recommendations_submitted'];    
        } else {
            $recomendationsSubmitted = 0;    
        }
    }
    
    if ( !empty($recommendationArray) ) {
        $statusMessage = $recomendationsSubmitted . ' of ';
        $statusMessage .= $recommendationArray[$applicationId]['ct_recommendations_requested'];
        $statusMessage .= ' letters received';     
    }
    return $statusMessage;
}

/*
* "Old" functions 
*/
function getProgress($appid)
{
    global $submitted;
    global $paid;
    global $submissionDate;
    global $paymentdate;
    global $notified;
    global $reviewed;
    $sql = "SELECT 
    submitted, submitted_date, paid, paymentamount, paymentdate, notificationsent,waive
    FROM application
    where id=".$appid;
    //echo $sql;
    $result = mysql_query($sql) or die(mysql_error().$sql);
    
    while($row = mysql_fetch_array( $result )) 
    {
        if($row['submitted'] == "1")
        {
            $submitted = "Submitted";
        }
        if($row['waive'] == "1")
        {
            $submitted = "Payment Waived";
        }else
        {
            if($row['paid'] == "1")
            {
                $submitted = "Paid";
            }
            $paymentdate = $row['paymentdate'];
        }
        $submissionDate = $row['submitted_date'];
        
        $notified = $row['notificationsent'];
        $reviewed = false;
    }
}

function getProgs($appid)
{
    $ret = array();
    $sql = "SELECT programs.id, degree.name as degreename,fieldsofstudy.name as fieldname,programs.linkword  from lu_application_programs
    inner join programs on programs.id = lu_application_programs.program_id
    inner join degree on degree.id = programs.degree_id
    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
    inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
    inner join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
    where lu_application_programs.application_id = ". $appid . " group by programs.id order by lu_application_programs.choice
     " ;
    $result = mysql_query($sql) or die(mysql_error());
    
    while($row = mysql_fetch_array( $result )) 
    {
        $arr = array();
        array_push($arr, $row['id']);
        array_push($arr, $row['degreename']);
        array_push($arr, $row['fieldname']);
        array_push($arr, $row['linkword']);
        array_push($ret, $arr);
    }
    
    return $ret;
}

function getDomainPrograms () {

        $sql = "SELECT 
            programs.id,
            domain.id as domain
             FROM programs  
              inner join lu_programs_departments on lu_programs_departments.program_id = programs.id
              inner join department on department.id = lu_programs_departments.department_id
              inner join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
              inner join domain on domain.id = lu_domain_department.domain_id
              order by domain.id";
        
        $result = mysql_query($sql) or die(mysql_error(). $sql);
        $lastDomainId = 1;
        $domainProgs = array();
        $programArray = array();
        while($row = mysql_fetch_array( $result )) {
            if ($row['domain'] != $lastDomainId) {
            //    $prog = array($lastDomainId => $programArray);
                $domainProgs[$lastDomainId] = $programArray;
                $lastDomainId = $row['domain'];
                $programArray = array();
            }
            array_push($programArray, $row['id']);
        }
        $domainProgs[$lastDomainId] = $programArray;
        return ($domainProgs);
}

function appFromCurrentDomainP () {
    global $progs;
    global $domainid;
    
    $domainPrograms = getDomainPrograms();
    $progInDomain = TRUE;
    foreach ($progs as $curProg) {      
        foreach ($curProg as $testProg) {
            if (in_array($curProg[0], $domainPrograms[$domainid]) === FALSE) {
                $progInDomain = FALSE;
            }
        }
    }
    return ($progInDomain);
}


// This is the new check to keeep from using the same application across two domains
// Variable initialization and redirect logic moved from line 572.
// NOTE: The assumption here is that there is at most one application (see query line 144).
for($i = 0; $i < count($apps); $i++) {
    $progs = getProgs($apps[$i][0]);
    $appValidForDomain = appFromCurrentDomainP();
    if ($appValidForDomain == FALSE) {
        header("Location: domainIssue.php");
        exit;
    } 
    if(count($progs) == 0 || (isset($_POST['btnNewApp']) && $domainid == 44 && count($progs) == 1)){ 
        header("Location: programs.php");
        exit;    
    }
    $_SESSION['programs'] = $progs; 
}

// Include the shared page header elements.
$pageTitle = 'Welcome, ' . $firstname . '!';
include '../inc/tpl.pageHeaderApply.php';

//if ($_SESSION['domainid'] == 42) {
if ($isIsreeDomain) {
    $documentName = 'registration';    
} else if ($isEngineeringDomain) {
    $documentName = 'deposit';
} else {
    $documentName = 'application';    
}
        
if(count($apps) > 0)
{
    //do nothing
}else{
    $domainid = 1;
    if(isset($_SESSION['domainid']))
    {
        if($_SESSION['domainid'] > -1)
        {
            $domainid = $_SESSION['domainid'];
        }
    }
    $sql = "select content from content where name='Home Page' and domain_id=".$domainid;
    $result = mysql_query($sql)    or die(mysql_error());
    while($row = mysql_fetch_array( $result )) 
    {
        echo html_entity_decode($row["content"]);
        echo "<br>";
    }
    showEditText("Start a new application", "button", "btnNewApp", $_SESSION['allow_edit']); 
}//end count apps

if(count($apps) > 0)
{ 
$class = "tblItem";
$paid = "not paid";

?>
<table width="660" height="480"  border="0" cellpadding="4" cellspacing="6">
  <tr>
    <td valign="top" class="tblItem">               

<?
// Instantiate payment manager with higherFeeDate
if(isset($activePeriod)) {
    $higherFeeDate = $activePeriod->getHigherFeeDate();    
} else {
    $higherFeeDate = NULL;
}

$paymentManager = new PaymentManager($_SESSION['appid'], $higherFeeDate);
$totalFee = $paymentManager->getTotalFee();
$totalPaymentsPaid = $paymentManager->getTotalPaid();  
        
for($i = 0; $i < count($apps); $i++)
{
    $submissionDate = $apps[$i][5];
    $paymentdate = $apps[$i][9];
    $paymentamount = $apps[$i][3];
    echo "<input name='appid' type='hidden' value='".$apps[$i][0]."'>";
/*
    if($i == 0)
    {
    ?><span class="subTitle">Your application </span><br><?
    }
*/
    if($i == 1)
    {
        ?>
        <br><span class="subTitle">Your previous applications</span><br>
        <?
    }
    if($i > 0)
    {
        echo "<em>".$i.") - ".formatUSDate($apps[$i][6])."</em><br>";
    }
    if($apps[$i][4] == 1)
    {
        $submitted = "checked";
    }
    if($i %2 == 0)
    {
        $class = "tblItemAlt";
    }
    if($apps[$i][8] > 0 && $apps[$i][2] != 1 && $apps[$i][9] != NULL)
    {
        // PLB added amount 8/13/09
        $totalPayments = $apps[$i][3];
        $paymentsUnpaid = abs($totalPayments - $totalPaymentsPaid);
        $paid = '';
        $allowVoidPayment = FALSE;
        if ( ($totalPaymentsPaid != $totalFee) && ($paymentsUnpaid > 0) ) {
            if ($apps[$i][8] == 3) {
                $paid = 'Pending confirmation of upload: ';    
            } else {
                $paid = 'Pending Receipt of Payment: ';    
            }
            $paid .= ' $' . number_format( $paymentsUnpaid );
            if ($apps[$i][8] != 2) {
                // Not a credit card payment.
                $allowVoidPayment = TRUE;    
            }
        } else {
            $paid .= ' $' . number_format( $totalPaymentsPaid );    
        }
        switch ($apps[$i][8])
        {
            case 1:
                $paid .= " Mailed in Payment";
                break;
            case 2:
                $paid .= " Paid by Credit Card";
                break;
            case 3:
                $paid .= " Voucher";
                break;
            case 4:
                $paid .= " (Other)";
                break;                
            case 5:
                $paid .= " Wire Transfer";
                break;
        }
        $balance = "";
        $totalcost = "";
    }
    if($apps[$i][7] == 1)
    {
        $paid = "Payment Waived";
        $balance = "";
        $totalcost = "";
    }else
    {
        if($i ==0)
        {
            if($apps[$i][2] == 1 && $apps[$i][9] != NULL)
            {
                $paid = "paid";
            }
            $amountPaid = $apps[$i][3];
            
            // $totalcost = totalCost($apps[$i][0]);
            $totalcost = $paymentManager->getTotalFee();
            
            $balance = $totalcost - $amountPaid;
        }
    }//end if waived
    
// This is the new check to keeep from using the same application across two domains
// NOTE variable initialization and redirect logic moved to line 425.    
/*
    $progs = getProgs($apps[$i][0]);
    $appValidForDomain = appFromCurrentDomainP();
    if ($appValidForDomain == FALSE) {
        echo "<input type=\"hidden\" name=\"appID\" value=".$apps[$i][0].">";
        header("Location: domainIssue.php");
    } 
*/
    if(count($progs)>0){
    ?>
     <br>
    <?php
    if ($isIsreeDomain) 
    {
    ?>
        <strong>Course</strong>     
    <?php 
    } 
    elseif ($isHistoryDomain)
    {
    ?>
        <strong>Program</strong>     
    <?php         
    }
    else 
    {
    ?>
        <strong>Program(s) selected in order of preference</strong> 
    <?php    
    }
    ?>
     <br>


      <div id="list">
          <ul>
            <?
            for($j = 0; $j < count($progs); $j++)
            { 
                echo "<li>".$progs[$j][1]." ".$progs[$j][3]." " .$progs[$j][2]. "</li>" ;
            }
          ?>
          </ul>
      </div>
    <? 
    /*
    // This redirect now happens at line 435. 
    }
    else{
        header("Location: programs.php");
        exit;
    */
    }//end if count progs
}//end for apps 
?>
<br>
<?
if($_SESSION['appid'] > -1)
{
if(count($compReqs) == 0 )
{
    echo "<strong>You need to apply to at least 1 program.</strong><br><br><a href='programs.php'><span class='subtitle'>Click here to view available programs</span></a>.<br>";
}
else
{
    
?>
    <span><strong><?php echo ucfirst($documentName); ?> Checklist</strong></span><br>
    <?php
    if ($unit->getId() != "54") {  ?>
        All sections of the <?php echo $documentName; ?> must be completed before the 
        <?php echo $documentName; ?> can be submitted. 
        A check will appear in the box when the section is complete. 
        <?php
        if (!$isIsreeDomain && !$isMsrtDomain && !$isEngineeringDomain) {
        ?>
        Once the <?php echo $documentName; ?> has been submitted, you may return to the  
        Application Status page to track receipt of test score reports, official transcripts,
        and letters of recommendation.
        <?php
            if ($unit->getId() == 1) {    
            ?>
                <br><br>
                After the deadline you will have access only to the Supplemental
                Information page to update the status of a publication that has
                previously been listed. No other changes or additions to the
                application will be allowed after the deadline.
    <?php
            }
        } 
    } else { 
    ?>
        The following sections of the <?php echo $documentName; ?> must be completed before your 
        <?php echo $documentName; ?> can be submitted. 
        A check will appear in the box when the section is complete. 
        You must submit your completed application by the deadline. 
        Once the application has been submitted, you may return to the corresponding 
        section to track receipt of letters of recommendation.
    <?php 
    } 
    ?>
    
    <br>                                 
    <br>
    <table width='100%'  border='0' cellspacing='0' cellpadding='0'>
    <tr>
    <td class="tblItem"><strong> </strong></td>
    <td class="tblItem"><i>Last Modified</i></td>
    </tr>
    <?
    $appComp = true;
       
     if(count($compReqs) == 0)
     {
      $appComp = false;
     }
     
     for($i = 0; $i < count($compReqs); $i++)
      {
        if($compReqs[$i][5] != 1)
        {
            $appComp = false;
        }

        $date = "";
        $comp = "0";
        if($compReqs[$i][4] != "" && $compReqs[$i][4] != "00/00/0000")
        {
            $date = "(".$compReqs[$i][4].")";
            
        }
        //$comp = "1";
        echo "<tr><td class='tblItem'>";
        // PLB added section status info 7/30/09
        $applicationrecsId = $compReqs[$i][0]; 
         showEditText($compReqs[$i][5], "checkbox", "checkbox",true,false, null,false);
         
         if ($isDesignPhdDomain && $compReqs[$i][2] == 'resume.php')
         {
            echo "<a href='". $compReqs[$i][2] ."'>Experience</a>";
             
         }
         elseif ($isDesignPhdDomain && $compReqs[$i][2] == 'portfolio.php')
         {
            echo "<a href='". $compReqs[$i][2] ."'>Portfolio of Expertise</a>";
         }
         else
         {
            echo "<a href='". $compReqs[$i][2] ."'>" . str_replace("<br>"," ",$compReqs[$i][1]) . "</a>";
         }

         switch ($applicationrecsId) {
             
             case 3:                          
                $requirementStatusMessage = getTestScoreStatus($_SESSION['appid']);
                break;

             case 4:
                if ($unit->getId() != "54") {                         
                $requirementStatusMessage = getTranscriptStatus($_SESSION['appid']);
                }
                break;
             
             case 6:
             
                $requirementStatusMessage = getRecommendationStatus($_SESSION['appid']);
                break;
                
             default:
                $requirementStatusMessage = '';
         }
         
         if ($requirementStatusMessage && !$isMsrtDomain && !$isPhilosophyDomain) {
             if(($isScsDomain || $isREUSEDomain) && ($applicationrecsId == 3 || $applicationrecsId == 4)) {
                 // do not display status
             } else {
                echo ' <i>(' . $requirementStatusMessage . ')</i>';
             }
         }
         echo "</td><td class='tblItem'><em>".$date."</em></td></tr>";
      }

       $submitted = "";
        for($i = 0; $i < count($apps); $i++)
        { 
            if($apps[$i][4] == 1 && $apps[$i][0] == $_SESSION['appid'])
            {
                $submitted = "checked";
            }
        
        }
      if($appComp == true && $submitted == "checked")
      {
      ?>
      <tr class = "tblItem">
      <td><? showEditText(true, "checkbox", "checkbox",true,false, null,false); ?>
      <?php echo ucfirst($documentName); ?> Submitted
      </td>
      <td><em>(<?= $submissionDate ?>)</em></td>
      </tr>
      <?
      
      if ( isMsrtDomain($_SESSION['domainid']) ) {

          // sent_to_program checked means partner app(s) have been submitted
          if ($apps[0][10]) {
              $partnersChecked = TRUE;     
          } else {
              $partnersChecked = FALSE;   
          }
          
          $partnerUrls = array(
            100009 => array(
                'name' => 'Nanjing University of Science and Technology',
                'url' => 'http://cs.njust.edu.cn/nc/'
            ),
            100026 => array(
                'name' => 'Plymouth University',
                'url' => 'http://www.plymouth.ac.uk/courses/postgraduate/taught/4342/MSc+Robotics+Technology'
            )
            /*
            ,             
            100010 => array(
                'name' => 'Partner in the Dominican Republic',
                'url' => 'http://www.ri.cmu.edu/ri_static_content.html?menu_id=388'
            ), 
            100011 => array(
                'name' => 'Partner in Mexico',
                'url' => 'http://www.ri.cmu.edu/ri_static_content.html?menu_id=388'
            )
            */  
          );
            
      ?>
          <tr class = "tblItem">
          <td><? showEditText($partnersChecked, "checkbox", "checkbox", true, false, null,false); ?>
          Partner School Application(s) Submitted
            <ul style="margin-top: 0px;">
                <?php
                foreach ($progs as $progArray) {
                    $partnerProgramId = $progArray[0];
                    if ( isset($partnerUrls[$partnerProgramId]) ) {
                        echo '<li><a href="' . $partnerUrls[$partnerProgramId]['url'] . '">';
                        echo $partnerUrls[$partnerProgramId]['name'] . '</a></li>';    
                    }
                }
                ?>
            </ul>
          </td>
          <td><!-- <em>(<?= $submissionDate ?>)</em> --></td>
          </tr>
      <?php
      } 
      
      }//end checked submit
                  

      // Do not display Application Fee Status if there is no fee required
      // no fee required equates to totalcost==0
      if ( $totalcost > 0 || $paid == "Payment Waived" )
      {
        if($appComp == true && 
        ( $paid == "Payment Waived" 
            || ($paid == "paid" && $paymentdate != "" 
                && $paymentdate != "00/00/0000" && $paymentamount >= $totalcost) )
        )
        {
      ?>
          <tr class = "tblItem">
          <td><? showEditText(true, "checkbox", "checkbox",true,false, null,false); ?>
          <?php echo ucfirst($documentName); ?> Fee 
                          <?
          if($paid == "paid")
          {
            echo "Paid";
          } 
          elseif($paid == "Payment Waived")
          {
            echo "Waived";
          } else {
            echo "Submitted";
          }
      ?>
          </td>
          <td><em>
          <?php
          if ($paid != "Payment Waived" && $paymentdate != "" && $paymentdate != "00/00/0000") {
              echo '(' . $paymentdate . ')'; 
          }
    ?>
          </em></td>
          </tr>
      <?
        }
                          } // if (totalcost > 0 ) 
      ?>
      
  </table>
                
    <?
    if($appComp == true && $submitted != "checked"){
    ?>
    <br>
    You have provided the required information for all sections of this <?php echo $documentName; ?>. 

      <? // if totalcost > 0, this means that an application fee is required
         // so display the fee and instructions
        if ($totalcost > 0)
        {
      ?>
          The final step of the process is to submit the completed <?php echo $documentName; ?> 
          and pay your <?php echo $documentName; ?> fee of $<?= $totalcost ?>
      <?
          $buttonLabel="Proceed to Submission & Payment";
        } else {
      ?>
          The final step of the process is to submit the completed <?php echo $documentName; ?>.
      <?
          $buttonLabel="Proceed to Submission";
        }
      ?>
    <br>
    <br>
    <input class="tblItem" name="btnSubmit" type="button" id="btnSubmit" value="<?=$buttonLabel?>" onClick="document.location='submit.php'"><br>

    <? } ?>   
            
<? if($submitted == "checked")
{   
?>
<br>
<div id="list">

<?
$invoiceButtonDisplayed = FALSE;  
if($paid == "Payment Waived") { 
/*
?>
  Payment Status: Waived
<?  
*/
    if ($isIsreeDomain && !$invoiceButtonDisplayed) {
    ?>
        <input class="tblItem" name="btnInvoice" type="button" id="btnInvoice" 
            value="Print Invoice" onClick="window.open('invoice.php')"> 
    <?php
        $invoiceButtonDisplayed = TRUE;
    } 

} else { // Payment not waived 
  
  $balance = $totalcost - $amountPaid;
  //DebugBreak();
  if($paymentdate != "" && $paymentdate != "00/00/0000 00:00:00")
  {
        //$paid == "Pending Receipt of Payment"
        // PLB added display of pending payments 8/13/09
        if ( strstr($paid, 'Pending') && ($balance <= 0) ) {
            echo '<ul><li>Payment Status: ' . $paid;
            if ($allowVoidPayment) {
            ?>
                <br><br>
                <input class="tblItem" name="btnCancel" type="button" id="btnCancel" value="Change Payment Method" onClick="document.location='pay.php'">                
            <?php 
                if ($isIsreeDomain && !$invoiceButtonDisplayed) {
                ?>
                    <input class="tblItem" name="btnInvoice" type="button" id="btnInvoice" 
                        value="Print Invoice" onClick="window.open('invoice.php')"> 
                <?php
                    $invoiceButtonDisplayed = TRUE;
                }   
            }
            echo '</li></ul>';    
        }
        
        // Copied more or less from pay.php line 396
        if ( strstr($paid, 'Voucher') ) {

            echo '<br><ul>';
            foreach ($paymentManager->getPayments() as $payment) {

                if ($payment['payment_type'] == 3 
                    && $payment['payment_status'] == 'pending'
                    ) {
                    
                    if ($paymentsUnpaid > 0) {
                ?>
                    <li>
                    <input class="tblItem" name="btnUpload" value="Upload New Voucher" type="button" 
                        onClick="parent.location='fileUpload.php?id=<?php echo $payment['payment_id']; ?>&t=21'" />                        
                <?php
                    }
                    $voucherQuery = "SELECT payment.application_id, 
                        lu_users_usertypes.user_id AS users_id, datafileinfo.* 
                        FROM payment
                        INNER JOIN payment_voucher ON payment.payment_id = payment_voucher.payment_id 
                        INNER JOIN datafileinfo ON payment_voucher.datafileinfo_id = datafileinfo.id
                        INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
                        WHERE payment.payment_id = " . $payment['payment_id'];
                    $voucherResult = mysql_query($voucherQuery) or die(mysql_error());
                    while ($row = mysql_fetch_array($voucherResult)) {
                        showFileInfo("voucher." . $row['extension'], $row['size'], 
                            formatUSdate2($row['moddate']), 
                            getFilePath(21, $payment['payment_id'], 
                            $row['user_id'], $row['users_id'], $row['application_id'])
                            );    
                    }
                    ?>
                    </li>
                    <?php 
                } 
            }
            echo '</ul>'; 
        }  
  }
?>
  <ul style="list-style: none;">
<? 
  // Display Payment Button if there is a balance due AND 
  // the totalcost is > 0 i.e. no application fee required
  if(($submitted == true) && ($paymentdate == "" || $balance > 0) && ($totalcost > 0) && $activePeriod->beforeLastPaymentDate())
  {
    if ($paid == 'paid') {
        $displayAmountPaid = '$' . $amountPaid . ' ';
    } else {
        $displayAmountPaid = '';    
    } 
?>
    <li><strong><?php echo ucfirst($documentName); ?> fee :</strong> $<?= $totalcost?>
    <em>(<?=$displayAmountPaid;?><?=$paid;?>)</em></li>
    <li style="margin-top: 3px;">You have a balance due of $<?=$balance?></li>
    <!--<li></li>-->
    <li style="margin-top: 3px;">
    <input class="tblItem" name="btnPay" type="button" id="btnBack" 
    value="Pay <?php echo ucfirst($documentName); ?> Fee Now" onClick="document.location='pay.php'">
    <?php
    if ($isIsreeDomain && !$invoiceButtonDisplayed) {
    ?>
        <input class="tblItem" name="btnInvoice" type="button" id="btnInvoice" 
            value="Print Invoice" onClick="window.open('invoice.php')"> 
    <?php
        $invoiceButtonDisplayed = TRUE;
    }
    ?>
    </li>
    <!--<li></li>-->
    <?php
    if (!$isEngineeringDomain) {
    ?>
    <li style="margin-top: 3px;"><strong>Note:</strong> Your <?php echo $documentName; ?> will not be considered unless the required fee is paid in full.
                   Please allow 2-3 business days for your credit card payment to be recorded as 
                 <em>Paid</em> on your submitted <?php echo $documentName; ?>. 
    </li>
<?    } } else { 
    // Display application complete if no balance remains OR
    // the totalcost == 0 i.e.no application fee
    /*
    if( ( ($appComp == true ) 
        && ( ( ($paymentdate != "") || ($balance == 0)) || ($totalcost == 0)) || $paid == "Payment Waived" ) )
    {
      echo "<br>";
                        
      $domainid = 1;
  if(isset($_SESSION['domainid']))
  {
        if($_SESSION['domainid'] > -1)
    {
      $domainid = $_SESSION['domainid'];
    }
      }
  $sql = "select content from content where name='Home Page - Application Complete' and domain_id=".$domainid;
  $result = mysql_query($sql)    or die(mysql_error());
      while($row = mysql_fetch_array( $result )) 
  {
      echo html_entity_decode($row["content"]);
    echo "<br>";
  }
    }
    */
    } //end if not submitted payment 
?>
    </ul>
<?    } //end if payment not waived


    if( ( ($appComp == true ) 
        && ( ( ($paymentdate != "") || ($balance == 0)) || ($totalcost == 0)) || $paid == "Payment Waived" ) )
    {
      echo "<br>";
                        
      $domainid = 1;
  if(isset($_SESSION['domainid']))
  {
        if($_SESSION['domainid'] > -1)
    {
      $domainid = $_SESSION['domainid'];
    }
  }
      if ($isIsreeDomain && !$invoiceButtonDisplayed) {    
      ?>        
            <input class="tblItem" name="btnInvoice" type="button" id="btnInvoice"             
            value="Print Invoice" onClick="window.open('invoice.php')">     
      <?php        
        $invoiceButtonDisplayed = TRUE;    
      }    
  $sql = "select content from content where name='Home Page - Application Complete' and domain_id=".$domainid;
  $result = mysql_query($sql)    or die(mysql_error());
      while($row = mysql_fetch_array( $result )) 
  {
      echo html_entity_decode($row["content"]);
    echo "<br>";
  }
    }


?>
</div>                
<span class="subtitle">
<? 
} //end if submitted 

?>

<? 
}//end if compreqs  
}//end appid
?>                  
</td>

<? if($_SESSION['appid'] != -1 ){ ?>
<? }//END IF APPID 
?>

</tr>
</table>
<?php
}//end count apps 

// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>