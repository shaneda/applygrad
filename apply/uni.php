<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
// Don't do the scriptaculous include in function.php
// The files are included in $headJavascriptFiles below.
$exclude_scriptaculous = TRUE;  
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../apply/header_prefix.php';
include '../inc/specialCasesApply.inc.php';

global $db_conn;
mysql_set_charset('utf8', $db_conn); 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

/*
* Test for special cases. 
*/ 
$isIniDomain = isIniDomain($domainid);
$isEM2Domain = isEM2Domain($domainid);
$isDesignPhdDomain = isDesignPhdDomain($domainid);
$designPhdTranscriptOptional = FALSE;
if ($isDesignPhdDomain)
{
    $designPhdTranscriptOptional = designPhdTranscriptOptional($_SESSION['appid']);
}

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
        {
            $prevPage = "";
        }
if (sizeof ($appPageList) > $curPageIndex + 1)
    {
        $nextPage =  $appPageList[$curPageIndex + 1]; 
    } 
    else
        {
            $nextPage = "";
        }


$degrees = array();
$uniTypes = array();
$institutes = array();
$myInstitutes = array();
$scales = array();
$err = "";
$transUploaded = true;
$comp = false;
$classRank = "";

$arr = array(2, "Graduate");
array_push($uniTypes, $arr);
$arr = array(3, "Additional College/University");
array_push($uniTypes, $arr);
	
/*
foreach($_POST as $key=>$val)
		{
			echo "key: ".$key . " val: ".$val."<br>";
		}
*/

// DEGREES
$result = mysql_query("SELECT * FROM degreesall order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);

	array_push($degrees, $arr);
}

// GPA SCALES
$result = mysql_query("SELECT * FROM gpascales order by name") or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);

	array_push($scales, $arr);
}


// INSTITUTES - used for autocomplete too
$sql = "SELECT * FROM institutes WHERE hide = 0 order by name";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$arr = array();
	array_push($arr, $row['id']);
    
    //  this is coming out double encoded -  should be htmlspecialchars_decode($row['name']);
	//  array_push($arr, htmlspecialchars(trim($row['name']), ENT_NOQUOTES, 'UTF-8'));
    array_push($arr, htmlspecialchars_decode($row['name']));

	array_push($institutes, $arr);
}

function addUni($allowDup, $type)
{
	$doAdd = true;
	global $err;
	if($allowDup == false)
	{
		// PLB changed query to use application id 9/23/09
        $sql = "SELECT id FROM usersinst WHERE application_id = ".$_SESSION['appid'] . " AND educationtype = " . $type;
        //$sql = "select id from usersinst where user_id = ".$_SESSION['userid'] . " and educationtype=".$type;
		$result = mysql_query($sql)	or die(mysql_error());
		while($row = mysql_fetch_array( $result )) 
		{
			$doAdd = false;
		}
	}
	if($doAdd == true)
	{
		// PLB changed query to include application id 9/23/09 
        $sql = "INSERT INTO usersinst (user_id, application_id, educationtype) 
                VALUES (" . $_SESSION['userid'] . ", " . $_SESSION['appid'] . ", " . $type . ")";
        //$sql = "insert into usersinst(user_id, educationtype)values(".$_SESSION['userid'].", ".$type.")"; 
		mysql_query($sql)	or die(mysql_error().$sql);
	}else
	{
		$err .= "You cannot add more than 1 school of this type.";
	}
}

//INSERT RECORD TABLE FOR USER
// ?????? an educationtype=3 record is inserted automatically? 
if(isset($_POST['btnAdd']))
{
	$allowdups = true;
	
	
	addUni($allowdups, 3);
}//END POST BTNADD

if( isset( $_POST ) && !isset($_POST['btnAdd']) )
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{		
        // This is for deleting a record (????????)
        if( strstr($key, 'btnDel') !== false ) 
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
			
			$datafileid = -1;
			$type = -1;
			if($itemId != "")
			{
				// PLB changed query to use application id 9/23/09
                $sql = "SELECT datafile_id, educationtype FROM usersinst 
                        WHERE application_id = " . $_SESSION['appid'] . " AND id = " . $itemId;
                //$sql = "select datafile_id, educationtype from usersinst where user_id=".$_SESSION['userid']. " and id=" . $itemId;
				$result = mysql_query($sql) or die(mysql_error());
				while($row = mysql_fetch_array( $result )) 
				{
					$datafileid = $row['datafile_id'];
					$type = $row['educationtype'];
				}
				if($datafileid != "")
				{
					$sql = "delete from datafileinfo where id = " . $datafileid;
					mysql_query($sql) or die(mysql_error().$sql);
				}
	
				// ???????????????????? isn't id sufficient?
                // PLB changed query to use application id 9/23/09
                $sql = "DELETE FROM usersinst 
                        WHERE application_id = " .$_SESSION['appid'] . " AND id = " . $itemId;
				//$sql = "DELETE FROM usersinst WHERE user_id=".$_SESSION['userid']. " and id=" . $itemId;
                mysql_query($sql) or die(mysql_error().$sql);
				if($type == 1)
				{
					updateReqComplete("uni.php", 0, 1);
				}
			}
		}
	}//END FOR
}//END IF

//UPDATE
if( isset($_POST['btnSave'])) 
{
	$vars = $_POST;
	$itemId = -1;
	$tmpid = -1;
	$arrItemId = array();
	//HERE WE GET THE RECORD IDS FOR THE UPDATED SCHOOLS
	foreach($vars as $key => $value)
	{
		$arr = split("_", $key);
		if($arr[0] == "txtType")
		{
			$tmpid = $arr[1];
			if($itemId != $tmpid)
			{
				$itemId = $tmpid;
				array_push($arrItemId,$itemId);
			}
		}
	}//END FOR
	//ITERATE THROUGH IDS AND DO UPDATES
	for($i = 0; $i < count($arrItemId); $i++)
	{
		$major2 = NULL;
		$minor1 = NULL;
		$minor2 = NULL;
		$type = "";
		$localErr = "";
		$doUpdate = true;
	    // encoded version
		// $institute= trim(htmlspecialchars($_POST["lbName_".$arrItemId[$i]]));
        //   unencoded version
         $institute= trim($_POST["lbName_".$arrItemId[$i]]);
        //$college_name = mysql_real_escape_string(trim(htmlspecialchars($_POST["lbCollegeName_".$arrItemId[$i]])));
		$date_entered= $_POST["txtDateE_".$arrItemId[$i]];
		$date_grad= $_POST["txtDateG_".$arrItemId[$i]];
		//$date_left= $_POST["txtDateL_".$arrItemId[$i]];
		$degree= intval($_POST["lbDegree_".$arrItemId[$i]]);
		$major1= mysql_real_escape_string(htmlspecialchars($_POST["txtMajor1_".$arrItemId[$i]]));
		if(isset($_POST["txtMajor2_".$arrItemId[$i]])){
		$major2= mysql_real_escape_string(htmlspecialchars($_POST["txtMajor2_".$arrItemId[$i]]));
		}
		$major3= NULL;
		if(isset($_POST["txtMinor1_".$arrItemId[$i]])){
		$minor1= mysql_real_escape_string(htmlspecialchars($_POST["txtMinor1_".$arrItemId[$i]]));
		}
		if(isset($_POST["txtMinor2_".$arrItemId[$i]])){
		$minor2= mysql_real_escape_string(htmlspecialchars($_POST["txtMinor2_".$arrItemId[$i]]));
		}
		$gpa= htmlspecialchars($_POST["txtGPA_".$arrItemId[$i]]);
		$gpa_major= htmlspecialchars($_POST["txtGPAMajor_".$arrItemId[$i]]);
		$gpa_scale= intval($_POST["lbScale_".$arrItemId[$i]]);
		if(isset($_POST["lbType_".$arrItemId[$i]]))
		{
			$type = intval($_POST["lbType_".$arrItemId[$i]]);
		}
		$institute = trim($institute);
        $instituteName = $institute;
        
		$key = marray_search($institute,$institutes);
		
		if($key === false )
		{
			$localErr .="You have entered an invalid university name (".$institute.")<br>";
			$doUpdate = false;
		}else
		{
			$institute = $institutes[$key][0];
		}
        if(isset($_POST["txtClassRank_".$arrItemId[$i]])){
        $classRank= htmlspecialchars($_POST["txtClassRank_".$arrItemId[$i]]);
        }
		//POPULATE ARRAY FOR POSTBACK
		$arr = array();
		array_push($arr, $arrItemId[$i]);
		if(isset($_POST["lbName_".$arrItemId[$i]])){array_push($arr, $_POST["lbName_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtDateE_".$arrItemId[$i]])){array_push($arr, $_POST["txtDateE_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtDateG_".$arrItemId[$i]])){array_push($arr, $_POST["txtDateG_".$arrItemId[$i]]);}else{array_push($arr,"");}
		array_push($arr,"" );
		if(isset($_POST["lbDegree_".$arrItemId[$i]])){array_push($arr, $_POST["lbDegree_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtMajor1_".$arrItemId[$i]])){array_push($arr, $_POST["txtMajor1_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtMajor2_".$arrItemId[$i]])){array_push($arr, $_POST["txtMajor2_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtMajor3_".$arrItemId[$i]])){array_push($arr, $_POST["txtMajor3_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtMinor1_".$arrItemId[$i]])){array_push($arr, $_POST["txtMinor1_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtMinor2_".$arrItemId[$i]])){array_push($arr, $_POST["txtMinor2_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtGPA_".$arrItemId[$i]])){array_push($arr, $_POST["txtGPA_".$arrItemId[$i]]);}else{array_push($arr,"");}		
		if(isset($_POST["txtGPAMajor_".$arrItemId[$i]])){array_push($arr, $_POST["txtGPAMajor_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["lbScale_".$arrItemId[$i]])){array_push($arr, $_POST["lbScale_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["txtClassRank_".$arrItemId[$i]])){array_push($arr, $_POST["txtClassRank_".$arrItemId[$i]]);}else{array_push($arr,"");}
		array_push($arr, 0);
		if(isset($_POST["txtDatafile_".$arrItemId[$i]])){array_push($arr, $_POST["txtDatafile_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["lbType_".$arrItemId[$i]])){array_push($arr, $_POST["lbType_".$arrItemId[$i]]);}else{array_push($arr,1);}
		if(isset($_POST["txtModdate_".$arrItemId[$i]])){array_push($arr, $_POST["txtModdate_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtSize_".$arrItemId[$i]])){array_push($arr, $_POST["txtSize_".$arrItemId[$i]]);}else{array_push($arr,"");}
		if(isset($_POST["txtExtension_".$arrItemId[$i]])){array_push($arr, $_POST["txtExtension_".$arrItemId[$i]]);}else{array_push($arr,"");}
        
        // Push some zeroes to get the numerical indexes to line up ?!?!?!?!?!?!?!!?!
        array_push($arr,"");
        array_push($arr,"");
        
        if(isset($_POST["lbCollegeName_".$arrItemId[$i]])){array_push($arr, $_POST["lbCollegeName_".$arrItemId[$i]]);}else{array_push($arr,"");}
        
		array_push($myInstitutes, $arr);
		
		//DO VERIFICATION
//        if ($isIniDomain && $college_name == '')
//        {
//            $localErr .= "You must enter the Affiliated College/School.<br>";    
//        }
        
		if(check_Date2($date_entered) == false)
		{
			$localErr .= "Date Entered is invalid (".$date_entered.").<br>";
		}
		if(check_Date2($date_grad) == false)
		{
			$localErr .= "Date of Graduation is invalid (".$date_grad.").<br>";
		}
		if($degree == 0)
		{
			$localErr .= "You must select a degree.<br>";
		}
		if($major1 == "")
		{
			$localErr .= "You must list your major.<br>";
		}
		if($gpa == "")
		{
			$localErr .= "You must list your GPA.<br>";
		}
		if($gpa_major == "")
		{
			$localErr .= "You must list your major GPA.<br>";
		}
		if($gpa_scale == 0)
		{
			$localErr .= "You must select a GPA scale.<br>";
		}
		
		if($doUpdate == true)
		{
            $college_name = '';
            
			$sql = "update usersinst set
			institute_id = " . $institute . ", 
            college_name = '" . $college_name . "',";
			if($type != "")
			{
				$sql .= "educationtype=".$type.", ";
			}
			// Again, isn't id sufficient here?
            // PLB changed query to use application id, anyway 9/23/09 
            
            $sql .= "date_entered = '" . formatMySQLdate2($date_entered,'/','-') . "',
            date_grad = '" . formatMySQLdate2($date_grad,'/','-') . "',
            degree = '" . $degree . "',
            major1 = '" . $major1 . "',
            major2 = '" . $major2 . "',
            major3 = '" . $major3 . "',
            minor1 = '" . $minor1 . "',
            minor2 = '" . $minor2 . "',
            gpa = '" . $gpa . "',
            gpa_major = '" . $gpa_major . "',
            gpa_scale =" . $gpa_scale . ",
            class_rank = '" . mysql_real_escape_string($classRank) . "'
            WHERE id = " . $arrItemId[$i] . " AND application_id= " . $_SESSION['appid'];
            /*
            $sql .="date_entered= '".formatMySQLdate2($date_entered,'/','-')."',
			date_grad= '".formatMySQLdate2($date_grad,'/','-')."',
			degree= '".$degree."',
			major1= '".$major1."',
			major2= '".$major2."',
			major3= '".$major3."',
			minor1= '".$minor1."',
			minor2= '".$minor2."',
			gpa= '".$gpa."',
			gpa_major= '".$gpa_major."',
			gpa_scale=".$gpa_scale.",
            class_rank= '".$classRank."'
			where id = ".$arrItemId[$i]." and user_id=".$_SESSION['userid'];
            */
			mysql_query($sql) or die(mysql_error());
			//echo $sql;
			//header("Location: home.php");
		}
		if($localErr == "")
		{
			/* 
            * Waive GRE requirement for applicants who:
            *   are applying only to MS in CS
            *   have entered a degree
            *   have enetered a graduation date  
            */
            if ($type == NULL && $institute == 261
                && $date_grad != NULL && $degree != NULL) {
            
                $isMsCsApplication = isMsCsApplication($_SESSION['appid']);
                if ($isMsCsApplication) {
                
                    updateReqComplete("scores.php", 1); 
                }
            }
            
            // Set transcript requirement based on transcript upload
            
            $sql = "select datafile_id from usersinst where id=".$arrItemId[$i];
			$result = mysql_query($sql) or die(mysql_error());			
			while($row = mysql_fetch_array( $result )) 
			{
				if($row['datafile_id'] == "")
				{
					$transUploaded = false;
				}
			}
			if($transUploaded == true)
			{   
				updateReqComplete("uni.php", 1);
			}
            elseif ($domainid == 34 || $designPhdTranscriptOptional)
            {
                updateReqComplete("uni.php", 1);    
            }
            else
			{
				updateReqComplete("uni.php", 0,1);
				$localErr .= "You must upload an unofficial transcript for " . $instituteName . ".<br>";
			}
		}

		if($localErr != "")
		{
			$err .= $localErr;
		}
		
        $localErr = "";
	}
}

//RETRIEVE USER INFORMATION
//USER MUST HAVE AT LEAST ONE UNDERGRAD SCHOOL SO ADD IT IF IT DOESN'T EXIST
$doAdd = true;
// PLB changed query to use application_id 9/23/09
$sql = "SELECT id FROM usersinst 
        WHERE application_id = " . $_SESSION['appid'] . " AND educationtype = 1";
//$sql = "select id from usersinst where user_id = ".$_SESSION['userid'] . " and educationtype=1";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	$doAdd = false;
}
if($doAdd == true)
{
	addUni(false, 1);
}

if(!isset($_POST['btnSave']))
{
	// PLB changed query to use application_id 9/23/09      
    $sql = "SELECT 
    usersinst.id, institutes.name, date_entered,
    date_grad, date_left, degree,
    major1, major2, major3, minor1, minor2, 
    gpa, gpa_major, gpa_scale,
    class_rank as classRank,
    transscriptreceived, datafile_id, educationtype,
    datafileinfo.moddate,
    datafileinfo.size,
    datafileinfo.extension,
    degreesall.name as degreename,
    gpascales.name as gpascalename,
    college_name
    FROM usersinst 
    LEFT OUTER JOIN institutes ON institutes.id = usersinst.institute_id
    LEFT OUTER JOIN datafileinfo ON datafileinfo.id = usersinst.datafile_id
    LEFT OUTER JOIN degreesall ON degreesall.id = usersinst.degree
    LEFT OUTER JOIN gpascales ON gpascales.id = usersinst.gpa_scale
    WHERE usersinst.application_id = " . $_SESSION['appid'] . " ORDER BY usersinst.educationtype";
    /*
    $sql = "SELECT 
	usersinst.id,institutes.name,date_entered,
	date_grad,date_left,degree,
	major1,major2,major3,minor1,minor2,gpa,gpa_major,
	gpa_scale,
    class_rank as classRank,
    transscriptreceived,datafile_id,educationtype,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension,
	degreesall.name as degreename,
	gpascales.name as gpascalename
	FROM usersinst 
	left outer join institutes on institutes.id = usersinst.institute_id
	left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
	left outer join degreesall on degreesall.id = usersinst.degree
	left outer join gpascales on gpascales.id = usersinst.gpa_scale
	where usersinst.user_id=".$_SESSION['userid']. " order by usersinst.educationtype";
    */
	$result = mysql_query($sql) or die(mysql_error());
	$comp = true;
	while($row = mysql_fetch_array( $result )) 
	{
		$arr = array();
		array_push($arr, $row['id']);
        $instituteName = trim($row['name']);
		array_push($arr, $instituteName);
		if($row['name'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_entered'],'-','/')  );
		if($row['date_entered'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_grad'],'-','/') );
		if($row['date_grad'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_left'],'-','/') );
		array_push($arr, $row['degree']);
		if($row['degree'] == ""){$comp = false; }
		array_push($arr, $row['major1']);
		array_push($arr, $row['major2']);
		array_push($arr, $row['major3']);
		array_push($arr, $row['minor1']);
		array_push($arr, $row['minor2']);
		array_push($arr, $row['gpa']);
		if($row['gpa'] == ""){$comp = false; }
		array_push($arr, $row['gpa_major']);
		if($row['gpa_major'] == ""){$comp = false; }
		array_push($arr, $row['gpa_scale']);
		if($row['gpa_scale'] == ""){$comp = false; }
        array_push($arr, $row['classRank']);
		array_push($arr, $row['transscriptreceived']);
		array_push($arr, $row['datafile_id']);
		if($row['datafile_id'] == ""){$comp = false; }
		array_push($arr, $row['educationtype']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['gpascalename']);
        array_push($arr, trim($row['college_name'])); // $myInstitutes[23]
        
		array_push($myInstitutes, $arr);
		if(count($myInstitutes) == 0 || $comp == false )
		{   
			updateReqComplete("uni.php", 0,1);
            if ($instituteName && !$designPhdTranscriptOptional) {
                $err .= "You must upload an unofficial transcript for " . $instituteName . ".<br>";
            }
		}
        else
		{
            /*
            * Check INI applications for supporting coursework
            */
            if (isIniDomain($domainid) || isIniKobeDomain($domainid))
            {
                $courseworkRequirementMet = FALSE;
                if (isIniKobeDomain($domainid))
                {
                    $iniKobeSupportingCourseworkQuery = "SELECT id FROM ini_supporting_coursework_kobe
                        WHERE application_id = " . $_SESSION['appid']; 
                    $iniSupportingCourseworkKobeResult = mysql_query($iniKobeSupportingCourseworkQuery);
                    if (mysql_numrows($iniSupportingCourseworkKobeResult) > 0)
                    {
                        $courseworkRequirementMet = TRUE;
                    }   
                }
                else
                {
                    if (iniSupportingCourseworkComplete($_SESSION['appid']))
                    {
                        $courseworkRequirementMet = TRUE;    
                    }   
                }

                if ($courseworkRequirementMet)
                {
                    // Supporting courswork info has been entered, so page is complete
                    updateReqComplete("uni.php", 1,1,0);
                }
                else
                {
                    // Supporting courswork info has not been entered, so page is incomplete.
                    updateReqComplete("uni.php", 0, 1);
                }
            }
            elseif (isEM2Domain($domainid))
            {
                $courseworkRequirementMet = FALSE;
                
                    if (em2SupportingCourseworkComplete($_SESSION['appid']))
                    {
                        $courseworkRequirementMet = TRUE;    
                    }   
                

                if ($courseworkRequirementMet)
                {
                    // Supporting courswork info has been entered, so page is complete
                    updateReqComplete("uni.php", 1,1,0);
                }
                else
                {
                    // Supporting courswork info has not been entered, so page is incomplete.
                    updateReqComplete("uni.php", 0, 1);
                }
            }
            else
            {
                updateReqComplete("uni.php", 1,1,0);    
            }
		}
	}
}//end if not btnSave



// Include the shared page header elements.
$pageTitle = 'Education';
$headJavascriptFiles = array(
    '../inc/prototype.js',
    '../inc/scriptaculous.js'
);
include '../inc/tpl.pageHeaderApply.php';
?>

<br>
<span class="tblItem">
<?
$domainid = 1;
if(isset($_SESSION['domainid']))
{
if($_SESSION['domainid'] > -1)
{
$domainid = $_SESSION['domainid'];
}
}
$sql = "select content from content where name='College/University' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
echo html_entity_decode($row["content"]);
}
?></span><br>
	<span class="errorSubtitle"><?=$err." ";?></span>
    
	<br>

<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
		<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
	</div><span class="tblItem">= required</span><br>
<br>

    <span class="subtitle">
    <? showEditText("Save College/University Info", "button", "btnSave", $_SESSION['allow_edit']); ?>
    </span>
	<? if($myInstitutes[0][1] > -1){ 
	showEditText("Add College/University", "button", "btnAdd", $_SESSION['allow_edit']);
	} ?>
    <br>
<hr size="1" noshade color="#990000">
<a name="colleges"></a>
    
	<?
	for($i = 0; $i < count($myInstitutes); $i++)
	{
		
		$title = "";
		switch($myInstitutes[$i][17])    //das changed 16 to 17
		{
			case "1":
				$title = "Undergraduate";
			break;
			case "2":
				$title = "Graduate";
			break;
			case "3":
				$title = "Additional";
			break;
		
		}//end switch
		
		?>
	<table width="650" border="0" cellpadding="1" cellspacing="1">
      <tr>
        <td><span class="subtitle">
          <?=$title;?> College/University <em><strong><?
			if($myInstitutes[$i][15] > 0) //das 
			{
				echo "(Official transcript received.)";
			}else
			{  
				// Don't show 'You must mail...' for MS-RT or Summer Scholars.
                if (( (isset ($domainid) && $domainid == 38) 
                    || isMsrtDomain($_SESSION['domainid'])
                )  // do not show if INI
                || ((isset ($domainid) && isIniDomain($domainid)) || isIniDomain($_SESSION['domainid']))
                // do not show for REU-SE
                || ((isset ($domainid) && isREUSEDomain($domainid)) || isREUSEDomain($_SESSION['domainid']))
                || ((isset ($domainid) && isSCSDomain($domainid)) || isScsDomain($_SESSION['domainid']))
                || ((isset ($domainid) && isPhilosophyDomain($domainid) || isPhilosophyDomain($_SESSION['domainid']))))
                 {
                     // do not display message  
                }
                else {
                        echo "(You must mail an official transcript.)";
                }
			}
			?>
</strong> </em></span><br>
<span class="tblItem">
<? 
	  if($myInstitutes[$i][17] != 1)   //das changed 16 to 17
	  {
		echo "<strong>Type:</strong>";
		if($_SESSION['allow_edit'] == true)
		{
			showEditText($myInstitutes[$i][17], "radiogrouphoriz", "lbType_".$myInstitutes[$i][0], $_SESSION['allow_edit'],false, $uniTypes);
		}
		else
		{
			for($j = 0; $j < count($uniTypes); $j++)
				{
					if($myInstitutes[$i][17] == $uniTypes[$j][0])   //das changed 16 to 17
					{
						echo $uniTypes[$j][1];
						break;
					}
				}
		}
		
	  }
	  ?></span></td>
       <td align="right">
		<? showEditText("Delete", "button", "btnDel_".$myInstitutes[$i][0], $_SESSION['allow_edit']); ?>
		<input name="txtType_<?=$myInstitutes[$i][0];?>" type="hidden" id="txtType_<?=$myInstitutes[$i][0];?>" value="<?=$myInstitutes[$i][17];   // das changed 16 to 17?>">
		<input name="txtId_<?=$myInstitutes[$i][0];?>" type="hidden" id="txtId_<?=$myInstitutes[$i][0];?>" value="<?=$myInstitutes[$i][0];?>">

		<input name="txtDatafile_<?=$myInstitutes[$i][0];?>" type="hidden" id="txtDatafile_<?=$myInstitutes[$i][0];?>" 
        value="<?=$myInstitutes[$i][16];  //das ?>">
		<input name="txtModdate_<?=$myInstitutes[$i][0];?>" type="hidden" id="txtModdate_<?=$myInstitutes[$i][0];?>" value="<?=$myInstitutes[$i][18];  //das?>">
		<input name="txtSize_<?=$myInstitutes[$i][0];?>" type="hidden" id="txtSize_<?=$myInstitutes[$i][0];?>" value="<?=$myInstitutes[$i][19];  //das?>">
		<input name="txtExtension_<?=$myInstitutes[$i][0];?>" type="hidden" id="txtExtension_<?=$myInstitutes[$i][0];?>" value="<?=$myInstitutes[$i][20];// das?>">

		</td>
	  </tr>
		</table>
	<table width="650" border="0" cellpadding="3" cellspacing="0" class="tblItem">
		  <tr>
			<td  align="right">
            <strong>
            <?php 
            /*
            if ($isIniDomain) 
            {
            ?>
                Institution:
            <?php    
            }
            else
            {
            ?>
                College/University:    
            <?php
            }
            */
            ?>
            College/University:
            </strong>
            </td>
			<td colspan="5"><? showEditText(trim($myInstitutes[$i][1]), "inst-textbox", 
                                                            "lbName_".$myInstitutes[$i][0], $_SESSION['allow_edit'],
                                                            true, $institutes,true,80); ?></td>
          </tr>
          
      <?php
      /*
      if ($isIniDomain) 
      {
      ?>
         <tr>
            <td  align="right">
            <strong>Affiliated College/School:
            </strong>
            </td>
            <td colspan="5">
            <?php 
            showEditText(trim($myInstitutes[$i][23]), "textbox", "lbCollegeName_".$myInstitutes[$i][0], $_SESSION['allow_edit'], TRUE, $institutes, true, 80);
            ?>
            </td>
          </tr>
      <?php    
      }
        */
      ?>
<!-- NEW ROW -->
                          <tr>
			<td  align="right"><strong>Graduation Date 
			<? if($myInstitutes[$i][17] == 3){ //das changed 16 to 17?>    
			or Date left
			<? } ?></strong><strong>:</strong></td>
			<td><? showEditText($myInstitutes[$i][3], "textbox", "txtDateG_".$myInstitutes[$i][0], 
                                                $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>
			<td align="right"><strong>Date Entered:</strong></td>
			<td><? showEditText($myInstitutes[$i][2], "textbox", "txtDateE_".$myInstitutes[$i][0], 
                                                $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
<!-- NEW ROW -->
		  <tr>
			<td  align="right"><strong>Degree Earned</strong><strong>:</strong></td>
			<td><? 
			if($_SESSION['allow_edit'] == true)
			{
				showEditText($myInstitutes[$i][5], "listbox", "lbDegree_".$myInstitutes[$i][0], $_SESSION['allow_edit'],true, $degrees); 
			}else
			{
				echo $myInstitutes[$i][21];  //das
			}
			?></td>
			<td align="right">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
<!-- NEW ROW -->				 
		  <tr>
			<td align="right"><strong>Major:</strong></td>
			<td><? showEditText($myInstitutes[$i][6], "textbox", "txtMajor1_".$myInstitutes[$i][0],
                                                $_SESSION['allow_edit'], true, null,true,20); ?></td>
			<td align="right"><strong>Minor:</strong></td>
			<td><? showEditText($myInstitutes[$i][9], "textbox", "txtMinor1_".$myInstitutes[$i][0],
                                                $_SESSION['allow_edit'], false, null,true,20); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
 			  </tr>
<!-- NEW ROW -->
		   <? if($myInstitutes[$i][17] == "1"){ ///das changed 16 to 17?>
		  <tr>
			<td  align="right"><strong>2nd Major:</strong></td>
			<td><? showEditText($myInstitutes[$i][7], "textbox", "txtMajor2_".$myInstitutes[$i][0],
                                                $_SESSION['allow_edit'], false, null,true,20); ?></td>
			<td align="right"><strong>2nd minor:</strong></td>
			<td><? showEditText($myInstitutes[$i][10], "textbox", "txtMinor2_".$myInstitutes[$i][0],
                                                $_SESSION['allow_edit'], false, null,true,20); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <? } ?>
<!-- NEW ROW -->

		  <tr>
			<td align="right"><strong>GPA Major:</strong></td>
                            <td><? showEditText($myInstitutes[$i][12], "textbox", 
                                                "txtGPAMajor_".$myInstitutes[$i][0], 
                                                $_SESSION['allow_edit'], true); ?></td>
			<td align="right"><strong>GPA Overall:</strong></td>
		            <td><? showEditText($myInstitutes[$i][11], "textbox", "txtGPA_".$myInstitutes[$i][0],
                                                $_SESSION['allow_edit'], true); ?></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
                         </tr>
<!-- NEW ROW -->
                         <tr>
                      
                           <td align="right"><strong>GPA Scale: </strong></td>
           	           <td><? 
					   if($_SESSION['allow_edit'] ==  true)
					   {
							showEditText($myInstitutes[$i][13], "listbox", "lbScale_".$myInstitutes[$i][0],$_SESSION['allow_edit'],true, $scales); 
					   }else
					   {
							echo $myInstitutes[$i][22];  //das
					   }
					   
					   ?>
                       <? if ($domainid ==1) { ?>
                                </td>
				                <td align="right"><strong>Class Rank:</strong></td>
				                <td><? showEditText($myInstitutes[$i][14], "textbox", "txtClassRank_".$myInstitutes[$i][0],
                                                $_SESSION['allow_edit'], false, null,true,30); ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>

                                                
                                            <?php }  else {  ?> 
			<td>&nbsp;</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <?php } ?>
 			  </tr>
              
<!-- NEW ROW -->
		  <tr>
			<td colspan="4">
			</td></tr>
		  <tr>
            <td colspan="4" valign="middle"> 
		   <?
		   if((marray_search($myInstitutes[$i][1], $institutes) > -1) ||
                (marray_search(html_entity_decode ($myInstitutes[$i][1]), $institutes) > -1))
		   {
				$received = "Not Received";
				$recDate = "";
				$qs = "?id=".$myInstitutes[$i][0];
				if($myInstitutes[$i][16] > 0)   //das
				{
				
					$received = "Received.";
					$recDate = "Last updated: ". $myInstitutes[$i][18];  //das
				}
				//echo $received;
				?>
				
				
				<? 
				if($myInstitutes[$i][0] > -1)
				{
					if($_SESSION['allow_edit'] == true)
					{ 
						?><input class="tblItem" name="btnUpload" value="Upload Unofficial Transcript" type="button" onClick="parent.location='transcripts.php<?=$qs;?>'"><? 
					}//end if allowedit
					if($recDate!= "") 
					{
						showFileInfo("transcript.".$myInstitutes[$i][20], $myInstitutes[$i][19], formatUSdate2($myInstitutes[$i][18]), 
						getFilePath(1, $myInstitutes[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'],$myInstitutes[$i][16]));   //das changed 15 to 16 and 17 to 18 and 19 to 20
					} 
				
				}
			}//end if name
			?>				      				
			  
			  </td>
		  </tr>
	</table>
		<hr size="1" noshade color="#990000">
	<? 
		
	}//end for 
	
    
/*
* DIETRICH/INI
* Supporting educational data section for INI
*/
if ($isIniDomain)
{
    include('../inc/iniSupportingCoursework.inc.php');
}
if ($isEM2Domain)
{
    include('../inc/em2SupportingCoursework.inc.php');
}
?>


<span class="tblItem"><span class="subtitle">
<? showEditText("Save College/University Info", "button", "btnSave", $_SESSION['allow_edit']); ?>
</span></span>
<br>
            
<?php
if (isDesignDomain($domainid))
{
?>
<script src="https://code.jquery.com/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$.noConflict();
(function() {
jQuery(document).ready(function() {
    jQuery('#btnAdd').click(function() {
        jQuery('#form1').attr('action', '#colleges');     
    });
    
    jQuery('#btnDel').click(function() {
        jQuery('#form1').attr('action', '#colleges');     
    });
});
})(jQuery);
</script>
<?php
}

// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>