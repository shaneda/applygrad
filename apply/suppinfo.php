<?php      
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/section_required.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

// Special cases
$isDesignDomain = isDesignDomain($_SESSION['domainid']);
$isDesignPhdDomain = isDesignPhdDomain($_SESSION['domainid']);

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
    {
        $prevPage = $appPageList[$curPageIndex - 1]; 
    }
    else
        {
            $prevPage = "";
        }
if (sizeof ($appPageList) > $curPageIndex + 1)
    {
        $nextPage =  $appPageList[$curPageIndex + 1]; 
    } 
    else
        {
            $nextPage = "";
        }

if(!isset($_POST['btnSave']))
{   
	if ($SUPPINFO_REQUIRED[$domainid] == false ) 
	{
		//SUPPLEMENTAL INFO NOT REQUIRED SO SAVE WITHOUT CHECKING
		updateReqComplete("suppinfo.php", 1, 1, 0);
	} 
    else 
	{   
        // do not automatically set for Statistics-MS or INI
        if (!isIniDomain($domainid) && $domainid != 49 && !isEM2Domain($domainid)) 
        {     
		    $myPrograms = get_user_selected_programs($_SESSION['appid']);
		    /* 
		    if the program is NOT required, then go ahead and
		    save i.e. requirements complete. TARGET_PROGRAM is
		    defined in the section_required.php file. this should
		    be redesigned to be more generic and dynamic
		    */
		    if( marray_search( $TARGET_PROGRAM, $myPrograms) == false )
		    { 
			    //THIS PROGRAM DOES NOT REQUIRE CHECKING
			    updateReqComplete("suppinfo.php", 1, 1, 0);
		    }
	    }
    }
}

// DAS added resume stuff for statistics

$resName = "";
$resSize = 0;
$resModDate = "";
$resumeFileId = -1;

//RETRIEVE USER INFORMATION
$sql = "SELECT id,moddate,size,extension FROM datafileinfo where user_id=".$_SESSION['userid'] . " and section=2";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
    $resName  = "resume.".$row['extension'];
    $resSize  = $row['size'];
    $resumeFileId = $row['id'];
    $resModDate = $row['moddate'];
} 
//  DAS end of additions

$err = "";
$fellowshipsError = '';
$applyingToLtiProgram = applyingToLtiProgram();

$myPrograms = array();

$pubStatus = array(
    array('In Progress', 'In Progress'), 
    array('Submitted','Submitted'), 
    array('Accepted','Accepted'),
	array('Not Accepted','Not Accepted'), 
    array('No Plans to Submit','No Plans to Submit')
);

$pubTypes = array(
    array('Full Paper', 'Full Paper'), 
    array('Short Paper','Short Paper'), 
    array('Poster','Poster'),
    array('Other','Other')
);

$fellowshipStatus = array(
    array('Applied For', 'Applied For'), 
    array('Awarded','Awarded')
);

$vlisReferrals = array(
    array("Website","Website"),
    array("Alumni","Alumni"),
    array("Friend","Friend"),
    array("Current Student","Current Student"),
    array("Former Faculty","Former Faculty"),
    array("Former Faculty/Educational Institution","Former Faculty/Educational Institution"),
    array("Advertisement","Advertisement-Please explain"),
    array("Other","Other-Please explain")
);

$mseMsitReferrals = array(
    array("Web", "Web"),
    array("Friend", "Friend / Co-Worker"), 
    array("Alumni", "Alumni"),
    array("Sponsor", "Company Sponsor"),
    array("Other", "Other-Please explain")
);

$arrCrossDeptProgs = array(
    array("0","Biological Sciences at CMU"),
    array("1","Biomedical Engineering at CMU"),
    array("2","Computer Science at CMU"),
    array("3","Machine Learning at CMU"),
    array("4","Psychology at CMU"),
    array("5","Robotics at CMU"),
    array("6","Statistics at CMU"),
    array("7","Bioengineering at Pitt"),
    array("8","Center for Neuroscience at Pitt"),
    array("9","Mathematics at Pitt"),
    array("10","Psychology at Pitt"), 
    array("11","Electrical and Computer Engineering at CMU"),
    array("12","Other")
);

$crossDeptProgs = array();
$crossDeptProgsOther = "";
$CrossDeptProgList = false;
$RecPermission = false;
$pubs = array();
$fellows = array();
$courses = array();
$advisors = array();//FACULTY DESIGNATED AS ADVISORS
$advisor1 = "";
$advisor2 = "";
$advisor3 = "";
$advisor4 = "";
$expResearch = "";
$expInd = "";
$exFileId = "";
$exFileDate = "";
$exFileSize = "";
$exFileExt = "";
$pier = 0;
$cups = 0;
$wFellow = 0;
$port = "NULL";
$portLink = "";
$videoEssayLink = filter_input(INPUT_POST, 'txtVideoEssayLink', FILTER_SANITIZE_STRING);
$videoEssayCode = filter_input(INPUT_POST, 'txtVideoEssayCode', FILTER_SANITIZE_STRING);
$prog = "";
$design = "";
$stats = "";
$enrolled = 0;
$referral = ""; 
$honors = "";
$otherUni = "";
$tmpCrossDeptProgsId = "";
$permission= 0;
$previousCoursework = 0;

//GET ADVISORS
$sql = "select lu_users_usertypes.id as uid, concat(users.lastname , ', ' , users.firstname) as name,
users_info.additionalinfo
from lu_users_usertypes
left outer join users on users.id = lu_users_usertypes.user_id
left outer join users_info on users_info.user_id = lu_users_usertypes.id
left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id 
where lu_users_usertypes.usertype_id=8 and lu_domain_department.domain_id=".$_SESSION['domainid']." group by users.id
order by lastname, firstname";
$result = mysql_query($sql) or die(mysql_error());
//echo $sql;
$tmpTitle = "";
$title = "";
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	/*
	$tmpTitle = $row['additionalinfo'];
	if($tmpTitle == $title)
	{
		array_push($arr, $row['uid']);
		array_push($arr, $row['name']);
	}else
	{
		array_push($arr, -1);
		array_push($arr, "-- ".$row['additionalinfo']." --");
		$title = $tmpTitle;
	}
	*/
	array_push($arr, $row['uid']);
	array_push($arr, $row['name']);
	
	array_push($advisors, $arr);
}
             
//HANDLE DELETIONS
$dataSavedAfterDeletions = FALSE;
if( isset( $_POST['txtPost'] ) 
    && !isset($_POST['btnAddPub']) 
    && !isset($_POST['btnAddFellow']) 
    && !isset($_POST['btnAddCourse']) )
{
	$vars = $_POST;
	$itemId = -1;
	foreach($vars as $key => $value)
	{
		if( strstr($key, 'btnDelPub') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];

			$sql = "DELETE FROM publication WHERE application_id=".$_SESSION['appid']. " and id=" . $itemId;
			mysql_query($sql) or die(mysql_error());
		}
        
		if( strstr($key, 'btnDelFellow') !== false )
		{
			$arr = split("_", $key);
			$itemId = $arr[1];
            $awardletterDatafileid = '';
            
            if($itemId != "")
            {
                $awardletterSelectSql = "SELECT datafile_id FROM fellowships 
                        WHERE application_id = " . $_SESSION['appid'] . " AND id = " . intval($itemId);
                $awardletterSelectResult = mysql_query($awardletterSelectSql);
                while($row = mysql_fetch_array( $awardletterSelectResult )) 
                {
                    $awardletterDatafileid = $row['datafile_id'];
                }
                if($awardletterDatafileid != "")
                {
                    $awardletterDeleteSql = "DELETE FROM datafileinfo where id = " . $awardletterDatafileid;
                    mysql_query($awardletterDeleteSql);
                }
            
			    $fellowshipDeleteSql = "DELETE FROM fellowships 
                    WHERE application_id = " . $_SESSION['appid'] . " AND id = " . intval($itemId);
			    mysql_query($fellowshipDeleteSql);
            }
		}
        
        if( strstr($key, 'btnDelCourse') !== false )
        {
            $arr = split("_", $key);
            $itemId = $arr[1];

            $sql = "DELETE FROM sem_previous_course WHERE application_id=".$_SESSION['appid']
                . " and sem_previous_course_id = " . $itemId;
            mysql_query($sql) or die(mysql_error());
        }
        
	}//END FOR
    
	saveData();
    $dataSavedAfterDeletions = TRUE;
    
}//END DELETIONS

if(isset($_POST['btnAddPub']))
{
	saveData();
	$sql = "insert into publication(application_id)values(".$_SESSION['appid'].")";
	$result = mysql_query($sql) or die(mysql_error());
}//END ADD PUB
if(isset($_POST['btnAddFellow']))
{
	saveData();
	$sql = "insert into fellowships(application_id)values(".$_SESSION['appid'].")";
	$result = mysql_query($sql) or die(mysql_error());
}//END ADD FELLOW

if(isset($_POST['btnAddCourse']))
{
    saveData();
    $sql = "insert into sem_previous_course(application_id)values(".$_SESSION['appid'].")";
    $result = mysql_query($sql) or die(mysql_error());
}//END ADD FELLOW

if(isset($_POST['btnSave']) && !$dataSavedAfterDeletions)
{
	saveData();
}

function saveData()
{
    global $err;
    global $fellowshipsError;
    global $myPrograms;
    global $applyingToLtiProgram;
    global $TARGET_PROGRAM; /* defined in section_required.php temp file */
    global $pubStatus;
    global $fellowshipStatus;
    global $vlisReferrals;
    global $mseMsitReferrals;
    global $pubs;
    global $fellows;
    global $courses;
    global $advisors;//FACULTY DESIGNATED AS ADVISORS
    global $advisor1;
    global $advisor2;
    global $advisor3;
    global $advisor4;
    global $expResearch;
    global $expInd;
    global $exFileId;
    global $exFileDate;
    global $exFileSize;
    global $exFileExt;
    global $pier;
    global $wFellow;
    global $cups;
    global $port;
    global $portLink;
    global $videoEssayLink;
    global $videoEssayCode;
    global $prog;
    global $design;
    global $stats;
    global $enrolled;
    global $referral;
    global $honors;
    global $otherUni;
    global $arrCrossDeptProgs;
    global $crossDeptProgs;
    global $permission;
    global $CrossDeptProgList;
    global $RecPermission ;
    global $crossDeptProgsOther;
    global $tmpCrossDeptProgsId;
           $tmpCrossDeptProgsId = "";
    global $tmpCrossDeptProgs;
           $tmpCrossDeptProgs = "";
    global $previousCoursework;

	//VERIFY DATA
	$arrPubId = array();
	$arrFellowId = array();
    $arrCourseId = array();
	$tmpType="";
	$itemId = -1;
	$vars = $_POST;
	foreach($vars as $key => $value)
	{
		//echo $key ." ". $value."<br>";
		$arr = split("_", $key);
		if($arr[0] == "txtPub" || $arr[0] == "txtFellow" || $arr[0] == "txtCourse")
		{
			$tmpid = $arr[1];
			if($itemId != $tmpid || $tmpType != $arr[0])
			{
				$itemId = $tmpid;
				$tmpType=$arr[0];
				
				if($arr[0] == "txtPub")
				{
					array_push($arrPubId,$itemId);
				} elseif ($arr[0] == "txtFellow") {
					array_push($arrFellowId,$itemId);
				} else {
                    array_push($arrCourseId,$itemId);    
                }
			}
		}
		if( strstr($key, 'chkCrossDeptProgs') !== false )
		{
			$k = $arr[1];
			$arr = array();

                        /* There are cross department & programs chosen */
                        $CrossDeptProgList = true;

			array_push($arr, $arrCrossDeptProgs[$k][0]);
			array_push($arr, $arrCrossDeptProgs[$k][1]);
			array_push($crossDeptProgs, $arr);
			/*
			if($tmpCrossDeptProgs != "")
			{
				$tmpCrossDeptProgs .= ",";
				$tmpCrossDeptProgsId .= ",";
			}
			$tmpCrossDeptProgsId .= $arrCrossDeptProgs[$k][0];
			$tmpCrossDeptProgs .= $arrCrossDeptProgs[$k][1];
			*/
		}
	}//END FOR each key
	if($tmpCrossDeptProgs != "")
	{
		//$crossDeptProgs = $tmpCrossDeptProgs;
                $CrossDeptProgList = true;

	}
	//ITERATE THROUGH IDS AND DO UPDATES
	for($i = 0; $i < count($arrPubId); $i++)
	{
		$title= addslashes($_POST["txtTitle_".$arrPubId[$i]]);
		$author= addslashes($_POST["txtAuthor_".$arrPubId[$i]]);
		$forum= addslashes($_POST["txtForum_".$arrPubId[$i]]);
		$citation= addslashes($_POST["txtCitation_".$arrPubId[$i]]);
		$url= addslashes($_POST["txtUrl_".$arrPubId[$i]]);
		$status= addslashes($_POST["lbPubStatus_".$arrPubId[$i]]);
        $type = addslashes($_POST["lbPubType_".$arrPubId[$i]]);
        $typeOther = addslashes($_POST["txtPubTypeOther_".$arrPubId[$i]]);

		if($title == "")
		{
			$err .= "ERROR: Title ". ($i+1) ." is required.<br>";
		}
		if($author == "")
		{
			$err .= "ERROR: Author ". ($i+1) ." is required.<br>";
		}

		if($err == "")
		{
			$sql = "update publication set
			title= '".$title."',
			author= '".$author."',
			forum= '".$forum."',
			citation= '".$citation."',
			url= '".$url."',
			status= '" . $status . "',
            type = '" . $type . "',
            type_other = '" . $typeOther . "'
			where id = ".$arrPubId[$i]." and application_id=".$_SESSION['appid'];
			mysql_query($sql) or die(mysql_error());
		}
	}//END PUBS UPDATE

    for($i = 0; $i < count($arrFellowId); $i++)
	{
		$name= addslashes($_POST["txtName_".$arrFellowId[$i]]);
		$amount= addslashes($_POST["txtAmount_".$arrFellowId[$i]]);
		$status= addslashes($_POST["lbFellowStatus_".$arrFellowId[$i]]);
		$awDate = addslashes($_POST["txtAwDate_".$arrFellowId[$i]]);
		$appDate = addslashes($_POST["txtAppDate_".$arrFellowId[$i]]);
		$duration = intval($_POST["txtfellowLen_".$arrFellowId[$i]]);

		if($name == "" && !isset($_POST['btnDelFellow_' . $arrFellowId[$i]]) )
		{
			$err .= "ERROR: Fellowship Name" . ($i+1) ." is required.<br>";
		}
		if($status == "")
		{
			//$err .= "ERROR: Status ". ($i+1) ." is required.<br>";
		}
		if($appDate != "" && $appDate != "00/0000" && check_Date2($appDate) === false)
		{
			$err .= "ERROR: Application date is invalid.<br>";
		}
		if($awDate != "" && $awDate != "00/0000" && check_Date2($awDate) === false)
		{
			$err .= "ERROR: Award date is invalid.<br>";
		}

		if($err == "")
		{
			$sql = "update fellowships set
			name= '".$name."',
			amount= '".$amount."',
			status= '".$status."',
			duration=".$duration. " ";
			if($appDate != "" && $appDate != "00/0000" && check_Date2($appDate) !== false)
			{
				$sql .= ", applied_date = '".formatMySQLdate2($appDate,'/','-')."' ";
			}
			if($awDate != "" && $awDate != "00/0000" && check_Date2($awDate) !== false)
			{
				$sql .= ", award_date='".formatMySQLdate2($awDate,'/','-')."' ";
			}
			$sql .= " where id = ".$arrFellowId[$i]." and application_id=".$_SESSION['appid'];
			mysql_query($sql);
            
            // Notify LTI award letter requirement
            if ($applyingToLtiProgram && $status == 'Awarded')
            {
                $awardLetterUploaded = FALSE;
                $awardletterSql = "SELECT datafile_id FROM fellowships where id = " . $arrFellowId[$i];
                $awardletterResult = mysql_query($awardletterSql);
                while($row = mysql_fetch_array($awardletterResult)) 
                {
                    if($row['datafile_id'] != NULL)
                    {
                        $awardLetterUploaded = TRUE;
                    }
                }
                
                if (!$awardLetterUploaded && !isset($_POST['btnDelFellow_' . $arrFellowId[$i]]))
                {
                    updateReqComplete("suppinfo.php", 0,1);
                    $fellowshipsError .= 'You must upload an award letter for fellowship: ' . htmlspecialchars($name) . '<br>';        
                }
            }
        }
	}//END FELLOWSHIPS UPDATE

    for($i = 0; $i < count($arrCourseId); $i++)
    {
        $courseName = addslashes($_POST["txtCourseName_".$arrCourseId[$i]]);
        $courseYear = intval($_POST["txtCourseYear_".$arrCourseId[$i]]);

        if($courseName == "")
        {
            $err .= "ERROR: Course Name ". ($i+1) ." is required.<br>";
        }
        if($courseYear == "")
        {
            $err .= "ERROR: Course Year ". ($i+1) ." is required.<br>";
        }

        if($err == "")
        {
            $sql = "update sem_previous_course set
            course_name = '".$courseName."',
            course_year = '".$courseYear."'
            where sem_previous_course_id = ".$arrCourseId[$i]
            ." and application_id=".$_SESSION['appid'];
            mysql_query($sql) or die(mysql_error());
        }
    }//END courses UPDATE
    
	$wFellow = 0;
	$pier = 0;
    $cups = 0;

	if(isset($_POST['chkWomen']))
	{
		$wFellow = 1;
	}
	if(isset($_POST['chkPier']))
	{
		$pier = 1;
	}
    if(isset($_POST['chkCups']))
    {
        $cups = 1;
    }
	if(isset($_POST['chkPort']))
	{
		$port = 1;
	}
	if(isset($_POST['txtPort']))
	{
		$portLink = htmlspecialchars(addslashes($_POST['txtPort']));
	}
	if(isset($_POST['txtProg']))
	{
		$prog = htmlspecialchars($_POST['txtProg']);
	}
	if(isset($_POST['txtDesign']))
	{
		$design = htmlspecialchars($_POST['txtDesign']);
	}
	if(isset($_POST['txtStats']))
	{
		$stats = htmlspecialchars($_POST['txtStats']);
	}
	
    if(isset($_POST['lbReferral']))
	{
		if ($_POST['lbReferral'] != "Other-Please explain" 
            && $_POST['lbReferral'] != "" 
            && $_POST['lbReferral'] != "Other"
            && $_POST['lbReferral'] != 'Graduate school search site') // Dietrich "please specify"
		{
			$referral = addslashes($_POST['lbReferral']);
		}
        else
		{	
			if(isset( $_POST['txtReferral']))
			{
				$referral = addslashes(substr ( $_POST['txtReferral'], 0 , 255 ) );
			}
		}
	}
	else
	{
		if(isset( $_POST['txtReferral']))
		{
			$referral = addslashes(substr ( $_POST['txtReferral'], 0 ,255 ) );
		}
	}
	
	if(isset( $_POST['lbEnrolled']))
	{
		$enrolled = intval($_POST['lbEnrolled']);
	}
	
	if(isset( $_POST['txtHonors']))
	{
		$honors = addslashes($_POST['txtHonors']);
	}
	
	if(isset($_POST['txtOtherUni']))
	{
		$otherUni = htmlspecialchars(addslashes($_POST['txtOtherUni']));
	}
	if(isset($_POST['chkPermission']))
	{
		$permission = 1;
                /* permission to get records has been given by user */
                $RecPermission = true;
	}
    
    if(isset($_POST['chkPreviousCoursework']))
    {
        $previousCoursework = 1;
    }

	if(isset($_POST['txtCrossDeptProgs']))
	{
		$crossDeptProgsOther = addslashes(substr ( $_POST['txtCrossDeptProgs'], 0 ,50 ) );
		//echo $crossDeptProgsOther;
	}
	
	if($_SESSION['domainname'] == "MS-HCII")
	{
		if($prog == "")
		{
			$err .= "ERROR: Programming knowledge is required<br>";
		}
		if($design == "")
		{
			$err .= "ERROR: Design knowledge is required<br>";
		}
		if($stats == "")
		{
			$err .= "ERROR: Statistics knowledge is required<br>";
		}
	}
    
    //   Added for MS Statistics
    if($_SESSION['domainname'] == "Statistics-MS")
    {
        if($prog == "")
        {
            $err .= "ERROR: Prerequisite courses is a required field<br>";
        }
    }
	
	$progs = "";
	for($i = 0; $i < count($crossDeptProgs); $i++)
	{	
		if($i > 0 && $i < count($crossDeptProgs))
		{
			$progs .= ",";
		}
		$progs .= $crossDeptProgs[$i][1];
	}
	$sql = "update application set
			cups = ".$cups.",
            womenfellowship= ".$wFellow.",
			pier = ".$pier.",
			portfoliosubmitted = ".$port.",
			portfolio_link = '".addslashes($portLink)."',
			area1 = '".addslashes($prog)."',
			area2 = '".addslashes($design)."',
			area3 = '".addslashes($stats)."',
			referral_to_program = '".addslashes($referral)."',
			other_inst = '".addslashes($otherUni)."',
			cross_dept_progs = '".addslashes($progs)."',
			cross_dept_progs_other = '".addslashes($crossDeptProgsOther)."',
			records_permission = ".$permission.",
			cur_enrolled = ".$enrolled.",
			honors = '".addslashes($honors)."',
            previous_coursework = '" . $previousCoursework . "'
			where id=".$_SESSION['appid'];
			mysql_query($sql) or die(mysql_error().$sql);

    if($_SESSION['domainname'] == "CompBio" || marray_search( "CompBio", $myPrograms) != false 
        || $_SESSION['domainname'] == "CNBC" || marray_search( "CNBC", $myPrograms) != false 
        || $_SESSION['domainname'] == "CNBC-Grad-Training" || marray_search( "CNBC-Grad-Training", $myPrograms) != false){
	        $sql = "delete from lu_application_advisor where application_id=".$_SESSION['appid'];
	        mysql_query($sql) or die(mysql_error());
    }
	if(isset($_POST['lbAdvisor_1']))
	{
		$advisor1 = intval($_POST['lbAdvisor_1']);
		$type = intval($_POST['txtAdvisorType_1']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor1.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['lbAdvisor_2']))
	{
		$advisor2 = intval($_POST['lbAdvisor_2']);
		$type = intval($_POST['txtAdvisorType_2']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor2.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['lbAdvisor_3']))
	{
		$advisor3 = intval($_POST['lbAdvisor_3']);
		$type = intval($_POST['txtAdvisorType_3']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor3.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['lbAdvisor_4']))
	{
		$advisor4 = stripslashes($_POST['lbAdvisor_4']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, advisor_type)values(".$_SESSION['appid'].",".$advisor4.", ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	
	//HANDLE ADVISOR TEXT BOXES
	if(isset($_POST['txtAdvisor_1']))
	{
		$advisor1 = stripslashes($_POST['txtAdvisor_1']);
		$type = intval($_POST['txtAdvisorType_1']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id, name, advisor_type)values(".$_SESSION['appid'].", null, '".$advisor1."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['txtAdvisor_2']))
	{
		$advisor2 = stripslashes($_POST['txtAdvisor_2']);
		$type = intval($_POST['txtAdvisorType_2']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id,  name, advisor_type)values(".$_SESSION['appid'].",null, '".$advisor2."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['txtAdvisor_3']))
	{
		$advisor3 = stripslashes($_POST['txtAdvisor_3']);
		$type = intval($_POST['txtAdvisorType_3']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id,  name, advisor_type)values(".$_SESSION['appid'].",null, '".$advisor3."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	if(isset($_POST['txtAdvisor_4']))
	{
		$advisor4 = stripslashes($_POST['txtAdvisor_4']);
		$sql = "insert into lu_application_advisor(application_id, advisor_user_id,  name, advisor_type)values(".$_SESSION['appid'].",null, '".$advisor4."', ".$type.")";
		mysql_query($sql) or die(mysql_error());
	}
	
	//EXPERIENCE
	/*
	if(isset($_POST['txtExpResearch']))
	{
		$expResearch = htmlspecialchars($_POST['txtExpResearch']);
	}
	if(isset($_POST['txtExpInd']))
	{
		$expInd = htmlspecialchars($_POST['txtExpInd']);
	}
	*/
	$doInsert = true;
	$sql = "select id from experience where application_id = ".$_SESSION['appid'];
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$doInsert = false;
	}
	if($doInsert == true)
	{
		$sql = "insert into experience(application_id)values(".$_SESSION['appid'].")";
		$result = mysql_query($sql) or die(mysql_error());
	}else{
		/*
		$sql = "update experience set
		research='".$expResearch."',
		industry='".$expInd."'
		where application_id=".$_SESSION['appid'];
		$result = mysql_query($sql) or die(mysql_error());
		*/
	}
    
    /*
    * VIDEO ESSAY
    */
    
    // Check for validation error
    if ($videoEssayCode && !$videoEssayLink)
    {
        $err .= "ERROR: a video essay URL is required when an access code is specified<br>";
    }
    else
    {
        // Check for existing record
        $videoEssaySelectQuery = "SELECT * FROM video_essay WHERE application_id = " . intval($_SESSION['appid']);
        $videoEssaySelectResult = mysql_query($videoEssaySelectQuery);
        $existingVideoEssayRecord = mysql_num_rows($videoEssaySelectResult);
      
        if ($existingVideoEssayRecord)
        {
            if (!$videoEssayLink)
            {
                // Delete the record
                $videoEssayDeleteQuery = "DELETE FROM video_essay WHERE application_id = " . intval($_SESSION['appid']);
                mysql_query($videoEssayDeleteQuery);
            }
            else
            {
                // Update the record
                $videoEssayUpdateQuery = "UPDATE video_essay 
                    SET url = '" . mysql_real_escape_string($videoEssayLink) . "',
                    access_code = '" . mysql_real_escape_string($videoEssayCode) . "'
                    WHERE application_id = " . intval($_SESSION['appid']);
                mysql_query($videoEssayUpdateQuery);
            }
        }
        else
        {
            if ($videoEssayLink)
            {
                // Insert the record
                $videoEssayInsertQuery = "INSERT INTO video_essay (application_id, url, access_code) 
                    VALUES (" . intval($_SESSION['appid']) . ",
                    '" . mysql_real_escape_string($videoEssayLink) . "',
                    access_code = '" . mysql_real_escape_string($videoEssayCode) . "')";
                mysql_query($videoEssayInsertQuery);
            }    
        }
    }
    
    // ??????????????????????????????????????????????????????????????????????????????????
	if(isset($_FILES["file"]))
	{
		if($_FILES["file"]["size"] > 0)
		{
			$ret = handle_upload_file(5, $_SESSION['userid'],$_SESSION['usermasterid'], $_FILES["file"],$_SESSION['appid']."_1" );
			echo $ret;
	
			if(intval($ret) < 1)
			{
				$err .= $ret."<br>";
			}else
			{
				$sql = "update experience set datafile_id =".intval($ret)." where application_id = ".$_SESSION['appid'];
				$result = mysql_query($sql) or die(mysql_error());	
			}
		}
	}
    
	if($err == "" && $fellowshipsError == '')
	{
		updateReqComplete("suppinfo.php", 1);

		/* This is ID for CNBC Graduate Training Program  */
		/* The applicants to this program are required to */
		/* give permission to obtain supprting docs       */
		/* if not given, the application is NOT complete  */
		$myPrograms = get_user_selected_programs($_SESSION['appid']);
		
		if( marray_search( $TARGET_PROGRAM, $myPrograms) != false )
		{ 
			/* program applied too provided? */
			/* permission to get records given? */
			if ($CrossDeptProgList == false || $RecPermission == false) 
			{
				/* reset supplemental section to incomplete */
				updateReqComplete("suppinfo.php", 0);
			}
		}

		//header("Location: home.php");
	}  else {
            updateReqComplete("suppinfo.php", 0, 1, 1);
	}

}//END SAVE

//GET USER INFO
$sql = "select
publication.id,
title,
author,
forum,
citation,
url,
status,
publication.type,
type_other,
datafile_id,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from publication 
LEFT OUTER JOIN datafileinfo ON datafileinfo.id = publication.datafile_id
where application_id=".$_SESSION['appid'] . " order by id";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, stripslashes($row['title']));
	array_push($arr, stripslashes($row['author']));
	array_push($arr, stripslashes($row['forum']));
	array_push($arr, stripslashes($row['citation']));
	array_push($arr, stripslashes($row['url']));
	array_push($arr, stripslashes($row['status']));
    array_push($arr, stripslashes($row['type']));
    array_push($arr, stripslashes($row['type_other']));
    array_push($arr, $row['datafile_id']);              // $arr[9]
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
	array_push($pubs, $arr);
}
$sql = "select fellowships.id,
name,
amount,
status,
applied_date,
award_date,
duration,
datafile_id,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from fellowships 
LEFT OUTER JOIN datafileinfo ON datafileinfo.id = fellowships.datafile_id
where fellowships.application_id=".$_SESSION['appid'];
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, stripslashes($row['name']));
	array_push($arr, stripslashes($row['amount']));
	array_push($arr, stripslashes($row['status']));
	array_push($arr, formatUSdate2(stripslashes($row['applied_date']),'-','/'));
	array_push($arr, formatUSdate2(stripslashes($row['award_date']),'-','/'));
	array_push($arr, stripslashes($row['duration']));
    array_push($arr, $row['datafile_id']); // 7
    array_push($arr, $row['moddate']);
    array_push($arr, $row['size']);
    array_push($arr, $row['extension']);
	array_push($fellows, $arr);
    
    // Notify LTI award letter requirement
    if ($applyingToLtiProgram && $row['status'] == 'Awarded' && $row['datafile_id'] == NULL)
    {
        updateReqComplete("suppinfo.php", 0,1);
        $fellowshipsError .= 'You must upload an award letter for fellowship: ' . htmlspecialchars($row['name']) . '<br>';
    }
}

$sql = "select sem_previous_course_id,
course_name,
course_year
from sem_previous_course where application_id=".$_SESSION['appid'];
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $arr = array();
    array_push($arr, $row['sem_previous_course_id']);
    array_push($arr, stripslashes($row['course_name']));
    array_push($arr, $row['course_year']);
    array_push($courses, $arr);
}

$sql = "select pier, womenfellowship, cups, portfoliosubmitted,portfolio_link, area1,area2,area3,
referral_to_program, other_inst, cross_dept_progs,cross_dept_progs_other, records_permission, 
cur_enrolled, honors, previous_coursework, 
video_essay.url AS video_essay_url, video_essay.access_code AS video_essay_access_code
from application 
LEFT OUTER JOIN video_essay ON application.id = video_essay.application_id
where application.id=".$_SESSION['appid'];
$result = mysql_query($sql) or die(mysql_error());
$tmpProgs = "";
while($row = mysql_fetch_array( $result ))
{
    $cups = $row['cups'];
    $wFellow = $row['womenfellowship'];
	$pier = $row['pier'];
	$port = $row['portfoliosubmitted'];
	$prog = $row['area1'];
	$design = $row['area2'];
	$stats = $row['area3'];
	$portLink = stripslashes($row['portfolio_link']);
    $videoEssayLink = $row['video_essay_url'];
    $videoEssayCode = $row['video_essay_access_code'];
	$referral = stripslashes($row['referral_to_program']);
	$otherUni = stripslashes($row['other_inst']);
	$tmpProgs = $row['cross_dept_progs'];
	$crossDeptProgsOther = $row['cross_dept_progs_other'];
	$permission = $row['records_permission'];
	$enrolled = $row['cur_enrolled'];
	$honors = stripslashes($row['honors']);
    $previousCoursework = $row['previous_coursework'];
}
if($tmpProgs != "")
{
	$crossDeptProgs = explode(",",$tmpProgs);
}
for($i = 0; $i < count($crossDeptProgs); $i++)
{
	$crossDeptProgs[$i] = marray_search( $crossDeptProgs[$i], $arrCrossDeptProgs);
}

$sql = "select advisor_user_id, name from lu_application_advisor where application_id=".$_SESSION['appid'] . " order by id" ;
$result = mysql_query($sql) or die(mysql_error());
$myAdvisors = array();
$k=0;
while($row = mysql_fetch_array( $result ))
{
	if($k == 0)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor1 = $row['name'];
		}else
		{
			$advisor1 = $row['advisor_user_id'];
		}
	}
	if($k == 1)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor2 = $row['name'];
		}else
		{
			$advisor2 = $row['advisor_user_id'];
		}
	}
	if($k == 2)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor3 = $row['name'];
		}else
		{
			$advisor3 = $row['advisor_user_id'];
		}
	}
	if($k == 3)
	{
		if($row['advisor_user_id'] == "")
		{
			$advisor4 = $row['name'];
		}else
		{
			$advisor4 = $row['advisor_user_id'];
		}
	}
	$k++;
}

$myPrograms = get_user_selected_programs($_SESSION['appid']);

// Include the shared page header elements.
$pageTitle = 'Supplemental Information';
include '../inc/tpl.pageHeaderApply.php';

$domainid = 1;
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
$sql = "select content from content where name='Supplemental Information' and domain_id=".$domainid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?>

<br>                            
<span class="errorSubtitle"><?=$err?></span><br>
<span class="errorSubtitle"><?=$fellowshipsError?></span><br>
<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
<div class="tblItemRequired" style="height:10px; width:10px; float:left;  "></div>
</div> = required<br>
<br>
<? showEditText("Save Supplemental Info", "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
<input name="txtPost" type="hidden" id="txtPost" value="1">
<br>
<br>
<hr size="1" noshade color="#990000">

<?php
if ($_SESSION['domainname'] == "RI-SCHOLARS")
{
    include '../inc/rissFunding.inc.php';   
?>
    
    <span class="subtitle">
    <? showEditText("Save Supplemental Info", "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
    </span>
    <br/>
    
    <?php
    include '../inc/tpl.pageFooterApply.php';
    exit;
}                
?>            
     
<?php 
if ($_SESSION['domainname'] == "MS-HCII") { ?>
    <span class="subtitle">Portfolio</span><br>
    <br>
    List URL to a website containing your portfolio if you have a Design background.<br><br>
    <? showEditText($portLink, "textbox", "txtPort", $_SESSION['allow_edit'],false, null, true, 40); ?>
    <br>
    <hr size="1" noshade color="#990000">
<?php  } 


// For MSE-SEM, put "have you taken courses" 1st,
// put "where did you learn about us" 2nd
if ( $domainid == 40) {
?>
    <span class="subtitle">
    <strong>Previous Coursework</strong></span>
    <br><br>
    <div>
    Have you taken previous coursework at CMU or SEI?
    <br>
    <?php 
    showEditText($previousCoursework, "checkbox", "chkPreviousCoursework", $_SESSION['allow_edit'], false); 
    if($_SESSION['allow_edit'] == true)
    {
        echo "Check if yes";
    }
    ?>
    <br><br>
    Please list any previous CMU or SEI coursework you have taken.
    <br><br>
 
<? 
showEditText("Add Course", "button", "btnAddCourse", $_SESSION['allow_edit']);
for($i = 0; $i < count($courses); $i++){ ?>
    <input name="txtCourse_<?=$courses[$i][0];?>" type="hidden" id="txtCourse_<?=$courses[$i][0];?>" value="Course">
    <hr size="1">
    <table width="650" border=0 cellpadding=2 cellspacing=2 class="tblItem">
      <tr>
        <td valign="top"><?=$i+1?></td>
        <td valign="top"><p>Course Name:<br>
          <? showEditText($courses[$i][1], "textbox", "txtCourseName_".$courses[$i][0], $_SESSION['allow_edit'], true,null,true,30); ?>
          <br>
        </p></td>
        <td valign="top"><p>Year:<br>
          <? showEditText(strval($courses[$i][2]), "textbox", "txtCourseYear_".$courses[$i][0], $_SESSION['allow_edit'], true,null,true,4); ?>
          <br>
        </td>
        <td valign="top">
        <? showEditText("Delete", "button", "btnDelCourse_".$courses[$i][0], $_SESSION['allow_edit']); ?>
        </td>
      </tr>
    </table>
<? 
}//END coursework LOOP  
?>
    
    </div>
    <br>
    <hr size="1" noshade color="#990000">

    <span class="subtitle">
    <strong>Where did you learn about the Carnegie Mellon MSE/MSIT programs?</strong></span>
    <span>
    <br>
    <br>
    <? showEditText($referral, "listbox", "lbReferral", $_SESSION['allow_edit'],false, $mseMsitReferrals); ?><br>
    <br>
    <? showEditText($referral, "textbox", "txtReferral", $_SESSION['allow_edit'],false,null,true,35,35); ?>
    If Other, please explain: (35 characters max)<br>
    <br>
    </span>
    <br>
    <hr size="1" noshade color="#990000">
<?php
}
?>
 
<?php
if (!$isDesignDomain && !$isDesignPhdDomain)
{
?>         
<span class="subtitle">Publications</span>
<br>
<br>
List relevant publications, including journal or conference papers (submitted, accepted, or published), 
thesis, or technical reports. If you would like for the admissions committee to have access to a copy 
of the paper, please provide the URL.
<br> 
<br>
<? showEditText("Add Publication", "button", "btnAddPub", $_SESSION['allow_edit']); ?>
<? for($i = 0; $i < count($pubs); $i++){ ?>
	<input name="txtPub_<?=$pubs[$i][0];?>" type="hidden" id="txtPub_<?=$pubs[$i][0];?>" value="Pub">
    <hr size="1">
    <table width="650" border="0" cellpadding="2" cellspacing="2" class="tblItem">
      <tr>
        <td valign="bottom"><?=$i+1?></td>
        <td valign="bottom">Author(s):<br>
          <? showEditText($pubs[$i][2], "textbox", "txtAuthor_".$pubs[$i][0], $_SESSION['allow_edit'], true,null,true,30); ?>
          <br>
        </td>
        <td valign="bottom">Title:<br>
          <? showEditText($pubs[$i][1], "textbox", "txtTitle_".$pubs[$i][0], $_SESSION['allow_edit'], true,null,true,20, 200); ?>
          <br>
        </td>
        <td valign="bottom">Journal or Conference:<br>
        <? showEditText($pubs[$i][3], "textbox", "txtForum_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,20, 200); ?></td>
        <td rowspan="2" valign="top" align="right"><? showEditText("Delete", "button", "btnDelPub_".$pubs[$i][0], $_SESSION['allow_edit']); ?></td>
      </tr>

      <tr>
        <td valign="bottom">&nbsp;</td>
        <td valign="bottom">Citation: (vol., pp., year)<br>
            <? showEditText($pubs[$i][4], "textbox", "txtCitation_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,30); ?>
        </td>
        <td valign="bottom">URL (If available):<br>
            <? showEditText($pubs[$i][5], "textbox", "txtUrl_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,20,250); ?>
        </td>
        <td valign="bottom">Status:<br>
        <? showEditText($pubs[$i][6], "listbox", "lbPubStatus_".$pubs[$i][0], $_SESSION['allow_edit'] || $_SESSION['allow_edit_late'] ,false, $pubStatus); ?></td>
      </tr>

      <tr>
        <td valign="bottom">&nbsp;</td>
        <td valign="bottom">Type:<br>
            <? showEditText($pubs[$i][7], "listbox", "lbPubType_".$pubs[$i][0], $_SESSION['allow_edit'] || $_SESSION['allow_edit_late'] ,false, $pubTypes); ?>
        </td>
        <td valign="bottom" colspan="2">Type (If other):<br>
            <? showEditText($pubs[$i][8], "textbox", "txtPubTypeOther_".$pubs[$i][0], $_SESSION['allow_edit'],false,null,true,20,250); ?>
        </td>
      </tr>
      
      <?php 
      if(isSdsDomain($domainid) && $pubs[$i][0] > -1)
      {
      ?>
        <tr>
            <td colspan="5">
            <?php
            if($pubs[$i][1] != '' && $_SESSION['allow_edit'] == true)
            {
                $qs = "?id=" . $pubs[$i][0] . "&t=26"; 
            ?>
                <input class="tblItem" name="btnUpload" value="Upload Publication" type="button" 
                    onClick="parent.location='fileUpload.php<?php echo $qs; ?>'">
            <?php 
            }
            if($pubs[$i][10] != "") 
            {
                showFileInfo("publication" . ($i + 1)  . "." . $pubs[$i][12], $pubs[$i][11], formatUSdate2($pubs[$i][10]), 
                    getFilePath(26, $pubs[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $pubs[$i][9]));
            }    
            ?>
            </td>
        </tr>
      <?php
      }
      ?>
      
    </table>
<? }//END PUBS LOOP
?>
    <br>
    <hr size="1" noshade color="#990000">
<?php 
}             
?>

<? if($_SESSION['domainname'] != "VLIS" && $domainid != 49
&& !isMsrtDomain($_SESSION['domainid']) && !isMITSDomain($_SESSION['domainid'])
&& !isPhilosophyDomain($_SESSION['domainid'])
&& !isIniDomain($_SESSION['domainid'])
&& !isEM2Domain($_SESSION['domainid'])
&& !$isDesignDomain
&& !$isDesignPhdDomain)
{ 
    if (isModernLanguageMaDomain($_SESSION['domainid']))
    {   ?>
        <span class="subtitle">Scholarships</span><br>
    
        <br>
        List only those scholarships which you are applying for or have been awarded for your graduate studies. <br>
        Do not list scholarshipsships that were awarded for undergraduate studies.<br>
        <BR>
        <? showEditText("Add Scholarship", "button", "btnAddFellow", $_SESSION['allow_edit']); ?><br>
        
<?php    }
    else 
    {
?>
    <span class="subtitle">Fellowships</span><br>
    
    <br>
    List only those fellowships which you are applying for or have been awarded for your graduate studies. <br>
    Do not list fellowships that were awarded for undergraduate studies.<br>
    <BR>
	<? showEditText("Add Fellowship", "button", "btnAddFellow", $_SESSION['allow_edit']); 
    }   ?><br> 
    
    <? for($i = 0; $i < count($fellows); $i++){ ?>
    <input name="txtFellow_<?=$fellows[$i][0];?>" type="hidden" id="txtFellow_<?=$fellows[$i][0];?>" value="Fellow">
    <hr size="1">
	<!--BEGIN FELLOWSHIPS TABLE-->
    <table width="650" border=0 cellpadding=2 cellspacing=2 class="tblItem">
      <tr>
        <td valign="top"><?=$i+1?></td>
        <td valign="top"><p> <?php if (isModernLanguageMaDomain($_SESSION['domainid'])) { ?>Scholarship <?php } else { ?>Fellowship<?php } ?> Name:<br>
          <? showEditText($fellows[$i][1], "textbox", "txtName_".$fellows[$i][0], $_SESSION['allow_edit'],true,null,true,30); ?>
          <br>
        </td>
        <td valign="bottom">$</td>
        <td valign="top"><p>Amount/Year (If available):<br>
        <? showEditText($fellows[$i][2], "textbox", "txtAmount_".$fellows[$i][0], $_SESSION['allow_edit']); ?>
        </td>
          <td valign="top">Duration of Award (years): <br>
			<? showEditText($fellows[$i][6], "textbox", "txtfellowLen_".$fellows[$i][0], $_SESSION['allow_edit']); ?>                  </td>
        <td rowspan="2" valign="top" align="right"><? showEditText("Delete", "button", "btnDelFellow_".$fellows[$i][0], $_SESSION['allow_edit']); ?></td>
      </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top"><p>Status:<br>
          <? showEditText($fellows[$i][3], "listbox", "lbFellowStatus_".$fellows[$i][0], $_SESSION['allow_edit'],false, $fellowshipStatus); ?></td>
        <td valign="top">&nbsp;</td>
        <td valign="top">Applied Date <font size="1">(MM/YYYY)</font>: <br>
          <? showEditText($fellows[$i][4], "textbox", "txtAppDate_".$fellows[$i][0], $_SESSION['allow_edit']); ?></td>
          <td valign="top">Award Date <font size="1">(MM/YYYY)</font>: <br>
          <? showEditText($fellows[$i][5], "textbox", "txtAwDate_".$fellows[$i][0], $_SESSION['allow_edit']); ?></td>
      </tr>
      
      <tr>
        <td colspan="5">
        
        
        </td>
      </tr>
      
      <?php 
      if($fellows[$i][0] > -1)
      {
      ?>
        <tr>
            <td colspan="5">
            <?php
            if($fellows[$i][1] != '' && $_SESSION['allow_edit'] == true)
            {
                $qs = "?id=" . $fellows[$i][0] . "&t=22"; 
            ?>
                <input class="tblItem" name="btnUpload" value="Upload Award Letter" type="button" onClick="parent.location='fileUpload.php<?php echo $qs; ?>'">
            <?php 
            }
            if($fellows[$i][8] != "") 
            {
                showFileInfo("awardletter.".$fellows[$i][10], $fellows[$i][9], formatUSdate2($fellows[$i][8]), 
                    getFilePath(22, $fellows[$i][0], $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid'], $fellows[$i][7]));
            }    
            ?>
            </td>
        </tr>
      <?php
      }
      ?>    
                               
    </table>
	<!-- END FELLOWSHIPS TABLE-->
    
<?php    
    } //END FELLOWS LOOP 
?>
    <br>
    <hr size="1" noshade color="#990000">
<?php
} ?>



<?
if($_SESSION['domainname'] == "SCS" 
|| marray_search( "SCS", $myPrograms) != false 
|| $_SESSION['domainname'] == "RI"
|| marray_search( "RI", $myPrograms) != false 
){ 
    ?>
    <span class="subtitle">Program in Interdisciplinary Educational Research (PIER)<br>
    </span><br>
    <strong>PIER</strong> is a CMU program to train scientists to do rigorous research needed for 
    evidence-based educational practice and policy. Note that a Supplementary 
    Application for Admission is required. Further information can be found at
    <a href="http://www.cmu.edu/pier/" target="_blank">http://www.cmu.edu/pier/</a>. 
    <br><br>
    Do you plan to apply to the Program in Interdisciplinary Educational Research (PIER)?            
    <br><br>            
    <? showEditText($pier, "checkbox", "chkPier", $_SESSION['allow_edit'],false); 
	if($_SESSION['allow_edit'] == true)
	{
		echo "Check if yes";
	}?>

    <br>
    <br> 
    <hr size="1" noshade color="#990000">
    
    <span class="subtitle">Portfolio <br>
    </span><br>
    <strong>Human Computer Interaction and METALS</strong> applicants only: 
    If appropriate to your background you may include a link to your portfolio. 
    <br>
    <br>
    Do you plan to submit a portfolio?<br>            
    <br>
    <? showEditText($port, "checkbox", "chkPort", $_SESSION['allow_edit'],false); 
	if($_SESSION['allow_edit'] == true)
	{
		echo "Check if yes";
	}?>
    <br>
    <br>
    Please provide a link to your portfolio (optional)<br>
    <? showEditText($portLink, "textbox", "txtPort", $_SESSION['allow_edit'],false, null, true, 40); ?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<? }//end scs ?>

<? if($_SESSION['domainname'] == "SCS" 
|| marray_search( "SCS", $myPrograms) != false  
|| $_SESSION['domainname'] == "RI"
|| marray_search( "RI", $myPrograms) != false ){ ?>
			
    <span class="subtitle">Experience</span>
    <br>
    <br>
    <strong>Software Engineering</strong> and/or 
    <strong>Societal Computing</strong> applicants only:
    Details related to your Research Experience and/or your Industry/Government Experience must be submitted in PDF. 
    Any other format will not be accepted. The maximum file size is 7MB.
    <br>
    <br>
    Research Experience<br>
    <? $qs = "?id=1&t=5"; 
    if($_SESSION['allow_edit'] == 1)
    {
	    ?>
	    <input class="tblItem" name="btnUpload" value="Upload Research Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
	    <?
    }
    $ex = getExperience($_SESSION['appid'], 1);
    for($i = 0; $i < count($ex); $i++)
    {
	    
	    showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }?>
    <br>
               <hr size="1">
               
    Industry / Government Experience
    <br>
    <? $qs = "?id=2&t=5"; 
    if($_SESSION['allow_edit'] == 1)
    {
	    ?>
	    <input class="tblItem" name="btnUpload" value="Upload Industry / Government Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
	    <?
    }
    $ex = getExperience($_SESSION['appid'], 2);
    for($i = 0; $i < count($ex); $i++)
    {
	    showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 2, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
    
    
    <span class="subtitle">Video Essay</span>
    <p>
    <b>LTI Applicants (optional, but strongly recommended):</b> Prepare a 1-2 minute video that lets the 
    admission committee know who you are, what you do well, and what you are
    looking to achieve in your graduate study at the LTI. Please note
    that the video (and audio) must be of the applicant (not a proxy). If there
    is more than one person in the video, please clarify which person is the
    applicant. What you do or say is totally up to you as long as you provide
    the information requested. If all else is equal, an applicant with a video
    essay will have an advantage over an otherwise identical applicant
    without a video essay.
    </p>
    
    <p>Upload the video to an easily accessible website (such as YouTube) and give
    us the URL and any required access codes in the appropriate fields within
    the application for admission. It is the applicant's responsibility to
    correctly configure the security and access settings for the video. We
    recommend that the applicant use the strongest privacy settings available
    while still allowing access to the LTI. If using YouTube, we
    suggest using the "Unlisted Video" setting so that only individuals who have
    the URL can view it. The applicant is also responsible for adhering to the
    terms and conditions of the website used to share the video essay. Following
    evaluation of the video essay, the LTI will retain a short excerpt
    or screen shot of the video as a part of the official student record.
    </p>
    
    <p>
    We are unable to watch videos that come in any form other than a URL link.
    We will not accept videos sent by email or on DVD through the mail.
    </p>
    
    <table>
        <tr>
            <td>Video essay URL (website address):</td>
            <td><? showEditText($videoEssayLink, "textbox", "txtVideoEssayLink", $_SESSION['allow_edit'], false, null, true, 50); ?></td>
        </tr>
        <tr>
            <td>Video essay access code (if applicable):</td>
            <td><? showEditText($videoEssayCode, "textbox", "txtVideoEssayCode", $_SESSION['allow_edit'], false, null, true, 30); ?></td>
        </tr>
    </table>

    <hr size="1" noshade color="#990000">
<? }//end scs ?>


<?php
if($_SESSION['domainname'] == "MSE-Campus" 
    || marray_search( "MSE_Campus", $myPrograms) != false)
     {
?>

    <span class="subtitle">Video Essay</span>
    <p>
        <b>(Mandatory):</b>Prepare a 1-2 minute video that lets the admission
        committee know who your are.<font color="blue">&nbsp;Talk about what you like, 
        what you do outside of work and school, how you spend your free time, your 
        personal and professional goals, or anything that you did not already share 
        in your Statement of Purpose.</font> This will help the committee know you better 
        and help us to evaluate your communication skills.
    </p>  
    
    <p>
        The video (and audio) must be of the 
        applicant (not a proxy).  There should be only one person in the video. The
        video essay can be simple or more elaborate.
    </p>
    
    <p>
        The content of the video essay is important, but be aware that other factors 
        also contribute to the overall quality and impression of the video essay.  These are:
     </p>
        <ul>
            <li>Ensure that your visual appearance and dress are appropriate.</li>
            <li>Make sure that your recorded sound is clean, sufficiently loud, and understandable.</li>
            <li>Minimize distractions!  Avoid:
                <ul>
                    <li>Distracting camera angles</li>
                    <li>Background noise</li>
                    <li>Cluttered backgrounds (e.g., lots of disorderly arranged, distracting objects behind you)</li>
                    <li>Lighting that is inappropriate (e.g. too dark or too light)</li>
                    <li>Unwanted camera motion (use a tripod or other image stabilization device or techniques)</li>
                    <li>Recording outdoors on a windy day.</li>
                </ul>
            </li>
        </ul>
   
    
    <p>
        Upload the video to an easily accessible website (such as YouTube), and 
        give us the URL and the required access codes.  You will find an appropriate
        field for both below.   
    </p>
    <p>
        It is the applicant's responsibility to correctly 
        configure the security and access settings for the video. We recommend that 
        you, the applicant, use the strongest privacy settings that still allow our 
        access to the video. If using YouTube, we suggest using the "Unlisted Video"
        setting so that only individuals who have the URL can view it. Ensure that 
        your video essay link works for others and if protected, the access code 
        works.  Test this by having a friend access your video at the URL. 
    </p>
    
    <p>The applicant is also responsible for adhering to the terms and 
    conditions of the website that you use to share the video essay. Following 
    evaluation of the video essay, please know that the program will retain a 
    short excerpt (or screen shot) of the video as a part of the official 
    student record.
    </p>
    
    <p>
    Be advised that we are unable to watch videos that come in any form other 
    than a URL link. We will not accept videos sent via email or on DVD through 
    the mail.
    </p>
    
    <table>
        <tr>
            <td>Video essay URL (website address):</td>
            <td><? showEditText($videoEssayLink, "textbox", "txtVideoEssayLink", $_SESSION['allow_edit'], false, null, true, 50); ?></td>
        </tr>
        <tr>
            <td>Video essay access code (if applicable):</td>
            <td><? showEditText($videoEssayCode, "textbox", "txtVideoEssayCode", $_SESSION['allow_edit'], false, null, true, 30); ?></td>
        </tr>
    </table>

    <hr size="1" noshade color="#990000">
<?php    
}
?>

<?php
if( $_SESSION['domainname'] == "MITS" 
    || marray_search( "MITS", $myPrograms) != false) {
?>

    <span class="subtitle">Video Essay</span>
    <p>
    <b>(Mandatory):</b>Prepare a 1-2 minute video that lets the admission
        committee know who your are.<font color="blue">&nbsp;Talk about what you like, 
        what you do outside of work and school, how you spend your free time, your 
        personal and professional goals, or anything that you did not already share 
        in your Statement of Purpose.</font> This will help the committee know you better 
        and help us to evaluate your communication skills.
    </p>  
    
    <p>
        The video (and audio) must be of the 
        applicant (not a proxy).  There should be only one person in the video. The
        video essay can be simple or more elaborate.
    </p>
    
    <p>
        The content of the video essay is important, but be aware that other factors 
        also contribute to the overall quality and impression of the video essay.  These are:
     </p>
        <ul>
            <li>Ensure that your visual appearance and dress are appropriate.</li>
            <li>Make sure that your recorded sound is clean, sufficiently loud, and understandable.</li>
            <li>Minimize distractions!  Avoid:
                <ul>
                    <li>Distracting camera angles</li>
                    <li>Background noise</li>
                    <li>Cluttered backgrounds (e.g., lots of disorderly arranged, distracting objects behind you)</li>
                    <li>Lighting that is inappropriate (e.g. too dark or too light)</li>
                    <li>Unwanted camera motion (use a tripod or other image stabilization device or techniques)</li>
                    <li>Recording outdoors on a windy day.</li>
                </ul>
            </li>
        </ul>
   
    
    <p>
        Upload the video to an easily accessible website (such as YouTube), and 
        give us the URL and the required access codes.  You will find an appropriate
        field for both below.   
    </p>
    <p>
        It is the applicant's responsibility to correctly 
        configure the security and access settings for the video. We recommend that 
        you, the applicant, use the strongest privacy settings that still allow our 
        access to the video. If using YouTube, we suggest using the "Unlisted Video"
        setting so that only individuals who have the URL can view it. Ensure that 
        your video essay link works for others and if protected, the access code 
        works.  Test this by having a friend access your video at the URL. 
    </p>
    
    <p>The applicant is also responsible for adhering to the terms and 
    conditions of the website that you use to share the video essay. Following 
    evaluation of the video essay, please know that the program will retain a 
    short excerpt (or screen shot) of the video as a part of the official 
    student record.
    </p>
    
    <p>
    Be advised that we are unable to watch videos that come in any form other 
    than a URL link. We will not accept videos sent via email or on DVD through 
    the mail.
    </p>
    
    <table>
        <tr>
            <td>Video essay URL (website address):</td>
            <td><? showEditText($videoEssayLink, "textbox", "txtVideoEssayLink", $_SESSION['allow_edit'], false, null, true, 50); ?></td>
        </tr>
        <tr>
            <td>Video essay access code (if applicable):</td>
            <td><? showEditText($videoEssayCode, "textbox", "txtVideoEssayCode", $_SESSION['allow_edit'], false, null, true, 30); ?></td>
        </tr>
    </table>

    <hr size="1" noshade color="#990000">
<?php    
}
?>

<? if($_SESSION['domainname'] == "VLIS" || marray_search( "VLIS", $myPrograms) != false )
{ 
?>
    <span class="subtitle">
    <strong>Where did you learn about the Very Large Information Systems at Carnegie Mellon program?</strong></span><br>
    <br>
    <? showEditText($referral, "listbox", "lbReferral", $_SESSION['allow_edit'],false, $vlisReferrals); ?><br>
    <br>
    <? showEditText($referral, "textbox", "txtReferral", $_SESSION['allow_edit'],false,null,true,35,35); ?>
    If Other, please explain: (35 characters max)<br>
    <br>
    <hr size="1" noshade color="#990000">
    <span class="subtitle">
    Professional Experience</span><br><br>
    <!--        Only for Software Engineering Applicants and/or Societal Computing. -->
    Details related to your Professional Experience must be submitted in PDF format. 
    Any other format will not be accepted. The maximum file size is 3MB.
    <br><br>
    <? $qs = "?id=4&t=5"; 
    if($_SESSION['allow_edit'] == 1)
    {
	    ?>
	    <input class="tblItem" name="btnUpload" value="Upload Professional Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'"><br>
	    <?
    }
    $ex = getExperience($_SESSION['appid'], 4);
    for($i = 0; $i < count($ex); $i++)
    {
	    showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 4, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
    }
    ?>
    <br>
    <hr size="1" noshade color="#990000">
    
    <span class="subtitle">
    Honors and Awards</span><br>
    <? showEditText($honors, "textarea", "txtHonors", $_SESSION['allow_edit'],false); ?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<? 
}//end MSE/VLIS


if ( isMseMsitDomain($_SESSION['domainid']) && $domainid != 40 && $domainid != 66) {
?>
    <span class="subtitle">
    <strong>Where did you learn about the Carnegie Mellon MSE/MSIT programs?</strong></span>
    <span>
    <br>
    <br>
    <? showEditText($referral, "listbox", "lbReferral", $_SESSION['allow_edit'],false, $mseMsitReferrals); ?><br>
    <br>
    <? showEditText($referral, "textbox", "txtReferral", $_SESSION['allow_edit'],false,null,true,35,35); ?>
    If Other, please explain: (35 characters max)<br>
    <br>
    </span>
    <br>
    <hr size="1" noshade color="#990000">
<?php
}

if (isMITSDomain($_SESSION['domainid'])) {
?>
    <span class="subtitle">
    <strong>Where did you learn about the Carnegie Mellon MITS programs?</strong></span>
    <span>
    <br>
    <br>
    <? showEditText($referral, "listbox", "lbReferral", $_SESSION['allow_edit'],false, $mseMsitReferrals); ?><br>
    <br>
    <? showEditText($referral, "textbox", "txtReferral", $_SESSION['allow_edit'],false,null,true,35,35); ?>
    If Other, please explain: (35 characters max)<br>
    <br>
    </span>
    <br>
    <hr size="1" noshade color="#990000">
<?php
}

if($_SESSION['domainname'] == "CNBC" || marray_search( "CNBC", $myPrograms) != false ){ ?>
    <span class="subtitle"><u>For candidates applying to the Ph.D. Program in Neural Computation only</u></span><br><br>
    <span class="subtitle">Advisors</span><br>
    Please list the names of up to 4 potential research advisors you might want to work with if admitted. 
    For a list of PNC training faculty, please visit this page: 
    <a href="http://www.cnbc.cmu.edu/GradTrain/pnc_trainingfaculty.shtml" target="_blank">
             http://www.cnbc.cmu.edu/GradTrain/pnc_trainingfaculty.shtml</a> . 
    This list is not exclusive; students may also work with faculty not listed here if the research is
    appropriate for the Program in Neural Computation. 
    <BR><BR>

<!-- Change this to false i.e. Required field -->

    1. <? showEditText($advisor1, "textbox", "txtAdvisor_1", $_SESSION['allow_edit'],false, null, true, 30); ?>
	<? showEditText(2, "hidden", "txtAdvisorType_1", $_SESSION['allow_edit']); ?>
    <br>
    2.
    <? showEditText($advisor2, "textbox", "txtAdvisor_2", $_SESSION['allow_edit'],false,  null, true, 30); ?>
	<? showEditText(2, "hidden", "txtAdvisorType_2", $_SESSION['allow_edit']); ?>
    <br>
    3.
    <? showEditText($advisor3, "textbox", "txtAdvisor_3", $_SESSION['allow_edit'],false, null, true, 30); ?>
	<? showEditText(2, "hidden", "txtAdvisorType_3", $_SESSION['allow_edit']); ?>
	<br>
	4.
    <? showEditText($advisor4, "textbox", "txtAdvisor_4", $_SESSION['allow_edit'],false, null, true, 30); ?>
	<? showEditText(2, "hidden", "txtAdvisorType_4", $_SESSION['allow_edit']); ?>
    <br>
    <br>
	<span class="subtitle">Other Institutions/Programs</span>
	<br>
	Please tell us the names of other institutions/programs you are applying to in addition to 
                Carnegie Mellon and the University of Pittsburgh.
	<br> 
	<? showEditText($otherUni, "textarea", "txtOtherUni", $_SESSION['allow_edit'], true); ?>
    <br><br>
    <hr size="1" noshade color="#990000">
<?php } // end CNBC
            
 if($_SESSION['domainname'] == "CNBC-Grad-Training" // || marray_search( "CNBC", $myPrograms) != false 
)
{ ?>           
    <span class="subtitle"><u>For candidates applying to the CNBC Graduate Training Program only</u></span><br><br>
    Please indicate the affiliated Ph.D. programs to which you have applied.<br>
    For information about these programs, click
    <a href="http://www.cnbc.cmu.edu/Affiliated" target=_blank>here</a>.

    <BR><BR>
    <? 
	/*
	if($tmpCrossDeptProgsId != "")
	{
		$crossDeptProgs = $tmpCrossDeptProgsId;
	}
	if(!is_array($crossDeptProgs))
	{
		$crossDeptProgs = explode(",",$crossDeptProgs);
	}
	*/
	showEditText($crossDeptProgs, "checklist", "chkCrossDeptProgs", $_SESSION['allow_edit'], false, $arrCrossDeptProgs); 
	?>
    <br>
    <? showEditText($crossDeptProgsOther, "textbox", "txtCrossDeptProgs", $_SESSION['allow_edit'],false,null,true,35,35); ?>
	If Other, please explain: (35 characters max)			
    <br><br>
    <hr size="1" noshade color="#990000"> 
    <span class="subtitle">Authorization</span>
	<br>
    
	<? showEditText($permission, "checkbox", "chkPermission", $_SESSION['allow_edit'], false); ?>
	
	I authorize the CNBC to obtain copies of my  transcripts, GRE scores, and letters of reference from the  affiliated PhD program(s) to which I am applying.
	<br>
    <br>
    <hr size="1" noshade color="#990000">
 <? }//end if grad training?>

<!--	    
<? if($_SESSION['domainname'] == "CompBio" || marray_search( "CompBio", $myPrograms) != false ){ ?>
    <span class="subtitle">Advisors</span><br>
    The following drop down menus contain the list of training faculty associated with this program at Carnegie Melon University and the University of Pittsburgh. If you are interested in working with a specific faculty member please select her / his name from the lists below. You may select up to three potential advisors.<br>

    1. <? showEditText($advisor1, "listbox", "lbAdvisor_1", $_SESSION['allow_edit'],true, $advisors); ?>
    <? showEditText(1, "hidden", "txtAdvisorType_1", $_SESSION['allow_edit']); ?>
    <br>
    2.
    <? showEditText($advisor2, "listbox", "lbAdvisor_2", $_SESSION['allow_edit'],true, $advisors); ?>
    <? showEditText(1, "hidden", "txtAdvisorType_2", $_SESSION['allow_edit']); ?>
    <br>
    3.
    <? showEditText($advisor3, "listbox", "lbAdvisor_3", $_SESSION['allow_edit'],true, $advisors); ?>
    <? showEditText(1, "hidden", "txtAdvisorType_3", $_SESSION['allow_edit']); ?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<? }//end if compbio ?>
-->

<? 
// Section not functional - handled above.
if($_SESSION['domainname'] == "HCII" || marray_search( "HCII", $myPrograms) != false ){ ?>
    <span class="subtitle">Portfolio</span><br>
    Have you submitted a portfolio?
    <? 
    $vars = array(array('0', 'No'), array('1','Yes'));
    showEditText($port, "listbox", "lbPort", $_SESSION['allow_edit'],true, $vars); 
    ?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<? }//end if HCII ?>


<? 
if($_SESSION['domainname'] == "MS-HCII" || marray_search( "MS-HCII", $myPrograms) != false ){ ?>
    <span class="subtitle">Additional Knowledge</span>
    <br>
    Please give a detailed description of your experience in the field of <strong>programming</strong>.<br> 
    <? showEditText($prog, "textarea", "txtProg", $_SESSION['allow_edit'], true); ?>
    <br>
    <br>
    Please give a detailed description of your experience in the field of <strong>design</strong>.<br>
    <? showEditText($design, "textarea", "txtDesign", $_SESSION['allow_edit'], true); ?>
    <br>
    <br>
    Please give a detailed description of your experience in the field of <strong>statistics</strong>.<br>
    <? showEditText($stats, "textarea", "txtStats", $_SESSION['allow_edit'], true); ?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<? }//end if MHCI ?>

<? 
if($domainid == 49){ ?>
    <span class="subtitle">Prerequisite Courses</span>
    <br>
    <!-- Please give a detailed description of your experience in the field of <strong>statistics</strong>.<br>   --->
    <? showEditText($prog, "textarea", "txtProg", $_SESSION['allow_edit'], true); ?>
    <br>
    <br>
    <span class="subtitle">Experience</span>
    <br>
    <!-- Please give a detailed description of your experience in the field of <strong>statistics</strong>.<br>   --->
    <? showEditText($stats, "textarea", "txtStats", $_SESSION['allow_edit'], false); ?>
    <br>
    <br>
    <hr size="1" noshade color="#990000">
<? }//end if MHCI ?>

<?php 
   // if Statistics
   if ($domainid == 44) { 
   ?>
   <span class="subtitle">Resume</span><span class="tblItem"><br>
       <? 
           $qs = "?id=1&t=2";
       ?>
       <input class="tblItem" name="btnUpload" value="Upload Resume" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
       <?
           if($resModDate != "")
           {
               showFileInfo($resName, $resSize, formatUSdate($resModDate), getFilePath(2, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
           }//end if ?>

       <br><br>
       </span>
       <hr size="1" noshade color="#990000">
<?php 
   }  
   
/*
* DIETRICH/INI
* Disciplinary action, financial support sections for INI
*/
if (isIniDomain($domainid))
{
    include('../inc/iniDisciplinaryAction.inc.php'); 
    include('../inc/iniFinancialSupport.inc.php');
    include('../inc/iniFinancialDocument.inc.php');   
}

if ( isEM2Domain($domainid))
{
    include('../inc/iniDisciplinaryAction.inc.php'); 
    include('../inc/em2FinancialSupport.inc.php');
    include('../inc/iniFinancialDocument.inc.php');   
}


/*
* DIETRICH/INI
* Attendance section for english
*/
if (isEnglishDomain($domainid))
{
    include('../inc/dietrichAttendance.inc.php');   
}

/*
* DIETRICH/INI
* Affiliation, hear about sections for english, modern languages
*/
if (isEnglishDomain($domainid) || isModernLanguagesDomain($domainid))
{
    include('../inc/dietrichCmuAffiliation.inc.php');
    include('../inc/dietrichHearAbout.inc.php');   
}

/*
* DIETRICH/INI/Design
* Disability, financial, recognitions section for psychology, philosophy
*/
if (isPsychologyDomain($domainid))
{
    include('../inc/dietrichDisability.inc.php'); 
    include('../inc/dietrichFinancialSupport.inc.php'); 
    include('../inc/dietrichRecognitions.inc.php'); 
}

if (isPhilosophyDomain($domainid))
{
    include('../inc/dietrichRecognitions.inc.php'); 
}

if ($isDesignDomain || $isDesignPhdDomain)
{
    include('../inc/designFinancialSupport.inc.php'); 
}
?>

<span class="subtitle">
<? showEditText("Save Supplemental Info", "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
</span><br>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';

function applyingToLtiProgram()
{
    for ($i = 0; $i < count($_SESSION['programs']); $i++)
    {
        if (isLtiProgram($_SESSION['programs'][$i][0]))
        {
            return TRUE;
        }
    }
    
    return FALSE;
}
?>