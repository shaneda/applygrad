<?php
 include "../inc/db_connect.php";
 include "../inc/tpl.pageHeaderApply.php";
 include "../classes/DB_Applyweb/class.DB_Applyweb.php";
 include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
 include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
 include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
 include '../classes/class.PaymentManager.php';
 include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
include '../classes/class.RegistrationFeePaymentManager.php';
include '../classes/class.RegistrationFeePayment.php';

function getPaymentRecordId ($applicationId)
{
    $id = 0;
    $sql = "SELECT payment_id FROM payment WHERE application_id = " . $applicationId . ' AND payment_status = "pending"';
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $query;
        die($message);
    }
    $id = mysql_result($result, 0);
    return $id;
}

function updatePaymentTable($rowId)
{
    $sql = "UPDATE payment SET payment_status = 'void', last_mod_time = NOW() WHERE payment_id = " . $rowId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}

function updateApplication($appId)
{
    $sql = "UPDATE application SET paymentamount = NULL, paymenttype= NULL WHERE id = " . $appId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}
    

function voidPaymentTransaction ($appId)
{
    $paymentManager = new PaymentManager($appId);
    
   $rowId = getPaymentRecordId($appId);
   if ($rowId != 0) {
      $paymentManager->deletePayment($rowId);
      updateApplication($appId);
       
   }
    
}

function getLastRegistrationPaymentRecordId ($applicationId)
{
    $id = 0;
    $sql = "SELECT id FROM registration_fee_payment WHERE application_id = " . $applicationId . ' AND payment_status = "pending"';
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $query;
        die($message);
    }
    $id = mysql_result($result, 0);
    return $id;
}

function updateRegistrationPaymentTableVoid($rowId)
{
    $sql = "UPDATE registration_fee_payment SET payment_status = 'void', last_mod_time = NOW() WHERE id = " . $rowId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}

function updateRegistrationUnpaid($appId)
{
    $sql = "UPDATE registration_fee_status SET paid = 0 WHERE application_id = " . $appId;
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }
}
    

function voidRegistrationTransaction ($appId)
{
    $paymentManager = new RegistrationFeePaymentManager($appId);
   $rowId = getLastRegistrationPaymentRecordId($appId);

   if ($rowId != 0) {
       $paymentManager->deletePayment($rowId);
      updateRegistrationPaymentTableVoid($rowId);
       
   } 
}

function addFailedTransaction ($app_id, $tx, $name, $email, $merchant, $status)
{
    $sql = 'INSERT INTO cashnet_payment (app_id, transaction_id, applicant_name, applicant_email, merchant, status) VALUES ('
    . $app_id . ', ' . $tx . ', "' . $name . '", "' . $email . '", ' . $merchant . ', "' . $status . '")';
    $result = mysql_query($sql);
    if (!$result) {
        $message  = 'Invalid query: ' . mysql_error() . "\n";
        $message .= 'Whole query: ' . $sql;
        die($message);
    }    
}

    
    echo '<span class="title">Transaction Failed</span>'."<BR /><br />";
    echo $_POST['ccerrormessage'] . "<br />";
    $paymentAppId = $_POST['FLEX_FIELD3'];
    $paymentCategory = $_POST['FLEX_FIELD5'];
    addFailedTransaction($paymentAppId, $_POST['failedtx'], $_POST['FLEX_FIELD1'], $_POST['FLEX_FIELD2'], $_POST['merchant'], $_POST['respmessage']);
    //debugbreak();
    error_log("Call made to failed transaction code", 1, "awtest@cs.cmu.edu");
    echo "<p>We are sorry but your payment has not been successful.  Please use the link below to try your payment again. </p>";
    if ($paymentCategory == "Registration Fee") {
        voidRegistrationTransaction($paymentAppId);
        
        if ($_SERVER['SERVER_NAME'] == "web28.srv.cs.cmu.edu") {
                    echo '<br /><a href="https://web28.srv.cs.cmu.edu:446/replyform/admitted_applicant.php" > Back to Reply Form </a>';
                } else {
                     echo '<br /><a href="https://' .$_SERVER['SERVER_NAME'] . '/admitted_applicant.php" > Back to Reply Form </a>';
                }
    } else {
                voidPaymentTransaction($paymentAppId);

                if ($_SERVER['SERVER_NAME'] == "web28.srv.cs.cmu.edu") {
                    echo '<br /><a href="https://web28.srv.cs.cmu.edu:446/apply/home.php" > Back to Application </a>';
                } else {
                     echo '<br /><a href="https://' .$_SERVER['SERVER_NAME'] . '/apply/home.php" > Back to Application </a>';
                }
    }
   
  
  
  /*
   if there is a record in the payment table
   void payment and update date
   
   null out payment amount and payment type in the application table 
  
  */
?>
