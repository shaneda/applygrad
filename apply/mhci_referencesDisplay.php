<?php
  for($i = 0; $i < count($myReferences); $i++)
            {
                $allowEdit = $_SESSION['allow_edit'];
                $submitted = "";
                if($myReferences[$i][1] > -1)
                {
                    $allowEdit = false;
                }
                $title = "";
   /*             switch($myReferences[$i][8])
                {
                    case "1":
                        $title = "Undergraduate";
                    break;
                    case "2":
                        $title = "Graduate";
                    break;
                    case "3":
                        $title = "Additional";
                    break;
                
                }//end switch
                */
       
                if($myReferences[$i][12] > 0)
                {
                    $submitted = "(Reference Requested)";
                } else {

                                  if($myReferences[$i][6] != "") // Is there an email address?
                                  {
                          $submitted = "(NOTE: Email Request to this reference has NOT been sent)";
                                  }
                                }
                $attCount = sizeof($myReferences[$i]);
                if ($attCount < 16) {
                    if ($myReferences[$i][8] > 0) 
                    {
                     $submitted = "(Reference Received)";
                     }
                } else {
                    if (($myReferences[$i][8] > 0) || ($myReferences[$i][15] != NULL))
                    {
                     $submitted = "(Reference Received)";
                     }
                }
                ?><!--BEGIN MAIN TABLE -->
            <!-- REC TABLE -->
<table width="650" border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td><span class="subtitle">Reference <?=($i+1)?> <?=$submitted?></span> </td>
    <td align="right">
<!-- Display DEL Button only after this reference has been saved -->
<?      // If the reference in question has been created, that means its been saved
//echo $i . " " . $NUMRECS;
        if ( $allowEdit == false || $i > $NUMRECS-1)
        {
        showEditText("Delete", "button", "btnDelete_".$myReferences[$i][0], $_SESSION['allow_edit']); 
        }
?>
    <input name="txtRecord_<?=$myReferences[$i][0];?>" type="hidden" id="txtRecord_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][0]?>" />
    <input name="txtUid_<?=$myReferences[$i][0];?>" type="hidden" id="txtUid_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][1]?>" />
    <input name="txtUMasterid_<?=$myReferences[$i][0];?>" type="hidden" id="txtUMasterid_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][15];?>">

    <input name="txtDatafile_<?=$myReferences[$i][0];?>" type="hidden" id="txtDatafile_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][8];?>">
    <input name="txtModdate_<?=$myReferences[$i][0];?>" type="hidden" id="txtModdate_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][9];?>">
    <input name="txtSize_<?=$myReferences[$i][0];?>" type="hidden" id="txtSize_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][10];?>">
    <input name="txtExtension_<?=$myReferences[$i][0];?>" type="hidden" id="txtExtension_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][11];?>">
    <input name="txtRecSent_<?=$myReferences[$i][0];?>" type="hidden" id="txtExtension_<?=$myReferences[$i][0];?>" value="<?=$myReferences[$i][12];?>">

</td>
  </tr>
</table>


<table border=0 cellpadding=1 cellspacing=1 class="tblItem" width="650">
<?  if(count($types) > 1){ ?>
<tr>
<td></td>
<td></td>
</tr>
<? }else
{
    showEditText("1", "hidden", "lbType_".$myReferences[$i][0], $_SESSION['allow_edit'],true, null);
} ?>
<tr>
<td width="216"><strong>First Name:</strong><br>
<? showEditText($myReferences[$i][2], "textbox", "txtFname_".$myReferences[$i][0], $allowEdit,true,null,true,20); ?></td>
<td width="216"><strong>Last Name:</strong><br>
<? showEditText($myReferences[$i][3], "textbox", "txtLname_".$myReferences[$i][0], $allowEdit,true,null,true,20); ?></td>
<td width="218"><strong>Job Title:</strong><br>
<? showEditText($myReferences[$i][4], "textbox", "txtTitle_".$myReferences[$i][0], $allowEdit,true,null,true,20); ?></td>
</tr>
<tr>
<td><strong>Company/Institution:</strong><br>
<? showEditText($myReferences[$i][5], "textbox", "txtAffiliation_".$myReferences[$i][0], $allowEdit,true,null,true,40); ?></td>
<td><strong>Email:</strong><br>
<? showEditText($myReferences[$i][6], "textbox", "txtEmail_".$myReferences[$i][0], $allowEdit,true,null,true,30); ?> </td>
<td><strong>Phone:</strong><br>
<? showEditText($myReferences[$i][7], "textbox", "txtPhone_".$myReferences[$i][0], $allowEdit,true,null,true,20,20); ?></td>
</tr>
<tr>
<td colspan=3>
<? 
// If recommendation already received, do not show any buttons
 
if($myReferences[$i][9] == 0)
{   
if ($myReferences[$i][2] != "" && $allowEdit == false  
    && ($submitted != "(Reference Received)"))
{    
    if($myReferences[$i][1] != "" && $myReferences[$i][12] > 0)  { ?>
    <p><b>
     Send email reminder to  reference.</b> 
    <? showEditText("Send Reminder", "button", "btnSendReminder_".$myReferences[$i][0], $_SESSION['allow_edit_late'],false); 
    }else{
    
    ?>
    <p><b>
     Send email request to reference.</b> <? showEditText("Send Request", "button", "btnSendmail_".$myReferences[$i][0], $_SESSION['allow_edit_late'],false); 
     
    } 
}
}
?>
</td>
</tr>
</table>
<!--END MAIN TABLE -->
                <hr size="1" noshade color="#990000">
            <? 
                
            }//end for
?>
