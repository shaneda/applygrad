<?php      
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/section_required.php';
include_once '../apply/header_prefix.php'; 
include '../inc/specialCasesApply.inc.php';

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
{
    $prevPage = $appPageList[$curPageIndex - 1]; 
}
else
{
    $prevPage = "";
}

if (sizeof ($appPageList) > $curPageIndex + 1)
{
    $nextPage =  $appPageList[$curPageIndex + 1]; 
} 
else
{
    $nextPage = "";
}

$err = "";
$portfolioLink = "";
$portfolioPassword = "";
             
if(isset($_POST['btnSave']))
{
	saveData();
}

function saveData()
{
    global $err;
    global $portfolioLink;
    global $portfolioPassword;

    $portfolioLink = filter_input(INPUT_POST, 'portfolioLink', FILTER_SANITIZE_STRING);
    $portfolioPassword = filter_input(INPUT_POST, 'portfolioPassword', FILTER_SANITIZE_STRING);
    
    if (!$portfolioLink)
    {
        $err = 'You must enter a portfolio URL.';
    }
    
	if($err == "")
	{
        $sql = "UPDATE application SET
            portfolio_link = '" . mysql_escape_string($portfolioLink) . "',
            portfolio_password = '" . mysql_escape_string($portfolioPassword) . "'
            where id = " . $_SESSION['appid'];
    
        mysql_query($sql);
        
		updateReqComplete("portfolio.php", 1);
	}  
    else 
    {
        updateReqComplete("portfolio.php", 0, 1, 1);
	}
}

//GET USER INFO
$sql = "SELECT portfolio_link, portfolio_password
FROM application 
WHERE application.id = " . $_SESSION['appid'];
$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
{
	$portfolioLink = $row['portfolio_link'];
    $portfolioPassword = $row['portfolio_password'];
}

$domainid = 1;
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}

// Include the shared page header elements.
if (isDesignPhdDomain($domainid))
{
    $pageTitle = 'Portfolio of Expertise';
}
else
{
    $pageTitle = 'Portfolio';
}
include '../inc/tpl.pageHeaderApply.php';


$sql = "select content from content where name='Portfolio' and domain_id=".$domainid;
$result = mysql_query($sql);
while($row = mysql_fetch_array( $result )) 
{
	echo html_entity_decode($row["content"]);
}
?>

<br>                            
<span class="errorSubtitle"><?=$err?></span><br>
<div style="height:10px; width:10px;  background-color:#000000; padding:1px;float:left;">
<div class="tblItemRequired" style="height:10px; width:10px; float:left;"></div>
</div> = required<br>
<br>
<? showEditText("Save", "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
<input name="txtPost" type="hidden" id="txtPost" value="1">
<br>
<br>
<hr size="1" noshade color="#990000">
<br>
List the URL and password, if required, to the website containing your portfolio.
<br><br>
<span style="display: inline-block; font-weight: bold; width: 100px;">URL:</span>
<?php  
showEditText($portfolioLink, "textbox", "portfolioLink", $_SESSION['allow_edit'], true, null, true, 40); 
?>
<br>
<br>
<span style="display: inline-block; font-weight: bold; width: 100px;">Password:</span>
<?php  
showEditText($portfolioPassword, "textbox", "portfolioPassword", $_SESSION['allow_edit'],false, null, true, 40); 
?>
<br>
<br>
<hr size="1" noshade color="#990000">

<span class="subtitle">
<? showEditText("Save", "button", "btnSave", $_SESSION['allow_edit'] || $_SESSION['allow_edit_late']); ?>
</span><br>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>