<?
/* Set whether or not GRE General Scores are required */
/* true = are required; false = are NOT required */
/* This will mark the section as completed when the score are not required */
/* can probably put this in the database, but will wait till later to do that  */ 

$GRE_GENERAL_REQUIRED[-1] = true;
$GRE_GENERAL_REQUIRED[0] = true;                             
$GRE_GENERAL_REQUIRED[1] = true; 
$GRE_GENERAL_REQUIRED[2] = true;
$GRE_GENERAL_REQUIRED[3] = true;
$GRE_GENERAL_REQUIRED[4] = true;
$GRE_GENERAL_REQUIRED[5] = true;
$GRE_GENERAL_REQUIRED[6] = true;
$GRE_GENERAL_REQUIRED[7] = true;
$GRE_GENERAL_REQUIRED[8] = true;
$GRE_GENERAL_REQUIRED[9] = true;
$GRE_GENERAL_REQUIRED[10] = true;
$GRE_GENERAL_REQUIRED[11] = true;
$GRE_GENERAL_REQUIRED[12] = true;
$GRE_GENERAL_REQUIRED[13] = true;
$GRE_GENERAL_REQUIRED[14] = true;
$GRE_GENERAL_REQUIRED[15] = true;
$GRE_GENERAL_REQUIRED[16] = false; /* MSE-Dist */
$GRE_GENERAL_REQUIRED[17] = true;
$GRE_GENERAL_REQUIRED[18] = true;
$GRE_GENERAL_REQUIRED[19] = true;
$GRE_GENERAL_REQUIRED[20] = true;
$GRE_GENERAL_REQUIRED[21] = true;
$GRE_GENERAL_REQUIRED[22] = true;
$GRE_GENERAL_REQUIRED[23] = true;
$GRE_GENERAL_REQUIRED[24] = true;
$GRE_GENERAL_REQUIRED[25] = true; 
$GRE_GENERAL_REQUIRED[26] = true;
$GRE_GENERAL_REQUIRED[27] = true;
$GRE_GENERAL_REQUIRED[28] = true;
$GRE_GENERAL_REQUIRED[29] = true;
$GRE_GENERAL_REQUIRED[30] = true;
$GRE_GENERAL_REQUIRED[31] = true;
$GRE_GENERAL_REQUIRED[32] = true;
$GRE_GENERAL_REQUIRED[33] = true;
$GRE_GENERAL_REQUIRED[34] = true;
$GRE_GENERAL_REQUIRED[35] = true;
$GRE_GENERAL_REQUIRED[36] = false; /* MSE-Dist */
$GRE_GENERAL_REQUIRED[37] = true;
$GRE_GENERAL_REQUIRED[38] = false;
$GRE_GENERAL_REQUIRED[39] = true;
$GRE_GENERAL_REQUIRED[40] = true;
?>