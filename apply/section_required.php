<?
/* Web page header information specific School-programs */
/* can probably put this in the database, but will wait */
/* till later to do that  */ 

/* Indexed by Domain */

for ($i = -1; $i < 100; $i++) {
    $GRE_SCORES_REQUIRED[$i] = true;
    $SUPPINFO_REQUIRED[$i] = false;     
}

/*
$GRE_SCORES_REQUIRED[-1] = true;
$GRE_SCORES_REQUIRED[0] = true;                             
$GRE_SCORES_REQUIRED[1] = true; 
$GRE_SCORES_REQUIRED[2] = true;
$GRE_SCORES_REQUIRED[3] = true;
$GRE_SCORES_REQUIRED[4] = true;
$GRE_SCORES_REQUIRED[5] = true;
$GRE_SCORES_REQUIRED[6] = true;
$GRE_SCORES_REQUIRED[7] = true;
$GRE_SCORES_REQUIRED[8] = true;
$GRE_SCORES_REQUIRED[9] = true;
$GRE_SCORES_REQUIRED[10] = true;
$GRE_SCORES_REQUIRED[11] = true;
$GRE_SCORES_REQUIRED[12] = true;
$GRE_SCORES_REQUIRED[13] = true;
$GRE_SCORES_REQUIRED[14] = true;
$GRE_SCORES_REQUIRED[15] = true;
$GRE_SCORES_REQUIRED[16] = true;
$GRE_SCORES_REQUIRED[17] = true;
$GRE_SCORES_REQUIRED[18] = true;
$GRE_SCORES_REQUIRED[19] = true;
*/
$GRE_SCORES_REQUIRED[20] = false; // MSIT-GM-Dist
/*
$GRE_SCORES_REQUIRED[21] = true;
*/
$GRE_SCORES_REQUIRED[22] = false; // MSIT-eBiz
/*
$GRE_SCORES_REQUIRED[23] = true;
$GRE_SCORES_REQUIRED[24] = true;
$GRE_SCORES_REQUIRED[25] = true;
$GRE_SCORES_REQUIRED[26] = true;
*/
$GRE_SCORES_REQUIRED[27] = false; // MSIT-eBiz
/*
$GRE_SCORES_REQUIRED[28] = true;
*/
$GRE_SCORES_REQUIRED[29] = false; // MSE-Campus-ARCHIVE
$GRE_SCORES_REQUIRED[30] = false; // MSE-Dist-ARCHIVE
/*
$GRE_SCORES_REQUIRED[31] = true; 
$GRE_SCORES_REQUIRED[32] = true;
$GRE_SCORES_REQUIRED[33] = true;
$GRE_SCORES_REQUIRED[34] = true;
$GRE_SCORES_REQUIRED[35] = true;
$GRE_SCORES_REQUIRED[36] = true;
$GRE_SCORES_REQUIRED[37] = true;
*/
$GRE_SCORES_REQUIRED[38] = false; // RI-SCHOLARS 
/*
$GRE_SCORES_REQUIRED[39] = true;
$GRE_SCORES_REQUIRED[40] = true;
$GRE_SCORES_REQUIRED[41] = true;
*/
$GRE_SCORES_REQUIRED[42] = false; // ISR-ET-EAF 
/*
$GRE_SCORES_REQUIRED[43] = true;
$GRE_SCORES_REQUIRED[44] = true;
*/
$GRE_SCORES_REQUIRED[45] = false; // ISR-ET-AEA
$GRE_SCORES_REQUIRED[46] = false; // ISR-ET-COTS
$GRE_SCORES_REQUIRED[47] = false; // ISR-ET-MSO
$GRE_SCORES_REQUIRED[48] = false; // ISR-ET-SI 

/* 
   Used for determining whether or not the supplemental
   information section has tests to ensure section is
   filled out by the applicant. On entry to the page
   it automatically updates as complete. Currently only
   the CNBC Training Program has a requirement to provide
   permission to obtain records
*/
   
/*
$SUPPINFO_REQUIRED[-1] = false;
$SUPPINFO_REQUIRED[0] = false;                             
$SUPPINFO_REQUIRED[1] = false; 
$SUPPINFO_REQUIRED[2] = false;
$SUPPINFO_REQUIRED[3] = false;
$SUPPINFO_REQUIRED[4] = false;
$SUPPINFO_REQUIRED[5] = false;
$SUPPINFO_REQUIRED[6] = false;
$SUPPINFO_REQUIRED[7] = false;
$SUPPINFO_REQUIRED[8] = false;
$SUPPINFO_REQUIRED[9] = false;
$SUPPINFO_REQUIRED[10] = false;
*/
$SUPPINFO_REQUIRED[11] = true; /* CNBC */
$TARGET_PROGRAM = 39; /* ?????????????? CNBC TRAINING  should be 41 in production */
/*
$SUPPINFO_REQUIRED[12] = false;
$SUPPINFO_REQUIRED[13] = false;
$SUPPINFO_REQUIRED[14] = false;
$SUPPINFO_REQUIRED[15] = false;
$SUPPINFO_REQUIRED[16] = false;
$SUPPINFO_REQUIRED[17] = false;
$SUPPINFO_REQUIRED[18] = false;
$SUPPINFO_REQUIRED[19] = false;
$SUPPINFO_REQUIRED[20] = false;
$SUPPINFO_REQUIRED[21] = false;
$SUPPINFO_REQUIRED[22] = false;
$SUPPINFO_REQUIRED[23] = false;
$SUPPINFO_REQUIRED[24] = false;
$SUPPINFO_REQUIRED[25] = false; 
$SUPPINFO_REQUIRED[26] = false;
$SUPPINFO_REQUIRED[27] = false;
$SUPPINFO_REQUIRED[28] = false;
$SUPPINFO_REQUIRED[29] = false;
$SUPPINFO_REQUIRED[30] = false;
$SUPPINFO_REQUIRED[31] = false;
$SUPPINFO_REQUIRED[32] = false;
*/
$SUPPINFO_REQUIRED[33] = true;  /* CNBC Archive*/
$SUPPINFO_REQUIRED[34] = true;  /* CNBC Grad Training*/
/*
$SUPPINFO_REQUIRED[35] = false;
$SUPPINFO_REQUIRED[36] = false;
$SUPPINFO_REQUIRED[37] = false;
$SUPPINFO_REQUIRED[38] = false;
$SUPPINFO_REQUIRED[39] = false;
$SUPPINFO_REQUIRED[40] = false;
$SUPPINFO_REQUIRED[41] = false;
$SUPPINFO_REQUIRED[42] = false;
$SUPPINFO_REQUIRED[43] = false;   
$SUPPINFO_REQUIRED[44] = false;   
*/
$SUPPINFO_REQUIRED[49] = true;  /* MS - Statistics */
$SUPPINFO_REQUIRED[79] = true;  /* INI */
$SUPPINFO_REQUIRED[83] = true;  /* INI Kobe */
?>