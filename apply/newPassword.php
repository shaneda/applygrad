<?php
include_once '../inc/config.php'; 
//include_once '../inc/session.php';
session_start(); 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php  
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$_SESSION['usertypeid']= -1;
$domainname = "";
$domainid = -1;
$sesEmail = "";

if( isset($_SESSION['domainid']) && $_SESSION['domainid'] != -1)
{
    $domainid = $_SESSION['domainid'];
} 
else 
{
    $domainid = filter_input(INPUT_GET, 'domain', FILTER_VALIDATE_INT);   
}

if ($domainid) 
{
    $_SESSION['domainid'] = $domainid;
} 
else 
{
    header("Location: nodomain.php");
    exit;     
}

if( isset($_SESSION['domainname']) ) 
{
	$domainname = $_SESSION['domainname'];
} 
else 
{
    $domainnameQuery = "SELECT name FROM domain WHERE id = " . intval($domainid);
    $domainnameResult = mysql_query($domainnameQuery);
    while ( $row = mysql_fetch_array($domainnameResult) ) {
        $domainname = $row['name'];
        $_SESSION['domainname'] = $domainname;        
    }         
}

if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

$err = "";
$email = "";
$guid = "";
$passUpdated = false;
$allowContinue = true;
$recommender = false;
if(isset($_GET["r"]))
{
	$recommender = true;
}
if(isset($_GET['email']) && isset($_GET['id']))
{
	$email = filter_input(INPUT_GET, 'email', FILTER_VALIDATE_EMAIL);
	$guid = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
	$sql = "SELECT email, guid from users 
        where email = '". mysql_real_escape_string($email) ."' 
        and guid='". mysql_real_escape_string($guid) ."'";
	$result = mysql_query($sql)
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
	$guid = "";
	while($row = mysql_fetch_array( $result )) 
	{
		$email = $row['email'];
		$guid = $row['guid'];
	}
	if($email == "")
	{
		$err .= "Invalid email " . htmlspecialchars($_GET['email']) . ".<br>";
		$allowContinue = false;
	}
}else
{
	if(isset($_POST['txtEmail']))
	{
        $email = filter_input(INPUT_POST, 'txtEmail', FILTER_VALIDATE_EMAIL);
        $email = htmlspecialchars($email);
        
        $pass = filter_input(INPUT_POST, 'txtPass', FILTER_UNSAFE_RAW);
        $pass = htmlspecialchars($pass);
    
        $pass2 = filter_input(INPUT_POST, 'txtPass2', FILTER_UNSAFE_RAW);
        $pass2 = htmlspecialchars($pass2);

        $guid = filter_input(INPUT_POST, 'txtGuid', FILTER_SANITIZE_STRING);

		if ($email == "" && $email != "*")
		{
			$err .= "Email is required.<br>";
		}
		if($pass == "")
		{
			$err .= "Password is required.<br>";
		}
		if($pass2 == "")
		{
			$err .= "Please confirm your password.<br>";
		}
		if($pass != $pass2)
		{
			$err .= "Passwords do not match.<br>";
		}
		if($recommender != true)
		{
			$pass = sha1($pass);
		}
		if($err == "")
		{
            $updateQuery = sprintf("UPDATE users SET password='%s' 
                where email='%s' and guid = '%s'",
                mysql_real_escape_string($pass),
                mysql_real_escape_string($email),
                mysql_real_escape_string($guid)
                );
            
			mysql_query($updateQuery) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $updateQuery);
			
            if(mysql_affected_rows() > 0)
			{
				$passUpdated = true;
			}
            else
            {
                $err = "New password same as old password.";
            }
		}
	}
    else
	{
		$err .= "No parameters specified<br>";
		$allowContinue = false;
	}
}

$pageTitle = 'Forgot Password';
$formAction = 'newPassword.php';
include '../inc/tpl.pageHeaderApply.php';
?>
    
    <span class="errorSubtitle"><?=$err?></span> 
    <span class="tblItem">
	<?
	if($passUpdated == true)
	{
		$loginHref = "index.php?domain=" . $domainid;
        if($recommender == true) {
            $loginHref .= "&r=1";
        }
        ?>
        Your password has been changed. 
        <strong><a href="<?php echo $loginHref;  ?>">Click here to login</a></strong>.
		<?
	}?>
    </span>
	<? if($passUpdated == false && $allowContinue == true){ ?>
    <table width="300" border="0" cellpadding="4" cellspacing="2" class="tblItem">
      <tr>
        <td align="right"><input name="txtEmail" type="hidden" id="txtEmail" value="<?=$email?>">
          <input name="txtGuid" type="hidden" id="txtGuid" value="<?=$guid?>">
        New Password: </td>
        <td><? showEditText("", "password", "txtPass", true); ?></td>
      </tr>
      <tr>
        <td align="right">Confirm New Password: </td>
        <td><? showEditText("", "password", "txtPass2", true); ?></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td><span class="subtitle">
          <? showEditText("Submit", "button", "btnSubmit", true); ?>
        </span></td>
      </tr>
    </table>
<?php
			}

// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>