<?php
include_once '../inc/config.php';
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Skip the scriptaculous include in function.php
include_once '../inc/functions.php';

/* 
* New 5/31/11: Includes for the payment manager. 
*/
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_Payment.php";
include "../classes/DB_Applyweb/Table/class.DB_PaymentItem.php";
include '../classes/class.PaymentManager.php';
include '../inc/specialCasesAdmin.inc.php';

session_start();

/* 
* Kluge: Set otherwise unset session variables
* in order to make printViewData work.
*/
$luUsersUsertypesId = $_SESSION['userid']; 
if ( !isset($_SESSION['A_userid']) ) 
{
    $_SESSION['A_userid'] = -1;
    $_SESSION['A_allow_admin_edit'] = FALSE;
    $_SESSION['A_usertypeid'] = 5;    
}

include '../inc/printViewData.inc.php';

$domainid = 1;
if(isset($_SESSION['domainid']))
{
    if($_SESSION['domainid'] > -1)
    {
        $domainid = $_SESSION['domainid'];
    }
}
 
$invoiceNumber = '270364-' . $appid;
$studentName = $fName . ' ' . $lName;
$contentVars = array(
    array('invoice_number', $invoiceNumber),
    array('student_name', $studentName)
);

$paymentManager = new PaymentManager($appid);
$payments = $paymentManager->getPayments();

include '../inc/tpl.invoice.php';   
?>