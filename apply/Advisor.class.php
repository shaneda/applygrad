<?php
  require_once("../phplib/AjaxCore/AjaxCore.class.php");
  
  class Advisor extends AjaxCore
{        
     function Advisor()
     {
        $this->setup();
        parent::AjaxCore();
     }

     function setup()
     {
        $this->setMethod("POST");
        $this->setCurrentFile("Recommender.class.php");
        $this->setCache(false);
        $this->setDebug(false);
        $this->setUpdating("Updating");
        $this->setPlaceHolder("resultsDiv");
     }
}

new Advisor();
?>
