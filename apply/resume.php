<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php 
include_once '../inc/functions.php';
include_once '../inc/prev_next.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/header_prefix.php';
include '../inc/specialCasesApply.inc.php';
 
$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}

/*
* Check for special cases; 
*/
$isMseMsitDomain = isMseMsitDomain($_SESSION['domainid']);
$isMshciiDomain = isMshciiDomain($_SESSION['domainid']);
$isMsrtDomain = isMsrtDomain($_SESSION['domainid']);   
$isBICDomain = isBICDomain($_SESSION['domainid']); 
$isMsLSEDomain = isMsLSEDomain($_SESSION['domainid']);
$isHistoryDomain = isHistoryDomain($_SESSION['domainid']);
$isPsychologyDomain = isPsychologyDomain($_SESSION['domainid']);
$isPhilosophyDomain = isPhilosophyDomain($_SESSION['domainid']);
$isIniDomain = isIniDomain($_SESSION['domainid']);
$isIniKobeDomain = isIniKobeDomain($_SESSION['domainid']);
$isEM2Domain = isEM2Domain($_SESSION['domainid']);
$isDesignDomain = isDesignDomain($_SESSION['domainid']);
$isDesignPhdDomain = isDesignPhdDomain($_SESSION['domainid']);

$id = -1;
$err = "";
$resName = "";
$resSize = 0;
$resModDate = "";

$stateName = "";
$stateSize = "";
$stateModDate = "";
$resumeFileId = -1;
$statementFileId = -1;

$yearsExperience = '';

//RETRIEVE USER INFORMATION
$sql = "SELECT id, moddate, size, extension 
    FROM datafileinfo 
    WHERE user_id = " . $_SESSION['userid'] . " 
    AND section = 2
    AND userdata LIKE '" . $_SESSION['appid'] . "_%'";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
    if ($isHistoryDomain)
    {
        $resName  = "cv.".$row['extension'];    
    }
    else
    {
        $resName  = "resume.".$row['extension'];    
    }
    $resSize  = $row['size'];
    $resumeFileId = $row['id'];
    $resModDate = $row['moddate'];
} 

$sql = "SELECT id, moddate, size, extension 
    FROM datafileinfo 
    WHERE user_id = " . $_SESSION['userid'] . " 
    AND section = 4
    AND userdata LIKE '" . $_SESSION['appid'] . "_%'";
$result = mysql_query($sql) or die(mysql_error());

while($row = mysql_fetch_array( $result )) 
{
    $stateName  = "statement.".$row['extension'];
    $stateSize  = $row['size'];
    $statementFileId = $row['id'];
    $stateModDate = $row['moddate'];
} 

// used for finding the previous and next pages
$appPageList = getPagelist($_SESSION['appid']);
$curLocationArray = explode ("/", $currentScriptName);
$curPageIndex = array_search (end($curLocationArray), $appPageList);
if (0 <= $curPageIndex - 1) 
{
    $prevPage = $appPageList[$curPageIndex - 1]; 
}
else
{
    $prevPage = "";
}
if (sizeof ($appPageList) > $curPageIndex + 1)
{
    $nextPage =  $appPageList[$curPageIndex + 1]; 
} 
else
{
    $nextPage = "";
}
        
// Include the shared page header elements.
if ($isMsrtDomain || $isPhilosophyDomain || $isPsychologyDomain) 
{
    $pageTitle = 'Statement and Experience';    
}
elseif ($isHistoryDomain) 
{
    $pageTitle = 'Statement and C.V.';
} 
elseif ($domainid == 44 || $domainid == 49) 
{
    $pageTitle = 'Statement of Purpose';
} 
elseif ($isDesignPhdDomain) 
{
    $pageTitle = 'Experience';
}
else 
{
    $pageTitle = 'Resume and Statement';
}
include '../inc/tpl.pageHeaderApply.php';         

$domainid = 1;
if(isset($_SESSION['domainid']))
{
	if($_SESSION['domainid'] > -1)
	{
		$domainid = $_SESSION['domainid'];
	}
}

if ($domainid == 44 || $domainid == 49) {
    $sql = "select content from content where name='Statement of Purpose' and domain_id=".$domainid;
} else {
    $sql = "select content from content where name='Resume and Statement' and domain_id=".$domainid;
}
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result )) 
{
    echo html_entity_decode($row["content"]);
}

if ($_SESSION['domainid'] == 1 || $_SESSION['domainid'] == 3) {

    echo <<<EOF
    <br><br><br>
    <span>
    Your submitted materials will be incorporated into a database that can be searched by applicant reviewers. 
    Note that there are sometimes issues with generating the raw text used to create the search index from your 
    application materials, and, as a result, your application may not match the appropriate search queries.  
    If you wish to review the raw text generated from your materials to ensure that it reflects the contents of 
    application, please follow the "Review search text" link for each uploaded document.
    </span>
EOF;
}

?>

<span class="errorSubtitle" id="resumePageError"><?=$err;?></span>
<br>

<br>
<? showEditText("Save", "button", "btnSubmit", $_SESSION['allow_edit']); ?>
<br>
<hr size="1" noshade color="#990000">

<?php
if (!$isMsrtDomain && $domainid != 44 && !$isPhilosophyDomain) {
?>
<span class="subtitle"><?php echo ($isHistoryDomain ? 'C.V.' : 'Resume'); ?></span><span class="tblItem"><br>
<? 
$qs = "?id=1&t=2";
?>
<input class="tblItem" name="btnUpload" value="Upload <?php echo ($isHistoryDomain ? 'C.V.' : 'Resume'); ?>" 
    type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
<?
if($resModDate != "")
{
    showFileInfo($resName, $resSize, formatUSdate($resModDate), getFilePath(2, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
}//end if 
?>
<br><br>
</span>
<hr size="1" noshade color="#990000">
<?php
}

/*
* DIETRICH/INI
* SOP text inputs
*/
if (isIniDomain($domainid))
{
    include('../inc/iniSop.inc.php');    
}
elseif (isEM2Domain($domainid))
{
    include('../inc/em2Sop.inc.php');    
}
elseif (!$isDesignPhdDomain)
{
?>
<span class="subtitle">Statement of Purpose</span><span class="tblItem"><br> </span>
<? $qs = "?id=1&t=4"; ?>
<input class="tblItem" name="btnUpload" value="Upload Statement of Purpose" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
<? if($stateModDate != "")
{
	showFileInfo($stateName, $stateSize, formatUSdate($stateModDate), getFilePath(4, 1, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
}//end if ?>
<br><br>
<? 
}

/*
* Professional experience
*/
if ($isMseMsitDomain || $isMshciiDomain || $isMsrtDomain || $isBICDomain || $isMsLSEDomain) 
{
?>
<hr size="1" noshade color="#990000">
<span class="subtitle">Professional Experience</span><br>
<?php
if ($isMshciiDomain || $isMsLSEDomain) {

    $yearsExperienceId = NULL;
    $yearsExperience = '';

    $yearsExperienceQuery = "SELECT id, years_exp FROM experience
                                WHERE datafile_id IS NULL
                                AND experiencetype = 5 
                                AND application_id = " . $_SESSION['appid'];
    $yearsExperienceResult = mysql_query($yearsExperienceQuery);
    while ( $row = mysql_fetch_array($yearsExperienceResult) ) {
        $yearsExperienceId = $row['id'];
        if ($row['years_exp'] !== NULL) {
            $yearsExperience = $row['years_exp'];    
        }    
    }

    if( isset($_POST['btnSubmit']) ) {
        if ( isset($_POST['txtExperience']) && $_POST['txtExperience'] != '') {
            $postYearsExperience = mysql_real_escape_string( intval($_POST['txtExperience']) );        
        } else {
            $postYearsExperience = NULL;
        }
        if ($yearsExperienceId) {
            if ($postYearsExperience === NULL) {
                $addExperienceQuery = "UPDATE experience SET years_exp = NULL";    
            } else {
                $addExperienceQuery = "UPDATE experience SET years_exp = '" . $postYearsExperience . "'";
            }
            $addExperienceQuery .= " WHERE id = " . $yearsExperienceId;
        } else {
            $addExperienceQuery = "INSERT INTO experience (application_id, experiencetype, years_exp)
                                    VALUES (" . $_SESSION['appid'] . ", 5, ";
            if ($postYearsExperience === NULL) {
                $addExperienceQuery .= "NULL)";    
            } else {
                $addExperienceQuery .= "'" . $postYearsExperience . "')";
            }  
        }
        $addExperienceResult = mysql_query($addExperienceQuery);
        if ($addExperienceResult) {
            $yearsExperience = $postYearsExperience;       
        } 
    } elseif ( 1 == 1 ) {
        echo '';    
    }

    echo "How many years of professional experience do you have? &nbsp;";
    showEditText($yearsExperience, "textbox", "txtExperience", $_SESSION['allow_edit'], false, null, true, 2);
    ?>
    <br><br>
    If you would like to tell us more about your experience that is not in your resume, 
    please upload a detailed document.  The document must be submitted in PDF format. 
    Any other format will not be accepted. The maximum file size is 7MB.
<?  
}
else {
?>
    <!-- Only for Software Engineering Applicants and/or Societal Computing. -->
    Details related to your Professional Experience must be submitted in PDF<!--, MS Word or text --> format. 
    Any other format will not be accepted. The maximum file size is 7MB.
<? 
}
echo '<br>';
$qs = "?id=4&t=5";
if($_SESSION['allow_edit'] == 1)
{
?>
    <input class="tblItem" name="btnUpload" value="Upload Professional Experience" type="button" onClick="parent.location='fileUpload.php<?=$qs;?>'">
<?
}
$ex = getExperience($_SESSION['appid'], 4);
for($i = 0; $i < count($ex); $i++)
{
    showFileInfo("experience.".$ex[$i][4], $ex[$i][3], formatUSdate($ex[$i][2]), getFilePath(5, 4, $_SESSION['userid'], $_SESSION['usermasterid'], $_SESSION['appid']));
}
} // close conditional for MSE, MS-HCII, RI-MS-RT professional experience specifics


/*
* INI years experience 
*/
/*   DAS removed 7/18/2016
if ($isIniDomain)
{
?>
    <hr size="1" noshade color="#990000">
    <span class="subtitle">Professional Experience</span><br>
<?php
    include "../inc/iniYearsExperience.inc.php";
} 
*/  

/*
* Employment history
*/
if ($isMseMsitDomain || $isHistoryDomain || $isPsychologyDomain 
    || $isIniDomain || $isPhilosophyDomain || $isDesignDomain || $isDesignPhdDomain)
{
?>
    <hr size="1" noshade color="#990000">
    <a name="employment_history"></a>
<?php
    include "employment.php";
}   

if ($isDesignDomain || $isDesignPhdDomain)
{
?>
    <hr size="1" noshade color="#990000">
    <a name="teaching_experience"></a>
<?php
    include "teachingExperience.php";
}   
?>

<hr size="1" noshade color="#990000">
<? showEditText("Save", "button", "btnSubmit", $_SESSION['allow_edit']); ?>

<?php 
if ($isDesignDomain || $isDesignPhdDomain)
{
?>
<script src="https://code.jquery.com/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#btnAddEmp').click(function() {
        $('#form1').attr('action', '#employment_history');     
    });
    
    $('#btnDelEmp').click(function() {
        $('#form1').attr('action', '#employment_history');     
    });
    
    $('#btnAddTeachingExperience').click(function() {
        $('#form1').attr('action', '#teaching_experience');     
    });
    
    $('#btnDelTeachingExperience').click(function() {
        $('#form1').attr('action', '#teaching_experience');     
    });
});
</script>
<?php
}

// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?>