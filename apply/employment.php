<?
$myEmployments = array();
$err = "";
$itemErrors = array();
$comp = false;

function addEmployment($allowDup)
{
    $doAdd = true;
    global $err;
    if($allowDup == false)
    {
        $sql = "select * from experience where application_id = " . intval($_SESSION['appid']);
        $result = mysql_query($sql)    
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        while($row = mysql_fetch_array( $result )) 
        {
            $doAdd = false;
        }
    }
    $sql = "select * from experience where application_id = " . intval($_SESSION['appid']) . "
             and experiencetype = 5";
    $result = mysql_query($sql) 
        or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    $numEmps = mysql_affected_rows();
    if ($numEmps > 4) {
        $doAdd = false;
    }
    if($doAdd == true)
    {
        $sql = "insert into experience (application_id, experiencetype)
                values(" . intval($_SESSION['appid']) . ", 5)";
        mysql_query($sql)
            or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
    }
    else
    {
        $err .= "You cannot add more than 5 employment entries.";
    }
}

function saveEmployment ($vars) {
    global $myEmployments;
    global $err;
    global $itemErrors;
    
    $itemId = -1;
    $tmpid = -1;
    $arrItemId = array();
    
    //HERE WE GET THE RECORD IDS FOR THE UPDATED Employments
    foreach($vars as $key => $value)
    {
        $arr = split("_", $key);
        if($arr[0] == "txtId")
        {
            $tmpid = $arr[1];
            if($itemId != $tmpid)
            {
                $itemId = $tmpid;
                array_push($arrItemId, $itemId);
            }
        }
    }//END FOR
    
     //ITERATE THROUGH IDS AND DO UPDATES
    $localErr = "";
    for($i = 0; $i < count($arrItemId); $i++)
    {
        $itemError = '';
        $experienceId = $arrItemId[$i];
        $doUpdate = true;
        $company = trim(htmlspecialchars($_POST["lbName_".$arrItemId[$i]]));
        $start_date = $_POST["txtStartDate_".$arrItemId[$i]];
        $end_date = $_POST["txtEndDate_".$arrItemId[$i]];
        $years_exp = trim(htmlspecialchars($_POST["lbYrsExp_".$arrItemId[$i]]));
        
        // dietrich/ini fields
        $address = trim(htmlspecialchars($_POST["lbAddress_".$arrItemId[$i]]));
        $jobTitle = NULL;
        if (isset($_POST["lbJobTitle_".$arrItemId[$i]]))
        {
            $jobTitle = trim(htmlspecialchars($_POST["lbJobTitle_".$arrItemId[$i]]));
        }
        $jobDescription = trim(htmlspecialchars($_POST["lbJobDescription_".$arrItemId[$i]]));

        //POPULATE ARRAY FOR POSTBACK
        $arr = array();
        array_push($arr, $experienceId);
        if(isset($_POST["lbName_".$arrItemId[$i]])){array_push($arr, $_POST["lbName_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["txtStartDate_".$arrItemId[$i]])){array_push($arr, $_POST["txtStartDate_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["txtEndDate_".$arrItemId[$i]])){array_push($arr, $_POST["txtEndDate_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["lbYrsExp_".$arrItemId[$i]])){array_push($arr, $_POST["lbYrsExp_".$arrItemId[$i]]);}else{array_push($arr,"");}
        
        // dietrich/ini fields
        if(isset($_POST["lbAddress_".$arrItemId[$i]])){array_push($arr, $_POST["lbAddress_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["lbJobTitle_".$arrItemId[$i]])){array_push($arr, $_POST["lbJobTitle_".$arrItemId[$i]]);}else{array_push($arr,"");}
        if(isset($_POST["lbJobDescription_".$arrItemId[$i]])){array_push($arr, $_POST["lbJobDescription_".$arrItemId[$i]]);}else{array_push($arr,"");}
        
        array_push($arr, '');   // $arr[8] Empty error string (yes, it is an ugly, ugly kluge, but when in Rome...)
        array_push($myEmployments, $arr);
        
        //DO VERIFICATION
        if(checkEmploymentDate($start_date) == false)
        {
            $itemError .= "Start date is invalid<br>";
        }
        if(strtolower(trim($end_date)) != 'present'
            && checkEmploymentDate($end_date) == false)
        {
            $itemError .= "End Date is invalid<br>";
        }
        if($years_exp == '')
        {
            $itemError .= "You must enter number of years of experience.<br>";
        }
        
        // dietrich/ini fields
        $isHistoryDomain = isHistoryDomain($_SESSION['domainid']); 
        $isPsychologyDomain =  isPsychologyDomain($_SESSION['domainid']); 
        $isIniDomain = isIniDomain($_SESSION['domainid']);
        if ($isHistoryDomain || $isPsychologyDomain || $isIniDomain)
        {
            if ($address == '')
            {
                $itemError .= "You must enter an address.<br>";
            }
            
            if ($jobDescription == '')
            {
                $itemError .= "You must enter a job description.<br>";
            }
            
            if ($isIniDomain)
            {
                if ($jobTitle == '')
                {
                    $itemError .= "You must enter a job description.<br>";
                }    
            } 
        }
        
    
        if($itemError != "")
        {
            $itemErrors[$experienceId] = $itemError;
        } 
        elseif($doUpdate == true)
        {
            if (strtolower(trim($end_date)) == 'present') {
                $endDateString = 'present';
            } else {
                $endDateString = formatMySQLdate2($end_date,'/','-');    
            }
            
            $sql = "update experience set
            company= '" . mysql_real_escape_string($company) . " ', ";
            $sql .= "start_date= '" . formatMySQLdate2($start_date,'/','-') . "',
            end_date = '" . mysql_real_escape_string($endDateString) . "',
            years_exp = '" . mysql_real_escape_string($years_exp) . "',
            address = '" . mysql_real_escape_string($address) . "',
            job_title = '" . mysql_real_escape_string($jobTitle) . "',
            job_description = '" . mysql_real_escape_string($jobDescription) . "'
            where id = " . $arrItemId[$i];
            mysql_query($sql) 
                or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
        }
    }
}

function checkEmploymentDate($date)
{
    $ret = false;
    $dateArray = explode('/', $date);
    if(count($dateArray) == 2)
    {
        $matched = preg_match("#^(0[1-9]|1[012])/(19|20)([0-9]{2})$#", $date);
        if($date != "" &&  $matched != false)
        {
            $ret = true;
        }
    }
    return $ret;
}
    

//INSERT RECORD TABLE FOR USER 
if(isset($_POST['btnAddEmp']))
{
    $allowdups = true;
    saveEmployment($_POST);
    addEmployment($allowdups);
}//END POST BTNADD

function deleteEmployment ($vars) {

    $itemId = -1;
    
    foreach($vars as $key => $value)
    {
        if( strstr($key, 'btnDel') !== false ) 
        {
            $arr = split("_", $key);
            $itemId = $arr[1];
            
            $datafileid = -1;
            $type = -1;
            if($itemId != "")
            {   
                $sql = "select * from experience 
                    where application_id = " . intval($_SESSION['appid']) . " 
                    and id = " . intval($itemId);
                $result = mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
                
                $sql = "DELETE FROM experience WHERE id = " . intval($itemId);
                mysql_query($sql) 
                    or diePretty($_SERVER['SCRIPT_NAME'] . ': ' . mysql_error() . ': ' .  $sql);
            }
        }
    }//END FOR
}

// Delete
$dataSavedAfterDeletions = FALSE;
if( isset($_POST) && !isset($_POST['btnAddEmp']) )
{
    saveEmployment($_POST);
    deleteEmployment($_POST);
    $dataSavedAfterDeletions = TRUE;
}//END IF

//UPDATE
if( isset($_POST['btnSubmit']) && !$dataSavedAfterDeletions) {
    saveEmployment($_POST);
}

if(!isset($_POST['btnSubmit']))
{
  $myEmployments = getEmployments($_SESSION['appid']);  
    
} 
//end if not btnSave
if (isset($_POST['btnAddEmp'])) {
  $myEmployments = getEmployments($_SESSION['appid']); 
}
?>

<span class="subtitle">
Employment History
</span>
<br>
<br>
<? 
if(count($myEmployments) < 5){ 
showEditText("Add Employment", "button", "btnAddEmp", $_SESSION['allow_edit']);
} ?>
<br>            
<?
/*
* DIETRICH/INI
* Employment section for psychology, history
*/


for($i = 0; $i < count($myEmployments); $i++)
{
    $title = "";
    $myEmploymentId = $myEmployments[$i][0];
?>
    <hr size="1">
    <?php
    if (isHistoryDomain($_SESSION['domainid']) || isPsychologyDomain($_SESSION['domainid']) 
        || isPhilosophyDomain($_SESSION['domainid']))
    {
        include('../inc/dietrichEmployment.inc.php');
    }
    elseif (isIniDomain($_SESSION['domainid']) || isDesignDomain($_SESSION['domainid'])
        || isDesignPhdDomain($_SESSION['domainid']))
    {
        include('../inc/iniEmployment.inc.php');
    }
    else
    { 
    ?>   
    <table width="650" border="0" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <span class="subtitle"><?=$title;?> Employment</span><br>
          <span class="errorSubtitle itemError">
          <?php 
          if (isset($itemErrors[$myEmploymentId])) {
            echo $itemErrors[$myEmploymentId];    
          }
          ?>
          </span>
        </td>
               <td align="right">
                <? showEditText("Delete", "button", "btnDel_".$myEmployments[$i][0], $_SESSION['allow_edit']); ?>
                <input name="txtId_<?=$myEmployments[$i][0];?>" type="hidden" id="txtId_<?=$myEmployments[$i][0];?>" value="<?=$myEmployments[$i][0];?>">
                </td>
              </tr>
                </table>
            <table width="650" border="0" cellpadding="3" cellspacing="0" class="tblItem">
                  <tr>
                    <td  align="right"><strong>Company:</strong></td>
                    <td colspan="5">
                    <? showEditText(trim($myEmployments[$i][1]), "textbox", 
                            "lbName_".$myEmployments[$i][0], $_SESSION['allow_edit'], true, NULL, true, 80); 
                    ?>
                    </td>
                  </tr>
<!-- NEW ROW -->
                  <tr>
                    <td align="right"><strong>Start Date:</strong></td>
                    <td><? showEditText($myEmployments[$i][2], "textbox", "txtStartDate_".$myEmployments[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY)</td>              
                    <td  align="right"><strong>End Date 
                    <?  ?></strong><strong>:</strong></td>
                    <td><? showEditText($myEmployments[$i][3], "textbox", "txtEndDate_".$myEmployments[$i][0], 
                                                        $_SESSION['allow_edit'], true); ?> (MM/YYYY or 'present')</td>
                    
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
<!-- NEW ROW -->
                  <tr>
                    <td  align="right"><strong>Years of Experience</strong><strong>:</strong></td>
                    <td><? 
                    if($_SESSION['allow_edit'] == true)
                    {
                        showEditText($myEmployments[$i][4], "textbox", "lbYrsExp_".$myEmployments[$i][0], $_SESSION['allow_edit'],true); 
                    }
                    else
                    {
                        echo $myInstitutes[$i][21];  //das
                    }
                    ?>
                    </td>
                    <td align="right">
                    <?php
                    // hidden vars for dietrich/ini fields
                    showEditText(trim($myEmployments[$i][5]), "hidden", "lbAddress_".$myEmployments[$i][0], $_SESSION['allow_edit']);
                    showEditText(trim($myEmployments[$i][6]), "hidden", "lbJobTitle_".$myEmployments[$i][0], $_SESSION['allow_edit']);
                    showEditText(trim($myEmployments[$i][7]), "hidden", "lbJobDescription_".$myEmployments[$i][0], $_SESSION['allow_edit']);
                    ?>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr> 
            </table>   
    <?
        } // end include if
    }//end for loop 
?>