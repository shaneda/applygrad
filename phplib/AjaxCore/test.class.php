<?php
# include ajaxcore class
require_once("AjaxCore.class.php");
# extended test class
class AjaxTest extends AjaxCore
{
	# public javascript path
	public $JsPath;
	# construct
	function __construct()
	{	
		$this->setup();
		parent::AjaxCore();
	}
	# setup
	private function setup()
	{
		$this->setCurrentFile("test.class.php"); // extended class file
		$this->setPlaceHolder("results"); // placeholde element
		$this->setUpdating("Listening <img src='loading.gif'>"); // wait message
		$this->setMethod("POST"); // method
		$this->setCache(true); // caching?
	}
	# load core javascript
	public function LoadRequiredHead()
	{
		echo '<script type = "text/javascript" src = "'.$this->JsPath.'prototype.js" /></script>';
		echo '<script type = "text/javascript" src = "'.$this->JsPath.'AjaxCore.js" /></script>';
		echo $this->getJSCode();
	}
	######################################################
	# START LOGIN EXAMPLE
	######################################################
	# check is logged
	public function IsLogged()
	{
		sleep(1); // remove this in production
		if(isset($_SESSION['Logged_Administrator']) && $_SESSION['Logged_Administrator'])
		{
			$html='You \'re Logged, click here to <a href="#" onclick="'.$this->bindInline("Logout") .'">logout</a>';
			echo $this->htmlInner("results",$html);
		}
		else
		{
			$this->PopulateLoginForm();
		}
	}
	# populate form 
	private function PopulateLoginForm()
	{
		$user = (!empty($_REQUEST['user_form_field']))?$_REQUEST['user_form_field']:'';
		$html = '<form name="_login_frm" id="_login_frm" 
				 onsubmit="'.$this->bindInline("Login","user_form_field,pass_form_field").'">
				 <strong>Username:</strong><br />
				 <input class="textfield" type="text" name="user_form_field" id="user_form_field" value="'.$user.'" /><br />
				 <strong>Password:</strong><br />
				 <input class="textfield" type="password" name="pass_form_field" id="pass_form_field" /><br />				 
				 <input class="btnfield" type="submit" name ="user_login_button" id="user_login_button" value="Fire!" />
				 </form>
				 ';
		 echo $this->htmlInner("results",$html);
	}
	# login process
	public function Login()
	{
		sleep(1); // remove this in production
		$user = (!empty($_REQUEST['user_form_field']))?$_REQUEST['user_form_field']:'';
		$pass = (!empty($_REQUEST['pass_form_field']))?$_REQUEST['pass_form_field']:'';

		if($user=='ajax' && $pass='core')
		{
			$_SESSION['Logged_Administrator'] = true;
			$this->IsLogged();
		}
		else
		{
			echo $this->alert("Login Failed",false);
			$this->PopulateLoginForm();
		}
	}
	# logout process
	public function Logout()
	{
		sleep(1); // remove this in production
		$_SESSION['Logged_Administrator']=false;
		echo $this->alert("Logout Sucessfuly",false);
		$this->IsLogged();
	}

	######################################################
	# START WRITE NUMBER
	######################################################
	public function WriteNum()
	{
		sleep(1); // remove this in production
		for($i=1;$i<=100;$i++){echo ' &nbsp; '.$i;}
	}
	######################################################
	# START GREETING
	######################################################	
	public function WriteMsg()
	{
		sleep(1); // remove this in production
		$user = (!empty($_REQUEST['your_name_field']))?$_REQUEST['your_name_field']:'';
		$code=array();
		
		if(!empty($user))
		{
			$code[]=$this->alert("Welcome ".$this->escapeJS($user),false);	
		}

		$html = '<form name="_greet_frm" id="_greet_frm" onsubmit="'.$this->bindInline("WriteMsg","your_name_field").'">
				 <input class="textfield" type="text" name="your_name_field" id="your_name_field" value="'.$user.'" />
				 <input class="btnfield" type="submit" name ="user_login_button" id="user_login_button" value="Fire!" />
				 </form>';
				 
		 $code[]=$this->htmlInner("results",$html);
		 echo $this->arrayToString($code);		
	}	
	
}

new AjaxTest();
?>