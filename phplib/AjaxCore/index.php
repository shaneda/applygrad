<?php
# PHP CONFIG
ini_set("display_errors",1);
ini_set("register_globals",1);

error_reporting(E_ALL);
session_start();

#START AJAX CORE
require_once("test.class.php"); //include class AjaxTest
$ajax=new AjaxTest(); //create new instance

#USED BY LOGIN SAMPLE
if(!isset($_SESSION['Logged_Administrator']))$_SESSION['Logged_Administrator']=false;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ajax Prototype using AjaxCore - PHP Ajax Framework</title>
<link href="style.css" rel="stylesheet" media="all" />
<?php
#########################################
# Load Required Core Javascript
#########################################
$ajax->JsPath = ''; //set javascript path
$ajax->LoadRequiredHead(); // load
?>
</head>
<body>
<div id="intructions">
<h1>AjaxCore - Ajax PHP Framework Samples</h1>
<p>
These examples will show you how to implement <a href="http://www.ajaxcore.org/">AjaxCore</a> an Ajax PHP Framework which is based on <a href="http://www.prototypejs.org/">prototype</a> javascript library.</p>
<p>
There are 3 examples:
<ul>
<li>Simple ajax php : write number 1 - 100</li>
<li>Simple Ajax Post Form, create a greeting message based on input form</li>
<li>Simple Login Form, this will show you how to create ajax based login form<br />Login : ajax<br />Pass : core</li>
</ul>
</p>
<p><a href="http://www.chazzuka.com/blog/?p=166">Download Source Files</a></p>
</div>
<!-- place holder div -->
<div id="results">Click one of the buttons below</div>

<?php
########################################
# Write Number Sample
# Use bind method
########################################
$ajax->setJSCode("num_btn", $ajax->htmlDisable("num_btn"), $ajax->htmlEnable("num_btn")); // disable/enable button 
echo '<input class="btnfield" type = "button" id = "num_btn" name = "num_btn" value = "Write Number!"/>'; // button instance
echo $ajax->bind("num_btn", "onclick","WriteNum"); // assign action (bind method)

#######################################
# Greet Sample
# use form with bindInline method
#######################################
$ajax->setJSCode("msg_btn", $ajax->htmlDisable("msg_btn"), $ajax->htmlEnable("msg_btn")); //disable/enable button
echo '<input class="btnfield" type = "button" id = "msg_btn" name = "msg_btn" value = "Greet Me!" onclick="'.$ajax->bindInline("WriteMsg").'"/> '; //button with bindinline

#######################################
# Login Sample
#######################################
$ajax->setJSCode("login_btn", $ajax->htmlDisable("login_btn"), $ajax->htmlEnable("login_btn"));
echo '<input class="btnfield" type = "button" id = "login_btn" name = "login_btn" value = "Login!"/> ';
echo $ajax->bind("login_btn", "onclick","IsLogged");
?>
<div id="footer">created by <a href="http://www.chazzuka.com/" title="freelance web designer">chazzuka.com</a> - <a href="http://www.chazzuka.com/blog/?p=166">original article</a></div>
</body>
</html>
