<?php
/*
* WebIsoLogin: logic for handling WebISO authentication/authorization
* in tandem with the applyweb-based username/password functionality.
*/

// Just in case, include the session logic, 
// which also has the necessary config and db includes.
include_once '../inc/session_admin.php';

class WebIsoLogin
{
    
    private $webIso;        // string, e.g., pbergen@CS.CMU.EDU
    private $scsUserData;   // 2D array    
    private $usersData;     // 2D array
    private $status;        // boolean: TRUE on WebISO login and match in applyweb

    public function __construct() {
        
        $this->status = FALSE;
        
        // If the user has previously authenticated with WebISO
        // and has selected a login role from the menu,
        // set the session and send the user on his way; 
        if ( isset($_GET['luuId']) ) 
        {            
            if ( 
                    isset($_SERVER['REMOTE_USER']) || 
                    isset($_SERVER['HTTP_REMOTE_USER']) || 
                    isset($_SESSION['userWebIso']) || 
                    ( isset($_SESSION['A_email']) && $_SESSION['A_email'] != '' ) 
                ) 
            {
                $this->setSession($_GET['luuId']);
                $this->redirectToHome();
                return TRUE;               
            }
        }
        
        // Otherwise...
        
        // Start fresh with the role-related session vars
        unset($_SESSION['roleLuuId']);
        unset($_SESSION['roleDepartmentId']);
        unset($_SESSION['roleDepartmentName']);
        unset($_SESSION['A_usertypeid']);
        
        // Get the WebISO credentials from REMOTE_USER. 
        // Redirect the user to webIso login on failure.
        if ( isset($_SERVER['REMOTE_USER']) ) 
        {
            $this->webIso = $_SERVER['REMOTE_USER'];
            $_SESSION['userWebIso'] = $_SERVER['REMOTE_USER'];
        }
        elseif ( isset($_SERVER['HTTP_REMOTE_USER']) ) 
        {
            $this->webIso = $_SERVER['HTTP_REMOTE_USER'];
            $_SESSION['userWebIso'] = $_SERVER['HTTP_REMOTE_USER'];            
        } 
        elseif ( isset($_SESSION['userWebIso']) ) 
        {
            $this->webIso = $_SESSION['userWebIso'];     
        } else {
            //return FALSE;
            $this->redirectToLogin();
        }
        
        // Get the applyweb scsUserIds with the WebISO credentials.
        // Pass the user on to the "old" login screen on failure.
        $this->loadScsUserData();
        if ( !isset($this->scsUserData) ) {
            return FALSE;
            //$this->redirectToLogin();
        }
        
        // Get the applyweb lu_users_usertypes_id with the scsUserId.
        // Pass the user on to the "old" login screen on failure.        
        $this->loadUsersData();
        if ( isset($this->usersData) ) {
            $this->status = TRUE;
        } else {
            return FALSE;
            //$this->redirectToLogin();
        }

        return TRUE; 
    }

    public function getWebIso() {
        if ( isset($this->webIso) ) {
            return $this->webIso;     
        } else {
            return NULL;
        }
        
    } 
    
    public function getScsUserData() {
        return $this->scsUserData;
    }
    
    public function getUsersData() {
        return $this->usersData;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
    private function loadScsUserData() {
        
        $query = sprintf("SELECT * FROM users_remote_auth_string 
            WHERE users_remote_auth_string.remote_auth_string = '%s'",
            mysql_real_escape_string($this->webIso));        
        
        $result = mysql_query($query) or die(mysql_error());
        
        while ( $row = mysql_fetch_array($result) ) {
            $this->scsUserData[] = $row;
        }

        return TRUE;
    }
    
    private function loadUsersData() {
        
        $userIds = array();
        foreach ( $this->scsUserData as $scsUser ) {
            $userIds[] = $scsUser['users_id'];
        }
        $userIdSet = implode(',', $userIds);        
        
        $query = "SELECT lu_users_usertypes.*, 
            users.firstname, users.lastname, users.email, usertypes.name as usertypes_name, 
            lu_user_department.department_id, department.name as department_name  
            FROM users, 
            lu_users_usertypes
                LEFT JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
                LEFT JOIN department ON lu_user_department.department_id = department.id,
            usertypes 
            WHERE users.id IN (" . $userIdSet . ")
            AND users.id = lu_users_usertypes.user_id
            AND lu_users_usertypes.usertype_id = usertypes.id
            ORDER BY department.name, usertypes.name"; 
            
        $result = mysql_query($query);
        while ( $row = mysql_fetch_array($result) ) {
            $this->usersData[] = $row;
        }
        
        return TRUE;
    }
    

    /*
    * setSession() is copied almost verbatim from admin/index.php lines 103-179.
    * This should eventually be moved to a session handler class. 
    */
    private function setSession($lu_users_usertypes_id) {

        $_SESSION['A_allow_admin_edit'] = false;
        $_SESSION['A_allow_admin_edit_late'] = false;
        $_SESSION['A_allow_admin_edit']= false;

        $_SESSION['A_firstname'] = "";
        $_SESSION['A_lastname'] = "";
        $_SESSION['A_email'] = "";

        $_SESSION['A_usermasterid'] = -1;
        $_SESSION['A_userid'] = -1;
        $_SESSION['A_usertypeid'] = -1;
        $_SESSION['A_usertypename'] = "";
        $_SESSION['A_admin_domains'] = array(); // PLB added 9/16/09
        $_SESSION['A_admin_depts'] = array();
        
        // New session vars for roles
        $_SESSION['roleLuuId'] = -1;
        $_SESSION['roleDepartmentId'] = NULL;
        $_SESSION['roleDepartmentName'] = "";
        
        $sql = "(select
        users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email,
        lu_user_department.department_id as deptid,
        department.name as department_name,
        lu_domain_department.domain_id as domainid
        from users
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
        LEFT OUTER JOIN department ON lu_user_department.department_id = department.id
        left outer join lu_domain_department on lu_domain_department.department_id=lu_user_department.department_id
        where lu_users_usertypes.id = " . $lu_users_usertypes_id . ")";
        
        // PLB added unit query 9/15/09
        $sql .= " UNION (SELECT
        users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email,
        department.id as deptid,
        unit.unit_name as department_name,
        lu_domain_department.domain_id as domainid
        FROM users
        INNER JOIN lu_users_usertypes on lu_users_usertypes.user_id = users.id
        INNER JOIN usertypes on usertypes.id = lu_users_usertypes.usertype_id
        INNER JOIN unit_role on lu_users_usertypes.id = unit_role.lu_users_usertypes_id
        INNER JOIN unit ON unit_role.unit_id = unit.unit_id
        INNER JOIN domain_unit ON unit_role.unit_id = domain_unit.unit_id
        INNER JOIN lu_domain_department ON domain_unit.domain_id = lu_domain_department.domain_id
        INNER JOIN department ON lu_domain_department.department_id = department.id
        WHERE unit_role.lu_users_usertypes_id = " . $lu_users_usertypes_id . ")"; 
        
        
        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result ))
        {
            if ($row['typeid']==1 || $row['typeid']==0 || $row['typeid']==2 
                || $row['typeid']==4 || $row['typeid']==3 || $row['typeid']==10 
                || $row['typeid']==11 || $row['typeid']==12 || $row['typeid']==17
                || $row['typeid']== 18 || $row['typeid']== 19 || $row['typeid']== 20
                || $row['typeid']== 21)
            {
                $_SESSION['A_usermasterid'] = $row['id'];
                $_SESSION['A_userid'] = $row['uid'];
                $_SESSION['A_usertypeid'] = $row['typeid'];
                $_SESSION['A_usertypename'] = $row['typename'];
                $_SESSION['A_email'] = $row['email'];
                $_SESSION['A_firstname'] = $row['firstname'];
                $_SESSION['A_lastname'] = $row['lastname'];
                array_push($_SESSION['A_admin_domains'], $row['domainid']);
                array_push($_SESSION['A_admin_depts'], $row['deptid']);
                
                $_SESSION['roleLuuId'] = $row['uid'];
                $_SESSION['roleDepartmentId'] = $row['deptid'];
                $_SESSION['roleDepartmentName'] = $row['department_name'];
                
                /*
                $_SESSION['viewPeriods'] = 'current';
                $_SESSION['unit'] = NULL;
                $_SESSION['unitId'] = NULL;
                $_SESSION['periodId'] = NULL;
                */
                
                $_SESSION['A_allow_admin_edit'] = true;
                $_SESSION['A_allow_admin_edit_late'] = true;
                $_SESSION['A_allow_admin_edit']= true; // ?????????????
            }
            if ( $row['typeid'] == 4 )
            {
                $_SESSION['A_allow_admin_edit'] = false;
                $_SESSION['A_allow_admin_edit_late'] = false;
                $_SESSION['A_allow_admin_edit']= false; // ?????????????

            }
        }
        if($_SESSION['A_userid'] == -1)
        {
            $err .= "Invalid username or password for ".$_POST['txtEmail'].".<br>";
            return FALSE;
        }

        // New: set a variable so that home.php only display actions for a single role.
        $_SESSION['adminByRole'] = TRUE;
        
        return TRUE;      
    }

    private function redirectToLogin() {
        
        //header("Location: ../admin/");
        
        $viewApplication = "";
        if ( isset($_REQUEST['viewApplication']) && isset($_REQUEST['viewDepartment']) ) {
            $validApplicationId = filter_var($_REQUEST['viewApplication'], FILTER_VALIDATE_INT);
            $validDepartmentId = filter_var($_REQUEST['viewDepartment'], FILTER_VALIDATE_INT);
            if ($validApplicationId && $validDepartmentId) {
                $viewApplication = "?viewApplication=" . $validApplicationId;
                $viewApplication .= "&viewDepartment=" . $validDepartmentId;
            }     
        }
        
        header('Location: ../webIso/login.php' . $viewApplication);
        
        return TRUE;  
    }
    
    private function redirectToHome() {
        
        if ($_SESSION['A_usertypeid'] == 0)
        {
            header("Location: ../admin/home_su.php");    
        }
        else
        {
            header("Location: ../admin/home.php");    
        }
        
        return TRUE; 
    }

}
?>