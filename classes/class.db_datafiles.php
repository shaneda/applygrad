<?php

/*
Class for datafiles
Primary tables: datafileinfo
*/

class DB_Datafiles extends DB_Applyweb
{

    //protected $application_id;
    
    function getDatafiles($application_id) {
    
        $query = "SELECT datafileinfo.userdata,
        			case
                        when section = 1 then 'transcript'
                        when section = 2 then 'resume'
                        when section = 3 then 'recommendation'
                        when section = 4 then 'statement'
                        when section = 5 then 'experience'
                        when section = 6 then 'grescore'
                        when section = 7 then 'toefliscore'
                        end as filetype_name, 
                    datafileinfo.*, 
                    concat( 
                    	case
                        	when section = 1 then 'transcript'
                        	when section = 2 then 'resume'
                        	when section = 3 then 'recommendation'
                        	when section = 4 then 'statement'
                        	when section = 5 then 'experience'
                            when section = 6 then 'grescore'
                            when section = 7 then 'toefliscore'
                        	else section
                        end,
                    	'_', datafileinfo.userdata, '.', datafileinfo.extension 
                    ) as datafile
                    FROM application, datafileinfo                 
                    WHERE application.id = " . $application_id . "
                    AND datafileinfo.user_id = application.user_id";
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    // Update type, extension, size, and moddate for a converted datafile. 
    function updateConvertedDatafile($recommendation_id, $type, $extension, $size) {

        $datafile_query = "UPDATE datafileinfo SET type = '" . $type .  "', ";
        $datafile_query .= "extension = '" . $extension . "', ";
        $datafile_query .= "size = " . $size . ", ";
        $datafile_query .= "moddate = NOW() ";
        $datafile_query .= "WHERE id = '" . $recommendation_id . "'";        

        $status = $this->handleUpdateQuery($datafile_query);
        
        //return addslashes($datafile_query);
        return $status;        
    
    }
    
    // Get info for files that failed in the conversion/merge process
    function getFailedMergeFiles($application_id) {
    
         $query = "SELECT *
                    FROM failed_merges                 
                    WHERE application_id = " . $application_id;

        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }   
    
}

?>
