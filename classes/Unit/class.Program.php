<?php
/*
* Required class files:
*   classes/DB_Applyweb/class.DB_Applyweb.php
*   classes/DB_Applyweb/class.DB_Unit.php
*   classes/DB_Applyweb/class.DB_Program.php   
*   classes/DB_Applyweb/class.DB_ProgramType.php
*   classes/Unit/class.UnitBase.php 
*/

class Program
{
    private $unitBase;          // UnitBase object
    private $programTypeId;
    
    public function __construct($unitBase) {       
        
        $this->unitBase = $unitBase;
        if ( $this->unitBase->getId() ) {
            $this->loadFromDb();
            $this->unitBase->setProgramTypeId($this->programTypeId);    
        }
    }
    
    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ( $prefix == 'get') {
        
            if ($property == 'programTypeId') {
                return $this->programTypeId;    
            } else {
                return $this->unitBase->$method();
            }
        }
        
        if ( $prefix == 'set') {
            if ($property == 'programTypeId') {
                $this->programTypeId = $arguments[0];    
            } else {
                $this->unitBase->$method($arguments[0]);
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getProgramType() {
        
        $DB_ProgramType = new DB_ProgramType();
        $programTypeArray = $DB_ProgramType->get($this->programTypeId);
        
        if (isset($programTypeArray[0])) {
            return $programTypeArray[0]['program_type'];
        } else {
            return NULL;
        }
    } 
    
    public function isProgram() {
        return TRUE;
    }
    
    private function loadFromDb() {
        
        $DB_Program = new DB_Program();       
        $programArray = $DB_Program->get( $this->unitBase->getId() );
        $this->programTypeId = NULL;
        foreach ($programArray as $programRecord) {
            $this->programTypeId = $programRecord['program_type_id'];
        }
    }

    public function importValues($valueArray) {
        
        $this->unitBase->importValues($valueArray);

        if ( isset($valueArray['program_type_id']) ) {
            $this->programTypeId = $valueArray['program_type_id'];
        }
        
        return TRUE;        
    }

    public function exportValues() {
       
        $values = $this->unitBase->exportValues();
        $values['program_type_id'] = $this->programTypeId;
        
        return $values;        
    }
    
    public function save() {
        
        $this->unitBase->save();
        
        /*
        $DB_Program = new DB_Program();
        $DB_Program->save( array(
                'unit_id' => $this->unitBase->getId(),
                'program_type_id' => $this->programTypeId
                )
            );
        */
        
        return TRUE;
    }
}
?>