<?php
/*
* Required class files:
*   classes/DB_Applyweb/class.DB_Applyweb.php
*   classes/DB_Applyweb/class.DB_Unit.php
*   classes/DB_Applyweb/class.DB_Program.php 
*   classes/DB_Applyweb/class.DB_ProgramType.php   
*   classes/DB_Applyweb/class.DB_DomainUnit.php
*   classes/DB_Applyweb/class.DB_DepartmentUnit.php
*   classes/DB_Applyweb/class.DB_ProgramsUnit.php
*   classes/DB_Applyweb/class.DB_Period.php 
*   classes/Unit/class.UnitBase.php
*   classes/Unit/class.Program.php
*   classes/class.Period.php
*   classes/Unit/class.UnitBase.php
*   classes/Unit/class.Program.php 
*/



class Unit
{        
    private $unitBase;
    private $childUnitIds = array();
    private $unitDomainIds = array();       // Corresponding old domain id(s)
    private $unitDepartmentId;              // Corresponding old department id
    private $unitProgramId;                 // Corresponding old program id
    private $periodIds = array();           // 'umbrella' admission period ids 
    
    public function __construct($unitId = NULL, $unitType = NULL) {       

        if ($unitId) {

            $this->unitBase = new UnitBase($unitId);
            if ( $this->unitBase->isProgram() ) {
                $this->unitBase = new Program($this->unitBase);
            }
            $this->loadFromDb();
            
        } else {
            
            $this->unitBase = new UnitBase();
            if ($unitType && $unitType == 'program') {
                $this->unitBase = new Program($this->unitBase);    
            } 
        }
    }

    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ( $prefix == 'get' ) {               
            if ( property_exists($this, $property) ) {
                if ( isset($this->$property) ) {
                    return $this->$property;    
                } else {
                    return NULL;    
                }
            } else { 
                return $this->unitBase->$method();
            }   
        }
        
        if ( $prefix == 'set') {
            if ( property_exists($this, $property) ) {
                $this->$property = $arguments[0];
                return TRUE;
            } else {
                return $this->unitBase->$method($arguments[0]);
            }
        }
        
        return FALSE;
    }

    public function getParentUnit() {

        $parentUnitId = $this->unitBase->getParentUnitId();
        if ($parentUnitId) {
            return new Unit($parentUnitId);     
        } else {
            return NULL;
        }
    }
    
    public function getChildUnits() {
        
        $childUnits = array();
        foreach ($this->childUnitIds as $childUnitId) {
            $childUnits[] = new Unit($childUnitId);    
        }
        
        return $childUnits;
    }
    
    public function getDescendantUnitIds() {
        
        $descendantUnits = $this->getDescendantUnits();
        $descendantUnitIds = array();
        foreach($descendantUnits as $descendantUnit) {
            $descendantUnitIds[] = $descendantUnit->getId();    
        }
        
        return $descendantUnitIds;
    }

    public function getDescendantUnits() {
        
        $childUnits = $this->getChildUnits();
        $descendantUnits = array();
        foreach($childUnits as $childUnit) {
            $descendantUnits[] = $childUnit;
            $descendantUnits = array_merge( $descendantUnits, $childUnit->getDescendantUnits() );    
        }
        
        return $descendantUnits; 
    }
    
    public function isProgram() {
        
        return $this->unitBase->isProgram();
        
    }
    
    public function getProgramType() {
        
        if ( $this->isProgram() ) {
            return $this->unitBase->getProgramType();
        } else {
            return FALSE;
        }
        
    }

    public function getPrograms( $includeChildren = TRUE ) {       
        $programs = array();       
        if ( $this->isProgram() ) {
            $programs[$this->getId()] = $this;
        } else {
            if ($includeChildren) {
                foreach ($this->getChildUnits() as $childUnit) {
                    $programs = $programs + $childUnit->getPrograms();
                }    
            }           
        }
        return $programs;    
    }
    
    public function getActivePrograms($periodTypeId = 2, 
                        $includeChildren = TRUE, $includeParent = TRUE ) {
        
        $activePrograms = array();
        $activePeriods = $this->getActivePeriods($periodTypeId, $includeChildren, $includeParent);
        foreach ($activePeriods as $activePeriod) {
            $periodPrograms = $activePeriod->getPrograms();
            $activePrograms += $periodPrograms;
        }
        return $activePrograms;
    }    

    public function getPeriods() {
        
        $periods = array();
        foreach ($this->periodIds as $periodId) {
            $periods[] = new Period($periodId);
        }
        
        return $periods;
    }
    
    
    /*
    * Get the umbrella periods that are active at the level specified by the periodTypeId parameter
    * (default is 2 => 'application submission').   
    */
    public function getActivePeriods( $periodTypeId = 2, $includeChildren = TRUE, 
                        $includeParent = TRUE, $searchProgramIds = NULL) {
                                            
        $activePeriods = array();
        
        foreach ($this->getPeriods() as $period)  {            
            
            $periodId = $period->getId();
            
            if ($period->getPeriodTypeId() == $periodTypeId && $period->getStatus() == 'active') {
                    $activePeriods[$periodId] = $period;
                    continue;  
            }
            
            foreach ($period->getChildPeriods() as $subperiod) {
                if ($subperiod->getPeriodTypeId() == $periodTypeId 
                        && $subperiod->getStatus() == 'active') 
                {
                    $activePeriods[$periodId] = $period;
                    break;   
                } 
            }   
        }
        
        if ($includeChildren) {
            $childrenActivePeriods = array();
            foreach ($this->getChildUnits() as $childUnit) {
                $childActivePeriods = $childUnit->getActivePeriods($periodTypeId, FALSE, FALSE);
                $childrenActivePeriods = $childrenActivePeriods + $childActivePeriods;
            }
            $activePeriods = $activePeriods + $childrenActivePeriods;
        } 
        
        if ($includeParent) {            
            // Get program ids of unit of interest to be passed through recursion.
            if (!$searchProgramIds) {
                $searchProgramIds = array();
                foreach ( $this->getPrograms() as $program ) {
                    $searchProgramIds[] = $program->getId();    
                }
            }

            $parentUnit = $this->getParentUnit();
            if ($parentUnit) {
                $parentActivePeriods = 
                    $parentUnit->getActivePeriods($periodTypeId, FALSE, TRUE, $searchProgramIds);
                foreach ($parentActivePeriods as $parentPeriod) {                    
                    /*
                    $periodProgramIds = array();
                    foreach ( $parentPeriod->getPrograms() as $parentProgram ) {
                        $periodProgramIds[] = $parentProgram->getId();    
                    }
                    */
                    $periodProgramIds = array_keys( $parentPeriod->getPrograms() );
                    foreach ($searchProgramIds as $programId) {                       
                        if ( in_array($programId, $periodProgramIds) ) {                  
                            if ( !in_array($parentPeriod, $activePeriods) ) {
                                $periodId = $parentPeriod->getId();
                                $activePeriods[$periodId] = $parentPeriod;    
                            }         
                        }
                    }
                }   
            }               
        }
            
        return $activePeriods;
    }
    
    private function loadFromDb() {
        
        $unitId = $this->getId();

        $DB_Unit = new DB_Unit();
        $childUnitArray = $DB_Unit->find($unitId);
        foreach ($childUnitArray as $childUnitRecord) {
            $this->childUnitIds[] = $childUnitRecord['unit_id'];    
        }

        $DB_DomainUnit = new DB_DomainUnit();
        $domainUnitArray = $DB_DomainUnit->find(NULL, $unitId);
        foreach ($domainUnitArray as $domainUnitRecord) {
            $this->unitDomainIds[] = $domainUnitRecord['domain_id'];    
        }

        $DB_DepartmentUnit = new DB_DepartmentUnit();
        $departmentUnitArray = $DB_DepartmentUnit->find(NULL, $unitId);
        foreach ($departmentUnitArray as $departmentUnitRecord) {
            $this->unitDepartmentId = $departmentUnitRecord['department_id'];    
        }        
        
        $DB_ProgramsUnit = new DB_ProgramsUnit();
        $programsUnitArray = $DB_ProgramsUnit->find(NULL, $unitId);
        foreach ($programsUnitArray as $programsUnitRecord) {
            $this->unitProgramId = $programsUnitRecord['programs_id'];    
        }   

        $DB_Period = new DB_Period();
        $periodArray = $DB_Period->find($unitId, NULL, 1);
        foreach ($periodArray as $periodRecord) {
            $this->periodIds[] = $periodRecord['period_id'];    
        }
    
    }
     
    public function importValues($valueArray) {
        
        $this->unitBase->importValues($valueArray);

        $fields = array(
            'unit_domain_ids' => 'unitDomainIds',
            'unit_department_id' => 'unitDepartmentId',
            'unit_programs_id' => 'unitProgramId'
        );
        
        foreach ($fields as $key => $field) {
            if ( isset($valueArray[$key]) ) {
                $this->$field = $valueArray[$key];
            }
        }
        
        return TRUE;        
    }

    public function exportValues() {
    
        $values = $this->unitBase->exportValues();
        $values['unit_domain_ids'] = $this->unitDomainIds;
        $values['unit_department_id'] = $this->unitDepartmentId;
        $values['unit_programs_id'] = $this->unitProgramId;

        return $values;        
    }
    
    public function save() {
        
        $this->unitBase->save();
        
        $unitId = $this->getId();

        $DB_DomainUnit = new DB_DomainUnit();
        $DB_DomainUnit->delete(NULL, $unitId);
        foreach ($this->unitDomainIds as $domainId) {
            $DB_DomainUnit->save( array('domain_id' => $domainId, 'unit_id' => $unitId) );
        }
        
        
        $DB_DepartmentUnit = new DB_DepartmentUnit();
        $DB_DepartmentUnit->delete(NULL, $unitId);
        if ($this->unitDepartmentId) {
            $DB_DepartmentUnit->save( array(
                    'department_id' => $this->unitDepartmentId, 
                    'unit_id' => $unitId) 
                );
        }

        $DB_ProgramsUnit = new DB_ProgramsUnit();
        $DB_ProgramsUnit->delete(NULL, $unitId);
        if ($this->unitProgramId) {
            $DB_ProgramsUnit->save( array(
                    'programs_id' => $this->unitProgramId, 
                    'unit_id' => $unitId) 
                );
        }
        
        return TRUE;
    }


}
?>