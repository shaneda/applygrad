<?php
/*
* Required class files:
*   classes/DB_Applyweb/class.DB_Applyweb.php
*   classes/DB_Applyweb/class.DB_Unit.php
*/

class UnitBase
{    
    
    private $DB_Unit;
    //private $DB_Program;
    
    private $id;
    private $name;
    private $nameShort;
    private $description;
    private $url;
    private $oracleString;
    private $systemEmail;
    private $ccEmail;
    private $applicationBasePrice;
    private $applicationProgramPrice;
    private $programTypeId = NULL;
    private $parentUnitId;
    
    public function __construct($unitId = NULL) {       
        
        $this->DB_Unit = new DB_Unit();
        $this->DB_Program = new DB_Program();
        
        $this->id = $unitId;
        if ($unitId) {
            $this->loadFromDb();            
        }
    }
    
    public function __isset($name) {
        return isset($this->$name);
    }

    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ($prefix == 'get' && property_exists($this, $property)) {
            if ( isset($this->$property) ) {
                return $this->$property;    
            } else {
                return NULL;
            }
        }
        
        if ( $prefix == 'set' && property_exists($this, $property) ) {
            $this->$property = $arguments[0];
            return TRUE;
        }
        
        return FALSE;
    }

    public function isProgram() {
        
        /*
        $childUnitArray = $this->DB_Unit->find($this->id);
        if ( count($childUnitArray) == 0 ) {
            return TRUE;
        } else {
            return FALSE;
        }
        */
        
        $programArray = $this->DB_Program->get($this->id);
        if ( count($programArray) > 0 ) {
            return TRUE;
        } else {
            return FALSE;
        }
        
    }

    private function loadFromDb() {
               
        $unitArray = $this->DB_Unit->get($this->id);
        foreach ($unitArray as $unitRecord) {
            $this->name = $unitRecord['unit_name'];
            $this->nameShort = $unitRecord['unit_name_short'];
            $this->description = $unitRecord['unit_description'];
            $this->url = $unitRecord['unit_url'];
            $this->oracleString = $unitRecord['unit_oracle_string'];
            $this->systemEmail = $unitRecord['unit_system_email'];
            $this->ccEmail = $unitRecord['unit_cc_email'];
            $this->applicationBasePrice = $unitRecord['application_base_price'];
            $this->applicationProgramPrice = $unitRecord['application_program_price'];
            if ($unitRecord['parent_unit_id']) {
                $this->parentUnitId = $unitRecord['parent_unit_id'];
            } else {
                $this->parentUnitId = NULL;    
            }
        }
        
        return TRUE;
    }

    public function importValues($valueArray) {
        
        $fields = array(
            'unit_id' => 'id',
            'unit_name' => 'name',
            'unit_name_short' => 'nameShort',
            'unit_description' => 'description',
            'unit_url' => 'url',
            'unit_oracle_string' => 'oracleString',
            'unit_system_email' => 'systemEmail',
            'unit_cc_email' => 'ccEmail',
            'application_base_price' => 'applicationBasePrice',
            'application_program_price' => 'applicationProgramPrice',
            'program_type_id' => 'programTypeId',
            'parent_unit_id' => 'parentUnitId'
        );
        
        foreach ($fields as $key => $field) {
            if ( isset($valueArray[$key]) ) {
                if ($valueArray[$key] === '') {
                    $this->$field = NULL;    
                } else {
                    $this->$field = $valueArray[$key];     
                } 
            }
        }
        
        return TRUE;        
    }

    public function exportValues() {
       
        $values = array(
            'unit_id' => $this->id,
            'unit_name' => $this->name,
            'unit_name_short' => $this->nameShort,
            'unit_description' => $this->description,
            'unit_url' => $this->url,
            'unit_oracle_string' => $this->oracleString,
            'unit_system_email' => $this->systemEmail,
            'unit_cc_email' => $this->ccEmail,
            'application_base_price' => $this->applicationBasePrice,
            'application_program_price' => $this->applicationProgramPrice,
            'parent_unit_id' => $this->parentUnitId
        );
        
        return $values;        
    }
    
    public function save() {
        
        $this->id = $this->DB_Unit->save( $this->exportValues() );

        $DB_Program = new DB_Program();
        if ($this->programTypeId) {
            $DB_Program->save( array(
                'unit_id' => $this->id,
                'program_type_id' => $this->programTypeId
                )    
            );    
        } else {
            $DB_Program->delete($this->id);            
        }
        
        return TRUE;
    }

}
?>