<?php

/*
Class for recommendations
Primary tables: recommend, users, users_info, datafileinfo
*/

class DB_Recommendations extends DB_Applyweb
{

    //protected $application_id;
    
    function getAll($application_id) {
    
        $query = "SELECT recommend.*, 
                    lu_users_usertypes.id as uid, 
                    users.id as usermasterid,
                    users.title,
                    users.firstname, 
                    users.lastname,
                    users.email, 
                    users_info.address_perm_tel,
                    users_info.company, 
                    reminder_sent_count,
                    datafileinfo.type,
                    datafileinfo.extension,
                    datafileinfo.size,
                    datafileinfo.user_id,
                    datafileinfo.section,
                    datafileinfo.moddate,
                    datafileinfo.userdata,
                    applicants_usertypes.id as applicant_id,
                    applicants.guid
                    FROM recommend
                    left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
                    left outer join users on users.id = lu_users_usertypes.user_id
                    left join users_info on users_info.user_id = lu_users_usertypes.id
                    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id                    
                    left outer join application on recommend.application_id = application.id
                    left outer join lu_users_usertypes as applicants_usertypes on applicants_usertypes.id = application.user_id 
                    left outer join users as applicants on applicants.id = applicants_usertypes.user_id                    
                    WHERE recommend.application_id = " . $application_id . 
                    " ORDER BY users.lastname";
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }


    function addFile($recommendation_id, $type, $extension, 
                        $size, $applicant_luu_id, $section, $user_data) {

        $datafile_query = "INSERT INTO datafileinfo (type, extension, size, user_id, section, moddate, userdata)";
        $datafile_query .= " VALUES ('" . $type . "', '" . $extension;
        $datafile_query .= "', " . $size . ", " . $applicant_luu_id . ", " . $section . ", NOW(), '" . $user_data . "')";

        $status = $this->handleInsertQuery($datafile_query);
        
        if ( isset(self::$mysqli) ) {
            $datafile_id = self::$mysqli->insert_id;    
        } else {
            $datafile_id = self::$dbh->insert_id;    
        }

        if ( $status ) {
      
            $recommendation_query = "UPDATE recommend SET datafile_id = "
                                    . $datafile_id .
                                    ", submitted = 1
                                     WHERE id = "
                                    . $recommendation_id;
            
            $status = $this->handleUpdateQuery($recommendation_query);
        }
        
        //return addslashes($recommendation_query);
        return $status;        
    
    }

    
    function updateFile($recommendation_id, $type, $extension, 
                        $size, $applicant_luu_id, $section, $user_data) {

        $datafile_query = "UPDATE datafileinfo SET type = '" . $type .  "', ";
        $datafile_query .= "extension = '" . $extension . "', ";
        $datafile_query .= "size = " . $size . ", ";
        $datafile_query .= "user_id = ". $applicant_luu_id . ", ";
        $datafile_query .= "section = " . $section . ", ";
        $datafile_query .= "moddate = NOW() ";
        $datafile_query .= "WHERE userdata = '" . $user_data . "'";        

        $status = $this->handleUpdateQuery($datafile_query);
        
        //return addslashes($datafile_query);
        return $status;        
    
    }    
    
    
    
}

?>
