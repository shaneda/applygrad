<?php
class FileDownload
{
    private $fileName;
    private $guid;
    private $fileType;
    private $fileTypeIndex;
    private $fileApplicationId;
    private $fileUserData;
    private $fileLuuId;
    private $filePath;
    private $datafileinfo = array();
    
    public function __construct($fileName, $guid) {
    
        $this->fileName = $fileName;
        $this->guid = $guid;
        $this->parseFileName();
        
        $this->setFileLuuId();
        if (!$this->fileLuuId) {
            throw new Exception("file not found", 404);    
        }
        
        $this->setFilePath();
        if (!file_exists($this->filePath)) {
            throw new Exception("file " . $this->fileName . " not found", 404);   
        }
        
        $this->setDatafileinfo();
        if ($this->fileType != 'merged' && $this->fileType != 'admissionLetter') {
            if ($this->guid != $this->datafileinfo['guid'] 
                || $this->fileLuuId != $this->datafileinfo['luuId']) 
            {
                throw new Exception("file " . $this->filePath . " not found", 404);    
            }   
        }
    }
    
    private function parseFileName() {
    
        $fileNameExtensionArray = explode('.', $this->fileName);
        $fileName = $fileNameExtensionArray[0];
        $fileNameArray = explode('_', $fileName);
         
        if ($fileNameArray[1] == 'merged') {
            $this->fileType = 'merged';
            $this->fileApplicationId = $fileNameArray[0];
            $this->fileUserData = NULL;
        } elseif ($fileNameArray[0] == 'admissionLetter') {
            $this->fileType = 'admissionLetter';
            $this->fileApplicationId = $fileNameArray[1];
            $this->fileUserData = NULL;
        } else {
            $this->fileType = $fileNameArray[0];
            $this->fileApplicationId = $fileNameArray[1];
            $this->fileUserData = $this->fileApplicationId . '_' . $fileNameArray[2];
        }
        
        $fileTypes = $this->getFileTypes();
        $fileTypeIndex = array_search($this->fileType, $fileTypes);
        if($fileTypeIndex) {
            $this->fileTypeIndex = $fileTypeIndex;    
        } else {
            $this->fileTypeIndex = NULL;    
        }
        
        return TRUE;         
    }
    
    private function setFileLuuId() {
        
        $this->fileLuuId = NULL;
        
        $fileLuuQuery = "SELECT lu_users_usertypes.id 
            FROM lu_users_usertypes
            INNER JOIN users ON lu_users_usertypes.user_id = users.id
            INNER JOIN application ON lu_users_usertypes.id = application.user_id
            WHERE users.guid = '" . mysql_real_escape_string($this->guid) . "'
            AND application.id = '" . mysql_real_escape_string($this->fileApplicationId) . "'";
        $fileLuuResult = mysql_query($fileLuuQuery);
        while ( $row = mysql_fetch_array($fileLuuResult) ) {
            $this->fileLuuId = $row['id'];
        }
        
        return TRUE;        
    }
    
    private function setFilePath() {

        global $datafileroot;

        if ($this->fileType == 'merged' || $this->fileType == 'admissionLetter') {
            $fileTypeDirectory = '/';     
        } else {
            $fileTypeDirectory = '/' . $this->fileType . '/';
        }         
        $this->filePath = $datafileroot . '/' . $this->guid . $fileTypeDirectory . $this->fileName;
        
        return TRUE;    
    }
    
    private function setDatafileinfo() {

        $this->datafileinfo['id'] = NULL;
        $this->datafileinfo['luuId'] = NULL;
        $this->datafileinfo['contentType'] = NULL;
        $this->datafileinfo['size'] = NULL;
        $this->datafileinfo['typeIndex'] = NULL;
        $this->datafileinfo['extension'] = NULL;
        $this->datafileinfo['moddate'] = NULL;
        $this->datafileinfo['userdata'] = NULL;
        $this->datafileinfo['guid'] = NULL;
        
        $datafileinfoQuery = "SELECT datafileinfo.*, users.guid 
            FROM datafileinfo
            INNER JOIN lu_users_usertypes ON datafileinfo.user_id = lu_users_usertypes.id
            INNER JOIN users ON lu_users_usertypes.user_id = users.id
            WHERE section = '" . mysql_real_escape_string($this->fileTypeIndex) . "'
            AND userdata = '" . mysql_real_escape_string($this->fileUserData) . "'";
        $datafileinfoResult = mysql_query($datafileinfoQuery);
        
        while ( $row = mysql_fetch_array($datafileinfoResult) ) {
            $this->datafileinfo['id'] = $row['id'];
            $this->datafileinfo['luuId'] = $row['user_id'];
            $this->datafileinfo['contentType'] = $row['type'];
            $this->datafileinfo['size'] = $row['size'];
            $this->datafileinfo['typeIndex'] = $row['section'];
            $this->datafileinfo['extension'] = $row['extension'];
            $this->datafileinfo['moddate'] = $row['moddate'];
            $this->datafileinfo['userdata'] = $row['userdata'];
            $this->datafileinfo['guid'] = $row['guid'];
        }
        
        return TRUE; 
    }
  
    public function __get($property) {

        // http://us2.php.net/manual/en/language.oop5.overloading.php
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $property .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }
    

            
    public function serve() {
        
        //http://stackoverflow.com/questions/1597732/php-force-file-download-and-ie-yet-again
        
        $contentType = shell_exec("file -b -i " . escapeshellarg($this->filePath));
        $lastModified = date('r', filemtime($this->filePath));            

        header('Content-Type: ' . $contentType);
        header('Last-Modified: ' . $lastModified);
        header('Content-Disposition: filename=' . basename($this->filePath));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($this->filePath));
        ob_clean();
        flush();
        readfile($this->filePath);
        exit;
    }
    
    private function getFileTypes() {
        
        $fileTypes = array(
            0 => 'merged',
            1 => 'transcript',
            2 => 'resume',
            3 => 'recommendation',
            4 => 'statement',
            5 => 'experience',
            6 => 'grescore',
            7 => 'toefliscore',
            8 => 'gresubjectscore',
            9 => 'toeflcscore',
            10 => 'toeflpscore',
            11 => 'Multi-wayANOVACourse',
            12 => 'Multi-factorregressionCourse',
            13 => 'Single-wayANOVACourse',
            14 => 'Single-factorregressionCourse',
            15 => 'DesignCourse',
            16 => 'ProgrammingCourse',
            17 => 'CodeSample',
            18 => 'ProgrammingTest',
            19 => 'ieltsscore',
            20 => 'gmatscore',
            21 => 'PaymentVoucher',
            22 => 'awardletter',
            23 => 'writingsample',
            24 => 'speakingsample',
            25 => 'langprofrecommendation',
            26 => 'publication',
            27 => 'financialsupport',
            28 => 'researchproposal',
            29 => 'biographicalessay'
        );        
        
        return $fileTypes;
    }
    
}


/*
http://us3.php.net/manual/en/function.readfile.php
$file = 'monkey.gif';

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    exit;
}
*/

/*
http://www.satya-weblog.com/2007/05/php-file-upload-and-download-script.html
// your file to upload
$file = '2007_SalaryReport.pdf';
$content = fopen ($hn, $file);
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Content-type: application/pdf");
// tell file size
header('Content-length: '.filesize($file));
// set file name
header('Content-disposition: attachment; filename='.basename($file));
readfile($file);
// Exit script. So that no useless data is output-ed.
exit;
?>
*/

?>