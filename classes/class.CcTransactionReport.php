<?php
/*
* CcTransactionReport: get file info and extract data
* from a CCPS daily transaction report.
* 
* See http://www.cmu.edu/computing/web/credit/database.html
* for more info about transaction reports, including a 
* complete list of report records/fields.
* 
* Selected summary record fields:
* [1] => cc_id
* [2] => date
* [3] => time
* [6] => merchant ref no
* [7] => cardholder last name
* [8] => cardholder first name
* [21] => auth_code
* [22] => auth_msg
* [23] => settle_code
* [24] => settle_msg
* [25] => auth_amount
* [26] => settle_amount
* [27] => credit_amount
* 
* Selected detail record fields:
* [1] => cc_id
* [2] => transaction_type
* [3] => item_name
* [5] => item_qty
* [6] => item_price_each
* [7] => item_gl_string
*/

class CcTransactionReport
{    
    private $fullPathToFile;
    private $fileName;
    private $fileSize;
    private $fileModDate;
    private $fileContents;
    
    public function __construct($fullPathToFile = NULL) {
        
        if ($fullPathToFile) {
            
            $this->fullPathToFile = $fullPathToFile;
            $this->extractMetadata();   
        } 
    }
    
    private function extractMetadata() {
        
        if ($this->fullPathToFile && file_exists($this->fullPathToFile)) {
            
            $this->fileName = basename($this->fullPathToFile);
            $this->fileSize = filesize($this->fullPathToFile);
            $this->fileModDate = filemtime($this->fullPathToFile);
            $this->fileContents = file_get_contents($this->fullPathToFile);
            
        } else {
            
            $this->fileName = NULL;
            $this->fileSize = NULL;
            $this->fileModDate = NULL;
            $this->fileContents = NULL;            
        }
    }
    
    public function getFileName() {
        return $this->fileName;    
    }
    
    public function setFileName($fileName) {
        $this->fileName = $fileName;    
    }
    
    public function getFileSize() {
        return $this->fileSize;    
    }
    
    public function setFileSize($fileSize) {
        $this->fileSize = $fileSize;    
    }
    
    public function getFileModDate() {
        return $this->fileModDate;    
    }

    public function setFileModDate($fileModDate) {
        $this->fileModDate = $fileModDate;    
    }
        
    public function getFileContents() {
        return $this->fileContents;    
    }
    
    public function setFileContents($fileContents) {
        $this->fileContents = $fileContents;    
    }
    
    public function getFileContentsArray() {
        
        $contents = array();
        $lines = explode("\n", $this->fileContents);
        foreach ($lines as $line) {
            $contents[] = str_getcsv($line);    
        }
        
        return $contents;    
    }

    public function getMetadata() {
        
        $reportMetadata = array(
            'filename' => $this->fileName,
            'size' =>  $this->fileSize,
            'last_mod_date' => date('Y-m-d H:i:s', $this->fileModDate)
        );
        
        return $reportMetadata;    
    }
    
    public function getData() {
        
        $reportData = array(
            'head' => array('report_date' => NULL, 'store_number' => NULL),
            'summary_records' => array(),
            'detail_records' => array()
        );
        
        $contentsArray = explode("\n", $this->fileContents);
        
        foreach ($contentsArray as $line) {
            
            $lineArray = str_getcsv($line);
            
            switch ($lineArray[0]) {

                case 'HEAD':

                    $reportDate = date('Y-m-d', strtotime($lineArray[1]));
                    $reportData['head']['report_date'] = $reportDate;
                    $reportData['head']['store_number'] = $lineArray[2];
                    break;

                case 'SUMMARY':

                    $reportData['summary_records'][] = $lineArray;
                    break;    

                case 'DETAIL':
                
                    $ccId = $lineArray[1];
                    $reportData['detail_records'][$ccId][] = $lineArray;
                    break;
                
                default:
                    continue;
            }        
        }

        return $reportData;  
    }
    
}

?>