<?php
/*
* Class for application payment data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Payment2.php';
* require_once 'DB_Applyweb/Table/class.DB_PaymentItem2.php';
* require_once 'DB_Applyweb/Table/class.DB_PaymentVoucher2.php';
* require_once 'DB_Applyweb/Table/class.DB_CcPaymentStatus2.php';
*/

class ApplicationData2_PaymentData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_Payment;
    private $DB_PaymentItem;
    private $DB_PaymentVoucher;

    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_Payment = new DB_Payment2($this->DB_Applyweb2);
        $this->DB_PaymentItem = new DB_PaymentItem2($this->DB_Applyweb2);
        $this->DB_PaymentVoucher = new DB_PaymentVoucher2($this->DB_Applyweb2);
        $this->DB_CcPaymentStatus = new DB_CcPaymentStatus2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $paymentDataRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'payment' => array(),                   // 2D, unindexed
            'payment_item' => array(),              // 3D, indexed by payment_id
            'payment_voucher' => array(),           // 3D, indexed by payment_id
            'cc_payment_status' => array()          // 3D, indexed by payment_id
        );

        $paymentDataRecords['payment'] = $this->DB_Payment->find($applicationId);
        
        foreach ($paymentDataRecords['payment'] as $paymentRecord) {
            
            $paymentId = $paymentRecord['payment_id'];
            
            $paymentDataRecords['payment_item'][$paymentId] = 
                $this->DB_PaymentItem->find($paymentId);
                
            $paymentDataRecords['payment_voucher'][$paymentId] = 
                $this->DB_PaymentVoucher->find($paymentId);
                
            $paymentDataRecords['cc_payment_status'][$paymentId] = 
                $this->DB_CcPaymentStatus->find($paymentId);
        }
        
        return $paymentDataRecords;
    }
}
?>
