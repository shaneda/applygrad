<?php
/*
* Class for education-related application data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Usersinst2.php';
*/

class ApplicationData2_EducationData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_Usersinst;
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_Usersinst = new DB_Usersinst2($this->DB_Applyweb2);;
    }
    
    public function export($applicationId) {

        $educationRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'usersinst' => array(),           // 2D, unindexed
        );

        $educationRecords['usersinst'] = $this->DB_Usersinst->find($applicationId);
        
        return $educationRecords;
    }
}
?>
