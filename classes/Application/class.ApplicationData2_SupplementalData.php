<?php
/*
* Class for other, supplemental application data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Experience2.php';
* require_once 'DB_Applyweb/Table/class.DB_Fellowships2.php';
* require_once 'DB_Applyweb/Table/class.DB_Publication2.php';
*/

class ApplicationData2_SupplementalData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_Experience;
    private $DB_Fellowships;
    private $DB_Publication;
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_Experience = new DB_Experience2($this->DB_Applyweb2);
        $this->DB_Fellowships = new DB_Fellowships2($this->DB_Applyweb2);
        $this->DB_Publication = new DB_Publication2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $supplementalDataRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'experience' => array(),             // 2D, unindexed
            'fellowships' => array(),            // 2D, unindexed
            'publication' => array()             // 2D, unindexed
        );

        $supplementalDataRecords['experience'] = $this->DB_Experience->find($applicationId);
        
        $supplementalDataRecords['fellowships'] = $this->DB_Fellowships->find($applicationId);
        
        $supplementalDataRecords['publication'] = $this->DB_Publication->find($applicationId);
        
        return $supplementalDataRecords;
    }
}
?>
