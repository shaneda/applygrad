<?php
/*
* Class for application review data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_PromotionStatus2.php';
* require_once 'DB_Applyweb/Table/class.DB_PromotionHistory2.php';
* require_once 'DB_Applyweb/Table/class.DB_Review2.php';
* require_once 'DB_Applyweb/Table/class.DB_ReviewPositiveFactor2.php';
* require_once 'DB_Applyweb/Table/class.DB_ReviewPositiveFactorOther2.php';
* require_once 'DB_Applyweb/Table/class.DB_ReviewRiskFactor2.php';
* require_once 'DB_Applyweb/Table/class.DB_SpecialConsideration2.php';
* require_once 'DB_Applyweb/Table/class.DB_PhoneScreens2.php';
* require_once 'DB_Applyweb/Table/class.DB_MseInterview2.php';
* require_once 'DB_Applyweb/Table/class.DB_MseRiskFactors2.php';
* require_once 'DB_Applyweb/Table/class.DB_MseRiskFactorsDecision2.php';
*/

class ApplicationData2_ReviewData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_PromotionStatus;
    private $DB_PromotionHistory;
    private $DB_Review;
    private $DB_ReviewPositiveFactor;
    private $DB_ReviewPositiveFactorOther;
    private $DB_ReviewRiskFactor;
    private $DB_SpecialConsideration;
    private $DB_PhoneScreens;
    private $DB_MseInterview;
    private $DB_MseRiskFactors;
    private $DB_MseRiskFactorsDecision;
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_PromotionStatus = new DB_PromotionStatus2($this->DB_Applyweb2);
        $this->DB_PromotionHistory = new DB_PromotionHistory2($this->DB_Applyweb2);
        $this->DB_Review = new DB_Review2($this->DB_Applyweb2);
        $this->DB_ReviewPositiveFactor = new DB_ReviewPositiveFactor2($this->DB_Applyweb2);
        $this->DB_ReviewPositiveFactorOther = new DB_ReviewPositiveFactorOther2($this->DB_Applyweb2);
        $this->DB_ReviewRiskFactor = new DB_ReviewRiskFactor2($this->DB_Applyweb2);
        $this->DB_SpecialConsideration = new DB_SpecialConsideration2($this->DB_Applyweb2);
        $this->DB_PhoneScreens = new DB_PhoneScreens2($this->DB_Applyweb2);
        $this->DB_MseInterview = new DB_MseInterview2($this->DB_Applyweb2);
        $this->DB_MseRiskFactors = new DB_MseRiskFactors2($this->DB_Applyweb2);
        $this->DB_MseRiskFactorsDecision = new DB_MseRiskFactorsDecision2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $reviewDataRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'promotion_status' => array(),              // 2D, unindexed
            'promotion_history' => array(),             // 2D, unindexed
            'review' => array(),                        // 2D, unindexed
            'review_positive_factor' => array(),        // 3D, indexed by review_id
            'review_positive_factor_other' => array(),  // 3D, indexed by review_id
            'review_risk_factor' => array(),            // 3D, indexed by review_id
            'special_consideration' => array(),         // 2D, unindexed
            'phone_screens' => array(),                 // 2D, unindexed
            'mse_interview' => array(),                 // 2D, unindexed
            'mse_risk_factors' => array(),              // 2D, unindexed
            'mse_risk_factors_decision' => array()      // 2D, unindexed
        );

        $reviewDataRecords['promotion_status'] = $this->DB_PromotionStatus->find($applicationId);
        
        $reviewDataRecords['promotion_history'] = $this->DB_PromotionHistory->find($applicationId);
        
        $reviewDataRecords['review'] = $this->DB_Review->find($applicationId);
        
        foreach ($reviewDataRecords['review'] as $reviewRecord) {
            
            $reviewId = $reviewRecord['id'];
            
            $reviewDataRecords['review_positive_factor'][$reviewId] = 
                $this->DB_ReviewPositiveFactor->find($reviewId);
            
            $reviewDataRecords['review_positive_factor_other'][$reviewId] = 
                $this->DB_ReviewPositiveFactorOther->find($reviewId);
            
            $reviewDataRecords['review_risk_factor'][$reviewId] = 
                $this->DB_ReviewRiskFactor->find($reviewId);
        }
        
        $reviewDataRecords['special_consideration'] = 
            $this->DB_SpecialConsideration->find($applicationId);
            
        $reviewDataRecords['phone_screens'] = 
            $this->DB_PhoneScreens->find($applicationId);
        
        $reviewDataRecords['mse_interview'] = 
            $this->DB_MseInterview->find($applicationId);
            
        $reviewDataRecords['mse_risk_factors'] = 
            $this->DB_MseRiskFactors->find($applicationId);
            
        $reviewDataRecords['mse_risk_factors_decision'] = 
            $this->DB_MseRiskFactorsDecision->find($applicationId);
        
        return $reviewDataRecords;
    }
}
?>
