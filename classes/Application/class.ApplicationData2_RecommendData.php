<?php
/*
* Class for recommendation-related application data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Recommend2.php';
* require_once 'DB_Applyweb/Table/class.DB_RecommenderInfo2.php';
* require_once 'DB_Applyweb/Table/class.DB_Recommendforms2.php';
*/

class ApplicationData2_RecommendData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_Recommend;
    private $DB_RecommenderInfo;
    private $DB_Recommendforms;

    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_Recommend = new DB_Recommend2($this->DB_Applyweb2);
        $this->DB_RecommenderInfo = new DB_RecommenderInfo2($this->DB_Applyweb2);
        $this->DB_Recommendforms = new DB_Recommendforms2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $applicationRecommendRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'recommend' => array(),             // 2D, unindexed
            'recommender_info' => array(),      // 2D, unindexed
            'recommendforms' => array()         // 3D, indexed by recommend.id
        );
        
        // Get recommend table records.
        $recommendRecords = $this->DB_Recommend->find($applicationId);
        $applicationRecommendRecords['recommend'] = $recommendRecords; 
        
        foreach ($recommendRecords as $recommendRecord) {
            
            $recommendId = $recommendRecord['id'];
        
            // Get recommender info for each recommendation.
            $applicationRecommendRecords['recommender_info'][] =
                $this->DB_RecommenderInfo->get($recommendId);
                
            // Get recommend form records for each recommendation.
            $applicationRecommendRecords['recommendforms'][$recommendId] =
                $this->DB_Recommendforms->find($recommendId);       
        }
        
        return $applicationRecommendRecords;
    }
}
?>
