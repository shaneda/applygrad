<?php
/*
* Class for application table-related application data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Application2.php';
* require_once 'DB_Applyweb/Table/class.DB_PeriodApplication2.php';
* require_once 'DB_Applyweb/Table/class.DB_LuApplicationAppreqs2.php';
*/

class ApplicationData2_ApplicationData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_Application;
    private $DB_PeriodApplication;
    private $DB_LuApplicationAppreqs;
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_Application = new DB_Application2($this->DB_Applyweb2);
        $this->DB_PeriodApplication = new DB_PeriodApplication2($this->DB_Applyweb2);
        $this->DB_LuApplicationAppreqs = new DB_LuApplicationAppreqs2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $applicationRelatedRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'application' => array(),           // 2D, unindexed
            'period_application' => array(),    // 2D, unindexed
            'lu_application_appreqs' => array() // 2D, unindexed
        );
        
        // Get application table records.
        $applicationRecords = $this->DB_Application->get($applicationId);
        $applicationRelatedRecords['application'] = $applicationRecords;
        
        // Get period_application records.
        $periodApplicationRecords = $this->DB_PeriodApplication->find($applicationId);
        $applicationRelatedRecords['period_application'] = $periodApplicationRecords; 
        
        // Get lu_application_appreqs records.
        $luApplicationAppreqsRecords = $this->DB_LuApplicationAppreqs->find($applicationId);
        $applicationRelatedRecords['lu_application_appreqs'] = $luApplicationAppreqsRecords;
        
        return $applicationRelatedRecords;
    }
}
?>
