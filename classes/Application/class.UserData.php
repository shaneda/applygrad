<?php
/*
* require_once 'DB_Applyweb/class.DB_Applyweb2.php';
* require_once 'DB_Applyweb/class.DB_Applyweb_Table2.php'; 
* require_once 'DB_Applyweb/Table/class.DB_Users2.php';
* require_once 'DB_Applyweb/Table/class.DB_UsersRemoteAuthString2.php';
* require_once 'DB_Applyweb/Table/class.DB_LuUsersUsertypes2.php';
* require_once 'DB_Applyweb/Table/class.DB_LuUserDepartment2.php';
* require_once 'DB_Applyweb/Table/class.DB_UsersInfo2.php';
* require_once 'DB_Applyweb/Table/class.DB_UsersRegistry2.php';
* require_once 'DB_Applyweb/Table/class.DB_LuUsersUsertypesRegistry2.php';
*/

class UserData
{
    private $DB_Applyweb2;
    
    private $DB_Users;
    private $DB_LuUsersUsertypes;
    private $DB_UsersInfo;
    private $DB_LuUserDepartment;
    
    private $DB_ScsUser;
    private $DB_ScsUserWebiso;
    private $DB_UsersRemoteAuthString;
    
    private $DB_UsersRegistry;
    private $DB_LuUsersUsertypesRegistry;
    
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        
        $this->DB_Users = new DB_Users2($this->DB_Applyweb2);
        $this->DB_LuUsersUsertypes = new DB_LuUsersUsertypes2($this->DB_Applyweb2);
        $this->DB_UsersInfo = new DB_UsersInfo2($this->DB_Applyweb2);
        $this->DB_LuUserDepartment = new DB_LuUserDepartment2($this->DB_Applyweb2);
    
        $this->DB_ScsUser = new DB_ScsUser2($this->DB_Applyweb2);
        $this->DB_ScsUserWebiso = new DB_ScsUserWebiso2($this->DB_Applyweb2);
        
        if ($this->DB_Applyweb2->tableExists('users_remote_auth_string')) {
            $this->DB_UsersRemoteAuthString = new DB_UsersRemoteAuthString2($this->DB_Applyweb2);    
        }
        
        if ($this->DB_Applyweb2->tableExists('users_registry')) {
            $this->DB_UsersRegistry = new DB_UsersRegistry2($this->DB_Applyweb2);
        }
        
        if ($this->DB_Applyweb2->tableExists('lu_users_usertypes_registry')) {
            $this->DB_LuUsersUsertypesRegistry = new DB_LuUsersUsertypesRegistry2($this->DB_Applyweb2); 
        }
    }
    
    public function getUsersId($luUsersUsertypesId) {
    
        $record = $this->DB_LuUsersUsertypes->get($luUsersUsertypesId);
            
        if (isset($record[0]['user_id'])) {
            return $record[0]['user_id'];
        } else {
            return NULL;
        }
    }
    
    public function checkEmailCollision($email) {
        return $this->DB_Users->find($email);
    }
    
    public function checkGuidCollision($guid) {
        return $this->DB_Users->find($guid, 'guid');
    }
    
    public function getDbHostname() {
        return $this->DB_Applyweb2->getDbHostname();
    }
    
    public function getDb() {
        return $this->DB_Applyweb2->getDb();
    }

    public function export($usersId) {

        $userData = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'users' => array(),                     // 1D
            'lu_users_usertypes' => array(),        // 2D, uninidexed
            'users_info' => array(),                // 2D, indexed by luu_id
            'lu_user_department' => array(),        // 2D, indexed by luu_id
            'scs_user' => array(),                  // 2D, uninidexed
            'scs_user_webiso' => array(),           // 2D, indexed by scs_user_id
            'users_remote_auth_string' => array()   // 2D, indexed by users_id
        );
        
        $usersRecord = $this->DB_Users->get($usersId);
        
        if (isset($usersRecord[0]['id'])) {
            $userData['users'] = $usersRecord[0];
        } else {
            return FALSE;    
        }
        
        // Get the scs_user and scs_user_webiso records
        $userData['scs_user'] = $this->DB_ScsUser->find($usersId);
        
        foreach ($userData['scs_user'] as $scsUserRecord) {
        
            $scsUserId = $scsUserRecord['scs_user_id'];
            
            $scsUserWebisoRecords = $this->DB_ScsUserWebiso->find($scsUserId);
            
            foreach ($scsUserWebisoRecords as $scsUserWebisoRecord) {
                
                $userData['scs_user'][$scsUserId][] = $scsUserWebisoRecord;
            }
            
        }
        
        // Get users_remote_auth_string records.
        if (isset($this->DB_UsersRemoteAuthString)) {
            $userData['users_remote_auth_string'] = 
            $this->DB_UsersRemoteAuthString->find($usersId); 
        }
        
        // Get lu_users_usertype records.
        $userData['lu_users_usertypes'] =
            $this->DB_LuUsersUsertypes->find($usersId);
        
        
        // Get users_info, lu_user_department records for each lu_users_usertype record.
        foreach($userData['lu_users_usertypes'] as $usertypeRecord) {
            
            $luuId = $usertypeRecord['id'];
            
            // Should be 0..1 users_info record for each luu_id.
            $usersInfoRecords = $this->DB_UsersInfo->find($luuId);
            foreach ($usersInfoRecords as $usersInfoRecord) {
                $userData['users_info'][$luuId] = $usersInfoRecord;    
            }
            
            // Should be 0..1 lu_user_department record for each luu_id.
            $userDepartmentRecords = $this->DB_LuUserDepartment->find($luuId);
            foreach ($userDepartmentRecords as $userDepartmentRecord) {
                $userData['lu_user_department'][$luuId] = $userDepartmentRecord;    
            } 
        }
        
        return $userData;
    }
    
    public function makeGuid() {
        return uniqid(substr(md5(rand()), 0, 19));
    }
    
    public function replicate($userDataArray) {
        
        if ( !isset($userDataArray['users'][0]['id']) ) {
            return FALSE;
        }
        
        // Check to see whether this user is already in the DB.
        $email = $userDataArray['users'][0]['email'];
        $exisitingUserRecords = $this->DB_Users->find($email);
        
        if (count($exisitingUserRecords) > 0) {
            
            // User is already in DB.
            $this->replicateExistingUser($userDataArray, $existingUserRecords);
            
        } else {
            
            $this->replicateNewUser($userDataArray);
        }
    }
    
    private function replicateNewUser($userDataArray) {

        // Get the old users record from the exported data array
        $oldUsersRecord = $userDataArray['users'][0];
        $oldUsersId = $oldUsersRecord['id'];
        
        // Insert the new user record and get the new users.id.
        $newUsersRecord = $oldUsersRecord;
        $newUsersRecord['id'] = NULL;
        $newUsersId = $this->DB_Users->save($usersRecord);
        
        // Record the new users.id in the registry
        if (isset($this->DB_UsersRegistry)) {
            
            $usersRegistryRecord = array(
                'remote_host' => $usersRecord['host'],
                'remote_db' => $usersRecord['db'],
                'remote_users_id' => $oldUsersId,
                'users_id' => $newUsersId
            );
            
            $this->DB_UsersRegistry->save($usersRegistryRecord);
        }
        
        // Insert the webiso/remote_auth data
        if (isset($this->DB_UsersRemoteAuthString)) {
        
            // Insert the user_remote_auth_string records using the new users.id.
            foreach ($userDataArray['user_remote_auth_string'] as $userRemoteAuthStringRecord) {
                        
                $userRemoteAuthStringRecord['users_id'] = $newUsersId;
                $this->DB_UsersRemoteAuthString->save($userRemoteAuthStringRecord);    
            }
        
        } else {
            
            // Insert the scs_user and scs_user_webiso records using the new users.id.
            foreach ($userDataArray['scs_user'] as $oldScsUserRecord) {
                
                // Insert the scs_user record with the new users.id
                // and get the new scs_user_id.
                $newScsUserRecord = $oldScsUserRecord;
                $newScsUserRecord['scs_user_id'] = null;
                $newScsUserRecord['users_id'] = $newUsersId;
                $newScsUserId = $this->DB_ScsUser->save($newScsUserRecord);
                
                // Insert the scs_user_webiso records with the new scs_user_id.
                $oldScsUserId = $oldScsUserRecord['scs_user_id'];
                foreach ($userDataArray['scs_user_webiso'][$oldScsUserId] as $oldScsUserWebisoRecord) {
                
                    $newScsUserWebisoRecord = $oldScsUserWebisoRecord;
                    $newScsUserWebisoRecord['scs_user_id'] = $newScsUserId;
                    $this->DB_ScsUserWebiso->save($newScsUserWebisoRecord);
                }
            }
        }
        
        // Insert the lu_users_usertypes records using the new users.id.
        // Insert the remaining records using the new lu_users_usertypes.id.
        foreach ($userDataArray['lu_users_usertypes'] as $oldLuuRecord) {
        
            // Get the old luu_id.
            $oldLuuId = $oldLuuRecord['id'];
            
            // Insert the new luu record and with the new users.id
            // and get the new luu_id.
            $newLuuRecord = $oldLuuRecord;
            $newLuuRecord['id'] = NULL;
            $newLuuRecord['user_id'] = $newUsersId;    
            $newLuuId = $this->DB_LuUsersUsertypes->save($luuRecord);
            
            // Record the new luu id in the registry.
            if (isset($this->DB_LuUsersUsertypesRegistry)) {
            
                $luUsersUsertypesRegistryRecord = array(
                    'remote_host' => $usersRecord['host'],
                    'remote_db' => $usersRecord['db'],
                    'remote_lu_users_usertypes_id' => $oldLuuId,
                    'lu_users_usertypes_id' => $newLuuId
                );
                
                $this->DB_LuUsersUsertypesRegistry->save($luUsersUsertypesRegistryRecord);
            }
            
            // Insert any users_info records with the new luu_id.
            if (isset($userDataArray['users_info'][$oldLuuId])) {
                
                $newUsersInfoRecord = $userDataArray['users_info'][$oldLuuId];
                $newUsersInfoRecord['id'] = NULL;
                $newUsersInfoRecord['user_id'] = $newLuuId;
                $this->DB_UsersInfo->save($newUsersInfoRecord);
            }

            // Insert any lu_user_department records with the new luu_id.
            if (isset($userDataArray['lu_user_department'][$oldLuuId])) {
                
                $newLuUserDepartmentRecord = $userDataArray['users_info'][$oldLuuId];
                $newLuUserDepartmentRecord['id'] = NULL;
                $newLuUserDepartmentRecord['user_id'] = $newLuuId;
                $this->DB_LuUserDepartment->save($newLuUserDepartmentRecord);
            }                
        } 
    
        return TRUE; 
    }
    
    private function replicateExistingUser($userDataArray, $existingUserRecords) {
    
        // If user only has student role, record existing id in registry.
        
        /*
        SELECT luu1.id, luu1.usertype_id, luu2.id, luu2.usertype_id, users . *
        FROM lu_users_usertypes AS luu1, lu_users_usertypes AS luu2, users
        WHERE luu1.usertype_id =5
        AND luu2.usertype_id !=5
        AND luu1.user_id = luu2.user_id
        AND luu1.user_id = users.id 
        */
        
        
        // Otherwise, replicate as a new user.
        
        
    }
    
}
  
?>