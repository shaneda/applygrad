<?php
/*
* Class for program-related application tables.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_LuApplicationPrograms2.php';
* require_once 'DB_Applyweb/Table/class.DB_LuApplicationInterest2.php';
* require_once 'DB_Applyweb/Table/class.DB_LuApplicationAdvisor2.php';
*/

class ApplicationData2_ProgramData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_LuApplicationPrograms;
    private $DB_LuApplicationInterest;
    private $DB_LuApplicationAdvisor;

    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_LuApplicationPrograms = new DB_LuApplicationPrograms2($this->DB_Applyweb2);
        $this->DB_LuApplicationInterest = new DB_LuApplicationInterest2($this->DB_Applyweb2);
        $this->DB_LuApplicationAdvisor = new DB_LuApplicationAdvisor2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $applicationProgramRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'lu_application_programs' => array(),   // 2D, unindexed
            'lu_application_interest' => array(),   // 3D, indexed by lu_application_programs.id
            'lu_application_advisor' => array()     // 2D, unindexed
        );
        
        // Get application programs.
        $applicationPrograms = $this->DB_LuApplicationPrograms->find($applicationId);
        $applicationProgramRecords['lu_application_programs'] = $applicationPrograms; 
        
        // Get interests for each application program.
        foreach ($applicationPrograms as $applicationProgram) {
            
            $applicationProgramId = $applicationProgram['id'];
            $applicationProgramRecords['lu_application_interest'][$applicationProgramId] =
                $this->DB_LuApplicationInterest->find($applicationProgramId);    
        }
        
        // Get advisors.
        $applicationProgramRecords['lu_application_advisor'] = 
            $this->DB_LuApplicationAdvisor->find($applicationId);
        
        return $applicationProgramRecords;
    }
    
}
?>