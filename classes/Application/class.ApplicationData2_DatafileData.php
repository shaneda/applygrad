<?php
/*
* Class for application datafileinfo data.
* 
* require_once 'Application/class.ApplywebData.php';
*/

class ApplicationData2_DatafileData extends ApplywebData
{
    protected $DB_Applyweb2;
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
    }
    
    public function export($applicationId, $luUsersUsertypesId) {

        $datafileDataRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'lu_users_usertypes_id' => $luUsersUsertypesId,
            'datafileinfo' => array(),                   // 2D, indexed by datafileinfo.id
        );

        $datafileQuery = "SELECT * FROM datafileinfo 
                            WHERE user_id = " . $luUsersUsertypesId . "
                            AND (userdata LIKE '" . $applicationId . "_%'
                            OR userdata LIKE '-1_%')";
        
        $datafileDataRecords['datafileinfo'] = $this->DB_Applyweb2->handleSelectQuery($datafileQuery, 'id');
        
        return $datafileDataRecords;
    }
}
?>
