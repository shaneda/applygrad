<?php
/*
* Class for test score-related application data.
* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Grescore2.php';
* require_once 'DB_Applyweb/Table/class.DB_GrescoreMscsWaiver2.php';
* require_once 'DB_Applyweb/Table/class.DB_Gresubjectscore2.php';
* require_once 'DB_Applyweb/Table/class.DB_Gmatscore2.php';
* require_once 'DB_Applyweb/Table/class.DB_Toefl2.php';
* require_once 'DB_Applyweb/Table/class.DB_Ieltsscore2.php';
*/

class ApplicationData2_ScoresData extends ApplywebData
{
    protected $DB_Applyweb2;
    private $DB_Grescore;
    private $DB_GrescoreMscsWaiver;
    private $DB_Gresubjectscore;
    private $DB_Gmatscore;
    private $DB_Toefl;
    private $DB_Ieltsscore;
    
    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->DB_Grescore = new DB_Grescore2($this->DB_Applyweb2);
        $this->DB_GrescoreMscsWaiver = new DB_GrescoreMscsWaiver2($this->DB_Applyweb2);
        $this->DB_Gresubjectscore = new DB_Gresubjectscore2($this->DB_Applyweb2);
        $this->DB_Gmatscore = new DB_Gmatscore2($this->DB_Applyweb2);
        $this->DB_Toefl = new DB_Toefl2($this->DB_Applyweb2);
        $this->DB_Ieltsscore = new DB_Ieltsscore2($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $testScoreRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'grescore' => array(),              // 2D, unindexed
            'grescore_mscs_waiver' => array(),  // 2D, unindexed
            'gresubjectscore' => array(),       // 2D, unindexed
            'gmatscore' => array(),             // 2D, unindexed
            'toefl' => array(),                 // 2D, unindexed
            'ieltsscore' => array()             // 2D, unindexed
        );

        $testScoreRecords['grescore'] = $this->DB_Grescore->find($applicationId);
        
        $testScoreRecords['grescore_mscs_waiver'] = $this->DB_GrescoreMscsWaiver->get($applicationId);
        
        $testScoreRecords['gresubjectscore'] = $this->DB_Gresubjectscore->find($applicationId);
        
        $testScoreRecords['gmatscore'] = $this->DB_Gmatscore->find($applicationId);
        
        $testScoreRecords['toefl'] = $this->DB_Toefl->find($applicationId);
        
        $testScoreRecords['ieltsscore'] = $this->DB_Ieltsscore->find($applicationId);
        
        return $testScoreRecords;
    }
}
?>
