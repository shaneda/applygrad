<?php
/*
* require_once 'DB_Applyweb/class.DB_Applyweb2.php';
* require_once 'DB_Applyweb/class.DB_Applyweb_Table2.php';
*/

class ApplywebData
{
    protected $DB_Applyweb2;

    public function __construct($DB_Applyweb2) {
        $this->DB_Applyweb2 = $DB_Applyweb2;
    }
    
    public function getDbHostname() {
        return $this->DB_Applyweb2->getDbHostname();
    }
    
    public function getDb() {
        return $this->DB_Applyweb2->getDb();
    }    
}

?>