<?php

class Application
{

    private $DB_Application;
    
    private $id;
    private $periodId;
    private $createdDate;
    private $submittedDate;
    
    private $luUsersUsertypesId;
    private $usersId;

    private $programIds;
    

    
    public function __construct($applicationId = NULL) {
        
        $this->DB_Application = new DB_Application();
        $this->id = $applicationId;
        
        if ($applicationId) {
            $this->loadFromDb();
        }
    }
    
    private function loadFromDb() {
        
        $query = "SELECT application.user_id AS lu_users_usertypes_id,
                    lu_users_usertypes.user_id AS users_id,
                    period_id,
                    created_date,
                    submitted_date
                    FROM application
                    INNER JOIN lu_users_usertypes 
                        ON application.user_id = lu_users_usertypes.id
                    LEFT OUTER JOIN period_application
                        ON application.id = period_application.application_id
                    WHERE application.id = " . $this->id;
                    
        $recordArray = $this->DB_Application->handleSelectQuery($query);
        
        foreach ($recordArray as $record) {
            $this->periodId = $record['period_id'];
            $this->createdDate = $record['created_date'];
            $this->submittedDate = $record['submitted_date'];
        }    
        
        return TRUE;
    }

    public function getId() {
        return $this->id;
    }

    public function getLuUsersUsertypesId() {
        return $this->luUsersUsertypesId;
    }

    public function getUsersId() {
        return $this->usersId;
    }
    
    public function getPeriodId() {
        return $this->periodId;
    }





    public function isSubmittable() {
        
    }
    
    public function isEditable() {
        
    }
    
    public function isViewable() {
        
    }
    
    private function isRecommendable() {
        
    }
    
}
?>