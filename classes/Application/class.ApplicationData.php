<?php

class ApplicationData
{

    private $userTables = array(
        'users',
        'users_info'
        );
 
    private $periodTables = array(
        'period_application'
        );
    
    private $programTables = array(
        'lu_application_programs',
        'lu_application_advisor',
        'lu_application_interest'
        ); 
    
    private static $applicationTables = array(
        'application',
        'experience',
        'fellowships',
        'gmatscore',
        'grescore',
        'gresubjectscore',
        'ieltsscore',            
        'lu_application_appreqs',
        'publication',
        'toefl',
        'usersinst'
        );    

    private $datafileTables = array(
        'datafileinfo'  
        );
    
    private $paymentTables = array(
        'payment',
        'payment_item'
        );

    private $recommendTables = array(
        'recommend',
        'recommendforms'
        );
        
    /*
    private $reviewTables = array(
        'review',
        'special_consideration',
        'phone_screens',
        'application_decision',
        'lti_masters_admits',
        'student_decision'    
        );
    */
    
    
    public static function replicate( $applicationId, $targetPeriodId, 
                                        $targetProgramIds = array(), $retainPayment = FALSE) {

        $DB_Application = new DB_Application();
        $DB_PeriodApplication = new DB_PeriodApplication();
        $DB_Recommend = new DB_Recommend();
        
        $applicationData = self::export($applicationId);
        $guid = $applicationData['users'][0]['guid'];
        $applicationRecord = $applicationData['application'][0];
        $applicationRecord['id'] = NULL;
        $newDate = date("Y-m-d H:i:s");
        $applicationRecord['created_date'] = $newDate;
        if ($applicationRecord['submitted']) {
            $applicationRecord['submitted_date'] = $newDate;    
        }
        $newApplicationId = $DB_Application->save($applicationRecord);
        
        $periodRecord = array(
            'period_id' => $targetPeriodId,
            'application_id' => $newApplicationId
            );
        $DB_PeriodApplication->save($periodRecord);
        
        $targetProgramCount = count($targetProgramIds);
        for ($i = 0; $i < $targetProgramCount; $i++) {
            $programRecord = array(
                'application_id' => $newApplicationId,
                'program_id' => $targetProgramIds[$i],
                'choice' => $i + 1
                );        
            $programQuery = self::getInsertQuery('lu_application_programs', $programRecord, $DB_Application);
            $DB_Application->handleInsertQuery($programQuery);
        }
        
        foreach ($applicationData['datafileinfo'] as $datafileninfoRecord) {
            
            // Copy resume and statement
            if ($datafileninfoRecord['section'] == 2 
                || $datafileninfoRecord['section'] == 4) 
            {
                $newUserdata = $newApplicationId . '_1';
                $newDatafileinfoId = self::copyDatafile($datafileninfoRecord, $guid, $newUserdata);     
            }
            
            // For English, copy writing samples
            if ($datafileninfoRecord['section'] == 23 ) 
            {
                $oldUserdata = $datafileninfoRecord['userdata'];
                $oldUserdataArray = explode('_', $oldUserdata);
                $newUserdata = $newApplicationId . '_' . $oldUserdataArray[1];
                $newDatafileinfoId = self::copyDatafile($datafileninfoRecord, $guid, $newUserdata);     
            }
        } 

        foreach (self::$applicationTables as $applicationTable) {
            
            if ($applicationTable != 'application') {
                
                foreach ($applicationData[$applicationTable] as $record) {
                    
                    $record['application_id'] = $newApplicationId;
                    $oldRecordId = array_shift($record);
                    $query = self::getInsertQuery($applicationTable, $record, $DB_Application);
                    $DB_Application->handleInsertQuery($query);
                    $newRecordId = $DB_Application->getLastInsertId(); 
                    
                    if ( isset($record['datafile_id']) ) {
                    
                        if ($applicationTable == 'experience') {
                            $newUserdata = $newApplicationId . '_' . $record['experiencetype'];
                        } else {
                            $newUserdata = $newApplicationId . '_' . $newRecordId;    
                        }
                        
                        if ( isset($applicationData['datafileinfo'][$record['datafile_id']]) ) {
                        
                            $datafileninfoRecord = $applicationData['datafileinfo'][$record['datafile_id']];
                            
                            $newDatafileinfoId = self::copyDatafile($datafileninfoRecord, $guid, $newUserdata);    
                            
                            $updateQuery = "UPDATE " . $applicationTable;
                            if ($newDatafileinfoId) {
                                $updateQuery .= " SET datafile_id = " . $newDatafileinfoId;
                            } else {
                                $updateQuery .= " SET datafile_id = NULL";    
                            }
                            $updateQuery .= " WHERE id = " . $newRecordId;
                            $DB_Application->handleUpdateQuery($updateQuery);
                            
                        }
                    }
                }
            }
        }
        
        /*
        * Copy recommendations and update datafile_id
        */
        foreach ($applicationData['recommend'] as $recommendRecord) {
            
            $recommendRecord['application_id'] = $newApplicationId;
            $oldRecommendId = array_shift($recommendRecord);
            $newRecommendId = $DB_Recommend->save($recommendRecord);

            if ( isset($recommendRecord['datafile_id']) ) {
            
                $newUserdata = $newApplicationId . '_' . $newRecommendId;

                if ( isset($applicationData['datafileinfo'][$recommendRecord['datafile_id']]) ) {
                
                    $datafileninfoRecord = $applicationData['datafileinfo'][$recommendRecord['datafile_id']];
                    
                    $newDatafileinfoId = self::copyDatafile($datafileninfoRecord, $guid, $newUserdata);    
                    
                    $updateQuery = "UPDATE recommend";
                    if ($newDatafileinfoId) {
                        $updateQuery .= " SET datafile_id = " . $newDatafileinfoId;
                    } else {
                        $updateQuery .= " SET datafile_id = NULL";    
                    }
                    $updateQuery .= " WHERE id = " . $newRecommendId;
                    $DB_Application->handleUpdateQuery($updateQuery);
                    
                }
            }
            
            // Do not copy recommend form data.
            /*
            foreach ($applicationData['recommendforms'] as $recommendformRecord) {
            
                if ($recommendformRecord['recommend_id'] == $oldRecommendId) {
                    
                    $recommendformRecord['recommend_id'] = $newRecommendId;
                    $oldRecommendformId = array_shift($recommendformRecord);
                    $recommendformQuery = self::getInsertQuery('recommendforms', $recommendformRecord, $DB_Recommend);
                    $newRecommendformId = $DB_Recommend->handleInsertQuery($recommendformQuery);
                                        
                }    
            }
            */
        }
        
        return $newApplicationId;
    }

    
    public static function move($applicationId, $targetPeriodId, 
                                    $targetProgramIds = array() ) {
        
    }

    
    public static function delete($applicationId) {
        
    }

    
    public static function export($applicationId, $format = 'raw') {
    
        switch ($format) {
            
            case 'raw':
            
                return self::exportRaw($applicationId); 
            
            default:
                
                return NULL;
        }
    }
    
    
    private function exportRaw($applicationId) {

        $rawData = array();
        
        $DB_Application = new DB_Application;
        $applicationRecord = $DB_Application->get($applicationId);
        if ( isset($applicationRecord[0]['user_id']) ) {
            $luUsersUsertypesId = $applicationRecord[0]['user_id'];
            $rawData['application'] = $applicationRecord;    
        } else {
            return FALSE;
        }
        
        $userRecords = self::exportUserTables($luUsersUsertypesId);
        foreach ($userRecords as $tableName => $recordArray) {
            $rawData[$tableName] = $recordArray;   
        }
        
        $programRecords = self::exportProgramTables($applicationId);
        foreach ($programRecords as $tableName => $recordArray) {
            $rawData[$tableName] = $recordArray;   
        }
        
        foreach(self::$applicationTables as $applicationTable) {
            if ($applicationTable != 'application') {
                $query = "SELECT * FROM " . $applicationTable . " WHERE application_id = ". $applicationId;
                $resultArray = $DB_Application->handleSelectQuery($query);
                $rawData[$applicationTable] = $resultArray; 
            }
        }
        
        $datafileQuery = "SELECT * FROM datafileinfo WHERE user_id = " . $luUsersUsertypesId;
        $datafileQuery .= " AND userdata LIKE '" . $applicationId . "_%'"; 
        $rawData['datafileinfo'] = $DB_Application->handleSelectQuery($datafileQuery, 'id'); 
        
        $paymentRecords = self::exportPaymentTables($applicationId);
        foreach ($paymentRecords as $tableName => $recordArray) {
            $rawData[$tableName] = $recordArray;   
        }

        $recommendRecords = self::exportRecommendTables($applicationId);
        foreach ($recommendRecords as $tableName => $recordArray) {
            $rawData[$tableName] = $recordArray;   
        }        

        return $rawData;
    }
    
    private function exportUserTables($luUsersUsertypesId) {
    
        $DB_Users = new DB_Users();
        $DB_UsersInfo = new DB_UsersInfo();
        $rawData = array();
        
        $usersIdQuery = "SELECT * FROM lu_users_usertypes WHERE id = " . $luUsersUsertypesId;
        $usersIdArray = $DB_Users->handleSelectQuery($usersIdQuery);
        if ( isset($usersIdArray[0]['user_id']) ) {
            $usersId = $usersIdArray[0]['user_id'];
        } else {
            return;
        }
        
        $rawData['users'] = $DB_Users->get($usersId);
        $rawData['users_info'] = $DB_UsersInfo->find($luUsersUsertypesId);

        return $rawData;
    }

    private function exportProgramTables($applicationId) {
        
        $rawData = array();
        
        $DB_LuApplicationPrograms = new DB_LuApplicationPrograms();
        $rawData['lu_application_programs'] = $DB_LuApplicationPrograms->find($applicationId); 
        
        return $rawData;
    }

    private function exportPaymentTables($applicationId) {
    
        $rawData['payment'] = array();
        $rawData['payment_item'] = array();
        
        $DB_Payment = new DB_Payment();
        $rawData['payment'] = $DB_Payment->find($applicationId);
        
        $DB_PaymentItem = new DB_PaymentItem();
        foreach ($rawData['payment'] as $paymentRecord) {
            $paymentId = $paymentRecord['payment_id'];
            foreach ( $DB_PaymentItem->find($paymentId) as $paymentItemRecord) {
                $rawData['payment_item'][] = $paymentItemRecord;    
            }   
        }
        
        return $rawData;    
    }

    private function exportRecommendTables($applicationId) {
        
        $rawData['recommend'] = array();
        $rawData['recommendforms'] = array();
        
        $DB_Recommend = new DB_Recommend();
        $rawData['recommend'] = $DB_Recommend->find($applicationId);
        
        foreach ($rawData['recommend'] as $recommendRecord) {
            $recommendId = $recommendRecord['id'];
            $recommendformQuery = "SELECT * FROM recommendforms WHERE recommend_id = " . $recommendId; 
            foreach ( $DB_Recommend->handleSelectQuery($recommendformQuery) as $recommendformRecord) {
                $rawData['recommendforms'][] = $recommendformRecord;    
            }   
        }
        
        return $rawData;
    }
    
    
    private function getInsertQuery($tableName, $record, &$DB_Applyweb) {
        
        $i = 0;
        $fieldList = $valueList = "";
        foreach ($record as $field => $value) {

            if (!$value) {
                 continue;
            }
            
            if ($i > 0) {
                $fieldList .= ", ";
                $valueList .= ", ";
            }
            
            $fieldList .= $field;
            $valueList .= "'" . $DB_Applyweb->escapeString($value) . "'";
            
            $i++;
        }
        
        $query = "INSERT INTO " . $tableName;     
        $query .= " (" . $fieldList . ") VALUES (" . $valueList . ")";
    
        return $query;
    }
    
    
    private function copyDatafile($datafileRecord, $guid, $newUserdata) {
        
        $DB_Datafileinfo = new DB_Datafileinfo();
        
        $datafileTypeQuery = "SELECT * FROM datafile_type";
        $datafileTypes = $DB_Datafileinfo->handleSelectQuery($datafileTypeQuery, 'datafile_type_id');
        
        $datafileRoot = realpath($GLOBALS['datafileroot']);
        $datafileType = $datafileTypes[$datafileRecord['section']]['datafile_type'];
        $fileName = $datafileType . '_' . $datafileRecord['userdata'] . '.' . $datafileRecord['extension'];  
        $filePath = $datafileRoot . '/' . $guid . '/' . $datafileType . '/' . $fileName;
        
        if (file_exists($filePath)) {
            
            $newFileName = $fileName = $datafileType . '_' . $newUserdata . '.' . $datafileRecord['extension'];
            $newFilePath = $datafileRoot . '/' . $guid . '/' . $datafileType . '/' . $fileName;
            copy($filePath, $newFilePath);
            
            $datafileRecord['userdata'] = $newUserdata;
            $datafileRecord['moddate'] = date("Y-m-d H:i:s");
            $oldDatafileId = array_shift($datafileRecord);
            $newDatafileinfoId = $DB_Datafileinfo->save($datafileRecord);
            
            return $newDatafileinfoId;
        }
        
        return FALSE;
    }
    
    
    
}
?>