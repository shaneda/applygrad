<?php
/* 
* require_once 'Application/class.ApplywebData.php';
* require_once 'DB_Applyweb/Table/class.DB_Application2.php'; 
* require_once 'Application/class.UserData.php';
* require_once 'Application/class.ApplicationData2_ApplicationData.php';
* require_once 'Application/class.ApplicationData2_ProgramData.php';
* require_once 'Application/class.ApplicationData2_ScoresData.php';
* require_once 'Application/class.ApplicationData2_EducationData.php';
* require_once 'Application/class.ApplicationData2_RecommendData.php';
* require_once 'Application/class.ApplicationData2_SupplementalData.php';
* require_once 'Application/class.ApplicationData2_DatafileData.php';
* require_once 'Application/class.ApplicationData2_PaymentData.php';
* require_once 'Application/class.ApplicationData2_ReviewData.php';
*/

class ApplicationData2 extends ApplywebData
{
    protected $DB_Applyweb2;
    public $userData;
    private $applicationData;
    private $programData;
    private $scoresData;
    private $educationData;
    private $recommendData;
    private $supplementalData;
    private $datafileData;
    private $paymentData;
    private $reviewData;
    
    // TODO: mhci placeout?

    public function __construct($DB_Applyweb2) {
        
        $this->DB_Applyweb2 = $DB_Applyweb2;
        $this->userData = new UserData($this->DB_Applyweb2);
        $this->applicationData = new ApplicationData2_ApplicationData($this->DB_Applyweb2);
        $this->programData = new ApplicationData2_ProgramData($this->DB_Applyweb2);
        $this->scoresData = new ApplicationData2_ScoresData($this->DB_Applyweb2);
        $this->educationData = new ApplicationData2_EducationData($this->DB_Applyweb2);
        $this->recommendData = new ApplicationData2_RecommendData($this->DB_Applyweb2);
        $this->supplementalData = new ApplicationData2_SupplementalData($this->DB_Applyweb2);
        $this->datafileData = new ApplicationData2_DatafileData($this->DB_Applyweb2);
        $this->paymentData = new ApplicationData2_PaymentData($this->DB_Applyweb2);
        $this->reviewData = new ApplicationData2_ReviewData($this->DB_Applyweb2);
    }
    
    public function export($applicationId) {

        $applicationRecords = array(
            'host' => $this->getDbHostname(),
            'db' => $this->getDb(),
            'application_id' => $applicationId,
            'lu_users_usertypes_id' => NULL,            // needed to retrieve user data
            'user_data' => array(),                     // 3D, unindexed
            'application' => array(),                   // 1D, unindexed
            'period_application' => array(),            // 2D, unindexed
            'lu_application_appreqs' => array(),        // 2D, unindexed              
            'lu_application_programs' => array(),       // 2D, unindexed
            'lu_application_interest' => array(),       // 3D, indexed by lu_application_programs.id
            'lu_application_advisor' => array(),        // 2D, unindexed
            'grescore' => array(),                      // 2D, unindexed
            'grescore_mscs_waiver' => array(),          // 2D, unindexed
            'gresubjectscore' => array(),               // 2D, unindexed
            'gmatscore' => array(),                     // 2D, unindexed
            'toefl' => array(),                         // 2D, unindexed
            'ieltsscore' => array(),                    // 2D, unindexed
            'usersinst' => array(),                     // 2D, unindexed
            'recommend_data' => array(),                // 4D, unindexed
            'experience' => array(),                    // 2D, unindexed
            'fellowships' => array(),                   // 2D, unindexed
            'publication' => array(),                   // 2D, unindexed
            'datafileinfo' => array(),                  // 2D, indexed by datafileinfo.id
            'payment_data' => array(),
            'review_data' => array()
        );

        // Get the application table-related records
        if (!$this->exportApplicationData($applicationId, $applicationRecords)) {
            return FALSE;
        }
        
        // Get the user data with the lu_users_usertypes.id
        $this->exportUserData($applicationRecords['lu_users_usertypes_id'], $applicationRecords);
        
        // Get the program-related records
        $this->exportProgramData($applicationId, $applicationRecords);

        // Get the scores-related records.
        $this->exportScoresData($applicationId, $applicationRecords);        
        
        // Get the education-related records.
        $this->exportEducationData($applicationId, $applicationRecords);
        
        // Get the recommendation-related records
        $this->exportRecommendData($applicationId, $applicationRecords);
        
        // Get the supplemental records
        $this->exportSupplementalData($applicationId, $applicationRecords);
        
        // Get the datafileinfo records
        $this->exportDatafileData($applicationId, 
            $applicationRecords['lu_users_usertypes_id'], $applicationRecords);
        
        // Get the payment records
        $this->exportPaymentData($applicationId, $applicationRecords);
        
        // Get the review records
        $this->exportReviewData($applicationId, $applicationRecords);
        
        return $applicationRecords;
    }
    
    private function exportApplicationData($applicationId, &$applicationRecordArray) {
    
        $applicationRecords = $this->applicationData->export($applicationId);
        
        if ( isset($applicationRecords['application'][0]['user_id']) ) {
        
            $applicationRecordArray['lu_users_usertypes_id'] = 
                $applicationRecords['application'][0]['user_id'];
            $applicationRecordArray['application'] = $applicationRecords['application'][0];    
        
        } else {
            
            return FALSE;
        }
        
        $applicationRecordArray['period_application'] = $applicationRecords['period_application'];
        $applicationRecordArray['lu_application_appreqs'] = $applicationRecords['lu_application_appreqs'];
        
        return TRUE;
    }
    
    private function exportUserData($luUsersUsertypesId, &$applicationRecordArray) {
        
        $usersId = $this->userData->getUsersId($luUsersUsertypesId);
        $userRecords = $this->userData->export($usersId);
        
        $applicationRecordArray['user_data']['users']               // 1D, unindexed 
            = $userRecords['users'];                    
        
        $applicationRecordArray['user_data']['lu_users_usertypes']  // 2D, uninidexed   
            = $userRecords['lu_users_usertypes'];
        
        $applicationRecordArray['user_data']['users_info']          // 2D, indexed by luu_id 
            = $userRecords['users_info'];

        return TRUE;
    }
        
    private function exportProgramData($applicationId, &$applicationRecordArray) {
        
        $programRecords = $this->programData->export($applicationId);
        
        $applicationRecordArray['lu_application_programs'] = $programRecords['lu_application_programs'];
        $applicationRecordArray['lu_application_interest'] = $programRecords['lu_application_interest'];
        $applicationRecordArray['lu_application_advisor'] = $programRecords['lu_application_advisor'];
        
        return TRUE;
    }
        
    private function exportScoresData($applicationId, &$applicationRecordArray) {
        
        $scoreRecords = $this->scoresData->export($applicationId);
        
        $applicationRecordArray['grescore'] = $scoreRecords['grescore'];
        $applicationRecordArray['grescore_mscs_waiver'] = $scoreRecords['grescore_mscs_waiver'];
        $applicationRecordArray['gresubjectscore'] = $scoreRecords['gresubjectscore'];
        $applicationRecordArray['gmatscore'] = $scoreRecords['gmatscore'];
        $applicationRecordArray['toefl'] = $scoreRecords['toefl'];
        $applicationRecordArray['ieltsscore'] = $scoreRecords['ieltsscore'];
        
        return TRUE;
    }
    
    private function exportEducationData($applicationId, &$applicationRecordArray) {
        
        $educationRecords = $this->educationData->export($applicationId);    

        $applicationRecordArray['usersinst'] = $educationRecords['usersinst'];

        return TRUE;
    }

    private function exportRecommendData($applicationId, &$applicationRecordArray) {
        
        $recommendRecords = $this->recommendData->export($applicationId);
        
        $applicationRecordArray['recommend_data']['recommend'] =            // 2D, unindexed            
            $recommendRecords['recommend'];
        
        $applicationRecordArray['recommend_data']['recommender_info'] =     // 2D, unindexed
            $recommendRecords['recommender_info'];
        
        $applicationRecordArray['recommend_data']['recommendforms'] =       // 3D, indexed by recommend.id
            $recommendRecords['recommendforms'];

        return TRUE;
    }
    
    private function exportSupplementalData($applicationId, &$applicationRecordArray) {
        
        $supplementalRecords = $this->supplementalData->export($applicationId);
        
        $applicationRecordArray['experience'] = $supplementalRecords['experience'];
        $applicationRecordArray['fellowships'] = $supplementalRecords['fellowships'];
        $applicationRecordArray['publication'] = $supplementalRecords['publication'];
        
        return TRUE;
    }
    
    private function exportDatafileData($applicationId, $luUsersUsertypesId, &$applicationRecordArray) {
    
        $datafileinfoRecords = $this->datafileData->export($applicationId, $luUsersUsertypesId);  
          
        $applicationRecordArray['datafileinfo'] = $datafileinfoRecords['datafileinfo'];  
    }

    private function exportPaymentData($applicationId, &$applicationRecordArray) {
        
        $applicationRecordArray['payment_data'] = 
            $this->paymentData->export($applicationId);    
        
        return TRUE;
    }
    
    private function exportReviewData($applicationId, &$applicationRecordArray) {
        
        $applicationRecordArray['review_data'] = 
            $this->reviewData->export($applicationId);    
        
        return TRUE;
    }
    
}
?>