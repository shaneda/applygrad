<?php
/*
* Class for system user business logic. 
* 
* Required classes:
*   DB_Applyweb  
*   DB_UnitRole 
*/

class User
{    
    
    private $DB_UnitRole;
    private $DB_ProgramGroupRole;
    
    private $firstName;
    private $middleName;
    private $lastName;
    private $email;
    private $csId;
    private $andrewId;
    private $eceId;
    private $qatarId;
    
    private $webIso;
    private $userIds;
    private $unitRoles;
    private $periodRoles;
    
    private $userId;
    private $userRoleId;
    private $userRole;
    private $userRoleDepartment;
    private $adminDomains = array();        // array of domain ids
    private $adminDepartments = array();    // array of department ids
    private $adminUnits = array();          // array of unit ids           

    
    public function __construct() {
        
        $this->DB_UnitRole = new DB_UnitRole();
        $this->DB_ProgramGroupRole = new DB_ProgramGroupRole();
    }
    
    public function getFullName($lastNameFirst = FALSE, $includeMiddleName = FALSE) {
    
        $familyName = $this->lastName;
        $givenName = $this->firstName;
        if ($includeMiddleName && $this->middleName) {
            $givenName .= ' ' . $this->middleName; 
        }
        if ($lastNameFirst) {
            return $familyName . ', ' . $givenName;
        } else {
            return $givenName . ' ' . $familyName;
        }
        
    }
    
    public function getUserId() {
        return $this->userId;
    }
    
    public function getUserRoleId() {
        return $this->userRoleId;
    }
    
    public function getUserRole() {
        return $this->userRole;
    }
    
    public function getAdminUnits() {
        return $this->adminUnits;
    }
    
    public function getUnitRoles() {
        return $this->unitRoles;
    }

    public function getPeriodRoles() {
        return $this->periodRoles;
    }
    
    public function hasUnitRole($unitId, $roleId) {
    
        foreach ($this->unitRoles as $unitRole) {
            if ($unitRole['unit_id'] == $unitId 
                && $unitRole['role_id'] == $roleId) 
            {
                return TRUE;
            }
        }
        
        return FALSE;
    }

    public function hasPeriodRole($programGroupId, $roleId) {
        
        foreach ($this->periodRoles as $periodRole) {
            if ($periodRole['program_group_id'] == $programGroupId 
                && $periodRole['role_id'] == $roleId) 
            {
                return TRUE;
            }
        }
        
        return FALSE;        
    }

    public function loadFromSession() {
        
        $this->firstName = $_SESSION['A_firstname'];
        $this->lastName = $_SESSION['A_lastname'];
        $this->userId = $_SESSION['A_usermasterid'];
        $this->userRoleId = $_SESSION['A_userid'];
        $this->userRole = $_SESSION['A_usertypename'];
        
        if ( isset($_SESSION['roleDepartmentName']) ) {
            $this->userRoleDepartment = $_SESSION['roleDepartmentName'];    
        }
        
        if ( isset($_SESSION['userWebIso']) ) {           
            $this->webIso = $_SESSION['userWebIso'];
            $this->loadUserIds();
            $this->loadUnitRoles();
            $this->loadPeriodRoles();
        }
        
        foreach ($_SESSION['A_admin_domains'] as $domainId) {
            if ( $domainId && !in_array($domainId, $this->adminDomains) ) {
                $this->adminDomains[] = intval($domainId);    
            }
        }
        
        foreach ($_SESSION['A_admin_depts'] as $departmentId) {
            if ( $departmentId && !in_array($departmentId, $this->adminDepartments) ) {
                $this->adminDepartments[] = intval($departmentId);    
            }
        }
        
        $this->loadAdminUnits();
    }
    
    private function loadUserIds() {
        
        $userIdQuery = sprintf("SELECT scs_user.* FROM scs_user_webiso, scs_user 
            WHERE scs_user_webiso.webiso = '%s' AND scs_user_webiso.scs_user_id = scs_user.scs_user_id",
            mysql_real_escape_string($this->webIso));
        
        $userIdRecords = $this->DB_UnitRole->handleSelectQuery($userIdQuery);
        foreach ($userIdRecords as $userIdRecord) {
            $this->userIds[] = $userIdRecord['users_id'];
        } 
        
        return TRUE;
    }

    private function loadUnitRoles() {
    
        if (!$this->userIds)
        {
            return TRUE;
        }
        
        $userIdString = implode(',', $this->userIds);
        
        if ($userIdString) {
            
            $unitRoleQuery = "SELECT unit_id, role_id 
                                FROM unit_role 
                                WHERE users_id IN (" . $userIdString . ")";     
            
            $this->unitRoles = $this->DB_UnitRole->handleSelectQuery($unitRoleQuery); 
            
            return TRUE;
            
        } else {
            
            return FALSE;
        }
    }
    
    private function loadPeriodRoles() {

        if (!$this->userIds)
        {
            return TRUE;
        }
        
        $userIdString = implode(',', $this->userIds);
        
        if ($userIdString) {
            
            $unitRoleQuery = "SELECT program_group_id, role_id 
                                FROM program_group_role 
                                WHERE users_id IN (" . $userIdString . ")";     
            
            $this->periodRoles = $this->DB_UnitRole->handleSelectQuery($unitRoleQuery); 
            
            return TRUE;
            
        } else {
            
            return FALSE;
        }
        
    }    
    
    private function loadAdminUnits() {
    
        $unitRoleRecords = $this->DB_UnitRole->find($this->userRoleId);
        foreach ($unitRoleRecords as $unitRoleRecord) {
            $this->adminUnits[] = $unitRoleRecord['unit_id'];    
        }
        if ( count($this->adminUnits) > 0 || 
                ($this->userRole != 'Super User' && $this->userRole != 'Administrator') ) {
            return TRUE;
        }        
    
        // Get admin units from departments when no unit roles are assigned.
        $adminDepartmentCount = count($this->adminDepartments); 
        if ($this->userRole == 'Super User' || $adminDepartmentCount > 0) {
            if ($this->userRole == 'Super User') {
                $programQuery = "SELECT unit.* FROM unit WHERE unit.parent_unit_id IS NULL";    
            } else {
                if ($adminDepartmentCount == 1) {
                    $programWhere = "lu_programs_departments.department_id = " . $this->adminDepartments[0];
                } else {
                    $programWhere = "lu_programs_departments.department_id IN (";
                    $programWhere .= implode(',', $this->adminDepartments) . ")";
                }
                $programQuery = "SELECT DISTINCT unit.*
                                    FROM unit
                                    INNER JOIN programs_unit ON unit.unit_id = programs_unit.unit_id
                                    INNER JOIN lu_programs_departments ON programs_unit.programs_id = lu_programs_departments.program_id
                                    WHERE " . $programWhere;                              
            }
            $programRecords = $this->DB_UnitRole->handleSelectQuery($programQuery, 'unit_id');
            foreach ($programRecords as $programRecord) {
                if ($programRecord['parent_unit_id'] != NULL) {
                    $unitId = $programRecord['parent_unit_id'];
                } else {
                    $unitId = $programRecord['unit_id'];
                }
                if ( !in_array($unitId, $this->adminUnits) ) {
                    $this->adminUnits[] = $unitId;    
                }    
            }
        }   
    }
    
}
?>