<?php

/*
Class for phonescreen requests
Primary tables: phone_screens
*/

class DB_Phonescreens extends DB_Applyweb
{

    //protected $application_id;

    // Check whether phone screens have been requested by *anyone*.
    // This should always return a record for a valid application id.
    function getPhoneScreenRequested($application_id) {
    
        /*
        $query = "SELECT application_id,
                    MAX(technical_screen) as technical_screen_requested,
                    MAX(language_screen) as language_screen_requested
                    FROM phone_screens                 
                    WHERE application_id = " . $application_id . "
                    GROUP BY application_id";
        */
        $query = "SELECT application.id,
                    IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
                    IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested
                    FROM application
                    LEFT OUTER JOIN (
                        SELECT application_id, 
                        MAX(technical_screen) as technical_screen_requested,
                        MAX(language_screen) as language_screen_requested
                        FROM phone_screens
                        GROUP BY application_id            
                    ) as phone_screens ON application.id = phone_screens.application_id
                    WHERE application.id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }
    
    
    function getPhoneScreen($application_id, $reviewer_id) {
    
        $query = "SELECT * FROM phone_screens                 
                    WHERE application_id = " . $application_id . "
                    AND reviewer_id =  " . $reviewer_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    function updatePhoneScreen($application_id, $reviewer_id, $technical_screen, $language_screen) {
    
        $query = "REPLACE INTO phone_screens VALUES ("
                    . $application_id . ","
                    . $reviewer_id . ","
                    . $technical_screen . ","
                    . $language_screen .
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }
    

    
}

?>
