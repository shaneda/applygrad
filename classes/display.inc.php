<?php

function displayBio($bio_records) {
    ?>
    
    <p><b><?= $bio_records[0]['lastname'] ?>, <?= $bio_records[0]['firstname'] ?></b></p>
    
    <?php    
}


function displayPayment($payment_records, $total_fees_record) {
    
    $payment_record = $payment_records[0];
    
    $fee_waived = $payment_record['waive'];
    
    $total_fees = number_format($total_fees_record['total_fees'], 2);
    
    // change display if fee waived
    if ( $fee_waived )  {
        $form_disabled = "disabled";
        //print "<b><p>Fee Waived</p></b>";                
    } else {
        $form_disabled = "";
    } 

    // determine whether to have the "payment received" box checked
    $paid_checked = "";
    if ($payment_record['paid'] == 1) {
        $paid_checked = "checked";
    }

    $application_id = $payment_record['id'];
    $payment_amount = $payment_record['paymentamount'];
    $payment_date = date("d/m/Y H:i:s", strtotime($payment_record['paymentdate']));
    
    $payment_form_id = $application_id . "_payment";
    $fee_paid_id = $application_id . "_fee_paid";
    $update_payment_amount_id = $application_id . "_update_payment_amount";
    $update_payment_submit_id = $application_id . "_update_payment_submit";
    $reset_payment_submit_id = $application_id . "_reset_payment_submit";
    $total_fees_span_id =  $application_id . "_total_fees_span"; 
    
    ?>

    <form id="<?= $payment_form_id ?>">
    <table cellpadding="2px" cellspacing="2px">
 
        <tr>
        <td colspan="2">
        <?
        $checkbox_id = $application_id . "_fee_waived";
        if ( $fee_waived ) {
            $waived_checked = "checked";
        } else {
            $waived_checked = "";
        }
        print '<input type="checkbox" class="fee_waived" id="'. $checkbox_id . '" ' . $waived_checked . ' />';
        ?>
        Fee Waived
        </td>
        </tr>
        <tr>
            
            <td colspan="2">Total Fees: $<span id="<?= $total_fees_span_id ?>"><?= $total_fees ?></span> &nbsp; 
            <input type="checkbox" class="fee_paid" id="<?= $fee_paid_id ?>" <?= $paid_checked ?> <?= $form_disabled ?> /> Paid

            
            <?php 
            if ($payment_amount) {
                print "<br /><br />Payment:  $" . $payment_amount;
            }
            ?>
            </td>
        </tr>
    <?php
    if ($payment_record['paid'] == 1) {
        // show payment date
        ?>
        <tr>
            <td colspan="2">Payment Date: <?= $payment_date ?></td>
        </tr>
        <?php
    }
    ?>
        <tr>
            <td>New Payment Amount:</td>
            <td>
            <input type="text" size="5" class="update_payment_amount" id="<?= $update_payment_amount_id ?>" value="" <?= $form_disabled ?>> 
            <input type="submit" class="update_payment_submit" id="<?= $update_payment_submit_id ?>" value="Update Amount"  <?= $form_disabled ?>/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <br />
            <input type="submit" class="reset_payment_submit" id="<?= $reset_payment_submit_id ?>" value="Reset Payment"  <?= $form_disabled ?>/>
            </td>
        </tr>
    </table>
    </form>
    
    <?php
     
}


function displayEducation($education_records) {
    
     $application_id = $education_records[0]['application_id'];
     $transcript_form_id = $application_id . "_transcripts"; 

    ?>
    
    <form id="<?= $transcript_form_id ?>">
    <table cellspacing="5px" cellpadding="5px" border="0">
    <?php
    foreach ($education_records as $record) {
        
        // if record isn't full of null values
        $trascript_counter = 0;
        if ($record['institute_id'] != NULL) {
        
            // determine whether to have the "transcript received" box checked
            $received_checked = "";
        
            // NOTE misspelling of field transcriptreceived with "ss"
            if ($record['transscriptreceived'] == 1) {
                $received_checked = "checked";
                }
        
            $transcript_received_id = $record['id'] . "_transcript_received";
        
            print "<tr valign=\"top\"><td>";
            //print $record['id'] . ".&nbsp;";
            print $record['name'] . "<br/>";
            print $record['degree'] . ", " . $record['major1'] . "<br/>";
            print $record['date_entered'] . " - ";
            if ( $record['date_grad'] ) {
                print $record['date_grad'];
            } else {
                print $record['date_left'];
            }
            ?>
            </td><td align="center">
            <input type="checkbox" class="transcript_received" id="<?= $transcript_received_id ?>" <?= $received_checked ?> /> Received
            </td></tr>
            <?php
            $transcript_counter++;
        }
    }
    
    if ( $transcript_counter == 0 ) {
        ?>
        <td colspan="3"><p>No transcripts found.</p></td> 
        <?php
    }
      
    print "</table></form>";
}


function displayRecommendations($recommendation_records) {
    
    // vars for use in forms
    $application_id = $recommendation_records[0]['application_id'];
    $applicant_id = $recommendation_records[0]['applicant_id'];
    $applicant_guid = $recommendation_records[0]['guid'];
    $recommendation_form_id = $application_id . "_recommendations";
    ?>

    <br />
    <form id="<?= $recommendation_form_id ?>">
    <input type="hidden" class="applicant_id" id="<?= $applicant_id ?>" />
    <input type="hidden" class="applicant_guid" id="<?= $applicant_guid ?>" />
    <table cellspacing="5px" cellpadding="5px" border="0">
    <?php
    $recommendation_count = 0;
    foreach ($recommendation_records as $record) {
                
        // if the record isn't full of null values, then display
        if ( $record['rec_user_id'] != -1 ) {
        
            print "<tr valign=\"top\"><td>";
            //print $record['id'] . ". ";
            print $record['lastname'] . ", " . $record['firstname'] . "<br/>";
            //print $record['company'];
            print "</td>";
        
            // more vars for use in forms
            $upload_recommendation_file_id = $record['id'] . "_upload_recommendation_file";
            $upload_recommendation_submit_id = $record['id'] . "_upload_recommendation_submit";

            // if the recommendation has been submitted with an uploaded file
            if ( $record['datafile_id'] ) {
                
                // vars for link to file - needs to be updated
                /*
                $filename = "recommendation_" . $record['userdata'] . "." . $record['extension'];                 
                $filedir = "./files/";
                $filepath = $filedir . $filename;
                */
                
                ?>
                <td>
                <?php
                /* no link for now
                <a href="<?= $filepath ?>" target="_blank"><?= $filename ?></a>&nbsp;&nbsp;
                */
                print '<span class="confirm_complete">Received</span><br />(file)';
                ?>
                </td>
                <td>
                <input type="hidden" class="upload_mode" value="update" />
                <!-- Note: name of file input must be "fileToUpload" for ajax function to work -->
                <input size="30" class="upload_recommendation_file" id="<?= $upload_recommendation_file_id ?>" name="fileToUpload" type="file" /> 
                <input class="upload_recommendation_submit" id="<?= $upload_recommendation_submit_id ?>" type="submit" value="Update Letter" />
                
                </td>
                <?php
            
            } else {
                          
                print "<td>";
                // if the recommendation was entered as "content"
                // check first to see if field contains more than "&lt;!--tml_entity_decode($content--&gt;" 
                $content_length = strlen($record['content']); 
                if ( $content_length > 50 ) {
                    print '<span class="confirm_complete">Received</span><br />(text)';
                } else {
                    print "Requested";
                }
                ?> 
                </td>
                <td>
                <input type="hidden" class="upload_mode" value="new" />
                <!-- Note: name of file input must be "fileToUpload" for ajax function to work -->  
                <input size="30" class="upload_recommendation_file" id="<?= $upload_recommendation_file_id ?>" name="fileToUpload" type="file" /> 
                <input class="upload_recommendation_submit" id="<?= $upload_recommendation_submit_id ?>" type="submit" value="Upload Letter" />
                </td>
                
            <?php
            }
            $recommendation_count++;
            print "</tr>";
        
        } 
    }
    
    // indicate no records found
    if ( $recommendation_count == 0 ) {
        ?>
        <td colspan="3"><p>No recommendations found.</p></td>
        <?php
    }
      
    print "</table></form>";
}


function displayGREGeneralScore($gre_general_records) {

    $gre_record = $gre_general_records[0];
    
    $gre_general_score_form_id = $gre_record['application_id'] . "_grescore";
    
    $gre_general_received_id = $gre_record['id'] . "_gre_general_received";
    
    $gre_general_testdate = date("m/Y", strtotime($gre_record['testdate']));
    
    $gre_general_verbal_score_id = $gre_record['id'] . "_gre_general_verbal_score";
    $gre_general_verbal_score = $gre_record['verbalscore'];
    $gre_general_verbal_percentile_id = $gre_record['id'] . "_gre_general_verbal_percentile";
    $gre_general_verbal_percentile = $gre_record['verbalpercentile'];
    
    $gre_general_quantitative_score_id = $gre_record['id'] . "_gre_general_quantitative_score";
    $gre_general_quantitative_score = $gre_record['quantitativescore'];
    $gre_general_quantitative_percentile_id = $gre_record['id'] . "_gre_general_quantitative_percentile";
    $gre_general_quantitative_percentile = $gre_record['quantitativepercentile'];         

    $gre_general_analytical_score_id = $gre_record['id'] . "_gre_general_analytical_score";
    $gre_general_analytical_score = $gre_record['analyticalscore'];
    $gre_general_analytical_percentile_id = $gre_record['id'] . "_gre_general_analytical_percentile";
    $gre_general_analytical_percentile = $gre_record['analyticalpercentile'];

    $gre_general_writing_score_id = $gre_record['id'] . "_gre_general_writing_score";
    $gre_general_writing_score = $gre_record['analyticalwritingscore'];
    $gre_general_writing_percentile_id = $gre_record['id'] . "_gre_general_writing_percentile";
    $gre_general_writing_percentile = $gre_record['analyticalwritingpercentile']; 

    $update_gre_general_score_submit_id = $gre_record['id'] . "_update_gre_general_score_submit";
    
    // determine whether to have the "score received" box checked
    $received_checked = "";
    if ($gre_record['scorereceived'] == 1) {
        $received_checked = "checked";
    }
?>

<form class="gre_general_score" id="<?= $gre_general_score_form_id ?>">
<table cellpadding="2px" cellspacing="2px">
    <tr>
        <td colspan="3">GRE General&nbsp;&nbsp;
        <input type="checkbox" class="gre_general_received" id="<?= $gre_general_received_id ?>"  <?= $received_checked ?> /> Received
        </td>
    </tr>
    <tr>
        <td colspan="3"><i>Test Date: <?= $gre_general_testdate ?></i></td>
    </tr>
    <tr>
        <td></td>
        <td>Score</td>
        <td>Percentile</td>
    </tr>
    <tr>
        <td>Verbal</td>
        <td>
        <input type="text" size="5" class="gre_general_verbal_score" id="<?= $gre_general_verbal_score_id ?>" value="<?= $gre_general_verbal_score ?>">
        </td>
        <td>
        <input type="text" size="5" class="gre_general_verbal_percentile" id="<?= $gre_general_verbal_percentile_id ?>" value="<?= $gre_general_verbal_percentile ?>">
        </td>
    </tr>
    <tr>
        <td>Quantitative</td>
        <td>
        <input type="text" size="5" class="gre_general_quantitative_score" id="<?= $gre_general_quantitative_score_id ?>" value="<?= $gre_general_quantitative_score ?>">
        </td>
        <td>
        <input type="text" size="5" class="gre_general_quantitative_percentile" id="<?= $gre_general_quantitative_percentile_id ?>" value="<?= $gre_general_quantitative_percentile ?>">
        </td>
    </tr>
    <tr>
        <td>Analytical</td>
        <td>
        <input type="text" size="5" class="gre_general_analytical_score" id="<?= $gre_general_analytical_score_id ?>" value="<?= $gre_general_analytical_score ?>">
        </td>
        <td>
        <input type="text" size="5" class="gre_general_analytical_percentile" id="<?= $gre_general_analytical_percentile_id ?>" value="<?= $gre_general_analytical_percentile ?>">
        </td>
    </tr>
    <tr>
        <td>Writing</td>
        <td>
        <input type="text" size="5" class="gre_general_writing_score" id="<?= $gre_general_writing_score_id ?>" value="<?= $gre_general_writing_score ?>">
        </td>
        <td>
        <input type="text" size="5" class="gre_general_writing_percentile" id="<?= $gre_general_writing_percentile_id ?>" value="<?= $gre_general_writing_percentile ?>">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <!-- <input type="reset" value="Reset"> --> 
        <input type="submit" class="update_gre_general_score_submit" id="<?= $update_gre_general_score_submit_id ?>" value="Update Score"/>
        </td>
    </tr>
</table> 
</form>    
    
<?php   
}


function displayGRESubjectScore($gre_subject_record) {
    
    $gre_subject_score_form_id = $gre_subject_record['application_id'] . "_gresubjectscore";
    
    $gre_subject_received_id = $gre_subject_record['id'] . "_gre_subject_received";
    
    $gre_subject_testdate = date("m/Y", strtotime($gre_subject_record['testdate']));
    
    $gre_subject_name =  $gre_subject_record['name'];
    $gre_subject_name_id = $gre_subject_record['id'] . "_gre_subject_name";
    $gre_subject_score_id = $gre_subject_record['id'] . "_gre_subject_score";
    $gre_subject_score = $gre_subject_record['score'];
    $gre_subject_percentile_id = $gre_subject_record['id'] . "_gre_subject_percentile";
    $gre_subject_percentile = $gre_subject_record['percentile'];

    $update_gre_subject_score_submit_id = $gre_subject_record['id'] . "_update_gre_subject_score_submit";
    
    // determine whether to have the "score received" box checked
    $received_checked = "";
    if ($gre_subject_record['scorereceived'] == 1) {
        $received_checked = "checked";
    }
?>

<form class="gre_subject_score" id="<?= $gre_subject_score_form_id ?>">
<table cellpadding="2px" cellspacing="2px">
    <tr>
        <td colspan="2">GRE Subject&nbsp;&nbsp;
        <input type="checkbox" class="gre_subject_received" id="<?= $gre_subject_received_id ?>"  <?= $received_checked ?> /> Received
        </td>
    </tr>
    <tr>
        <td colspan="3"><i>Test Date: <?= $gre_subject_testdate ?></i></td>
    </tr>
    <tr>
        <td>Name</td>
        <td>
        <input type="text" size="15" class="gre_subject_name" id="<?= $gre_subject_name_id ?>" value="<?= $gre_subject_name ?> ">
        </td>
    </tr>
    <tr>
        <td>Score</td>
        <td>
        <input type="text" size="5" class="gre_subject_score" id="<?= $gre_subject_score_id ?>" value="<?= $gre_subject_score ?>">
        </td>
    </tr>
    <tr>
        <td>Percentile</td>
        <td>
        <input type="text" size="5" class="gre_subject_percentile" id="<?= $gre_subject_percentile_id ?>" value="<?= $gre_subject_percentile ?>">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <!-- <input type="reset" value="Reset"> -->
        <input type="submit" class="update_gre_subject_score_submit" id="<?= $update_gre_subject_score_submit_id ?>" value="Update Score"/>
        </td>
    </tr>
</table> 
</form>   
    
<?php   
}


function displayTOEFLScore($toefl_records) {

    $toefl_record = $toefl_records[0];

    $toefl_score_form_id = $toefl_record['application_id'] . "_toeflscore";
    
    $toefl_received_id = $toefl_record['id'] . "_toefl_received";
    
    $toefl_testdate = date("m/Y", strtotime($toefl_record['testdate']));
    
    $toefl_section1_score_id = $toefl_record['id'] . "_toefl_section1_score";
    $toefl_section1_score = $toefl_record['section1'];
    
    $toefl_section2_score_id = $toefl_record['id'] . "_toefl_section2_score";
    $toefl_section2_score = $toefl_record['section2'];     

    $toefl_section3_score_id = $toefl_record['id'] . "_toefl_section3_score";
    $toefl_section3_score = $toefl_record['section3'];

    $toefl_essay_score_id = $toefl_record['id'] . "_toefl_essay_score";
    $toefl_essay_score = $toefl_record['essay'];
    
    $toefl_total_score_id = $toefl_record['id'] . "_toefl_total_score";
    $toefl_total_score = $toefl_record['total'];


    $update_toefl_score_submit_id = $toefl_record['id'] . "_update_toefl_score_submit";
    
    // determine whether to have the "score received" box checked
    $received_checked = "";
    if ($toefl_record['scorereceived'] == 1) {
        $received_checked = "checked";
    }
?>

<form class="toefl_score" id="<?= $toefl_score_form_id ?>">
<table cellpadding="2px" cellspacing="2px">
    <tr>
        <td colspan="3">TOEFL&nbsp;&nbsp;
        <input type="checkbox" class="toefl_received" id="<?= $toefl_received_id ?>"  <?= $received_checked ?> /> Received
        </td>
    </tr>
    <tr>
        <td colspan="3"><i>Test Date: <?= $toefl_testdate ?></i></td>
    </tr>
    <tr>
        <td>Section 1</td>
        <td>
        <input type="text" size="5" class="toefl_section1_score" id="<?= $toefl_section1_score_id ?>" value="<?= $toefl_section1_score ?>">
        </td>
    </tr>
    <tr>
        <td>Section 2</td>
        <td>
        <input type="text" size="5" class="toefl_section2_score" id="<?= $toefl_section2_score_id ?>" value="<?= $toefl_section2_score ?>">
        </td>
    </tr>
    <tr>
        <td>Section 3</td>
        <td>
        <input type="text" size="5" class="toefl_section3_score" id="<?= $toefl_section3_score_id ?>" value="<?= $toefl_section3_score ?>">
        </td>
    </tr>
    <tr>
        <td>Essay</td>
        <td>
        <input type="text" size="5" class="toefl_essay_score" id="<?= $toefl_essay_score_id ?>" value="<?= $toefl_essay_score ?>">
        </td>
    </tr>
    <tr>
        <td>Total</td>
        <td>
        <input type="text" size="5" class="toefl_total_score" id="<?= $toefl_total_score_id ?>" value="<?= $toefl_total_score ?>">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
        <!-- <input type="reset" value="Reset"> -->
        <input type="submit" class="update_toefl_score_submit" id="<?= $update_toefl_score_submit_id ?>" value="Update Score"/>
        </td>
    </tr>
</table> 
</form>    
    
<?php   
}


function displayTestScores($gre_general_records, $gre_subject_records, $toefl_records) {
?>

<table cellpadding="12px">
    <tr valign="top">
        <td><?php displayGREGeneralScore($gre_general_records); ?></td>
        <?php 
        foreach ($gre_subject_records as $gre_subject_record) {
            if ( $gre_subject_record['score'] != NULL ) {
                print "<td>";
                displayGRESubjectScore($gre_subject_record);
                print "</td>";
            }
        }
        ?>
        <td>
        <?php 
        if ($toefl_records[0]['section1'] != NULL ) {
            displayTOEFLScore($toefl_records);
        }
        ?>
        </td>
    </tr>
</table>
    
<?php
}


function displayPublications($publication_records) {

    $publications_form_id = $publication_records[0]['application_id'] . "_publications";
?>

<form class="publications" id="<?= $publications_form_id ?>">
<table cellpadding="12px">
    <tr valign="top">
    <?php
        if ( count($publication_records) > 0 ) {
            $publication_count = 0;
            foreach($publication_records as $publication_record) {
                if ( $publication_record['title'] != NULL ) {
                    print "<td>";
                    print $publication_record['title'] . ". ";
                    print $publication_record['author'] . ". ";
                    print $publication_record['forum'] . ". ";
                    if ($publication_record['citation'] != "") {
                        print $publication_record['citation'] . "."; 
                    }
                    print "<br /><br />Status: ";
                    makePublicationStatusSelect($publication_record['id'], $publication_record['status']);
                    print "</td>";
                    $publication_count++;
                }
            }
            if ( $publication_count == 0 ) {
                print "<td>No publications found.</td>";
            }     
        } else {
            print "<td>No publications found.</td>"; 
        }          
    ?>
    </tr>
</table>
    
<?php
}

// function to use with displayPublications
function makePublicationStatusSelect($publication_id, $status) {
    $options = array("In Progress", "Submitted", "Accepted", "Not Accepted", "No Plans to Submit");
    ?>
    <select class="publication_status" id="<?= $publication_id ?>_publication_status" name="<?= $publication_id ?>_publication_status">
    <?php
    foreach($options as $option) {
        print '<option ="' . $option . '" ';
        if ($option == $status) {
            print 'selected';
        }
        print '>' . $option . '</option>';
    }
    ?>
    </select>
    <?php
}



/*
Simple function to print key/value pairs for fields in a db record
*/
function loopValues($db_records) {
    if ( is_array($db_records[0]) ) {
        foreach ($db_records as $record) {
            foreach ($record as $key => $value) {
                print $key . ": " . $value . "<br/>\n";
            }
        print "----<br/>\n";
        }    
    } else {
        foreach ($db_records as $key => $value) {
            print $key . ": " . $value . "<br/>\n";
        }    
    }
}




?>