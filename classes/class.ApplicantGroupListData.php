<?php
include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_ReviewList.php";
include "../classes/DB_Applyweb/class.VW_ApplicantGroupListBase.php";
include "../classes/DB_Applyweb/class.VW_ApplicantGroupListUserInfo.php";
include "../classes/DB_Applyweb/class.VW_ReviewListGroups.php";
include "../classes/DB_Applyweb/class.VW_ReviewListReviews.php";

class ApplicantGroupListData
{
    
    private $departmentId;
    
    public function __construct($departmentId) {
        $this->departmentId = $departmentId;    
    }
    
    public function getData($periodId = NULL, $round = 1, $reviewerId = NULL, $groups = 'allGroups', $searchString = '', $sort = NULL) {
        
        $groupsLimitReviewerId = NULL;
        if ( $reviewerId && ($groups == 'myGroups') ) {
            $groupsLimitReviewerId = $reviewerId;    
        }
        //DebugBreak();
        $baseView = new VW_ApplicantGroupListBase($sort);
        $baseArray = $baseView->find($periodId, $this->departmentId, $round, $groupsLimitReviewerId, $searchString);
        
        $userInfoView = new VW_ApplicantGroupListUserInfo();
        $userInfoArray = $userInfoView->find($periodId, $this->departmentId, $round);

        $groupsView = new VW_ReviewListGroups();
        $groupsArray = $groupsView->find($periodId, $this->departmentId);

        $reviewsView = new VW_ReviewListReviews();
        $reviewsArray = $reviewsView->find($periodId, $this->departmentId, $round);
        
        $recordCount = count($baseArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $applicationId = $baseArray[$i]['application_id'];
            
            if ( array_key_exists($applicationId, $userInfoArray) ) { 
                
                $baseArray[$i] = array_merge($baseArray[$i], $userInfoArray[$applicationId]);
            
            } else {

                $baseArray[$i]['gender'] = '';
                $baseArray[$i]['cit_country_iso_code'] = '';
                $baseArray[$i]['cit_country'] = '';
                $baseArray[$i]['undergrad_institute_name'] = '';
                
            }
            
            if ( array_key_exists($applicationId, $userInfoArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $groupsArray[$applicationId]);

            } else {
                
                $baseArray[$i]['round1_groups'] = '';
                $baseArray[$i]['round2_groups'] = '';
                $baseArray[$i]['round3_groups'] = '';
                $baseArray[$i]['round4_groups'] = '';
                                
                
            }
            
            if ( array_key_exists($applicationId, $reviewsArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $reviewsArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['round1_point1_scores'] = '';
                $baseArray[$i]['round1_point1_average'] = '';
                $baseArray[$i]['round1_point2_scores'] = '';
                $baseArray[$i]['round1_point2_average'] = '';
                $baseArray[$i]['round1_comments'] = '';
                $baseArray[$i]['votes_for_round2'] = ''; 
                $baseArray[$i]['round2_point1_committee_scores'] = '';
                $baseArray[$i]['round2_point1_committee_average'] = '';
                $baseArray[$i]['all_point1_committee_scores'] = '';
                $baseArray[$i]['all_point1_committee_average'] = '';
                $baseArray[$i]['all_point2_committee_scores'] = '';
                $baseArray[$i]['all_point2_committee_average'] = '';
                $baseArray[$i]['round2_point1_faculty_scores'] = '';
                $baseArray[$i]['round2_point1_faculty_average'] = '';
                $baseArray[$i]['round2_comments'] = '';
                $baseArray[$i]['round2_pertinent_info'] = '';
                $baseArray[$i]['decision'] = '';
                $baseArray[$i]['admit_to'] = ''; 
            }
            
        }        

        return $baseArray;
    }
}

?>
