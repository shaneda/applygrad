<?php

/*
Class for MSE risk factors
Primary tables: mse_letter_recs
*/

class DB_MseLetterRecs extends DB_Applyweb
{

    //protected $application_id;
   
    function getLetterRecs($application_id, $reviewer_id) {
    
        $query = "SELECT * FROM mse_letter_recs                 
                    WHERE application_id = " . $application_id . "
                    AND reviewer_id =  " . $reviewer_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    function updateLetterRecs($application_id, $reviewer_id, $java, $discrete, 
                                $algorithms, $data_structures) {
        
        $query = "REPLACE INTO mse_letter_recs VALUES ("
                    . $application_id . ","
                    . $reviewer_id . ","
                    . $java . ","
                    . $discrete . ","
                    . $algorithms . ","
                    . $data_structures . 
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }
    
    function getLetterRecsDecision($application_id, $program_id) {
    
        $query = "SELECT * FROM mse_letter_recs_decision                 
                    WHERE application_id = " . $application_id . "
                    AND program_id =  " . $program_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }
    
    function updateLetterRecsDecision($application_id, $program_id, $java, $discrete, 
                                $algorithms, $data_structures) {

        $query = "REPLACE INTO mse_letter_recs_decision VALUES ("
                    . $application_id . ","
                    . $program_id . ","
                    . $java . ","
                    . $discrete . ","
                    . $algorithms . ","
                    . $data_structures  . 
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }

}

?>
