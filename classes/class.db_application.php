<?php
/*
Class for application admin info
Primary table(s): application
*/

class DB_Application extends DB_Applyweb
{
    
    //protected $application_id;


    function getApplication($application_id) {
        
    }
   
    function updateSubmitted($application_id, $submitted) {
   
        if ($submitted) {
            $submitted_date = "NOW()";
        } else {
            $submitted_date = "NULL";
        }
        
        $query = "UPDATE application SET submitted = " . $submitted .
                ", submitted_date = " . $submitted_date . 
                " WHERE id = " . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $query; 
   }


   function updateComplete($application_id, $complete) {
   
       $query = "UPDATE application SET sent_to_program=" . $complete . 
                " WHERE id=" . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }

   
   // OLD METHOD
   /*
   function updatePromotionStatus($application_id, $program_id, $promotion_status) {
   
       $query = "UPDATE lu_application_programs" .
                " SET round2 = '" . $promotion_status . "' 
                 WHERE application_id = " . $application_id .
                " AND program_id = " . $program_id;
       
       $status = $this->handleUpdateQuery($query);
              
       return $status;
   }
   */
   
   function updatePromotionStatus($applicationId, $departmentId, $round) {
       
       $query = "INSERT INTO promotion_status
                    (application_id, department_id, round)
                    VALUES (
                    " . $applicationId . ",
                    " . $departmentId . ",
                    " . $round . ")
                    ON DUPLICATE KEY UPDATE round = " . $round;                    
      
       $status = $this->handleInsertQuery($query);
              
       return $status;       
   }
   
   function updatePromotionHistory($applicationId, $programId, $round, $promotionMethod, $userId = NULL) {
       
       $query = "INSERT INTO promotion_history
                    (application_id, program_id, round, promotion_method, users_id)
                    VALUES (
                    " . $applicationId . ",
                    " . $programId . ",
                    " . $round . ",
                    '" . $promotionMethod . "',";
       if ($userId) {
            $query .= $userId;    
       } else {
            $query .= "NULL";    
       }            
       $query .= ")";       
       
       $status = $this->handleInsertQuery($query);
              
       return $status;       
   }

}

?>