<?php

class AdmissionLetterApplicantListData
{
    
    private $unit;
    private $unitId;
    private $applicationId;
    private $data;          // 2D array
    
    public function __construct($unit, $unitId, $applicationId = NULL) {
        $this->unit = $unit;
        $this->unitId = $unitId;    
        $this->applicationId = $applicationId;
    }
    
    public function getData($admissionPeriodId = NULL, $applicationId = NULL, 
                                $searchString = '', $submissionStatus = 'all',
                                $completionStatus = 'all', $paymentStatus = 'all',
                                $testScoreStatus = 'all', $transcriptStatus = 'all',
                                $recommendationStatus = 'all', $round = 'all', 
                                $luUsersUsertypesId = NULL) 
    {                                                
        if ($this->unit == 'department' && isIniDepartment($this->unitId))
        {
            $baseView = new VW_AdmissionLetterListBaseIni();    
        }
        elseif ($this->unit == 'department' && isDesignDepartment($this->unitId))
        {
            $baseView = new VW_AdmissionLetterListBaseDesignMasters();    
        }
        else
        {
            $baseView = new VW_AdmissionLetterListBase();    
        }
        
        $baseArray = $baseView->find($this->unit, $this->unitId, $admissionPeriodId, 
                                        $applicationId, $searchString, $submissionStatus,
                                        $completionStatus, $paymentStatus, $testScoreStatus,
                                        $transcriptStatus, $recommendationStatus,
                                        $luUsersUsertypesId);
        
        $returnArray = array();
        $baseCount = count($baseArray);
        for ($i = 0; $i < $baseCount; $i++) {
            
            // Set datagrid selection element
            if ($baseArray[$i]['application_id'] == $this->applicationId)
            {
                $baseArray[$i]['selected'] = 1;    
            }
            else
            {
                $baseArray[$i]['selected'] = 0;    
            }
            
            $baseArray[$i]['letter'] = 
                $this->getLetter($baseArray[$i]['application_id'], $baseArray[$i]['guid']);
            
            $baseApplicationId = $baseArray[$i]['application_id'];

            $returnArray[] = $baseArray[$i];
        }  
        
        return $returnArray;
    }
    
    private function getLetter($applicationId, $guid)
    {
        $letterFile = 'admissionLetter_' . $applicationId . '_' . $this->unitId . '.pdf';
        $letterPath = $_SESSION['datafileroot'] . '/' . $guid . '/' . $letterFile;

        if (file_exists($letterPath))
        {
            return $letterFile;    
        }        
        
        return null;
    }

}
?>
