<?php

/*
Class for education info
Primary table(s): usersinst
*/

class DB_Education extends DB_Applyweb
{

    //protected $application_id;
    
    function GetTranscriptApplicationId($transcriptId)
    {
        $query = "SELECT application_id
                    FROM usersinst
                    WHERE id=" . $transcriptId;  
        
        $resultsArray = $this->handleSelectQuery($query);
        if (count($resultsArray) == 1)
        {
            return $resultsArray[0]['application_id'];
        }
        
        return NULL;   
    }
    
    function getAll($application_id) {
    
        $query = "SELECT application.id as application_id,
                    usersinst.*,
                    institutes.name,                    
                    case
                        when educationtype = 1 then 'undergrad'
                        when educationtype = 2 then 'grad'
                        when educationtype = 3 then 'additional'
                        end as educationtype_name, 
                    degreesall.name as degree,
                    gpascales.name as gpa_scale,
                    converted_gpa,
                    datafileinfo.moddate,
                    datafileinfo.size,
                    datafileinfo.extension
                    FROM usersinst
                    /*
                    inner join application on application.user_id = usersinst.user_id
                    */
                    inner join application on application.id = usersinst.application_id
                    left outer join institutes on institutes.id = usersinst.institute_id
                    left outer join degreesall on degreesall.id = usersinst.degree
                    left outer join gpascales on gpascales.id = usersinst.gpa_scale
                    left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
                    WHERE application.id = " . $application_id . 
                    " order by usersinst.educationtype";
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;
    }

   function updateTranscriptReceived($transcript_id, $transcript_received) {
   
       // NOTE field name "transscriptrecieved" is misspelled in db
       $query = "UPDATE usersinst SET transscriptreceived=" . $transcript_received . 
                " WHERE id=" . $transcript_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }

   function updateConvertedGpa($transcript_id, $converted_gpa) {
   
       $query = "UPDATE usersinst 
                SET converted_gpa = '" . $this->escapeString($converted_gpa) . "'
                WHERE id = " . $transcript_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }

    
}                                                                                                               

?>
