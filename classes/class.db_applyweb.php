<?php

include_once "../inc/config.php";

/*
Base class for Applyweb DB model
*/

class DB_Applyweb
{

    protected static $dbh;


    function __construct() {

        // Connect if connection is not already available
        if (!self::$dbh) {
            $this->connect();
        } 

    }


    function __destruct() {

        // Explicitly close connection
        if (self::$dbh) {

            self::$dbh->close();
        
            self::$dbh = FALSE;
        
            // for testing
            // echo "<p>disconnected from db</p>"; 
        }

    }


    function connect() {
        
        // use globals from config.php
        global $db;
        global $db_host;
        global $db_username;
        global $db_password;
        
        // Establish db connection
        // self::$dbh = new mysqli("rogue.fac.cs.cmu.edu", "phdApp", "phd321app");
        self::$dbh = new mysqli($db_host, $db_username, $db_password);
           
        // Exit if connection error
        if (self::$dbh->connect_error) {
            $this->fatal_error( mysqli_connect_error() );
        } else {
            // proceed
            // echo "connected to db<br>\n";
        }
        
        // Select db, exit if error
        if ( self::$dbh->select_db($db) ) {
            // proceed
            // echo "db selected<br>\n";
        } else {
            $this->fatal_error( self::$dbh->error );
        }
        
        // for testing
        // echo "<p>new db connection</p>\n";
        
    }

    
    // Submit a select query, fetch each result row as an associative array
    // with field names as keys, and return a 2D array of result row arrays
    protected function handleSelectQuery($query) {
    
        // Connect to db if established connection not available
        if (!self::$dbh) {
            $this->connect();
        } 
          
        // create new array to hold query results
        $results_array = array();

        // execute the query
        $result = self::$dbh->query($query);
        
        // move results into results_arrray
        while ($row = $result->fetch_assoc()) {
            $results_array[] = $row;
        }
        $result->free();
        
        return $results_array;
    }


    // Submit a stored select query, fetch each result row as an associative 
    // array with field names as keys, take care of the second 'status' result 
    // set from the stored procedure, and return the 2D array of result row arrays
    protected function handleStoredSelectQuery($query) {

        // Connect to db if established connection not available 
        if (!self::$dbh) {
            $this->connect();
        } 
        
        // create new array to hold query results
        $results_array = array();

        // execute multi query to handle multiple result sets
        self::$dbh->multi_query($query);

        // move first result set to results_array
        if ($result = self::$dbh->store_result()) {
            while ($row = $result->fetch_assoc()) {
                $results_array[] = $row;
            }
            $result->free();
        } // ADD ERROR HANDLING HERE

        // check for second result set returned with stored procedure
        if (self::$dbh->more_results()) {
            // loop through (once?) and do nothing
            while (self::$dbh->next_result());
        }
        
        return $results_array;     
    }    

    
    // Submit an insert query and return true/false for success/failure
    protected function handleInsertQuery($query) {
    
        // Connect to db if established connection not available
        if (!self::$dbh) {
            $this->connect();
        } 
        
        // execute the query
        self::$dbh->query($query);
        
        // get the affected rows as return value
        $affected_rows = self::$dbh->affected_rows;
        
        // return affected rows
        return $affected_rows;
          
    }


    // Submit an update query and return true/false for success/failure
    protected function handleUpdateQuery($query) {
    
        // Connect to db if established connection not available
        if (!self::$dbh) {
            $this->connect();
        } 
        
        // execute the query
        self::$dbh->query($query);
        
        // get the affected rows as return value
        $affected_rows = self::$dbh->affected_rows;
        
        // return affected rows
        return $affected_rows;
          
    }


    // use for all db-related errors
    protected function fatal_error($msg) {
        echo "<pre>Error!: $msg\n";
        $bt = debug_backtrace();
        foreach($bt as $line) {
            $args = var_export($line['args'], true);
            echo "{$line['function']}($args) at {$line['file']}:{$line['line']}\n";
        }
        echo "</pre>";
        die();
    }

}

?>
