<?php
require 'MDB2.php';
include '../classes/DB_Table/Department_Table.php';
include '../classes/DB_Table/Domain_Table.php';
include '../classes/DB_Table/AdmissionPeriod_Table.php';  
include '../classes/DB_Table/DepartmentAdmissionPeriod_Table.php';
include '../classes/DB_Table/DomainAdmissionPeriod_Table.php';
include '../classes/HTML_QuickForm/class.AdmissionPeriod_QuickForm.php';
include '../classes/Structures_DataGrid/class.AdmissionPeriod_DataGrid.php';


class AdmissionPeriod_Controller {
    
    // REQUEST variables
    private $unit;                  // domain | [school] | department | program
    private $unitId;
    private $admissionPeriodId;
    private $action;                // list | create | update | delete 
    
    // business/view objects
    private $quickForm = NULL;
    private $dataGrid = NULL;
    
    // database handle, DB_Table objects
    public $dbConnection;
    private $admissionPeriodTable;
    private $unitTable;
    private $unitAdmissionPeriodTable;
    
    public function __construct() {
        
        // Take care of the REQUEST variables.
        $this->handleRequest();

        // Connect to the database.
        global $db_host;
        global $db_username;
        global $db_password;
        global $db;
        global $dbConnection;   // quickForm uses this for date rule callbacks
        //$dsn = 'mysql://phdApp:phd321app@localhost/gradAdmissions2008Dev'; 
        $dsn = 'mysql://' . $db_username . ':' . $db_password . '@' . $db_host . '/' . $db;
        $dbConnection = MDB2::connect( $dsn, array('portability' => MDB2_PORTABILITY_NONE) );
        if (PEAR::isError($dbConnection)) {
            die($dbConnection->getMessage());
        }
        $this->dbConnection = $dbConnection;
        
        // Instantiate a quickform to handle controller logic for most cases. 
        $this->quickForm = new AdmissionPeriod_QuickForm();
        /*
        $this->departmentAdmissionPeriodTable->addFormElements( $this->quickForm, 
                                array('departmentId') );
        */
        
        // Instantiate the unit admission period table and add the id element to the quickForm.
        /*
        $this->departmentAdmissionPeriodTable = new DepartmentAdmissionPeriod_Table(
                                            $dbConnection, "DepartmentAdmissionPeriod");
        $this->departmentAdmissionPeriodTable->addFormElements( $this->quickForm, 
                                array('departmentId') ); 
        */
        
        switch ($this->unit) {
            
            case 'department':
            
                $this->unitTable = new Department_Table($dbConnection, "department");
                $this->unitAdmissionPeriodTable = new DepartmentAdmissionPeriod_Table(
                                            $dbConnection, "DepartmentAdmissionPeriod");
                $this->unitAdmissionPeriodTable->addFormElements( $this->quickForm, 
                                array('departmentId') );
                break;
            
            case 'domain':
            
                $this->unitTable = new Domain_Table($dbConnection, "domain");
                $this->unitAdmissionPeriodTable = new DomainAdmissionPeriod_Table(
                                            $dbConnection, "DomainAdmissionPeriod");
                $this->unitAdmissionPeriodTable->addFormElements( $this->quickForm, 
                                array('domainId') );
                break;
            
            default:
                // Do nothing.
        }
        
        // Instantiate the admission period table and add its field elements to the quickForm.
        $this->admissionPeriodTable = new AdmissionPeriod_Table($dbConnection, "AdmissionPeriod");
        $this->admissionPeriodTable->autoRecast(TRUE);  // to handle conversion of quickForm date arrays
        $this->admissionPeriodTable->addFormElements( $this->quickForm, 
                                array_keys($this->admissionPeriodTable->col) );
        
        // Instantiate an application periods datagrid.
        //$this->dataGrid = new AdmissionPeriod_DataGrid($this->admissionPeriodTable->col, 10);

        // Bind the application periods table to the datagrid.
        /*
        $tableWhere = "departmentId = " . $this->departmentId;
        $tableOptions = array( 'where' => $tableWhere, 'view' => 'listByDepartment');
        $this->dataGrid->bind($this->admissionPeriodTable, $tableOptions);
        */
        
        switch ($this->action) {
            
            case 'create':
            case 'update':

                // Process the form.
                if ($this->quickForm->isSubmitted() && $this->quickForm->validate()) {

                    // Get the form values and drop the unitId for the admissionPeriod insert.
                    $admissionPeriodValues = $this->quickForm->exportValues();
                    array_shift($admissionPeriodValues);
                    //$this->admissionPeriodTable->autoRecast(TRUE);
                    
                    if ($this->action == 'update') {
                        
                        // Update the admission period record. 
                        $admissionPeriodId = $this->admissionPeriodId;
                        $where = "admissionPeriodId = " . $admissionPeriodId;
                        $result = $this->admissionPeriodTable->update($admissionPeriodValues, $where);
                        
                    } else {
                        
                        // Insert new admission period and unit admission period records.
                        $admissionPeriodId = $this->admissionPeriodTable->nextID();
                        $admissionPeriodValues['admissionPeriodId'] = $admissionPeriodId;
                        $result = $this->admissionPeriodTable->insert($admissionPeriodValues);
                        if (PEAR::isError($result)) {
                            die($result->getMessage());    
                        }
                        $unitAdmissionPeriodValues = array(
                                                        $this->unit . 'Id' => $this->unitId, 
                                                        'admissionPeriodId' => $admissionPeriodId);
                        $result = $this->unitAdmissionPeriodTable->insert($unitAdmissionPeriodValues);
                        /*
                        $admissionPeriodDepartmentValues = array(
                                                        'departmentId' => $this->departmentId, 
                                                        'admissionPeriodId' => $admissionPeriodId);
                        $result = $this->departmentAdmissionPeriodTable->insert($admissionPeriodDepartmentValues);
                        */
                    }
                    
                    if (PEAR::isError($result)) {
                        die($result->getMessage());    
                    }
                    
                    // Set the quickForm header.
                    $headerLabel = 'New Admission Period Added';
                    if ($this->action == 'update') {
                        $headerLabel = 'Admission Period Updated';
                    }
                    $this->quickForm->setHeaderLabel($headerLabel);
                    
                    // Add an edit link to the quickForm.
                    $editLink  = '<a href="' . htmlentities($_SERVER['PHP_SELF']);
                    $editLink .= '?unit=' . $this->unit . '&unitId=' . $this->unitId;
                    //$editLink .= '?departmentId=' . $this->departmentId;
                    $editLink .= '&action=update&admissionPeriodId=' . $admissionPeriodId;
                    $editLink .= '">Edit</a>';
                    $this->quickForm->addElement('link', NULL, $editLink);
                    
                    // Freeze the quickForm to display values only.
                    $this->quickForm->freeze();
                    
                    // Bind the application periods table to the datagrid
                    // for brief display alongside the form. (Do it here so changes
                    // are reflected after insert/update.)
                    $this->bindDataGrid(3);
                    
                    // Forego adding the submit button.
                    continue;
                          
                } else {
                    
                    // Set up the form for editing.
                    if ($this->action == 'update') {
                        
                        // Set the defaults with the admission period values.
                        $formDefaults = $this->admissionPeriodTable->get($this->admissionPeriodId);    
                    
                    } else {
                        
                        // Set only the unit id as a default
                        $formDefaults = array($this->unit . 'Id' => $this->unitId);
                        //$formDefaults = array('departmentId' => $this->departmentId);    
                    
                    }
                    
                    // Set the form defaults, and add the header.
                    $this->quickForm->setDefaults($formDefaults);
                    $headerLabel = 'New Admission Period';
                    if ($this->action == 'update') {
                        $headerLabel = 'Edit Admission Period';
                    }
                    $this->quickForm->setHeaderLabel($headerLabel);
                }
                
                // Add submit button, hidden elements when unsubmitted or unvalidated.
                $action = 'create';
                if ($this->action == 'update') {
                    $action = 'update';
                }
                $this->quickForm->addElement('hidden', 'unit', $this->unit);
                $this->quickForm->addElement('hidden', 'unitId', $this->unitId);
                $this->quickForm->addElement('hidden', 'action', $action);
                $this->quickForm->addElement('submit', 'submitForm', 'Submit');
                
                // Bind the application periods table to the datagrid
                // for brief display alongside the form. (Necessary to repeat
                // line 170 here because of continue at line 173.)
                $this->bindDataGrid(3);
                
                break;
                
            case 'delete':
            
                // Delete records.
                $where = "admissionPeriodId = " . $this->admissionPeriodId;
                $this->unitAdmissionPeriodTable->delete($where);
                //$this->departmentAdmissionPeriodTable->delete($where);
                $this->admissionPeriodTable->delete($where);
                
                // Do not break; continue to list.
            
            case 'list':
            default:

                // Nullify the quickForm.
                $this->quickForm = NULL;
                
                // Bind the application periods table to the datagrid for full display.
                $this->bindDataGrid();
        }
        return TRUE; 
    }

    
    private function bindDataGrid($recordLimit = NULL) {
        $this->dataGrid = new AdmissionPeriod_DataGrid($this->admissionPeriodTable->col, $recordLimit);
        $tableWhere = $this->unit . "Id = " . $this->unitId;
        $tableOptions = array( 'where' => $tableWhere, 'view' => 'listBy' . ucfirst($this->unit) );
        //$tableWhere = "departmentId = " . $this->departmentId;
        //$tableOptions = array( 'where' => $tableWhere, 'view' => 'listByDepartment');
        $this->dataGrid->bind($this->admissionPeriodTable, $tableOptions);
    }

    
    public function getUnit() {
        return $this->unit;
    }

    
    public function getUnitId() {
        return $this->unitId;
    }
    /*
    public function getDepartmentId() {
        return $this->departmentId;
    }
    */
    
    public function getUnitName() {
        
        $record = $this->unitTable->get($this->unitId);
        return $record['name'];    
        
    }

    
    public function getQuickForm() {
        return $this->quickForm;
    }

    
    public function getDataGrid() {
        return $this->dataGrid;
    }

    
    private function handleRequest() {
        
        //  If department id is not set, redirect home
        /*
        if ( isset($_REQUEST['departmentId']) ) {
            $this->departmentId = $_REQUEST['departmentId'];
        } else {
            $this->departmentId = NULL;
            $this->redirectHome();
        }
        */
        //  If unit and unitId are not set, redirect home.
        if ( isset($_REQUEST['unit']) && isset($_REQUEST['unitId']) ) {
            $this->unit = $_REQUEST['unit'];
            $this->unitId = $_REQUEST['unitId'];
        } else {
            $this->redirectHome();
        }
        // Otherwise...    

        // Set the data action.
        if ( isset($_REQUEST['action']) ) {
            $this->action = $_REQUEST['action'];
        } else {
            $this->action = "list";    
        }
        
        // Set the period id.
        if ( isset($_REQUEST['admissionPeriodId']) ) {
            $this->admissionPeriodId = $_REQUEST['admissionPeriodId'];
        } else {
            $this->admissionPeriodId = -1;    
        }

        return TRUE;
    }
        

    private function redirectHome() {
        header('Location: home.php');
        exit; 
    }   
}
?>