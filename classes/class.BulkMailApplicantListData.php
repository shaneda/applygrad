<?php

class BulkMailApplicantListData
{
    
    private $unit;
    private $unitId;
    private $allUsersSelected;
    private $selectedUsers;
    private $data;          // 2D array
    
    public function __construct($unit, $unitId, $allUsersSelected, $selectedUsers = NULL) {
        $this->unit = $unit;
        $this->unitId = $unitId;    
        $this->allUsersSelected = $allUsersSelected;
        $this->selectedUsers = $selectedUsers;
    }
    
    public function getData($admissionPeriodId = NULL, $applicationId = NULL, 
                                $searchString = '', $submissionStatus = 'all',
                                $completionStatus = 'all', $paymentStatus = 'all',
                                $testScoreStatus = 'all', $transcriptStatus = 'all',
                                $recommendationStatus = 'all', $round = 'all', 
                                $luUsersUsertypesId = NULL) {
                                    
        // Only base view find method has all limit parameters.
        
        if (isset($_GET['test']) && $_GET['test'] == 'reject') {
            $baseView = new VW_AdminListBaseReject();
        } else {
            $baseView = new VW_AdminListBase();
        }
        $baseArray = $baseView->find($this->unit, $this->unitId, $admissionPeriodId, 
                                        $applicationId, $searchString, $submissionStatus,
                                        $completionStatus, $paymentStatus, $testScoreStatus,
                                        $transcriptStatus, $recommendationStatus,
                                        $luUsersUsertypesId);
                           
        /* 
        * Only limit the other queries by $admissionPeriodId and $applicationId 
        * to speed things up by avoiding unnecessary joins.
        */ 
        $recommendationView = new VW_AdminListRecommendationCount();
        $recommendationArray = $recommendationView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);
        
        $gresubjectView = new VW_AdminListGreSubjectCount();
        $gresubjectArray = $gresubjectView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $toeflView = new VW_AdminListToeflCount();
        $toeflArray = $toeflView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $ieltsView = new VW_AdminListIeltsCount();
        $ieltsArray = $ieltsView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $gmatView = new VW_AdminListGmatCount();
        $gmatArray = $gmatView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);
        
        $feeView = new VW_AdminListFees();
        $feeArray = $feeView->getFeeTotals(NULL, NULL, $admissionPeriodId, 
            $applicationId , '', 'all', $luUsersUsertypesId);

        $domainView = new VW_AdminListDomain();
        $domainArray = $domainView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $periodView = new VW_AdminListPeriod();
        $periodArray = $periodView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);
        
        // Get all programs selected for the given period, not just those that are in the admin unit.
        $programsView = new VW_AdminListPrograms();
        $programsArray = $programsView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $notesView = new VW_AdminListNoteCount();
        $notesArray = $notesView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);
         
        if ($round == 2) {
            $round2Applications = $this->promotedApplications($baseView, $admissionPeriodId, $round);
        }

        $returnArray = array();
        $baseCount = count($baseArray);
        for ($i = 0; $i < $baseCount; $i++) {
            
            // Set datagrid selection element
            if ($this->allUsersSelected || 
                (is_array($this->selectedUsers) && in_array($baseArray[$i]['user_id'], $this->selectedUsers)))
            {
                $baseArray[$i]['selected'] = 1;    
            }
            else
            {
                $baseArray[$i]['selected'] = 0;    
            }
            
            $baseApplicationId = $baseArray[$i]['application_id'];

            if (!$applicationId && !$luUsersUsertypesId && $round == 2) {
                if ( !array_key_exists($baseApplicationId, $round2Applications) ) {
                    continue;        
                }
            }
            
            if ( array_key_exists($baseApplicationId, $recommendationArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $recommendationArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['ct_recommendations_requested'] = 0;
                $baseArray[$i]['ct_recommendations_submitted'] = 0;
                $baseArray[$i]['ct_recommendation_files_submitted'] = 0;
                $baseArray[$i]['ct_recommendation_files_forms_submitted'] = 0;
            }

            if ( array_key_exists($baseApplicationId, $gresubjectArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $gresubjectArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['ct_gresubject_entered'] = 0;
                $baseArray[$i]['ct_gresubject_received'] = 0;
            }

            if ( array_key_exists($baseApplicationId, $toeflArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $toeflArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['toefl_entered'] = 0;
                $baseArray[$i]['toefl_submitted'] = 0;
            }
            
            if ( array_key_exists($baseApplicationId, $ieltsArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $ieltsArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['ct_ielts_entered'] = 0;
                $baseArray[$i]['ct_ielts_received'] = 0;
            }
            
            if ( array_key_exists($baseApplicationId, $gmatArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $gmatArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['ct_gmat_entered'] = 0;
                $baseArray[$i]['ct_gmat_received'] = 0;
            }

            if ( array_key_exists($baseApplicationId, $feeArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $feeArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['total_fees'] = 0;
            }
            
            if ( array_key_exists($baseApplicationId, $domainArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $domainArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['application_domain'] = 0;
            }

            if ( array_key_exists($baseApplicationId, $periodArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $periodArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['period_id'] = 0;
                $baseArray[$i]['umbrella_name'] = '';
                $baseArray[$i]['unit_name_short'] = '';
            }            

            if ( array_key_exists($baseApplicationId, $programsArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $programsArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['programs'] = '';
            }

            if ( array_key_exists($baseApplicationId, $notesArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $notesArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['admin_note_count'] = '';
            }
            
            $returnArray[] = $baseArray[$i];
        }  
        
        return $returnArray;
    }

    
    private function promotedApplications(&$DB_Applyweb, $periodId, $round = 2) {
    
        if ($this->unit == 'domain' || $round != 2) {
            return FALSE;
        }
        
        if ($this->unit == 'program') {
            
            $departmentQuery = "SELECT department_id 
                                FROM lu_programs_departments
                                WHERE program_id = " . $this->unitId;
            $resultArray = $DB_Applyweb->handleSelectQuery($departmentQuery);
            $departmentId = $resultArray[0]['department_id'];
                 
        } else {
            
            $departmentId = $this->unitId;
        }
         $joins[] = "LEFT OUTER JOIN promotion_status ON (application.id = promotion_status.application_id
                       AND promotion_status.department_id = " . $departmentId . ")";
         /*  before Andy Pavlo fix
        $query = "SELECT DISTINCT application.id AS application_id
                    FROM application
                    INNER JOIN period_application 
                        ON application.id = period_application.application_id
                    INNER JOIN lu_application_programs 
                        ON lu_application_programs.application_id = application.id
                    INNER JOIN lu_programs_departments 
                        ON lu_programs_departments.program_id = lu_application_programs.program_id
                    LEFT OUTER JOIN ( 
                        SELECT application_id, 
                        round AS promotion_status_round
                        FROM promotion_status
                        WHERE promotion_status.department_id = " . $departmentId . "
                        ) AS promotion_status ON application.id = promotion_status.application_id";
        */
        $query = "SELECT DISTINCT application.id AS application_id
                    FROM application
                    INNER JOIN period_application 
                        ON application.id = period_application.application_id
                    INNER JOIN lu_application_programs 
                        ON lu_application_programs.application_id = application.id
                    INNER JOIN lu_programs_departments 
                        ON lu_programs_departments.program_id = lu_application_programs.program_id
                    LEFT OUTER JOIN promotion_status ON (application.id = promotion_status.application_id
                       AND promotion_status.department_id = " . $departmentId . ")";
        
        if ($departmentId == 1 || $departmentId == 74 || $departmentId == 2 || $departmentId == 58) {
            
            $query .= " LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          MIN( IFNULL(round2, 0) ) as min_vote,
                          MAX( IFNULL(round2, 0) ) as max_vote,
                          COUNT(round2) AS vote_count 
                          FROM review
                          INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id 
                            AND lu_users_usertypes.usertype_id = 2
                          WHERE review.round = 1
                          AND review.department_id = " . $departmentId . "
                          GROUP BY application_id
                          ) AS round2_votes ON application.id = round2_votes.application_id";           
        }
        
        $query .= " WHERE lu_programs_departments.department_id = " . $departmentId;
        
        if ($periodId) {
            $query .= " AND period_application.period_id = " . $periodId;
        }
        
        if ($departmentId == 1)  {
                
            // CSD has promotion with 2 unanimous votes.
            /* prior to Andy Pavlo fix  
            $query .= " AND ( promotion_status_round = 2
                                OR (
                                    round2_votes.vote_count >= 2 
                                    AND round2_votes.min_vote != 0     
                                    )
                                )";
            */
            $query .= " AND ( promotion_status.round = 2
                                OR (
                                    round2_votes.vote_count >= 2 
                                    AND round2_votes.min_vote != 0     
                                    )
                                )";
                                
        } elseif ($departmentId == 74) { 
            
            // CS MS is round two on one yes vote.
            /* before Andy Pavlo fix
            $query .= " AND ( promotion_status_round = 2
                            OR (
                                round2_votes.vote_count > 0 
                                AND round2_votes.max_vote = 1     
                                )
                            )";
            */
            $query .= " AND ( promotion_status.round = 2
                            OR (
                                round2_votes.vote_count > 0 
                                AND round2_votes.max_vote = 1     
                                )
                            )"; 
                                    
        } elseif ($departmentId == 2) {
            
            // ML has had promotion with three unanimous votes.
            /*    before Andy Pavlo change
            $query .= " AND !(lu_application_programs.round2 <=> '2')
                                AND 
                                ( promotion_status_round = 2
                                    OR promotion_status_round = 3 
                                    OR (
                                        round2_votes.vote_count >= 3 
                                        AND round2_votes.min_vote != 0     
                                    )
                                )";
            */
            $query .= " AND !(lu_application_programs.round2 <=> '2')
                                AND 
                                ( promotion_status.round = 2
                                    OR promotion_status.round = 3 
                                    OR (
                                        round2_votes.vote_count >= 3 
                                        AND round2_votes.min_vote != 0     
                                    )
                                )";
                                              
        } elseif ($departmentId == 58) { 
                
            // Statistics MS has single evaluator, promotion with one yes vote.
            /*  before Andy Pavlo fix
            $wheres[] = " !(lu_application_programs.round2 <=> '2')
                            AND 
                            ( promotion_status_round = 2
                                OR promotion_status_round = 3 
                                OR (
                                    round2_votes.vote_count >= 1 
                                    AND round2_votes.min_vote != 0     
                                )
                            )";
            */
            $wheres[] = " !(lu_application_programs.round2 <=> '2')
                            AND 
                            ( promotion_status_round = 2
                                OR promotion_status_round = 3 
                                OR (
                                    round2_votes.vote_count >= 1 
                                    AND round2_votes.min_vote != 0     
                                )
                            )"; 

        } else {
            
            /* before Andy Pavlo fix
            $query .= " AND promotion_status_round = 2";
            */
            $query .= " AND promotion_status.round = 2";
        }
            
        return $DB_Applyweb->handleSelectQuery($query, 'application_id');
    }
}
?>
