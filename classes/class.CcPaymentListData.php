<?php

class CcPaymentListData
{
    
    private $unit;
    private $unitId;
    
    public function __construct($unit, $unitId) {
        $this->unit = $unit;
        $this->unitId = $unitId;    
    }
    
    public function getData($periodId = NULL, $applicationId = NULL, 
                                $searchString = '', $submissionStatus = 'all',
                                $completionStatus = 'all', $paymentStatus = 'all',
                                $testScoreStatus = 'all', $transcriptStatus = 'all',
                                $recommendationStatus = 'all', $round = 'all', 
                                $luUsersUsertypesId = NULL) {
                                  
        // Only base view find method has all limit parameters.
        $baseView = new VW_AdminListBase();
        $baseArray = $baseView->find($this->unit, $this->unitId, $periodId, 
                                        $applicationId, $searchString, $submissionStatus,
                                        $completionStatus, $paymentStatus, $testScoreStatus,
                                        $transcriptStatus, $recommendationStatus,
                                        $luUsersUsertypesId);

        // 2D array indexed by applicationId
        $ccPayments = $this->getCcPayments($baseView, $periodId, $applicationId);

        // 2D array indexed by paymentId
        $systemStatusRecords = $this->getCcSystemStatus($baseView, $periodId, $applicationId);
              
        // List by payment
        $returnArray = array();
        $baseCount = count($baseArray);
        for ($i = 0; $i < $baseCount; $i++) {
            
            $baseApplicationId = $baseArray[$i]['application_id'];
            if (isset($ccPayments[$baseApplicationId])) {
                
                foreach ($ccPayments[$baseApplicationId] as $paymentRecord) {
                    
                    $paymentId = $paymentRecord['payment_id'];
                    
                    // Fill in payment record with base array data
                    $paymentRecord['payment_number'] = '';
                    $paymentRecord['name'] = $baseArray[$i]['name'];
                    $paymentRecord['email'] = $baseArray[$i]['email'];
                    if ($baseArray[$i]['wd'] == 'yes') {
                        $paymentRecord['fee_status'] = 'waived';
                    } elseif ($baseArray[$i]['pd'] == 'yes') {
                        $paymentRecord['fee_status'] = 'paid';
                    } else {
                        $paymentRecord['fee_status'] = 'unpaid';
                    } 
                    
                    // Determine the CCPS status
                    if ($paymentRecord['last_mod_user_id'] == 0) {
                        $paymentRecord['ccps_status'] = 'pending';    
                    } else {
                        // Admin created the payment
                        $paymentRecord['ccps_status'] = 'N/A';
                    }
                    
                    if (isset($systemStatusRecords[$paymentId])) {

                        if ( isset($systemStatusRecords[$paymentId]['status_date']) ) {
                            
                            $statusRecord = $systemStatusRecords[$paymentId];
                            $authTotal = $statusRecord['auth_total'];
                            $settleTotal = $statusRecord['settle_total'];
                            $creditTotal = $statusRecord['credit_total'];
                            $netSettled =  $settleTotal - $creditTotal;
                            /*
                            $failureSeconds = 216000;   // 2.5 days            
                            if ($authTotal > 0) {
                                $statusTimestamp = strtotime($statusRecord['status_date']);
                            } else {
                                // Auth failed.
                                $statusTimestamp = time();    
                            }
                            */
                        } else {
                            
                            // No status info available.
                            $authTotal = $settleTotal 
                                = $creditTotal = $netSettled = 0;
                            //$failureSeconds = 259200;   // 3 days
                            //$statusTimestamp = time();       
                        }
                        
                        // Check first for settled/refunded payments
                        $paymentAmount = $paymentRecord['payment_amount'];
                        if ($netSettled >= $paymentAmount) 
                        {
                            $paymentRecord['ccps_status'] = 'paid in full';    
                        }
                        elseif ($netSettled > 0 && $netSettled < $paymentAmount)
                        {
                            $paymentRecord['ccps_status'] = 'partial payment';   
                        }
                        elseif ($netSettled == 0 && $creditTotal > 0)
                        {
                            $paymentRecord['ccps_status'] = 'payment refunded';   
                        }
                        else
                        {                       
                            // Then check for failed payments.
                            
                            // Base the failure deadline on the intent date and time
                            $intentDateArray = explode(' ', $paymentRecord['payment_intent_date']);
                            $intentDate = $intentDateArray[0];
                            $intentTime = $intentDateArray[1];
                            $intentTimeArray = explode(':', $intentTime);
                            $intentHour = intval($intentTimeArray[0]);
                            
                            $intentDateMidnightTimestamp = strtotime($intentDate . ' 23:59:59');
                            if ($intentHour < 22) {
                                
                                // Intent before 10:00 PM, base failure on next day's report:
                                // failed if more than 9 hours after midnight of intent day.
                                $failureTimestamp = $intentDateMidnightTimestamp + 32400;
                                
                            } else {
                                
                                // Intent before 10:00 PM, base failure on day after next day's report,
                                // in case ccps form not submitted before midnight:
                                // failed if more than 33 hours after midnight of intent day.
                                $failureTimestamp = $intentDateMidnightTimestamp + 32400;
                            }
                            
                            $currentTimestamp = time();
                            if ($currentTimestamp > $failureTimestamp) {
                            
                                $paymentRecord['ccps_status'] = 'payment failed';     
                            }
                        }
                        
                        /*
                        $paymentIntentTimestamp = strtotime($paymentRecord['payment_intent_date']);
                        $secondsSinceIntent = $statusTimestamp - $paymentIntentTimestamp;
                        $failureDeadlinePassed = FALSE;
                        if ($secondsSinceIntent > $failureSeconds) {
                            $failureDeadlinePassed = TRUE;    
                        }
                        
                        if ($authTotal == 0) 
                        { 
                            if ($failureDeadlinePassed) 
                            {    
                                $paymentRecord['ccps_status'] = 'payment failed';
                            }        
                        }
                        else
                        {
                            if ($netSettled >= $paymentAmount) 
                            {
                                $paymentRecord['ccps_status'] = 'paid in full';    
                            }
                            elseif ($netSettled > 0 && $netSettled < $paymentAmount)
                            {
                                $paymentRecord['ccps_status'] = 'partial payment';   
                            }
                            elseif ($netSettled == 0 && $creditTotal > 0)
                            {
                                $paymentRecord['ccps_status'] = 'payment refunded';   
                            }
                            elseif ($netSettled == 0 && $creditTotal == 0
                                && ($failureDeadlinePassed))
                            {
                                $paymentRecord['ccps_status'] = 'payment failed';    
                            }
                        }
                        */
                    }

                    $returnArray[] = $paymentRecord;
                }
            }
        }  
        
        return $returnArray;
    }
    

    private function getCcPayments(&$DB_Applyweb, $periodId = NULL, $applicationId = NULL) {
        
        $paymentQuery = 
        "
        SELECT application.id AS application_id,
        payment.payment_id,
        payment.payment_intent_date,
        payment.payment_amount,
        payment.payment_status,
        payment.last_mod_time,
        payment.last_mod_user_id
        FROM payment
        INNER JOIN application ON payment.application_id = application.id
        INNER JOIN period_application ON application.id = period_application.application_id
        WHERE payment.payment_type = 2
        AND application.submitted = 1
        ";
        
        if ($applicationId) {
            
            $paymentQuery .= "AND application.id = " . $applicationId;    
            
        } else {
            
            if ($periodId) {
                if (is_array($periodId)) {
                    $periodIds = '(' . implode(',' , $periodId) . ')';
                    $paymentQuery .= "AND period_application.period_id IN " . $periodIds;    
                } else {
                    $paymentQuery .= "AND period_application.period_id = " . $periodId;    
                }
            }

            if ($this->unit && $this->unit == 'department') {
                
                $paymentQuery .=
                "
                AND application.id IN
                (
                  SELECT DISTINCT lu_application_programs.application_id
                  FROM lu_application_programs
                  INNER JOIN lu_programs_departments
                    ON lu_programs_departments.program_id = lu_application_programs.program_id
                  WHERE lu_programs_departments.department_id = " . $this->unitId . "
                )
                ";            
            }
            
            if ($this->unit && $this->unit == 'domain') {
                
                $paymentQuery .=
                "
                AND application.id IN
                (
                  SELECT DISTINCT lu_application_programs.application_id
                  FROM lu_application_programs
                  INNER JOIN lu_programs_departments
                    ON lu_programs_departments.program_id = lu_application_programs.program_id
                  INNER JOIN lu_domain_department
                    ON lu_programs_departments.department_id = lu_domain_department.department_id
                  WHERE lu_domain_department.domain_id = " . $this->unitId . "
                )
                ";            
            }
        }
        
        $paymentQuery .= "ORDER BY payment.payment_id";
        
        $paymentResultArray = $DB_Applyweb->handleSelectQuery($paymentQuery);
        
        $paymentRecords = array();
        foreach ($paymentResultArray as $paymentRecord) {
            $paymentApplicationId = $paymentRecord['application_id'];    
            $paymentRecords[$paymentApplicationId][] = $paymentRecord;
        }
        
        return $paymentRecords;
    }

    private function getCcSystemStatus(&$DB_Applyweb, $periodId = NULL, $applicationId = NULL) {
        
        $systemStatusQuery = 
        "
        SELECT application.id AS application_id,
        payment.payment_id,
        cc_payment_status.status_date,
        cc_payment_status.auth_total,
        cc_payment_status.settle_total,
        cc_payment_status.credit_total
        FROM payment
        INNER JOIN application ON payment.application_id = application.id
        INNER JOIN period_application ON application.id = period_application.application_id
        LEFT OUTER JOIN cc_payment_status ON payment.payment_id = cc_payment_status.payment_id
        WHERE payment.payment_type = 2
        AND application.submitted = 1
        ";
        
        if ($applicationId) {
            
            $systemStatusQuery .= "AND application.id = " . $applicationId;    
            
        } else {
            
            if ($periodId) {
                if (is_array($periodId)) {
                    $periodIds = '(' . implode(',' , $periodId) . ')';
                    $systemStatusQuery .= "AND period_application.period_id IN " . $periodIds;    
                } else {
                    $systemStatusQuery .= "AND period_application.period_id = " . $periodId;    
                }  
            }

            if ($this->unit && $this->unit == 'department') {
                
                $systemStatusQuery .=
                "
                AND application.id IN
                (
                  SELECT DISTINCT lu_application_programs.application_id
                  FROM lu_application_programs
                  INNER JOIN lu_programs_departments
                    ON lu_programs_departments.program_id = lu_application_programs.program_id
                  WHERE lu_programs_departments.department_id = " . $this->unitId . "
                )
                ";            
            }
            
            if ($this->unit && $this->unit == 'domain') {
                
                $systemStatusQuery .=
                "
                AND application.id IN
                (
                  SELECT DISTINCT lu_application_programs.application_id
                  FROM lu_application_programs
                  INNER JOIN lu_programs_departments
                    ON lu_programs_departments.program_id = lu_application_programs.program_id
                  INNER JOIN lu_domain_department
                    ON lu_programs_departments.department_id = lu_domain_department.department_id
                  WHERE lu_domain_department.domain_id = " . $this->unitId . "
                )
                ";            
            }
        }
        
        $systemStatusQuery .= "ORDER BY payment.payment_id";
        
        $systemStatusResultArray = $DB_Applyweb->handleSelectQuery($systemStatusQuery);
        
        $systemStatusRecords = array();
        foreach ($systemStatusResultArray as $systemStatusRecord) {
            $statusPaymentId = $systemStatusRecord['payment_id'];    
            $systemStatusRecords[$statusPaymentId] = $systemStatusRecord;
        }
        
        return $systemStatusRecords;
    }
    
    private function getCcAuthNotifications(&$DB_Applyweb, $periodId = NULL, $applicationId = NULL) {
        
        $notificationQuery = 
        "
        SELECT application.id AS application_id,
        payment.payment_id,
        cc_auth_notification.auth_notification_time
        FROM payment
        INNER JOIN application ON payment.application_id = application.id
        INNER JOIN period_application ON application.id = period_application.application_id
        INNER JOIN cc_auth_notification ON payment.payment_id = cc_auth_notification.payment_id
        WHERE payment.payment_type = 2
        AND application.submitted = 1
        ";
        
        if ($applicationId) {
            
            $notificationQuery .= "AND application.id = " . $applicationId;    
            
        } else {
            
            if ($periodId) {
                $periodIds = '(' . implode(',' , $periodId) . ')';
                $notificationQuery .= "AND period_application.period_id IN " . $periodIds;    
            }

            if ($this->unit && $this->unit == 'department') {
                
                $notificationQuery .=
                "
                AND application.id IN
                (
                  SELECT DISTINCT lu_application_programs.application_id
                  FROM lu_application_programs
                  INNER JOIN lu_programs_departments
                    ON lu_programs_departments.program_id = lu_application_programs.program_id
                  WHERE lu_programs_departments.department_id = " . $this->unitId . "
                )
                ";            
            }
            
            if ($this->unit && $this->unit == 'domain') {
                
                $notificationQuery .=
                "
                AND application.id IN
                (
                  SELECT DISTINCT lu_application_programs.application_id
                  FROM lu_application_programs
                  INNER JOIN lu_programs_departments
                    ON lu_programs_departments.program_id = lu_application_programs.program_id
                  INNER JOIN lu_domain_department
                    ON lu_programs_departments.department_id = lu_domain_department.department_id
                  WHERE lu_domain_department.domain_id = " . $this->unitId . "
                )
                ";            
            }
        }
        
        $notificationQuery .= "ORDER BY payment.payment_id, cc_auth_notification.auth_notification_time";
        
        $notificationResultArray = $DB_Applyweb->handleSelectQuery($notificationQuery);
        
        $notificationRecords = array();
        foreach ($notificationResultArray as $notificationRecord) {
            $notificationPaymentId = $notificationRecord['payment_id'];    
            $notificationRecords[$notificationPaymentId][] = $notificationRecord;
        }
        
        return $notificationRecords;
    }
    
}
?>