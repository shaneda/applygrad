<?php
require_once("Structures/DataGrid.php");
/*
* Create a datagrid with the specified columns according to
* round, decision, semiblind, etc. 
* 
* myColumns key:
* "[[prefix:]field to map]" => array(               // Field from the reviewListData db views
*       "label" => "[column label]",                // Label (column, checkbox, etc.) to use for the db field
*       "title" => "[column title]",                 // Optional full name for a short label 
*       "default" => TRUE|FALSE,                    // Should the column be displayed by default?
*       "required" => TRUE|FALSE,                   // Is display of the column required?
*       "rounds" => array(1|2|3[, 1|2|3[, 1|2|3]]), // Round(s) in which the column will be displayed (3 = decision round)
*       "semiblind" => TRUE|FALSE,                  // Should this column be displayed during semiblind review?
*       "view" => "committee|faculty|all",          // Who can view this column?
*       "formatter" => "[callback function]",       // Datagrid formatting callback
*       "formatterArg" => "[callback argument]"     // Datagrid formatting callback argument
*       )
*/
class ReviewListSds_DataGrid extends Structures_DataGrid
{

    protected $myColumns = array(

        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),
            
        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F",  "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad",  "title" => "Undergraduate Institution",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "title" => "Undergraduate GPA",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "title" => "GRE Verbal Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "title" => "GRE Quantitative Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),

        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL),        

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "possible_advisors" => array("label"=>"Poss Advisors", "title" => "Possible Advisors",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),

        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "round2_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),
            
        "round2_reviewer_point1" => array("label"=>"My Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewListCsd_DataGrid::printCommitteeRank()", "formatterArg" => NULL),

        "committee_reviewers" => array("label"=>"Revd By", "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),
            
        "language_screen_requested" => array("label"=>" Spec Reqs", "title" => "Special Requests", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecial()", "formatterArg" => NULL),
    
        "special_consideration_requested" => array("label"=>"Spec Cons", "title" => "Special Consideration",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecialConsideration()", "formatterArg" => NULL),

        "all_point1_committee_average" => array("label"=>"Avg Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => FALSE, "view" => "committee",
            "formatter" => NULL, "formatterArg" => NULL),

        "all_point1_committee_scores" => array("label"=>"Rank/Conf", "title" => "Rank/Confidence", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),

        "votes_for_round2" => array("label"=>"Round 2", "title" => "Votes for Round 2",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),        

        "admit_to" => array("label"=>"Admit To", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printAdmitTo()", "formatterArg" => NULL)
             
        );

    protected $round;               // int - The review round (3 = decision round)
    protected $decisionRound;       // bool - Is this for a decision round? 
    protected $setSemiblind;        // bool - Is the user toggling semiblind review?
    protected $semiblind;           // bool - Is this a semiblind view?
    protected $facultyView;         // bool - Is this a faculty view?
    protected $searchString;        // string - Indicates a search has been executed.
    protected $selectedColumns;     // array - fieldNames of specific selectable columns to display 
    
    public function __construct( $round = 1, $decisionRound = FALSE, $setSemiblind = FALSE, $semiblind = TRUE, 
                                    $facultyView = FALSE, $searchString = '', $selectedColumns = array(), 
                                    $limit = NULL, $page = NULL, $allCommentsOn = FALSE ) {
       
       parent::__construct($limit, $page);
       
       // Set the column parameters.
       $this->round = $round;
       $this->decisionRound = $decisionRound;
       if ($decisionRound) {
            $this->round = 3;   
       }
       $this->setSemiblind = $setSemiblind;
       $this->semiblind = $semiblind;
       $this->facultyView = $facultyView;
       $this->searchString = $searchString;
       $this->selectedColumns = $selectedColumns;

       // Set the table columns.
       $this->setColumns();
       
       // Set the table attributes.
       $this->setAttributes();
    }
    
    private function trimFieldNamePrefix($columnKey) {
        
        $columnKeyLength = strlen($columnKey);
        $prefixEnd = strpos($columnKey, ':');
        if ($prefixEnd) {
            $fieldName = substr($columnKey, $prefixEnd + 1);    
        } else {
            $fieldName = $columnKey;
        }
        return $fieldName;
    }
    
    private function setColumns() {
        
        
        foreach ($this->myColumns as $columnKey => $columnProperties ) {
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            
            // Don't add a column that doesn't match the round.
            if ( !in_array($this->round, $columnProperties['rounds']) ) {
                continue;    
            }
            
            // Don't add a faculty column to a non-faculty view and vice-versa.
            if ($this->facultyView) {
                if ( !($columnProperties['view'] == 'all' || $columnProperties['view'] == 'faculty') ) {
                    continue;
                }    
            } else {
                if ($columnProperties['view'] == 'faculty') {
                    continue;
                }                 
            }
            
            // Don't add non-semiblind columns to a semiblind view.
            if ( $this->semiblind && !$columnProperties['semiblind'] ) {
                continue;
            }

            // Don't add a non-required, non-default column that hasn't been selected.
            if ( !empty($this->selectedColumns) ) {
                if ( !$columnProperties['required'] && !in_array($fieldName, $this->selectedColumns) ) {  
                    if ( $this->setSemiblind && !$columnProperties['semiblind']) {
                        // Only skip the non-default non-semiblind columns when toggling off semiblind 
                        if (!$columnProperties['default']) {
                            continue;
                        }     
                    } else {
                        continue;
                    }
                }
            } else {
                if (!$columnProperties['default']) {
                    continue;
                }
            }            

            // Otherwise, add the column.
            $formatterArg = NULL;
            if ($columnProperties['formatterArg']) {
                if ( is_array($columnProperties['formatterArg']) ) {
                    $formatterArg = $columnProperties['formatterArg'];    
                } else {
                    $formatterArg = '$' . $columnProperties['formatterArg'];    
                }    
            }
            /*
            if ( isset($columnProperties['title']) &&  $columnProperties['title']) {
                $columnTitle = $columnProperties['title'];
            } else {
                $columnTitle = $columnProperties['label']; 
            }
            $columnLabel = '<div title="' . $columnTitle . '">' . $columnProperties['label'] . '</div>';
            */
            if ($columnProperties['label'] == 'row_number') {
                $columnLabel = '';
                $columnAttributes = array('class' => 'row_number');    
            } else {
                $columnLabel = $columnProperties['label'];
                $columnAttributes = array();    
            }
            
            $this->addColumn(
                new Structures_DataGrid_Column( 
                    $columnLabel, 
                    $fieldName, 
                    null, 
                    $columnAttributes, 
                    null, 
                    $columnProperties['formatter'],
                    $formatterArg
                )
            );  
            
        }
        
        // Always add a search column after a search.
        if ($this->searchString) {
            $this->addColumn(
                new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) ); 
        }
        
        return TRUE;
    }
    
    public function getRequiredColumns() {
    
        $requiredColumns = array();
        foreach ($this->myColumns as $columnKey => $columnProperties) {
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            if( $columnProperties['required'] && in_array($this->round, $columnProperties['rounds']) ) {
                $requiredColumns[$fieldName] = $columnProperties['label']; 
            }
        }
        return $requiredColumns; 
    }
    
    public function getSelectableColumns() {
        
        $selectableColumns = array();
        foreach ($this->myColumns as $columnKey => $columnProperties) {    
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            
            // Don't add a required column.
            if ($columnProperties['required']) {
                continue;     
            }
            
            // Don't add a column that doesn't match the round.
            if ( !in_array($this->round, $columnProperties['rounds']) ) {
                continue;    
            }
            
            // Don't add non-semiblind columns to a semiblind view.
            if ( $this->semiblind && !$columnProperties['semiblind'] ) {
                continue;
            }
            
            // Don't add a faculty column to a non-faculty view,
            // and don't add a committee column to a faculty view.
            if ($this->facultyView) {
                if ( !($columnProperties['view'] == 'all' || $columnProperties['view'] == 'faculty') ) {
                    continue;
                }    
            } else {
                if ($columnProperties['view'] == 'faculty') {
                    continue;
                }                 
            }
            
            // Otherwise, add the column.
            $selectableColumns[$fieldName] = $columnProperties['label'];
        }
        return $selectableColumns;
    }

    protected function setAttributes() {

        $this->renderer->setTableHeaderAttributes( array('color' => 'white', 'bgcolor' => '#99000') );
        $this->renderer->setTableEvenRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableOddRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableAttribute('width', '100%');
        $this->renderer->setTableAttribute('border', '0');
        $this->renderer->setTableAttribute('cellspacing', '1');
        $this->renderer->setTableAttribute('cellpadding', '5px');
        $this->renderer->setTableAttribute('id', 'applicantTable');
        $this->renderer->sortIconASC = '&uArr;';
        $this->renderer->sortIconDESC = '&dArr;';
        
        return TRUE;        
    }

    //########## Callback methods
    
    static function printCommitteeRank($params) 
    {
        extract($params);
        
        $score = NULL;
        if (isset($record['round2_reviewer_point1']))
        {
            $score = $record['round2_reviewer_point1'];
        }
        elseif (isset($record['round1_reviewer_point1']))
        {
            $score = $record['round1_reviewer_point1'];    
        }
        
        return $score;
    }
}
?>