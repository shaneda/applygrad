<?php
require("Structures/DataGrid.php");

class AdmissionPeriod_DataGrid extends Structures_DataGrid
{

    function __construct( $admissionPeriodTableColumns, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);
       
       // Add table columns.
       foreach($admissionPeriodTableColumns as $columnKey => $columnArray) {
           
           $field = $columnKey;
           $label = $columnArray['qf_label'];
           $orderBy = $field;
           
           if ($columnKey == 'admissionPeriodId') {
               $label = '';
               $orderBy = NULL;
               $callback = 'AdmissionPeriod_DataGrid::printEditLink()';
           }
           elseif ($columnArray['type'] == 'date') {
               $callback = 'AdmissionPeriod_DataGrid::printDate()';
           }
           elseif ($columnKey == 'reviewClosed') {
               $callback = 'AdmissionPeriod_DataGrid::printReviewClosed()';
           } 
           else {
               $callback = NULL;
           }
           
           $this->addColumn(
                new Structures_DataGrid_Column(
                    $label, $field, $orderBy, 
                    array('class' => $field), null, $callback
                    ) 
                ); 
       }
       
       // Add delete column.
       $this->addColumn(
            new Structures_DataGrid_Column(
                '', 'admissionPeriodId', null, 
                array(), null, 
                'AdmissionPeriod_DataGrid::printDeleteButton()'
                ) 
            ); 

            
        $this->renderer->setTableHeaderAttributes( 
            array('style' => 'color: #FFFFFF;', 'bgcolor' => '#990000', 'class'=>'tblHead') );
        $this->renderer->setTableEvenRowAttributes( 
            array('valign' => 'top', 'bgcolor' => '#DDDDDD', 'class' => 'record tblItemAlt',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#DDDDDD\';'));
        $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE',  'class' => 'record tblItem',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'));
        $this->renderer->setTableAttribute('border', '0');
        //$this->renderer->setTableAttribute('style', 'border: 1px solid #D3DCE3;');
        $this->renderer->setTableAttribute('cellspacing', '1');
        $this->renderer->setTableAttribute('cellpadding', '5px');
        $this->renderer->setTableAttribute('id', 'admissionPeriodTable');
        $this->renderer->sortIconASC = '&uArr;';
        $this->renderer->sortIconDESC = '&dArr;';
       
   }

   
    static function printEditLink($params) {
        
        extract($params);
        $unit = $_REQUEST['unit'];
        $unitIdFieldName = $unit . 'Id';
        $link = '<a href="' . htmlentities($_SERVER['PHP_SELF']);
        $link .= '?unit=' . $unit . '&unitId=' . $record[$unitIdFieldName];
        //$link .= '?departmentId=' . $record['departmentId'];
        $link .= '&action=update';
        $link .= '&admissionPeriodId=' . $record['admissionPeriodId'];
        $link .= '">Edit</a>';
        return $link;        
        
    }

    
    static function printDate($params) {

        extract($params);
        if ($record[$fieldName] != NULL) {
            $date = date( "m/d/Y", strtotime($record[$fieldName]) );    
        } else {
            $date = $record[$fieldName];
        }
        return $date;    
        
    }

    static function printReviewClosed($params) {

        extract($params);
        if ($record[$fieldName] == 0) {
            $reviewClosed = 'no';    
        } else {
            $reviewClosed = 'yes';
        }
        return $reviewClosed;    
        
    }

   
    static function printDeleteButton($params) {
        
        extract($params);
        $unit = $_REQUEST['unit'];
        $unitIdFieldName = $unit . 'Id';
        $href = htmlentities($_SERVER['PHP_SELF']);
        $href .= '?action=delete';
        $href .= '?unit=' . $unit . '&unitId=' . $record[$unitIdFieldName];
        //$href .= '&departmentId=' . $record['departmentId'];
        $href .= '&admissionPeriodId=' . $record['admissionPeriodId'];
        $button = '<button title="delete" type="button" class="delete"';
        //$button .= 'OnClick="location.href=\'' . $href . '\';"';
        $button .= '>x</button>';
        return $button;        
        
    }

}
?>
