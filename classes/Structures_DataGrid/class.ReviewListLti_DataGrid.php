<?php

class ReviewListLti_DataGrid extends ReviewList_DataGrid
{

    
    protected $myColumns = array(
               
        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),

        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListLti_DataGrid::printAoi()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),

        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL),          

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "possible_advisors" => array("label"=>"Poss Advisors", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "faculty",
            "formatter" => NULL, "formatterArg" => NULL),

        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),
        
        "round2_reviewer_touched" => array("label" => "I Revd", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),
        
         "committee_reviewers" => array("label"=>"Revd By", "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),
            
        "language_screen_requested" => array("label"=> "Spec Reqs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecial()", "formatterArg" => NULL),
        
        "special_consideration_requested" => array("label"=>"Spec Cons", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecialConsideration()", "formatterArg" => NULL),
        
        "ct_recommendations_requested" => array("label"=>"Recs Rcd", "title" => "Recommendations Received", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printRecommendationsReceived()", "formatterArg" => NULL),
            
        "mergedFileDate" => array("label"=>"Merged File", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMergedDate()", "formatterArg" => NULL),
           
        "mergedFilePath" => array("label"=>"Add to Zip", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printZipAdd()", "formatterArg" => NULL)

        /*
        "promotion_status_round" => array("label"=>"Round 2", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListLti_DataGrid::printRound2()", "formatterArg" => NULL)
        )
        */
        )
        ;  

    
    public function __construct($round = 1, $decisionRound = FALSE, $setSemiblind = FALSE, $semiblind = TRUE, 
                                $facultyView = FALSE, $searchString = '', $selectedColumns = array(), 
                                $limit = NULL, $page = NULL, $allCommentsOn = FALSE) {
    
        global $department_name;
        
        /* before Andy Pavlo fix
        if ($department_name == "LTI-IIS") {
           $this->myColumns["promotion_status_round"] = array("label"=>"Needs Review", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListLti_DataGrid::printNeedsReview()", "formatterArg" => NULL); 
        } else {
             $this->myColumns["promotion_status_round"] = array("label"=>"Needs Review", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListLti_DataGrid::printNeedsReview()", "formatterArg" => NULL);
        }
        */
        if ($department_name == "LTI-IIS") {
           $this->myColumns["promotion_status.round"] = array("label"=>"Needs Review", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListLti_DataGrid::printNeedsReview()", "formatterArg" => NULL); 
        } else {
             $this->myColumns["promotion_status.round"] = array("label"=>"Needs Review", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListLti_DataGrid::printNeedsReview()", "formatterArg" => NULL);
        }
       
        if ($allCommentsOn) {
            
            $this->addAllCommentsColumns();
            
        } else {
            
            $this->addReviewerColumns();
            
        }
        
        $this->myColumns["admit_to"] = array("label"=>"Admit To", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printAdmitTo()", "formatterArg" => NULL);
        
        parent::__construct($round, $decisionRound, $setSemiblind, $semiblind, 
                            $facultyView, $searchString, $selectedColumns, 
                            $limit, $page);                            
                                
    }
    

    private function addReviewerColumns() {

        $this->myColumns["round1_reviewer_point1"] = array("label"=>"PhD Score", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1,2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL);
        
        $this->myColumns["round1_reviewer_point2"] = array("label"=>"MS Score", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1,2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL);
        
        /*
        $this->myColumns["round1_reviewer_comments"] = array("label"=>"Comments", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListLti_DataGrid::printReviewerComments()", "formatterArg" => NULL);
        
        $this->myColumns["round2_reviewer_comments"] = array("label"=>"Comments", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListLti_DataGrid::printReviewerComments()", "formatterArg" => NULL);
        */
    }
    
    
    private function addAllCommentsColumns() {

        $this->myColumns["round1_point1_average"] = array("label"=>"Avg PhD Score", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL);

        $this->myColumns["round1_point2_average"] = array("label"=>"Avg MS Score", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL);

        $this->myColumns["round1_point1_scores"] = array("label"=>"PhD Scores", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1,2), "semiblind" => FALSE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL);
        
        $this->myColumns["round1_point2_scores"] = array("label"=>"MS Scores", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1,2), "semiblind" => FALSE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL);
        
        /*
        $this->myColumns["round1_comments"] = array("label"=>"Comments", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL);
        
        $this->myColumns["round2_comments"] = array("label"=>"Comments", "default" => TRUE, 
            "required" => FALSE, "rounds" => array(2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL);
        */
        
    }
    
    static function printAoi($params) {
        
        $ltiSeminars = array(
            1 => "<span title='INFORMATION RETRIEVAL, TEXT MINING'>IR</span>",
            2 => "<span title='MACHINE TRANSLATION'>MT</span>",
            3 => "<span title='SPEECH RECOGNITION, SPEECH SYNTHESIS'>SR</span>",
            4 => "<span title='NATURAL LANGUAGE PROCESSING, COMPUTATIONAL LINGUISTICS'>CL</span>",
            5 => "<span title='BIOLOGICAL LANGUAGE MODELING'>BIO</span>",
            6 => "<span title='INTELLIGENT TUTORS'>IT</span>",
            7 => "<span title='MACHINE LEARNING'>ML</span>"
        );
        
        extract($params);
        
        // Applicant's AOI selections
        $aoi = str_replace("|", "<br />" , $record[$fieldName]);
        
        // Reviewer checkbox selections
        $reviewerAoi = $record['reviewer_aoi'];
        if ($reviewerAoi != '') {
            if ($aoi != '') {
                $aoi .= '<br/>';
            }
            $reviewerAoiArray = explode('|', $reviewerAoi);
            for ($i = 0; $i < count($reviewerAoiArray); $i++) {
                $code = $ltiSeminars[$reviewerAoiArray[$i]];
                $reviewerAoiArray[$i] = $code;    
            }
            $aoi .= implode(', ', $reviewerAoiArray);    
        }
        
        // Reviewer text
        $reviewerOtherAoi = $record['reviewer_other_aoi'];
        if ($reviewerOtherAoi != '') {
            if ($aoi != '') {
                $aoi .= '<br/>';
            }
            $aoi .= $reviewerOtherAoi;    
        }
        
        return $aoi;        
    }

    static function printReviewerComments($params) {
        global $round;
        extract($params);
        $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round1_reviewer_comments']) );
        if ($round == 2 || $round == 3) {
            $comments .= "<br /><br />";
            $comments .= htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_reviewer_comments']) );
        }
        return $comments;
    }
    
    static function printRound2($params) {
        extract($params);
        /* before Andy Pavlo fix
        if ($record['promotion_status_round'] > 1) {
            $round2 = '<div class="yes">Y</div>';
        } else {
            $round2 = '<div class="no">N</div>';    
        }
        */
        if ($record['promotion_status.round'] > 1) {
            $round2 = '<div class="yes">Y</div>';
        } else {
            $round2 = '<div class="no">N</div>';    
        }
        return $round2;
    }
    
    static function printNeedsReview($params) 
    {
        extract($params);

        $phdNeedsReview = ReviewListLti_DataGrid::needsReview($record['round1_point1_scores']);
        $msNeedsReview = ReviewListLti_DataGrid::needsReview($record['round1_point2_scores']);

        $needsReview = 'Yes';
        
        if ($phdNeedsReview == 'Round 2' || $msNeedsReview == 'Round 2')
        {
            $needsReview = 'Round 2';    
        }
        
        if ($phdNeedsReview == 'No' && $msNeedsReview == 'No')
        {
            $needsReview = 'No';    
        }
        
        return $needsReview;
    }
    
    private static function needsReview($scoreString)
    {
        $scores = array();
        foreach (explode('|', $scoreString) as $reviewerScore)
        {
            $reviewerScoreArray = explode(':', $reviewerScore);
            if (isset($reviewerScoreArray[1])) {
                $scores[] = floatval(trim($reviewerScoreArray[1]));
            } 
        }
        
        $needsReview = 'Yes';
        
        if (count($scores) >= 2)
        {
            if (max($scores) <= 1.50)
            {
                $needsReview = 'Round 2';    
            } 
            
            if (min($scores) >= 2.50)
            {
                $needsReview = 'No';    
            }   
        }
        
        return $needsReview;        
    }
}
?>