<?php

class NormalizationListNormalizations_DataGrid extends ReviewList_DataGrid
{

    protected $myColumns = array(
        
        "name" => array("label" => "Reviewer", "title" => "Reviewer",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListNormalizations_DataGrid::printName()", "formatterArg" => NULL),
        
        "score_sum" => array("label" => "Sum Scores", "title" => "Sum Scores",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "score_count" => array("label"=> "# Scores", "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "score_average" => array("label"=>"Avg Score", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printRounded()", 
            "formatterArg" => array('precision' => 3)),
        
        "weight_factor" => array("label" => "Weight Factor", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printRounded()", 
            "formatterArg" => array('precision' => 3))
             
        );

    protected function setAttributes() {

        $this->renderer->setTableHeaderAttributes( array('color' => 'white', 'bgcolor' => '#99000') );
        $this->renderer->setTableEvenRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableOddRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        //$this->renderer->setTableAttribute('width', '100%');
        $this->renderer->setTableAttribute('border', '0');
        $this->renderer->setTableAttribute('cellspacing', '1');
        $this->renderer->setTableAttribute('cellpadding', '5px');
        $this->renderer->setTableAttribute('id', 'normalizationsTable');
        $this->renderer->sortIconASC = '&uArr;';
        $this->renderer->sortIconDESC = '&dArr;';
        
        return TRUE;        
    }
    
    public static function printName($params) {       
        extract($params);
        $name = $record[$fieldName];
        if ($name == 'All Reviewers') {
            $name = str_replace('All', '&#8746;', $name);
        }
        return $name;
    }
        
}
?>