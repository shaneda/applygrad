<?php
require_once("Structures/DataGrid.php");

/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for administerApplications.php 
*/

class AdminListReject_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'AdminList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart())); 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'user_id', 'user_id', 
                array('width' => '9%', 'align' => 'left', 'valign' => 'top'), null,
                'AdminList_Datagrid::printPrintLink()')); 
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '15%', 'align' => 'left'), null, 'AdminList_Datagrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Submitted Date', 'submitted_date', 'submitted_date', 
                array('width' => '10%', 'align' => 'left'), null));
       $this->addColumn(
            new Structures_DataGrid_Column('Period', 'period_id', 'period_id', 
                array('width' => '10%', 'align' => 'left'), null, 'AdminListSu_Datagrid::printPeriodLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Programs', 'programs', 'programs', 
                array('width' => '10%', 'align' => 'left'), null, 'AdminList_Datagrid::printPrograms()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', '', '', 
                array('width' => '10%', 'align' => 'center'), null, 'AdminListSu_Datagrid::printCopyLink()'));
       
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    /*
    * ########## Callback methods #############
    */

    static function printCopyLink($params) {
        
        extract($params);
        $application_id = $record['application_id'];
        $link_id = 'copy_' . $application_id;
        $link = '<a class="menu" href="javascript:;" title="Move / Copy" id="' . $link_id . '">';
        $link .= 'Move / Copy</a>';
        
        return $link;
    }

    static function printPeriodLink($params) {
        
        extract($params);
        
        if (!$record['period_id']) {
            return '';
        }
        
        $href = 'administerApplications.php?unit=domain&unitId=' . $record['application_domain'];
        $href .= '&period=' . $record['period_id'] . '&applicationId=' . $record['application_id'];
        $link = '<a class="menu login" href="' . $href . '">';
        $link .= $record['unit_name_short'] . ', ' . $record['umbrella_name'] . '</a>';
        
        return $link;
    }
    
}
?>