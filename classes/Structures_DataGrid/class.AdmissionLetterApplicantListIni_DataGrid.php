<?php
require_once("Structures/DataGrid.php");

class AdmissionLetterApplicantListIni_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct($dataArray, $limit = NULL, $page = NULL, $allUsersSelected = NULL) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'AdminList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart()));  
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '20%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Admitted Program(s)', 'admitted_programs', 'admitted_programs', 
                array('width' => '20%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printPrograms()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Scholarship Amt', 'scholarship_amt', 'scholarship_amt', 
                array('width' => '15%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printScholarshipAmt()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Decision Date', 'timestamp', 'timestamp', 
                array('width' => '15%', 'align' => 'left'), null, 'AdmissionLetterApplicantListIni_DataGrid::printTimestamp()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Current Letter', 'letter', 'letter', 
                array('width' => '20%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printLetter()'));
       $this->addColumn(
            new Structures_DataGrid_Column("", null, null, 
                array('width' => '2%', 'align' => 'left', 'valign' => 'top'), null,
                'AdmissionLetterApplicantList_DataGrid::printSelectApplicationLink()'));
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }
    
    /*
    * ########## Callback methods #############
    */
    static function printTimestamp($params) {
        extract($params);
        $date = $record[$fieldName];
        return $date;
    }
}
?>