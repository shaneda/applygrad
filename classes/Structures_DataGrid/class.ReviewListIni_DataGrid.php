<?php

class ReviewListIni_DataGrid extends ReviewList_DataGrid
{
    protected $myColumns = array(
        
        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),

        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F", "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "undergrad_institute_name" => array("label"=>"Undergrad", "title" => "Undergraduate Institution", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "major1" => array("label" => "Major", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "title" => "Undergraduate GPA",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "title" => "GRE Verbal Score", 
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "title" => "GRE Quantitative Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total1" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),
            
        "toefl_total2" => array("label"=>"IBT Tot", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_total')),

        "toefl_section1" => array("label"=>"IBT Spk", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_section1')),
            
        "toefl_section2" => array("label"=>"IBT Ln", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_section2')),

        "toefl_section3" => array("label"=>"IBT Rd", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_section3')),
            
        "toefl_essay" => array("label"=>"IBT Wrt", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_essay')),
        
        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL), 

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "possible_advisors" => array("label"=>"Poss Advisors", "title" => "Possible Advisors",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "faculty",
            "formatter" => NULL, "formatterArg" => NULL),
            
        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),
        
        "round2_reviewer_touched" => array("label" => "I Revd",  "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "committee_reviewers" => array("label"=>"Revd By",  "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),

        "moeller_score" => array("label"=>"Predictive",  "title" => "Predictive",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListIni_DataGrid::printPredictive()", "formatterArg" => NULL),
            
        "all_overall_ratings" => array("label"=>"Recommendation",  "title" => "Recommendation",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),    
        
        "all_overall_average" => array("label"=>"Avg Recommendation",  "title" => "Average Recommendation",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),

        "coordinator_comments" => array("label"=>"Comments",  "title" => "Coordinator Comments",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => "ReviewListIni_DataGrid::printCoordinatorComments()", "formatterArg" => NULL),

        "admit_to_decision3" => array("label"=>"Decision", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "admit_to" => array("label"=>"Admit To", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "scholarship_amt" => array("label"=>"Scholarship Amt", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewListIni_DataGrid::printScholarshipAmounts()", "formatterArg" => NULL)
             
        );
        
    public function __construct($round = 1, $decisionRound = FALSE, $setSemiblind = FALSE, $semiblind = TRUE, 
                                $facultyView = FALSE, $searchString = '', $selectedColumns = array(), 
                                $limit = NULL, $page = NULL, $allCommentsOn = FALSE) 
    {
        $this->myColumns["admit_to"] = array("label"=>"Admit To", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewListIni_DataGrid::printAdmitTo()", "formatterArg" => NULL);
        
        parent::__construct($round, $decisionRound, $setSemiblind, $semiblind, 
                            $facultyView, $searchString, $selectedColumns, 
                            $limit, $page);                                                            
    }

    static function printAdmitTo($params) {
        
        extract($params);
        
        if ($record['admit_to_decision2']) {
            $programString = $record['admit_to_decision2'];
        } else {
            $programString = $record['admit_to'];    
        }
        
        $programsArray = explode('|', $programString);
        for ($i = 0; $i < count($programsArray); $i++) {
            $programsArray[$i] = str_replace('(wait)', '<i>(wait)</i>', $programsArray[$i]);        
        }
        $programs = implode('<br>', $programsArray);
        
        return $programs;
    }
    
    static function printScholarshipAmounts($params) {
        extract($params);
        $amountArray = explode("|", $record[$fieldName]);
        $amountArray = 
            array_map(function($number){return number_format(intval($number), 2);}, $amountArray);
        $amounts = implode("<br>", $amountArray);
        return $amounts;
    }
    
    static function printPredictive($params) {
        extract($params);
        $predictive = 'Score:&nbsp;' . $record['moeller_score'] . '<br>';
        $predictive .= 'Rank:&nbsp;' . $record['moeller_rank'] . '<br>';
        $predictive .= 'n-value (school):&nbsp;' . $record['moeller_n_count'] . '<br>';
        $predictive .= 'Group:&nbsp;' . $record['moeller_n_group'] . '<br>';
        return $predictive;
    }
    
    static function printCoordinatorComments($params) {
        extract($params);
        
        $comments = '';
        
        if ($record['coordinator_comments'])
        {
            $comments .= $record['coordinator_comments'] . '<br>';    
        }
        
        if ($record['scholarship_comments'])
        {
            $comments .= 'Scholarship: ' . $record['scholarship_comments'];    
        }

        return $comments;
    }
}

?>