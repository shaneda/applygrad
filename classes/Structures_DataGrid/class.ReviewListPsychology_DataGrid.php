<?php
class ReviewListPsychology_DataGrid extends ReviewList_DataGrid
{
    protected $myColumns = array(

        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),
            
        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F",  "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad",  "title" => "Undergraduate Institution",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
            
        "pier" => array("label"=>"PIER",  "title" => "PIER",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewListPsychology_DataGrid::printPier()", "formatterArg" => NULL),
            
        "cnbc" => array("label"=>"CNBC",  "title" => "CNBC",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewListPsychology_DataGrid::printCnbc()", "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "title" => "Undergraduate GPA",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "title" => "GRE Verbal Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "title" => "GRE Quantitative Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),

        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL),        

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "possible_advisors" => array("label"=>"Poss Advisors", "title" => "Possible Advisors",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),

        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "round2_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "committee_reviewers" => array("label"=>"Revd By", "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),
            
        "language_screen_requested" => array("label"=>" Spec Reqs", "title" => "Special Requests", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecial()", "formatterArg" => NULL),
    
        "special_consideration_requested" => array("label"=>"Spec Cons", "title" => "Special Consideration",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecialConsideration()", "formatterArg" => NULL),

        "all_point1_committee_average" => array("label"=>"Avg Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => FALSE, "view" => "committee",
            "formatter" => NULL, "formatterArg" => NULL),

        "all_point1_committee_scores" => array("label"=>"Rank/Conf", "title" => "Rank/Confidence", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),

        "votes_for_round2" => array("label"=>"Round 2", "title" => "Votes for Round 2",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListPsychology_DataGrid::printRound2()", "formatterArg" => NULL),        

        /*
        "round1_comments" => array("label"=>"Comments", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
        
        "round2_comments" => array("label"=>"Comments", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
            
        "round2_pertinent_info" => array("label"=>"Comments", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "faculty",
            "formatter" => "ReviewList_DataGrid::printPertinentInfo()", "formatterArg" => NULL),
        */
        
        "admit_to" => array("label"=>"Admit To", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printAdmitTo()", "formatterArg" => NULL)
             
        );

    static function printRound2($params) {
        extract($params);
        $scores = '';
        $scoresArray = explode('|', $record[$fieldName]);
        for ($i = 0; $i < count($scoresArray); $i++) {
            $scoreArray = explode(':', $scoresArray[$i]);
            $name = $scoreArray[0];
            $nameArray = explode(' ', $name);
            if ($nameArray[0])
            {
                $scores .= substr($nameArray[0], 0, 1) . ' ';
            }
            $scores .= end($nameArray);
            if ( isset($scoreArray[1]) ) 
            {
                if ($scoreArray[1] == -1)
                {
                    $score = 'P';
                }    
                elseif ($scoreArray[1] == 0)
                {
                    $score = 'N';
                } 
                elseif ($scoreArray[1] == 1)
                {
                    $score = 'Y';
                }
                
                $scores .=  ': ' . $score . '<br/>';  
            }
        }
        return $scores;
    }
    
    static function printPier($params) {
        
        extract($params);
        if ($record['pier_requested']) {
            $pier_req1 = $record['pier_requested'];
            if ($pier_req1 == 1) {
               $pier_req = "Yes";}
               else {
                   $pier_req = "No";
               } 
        }  else {
              $pier_req = "No";
        }
        
        return $pier_req;
    }
    
    static function printCnbc($params) {
        
        extract($params);
        if ($record['cnbc_requested']) {
            $cnbc_req1 = $record['cnbc_requested'];
            if ($cnbc_req1 == 1) {
               $cnbc_req = "Yes";}
               else {
                   $cnbc_req = "No";
               } 
        }  else {
              $cnbc_req = "No";
        }
        
        return $cnbc_req;
    }
}
?>