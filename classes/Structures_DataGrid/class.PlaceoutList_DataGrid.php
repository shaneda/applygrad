<?php
require_once("Structures/DataGrid.php");
include "../inc/mhci_prereqsSummaryData.class.php";

 function getStatusMessage($studentId, $summaryDataStatusArray, $prereqType) {

    $statusMessage = "Not yet begun";    
    if ( isset($summaryDataStatusArray[$studentId][$prereqType]) ) {
        $studentStatus = $summaryDataStatusArray[$studentId][$prereqType]['student_status'];
        $reviewerStatus = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_status'];
        $reviewerExplanation = $summaryDataStatusArray[$studentId][$prereqType]['reviewer_explanation'];
        if ($reviewerStatus == NULL) {
            $statusMessage = $studentStatus;
        } else {  
            $statusMessage = $reviewerStatus . " " . $reviewerExplanation;
        }        
            
    }
    return $statusMessage;
}

/*
* Get the record for the last comment
*/

function getLastConverstionComment($studentId, $summaryDataStatusArray, $prereqType) {

    $lastComment = "";    
    if ( isset($summaryDataStatusArray[$studentId][$prereqType]) ) {
        $commentSql = "SELECT * FROM mhci_prereqsConversationComments WHERE prereq_id = ";
        $commentSql .= $summaryDataStatusArray[$studentId][$prereqType]['id'] . " ORDER BY id DESC LIMIT 1";
        $result = mysql_query($commentSql);
        $numRows = mysql_num_rows($result);
        if ($numRows > 0) {
             $row = mysql_fetch_array($result);
             if ($studentId == $row['lu_users_usertypes_id']) {
                 $lastComment =  "Awaiting Faculty Reply"; 
             } else {
                   $lastComment =  "Awaiting Student Reply";
             }
        }
    }
    return $lastComment;
}

/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for administerApplications.php 
*/

class PlaceoutList_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       $myRoles = $this->getRoles($_SESSION['A_userid']);
       
       // Set the table columns. 
       /*
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'PlaceoutList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart())); 
       */
/*       $this->addColumn(
            new Structures_DataGrid_Column('', 'lu_users_usertypes_id', 'lu_users_usertypes_id', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null, 
                'PlaceoutList_Datagrid::printPrintLink()')); 
                */
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'lastname', 'lastname', 
                array('width' => '15%', 'align' => 'left'), null, 'PlaceoutList_Datagrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Email', 'email', 'email', 
                array('width' => '10%', 'align' => 'left'), null, 'PlaceoutList_Datagrid::printEmail()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Program', 'program', 'program', 
                array('width' => '15%', 'align' => 'left'), null));
       if (in_array('design', $myRoles)) {
           $this->addColumn(
                new Structures_DataGrid_Column('Design', 'design_reviewer_status', 'design_reviewer_status', 
                    array('width' => '20%', 'align' => 'left'), null, 'PlaceoutList_Datagrid::printDesignStatus()'));
       }
       if (in_array('programming', $myRoles)) {
           $this->addColumn(
                new Structures_DataGrid_Column('Programming', 'programming_reviewer_status', 'programming_reviewer_status', 
                    array('width' => '20%', 'align' => 'left'), null, 'PlaceoutList_Datagrid::printProgrammingStatus()'));
       }
       /* remove statistics
       if (in_array('statistics', $myRoles)) {
       $this->addColumn(
            new Structures_DataGrid_Column('Statistics', 'statistics_reviewer_status', 'statistics_reviewer_status', 
                array('width' => '20%', 'align' => 'left'), null, 'PlaceoutList_Datagrid::printStatisticsStatus()'));
       }
       */

       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                ) 
            );
       $this->renderer->setTableOddRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                 )
            );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '1px');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    //########## Callback methods

    static function printNumber($params, $recordNumberStart) {
        
        extract($params);
        $number = $params['currRow'] + $recordNumberStart . ".";
        return $number;
    }    

    function printPrintLink($params) {
        
        extract($params);
        $applicationId = $record['application_id']; 
        $luUsersUsertypesId = $record['lu_users_usertypes_id'];
        
        $links = '<ul style="list-style: circle; margin-left: 6px; padding-left: 6px; margin-top: 0px;"><li>';
        $links .= '<a class="menu print" href="javascript:;" id="print_'. $luUsersUsertypesId . '_' . $applicationId . '" ';
        $links .= 'title="Print Application ' . $applicationId . '">';
        $links .= 'View&nbsp;/&nbsp;Print&nbsp;Application</a></li>';
        $links .= '<li><a class="menu login" target="_blank" href="../placeout/index.php?uid=';
        $links .= $record['guid'] . '&a=1&domain=' . $record['application_domain'];
        $links  .= '&applicationId=' . $applicationId . '">';
        $links .= 'Login As Applicant</a></li></ul>';

        return $links;
    }


    static function printFullName($params)
    {   
        extract($params);
        $name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">';
        $name .= $record['lastname'] . ', ' . $record['firstname'] . '</span>';    

        return $name;
    }
    
    static function printEmail($params)
    {   
        extract($params);
        $email = '<span id="applicantEmail_' . $record['application_id'] . '" style="font-weight: normal;">';
        $email .= $record['email']  . '</span>';    

        return $email;
    }
    
    static function printDesignStatus($params) {
        
        extract($params);
        $luUsersUsertypesId = $record['lu_users_usertypes_id'];
        $applicationId = $record['application_id'];
        $programId = $record['program_id'];;
        $summaryData = new MHCI_PrereqsSummaryData();
        $statusArray = $summaryData->getStatus($luUsersUsertypesId);
        $status = getStatusMessage($luUsersUsertypesId, $statusArray, "design");
        $explanantion = $record['design_reviewer_explanation'];
        $status = self::printStatus($applicationId, $programId, $status, $explanantion);
        $link = '<a class="print" target="_self" href="../placeout/mhci_prereqs_design.php?role=reviewer&studentLuId=';
        $link .= $luUsersUsertypesId . '&appid=' . $applicationId . '&pid=' . $programId . '">' . $status  . '</a>';
        $lastComment = getLastConverstionComment($luUsersUsertypesId, $statusArray, "design");
        if ($lastComment != "") {
            $link .= '<br />' . $lastComment;
        } 
        
        return $link;
    }

    static function printProgrammingStatus($params) {
        
        extract($params);
        $luUsersUsertypesId = $record['lu_users_usertypes_id'];
        $applicationId = $record['application_id'];
        $programId = $record['program_id'];
        $summaryData = new MHCI_PrereqsSummaryData();
        $statusArray = $summaryData->getStatus($luUsersUsertypesId);
        $status = getStatusMessage($luUsersUsertypesId, $statusArray, "programming");
        $explanantion = $record['programming_reviewer_explanation'];
        
        $status = self::printStatus($applicationId, $programId, $status, $explanantion);
        $link = '<a class="print" target="_self" href="../placeout/mhci_prereqs_programming.php?role=reviewer&studentLuId=';
        $link .= $luUsersUsertypesId . '&appid=' . $applicationId . '&pid=' . $programId . '">' . $status . '</a>';
        $lastComment = getLastConverstionComment($luUsersUsertypesId, $statusArray, "programming");
        if ($lastComment != "") {
            $link .= '<br />' . $lastComment;
        }
      //  DebugBreak();
        return $link;
    }

    static function printStatisticsStatus($params) {
        
        extract($params);
        $luUsersUsertypesId = $record['lu_users_usertypes_id'];
        $applicationId = $record['application_id'];
        $programId = $record['program_id'];
        $summaryData = new MHCI_PrereqsSummaryData();
        $statusArray = $summaryData->getStatus($luUsersUsertypesId);
        $status = getStatusMessage($luUsersUsertypesId, $statusArray, "statistics");
        $explanantion = $record['statistics_reviewer_explanation'];
        
        $status = self::printStatus($applicationId, $programId, $status, $explanantion);
        $link = '<a class="print" target="_self" href="../placeout/mhci_prereqs_statistics.php?role=reviewer&studentLuId=';
        $link .= $luUsersUsertypesId . '&appid=' . $applicationId . '&pid=' . $programId . '">' . $status . '</a>';
        $lastComment = getLastConverstionComment($luUsersUsertypesId, $statusArray, "statistics");
        if ($lastComment != "") {
            $link .= '<br />' . $lastComment;
        }
        
        return $link;        
    }
    
    static function printStatus($applicationId, $programId, $status, $explanantion) {

        $statusClass = 'confirm';
        if ( $status == 'Fulfilled:' || $status == 'Fulfilled: undergraduate degree') {
            $statusClass = 'confirmComplete';
        }
        $statusText = '<span id="designStatusMessage_' . $applicationId . '_' . $programId . '"';
        $statusText .= ' class="' . $statusClass . '">';
        if ($status) {
            $statusText .= $status . ' ' . $explanantion;    
        } else {
            $statusText .= 'Not started';    
        } 
        $statusText .= '</span>';

        return $statusText;    
    }
    
    private function getRoles($reviewerLuuId)  {
        $sql = "SELECT prereq_type FROM mhci_prereqsReviewers WHERE reviewer_luu_id = " .  $reviewerLuuId;
        $result = mysql_query($sql);
        $roleArray = array();
        while ($row = mysql_fetch_array($result)) {
            array_push($roleArray, $row['prereq_type']);
        }
        return $roleArray;
    }
    
    static function printFileUploadStatus($params)
    {   
        extract($params);
        $name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">';
        $name .= $record['lastname'] . ', ' . $record['firstname'] . '</span>';    

        return $name;
    }

}
?>