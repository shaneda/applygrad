<?php
require_once("Structures/DataGrid.php");

/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for administerApplications.php 
*/

class RegistrationList_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'RegistrationList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart())); 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'user_id', 'user_id', 
                array('width' => '9%', 'align' => 'left', 'valign' => 'top'), null,
                'RegistrationList_Datagrid::printPrintLink()')); 
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '15%', 'align' => 'left'), null, 'RegistrationList_Datagrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Program', 'accepted_program_name', 'accepted_program_name', 
                array('width' => '14%', 'align' => 'left'), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'paid', 'paid', 
                array('width' => '14%', 'align' => 'center'), null, 'RegistrationList_Datagrid::printPaymentLink()'));
  

       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    /*
    * ########## Callback methods #############
    */

    static function printNumber($params, $recordNumberStart) {
        extract($params);
        $number = $params['currRow'] + $recordNumberStart . ".";
        return $number;
    }    

    static function printPrintLink($params) {
        extract($params);
        $userId = $record['user_id'];
        $applicationId = $record['application_id'];
        $links = '<ul style="list-style: circle; margin-left: 6px; padding-left: 6px; margin-top: 0px;"><li>';
        $links .= '<a class="menu print" href="javascript:;" id="print_'. $userId . '_' . $applicationId . '" ';
        $links .= 'title="Print Application ' . $applicationId . '">';
        $links .= 'View&nbsp;/&nbsp;Print</a></li>';
        $links .= '</ul>';
        return $links;
    }

    static function printFullName($params) {   
        extract($params);

        $name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">' . $record['name'] . '</span>';    

        $name .= '<br />';
        $name .= '<a title="E-mail Applicant" href="mailto:' . $record['email'] . '">' . $record['email'] . '</a>'; 

        return $name;
    }

    static function printPaymentLink($params) 
    {
        extract($params);
        $applicationId = $record['application_id'];
        $programId = $record['department_id'];
        $waived = $record['waived'];
        $paid = $record['paid'];
        $linkId = 'payment_' . $applicationId . '_' . $programId; 
        $link = '<a class="menu" href="javascript:;" title="Edit Payment" id="' . $linkId . '">Payment</a>';

        if ($waived) {
            $paymentMessage = '<span class="confirmComplete">waived</span>';
        } elseif ($paid) {
            $paymentMessage = '<span class="confirmComplete">paid</span>';
        } else {
            $paymentMessage = '<span class="confirm">unpaid</span>';
        }
        
        if ( isset($record['unconfirmed_payments']) && $record['unconfirmed_payments'] > 0 
            && $_SESSION['A_usertypeid'] == 0) 
        {
            $paymentMessage .= ' <b>*U*</b>';
        }
        
        $link .= '<div id="paymentMessage_' . $applicationId . '">' . $paymentMessage . '</div>';
        
        return $link;
    }
}
?>