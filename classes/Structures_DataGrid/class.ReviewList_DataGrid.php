<?php
require_once("Structures/DataGrid.php");
/*
* Create a datagrid with the specified columns according to
* round, decision, semiblind, etc. 
* 
* myColumns key:
* "[[prefix:]field to map]" => array(               // Field from the reviewListData db views
*       "label" => "[column label]",                // Label (column, checkbox, etc.) to use for the db field
*       "title" => "[column title]",                 // Optional full name for a short label 
*       "default" => TRUE|FALSE,                    // Should the column be displayed by default?
*       "required" => TRUE|FALSE,                   // Is display of the column required?
*       "rounds" => array(1|2|3[, 1|2|3[, 1|2|3]]), // Round(s) in which the column will be displayed (3 = decision round)
*       "semiblind" => TRUE|FALSE,                  // Should this column be displayed during semiblind review?
*       "view" => "committee|faculty|all",          // Who can view this column?
*       "formatter" => "[callback function]",       // Datagrid formatting callback
*       "formatterArg" => "[callback argument]"     // Datagrid formatting callback argument
*       )
*/
class ReviewList_DataGrid extends Structures_DataGrid
{

    protected $myColumns = array(

        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),
            
        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F",  "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad",  "title" => "Undergraduate Institution",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "title" => "Undergraduate GPA",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "title" => "GRE Verbal Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "title" => "GRE Quantitative Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),

        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL),        

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "possible_advisors" => array("label"=>"Poss Advisors", "title" => "Possible Advisors",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),

        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "round2_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "committee_reviewers" => array("label"=>"Revd By", "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),
            
        "language_screen_requested" => array("label"=>" Spec Reqs", "title" => "Special Requests", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecial()", "formatterArg" => NULL),
    
        "special_consideration_requested" => array("label"=>"Spec Cons", "title" => "Special Consideration",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecialConsideration()", "formatterArg" => NULL),

        "all_point1_committee_average" => array("label"=>"Avg Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => FALSE, "view" => "committee",
            "formatter" => NULL, "formatterArg" => NULL),

        "all_point1_committee_scores" => array("label"=>"Rank/Conf", "title" => "Rank/Confidence", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),

        "votes_for_round2" => array("label"=>"Round 2", "title" => "Votes for Round 2",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),        

        /*
        "round1_comments" => array("label"=>"Comments", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
        
        "round2_comments" => array("label"=>"Comments", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
            
        "round2_pertinent_info" => array("label"=>"Comments", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "faculty",
            "formatter" => "ReviewList_DataGrid::printPertinentInfo()", "formatterArg" => NULL),
        */
        
        "admit_to" => array("label"=>"Admit To", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printAdmitTo()", "formatterArg" => NULL),
        
        "ct_recommendations_requested" => array("label"=>"Recs Rcd", "title" => "Recommendations Received", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printRecommendationsReceived()", "formatterArg" => NULL),
            
        "mergedFileDate" => array("label"=>"Merged File", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMergedDate()", "formatterArg" => NULL),
           
        "mergedFilePath" => array("label"=>"Add to Zip", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printZipAdd()", "formatterArg" => NULL)
             
        );

    protected $round;               // int - The review round (3 = decision round)
    protected $decisionRound;       // bool - Is this for a decision round? 
    protected $setSemiblind;        // bool - Is the user toggling semiblind review?
    protected $semiblind;           // bool - Is this a semiblind view?
    protected $facultyView;         // bool - Is this a faculty view?
    protected $searchString;        // string - Indicates a search has been executed.
    protected $selectedColumns;     // array - fieldNames of specific selectable columns to display 
    
    public function __construct( $round = 1, $decisionRound = FALSE, $setSemiblind = FALSE, $semiblind = TRUE, 
                                    $facultyView = FALSE, $searchString = '', $selectedColumns = array(), 
                                    $limit = NULL, $page = NULL, $allCommentsOn = FALSE ) {
       
       parent::__construct($limit, $page);
       
       // Set the column parameters.
       $this->round = $round;
       $this->decisionRound = $decisionRound;
       if ($decisionRound) {
            $this->round = 3;   
       }
       $this->setSemiblind = $setSemiblind;
       $this->semiblind = $semiblind;
       $this->facultyView = $facultyView;
       $this->searchString = $searchString;
       $this->selectedColumns = $selectedColumns;

       // Set the table columns.
       $this->setColumns();
       
       // Set the table attributes.
       $this->setAttributes();
    }
    
    private function trimFieldNamePrefix($columnKey) {
        
        $columnKeyLength = strlen($columnKey);
        $prefixEnd = strpos($columnKey, ':');
        if ($prefixEnd) {
            $fieldName = substr($columnKey, $prefixEnd + 1);    
        } else {
            $fieldName = $columnKey;
        }
        return $fieldName;
    }
    
    private function setColumns() {
        
        
        foreach ($this->myColumns as $columnKey => $columnProperties ) {
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            
            // Don't add a column that doesn't match the round.
            if ( !in_array($this->round, $columnProperties['rounds']) ) {
                continue;    
            }
            
            // Don't add a faculty column to a non-faculty view and vice-versa.
            if ($this->facultyView) {
                if ( !($columnProperties['view'] == 'all' || $columnProperties['view'] == 'faculty') ) {
                    continue;
                }    
            } else {
                if ($columnProperties['view'] == 'faculty') {
                    continue;
                }                 
            }
            
            // Don't add non-semiblind columns to a semiblind view.
            if ( $this->semiblind && !$columnProperties['semiblind'] ) {
                continue;
            }

            // Don't add a non-required, non-default column that hasn't been selected.
            if ( !empty($this->selectedColumns) ) {
                if ( !$columnProperties['required'] && !in_array($fieldName, $this->selectedColumns) ) {  
                    if ( $this->setSemiblind && !$columnProperties['semiblind']) {
                        // Only skip the non-default non-semiblind columns when toggling off semiblind 
                        if (!$columnProperties['default']) {
                            continue;
                        }     
                    } else {
                        continue;
                    }
                }
            } else {
                if (!$columnProperties['default']) {
                    continue;
                }
            }            

            // Otherwise, add the column.
            $formatterArg = NULL;
            if ($columnProperties['formatterArg']) {
                if ( is_array($columnProperties['formatterArg']) ) {
                    $formatterArg = $columnProperties['formatterArg'];    
                } else {
                    $formatterArg = '$' . $columnProperties['formatterArg'];    
                }    
            }
            /*
            if ( isset($columnProperties['title']) &&  $columnProperties['title']) {
                $columnTitle = $columnProperties['title'];
            } else {
                $columnTitle = $columnProperties['label']; 
            }
            $columnLabel = '<div title="' . $columnTitle . '">' . $columnProperties['label'] . '</div>';
            */
            if ($columnProperties['label'] == 'row_number') {
                $columnLabel = '';
                $columnAttributes = array('class' => 'row_number');    
            } else {
                $columnLabel = $columnProperties['label'];
                $columnAttributes = array();    
            }
            
            $this->addColumn(
                new Structures_DataGrid_Column( 
                    $columnLabel, 
                    $fieldName, 
                    null, 
                    $columnAttributes, 
                    null, 
                    $columnProperties['formatter'],
                    $formatterArg
                )
            );  
            
        }
        
        // Always add a search column after a search.
        if ($this->searchString) {
            $this->addColumn(
                new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) ); 
        }
        
        return TRUE;
    }
    
    public function getRequiredColumns() {
    
        $requiredColumns = array();
        foreach ($this->myColumns as $columnKey => $columnProperties) {
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            if( $columnProperties['required'] && in_array($this->round, $columnProperties['rounds']) ) {
                $requiredColumns[$fieldName] = $columnProperties['label']; 
            }
        }
        return $requiredColumns; 
    }
    
    public function getSelectableColumns() {
        
        $selectableColumns = array();
        foreach ($this->myColumns as $columnKey => $columnProperties) {    
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            
            // Don't add a required column.
            if ($columnProperties['required']) {
                continue;     
            }
            
            // Don't add a column that doesn't match the round.
            if ( !in_array($this->round, $columnProperties['rounds']) ) {
                continue;    
            }
            
            // Don't add non-semiblind columns to a semiblind view.
            if ( $this->semiblind && !$columnProperties['semiblind'] ) {
                continue;
            }
            
            // Don't add a faculty column to a non-faculty view,
            // and don't add a committee column to a faculty view.
            if ($this->facultyView) {
                if ( !($columnProperties['view'] == 'all' || $columnProperties['view'] == 'faculty') ) {
                    continue;
                }    
            } else {
                if ($columnProperties['view'] == 'faculty') {
                    continue;
                }                 
            }
            
            // Otherwise, add the column.
            $selectableColumns[$fieldName] = $columnProperties['label'];
        }
        return $selectableColumns;
    }

    protected function setAttributes() {

        $this->renderer->setTableHeaderAttributes( array('color' => 'white', 'bgcolor' => '#99000') );
        $this->renderer->setTableEvenRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableOddRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableAttribute('width', '100%');
        $this->renderer->setTableAttribute('border', '0');
        $this->renderer->setTableAttribute('cellspacing', '1');
        $this->renderer->setTableAttribute('cellpadding', '5px');
        $this->renderer->setTableAttribute('id', 'applicantTable');
        $this->renderer->sortIconASC = '&uArr;';
        $this->renderer->sortIconDESC = '&dArr;';
        
        return TRUE;        
    }

    //########## Callback methods
    
    static function printNumber($params, $recordNumberStart) {
        extract($params);
        $number = $params['currRow'] + 1 + $recordNumberStart . ".";
        return $number;
    }

    static function printTouched($params) {
    
        extract($params);
        $touched1 = $record['round1_reviewer_touched'];
        $touched2 = $record['round2_reviewer_touched'];
        $touched3 = $record['round3_reviewer_touched'];
        if ($touched1 || $touched2 || $touched3) {
            $check = "<div class='touched yes'>Y</div>";
        } else {
            $check = "<div class='touched no'>N</div>";
        }
        return $check;
    }
    
    static function printCitizenship($params) {
        
        extract($params);
        $citizenship = '<div title="' . $record['cit_country'] . '">' . $record['cit_country_iso_code'] . '</div>';
        return $citizenship;    
    }
        
    static function printEditLink($params) {
        
        extract($params);
        
        // get "global" variables for setting up proper link
        global $usertypeid;
        global $round;
        global $department_id;
        global $decision;
        
        if ($usertypeid == 3) {
        
            $view = "3";
            
            if ($department_id == 6) {
                $round_param = $round;  
            } // elseif ($department_id == 2) {
            //    $round_param = 3;    
//}
             else {
                $round_param = 2;
            }
            
            /*
            if ($round == 1) {           
                $round_param = 2;
            } else {
                $round_param = $round;
            }
            */
               
        } else {
        
            $view = "2";
            $round_param = $round;  
        }
        
        if ($decision) {
            $show_decision = "&showDecision=1";
        } else {
            $show_decision = "";
        }
        
        $user_id = $record['lu_users_usertypes_id'];
        if (isRiMsRtChinaDepartment($department_id))
        {
            $nameArray = explode(',', $record['name']);
            $nameArray[0] = strtoupper($nameArray[0]);
            $name = implode(',', $nameArray);    
        }
        else
        {
            $name = $record['name'];    
        }
        $link = '<a class="menu" href="javascript: openForm(\'';
        // PLB added application id param 10/19/09
        //$link .= $user_id . '\',\'../review/userroleEdit_student_review.php?applicationId=' . $record['application_id'];
        $link .= $user_id . '\',\'../review/reviewApplicationSingle.php?applicationId=' . $record['application_id'];
        $link .= '&v=' . $view . '&r=' . $round_param;
        $link .= '&d=' . $department_id . $show_decision . '\')"' ;
        $link.= 'id="edit_' . $user_id . '" title="application id: ' . $record['application_id'] . '">' . $name . '</a>';
        return $link;
    }
 
    static function printSpecial($params) {
        
        extract($params);
        $specialConsiderationRequested = $record['special_consideration_requested'];
        $languageScreenRequested = $record['language_screen_requested'];
        $technicalScreenRequested = $record['technical_screen_requested'];
        
        $special = "";
        if ($specialConsiderationRequested) {
            $special .= "<div class='specialConsideration yes' title='Special Consideration'>SC</div>";
        }
        
        if ($languageScreenRequested) {
            $special .= "<div class='languageScreen yes' title='Language Screen'>LS</div>";
        }
        
        if ($technicalScreenRequested) {
            $special .= "<div class='technicalScreen yes' title='Technical Screen'>TS</div>";
        }
        
        return $special;
    }

    static function printSpecialConsideration($params) {
        
        extract($params);
        $special_consideration_requested = $record['special_consideration_requested'];
        $check = '';
        if ($special_consideration_requested) {
            $check = '<div class="yes">Y</div>';
        }
        return $check;
    }

    static function printRecommendationsReceived($params) {
        
        extract($params);
        $numRequested = $record['ct_recommendations_requested'];
        $numReceived = $record['ct_recommendations_submitted']; 
        $count = '';
        if ( ($numRequested > 0) && ($numReceived == $numRequested) ) {
            $count = '<div class="yes">All</div><br />Last:' . $record['recommend_last_letter_date'];  
        } else {
            $count = $numReceived . ' of ' . $numRequested;
            if (isset($record['recommend_last_letter_date'])) {
                $count .= '<br />Last:' . $record['recommend_last_letter_date'];
            }
        }

        return $count;
    }
    
    static function printMultivalue($params) {
        extract($params);
        $programs = str_replace("|", "<br />" , $record[$fieldName]);
        return $programs;
    }

    static function printTOEFL($params) {
    
        extract($params);
    
        $sectionLabels = array(
                        'IBT' => array(
                            'toefl_total' => 'Total',
                            'toefl_section1' => 'Speaking', 
                            'toefl_section2' => 'Listening', 
                            'toefl_section3' => 'Reading', 
                            'toefl_essay' => 'Writing'
                            ),
                        'PBT' => array(
                            'toefl_total' => 'Total',
                            'toefl_section1' => 'Section 1',
                            'toefl_section2' => 'Section 2',
                            'toefl_section3' => 'Section 3',
                            'toefl_essay' => 'TWE'
                            ),
                        'CBT' => array(
                            'toefl_total' => 'Total',
                            'toefl_section1' => 'Structure/Writing',
                            'toefl_section2' => 'Listening',
                            'toefl_section3' => 'Reading',
                            'toefl_essay' => 'Essay'
                            )
                        );

        $toefl = "";
        $toeflDate = $record['toefl_testdate'];
        $toeflType = $record['toefl_type'];
        if ($toeflType) {
            $toefl = "Type:&nbsp;" . $toeflType . "<br />"; 
            foreach ($sectionLabels[$toeflType] as $key => $label) {
                if ($key == 'toefl_total') {
                    $score = str_pad($record[$key], 3, '0', STR_PAD_LEFT);    
                } elseif ($key == 'toefl_essay')  {
                    $score = str_pad($record[$key], 4, '0', STR_PAD_LEFT); 
                } else {
                    $score = str_pad($record[$key], 2, '0', STR_PAD_LEFT);    
                }
                $toefl .= $label . ":&nbsp;" . $score;
                if ($key != "toefl_essay") {
                    $toefl .= "<br />";
                }   
            }
        }
            
        return $toefl;
    }

    static function printIELTS($params) {
    
        extract($params);
        $listeningScore = $record['ieltsscore_listeningscore'];
        $readingScore = $record['ieltsscore_readingscore'];
        $writingScore = $record['ieltsscore_writingscore'];
        $speakingScore = $record['ieltsscore_speakingscore'];
        $overallScore = $record['ieltsscore_overallscore'];
        
        if ( !$listeningScore && !$readingScore && !$writingScore 
            && !$speakingScore && !$overallScore)
        {
            return '';
        }
        
        $ielts = 'Listening:&nbsp;' . $listeningScore . '<br/>';
        $ielts .= 'Reading:&nbsp;' . $readingScore . '<br/>';
        $ielts .= 'Writing:&nbsp;' . $writingScore . '<br/>';
        $ielts .= 'Speaking:&nbsp;' . $speakingScore . '<br/>';
        $ielts .= 'Overall:&nbsp;' . $overallScore;
            
        return $ielts;
    }

    static function printReviewerRanks($params) {
        extract($params);
        $rank = str_replace("|", "<br />" , $record['round1_point1_scores']);
        return $rank;
    }

    static function printComments($params) {
        global $round;
        extract($params);
        $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round1_comments']) );
        if ($round == 2 || $round == 3) {
            $comments .= "<br /><br />";
            $comments .= htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_comments']) );
            $comments .= "<br /><br />";
            $comments .= htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_pertinent_info']) );    
        }
        return $comments;
    }
    
    static function printPertinentInfo($params) {
        extract($params);
        $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_pertinent_info']) );
        return $comments;
    } 

    static function printReviewers($params) {
        extract($params);
        $reviewers = '';
        $reviewerArray = explode('|', $record[$fieldName]);
        for ($i = 0; $i < count($reviewerArray); $i++) {
            $name = $reviewerArray[$i];
            $nameArray = explode(' ', $name);
            $reviewers .= '<a title="' . $name . '">';
            if ($nameArray[0])
            {
                $reviewers .= substr($nameArray[0], 0, 1) . ' ';
            } 
            $reviewers .= end($nameArray) . '</a><br/>';
        }
        return $reviewers;
    }
    
    static function printMultiScore($params) {
        extract($params);
        $scores = '';
        $scoresArray = explode('|', $record[$fieldName]);
        for ($i = 0; $i < count($scoresArray); $i++) {
            $scoreArray = explode(':', $scoresArray[$i]);
            $name = $scoreArray[0];
            $nameArray = explode(' ', $name);
            if ($nameArray[0])
            {
                $scores .= substr($nameArray[0], 0, 1) . ' ';
            }
            $scores .= end($nameArray);
            if ( isset($scoreArray[1]) ) {
                $scores .=  ': ' . $scoreArray[1] . '<br/>';  
            }
        }
        return $scores;
    }

    public static function printRounded($params, $args = array()) {
        extract($params);
        extract($args);
        
        $value = $record[$fieldName];
        if ($value) {
            $roundedValue = round($record[$fieldName], $precision);
            return number_format($roundedValue, $precision);    
        } else {
            return $value;
        }
        
        return round($record[$fieldName], 3);    
    }

    static function printAdmitTo($params) {
        
        extract($params);
        
        if ($record['admit_to_decision']) {
            $programString = $record['admit_to_decision'];
        } else {
            $programString = $record['admit_to'];    
        }
        
        $programsArray = explode('|', $programString);
        for ($i = 0; $i < count($programsArray); $i++) {
            $programsArray[$i] = str_replace('(wait)', '<i>(WAIT)</i>', $programsArray[$i]);        
        }
        $programs = implode('<br>', $programsArray);
        
        return $programs;
    }
    
    static function printRound3Entered($params) {
        
        extract($params);
        
        if ($record['status_time']) {
            $status_time = $record['status_time'];
        }  else {
              $status_time = "None";
        }
        
        return $status_time;
    }
    
    static function printMergedDate($params) 
        {
            extract($params);
            $mergeDate = $record['mergedFileDate'];
            if (isset($mergeDate)) {
                $mergeDate .= $pdf_link = '<a target="_blank" href="' . $record['mergedFilePath'] . '">Link</a>';
            } else {
                $mergeDate = "";
            }
            return $mergeDate;             
        }
                                                              
        static function printZipAdd($params) 
        {
            extract($params);
            //debugbreak();
            //$mergeDate =  '<input type="checkbox" name="files" value="' . $record['mergedFilePath'] .'" onChange="process1(this)"/>';
            $mergeDate =  '<input type="checkbox" name="files[]" class="files" value="' . $record['mergedFilePath'] .'"/>';
         //   $mergeDate .=  '<input type="hidden" id=' . $record['application_id'] . ' name="files2[]" value="' . $record['mergedFilePath'] . '" />';
            return $mergeDate;             
        }
    
}
?>