<?php
require_once("Structures/DataGrid.php");

class BulkMailApplicantList_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct($dataArray, $limit = NULL, $page = NULL, $allUsersSelected = NULL) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'AdminList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart()));  
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '20%', 'align' => 'left'), null, 'BulkMailApplicantList_Datagrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Submitted', 'sbmd', 'sbmd', 
                array('width' => '10%', 'align' => 'left'), null, 'BulkMailApplicantList_Datagrid::printSubmitted()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Paid/Waived', 'pd', 'pd', 
                array('width' => '10%', 'align' => 'center'), null, 'BulkMailApplicantList_Datagrid::printPaid()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Programs', 'programs', 'programs', 
                array('width' => '59%', 'align' => 'left'), null, 'AdminList_Datagrid::printPrograms()'));
       
       /*
       $this->addColumn(
            new Structures_DataGrid_Column('', 'gre_rcvd', 'gre_rcvd', 
                array('width' => '14%', 'align' => 'center'), null, 'AdminList_Datagrid::printTestScoresLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'trans_rcvd', 'trans_rcvd', 
                array('width' => '14%', 'align' => 'center'), null, 'AdminList_Datagrid::printTranscriptLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'ct_recommendations_requested', 'ct_recommendations_requested', 
                array('width' => '14%', 'align' => 'center'), null, 'AdminList_Datagrid::printRecommendationsLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Complete', 'cmp', 'cmp', 
                array('width' => '5%', 'align' => 'center'), null, 'AdminList_Datagrid::printCompleteCheckbox()'));  
       */
       
       $this->addColumn(
            new Structures_DataGrid_Column($this->getSelectAllCheckbox($allUsersSelected), null, null, 
                array('width' => '2%', 'align' => 'left', 'valign' => 'top'), null,
                'BulkMailApplicantList_Datagrid::printCheckbox()'));
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }
    
    /*
    * Private methods
    */
    private function getSelectAllCheckbox($allUsersSelected = NULL)
    {
        if ($allUsersSelected)
        {
            return '<input type="checkbox" id="selectAllUsers" checked="checked">';
        }
        else
        {
            return '<input type="checkbox" id="selectAllUsers">';        
        }
    }

    /*
    * ########## Callback methods #############
    */

    static function printFullName($params) 
    {   
        extract($params);
        $userId = $record['user_id'];
        $applicationId = $record['application_id'];
        
        if ($record['sbmd'] == 'yes') 
        {
            $name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">' . $record['name'] . '</span>';    
        } 
        else 
        {
            $name = '<span id="applicantName_' . $record['application_id'] . '">' . $record['name'] . '</span>';
        }
        $name .= '<br />';
        $name .= '<a class="menu print" href="javascript:;" id="print_'. $userId . '_' . $applicationId . '" ';
        $name .= 'title="Print Application ' . $applicationId . '">';
        $name .= 'View&nbsp;/&nbsp;Print</a>';

        return $name;
    }
    
    static function printSubmitted($params) 
    {   
        extract($params);

        if ($record['sbmd'] == 'yes') 
        {
            $submitted = '<span class="confirmComplete">yes</span>';    
        } 
        else 
        {
            $submitted = '<span class="confirm">no</span>';
        }

        return $submitted;
    }
    
    static function printPaid($params) 
    {   
        extract($params);

        if ($record['pd'] == 'yes') 
        {
            $paid = '<span class="confirmComplete">paid</span>';    
        }
        elseif ($record['wd'] == 'yes')
        {
            $paid = '<span class="confirmComplete">waived</span>';    
        } 
        else 
        {
            $paid = '<span class="confirm">unpaid</span>';
        }

        return $paid;
    }
    
    static function printCheckbox($params) 
    {
        extract($params);
        $luuId = $record['user_id'];
        $checkboxId = 'checkbox_' . $luuId;
        
        $checkbox = '<input class="userCheckbox shiftclick" type="checkbox" 
            name="selectedUsers[' . $luuId . ']" id="' . $checkboxId . '"';
            
        if ($record['selected'])
        {
            $checkbox .= ' checked="checked">';    
        }
        else
        {
            $checkbox .= '>';    
        }    
        
    
        return $checkbox;
    }
    
    
    
    
    
    static function printNumber($params, $recordNumberStart) {
        extract($params);
        $number = $params['currRow'] + $recordNumberStart . ".";
        return $number;
    }    

    static function printPrintLink($params) {
        extract($params);
        $userId = $record['user_id'];
        $applicationId = $record['application_id'];
        $links = '<ul style="list-style: circle; margin-left: 6px; padding-left: 6px; margin-top: 0px;"><li>';
        $links .= '<a class="menu print" href="javascript:;" id="print_'. $userId . '_' . $applicationId . '" ';
        $links .= 'title="Print Application ' . $applicationId . '">';
        $links .= 'View&nbsp;/&nbsp;Print</a></li>';
        $links .= '<li><a class="menu login" target="_blank" href="../apply/index.php?uid=';
        $links .= $record['guid'] . '&a=1&domain=' . $record['application_domain'];
        $links  .= '&applicationId=' . $applicationId . '">';
        $links .= 'Login As Applicant</a></li></ul>';
        return $links;
    }



    static function printPrograms($params) {
        extract($params);
        $programs = str_replace("|", "<br />" , $record[$fieldName]);
        return $programs;
    }
    
    static function printTestScoresLink($params) {
        
        extract($params);
        
        $applicationId = $record['application_id'];
        $linkId = 'testscores_' . $applicationId;     
        $link = '<a class="menu" href="javascript:;" title="Edit Test Scores" id="' . $linkId . '">Test Scores</a>';

        $greReceived = $record['gre_rcvd'];
        $greClass = 'confirm';
        $greMessage = '';
        if ( $greReceived ) {
            $greClass = 'confirmComplete';
            $greMessage = 'rcd';
        }
        $link .= '<br /><span class="' . $greClass . '">GRE </span>';
        $link .= '<span class="confirm_complete" id="greReceivedMessage_' . $applicationId . '">' . $greMessage . '</span>';
        
        $countGreSubjectEntered = $record['ct_gresubject_entered'];
        $countGreSubjectReceived = $record['ct_gresubject_received'];
        $greSubjectClass = 'confirm';
        $greSubjectMessage = '';
        if ( $countGreSubjectEntered ) {
            if ( $countGreSubjectReceived ) {
                $greSubjectClass = 'confirmComplete';
                $greSubjectMessage = 'rcd';
            }
            $link .= '<br /><span class="' . $greSubjectClass . '">GRE Subj. </span>';
            $link .= '<span class="confirmComplete" id="greSubjectReceivedMessage_' . $applicationId . '">';
            $link .= $greSubjectMessage . '</span>';
        }
        
        $countGmatEntered = $record['ct_gmat_entered'];
        $countGmatReceived = $record['ct_gmat_received'];
        $gmatClass = 'confirm';
        $gmatMessage = '';
        if ( $countGmatEntered ) {
            if ( $countGmatReceived ) {
                $gmatClass = 'confirmComplete';
                $gmatMessage = 'rcd';
            }
            $link .= '<br /><span class="' . $gmatClass . '">GMAT </span>';
            $link .= '<span class="confirmComplete" id="gmatReceivedMessage_' . $applicationId . '">';
            $link .= $gmatMessage . '</span>';
        }

        $toeflEntered = $record['toefl_entered'];
        $toeflReceived = $record['toefl_submitted'];
        $toeflClass = 'confirm';
        $toeflMessage = '';
        if ( $toeflEntered ) {
            if ( $toeflReceived ) {
                $toeflClass = 'confirmComplete';
                $toeflMessage = 'rcd';
            }
            $link .= '<br /><span class="' . $toeflClass . '">TOEFL </span>';
            $link .= '<span class="confirmComplete" id="toeflReceivedMessage_' . $applicationId . '">';
            $link .= $toeflMessage . '</span>'; 
        } 

        $countIeltsEntered = $record['ct_ielts_entered'];
        $countIeltsReceived = $record['ct_ielts_received'];
        $ieltsClass = 'confirm';
        $ieltsMessage = '';
        if ( $countIeltsEntered ) {
            if ( $countIeltsReceived ) {
                $ieltsClass = 'confirmComplete';
                $ieltsMessage = 'rcd';
            }
            $link .= '<br /><span class="' . $ieltsClass . '">IELTS </span>';
            $link .= '<span class="confirmComplete" id="ieltsReceivedMessage_' . $applicationId . '">';
            $link .= $ieltsMessage . '</span>';
        }
        
        $nativeTongue = strtolower(trim($record['native_tongue']));
        if ($nativeTongue != 'english' && $toeflEntered == 0 && $countIeltsEntered == 0)
        {
            $eslClass = 'confirm';
            $eslMessage = 'ESL';

            $link .= '<br /><span class="' . $eslClass . '" id="eslMessage_' . $applicationId . '">';
            $link .= $eslMessage . '</span>';    
        }
        
        return $link;
    }


    static function printTranscriptLink($params) {
        
        extract($params);
        
        $applicationId = $record['application_id'];
        $linkId = 'transcripts_' . $applicationId;
        $link = '<a class="menu" href="javascript:;" title="Edit Transcripts" id="' . $linkId . '">Transcripts</a>';
        
        $countTranscriptsReceived = $record['ct_transcripts_received'];
        $countTranscriptsSubmitted = $record['ct_transcripts_submitted'];
        $transcriptClass = 'confirm';
        if ( $countTranscriptsReceived == $countTranscriptsSubmitted && $countTranscriptsSubmitted > 0 ) {
            $transcriptClass = 'confirmComplete';
        }
        $link .= '<br /><span id="transcriptsReceivedMessage_' . $applicationId . '" class="' . $transcriptClass . '">';
        $link .= $countTranscriptsReceived . ' of ' .  $countTranscriptsSubmitted . '</span>';  
        
        return $link;
    }


    static function printRecommendationsLink($params) {
        
        extract($params);
        
        /*
        * ALERT: This is hack to handle MSE && VLIS special cases 
        * for not counting forms when determining whether a
        * recommendation has been submtted. 
        */
        $departmentId = NULL;
        $unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
        $unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
        if ($unit == 'department') {
            $departmentId = $unitId;
        }
        
        $applicationId = $record['application_id'];
        $linkId = 'recommendations_' . $applicationId;
        $link = '<a class="menu" href="javascript:;" title="Edit Recommendations" id="' . $linkId . '">Recommendations</a>';
        
        // Form submission is required only for MS-HCII. 
        if ( isMseMsitDepartment($departmentId) || $departmentId == 20) {
            $countRecommendationsReceived = $record['ct_recommendation_files_submitted'];    
        } elseif ($departmentId == 49) {
            $countRecommendationsReceived = $record['ct_recommendation_files_forms_submitted'];    
        } else {
            $countRecommendationsReceived = $record['ct_recommendations_submitted'];    
        }
        
        $countRecommendationsRequested = $record['ct_recommendations_requested'];
        $recommendationClass = 'confirm';
        if ( $countRecommendationsReceived == $countRecommendationsRequested && $countRecommendationsRequested > 0 ) {
            $recommendationClass = 'confirmComplete';
        }
        $link .= '<br /><span id="recommendationsReceivedMessage_' . $applicationId . '" class="' . $recommendationClass . '">';
        $link .= $countRecommendationsReceived . ' of ' .  $countRecommendationsRequested . '</span>';
        
        return $link;
    }

    static function printPaymentLink($params) {

        extract($params);
        $applicationId = $record['application_id'];
        $paymentAmount = $record['paymentamount'];
        $waived = $record['wd'];
        $paid = $record['pd'];
        $link = '<a class="menu" href="javascript:;" title="Edit Payment" id="payment_' . $applicationId . '">Payment</a>';

        if ($waived == 'yes') {
            $paymentMessage = '<span class="confirmComplete">waived</span>';
        } elseif ($paid == 'yes') {
            //$paymentMessage = '<span class="confirmComplete">paid</span>: <span class="confirm">' . $paymentAmount . '</span>';
            $paymentMessage = '<span class="confirmComplete">paid</span>';
        } else {
            //$totalFees = '$' . number_format($record['total_fees'], 2);
            //$paymentMessage = '<span class="confirm">unpaid: ' . $totalFees . '</span>';
            $paymentMessage = '<span class="confirm">unpaid</span>';
        }
        
        if ( isset($record['unconfirmed_payments']) && $record['unconfirmed_payments'] > 0 
            && $_SESSION['A_usertypeid'] == 0) 
        {
            $paymentMessage .= ' <b>*U*</b>';
        }
        
        $link .= '<div id="paymentMessage_' . $applicationId . '">' . $paymentMessage . '</div>';
        
        return $link;
    }

    static function printCompleteCheckbox($params) {
        extract($params);
        $applicationId = $record['application_id'];
        if ($record['cmp'] == "yes") {
            $checked = "checked";
        } else {
            $checked = "";
        }
        $checkbox = '<input type="checkbox" class="applicationComplete" id="applicationComplete_'. $applicationId . '" ' . $checked . ' />';
        return $checkbox;
    }


    static function printPublicationsLink($params) {
        extract($params);
        $application_id = $record['application_id'];
        $link_id = 'publications_' . $application_id;
        if ( $record['ct_publications'] == NULL ) {
            $ct_publications = "0";
        } else {
            $ct_publications = $record['ct_publications'];
        }
        $link = '<a class="menu" href="javascript:;" title="Edit Publications" id="' . $link_id . '">';
        $link .= 'Publications</a>';
        $link .= '<br /><span class="confirm">' . $ct_publications . '</span>';
        return $link;
    }    

}
?>