<?php

class NormalizationListScoresMsCs_DataGrid extends ReviewList_DataGrid
{

    protected $myColumns = array(
        
        "rank" => array("label" => "Rk", "title" => "Rank",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printPadded()", "formatterArg" => NULL),
            
        "rank_weighted" => array("label" => "Wtd Rk", "title" => "Weighted Rank",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printPadded()", "formatterArg" => NULL),

        "application_id1" => array("label" => "&#916; Rk", "title" => "Rank Change",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printDelta()", "formatterArg" => NULL),

        "decision" => array("label"=>"Dec", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "name" => array("label" => "Name", "title" => "Name",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printEditLink()", "formatterArg" => NULL),
            
        "application_id" => array("label" => "AppId", "title" => "Application ID",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
                    
        "cit_country_iso_code" => array("label" => "Ctzn", "title" => "Citizenship",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printCitizenship()", "formatterArg" => NULL),

        "gender" => array("label"=>"M/F",  "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "undergrad_institute_name" => array("label"=>"Undergrad",  "title" => "Undergraduate Institution",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),

        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "score_average" => array("label"=>"Avg", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printRounded()", 
            "formatterArg" => array('precision' => 3)),
        
        "score_average_weighted" => array("label" => "Wtd Avg", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printRounded()", 
            "formatterArg" => array('precision' => 3)),
            
        "score_count" => array("label" => "n", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL)
             
        );
        
    public function addReviewerColumns($scoreArrayKeys) {
        
        foreach ($scoreArrayKeys as $field) {
            
            $fieldArray = explode('|', $field);
            if ( count($fieldArray) > 1 ) {

                $initials = $fieldArray[0];
                
                $this->addColumn(
                    new Structures_DataGrid_Column(
                        $initials, 
                        $field, 
                        null, 
                        null,
                        null, 
                        "ReviewList_DataGrid::printRounded()",
                        array('precision' => 2)
                        )
                    );            
    
            }    
        }
        
        return TRUE;        
    }
}
?>