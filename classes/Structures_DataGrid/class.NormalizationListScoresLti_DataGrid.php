<?php

class NormalizationListScoresLti_DataGrid extends ReviewList_DataGrid
{

    protected $myColumns = array(
        
        "rank" => array("label" => "Rk", "title" => "Rank",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printPadded()", "formatterArg" => NULL),
            
        "rank_weighted" => array("label" => "Wtd Rk", "title" => "Weighted Rank",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printPadded()", "formatterArg" => NULL),

        "application_id" => array("label" => "&#916; Rk", "title" => "Rank Change",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printDelta()", "formatterArg" => NULL),

        "decision" => array("label"=>"Dec", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "name" => array("label" => "Name", "title" => "Name",
            "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "NormalizationListScores_DataGrid::printEditLink()", "formatterArg" => NULL),
                    
        "cit_country_iso_code" => array("label" => "Ctzn", "title" => "Citizenship",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printCitizenship()", "formatterArg" => NULL),

        "gender" => array("label"=>"M/F",  "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),

        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "score_average" => array("label"=>"Avg", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printRounded()", 
            "formatterArg" => array('precision' => 3)),
        
        "score_average_weighted" => array("label" => "Wtd Avg", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printRounded()", 
            "formatterArg" => array('precision' => 3)),
            
        "score_count" => array("label" => "n", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL)
             
        );

    public function addReviewerColumns($scoreArrayKeys) {
        
        foreach ($scoreArrayKeys as $field) {
            
            $fieldArray = explode('|', $field);
            if ( count($fieldArray) > 1 ) {

                $initials = $fieldArray[0];
                
                $this->addColumn(
                    new Structures_DataGrid_Column(
                        $initials, 
                        $field, 
                        null, 
                        null,
                        null, 
                        "ReviewList_DataGrid::printRounded()",
                        array('precision' => 2)
                        )
                    );            
    
            }    
        }
        
        return TRUE;        
    }
    
    public static function printName($params) {       
        extract($params);
        $name = $record[$fieldName];
        if ($name == 'All Reviewers') {
            $name = str_replace('All', '&#8746;', $name);
        }
        return $name;
    }
        
    public static function printDelta($params) {       
        extract($params);
        $rank = $record['rank'];
        $rankWeighted = $record['rank_weighted'];
        $rankChange = $rank - $rankWeighted; 
        if ($rankChange < 0) {
            $rankChangeString = '-';
        } else {
            $rankChangeString = '';    
        }
        $rankChangeString .=  str_pad( abs($rankChange), 2, '0', STR_PAD_LEFT );        
        return $rankChangeString;
    }

    public static function printPadded($params) {       
        extract($params);        
        return str_pad( $record[$fieldName], 3, '0', STR_PAD_LEFT );
    }
    
    static function printEditLink($params) {
        
        extract($params);

        $usertypeid = $_SESSION['A_usertypeid'];
        global $round;
        global $department_id;

        
        if ($usertypeid == 3) {
        
            $view = "3";
            
            if ($department_id == 2) {
                $round_param = 3;    
            } else {
                $round_param = 2;
            }
            
        } else {
        
            $view = "2";
            $round_param = $round;  
        }
        
        if ($usertypeid == 1) {
            $show_decision = "&showDecision=1";
        } else {
            //$show_decision = "";
            $show_decision = "&showDecision=1";
        }
        
        $user_id = $record['lu_users_usertypes_id'];
        if (isRiMsRtChinaDepartment($department_id))
        {
            $nameArray = explode(',', $record['name']);
            $nameArray[0] = strtoupper($nameArray[0]);
            $name = implode(',', $nameArray);    
        }
        else
        {
            $name = $record['name'];    
        }
        $link = '<a class="menu" href="javascript: openForm(\'';
        $link .= $user_id . '\',\'../review/reviewApplicationSingle.php?applicationId=' . $record['application_id'];
        $link .= '&v=' . $view . '&r=' . $round_param;
        $link .= '&d=' . $department_id . $show_decision . '\')"' ;
        $link.= 'id="edit_' . $user_id . '" title="application id: ' . $record['application_id'] . '">' . $name . '</a>';
        return $link;
    }

}
?>