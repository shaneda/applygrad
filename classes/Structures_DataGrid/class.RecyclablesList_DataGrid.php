<?php
require_once("Structures/DataGrid.php");
/*
* Create a datagrid with the specified columns according to
* round, decision, semiblind, etc. 
* 
* myColumns key:
* "[[prefix:]field to map]" => array(               // Field from the reviewListData db views
*       "label" => "[column label]",                // Label (column, checkbox, etc.) to use for the db field
*       "title" => "[column title]",                 // Optional full name for a short label 
*       "default" => TRUE|FALSE,                    // Should the column be displayed by default?
*       "required" => TRUE|FALSE,                   // Is display of the column required?
*       "rounds" => array(1|2|3[, 1|2|3[, 1|2|3]]), // Round(s) in which the column will be displayed (3 = decision round)
*       "semiblind" => TRUE|FALSE,                  // Should this column be displayed during semiblind review?
*       "view" => "committee|faculty|all",          // Who can view this column?
*       "formatter" => "[callback function]",       // Datagrid formatting callback
*       "formatterArg" => "[callback argument]"     // Datagrid formatting callback argument
*       )
*/
class RecyclablesList_DataGrid extends Structures_DataGrid
{

    protected $myColumns = array(
    
        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "RecyclablesList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),
            
        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "RecyclablesList_DataGrid::printNameLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F",  "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad",  "title" => "Undergraduate Institution",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "title" => "Undergraduate GPA",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "title" => "GRE Verbal Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "title" => "GRE Quantitative Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),

        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL),        

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "possible_advisors" => array("label"=>"Poss Advisors", "title" => "Possible Advisors",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3,4), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL)
        );

    protected $round;               // int - The review round (3 = decision round)
    protected $decisionRound;       // bool - Is this for a decision round? 
    protected $setSemiblind;        // bool - Is the user toggling semiblind review?
    protected $semiblind;           // bool - Is this a semiblind view?
    protected $facultyView;         // bool - Is this a faculty view?
    protected $searchString;        // string - Indicates a search has been executed.
    protected $selectedColumns;     // array - fieldNames of specific selectable columns to display 
    
    public function __construct($searchString = '', $selectedColumns = array(), $limit = NULL, $page = NULL) {
       
       parent::__construct($limit, $page);
       
       // Set the column parameters.
       $this->round = 1;
       $this->searchString = $searchString;
       $this->selectedColumns = $selectedColumns;

       $this->addColumn(
            new Structures_DataGrid_Column('<input type="checkbox" id="selectAllApplications">', null, null, 
                array('width' => '2%', 'align' => 'left', 'valign' => 'top'), null,
                'RecyclablesList_DataGrid::printCheckbox()'));
       
       // Set the table columns.
       $this->setColumns();
       
       // Set the table attributes.
       $this->setAttributes();
    }
    
    private function trimFieldNamePrefix($columnKey) {
        
        $columnKeyLength = strlen($columnKey);
        $prefixEnd = strpos($columnKey, ':');
        if ($prefixEnd) {
            $fieldName = substr($columnKey, $prefixEnd + 1);    
        } else {
            $fieldName = $columnKey;
        }
        return $fieldName;
    }
    
    private function setColumns() {
        
        
        foreach ($this->myColumns as $columnKey => $columnProperties ) {
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            
            // Don't add a column that doesn't match the round.
            if ( !in_array($this->round, $columnProperties['rounds']) ) {
                continue;    
            }
            
            // Don't add a faculty column to a non-faculty view and vice-versa.
            if ($this->facultyView) {
                if ( !($columnProperties['view'] == 'all' || $columnProperties['view'] == 'faculty') ) {
                    continue;
                }    
            } else {
                if ($columnProperties['view'] == 'faculty') {
                    continue;
                }                 
            }
            
            // Don't add non-semiblind columns to a semiblind view.
            if ( $this->semiblind && !$columnProperties['semiblind'] ) {
                continue;
            }

            // Don't add a non-required, non-default column that hasn't been selected.
            if ( !empty($this->selectedColumns) ) {
                if ( !$columnProperties['required'] && !in_array($fieldName, $this->selectedColumns) ) {  
                    if ( $this->setSemiblind && !$columnProperties['semiblind']) {
                        // Only skip the non-default non-semiblind columns when toggling off semiblind 
                        if (!$columnProperties['default']) {
                            continue;
                        }     
                    } else {
                        continue;
                    }
                }
            } else {
                if (!$columnProperties['default']) {
                    continue;
                }
            }            

            // Otherwise, add the column.
            $formatterArg = NULL;
            if ($columnProperties['formatterArg']) {
                if ( is_array($columnProperties['formatterArg']) ) {
                    $formatterArg = $columnProperties['formatterArg'];    
                } else {
                    $formatterArg = '$' . $columnProperties['formatterArg'];    
                }    
            }
            /*
            if ( isset($columnProperties['title']) &&  $columnProperties['title']) {
                $columnTitle = $columnProperties['title'];
            } else {
                $columnTitle = $columnProperties['label']; 
            }
            $columnLabel = '<div title="' . $columnTitle . '">' . $columnProperties['label'] . '</div>';
            */
            if ($columnProperties['label'] == 'row_number') {
                $columnLabel = '';
                $columnAttributes = array('class' => 'row_number');    
            } else {
                $columnLabel = $columnProperties['label'];
                $columnAttributes = array();    
            }
            
            $this->addColumn(
                new Structures_DataGrid_Column( 
                    $columnLabel, 
                    $fieldName, 
                    null, 
                    $columnAttributes, 
                    null, 
                    $columnProperties['formatter'],
                    $formatterArg
                )
            );  
            
        }
        
        // Always add a search column after a search.
        if ($this->searchString) {
            $this->addColumn(
                new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) ); 
        }
        
        return TRUE;
    }
    
    public function getRequiredColumns() {
    
        $requiredColumns = array();
        foreach ($this->myColumns as $columnKey => $columnProperties) {
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            if( $columnProperties['required'] && in_array($this->round, $columnProperties['rounds']) ) {
                $requiredColumns[$fieldName] = $columnProperties['label']; 
            }
        }
        return $requiredColumns; 
    }
    
    public function getSelectableColumns() {
        
        $selectableColumns = array();
        foreach ($this->myColumns as $columnKey => $columnProperties) {    
            
            $fieldName = $this->trimFieldNamePrefix($columnKey);
            
            // Don't add a required column.
            if ($columnProperties['required']) {
                continue;     
            }
            
            // Don't add a column that doesn't match the round.
            if ( !in_array($this->round, $columnProperties['rounds']) ) {
                continue;    
            }
            
            // Don't add non-semiblind columns to a semiblind view.
            if ( $this->semiblind && !$columnProperties['semiblind'] ) {
                continue;
            }
            
            // Don't add a faculty column to a non-faculty view,
            // and don't add a committee column to a faculty view.
            if ($this->facultyView) {
                if ( !($columnProperties['view'] == 'all' || $columnProperties['view'] == 'faculty') ) {
                    continue;
                }    
            } else {
                if ($columnProperties['view'] == 'faculty') {
                    continue;
                }                 
            }
            
            // Otherwise, add the column.
            $selectableColumns[$fieldName] = $columnProperties['label'];
        }
        return $selectableColumns;
    }

    protected function setAttributes() {

        $this->renderer->setTableHeaderAttributes( array('color' => 'white', 'bgcolor' => '#99000') );
        $this->renderer->setTableEvenRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableOddRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
        $this->renderer->setTableAttribute('width', '100%');
        $this->renderer->setTableAttribute('border', '0');
        $this->renderer->setTableAttribute('cellspacing', '1');
        $this->renderer->setTableAttribute('cellpadding', '5px');
        $this->renderer->setTableAttribute('id', 'applicantTable');
        $this->renderer->sortIconASC = '&uArr;';
        $this->renderer->sortIconDESC = '&dArr;';
        
        return TRUE;        
    }

    //########## Callback methods
    
    static function printCheckbox($params) 
    {
        global $selectedApplications;    
        extract($params);
        $applicationId = $record['application_id'];
        $checkboxId = 'applicationId_' . $applicationId;
        if (in_array($applicationId, $selectedApplications))
        {
            $checked = 'checked="checked"';
        }
        else
        {
            $checked = '';
        }
        $checkbox = '<input class="applicationCheckbox shiftclick" type="checkbox" 
            name="applicationId[]" id="' . $checkboxId . '" value="' . $applicationId . '" ' . $checked . ">";
        return $checkbox;
    }
    
    static function printNumber($params, $recordNumberStart) 
    {
        extract($params);
        $number = $params['currRow'] + 1 + $recordNumberStart . ".";
        return $number;
    }
    
    static function printNameLink($params) 
    {
        extract($params);
        $name = '<a href="../review/userroleEdit_student_print.php?id=' . $record['lu_users_usertypes_id'];
        $name.= '&applicationId=' . $record['application_id']  . '" target="_blank">';
        $name .= $record['name'] . '</a>';
        return $name;   
    }

    static function printRecommendationsReceived($params) {
        
        extract($params);
        $numRequested = $record['ct_recommendations_requested'];
        $numReceived = $record['ct_recommendations_submitted']; 
        $count = '';
        if ( ($numRequested > 0) && ($numReceived == $numRequested) ) {
            $count = '<div class="yes">All</div>';  
        } else {
            $count = $numReceived . ' of ' . $numRequested;
        }

        return $count;
    }
    
    static function printMultivalue($params) {
        extract($params);
        $programs = str_replace("|", "<br />" , $record[$fieldName]);
        return $programs;
    }

    static function printTOEFL($params) {
    
        extract($params);
    
        $sectionLabels = array(
                        'IBT' => array(
                            'toefl_total' => 'Total',
                            'toefl_section1' => 'Speaking', 
                            'toefl_section2' => 'Listening', 
                            'toefl_section3' => 'Reading', 
                            'toefl_essay' => 'Writing'
                            ),
                        'PBT' => array(
                            'toefl_total' => 'Total',
                            'toefl_section1' => 'Section 1',
                            'toefl_section2' => 'Section 2',
                            'toefl_section3' => 'Section 3',
                            'toefl_essay' => 'TWE'
                            ),
                        'CBT' => array(
                            'toefl_total' => 'Total',
                            'toefl_section1' => 'Structure/Writing',
                            'toefl_section2' => 'Listening',
                            'toefl_section3' => 'Reading',
                            'toefl_essay' => 'Essay'
                            )
                        );

        $toefl = "";
        $toeflDate = $record['toefl_testdate'];
        $toeflType = $record['toefl_type'];
        if ($toeflType) {
            $toefl = "Type:&nbsp;" . $toeflType . "<br />"; 
            foreach ($sectionLabels[$toeflType] as $key => $label) {
                if ($key == 'toefl_total') {
                    $score = str_pad($record[$key], 3, '0', STR_PAD_LEFT);    
                } elseif ($key == 'toefl_essay')  {
                    $score = str_pad($record[$key], 4, '0', STR_PAD_LEFT); 
                } else {
                    $score = str_pad($record[$key], 2, '0', STR_PAD_LEFT);    
                }
                $toefl .= $label . ":&nbsp;" . $score;
                if ($key != "toefl_essay") {
                    $toefl .= "<br />";
                }   
            }
        }
            
        return $toefl;
    }

    static function printIELTS($params) {
    
        extract($params);
        $listeningScore = $record['ieltsscore_listeningscore'];
        $readingScore = $record['ieltsscore_readingscore'];
        $writingScore = $record['ieltsscore_writingscore'];
        $speakingScore = $record['ieltsscore_speakingscore'];
        $overallScore = $record['ieltsscore_overallscore'];
        
        if ( !$listeningScore && !$readingScore && !$writingScore 
            && !$speakingScore && !$overallScore)
        {
            return '';
        }
        
        $ielts = 'Listening:&nbsp;' . $listeningScore . '<br/>';
        $ielts .= 'Reading:&nbsp;' . $readingScore . '<br/>';
        $ielts .= 'Writing:&nbsp;' . $writingScore . '<br/>';
        $ielts .= 'Speaking:&nbsp;' . $speakingScore . '<br/>';
        $ielts .= 'Overall:&nbsp;' . $overallScore;
            
        return $ielts;
    }

    static function printReviewerRanks($params) {
        extract($params);
        $rank = str_replace("|", "<br />" , $record['round1_point1_scores']);
        return $rank;
    }

    static function printComments($params) {
        global $round;
        extract($params);
        $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round1_comments']) );
        if ($round == 2 || $round == 3) {
            $comments .= "<br /><br />";
            $comments .= htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_comments']) );
            $comments .= "<br /><br />";
            $comments .= htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_pertinent_info']) );    
        }
        return $comments;
    }
    
    static function printPertinentInfo($params) {
        extract($params);
        $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['round2_pertinent_info']) );
        return $comments;
    } 

    static function printReviewers($params) {
        extract($params);
        $reviewers = '';
        $reviewerArray = explode('|', $record[$fieldName]);
        for ($i = 0; $i < count($reviewerArray); $i++) {
            $name = $reviewerArray[$i];
            $nameArray = explode(' ', $name);
            $reviewers .= '<a title="' . $name . '">' . end($nameArray) . '</a><br/>';
        }
        return $reviewers;
    }
    
    static function printMultiScore($params) {
        extract($params);
        $scores = '';
        $scoresArray = explode('|', $record[$fieldName]);
        for ($i = 0; $i < count($scoresArray); $i++) {
            $scoreArray = explode(':', $scoresArray[$i]);
            $name = $scoreArray[0];
            $nameArray = explode(' ', $name);
            //$scores .= '<a title="' . $name . '">' . end($nameArray) . '</a>: ' . $score . '<br/>';
            $scores .= end($nameArray);
            if ( isset($scoreArray[1]) ) {
                $scores .=  ': ' . $scoreArray[1] . '<br/>';  
            }
        }
        return $scores;
    }

    public static function printRounded($params, $args = array()) {
        extract($params);
        extract($args);
        
        $value = $record[$fieldName];
        if ($value) {
            $roundedValue = round($record[$fieldName], $precision);
            return number_format($roundedValue, $precision);    
        } else {
            return $value;
        }
        
        return round($record[$fieldName], 3);    
    }

    static function printAdmitTo($params) {
        
        extract($params);
        
        if ($record['admit_to_decision']) {
            $programString = $record['admit_to_decision'];
        } else {
            $programString = $record['admit_to'];    
        }
        
        $programsArray = explode('|', $programString);
        for ($i = 0; $i < count($programsArray); $i++) {
            $programsArray[$i] = str_replace('(wait)', '<i>(WAIT)</i>', $programsArray[$i]);        
        }
        $programs = implode('<br>', $programsArray);
        
        return $programs;
    }
    
    static function printRound3Entered($params) {
        
        extract($params);
        
        if ($record['status_time']) {
            $status_time = $record['status_time'];
        }  else {
              $status_time = "None";
        }
        
        return $status_time;
    }
    
}
?>