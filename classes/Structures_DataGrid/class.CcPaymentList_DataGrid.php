<?php
require_once("Structures/DataGrid.php");

/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for administerCcPayments.php 
*/

class CcPaymentList_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray , array(), 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', null, null, 
                array('width' => '2%', 'align' => 'left', 'valign' => 'top'), null,
                'CcPaymentList_DataGrid::printNumber()', $this->getCurrentRecordNumberStart())); 
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '18%', 'align' => 'left', 'valign' => 'top'), null, 
                'CcPaymentList_DataGrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('App Fee Status', 'fee_status', 'fee_status', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null));
       $this->addColumn(
            new Structures_DataGrid_Column('Payment ID', 'payment_id', 'payment_id', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null)); 
       $this->addColumn(
            new Structures_DataGrid_Column('Amount', 'payment_amount', 'payment_amount', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null)); 
       $this->addColumn(
            new Structures_DataGrid_Column('Date', 'payment_intent_date', 'payment_intent_date', 
                array('width' => '15%', 'align' => 'left', 'valign' => 'top'), null, 
                'CcPaymentList_DataGrid::printIntentDate()'));  
       $this->addColumn(
            new Structures_DataGrid_Column('Payment Status', 'payment_status', 'payment_status', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null)); 
       $this->addColumn(
            new Structures_DataGrid_Column('CCPS Status', 'ccps_status', 'ccps_status', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null,
                'CcPaymentList_DataGrid::printSummaryLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('<input type="checkbox" id="selectAllPayments">', null, null, 
                array('width' => '2%', 'align' => 'left', 'valign' => 'top'), null,
                'CcPaymentList_DataGrid::printCheckbox()'));

       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF', 'align' => 'left' ) );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    /*
    * ########## Callback methods #############
    */

    static function printNumber($params, $recordNumberStart) {
        extract($params);
        $number = $params['currRow'] + $recordNumberStart . ".";
        return $number;
    }    

    static function printFullName($params) {  

        global $unit;
        global $unitId;
        global $periodId;
        
        extract($params);

        //$name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">'; 
        //$name .= $record['name'] . '</span>';

        /*
        $name = '<a href="administerCcPayments.php?unit=' . $unit;
        $name .= '&amp;unitId=' . $unitId . '&amp;period=' . $periodId;
        $name .= '&amp;searchString=' . $record['application_id'];
        $name .= '" style="font-weight: bold;" ';
        $name .= 'title="Application ID: ' . $record['application_id'] . '">';
        $name .= $record['name'] . '</a>';
        */
        
        $href = $_SERVER['REQUEST_URI'] . '&amp;searchString=' . $record['application_id'];
        
        $name = '<a href="' . $href . '" style="font-weight: bold;" ';
        $name .= 'title="Application ID: ' . $record['application_id'] . '">';
        $name .= $record['name'] . '</a>';      

        return $name;
    }
    
    static function printApplicationLink($params) {  
    
        global $unit;
        global $unitId;
        global $periodId;
     
        extract($params); 
        $link = '<a href="administerApplications.php?unit=' . $unit;
        $link .= '&amp;unitId=' . $unitId . '&amp;period=' . $periodId;
        $link .= '&amp;searchString=' . $record['application_id'] . '" target="_blank">';
        $link .= $record['application_id'] . '</a>';     
        return $link;
    }

    static function printIntentDate($params) {
        extract($params);
        $intentDate = str_replace(' ', '&nbsp;', $record['payment_intent_date']);
        return $intentDate;    
    }
    
    static function printAuthNotifications($params) {
        extract($params);
        $authNotifications = str_replace('|', '<br>', $record['auth_notifications']);
        $authNotifications = str_replace(' ', '&nbsp;', $authNotifications);
        return $authNotifications;    
    }
    
    static function printSummaryLink($params) {
        extract($params);
        $paymentId = $record['payment_id'];
        $linkId = 'ccPaymentSummary_' . $paymentId;
        $link = '<a class="menu" href="javascript:;" title="View Details" id="' . $linkId . '">';
        $link .= $record['ccps_status'] . '</a>';
        return $link;
    }
    
    static function printCheckbox($params) {
        extract($params);
        $paymentId = $record['payment_id'];
        $checkboxId = 'checkbox_' . $paymentId;
        $checkbox = '<input class="paymentCheckbox shiftclick" type="checkbox" 
            name="payments[' . $paymentId . ']" id="' . $checkboxId . '">';
        return $checkbox;
    }

}
?>