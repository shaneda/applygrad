<?php

class ReviewListCsdMs_DataGrid extends ReviewList_DataGrid
{

    protected $myColumns = array(
        
        "application_id1" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),
            
        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),

        "application_id" => array("label" => "AppId", "title" => "Application ID",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gender" => array("label"=>"M/F", "title" => "Gender",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printAoi()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad", "title" => "Undergraduate Institution", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "title" => "Undergraduate GPA",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "title" => "GRE Verbal Score", 
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "title" => "GRE Quantitative Score",
            "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total1" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),
            
        "toefl_total2" => array("label"=>"IBT Tot", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_total')),

        "toefl_section1" => array("label"=>"IBT Spk", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_section1')),
            
        "toefl_section2" => array("label"=>"IBT Ln", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_section2')),

        "toefl_section3" => array("label"=>"IBT Rd", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_section3')),
            
        "toefl_essay" => array("label"=>"IBT Wrt", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewListCsd_DataGrid::printIBT()", 
            "formatterArg" => array('score' => 'toefl_essay')),
        
        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL), 

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "ct_recommendations_requested" => array("label"=>"Recs Rcd", "title" => "Recommendations Received", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printRecommendationsReceived()", "formatterArg" => NULL),
            
        "possible_advisors" => array("label"=>"Poss Advisors", "title" => "Possible Advisors",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),  
            
        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),
        
        "round2_reviewer_touched" => array("label" => "I Revd",  "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),
            
        "round2_reviewer_point1" => array("label"=>"My Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewListCsd_DataGrid::printCommitteeRank()", "formatterArg" => NULL),
        
        "round2_reviewer_rank" => array("label"=>"My Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "faculty",
            "formatter" => "ReviewListCsd_DataGrid::printFacultyRank()", "formatterArg" => NULL),

        "committee_reviewers" => array("label"=>"Revd By",  "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),
        
        "language_screen_requested" => array("label"=>"Spec Reqs",  "title" => "Special Requests",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecial()", "formatterArg" => NULL),
        
        "special_consideration_requested" => array("label"=>"Spec Cons",  "title" => "Special Consideration",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecialConsideration()", "formatterArg" => NULL),

        /*
        "round1_point1_average" => array("label"=>"Avg Rank", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => NULL, "formatterArg" => NULL),

        "round1_point1_scores" => array("label"=>"Rank/Conf", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        */
        
        "all_point1_committee_scores" => array("label"=>"Rank/Conf",  "title" => "Rank/Confidence",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),    
        
        "all_point1_committee_average" => array("label"=>"Avg Rank",  "title" => "Average Rank",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => FALSE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
            
        "votes_for_round2" => array("label"=>"Round 2", "title" => "Votes for Round 2",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultiScore()", "formatterArg" => NULL),        

        "round1_comments" => array("label"=>"Comments", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
        
        "round2_comments" => array("label"=>"Comments", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
            
        "round2_pertinent_info" => array("label"=>"Comments", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "faculty",
            "formatter" => "ReviewList_DataGrid::printPertinentInfo()", "formatterArg" => NULL),

        "full_reject" => array("label"=>"Decision Reject", "title" => "Rejected from All Programs",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee", 
            "formatter" => NULL, "formatterArg" => NULL),
            
        "decision" => array("label"=>"Decision", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "admit_to" => array("label"=>"Admit To", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "mergedFileDate" => array("label"=>"Merged File", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMergedDate()", "formatterArg" => NULL),
           
        "mergedFilePath" => array("label"=>"Add to Zip", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printZipAdd()", "formatterArg" => NULL)
             
        );
        
    public function __construct( $round = 1, $decisionRound = FALSE, $setSemiblind = FALSE, $semiblind = TRUE, 
                                    $facultyView = FALSE, $searchString = '', $selectedColumns = array(), 
                                    $limit = NULL, $page = NULL, $allCommentsOn = FALSE ) {
       
       if (isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] == 2)
       {
            $this->myColumns["possible_advisors"]["default"] = FALSE;
       }
                                        
        parent::__construct($round, $decisionRound, $setSemiblind, $semiblind, $facultyView,
            $searchString, $selectedColumns, $limit, $page, $allCommentsOn);
       
    }

    static function printAoi($params) {
        
        $csdAois = array(
            101 => "AC",
            102 => "AI",
            103 => "CB",
            104 => "CN",
            105 => "CA",
            106 => "D",
            107 => "FM",
            108 => "GT",
            109 => "GA",
            110 => "HCI",
            111 => "ML",
            112 => "MPC",
            113 => "N",
            114 => "OS",
            115 => "PL",
            116 => "PAL",
            117 => "R",
            118 => "SC",
            119 => "SP",
            120 => "SE",
            121 => "SNL",
            122 => "TS",
            123 => "V"
        );
        
        extract($params);
        $aoi = str_replace("|", "<br />" , $record[$fieldName]);
        $reviewerAoi = $record['reviewer_aoi'];
        if ($reviewerAoi != '') {
            if ($aoi != '') {
                $aoi .= '<br/>';
            }
            $reviewerAoiArray = explode('|', $reviewerAoi);
            for ($i = 0; $i < count($reviewerAoiArray); $i++) {
                $code = $csdAois[$reviewerAoiArray[$i]];
                $reviewerAoiArray[$i] = $code;    
            }
            $aoi .= implode(', ', $reviewerAoiArray);    
        }
        
        return $aoi;        
    }

    static function printIBT($params, $args) {
    
        extract($params);
        extract($args);
        
        $scoreValue = '';
        if ($record['toefl_type'] == 'IBT') {
            
            $scoreValue = $record[$score];
            
            if ($score == 'toefl_total') {
                $scoreValue = str_pad($scoreValue, 3, '0', STR_PAD_LEFT);    
            } elseif ($score == 'toefl_essay')  {
                $scoreValue = str_pad($scoreValue, 4, '0', STR_PAD_LEFT); 
            } else {
                $scoreValue = str_pad($scoreValue, 2, '0', STR_PAD_LEFT);    
            }
        }
            
        return $scoreValue;
    }
    
    static function printFacultyRank($params) 
    {
        extract($params);
        
        $score = NULL;
        if (isset($record['round2_reviewer_rank']))
        {
            $score = $record['round2_reviewer_rank'];
        }
        
        $display = '';
        switch ($score)
        {
            case '1':
            
                $display = '1 ';
                break;

            case '2':
            
                $display = '2 ';
                break;
                
            case '3':
            
                $display = '3 ';
                break;
                
            case '4':
            
                $display = '4 ';
                break;

            case '5th':
            
                $display = '5 ';
                break;
            
            case '0':
            
                $display = '6+';
                break;
            
            default:
        }
        
        return $display;
    }
}

?>