<?php

class ReviewListRi_DataGrid extends ReviewList_DataGrid
{

    protected $myColumns = array(
        
        "application_id" => array("label" => "row_number", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printNumber()", 
            "formatterArg" => "self::getCurrentRecordNumberStart()"),

        "lastname" => array("label" => "Name", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printEditLink()", "formatterArg" => NULL),
        
        "gender" => array("label"=>"M/F", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "programs" => array("label" => "Programs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "areas_of_interest" => array("label"=>"Interests", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "undergrad_institute_name" => array("label"=>"Undergrad", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gpa" => array("label"=>"GPA", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_verbal" => array("label"=>"GRE Vbl", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => NULL, "formatterArg" => NULL),
        
        "gre_quantitative" => array("label"=>"GRE Quant", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),

        "gre_writing" => array("label"=>"GRE AW", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => NULL, "formatterArg" => NULL),
        
        "toefl_total" => array("label"=>"TOEFL", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printTOEFL()", "formatterArg" => NULL),
        
        "ielts_overallscore" => array("label"=>"IELTS", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printIELTS()", "formatterArg" => NULL),  

        "recommenders" => array("label"=>"Recommenders", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
            
        "language_screen_requested" => array("label"=>" Spec Reqs", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printSpecial()", "formatterArg" => NULL),

        "round1_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "round2_groups" => array("label"=>"Groups", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),

        "round1_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "round2_reviewer_touched" => array("label" => "I Revd", "title" => "I have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => TRUE, "view" => "all", 
            "formatter" => "ReviewList_DataGrid::printTouched()", "formatterArg" => NULL),

        "committee_reviewers" => array("label"=>"Revd By", "title" => "Application has been reviewed by",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printReviewers()", "formatterArg" => NULL),

        "faculty_rankings" => array("label"=>"Fac Rev", "title" => "Faculty have reviewed this application",
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printFacultyReviews()", "formatterArg" => NULL),
            
        "possible_advisors" => array("label"=>"Poss Advisors", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2), "semiblind" => TRUE, "view" => "faculty",
            "formatter" => NULL, "formatterArg" => NULL),

        "round1_point1_average" => array("label"=>"Avg PhD Score", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,3), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printPhdAverage()", "formatterArg" => NULL),

        "round1_point2_average" => array("label"=>"Avg MS Score", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,3), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printMsAverage()", "formatterArg" => NULL),    

        "round1_point1_scores" => array("label"=>"PhD Scores", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printPhdScores()", "formatterArg" => NULL),
                    
        "round1_point2_scores" => array("label"=>"MS Scores", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printMsScores()", "formatterArg" => NULL),

        "all_point1_committee_scores" => array("label"=>"PhD Scores", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printPhdScores()", "formatterArg" => NULL),    
        
        "all_point2_committee_scores" => array("label"=>"MS Scores", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printMsScores()", "formatterArg" => NULL), 
        
        "all_point1_committee_average" => array("label"=>"Avg PhD Score", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewListRi_DataGrid::printPhdAverage()", "formatterArg" => NULL),       

        "all_point2_committee_average" => array("label"=>"Avg MS Score", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(2,3), "semiblind" => FALSE, "view" => "committee",
            "formatter" =>"ReviewListRi_DataGrid::printMsAverage()", "formatterArg" => NULL), 

        "round1_comments" => array("label"=>"Comments", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(1), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
        
        "round2_comments" => array("label"=>"Comments", "default" => FALSE, "required" => FALSE, 
            "rounds" => array(2), "semiblind" => FALSE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printComments()", "formatterArg" => NULL),
            
        "admit_to" => array("label"=>"Admit To", "default" => TRUE, "required" => TRUE, 
            "rounds" => array(3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMultivalue()", "formatterArg" => NULL),
        
        "ct_recommendations_requested" => array("label"=>"Recs Rcd", "title" => "Recommendations Received", 
            "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "all",
            "formatter" => "ReviewList_DataGrid::printRecommendationsReceived()", "formatterArg" => NULL),
            
        "mergedFileDate" => array("label"=>"Merged File", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printMergedDate()", "formatterArg" => NULL),
           
        "mergedFilePath" => array("label"=>"Add to Zip", "default" => TRUE, "required" => FALSE, 
            "rounds" => array(1,2,3), "semiblind" => TRUE, "view" => "committee",
            "formatter" => "ReviewList_DataGrid::printZipAdd()", "formatterArg" => NULL)                        
        );

        
        static function parseScores($scoresString) {
            if ($scoresString == '') {
                return $scoresString;
            } else {
                $scores = explode('|', $scoresString);
                $scoreCount = count($scores);
                for ($i = 0; $i < $scoreCount; $i++) {
                    $scoreArray = explode(': ', $scores[$i]);
                    if (count($scoreArray) < 2) {
                        continue;
                    }
                    $scoreArray[1] = number_format(floatval($scoreArray[1]), 2);
                    $scores[$i] = implode(": ", $scoreArray);
                }                
                return implode('<br/>', $scores);
            }
        }
        
        static function printPhdScores($params) {
            global $round;
            extract($params);
            $programs = $record['programs'];
            if ($round == 2 && !stristr($programs, 'Ph.D')) {
                $scores = '';    
            } else {
                $scores = ReviewListRi_DataGrid::parseScores($record[$fieldName]);    
            }
            return $scores;    
        }
        
        static function printMsScores($params) {
            global $round;
            extract($params);
            $programs = $record['programs'];
            if ($round == 2 && !stristr($programs, 'M.S.')) {
                $scores = '';    
            } else {
                $scores = ReviewListRi_DataGrid::parseScores($record[$fieldName]);
            }
            return $scores;    
        }

        static function printPhdAverage($params) {
            global $round;
            extract($params);
            $programs = $record['programs'];
            if ($round == 2 && !stristr($programs, 'Ph.D')) {
                $average = '';    
            } else {
                $average = $record[$fieldName];
            }
            return $average;            
        }

        static function printMsAverage($params) {
            global $round;
            extract($params);
            $programs = $record['programs'];
            if ($round == 2 && !stristr($programs, 'M.S.')) {
                $average = '';    
            } else {
                $average = $record[$fieldName];
            }
            return $average;             
        }
        
        static function printFacultyReviews($params) 
        {
            extract($params);
            
            $facultyReviews = '<div class="no">N</div>';
            if ($record['faculty_rankings']) {
                $facultyReviews = '<div class="yes">Y</div>';    
            }
            
            return $facultyReviews;             
        }
}
?>