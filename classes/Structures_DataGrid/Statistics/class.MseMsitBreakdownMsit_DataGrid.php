<?php
/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for applicantsByUnit view in statistics.php 
* 
* require_once("Structures/DataGrid.php");
*/

class MseMsitBreakdownMsit_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('Applied MSIT', 'submitted_count', 'submitted_count', 
                array(), null, null));

       $this->addColumn(
            new Structures_DataGrid_Column('MSIT Admitted MSIT', 
                'msit_admitted_msit_count', 'msit_admitted_msit_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('MSIT Waitlisted MSIT', 
                'msit_waitlisted_msit_count', 'msit_waitlisted_msit_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('MSIT Accepted MSIT', 
                'msit_accepted_msit_count', 'msit_accepted_msit_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('MSIT Deferred MSIT', 
                'msit_deferred_msit_count', 'msit_deferred_msit_count', 
                array(), null, null));

       $this->addColumn(
            new Structures_DataGrid_Column('MSE Admitted MSIT', 
                'mse_admitted_msit_count', 'mse_admitted_msit_count', 
                array(), null, 'MseMsitBreakdownMsit_DataGrid::printMseAdmitted()'));
       $this->addColumn(
            new Structures_DataGrid_Column('MSE Waitlisted MSIT', 
                'mse_waitlisted_msit_count', 'mse_waitlisted_msit_count', 
                array(), null, 'MseMsitBreakdownMsit_DataGrid::printMseWaitlisted()'));
       $this->addColumn(
            new Structures_DataGrid_Column('MSE Accepted MSIT', 
                'mse_accepted_msit_count', 'mse_accepted_msit_count', 
                array(), null, 'MseMsitBreakdownMsit_DataGrid::printMseAccepted()'));
       $this->addColumn(
            new Structures_DataGrid_Column('MSE Deferred MSIT', 
                'mse_deferred_msit_count', 'mse_deferred_msit_count', 
                array(), null, 'MseMsitBreakdownMsit_DataGrid::printMseDeferred()'));
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                ) 
            );
       $this->renderer->setTableOddRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                 )
            );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '1px');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    //########## Callback methods

    public static function printMseAdmitted($params) {
        extract($params);
        return $record['mse_admitted_msit_count'] + $record['mba_admitted_msit_count'];    
    }

    public static function printMseWaitlisted($params) {
        extract($params);
        return $record['mse_waitlisted_msit_count'] + $record['mba_waitlisted_msit_count'];    
    }

    public static function printMseAccepted($params) {
        extract($params);
        return $record['mse_accepted_msit_count'] + $record['mba_accepted_msit_count'];    
    }

    public static function printMseDeferred($params) {
        extract($params);
        return $record['mse_deferred_msit_count'] + $record['mba_deferred_msit_count'];    
    }
}
?>