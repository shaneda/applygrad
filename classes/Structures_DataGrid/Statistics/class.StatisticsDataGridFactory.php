<?php

class StatisticsDataGridFactory {
    
    public function createDataGrid($dataArray, $unitId, $view, $grain = NULL) {
        
        switch ($view) {

            case 'unitCrosstab':
            
                $datagrid = new UnitCrosstab_DataGrid($dataArray);
                break;

            case 'genderCitizenshipCrosstab':
            
                $datagrid = new GenderCitizenshipCrosstab_DataGrid($dataArray);
                break;
                
            case 'applicantsByCitizenship':
            
                $datagrid = new ApplicantsByCitizenship_DataGrid($dataArray);
                break;
                
            case 'applicantsByEthnicity':
            
                $datagrid = new ApplicantsByEthnicity_DataGrid($dataArray);
                break;
            
            case 'applicantsByGender':
            
                $datagrid = new ApplicantsByGender_DataGrid($dataArray);
                break;
            
            case 'applicantsByInstitution':
            
                $datagrid = new ApplicantsByInstitution_DataGrid($dataArray);
                break;

            case 'applicantsByNumPrograms':
            
                $datagrid = new ApplicantsByNumPrograms_DataGrid($dataArray);
                break;

            case 'mseMsitBreakdown':

                if ($grain == 'msit') {
                    $datagrid = new MseMsitBreakdownMsit_DataGrid($dataArray);    
                } elseif ($grain == 'mba') {
                    $datagrid = new MseMsitBreakdownMba_DataGrid($dataArray);    
                } else {
                    $datagrid = new MseMsitBreakdown_DataGrid($dataArray);
                }
                break;

            case 'applicantsByUnit':
            default:
            
                $datagrid = new ApplicantsByUnit_DataGrid($dataArray);
            
        }
        
        $datagrid->renderer->setTableAttribute('id', 'statisticsTable');
        
        return $datagrid;
    }
    
}
  
?>