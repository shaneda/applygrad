<?php
/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for applicationsByProgram view in statistics.php
* 
* require_once 'Structures/DataGrid.php'';    
*/

class GenderCitizenshipCrosstab_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);
       
       // Bind the data
       $this->bind($dataArray, array(), 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('Gender', 'gender', 'unit_name', 
                array(), NULL, NULL));
       $this->addColumn(
            new Structures_DataGrid_Column('US', 'submitted_us', 'submitted_us', 
                array(), NULL, NULL));
       $this->addColumn(
            new Structures_DataGrid_Column('Foreign', 'submitted_foreign', 'submitted_foreign', 
                array(), NULL, NULL));


       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                ) 
            );
       $this->renderer->setTableOddRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                 )
            );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '1px');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    //########## Callback methods

    
    static function printUnit($params) {
        extract($params);
        $unitName = '';
        $unitNameArray = explode(' ', $record[$fieldName]);
        $nameWordCount = count($unitNameArray);
        if ($nameWordCount  > 1 ) {
            for ($i = 0; $i < $nameWordCount; $i++) {
                $nameWord = trim($unitNameArray[$i]);
                $unitName .= $nameWord[0];     
            } 
            $unitName = $record[$fieldName];   
        } else {
            $unitName = $record[$fieldName];
        }
        return $unitName . '<br/>1st&nbsp;choice';
    }
    
}
?>