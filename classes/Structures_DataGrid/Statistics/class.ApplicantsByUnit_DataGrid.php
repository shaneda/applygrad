<?php
/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for applicantsByUnit view in statistics.php 
* 
* require_once("Structures/DataGrid.php");
*/

class ApplicantsByUnit_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('Unit', 'unit_name', 'unit_name', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Submitted', 'submitted_count', 'submitted_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('1st Choice', 'first_choice_count', 'first_choice_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Admitted', 'admitted_count', 'admitted_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Waitlisted', 'waitlisted_count', 'waitlisted_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Accepted', 'accepted_count', 'accepted_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Deferred', 'deferred_count', 'deferred_count', 
                array(), null, null));
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                ) 
            );
       $this->renderer->setTableOddRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                 )
            );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '1px');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    //########## Callback methods


}
?>