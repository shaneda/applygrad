<?php
/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for applicationsByProgram view in statistics.php
* 
* require_once 'Structures/DataGrid.php'';    
*/

class UnitCrosstab_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);
       
       // Bind the data
       $this->bind($dataArray, array(), 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'unit_name', NULL, 
                array('class' => 'rowHeading'), 
                NULL, 'UnitCrosstab_DataGrid::printUnit()'));
       foreach ($dataArray as $row) {
           $column = $row['unit_name'];
           $heading = $column . '<br/>2nd/3rd&nbsp;choice'; 
           $this->addColumn( new Structures_DataGrid_Column($heading, $column, NULL, 
                array('class' => 'crosstabCell'), NULL, NULL));     
       }     


       // Set the table attributes.
       /*
       $this->renderer->setTableHeaderAttributes( 
            array( 'valign' => 'bottom',
                'style' => 'background-color: #EEEEEE; color: black; font-weight: normal;') );
       */
       $this->renderer->setTableHeaderAttributes( 
            array( 'valign' => 'bottom', 'class' => 'columnHeading') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '1px');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    //########## Callback methods

    
    static function printUnit($params) {
        extract($params);
        $unitName = '';
        $unitNameArray = explode(' ', $record[$fieldName]);
        $nameWordCount = count($unitNameArray);
        if ($nameWordCount  > 1 ) {
            for ($i = 0; $i < $nameWordCount; $i++) {
                $nameWord = trim($unitNameArray[$i]);
                $unitName .= $nameWord[0];     
            } 
            $unitName = $record[$fieldName];   
        } else {
            $unitName = $record[$fieldName];
        }
        return $unitName . '<br/>1st&nbsp;choice';
    }
    
}
?>