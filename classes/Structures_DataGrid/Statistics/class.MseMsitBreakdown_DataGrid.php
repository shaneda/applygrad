<?php
/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for applicantsByUnit view in statistics.php 
* 
* require_once("Structures/DataGrid.php");
*/

class MseMsitBreakdown_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('Applied MSE', 'submitted_count', 'submitted_count', 
                array(), null, null));

       $this->addColumn(
            new Structures_DataGrid_Column('Admitted MSE', 
                'mse_admitted_mse_count', 'mse_admitted_mse_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Waitlisted MSE', 
                'mse_waitlisted_mse_count', 'mse_waitlisted_mse_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Accepted MSE', 
                'mse_accepted_mse_count', 'mse_accepted_mse_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Deferred MSE', 
                'mse_deferred_mse_count', 'mse_deferred_mse_count', 
                array(), null, null));

       $this->addColumn(
            new Structures_DataGrid_Column('Admitted MSIT', 
                'mse_admitted_msit_count', 'mse_admitted_msit_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Waitlisted MSIT', 
                'mse_waitlisted_msit_count', 'mse_waitlisted_msit_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Accepted MSIT', 
                'mse_accepted_msit_count', 'mse_accepted_msit_count', 
                array(), null, null));
       $this->addColumn(
            new Structures_DataGrid_Column('Deferred MSIT', 
                'mse_deferred_msit_count', 'mse_deferred_msit_count', 
                array(), null, null));
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                ) 
            );
       $this->renderer->setTableOddRowAttributes(
            array(
                'valign' => 'top', 
                'bgcolor' => '#EEEEEE', 
                'class' => 'menu',
                'onMouseOver' => 'this.style.backgroundColor=\'#FFFFFF\';',
                'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'
                 )
            );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '1px');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    //########## Callback methods


}
?>