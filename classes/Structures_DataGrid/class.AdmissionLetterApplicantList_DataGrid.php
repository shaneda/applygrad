<?php
require_once("Structures/DataGrid.php");

class AdmissionLetterApplicantList_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct($dataArray, $limit = NULL, $page = NULL, $allUsersSelected = NULL) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'AdminList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart()));  
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '20%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Admitted Program(s)', 'admitted_programs', 'admitted_programs', 
                array('width' => '20%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printPrograms()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Current Letter', 'letter', 'letter', 
                array('width' => '20%', 'align' => 'left'), null, 'AdmissionLetterApplicantList_DataGrid::printLetter()'));
       $this->addColumn(
            new Structures_DataGrid_Column("", null, null, 
                array('width' => '2%', 'align' => 'left', 'valign' => 'top'), null,
                'AdmissionLetterApplicantList_DataGrid::printSelectApplicationLink()'));
       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }
    
    /*
    * ########## Callback methods #############
    */
    static function printNumber($params, $recordNumberStart) {
        extract($params);
        $number = $params['currRow'] + $recordNumberStart . ".";
        return $number;
    }
    
    static function printFullName($params) 
    {   
        extract($params);
        $userId = $record['user_id'];
        $applicationId = $record['application_id'];
        
        $name = '<span id="applicantName_' . $record['application_id'] . '" style="font-weight: bold;">' . $record['name'] . '</span>';    
        $name .= '<br />';
        $name .= '<a class="menu print" href="javascript:;" id="print_'. $userId . '_' . $applicationId . '" ';
        $name .= 'title="Print Application ' . $applicationId . '">';
        $name .= 'View&nbsp;/&nbsp;Print</a>';

        return $name;
    }
    
    static function printPrograms($params) {
        extract($params);
        $programs = str_replace("|", "<br />" , $record[$fieldName]);
        return $programs;
    }
    
    static function printScholarshipAmt($params) {
        extract($params);
        $scholarshipAmt = number_format($record[$fieldName], 2);
        return $scholarshipAmt;
    }
    
    static function printLetter($params) 
    {
        extract($params);
        $fileName = $record['letter'];
        
        $letterLink = '';
        if ($fileName)
        {
            $guid = $record['guid'];
            $letterHref = 'fileDownload.php?file=' . urlencode($fileName) .'&amp;guid=' . $guid;
            $letterLink = '<a href="' . $letterHref . '" target="_blank">' . $fileName . '</a>';
        }

        return $letterLink;
    }

    static function printCheckbox($params) 
    {
        extract($params);
        $applicationId = $record['application_id'];
        $checkboxId = 'checkbox_' . $applicationId;
        
        $checkbox = '<input class="userCheckbox" type="radio" value="' . $applicationId . '" 
            name="applicationId" id="' . $checkboxId . '"';
            
        if ($record['selected'])
        {
            $checkbox .= ' checked="checked">';    
        }
        else
        {
            $checkbox .= '>';    
        }    
        
        return $checkbox;
    }
    
    static function printSelectApplicationLink($params) 
    {
        extract($params);
        $applicationId = $record['application_id'];
        $name = $record['name'];
        $linkId = 'selectApplicationId_' . $applicationId;
        
        $link = '<a class="selectApplicationId" name="selectApplicationId" id="' . $linkId . '" 
            href="" title="Create letter for ' . $name . ' (application ' . $applicationId . ')">Create&nbsp;Letter</a>';
            
        return $link;
    }
}
?>