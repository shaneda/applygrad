<?php
require_once("Structures/DataGrid.php");

/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for administerApplications.php 
*/

class AdminListIsret_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'AdminList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart())); 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'user_id', 'user_id', 
                array('width' => '10%', 'align' => 'left', 'valign' => 'top'), null,
                'AdminList_Datagrid::printPrintLink()')); 
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '17%', 'align' => 'left'), null, 'AdminList_Datagrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Organization', 'company', 'company', 
                array('width' => '17%', 'align' => 'center', 'valign' => 'top'), null)); 
       $this->addColumn(
            new Structures_DataGrid_Column('Submission Date', 'submitted_date', 'submitted_date', 
                array('width' => '12%', 'align' => 'center', 'valign' => 'top'), null));
       $this->addColumn(
            new Structures_DataGrid_Column('', '', '', 
                array('width' => '8%', 'align' => 'center'), null, 'AdminListIsret_Datagrid::printNotesLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'pd', 'pd', 
                array('width' => '8%', 'align' => 'center'), null, 'AdminList_Datagrid::printPaymentLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Complete', 'cmp', 'cmp', 
                array('width' => '7%', 'align' => 'center'), null, 'AdminList_Datagrid::printCompleteCheckbox()'));  

       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    /*
    * ########## Callback methods #############
    */
    static function printNotesLink($params) {
        extract($params);
        $link = '<a class="menu" href="javascript:;" title="Edit Notes" id="notes_';
        $link .= $record['application_id'] . '">Notes</a>';
        if ($record['hide'] || $record['admin_note_count']) {
            $link .= '<br /><span id="notesMessage_' . $record['application_id'];
            $link .= '" class="confirm">';    
        }
        if ($record['hide']) {
            $link .= 'Hidden<br/>';    
        }
        if ($record['admin_note_count']) {
            
            $link .= $record['admin_note_count'];
            if ($record['admin_note_count'] > 1) {
                $link .= ' notes</span>';
            } else {
                $link .= ' note</span>';    
            }
        }
        return $link;
    }
}
?>