<?php
require_once("Structures/DataGrid.php");

/*
* Create a datagrid with data bound and columns/callbacks/renderer set
* for administerApplications.php 
*/

class AdminListCnbcgt_DataGrid extends Structures_DataGrid
{ 

    // Construct with $dataArray so the row count is available for getCurrentRecordNumberStart
    public function __construct( $dataArray, $limit = NULL, $page = NULL ) {
       
       parent::__construct($limit, $page);

       // Bind the data
       $this->bind($dataArray ,array() , 'Array');
       
       // Set the table columns. 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'application_id', 'application_id', 
                array('width' => '1%', 'align' => 'right'), null,
                'AdminList_Datagrid::printNumber()', $this->getCurrentRecordNumberStart())); 
       $this->addColumn(
            new Structures_DataGrid_Column('', 'user_id', 'user_id', 
                array('width' => '9%', 'align' => 'left', 'valign' => 'top'), null,
                'AdminList_Datagrid::printPrintLink()')); 
       $this->addColumn(
            new Structures_DataGrid_Column('Name', 'name', 'name', 
                array('width' => '15%', 'align' => 'left'), null, 'AdminList_Datagrid::printFullName()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Date', 'submitted_date', 'submitted_date', 
                array('width' => '9%', 'align' => 'left'), null));
       $this->addColumn(
            new Structures_DataGrid_Column('Affiliation', 'cross_dept_progs', 'cross_dept_progs', 
                array('width' => '9%', 'align' => 'left'), null, 'AdminListCnbcgt_Datagrid::printAffiliation()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'gre_rcvd', 'gre_rcvd', 
                array('width' => '13%', 'align' => 'center'), null, 'AdminList_Datagrid::printTestScoresLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'trans_rcvd', 'trans_rcvd', 
                array('width' => '13%', 'align' => 'center'), null, 'AdminList_Datagrid::printTranscriptLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'ct_recommendations_requested', 'ct_recommendations_requested', 
                array('width' => '13%', 'align' => 'center'), null, 'AdminList_Datagrid::printRecommendationsLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('', 'pd', 'pd', 
                array('width' => '13%', 'align' => 'center'), null, 'AdminList_Datagrid::printPaymentLink()'));
       $this->addColumn(
            new Structures_DataGrid_Column('Complete', 'cmp', 'cmp', 
                array('width' => '5%', 'align' => 'center'), null, 'AdminList_Datagrid::printCompleteCheckbox()'));  

       
       // Set the table attributes.
       $this->renderer->setTableHeaderAttributes( 
            array('bgcolor' => '#990000', 'color' => '#FFFFFF') );
       $this->renderer->setTableEvenRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu') );
       $this->renderer->setTableOddRowAttributes(
            array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu' ) );
       $this->renderer->setTableAttribute('width', '100%');
       $this->renderer->setTableAttribute('border', '0');
       $this->renderer->setTableAttribute('cellspacing', '0');
       $this->renderer->setTableAttribute('cellpadding', '5px');
       $this->renderer->setTableAttribute('class', 'datagrid');
       $this->renderer->sortIconASC = '&uArr;';
       $this->renderer->sortIconDESC = '&dArr;';
    }

    /*
    * ########## Callback methods #############
    */

    static function printAffiliation($params)
    {   
        extract($params);
        
        $affiliations = $record['cross_dept_progs'];
        $affiliationsOther = $record['cross_dept_progs_other'];
 
        $returnString = '';
        $returnArray = array();
        if ($affiliations) {
            $affiliationsArray = explode(',', $affiliations);
            foreach ($affiliationsArray as $affiliation) {
                if (!in_array($affiliation, $returnArray)) {
                    $returnArray[] = $affiliation;    
                }
            }    
        }
        if ($affiliationsOther) {
            $returnArray[] = $affiliationsOther;   
        }
        if ( count($returnArray) > 0 ) {
            $returnString = implode(',', $returnArray);
        }
 
        return $returnString;
    }

}
?>