<?php
/*
* File conversion utility.
* File arguments should have full (non-relative) paths.
*/

class FileConverter
{
    
    public static function string2file($instring, $outfile) {
        
        if ($instring == '') {
            
            return FALSE;
            
        } else {
            
            $fp = fopen($outfile, "w");
            if ($fp === FALSE) {           
                 return FALSE;
            }
            
            $writeStatus = fwrite($fp, $instring);
            if ($writeStatus === FALSE) {          
                fclose($fp);           
                return FALSE;
            } 
                
            fclose($fp);        
            return TRUE;
        }    
    }


    public static function html2pdf($infile, $outfile, $paperSize='letter', $paperOrientation='portrait') {

        $dompdfPath = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/') 
                        . '../pdf/dompdf-0.5.1/dompdf.php';
        
        $command = $dompdfPath . ' ' . $infile;
        $command .= ' -f ' . $outfile . ' -p ' . $paperSize;
        $command .= ' -o ' . $paperOrientation . ' 2>&1';
        exec($command, $output, $error);
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }
    
    public static function html2pdf2($infile, $outfile, $paperSize='letter', $paperOrientation='portrait') {

        $dompdfPath = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/') 
                        . '../pdf/dompdf-0.5.2/dompdf.php';
        
        $command = $dompdfPath . ' ' . $infile;
        $command .= ' -f ' . $outfile . ' -p ' . $paperSize;
        $command .= ' -o ' . $paperOrientation . ' -v 2>&1';
        exec($command, $output, $error);
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }
    
    
    public static function text2pdf($infile, $outfile, $paperSize='letter', $paperOrientation='portrait') {
        return self::html2pdf($infile, $outfile, $paperSize, $paperOrientation);
    } 

    
    public static function doc2pdf($infile, $outfile) {

        /*
        * Check to see whether the open office service is listening on port 8100.
        * If it isn't, start it, do the conversion, and stop it.
        */
        $handleOoService = FALSE;
        $fp = @fsockopen('localhost', 8100);
        if (!$fp) {
            $handleOoService = TRUE;        
        }
        print_r($handleOoService);
        
        if ($handleOoService) {
            $ooStartStatus = self::startOoService();    
        }
        
        $baseDir = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');
        
        $converterPath = $baseDir . '../pdf/jodconverter-2.2.2/lib/jodconverter-cli-2.2.2.jar'; 
        $command = 'java -jar ' . $converterPath . ' ' . $infile . ' ' . $outfile . ' 2>&1'; 
        
        /*
        $converterPath = $baseDir . '../pdf/DocumentConverter.py';
        $command = 'python ' . $documentConverterPath;
        $command .= ' ' . $infile . ' ' . $outfile . ' 2>&1';
        */
        
        exec($command, $output, $error);

        if ($handleOoService) {
            $ooStopStatus = self::stopOoService();    
        }
        
        $status = array('error' => $error, 'output' => $output);
        return $status;        
    }        

    
    public static function image2pdf($infile, $outfile) {
        
        $command = 'convert ' . $infile . ' ' . $outfile . ' 2>&1';
        exec($command, $output, $error);        
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }


    public static function pdf2ps($infile, $outfile) {
        
        $command = 'pdftops ' . $infile. ' ' . $outfile . ' 2>&1';
        exec($command, $output, $error);        
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }    


    public static function pdf2text($infile) {
        
        // Outfile will have same filename as infile but with .txt extension.
        $command = 'pdftotext ' . $infile . ' 2>&1';
        exec($command, $output, $error);        
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }       

    public static function startOoService($port = '8100', $sleep = '5', $tempfile = 'ooffice_tmp') {

        if ($tempfile == 'ooffice_tmp') {
            $tempfile = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/') . $tempfile;    
        }

        exec("killall -u `whoami` Xvfb", $output, $error);
        exec("Xvfb :25 -screen 0 800x600x24  &", $output, $error);
        exec("killall -u `whoami` -q soffice", $output, $error);
        $ooCommand = 'ooffice -accept="socket,host=localhost,port=' . $port;
        $ooCommand .= ';urp;OpenOffice.ServiceManager" -norestore -nofirststartwizard';
        $ooCommand .= ' -nologo -invisible -display :25 > ' . $tempfile . ' &'; 
        exec($ooCommand, $output, $error);
        exec('sleep ' . $sleep, $output, $error);
        
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }

    public static function stopOoService() {
        
        exec("killall -u `whoami` -q soffice", $output, $error);
        exec("killall -u `whoami` Xvfb", $output, $error);
        
        $status = array('error' => $error, 'output' => $output);
        return $status;
    }

}
?>