<?php

class RegistrationListData
{
    
    private $unit;
    private $unitId;
    private $data;          // 2D array
    
    public function __construct($unit, $unitId) {
        $this->unit = $unit;
        $this->unitId = $unitId;    
    }
    
    public function getData($admissionPeriodId = NULL, $applicationId = NULL, 
                                $searchString = '', $submissionStatus = 'all',
                                $completionStatus = 'all', $paymentStatus = 'all',
                                $testScoreStatus = 'all', $transcriptStatus = 'all',
                                $recommendationStatus = 'all', $round = 'all', 
                                $luUsersUsertypesId = NULL) {
                                    
        // Only base view find method has all limit parameters.
        $baseView = new VW_RegistrationListBase();
        $baseArray = $baseView->find($this->unit, $this->unitId, $admissionPeriodId, 
                                        $applicationId, $searchString, $submissionStatus,
                                        $completionStatus, $paymentStatus, $testScoreStatus,
                                        $transcriptStatus, $recommendationStatus,
                                        $luUsersUsertypesId);

        /* 
        * Only limit the other queries by $admissionPeriodId and $applicationId 
        * to speed things up by avoiding unnecessary joins.
        */ 
        $domainView = new VW_AdminListDomain();
        $domainArray = $domainView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $periodView = new VW_AdminListPeriod();
        $periodArray = $periodView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);
        
        // Get all programs selected for the given period, not just those that are in the admin unit.
        $programsView = new VW_AdminListPrograms();
        $programsArray = $programsView->find(NULL, NULL, $admissionPeriodId, 
            $applicationId, '', 'all', $luUsersUsertypesId);

        $returnArray = array();
        $baseCount = count($baseArray);
        for ($i = 0; $i < $baseCount; $i++) {
            
            $baseApplicationId = $baseArray[$i]['application_id'];

            if ( array_key_exists($baseApplicationId, $domainArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $domainArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['application_domain'] = 0;
            }

            if ( array_key_exists($baseApplicationId, $periodArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $periodArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['period_id'] = 0;
                $baseArray[$i]['umbrella_name'] = '';
                $baseArray[$i]['unit_name_short'] = '';
            }            

            if ( array_key_exists($baseApplicationId, $programsArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $programsArray[$baseApplicationId]);    
            } else {
                $baseArray[$i]['programs'] = '';
            }
            
            $returnArray[] = $baseArray[$i];
        }  
        
        return $returnArray;
    }
}
?>
