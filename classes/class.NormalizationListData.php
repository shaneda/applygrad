<?php
/*
* Create a data set for round 2 with scores normalized using CSD business rules.
*/
            
include_once "../inc/specialCasesAdmin.inc.php";
include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_ReviewList.php";
include "../classes/DB_Applyweb/class.VW_ReviewListBase.php";
include "../classes/DB_Applyweb/class.VW_ReviewListPrograms.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAdmissionStatus.php";

class NormalizationListData
{
    
    private $departmentId;
    private $periodId;
    private $programId;
    private $score;

    private $reviews = array();                 // 2D
    private $scores = array();                  // 2D 
    private $normalizations = array();          // 2D 
    
    private $scoresByApplication = array();     // 'application_id' => array('score1' => array(), 'score2' => array())
    private $scoresByReviewer = array();        // 'reviewer_id' => array('score1' => array(), 'score2' => array())
    private $scoresAll = array(
        'score1' => array('count' => 0, 'sum' => 0),
        'score2' => array('count' => 0, 'sum' => 0)        
        );             
      

    public function __construct($departmentId, $periodId = NULL,  $programId = NULL, $round = NULL) {
        
        $this->departmentId = $departmentId;
        $this->periodId = $periodId;
        $this->programId = $programId;
        $this->round = $round;

        $this->loadApplications();
        $this->loadReviews();
        $this->calculateScores(1);
        $this->calculateScores(2);            
    }    
    
    private function loadApplications() {

        $VW_ReviewListBase = new VW_ReviewListBase();
        $baseArray = $VW_ReviewListBase->find($this->periodId, $this->departmentId, $this->round);

        $VW_ReviewListPrograms = new VW_ReviewListPrograms();
        $programArray = 
            $VW_ReviewListPrograms->find($this->periodId, $this->departmentId, $this->round);

        $VW_ReviewListAdmissionStatus = new VW_ReviewListAdmissionStatus();
        $admissionStatusArray = 
            $VW_ReviewListAdmissionStatus->find($this->periodId, $this->departmentId, $this->round);
        
        $summaryArray = array( 
            'count' => 0, 
            'sum' => 0, 
            'sum_weighted' => 0
            );

        // Include MBA/MSE with MSE
        if ($this->programId == 21) {
            $this->programId = array(21, 30);    
        }
        
        if ($this->programId) {
            
            if ( $this->isRiRound2() ) {
                
                $programApplicationQuery = "SELECT promotion_history.application_id
                                            FROM promotion_history
                                            INNER JOIN (
                                              SELECT application_id, program_id,
                                              MAX(status_time) AS latest_status
                                              FROM promotion_history";
                if ( is_array($this->programId) ) {
                    $programApplicationQuery .= " WHERE program_id IN (" . implode(',', $this->programId) . ")";    
                } else {
                    $programApplicationQuery .= " WHERE program_id = " . $this->programId;    
                }
                $programApplicationQuery .= " GROUP BY application_id, program_id
                                            ) AS program_status
                                                ON promotion_history.application_id = program_status.application_id
                                                AND promotion_history.program_id = program_status.program_id";        
                if ( is_array($this->programId) ) {
                    $programApplicationQuery .= " WHERE promotion_history.program_id IN (" . implode(',', $this->programId) . ")";    
                } else {
                    $programApplicationQuery .= " WHERE promotion_history.program_id = " . $this->programId;    
                }
                $programApplicationQuery .= " AND promotion_history.status_time = program_status.latest_status
                                                AND promotion_history.round = 2
                                                AND promotion_history.promotion_method = 'promotion'";                
                
            } else {
                
                $programApplicationQuery = "SELECT application_id FROM lu_application_programs";
                if ( is_array($this->programId) ) {
                    $programApplicationQuery .= " WHERE program_id IN (" . implode(',', $this->programId) . ")";    
                } else {
                    $programApplicationQuery .= " WHERE program_id = " . $this->programId;    
                }                            

            }
            
            if ($this->programId == 29) {
                
                // Get MSIT scores for the MSE rejects 
                $programApplicationQuery .= " 
                    UNION (
                    SELECT DISTINCT lu_application_programs.application_id 
                    FROM lu_application_programs
                    LEFT OUTER JOIN review 
                        ON lu_application_programs.application_id = review.application_id
                    LEFT OUTER JOIN application_decision
                        ON lu_application_programs.application_id = application_decision.application_id
                        AND lu_application_programs.program_id = application_decision.program_id    
                    WHERE 
                    (lu_application_programs.program_id = 21
                        OR lu_application_programs.program_id = 30)
                    AND 
                    (
                        (
                        (lu_application_programs.admission_status IS NULL
                        OR lu_application_programs.admission_status = '0')
                        AND review.point = 4
                        AND review.point2 IS NOT NULL
                        )
                        OR
                        (
                        application_decision.admission_program_id = 29
                        AND
                        (application_decision.admission_status = 1
                        OR application_decision.admission_status = 2)
                        )    
                    )
                    )
                ";
                
            }            

            $programApplications = $VW_ReviewListBase->handleSelectQuery($programApplicationQuery, 'application_id');
            
        }  
        
        foreach ($baseArray as $applicationBase) {
            
            $applicationId = $applicationBase['application_id'];
            
            if ($this->programId) {
                if ( !array_key_exists($applicationId, $programApplications) ) {
                    continue;
                }
            }
            
            $applicationRecord = array_merge($applicationBase, $programArray[$applicationId]);
            
            
            if ( array_key_exists($applicationId, $admissionStatusArray) ) {
                $applicationRecord['admit_to'] = $admissionStatusArray[$applicationId]['admit_to']; 
                $applicationRecord['admit_to_decision'] = 
                    $admissionStatusArray[$applicationId]['admit_to_decision'];           
            } else {
                $applicationRecord['admit_to'] = ''; 
                $applicationRecord['admit_to_decision'] = '';   
            }
            
            $this->scores[$applicationId] = $applicationRecord;
            
            $this->scoresByApplication[$applicationId]['score1'] = $summaryArray;
            $this->scoresByApplication[$applicationId]['score2'] = $summaryArray;        
        }        
        
        return TRUE;
    }
     
    private function loadReviews() {
        
        $DB_Applyweb = new DB_Applyweb();
        $reviewQuery = "SELECT users.firstname, users.lastname,
                        review.id,
                        review.application_id,
                        review.reviewer_id,";
                        if (($this->departmentId == 3 || $this->departmentId == 66)) {
                            $reviewQuery .= " ROUND(review.point, 2) AS point,
                            ROUND(review.point_certainty, 1) AS point_certainty,
                            ROUND(review.point2, 2) AS point2,
                            ROUND(review.point2_certainty, 1) AS point2_certainty,";    
                        } else {
                            $reviewQuery .= " review.point,
                            review.point_certainty,
                            review.point2,
                            review.point2_certainty,";
                        }                        
          $reviewQuery .= " review.round
                        FROM review
                        INNER JOIN lu_users_usertypes 
                            ON review.reviewer_id = lu_users_usertypes.id
                            AND lu_users_usertypes.usertype_id = 2
                        INNER JOIN users 
                            ON lu_users_usertypes.user_id = users.id
                        INNER JOIN period_application 
                            ON review.application_id = period_application.application_id";
        if (is_array($this->periodId)) {
            $reviewQuery .= " AND period_application.period_id  
                IN (" . implode(',', $this->periodId) . ")";    
        } else {
            $reviewQuery .= " AND period_application.period_id = " . $this->periodId;    
        }                  
        $reviewQuery .= " WHERE review.department_id = " . $this->departmentId;
        if ($this->departmentId != 1 && $this->departmentId != 3 && $this->round) {
            if ($this->round == 1) {
                $reviewQuery .= " AND review.round = " . $this->round;    
            }     
        }
        $reviewQuery .= " AND review.fac_vote = '0'
                        AND !(review.point IS NULL AND review.point2 IS NULL)
                        ";
        $reviewQuery .= " ORDER BY users.lastname, users.firstname";                
        $this->reviews = $DB_Applyweb->handleSelectQuery($reviewQuery);

        return TRUE;
    }

    private function calculateScores($scoreType = 1) {
        
        if ($scoreType == 2) {
            $reviewField = 'point2';
            $scoreIndex = 'score2';            
        } else {
            $reviewField = 'point';
            $scoreIndex = 'score1';
        }
        
        foreach ($this->reviews as $review) {
    
            $applicationId = $review['application_id'];
            $reviewerId = $review['reviewer_id'];
            $round = $review['round'];
            
            // Skip scores for non-round2 applicants.
            if ( !array_key_exists($applicationId, $this->scoresByApplication) ) {
                continue;
            }
            
            /*
            * When getting scores for the MSIT program take as the score 
            * the point2 score, which will only be set for MSE rejects.
            */
            if ($this->programId == 29) {
                if ($review['point2']) {    
                    // This *has* to be a review for a MSE reject
                    if ($scoreType == 2) {
                        // point2/score2 should always be null for MSIT
                        $score = NULL;    
                    } else {
                        // Use the point2 score as the point1 score,
                        // bumping it by 1, because the MSIT scale in the
                        // MSE review starts at 'admit' instead of 'definite'

                        // OOOPS! no need to bump it any more
                        $score = $review['point2'];
                            
                    }   
                } else {
               
                    // Make sure this is not an MSE review.
                    $mseReview = FALSE;
                    $programsArray = explode('|', $this->scores[$applicationId]['programs']);
                    foreach ($programsArray as $programString) {
                        $programArray = explode('(', $programString);
                        $program = $programArray[0];
                        if ( trim($program) == 'Master of Software Engineering' ) {
                            $mseReview = TRUE;
                            continue;    
                        }
                    }
                    
                    if ($mseReview) {
                        // This is an MSE score, so don't count it.
                        $score = NULL;    
                    } else {
                        // This is an MSIT (non-MSE) score, so count it.
                        $score = $review[$reviewField];     
                    }
   
                }     
            } else {
                $score = $review[$reviewField];
            }
            
            // Skip reviews with empty scores.
            if (!$score) {
                continue;
            }
            
            // Fill (overwrite) the reviewer name fields.
            $firstName = $review['firstname'];
            $lastName = $review['lastname'];
            $firstInitial = strtoupper( substr($firstName, 0, 1) );
            $lastInitial = strtoupper( substr($lastName, 0, 1) );
            $this->scoresByReviewer[$reviewerId]['firstname'] = $firstName;
            $this->scoresByReviewer[$reviewerId]['lastname'] = $lastName;
            $this->scoresByReviewer[$reviewerId]['fullname'] = $firstName . ' ' . $lastName;
            $this->scoresByReviewer[$reviewerId]['initials'] = $firstInitial . $lastInitial;   
            
            // If an application has multiple scores from the same reviewer, take the round2 score.
            if ( isset($this->scoresByApplication[$applicationId][$scoreIndex][$reviewerId]) ) {
                
                if ( $round == 1 ) {
                    
                    // Skip this review: the previously recorded score is the round2 score.
                    continue;
                    
                } else {
                    
                    // Correct the previously recorded score stats so the "extra" score is not counted.
                    $previousScore = $this->scoresByReviewer[$reviewerId]['reviews'][$applicationId][$reviewField];
                    
                    $this->scoresByReviewer[$reviewerId][$scoreIndex]['count']--;
                    $this->scoresByReviewer[$reviewerId][$scoreIndex]['sum'] -= $previousScore;
                    
                    $this->scoresAll[$scoreIndex]['count']--;
                    $this->scoresAll[$scoreIndex]['sum'] -= $previousScore;
                    
                    $this->scoresByApplication[$applicationId][$scoreIndex]['count']--;
                    $this->scoresByApplication[$applicationId][$scoreIndex]['sum'] -= $previousScore;             
                }    
            }

            // Add the review to the reviewer's array.
            $this->scoresByReviewer[$reviewerId]['reviews'][$applicationId] = $review;

            // Add the score to the application's score array.
            $this->scoresByApplication[$applicationId][$scoreIndex][$reviewerId] = $score;
             
            // Update the total score stats.
            $this->scoresAll[$scoreIndex]['count']++;
            $this->scoresAll[$scoreIndex]['sum'] += $score;
            
            // Update the application's score stats.
            $this->scoresByApplication[$applicationId][$scoreIndex]['count']++;
            $this->scoresByApplication[$applicationId][$scoreIndex]['sum'] += $score;

            // Update the reviewer's score stats. 
            if ( isset($this->scoresByReviewer[$reviewerId][$scoreIndex]['sum']) ) {
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['count']++;
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['sum'] += $score;    
            } else {
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['count'] = 1;
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['sum'] = $score;
            }
            $this->scoresByReviewer[$reviewerId][$scoreIndex]['average'] = 
                ($this->scoresByReviewer[$reviewerId][$scoreIndex]['sum'] / 
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['count']); 
        }        

        // Update the reviewer's weight factor            
        foreach ($this->scoresByReviewer as $reviewerId => $reviewerRecord) {
            
            if ( !isset($reviewerRecord[$scoreIndex]) ) {
                continue;
            }
            
            $reviewerCount = $reviewerRecord[$scoreIndex]['count'];
            $reviewerSum = $reviewerRecord[$scoreIndex]['sum'];
            $reviewerAverage = $reviewerRecord[$scoreIndex]['average'];
             
            $otherScoreCount = $this->scoresAll[$scoreIndex]['count'] - $reviewerCount;
            $otherScoreSum = $this->scoresAll[$scoreIndex]['sum'] - $reviewerSum;                  
            if ($otherScoreCount <= 0 ) {
                $otherScoreAverage = 0;
            } else {    
                $otherScoreAverage = $otherScoreSum / $otherScoreCount; 
            }
            
            $this->scoresByReviewer[$reviewerId][$scoreIndex]['other_count'] =
                $otherScoreCount;
            $this->scoresByReviewer[$reviewerId][$scoreIndex]['other_sum'] =
                $otherScoreSum;                        
            if ($otherScoreAverage == 0) {
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['weight_factor'] = 1;    
            } else {
                $this->scoresByReviewer[$reviewerId][$scoreIndex]['weight_factor'] = 
                    $otherScoreAverage / $reviewerAverage;
            }
            $this->scoresByReviewer[$reviewerId][$scoreIndex]['last_review_id'] = $review['id'];
        }
        
        return TRUE;        
    }

    public function getScores($scoreType = 1) {
    
        if ( count($this->scoresByApplication) == 0 ) {
            return $this->scores;
        }

        if ($scoreType == 2) {
            $scoreIndex = 'score2';            
        } else {
            $scoreIndex = 'score1';
        }

        foreach ($this->scoresByApplication as $applicationId => $applicationScore) {

            $name = $this->scores[$applicationId]['name'];
            $numScores = $applicationScore[$scoreIndex]['count'];
            $sumScores = $applicationScore[$scoreIndex]['sum'];
            if ($numScores > 0) {
                $averageScore = $sumScores / $numScores;    
            } else {
                //$averageScore = 0;
                // Kluge - give apps with no scores 999 for their averages.
                $averageScore = 999;    
            }
            
            // Set the fields in the application array.
            $this->scores[$applicationId]['score_count'] = $numScores;
            $this->scores[$applicationId]['score_sum'] = $sumScores;
            $this->scores[$applicationId]['score_average'] = $averageScore;
            $this->scores[$applicationId]['score_sum_weighted'] = 0;
            $this->scores[$applicationId]['score_average_weighted'] = 0;
            
            // Add the scores to the applications array.
            foreach ($this->scoresByReviewer as $reviewerId => $reviewerScore) {
                $reviewerInitials = $reviewerScore['initials'];
                $reviewerName = $reviewerScore['fullname'];
                $reviewerIndex = $reviewerInitials . '|' . $reviewerName . '|' . $reviewerId;
                if ( isset($applicationScore[$scoreIndex][$reviewerId]) ) {
                    $score = $applicationScore[$scoreIndex][$reviewerId];
                    $this->scores[$applicationId][$reviewerIndex] = $score;
                    $this->scores[$applicationId]['score_sum_weighted'] += 
                        ($score * $reviewerScore[$scoreIndex]['weight_factor']);    
                } else {
                    $this->scores[$applicationId][$reviewerIndex] = '';
                }
            }
            
            // Compute, set the weighted average score.
            if ($numScores > 0) {
                $averageScoreWeighted = 
                    $this->scores[$applicationId]['score_sum_weighted'] / $numScores;        
            } else {
                //$averageScoreWeighted = 0;
                // Kluge - give apps with no scores 999 for their averages. 
                $averageScoreWeighted = 999;
            }
            $this->scores[$applicationId]['score_average_weighted'] = $averageScoreWeighted;

            // Fill array to sort by weighted average to get weighted rank.
            $weightedSortArray[$applicationId]['application_id'] = $applicationId;
            $weightedSortArray[$applicationId]['name'] = $name;
            $weightedSortArray[$applicationId]['average_score_weighted'] = $averageScoreWeighted;
            
            // Fill arrays to sort against.
            $names[$applicationId] = $name;
            $names2[$applicationId] = $name;    // The 2nd sort doesn't work if you re-use $names    
            $average_scores[$applicationId]  = $averageScore;
            $average_scores_weighted[$applicationId]  = $averageScoreWeighted; 
        }
        
        // Sort according to weighted average and get the weighted rank.
        if ($this->departmentId == 3 || $this->departmentId == 66 || $this->departmentId == 50
            || isDesignDepartment($this->departmentId) || isDesignPhdDepartment($this->departmentId) || isDesignDdesDepartment($this->departmentId)) {
            // RI is unique in that higher scores are better.
            array_multisort($average_scores_weighted, SORT_DESC, $names, SORT_ASC, $weightedSortArray);    
        } else {
            array_multisort($average_scores_weighted, SORT_ASC, $names, SORT_ASC, $weightedSortArray);    
        }
        
        $rank = 1;
        $previousAverage = NULL;
        foreach ($weightedSortArray as $index => $record) {
            $applicationId = $record['application_id'];
            if ($record['average_score_weighted'] !== $previousAverage) {
                $rank = $index + 1;            
            }
            $previousAverage = $record['average_score_weighted']; 
            $this->scores[$applicationId]['rank_weighted'] = $rank;
            // De-klugify the no score apps
            if ($this->scores[$applicationId]['score_average_weighted'] == 999) {
                $this->scores[$applicationId]['score_average_weighted'] = NULL;
            }   
        }

        // Finally, sort according to average score and display.
        if ($this->departmentId == 3 || $this->departmentId == 66 || $this->department = 50
            // added this for design scale 
            || isDesignDepartment($this->departmentId) || isDesignPhdDepartment($this->departmentId) || isDesignDdesDepartment($this->departmentId)
            ) {
            //  higher scores are better.
            array_multisort($average_scores, SORT_DESC, $names2, SORT_ASC, $this->scores);    
        } else {
            array_multisort($average_scores, SORT_ASC, $names2, SORT_ASC, $this->scores);    
        }
        $rank = 1;
        $previousAverage = NULL;
        foreach ($this->scores as $index => $applicationRecord) {
            $number = $index + 1;
            $average = $applicationRecord['score_average'];
            if ($average != $previousAverage) {
                $rank = $number;            
            }
            $previousAverage = $average;
            $this->scores[$index]['rank'] = $rank;
            // De-klugify the no score apps
            if ($this->scores[$index]['score_average'] == 999) {
                $this->scores[$index]['score_average'] = NULL;
            }  
        }  

        return $this->scores;
    }

    public function getNormalizations($scoreType = 1) {

        if ( count($this->scoresByReviewer) == 0 ) {
            return $this->normalizations;
        }

        if ($scoreType == 2) {
            $scoreIndex = 'score2';            
        } else {
            $scoreIndex = 'score1';
        }
        
        foreach ($this->scoresByReviewer as $reviewerId => $reviewerScore) {
            
            $name = $reviewerScore['fullname'];
            $initials = $reviewerScore['initials'];
            
            if (array_key_exists($scoreIndex, $reviewerScore)) {
                $scoreSum = $reviewerScore[$scoreIndex]['sum'];
                $scoreCount = $reviewerScore[$scoreIndex]['count'];
                $scoreAverage = $reviewerScore[$scoreIndex]['average'];
                $weightFactor = $reviewerScore[$scoreIndex]['weight_factor'];
            } else {
                $scoreSum = NULL;
                $scoreCount = 0;
                $scoreAverage = NULL;
                $weightFactor = NULL;    
            }
            
            $this->normalizations[] = array(
                'reviewer_id' => $reviewerId,
                'name' => $name,
                'initials' => $initials,
                'score_sum' => $scoreSum,
                'score_count' => $scoreCount,
                'score_average' => $scoreAverage,
                'weight_factor' => $weightFactor
                );
        }
        
        $allScoreSum = $this->scoresAll[$scoreIndex]['sum'];
        $allScoreCount = $this->scoresAll[$scoreIndex]['count'];
        
        if ($allScoreCount > 0) {
            $allScoreAverage = $allScoreSum / $allScoreCount;    
        } else {
            $allScoreAverage = 0;   
        }
        
        $this->normalizations[] = array(
            'reviewer_id' => NULL,
            'name' => 'All Reviewers',
            'initials' => 'All',
            'score_sum' => $allScoreSum,
            'score_count' => $allScoreCount,
            'score_average' => $allScoreAverage,
            'weight_factor' => NULL
            );
    
        return $this->normalizations;            
    }

    public function getReviews() {
        return $this->reviews;
    }

    public function getScoresByReviewer() {
        return $this->scoresByReviewer;
    }    

    public function getScoresByApplication() {
        return $this->scoresByApplication;
    }
    
    private function isRiRound2() {
        
        if ($this->round != 2) {
            return FALSE;
        }

        if ( is_array($this->programId) ) {         
            $riProgramIds = array(4, 12, 17);
            foreach ($this->programId as $programId) {
                if ( !in_array($programId, $riProgramIds) ) {
                    return FALSE;
                }
            } 
            return TRUE;
        }        

        if ($this->programId == 4) {
            return TRUE;
        }
        
        if ($this->programId == 12) {
            return TRUE;
        }

        if ($this->programId == 17) {
            return TRUE;
        }
                
        return FALSE;
    }

}
?>