<?php

class RegistrationFeePaymentManager {

    private $DB_RegistrationFeeStatus;
    private $DB_RegistrationFeePayment;
    
    private $applicationId;
    private $departmentId;
    private $registrationFeeStatus;     // 1D array
    private $payments;                  // 2D array, indexed by id

    private $newPaymentId = NULL;       // NULL|int
    
    public function __construct($applicationId, $departmentId) 
    {    
        $this->DB_RegistrationFeeStatus = new DB_RegistrationFeeStatus();
        $this->DB_RegistrationFeePayment = new DB_RegistrationFeePayment();
        
        $this->applicationId = $applicationId;
        $this->departmentId = $departmentId;
        
        $this->loadRegistrationFeeStatus(); 
        $this->loadPayments();
    }
    
    public function getApplicationId() {
        return $this->applicationId;
    }
      
    public function getPayments() {
        return $this->payments;
    }
            
    public function getTotalPayments() {
    
        $totalPayments = 0;
        foreach ($this->payments as $row){
            if ($row['payment_status'] == 'pending' || $row['payment_status'] == 'paid') {
                $totalPayments += $row['payment_amount'];
            }    
        }
        return $totalPayments;
    }    
    
    public function getTotalPaid() {
    
        $totalPaid = 0;
        foreach ($this->payments as $row){            
            if ($row['payment_status'] == 'paid') {
                $totalPaid += $row['payment_amount'];    
            }
        }
        
        return $totalPaid;
    }
    
    public function getFeeAmount() {
      return $this->registrationFeeStatus['amount'];
      //return 100.00;
    }
    
    public function getFeePaid()
    {
        return $this->registrationFeeStatus['paid'];
    }
    
    public function getFeeWaived()
    {
        return $this->registrationFeeStatus['waived'];
    }
    
    public function getBalanceDue() {
        
        $totalFee = $this->getFeeAmount();
        $totalPayments = $this->getTotalPayments();
        $difference = $totalFee - $totalPayments;
        
        if ($difference > 0) {
            $balanceDue = $difference;
        } else {
            $balanceDue = 0;    
        }
        
        return $balanceDue;       
    }

    public function getBalanceUnpaid() {

        $totalFee = $this->getFeeAmount();
        $totalPaid = $this->getTotalPaid();
        $difference = $totalFee - $totalPaid;

        if ($difference > 0) {
            $balanceUnpaid = $difference;
        } else {
            $balanceUnpaid = 0;    
        }
        
        return $balanceUnpaid;       
    }

    public function getNewPaymentId() {
        return $this->newPaymentId;
    }
    
    public function savePayment($paymentRecord = array()) {
        
        $paymentId = NULL;
        if ( isset($paymentRecord['id']) ) {
            $paymentId = $paymentRecord['id'];
        }
        
        $payment = new RegistrationFeePayment($paymentId, $this->applicationId, $this->departmentId);
        $payment->importValues($paymentRecord);
        $payment->save();
        
        if (!$paymentId) {
            $this->newPaymentId = $payment->getId();    
        }
        
        // Get the latest db state
        $this->loadPayments();        

        // Update the fee status
        $paid = $this->getBalanceUnpaid() ? 0 : 1;
        $this->saveFeePaid($paid);
        
        return TRUE;
    }
    
    public function saveFeePaid($paid)
    {
        $this->registrationFeeStatus['paid'] = $paid ? 1 : 0;
        $this->DB_RegistrationFeeStatus->save($this->registrationFeeStatus);
        return TRUE; 
    }
    
    public function saveFeeWaived($waived) 
    {
        $this->registrationFeeStatus['waived'] = $waived ? 1 : 0;
        $this->DB_RegistrationFeeStatus->save($this->registrationFeeStatus);
        return TRUE;    
    }
    
    public function deletePayment($paymentId) {
    
        $numAffectedRows = 0;
        $numAffectedRows += $this->DB_RegistrationFeePayment->delete($paymentId);

        // Get the latest db state
        $this->loadPayments();

        return TRUE;    
    }
    
    private function loadPayments() 
    {   
        $this->payments = $this->DB_RegistrationFeePayment->find($this->applicationId, $this->departmentId);
        krsort($this->payments);
        return TRUE;
    }
    
    private function loadRegistrationFeeStatus() 
    {
        $feeStatusArray = $this->DB_RegistrationFeeStatus->find($this->applicationId, $this->departmentId);
        if (count($feeStatusArray) > 0)
        {
            $this->registrationFeeStatus = array_shift($feeStatusArray);
        }
        return TRUE;
    }
}
?>