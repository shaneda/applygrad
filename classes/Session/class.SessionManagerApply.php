<?php

class SessionManagerApply 
{
    
    private $aliasLogin = FALSE;
    
    private $sessionVars = array(
        'domainid',
        'usermasterid',
        'appid',
        'domainname',
        'datafileroot',
        'userid',
        'usertypeid',
        'usertypename',
        'email',
        'firstname',
        'lastname',
        'allow_edit',
        'allow_edit_late',
        'expdate',
        'expdate2',
        'msgsSent',
        'programs'
        );
    
    public function __construct($aliasLogin = FALSE) {
        
        ini_set('session.use_only_cookies', 'On');
        ini_set('session.cookie_httponly', 'On');
        ini_set('session.cookie_secure', 'On');
        
        if ($aliasLogin) {
            $this->aliasLogin = TRUE;
        }
        
        if (!isset($_SESSION['initiated'])) { 
            $this->initiateSession(); 
        }    
    }

    private function initiateSession() {
        if ( !isset($_SESSION) ) {
            session_start();    
        }   
        $_SESSION['initiated'] = TRUE;   
    }
    
    private function createToken() {
        $token = md5( uniqid(rand(), TRUE) );
        $_SESSION['token'] = $token;    
    }
    
    public function newSession() {

        global $datafileroot;
        
        if ( isset($_SESSION['initialized']) && $_SESSION['initialized'] ) {
            if (!isset($_SESSION['initiated'])) {
                $this->initiateSession();   
            }   
            if ($this->aliasLogin) {
                foreach ($this->sessionVars as $var) {
                    if ( isset($_SESSION[$var]) ) {
                        unset($_SESSION[$var]);    
                    }
                }    
            } else {
                $_SESSION = array();    
            }
                       
        }
        
        $_SESSION['initialized'] = TRUE;

        // retain future
        $_SESSION['domainid']= -1;      // system context or "door"
        $_SESSION['usermasterid']= -1;  // users.id
        $_SESSION['appid']= -1;         // application.id
        
        // use foreseeable
        $_SESSION['domainname']= "";
        $_SESSION['datafileroot'] = $datafileroot;
        $_SESSION['userid']= -1;        // lu_users_usertypes.id
        $_SESSION['usertypeid']= -1;
        $_SESSION['usertypename']= "";
        $_SESSION['email']= "";
        $_SESSION['firstname']= "";
        $_SESSION['lastname']= "";
        $_SESSION['allow_edit']= FALSE;
        $_SESSION['allow_edit_late']= FALSE;
        $_SESSION['expdate'] = "";
        $_SESSION['expdate2'] = "";
        $_SESSION['msgsSent']= 0;
        $_SESSION['programs']= array();
         
        // deprecate???
        $_SESSION['SECTION']= "1";
        $_SESSION['level']= 2;

        return TRUE;
    }
    
    
    public function setLoggedIn($usersId) {
        
        // Unnecessary??
        if (!isset($_SESSION['initiated'])) {
            $this->initiateSession();   
        }
        
        session_regenerate_id(TRUE);
        
        $this->createToken(); 
        
        $_SESSION['usermasterid'] = $usersId;
        
        return TRUE;    
    }


    public function userLoggedIn() {
        
        if ( isset($_SESSION['usermasterid']) && $_SESSION['usermasterid'] > 0 ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    

    public function destroySession() {
        
        // Unnecessary??
        if (!isset($_SESSION['initiated'])) {
            session_start();    
        }

        $_SESSION = array();

        $this->expireCookie();

        session_destroy();
        
        return TRUE;
    }

    
    public function expireCookie() {
        
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }        
    }
}
?>