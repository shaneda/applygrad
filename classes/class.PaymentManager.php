<?php

class PaymentManager {

    private $DB_Payment;                // DB_Applyweb subclass
    private $DB_PaymentItem;            // DB_Applyweb subclass
    
    private $applicationId;
    private $higherFeeDate;
    private $payments;                  // 2D array, indexed by payment_id
    private $paymentItems;              // 3D array, indexed by payment_id, payment_item_id
    private $applicationPaymentStatus;  // array(feePaid, paymentdate, paymentamount, feeWaived)
    private $applicationProgramFees;    // array, indexed by program_id
    
    private $newPaymentId = NULL;       // NULL|int
    
    public function __construct($applicationId, $higherFeeDate = NULL) {
        
        $this->DB_Payment = new DB_Payment();
        $this->DB_PaymentItem = new DB_PaymentItem();
        
        $this->applicationId = $applicationId;
        $this->higherFeeDate = $higherFeeDate;
        
        $this->loadPayments();
        $this->loadPaymentItems();
        $this->loadApplicationPaymentStatus();
        $this->loadApplicationProgramFees();    
    }
    
    public function getApplicationId() {
        return $this->applicationId;
    }
      
    public function getPayments() {
        return $this->payments;
    }
    
    public function getPaymentItems() {
        return $this->paymentItems;
    }    
  
    public function getStatusFeeWaived() {
        return $this->applicationPaymentStatus['feeWaived'];
    }
    
    public function getStatusWaiveHigherFee() {
        return $this->applicationPaymentStatus['waive_higher_fee'];
    }
    
    public function getStatusFeePaid() {
        return $this->applicationPaymentStatus['feePaid'];
    }
    
    public function getStatusAmountPaid() {
        return $this->applicationPaymentStatus['paymentamount'];
    }

    public function getApplicationProgramFees() {
        return $this->applicationProgramFees;
    }
    
    public function getTotalFee() {
        
        $totalFee = 0;
        $i = 0;

        foreach ($this->applicationProgramFees as $applicationProgramFee){
            $totalFee += $this->getProgramFee($applicationProgramFee);
            $i++;
        }
        
        return $totalFee;
    }
    
    public function getTotalPayments() {
    
        $totalPayments = 0;
        foreach ($this->payments as $row){
            if ($row['payment_status'] == 'pending' || $row['payment_status'] == 'paid') {
                $totalPayments += $row['payment_amount'];
            }    
        }
        return $totalPayments;
    }    
    
    public function getTotalPaid() {
    
        $totalPaid = 0;
        foreach ($this->payments as $row){            
            if ($row['payment_status'] == 'paid') {
                $totalPaid += $row['payment_amount'];    
            }
        }
        
        return $totalPaid;
    }
    
    public function getBalanceDue() {

        $totalFee = $this->getTotalFee();
        $totalPayments = $this->getTotalPayments();
        $difference = $totalFee - $totalPayments;
        
        if ($difference > 0) {
            $balanceDue = $difference;
        } else {
            $balanceDue = 0;    
        }
        
        return $balanceDue;       
    }

    public function getBalanceUnpaid() {

        $totalFee = $this->getTotalFee();
        $totalPaid = $this->getTotalPaid();
        $difference = $totalFee - $totalPaid;

        if ($difference > 0) {
            $balanceUnpaid = $difference;
        } else {
            $balanceUnpaid = 0;    
        }
        
        return $balanceUnpaid;       
    }
    
    public function getProgramBalancesDue() {
        
        // Get the payments already made for each program
        $programPayments = array();
        foreach ($this->paymentItems as $paymentId => $paymentItemRecords) {        
            
            foreach($paymentItemRecords as $paymentItemId => $paymentItem) {
                
                if ($this->payments[$paymentId]['payment_status'] == 'void') {
                    continue;
                }
                
                if ( isset($paymentItem['program_id']) ) {
                    
                    $programId = $paymentItem['program_id'];
                    if ( isset($programPayments[$programId]) ) {
                        $programPayments[$programId] += $paymentItem['payment_item_amount'];    
                    } else {
                        $programPayments[$programId] = $paymentItem['payment_item_amount'];    
                    }                   
                }                
            }
        }
        
        /* 
         * Set each program balance (min 0) by crediting any previous program payments
         * and applying any credit remaining from the total payment.
         * Reduce the total payment credit by subtracting the program payment/fee. 
        */
        $programBalances = array();
        $totalBalanceDue = $this->getBalanceDue();
        $credit = $totalPayments = $this->getTotalPayments();
        $i = 0;
        foreach ($this->applicationProgramFees as $programId => $applicationProgramRecord) {
            
            // Get any previous payment for the program.
            $programPayment = 0;
            if ( array_key_exists($programId, $programPayments) ) {
                $programPayment = $programPayments[$programId];    
            }
            
            // Determine the appropriate fee for the program.
            $programFee = $this->getProgramFee($applicationProgramRecord);
            
            // Get the initial balance by crediting any payments for that program.
            $initialProgramBalance = $programFee - $programPayment;
            if ($initialProgramBalance < 0) {
                $initialProgramBalance = 0;
            }
            $programBalances[$programId] = $initialProgramBalance;
            
            if ($initialProgramBalance > 0) {
                
                // If previous payment has been made for the program...
                if ($programPayment > 0) {
                    
                    if ($totalBalanceDue > 0) {
                        
                        // Only reduce credit by the previous payment amount,
                        // because the unpaid program balance will remain.
                        $credit -= $programPayment; 
                        
                    } else {
                        
                        // Zero out the program balance
                        $programBalances[$programId] = 0;
                        // No program balance remains, so reduce credit by the full program fee.
                        $credit -= $programFee;    
                    }
                    
                } elseif ($credit > 0) {
                    
                    // Payment has been made for another program.
                    // Don't apply credit here. 
                    $programBalances[$programId] = $initialProgramBalance;

                } else {
                    
                    // No payments have been made, so the initial balance 
                    // should equal the full program fee.
                    
                    // Apply any remaining credit toward the balance.
                    $newProgramBalance = $initialProgramBalance - $credit;
                    
                    if ($newProgramBalance >= 0) {
                        
                        // The balance exceeded the credit.
                        // Credit will be zeroed out before next loop iteration.
                        // Set the program balance to the new balance.
                        $programBalances[$programId] = $newProgramBalance;
                        
                    } else {
                    
                        // Credit has exceeded the initial balance.
                        // Zero out the program balance.
                        $programBalances[$programId] = 0;       
                    }
                    
                    // In either case, credit was applied against the full program fee,
                    // so reduce credit accordingly. 
                    $credit -= $programFee;
                }     

            } else {
                
                // The program fee was fully covered by previous payment.
                // No program balance will remain, so reduce credit by the full program fee.
                $credit -= $programFee;
                
            } 

            // Set credit to min 0.
            if ($credit < 0) {
                $credit = 0;
            }
            
            $i++;    
        }
        
        /*
        * Apply any remaining credit. 
        */
        if ($credit > 0) {

            foreach ($programBalances as $programId => $programBalance) {
            
                $newProgramBalance = $programBalance - $credit;
                if ($newProgramBalance < 0) {
                    // credit exceeded balance
                    $credit += $newProgramBalance;
                    $programBalances[$programId] = 0;
                } else {
                    $credit = 0;
                    $programBalances[$programId] = $newProgramBalance;
                    break;
                }    
            }
        }
        
        return $programBalances;
    }
    
    public function getNewPaymentId() {
        return $this->newPaymentId;
    }
    
    public function saveFeeWaived($waive) {
        if ($waive == 1) 
        {
          $query = "UPDATE application SET waive = " . $waive . ", waivedate = NOW()";  
        } else
            {
              $query = "UPDATE application SET waive = " . $waive . ", waivedate = '2000-11-29 11:00:00'";  
            }
        $query .= " WHERE application.id = " . $this->applicationId;
        $numAffectedRows = $this->DB_Payment->handleUpdateQuery($query);
        
        if ($numAffectedRows > 0) {
            $this->applicationPaymentStatus['feeWaived'] = $waive;
            return TRUE;    
        } else {
            return FALSE;
        }  
    }
    
    public function saveWaiveHigherFee($waiveHigherFee) {
    
        $query = "UPDATE application SET waive_higher_fee = " . $waiveHigherFee;
        $query .= " WHERE application.id = " . $this->applicationId;
        $numAffectedRows = $this->DB_Payment->handleUpdateQuery($query);
        
        if ($numAffectedRows > 0) {
            $this->applicationPaymentStatus['waive_higher_fee'] = $waiveHigherFee;
            return TRUE;    
        } else {
            return FALSE;
        }  
    }

    public function savePayment( $paymentRecord = array(), $paymentItemRecords = array() ) {
        
        $paymentId = NULL;
        if ( isset($paymentRecord['payment_id']) ) {
            $paymentId = $paymentRecord['payment_id'];
        }
        
        $payment = new Payment($paymentId, $this->applicationId);
        $payment->importValues($paymentRecord);
        $payment->setPaymentItems($paymentItemRecords);
        $payment->save();
        
        if (!$paymentId) {
            $this->newPaymentId = $payment->getId();    
        }
        
        // Get the latest db state
        $this->loadPayments();
        $this->loadPaymentItems();
        
        $paymentType = $payment->getPaymentType();
        $paymentStatusUpdated = $this->updateApplicationPaymentStatus($paymentType);
        if ( $paymentStatusUpdated ) {
            return TRUE;
        } else {
            return FALSE;
        }        
    }

    public function deletePayment($paymentId) {
    
        $numAffectedRows = 0;
        $numAffectedRows += $this->DB_Payment->delete($paymentId);
        $numAffectedRows += $this->DB_PaymentItem->delete($paymentId);
        
        // Get datafile info for vouchers.
        $voucherQuery = "SELECT datafileinfo_id 
                            FROM payment_voucher
                            WHERE payment_voucher.payment_id = " . $paymentId;
        $voucherRecords = $this->DB_Payment->handleSelectQuery($voucherQuery);
        
        // Delete the datafile and voucher records
        foreach ($voucherRecords as $voucherRecord) {
            $datafileDeleteQuery = "DELETE FROM datafileinfo WHERE id = " . $voucherRecord['datafileinfo_id'];
            $this->DB_Payment->handleUpdateQuery($datafileDeleteQuery); 
        }
        $voucherDeleteQuery = "DELETE FROM payment_voucher WHERE payment_id = " . $paymentId;
        $this->DB_Payment->handleUpdateQuery($voucherDeleteQuery); 
        
        // Get the latest db state
        $this->loadPayments();
        $this->loadPaymentItems();
        
        $applicationPaymentStatusUpdated = $this->updateApplicationPaymentStatus();
        
        if ($applicationPaymentStatusUpdated) {
            return TRUE;    
        } else {
            return FALSE;
        }    
    }
    
    private function updateApplicationPaymentStatus($paymentType = NULL) {
        
        $balanceUnpaid = $this->getBalanceUnpaid();
        if ($balanceUnpaid) {
            $paid = 0;
        } else {
            $paid = 1;    
        }
        
        $paymentAmount = $this->getTotalPayments();
        if ($paymentAmount <= 0) {
            $paymentAmount = "NULL";
            $paymentType = "NULL";    
        }
        
        $query = "UPDATE application SET
                    paid = " . $paid . ",
                    paymentdate = NOW(),
                    paymentamount = " . $paymentAmount;
        if ($paymentType) {
            $query .= ", paymenttype = " . $paymentType;    
        }
        $query .= " WHERE application.id =  " . $this->applicationId;
        $numAffectedRows = $this->DB_Payment->handleUpdateQuery($query);
        
        if ($numAffectedRows > 0) {
            return TRUE;    
        } else {
            return FALSE;
        } 
    }
     
    private function loadPayments() {   
        $this->payments = $this->DB_Payment->find($this->applicationId);
        krsort($this->payments);
        return TRUE;
    }
    
    private function loadPaymentItems() {
        $this->paymentItems = array();
        foreach($this->payments as $paymentId => $paymentRecord) {
            $this->paymentItems[$paymentId] = $this->DB_PaymentItem->find($paymentId);
        }
        return TRUE;
    }

    private function loadApplicationPaymentStatus() {

        $query = "SELECT paid AS feePaid, paymentdate, paymentamount, 
                    waive AS feeWaived, waive_higher_fee 
                    FROM application WHERE application.id = " . $this->applicationId;
        $paymentStatusArray = $this->DB_Payment->handleSelectQuery($query);
        foreach ($paymentStatusArray as $row) {
            $this->applicationPaymentStatus = $row;
        }
        return TRUE;
    }
    
    private function loadApplicationProgramFees() {
                
        $query = "SELECT programs.id AS program_id,
                    degree.name AS degreename,
                    fieldsofstudy.name AS fieldname,
                    lu_application_programs.choice,
                    lu_application_programs.id AS itemid,
                    programs.programprice,
                    programs.baseprice,
                    programs.programprice_late,
                    programs.baseprice_late
                    FROM lu_application_programs
                    INNER JOIN programs on programs.id = lu_application_programs.program_id
                    INNER JOIN degree on degree.id = programs.degree_id
                    INNER JOIN fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
                    WHERE lu_application_programs.application_id = " . $this->applicationId . " 
                    ORDER BY lu_application_programs.choice";
                
        $this->applicationProgramFees = $this->DB_Payment->handleSelectQuery($query, 'program_id');
        return TRUE;
    }
    
    private function getProgramFee($applicationProgramFee) {

        $programId = $applicationProgramFee['program_id'];
        
        $waiveHigherFee = $this->getStatusWaiveHigherFee();
        
        if ($waiveHigherFee) {
        
            $useHigherFee = FALSE;
            
        } else {
        
            // Base higher fee first on current date.
            $currentTimestamp = time();
            $useHigherFee = $this->afterHigherFeeDate($currentTimestamp);
                    
            // Check to see whether there is a non-void payment for the program.
            $payments = $this->payments;
            ksort($payments);   // Should base date on earliest non-void payment.
            foreach($payments as $payment) {
            
                $paymentId = $payment['payment_id'];
                if ($payment['payment_status'] != 'void') {
                    
                    foreach ($this->paymentItems[$paymentId] as $paymentItem) {
                        
                        $paymentProgramId = $paymentItem['program_id'];
                        if ($paymentProgramId == $programId) {
                            
                            // If there's a matching payment item, base the fee on the payment date.
                            $paymentTimestamp = strtotime($payment['payment_intent_date']);
                            $useHigherFee = $this->afterHigherFeeDate($paymentTimestamp);
                            break 2;
                        }   
                    }
                }    
            }       
        }
        
        if ($applicationProgramFee['choice'] == 1) {
            
            if ($useHigherFee 
                && ($applicationProgramFee['baseprice_late'] > $applicationProgramFee['baseprice'])) 
            {
                $programFee = $applicationProgramFee['baseprice_late'];
            } 
            else 
            {
                $programFee = $applicationProgramFee['baseprice'];
            }

        } else {
            
            if ($useHigherFee 
                && ($applicationProgramFee['programprice_late'] > $applicationProgramFee['programprice'])) 
            {
                $programFee = $applicationProgramFee['programprice_late'];
            } 
            else 
            {
                $programFee = $applicationProgramFee['programprice'];
            }
        }
        
        return $programFee;   
    }
    
    private function afterHigherFeeDate($timestamp) {
        
        $higherFeeTimestamp = strtotime($this->higherFeeDate);

        if ($this->higherFeeDate == NULL) {
            return FALSE;
        } else {
            if ($timestamp > $higherFeeTimestamp) {
                return TRUE;
            }
        }  
        
        return FALSE;
    }
    
}
?>