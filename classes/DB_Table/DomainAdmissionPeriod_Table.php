<?php
/*
 * Include basic class
 */
require_once 'DB/Table.php';

/*
 * Create the table object
 */
class DomainAdmissionPeriod_Table extends DB_Table {

    /*
     * Column definitions
     */
    var $col = array(

        'domainId'      => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0,
            'qf_type' => 'hidden'
        ),

        'admissionPeriodId' => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0,
            'qf_type' => 'hidden'
        )

    );

    /*
     * Index definitions
     */
    var $idx = array(

        'PRIMARY' => array(
            'type' => 'primary',
            'cols' => array(
                'domainId',
                'admissionPeriodId'
            )
        )

    );

}

?>
