<?php
/*
 * Include basic class
 */
require_once 'DB/Table.php';

/*
 * Create the table object
 */
class Department_Table extends DB_Table {

    /*
     * Column definitions
     */
    var $col = array(

        'id'                   => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0
        ),

        'name'                 => array(
            'type'    => 'varchar',
            'size'    => 50,
            'require' => true,
            'default' => ''
        ),

        'parent_school_id'     => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0
        ),

        'oraclestring'         => array(
            'type' => 'varchar',
            'size' => 50
        ),

        'rank'                 => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0
        ),

        'cc_email'             => array(
            'type' => 'varchar',
            'size' => 255
        ),

        'enable_round1'        => array(
            'type' => 'char',
            'size' => 1
        ),

        'enable_round2'        => array(
            'type' => 'char',
            'size' => 1
        ),

        'enable_final'         => array(
            'type' => 'char',
            'size' => 1
        ),

        'semiblind_review'     => array(
            'type' => 'char',
            'size' => 1
        ),

        'allowRequestAdvisors' => array(
            'type' => 'smallint'
        )

    );

    /*
     * Index definitions
     */
    var $idx = array(

        'parent_school_id' => array(
            'type' => 'normal',
            'cols' => 'parent_school_id'
        ),

        'PRIMARY'          => array(
            'type' => 'primary',
            'cols' => 'id'
        )

    );

    /*
     * Auto-increment declaration
     */
    var $auto_inc_col = 'id';
    
    
    /*
     * Baseline queries
     */
    var $sql = array( 
    
        // multiple rows for a list 
        'list' => array( 
            'select' => "*"
        ),
        
        // one row for an item detail
        'item' => array( 
            'select' => '*',
            'get' => 'row'
        )
    
    );
    
    public function get($department_id) {
    
        $this->fetchmode = MDB2_FETCHMODE_ASSOC; 
        $filter = "id=" . $department_id;
        return $this->select('item', $filter);
        
    }
}

?>
