<?php
/*
 * Include basic class
 */
require_once 'DB/Table.php';

/*
 * Create the table object
 */
class AdmissionPeriod_Table extends DB_Table {

    /*
     * Column definitions
     */
    
    var $col = array(

        'admissionPeriodId'   => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0,
            'qf_type' => 'hidden',
            'qf_label' => ''
        ),

        'description' => array(
            'type' => 'varchar',
            'require' => true,
            'size' => 255,
            'qf_label' => 'Description'
        ),

        'startDate'      => array(
            'type'    => 'date',
            'require' => true,
            'qf_label' => 'Start Date'
        ),

        'submitDeadline'  => array(
            'type'    => 'date',
            'require' => true,
            'qf_label' => 'Submit Deadline'
        ),

        'editDeadline'    => array(
            'type'    => 'date',
            'require' => true,
            'qf_label' => 'Edit Deadline'
        ),

        'viewDeadline'    => array(
            'type'    => 'date',
            'require' => true,
            'qf_label' => 'View Deadline'
        ),

        'admitTermDate'     => array(
            'type'    => 'date',
            'require' => true,
            'qf_label' => 'Admit Term' 
        ),

        'reviewClosed' => array(
            'type'    => 'boolean',
            'require' => false,
            'default' => '0',
            'qf_label' => 'Review Closed' 
        )

    );

    /*
     * Index definitions
     */
    var $idx = array(

        'PRIMARY' => array(
            'type' => 'primary',
            'cols' => 'admissionPeriodId'
        )

    );

    /*
     * Auto-increment declaration
     */
    // WARNING: this turns on some bogus auto increment emulation
    // that uses an extra table to track the increment??????
    var $auto_inc_col = 'admissionPeriodId';
    
    
    /*
     * Baseline queries
     */
    var $sql = array( 
    
        // one row for an item detail
        'item' => array( 
            'select' => '*',
            'get' => 'row'
        ),
        
        // multiple rows for a list 
        'list' => array( 
            'select' => '*',
            'order'  => 'admissionPeriodId DESC'
        ),
        
        'listByDepartment' => array( 
            'select' => '*',
            'join' => 'INNER JOIN DepartmentAdmissionPeriod ON 
                AdmissionPeriod.admissionPeriodId = DepartmentAdmissionPeriod.admissionPeriodId',
            'order'  => 'AdmissionPeriod.startDate DESC'
        ),
        
        'listByDomain' => array( 
            'select' => '*',
            'join' => 'INNER JOIN DomainAdmissionPeriod ON 
                AdmissionPeriod.admissionPeriodId = DomainAdmissionPeriod.admissionPeriodId',
            'order'  => 'AdmissionPeriod.startDate DESC'
        )
    
    );
    

    public function get($admissionPeriodId) {
    
        $this->fetchmode = MDB2_FETCHMODE_ASSOC; 
        $filter = "AdmissionPeriod.admissionPeriodId = " . $admissionPeriodId;
        return $this->select('item', $filter);
        
    }    
    
    public function find($departmentId) {
    
        $this->fetchmode = MDB2_FETCHMODE_ASSOC; 
        $filter = "departmentId = "  . $departmentId;
        return $this->select('listByDepartment', $filter);
        
    }
    
    public function findDateOverlap($date, $unit, $unitId, $admissionPeriodId = NULL) {

        $this->fetchmode = MDB2_FETCHMODE_ASSOC; 
        $filter = $unit . "Id = " . $unitId;
        if ($admissionPeriodId) {
            $filter .= " AND AdmissionPeriod.admissionPeriodId != " . $admissionPeriodId;    
        }
        $filter .= " AND '" . $date . "' BETWEEN startDate AND submitDeadline";

        return $this->select('listBy' . ucfirst($unit), $filter);
        
    }
    

}

?>
