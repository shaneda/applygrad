<?php
/*
 * Include basic class
 */
require_once 'DB/Table.php';

/*
 * Create the table object
 */
class Domain_Table extends DB_Table {

    /*
     * Column definitions
     */
    var $col = array(

        'id'                   => array(
            'type'    => 'integer',
            'require' => true,
            'default' => 0
        ),

        'name'                 => array(
            'type'    => 'varchar',
            'size'    => 100,
            'require' => true,
            'default' => ''
        ),

        'description'          => array(
            'type'    => 'varchar',
            'size'    => 180,
            'require' => false,
            'default' => ''
        ),
        
        'path'                 => array(
            'type'    => 'clob',
            'require' => true,
            'default' => ''
        ),

        'active'               => array(
            'type' => 'smallint',
            'require' => true
        ),



    );

    /*
     * Index definitions
     */
    var $idx = array(

        'PRIMARY'          => array(
            'type' => 'primary',
            'cols' => 'id'
        )

    );

    /*
     * Auto-increment declaration
     */
    var $auto_inc_col = 'id';
    
    
    /*
     * Baseline queries
     */
    var $sql = array( 
    
        // multiple rows for a list 
        'list' => array( 
            'select' => "*"
        ),
        
        // one row for an item detail
        'item' => array( 
            'select' => '*',
            'get' => 'row'
        )
    
    );
    
    public function get($domainId) {
    
        $this->fetchmode = MDB2_FETCHMODE_ASSOC; 
        $filter = "id=" . $domainId;
        return $this->select('item', $filter);
        
    }
}

?>
