<?php
/*
Class for basic application-related info
Primary tables: application, users, users_info
*/

class DB_Application extends DB_Applyweb
{
    
    //protected $application_id;

    function getApplicantInfo($application_id) {
        
        $query = "SELECT users.*, users_info.*,
            cur_states.name as currentstate,
            cur_countries.name as currentcountry,
            perm_states.name as permstate,
            perm_countries.name as permcountry,
            ethnicity.name as ethnicity,
            visatypes.short as visatype_short,
            cit_countries.name as citcountry,
            application.buckleywaive,
            application.id as appid
            FROM
            application
            left outer join lu_users_usertypes on lu_users_usertypes.id = application.user_id 
            inner join users on users.id = lu_users_usertypes.user_id
            left outer join users_info on users_info.user_id = lu_users_usertypes.id
            left outer join states as cur_states on cur_states.id = users_info.address_cur_state
            left outer join states as perm_states on perm_states.id = users_info.address_perm_state
            left outer join countries as cur_countries on cur_countries.id = users_info.address_cur_country
            left outer join countries as perm_countries on perm_countries.id = users_info.address_perm_country
            left outer join countries as cit_countries on cit_countries.id = users_info.cit_country
            left outer join ethnicity on ethnicity.id = users_info.ethnicity
            left outer join visatypes on visatypes.id = users_info.visastatus 
            WHERE application.id = " . $application_id; 
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;     
    }


    function getStatus($application_id) {
        
        $query = "SELECT application.* FROM application WHERE application.id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;
    }
    

    
}

?>
