<?php
class Department
{
    
    public static function getRecord($departmentId) {
        
        $query = "SELECT * from department where id = " . intval($departmentId);
        $result = mysql_query($query);    
        $departmentRecord = array();
        while($row = mysql_fetch_array($result)) {    
            $departmentRecord = $row;
        }
        return $departmentRecord;   
    }


    public static function getName($departmentId) {
        
        $query = "SELECT name from department where id = " . intval($departmentId);
        $result = mysql_query($query);    
        $departmentName = FALSE;
        while($row = mysql_fetch_array($result)) {    
            $departmentName = $row['name'];
        }
        return $departmentName;   
    }
    
    
    public static function isValid($departmentId) {
        
        $query = "SELECT programs.id FROM programs
                        INNER JOIN lu_programs_departments 
                            ON programs.id = lu_programs_departments.program_id
                        WHERE programs.enabled = 1
                        AND lu_programs_departments.department_id = " . intval($departmentId);
        
        $result = mysql_query($query);
        
        if (mysql_numrows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function getPrograms($departmentId) {
    
        $query = "SELECT programs.*,
                    CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS name
                    FROM programs
                    INNER JOIN lu_programs_departments ON programs.id = lu_programs_departments.program_id
                    INNER JOIN degree ON degree.id = programs.degree_id
                    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id";
        $query .= " WHERE lu_programs_departments.department_id = " . intval($departmentId);
        
        $result = mysql_query($query);
        
        $programs = array();
        while ( $row = mysql_fetch_array($result) ) {
            $programs[] = $row;    
        }
        
        return $programs;    
    }
    
    public static function hasProgram($departmentId, $programId) {
        
        $query = "SELECT program_id FROM lu_programs_departments
                        WHERE department_id = " . intval($departmentId);
        $query .= " AND program_id = " . intval($programId);
        
        $result = mysql_query($query);
        
        if (mysql_numrows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }    
    }
    

    public static function getDomains($departmentId) {
    
        $query = "SELECT domain.id, domain.name, domain.active
                    FROM domain
                    INNER JOIN lu_domain_department 
                        ON domain.id = lu_domain_department.domain_id";
        $query .= " WHERE lu_domain_department.department_id = " . intval($departmentId);
        
        $result = mysql_query($query);
        
        $domains = array();
        while ( $row = mysql_fetch_array($result) ) {
            $domains[] = $row;    
        }
        
        return $domains;    
    }


    public static function getContent($departmentId, $contentName, $domainId = NULL) {
        
        $query = "SELECT content FROM content
                    INNER JOIN lu_domain_department
                        ON content.domain_id = lu_domain_department.domain_id 
                    WHERE name = '" . mysql_real_escape_string($contentName);
        $query .= "' AND lu_domain_department.department_id = " . intval($departmentId); 
 
        $result = mysql_query($query);
        
        $content = '';
        while ( $row = mysql_fetch_array($result) ) {
            $content = $row['content'];    
        }
        
        return $content;
    }
    

    public static function hasContent($departmentId, $contentName, $domainId = NULL) {
        
        $content = Department::getContent($departmentId, $contentName, $domainId = NULL);
        
        if ($content) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
}
?>