<?php

class RegistrationFeePayment 
{

    private $DB_RegistrationFeePayment;

    private $id;
    private $applicationId;
    private $departmentId;
    private $paymentType;
    private $amount;
    private $intentDate;
    private $status;
    private $lastModUserId;
    
    public function __construct($id = NULL, $applicationId = NULL, $departmentId = NULL) {       
        
        $this->DB_RegistrationFeePayment = new DB_RegistrationFeePayment();
        
        $this->id = $id;
        $this->applicationId = $applicationId;
        $this->departmentId = $departmentId;
        
        if ($id) {
            $this->loadFromDb();
        }
    }
    
    public function getId() {
        return $this->id;
    }

    public function getApplicationId() {
        return $this->applicationId;
    }    

//    public function setApplicationId($applicationId) {
//        $this->applicationId = $applicationId;
//    } 
    
    public function getDepartmentId() {
        return $this->departmentId;
    }    

//    public function setDepartmentId($departmentId) {
//        $this->departmentId = $departmentId;
//    } 

    public function getPaymentType() {
        return $this->paymentType;    
    }

    public function getAmount() {
        return $this->amount;
    }
    
    public function setAmount($amount) {
        $this->amount = $amount;
        return TRUE;
    }
    
    public function getIntentDate() {
        return $this->intentDate;
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return TRUE;
    }
    
    public function getLastModUserId() {
        return $this->lastModUserId;
    }
    
    public function loadFromDb() {
    
        $paymentArray = $this->DB_RegistrationFeePayment->get($this->id);
        
        if ( isset($paymentArray[0]) ) {
        
            $this->applicationId = $paymentArray[0]['application_id'];
            $this->departmentId = $paymentArray[0]['department_id'];
            $this->paymentType = $paymentArray[0]['payment_type'];
            $this->amount = $paymentArray[0]['payment_amount'];
            $this->intentDate = $paymentArray[0]['payment_intent_date'];
            $this->status = $paymentArray[0]['payment_status'];
            $this->lastModUserId = $paymentArray[0]['last_mod_user_id'];

            return TRUE; 
            
        } else {
        
            return FALSE;
        }
    }
    
    public function importValues( $paymentRecord = array() ) {
    
        if ( isset($paymentRecord['id']) ) {
            $this->id = $paymentRecord['id'];
        }

        if ( isset($paymentRecord['application_id']) ) {
            $this->applicationId = $paymentRecord['application_id'];
        }
        
        if ( isset($paymentRecord['department_id']) ) {
            $this->departmentId = $paymentRecord['department_id'];
        }

        if ( isset($paymentRecord['payment_type']) ) {
            $this->paymentType = $paymentRecord['payment_type'];
        }

        if ( isset($paymentRecord['payment_amount']) ) {
            $this->amount = $paymentRecord['payment_amount'];
        }
        
        if ( isset($paymentRecord['payment_intent_date']) ) {
            $this->intentDate = $paymentRecord['payment_intent_date'];
        }

        if ( isset($paymentRecord['payment_status']) ) {
            $this->status = $paymentRecord['payment_status'];
        }

        if ( isset($paymentRecord['last_mod_user_id']) ) {
            $this->lastModUserId = $paymentRecord['last_mod_user_id'];
        }
        
        return TRUE;
    }
    
    public function save() {
        
        $paymentRecord = array(
            'id' => $this->id,
            'application_id' => $this->applicationId,
            'department_id' => $this->departmentId,
            'payment_type' => $this->paymentType,
            'payment_amount' => $this->amount,
            'payment_intent_date' => $this->intentDate,
            'payment_status' => $this->status,
            'last_mod_user_id' => $this->lastModUserId
            );
        
        $this->id = $this->DB_RegistrationFeePayment->save($paymentRecord);

        return TRUE;
    }
    
}
?>