<?php

/*
Class for MSE risk factors
Primary tables: mse_risk_factors
*/

class DB_MseBridgeCourse extends DB_Applyweb
{

    //protected $application_id;
   
    function getBridgeCourse($application_id, $reviewer_id) {
    
        $query = "SELECT * FROM mse_bridge_course                 
                    WHERE application_id = " . $application_id . "
                    AND reviewer_id =  " . $reviewer_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    function updateBridgeCourse($application_id, $reviewer_id, $bridge_course) {
    
        $query = "REPLACE INTO mse_bridge_course VALUES ("
                    . $application_id . ","
                    . $reviewer_id . ","
                    . $bridge_course .  
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }
    
    function getBridgeCourseDecision($application_id, $program_id) {
    
        $query = "SELECT * FROM mse_bridge_course_decision                 
                    WHERE application_id = " . $application_id . "
                    AND program_id =  " . $program_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }
    
    function updateBridgeCourseDecision($application_id, $program_id, $bridge_course) {
    
        $query = "REPLACE INTO mse_bridge_course_decision VALUES ("
                    . $application_id . ","
                    . $program_id . ","
                    . $bridge_course .  
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }

}

?>
