<?php
include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminList.php";  
include "../classes/DB_Applyweb/class.VW_ReviewListRecommendationCount.php";
include "../classes/DB_Applyweb/class.VW_ReviewList.php";
include "../classes/DB_Applyweb/class.VW_RecyclablesListBase.php";
include "../classes/DB_Applyweb/class.VW_ReviewListPrograms.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAdvisors.php";
include "../classes/DB_Applyweb/class.VW_ReviewListToefl.php";
include "../classes/DB_Applyweb/class.VW_ReviewListIelts.php";
include "../classes/DB_Applyweb/class.VW_ReviewListRecommenders.php";
include "../classes/DB_Applyweb/class.VW_ReviewListReviews.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAdmissionStatus.php";
include "../classes/DB_Applyweb/class.VW_ReviewListReviewerAoi.php";
include "../classes/DB_Applyweb/class.VW_ReviewListSpecial.php";
include "../classes/DB_Applyweb/class.VW_ReviewListTouched.php";
include "../classes/DB_Applyweb/class.VW_ReviewListGroups.php";

class RecyclablesListData
{
    private $departmentId;
    private $departmentName;
    private $data;          // 2D array
    
    public function __construct($departmentId, $departmentName = null) {
        $this->departmentId = $departmentId;    
        $this->departmentName = $departmentName;
    }
    
    public function getData($periodId = NULL, $searchString = '') {
        
        $groupsLimitReviewerId = NULL;
        $round = 1;
        
        $baseView = new VW_RecyclablesListBase($this->departmentName);
        $baseArray = $baseView->find($periodId, $this->departmentId, 1, null, $searchString);
        
        // PLB added round argument for programs 11/9/09.
        // This ensures that only the right program(s) are displayed for each round. 
        $programView = new VW_ReviewListPrograms($this->departmentName);
        $programArray = $programView->find($periodId, $this->departmentId, $round);

        $advisorView = new VW_ReviewListAdvisors($this->departmentName);
        $advisorArray = $advisorView->find($periodId, $this->departmentId);

        $toeflView = new VW_ReviewListToefl($this->departmentName);
        $toeflArray = $toeflView->find($periodId, $this->departmentId);

        $ieltsView = new VW_ReviewListIelts($this->departmentName);
        $ieltsArray = $ieltsView->find($periodId, $this->departmentId);

        $recommendersView = new VW_ReviewListRecommenders($this->departmentName);
        // xxxx  DebugBreak();
        if ($this->departmentId == 6) {
             $recommendersArray = $recommendersView->find($periodId, $this->departmentId, $round, NULL, NULL, 1, 1);
        }  else {
            $recommendersArray = $recommendersView->find($periodId, $this->departmentId, $round);
        }
        
        $recommendationCountView = new VW_ReviewListRecommendationCount($this->departmentName);
        $recommendationCountArray = $recommendationCountView->find('department', $this->departmentId, $periodId);

        /*
        * Admission view should not have a review dependency, i.e., it should show
        * even when there have been no reviews. 
        */
        $admissionView = new VW_ReviewListAdmissionStatus($this->departmentName);
        $admissionArray = $admissionView->find($periodId, $this->departmentId, $round);

        $recordCount = count($baseArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $applicationId = $baseArray[$i]['application_id'];
    
            $baseArray[$i] = array_merge($baseArray[$i], $programArray[$applicationId]);
            
            if ( array_key_exists($applicationId, $advisorArray) ) {
                $baseArray[$i]['possible_advisors'] = $advisorArray[$applicationId]['possible_advisors'];    
            } else {
                $baseArray[$i]['possible_advisors'] = '';
            }

            if ( array_key_exists($applicationId, $toeflArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $toeflArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['toefl_testdate'] = '';
                $baseArray[$i]['toefl_section1'] = '';
                $baseArray[$i]['toefl_section2'] = '';
                $baseArray[$i]['toefl_section3'] = '';
                $baseArray[$i]['toefl_essay'] = '';
                $baseArray[$i]['toefl_total'] = ''; 
                $baseArray[$i]['toefl_type'] = ''; 
            }

            if ( array_key_exists($applicationId, $ieltsArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $ieltsArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['ieltsscore_testdate'] = '';
                $baseArray[$i]['ieltsscore_listeningscore'] = '';
                $baseArray[$i]['ieltsscore_readingscore'] = '';
                $baseArray[$i]['ieltsscore_writingscore'] = '';
                $baseArray[$i]['ieltsscore_speakingscore'] = ''; 
                $baseArray[$i]['ieltsscore_overallscore'] = ''; 
            }

            //   xxxxx  DebugBreak();
            $baseArray[$i] = array_merge($baseArray[$i], $recommendersArray[$applicationId]);
            
            if ( array_key_exists($applicationId, $recommendationCountArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $recommendationCountArray[$applicationId]);    
            } else {
                $baseArray[$i]['ct_recommendations_requested'] = 0;
                $baseArray[$i]['ct_recommendations_submitted'] = 0;
            }
            
            if ( array_key_exists($applicationId, $admissionArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $admissionArray[$applicationId]);    
                if ($admissionArray[$applicationId]['decision'] == '') {
                    $baseArray[$i]['full_reject'] = 'N';    
                } else {
                    $fullReject = 'Y';
                    $decisionArray = explode('|', $admissionArray[$applicationId]['decision']);
                    foreach ($decisionArray as $decisionValue) {
                        if ($decisionValue != 'R') {
                            $fullReject = 'N';    
                        }
                    }
                    $baseArray[$i]['full_reject'] = $fullReject;
                }

            } else {

                $baseArray[$i]['full_reject'] = 'N';
                $baseArray[$i]['decision'] = '';
                $baseArray[$i]['admit_to'] = '';
                $baseArray[$i]['admit_to_decision'] = ''; 
            }
            
        }        

        return $baseArray;
    }
}

?>
