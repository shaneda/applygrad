<?php
/*
* Class for admission period business logic.
*/

class Period
{    
    private $id;
    private $unitId;
    private $periodTypeId;
    private $name;
    private $description;
    private $startDate;
    private $endDate;
    private $higherFeeDate;
    private $lastPaymentDate;
    private $parentPeriodId;
    private $childPeriods;      // array of period objects
    private $programs;          // array of unit names indexed by unit_id
    private $applications;      // array of application ids
    
    public function __construct($periodId = NULL) {
        
        $this->id = $periodId;
        $this->periodType = NULL;
        $this->name = NULL;
        $this->description = NULL;
        $this->startDate = NULL;
        $this->endDate = NULL;
        $this->higherFeeDate = NULL;
        $this->lastPaymentDate = NULL;
        $this->parentPeriodId = NULL;
        $this->childPeriods = array();
        $this->programs = array();
        $this->applications = array();
        if ($periodId) {      
            $this->loadFromDb();            
        }
    }

    public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getUnitId() {
        return $this->unitId;
    }

    public function setUnitId($unitId) {
        $this->unitId = $unitId;
    }
    
    public function getPeriodTypeId() {
        return $this->periodTypeId;
    }
    
    public function setPeriodTypeId($periodTypeId) {
        $this->periodTypeId = $periodTypeId;
    }
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    
    public function getStartDate($format = NULL) {
        return $this->formatDate($this->startDate, $format);
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }
    
    public function getEndDate($format = NULL) {
        return $this->formatDate($this->endDate, $format);
    }
    
    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    public function getHigherFeeDate($format = NULL) {
        return $this->formatDate($this->higherFeeDate, $format);
    }
    
    public function setHigherFeeDate($higherFeeDate) {
        $this->higherFeeDate = $higherFeeDate;
    }
    
    public function getLastPaymentDate($format = NULL) {
        return $this->formatDate($this->lastPaymentDate, $format);
    }
    
    public function setLastPaymentDate($lastPaymentDate) {
        $this->lastPaymentDate = $lastPaymentDate;
    }
    
    public function getParentPeriodId() {
        return $this->parentPeriodId;    
    }

    public function setParentPeriodId($parentPeriodId) {
        $this->parentPeriodId = $parentPeriodId;    
    }

    public function getParentPeriod() {
        $parentPeriod = NULL;
        if ($this->parentPeriodId) {
            $parentPeriod = new Period($this->parentPeriodId);     
        }
        return $parentPeriod;    
    }
    
    public function getChildPeriods($periodTypeId = NULL) {
        if ($periodTypeId) {
            $childPeriods = array();
            foreach ($this->childPeriods as $period) {
                if ($period->getPeriodTypeId() == $periodTypeId) {
                    $childPeriods[] = $period;
                }
            }
            return $childPeriods;
        }
        // Return all child periods by default.
        return $this->childPeriods;    
    }
    
    public function getPrograms() {
        return $this->programs;
    }
    
    public function setPrograms($programArray) {
        $this->programs = $programArray;    
    }

    public function getApplications() {
        return $this->applications;    
    }
    
    public function setApplications($applicationArray) {
        $this->applications = $applicationArray;    
    }
    
    public function addApplication($applicationId) {
    
        if ( $this->periodTypeId == 1 && !in_array($applicationId, $this->applications) ) {
            $dbPeriodApplication = new DB_PeriodApplication();
            $dbPeriodApplication->save($this->id, $applicationId);
            $this->loadApplications();             
        }        
    }

    public function getStatus() {
        
        $currentTimestamp = time();
        $startTimestamp = strtotime($this->startDate); 
        if ($this->endDate == NULL) {
            $endTimestamp = NULL;    
        } else {
            $endTimestamp = strtotime($this->endDate);    
        }
        
        if ($startTimestamp > $currentTimestamp) {
            return 'future';
        }
        
        if ($endTimestamp != NULL && $endTimestamp < $currentTimestamp) {
            return 'closed';    
        }
        
        if ($endTimestamp != NULL && $endTimestamp >= $currentTimestamp) {
            return 'active';    
        }

        if ($endTimestamp == NULL) {
            $parentPeriod = $this->getParentPeriod();
            if ($parentPeriod) {
                $parentPeriodEndDate = $parentPeriod->getEndDate();
                $parentPeriodEndTimestamp = strtotime($parentPeriodEndDate);
                if ( $parentPeriodEndTimestamp == NULL
                    || $parentPeriodEndTimestamp > $currentTimestamp ) {
                    return 'active';
                } else {
                    return 'closed';        
                }
            } else {
                // Must be an umbrella period.
                return 'active';
            } 
        }
        
        // return FALSE by default (should indicate an error condition)
        return FALSE;         
    }
    
    public function useHigherFee() {
        
        $currentTimestamp = time();
        $higherFeeTimestamp = strtotime($this->higherFeeDate);

        if ($this->higherFeeDate == NULL) {
            return FALSE;
        } else {
            if ($currentTimestamp > $higherFeeTimestamp) {
                return TRUE;
            }
        }  
        
        return FALSE;
    }
    
    public function beforeLastPaymentDate() {
        
        $currentTimestamp = time();
        $lastPaymentDateTimestamp = strtotime($this->lastPaymentDate);
        
        if ($this->lastPaymentDate == NULL) {
            return TRUE;
        } else {
            if ($currentTimestamp <= $lastPaymentDateTimestamp) {
                return TRUE;
            }
        }  
        
        return FALSE;
    }
    
    private function loadFromDb() {
        
        $dbPeriod = new DB_Period();
        
        $periodArray = $dbPeriod->get($this->id);
        foreach ($periodArray as $periodRecord) {           
            $this->unitId = $periodRecord['unit_id'];
            $this->periodTypeId = $periodRecord['period_type_id'];
            $this->description = $periodRecord['description'];
            $this->startDate = $periodRecord['start_date'];
            $this->endDate = $periodRecord['end_date'];
            if ($periodRecord['parent_period_id']) {
                $this->parentPeriodId = $periodRecord['parent_period_id'];
            }
        }

        $umbrellaPeriodQuery = "SELECT * FROM period_umbrella WHERE period_id = " . $this->id;
        $umbrellaPeriodArray = $dbPeriod->handleSelectQuery($umbrellaPeriodQuery);
        foreach ($umbrellaPeriodArray as $umbrellaPeriodRecord) {
            $this->name = $umbrellaPeriodRecord['umbrella_name'];
            if (isset($umbrellaPeriodRecord['higher_fee_date'])) {
                $this->higherFeeDate = $umbrellaPeriodRecord['higher_fee_date']; 
            }
            if (isset($umbrellaPeriodRecord['last_payment_date'])) {
                $this->lastPaymentDate = $umbrellaPeriodRecord['last_payment_date']; 
            }   
        }
        
        $childPeriodQuery = "SELECT * FROM period WHERE parent_period_id = " . $this->id;
        $childPeriodArray = $dbPeriod->handleSelectQuery($childPeriodQuery);
        foreach ($childPeriodArray as $childPeriodRecord) {
            $childPeriodId = $childPeriodRecord['period_id'];
            $this->childPeriods[] = new Period($childPeriodId);    
        }
        
        $this->loadPrograms();
        
        $this->loadApplications();

    }
    
    private function loadPrograms() {
    
        $dbPeriodProgram = new DB_PeriodProgram();
        
        $programsQuery = "SELECT unit.unit_id, unit.unit_name FROM period_program
                            INNER JOIN unit ON period_program.unit_id = unit.unit_id
                            WHERE period_program.period_id = " . $this->id;
        $programsArray = $dbPeriodProgram->handleSelectQuery($programsQuery);
        foreach ($programsArray as $programRecord) {
            $unitId = $programRecord['unit_id'];
            $unitName = $programRecord['unit_name'];
            $this->programs[$unitId] = $unitName;
        }     
    }
    
    private function loadApplications() {
    
        $this->applications = array();
        
        $dbPeriodApplication = new DB_PeriodApplication();
        $periodApplications = $dbPeriodApplication->get($this->id);
        foreach ($periodApplications as $periodApplication) {
            $this->applications[] = $periodApplication['application_id'];
        }        
    }
     
    public function getPeriodRecord() {
       $periodRecord = array(
            'period_id' => $this->id,
            'unit_id' => $this->unitId,
            'period_type_id' => $this->periodTypeId,
            'description' => $this->description,
            'start_date' => $this->startDate,
            'end_date' => $this->endDate,
            'parent_period_id' => $this->parentPeriodId
        );
        return $periodRecord;        
    }

    public function save() {
        
        $db_Period = new DB_Period();
        $periodRecord = $this->getPeriodRecord();
        $this->id = $db_Period->save($periodRecord);

        $db_UnitPeriod = new DB_UnitPeriod();
        $db_UnitPeriod->save( array('period_id' => $this->id, 'unit_id' => $this->unitId) );
        
        if ( count($this->programs) > 0 ) {            
            $db_PeriodProgram = new DB_PeriodProgram();
            $db_PeriodProgram->delete($this->id);
            foreach ( array_keys($this->programs) as $unitId) {
                $db_PeriodProgram->save( array('period_id' => $this->id, 'unit_id' => $unitId) );
            }
        }
        
        return TRUE;
    }

    protected function formatDate($date, $format = NULL) { 
        if($date == NULL) {
            return '';
        }
        if ($format) {
            $timestamp = strtotime($date);
            return date($format, $timestamp);    
        }
        // Return the unformatted date by default.
        return $date;
    }

}
?>