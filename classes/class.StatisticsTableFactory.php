<?php

class StatisticsTableFactory
{

    public function createTable($periodId, $unitId, $view, $grain, $grainId, $phdOnly,
                                $format = 'array', $includeHeadingRow = TRUE) {
    
        $format = strtolower($format);
        
        switch ($format) {
            
            case 'csv':
            
                $table = $this->tableCsv($periodId, $unitId, $view,
                            $grain, $grainId, $phdOnly);
                break;
            
            case 'datagrid':
            
                $table = $this->tableDatagrid($periodId, $unitId, $view, 
                            $grain, $grainId, $phdOnly);
                break;
                
            default:
            
                $table = $this->tableArray($periodId, $unitId, $view,
                            $grain, $grainId, $phdOnly);
           
        }
        
        return $table;
        
    }
    
    
    private function tableArray($periodId, $unitId, $view, $grain, $grainId, $phdOnly) {
        
        $dataFactory = new StatisticsDataFactory();
        $tableData = $dataFactory->createData($periodId, $unitId, $view, $grain, $grainId, $phdOnly); 
        return $tableData;
    }
    
    
    private function tableDatagrid($periodId, $unitId, $view, $grain, $grainId, $phdOnly) {
    
        $tableArray = $this->tableArray($periodId, $unitId, $view, $grain, $grainId, $phdOnly);
        $datagridFactory = new StatisticsDataGridFactory();
        $tableDatagrid = $datagridFactory->createDataGrid($tableArray, $unitId, $view, $grain);
        
        return $tableDatagrid;
    }
    
    
    private function tableCsv($unitId, $periodId, $view, $breakdown, $phdOnly) {
    
        $tableArray = $this->tableArray($unitId, $periodId, $view);
        return NULL;
        
    } 
    
}  

?>