<?php

/*
Class for retrieving lists of applications
*/

class DB_ApplicationList extends DB_Applyweb
{

    // PLB added application period checks 02/06/09
    protected $applicationPeriodWhere;
    
    
    /*
    // Return fields appropriate for domain admin list
    */
    function getByDomain($domain_id) {

        $query = 'select lu_users_usertypes.id as user_id,
            application.id as application_id, 
            concat(users.lastname, ", ", users.firstname) as name,
            users.email,
            
            GROUP_CONCAT(concat(" ",degree.name, " ", programs.linkword, " ", fieldsofstudy.name)) as program,
            
            if(application.sent_to_program = 1,"yes"," ") as cmp,
            if(application.submitted = 1,"yes"," ") as sbmd,
            application.paymentamount,
            if(application.paid = 1,"yes"," ") as pd,
            if(application.waive = 1,"yes"," ") as wd, 
            usersinst.transscriptreceived as trans_rcvd
            
            ,transcript_count.ct_transcripts_submitted
            ,transcript_count.ct_transcripts_received
            ,recommendation_count.ct_recommendations_requested
            ,recommendation_count.ct_recommendations_submitted 
            ,grescore.scorereceived AS gre_rcvd
            ,gresubject_count.ct_gresubject_entered
            ,gresubject_count.ct_gresubject_received
            ,toefl.toefl_entered
            ,toefl.toefl_submitted
            
            ,publications.ct_publications
            ,application.submitted_date as submitted
            ,application.created_date as created 

            from lu_users_usertypes
            inner join users on users.id = lu_users_usertypes.user_id
            inner join application on application.user_id = lu_users_usertypes.id
            
            left outer join lu_application_programs on lu_application_programs.application_id = application.id
            left outer join programs on programs.id = lu_application_programs.program_id
            left outer join degree on degree.id = programs.degree_id
            left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
            left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
            left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
          
            left outer join usersinst on usersinst.user_id = lu_users_usertypes.id
            left join grescore on application.id = grescore.application_id
            
            left outer join (
                SELECT user_id, 
                count( nullif( usersinst.institute_id, NULL ) ) AS ct_transcripts_submitted, 
                count( nullif( usersinst.transscriptreceived, 0 ) ) AS ct_transcripts_received
                FROM usersinst
                GROUP BY user_id            
            ) as transcript_count on transcript_count.user_id = lu_users_usertypes.id

           left outer join (
                SELECT application_id, 
                count(nullif(recommend.rec_user_id,-(1))) AS ct_recommendations_requested,
                count(
                    if (recommend.submitted = 0, NULL,
                        if ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 1)
                    )
                ) AS ct_recommendations_submitted
                FROM recommend
                GROUP BY application_id            
            ) as recommendation_count on recommendation_count.application_id = application.id 
            
            left outer join (
                SELECT application_id,
                /* PLB changed 1-2-09 */
                /* if(count(section1) >= 1,1,0) as toefl_entered, */
                if(count(testdate) >= 1,1,0) as toefl_entered,  
                if(count( nullif(scorereceived, 0) ) >= 1,1,0) as toefl_submitted
                from toefl
                /* PLB added where clause 1-2-09 */
                WHERE testdate IS NOT NULL
                AND testdate != "0000-00-01"
                AND testdate != "0000-00-00" 
                group by application_id
            ) as toefl on application.id = toefl.application_id

            left outer join (
                SELECT application_id,
                count( nullif( score, NULL) ) as ct_gresubject_entered, 
                count( nullif( scorereceived, 0 ) ) as ct_gresubject_received
                from gresubjectscore
                group by application_id
            ) as gresubject_count on application.id = gresubject_count.application_id
            
            left outer join (
                SELECT application_id,
                count( nullif( title, NULL) ) as ct_publications 
                from publication
                group by application_id
            ) as publications on application.id = publications.application_id

            WHERE lu_users_usertypes.usertype_id = 5
            AND application.submitted = 1
            AND lu_domain_department.domain_id = ' . $domain_id . ' 
            GROUP BY users.id
            ORDER BY name';
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }

    /*
    // Return fields appropriate for admin by department
    // PLB added 2/2/09
    // PLB added application period date params 2/6/09
    */
    function getAdminByDepartment($department_id, $start_date=null, $end_date=null) {

        // Set the application period, if applicable
        $this->setApplicationPeriodWhere($start_date, $end_date);
        
        $query = 'select lu_users_usertypes.id as user_id,
            application.id as application_id, 
            concat(users.lastname, ", ", users.firstname) as name,
            users.email,
            
            GROUP_CONCAT(concat(" ",degree.name, " ", programs.linkword, " ", fieldsofstudy.name)) as program,
            
            if(application.sent_to_program = 1,"yes"," ") as cmp,
            if(application.submitted = 1,"yes"," ") as sbmd,
            application.paymentamount,
            if(application.paid = 1,"yes"," ") as pd,
            if(application.waive = 1,"yes"," ") as wd, 
            usersinst.transscriptreceived as trans_rcvd
            
            ,transcript_count.ct_transcripts_submitted
            ,transcript_count.ct_transcripts_received
            ,recommendation_count.ct_recommendations_requested
            ,recommendation_count.ct_recommendations_submitted 
            ,grescore.scorereceived AS gre_rcvd
            ,gresubject_count.ct_gresubject_entered
            ,gresubject_count.ct_gresubject_received
            ,toefl.toefl_entered
            ,toefl.toefl_submitted
            
            ,publications.ct_publications 

            from lu_users_usertypes
            inner join users on users.id = lu_users_usertypes.user_id
            inner join application on application.user_id = lu_users_usertypes.id
            
            left outer join lu_application_programs on lu_application_programs.application_id = application.id
            left outer join programs on programs.id = lu_application_programs.program_id
            left outer join degree on degree.id = programs.degree_id
            left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
            left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
            /*
            left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
            */
          
            left outer join usersinst on usersinst.user_id = lu_users_usertypes.id
            left join grescore on application.id = grescore.application_id
            
            left outer join (
                SELECT user_id, 
                count( nullif( usersinst.institute_id, NULL ) ) AS ct_transcripts_submitted, 
                count( nullif( usersinst.transscriptreceived, 0 ) ) AS ct_transcripts_received
                FROM usersinst
                GROUP BY user_id            
            ) as transcript_count on transcript_count.user_id = lu_users_usertypes.id

           left outer join (
                SELECT application_id, 
                count(nullif(recommend.rec_user_id,-(1))) AS ct_recommendations_requested,
                count(
                    if (recommend.submitted = 0, NULL,
                        if ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 1)
                    )
                ) AS ct_recommendations_submitted
                FROM recommend
                GROUP BY application_id            
            ) as recommendation_count on recommendation_count.application_id = application.id 
            
            left outer join (
                SELECT application_id,
                /* PLB changed 1-2-09 */
                /* if(count(section1) >= 1,1,0) as toefl_entered, */
                if(count(testdate) >= 1,1,0) as toefl_entered,  
                if(count( nullif(scorereceived, 0) ) >= 1,1,0) as toefl_submitted
                from toefl
                /* PLB added where clause 1-2-09 */
                WHERE testdate IS NOT NULL
                AND testdate != "0000-00-01"
                AND testdate != "0000-00-00" 
                group by application_id
            ) as toefl on application.id = toefl.application_id

            left outer join (
                SELECT application_id,
                count( nullif( score, NULL) ) as ct_gresubject_entered, 
                count( nullif( scorereceived, 0 ) ) as ct_gresubject_received
                from gresubjectscore
                group by application_id
            ) as gresubject_count on application.id = gresubject_count.application_id
            
            left outer join (
                SELECT application_id,
                count( nullif( title, NULL) ) as ct_publications 
                from publication
                group by application_id
            ) as publications on application.id = publications.application_id

            WHERE lu_users_usertypes.usertype_id = 5
            AND application.submitted = 1
            ' . $this->applicationPeriodWhere . '
            AND lu_programs_departments.department_id = ' . $department_id . ' 
            GROUP BY users.id
            ORDER BY name';
      
        //echo $query;
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }

   /*
    // Return fields appropriate for promotion/demotion by department
    // PLB added 2/2/09
    // PLB added application period date params 2/6/09
    */
    function getPromoteByDepartment($department_id, $start_date=null, $end_date=null, $periodId=NULL) {

        // Set the application period, if applicable
        $this->setApplicationPeriodWhere($start_date, $end_date);
        
        $query = 'select lu_users_usertypes.id as user_id,
            application.id as application_id,
            programs.id as program_id, 
            concat(users.lastname, ", ", users.firstname) as name,
            users.email,
            /*           
            GROUP_CONCAT(concat(" ",degree.name, " ", programs.linkword, " ", fieldsofstudy.name)) as program,
            */
            concat(" ",degree.name, " ", programs.linkword, " ", fieldsofstudy.name) as program,            
            lu_application_programs.round2 as promotion_status

            from lu_users_usertypes
            inner join users on users.id = lu_users_usertypes.user_id
            inner join application on application.user_id = lu_users_usertypes.id';
            if ($periodId) {
                $query .= ' INNER JOIN period_application ON period_application.application_id = application.id';
            }
            $query .= ' left outer join lu_application_programs on lu_application_programs.application_id = application.id
            left outer join programs on programs.id = lu_application_programs.program_id
            left outer join degree on degree.id = programs.degree_id
            left outer join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
            left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id

            WHERE lu_users_usertypes.usertype_id = 5
            AND application.submitted = 1
            ' . $this->applicationPeriodWhere . ' 
            AND lu_programs_departments.department_id = ' . $department_id;
            if ($periodId) {
                if (is_array($periodId)) {
                    $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
                    $query .= " AND period_application.period_id IN " . $admissionPeriodIds;
                } else {
                    $query .= " AND period_application.period_id = " . $periodId;   
                }  
            }
            if (!($department_id == 1 /* || $department_id == 74 */
                || $department_id == 66 || $department_id == 50 || $department_id == 97)) {
                // Payment required for all departments except CSD, CS MS, MRSD, RI-Scholars. 
                $query .= " AND (application.paid = 1 OR application.waive = 1)";    
            }
            $query .= ' /* GROUP BY users.id */
            ORDER BY name';
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }

   /*
    // Return fields appropriate for recording acceptance of offer status by department
    // PLB added 2/3/09
    // PLB added application period date params 2/6/09
    */
    function getInvitedByDepartment($department_id, $start_date=null, 
        $end_date=null, $periodId=NULL, $isIniDepartment = FALSE, $isDesignDepartment = FALSE) {

        // Set the application period, if applicable
        $this->setApplicationPeriodWhere($start_date, $end_date);

        $query = "SELECT DISTINCT 
        lu_users_usertypes.id as user_id,
        application.id AS application_id,";
        
        if ($isIniDepartment) 
        {
            $query .= "application_decision_ini.admission_program_id AS program_id, 
                application_decision_ini.admission_program_id,";  
        }
        elseif ($isDesignDepartment) 
        {
            $query .= "application_decision_design.admission_program_id AS program_id, 
                application_decision_design.admission_program_id,";  
        } 
        else 
        {
            $query .= " programs.id AS program_id, 
                application_decision.admission_program_id,";        
        }
        
        
        $query .= " CONCAT(users.lastname, ', ',users.firstname) AS name,
        users.email,";
        
        if ($isIniDepartment) 
        {
            $query .= " IF( admit_programs2.id,
                        CONCAT(' ', admit_degree2.name, ' ', admit_programs2.linkword, ' ', admit_fieldsofstudy2.name),
                        CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) 
                    ) AS program,
                    application_decision_ini.admission_status as admission_status,";  
        }
        elseif ($isDesignDepartment) 
        {
            $query .= " IF( admit_programs2.id,
                        CONCAT(' ', admit_degree2.name, ' ', admit_programs2.linkword, ' ', admit_fieldsofstudy2.name),
                        CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) 
                    ) AS program,
                    application_decision_design.admission_status as admission_status,";  
        } 
        else 
        {
            $query .= " IF( admit_programs.id,
                        CONCAT(' ', admit_degree.name, ' ', admit_programs.linkword, ' ', admit_fieldsofstudy.name),
                        CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) 
                    ) AS program,
                    lu_application_programs.admission_status as admission_status,";        
        }
        
        $query .= " student_decision.decision as decision

        FROM application
        INNER JOIN period_application ON application.id = period_application.application_id

        INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
        INNER JOIN users ON lu_users_usertypes.user_id = users.id
        INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
        LEFT OUTER JOIN countries ON users_info.cit_country = countries.id

        INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
        INNER JOIN programs ON lu_application_programs.program_id = programs.id
        INNER JOIN degree ON programs.degree_id = degree.id
        INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
        INNER JOIN lu_programs_departments
          ON lu_application_programs.program_id = lu_programs_departments.program_id

        ";
                    
        if ($isIniDepartment)
        {
            $query .= " LEFT OUTER JOIN application_decision_ini
                  ON lu_application_programs.application_id = application_decision_ini.application_id
                LEFT OUTER JOIN programs AS admit_programs2
                  ON admit_programs2.id = application_decision_ini.admission_program_id
                LEFT OUTER JOIN degree AS admit_degree2
                  ON admit_degree2.id = admit_programs2.degree_id
                LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy2
                  ON admit_fieldsofstudy2.id = admit_programs2.fieldofstudy_id
                LEFT OUTER JOIN student_decision 
                  ON application.id = student_decision.application_id 
                  AND admit_programs2.id = student_decision.program_id
                WHERE application.submitted = 1
                AND (lu_application_programs.admission_status = 2
                  OR application_decision_ini.admission_status = 2)
                AND lu_programs_departments.department_id = " . $department_id;
        }
        elseif ($isDesignDepartment)
        {
            $query .= " LEFT OUTER JOIN application_decision_design
                  ON lu_application_programs.application_id = application_decision_design.application_id
                LEFT OUTER JOIN programs AS admit_programs2
                  ON admit_programs2.id = application_decision_design.admission_program_id
                LEFT OUTER JOIN degree AS admit_degree2
                  ON admit_degree2.id = admit_programs2.degree_id
                LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy2
                  ON admit_fieldsofstudy2.id = admit_programs2.fieldofstudy_id
                LEFT OUTER JOIN student_decision 
                  ON application.id = student_decision.application_id 
                  AND admit_programs2.id = student_decision.program_id
                WHERE application.submitted = 1
                AND (lu_application_programs.admission_status = 2
                  OR application_decision_design.admission_status = 2)
                AND lu_programs_departments.department_id = " . $department_id;
        }
        else
        {
            $query .= " 
            LEFT OUTER JOIN application_decision
          ON application.id = application_decision.application_id
          AND lu_application_programs.program_id = application_decision.program_id
        LEFT OUTER JOIN programs AS admit_programs
          ON application_decision.admission_program_id = admit_programs.id
        LEFT OUTER JOIN degree AS admit_degree
          ON admit_programs.degree_id = admit_degree.id
        LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy
          ON admit_programs.fieldofstudy_id = admit_fieldsofstudy.id
            LEFT OUTER JOIN student_decision 
                  ON application.id = student_decision.application_id 
                  AND lu_application_programs.program_id = student_decision.program_id
                WHERE application.submitted = 1
                AND (lu_application_programs.admission_status = 2
                  OR application_decision.admission_status = 2)
                AND lu_programs_departments.department_id = " . $department_id;    
        }

        if ($periodId) {
            $query .= " AND period_application.period_id = " .  $periodId;
        }
        if ($isIniDepartment)
        {
            $query .= " AND application_decision_ini.admission_status = 2";
        }
        elseif ($isDesignDepartment)
        {
            $query .= " AND application_decision_design.admission_status = 2";
        } 
        
        $query .= " ORDER BY name";
    
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }
 
    
    // Function to set where clause for limiting by application period   
    function setApplicationPeriodWhere($start_date=null, $end_date=null) {
    
        if ($start_date != NULL) {
        
            if ($end_date != NULL) {
            
                $this->applicationPeriodWhere = ' AND ( CAST(application.submitted_date AS DATE) BETWEEN ';
                $this->applicationPeriodWhere .= 'CAST("' . $start_date . '" AS DATE) AND ';
                $this->applicationPeriodWhere .= 'CAST("' . $end_date . '" AS DATE))';
                
            } else {
                
                $this->applicationPeriodWhere = 'AND ( CAST(application.submitted_date AS DATE) > ';
                $this->applicationPeriodWhere .= 'CAST("' . $start_date . '" AS DATE))';
                
            }
 
        } else {
        
            $this->applicationPeriodWhere = "";
            
        }
          
    }
    
    
}

?>
