<?php
include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_ReviewList.php";
include "../classes/DB_Applyweb/class.VW_ReviewDomainListBase.php";
include "../classes/DB_Applyweb/class.VW_ReviewDomainListPrograms.php";

class ReviewDomainListData
{
    private $unit;
    private $unitId;
    private $unitName;
    private $data;          // 2D array
    
    public function __construct($unit, $unitId, $unitName = null) 
    {
        $this->unit = $unit;
        $this->unitId = $unitId;    
        $this->unitName = $unitName;
    }
    
    public function getData(
        $periodId = NULL, 
        $reviewerId = NULL, 
        $groups = 'allGroups', 
        $searchString = '') 
    {    
        $groupsLimitReviewerId = NULL;
        if ( $reviewerId && ($groups == 'myGroups') ) {
            $groupsLimitReviewerId = $reviewerId;    
        }
        
        $baseView = new VW_ReviewDomainListBase($this->unitName);
        $baseArray = $baseView->find($this->unit, $this->unitId, $periodId, $groupsLimitReviewerId, $searchString);
        
        $programView = new VW_ReviewDomainListPrograms($this->unitName);
        $programArray = $programView->find($this->unit, $this->unitId, $periodId);
        
        $recordCount = count($baseArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $applicationId = $baseArray[$i]['application_id'];
    
            $baseArray[$i] = array_merge($baseArray[$i], $programArray[$applicationId]);
        }    

        return $baseArray;
    }
}

?>
