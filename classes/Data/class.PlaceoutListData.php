<?php

class PlaceoutListData
{

    private $unit;
    private $unitId;
    
    public function __construct($unit, $unitId) {
        $this->unit = $unit;
        $this->unitId = $unitId;    
    }

    /* remove statistics
    public function getData($periodId = NULL, $applicationId = NULL, $luUsersUsertypesId = NULL, 
        $searchString = '', $designStatus = '', $programmingStatus = '', $statisticsStatus = '') {
        */
    public function getData($periodId = NULL, $applicationId = NULL, $luUsersUsertypesId = NULL, 
        $searchString = '', $designStatus = '', $programmingStatus = '') {
        $statisticsStatus = '';
        $statusRaw = $this->getPrereqsStatus($periodId);
        $prereqsIndexed = array();
        foreach ($statusRaw as $statusRecord) {
            $mhciPrereqsId = $statusRecord['mhci_prereqs_id'];
            $applicationId = $statusRecord['application_id'];
            $programId = $statusRecord['program_id'];
            $prereqType = $statusRecord['prereq_type'];
            $email = $statusRecord['email'];
            $prereqsIndexed[$applicationId][$programId][$prereqType] = $statusRecord;   
        } 
        
        $VW_PlaceoutListBase = new VW_PlaceoutListBase(); 
        $dataArray = $VW_PlaceoutListBase->find($this->unit, $this->unitId, $periodId,
            $applicationId, $luUsersUsertypesId, $searchString,
            $designStatus, $programmingStatus, $statisticsStatus);
        
        $recordCount = count($dataArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $applicationId = $dataArray[$i]['application_id'];
            $programId = $dataArray[$i]['program_id'];    
            $email =  $dataArray[$i]['email'];
            
            if ( isset($prereqsIndexed[$applicationId][$programId]['design']) ) {
                $dataArray[$i]['design_reviewer_status'] = 
                    $prereqsIndexed[$applicationId][$programId]['design']['reviewer_status'];
                $dataArray[$i]['design_reviewer_explanation'] = 
                    $prereqsIndexed[$applicationId][$programId]['design']['reviewer_explanation'];
            } else {
                $dataArray[$i]['design_reviewer_status'] = NULL;
                $dataArray[$i]['design_reviewer_explanation'] = NULL;    
            }

            if ( isset($prereqsIndexed[$applicationId][$programId]['programming']) ) {
                $dataArray[$i]['programming_reviewer_status'] = 
                    $prereqsIndexed[$applicationId][$programId]['programming']['reviewer_status'];
                $dataArray[$i]['programming_reviewer_explanation'] = 
                    $prereqsIndexed[$applicationId][$programId]['programming']['reviewer_explanation'];                
            } else {
                $dataArray[$i]['programming_reviewer_status'] = NULL;
                $dataArray[$i]['programming_reviewer_explanation'] = NULL;                
            }
            /*
            if ( isset($prereqsIndexed[$applicationId][$programId]['statistics']) ) {
                $dataArray[$i]['statistics_reviewer_status'] = 
                    $prereqsIndexed[$applicationId][$programId]['statistics']['reviewer_status'];
                $dataArray[$i]['statistics_reviewer_explanation'] = 
                    $prereqsIndexed[$applicationId][$programId]['statistics']['reviewer_explanation'];                
            } else {
                $dataArray[$i]['statistics_reviewer_status'] = NULL;
                $dataArray[$i]['statistics_reviewer_explanation'] = NULL;                
            }            
            */
        }  
        
        return $dataArray;
    }



    private function getPrereqsStatus($periodId) { 

        $DB_Applyweb = new DB_Applyweb();
        
        $query = "SELECT mhci_prereqs.application_id, 
            mhci_prereqs.prereq_type,
            mhci_prereqs.student_assessment,
            mhci_prereqs.timestamp AS student_timestamp,
            mhci_prereqs_status.*
            FROM mhci_prereqs
            LEFT OUTER JOIN mhci_prereqs_status  
                ON mhci_prereqs_status.mhci_prereqs_id = mhci_prereqs.id

            WHERE mhci_prereqs.period_id = " . $periodId;
 
        return $DB_Applyweb->handleSelectQuery($query);         
    }    


}

?>