<?php

class ApplicantsByGenderData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $grain;
    private $grainId;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'total', $grainId = '0', $phdOnly = 0,
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        $this->grain = $grain;
        $this->grainId = $grainId;
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions;
        
        $dataArray = $this->getTotalSubmitted();
        $admittedArray = $this->getAdmitted();
        $waitlistedArray = $this->getWaitlisted();
        $acceptedArray = $this->getAccepted();
        $deferredArray = $this->getDeferred(); 
        
        $recordCount = count($dataArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $unitId = $dataArray[$i]['gender'];
            if ( isset($admittedArray[$unitId]) ) {
                $dataArray[$i]['admitted_count'] = $admittedArray[$unitId]['admitted_count'];    
            } else {
                $dataArray[$i]['admitted_count'] = NULL;    
            }
            if ( isset($waitlistedArray[$unitId]) ) {
                $dataArray[$i]['waitlisted_count'] = $waitlistedArray[$unitId]['waitlisted_count'];    
            } else {
                $dataArray[$i]['waitlisted_count'] = NULL;    
            }
            if ( isset($acceptedArray[$unitId]) ) {
                $dataArray[$i]['accepted_count'] = $acceptedArray[$unitId]['accepted_count'];    
            } else {
                $dataArray[$i]['accepted_count'] = NULL;    
            }            
            if ( isset($deferredArray[$unitId]) ) {
                $dataArray[$i]['deferred_count'] = $deferredArray[$unitId]['deferred_count'];    
            } else {
                $dataArray[$i]['deferred_count'] = NULL;    
            }            
        }
        
        return $dataArray;
    }
    
    private function getTotalSubmitted() {
        
        $query = "SELECT gender,
            count(DISTINCT application.id) AS submitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender 
                    ORDER BY submitted_count DESC";
                    
        return $this->DB_Applyweb->handleSelectQuery($query); 
    }

    private function getAdmitted() {
        
        $query = "SELECT gender, 
            count(DISTINCT application.id) AS admitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 2
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender";
                    
        return $this->DB_Applyweb->handleSelectQuery($query, 'gender'); 
    }
    
    private function getWaitlisted() {
        
        $query = "SELECT gender, 
            count(DISTINCT application.id) AS waitlisted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 1
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender";

        return $this->DB_Applyweb->handleSelectQuery($query, 'gender');  
    }

    private function getAccepted() {

        $query = "SELECT gender, 
            count(DISTINCT application.id) AS accepted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'accept' 
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender";

        return $this->DB_Applyweb->handleSelectQuery($query, 'gender'); 
    }    

    private function getDeferred() {
        
        $query = "SELECT gender, 
            count(DISTINCT application.id) AS deferred_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'defer' 
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender";
            
        return $this->DB_Applyweb->handleSelectQuery($query, 'gender'); 
    }
    
}

?>