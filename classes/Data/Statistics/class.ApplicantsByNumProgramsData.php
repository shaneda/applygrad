<?php

class ApplicantsByNumProgramsData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $grain;
    private $grainId;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'total', $grainId = '0', $phdOnly = 0,
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        $this->grain = $grain;
        $this->grainId = $grainId;
        $this->phdOnly = $phdOnly; 
        $this->round1Conditions = $round1Conditions;     
        
        $dataArray = $this->getSubmitted();

        return $dataArray;
    }

    private function getSubmitted() {
        
        $query = "select program_count.program_count AS num_programs,
            COUNT(DISTINCT application.id) AS submitted_count
            FROM 
            period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments 
                ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN
            (SELECT application.id AS application_id,
            count(DISTINCT lu_application_programs.id) AS program_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId;
        $query .= " GROUP BY application.id) 
            AS program_count ON application.id = program_count.application_id"; 
        $query .= " WHERE " . $this->round1Conditions;
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY program_count.program_count"; 
        $query .= " ORDER BY submitted_count DESC";

        return $this->DB_Applyweb->handleSelectQuery($query); 
    }

}

?>