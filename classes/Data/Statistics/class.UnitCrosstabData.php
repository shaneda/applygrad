<?php

class UnitCrosstabData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $selectId;
    private $selectName;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'department', $grainId = '0', $phdOnly = 0,
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        switch ($grain) {           
            case 'program':
                $this->selectId = "program_new.unit_id";
                $this->selectName = "program_new.unit_name_short";
                break;    
            case 'department':
            default:
                $this->selectId = "department.id";
                $this->selectName = "department.name"; 
        }
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions;        
        
        $dataArray = $this->getFirstChoiceCount();

        $crosstabCountsIndexed = array();
        $crosstabCountsRaw = $this->getCrosstabCount($unitId, $periodId);
        foreach ($crosstabCountsRaw as $crosstabRaw) {
            $firstChoiceDepartment = $crosstabRaw['first_choice_name'];
            $otherChoiceDepartment = $crosstabRaw['other_choice_name'];
            $crosstabCount = $crosstabRaw['crosstab_count'];
            $crosstabCountsIndexed[$firstChoiceDepartment][$otherChoiceDepartment] = $crosstabCount;   
        }
        
        $crosstabHeadings = array();
        foreach ($dataArray as $row) {
                $crosstabHeadings[] = $row['unit_name'];
        }
        
        $recordCount = count($dataArray);
        for ($i = 0; $i < $recordCount; $i++) {           
            $firstChoice = $dataArray[$i]['unit_name'];
            $firstChoiceCount = $dataArray[$i]['unit_count'];
            foreach ($crosstabHeadings as $crossHeading) {
                if ($firstChoice == $crossHeading) {
                    $dataArray[$i][$crossHeading] = $firstChoiceCount;  
                } else {
                    if ( isset($crosstabCountsIndexed[$firstChoice][$crossHeading]) ) {
                        $dataArray[$i][$crossHeading] = $crosstabCountsIndexed[$firstChoice][$crossHeading];    
                    } else {
                        $dataArray[$i][$crossHeading] = NULL;    
                    }
                }   
            }
        }

        return $dataArray;
    }
    

    private function getFirstChoiceCount() {
        
        $query = "SELECT " . $this->selectName . " AS unit_name,  
            count(DISTINCT application.id) AS unit_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND lu_application_programs.choice = 1
            AND period_application.period_id = " . $this->periodId;
            if ($this->phdOnly) {
                $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
            }
        $query .= " GROUP BY " . $this->selectName;
        $query .= " ORDER BY unit_count DESC";
        
        return $this->DB_Applyweb->handleSelectQuery($query); 
        
    }

     private function getCrosstabCount() {
        
        $query = "SELECT " . $this->selectName . " AS first_choice_name,
            other_unit.other_choice_name,
            count(DISTINCT application.id) AS crosstab_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            INNER JOIN (
                SELECT DISTINCT lu_application_programs.application_id,
                ". $this->selectId . " AS other_choice_id,
                " . $this->selectName . " AS other_choice_name
                FROM period_application
                INNER JOIN application ON period_application.application_id = application.id
                INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
                LEFT OUTER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
                LEFT OUTER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
                LEFT OUTER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id 
                INNER JOIN lu_programs_departments
                  ON lu_application_programs.program_id = lu_programs_departments.program_id
                INNER JOIN department
                  ON lu_programs_departments.department_id = department.id
                WHERE " . $this->round1Conditions . "
                AND period_application.period_id = " . $this->periodId ."
                AND lu_application_programs.choice > 1";
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= ") AS other_unit
              ON application.id = other_unit.application_id
              AND " . $this->selectId . " != other_unit.other_choice_id
            WHERE " . $this->round1Conditions . "
            AND lu_application_programs.choice = 1
            AND period_application.period_id = " . $this->periodId;
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY first_choice_name, other_choice_name";

        return $this->DB_Applyweb->handleSelectQuery($query);         
    }
    
}

?>