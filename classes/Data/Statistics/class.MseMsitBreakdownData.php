<?php

class MseMsitBreakdownData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $grain;
    private $grainId;
    private $selectField;
    private $groupByField;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'mse', $grainId = '21', $phdOnly = 0, 
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        if ($grain) {
            $this->grain = $grain;    
        } else {
            $this->grain = 'mse';    
        }
        if ($grainId) {
            $this->grainId = $grainId;    
        } else {
            $this->grainId = 21;    
        }        
        
        $this->selectField = $this->groupByField = "program_new.unit_name";
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions;    

        $dataArray = $this->getTotalSubmitted();
        
        $mseAdmittedMseArray = $this->getAdmitted(21, 21);
        $mseAdmittedMsitArray = $this->getAdmitted(21, 29);
        $mbaAdmittedMseArray = $this->getAdmitted(30, 30);
        $mbaAdmittedMsitArray = $this->getAdmitted(30, 29);
        $msitAdmittedMsitArray = $this->getAdmitted(29, 29);

        $mseWaitlistedMseArray = $this->getWaitlisted(21, 21);
        $mseWaitlistedMsitArray = $this->getWaitlisted(21, 29);
        $mbaWaitlistedMseArray = $this->getWaitlisted(30, 30);
        $mbaWaitlistedMsitArray = $this->getWaitlisted(30, 29);
        $msitWaitlistedMsitArray = $this->getWaitlisted(29, 29);
        
        $mseAcceptedMseArray = $this->getAccepted(21, 21);
        $mseAcceptedMsitArray = $this->getAccepted(21, 29);
        $mbaAcceptedMseArray = $this->getAccepted(30, 30);
        $mbaAcceptedMsitArray = $this->getAccepted(30, 29);
        $msitAcceptedMsitArray = $this->getAccepted(29, 29);        
        
        $mseDeferredMseArray = $this->getDeferred(21, 21);
        $mseDeferredMsitArray = $this->getDeferred(21, 29);
        $mbaDeferredMseArray = $this->getDeferred(30, 30);
        $mbaDeferredMsitArray = $this->getDeferred(30, 29);
        $msitDeferredMsitArray = $this->getDeferred(29, 29);         
        
        $recordCount = count($dataArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $index = 0;
            
            // Admitted
            if ( isset($mseAdmittedMseArray[$index]['admitted_count']) ) {
                $dataArray[$i]['mse_admitted_mse_count'] = $mseAdmittedMseArray[$index]['admitted_count'];     
            } else {
                $dataArray[$i]['mse_admitted_mse_count'] = NULL;     
            }
            if ( isset($mseAdmittedMsitArray[$index]['admitted_count']) ) {
                $dataArray[$i]['mse_admitted_msit_count'] = $mseAdmittedMsitArray[$index]['admitted_count'];     
            } else {
                $dataArray[$i]['mse_admitted_msit_count'] = NULL;     
            }
            if ( isset($mbaAdmittedMseArray[$index]['admitted_count']) ) {
                $dataArray[$i]['mba_admitted_mse_count'] = $mbaAdmittedMseArray[$index]['admitted_count'];     
            } else {
                $dataArray[$i]['mba_admitted_mse_count'] = NULL;     
            }
            if ( isset($mbaAdmittedMsitArray[$index]['admitted_count']) ) {
                $dataArray[$i]['mba_admitted_msit_count'] = $mbaAdmittedMsitArray[$index]['admitted_count'];     
            } else {
                $dataArray[$i]['mba_admitted_msit_count'] = NULL;     
            }
            if ( isset($msitAdmittedMsitArray[$index]['admitted_count']) ) {
                $dataArray[$i]['msit_admitted_msit_count'] = $msitAdmittedMsitArray[$index]['admitted_count'];     
            } else {
                $dataArray[$i]['msit_admitted_msit_count'] = NULL;     
            }
            
            // Waitlisted
            if ( isset($mseWaitlistedMseArray[$index]['waitlisted_count']) ) {
                $dataArray[$i]['mse_waitlisted_mse_count'] = $mseWaitlistedMseArray[$index]['waitlisted_count'];     
            } else {
                $dataArray[$i]['mse_waitlisted_mse_count'] = NULL;     
            }
            if ( isset($mseWaitlistedMsitArray[$index]['waitlisted_count']) ) {
                $dataArray[$i]['mse_waitlisted_msit_count'] = $mseWaitlistedMsitArray[$index]['waitlisted_count'];     
            } else {
                $dataArray[$i]['mse_waitlisted_msit_count'] = NULL;     
            }
            if ( isset($mbaWaitlistedMseArray[$index]['waitlisted_count']) ) {
                $dataArray[$i]['mba_waitlisted_mse_count'] = $mbaWaitlistedMseArray[$index]['waitlisted_count'];     
            } else {
                $dataArray[$i]['mba_waitlisted_mse_count'] = NULL;     
            }
            if ( isset($mbaWaitlistedMsitArray[$index]['waitlisted_count']) ) {
                $dataArray[$i]['mba_waitlisted_msit_count'] = $mbaWaitlistedMsitArray[$index]['waitlisted_count'];     
            } else {
                $dataArray[$i]['mba_waitlisted_msit_count'] = NULL;     
            }
            if ( isset($msitWaitlistedMsitArray[$index]['waitlisted_count']) ) {
                $dataArray[$i]['msit_waitlisted_msit_count'] = $msitWaitlistedMsitArray[$index]['waitlisted_count'];     
            } else {
                $dataArray[$i]['msit_waitlisted_msit_count'] = NULL;     
            }
            
            // Accepted
            if ( isset($mseAcceptedMseArray[$index]['accepted_count']) ) {
                $dataArray[$i]['mse_accepted_mse_count'] = $mseAcceptedMseArray[$index]['accepted_count'];     
            } else {
                $dataArray[$i]['mse_accepted_mse_count'] = NULL;     
            }
            if ( isset($mseAcceptedMsitArray[$index]['accepted_count']) ) {
                $dataArray[$i]['mse_accepted_msit_count'] = $mseAcceptedMsitArray[$index]['accepted_count'];     
            } else {
                $dataArray[$i]['mse_accepted_msit_count'] = NULL;     
            }
            if ( isset($mbaAcceptedMseArray[$index]['accepted_count']) ) {
                $dataArray[$i]['mba_accepted_mse_count'] = $mbaAcceptedMseArray[$index]['accepted_count'];     
            } else {
                $dataArray[$i]['mba_accepted_mse_count'] = NULL;     
            }
            if ( isset($mbaAcceptedMsitArray[$index]['accepted_count']) ) {
                $dataArray[$i]['mba_accepted_msit_count'] = $mbaAcceptedMsitArray[$index]['accepted_count'];     
            } else {
                $dataArray[$i]['mba_accepted_msit_count'] = NULL;     
            }
            if ( isset($msitAcceptedMsitArray[$index]['accepted_count']) ) {
                $dataArray[$i]['msit_accepted_msit_count'] = $msitAcceptedMsitArray[$index]['accepted_count'];     
            } else {
                $dataArray[$i]['msit_accepted_msit_count'] = NULL;     
            }     

            // Deferred
            if ( isset($mseDeferredMseArray[$index]['deferred_count']) ) {
                $dataArray[$i]['mse_deferred_mse_count'] = $mseDeferredMseArray[$index]['deferred_count'];     
            } else {
                $dataArray[$i]['mse_deferred_mse_count'] = NULL;     
            }
            if ( isset($mseDeferredMsitArray[$index]['deferred_count']) ) {
                $dataArray[$i]['mse_deferred_msit_count'] = $mseDeferredMsitArray[$index]['deferred_count'];     
            } else {
                $dataArray[$i]['mse_deferred_msit_count'] = NULL;     
            }
            if ( isset($mbaDeferredMseArray[$index]['deferred_count']) ) {
                $dataArray[$i]['mba_deferred_mse_count'] = $mbaDeferredMseArray[$index]['deferred_count'];     
            } else {
                $dataArray[$i]['mba_deferred_mse_count'] = NULL;     
            }
            if ( isset($mbaDeferredMsitArray[$index]['deferred_count']) ) {
                $dataArray[$i]['mba_deferred_msit_count'] = $mbaDeferredMsitArray[$index]['deferred_count'];     
            } else {
                $dataArray[$i]['mba_deferred_msit_count'] = NULL;     
            }
            if ( isset($msitDeferredMsitArray[$index]['deferred_count']) ) {
                $dataArray[$i]['msit_deferred_msit_count'] = $msitDeferredMsitArray[$index]['deferred_count'];     
            } else {
                $dataArray[$i]['msit_deferred_msit_count'] = NULL;     
            }  
        }
        
        return $dataArray;
    }
    
    private function getTotalSubmitted() {

        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS submitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND period_application.period_id = " . $this->periodId . "
            AND lu_application_programs.program_id = " . $this->grainId;            
        $query .= " GROUP BY " . $this->groupByField;  

        return $this->DB_Applyweb->handleSelectQuery($query); 
    }


    private function getAdmitted($applyProgramId, $admitProgramId) {
        
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS admitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND application_decision.admission_status = 2
            AND application_decision.program_id = " . $applyProgramId . "
            AND application_decision.admission_program_id = " . $admitProgramId . "
            AND period_application.period_id = " . $this->periodId;            
        $query .= " GROUP BY " . $this->groupByField;
 
        return $this->DB_Applyweb->handleSelectQuery($query); 
    }
        
    private function getWaitlisted($applyProgramId, $admitProgramId) {
        
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS waitlisted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND application_decision.admission_status = 1
            AND application_decision.program_id = " . $applyProgramId . "
            AND application_decision.admission_program_id = " . $admitProgramId . "
            AND period_application.period_id = " . $this->periodId;            
        $query .= " GROUP BY " . $this->groupByField;

        return $this->DB_Applyweb->handleSelectQuery($query); 
        
    }

    private function getAccepted($applyProgramId, $admitProgramId) {

        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS accepted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND student_decision.decision = 'accept'
            AND application_decision.program_id = " . $applyProgramId . "
            AND application_decision.admission_program_id = " . $admitProgramId . "
            AND period_application.period_id = " . $this->periodId . " 
            GROUP BY " . $this->groupByField;

        return $this->DB_Applyweb->handleSelectQuery($query);         
    }    

    private function getDeferred($applyProgramId, $admitProgramId) {
        
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS deferred_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.program_id = programs_unit.programs_id 
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND student_decision.decision = 'defer'
            AND application_decision.program_id = " . $applyProgramId . "
            AND application_decision.admission_program_id = " . $admitProgramId . "
            AND period_application.period_id = " . $this->periodId . " 
            GROUP BY " . $this->groupByField;

        return $this->DB_Applyweb->handleSelectQuery($query);
    }
    
}

?>