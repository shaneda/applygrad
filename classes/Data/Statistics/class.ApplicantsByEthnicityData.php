<?php

class ApplicantsByEthnicityData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $grain;
    private $grainId;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'total', $grainId = '0', $phdOnly = 0,
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        $this->grain = $grain;
        $this->grainId = $grainId;
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions;
        
        $baseArray = $this->getPeriodSubmitted();
        $submittedArray = $this->getGrainSubmitted();
        $admittedArray = $this->getAdmitted();
        $waitlistedArray = $this->getWaitlisted();
        $acceptedArray = $this->getAccepted();
        $deferredArray = $this->getDeferred(); 
        
        $dataArray = array();
        $recordCount = count($baseArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $index = $baseArray[$i]['ethnicity'];
            $includeRecord = FALSE;
            
            if ( isset($submittedArray[$index]) ) {
                $baseArray[$i]['submitted_count'] = $submittedArray[$index]['submitted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['submitted_count'] = NULL;    
            }
            
            if ( isset($admittedArray[$index]) ) {
                $baseArray[$i]['admitted_count'] = $admittedArray[$index]['admitted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['admitted_count'] = NULL;    
            }
            
            if ( isset($waitlistedArray[$index]) ) {
                $baseArray[$i]['waitlisted_count'] = $waitlistedArray[$index]['waitlisted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['waitlisted_count'] = NULL;    
            }
            
            if ( isset($acceptedArray[$index]) ) {
                $baseArray[$i]['accepted_count'] = $acceptedArray[$index]['accepted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['accepted_count'] = NULL;    
            }
                        
            if ( isset($deferredArray[$index]) ) {
                $baseArray[$i]['deferred_count'] = $deferredArray[$index]['deferred_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['deferred_count'] = NULL;    
            }
            
            // Exclude "empty" table rows.
            if ($includeRecord) {
                $dataArray[] = $baseArray[$i];    
            }            
        }
        
        return $dataArray;
    }

    private function getPeriodSubmitted() {
            
        $query = "SELECT ethnicity,
            count(DISTINCT application_id) AS submitted_count
            FROM (
            (SELECT ethnicity.name AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NOT NULL)
            UNION
            (SELECT ipeds_race.ipeds_race AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . "
            AND ipeds_race.ipeds_race IS NOT NULL)
            UNION
            (SELECT 'Unknown' AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NULL
            AND ipeds_race.ipeds_race IS NULL
            )
            ) AS all_ethnicity
            WHERE application_submitted = 1
            GROUP BY ethnicity 
            ORDER BY submitted_count DESC";
    
        return $this->DB_Applyweb->handleSelectQuery($query);    
    }

    private function getGrainSubmitted() {
            
        $query = "SELECT ethnicity,
            count(DISTINCT application_id) AS submitted_count
            FROM (
            (SELECT ethnicity.name AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NOT NULL)
            UNION
            (SELECT ipeds_race.ipeds_race AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . "
            AND ipeds_race.ipeds_race IS NOT NULL)
            UNION
            (SELECT 'Unknown' AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NULL
            AND ipeds_race.ipeds_race IS NULL
            )
            ) AS all_ethnicity
            WHERE application_submitted = 1";
        if ($this->grain == 'program') {
            $query .= " AND program_unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND old_degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY ethnicity"; 
    
        return $this->DB_Applyweb->handleSelectQuery($query, 'ethnicity');    
    }

    private function getAdmitted() {
    
        $query = "SELECT ethnicity,
            count(DISTINCT application_id) AS admitted_count
            FROM (
            (SELECT ethnicity.name AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id 
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 2
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NOT NULL)
            UNION
            (SELECT ipeds_race.ipeds_race AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 2
            AND period_application.period_id = " . $this->periodId . "
            AND ipeds_race.ipeds_race IS NOT NULL)
            UNION
            (SELECT 'Unknown' AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 2
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NULL
            AND ipeds_race.ipeds_race IS NULL
            )
            ) AS all_ethnicity
            WHERE application_submitted = 1";
        if ($this->grain == 'program') {
            $query .= " AND program_unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND old_degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY ethnicity";

        return $this->DB_Applyweb->handleSelectQuery($query, 'ethnicity');        
    }
    
    private function getWaitlisted() {

        $query = "SELECT ethnicity,
            count(DISTINCT application_id) AS waitlisted_count
            FROM (
            (SELECT ethnicity.name AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 1
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NOT NULL)
            UNION
            (SELECT ipeds_race.ipeds_race AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 1
            AND period_application.period_id = " . $this->periodId . "
            AND ipeds_race.ipeds_race IS NOT NULL)
            UNION
            (SELECT 'Unknown' AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 1
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NULL
            AND ipeds_race.ipeds_race IS NULL
            )
            ) AS all_ethnicity
            WHERE application_submitted = 1";
        if ($this->grain == 'program') {
            $query .= " AND program_unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND old_degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY ethnicity";
                    
        return $this->DB_Applyweb->handleSelectQuery($query, 'ethnicity');     
    }

    private function getAccepted() {
            
        $query = "SELECT ethnicity,
            count(DISTINCT application_id) AS accepted_count
            FROM (
            (SELECT ethnicity.name AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'accept'
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NOT NULL)
            UNION
            (SELECT ipeds_race.ipeds_race AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'accept'
            AND period_application.period_id = " . $this->periodId . "
            AND ipeds_race.ipeds_race IS NOT NULL)
            UNION
            (SELECT 'Unknown' AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'accept'
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NULL
            AND ipeds_race.ipeds_race IS NULL
            )
            ) AS all_ethnicity
            WHERE application_submitted = 1";
        if ($this->grain == 'program') {
            $query .= " AND program_unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND old_degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY ethnicity";

        return $this->DB_Applyweb->handleSelectQuery($query, 'ethnicity'); 
    }    

    private function getDeferred() {

        $query = "SELECT ethnicity,
            count(DISTINCT application_id) AS deferred_count
            FROM (
            (SELECT ethnicity.name AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'defer'
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NOT NULL)
            UNION
            (SELECT ipeds_race.ipeds_race AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'defer'
            AND period_application.period_id = " . $this->periodId . "
            AND ipeds_race.ipeds_race IS NOT NULL)
            UNION
            (SELECT 'Unknown' AS ethnicity,
            application.id AS application_id,
            application.submitted AS application_submitted,
            programs_unit.unit_id AS program_unit_id,
            lu_programs_departments.department_id,
            program_old.degree_id AS old_degree_id
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
            LEFT OUTER JOIN applicant_ipeds_race ON application.user_id = applicant_ipeds_race.lu_users_usertypes_id
            LEFT OUTER JOIN ipeds_race ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'defer'
            AND period_application.period_id = " . $this->periodId . "
            AND ethnicity.name IS NULL
            AND ipeds_race.ipeds_race IS NULL
            )
            ) AS all_ethnicity
            WHERE application_submitted = 1";
        if ($this->grain == 'program') {
            $query .= " AND program_unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND old_degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY ethnicity";
            
        return $this->DB_Applyweb->handleSelectQuery($query, 'ethnicity');    
    }
    
}


/*
* Query for grouping by ethnicity/race combinations. 
*/
/*
SELECT CONCAT(
IFNULL(ethnicity.name, ''), '|',
IFNULL(ipeds.race, '')
) AS ethnicity_race,
COUNT(DISTINCT application.id) AS submitted_count
FROM period_application
INNER JOIN application ON period_application.application_id = application.id
INNER JOIN users_info ON application.user_id = users_info.user_id
LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
LEFT OUTER JOIN (
  SELECT lu_users_usertypes_id,
  GROUP_CONCAT(DISTINCT ipeds_race ORDER BY ipeds_race SEPARATOR '|') AS race
  FROM applicant_ipeds_race
  INNER JOIN ipeds_race
    ON applicant_ipeds_race.ipeds_race_id = ipeds_race.ipeds_race_id
  GROUP BY lu_users_usertypes_id
) AS ipeds ON application.user_id = ipeds.lu_users_usertypes_id

WHERE period_application.period_id = 18
AND application.submitted = 1

GROUP BY ethnicity_race
ORDER BY submitted_count DESC
*/

?>