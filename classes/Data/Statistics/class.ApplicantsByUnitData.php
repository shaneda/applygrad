<?php

class ApplicantsByUnitData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $selectField;
    private $groupByField;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'total', $grainId = '0', $phdOnly = 0, 
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        switch ($grain) {           
            case 'program':
                $this->selectField = $this->groupByField = "program_new.unit_name";
                break;    
            case 'department':
                $this->selectField = "department.name AS unit_name";
                $this->groupByField = "department.name";
                break;
            case 'total':
            default:
                $this->selectField = $this->groupByField = "unit.unit_name";
                break;

        }
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions;    

        $dataArray = $this->getTotalSubmitted();
        $firstChoiceArray = $this->getFirstChoice();
        $admittedArray = $this->getAdmitted();
        $waitlistedArray = $this->getWaitlisted();
        $acceptedArray = $this->getAccepted();
        $deferredArray = $this->getDeferred(); 
        
        $recordCount = count($dataArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $index = $dataArray[$i]['unit_name'];
            if ( isset($firstChoiceArray[$index]['first_choice_count']) ) {
                $dataArray[$i]['first_choice_count'] = $firstChoiceArray[$index]['first_choice_count'];     
            } else {
                $dataArray[$i]['first_choice_count'] = NULL;     
            }
            if ( isset($admittedArray[$index]['admitted_count']) ) {
                $dataArray[$i]['admitted_count'] = $admittedArray[$index]['admitted_count'];     
            } else {
                $dataArray[$i]['admitted_count'] = NULL;     
            }
            if ( isset($waitlistedArray[$index]) ) {
                $dataArray[$i]['waitlisted_count'] = $waitlistedArray[$index]['waitlisted_count'];    
            } else {
                $dataArray[$i]['waitlisted_count'] = NULL;    
            }
            if ( isset($acceptedArray[$index]) ) {
                $dataArray[$i]['accepted_count'] = $acceptedArray[$index]['accepted_count'];    
            } else {
                $dataArray[$i]['accepted_count'] = NULL;    
            }        
            if ( isset($deferredArray[$index]) ) {
                $dataArray[$i]['deferred_count'] = $deferredArray[$index]['deferred_count'];    
            } else {
                $dataArray[$i]['deferred_count'] = NULL;    
            }           
        }
        
        return $dataArray;
    }
    
    private function getTotalSubmitted() {

        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS submitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND period_application.period_id = " . $this->periodId;            
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY " . $this->groupByField . "
                    ORDER BY submitted_count DESC"; 

        return $this->DB_Applyweb->handleSelectQuery($query); 
    }

    private function getFirstChoice() {
            
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS first_choice_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND lu_application_programs.choice = 1
            AND period_application.period_id = " . $this->periodId;            
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY " . $this->groupByField . "
                    ORDER BY first_choice_count DESC";

        return $this->DB_Applyweb->handleSelectQuery($query, 'unit_name');     
    }

    private function getAdmitted() {
        
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS admitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND application_decision.admission_status = 2
            AND period_application.period_id = " . $this->periodId;            
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY " . $this->groupByField;
 
        return $this->DB_Applyweb->handleSelectQuery($query, 'unit_name');         
    }
    
    private function getWaitlisted() {
        
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS waitlisted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND application_decision.admission_status = 1
            AND period_application.period_id = " . $this->periodId;            
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY " . $this->groupByField;

        return $this->DB_Applyweb->handleSelectQuery($query, 'unit_name');        
    }

    private function getAccepted() {

        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS accepted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND student_decision.decision = 'accept'
            AND period_application.period_id = " . $this->periodId;            
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY " . $this->groupByField;

        return $this->DB_Applyweb->handleSelectQuery($query, 'unit_name');         
    }    

    private function getDeferred() {
        
        $query = "SELECT " . $this->selectField . ", 
            count(DISTINCT application.id) AS deferred_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN unit AS program_new ON programs_unit.unit_id = program_new.unit_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            INNER JOIN period ON period_application.period_id = period.period_id
            INNER JOIN unit ON period.unit_id = unit.unit_id
            WHERE " . $this->round1Conditions . " 
            AND student_decision.decision = 'defer'
            AND period_application.period_id = " . $this->periodId;            
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY " . $this->groupByField;

        return $this->DB_Applyweb->handleSelectQuery($query, 'unit_name');
    }
    
}

?>