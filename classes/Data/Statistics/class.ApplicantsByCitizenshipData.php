<?php

class ApplicantsByCitizenshipData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $grain;
    private $grainId;
    private $phdOnly;
    
    private $round1Conditions;
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'total', $grainId = '0', $phdOnly = 0,
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        $this->grain = $grain;
        $this->grainId = $grainId;
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions; 
        
        $baseArray = $this->getPeriodSubmitted();
        $submittedArray = $this->getGrainSubmitted();
        $admittedArray = $this->getAdmitted();
        $waitlistedArray = $this->getWaitlisted();
        $acceptedArray = $this->getAccepted();
        $deferredArray = $this->getDeferred(); 
        
        $dataArray = array();
        $recordCount = count($baseArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $unitId = $baseArray[$i]['citizenship_id'];
            $includeRecord = FALSE;
            
            if ( isset($submittedArray[$unitId]) ) {
                $baseArray[$i]['submitted_count'] = $submittedArray[$unitId]['submitted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['submitted_count'] = NULL;    
            }
            
            if ( isset($admittedArray[$unitId]) ) {
                $baseArray[$i]['admitted_count'] = $admittedArray[$unitId]['admitted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['admitted_count'] = NULL;    
            }
            
            if ( isset($waitlistedArray[$unitId]) ) {
                $baseArray[$i]['waitlisted_count'] = $waitlistedArray[$unitId]['waitlisted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['waitlisted_count'] = NULL;    
            }
            
            if ( isset($acceptedArray[$unitId]) ) {
                $baseArray[$i]['accepted_count'] = $acceptedArray[$unitId]['accepted_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['accepted_count'] = NULL;    
            }
                        
            if ( isset($deferredArray[$unitId]) ) {
                $baseArray[$i]['deferred_count'] = $deferredArray[$unitId]['deferred_count'];
                $includeRecord = TRUE;    
            } else {
                $baseArray[$i]['deferred_count'] = NULL;    
            }            
        
            // Exclude "empty" table rows.
            if ($includeRecord) {
                $dataArray[] = $baseArray[$i];    
            }
        }
        
        return $dataArray;
    }
    
    private function getPeriodSubmitted() {

        $query = "SELECT users_info.cit_country AS citizenship_id,
            IFNULL(countries.name, '[Unknown]') AS citizenship,
            count(DISTINCT application.id) AS submitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId . " 
            GROUP BY countries.id 
            ORDER BY submitted_count DESC, countries.name";

        return $this->DB_Applyweb->handleSelectQuery($query); 
    }

    private function getGrainSubmitted() {

        $query = "SELECT users_info.cit_country AS citizenship_id,
            IFNULL(countries.name, '[Unknown]') AS citizenship, 
            count(DISTINCT application.id) AS submitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY countries.id 
                    ORDER BY submitted_count DESC, countries.name";

        return $this->DB_Applyweb->handleSelectQuery($query, 'citizenship_id'); 
    }
    
    private function getAdmitted() {
        
        $query = "SELECT users_info.cit_country AS citizenship_id,
            IFNULL(countries.name, '[Unknown]') AS citizenship,  
            count(DISTINCT application.id) AS admitted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 2 
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY countries.id";             

        return $this->DB_Applyweb->handleSelectQuery($query, 'citizenship_id'); 
    }
    
    private function getWaitlisted() {
        
        $query = "SELECT users_info.cit_country AS citizenship_id,
            IFNULL(countries.name, '[Unknown]') AS citizenship,  
            count(DISTINCT application.id) AS waitlisted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND application_decision.admission_status = 1  
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY countries.id"; 
                    
        return $this->DB_Applyweb->handleSelectQuery($query, 'citizenship_id');   
    }

    private function getAccepted() {
 
        $query = "SELECT users_info.cit_country AS citizenship_id,
            IFNULL(countries.name, '[Unknown]') AS citizenship,  
            count(DISTINCT application.id) AS accepted_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'accept' 
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY countries.id";

        return $this->DB_Applyweb->handleSelectQuery($query, 'citizenship_id');  
    }    

    private function getDeferred() {
        
        $query = "SELECT users_info.cit_country AS citizenship_id,
            IFNULL(countries.name, '[Unknown]') AS citizenship,  
            count(DISTINCT application.id) AS deferred_count
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            LEFT OUTER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN application_decision ON application.id = application_decision.application_id
            INNER JOIN student_decision ON application.id = student_decision.application_id    
                AND application_decision.program_id  = student_decision.program_id
            INNER JOIN programs_unit ON application_decision.admission_program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON application_decision.admission_program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND student_decision.decision = 'defer' 
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY countries.id";    
        
        return $this->DB_Applyweb->handleSelectQuery($query, 'citizenship_id');   
    }
    
}

?>