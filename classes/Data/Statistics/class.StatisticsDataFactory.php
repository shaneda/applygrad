<?php

class StatisticsDataFactory {
    
    private $round1Conditions = "application.submitted = 1
                AND (period_application.period_id < 61
                OR (period_application.period_id > 60 AND lu_programs_departments.department_id IN (66, 50, 97))
                OR (period_application.period_id > 60 AND lu_programs_departments.department_id != 66
                    AND (application.paid = 1 OR application.waive = 1))
                )";
    
    public function createData($periodId, $unitId, $view, $grain, $grainId, $phdOnly) {
        
        switch ($view) {
            
            case 'unitCrosstab':
            
                $data = new UnitCrosstabData();
                break;

            case 'genderCitizenshipCrosstab':
            
                $data = new GenderCitizenshipCrosstabData();
                break;

            case 'applicantsByCitizenship':
            
                $data = new ApplicantsByCitizenshipData();
                break;

            case 'applicantsByEthnicity':
            
                $data = new ApplicantsByEthnicityData();
                break;

            case 'applicantsByGender':
            
                $data = new ApplicantsByGenderData();
                break;

            case 'applicantsByInstitution':
            
                $data = new ApplicantsByInstitutionData();
                break;
            
            case 'applicantsByNumPrograms':
            
                $data = new ApplicantsByNumProgramsData();
                break;

            case 'mseMsitBreakdown':
            
                $data = new MseMsitBreakdownData();
                break;

            case 'applicantsByUnit':
            default:
            
                $data = new ApplicantsByUnitData();
            
        }
        
        return $data->getData($periodId, $unitId, $view, $grain, $grainId, 
                                $phdOnly, $this->round1Conditions);
    }
    
}
  
?>