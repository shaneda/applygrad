<?php

class GenderCitizenshipCrosstabData {
    
    private $DB_Applyweb;
    private $periodId;
    private $unitId;
    private $grain;
    private $grainId;
    private $phdOnly;
    
    private $round1Conditions; 
    
    public function getData($periodId, $unitId, $view, 
                                $grain = 'total', $grainId = '0', $phdOnly = 0,
                                $round1Conditions = 'application.submitted = 1') {
        
        $this->DB_Applyweb = new DB_Applyweb();
        $this->periodId = $periodId;
        $this->unitId = $unitId;
        $this->grain = $grain;
        $this->grainId = $grainId;
        $this->phdOnly = $phdOnly;
        $this->round1Conditions = $round1Conditions;      
        
        $dataArray = $this->getSubmittedUs();
        $submmittedForeign = $this->getSubmittedForeign();
        $recordCount = count($dataArray);
        for ($i = 0; $i < $recordCount; $i++) {
            $gender = $dataArray[$i]['gender'];
            if ( isset($submmittedForeign[$gender]['submitted_foreign']) ) {
                $dataArray[$i]['submitted_foreign'] = $submmittedForeign[$gender]['submitted_foreign'];    
            } else {
                $dataArray[$i]['submitted_foreign'] = NULL;    
            }        
        }

        return $dataArray;
    }

    private function getSubmittedUs() {
        
        $query = "SELECT gender,
            count(DISTINCT application.id) AS submitted_us
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND countries.name = 'UNITED STATES'
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender"; 
        $query .= " ORDER BY submitted_us DESC";
          
        return $this->DB_Applyweb->handleSelectQuery($query); 
    }

    private function getSubmittedForeign() {
        
        $query = "SELECT gender,
            count(DISTINCT application.id) AS submitted_foreign
            FROM period_application
            INNER JOIN application ON period_application.application_id = application.id
            INNER JOIN users_info ON application.user_id = users_info.user_id
            INNER JOIN countries ON users_info.cit_country = countries.id
            INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
            INNER JOIN programs_unit ON lu_application_programs.program_id = programs_unit.programs_id
            INNER JOIN programs AS program_old ON programs_unit.programs_id = program_old.id
            INNER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
            INNER JOIN department ON lu_programs_departments.department_id = department.id
            WHERE " . $this->round1Conditions . "
            AND countries.name != 'UNITED STATES'
            AND period_application.period_id = " . $this->periodId;            
        if ($this->grain == 'program') {
            $query .= " AND programs_unit.unit_id = " . $this->grainId;
        }
        if ($this->grain == 'department') {
            $query .= " AND lu_programs_departments.department_id = " . $this->grainId;
        }
        if ($this->phdOnly) {
            $query .= " AND program_old.degree_id IN (3, 9, 14, 17)";
        }
        $query .= " GROUP BY gender"; 
        $query .= " ORDER BY submitted_foreign DESC";
          
        return $this->DB_Applyweb->handleSelectQuery($query, 'gender'); 
    }

}

?>