<?php
class Domain
{
    
    public static function getName($domainId) {
        
        $query = "SELECT name from domain where id = " . intval($domainId);
        $result = mysql_query($query);    
        $domainName = FALSE;
        while($row = mysql_fetch_array($result)) {    
            $domainName = $row['name'];
        }
        return $domainName;   
    }
    
    
    public static function isValid($domainId) {
        
        $query = "SELECT programs.id FROM programs
                        INNER JOIN lu_programs_departments 
                            ON programs.id = lu_programs_departments.program_id
                        INNER JOIN lu_domain_department 
                            ON lu_programs_departments.department_id = lu_domain_department.department_id 
                        WHERE programs.enabled = 1
                        AND lu_domain_department.domain_id = " . intval($domainId);
        
        $result = mysql_query($query);
        
        if (mysql_numrows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    
    public static function hasContent($domainId, $contentName) {
        
        $query = "SELECT content from content where name = '" . mysql_real_escape_string($contentName);
        $query .= "' AND domain_id = " . intval($domainId); 
 
        $result = mysql_query($query);
        
        if (mysql_numrows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
}
?>