<?php

/*
Class for special consideration request
Primary tables: student_decision
*/

class DB_StudentDecision extends DB_Applyweb
{

    //protected $application_id;

    // Check whether special consideration has been requested by *anyone*.
    // This should always return a record for a valid application id.
    function getStudentDecision($application_id, $program_id) {
    

        $query = "SELECT * FROM student_decision
                    WHERE application_id = " . $application_id
                    . " AND program_id = " . $program_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }
    
    
    function updateStudentDecision($application_id, $program_id, $decision) {
    
        $query = "INSERT INTO student_decision (application_id, program_id, decision) VALUES ("
                    . $application_id . ","
                    . $program_id . "," 
                    . "'" . $decision . "'" .
                    ") ON DUPLICATE KEY UPDATE decision = '" . $decision . "'";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }
    

    
}

?>
