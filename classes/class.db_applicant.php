<?php
/*
Class for applicant biographic/demographic info
Primary tables: application, users, users_info
*/

class DB_Applicant extends DB_Applyweb
{
    
    //protected $application_id;

    function getApplicantInfo($application_id) {
        
        $query = "
            SELECT 
                application.id as application_id,
                lu_users_usertypes.id as luu_id, 
                users.*, 
                users_info.*,
                current_states.name as current_state,
                current_countries.name as current_country,
                permanent_states.name as permanenet_state,
                permanent_countries.name as permanent_country,
                ethnicity.name as ethnicity,
                visatypes.short as visa_type,
                citizenship_countries.name as citizenship_country
            FROM
                application,
                lu_users_usertypes,
                users,
                users_info
                left outer join states as current_states on current_states.id = users_info.address_cur_state
                left outer join states as permanent_states on permanent_states.id = users_info.address_perm_state
                left outer join countries as current_countries on current_countries.id = users_info.address_cur_country
                left outer join countries as permanent_countries on permanent_countries.id = users_info.address_perm_country
                left outer join countries as citizenship_countries on citizenship_countries.id = users_info.cit_country
                left outer join ethnicity on ethnicity.id = users_info.ethnicity
                left outer join visatypes on visatypes.id = users_info.visastatus
            WHERE 
                application.id = " . $application_id . "
                AND lu_users_usertypes.id = application.user_id
                AND users.id = lu_users_usertypes.user_id
                AND users_info.user_id = lu_users_usertypes.id
            "; 
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;     
    }
    

    
}

?>
