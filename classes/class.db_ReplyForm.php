<?php
  class DB_Invited_User extends DB_Applyweb
{
    
    //protected $application_id;

    function AuthenticatedUserP($email, $password) {
         
        $query = "select users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email
        from users 
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id  and usertypes.id = 5
        where email = '".addslashes(htmlspecialchars(trim($email)))."' and password = '".$password."'"; 
    
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;     
    }

    function AuthorizedUserP($luu_id, $period_id, $unit) {
        global $domainid;
         
        if (isIniDomain($domainid)) {
            $query = "select application.* FROM application        
            inner join period_application on period_application.application_id = application.id 
            and period_application.period_id = ". $period_id . 
            " inner join application_decision_ini on application_decision_ini.application_id = period_application.application_id  
            and application_decision_ini.admission_status = 2
            where application.user_id = ". $luu_id;
        } else if (isDesignDomain($domainid)) {
            $query = "select application.* FROM application        
            inner join period_application on period_application.application_id = application.id 
            and period_application.period_id = ". $period_id . 
            " inner join application_decision_design on application_decision_design.application_id = period_application.application_id  
            and application_decision_design.admission_status = 2
            where application.user_id = ". $luu_id;
        }
	else {      
            $query = "select application.* FROM application        
            inner join period_application on period_application.application_id = application.id 
            and period_application.period_id = ". $period_id . 
            " inner join  lu_application_programs on lu_application_programs.application_id = period_application.application_id  
             and lu_application_programs.admission_status = 2 
             inner join lu_programs_departments lpd on lpd.program_id = lu_application_programs.program_id
             and lpd.department_id = " .  $unit .
            " where application.user_id = ". $luu_id . " and lu_application_programs.program_id not in (100070)"; // (100070, 23)";
        } 
      //  debugbreak();
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;     
    }

    function getStatus($application_id) {
        
        $query = "SELECT application.* FROM application WHERE application.id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        return $results_array;
    }
    
}

/*

SQL for admitted students

  SELECT 
    lu_users_usertypes.id as id,
    application.id AS application_id,
    CONCAT(users.lastname, ', ',users.firstname) AS name,
    users.email,
    GROUP_CONCAT(
                  DISTINCT
                  CONCAT(' ', degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
                  ORDER BY programs.id
                  ) AS program,
                  lu_application_programs.faccontact,
    lu_application_programs.stucontact

    FROM application
    INNER JOIN period_application ON application.id = period_application.application_id

    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    LEFT OUTER JOIN countries ON users_info.cit_country = countries.id

    INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
    INNER JOIN programs ON lu_application_programs.program_id = programs.id
    INNER JOIN degree ON programs.degree_id = degree.id
    INNER JOIN fieldsofstudy ON programs.fieldofstudy_id = fieldsofstudy.id
    INNER JOIN lu_programs_departments
      ON lu_application_programs.program_id = lu_programs_departments.program_id
    LEFT OUTER JOIN lu_application_interest
      ON lu_application_programs.id = lu_application_interest.app_program_id
    LEFT OUTER JOIN interest ON lu_application_interest.interest_id = interest.id

    LEFT OUTER JOIN application_decision
      ON application.id = application_decision.application_id
      AND lu_application_programs.program_id = application_decision.program_id
    LEFT OUTER JOIN programs AS admit_programs
      ON application_decision.admission_program_id = admit_programs.id
    LEFT OUTER JOIN degree AS admit_degree
      ON admit_programs.degree_id = admit_degree.id
    LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy
      ON admit_programs.fieldofstudy_id = admit_fieldsofstudy.id

    WHERE application.submitted = 1
    AND (lu_application_programs.admission_status = 2
      OR application_decision.admission_status = 2)
    AND lu_programs_departments.department_id = 1 AND period_application.period_id = 358 GROUP BY application.id 

    */

?>
