<?php
include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php";
include "../classes/DB_Applyweb/class.VW_AdminList.php";  
include "../classes/DB_Applyweb/class.VW_ReviewListRecommendationCount.php";
include "../classes/DB_Applyweb/class.VW_ReviewList.php";
include "../classes/DB_Applyweb/class.VW_ReviewListBase.php";
include "../classes/DB_Applyweb/class.VW_ReviewListPrograms.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAdvisors.php";
include "../classes/DB_Applyweb/class.VW_ReviewListToefl.php";
include "../classes/DB_Applyweb/class.VW_ReviewListIelts.php";
include "../classes/DB_Applyweb/class.VW_ReviewListRecommenders.php";
include "../classes/DB_Applyweb/class.VW_ReviewListReviews.php";
include "../classes/DB_Applyweb/class.VW_ReviewListReviewsIIS.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAdmissionStatus.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAdmissionStatusDesign.php";
include "../classes/DB_Applyweb/class.VW_ReviewListReviewerAoi.php";
include "../classes/DB_Applyweb/class.VW_ReviewListSpecial.php";
include "../classes/DB_Applyweb/class.VW_ReviewListTouched.php";
include "../classes/DB_Applyweb/class.VW_ReviewListGroups.php";
include "../classes/DB_Applyweb/class.VW_ReviewListAssistantship.php";
include "../classes/DB_Applyweb/class.VW_ReviewListFacultyRankings.php";
include "../classes/DB_Applyweb/class.VW_ReviewListPsychology.php";
include_once "../classes/class.db_applicant.php";

class ReviewListData
{
    
    private $departmentId;
    private $departmentName;
    private $data;          // 2D array
    
    public function __construct($departmentId, $departmentName = null) {
        $this->departmentId = $departmentId;    
        $this->departmentName = $departmentName;
    }
    
    public function getData($periodId = NULL, $round = 1, $reviewerId = NULL, $groups = 'allGroups', $searchString = '') {
        
        $groupsLimitReviewerId = NULL;
        if ( $reviewerId && ($groups == 'myGroups') ) {
            $groupsLimitReviewerId = $reviewerId;    
        }
        
        
        
        $baseView = new VW_ReviewListBase($this->departmentName);
        $baseArray = $baseView->find($periodId, $this->departmentId, $round, $groupsLimitReviewerId, $searchString);
        
        // PLB added round argument for programs 11/9/09.
        // This ensures that only the right program(s) are displayed for each round. 
        $programView = new VW_ReviewListPrograms($this->departmentName);
        $programArray = $programView->find($periodId, $this->departmentId, $round);

        $advisorView = new VW_ReviewListAdvisors($this->departmentName);
        $advisorArray = $advisorView->find($periodId, $this->departmentId);

        $toeflView = new VW_ReviewListToefl($this->departmentName);
        $toeflArray = $toeflView->find($periodId, $this->departmentId);

        $ieltsView = new VW_ReviewListIelts($this->departmentName);
        $ieltsArray = $ieltsView->find($periodId, $this->departmentId);

        $recommendersView = new VW_ReviewListRecommenders($this->departmentName);
        // xxxx  DebugBreak();
        if ($this->departmentId == 6) {
             $recommendersArray = $recommendersView->find($periodId, $this->departmentId, $round, NULL, NULL, 1, 1);
        }  else {
            $recommendersArray = $recommendersView->find($periodId, $this->departmentId, $round);
        }
        
        $recommendationCountView = new VW_ReviewListRecommendationCount($this->departmentName);
        $recommendationCountArray = $recommendationCountView->find('department', $this->departmentId, $periodId);

        // PLB added round argument for reviews 11/9/09.
        if ($this->departmentId != 83)      //IIS
            {
                $reviewsView = new VW_ReviewListReviews($this->departmentName);
            } else {
                $reviewsView = new VW_ReviewListReviewsIIS($this->departmentName);
            }

        $reviewsArray = $reviewsView->find($periodId, $this->departmentId, $round);
        
        if (isRiDepartment($this->departmentId)
            || isRiMastersDepartment($this->departmentId)
            || ($this->departmentId == 1 && $round > 1))
        {
            
            $facultyRankingsView = new VW_ReviewListFacultyRankings($this->departmentName);
            $facultyRankingsArray = $facultyRankingsView->find($periodId, $this->departmentId, $round);    
        }

        /*
        * Admission view should not have a review dependency, i.e., it should show
        * even when there have been no reviews. 
        */
        if (isDesignDepartment($this->departmentId))
        {
            $admissionView = new VW_ReviewListAdmissionStatusDesign($this->departmentName);    
        }
        else
        {
            $admissionView = new VW_ReviewListAdmissionStatus($this->departmentName);
        }
        $admissionArray = $admissionView->find($periodId, $this->departmentId, $round);
        
        $reviewerAoiView = new VW_ReviewListReviewerAoi($this->departmentName);
        $reviewerAoiArray = $reviewerAoiView->find($periodId, $this->departmentId, $round);

        $specialView = new VW_ReviewListSpecial($this->departmentName);
        $specialArray = $specialView->find($periodId, $this->departmentId, $round);

        $touchedView = new VW_ReviewListTouched($reviewerId, $this->departmentName);
        $touchedArray = $touchedView->find($periodId, $this->departmentId, $round);
        
        $groupsView = new VW_ReviewListGroups($this->departmentName);
        $groupsArray = $groupsView->find($periodId, $this->departmentId, $round);

        if (isDesignDepartment($this->departmentId) 
            || isDesignPhdDepartment($this->departmentId)
            || isDesignDdesDepartment($this->departmentId))
        {
            $assistantshipView = new VW_ReviewListAssistantship($this->departmentName);
            $assistantshipArray = $assistantshipView->find($periodId, $this->departmentId, $round);
        }
        
        if (isPsychologyDepartment($this->departmentId)) {
            $psychologyView = new VW_ReviewListPsychology($this->departmentName);
            $psychologyArray =  $psychologyView->find($periodId, $this->departmentId, $round);
        }
        
        $recordCount = count($baseArray);
        for ($i = 0; $i < $recordCount; $i++) {
            
            $applicationId = $baseArray[$i]['application_id'];
    
            $baseArray[$i] = array_merge($baseArray[$i], $programArray[$applicationId]);
            
            if ( array_key_exists($applicationId, $advisorArray) ) {
                $baseArray[$i]['possible_advisors'] = $advisorArray[$applicationId]['possible_advisors'];    
            } else {
                $baseArray[$i]['possible_advisors'] = '';
            }

            if ( array_key_exists($applicationId, $toeflArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $toeflArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['toefl_testdate'] = '';
                $baseArray[$i]['toefl_section1'] = '';
                $baseArray[$i]['toefl_section2'] = '';
                $baseArray[$i]['toefl_section3'] = '';
                $baseArray[$i]['toefl_essay'] = '';
                $baseArray[$i]['toefl_total'] = ''; 
                $baseArray[$i]['toefl_type'] = ''; 
            }

            if ( array_key_exists($applicationId, $ieltsArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $ieltsArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['ieltsscore_testdate'] = '';
                $baseArray[$i]['ieltsscore_listeningscore'] = '';
                $baseArray[$i]['ieltsscore_readingscore'] = '';
                $baseArray[$i]['ieltsscore_writingscore'] = '';
                $baseArray[$i]['ieltsscore_speakingscore'] = ''; 
                $baseArray[$i]['ieltsscore_overallscore'] = ''; 
            }

            //   xxxxx  DebugBreak();
            $baseArray[$i] = array_merge($baseArray[$i], $recommendersArray[$applicationId]);
            
            if ( array_key_exists($applicationId, $recommendationCountArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $recommendationCountArray[$applicationId]);    
            } else {
                $baseArray[$i]['ct_recommendations_requested'] = 0;
                $baseArray[$i]['ct_recommendations_submitted'] = 0;
            }
            
            if ( array_key_exists($applicationId, $reviewsArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $reviewsArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['committee_reviewers'] = '';
                $baseArray[$i]['round1_point1_scores'] = '';
                $baseArray[$i]['round1_point1_average'] = '';
                $baseArray[$i]['round1_point2_scores'] = '';
                $baseArray[$i]['round1_point2_average'] = '';
                $baseArray[$i]['round1_comments'] = '';
                $baseArray[$i]['votes_for_round2'] = ''; 
                $baseArray[$i]['round2_point1_committee_scores'] = '';
                $baseArray[$i]['round2_point1_committee_average'] = '';
                $baseArray[$i]['all_point1_committee_scores'] = '';
                $baseArray[$i]['all_point1_committee_average'] = '';
                $baseArray[$i]['all_point2_committee_scores'] = '';
                $baseArray[$i]['all_point2_committee_average'] = '';
                $baseArray[$i]['round2_point1_faculty_scores'] = '';
                $baseArray[$i]['round2_point1_faculty_average'] = '';
                $baseArray[$i]['round2_comments'] = '';
                $baseArray[$i]['round2_pertinent_info'] = '';
                $baseArray[$i]['decision'] = '';
                $baseArray[$i]['admit_to'] = '';
                $baseArray[$i]['admit_to_decision'] = ''; 
            }
            
            if ( isset($facultyRankingsArray) && array_key_exists($applicationId, $facultyRankingsArray) ) 
            {
                $baseArray[$i]['faculty_rankings'] = $facultyRankingsArray[$applicationId]['faculty_rankings'];
                $baseArray[$i]['faculty_ranking_min'] = $facultyRankingsArray[$applicationId]['faculty_ranking_min'];    
            
            } else {
                
                $baseArray[$i]['faculty_rankings'] = '';
                $baseArray[$i]['faculty_ranking_min'] = '';
            }

            if ( array_key_exists($applicationId, $admissionArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $admissionArray[$applicationId]);    
                if ($admissionArray[$applicationId]['decision'] == '') {
                    $baseArray[$i]['full_reject'] = 'N';    
                } else {
                    $fullReject = 'Y';
                    $decisionArray = explode('|', $admissionArray[$applicationId]['decision']);
                    foreach ($decisionArray as $decisionValue) {
                        if ($decisionValue != 'R') {
                            $fullReject = 'N';    
                        }
                    }
                    $baseArray[$i]['full_reject'] = $fullReject;
                }

            } else {

                $baseArray[$i]['full_reject'] = 'N';
                $baseArray[$i]['decision'] = '';
                $baseArray[$i]['admit_to'] = '';
                $baseArray[$i]['admit_to_decision'] = ''; 
            }
            

            if ( array_key_exists($applicationId, $reviewerAoiArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $reviewerAoiArray[$applicationId]);
            } else {
                $baseArray[$i]['reviewer_aoi'] = '';
                $baseArray[$i]['reviewer_other_aoi'] = '';
            }            
            
            $baseArray[$i] = array_merge($baseArray[$i], $specialArray[$applicationId]);
            
            
            if ( array_key_exists($applicationId, $touchedArray) ) {
                
                $baseArray[$i] = array_merge($baseArray[$i], $touchedArray[$applicationId]);    
            
            } else {
                
                $baseArray[$i]['round1_reviewer_touched'] = '0';
                $baseArray[$i]['round2_reviewer_touched'] = '0';
                $baseArray[$i]['round3_reviewer_touched'] = '0';
                $baseArray[$i]['round1_reviewer_comments'] = '';
                $baseArray[$i]['round2_reviewer_comments'] = '';
                $baseArray[$i]['round1_reviewer_point1'] = '';
                $baseArray[$i]['round2_reviewer_point1'] = '';
                $baseArray[$i]['round1_reviewer_point2'] = '';
                $baseArray[$i]['round2_reviewer_point2'] = '';
            }
            // xxxxxx  DebugBreak();
            $baseArray[$i] = array_merge($baseArray[$i], $groupsArray[$applicationId]);
            
            if ( isset($assistantshipArray) && array_key_exists($applicationId, $assistantshipArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $assistantshipArray[$applicationId]);
            } else {
                $baseArray[$i]['assistantship_requested'] = '';
                $baseArray[$i]['assistantship_granted'] = '';
            }
            
            if ( isset($psychologyArray) && array_key_exists($applicationId, $psychologyArray) ) {
                $baseArray[$i] = array_merge($baseArray[$i], $psychologyArray[$applicationId]);
            } else {
                $baseArray[$i]['cnbc_requested'] = '';
                $baseArray[$i]['pier_requested'] = '';
            }
            //   The code below here is for the merging of pdf files
            global $datafileroot;

            // get the applicant's guid
            $db_applicant = new DB_Applicant();
            $applicant_info = $db_applicant->getApplicantInfo($applicationId);
            $applicant_guid = $applicant_info[0]['guid'];
           
           /* 
            global $webIsoUsername;
            
            if ($applicationId ==80434 && $webIsoUsername == 'dales@cs.cmu.edu') {
               debugBreak(); 
            }
            */
             
            // set up the pdf href
            $pdf_path = $datafileroot . "/" . $applicant_guid . "/" . $applicationId . "_merged.pdf";
            
            if ( file_exists($pdf_path) ) {
                $baseArray[$i]['mergedFileDate'] = date ("m/d/Y H:i:s.", filemtime($pdf_path));
                $baseArray[$i]['mergedFilePath'] = $pdf_path;                    
            }
             
                
        }        
        
        return $baseArray;
    }
}

?>
