<?php
/*
Class for program_type table data access.
*/

class DB_ProgramType extends DB_Applyweb
{

    function get($programTypeId = NULL) {

        $query = "SELECT * FROM program_type"; 
        if ($programTypeId) {
            $query .=  " WHERE period_type_id = " . $programTypeId;   
        }

        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

}

?>