<?php
/*
* Class for unit table data access.
* 
* Fields:
*   int         unit_id
*   varchar     unit_name
*   varchar     unit_name_short
*   varchar     unit_description
*   varchar     unit_url
*   varchar     unit_oracle_string
*   varchar     unit_system_email
*   varchar     unit_cc_email
*   varchar     parent_unit_id     
*/

class DB_Unit extends DB_Applyweb
{

    public function get($unitId = NULL) {
        $query = "SELECT unit.* FROM unit"; 
        if ($unitId) {
            $query .=  " WHERE unit_id = " . $unitId;   
        }
        $resultArray = $this->handleSelectQuery($query);
        return $resultArray;  
    }
    
    public function find($parentUnitId) {
        $query = "SELECT unit.* FROM unit";
        if ($parentUnitId === NULL) {
            $query .= " WHERE parent_unit_id IS NULL";    
        } else {
            $query .= " WHERE parent_unit_id = " . $parentUnitId;    
        }
        $resultArray = $this->handleSelectQuery($query);
        return $resultArray;           
    }

    public function save($recordArray) {

        $fieldCount = count($recordArray);
        $fieldList = $valueList = $updateList = "";
        $i = 1;
        foreach ($recordArray as $key => $value) {
            if (!$value) {
                continue;
            }
            if ($i != 1) {
                $fieldList .= ", ";
                $valueList .= ", ";
                $updateList .= ", ";
            }
            $fieldList .= $key;
            if ( $key != 'unit_id' && $key != 'parent_unit_id' ) {
                $value = "'" . self::escapeString($value) . "'";
            }
            $valueList .= $value;
            $updateList .= $key . "=" . $value;            
            $i++;     
        }
        $query = "INSERT INTO unit (" . $fieldList . ") VALUES (" . $valueList . ")
            ON DUPLICATE KEY UPDATE " . $updateList;

        $numAffectedRows = $this->handleInsertQuery($query);
        
        if ($recordArray['unit_id']) {
            return $recordArray['unit_id'];
        } else {
            return self::getLastInsertId();
        }    
    }
    
    public function delete($unitId) {
        
        if (!$unitId) {
            return FALSE;
        }
        
        $query = "DELETE FROM FROM unit WHERE unit_id = " . $unitId; 
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;        
    }
    
}
?>