<?php
/*
Class for datafileinfo table data access.
*/

class DB_Datafileinfo extends DB_Applyweb
{

    function get($datafileinfoId = NULL) {

        $query = "SELECT datafileinfo.* FROM datafileinfo"; 
        if ($datafileinfoId) {
            $query .=  " WHERE id = " . $datafileinfoId;   
        }

        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }

    public function find($luUsersUsertypesId, $applicationId = NULL) {
        
        $query = "SELECT datafileinfo.* FROM datafileinfo WHERE user_id = " . $luUsersUsertypesId;  

        if ($applicationId) {
        
            $query .= " AND userdata LIKE '" . $applicationId . "_%'"; 
            
        }
        
        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }

}

?>