<?php
/* 
* 
*/

class VW_Programs extends DB_VW_Applyweb
{

    //protected $arrayKey = 'program_id';
    
    protected $querySelect = 
    "
    SELECT
    programs.id AS program_id,
    CONCAT(
        degree.name,
        ' ',
        programs.linkword,
        ' ',
        fieldsofstudy.name
    ) AS program_name
    ";

    protected $queryFrom = 
    "
    FROM programs
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = programs.id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    ";    
    
    public function find($unit = NULL, $unitId = NULL) {                     

        if ($unit && $unitId) {
            switch ($unit) {
            
                case 'domain':
                
                    $this->findJoins[] = "INNER JOIN lu_domain_department on lu_programs_departments.department_id = lu_domain_department.department_id";    
                    $this->findWheres[] = "lu_domain_department.domain_id = " . $unitId;
                    break;
                
                case 'department':
                default:
                    $this->findWheres[] = "lu_programs_departments.department_id = " . $unitId;

            }
        }

        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    } 

}
?>