<?php

/* 
* A view, suitable for inclusion in a 2D review list array,
* of program/interest data from the lu_application_programs 
* and lu_application_interest tables.
*/

class VW_AdminListPrograms extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT
    lu_application_programs.application_id,
    GROUP_CONCAT( DISTINCT
      CONCAT(
        degree.name,
        ' ',
        programs.linkword,
        ' ',
        fieldsofstudy.name,
        ' (',
        CAST(lu_application_programs.choice AS CHAR),
        ')'
        )
      ORDER BY lu_application_programs.choice
      SEPARATOR '|'
      ) AS programs
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs AS lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    LEFT OUTER JOIN lu_application_interest ON lu_application_interest.app_program_id = lu_application_programs.id
    LEFT OUTER JOIN interest ON interest.id = lu_application_interest.interest_id
    ";
    
    protected $queryGroupBy = "application.id";

    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
}    
?>
