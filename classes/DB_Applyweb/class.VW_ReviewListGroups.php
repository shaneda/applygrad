<?php
/* 
* A view of review group data suitable for inclusion 
* in a 2D review list array.
* 
*/

class VW_ReviewListGroups extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    GROUP_CONCAT(DISTINCT round1_revgroup.name  ORDER BY round1_revgroup.name
        SEPARATOR '|') AS round1_groups,
    GROUP_CONCAT(DISTINCT round2_revgroup.name ORDER BY round2_revgroup.name
        SEPARATOR '|') AS round2_groups,
    GROUP_CONCAT(DISTINCT round3_revgroup.name ORDER BY round3_revgroup.name
        SEPARATOR '|') AS round3_groups,
    GROUP_CONCAT(DISTINCT round4_revgroup.name ORDER BY round4_revgroup.name
        SEPARATOR '|') AS round4_groups
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
    /* round 1 groups */
    LEFT OUTER JOIN lu_application_groups AS round1_groups
      ON round1_groups.application_id = application.id
      AND round1_groups.round = 1
    LEFT OUTER JOIN revgroup AS round1_revgroup
      ON round1_revgroup.id = round1_groups.group_id
      AND round1_revgroup.department_id = lu_programs_departments.department_id
    /* round 2 groups */
    LEFT OUTER JOIN lu_application_groups AS round2_groups
      ON round2_groups.application_id = application.id AND round2_groups.round = 2
    LEFT OUTER JOIN revgroup AS round2_revgroup
      ON round2_revgroup.id = round2_groups.group_id
      AND round2_revgroup.department_id = lu_programs_departments.department_id
    /* round 3 groups */
    LEFT OUTER JOIN lu_application_groups AS round3_groups
      ON round3_groups.application_id = application.id AND round3_groups.round = 3
    LEFT OUTER JOIN revgroup AS round3_revgroup
      ON round3_revgroup.id = round3_groups.group_id
      AND round3_revgroup.department_id = lu_programs_departments.department_id
      /* round 4 groups */
    LEFT OUTER JOIN lu_application_groups AS round4_groups
      ON round4_groups.application_id = application.id AND round4_groups.round = 4
    LEFT OUTER JOIN revgroup AS round4_revgroup
      ON round4_revgroup.id = round4_groups.group_id
      AND round4_revgroup.department_id = lu_programs_departments.department_id
    ";
    
    protected $queryGroupBy = 'application.id, lu_programs_departments.department_id';
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
}    
?>