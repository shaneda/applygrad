<?php
/* 
* A view of review table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListAdmissionStatus extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,

    /* decision */
    GROUP_CONCAT( DISTINCT
        lu_application_programs.decision
        SEPARATOR '|'
    ) AS decision,
    
    GROUP_CONCAT( DISTINCT 
        CASE lu_application_programs.admission_status when 1 then 
            CONCAT(
                IF(degree.name is not null, degree.name, ''), ' ',
                IF(programs.linkword is not null, programs.linkword, ''), ' ',
                IF(fieldsofstudy.name is not null, fieldsofstudy.name, ''), 
                ' (wait)'
            ) 
        WHEN 2 THEN
            CONCAT(
                degree.name, ' ', 
                IF(programs.linkword is not null, programs.linkword,''),
                ' ', 
                fieldsofstudy.name
            )
        END
        SEPARATOR '|'
    ) AS admit_to,
    
    GROUP_CONCAT( DISTINCT
        lu_application_programs.scholarship_amt
        SEPARATOR '|'
    ) AS scholarship_amt,
     
    GROUP_CONCAT( DISTINCT
        CASE application_decision.admission_status 
        WHEN 1 THEN
            CONCAT(
                IF(admit_degree.name is not null, admit_degree.name, ''), ' ',
                IF(admit_programs.linkword is not null, admit_programs.linkword, ''), ' ',
                IF(admit_fieldsofstudy.name is not null, admit_fieldsofstudy.name, ''),
                ' (wait)'
            )
        WHEN 2 THEN
            CONCAT(
                admit_degree.name, ' ',
                IF(admit_programs.linkword is not null, admit_programs.linkword, ''),
                ' ',
                admit_fieldsofstudy.name
            )
        END
        SEPARATOR '|'
    ) AS admit_to_decision
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    LEFT OUTER JOIN application_decision
        ON lu_application_programs.application_id = application_decision.application_id
        AND lu_application_programs.program_id = application_decision.program_id
    LEFT OUTER JOIN programs AS admit_programs
      ON admit_programs.id = application_decision.admission_program_id
    LEFT OUTER JOIN degree AS admit_degree
      ON admit_degree.id = admit_programs.degree_id
    LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy
      ON admit_fieldsofstudy.id = admit_programs.fieldofstudy_id
    ";
    
    //protected $queryWhere = 'review.department_id = lu_programs_departments.department_id';
    
    protected $queryGroupBy = 'application.id, lu_programs_departments.department_id';
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
}    
?>
