<?php

/* 
* A view, suitable for inclusion in a 2D review list array,
* of program/interest data from the lu_application_programs 
* and lu_application_interest tables.
*/

class VW_ApplicantGroupListBase extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name,
    GROUP_CONCAT( DISTINCT
      CONCAT(
        degree.name,
        ' ',
        programs.linkword,
        ' ',
        fieldsofstudy.name,
        ' (',
        CAST(lu_application_programs.choice AS CHAR),
        ')'
        )
      ORDER BY lu_application_programs.choice
      SEPARATOR '|'
      ) AS programs
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN lu_application_programs AS lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    LEFT OUTER JOIN lu_application_interest ON lu_application_interest.app_program_id = lu_application_programs.id
    LEFT OUTER JOIN interest ON interest.id = lu_application_interest.interest_id
    ";

    protected $queryGroupBy = "application.id, lu_programs_departments.department_id"; 
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';      


    public function __construct( $sort = NULL ) {
       
        parent::__construct();

        if ($sort && $sort != 'name') {
            
            if ($sort == 'applicationId')
            {
                $this->queryOrderBy = "application.id";    
            }
            else
            {
                $this->queryOrderBy = "areas_of_interest, users.lastname, users.firstname";
            }
                
            switch ($sort) {
               
                case 'int1':

                    $this->querySelect .= 
                    ",
                    GROUP_CONCAT( DISTINCT
                        IF ( (lu_application_interest.choice = 1 OR lu_application_interest.choice = 2), NULL,
                            CONCAT(
                                interest.name,
                                ' (',
                                CAST( (lu_application_interest.choice+1) AS CHAR),
                                ')'
                            )
                        )
                        ORDER BY lu_application_interest.choice
                        SEPARATOR '|'
                    ) AS areas_of_interest
                    ";
                    break;
                
                case 'int2':

                    $this->querySelect .= 
                    ",
                    GROUP_CONCAT( DISTINCT
                        IF ( (lu_application_interest.choice = 0 OR lu_application_interest.choice = 2), NULL,
                            CONCAT(
                                interest.name,
                                ' (',
                                CAST( (lu_application_interest.choice+1) AS CHAR),
                                ')'
                            )
                        )
                        ORDER BY lu_application_interest.choice
                        SEPARATOR '|'
                    ) AS areas_of_interest
                    ";
                    break;
                    
                case 'int3':

                    $this->querySelect .= 
                    ",
                    GROUP_CONCAT( DISTINCT
                        IF ( (lu_application_interest.choice = 0 OR lu_application_interest.choice = 1), NULL,
                            CONCAT(
                                interest.name,
                                ' (',
                                CAST( (lu_application_interest.choice+1) AS CHAR),
                                ')'
                            )
                        )
                        ORDER BY lu_application_interest.choice
                        SEPARATOR '|'
                    ) AS areas_of_interest
                    ";
                    break;
                
                default:

                    $this->querySelect .= 
                    ",
                    GROUP_CONCAT( DISTINCT
                        CONCAT(
                            interest.name,
                            ' (',
                            CAST( (lu_application_interest.choice+1) AS CHAR),
                            ')'
                        )
                        ORDER BY lu_application_interest.choice
                        SEPARATOR '|'
                    ) AS areas_of_interest
                    "; 
            }
                
        } else {
           
            $this->querySelect .= 
            ",
            GROUP_CONCAT( DISTINCT
                CONCAT(
                    interest.name,
                    ' (',
                    CAST( (lu_application_interest.choice+1) AS CHAR),
                    ')'
                )
                ORDER BY lu_application_interest.choice
                SEPARATOR '|'
            ) AS areas_of_interest
            ";    

            $this->queryOrderBy = 'users.lastname, users.firstname'; 
        }

    }

  
    
}    
?>
