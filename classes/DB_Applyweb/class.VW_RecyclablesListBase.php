<?php
/* 
* A view, suitable for inclusion in a 2D review list array,
* of the application data from the primary (non-lookup) tables 
* that can be efficiently inner joined.
* 
* This is the "foundational" view with which the other
* view data will be merged/appended. 
*/

class VW_RecyclablesListBase extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT DISTINCT
    /* student bio, application status */
    application.id AS application_id,
    application.submitted_date as submitted_date,
    application.created_date as created_date,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name,
    users_info.gender
    ,
    /* undergrad */
    institutes.name AS undergrad_institute_name,
    usersinst.major1,
    CONCAT(usersinst.gpa, ' / ', gpascales.name) AS gpa
    ,
    /* gre */
    CONCAT(grescore.verbalscore, ' / ', grescore.verbalpercentile) 
        AS gre_verbal, 
    CONCAT(grescore.quantitativescore, ' / ', grescore.quantitativepercentile) 
        AS gre_quantitative,
    CONCAT(grescore.analyticalscore, ' / ', grescore.analyticalpercentile) 
        AS gre_analytical,
    CONCAT(grescore.analyticalwritingscore, ' / ', grescore.analyticalwritingpercentile) 
        AS gre_writing  
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN (
        SELECT application_id, MAX(admission_status) AS admission_status
        FROM lu_application_programs
        GROUP BY application_id 
        HAVING admission_status IS NULL
        OR admission_status = 0 
    ) AS unadmitted_applications
        ON application.id = unadmitted_applications.application_id
    INNER JOIN (
        SELECT application.id AS application_id, 
        MIN(IFNULL(enable_recycling, 0)) AS enable_recycling
        FROM application 
        INNER JOIN period_application
            ON application.id = period_application.application_id
        INNER JOIN lu_application_programs 
            ON application.id = lu_application_programs.application_id
        INNER JOIN lu_programs_departments 
            ON lu_application_programs.program_id = lu_programs_departments.program_id
        LEFT OUTER JOIN department_enable_recycling
            ON lu_programs_departments.department_id = department_enable_recycling.department_id
            AND period_application.period_id = department_enable_recycling.period_id
        GROUP BY application_id 
        HAVING enable_recycling = 1
    ) AS recycling_enabled
        ON application.id = recycling_enabled.application_id
    /* student bio, application status */
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    /* undergrad */
    LEFT OUTER JOIN usersinst ON usersinst.user_id = application.user_id 
        AND usersinst.application_id = application.id
        AND usersinst.educationtype = 1
    LEFT OUTER JOIN degreesall ON degreesall.id = usersinst.degree
    LEFT OUTER JOIN institutes ON institutes.id = usersinst.institute_id
    LEFT OUTER JOIN gpascales ON gpascales.id = usersinst.gpa_scale
    /* gre */
    LEFT OUTER JOIN grescore ON grescore.application_id = application.id
    ";
    
    protected $queryWhere = "application.masters_review_waiver = 1";
    
    protected $queryGroupBy = "application.id";
    
    protected $queryOrderBy = 'users.lastname, users.firstname';
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';

}    
?>