<?php
/* 
* A view of gresubjectscore table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListGreSubjectCount extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT 
    gresubjectscore.application_id,
    /*
    COUNT( NULLIF( score, NULL) ) AS ct_gresubject_entered,
    */ 
    COUNT(*) AS ct_gresubject_entered,
    COUNT( NULLIF( scorereceived, 0 ) ) AS ct_gresubject_received
    ";
    
    protected $queryFrom = "FROM gresubjectscore INNER JOIN application ON application.id = gresubjectscore.application_id"; 

    protected $queryWhere = 
    "
    (
    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
    OR (gresubjectscore.name != '' AND gresubjectscore.name IS NOT NULL)
    OR (score !=0 AND score IS NOT NULL)
    OR (percentile != 0 AND percentile IS NOT NULL)
    )
    ";
    
    protected $queryGroupBy = "application_id"; 

}    
?>
