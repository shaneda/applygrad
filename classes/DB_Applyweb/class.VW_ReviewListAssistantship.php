<?php
/* 
* A view of review table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListAssistantship extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    assistantship.requested AS assistantship_requested,
    assistantship.granted AS assistantship_granted
    ";
    
    protected $queryFrom = 
    "
    FROM application
    LEFT OUTER JOIN assistantship
        ON application.id = assistantship.application_id
    ";

    protected $queryGroupBy = 'application.id';
}    
?>
