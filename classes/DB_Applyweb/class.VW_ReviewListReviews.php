<?php
/* 
* A view of review table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListReviews extends VW_ReviewList
{
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    
    /* committee reviewers */
    GROUP_CONCAT( DISTINCT
        IF (
            review.fac_vote != 1,
            CONCAT(users.firstname, ' ', users.lastname),
            NULL
        ) ORDER BY users.lastname, users.firstname
        SEPARATOR '|'    
    ) AS committee_reviewers,

    /* round 1 point 1 scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 1 AND review.point IS NOT NULL,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST( IF( review.department_id = 66, 
                        ROUND(review.point, 1), 
                        ROUND(review.point, 2)) AS CHAR), 
                IF( review.point_certainty IS NULL, '', CONCAT( '/', CAST( ROUND(review.point_certainty) AS CHAR) ) 
                    )
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname
        SEPARATOR '|'
    ) AS round1_point1_scores,
    
    /* round 1 point 1 average */
    ROUND( AVG(
        IF (
        review.round = 1 AND review.point IS NOT NULL,
        IF( review.department_id = 66, 
            ROUND(review.point, 1), 
            ROUND(review.point, 2)),
        NULL
        )
    ), 2) as round1_point1_average,
    
    /* round 1 point 2 scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 1 AND review.point2 IS NOT NULL,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST( IF( review.department_id = 66, 
                        ROUND(review.point2, 1), 
                        ROUND(review.point2, 2)) AS CHAR),  
                IF( review.point2_certainty IS NULL, '', CONCAT( '/', CAST( ROUND(review.point2_certainty) AS CHAR) ) 
                    )
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname
        SEPARATOR '|'
    ) AS round1_point2_scores,
    
    /* round 1 point 2 average */
    ROUND( AVG(
        IF (
        review.round = 1 AND review.point2 IS NOT NULL,
        IF( review.department_id = 66, 
            ROUND(review.point2, 1), 
            ROUND(review.point2, 2)),
        NULL
        )
    ), 2) as round1_point2_average,
    
    /* committee comments round 1 */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 1 AND review.comments != '',
            CONCAT(users.lastname, ': ', review.comments),
            NULL
        )
        ORDER BY users.lastname, users.firstname
        SEPARATOR '|'
    ) AS round1_comments,
    
    /* votes for moving to round 2 */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 1 AND review.round2 IS NOT NULL,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST(review.round2 AS CHAR)
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname 
        SEPARATOR '|'
    ) AS votes_for_round2,
    
    /* round 2 point 1 committee scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 2 AND review.point IS NOT NULL AND fac_vote = 0,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST( IF( review.department_id = 66, 
                        ROUND(review.point, 1), 
                        ROUND(review.point, 2)) AS CHAR), 
                IF( review.point_certainty IS NULL, '', CONCAT( '/', CAST( ROUND(review.point_certainty) AS CHAR) ) )
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname 
        SEPARATOR '|'
    ) AS round2_point1_committee_scores,
    
    /* round 2 point 1 committee average */
        ROUND( AVG(
        IF (
        review.round = 2 AND review.point IS NOT NULL AND fac_vote = 0,
        IF( review.department_id = 66, 
            ROUND(review.point, 1), 
            ROUND(review.point, 2)),
        NULL
        )
    ), 2) as round2_point1_committee_average,
    
    /* round 2 point 2 scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 2 AND review.point2 IS NOT NULL,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST( IF( review.department_id = 66, 
                        ROUND(review.point2, 1), 
                        ROUND(review.point2, 2)) AS CHAR),  
                IF( review.point2_certainty IS NULL, '', CONCAT( '/', CAST( ROUND(review.point2_certainty) AS CHAR) ) 
                    )
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname
        SEPARATOR '|'
    ) AS round2_point2_scores,
    
    /* round 2 point 2 average */
    ROUND( AVG(
        IF (
        review.round = 2 AND review.point2 IS NOT NULL,
        IF( review.department_id = 66, 
            ROUND(review.point2, 1), 
            ROUND(review.point2, 2)),
        NULL
        )
    ), 2) as round2_point2_average,
 
    /* all rounds point 1 committee scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.point IS NOT NULL AND fac_vote = 0,
            IF (
                (review.round = 3 || review.round = 2 || (review.round = 1 && review.reviewer_id NOT IN
                    (SELECT reviewer_id FROM review AS review2 
                    WHERE review2.round = 2 
                    && review2.application_id = review.application_id
                    && review2.reviewer_id = review.reviewer_id))
                ),                
                CONCAT(users.firstname, ' ', users.lastname, ': ', 
                    CAST( IF( review.department_id = 66, 
                            ROUND(review.point, 1), 
                            ROUND(review.point, 2)) AS CHAR), 
                    IF( review.point_certainty IS NULL, '', CONCAT( '/', CAST( ROUND(review.point_certainty) AS CHAR) ) )
                    ),
                NULL
            ),
            NULL
        ) ORDER BY users.lastname, users.firstname 
        SEPARATOR '|'
    ) AS all_point1_committee_scores,
    
    /* all rounds point 1 committee average */
    ROUND( AVG(
        IF (
            review.point IS NOT NULL AND fac_vote = 0,
            IF (
                (review.round = 3 || review.round = 2 || (review.round = 1 && review.reviewer_id NOT IN
                    (SELECT reviewer_id FROM review AS review2 
                    WHERE review2.round = 2 
                    && review2.application_id = review.application_id
                    && review2.reviewer_id = review.reviewer_id))
                ),
                IF (
                    review.department_id = 66, 
                    ROUND(review.point, 1), 
                    ROUND(review.point, 2)
                ),
                NULL
            ),
            NULL
        )
    ), 2) as all_point1_committee_average,
    
    /* all rounds point 2 committee scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.point2 IS NOT NULL AND fac_vote = 0,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST( IF( review.department_id = 66, 
                        ROUND(review.point2, 1), 
                        ROUND(review.point2, 2)) AS CHAR), 
                IF( review.point2_certainty IS NULL, '', CONCAT( '/', CAST( ROUND(review.point_certainty) AS CHAR) ) )
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname 
        SEPARATOR '|'
    ) AS all_point2_committee_scores,
    
    /* all rounds point 2 committee average */
        ROUND( AVG(
        IF (
        review.point2 IS NOT NULL AND fac_vote = 0,
        IF( review.department_id = 66, 
            ROUND(review.point2, 1), 
            ROUND(review.point2, 2)),
        NULL
        )
    ), 2) as all_point2_committee_average,

    /* round 2 point 1 faculty scores */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 2 AND review.point IS NOT NULL AND fac_vote = 1,
            CONCAT(users.firstname, ' ', users.lastname, ': ', 
                CAST( ROUND(review.point, 2) AS CHAR)
                ),
            NULL
        ) ORDER BY users.lastname, users.firstname
        SEPARATOR '|'
    ) AS round2_point1_faculty_scores,
    
    /* round 2 point 1 faculty average */
        ROUND( AVG(
        IF (
        review.round = 2 AND review.point IS NOT NULL AND fac_vote = 1,
        review.point,
        NULL
        )
    ), 2) as round2_point1_faculty_average,
    
    /* committee comments round 2 */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 2 AND review.comments != '' AND review.comments IS NOT NULL,
            CONCAT(users.firstname, ' ', users.lastname, ': ', review.comments),
            NULL
        )
        ORDER BY users.lastname, users.firstname
        SEPARATOR '|'
    ) AS round2_comments,
    
    /* faculty comments */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 2 AND review.pertinent_info != '' AND review.pertinent_info IS NOT NULL,
            CONCAT(users.firstname, ' ', users.lastname, ': ', review.pertinent_info),
            NULL
        )
        ORDER BY users.lastname, users.firstname 
        SEPARATOR '|'
    ) AS round2_pertinent_info,
    
    /* decision */
    GROUP_CONCAT( DISTINCT
        lu_application_programs.decision
        SEPARATOR '|'
    ) AS decision,
    
    GROUP_CONCAT( DISTINCT 
        CASE lu_application_programs.admission_status when 1 then 
            CONCAT(
                if(degree.name is not null, degree.name,''), ' ',
                if(programs.linkword is not null, programs.linkword,''), ' ',
                if(fieldsofstudy.name is not null, fieldsofstudy.name,''), 
                ' (wait)'
            ) 
        WHEN 2 THEN
            CONCAT(
                degree.name, ' ', 
                if(programs.linkword is not null, programs.linkword,''),
                ' ', 
                fieldsofstudy.name
            )
        END
        SEPARATOR '|'
    ) AS admit_to,
    
    '' AS admit_to_decision 
    /*
    application_decision.admit_to_decision   
    */
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id

    /* reviewers */
    INNER JOIN review ON review.application_id = application.id
    INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = review.reviewer_id
    INNER JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    
    /* admitted program */ 
    /*
    LEFT OUTER JOIN
    (
        SELECT
        application_decision.application_id,
        application_decision.program_id,
        GROUP_CONCAT( DISTINCT
            CASE application_decision.admission_status when 1 then
                CONCAT(
                    if(admit_degree.name is not null, admit_degree.name,''), ' ',
                    if(admit_programs.linkword is not null, admit_programs.linkword,''), ' ',
                    if(admit_fieldsofstudy.name is not null, admit_fieldsofstudy.name,''),
                    ' (wait)'
                )
            WHEN 2 THEN
                CONCAT(
                    admit_degree.name, ' ',
                    if(admit_programs.linkword is not null, admit_programs.linkword,''),
                    ' ',
                    admit_fieldsofstudy.name
                )
            END
            SEPARATOR '|'
        ) AS admit_to_decision 
        FROM application_decision
        LEFT OUTER JOIN programs AS admit_programs
          ON admit_programs.id = application_decision.admission_program_id
        LEFT OUTER JOIN degree AS admit_degree
          ON admit_degree.id = admit_programs.degree_id
        LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy
          ON admit_fieldsofstudy.id = admit_programs.fieldofstudy_id
        GROUP BY application_decision.application_id
    ) AS application_decision
        ON lu_application_programs.application_id = application_decision.application_id
        AND lu_application_programs.program_id = application_decision.program_id
    */
    ";
    
    protected $queryWhere = 'review.department_id = lu_programs_departments.department_id';
    
    protected $queryGroupBy = 'application.id, lu_programs_departments.department_id';
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
}    
?>
