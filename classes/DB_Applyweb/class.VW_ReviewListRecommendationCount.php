<?php
/* 
* A view of recommend table data suitable for inclusion 
* in a 2D admin list array.
*/

/*
Test for formless industrial and academic recommendations.

SELECT DISTINCT recommend.id
FROM recommend
LEFT OUTER JOIN recommendforms
ON recommend.id = recommendforms.recommend_id
WHERE (recommendtype = 2
OR recommendtype = 3)
AND recommendforms.id IS NULL 
*/

class VW_ReviewListRecommendationCount extends VW_AdminList
{
    protected $joinPeriodApplication = FALSE;
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
    protected $querySelect = 
    "
    SELECT recommend.application_id, 
    COUNT( NULLIF( recommend.rec_user_id, -(1) ) ) AS ct_recommendations_requested,
    COUNT(
        IF (recommend.submitted = 0, NULL,
            IF ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 
                IF ( ((recommendtype = 2 OR recommendtype = 3) AND recommendforms.recommend_id IS NULL), NULL, 1)
            ) 
        ) 
    ) AS ct_recommendations_submitted,
    COUNT(
        IF (recommend.submitted = 0, NULL,
            IF ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 1)
        ) 
    ) AS ct_recommendation_files_submitted,
    /*
    * ct_recommendation_files_forms_submitted is for use in MS-HCII cases, 
    * because a department test is unfeasible here.
    */
    COUNT(
        IF (recommend.submitted = 0, NULL,
            IF ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 
                IF ( (recommendforms.recommend_id IS NULL), NULL, 1)
            ) 
        ) 
    ) AS ct_recommendation_files_forms_submitted, max(datafileinfo.moddate) as recommend_last_letter_date
    ";
    
    protected $queryFrom = "FROM recommend 
        INNER JOIN application ON application.id = recommend.application_id
        INNER JOIN period_application ON application.id = period_application.application_id
        INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
        INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
        LEFT OUTER JOIN datafileinfo on datafileinfo.id = recommend.datafile_id
        LEFT OUTER JOIN (SELECT DISTINCT recommend_id FROM recommendforms)
            AS recommendforms ON recommend.id = recommendforms.recommend_id";
    
    protected $queryGroupBy = "application_id";

}    
?>
