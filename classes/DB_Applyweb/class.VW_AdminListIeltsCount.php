<?php
/* 
* A view of ieltsscore table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListIeltsCount extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT 
    ieltsscore.application_id,
    /*
    COUNT( NULLIF( testdate, NULL) ) AS ct_ielts_entered,
    */
    COUNT(*) AS ct_ielts_entered,
    COUNT( NULLIF( scorereceived, 0 ) ) AS ct_ielts_received
    ";
    
    protected $queryFrom = "FROM ieltsscore INNER JOIN application ON application.id = ieltsscore.application_id"; 
    
    protected $queryWhere = 
    "
    (
    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
    OR (listeningscore != 0 AND listeningscore IS NOT NULL)
    OR (readingscore != 0 AND readingscore IS NOT NULL)
    OR (writingscore != 0 AND writingscore IS NOT NULL)
    OR (speakingscore != 0 AND speakingscore IS NOT NULL)
    OR (overallscore != 0 AND overallscore IS NOT NULL)
    )
    ";

    protected $queryGroupBy = "application_id"; 

}    
?>
