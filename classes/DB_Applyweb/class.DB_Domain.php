<?php
/*
Class for domain table data access.
*/

class DB_Domain extends DB_Applyweb
{

    function get($domainId = NULL) {

        $query = "SELECT domain.* FROM domain"; 
        if ($domainId) {
            $query .=  " WHERE id = " . $domainId ;   
        }
        $query .= " ORDER BY domain.name";

        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

}

?>