<?php
/* 
* A view, suitable for inclusion in a 2D review list array,
* that shows which applications a particular reviewer has touched.
* 
* NOTE: The constructor has a reviewerId parameter.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListTouched extends VW_ReviewList
{

    
    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    
    IF ( MIN(review.round) = 1, 1, 0) AS round1_reviewer_touched,
    IF ( MAX(review.round) = 2, 1, 0) AS round2_reviewer_touched,
    IF ( MAX(review.round) = 3, 1, 0) AS round3_reviewer_touched,
    
    IF (review.round = 1, review.comments, NULL) AS round1_reviewer_comments,
    IF (review.round = 2, review.comments, NULL) AS round2_reviewer_comments,
    
    IF (review.round = 1 AND review.point IS NOT NULL, 
        CONCAT(
            CAST(review.point AS CHAR), 
            IF ( review.point_certainty IS NULL, 
                '', 
                CONCAT( ' / ', CAST(review.point_certainty AS CHAR) ) 
            )
        ), 
        NULL
    ) AS round1_reviewer_point1,
    IF (review.round = 2 AND review.point IS NOT NULL, 
        CONCAT(
            CAST(review.point AS CHAR), 
            IF ( review.point_certainty IS NULL, 
                '', 
                CONCAT( ' / ', CAST(review.point_certainty AS CHAR) ) 
            )
        ), 
        NULL
    ) AS round2_reviewer_point1,

    IF (review.round = 1 AND review.point2 IS NOT NULL, 
        CONCAT(
            CAST(review.point2 AS CHAR), 
            IF ( review.point2_certainty IS NULL, 
                '', 
                CONCAT( ' / ', CAST(review.point2_certainty AS CHAR) ) 
            )
        ), 
        NULL
    ) AS round1_reviewer_point2,
    IF (review.round = 2 AND review.point2 IS NOT NULL, 
        CONCAT(
            CAST(review.point2 AS CHAR), 
            IF ( review.point2_certainty IS NULL, 
                '', 
                CONCAT( ' / ', CAST(review.point2_certainty AS CHAR) ) 
            )
        ), 
        NULL
    ) AS round2_reviewer_point2,
    
    IF (review.round = 1 AND review.rank IS NOT NULL, 
        review.rank, 
        NULL
    ) AS round1_reviewer_rank,
    IF (review.round = 2 AND review.rank IS NOT NULL, 
        review.rank, 
        NULL
    ) AS round2_reviewer_rank
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN review ON review.application_id = application.id
    INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = review.reviewer_id
    INNER JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
    ";

    protected $queryGroupBy = 'application.id';

    public function __construct($reviewerId = -1, $departmentName = null) {
        parent::__construct($departmentName);

        if ($reviewerId != -1) {
            $this->queryWhere = 'review.reviewer_id = ' . $reviewerId . 
                                ' AND (review.department_id = lu_programs_departments.department_id
                                    OR lu_user_department.department_id = lu_programs_departments.department_id)';    
        } else {
        
            $this->queryWhere = '(review.department_id = lu_programs_departments.department_id 
                                    OR lu_user_department.department_id = lu_programs_departments.department_id)';    
        }
    } 
    
}    
?>
