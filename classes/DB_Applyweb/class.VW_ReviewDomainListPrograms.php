<?php
class VW_ReviewDomainListPrograms extends VW_ReviewList
{
    // These tables are already joined in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    protected $joinDomainDepartment = TRUE;

    protected $querySelect = 
    "
    SELECT
    lu_application_programs.application_id,
    
    /* programs */
    GROUP_CONCAT( DISTINCT
      CONCAT(
        degree.name,
        ' ',
        programs.linkword,
        ' ',
        fieldsofstudy.name,
        ' (',
        CAST(lu_application_programs.choice AS CHAR),
        ')'
        )
      ORDER BY lu_application_programs.choice
      SEPARATOR '|'
      ) AS programs
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs AS lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    ";
    
    protected $queryGroupBy;

    public function find(
        $unit = NULL,
        $unitId = NULL,
        $periodId = NULL,
        $submitted = 1) 
    {
        $joins = array();
        $wheres = array();
        
        if($this->joinApplicationPrograms && $this->joinProgramsDepartments) 
        {
            $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id"; 
            $joins[] = "INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id";            
        }
        
        if ($unit && $unitId) 
        {
            switch ($unit) 
            {
                case 'program':
                
                    $wheres[] = "lu_programs_departments.program_id = " . $unitId;
                    $this->queryGroupBy = "application.id, lu_application_programs.program_id";
                    break;    
                
                case 'domain':
                
                    if($this->joinDomainDepartment) 
                    {
                        $joins[] = "INNER JOIN lu_domain_department on lu_programs_departments.department_id = lu_domain_department.department_id";    
                    }
                    $wheres[] = "lu_domain_department.domain_id = " . $unitId;
                    $this->queryGroupBy = "application.id, lu_domain_department.domain_id";
                    break;
                
                case 'department':
                default:
                
                    $wheres[] = "lu_programs_departments.department_id = " . $unitId;
                    $this->queryGroupBy = "application.id, lu_programs_departments.department_id";

            }   
        }
        
        if ($periodId) 
        {
            $joins[] = "INNER JOIN period_application ON application.id = period_application.application_id";
            $wheres[] = "period_application.period_id = " . $periodId;
        }

        if ($submitted) {
            $wheres[] = "application.submitted = 1";
        }

        $query = $this->querySelect;

        if ($this->searchSelect) {
            $query .= $this->searchSelect;
        }
        
        $query .= $this->queryFrom;
        if ( isset($joins) ) {
            $query .= ' ' . implode(' ', $joins);     
        }
        
        if ($this->searchJoin) {
            $query .= $this->searchJoin;
        }
        
        if ($this->queryWhere) {
            $query .= ' WHERE ' . $this->queryWhere;
            if ( isset($wheres) ) {
                $query .= ' AND ' . implode(' AND ', $wheres);     
            }        
        } elseif ( isset($wheres) ) {
            $query .= ' WHERE ' . implode(' AND ', $wheres);     
        }
        
        if ($this->queryGroupBy) {
            $query .= " GROUP BY " . $this->queryGroupBy;    
        }
        
        if ($this->queryHaving) {
            $query .= " HAVING " . $this->queryHaving;    
        }
        
        if ($this->queryOrderBy) {
            $query .= " ORDER BY " . $this->queryOrderBy;    
        }
        
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    }
}    
?>
