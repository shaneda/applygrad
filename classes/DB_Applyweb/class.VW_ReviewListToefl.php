<?php
/* 
* A view of toefl table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not return a record for every application.
*/

class VW_ReviewListToefl extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    toefl.testdate AS toefl_testdate,
    toefl.section1 AS toefl_section1,
    toefl.section2 AS toefl_section2,
    toefl.section3 AS toefl_section3,
    toefl.essay AS toefl_essay,
    toefl.total AS toefl_total,
    toefl.type AS toefl_type
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN toefl ON toefl.application_id = application.id
    ";

    protected $queryWhere = 
    "
    toefl.section1 != 0
    AND toefl.section2 IS NOT NULL
    AND toefl.section2 != 0
    AND toefl.section3 IS NOT NULL
    AND toefl.section3 != 0
    AND toefl.essay IS NOT NULL
    AND toefl.essay != 0
    AND toefl.total IS NOT NULL
    AND toefl.total != 0
    ";
    
    // For students who have multiple tests, this should get the latest one.
    protected $queryOrderBy = 'application.id, toefl.testdate';
    
}    
?>
