<?php
/* 
* A view, suitable for inclusion in a 2D admin list array,
* of the application data from the primary (non-lookup) tables 
* that can be efficiently inner joined.
* 
* This is the "foundational" view with which the other
* view data will be merged/appended. 
*/

class VW_AdmissionLetterListBaseDesignMasters extends VW_AdminList
{
    
    // These are joined in $queryFrom
    protected $joinUsersinst = FALSE;
    protected $joinGrescore = FALSE;
    
    // Select/join only application, gre and transcript to avoid products from
    // multiple on-to-many joins and slowness from subqueries.
    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    lu_users_usertypes.id AS user_id,
    users.guid,
    CONCAT(users.lastname, ', ', users.firstname) AS name,
    users.email,
    GROUP_CONCAT( 
        DISTINCT CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name)
        ORDER BY application_decision_design.choice 
        SEPARATOR '|'
    ) AS admitted_programs,
    application_decision_design.timestamp
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    INNER JOIN application_decision_design 
        ON application.id = application_decision_design.application_id
        AND application_decision_design.admission_status = 2
    INNER JOIN programs 
        ON application_decision_design.admission_program_id = programs.id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    ";
    
    protected $queryWhere = 'lu_users_usertypes.usertype_id = 5 ';
    
    protected $queryGroupBy = "application.id";
    
    protected $queryOrderBy = 'users.lastname, users.firstname';
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';
    
    // Override findByApplicationId to resolve application_id ambiguity 
    protected function findByApplicationId($applicationId, $admissionPeriodId = NULL, 
                                            $unit = NULL, $unitId = NULL) {
    
        unset($this->findWheres);
        
        if ($admissionPeriodId) {
            $this->setAdmissionPeriod($admissionPeriodId, $unit);    
        }
        if ($unit && $unitId) {
            $this->setUnit($unit, $unitId);
        }
        $this->findWheres[] = "application.id = " . $applicationId;

        $query = $this->assembleFindQuery();   
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);            
        return $resultArray;   
    }
    
    
    // Override the find method to include the filter parameters.
    public function find($unit = NULL, $unitId = NULL, $admissionPeriodId = NULL, 
                            $applicationId = NULL, $searchString = '',  
                            $submissionStatus = 'all', $completionStatus = 'all', 
                            $paymentStatus = 'all', $testScoreStatus = 'all',  
                            $transcriptStatus = 'all', $recommendationStatus = 'all',
                            $luUsersUsertypesId = NULL) {
                            
        // If applicationId (from the autocomplete, e.g.), get the data and return
        // NOTE: application.id must be selected as application_id in $querySelect
        if ($applicationId || is_numeric($searchString)) {
            if (!$applicationId) {
                $applicationId = intval($searchString);    
            } 
            return $this->findByApplicationId($applicationId, $admissionPeriodId,
                                                $unit, $unitId);
        }
                                
        if ($luUsersUsertypesId) {
            return $this->findByLuuId($luUsersUsertypesId, $unit, $unitId, $admissionPeriodId); 
        }

        // Otherwise.... 
                               
        if ($unit && $unitId) {
            $this->setUnit($unit, $unitId);
        }

        if ($admissionPeriodId) {
            $this->setAdmissionPeriod($admissionPeriodId, $unit);
        }
        
        if ($searchString) {
            
            $this->setSearchString($searchString);   
        
        } else {
        
            $this->setSubmissionStatus($submissionStatus);

            switch ($completionStatus) {
                
                case 'incomplete':

                    $this->findWheres[] = "application.sent_to_program = 0";
                    break;
                
                case 'complete':
                
                    $this->findWheres[] = "application.sent_to_program = 1";
                    break;    
                
                default:
                    // no complete where
            }

            switch ($paymentStatus) {
                
                case 'unpaidUnwaived':

                    $this->findWheres[] = "application.paid = 0";
                    $this->findWheres[] = "application.waive = 0";
                    break;

                case 'paidWaived':

                    $this->findWheres[] = "(application.paid = 1 OR application.waive = 1)";
                    break;            

                case 'paid':
                
                    $this->findWheres[] = "application.paid = 1";
                    break;    

                case 'waived':
                
                    $this->findWheres[] = "application.waive = 1";
                    break;

                default:
                    // no paid/waive where
            }

            $joinGreSubjectTable = $joinToeflTable = $joinIeltsTable = FALSE;
            switch ($testScoreStatus) {
                
                case 'anyTestIncomplete':

                    $joinGreSubjectTable = $joinToeflTable = TRUE;
                    $this->findSelects[] = "greSubjectCount.ct_gresubject_entered, greSubjectCount.ct_gresubject_received";
                    $this->findSelects[] = "toeflCount.toefl_entered, toeflCount.toefl_submitted";
                    $this->findWheres[] = "( (grescore.scorereceived = 0)
                                            OR ( greSubjectCount.ct_gresubject_entered IS NOT NULL
                                                AND greSubjectCount.ct_gresubject_entered != greSubjectCount.ct_gresubject_received)
                                            OR (toeflCount.toefl_entered IS NOT NULL 
                                                AND toeflCount.toefl_entered > 0 
                                                AND toeflCount.toefl_submitted < 1) )";                     
                    break;

                case 'greIncomplete':

                    $this->findWheres[] = "grescore.scorereceived = 0";
                    break;            

                case 'greSubjectIncomplete':
                
                    $joinGreSubjectTable = TRUE;
                    $this->findWheres[] = "greSubjectCount.ct_gresubject_entered IS NOT NULL";
                    $this->findWheres[] = "greSubjectCount.ct_gresubject_entered != greSubjectCount.ct_gresubject_received";
                    break;    

                case 'toeflIncomplete':
                
                    $joinToeflTable = TRUE;
                    $this->findWheres[] = "toeflCount.toefl_entered IS NOT NULL";
                    $this->findWheres[] = "toeflCount.toefl_entered > 0";
                    $this->findWheres[] = "toeflCount.toefl_submitted < 1";
                    break;

                case 'allTestsComplete':
                
                    $joinGreSubjectTable = $joinToeflTable = TRUE;
                    $this->findWheres[] = "grescore.scorereceived = 1";
                    $this->findWheres[] = "(greSubjectCount.ct_gresubject_entered IS NULL
                                            OR (greSubjectCount.ct_gresubject_entered = greSubjectCount.ct_gresubject_received) )";
                    $this->findWheres[] = "(toeflCount.toefl_entered IS NULL
                                            OR (toeflCount.toefl_entered > 0
                                            AND toeflCount.toefl_submitted > 0) )";
                    break;
                    
                case 'esl':
                
                    $joinToeflTable = TRUE;
                    $joinIeltsTable = TRUE;
                    $this->findWheres[] = "(LOWER(TRIM(users_info.native_tongue)) != 'english'
                        AND (toeflCount.toefl_entered IS NULL || toeflCount.toefl_entered = 0)
                        AND (ieltsCount.ct_ielts_entered IS NULL || ieltsCount.ct_ielts_entered = 0))";
                
                    break;
                    
                default:
                    // no paid/waive where
            }
            if ($joinGreSubjectTable) {
                $greSubjectView = new VW_AdminListGreSubjectCount();
                $this->findJoins[] = "LEFT OUTER JOIN (" . $greSubjectView->assembleFindQuery() . ")
                                        AS greSubjectCount ON application.id = greSubjectCount.application_id"; 
            }
            if ($joinToeflTable) {
                $toeflView = new VW_AdminListToeflCount();
                $this->findJoins[] = "LEFT OUTER JOIN (" . $toeflView->assembleFindQuery() . ")
                                        AS toeflCount ON application.id = toeflCount.application_id"; 
            }
            if ($joinIeltsTable) {
                $ieltsView = new VW_AdminListIeltsCount();
                $this->findJoins[] = "LEFT OUTER JOIN (" . $ieltsView->assembleFindQuery() . ")
                                        AS ieltsCount ON application.id = ieltsCount.application_id"; 
            }
            
            switch ($transcriptStatus) {
                
                case 'transcriptsIncomplete':

                    $this->findHavings[] = "(ct_transcripts_submitted IS NULL
                                            OR ct_transcripts_submitted = 0
                                            OR (ct_transcripts_submitted != ct_transcripts_received) )";
                    break;
                
                case 'transcriptsComplete':
                
                    $this->findHavings[] = "(ct_transcripts_submitted IS NOT NULL
                                            AND ct_transcripts_submitted > 0
                                            AND ct_transcripts_submitted = ct_transcripts_received)";
                    break;    
                
                default:
                    // no transcript where
            }            

            $joinRecommendTable = FALSE;
            switch ($recommendationStatus) {
                
                case 'recommendationsIncomplete':

                    $joinRecommendTable = TRUE;
                    $this->findSelects[] = "ct_recommendations_requested, ct_recommendations_submitted"; 
                    $this->findHavings[] = "(ct_recommendations_requested IS NULL
                                            OR (ct_recommendations_requested != ct_recommendations_submitted) )";
                    break;
                
                case 'recommendationsComplete':
                
                    $joinRecommendTable = TRUE;
                    $this->findSelects[] = "ct_recommendations_requested, ct_recommendations_submitted"; 
                    $this->findHavings[] = "(ct_recommendations_requested IS NOT NULL
                                            AND ct_recommendations_requested = ct_recommendations_submitted)";
                    break;    
                
                default:
                    // no recommendation where
            }
            if ($joinRecommendTable) {
                $recommendationView = new VW_AdminListRecommendationCount();
                $this->findJoins[] = "LEFT OUTER JOIN (" . $recommendationView->assembleFindQuery() . ")
                                        AS recommendationCount ON application.id = recommendationCount.application_id";                
            }             
        }
    
        if (isset($_SESSION['roleDepartmentId']) && $_SESSION['roleDepartmentId'] == 5 && $_SESSION['A_usertypeid'] == 21) {
            $this->findWheres[] = 'application.masters_review_waiver = 1';
        }
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    }
}    
?>
