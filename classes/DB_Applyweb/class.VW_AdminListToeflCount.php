<?php
/* 
* A view of recommend table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListToeflCount extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT toefl.application_id, 
    /*
    IF( COUNT(testdate) >= 1, COUNT(testdate), 0 ) AS toefl_entered,
    */
    IF( COUNT(*) >= 1, COUNT(*), 0 ) AS toefl_entered,  
    IF( COUNT( nullif(scorereceived, 0) ) >= 1, COUNT( nullif(scorereceived, 0) ), 0) AS toefl_submitted
    ";
    
    protected $queryFrom = "FROM toefl INNER JOIN application ON application.id = toefl.application_id"; 
    
    protected $queryWhere = 
    "
    (
    (toefl.testdate != '0000-00-00' AND toefl.testdate NOT LIKE '0000-00-01%' AND toefl.testdate IS NOT NULL)
    OR (toefl.section1 != 0 AND toefl.section1 IS NOT NULL)
    OR (toefl.section2 != 0 AND toefl.section2 IS NOT NULL)
    OR (toefl.section3 != 0 AND toefl.section3 IS NOT NULL)
    OR (toefl.essay != 0 AND toefl.essay IS NOT NULL)
    OR (toefl.total != 0 AND toefl.total IS NOT NULL)
    )
    ";
    
    protected $queryGroupBy = "application_id";

}    
?>
