<?php
/*
* Class for querying a MySQL DB.
* Loosely based on:
* http://toys.lerdorf.com/archives/38-The-no-framework-PHP-MVC-framework.html
*/

class DB_Applyweb2
{
    protected $db;
    protected $db_host;
    protected $db_username;
    protected $db_password;
    protected $mysqli;

    function __construct($db, $db_host, $db_username, $db_password) {

        $this->db = $db;
        $this->db_host = $db_host;
        $this->db_username = $db_username;
        $this->db_password = $db_password;
        
        $this->connect();
    }

    function __destruct() {

        if (isset($this->mysqli)) {
            $this->mysqli->close();
        }
    }

    public function connect() {
        
        // Establish the db connection.
        $this->mysqli = new mysqli($this->db_host, $this->db_username, $this->db_password); 
           
        // Exit if there is a connection error.
        if ($this->mysqli->connect_error) {
            $this->fatal_error( mysqli_connect_error() );
        }
        
        // Select the db; exit if there is an error.
        if ( !$this->mysqli->select_db($this->db) ) {
            $this->fatal_error( $this->mysqli->error );
        }
    }
    
    public function getDb() {
        return $this->db;
    }
    
    public function getDbHostname() {
        
        if ($this->db_host == localhost) {
            
            return gethostname();
            
        } else {
            
            return $this->db_host;
        }
    }
    
    public function tableExists($tableName) {
        
        $query = "SHOW TABLES WHERE Tables_in_" . $this->db .
            " = '" . $this->escapeString($tableName) . "'";
            
        $result = $this->mysqli->query($query);
        
        if ($result->num_rows > 0) {
            
            return TRUE;
        
        } else {
            
            return FALSE;
        }
    }
    
    /*
    * Submit a select query, fetch each result row as an associative array
    * with field names as keys, and return a 2D array of result row arrays.
    * 
    * If arrayKey is set with a field name, the result rows will be indexed 
    * with the value of that field.
    * 
    * If returnArray is set to false, the result object will be returned. 
    */ 
    private function handleReadQuery($query, $arrayKey = "", $returnArray = TRUE) {
    
        // Connect to db if established connection not available.
        if (!isset($this->mysqli)) {
            $this->connect();
        } 

        // Tell MySQL to treat all data as UTF8.
        $this->mysqli->query("SET NAMES 'utf8'");

        // Increase the size limit of GROUP_CONCAT strings.
        $this->mysqli->query("SET SESSION group_concat_max_len=4096"); 
        
        // Create a new array to hold query results.
        $resultArray = array();

        // Execute the query.
        $resultObject = $this->mysqli->query($query);
        
        if ($returnArray) {
            
            // Move results into results_arrray.
            while ($row = $resultObject->fetch_assoc()) {
                
                if ($arrayKey) {
                    $key = $row[$arrayKey];
                    $resultArray[$key] = $row;     
                } else {
                    $resultArray[] = $row;    
                }
                
            }
            
            $resultObject->free();
            
            return $resultArray;
                
        } else {
            
            return $resultObject;
        }
    }
    
    /*
    * Submit an insert, update, or delete query and return the number of affected rows. 
    */
    private function handleCreateUpdateDeleteQuery($query) {
    
        // Connect to db if established connection not available
        if (!$this->mysqli) {
            self::connect();
        } 
        
        // Execute the query.
        $this->mysqli->query($query);
        
        // Retunr the number of affected rows.
        return $this->mysqli->affected_rows;    
    }  

    public function handleSelectQuery($query, $arrayKey = "", $returnArray = TRUE) {
        
        return $this->handleReadQuery($query, $arrayKey, $returnArray);
    }

    public function handleInsertQuery($query) {
    
        return $this->handleCreateUpdateDeleteQuery($query);      
    }

    public function handleUpdateQuery($query) {
    
        return $this->handleCreateUpdateDeleteQuery($query);       
    }
    
    public function handleDeleteQuery($query) {
    
        return $this->handleCreateUpdateDeleteQuery($query);       
    }
    
    public function escapeString($string) {
        return $this->mysqli->real_escape_string($string);
    }
    
    public function getLastInsertId() {
        return $this->mysqli->insert_id;
    }
    
    // Use for all db-related errors.
    protected function fatal_error($msg) {
        echo "<pre>Error!: $msg\n";
        $bt = debug_backtrace();
        foreach($bt as $line) {
            $args = var_export($line['args'], true);
            echo "{$line['function']}($args) at {$line['file']}:{$line['line']}\n";
        }
        echo "</pre>";
        die();
    }

}
?>