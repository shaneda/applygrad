<?php
class VW_ReviewListMoeller extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    moeller_data.score AS moeller_score,
    moeller_data.rank AS moeller_rank,
    moeller_data.n_count AS moeller_n_count,
    moeller_data.n_group AS moeller_n_group
    ";
    
    protected $queryFrom = 
    "
    FROM application
    LEFT OUTER JOIN moeller_data ON application.id = moeller_data.id
    ";
    
    protected $queryGroupBy = 'application.id';

}    
?>
