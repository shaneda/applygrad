<?php
/*
Class for programs table data access.
*/

class DB_Programs extends DB_Applyweb
{

    function get($programsId = NULL) {

        $query = "SELECT programs.* FROM programs"; 
        if ($programsId) {
            $query .=  " WHERE id = " . $programsId ;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }
    
    function getWithLookup($programsId = NULL) {

        $query = "SELECT programs.*, 
                    CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS name,
                    department.name AS department
                    FROM programs
                    INNER JOIN degree ON degree.id = programs.degree_id
                    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
                    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = programs.id
                    INNER JOIN department ON department.id = lu_programs_departments.department_id 
                    "; 
        if ($programsId) {
            $query .=  " WHERE id = " . $programsId ;   
        }
        
        $query .= " ORDER BY name";

        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

}

?>