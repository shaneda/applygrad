<?php
/* 
* A view of gmatscore table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListGmatCount extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT 
    gmatscore.application_id,
    COUNT(*) AS ct_gmat_entered,
    COUNT( NULLIF( scorereceived, 0 ) ) AS ct_gmat_received
    ";
    
    protected $queryFrom = "FROM gmatscore INNER JOIN application ON application.id = gmatscore.application_id"; 
    
    protected $queryWhere = 
    "
    (
    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
    OR (verbalscore IS NOT NULL)
    OR (verbalpercentile IS NOT NULL)
    OR (quantitativescore IS NOT NULL)
    OR (quantitativepercentile IS NOT NULL)
    OR (totalscore IS NOT NULL)
    OR (totalpercentile IS NOT NULL) 
    OR (analyticalwritingscore IS NOT NULL) 
    OR (analyticalwritingpercentile IS NOT NULL)  
    )
    ";

    protected $queryGroupBy = "application_id"; 

}    
?>
