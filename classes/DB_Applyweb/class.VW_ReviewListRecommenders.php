<?php
/* 
* A view of recommend table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not return a record for every application.
*/

class VW_ReviewListRecommenders extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    GROUP_CONCAT( DISTINCT
        CONCAT(
            users.firstname, ' ', users.lastname,
            ' (', users.title, ', ', users_info.company, ')'
            )
    SEPARATOR '|'
    ) AS recommenders
    ";
    
    protected $queryFrom = 
    "
    FROM application
    LEFT OUTER JOIN recommend ON recommend.application_id = application.id
    LEFT OUTER JOIN lu_users_usertypes ON lu_users_usertypes.id = recommend.rec_user_id
    LEFT OUTER JOIN users ON lu_users_usertypes.user_id = users.id
    LEFT OUTER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    ";
    
    protected $queryGroupBy = 'application.id';
    
}    
?>
