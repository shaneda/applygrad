<?php
/* 
* A view, suitable for inclusion in a 2D review list array,
* of the student data from the application, users, and users_info tables.
* 
* This is the "foundational" view with which the other
* view data will be merged/appended. 
*/

class VW_ReviewListStudents extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name,
    users_info.gender,
    ethnicity.name AS ethnicity,
    countries_cit.iso_code AS cit_country_iso_code,
    countries_cit.name AS cit_country
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
    LEFT OUTER JOIN visatypes ON users_info.visastatus = visatypes.id
    LEFT OUTER JOIN countries AS countries_cit ON users_info.cit_country = countries_cit.id
    ";
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';

}    
?>
