<?php

class VW_ReviewDomainListBase extends VW_ReviewList
{
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';
    
    protected $joinDomainDepartment = TRUE;
    
    protected $querySelect = 
    "
    SELECT DISTINCT
    /* student bio, application status */
    application.id AS application_id,
    application.submitted_date as submitted_date,
    application.created_date as created_date,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name,
    users_info.gender,
    CONCAT( 
        IF(ethnicity.name = 'Hispanic/Latino', CONCAT(ethnicity.name, ', '), ''), 
        IFNULL( GROUP_CONCAT(DISTINCT ipeds_race.ipeds_race SEPARATOR ', '), '') 
    ) AS ethnicity,
    countries_cit.iso_code AS cit_country_iso_code,
    countries_cit.name AS cit_country
    ,
    /* undergrad */
    institutes.name AS undergrad_institute_name,
    usersinst.major1,
    usersinst.date_grad,
    CONCAT(usersinst.gpa, ' / ', gpascales.name) AS gpa
    ,
    /* gre */
    CONCAT(grescore.verbalscore, ' / ', grescore.verbalpercentile) 
        AS gre_verbal, 
    CONCAT(grescore.quantitativescore, ' / ', grescore.quantitativepercentile) 
        AS gre_quantitative,
    CONCAT(grescore.analyticalscore, ' / ', grescore.analyticalpercentile) 
        AS gre_analytical,
    CONCAT(grescore.analyticalwritingscore, ' / ', grescore.analyticalwritingpercentile) 
        AS gre_writing  
    ";
    
    protected $queryFrom = 
    "
    FROM application
    /* student bio, application status */
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
    LEFT OUTER JOIN applicant_ipeds_race ON lu_users_usertypes.id = applicant_ipeds_race.lu_users_usertypes_id
    LEFT OUTER JOIN ipeds_race on ipeds_race.ipeds_race_id = applicant_ipeds_race.ipeds_race_id
    LEFT OUTER JOIN visatypes ON users_info.visastatus = visatypes.id
    LEFT OUTER JOIN countries AS countries_cit ON users_info.cit_country = countries_cit.id
    /* undergrad */
    LEFT OUTER JOIN usersinst ON usersinst.user_id = application.user_id 
        AND usersinst.application_id = application.id
        AND usersinst.educationtype = 1
    LEFT OUTER JOIN degreesall ON degreesall.id = usersinst.degree
    LEFT OUTER JOIN institutes ON institutes.id = usersinst.institute_id
    LEFT OUTER JOIN gpascales ON gpascales.id = usersinst.gpa_scale
    /* gre */
    LEFT OUTER JOIN grescore ON grescore.application_id = application.id
    ";
    
    protected $queryGroupBy = "application.id";
    
    protected $queryOrderBy = 'users.lastname, users.firstname';
    
    public function find(
        $unit = NULL,
        $unitId = NULL,
        $periodId = NULL,
        $groupsLimitReviewerId = NULL, 
        $searchString = NULL, 
        $submitted = 1) 
    {
        $joins = array();
        $wheres = array();
        
        if($this->joinApplicationPrograms && $this->joinProgramsDepartments) 
        {
            $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id"; 
            $joins[] = "INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id";            
        }
        
        if ($unit && $unitId) 
        {
            switch ($unit) 
            {
                case 'program':
                
                    $wheres[] = "lu_programs_departments.program_id = " . $unitId;
                    break;    
                
                case 'domain':
                
                    if($this->joinDomainDepartment) 
                    {
                        $joins[] = "INNER JOIN lu_domain_department on lu_programs_departments.department_id = lu_domain_department.department_id";    
                    }
                    $wheres[] = "lu_domain_department.domain_id = " . $unitId;
                    break;
                
                case 'department':
                default:
                
                    $wheres[] = "lu_programs_departments.department_id = " . $unitId;

            }   
        }
        
        if ($periodId) 
        {
            $joins[] = "INNER JOIN period_application ON application.id = period_application.application_id";
            $wheres[] = "period_application.period_id = " . $periodId;
        }

        // Limit query to a particualr reviewer's reviewer groups.
        if ($groupsLimitReviewerId) 
        {
            /*
            $joins[] = "INNER JOIN lu_application_groups ON lu_application_groups.application_id = application.id";
            $joins[] = "INNER JOIN lu_reviewer_groups ON lu_application_groups.group_id = lu_reviewer_groups.group_id";
            
            $wheres[] = "lu_application_groups.round = " . $round;
            $wheres[] = "lu_reviewer_groups.round = " . $round;
            $wheres[] = "lu_reviewer_groups.reviewer_id = " . $groupsLimitReviewerId;
            */ 
        }
        
        if ($searchString) {
            $this->parseSearch($searchString);
        }
        
        if ($submitted) {
            $wheres[] = "application.submitted = 1";
        }

        $query = $this->querySelect;

        if ($this->searchSelect) {
            $query .= $this->searchSelect;
        }
        
        $query .= $this->queryFrom;
        if ( isset($joins) ) {
            $query .= ' ' . implode(' ', $joins);     
        }
        
        if ($this->searchJoin) {
            $query .= $this->searchJoin;
        }
        
        if ($this->queryWhere) {
            $query .= ' WHERE ' . $this->queryWhere;
            if ( isset($wheres) ) {
                $query .= ' AND ' . implode(' AND ', $wheres);     
            }        
        } elseif ( isset($wheres) ) {
            $query .= ' WHERE ' . implode(' AND ', $wheres);     
        }
        
        if ($this->queryGroupBy) {
            $query .= " GROUP BY " . $this->queryGroupBy;    
        }
        
        if ($this->queryHaving) {
            $query .= " HAVING " . $this->queryHaving;    
        }
        
        if ($this->queryOrderBy) {
            $query .= " ORDER BY " . $this->queryOrderBy;    
        }
        
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    }
}    
?>