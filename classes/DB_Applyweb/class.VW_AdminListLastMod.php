<?php
/* 
* A view of accesslog table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListLastMod extends VW_AdminList
{

    protected $querySelect = 
        "
        SELECT accesslog.application_id,
        accesslog.timestamp AS last_mod_date,
        accesslog.activity AS last_mod_activity,
        users.email AS last_mod_user
        ";
    
    protected $queryFrom = "FROM accesslog
                            INNER JOIN
                            (SELECT accesslog.application_id, 
                            MAX(id) AS id
                            FROM accesslog
                            WHERE usertype_id = 1
                            || accesslog.usertype_id = 5 
                            || (accesslog.usertype_id = 6 && accesslog.activity LIKE 'upload%')
                            GROUP BY application_id) AS last_mod
                                ON accesslog.id = last_mod.id
                            INNER JOIN application 
                                ON accesslog.application_id = application.id
                            LEFT OUTER JOIN users 
                                ON accesslog.users_id = users.id"; 
    
    protected $queryGroupBy = "accesslog.application_id";

}    
?>
