<?php
/*
Class for cc_transaction table data access.
*/

class DB_CcTransaction extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('cc_transaction');
    }
    
    
    public function get($ccId) {
    
        $primaryKeyValues = array('cc_id' => $ccId);
        return parent::get($primaryKeyValues);
    }

    
    public function find($paymentId) {

        $query = "SELECT cc_transaction.* 
                    FROM cc_transaction 
                    WHERE payment_id = ";
        $query .= "'" . $this->escapeString($paymentId) . "'";
        $query .= " ORDER BY cc_id";
        $resultArray = $this->handleSelectQuery($query);

        return $resultArray;           
    }

    
    public function save($recordArray) {

        $transactionRecords = $this->get($recordArray['cc_id']);       
        
        if ( count($transactionRecords) > 0 ) {
        
            return $this->update($recordArray); 
            
        } else {
            
            return $this->insert($recordArray); 
        }
    }

}

?>