<?php
/*
Class for cc_payment_status table data access.
*/

class DB_CcPaymentStatus2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'cc_payment_status';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($paymentId) {
    
        $primaryKeyValues = array('payment_id' => $paymentId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'payment_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'payment_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['payment_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>