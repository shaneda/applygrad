<?php
/*
Class for payment_item table data access.
*/

class DB_PaymentItem extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('payment_item');
    }
    
    
    public function get($paymentItemId) {
    
        $primaryKeyValues = array('payment_item_id' => $paymentItemId);
        return parent::get($primaryKeyValues);
    }

    
    public function find($paymentId) {
        
        $query = "SELECT payment_item.* 
                    FROM payment_item 
                    WHERE payment_id = ";
        $query .= "'" . $this->escapeString($paymentId) . "'";
        $resultArray = $this->handleSelectQuery($query, 'payment_item_id');
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['payment_item_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }

    public function delete($paymentItemId) {
        
        $primaryKeyValues = array('payment_item_id' => $paymentItemId);
        return parent::delete($primaryKeyValues);    
    }
    
}

?>