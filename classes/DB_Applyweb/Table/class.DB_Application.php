<?php
/*
Class for application table data access.
*/

class DB_Application extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('application');
    }
    
    
    public function get($applicationId) {
    
        $primaryKeyValues = array('id' => $applicationId);
        return parent::get($primaryKeyValues);
    }


    public function find($luUsersUsertypesId) {
        
        $query = "SELECT application.* 
                    FROM application
                    WHERE user_id = ";
        $query .= "'" . $this->escapeString($luUsersUsertypesId) . "'";
        $query .=  " ORDER BY application.id";
        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }


}

?>