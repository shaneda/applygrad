<?php
/*
Class for recommenderforms table data access.
*/

class DB_Recommendforms2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'recommendforms';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($recommendformsId) {
    
        $primaryKeyValues = array('id' => $recommendformsId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'recommend_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && $key == 'recommend_id') {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

         $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>