<?php
/*
Class for recommend table data access.
*/

class DB_Recommend extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('recommend');
    }
    
    
    public function get($recommendId) {
    
        $primaryKeyValues = array('id' => $recommendId);
        return parent::get($primaryKeyValues);
    }


    public function find($applicationId) {
        
        $query = "SELECT recommend.* 
                    FROM recommend 
                    WHERE application_id = ";
        $query .= "'" . $this->escapeString($applicationId) . "'";
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
    }

    
    public function delete($recommendId) {
        
        $primaryKeyValues = array('id' => $recommendId);
        return parent::delete($primaryKeyValues);    
    }

}

?>