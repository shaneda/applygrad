<?php
/*
Class for program_group_type table data access.
*/

class DB_ProgramGroupType extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('program_group_type');
    }
    
    
    public function get($programGroupTypeId) {
    
        $primaryKeyValues = array('program_group_type_id' => $programGroupTypeId);
        return parent::get($primaryKeyValues);
    }


    public function find() {
        
        $query = "SELECT program_group_type.* 
                    FROM program_group_type";

        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['program_group_type_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);        
        }
    }


}

?>
