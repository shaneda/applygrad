<?php
/*
Class for lu_users_usertypes table data access.
*/

class DB_LuUsersUsertypes2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'lu_users_usertypes';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($luUsersUsertypesId) {
    
        $primaryKeyValues = array('id' => $luUsersUsertypesId);
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'user_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;
        
        if ($value && ($key == 'user_id' || $key == 'usertype_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query);

        return $resultArray;    
    }
    
    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>