<?php
/*
Class for lu_application_programs table data access.
*/

class DB_LuApplicationPrograms extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('lu_application_programs');
    }
    
    
    public function get($luApplicationProgramsId) {
    
        $primaryKeyValues = array('id' => $luApplicationProgramsId);
        return parent::get($primaryKeyValues);
    }


    public function find($applicationId) {
        
        $query = "SELECT lu_application_programs.* 
                    FROM lu_application_programs
                    WHERE application_id = ";
        $query .= "'" . $this->escapeString($applicationId) . "'";
        $query .=  " ORDER BY application_id, program_id";
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }


}

?>