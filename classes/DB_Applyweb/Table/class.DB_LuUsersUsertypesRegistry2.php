<?php
/*
Class for lu_users_usertypes_registry table data access.
*/

class DB_LuUsersUsertypesRegistry2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'lu_users_usertypes_registry';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($remoteDb, $remoteHost, $remoteLuUsersUsertypesId) {
    
        $primaryKeyValues = array(
            'remote_db' => $remoteDb,
            'remote_host' => $remoteHost,
            'remote_lu_users_usertypes_id' => $remoteLuUsersUsertypesId
            );
    
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'lu_users_usertypes_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'lu_users_usertypes_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        $applicationRegistryRecords = $this->get(
            $record['remote_db'], 
            $record['remote_host'], 
            $recordArray['remote_lu_users_usertypes_id']);

        if ( count($applicationRegistryRecords) > 0 ) {
        
            return $this->update($record); 
            
        } else {
            
            return $this->insert($record); 
        }
    }

}
?>