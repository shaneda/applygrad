<?php
/*
Class for grescore_mscs_waiver table data access.
*/

class DB_GrescoreMscsWaiver2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'grescore_mscs_waiver';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }    

    public function get($applicationId) {
    
        $primaryKeyValues = array('application_id' => $applicationId);
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && $key == 'application_id') {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['application_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>