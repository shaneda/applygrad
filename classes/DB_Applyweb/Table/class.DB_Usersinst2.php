<?php
/*
Class for usersinst table data access.
*/

class DB_Usersinst2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'usersinst';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }    

    public function get($usersinstId) {
    
        $primaryKeyValues = array('id' => $usersinstId);
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'application_id' || $key == 'user_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>