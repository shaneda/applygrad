<?php
/*
Class for program_group_program table data access.
*/

class DB_ProgramGroupProgram extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('program_group_program');
    }
    
    
    public function get($programGroupId, $unitId) {
    
        $primaryKeyValues = array('program_group_id' => $programGroupId, 'unit_id' => $unitId);
        return parent::get($primaryKeyValues);
    }


    public function find($programGroupId = NULL, $unitId = NULL) {
        
        $query = "SELECT program_group_program.* 
                    FROM program_group_program";
        
        if ($programGroupId) {           
            $query .=  " WHERE program_group_id = '" . $this->escapeString($programGroupId) . "'";   
            if ($unitId) {
                $query .= " AND unit_id = '" . $this->escapeString($unitId) . "'";    
            }
        } elseif ($unitId) {
            $query .=  " WHERE unit_id = '" . $this->escapeString($unitId) . "'";   
        }
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['program_group_id']) && $record['program_group_id']  
            && isset($record['unit_id']) && $record['unit_id'] ) 
        {
        
            return $this->insert($record);        
        
        } else {
            
            return FALSE;
        }
    }

    public function delete($programGroupId, $unitId = NULL) {

        if ($unitId) {
            
            $primaryKeyValues = array(
                'program_group_id' => $programGroupId, 
                'unit_id' => $unitId
            );

            return parent::delete($primaryKeyValues);
        
        } else {
        
            $deleteQuery = "DELETE FROM program_group_program 
                            WHERE program_group_id = " . $programGroupId;
                
            return $this->handleUpdateQuery($deleteQuery);
        }            
    }
    
}
?>