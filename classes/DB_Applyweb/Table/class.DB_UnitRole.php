<?php
/*
Class for unit_role table data access.
*/

class DB_UnitRole extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('unit_role');
    }
    
    
    public function get($unitId, $usersId, $roleId) {
    
        $primaryKeyValues = array(
            'unit_id' => $unitId, 
            'users_id' => $usersId,
            'role_id' => $roleId
            );
        return parent::get($primaryKeyValues);
    }


    public function find($unitId = NULL, $usersId = NULL, $roleId = NULL) {
        
        $query = "SELECT unit_role.* 
                    FROM unit_role";
        
        if ($unitId) {           
        
            $query .=  " WHERE unit_id = '" . $this->escapeString($unitId) . "'";   
            if ($usersId) {
                $query .= " AND users_id = '" . $this->escapeString($usersId) . "'";    
            }
            if ($roleId) {
                $query .= " AND role_id = '" . $this->escapeString($roleId) . "'";    
            }
        
        } elseif ($usersId) {
            
            $query .=  " WHERE users_id = '" . $this->escapeString($usersId) . "'";
            if ($roleId) {
                $query .= " AND role_id = '" . $this->escapeString($roleId) . "'";    
            }   
        
        } elseif ($roleId) {
        
            $query .= " WHERE role_id = '" . $this->escapeString($roleId) . "'"; 
        }
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function delete($unitId, $usersId, $roleId) {

        if ( $unitId && $usersId && $roleId ) {
        
            $primaryKeyValues = array(
                'unit_id' => $unitId,
                'users_id' => $usersId,
                'role_id' => $roleId
                );
            return parent::delete($primaryKeyValues);        
        
        } else {
            
            return FALSE;
        }
        
    }

    public function save($record = array()) {

        if ( isset($record['unit_id']) && $record['unit_id']  
            && isset($record['users_id']) && $record['users_id']
            && isset($record['role_id']) && $record['role_id'] ) 
        {
        
            return $this->insert($record);        
        
        } else {
            
            return FALSE;
        }
    }


}

?>