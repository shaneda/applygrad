<?php
/*
Class for scs_user table data access.
*/

class DB_ScsUser2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'scs_user';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($scsUserId) {
    
        $primaryKeyValues = array('scs_user_id' => $scsUserId);
        
        return parent::get($primaryKeyValues);
    }
    
    public function find($value = NULL, $key = 'users_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'users_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['scs_user_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>