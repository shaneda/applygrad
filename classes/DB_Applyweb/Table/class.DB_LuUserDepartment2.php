<?php
/*
Class for lu_user_department table data access.
*/

class DB_LuUserDepartment2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'lu_user_department';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }

    public function get($luUserDepartmentId) {
    
        $primaryKeyValues = array('id' => $luUserDepartmentId);
        return parent::get($primaryKeyValues);
    }

    /*
    * Find by lu_users_usertypes.id (default) or department.id
    */
    public function find($value = NULL, $key = 'user_id') {
        
        $query = "SELECT lu_user_department.* 
                    FROM lu_user_department";
                    
        if ( $value && ($key == 'user_id' || $key == 'department_id') ) {
            $query .= " WHERE " . $key . " = " . intval($value);    
        }

        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query);
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>