<?php
/*
Class for period_application table data access.
*/

class DB_PeriodApplication extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('period_application');
    }
    
    
    public function get($periodId, $applicationId = NULL) {
    
        $primaryKeyValues = array(
            'period_id' => $periodId, 
            'application_id' => $applicationId
        );
        return parent::get($primaryKeyValues);
    }


    public function find($periodId = NULL, $applicationId = NULL) {
        
        $query = "SELECT period_application.* 
                    FROM period_application";

        if ($periodId) {           
            $query .=  " WHERE period_id = '" . $this->escapeString($periodId) . "'";   
            if ($applicationId) {
                $query .= " AND application_id = '" . $this->escapeString($applicationId) . "'";    
            }
        } elseif ($applicationId) {
            $query .=  " WHERE application_id = '" . $this->escapeString($applicationId) . "'";   
        }


        $query .= "'" . $this->escapeString($luUsersUsertypesId) . "'";
        $query .=  " ORDER BY application.id";
        
        
        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['period_id']) && $record['period_id']  
            && isset($record['application_id']) && $record['application_id'] ) 
        {
        
            return $this->insert($record);        
        
        } else {
            
            return FALSE;
        }
    }

}
?>