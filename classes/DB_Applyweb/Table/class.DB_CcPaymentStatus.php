<?php
/*
Class for cc_payment_status table data access.
*/

class DB_CcPaymentStatus extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('cc_payment_status');
    }
    
    
    public function get($paymentId) {
    
        $primaryKeyValues = array('payment_id' => $paymentId);
        return parent::get($primaryKeyValues);
    }

    
    public function find($paymentId) {

        /*
        $query = "SELECT cc_payment_status.* 
                    FROM cc_payment_status 
                    WHERE payment_id = ";
        $query .= "'" . self::escapeString($paymentId) . "'";
        $query .= " ORDER BY cc_id";
        $resultArray = $this->handleSelectQuery($query);
        return $resultArray; 
        */
        
        return $this->get($paymentId);              
    }

    
    public function save($recordArray) {

        $paymentStatusRecords = $this->get($recordArray['payment_id']);
        
        if ( count($paymentStatusRecords) > 0 ) {
        
            return $this->update($recordArray); 
            
        } else {
            
            return $this->insert($recordArray); 
        } 
    }

}

?>