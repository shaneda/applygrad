<?php
/*
Class for cc_transaction_report table data access.
*/

class DB_CcTransactionReport extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('cc_transaction_report');
    }
    
    
    public function get($filename) {
    
        $primaryKeyValues = array('filename' => $filename);
        return parent::get($primaryKeyValues);
    }

    
    public function find($filename) {

        return $this->get($filename);           
    }

    
    public function save($recordArray) {

        $transactionRecords = $this->get($recordArray['filename']);       
        
        if ( count($transactionRecords) > 0 ) {
        
            return $this->update($recordArray); 
            
        } else {
            
            return $this->insert($recordArray); 
        }
    }

}

?>