<?php
/*
Class for period_application table data access.
*/

class DB_PeriodApplication2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'period_application';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($periodId, $applicationId) {
        
        $primaryKeyValues = array('period_id' => $periodId, 'application_id' => $applicationId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'application_id' || $key == 'period_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['period_id']) && isset($record['application_id']) ) {
            
            return $this->insert($record);    
        
        }
    }

}

?>