<?php
/*
Class for cc_transaction_detail table data access.
*/

class DB_CcTransactionDetail extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('cc_transaction_detail');
    }
    
    
    public function get($ccId, $date, $time, $transactionType, $itemName) {
    
        $primaryKeyValues = array(
            'cc_id' => $ccId,
            'date' => $date,
            'time' => $time,
            'transaction_type' => $transactionType,
            'item_name' => $itemName
        );
        return parent::get($primaryKeyValues);
    }    

    
    public function find($ccId) {

        $query = "SELECT cc_transaction_detail.* 
                    FROM cc_transaction_detail 
                    WHERE cc_id = ";
        $query .= "'" . self::escapeString($ccId) . "'";
        $query .= " ORDER BY item_name, report_date, transaction_type";
        $resultArray = $this->handleSelectQuery($query);

        return $resultArray;           
    }

    
    public function save($recordArray) {

        $transactionDetailRecords = $this->get($recordArray['cc_id'], $recordArray['date'], 
            $recordArray['time'], $recordArray['transaction_type'], $recordArray['item_name']);

        if ( count($transactionDetailRecords) > 0 ) {
        
            return $this->update($recordArray); 
            
        } else {
            
            return $this->insert($recordArray); 
        }
    }

}

?>