<?php
/*
Class for payment_item table data access.
*/

class DB_PaymentItem2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'payment_item';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($paymentItemId) {
    
        $primaryKeyValues = array('payment_item_id' => $paymentItemId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'payment_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'payment_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['payment_item_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>