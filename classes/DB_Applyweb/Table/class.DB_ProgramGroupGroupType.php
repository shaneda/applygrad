<?php
/*
Class for program_group_group_type table data access.
*/

class DB_ProgramGroupGroupType extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('program_group_group_type');
    }
    
    
    public function get($programGroupId, $programGroupTypeId) {
    
        $primaryKeyValues = array(
            'program_group_id' => $programGroupId, 
            'program_group_type_id' => $programGroupTypeId
            );
        return parent::get($primaryKeyValues);
    }


    public function find($programGroupId = NULL, $programGroupTypeId = NULL) {
        
        $query = "SELECT program_group_group_type.* 
                    FROM program_group_group_type";

        if ($programGroupId) {           
            $query .=  " WHERE program_group_id = '" . $this->escapeString($programGroupId) . "'";   
            if ($programGroupTypeId) {
                $query .= " AND program_group_type_id = '" . $this->escapeString($programGroupTypeId) . "'";    
            }
        } elseif ($programGroupTypeId) {
            $query .=  " WHERE program_group_type_id = '" . $this->escapeString($programGroupTypeId) . "'";   
        }

        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['program_group_id']) && $record['program_group_id']
            && isset($record['program_group_type_id']) && $record['program_group_type_id'] ) 
        {
            
            return $this->insert($record);
                
        } else {
            
            return FALSE;
        }
    }


    public function delete($programGroupId, $programGroupTypeId = NULL) {
        
        if ($programGroupTypeId) {
            
            $primaryKeyValues = array(
                'program_group_id' => $programGroupId, 
                'program_group_type_id' => $programGroupTypeId
            );
            return parent::delete($primaryKeyValues);
        
        } else {
        
            $deleteQuery = "DELETE FROM program_group_group_type 
                            WHERE program_group_id = " . $programGroupId;
                
            return $this->handleUpdateQuery($deleteQuery);
        }
    
    }
    
}

?>