<?php
/*
Class for cc_transaction_summary table data access.
*/

class DB_CcTransactionSummary extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('cc_transaction_summary');
    }
    
    
    public function get($ccId, $date, $time) {
    
        $primaryKeyValues = array(
            'cc_id' => $ccId,
            'date' => $date,
            'time' => $time
        );
        return parent::get($primaryKeyValues);
    }
    
    
    public function find($ccId = NULL, $paymentId = NULL) {

        $query = "SELECT cc_transaction_summary.* 
                    FROM cc_transaction_summary";
        if ($ccId) {
            $query .= " WHERE cc_id = '" . self::escapeString($ccId) . "'";
        }
        if ($paymentId) {
            if (!$ccId) {
                $query .= " WHERE payment_id = '" . self::escapeString($paymentId) . "'";    
            } else {
                $query .= " AND payment_id = '" . self::escapeString($paymentId) . "'";    
            }
        }  
        $query .= " ORDER BY report_date";
        $resultArray = $this->handleSelectQuery($query);

        return $resultArray;           
    }

    
    public function save($recordArray) {

        $transactionSummaryRecords = 
            $this->get($recordArray['cc_id'], $recordArray['date'], $recordArray['time']);
        
        if ( count($transactionSummaryRecords) > 0 ) {
        
            return $this->update($recordArray); 
            
        } else {
            
            return $this->insert($recordArray); 
        }
    }

}

?>