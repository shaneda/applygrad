<?php
/*
Class for users table data access.
*/

class DB_Users2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'users';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($usersId) {
    
        $primaryKeyValues = array('id' => $usersId);
    
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'email') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'email' || $key == 'guid')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>