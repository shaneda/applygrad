<?php
/*
Class for program_group_role table data access.
*/

class DB_ProgramGroupRole extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('program_group_role');
    }
    
    
    public function get($programGroupId, $usersId, $roleId) {
    
        $primaryKeyValues = array(
            'program_group_id' => $programGroupId, 
            'users_id' => $usersId,
            'role_id' => $roleId
            );
        return parent::get($primaryKeyValues);
    }


    public function find($programGroupId = NULL, $usersId = NULL, $roleId = NULL) {
        
        $query = "SELECT program_group_role.* 
                    FROM program_group_role";
        
        if ($programGroupId) {           
        
            $query .=  " WHERE program_group_id = '" . $this->escapeString($programGroupId) . "'";   
            if ($usersId) {
                $query .= " AND users_id = '" . $this->escapeString($usersId) . "'";    
            }
            if ($roleId) {
                $query .= " AND role_id = '" . $this->escapeString($roleId) . "'";    
            }
        
        } elseif ($usersId) {
            
            $query .=  " WHERE users_id = '" . $this->escapeString($usersId) . "'";
            if ($roleId) {
                $query .= " AND role_id = '" . $this->escapeString($roleId) . "'";    
            }   
        
        } elseif ($roleId) {
        
            $query .= " WHERE role_id = '" . $this->escapeString($roleId) . "'"; 
        }
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function delete($programGroupId, $usersId = NULL, $roleId = NULL) {

        if ( $programGroupId && $usersId && $roleId ) {
        
            $primaryKeyValues = array(
                'program_group_id' => $programGroupId,
                'users_id' => $usersId,
                'role_id' => $roleId
                );
            return parent::delete($primaryKeyValues);        
        
        } elseif ($programGroupId && !$usersId && !$roleId) { 
    
            $deleteQuery = "DELETE FROM program_group_role 
                            WHERE program_group_id = " . $programGroupId;
                
            return $this->handleUpdateQuery($deleteQuery);            
            
        } else {
            
            return FALSE;
        }
        
    }

    public function save($record = array()) {

        if ( isset($record['program_group_id']) && $record['program_group_id']  
            && isset($record['users_id']) && $record['users_id']
            && isset($record['role_id']) && $record['role_id'] ) 
        {
        
            return $this->insert($record);        
        
        } else {
            
            return FALSE;
        }
    }


}

?>