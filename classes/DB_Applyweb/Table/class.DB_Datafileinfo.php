<?php
/*
Class for datafileinfo table data access.
*/

class DB_Datafileinfo extends DB_Applyweb_Table
{

    public function __construct() {   
        parent::__construct('datafileinfo');
    }

    function get($datafileinfoId) {

        $primaryKeyValues = array('id' => $datafileinfoId);
        return parent::get($primaryKeyValues); 
    }

    public function find($luUsersUsertypesId, $applicationId = NULL) {
        
        $query = "SELECT datafileinfo.* FROM datafileinfo WHERE user_id = ";  
        $query .= "'" . $this->escapeString($luUsersUsertypesId) . "'";
        if ($applicationId) {
            $query .= " AND userdata LIKE '" . $applicationId . "_%'";
        }
        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }

    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
    }

}

?>