<?php
/*
Class for mse_risk_factors_decision table data access.
*/

class DB_MseRiskFactorsDecision2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'mse_risk_factors_decision';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($applicationId, $programId) {
    
        $primaryKeyValues = array('application_id' => $applicationId, 'program_id' => $programId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'application_id' || $key == 'program_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['application_id']) && isset($record['program_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>