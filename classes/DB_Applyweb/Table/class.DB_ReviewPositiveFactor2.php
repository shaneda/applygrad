<?php
/*
Class for review_positive_factor table data access.
*/

class DB_ReviewPositiveFactor2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'review_positive_factor';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($reviewId, $positiveFactorId) {
    
        $primaryKeyValues = array('review_id' => $reviewId, 'positive_factor_id' => $positiveFactorId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'review_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'review_id' || $key == 'positive_factor_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['review_id']) && isset($record['positive_factor_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>