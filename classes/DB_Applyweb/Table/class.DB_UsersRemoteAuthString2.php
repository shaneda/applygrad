<?php
/*
Class for users_remote_auth_string table data access.
*/

class DB_UsersRemoteAuthString2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'users_remote_auth_string';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    // Get is not particularly useful for this table.
    // Output will essentially be the same as input.
    public function get($usersId, $remoteAuthString) {
    
        $primaryKeyValues = array(
            'users_id' => $usersId, 
            'remote_auth_string' => $remoteAuthString
        );
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'users_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'users_id' || $key == 'remote_auth_string')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        // Will return error if record already exists.
        return $this->insert($record);
    }

}
?>