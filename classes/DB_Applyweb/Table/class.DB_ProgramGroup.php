<?php
/*
Class for program_group table data access.
*/

class DB_ProgramGroup extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('program_group');
    }
    
    
    public function get($programGroupId) {
    
        $primaryKeyValues = array('program_group_id' => $programGroupId);
        return parent::get($primaryKeyValues);
    }


    public function find($periodId) {
        
        $query = "SELECT program_group.* 
                    FROM program_group 
                    WHERE period_id = ";
        $query .= "'" . $this->escapeString($periodId) . "'";
        $query .=  " ORDER BY program_group_id";
        $resultArray = $this->handleSelectQuery($query, 'program_group_id');
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['program_group_id']) && $record['program_group_id'] ) {
            
            $this->update($record);
            return $record['program_group_id'];    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }

    public function delete($programGroupId) {
        
        $primaryKeyValues = array('program_group_id' => $programGroupId);
        return parent::delete($primaryKeyValues);
    }

}

?>