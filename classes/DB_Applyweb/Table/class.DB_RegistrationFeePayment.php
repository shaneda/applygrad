<?php
/*
Class for registration_fee_payment table data access.
*/

class DB_RegistrationFeePayment extends DB_Applyweb_Table
{
    public function __construct() 
    {   
        parent::__construct('registration_fee_payment');
    }
    
    public function get($id) 
    {
        $primaryKeyValues = array('id' => $id);
        return parent::get($primaryKeyValues);
    }

    public function find($applicationId, $departmentId) 
    {    
        $query = "SELECT registration_fee_payment.* 
                    FROM registration_fee_payment 
                    WHERE application_id = ";
        $query .= "'" . $this->escapeString($applicationId) . "'";
        $query .= " AND department_id = '" . $this->escapeString($departmentId) . "'";
        $query .=  " ORDER BY payment_intent_date";
        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }
    
    public function save($record = array()) 
    {
        if ( isset($record['id']) ) 
        {    
            return $this->update($record);    
        } 
        else 
        {
            return $this->insert($record);
        }
    }

    public function delete($id) 
    {    
        $primaryKeyValues = array('id' => $id);
        return parent::delete($primaryKeyValues);    
    }
}
?>