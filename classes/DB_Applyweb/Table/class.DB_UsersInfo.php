<?php
/*
Class for users_info table data access.
*/

class DB_UsersInfo extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('users_info');
    }
    
    
    public function get($usersInfoId) {
    
        $primaryKeyValues = array('id' => $usersInfoId);
        return parent::get($primaryKeyValues);
    }

    public function find($luUsersUsertypesId) {
        
        $query = "SELECT users_info.* 
                    FROM users_info
                    WHERE user_id = ";
        $query .= "'" . $this->escapeString($luUsersUsertypesId) . "'";
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }


}

?>