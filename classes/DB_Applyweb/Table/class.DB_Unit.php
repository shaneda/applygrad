<?php
/*
Class for unit table data access.
*/

class DB_Unit extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('unit');
    }
    
    
    public function get($unitId) {
    
        $primaryKeyValues = array(
            'unit_id' => $unitId
            );
        return parent::get($primaryKeyValues);
    }


    public function find($unitId = NULL) {
        
        $query = "SELECT unit.* 
                    FROM unit";
        if ($unitId) {           
            $query .=  " WHERE parent_unit_id = '" . $this->escapeString($unitId) . "'";   
        }
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    

    public function delete($unitId) {

        if ($unitId) {
        
            $primaryKeyValues = array(
                'unit_id' => $unitId
                );
            return parent::delete($primaryKeyValues);        
        
        } else {
            
            return FALSE;
        }
        
    }

    public function save($record = array()) {

        if ( isset($record['unit_id']) ) {
        
            $affectedRowCount = $this->update($record);
            $unitId = $record['unit_id']; 
            
        } else {
            
            $unitId = $this->insert($record);
        }
        
        return $unitId;
    }


}

?>