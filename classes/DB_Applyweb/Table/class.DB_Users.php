<?php
/*
Class for users table data access.
*/

class DB_Users extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('users');
    }
    
    
    public function get($usersId) {
    
        $primaryKeyValues = array('id' => $usersId);
        return parent::get($primaryKeyValues);
    }


    public function find($usersId) {
        
        if ($usersId) {
            
            return $this->get($usersId);
            
        } else {
            
            $query = "SELECT users.* FROM users";     
            $resultArray = $this->handleSelectQuery($query, 'id'); 
            return $resultArray;
            
        }    
    }
    

    public function save($record = array()) {

        if ( isset($record['id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }


}

?>