<?php
/*
Class for users_registry table data access.
*/

class DB_UsersRegistry2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'users_registry';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($remoteDb, $remoteHost, $remoteUsersId) {
    
        $primaryKeyValues = array(
            'remote_db' => $remoteDb,
            'remote_host' => $remoteHost,
            'remote_users_id' => $remoteUsersId
            );
    
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'users_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'users_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        $applicationRegistryRecords = $this->get(
            $record['remote_db'], 
            $record['remote_host'], 
            $recordArray['remote_users_id']);

        if ( count($applicationRegistryRecords) > 0 ) {
        
            return $this->update($record); 
            
        } else {
            
            return $this->insert($record); 
        }
    }

}
?>