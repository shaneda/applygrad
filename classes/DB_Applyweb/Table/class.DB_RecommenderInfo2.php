<?php
/*
Class for recommender_info table data access.
*/

class DB_RecommenderInfo2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'recommender_info';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($recommendId) {
    
        $primaryKeyValues = array('id' => $recommendId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'rec_user_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && $key == 'rec_user_id') {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

         $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['recommend_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>