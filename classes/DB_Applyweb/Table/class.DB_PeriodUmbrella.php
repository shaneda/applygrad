<?php
/*
Class for period_umbrella table data access.
*/

class DB_PeriodUmbrella extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('period_umbrella');
    }
    
    
    public function get($periodId) {
    
        $primaryKeyValues = array(
            'period_id' => $periodId
        );
        return parent::get($primaryKeyValues);
    }


    public function find($unitId = NULL) {
        
        $query = "SELECT period_umbrella.* 
                    FROM period_application";
        if ($unitId) {           
            $query .=  " WHERE unit_id = '" . $this->escapeString($unitId) . "'";   
        }
        $query .= "'" . $this->escapeString($luUsersUsertypesId) . "'";
        $query .=  " ORDER BY period_id";

        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['period_id']) ) {

            $umbrellaRecord = $this->get($record['period_id']);
            
            if ($record['higher_fee_date'] == '') {
                $record['higher_fee_date'] = NULL;
            }
            
            if ($record['last_payment_date'] == '') {
                $record['last_payment_date'] = NULL;
            }
            
            if (count($umbrellaRecord) > 0) {
                $affectedRowCount = $this->update($record);    
            } else {
                $affectedRowCount = $this->insert($record);    
            } 
            
            return $record['period_id'];    
        
        } else {
        
            return FALSE;
        
        }
    }

    
    public function delete($periodId) {

        $primaryKeyValues = array(
            'period_id' => $periodId
        );
        return parent::delete($primaryKeyValues);       
    } 

}
?>