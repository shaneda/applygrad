<?php
/*
Class for promotion_history table data access.
*/

class DB_PromotionHistory2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'promotion_history';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($applicationId, $programId, $statusTime) {
        
        $primaryKeyValues = array(
            'application_id' => $applicationId, 
            'program_id' => $programId,
            'status_time' => $statusTime
        );
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'application_id' || $key == 'program_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['application_id']) && isset($record['program_id'])
            && isset($record['status_time']) ) 
        {    
            return $this->insert($record);    
        }
    }

}

?>