<?php
/*
Class for payment table data access.
*/

class DB_Payment extends DB_Applyweb_Table
{
    
    public function __construct() {   
        parent::__construct('payment');
    }
    
    
    public function get($paymentId) {
    
        $primaryKeyValues = array('payment_id' => $paymentId);
        return parent::get($primaryKeyValues);
    }


    public function find($applicationId) {
        
        $query = "SELECT payment.* 
                    FROM payment 
                    WHERE application_id = ";
        $query .= "'" . $this->escapeString($applicationId) . "'";
        $query .=  " ORDER BY payment_intent_date";
        $resultArray = $this->handleSelectQuery($query, 'payment_id');
        
        return $resultArray;        
    }
    

    public function save($record = array()) {

        if ( isset($record['payment_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        
        }
 
    }

    public function delete($paymentId) {
        
        $primaryKeyValues = array('payment_id' => $paymentId);
        return parent::delete($primaryKeyValues);    
    }

}

?>