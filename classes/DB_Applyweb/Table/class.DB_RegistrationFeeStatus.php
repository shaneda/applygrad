<?php
/*
Class for registration_fee table data access.
*/

class DB_RegistrationFeeStatus extends DB_Applyweb_Table
{
    public function __construct() 
    {   
        parent::__construct('registration_fee_status');
    }
    
    public function get($id) 
    {
        $primaryKeyValues = array('id' => $id);
        return parent::get($primaryKeyValues);
    }

    public function find($applicationId, $departmentId) 
    {    
        $query = "SELECT registration_fee_status.* 
                    FROM registration_fee_status 
                    WHERE application_id = ";
        $query .= "'" . $this->escapeString($applicationId) . "'";
        $query .= " AND department_id = '" . $this->escapeString($departmentId) . "'";
        $resultArray = $this->handleSelectQuery($query, 'id');
        
        return $resultArray;        
    }
    
    public function save($record = array()) 
    {
        if ( isset($record['id']) ) 
        {    
            return $this->update($record);    
        } 
        else 
        {
            return $this->insert($record);
        }
    }

    public function delete($id) 
    {    
        $primaryKeyValues = array('id' => $id);
        return parent::delete($primaryKeyValues);    
    }
}
?>