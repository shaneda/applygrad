<?php
/*
Class for scs_user_webiso table data access.
*/

class DB_ScsUserWebiso2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'scs_user_webiso';
    
    public function __construct($DB_Applyweb2) {   
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    // Get is not particularly useful for this table.
    // Output will essentially be the same as input.
    public function get($scsUserId, $webiso) {
    
        $primaryKeyValues = array(
            'scs_user_id' => $scsUserId, 
            'webiso' => $webiso
        );
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'scs_user_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'scs_user_id' || $key == 'webiso')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        // Will return error if record already exists.
        return $this->insert($record);
    }

}
?>