<?php
/*
Class for mse_interview table data access.
*/

class DB_MseInterview2 extends DB_Applyweb_Table2
{   
    const TABLE_NAME = 'mse_interview';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($applicationId, $reviewerId) {
    
        $primaryKeyValues = array('application_id' => $applicationId, 'reviewer_id' => $reviewerId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'application_id' || $key == 'reviewer_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }

        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;        
    }
    
    public function save($record = array()) {

        if ( isset($record['application_id']) && isset($record['reviewer_id']) ) {
            
            return $this->update($record);    
        
        } else {
        
            return $this->insert($record);
        }
    }

}
?>