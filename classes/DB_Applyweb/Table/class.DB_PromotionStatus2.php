<?php
/*
Class for promotion_status table data access.
*/

class DB_PromotionStatus2 extends DB_Applyweb_Table2
{
    const TABLE_NAME = 'promotion_status';
    
    public function __construct($DB_Applyweb2) {
       
        parent::__construct($DB_Applyweb2, self::TABLE_NAME);
    }
    
    public function get($applicationId, $departmentId) {
        
        $primaryKeyValues = array('application_id' => $applicationId, 'department_id' => $departmentId);
        
        return parent::get($primaryKeyValues);
    }

    public function find($value = NULL, $key = 'application_id') {
        
        $query = "SELECT * FROM "  . self::TABLE_NAME;     
        
        if ($value && ($key == 'application_id' || $key == 'department_id')) {
            
            $query .= " WHERE {$key} = '{$this->DB_Applyweb2->escapeString($value)}'";
        }
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query); 
        
        return $resultArray;   
    }
    
    public function save($record = array()) {

        if ( isset($record['application_id']) && isset($record['department_id']) ) {
            
            return $this->insert($record);
        }
    }

}

?>