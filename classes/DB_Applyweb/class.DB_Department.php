<?php
/*
* Class for department table data access.
* 
*  id                   int(11) NOT NULL auto_increment
*  name                 varchar(50) NOT NULL default ''
*  parent_school_id     int(11) NOT NULL default '0'
*  oraclestring         varchar(50) default NULL
*  rank                 int(11) NOT NULL default '0'
*  cc_email             varchar(255) default NULL
*  enable_round1        char(1) default '1'
*  enable_round2        char(1) default '0'
*  enable_final         char(1) default '0'
*  semiblind_review     char(1) default '0'
*  allowRequestAdvisors tinyint(3) unsigned default '0'
*/

class DB_Department extends DB_Applyweb
{

    function get($departmentId = NULL) {
        
        $query = "SELECT department.* FROM department";
        if ($departmentId) {
            $query .= " WHERE id = " . $departmentId ;   
        }
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }

}

?>