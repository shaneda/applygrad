<?php
/*
* Class for unit_role table data access.
* 
* Required classes:
*   DB_Applyweb 
*/

class DB_UnitRole extends DB_Applyweb
{

    function get($userRoleId = NULL, $unitId = NULL) {

        $query = "SELECT unit_role.* FROM unit_role"; 
        if ($userRoleId) {
            $query .=  " WHERE lu_users_usertypes_id = " . $userRoleId;   
        }
        if ($userRoleId && $unitId) {
            $query .= " AND";    
        } 
        if ($unitId) {
            $query .=  " WHERE unit_id = " . $unitId;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    public function find($userRoleId = NULL, $unitId = NULL) {
        return $this->get($userRoleId, $unitId);        
    }

}
?>