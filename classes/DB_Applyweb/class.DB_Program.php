<?php
/*
* Class for program table data access.
* 
* Fields:
*   int unit_id
*   int program_type_id 
*/

class DB_Program extends DB_Applyweb
{

    public function get($unitId = NULL, $withLookup = FALSE) {

        if ($withLookup) {
            $query = "SELECT unit.*, program_type.* FROM unit
                        INNER JOIN program 
                            ON unit.unit_id = program.unit_id
                        INNER JOIN program_type 
                            ON program.program_type_id = program_type.program_type_id";    
        } else {
            $query = "SELECT program.* FROM program";    
        } 
        if ($unitId) {
            $query .=  " WHERE program.unit_id = " . $unitId ;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    public function find($programTypeId = NULL, $withLookup = FALSE) {

        if ($withLookup) {
            $query = "SELECT unit.*, program_type.* FROM unit
                        INNER JOIN program 
                            ON unit.unit_id = program.unit_id
                        INNER JOIN program_type 
                            ON program.program_type_id = program_type.program_type_id";    
        } else {
            $query = "SELECT program.* FROM program";    
        } 
        if ($programTypeId) {
            $query .=  " WHERE program.program_type_id = " . $programTypeId;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;         
    }
    
    public function save($valueArray) {
        
        $unitId = $valueArray['unit_id'];
        $programTypeId = $valueArray['program_type_id'];
        
        $query = "INSERT INTO program VALUES 
                    (" . $unitId . ", " . $programTypeId . ")
                    ON DUPLICATE KEY UPDATE 
                    unit_id = " . $unitId . ", program_type_id = " . $programTypeId;
                    
        $numAffectedRows = $this->handleInsertQuery($query);
        
        return $numAffectedRows;        
    }
    
    public function delete($unitId) {
        
        if (!$unitId) {
            return FALSE;
        }

        $query = "DELETE FROM program WHERE
                    unit_id = " . $unitId;
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;
    }
    
}
?>