<?php
/*
Class for department_unit table data access.
*/

class DB_DepartmentUnit extends DB_Applyweb
{

    public function get($departmentId = NULL, $unitId = NULL) {

        $query = "SELECT department_unit.* FROM department_unit"; 
        if ($departmentId) {
            $query .=  " WHERE domain_id = " . $departmentId;   
        }
        if ($departmentId && $unitId) {
            $query .= " AND";    
        }
        if ($unitId) {
            $query .=  " WHERE unit_id = " . $unitId;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    public function find($departmentId = NULL, $unitId = NULL) {
        return $this->get($departmentId, $unitId);        
    }
    
    public function save($recordArray) {
        
        $departmentId = $recordArray['department_id'];
        $unitId = $recordArray['unit_id'];
        
        $query = "INSERT INTO department_unit VALUES 
                    (" . $departmentId . ", " . $unitId . ")
                    ON DUPLICATE KEY UPDATE 
                    department_id = " . $departmentId . ", unit_id = " . $unitId;
                    
        $numAffectedRows = $this->handleInsertQuery($query);
        
        return $numAffectedRows;
    }

    
    public function delete($departmentId = NULL, $unitId = NULL) {
        
        if (!$departmentId && !$unitId) {
            return FALSE;
        }
        
        $query = "DELETE FROM department_unit WHERE";
        if ($departmentId) {
            $query .= " department_id = " . $departmentId;   
        }
        if ($departmentId && $unitId) {
            $query .= " AND";
        }
        if ($unitId) {
            $query .= " unit_id = " . $unitId;   
        }
        
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;
    }
    

}

?>