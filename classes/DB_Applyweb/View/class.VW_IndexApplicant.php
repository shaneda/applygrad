<?php

/*  
* View of application/user for finding applicants by name or email.
* Find method returns a 2D array indexed by user_id.
*/

class VW_IndexApplicant extends DB_Applyweb
{

    private $queryBase = 
        "
        SELECT DISTINCT
          users.id AS users_id,
          users.lastname,
          users.firstname,
          users.middlename,
          users.email
        FROM
          users
          INNER JOIN lu_users_usertypes ON users.id = lu_users_usertypes.user_id
        ";

    
    public function find($searchString = '', $periodId = NULL) {        
        
        $query = $this->queryBase;
        
        if ($periodId) {
            $query .= " INNER JOIN application 
                            ON lu_users_usertypes.id = application.user_id 
                        INNER JOIN period_application 
                            ON application.id = period_application.application_id";           
        }

        $query .= " WHERE lu_users_usertypes.usertype_id = 5";
        
        if ($periodId) {
            $query .= " AND period_application.period_id = " . $periodId;
        }
        
        if ($searchString) {
            
            $replaceChars = array(",", "(", ")");
            $queryString = trim( str_replace($replaceChars, "", $searchString) );
            
            $queryStringArray = explode(' ', $queryString);
            if ( count($queryStringArray) == 2) {
                
                $query .= sprintf(" AND ( 
                            (users.lastname LIKE '%s' AND users.firstname LIKE '%s') 
                            OR (users.lastname LIKE '%s' AND users.firstname LIKE '%s')
                             )",
                            self::$mysqli->real_escape_string($queryStringArray[0]) . "%",
                            self::$mysqli->real_escape_string($queryStringArray[1]) . "%",
                            self::$mysqli->real_escape_string($queryStringArray[1]) . "%",
                            self::$mysqli->real_escape_string($queryStringArray[0]) . "%"
                            );                          
            } else {
            
                $query .= sprintf(" AND (users.lastname LIKE '%s' OR users.firstname LIKE '%s' OR users.email LIKE '%s'
                            OR ( MATCH(users.firstname, users.lastname) AGAINST('%s') ) )",
                            self::$mysqli->real_escape_string($queryString) . "%",
                            self::$mysqli->real_escape_string($queryString) . "%",
                            self::$mysqli->real_escape_string($queryString) . "%",
                            self::$mysqli->real_escape_string($queryString)
                            );
            }
        }

        $query .= " ORDER BY users.lastname, users.firstname";
        
        $arrayKey = "users_id";
        $resultsArray = $this->handleSelectQuery($query, $arrayKey);
        
        return $resultsArray;  
    }
    
}    
?>