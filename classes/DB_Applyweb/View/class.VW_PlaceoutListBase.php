<?php

class VW_PlaceoutListBase extends VW_AdminList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    lu_users_usertypes.id AS  lu_users_usertypes_id,
    users.guid,
    users.lastname, users.firstname,
    users.email,
    lu_application_programs.program_id,
    CONCAT(
        degree.name,
        ' ',
        programs.linkword,
        ' ',
        fieldsofstudy.name
    ) AS program,
    MIN(domain_id) AS application_domain
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_users_usertypes
      ON application.user_id = lu_users_usertypes.id
    INNER JOIN users
      ON lu_users_usertypes.user_id = users.id
    INNER JOIN lu_application_programs AS lu_application_programs 
        ON application.id = lu_application_programs.application_id 
    INNER JOIN lu_programs_departments 
        ON lu_application_programs.program_id = lu_programs_departments.program_id 
    INNER JOIN lu_domain_department 
        ON lu_programs_departments.department_id = lu_domain_department.department_id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id 
    LEFT OUTER JOIN student_decision
      ON application.id = student_decision.application_id
    
    LEFT OUTER JOIN mhci_prereqs AS design_prereq
        ON design_prereq.prereq_type = 'design'
        AND application.id = design_prereq.application_id
    LEFT OUTER JOIN mhci_prereqs_status AS design_prereq_status
        ON design_prereq.id = design_prereq_status.mhci_prereqs_id
        
    LEFT OUTER JOIN mhci_prereqs AS programming_prereq
        ON programming_prereq.prereq_type = 'programming'
        AND application.id = programming_prereq.application_id
    LEFT OUTER JOIN mhci_prereqs_status AS programming_prereq_status
        ON programming_prereq.id = programming_prereq_status.mhci_prereqs_id
    /*    
    LEFT OUTER JOIN mhci_prereqs AS statistics_prereq
        ON statistics_prereq.prereq_type = 'statistics'
        AND application.id = statistics_prereq.application_id
    LEFT OUTER JOIN mhci_prereqs_status AS statistics_prereq_status
        ON statistics_prereq.id = statistics_prereq_status.mhci_prereqs_id */
    ";
    
    protected $queryWhere = "lu_application_programs.admission_status = 2
            AND (student_decision.decision IS NULL OR student_decision.decision = 'accept')";
    
    protected $queryGroupBy = "application_id, program_id";
    
    protected $queryOrderBy = 'users.lastname, users.firstname';

    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    protected $joinUsersinst = FALSE;
    protected $joinGrescore = FALSE;
    protected $joinRecommend = FALSE;
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';


    // Override findByApplicationId to resolve application_id ambiguity 
    protected function findByApplicationId($applicationId) {
    
        unset($this->findWheres);
        $this->findWheres[0] = "application.id = " . $applicationId;
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);            
        return $resultArray;   
    }
    
    
    // Override the find method to include the filter parameters.
    public function find($unit = NULL, $unitId = NULL, $admissionPeriodId = NULL, 
                            $applicationId = NULL, $luUsersUsertypesId = NULL, $searchString = '', 
                            $designStatus = '', $programmingStatus = '', $statisticsStatus = '') {
                            
        // If applicationId (from the autocomplete, e.g.), get the data and return
        // NOTE: application.id must be selected as application_id in $querySelect
        if ($applicationId || is_numeric($searchString)) {
            if (!$applicationId) {
                $applicationId = intval($searchString);    
            }
            return $this->findByApplicationId($applicationId);
        }
        
        if ($luUsersUsertypesId) {
            return $this->findByLuuId($luUsersUsertypesId, $unit, $unitId, $admissionPeriodId); 
        }
                                
        // Otherwise.... 
                               
        if ($unit && $unitId) {
            $this->setUnit($unit, $unitId);
        }

        if ($admissionPeriodId) {
            $this->setAdmissionPeriod($admissionPeriodId, $unit);
        }
        
        if ($searchString) {
            
            $this->setSearchString($searchString);   
        
        } else {
        
            $this->setSubmissionStatus('all');
            
            switch ($designStatus) {
                
                case 'designNotBegun':

                    $this->findWheres[] = "(design_prereq_status.reviewer_status IS NULL
                        OR design_prereq_status.reviewer_status = 'Not Submitted')";
                    break;
                
                case 'designSubmitted':
                
                    $this->findWheres[] = "design_prereq_status.reviewer_status = 'Submitted'";
                    break;    

                case 'designInProgress':
                
                    $this->findWheres[] = "design_prereq_status.reviewer_status = 'In progress'";
                    break; 
                    
                case 'designApprovedPlan':
                
                    $this->findWheres[] = "design_prereq_status.reviewer_status = 'Approved plan:'";
                    break; 

                case 'designFulfilledDegree':
                
                    $this->findWheres[] = "design_prereq_status.reviewer_status = 'Fulfilled: undergraduate degree'";
                    break;
                    
                case 'designFulfilledOther':
                
                    $this->findWheres[] = "design_prereq_status.reviewer_status = 'Fulfilled:'";
                    break;

                default:
                    // no design filter
            }
            
            switch ($programmingStatus) {
                
                case 'programmingNotBegun':

                    $this->findWheres[] = "(programming_prereq_status.reviewer_status IS NULL
                        OR programming_prereq_status.reviewer_status = 'Not Submitted')";
                    break;
                
                case 'programmingSubmitted':
                
                    $this->findWheres[] = "programming_prereq_status.reviewer_status = 'Submitted'";
                    break;    

                case 'programmingInProgress':
                
                    $this->findWheres[] = "programming_prereq_status.reviewer_status = 'In progress'";
                    break; 
                    
                case 'programmingApprovedPlan':
                
                    $this->findWheres[] = "programming_prereq_status.reviewer_status = 'Approved plan:'";
                    break; 

                case 'programmingFulfilledDegree':
                
                    $this->findWheres[] = "programming_prereq_status.reviewer_status = 'Fulfilled: undergraduate degree'";
                    break;
                    
                case 'programmingFulfilledOther':
                
                    $this->findWheres[] = "programming_prereq_status.reviewer_status = 'Fulfilled:'";
                    break;

                default:
                    // no programming filter
            }
            /*
            switch ($statisticsStatus) {
                
                case 'statisticsNotBegun':

                    $this->findWheres[] = "(statistics_prereq_status.reviewer_status IS NULL
                        OR statistics_prereq_status.reviewer_status = 'Not Submitted')";
                    break;
                
                case 'statisticsSubmitted':
                
                    $this->findWheres[] = "statistics_prereq_status.reviewer_status = 'Submitted'";
                    break;    

                case 'statisticsInProgress':
                
                    $this->findWheres[] = "statistics_prereq_status.reviewer_status = 'In progress'";
                    break; 
                    
                case 'statisticsApprovedPlan':
                
                    $this->findWheres[] = "statistics_prereq_status.reviewer_status = 'Approved plan:'";
                    break; 

                case 'statisticsFulfilledDegree':
                
                    $this->findWheres[] = "statistics_prereq_status.reviewer_status = 'Fulfilled: undergraduate degree'";
                    break;
                    
                case 'statisticsFulfilledOther':
                
                    $this->findWheres[] = "statistics_prereq_status.reviewer_status = 'Fulfilled:'";
                    break;

                default:
                    // no statistics filter
            }
            */
            
        }
        
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    }
}    
?>