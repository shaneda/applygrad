<?php
/* 
* A view of review table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListFacultyRankings extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    
    /* round 2 ranks */
    GROUP_CONCAT( DISTINCT
        IF (
            review.round = 2 AND review.fac_vote = 1 AND review.rank IS NOT NULL,
            CONCAT(users.lastname, ': ', 
                IF(review.rank = 0, '6', CAST(ROUND(review.rank) AS CHAR))
                ),
            NULL
        ) ORDER BY users.lastname
        SEPARATOR '|'
    ) AS faculty_rankings,
    
    /* round 2 rank MIN */
    MIN(
        IF (
            review.round = 2 AND review.fac_vote = 1 AND review.rank IS NOT NULL,
            IF (review.rank = 0, 6, ROUND(review.rank)),
            6
        )
    ) AS faculty_ranking_min
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id

    /* reviewers */
    INNER JOIN review ON review.application_id = application.id
    INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = review.reviewer_id
    INNER JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    
    ";
    
    protected $queryWhere = 'review.department_id = lu_programs_departments.department_id';
    
    protected $queryGroupBy = 'application.id, lu_programs_departments.department_id';
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
}    
?>
