<?php
/* 
* A view, suitable for inclusion in a 2D review list array,
* that shows which applications a particular reviewer has touched.
* 
* NOTE: The constructor has a reviewerId parameter.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListTouchedIni extends VW_ReviewList
{

    
    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    
    IF ( MIN(review_ini.round) = 1, 1, 0) AS round1_reviewer_touched,
    IF ( MAX(review_ini.round) = 2, 1, 0) AS round2_reviewer_touched,
    IF ( MAX(review_ini.round) = 3, 1, 0) AS round3_reviewer_touched
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN review_ini ON review_ini.application_id = application.id
    INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = review_ini.reviewer_id
    INNER JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
    ";

    protected $queryGroupBy = 'application.id';

    public function __construct($reviewerId = -1, $departmentName = null) {
        parent::__construct($departmentName);

        if ($reviewerId != -1) {
            $this->queryWhere = 'review_ini.reviewer_id = ' . $reviewerId . 
                                ' AND (review_ini.department_id = lu_programs_departments.department_id
                                    OR lu_user_department.department_id = lu_programs_departments.department_id)';    
        } else {
        
            $this->queryWhere = '(review_ini.department_id = lu_programs_departments.department_id 
                                    OR lu_user_department.department_id = lu_programs_departments.department_id)';    
        }
    } 
    
}    
?>
