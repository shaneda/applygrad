<?php
/* 
* A "synthetic" data access base class for assembling views
* of the database suitable for constructing a 2D (flat) review list array.
*/

class VW_ReviewList extends DB_Applyweb
{
    // Department info
    protected $departmentName;
    
    // Query pieces to be assembled.
    protected $querySelect;
    protected $queryFrom;
    protected $queryWhere = '';
    protected $queryGroupBy = ''; 
    protected $queryHaving = '';
    protected $queryOrderBy = '';
    
    // Search extras
    protected $searchSelect = '';
    protected $searchJoin = '';
    
    // These tables may already be joined in $queryFrom.
    protected $joinApplicationPrograms = TRUE;
    protected $joinProgramsDepartments = TRUE;
    
    // The field to use as the index for the first array dimension.
    protected $arrayKey = 'application_id';
    
    public function __construct($departmentName = null) {
        $this->departmentName = $departmentName;
        parent::__construct();
    }
    
    public function get($applicationId) {
    
        $query = $this->querySelect . $this->queryFrom;
        $query .= " WHERE application.id = " . $applicationId;
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;   
    }
    
    public function find($periodId = NULL, $departmentId = NULL, $round = 1, $groupsLimitReviewerId = NULL, 
                            $searchString = NULL, $submitted = 1) {
        
        $joins = array();
        $wheres = array();
        
        if ($periodId) {
            $joins[] = "INNER JOIN period_application ON application.id = period_application.application_id";
            
            // Compbio kluge 10/13/09: check to see if $admissionPeriodId is an array of period ids
            if ( is_array($periodId) ) {
                $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
                $wheres[] = "period_application.period_id IN " . $admissionPeriodIds;    
            } else {
                $wheres[] = "period_application.period_id = " . $periodId;    
            }
        }
        
        // Limit query to a particular department.
        if ($departmentId) {
            
            if($this->joinApplicationPrograms) {
                $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id";
            }
            if($this->joinProgramsDepartments) {
                $joins[] = "INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";
            }
            $wheres[] = "lu_programs_departments.department_id = " . $departmentId;
        }
        
        if ($round == 1) {
           if ($departmentId == 6) { 
               /* before Andy Pavlo fix 
                $wheres[] = "(promotion_status_round IS NULL OR promotion_status_round = 1)";
                */
                $wheres[] = "(promotion_status.round IS NULL OR promotion_status.round = 1)";
           } 
        }

        if ($round == 2) {
            
            // departmentId should be set, but...
            if (!$departmentId) {
                
                // Assign bogus id and do the joins to return empty set and avoid error.
                $departmentId = -1;
                
                if($this->joinApplicationPrograms) {
                    $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id";
                }
                if($this->joinProgramsDepartments) {
                    $joins[] = "INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";
                }    
            }
            
            if ($departmentId == 74)
            {
                $joins[] = "LEFT OUTER JOIN ( 
                              SELECT application_id, 
                              MIN( IFNULL(round2, 0) ) as min_vote,
                              MAX( IFNULL(round2, 0) ) as max_vote,
                              COUNT(round2) AS vote_count 
                              FROM review
                              LEFT OUTER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
                              WHERE review.round = 1
                              AND review.fac_vote = '0'
                              AND review.round2 IS NOT NULL
                              AND ( review.committee_vote = 1
                              OR (review.committee_vote = 0 
                                AND lu_users_usertypes.usertype_id = 1 
                                AND review.department_id = 74)
                              OR lu_users_usertypes.usertype_id IS NULL
                              OR lu_users_usertypes.usertype_id != 1 )
                              AND review.department_id = " . $departmentId . "
                              GROUP BY application_id
                              ) AS round2_votes ON application.id = round2_votes.application_id 
                            ";   
            }
            else
            {
                $joins[] = "LEFT OUTER JOIN ( 
                              SELECT application_id, 
                              MIN( IFNULL(round2, 0) ) as min_vote,
                              MAX( IFNULL(round2, 0) ) as max_vote,
                              COUNT(round2) AS vote_count 
                              FROM review
                              LEFT OUTER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
                              WHERE review.round = 1
                              AND review.fac_vote = '0'
                              AND review.round2 IS NOT NULL
                              AND ( review.committee_vote = 1
                              OR lu_users_usertypes.usertype_id IS NULL
                              OR lu_users_usertypes.usertype_id != 1 )
                              AND review.department_id = " . $departmentId . "
                              GROUP BY application_id
                              ) AS round2_votes ON application.id = round2_votes.application_id 
                            ";
            }
                                                
            // PLB added 12/1/09: CSD applicants may be passively promoted to round 2 through unanimous voting.
            // All other departments must use active promotion via the promote/demote interface.
            if ($departmentId == 1) {
                
                // CSD should be 2 unanimous votes.
                /*   before Andy Pavlo fix
                $wheres[] = " ( promotion_status_round = 2
                                OR (
                                    round2_votes.vote_count >= 2 
                                    AND round2_votes.min_vote != 0     
                                    )
                                )"; 
                */   
                $wheres[] = " ( promotion_status.round = 2
                                OR (
                                    round2_votes.vote_count >= 2 
                                    AND round2_votes.min_vote != 0     
                                    )
                                )";
            } elseif ($departmentId == 74) { 
                
                // CS MS is round two on one yes vote.
                /* before Andy Pavlo fix
                $wheres[] = " ( promotion_status_round = 2
                                OR (
                                    round2_votes.vote_count > 0 
                                    AND round2_votes.max_vote = 1     
                                    )
                                )";
                */
                $wheres[] = " ( promotion_status.round = 2
                                OR (
                                    round2_votes.vote_count > 0 
                                    AND round2_votes.max_vote = 1     
                                    )
                                )";                                  
            } elseif ($departmentId == 2) { 
                
                // Restored for "historical" purposes: ML should be three unanimous votes.
                /* before Andy Pavlo fix
                $wheres[] = " !(lu_application_programs.round2 <=> '2')
                                AND 
                                ( promotion_status_round = 2
                                    OR promotion_status_round = 3 
                                    OR (
                                        round2_votes.vote_count >= 3 
                                        AND round2_votes.min_vote != 0     
                                    )
                                )";
                */
                $wheres[] = " !(lu_application_programs.round2 <=> '2')
                                AND 
                                ( promotion_status.round = 2
                                    OR promotion_status.round = 3 
                                    OR (
                                        round2_votes.vote_count >= 3 
                                        AND round2_votes.min_vote != 0     
                                    )
                                )";
                                             
            } elseif ($departmentId == 58) { 
                
                // Statistics MS has single evaluator: one yes vote is sufficient.
                /* before Andy Pavlo fix
                $wheres[] = " !(lu_application_programs.round2 <=> '2')
                                AND 
                                ( promotion_status_round = 2
                                    OR promotion_status_round = 3 
                                    OR (
                                        round2_votes.vote_count >= 1 
                                        AND round2_votes.min_vote != 0     
                                    )
                                )"; 
                */
                $wheres[] = " !(lu_application_programs.round2 <=> '2')
                                AND 
                                ( promotion_status.round = 2
                                    OR promotion_status.round = 3 
                                    OR (
                                        round2_votes.vote_count >= 1 
                                        AND round2_votes.min_vote != 0     
                                    )
                                )";
            
            } elseif ($departmentId == 50 || $departmentId == 6) { 
                // depaartments where an applicant only appears in one round
                /* before Andy Pavlo fix
                $wheres[] = "(promotion_status_round = 2)";
                */
                $wheres[] = "(promotion_status.round = 2)"; 
            
            } 
            elseif ($this->departmentName == 'Psychology') 
            {
                // Promotion or at least one yes and zero no votes
                /* before Andy Pavlo fix
                $wheres[] = " ( promotion_status_round = 2
                                OR promotion_status_round = 3  
                                OR round2_votes.max_vote = 1
                                )";
                */
                $wheres[] = " ( promotion_status.round = 2
                                OR promotion_status.round = 3  
                                OR round2_votes.max_vote = 1
                                )";    

            }
            else 
            {

                // Default: no voting, only promotion.
                /* before Andy Pavlo fix
                $wheres[] = "(promotion_status_round = 2 OR promotion_status_round = 3)";
                */
                $wheres[] = "(promotion_status.round = 2 OR promotion_status.round = 3)";    
            }
                  
        }

        if ($round == 3) {

            if ($this->departmentName == 'Psychology')
            {
                $joins[] = "LEFT OUTER JOIN ( 
                              SELECT application_id, 
                              MIN( IFNULL(round3, 0) ) as min_vote,
                              MAX( IFNULL(round3, 0) ) as max_vote,
                              COUNT(round3) AS vote_count 
                              FROM review
                              LEFT OUTER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id
                              WHERE review.round = 2
                              AND review.fac_vote = '0'
                              AND review.round3 IS NOT NULL
                              AND ( review.committee_vote = 1
                              OR lu_users_usertypes.usertype_id IS NULL
                              OR lu_users_usertypes.usertype_id != 1 )
                              AND review.department_id = " . $departmentId . "
                              GROUP BY application_id
                              ) AS round3_votes ON application.id = round3_votes.application_id 
                            ";                
            }
            
            // departmentId should be set, but...
            if (!$departmentId) {
                
                // Assign bogus id and do the joins to return empty set and avoid error.
                $departmentId = -1;
                
                if($this->joinApplicationPrograms) {
                    $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id";
                }
                if($this->joinProgramsDepartments) {
                    $joins[] = "INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";
                }    
            }
            
            if ($this->departmentName == 'Psychology') 
            {
                // Promotion or at least one yes and zero no votes
                /* before Andy Pavlo fix
                $wheres[] = " ( promotion_status_round = 3
                                OR round3_votes.vote_count >= 1     
                                )";
                */
                $wheres[] = " ( promotion_status.round = 3
                                OR round3_votes.vote_count >= 1     
                                )";    

            }
            else
            {
                /* before Andy Pavlo fix
                $wheres[] = "promotion_status_round = 3"; 
                */
                $wheres[] = "promotion_status.round = 3";   
            }
        }
        
        if ($round == 4) {
            
            // departmentId should be set, but...
            if (!$departmentId) {
                
                // Assign bogus id and do the joins to return empty set and avoid error.
                $departmentId = -1;
                
                if($this->joinApplicationPrograms) {
                    $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id";
                }
                if($this->joinProgramsDepartments) {
                    $joins[] = "INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";
                }    
            }

            /* before Andy Pavlo fix
            $wheres[] = "promotion_status_round = 4";
            */
            $wheres[] = "promotion_status.round = 4";

        }
        
        // Always join promotion status, instead of just for round limit.
        /*     Before Andy Pavlo fix    
        $joins[] = "LEFT OUTER JOIN ( 
                          SELECT application_id, 
                          round AS promotion_status_round
                          FROM promotion_status
                          WHERE promotion_status.department_id = " . $departmentId . "
                          ) AS promotion_status ON application.id = promotion_status.application_id 
                        ";
                        */

        $joins[] = "LEFT OUTER JOIN promotion_status ON (application.id = promotion_status.application_id
                       AND promotion_status.department_id = " . $departmentId . ")";
        
        
        // Limit query to a particualr reviewer's reviewer groups.
        if ($groupsLimitReviewerId) {
            
            // departmentId should be set, but...
            if (!$departmentId) {
                if($this->joinApplicationPrograms) {
                    $joins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id";
                }
                if($this->joinProgramsDepartments) {
                    $joins[] = "INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id";
                }    
            }
            
            $joins[] = "INNER JOIN lu_application_groups ON lu_application_groups.application_id = application.id";
            $joins[] = "INNER JOIN lu_reviewer_groups ON lu_application_groups.group_id = lu_reviewer_groups.group_id";
            
            $wheres[] = "lu_application_groups.round = " . $round;
            $wheres[] = "lu_reviewer_groups.round = " . $round;
            $wheres[] = "lu_reviewer_groups.reviewer_id = " . $groupsLimitReviewerId;
             
        }
        
        if ($searchString) {
            $this->parseSearch($searchString);
        }
        
        if ($submitted) {
            $wheres[] = "application.submitted = 1";
            if (!(/* $departmentId == 74 || */ $departmentId == 50 || $departmentId == 103 || $departmentId == 97)) {
                // Payment required for all departments except CS MS, RI-Scholars, REU-SE, and msasbe. 
                $wheres[] = "(application.paid = 1 OR application.waive = 1)";    
            }  
        }

        $query = $this->querySelect;

        if ($this->searchSelect) {
            $query .= $this->searchSelect;
        }
        
        $query .= $this->queryFrom;
        if ( isset($joins) ) {
            $query .= ' ' . implode(' ', $joins);     
        }
        
        if ($this->searchJoin) {
            $query .= $this->searchJoin;
        }
        
        if ($this->queryWhere) {
            $query .= ' WHERE ' . $this->queryWhere;
            if ( isset($wheres) ) {
                $query .= ' AND ' . implode(' AND ', $wheres);     
            }        
        } elseif ( isset($wheres) ) {
            $query .= ' WHERE ' . implode(' AND ', $wheres);     
        }
        
        if ($this->queryGroupBy) {
            $query .= " GROUP BY " . $this->queryGroupBy;    
        }
        
        if ($this->queryHaving) {
            $query .= " HAVING " . $this->queryHaving;    
        }
        
        if ($this->queryOrderBy) {
            $query .= " ORDER BY " . $this->queryOrderBy;    
        }
        
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    }
    
    protected function parseSearch($searchString = '') {
        
        if ( $searchString != '' )
        {
            // format search string for boolean mode query
            $formattedSearchString = "";
            $ssParts = split( " ", $searchString);
            foreach ( $ssParts as $word )
            {
                $formattedSearchString .= "+" . $word . " ";
            }
            $formattedSearchString = trim($formattedSearchString);
            
            // addition to select parameters
            $stringEnd = strlen($searchString) + 200;
            $this->searchSelect = ", SUBSTRING( searchMatches.application_text, 
                LOCATE('$searchString', searchMatches.application_text) -10, $stringEnd ) as searchContext";
            // addition to join
            $this->searchJoin = " INNER JOIN (
                    SELECT application_id, application_text 
                    FROM searchText st 
                    WHERE MATCH (application_text) 
                    AGAINST( '$formattedSearchString' IN BOOLEAN MODE ) 
                ) AS searchMatches 
                ON searchMatches.application_id = application.id ";
        } else {
            $this->searchJoin = "";    
        }
        
        return TRUE;
    }
    
    protected function setDateRange($dateRangeStart=NULL, $dateRangeEnd=NULL) {

        $whereString = "";
        if ($dateRangeStart) {
            $whereString .= " AND CAST(application.submitted_date AS DATE) >=";
            $whereString .= " CAST('" . $dateRangeStart . "' AS DATE)";    
        }
        if ($dateRangeEnd) {
            $whereString .= " AND CAST(application.submitted_date AS DATE) <=";
            $whereString .= " CAST('" . $dateRangeEnd . "' AS DATE)";    
        }        
        
        return $whereString;
        
    }
    
}
?>
