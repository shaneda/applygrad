<?php
/* 
* A "synthetic" data access base class for assembling views
* of the database suitable for constructing a 2D (flat) review list array.
*/

class DB_VW_Applyweb extends DB_Applyweb
{
    // Base query pieces.
    protected $querySelect;
    protected $queryFrom;
    protected $queryWhere = '';
    protected $queryGroupBy = ''; 
    protected $queryHaving = '';
    protected $queryOrderBy = '';
    
    // Additional find criteria.
    protected $findSelects = array();
    protected $findJoins = array();
    protected $findWheres = array();
    protected $findHavings = array();
    
    // The field to use as the index for the first array dimension.
    protected $arrayKey;
    
    public function find() {
        
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    }
    
    public function assembleFindQuery() {

        $query = $this->querySelect;
        if ( !empty($this->findSelects) ) {
            $query .= ', ' . implode(', ', $this->findSelects);     
        }
        
        $query .= $this->queryFrom;
        if ( !empty($this->findJoins) ) {
            $query .= ' ' . implode(' ', $this->findJoins);     
        }
        
        if ($this->queryWhere) {
            $query .= ' WHERE ' . $this->queryWhere;
            if ( !empty($this->findWheres) ) {
                $query .= ' AND ' . implode(' AND ', $this->findWheres);     
            }        
        } elseif ( !empty($this->findWheres) ) {
            $query .= ' WHERE ' . implode(' AND ', $this->findWheres);     
        }
        
        if ($this->queryGroupBy) {
            $query .= " GROUP BY " . $this->queryGroupBy;    
        }
        
        if ($this->queryHaving) {
            $query .= " HAVING " . $this->queryHaving;
            if ( !empty($this->findHavings) ) {
                $query .= ' AND ' . implode(' AND ', $this->findHavings);     
            }        
        } elseif ( !empty($this->findHavings) ) {
            $query .= ' HAVING ' . implode(' AND ', $this->findHavings);     
        }
        
        if ($this->queryOrderBy) {
            $query .= " ORDER BY " . $this->queryOrderBy;    
        }
        
        return $query;        
    }
}
?>