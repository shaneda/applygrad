<?php
/* 
* A view of recommend table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListNoteCount extends VW_AdminList
{

    protected $querySelect = 
        "
        SELECT application_admin_note.application_id,
        COUNT(note) AS admin_note_count
        ";
    
    protected $queryFrom = "FROM application_admin_note
                            INNER JOIN application 
                            ON application_admin_note.application_id = application.id"; 
    
    protected $queryGroupBy = "application_admin_note.application_id";

}    
?>
