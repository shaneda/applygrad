<?php
/*
Class for admission period data access
*/

class DB_AdmissionPeriod extends DB_Applyweb
{  

    function get($admissionPeriodId) {
    
        $period_query = "SELECT * FROM application_periods WHERE application_period_id = " . $admissionPeriodId;
        $period_array = $this->handleSelectQuery($period_query);    
        return $period_array;
        
    }   

}  
?>