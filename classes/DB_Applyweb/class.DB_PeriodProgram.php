<?php
/*
Class for period_program table data access.
*/

class DB_PeriodProgram extends DB_Applyweb
{

      public function get($periodId = NULL, $unitId = NULL) {

        $query = "SELECT period_program.* FROM period_program"; 
        if ($periodId) {
            $query .=  " WHERE period_id = " . $periodId ;   
        }
        if ($unitId) {
            if (!$periodId) {
                $query .=  " WHERE";    
            } else {
                $query .=  " AND";    
            }
            $query .=  " unit_id = " . $unitId;
        }
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }
    
    public function find($periodId = NULL, $unitId = NULL) {

        $resultArray = $this->get($periodId, $unitId);

        return $resultArray;           
    }

    public function save($recordArray) {

        $periodId = intval($recordArray['period_id']);
        $unitId = intval($recordArray['unit_id']);
        $valueList = $periodId . ", " . $unitId;
        $updateList = "period_id=" . $periodId . ", unit_id=" . $unitId;
        
        $query = "INSERT INTO period_program VALUES (" . $valueList . ")
            ON DUPLICATE KEY UPDATE " . $updateList;
        $numAffectedRows = $this->handleInsertQuery($query);
        
        return $numAffectedRows;
    }
    
    public function insert($recordArray) {
        return $this->save($recordArray);    
    }
    
    public function update($recordArray) {
        return $this->save($recordArray);    
    }
    
    public function delete($periodId, $unitId = NULL) {

        if (!$periodId && !$unitId) {
            return FALSE;
        }
        
        $query = "DELETE FROM period_program WHERE period_id = " . $periodId;
        if ($unitId) {
            $query .= " AND unit_id = " . $unitId;   
        }
        
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;
    }

}

?>