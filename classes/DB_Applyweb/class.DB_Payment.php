<?php
/*
Class for payment table operations
*/

class DB_Payment extends DB_Applyweb
{
    
    public function get($paymentId) {
        
        $query = "SELECT * FROM payment WHERE payment_id = " . $paymentId;
        $resultArray = $this->handleSelectQuery($query, 'payment_id');
        return $resultArray;
    }
    
    public function find($applicationId) {
        
        $query = "SELECT * FROM payment WHERE application_id = " . $applicationId . " ORDER BY payment_intent_date";
        $resultArray = $this->handleSelectQuery($query, 'payment_id');
        return $resultArray;        
    }
    
    public function insert($applicationId, $paymentType, $paymentAmount, 
                                $paidConfirmed = 0, $paidConfirmedDate = NULL) {
        
        
        if ($paidConfirmedDate) {
            $paidConfirmedDate = "'" . $paidConfirmedDate . "'";
        } else {
            $paidConfirmedDate = "NULL";
        } 
        $query = "INSERT INTO payment 
                    (application_id, payment_type, payment_amount, payment_intent_date, paid_confirmed, paid_confirmed_date) 
                    VALUES (" 
                    . $applicationId . ", " 
                    . $paymentType . ", " 
                    . $paymentAmount .  ", 
                    NOW(), "
                    . $paidConfirmed . ", "
                    . $paidConfirmedDate . "
                    )";
        $numAffectedRows = $this->handleInsertQuery($query);
        $paymentId = self::$mysqli->insert_id;
        return $paymentId;        
    }

    public function update($paymentId, $paymentType = NULL, $paymentAmount = NULL, $paymentIntentDate = '', 
                            $systemConfirm = NULL, $systemConfirmDate = '', $paidConfirmed = NULL, $paidConfirmedDate = '') {
        
        if (!$paymentType) {
            $paymentType = "payment_type";
        }
        if (!$paymentAmount) {
            $paymentAmount = "payment_amount";
        }
        if ($paymentIntentDate != '') {
            $paymentIntentDate = "'" . $paymentIntentDate . "'";
        } else {
            $paymentIntentDate = "payment_intent_date";
        }
        if ($systemConfirm !== 0 && $systemConfirm !== 1) {
            $systemConfirm = 'system_confirm';
        }
        if ($systemConfirmDate != '') {
            $systemConfirmDate = "'" . $systemConfirmDate . "'";
        } else {
            $systemConfirmDate = "system_confirm_date";
        }
        if ($paidConfirmed !== 0 && $paidConfirmed !== 1) {
            $paidConfirmed = 'paid_confirmed';
        }        
        if ($paidConfirmedDate != '') {
            $paidConfirmedDate = "'" . $paidConfirmedDate . "'";
        } else {
            $paidConfirmedDate = "paid_confirmed_date";
        }
                                       
        $query = "UPDATE payment SET 
                    payment_type = " . $paymentType . ",
                    payment_amount = " . $paymentAmount . ",
                    payment_intent_date = " . $paymentIntentDate . ",
                    system_confirm  = " . $systemConfirm . ",
                    system_confirm_date  = " . $systemConfirmDate . ",
                    paid_confirmed  = " . $paidConfirmed . ",
                    paid_confirmed_date  = " . $paidConfirmedDate . "
                    WHERE payment_id = " . $paymentId;
        $numAffectedRows = $this->handleUpdateQuery($query);
        return $numAffectedRows;         
    }
    
    public function delete($paymentId) {
    
        if (!$paymentId) {
            return FALSE;
        }
        
        $query = "DELETE FROM payment WHERE payment_id = " . $paymentId;
        $numAffectedRows =  $this->handleUpdateQuery($query);
        return $numAffectedRows;
    }
   
}
?>