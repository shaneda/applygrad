<?php
/* 
* A view of recommend table data suitable for inclusion 
* in a 2D admin list array.
*/

class VW_AdminListFees extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT 
    lu_application_programs.application_id,
    programs.id AS program_id,
    choice,
    programs.programprice,
    programs.baseprice
    ";
    
    //protected $queryFrom = "FROM lu_application_programs
    //                        INNER JOIN programs on programs.id = lu_application_programs.program_id";
    protected $queryFrom = "FROM lu_application_programs
                            INNER JOIN application ON application.id = lu_application_programs.application_id
                            INNER JOIN programs on programs.id = lu_application_programs.program_id";
    
    
    protected $queryOrderBy = "application_id, choice";
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping in getFeeTotals()
    */ 
    protected $arrayKey = '';
    
    /*
    * Loop the one-to-many application_program records to sum the fees for each application.
    */
    public function getFeeTotals($unit = NULL, $unitId = NULL, $admissionPeriodId = NULL, 
                                    $applicationId = NULL, $searchString ='', 
                                    $applicationStatus = 'submitted', $luUsersUsertypesId = NULL) {

        $programFeeRecords = $this->find($unit, $unitId, $admissionPeriodId, 
                                            $applicationId, $searchString, 
                                            $applicationStatus, $luUsersUsertypesId);
                                    
        $totalFeeArray = array();
        $previousApplicationId = NULL;
        $totalFees = 0;
        foreach ($programFeeRecords as $record) {
            
            $applicationId = $record['application_id'];
            
            if ( $applicationId != $previousApplicationId ) {
                
                // New applicationId, so set array for previous applicationId
                if ($previousApplicationId != NULL) {
                    $totalFeeArray[$previousApplicationId] = array('total_fees' => $totalFees);      
                }
                // Reset fees, counter
                $totalFees = 0.0;
                $i = 0;     
            }
            
            if($i == 0) {
                $totalFees = $record['baseprice'];
            } else {
                $totalFees += $record['programprice'];
            }
            $i++;
            
            $previousApplicationId = $applicationId;
        }
        // Set the array for the final/single iteration.
        $totalFeeArray[$previousApplicationId] = array('total_fees' => $totalFees);
        
        return $totalFeeArray;                                                                    
    }
}    
?>
