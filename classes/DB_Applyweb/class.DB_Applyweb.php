<?php
/*
* Base class for querying the Applyweb DB.
* Loosely based on:
* http://toys.lerdorf.com/archives/38-The-no-framework-PHP-MVC-framework.html
*/

class DB_Applyweb
{

    protected static $mysqli = FALSE;

    function __construct() {

        if (!self::$mysqli) {
            self::connect();
        }
    }

/*
    function __destruct() {

        if (self::$mysqli) {
            self::$mysqli->close();       
            self::$mysqli = FALSE;
        }
    }
*/

    public static function connect() {
        
        // use globals from config.php
        global $db;
        global $db_host;
        global $db_username;
        global $db_password;

        // Establish db connection
        self::$mysqli = new mysqli($db_host, $db_username, $db_password); 
           
        // Exit if connection error
        if (self::$mysqli->connect_error) {
            self::fatal_error( mysqli_connect_error() );
        } else {
            // proceed
            // echo "connected to db<br>\n";
        }
        
        // Select db, exit if error
        if ( self::$mysqli->select_db($db) ) {
            // proceed
            // echo "db selected<br>\n";
        } else {
            self::fatal_error( self::$mysqli->error );
        }
        
        // for testing
        // echo "<p>new db connection</p>\n";
        
    }

    
    /*
    * Submit a select query, fetch each result row as an associative array
    * with field names as keys, and return a 2D array of result row arrays.
    * If arrayKey is set with a field name, the result rows will be indexed 
    * with the value of that field.
    * If returnArray is set to false, the result object will be returned. 
    */ 
    public function handleSelectQuery($query, $arrayKey="", $returnArray=TRUE) {
    
        //DebugBreak();
        // Connect to db if established connection not available
        if (!self::$mysqli) {
            self::connect();
        } 
          
        self::$mysqli->query("SET CHARACTER SET 'utf8'");
        self::$mysqli->query("SET SESSION group_concat_max_len=4096"); 
        
        // create new array to hold query results
        $resultArray = array();

        // execute the query
        $resultObject = self::$mysqli->query($query);
        if ($returnArray) {
            if (!$resultObject) {
                mail("dales+error@cs.cmu.edu", "Select Error", $_SESSION['roleLuuId']. ": " . $query);
            }
            // move results into results_arrray
            while ($row = $resultObject->fetch_assoc()) {
                
                if ($arrayKey) {
                    $key = $row[$arrayKey];
                    $resultArray[$key] = $row;     
                } else {
                    $resultArray[] = $row;    
                }
                
            }
            $resultObject->free();
            
            return $resultArray;
                
        } else {
            
            return $resultObject;
            
        }
        
    }   

    
    // Submit an insert query and return true/false for success/failure
    public function handleInsertQuery($query) {
    
        // Connect to db if established connection not available
        if (!self::$mysqli) {
            self::connect();
        } 
        
        // execute the query
        self::$mysqli->query($query);
        
        // get the affected rows as return value
        $affected_rows = self::$mysqli->affected_rows;
        
        // return affected rows
        return $affected_rows;          
    }


    // Submit an update query and return true/false for success/failure
    public function handleUpdateQuery($query) {
    
        // Connect to db if established connection not available
        if (!self::$mysqli) {
            self::connect();
        } 
        
        // execute the query
        self::$mysqli->query($query);
        
        // get the affected rows as return value
        $affected_rows = self::$mysqli->affected_rows;
        
        // return affected rows
        return $affected_rows;          
    }

    
    // Submit a stored select query, fetch each result row as an associative 
    // array with field names as keys, take care of the second 'status' result 
    // set from the stored procedure, and return the 2D array of result row arrays
    public function handleStoredSelectQuery($query) {

        // Connect to db if established connection not available 
        if (!self::$mysqli) {
            self::connect();
        } 
        
        // create new array to hold query results
        $results_array = array();

        // execute multi query to handle multiple result sets
        self::$mysqli->multi_query($query);

        // move first result set to results_array
        if ($result = self::$mysqli->store_result()) {
            while ($row = $result->fetch_assoc()) {
                $results_array[] = $row;
            }
            $result->free();
        } // ADD ERROR HANDLING HERE

        // check for second result set returned with stored procedure
        if (self::$mysqli->more_results()) {
            // loop through (once?) and do nothing
            while (self::$mysqli->next_result());
        }
        
        return $results_array;     
    }
    
    
    public function escapeString($string) {
        return self::$mysqli->real_escape_string($string);
    }
    
    public function getLastInsertId() {
        return self::$mysqli->insert_id;
    }
    
    // use for all db-related errors
    protected function fatal_error($msg) {
        echo "<pre>Error!: $msg\n";
        $bt = debug_backtrace();
        foreach($bt as $line) {
            $args = var_export($line['args'], true);
            echo "{$line['function']}($args) at {$line['file']}:{$line['line']}\n";
        }
        echo "</pre>";
        die();
    }

}
?>