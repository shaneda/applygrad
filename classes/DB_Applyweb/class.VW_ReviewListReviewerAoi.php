<?php
/* 
* A view of review table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListReviewerAoi extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    
    /* LTI reviewer AOI */
    GROUP_CONCAT( DISTINCT
        lu_review_interest.program_id
        ORDER BY lu_review_interest.program_id
        SEPARATOR '|'
    ) AS reviewer_aoi,
    other_interest AS reviewer_other_aoi
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN review ON review.application_id = application.id
    /* LTI reviewer AOI */
    INNER JOIN lu_review_interest ON review.id = lu_review_interest.review_id
    ";
    
    protected $queryWhere = 'review.department_id = lu_programs_departments.department_id';
    
    protected $queryGroupBy = 'application.id';
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
}    
?>
