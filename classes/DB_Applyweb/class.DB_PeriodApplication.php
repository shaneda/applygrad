<?php
/*
Class for period_application table data access.
*/

class DB_PeriodApplication extends DB_Applyweb
{

    public function get($periodId = NULL, $applicationId = NULL) {

        $query = "SELECT * FROM period_application"; 
        if ($periodId) {
            $query .=  " WHERE period_id = " . $periodId;
            if ($applicationId) {
                $query .=  " AND application_id = " . $applicationId;    
            }   
        } else {
            if ($applicationId) {
                $query .=  " WHERE application_id = " . $applicationId;    
            }            
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }
    
    public function find($periodId = NULL, $applicationId = NULL) {
        return $this->get($periodId, $applicationId);
    }
    
    public function save($periodId, $applicationId) {
        
        // Enforce cardinality by first deleting any existing records for the applicationId. 
        // (An application should be associated with, at most, one period.)
        $existingRecords = $this->get(NULL, $applicationId);
        if ( count($existingRecords) > 0 ) { 
            $affectedRows = $this->delete($applicationId);    
        }
    
        // Now insert the new record.
        $replaceQuery = "REPLACE INTO period_application VALUES 
                    (" . intval($periodId) . ", " . intval($applicationId) . ")";
        $affectedRowCount = $this->handleInsertQuery($replaceQuery); 
        
        return $affectedRowCount;
    }
    
    public function insert($periodId, $applicationId) {
        return $this->save($periodId, $applicationId);    
    }
    
    public function update($periodId, $applicationId) {
        return $this->save($periodId, $applicationId);    
    }
    
    public function delete($applicationId = NULL, $periodId = NULL) {
        
        if (!$applicationId && !$periodId) {
            return FALSE;
        }
        
        $query = "DELETE FROM period_application";
        if ($applicationId) {
            $query .=  " WHERE application_id = " . intval($applicationId);
            if ($periodId) {
                $query .=  " AND period_id = " . intval($periodId);    
            }   
        }
        $affectedRowCount = $this->handleInsertQuery($query);    
        
        return $affectedRowCount;    
    }

}

?>