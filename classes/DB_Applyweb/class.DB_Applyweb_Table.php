<?php
/* 
* Base class for basic Applyweb DB table access.
*/

class DB_Applyweb_Table extends DB_Applyweb
{

    protected $tableName;
    protected $fields = array();            // field info, 2D, indexed by field name
    protected $primaryKeyFields = array();  // field names, 1D, unindexed
    protected $autoIncrementId = FALSE;     // is the primary key an auto_increment field

    public function __construct($tableName = NULL) {
    
        if ($tableName) {
            
            $this->tableName = $tableName;
        }
        
        if ( isset($this->tableName) ) {
            
            $query = "SHOW FIELDS FROM " . $this->tableName;
            $this->fields = $this->handleSelectQuery($query, 'Field');            
        }
        
        foreach ($this->fields as $fieldName => $fieldArray) {
            
            if ($fieldArray['Key'] == 'PRI') {
                $this->primaryKeyFields[] = $fieldName;
            }
            
            if ($fieldArray['Extra'] == 'auto_increment') {
                $this->autoIncrementId = TRUE;
            }
        }
    }

    
    /*
    * Get a single record using its primary key value(s).  
    */
    protected function get( $primaryKeyValues = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }
         
        $keyWhere = $this->getKeyWhere($primaryKeyValues);
        if ( !$keyWhere ) {
            return FALSE;
        }
         
        $query = "SELECT * FROM " . $this->tableName;
        $query .= " WHERE " . $keyWhere;
        $query .= " LIMIT 1";
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;        
    }
    
    /*
    * Get records using one or more foreign key values. 
    * This is essentially an "interface." 
    */
    protected function find( $foreignKeyValues = array() ) {
        return NULL;
    }
    
    
    /*
    * Insert a single record.
    */
    protected function insert( $record = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }

        $i = 0;
        $fieldList = $valueList = "";
        foreach ($record as $field => $value) {
        
            if ( !array_key_exists($field, $this->fields) ) {
                return FALSE;    
            }
            
            if (!$value) {
                 continue;
            }
            
            if ($i > 0) {
                $fieldList .= ", ";
                $valueList .= ", ";
            }
            
            $fieldList .= $field;
            $valueList .= "'" . $this->escapeString($value) . "'";
            
            $i++;
        }
        
        $query = "INSERT INTO " . $this->tableName;     
        $query .= " (" . $fieldList . ") VALUES (" . $valueList . ")";
        
        $affectedRowCount = $this->handleInsertQuery($query);
        
        if ($this->autoIncrementId) {
            return $this->getLastInsertId();    
        } else {
            return $affectedRowCount;
        }    
    }

    
    /*
    * Update a single record using its primary key value(s). 
    */
    protected function update( $record = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }

        $keyWhere = $this->getKeyWhere($record);
        if ( !$keyWhere ) {
            return FALSE;
        }
        
        $i = 0;
        $setList = "";
        foreach ($record as $field => $value) {
        
            if ( in_array($field, $this->primaryKeyFields) ) {
                continue;
            }

            if ( !array_key_exists($field, $this->fields) ) {
                return FALSE;    
            }
            
            // Skip empty strings???
            if ( $value === '' ) {
                 //continue;
            }
            
            if ($i > 0) {
                $setList .= ", ";
            }
            $setList .= $field . " = ";
            if ($value === NULL) {
                $setList .= "NULL";
            } else {
                $setList .= "'" . $this->escapeString($value) . "'";   
            }
            
            $i++;
        }
    
        if ( !$setList ) {
            return FALSE;
        }

        $query = "UPDATE " . $this->tableName;
        $query .= " SET " . $setList;
        $query .= " WHERE " . $keyWhere;
        
        $affectedRowCount = $this->handleUpdateQuery($query);
        
        return $affectedRowCount; 
    }    


    /*
    * Delete a single record using its primary key value(s).  
    */
    protected function delete( $primaryKeyValues = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }
         
        $keyWhere = $this->getKeyWhere($primaryKeyValues);
        if ( !$keyWhere ) {
            return FALSE;
        }
         
        $query = "DELETE FROM " . $this->tableName;
        $query .= " WHERE " . $keyWhere;
        $query .= " LIMIT 1";
        
        $affectedRowCount = $this->handleUpdateQuery($query);
        
        return $affectedRowCount;        
    }


    protected function getKeyWhere($keyValues) {
        
        $i = 0;
        $keyWhere = "";
        foreach ($this->primaryKeyFields as $keyField) {
        
            if ( !isset($keyValues[$keyField]) ) {
                return FALSE;
            }
            
            if ($i > 0) {
                $keyWhere .= " AND ";
            }
            $keyWhere .= $keyField . " = ";
            $keyWhere .= "'" . $this->escapeString($keyValues[$keyField]) . "'"; 
            
            $i++;     
        }
        
        return $keyWhere;
    }
    

    
    
    
    // Potentially to be used with prepared statements.
    protected function getPrepareDatatype($mysqlType) {
        
        $commaPosition = strpos($mysqlType,'(');
        if ($commaPosition) {
            $datatype = substr($mysqlType, 0, $commaPosition);    
        } else {
            $datatype = $mysqlFieldType;
        }
        
        switch ( strtoupper($datatype) ) {
        
            case 'CHAR':
            case 'VARCHAR':
            case 'BINARY':
            case 'VARBINARY':
            
                $prepareDatatype = 's';
                break;    

            case 'BLOB':
            case 'TINYBLOB':
            case 'MEDIUMBLOB':
            case 'LONGBLOB':
            case 'TEXT':
            case 'TINYTEXT':
            case 'MEDIUMTEXT':
            case 'LONGTEXT':
                $prepareDatatype = 'b';
                break;

            
            case 'INT':
            case 'TINYINT':
            case 'SMALLINT':
            case 'MEDIUMINT':
            case 'BIGINT':
            case 'BIT':
            case 'BOOL':
            
                $prepareDatatype = 'i';
                break;

            case "FLOAT":
            case "DOUBLE":
            case "DECIMAL":
            
                $prepareDatatype = 'd';
                break;
            
            default:
            
                $prepareDatatype = FALSE;    
        
        }
        
        return $prepareDatatype;
    }
    
}
?>