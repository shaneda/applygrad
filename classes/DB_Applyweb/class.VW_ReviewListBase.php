<?php
/* 
* A view, suitable for inclusion in a 2D review list array,
* of the application data from the primary (non-lookup) tables 
* that can be efficiently inner joined.
* 
* This is the "foundational" view with which the other
* view data will be merged/appended. 
*/

class VW_ReviewListBase extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT DISTINCT
    /* student bio, application status */
    application.id AS application_id,
    application.submitted_date as submitted_date,
    application.created_date as created_date,
    lu_users_usertypes.id AS lu_users_usertypes_id,
    users.id AS users_id,
    CONCAT(
      users.lastname,
      ', ',
      users.firstname
      ) AS name,
    users_info.gender,
    CONCAT( 
        IF(ethnicity.name = 'Hispanic/Latino', CONCAT(ethnicity.name, ', '), ''), 
        IFNULL( GROUP_CONCAT(DISTINCT ipeds_race.ipeds_race SEPARATOR ', '), '') 
    ) AS ethnicity,
    countries_cit.iso_code AS cit_country_iso_code,
    countries_cit.name AS cit_country
    ,
    /* undergrad */
    institutes.name AS undergrad_institute_name,
    usersinst.major1,
    usersinst.date_grad,
    CONCAT(usersinst.gpa, ' / ', gpascales.name) AS gpa
    ,
    /* gre */
    CONCAT(grescore.verbalscore, ' / ', grescore.verbalpercentile) 
        AS gre_verbal, 
    CONCAT(grescore.quantitativescore, ' / ', grescore.quantitativepercentile) 
        AS gre_quantitative,
    CONCAT(grescore.analyticalscore, ' / ', grescore.analyticalpercentile) 
        AS gre_analytical,
    CONCAT(grescore.analyticalwritingscore, ' / ', grescore.analyticalwritingpercentile) 
        AS gre_writing  
    ";
    
    protected $queryFrom = 
    "
    FROM application
    /* student bio, application status */
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    LEFT OUTER JOIN ethnicity ON users_info.ethnicity = ethnicity.id
    LEFT OUTER JOIN applicant_ipeds_race ON lu_users_usertypes.id = applicant_ipeds_race.lu_users_usertypes_id
    LEFT OUTER JOIN ipeds_race on ipeds_race.ipeds_race_id = applicant_ipeds_race.ipeds_race_id
    LEFT OUTER JOIN visatypes ON users_info.visastatus = visatypes.id
    LEFT OUTER JOIN countries AS countries_cit ON users_info.cit_country = countries_cit.id
    /* undergrad */
    LEFT OUTER JOIN usersinst ON usersinst.user_id = application.user_id 
        AND usersinst.application_id = application.id
        AND usersinst.educationtype = 1
    LEFT OUTER JOIN degreesall ON degreesall.id = usersinst.degree
    LEFT OUTER JOIN institutes ON institutes.id = usersinst.institute_id
    LEFT OUTER JOIN gpascales ON gpascales.id = usersinst.gpa_scale
    /* gre */
    LEFT OUTER JOIN grescore ON grescore.application_id = application.id
    ";
    
    protected $queryGroupBy = "application.id";
    
    protected $queryOrderBy = 'users.lastname, users.firstname';
    
    /* 
    * Do not index the array elements with the application id.
    * The incremental, numerical index makes for more efficient looping
    * when merging/appending other view data.
    */ 
    protected $arrayKey = '';

}    
?>