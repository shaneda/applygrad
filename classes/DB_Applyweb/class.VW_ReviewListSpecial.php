<?php
/* 
* A view of grescore table data suitable for inclusion 
* in a 2D review list array.
*/

class VW_ReviewListSpecial extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    IFNULL(MAX(special_consideration), 0) AS special_consideration_requested,
    IFNULL(MAX(technical_screen), 0) AS technical_screen_requested,
    IFNULL(MAX(language_screen), 0) AS language_screen_requested
    ";
    
    protected $queryFrom = 
    "
    FROM application
    LEFT OUTER JOIN special_consideration ON application.id = special_consideration.application_id
    LEFT OUTER JOIN phone_screens ON application.id = phone_screens.application_id
    ";
    
    protected $queryGroupBy = 'application.id';

}    
?>
