<?php
/* 
* A view of grescore table data suitable for inclusion 
* in a 2D review list array.
*/

class VW_ReviewListGre extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    CONCAT(grescore.verbalscore, '/', grescore.verbalpercentile) 
        AS gre_verbal, 
    CONCAT(grescore.quantitativescore, ' - ', grescore.quantitativepercentile) 
        AS gre_quantitative,
    CONCAT(grescore.analyticalscore, ' - ', grescore.analyticalpercentile) 
        AS gre_analytical,
    CONCAT(grescore.analyticalwritingscore, ' - ', grescore.analyticalwritingpercentile) 
        AS gre_writing
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN grescore ON grescore.application_id = application.id
    ";

}    
?>
