<?php
/*
Class for period_type table data access.
*/

class DB_PeriodType extends DB_Applyweb
{

    function get($periodTypeId = NULL) {

        $query = "SELECT * FROM period_type"; 
        if ($periodTypeId) {
            $query .=  " WHERE period_type_id = " . $periodTypeId ;   
        }

        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }

}

?>