<?php
/* 
* Base class for basic Applyweb DB table access.
*/

class DB_Applyweb_Table2
{

    protected $DB_Applyweb2;
    protected $tableName;
    protected $fields = array();            // field info, 2D, indexed by field name
    protected $primaryKeyFields = array();  // field names, 1D, unindexed
    protected $autoIncrementId = FALSE;     // the primary key is an auto_increment field

    public function __construct($DB_Applyweb2, $tableName = NULL) {
    
        $this->DB_Applyweb2 = $DB_Applyweb2;
        
        if ($tableName) {
            
            $this->tableName = $tableName;
        }
        
        if ( isset($this->tableName) ) {
            
            $query = "SHOW FIELDS FROM " . $this->tableName;
            $this->fields = $this->DB_Applyweb2->handleSelectQuery($query, 'Field');            
        }
        
        foreach ($this->fields as $fieldName => $fieldArray) {
            
            if ($fieldArray['Key'] == 'PRI') {
                $this->primaryKeyFields[] = $fieldName;
            }
            
            if ($fieldArray['Extra'] == 'auto_increment') {
                $this->autoIncrementId = TRUE;
            }
        }
    }

    /*
    * Get a single record using its primary key value(s).  
    */
    protected function get( $primaryKeyValues = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }
         
        $keyWhere = $this->getKeyWhere($primaryKeyValues);
        if ( !$keyWhere ) {
            return FALSE;
        }
         
        $query = "SELECT * FROM " . $this->tableName;
        $query .= " WHERE " . $keyWhere;
        $query .= " LIMIT 1";
        
        $resultArray = $this->DB_Applyweb2->handleSelectQuery($query);
        
        return $resultArray;        
    }
    
    /*
    * Interface: find records using a foreign/alternate key.
    * Implementation depends on foreign keys.
    */
    protected function find($value = NULL, $key = NULL) {
        return NULL;
    }

    /*
    * Interface: insert/update a record. 
    * Implementation depends on primary key. 
    */
    protected function save( $record = array() ) {
        return NULL;
    }
    
    /*
    * Insert a single record.
    */
    protected function insert( $record = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }

        $i = 0;
        $fieldList = $valueList = "";
        foreach ($record as $field => $value) {
        
            if ( !array_key_exists($field, $this->fields) ) {
                return FALSE;    
            }
            
            if (!$value) {
                 continue;
            }
            
            if ($i > 0) {
                $fieldList .= ", ";
                $valueList .= ", ";
            }
            
            $fieldList .= $field;
            $valueList .= "'" . $this->DB_Applyweb2->escapeString($value) . "'";
            
            $i++;
        }
        
        $query = "INSERT INTO " . $this->tableName;     
        $query .= " (" . $fieldList . ") VALUES (" . $valueList . ")";
        
        $affectedRowCount = $this->DB_Applyweb2->handleInsertQuery($query);
        
        if ($this->autoIncrementId) {
            return $this->getLastInsertId();    
        } else {
            return $affectedRowCount;
        }    
    }

    /*
    * Update a single record using its primary key value(s). 
    */
    protected function update( $record = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }

        $keyWhere = $this->getKeyWhere($record);
        if ( !$keyWhere ) {
            return FALSE;
        }
        
        $i = 0;
        $setList = "";
        foreach ($record as $field => $value) {
        
            if ( in_array($field, $this->primaryKeyFields) ) {
                continue;
            }

            if ( !array_key_exists($field, $this->fields) ) {
                return FALSE;    
            }
            
            if ($i > 0) {
                $setList .= ", ";
            }
            $setList .= $field . " = ";
            if ($value === NULL) {
                $setList .= "NULL";
            } else {
                $setList .= "'" . $this->DB_Applyweb2->escapeString($value) . "'";   
            }
            
            $i++;
        }
    
        if ( !$setList ) {
            return FALSE;
        }

        $query = "UPDATE " . $this->tableName;
        $query .= " SET " . $setList;
        $query .= " WHERE " . $keyWhere;
        
        $affectedRowCount = $this->DB_Applyweb2->handleUpdateQuery($query);
        
        return $affectedRowCount; 
    }    

    /*
    * Delete a single record using its primary key value(s).  
    */
    protected function delete( $primaryKeyValues = array() ) {
    
        if ( !isset($this->tableName) ) {
            return FALSE;
        }
         
        $keyWhere = $this->getKeyWhere($primaryKeyValues);
        if ( !$keyWhere ) {
            return FALSE;
        }
         
        $query = "DELETE FROM " . $this->tableName;
        $query .= " WHERE " . $keyWhere;
        $query .= " LIMIT 1";
        
        $affectedRowCount = $this->DB_Applyweb2->handleDeleteQuery($query);
        
        return $affectedRowCount;        
    }

    protected function getKeyWhere($keyValues) {
        
        $i = 0;
        $keyWhere = "";
        foreach ($this->primaryKeyFields as $keyField) {
        
            if ( !isset($keyValues[$keyField]) ) {
                return FALSE;
            }
            
            if ($i > 0) {
                $keyWhere .= " AND ";
            }
            $keyWhere .= $keyField . " = ";
            $keyWhere .= "'" . $this->DB_Applyweb2->escapeString($keyValues[$keyField]) . "'"; 
            
            $i++;     
        }
        
        return $keyWhere;
    }

}
?>