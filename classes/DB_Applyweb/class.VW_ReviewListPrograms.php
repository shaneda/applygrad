<?php

/* 
* A view, suitable for inclusion in a 2D review list array,
* of program/interest data from the lu_application_programs 
* and lu_application_interest tables.
*/

class VW_ReviewListPrograms extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    lu_application_programs.application_id,
    
    /* programs */
    GROUP_CONCAT( DISTINCT
      CONCAT(
        degree.name,
        ' ',
        programs.linkword,
        ' ',
        fieldsofstudy.name,
        ' (',
        CAST(lu_application_programs.choice AS CHAR),
        ')'
        )
      ORDER BY lu_application_programs.choice
      SEPARATOR '|'
      ) AS programs,
    
    /* AOI */
    GROUP_CONCAT( DISTINCT
      CONCAT(
        interest.name,
        ' (',
        CAST( (lu_application_interest.choice+1) AS CHAR),
        ')'
        )
      ORDER BY lu_application_interest.choice
      SEPARATOR '|'
      ) AS areas_of_interest,
      
      /* promotion round  before Andy Pavlo fix
      IFNULL(promotion_status.promotion_status_round, 1) AS promotion_status_round,
      */
      IFNULL(promotion_status.round, 1) AS promotion_status_round,
      max(promotion_history.status_time) as status_time,
      
      
      
      /* decision(s) */
      GROUP_CONCAT( DISTINCT
        lu_application_programs.decision
        ORDER BY lu_application_programs.choice
        SEPARATOR '|'
      ) AS decision
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs AS lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id
    LEFT OUTER JOIN lu_application_interest ON lu_application_interest.app_program_id = lu_application_programs.id
    LEFT OUTER JOIN interest ON interest.id = lu_application_interest.interest_id 
    LEFT OUTER JOIN promotion_history on promotion_history.program_id = lu_application_programs.program_id and promotion_history.application_id =  application.id
    
    ";
    
    protected $queryGroupBy = "application.id, lu_programs_departments.department_id";

    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
}    
?>
