<?php

/*  
* View of application/user for finding applicants by name or email.
* Find method returns a 2D array indexed by application_id.
*/

class VW_RegistrationsApplicantIndex extends DB_Applyweb
{

    private $queryBase = 
        "
        SELECT DISTINCT
          users.id AS users_id,
          lu_users_usertypes.id AS lu_users_usertypes_id,
          users.guid,
          users.lastname,
          users.firstname,
          users.middlename,
          users.email
        FROM
          application
          INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
          INNER JOIN users ON lu_users_usertypes.user_id = users.id
          LEFT OUTER JOIN lu_application_programs ON application.id = lu_application_programs.application_id
          LEFT OUTER JOIN lu_programs_departments ON lu_application_programs.program_id = lu_programs_departments.program_id
          INNER JOIN student_decision 
            ON student_decision.application_id = application.id
            AND student_decision.program_id = lu_application_programs.program_id
            AND student_decision.decision = 'accept'
          LEFT OUTER JOIN lu_domain_department ON lu_programs_departments.department_id = lu_domain_department.department_id
        ";

    
    public function find($unit = NULL, $unitId = NULL, $periodId = NULL, $searchString = '') {        
        
        $query = $this->queryBase;
        
        if ($periodId) {
            $query .= " INNER JOIN period_application ON application.id = period_application.application_id  
                        WHERE lu_users_usertypes.usertype_id = 5";
            $query .= " AND period_application.period_id = " . $periodId;        
        } else {
            $query .= " WHERE lu_users_usertypes.usertype_id = 5";            
        }
        
        
        if ($unit && $unitId) {
            $unitField = 'lu_programs_departments.department_id';
            if ($unit == 'domain') {
                $unitField = 'lu_domain_department.domain_id';    
            }
            $query .= " AND " . $unitField . " = " . $unitId;
        }

        
        if ($searchString) {
            
            $replaceChars = array(",", "(", ")");
            $queryString = trim( str_replace($replaceChars, "", $searchString) );
            
            $queryStringArray = explode(' ', $queryString);
            if ( count($queryStringArray) == 2) {
                
                $query .= sprintf(" AND ( 
                            (users.lastname LIKE '%s' AND users.firstname LIKE '%s') 
                            OR (users.lastname LIKE '%s' AND users.firstname LIKE '%s')
                             )",
                            self::$mysqli->real_escape_string($queryStringArray[0]) . "%",
                            self::$mysqli->real_escape_string($queryStringArray[1]) . "%",
                            self::$mysqli->real_escape_string($queryStringArray[1]) . "%",
                            self::$mysqli->real_escape_string($queryStringArray[0]) . "%"
                            );                          
            } else {
            
                $query .= sprintf(" AND (users.lastname LIKE '%s' OR users.firstname LIKE '%s' OR users.email LIKE '%s'
                            OR ( MATCH(users.firstname, users.lastname) AGAINST('%s') ) )",
                            self::$mysqli->real_escape_string($queryString) . "%",
                            self::$mysqli->real_escape_string($queryString) . "%",
                            self::$mysqli->real_escape_string($queryString) . "%",
                            self::$mysqli->real_escape_string($queryString)
                            );
            }
        }
        
        $query .= " ORDER BY users.lastname";
        
        $arrayKey = "lu_users_usertypes_id";
        $resultsArray = $this->handleSelectQuery($query, $arrayKey);
        
        return $resultsArray;
        
    }
    
}    
?>
