<?php
/*
Class for period table data access.
*/

class DB_Period extends DB_Applyweb
{

    public function get($periodId = NULL) {

        $query = "SELECT period.* FROM period"; 
        if ($periodId) {
            $query .=  " WHERE period_id = " . $periodId ;   
        }
        $query .= " ORDER BY period_id DESC";
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    public function find($unitId, $parentPeriodId = NULL, $periodTypeId = NULL) {

        $query = "SELECT period.* FROM period WHERE unit_id = " . $unitId;
        if ($parentPeriodId) {
            $query .= " AND parent_period_id = " . $parentPeriodId;   
        }
        if ($periodTypeId) {
            $query .= " AND period_type_id = " . $periodTypeId;   
        }
        $query .= " ORDER BY period_id DESC";
        $resultArray = $this->handleSelectQuery($query);

        return $resultArray;           
    }

    public function save($recordArray) {

        $fieldCount = count($recordArray);
        $fieldList = $valueList = $updateList = "";
        $i = 1;
        foreach ($recordArray as $key => $value) {
            
            if (!$value) {

                if ($key == 'description' || $key == 'end_date'
                    || $key == 'parent_period_id') 
                {
                    $value = 'NULL';
                        
                } else {
                    
                    continue;
                }               
            }

            if ($i != 1) {
                $fieldList .= ", ";
                $valueList .= ", ";
                $updateList .= ", ";
            }
            $fieldList .= $key;
            if ( $key != 'period_id' && $key != 'unit_id' 
                && $key != 'period_type_id' && $key != 'parent_period_id' )
            {
                if ($value != 'NULL') {
                    $value = "'" . self::escapeString($value) . "'";    
                }
            }
            $valueList .= $value;
            $updateList .= $key . "=" . $value;            
            $i++;     
        }
        $query = "INSERT INTO period (" . $fieldList . ") VALUES (" . $valueList . ")
            ON DUPLICATE KEY UPDATE " . $updateList;
        $numAffectedRows = $this->handleInsertQuery($query);
        
        if ($recordArray['period_id']) {
            return $recordArray['period_id'];
        } else {
            return self::getLastInsertId();
        }    
    }

    public function insert($recordArray) {
        return $this->save($recordArray);    
    }
    
    public function update($recordArray) {
        return $this->save($recordArray);    
    }

    public function delete($periodId) {

        if (!$periodId) {
            return FALSE;
        }
        
        $query = "DELETE FROM period
                    WHERE period_id = " . $periodId ; 
        return $this->handleUpdateQuery($query);
        
    }
}

?>