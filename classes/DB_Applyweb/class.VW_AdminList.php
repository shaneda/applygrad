<?php
/* 
* A "synthetic" data access base class for assembling views
* of the database suitable for constructing a 2D (flat) review list array.
*/

class VW_AdminList extends DB_VW_Applyweb
{

    protected $arrayKey = 'application_id';
    
    // These tables may already be joined in $queryFrom.
    protected $joinApplicationPrograms = TRUE;
    protected $joinProgramsDepartments = TRUE;
    protected $joinDomainDepartment = TRUE;
    protected $joinPeriodApplication = TRUE;
    protected $joinUsersinst = TRUE;
    protected $joinGrescore = TRUE;
    protected $joinRecommend = TRUE;
    
    public function find($unit = NULL, $unitId = NULL, $admissionPeriodId = NULL,
                            $applicationId = NULL, $searchString = '', $submissionStatus = 'all',
                            $luUsersUsertypesId = NULL) {
                            
        // If applicationId (from the autocomplete, e.g.), get the data and return
        // NOTE: application.id must be selected as application_id in $querySelect
        if ($applicationId || is_numeric($searchString)) {
            if (!$applicationId) {
                $applicationId = intval($searchString);    
            }
            return $this->findByApplicationId($applicationId, $admissionPeriodId,
                                                $unit, $unitId);
        }
                                
        if ($luUsersUsertypesId) {
            return $this->findByLuuId($luUsersUsertypesId, $unit, $unitId, $admissionPeriodId);
        }
        
        // Otherwise.... 
                               
        if ($unit && $unitId) {
            $this->setUnit($unit, $unitId);
        }

        if ($admissionPeriodId) {
            $this->setAdmissionPeriod($admissionPeriodId, $unit);
        }
        
        if ($searchString) {
            
            $this->setSearchString($searchString);   
        
        } else {
        
            $this->setSubmissionStatus($submissionStatus);
 
        }
        
        if ($luUsersUsertypesId) {
            $this->findWheres[] = "application.user_id = " . $luUsersUsertypesId;    
        }
        
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);
        
        return $resultArray;
    } 

    protected function findByApplicationId($applicationId, 
                                            $admissionPeriodId = NULL, $unit = NULL) {
    
        // NOTE: application.id must be selected as application_id in $querySelect 
        unset($this->findWheres);

        if ($admissionPeriodId) {
            $this->setAdmissionPeriod($admissionPeriodId, $unit); 
        }        
        if ($unit && $unitId) {
            $this->setUnit($unit, $unitId);
        }        
        $this->findWheres[] = "application.id = " . $applicationId;
        
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);            
        return $resultArray;   
    }
    
    protected function findByLuuId($luUsersUsertypesId, 
        $unit = NULL, $unitId = NULL, $admissionPeriodId = NULL) {

        unset($this->findWheres);
        
        $this->findWheres[0] = "application.user_id = " . $luUsersUsertypesId;
        
        if ($unit && $unitId) {
            $this->setUnit($unit, $unitId);
        }

        if ($admissionPeriodId) {
            $this->setAdmissionPeriod($admissionPeriodId, $unit);
        }
        
        $query = $this->assembleFindQuery();
        $resultArray = $this->handleSelectQuery($query, $this->arrayKey);            
        return $resultArray;   
    }
    
    protected function setUnit($unit, $unitId) {
        
        if($this->joinApplicationPrograms && $this->joinProgramsDepartments) {
            $this->findJoins[] = "INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id"; 
            $this->findJoins[] = "INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id";            
        }
        switch ($unit) {
            
            case 'program':
            
                $this->findWheres[] = "lu_programs_departments.program_id = " . $unitId;
                break;    
            
            case 'domain':
            
                if($this->joinDomainDepartment) {
                    $this->findJoins[] = "INNER JOIN lu_domain_department on lu_programs_departments.department_id = lu_domain_department.department_id";    
                }
                $this->findWheres[] = "lu_domain_department.domain_id = " . $unitId;
                break;
            
            case 'department':
            default:
            
                $this->findWheres[] = "lu_programs_departments.department_id = " . $unitId;

        }        
    }

    protected function setSearchString($searchString) {
        
            $replaceChars = array(",", "(", ")");
            $searchString = trim( str_replace($replaceChars, "", $searchString) ); 
            $this->findWheres[] = sprintf(" (users.lastname LIKE '%s' OR users.firstname LIKE '%s' OR users.email LIKE '%s'
                                    OR ( MATCH(users.firstname, users.lastname) AGAINST('%s') ) )",
                                    self::$mysqli->real_escape_string($searchString) . "%",
                                    self::$mysqli->real_escape_string($searchString) . "%",
                                    self::$mysqli->real_escape_string($searchString) . "%",
                                    self::$mysqli->real_escape_string($searchString)
                                    );    
    }    
    
    protected function setAdmissionPeriod($admissionPeriodId, $unit) {
        
        // Using application_period table
        /*
        if ($unit == 'department') {           
            $this->findJoins[] = "LEFT OUTER JOIN application_periods ON lu_programs_departments.department_id = application_periods.department_id";
            $this->findWheres[] = "application_periods.application_period_id = " . $admissionPeriodId;
            $this->findWheres[] = "( CAST(application.submitted_date AS DATE) BETWEEN  
                                    CAST(start_date AS DATE) AND CAST(end_date AS DATE))";
        }
        */
        if ($this->joinPeriodApplication) {
            $this->findJoins[] = "INNER JOIN period_application ON application.id = period_application.application_id";  
        }
                
        // Compbio kluge 10/13/09: check to see if $admissionPeriodId is an array of period ids
        if ( is_array($admissionPeriodId) ) {
            $admissionPeriodIds = '(' . implode(',' , $admissionPeriodId) . ')';
            $this->findWheres[] = "period_application.period_id IN " . $admissionPeriodIds;    
        } else {
            $this->findWheres[] = "period_application.period_id = " . $admissionPeriodId;    
        }
        
        
             
    }
    
    protected function setSubmissionStatus($submissionStatus) {
    
        switch ($submissionStatus) {
                
            case 'unsubmitted':

                $this->findWheres[] = "application.submitted = 0";
                /*
                $this->findWheres[] = "(application.submitted = 0
                                        OR
                                        (application.paid = 0 
                                        AND application.waive = 0)                                        
                                        )";
                */
                break;
            
            case 'submitted':
            
                $this->findWheres[] = "application.submitted = 1";
                /*
                $this->findWheres[] = "(application.submitted = 1
                                        AND 
                                        (application.paid = 1 
                                        OR application.waive = 1)
                                        )";
                */
                break;                   
                
            default:
                // no submitted where
        }
    }       
}
?>