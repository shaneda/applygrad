<?php
/*
Class for programs_unit table data access.
*/

class DB_ProgramsUnit extends DB_Applyweb
{

    function get($programId = NULL, $unitId = NULL) {

        $query = "SELECT programs_unit.* FROM programs_unit"; 
        if ($programId) {
            $query .=  " WHERE programs_id = " . $programId;   
        }
        if ($programId && $unitId) {
            $query .= " AND";    
        }
        if ($unitId) {
            $query .=  " WHERE unit_id = " . $unitId;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    public function find($programId = NULL, $unitId = NULL) {
        return $this->get($programId, $unitId);        
    }

    
    public function save($recordArray) {
        
        $programsId = $recordArray['programs_id'];
        $unitId = $recordArray['unit_id'];
        
        $query = "INSERT INTO programs_unit VALUES 
                    (" . $programsId . ", " . $unitId . ")
                    ON DUPLICATE KEY UPDATE 
                    programs_id = " . $programsId . ", unit_id = " . $unitId;
                    
        $numAffectedRows = $this->handleInsertQuery($query);
        
        return $numAffectedRows;
    }

    
    public function delete($programId = NULL, $unitId = NULL) {
        
        if (!$programId && !$unitId) {
            return FALSE;
        }
        
        $query = "DELETE FROM programs_unit WHERE";
        if ($programId) {
            $query .= " programs_id = " . $programId;   
        }
        if ($programId && $unitId) {
            $query .= " AND";
        }
        if ($unitId) {
            $query .= " unit_id = " . $unitId;   
        }
        
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;
    }
    
}

?>