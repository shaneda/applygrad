<?php
/* 
* A view of ielts table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not return a record for every application.
*/

class VW_ReviewListIelts extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    ieltsscore.testdate AS ieltsscore_testdate,
    ieltsscore.listeningscore AS ieltsscore_listeningscore,
    ieltsscore.readingscore AS ieltsscore_readingscore,
    ieltsscore.writingscore AS ieltsscore_writingscore,
    ieltsscore.speakingscore AS ieltsscore_speakingscore,
    ieltsscore.overallscore AS ieltsscore_overallscore
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN ieltsscore ON ieltsscore.application_id = application.id
    ";

    protected $queryWhere = 
    "
    (
    (ieltsscore.listeningscore != 0 AND ieltsscore.listeningscore IS NOT NULL)
    OR (ieltsscore.readingscore != 0 AND ieltsscore.readingscore IS NOT NULL)
    OR (ieltsscore.writingscore != 0 AND ieltsscore.writingscore IS NOT NULL)
    OR (ieltsscore.speakingscore != 0 AND ieltsscore.speakingscore IS NOT NULL)
    OR (ieltsscore.overallscore != 0 AND ieltsscore.overallscore IS NOT NULL)
    )";
    
    // For students who have multiple tests, this should get the latest one.
    protected $queryOrderBy = 'application.id, ieltsscore.testdate';
    
}    
?>
