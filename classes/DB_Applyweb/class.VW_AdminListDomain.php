<?php
/* 
* Get a single domain id for each application 
*/

class VW_AdminListDomain extends VW_AdminList
{

    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    protected $joinDomainDepartment = FALSE;
    
    protected $querySelect = 
    "
    SELECT
    lu_application_programs.application_id,
    MIN(domain_id) AS application_domain
    ";
    
    /*
    protected $queryFrom = 
    "
    FROM lu_application_programs
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN lu_domain_department ON lu_programs_departments.department_id = lu_domain_department.department_id
    ";
    */
    
    protected $queryFrom = 
    "
    FROM lu_application_programs
    INNER JOIN application ON application.id = lu_application_programs.application_id
    INNER JOIN period_application AS pa1 ON application.id = pa1.application_id
    INNER JOIN period ON pa1.period_id = period.period_id 
    INNER JOIN domain_unit ON period.unit_id = domain_unit.unit_id
    /*
    INNER JOIN lu_programs_departments ON lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN lu_domain_department ON lu_programs_departments.department_id = lu_domain_department.department_id
    */
    ";

    
    protected $queryGroupBy = "application_id";
    
}    
?>
