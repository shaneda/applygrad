<?php
/*
Class for domain_unit table data access.
*/

class DB_DomainUnit extends DB_Applyweb
{

    public function get($domainId = NULL, $unitId = NULL) {

        $query = "SELECT domain_unit.* FROM domain_unit"; 
        if ($domainId) {
            $query .=  " WHERE domain_id = " . $domainId;   
        }
        if ($domainId && $unitId) {
            $query .= " AND";    
        }
        if ($unitId) {
            $query .=  " WHERE unit_id = " . $unitId;   
        }
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    public function find($domainId = NULL, $unitId = NULL) {
        return $this->get($domainId, $unitId);        
    }
    
    public function save($recordArray) {
        
        $domainId = $recordArray['domain_id'];
        $unitId = $recordArray['unit_id'];
        
        $query = "INSERT INTO domain_unit VALUES 
                    (" . $domainId . ", " . $unitId . ")
                    ON DUPLICATE KEY UPDATE 
                    domain_id = " . $domainId . ", unit_id = " . $unitId;
                    
        $numAffectedRows = $this->handleInsertQuery($query);
        
        return $numAffectedRows;
    }

    
    public function delete($domainId = NULL, $unitId = NULL) {
        
        if (!$domainId && !$unitId) {
            return FALSE;
        }
        
        $query = "DELETE FROM domain_unit WHERE";
        if ($domainId) {
            $query .= " domain_id = " . $domainId;   
        }
        if ($domainId && $unitId) {
            $query .= " AND";
        }
        if ($unitId) {
            $query .= " unit_id = " . $unitId;   
        }
        
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;
    }
    

}

?>