<?php
/*
* Class for lu_application_programs table data access.
*/

class DB_ApplicationProgram extends DB_Applyweb
{

    function get($luApplicationProgramsId = NULL, $withLookups = FALSE) {

        $query = "SELECT lu_application_programs.*"; 
        
        if ($withLookups) {
            
            $query .= ", CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS program";
                 
        }
        
        $query .= " FROM lu_application_programs";

        if ($withLookups) {
            
            $query .= " INNER JOIN programs ON lu_application_programs.program_id = programs.id
                        INNER JOIN degree ON degree.id = programs.degree_id
                        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id";             
        }
        
        if ($luApplicationProgramsId) {
            
            $query .=  " WHERE lu_application_programs.id = " . $luApplicationProgramsId;
               
        }
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray;  
    }

    
    public function find($applicationId = NULL, $programId = NULL, $withLookups = FALSE) {
    
        if (!$applicationId && !$programId) {
            
            return $this->get(NULL, $withLookups);
            
        }
        
        $query = "SELECT lu_application_programs.*";
        
        if ($withLookups) {
            
            $query .= ", CONCAT(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) AS program";
            
        }
        
        $query .= " FROM lu_application_programs";

        if ($withLookups) {
            
            $query .= " INNER JOIN programs ON lu_application_programs.program_id = programs.id
                        INNER JOIN degree ON degree.id = programs.degree_id
                        INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id";             
        }
        
        if ($applicationId) {
            
            $query .=  " WHERE lu_application_programs.application_id = " . $applicationId;
               
        }

        if ($programId) {

            if ($applicationId) {
                
                $query .=  " AND lu_application_programs.program_id = " . $programId;
                   
            } else {
                
                $query .=  " WHERE program_id = " . $programId;    
                
            }
                          
        }
        
        $resultArray = $this->handleSelectQuery($query);
        
        return $resultArray; 
    }


}
?>