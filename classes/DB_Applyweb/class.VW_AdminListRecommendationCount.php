<?php
/* 
* A view of recommend table data suitable for inclusion 
* in a 2D admin list array.
*/

/*
Test for formless industrial and academic recommendations.

SELECT DISTINCT recommend.id
FROM recommend
LEFT OUTER JOIN recommendforms
ON recommend.id = recommendforms.recommend_id
WHERE (recommendtype = 2
OR recommendtype = 3)
AND recommendforms.id IS NULL 
*/

class VW_AdminListRecommendationCount extends VW_AdminList
{

    protected $querySelect = 
    "
    SELECT recommend.application_id, 
    COUNT( NULLIF( recommend.rec_user_id, -(1) ) ) AS ct_recommendations_requested,
    COUNT(
        IF (recommend.submitted = 0, NULL,
            IF ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 
                IF ( ((recommendtype = 2 OR recommendtype = 3) AND recommendforms.recommend_id IS NULL), NULL, 1)
            ) 
        ) 
    ) AS ct_recommendations_submitted,
    COUNT(
        IF (recommend.submitted = 0, NULL,
            IF ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 1)
        ) 
    ) AS ct_recommendation_files_submitted,
    /*
    * ct_recommendation_files_forms_submitted is for use in MS-HCII cases, 
    * because a department test is unfeasible here.
    */
    COUNT(
        IF (recommend.submitted = 0, NULL,
            IF ( ( (datafile_id IS NULL) && (LENGTH(recommend.content) < 50) ), NULL, 
                IF ( (recommendforms.recommend_id IS NULL), NULL, 1)
            ) 
        ) 
    ) AS ct_recommendation_files_forms_submitted
    ";
    
    //protected $queryFrom = "FROM recommend";
    protected $queryFrom = "FROM recommend 
                            INNER JOIN application ON application.id = recommend.application_id
                            LEFT OUTER JOIN (SELECT DISTINCT recommend_id FROM recommendforms)
                            AS recommendforms ON recommend.id = recommendforms.recommend_id
                            ";
    
    
    protected $queryGroupBy = "application_id";

}    
?>
