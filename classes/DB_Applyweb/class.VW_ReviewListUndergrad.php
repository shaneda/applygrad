<?php
/* 
* A view of undergrad data from the usersinst table 
* suitable for inclusion in a 2D review list array.
*/

class VW_ReviewListUndergrad extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    institutes.name AS undergrad_institute_name,
    usersinst.major1,
    CONCAT(usersinst.gpa, '/', gpascales.name) AS gpa
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN usersinst ON usersinst.user_id = application.user_id
    LEFT OUTER JOIN degreesall ON degreesall.id = usersinst.degree
    LEFT OUTER JOIN institutes ON institutes.id = usersinst.institute_id
    LEFT OUTER JOIN gpascales ON gpascales.id = usersinst.gpa_scale
    ";
    
    protected $queryWhere = 'usersinst.educationtype = 1';

}    
?>