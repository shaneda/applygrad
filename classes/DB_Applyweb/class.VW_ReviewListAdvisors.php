<?php
/* 
* A view of lu_application_advisor table data for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not return a record for every application.
*/

class VW_ReviewListAdvisors extends VW_ReviewList
{

    protected $querySelect = 
    "
    SELECT
    application.id AS application_id,
    GROUP_CONCAT( DISTINCT application_advisors.name separator ', ') AS possible_advisors
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN (
        (
        SELECT application_id, name
        FROM lu_application_advisor
            WHERE name != ''
            AND name IS NOT NULL
        )
        UNION 
        (
        SELECT application_id, CONCAT( users.firstname, ' ', users.lastname ) AS name
        FROM lu_application_advisor
        INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = lu_application_advisor.advisor_user_id
        INNER JOIN users ON users.id = lu_users_usertypes.user_id
        ) 
    ) AS application_advisors ON application_advisors.application_id = lu_application_programs.application_id 
    ";
    
    protected $queryGroupBy = "application.id";

    // This table is already joined in the base query.
    protected $joinApplicationPrograms = FALSE;

}    
?>