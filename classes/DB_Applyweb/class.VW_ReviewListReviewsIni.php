<?php
/* 
* A view of review table data suitable for inclusion 
* in a 2D review list array.
* 
* NOTE: The find method query uses an inner join 
* instead of a left join with the application table, 
* so it does not always return a record for every application.
*/

class VW_ReviewListReviewsIni extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT application.id AS application_id,
    
    /* committee reviewers */
    GROUP_CONCAT( DISTINCT
        users.lastname ORDER BY users.lastname
        SEPARATOR '|'    
    ) AS committee_reviewers,

    /* all rounds overall ratings */
    GROUP_CONCAT( DISTINCT
        IF (
            review.overall_rating IS NOT NULL,
            IF (
                (review.round = 2 || (review.round = 1 && review.reviewer_id NOT IN
                    (SELECT reviewer_id FROM review AS review2 
                    WHERE review2.round = 2 
                    && review2.application_id = review.application_id
                    && review2.reviewer_id = review.reviewer_id))
                ),                
                CONCAT(users.lastname, ': ', 
                    CAST( review.overall_rating AS CHAR)
                    ),
                NULL
            ),
            NULL
        ) ORDER BY users.lastname 
        SEPARATOR '|'
    ) AS all_overall_ratings,
    
    /* all rounds overall average */
    ROUND( AVG(
        IF (
            review.overall_rating IS NOT NULL,
            IF (
                (review.round = 2 || (review.round = 1 && review.reviewer_id NOT IN
                    (SELECT reviewer_id FROM review AS review2 
                    WHERE review2.round = 2 
                    && review2.application_id = review.application_id
                    && review2.reviewer_id = review.reviewer_id))
                ),
                ROUND(review.overall_rating, 2),
                NULL
            ),
            NULL
        )
    ), 2) as all_overall_average,
    
    /* decision */
    GROUP_CONCAT( DISTINCT
        lu_application_programs.decision
        SEPARATOR '|'
    ) AS decision,
    
    GROUP_CONCAT( DISTINCT 
        CASE lu_application_programs.admission_status when 1 then 
            CONCAT(
                if(degree.name is not null, degree.name,''), ' ',
                if(programs.linkword is not null, programs.linkword,''), ' ',
                if(fieldsofstudy.name is not null, fieldsofstudy.name,''), 
                ' (wait)'
            ) 
        WHEN 2 THEN
            CONCAT(
                degree.name, ' ', 
                if(programs.linkword is not null, programs.linkword,''),
                ' ', 
                fieldsofstudy.name
            )
        END
        SEPARATOR '|'
    ) AS admit_to,
    
    '' AS admit_to_decision
    ";
    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_application_programs ON lu_application_programs.application_id = application.id
    INNER JOIN lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
    INNER JOIN programs ON programs.id = lu_application_programs.program_id
    INNER JOIN degree ON degree.id = programs.degree_id
    INNER JOIN fieldsofstudy ON fieldsofstudy.id = programs.fieldofstudy_id

    /* reviewers */
    INNER JOIN review_ini AS review ON review.application_id = application.id
    INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = review.reviewer_id
    INNER JOIN lu_user_department ON lu_users_usertypes.id = lu_user_department.user_id
    INNER JOIN users ON lu_users_usertypes.user_id = users.id
    
    /* admitted program */ 
    /*
    LEFT OUTER JOIN
    (
        SELECT
        application_decision.application_id,
        application_decision.program_id,
        GROUP_CONCAT( DISTINCT
            CASE application_decision.admission_status when 1 then
                CONCAT(
                    if(admit_degree.name is not null, admit_degree.name,''), ' ',
                    if(admit_programs.linkword is not null, admit_programs.linkword,''), ' ',
                    if(admit_fieldsofstudy.name is not null, admit_fieldsofstudy.name,''),
                    ' (wait)'
                )
            WHEN 2 THEN
                CONCAT(
                    admit_degree.name, ' ',
                    if(admit_programs.linkword is not null, admit_programs.linkword,''),
                    ' ',
                    admit_fieldsofstudy.name
                )
            END
            SEPARATOR '|'
        ) AS admit_to_decision 
        FROM application_decision
        LEFT OUTER JOIN programs AS admit_programs
          ON admit_programs.id = application_decision.admission_program_id
        LEFT OUTER JOIN degree AS admit_degree
          ON admit_degree.id = admit_programs.degree_id
        LEFT OUTER JOIN fieldsofstudy AS admit_fieldsofstudy
          ON admit_fieldsofstudy.id = admit_programs.fieldofstudy_id
        GROUP BY application_decision.application_id
    ) AS application_decision
        ON lu_application_programs.application_id = application_decision.application_id
        AND lu_application_programs.program_id = application_decision.program_id
    */
    ";
    
    protected $queryWhere = 'review.department_id = lu_programs_departments.department_id';
    
    protected $queryGroupBy = 'application.id, lu_programs_departments.department_id';
    
    // These tables are already joind in the base query.
    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    
}    
?>
