<?php
/* 
* Get a single domain id for each application 
*/

class VW_AdminListPeriod extends VW_AdminList
{

    protected $joinApplicationPrograms = FALSE;
    protected $joinProgramsDepartments = FALSE;
    protected $joinDomainDepartment = FALSE;
    protected $joinPeriodApplication = FALSE;
    
    protected $querySelect = 
    "
    SELECT
    period_application.application_id,
    period_application.period_id,
    period_umbrella.umbrella_name,
    unit.unit_name_short
    ";

    protected $queryFrom = 
    "
    FROM period_application
    INNER JOIN application ON application.id = period_application.application_id
    INNER JOIN period_umbrella ON period_umbrella.period_id = period_application.period_id
    INNER JOIN period ON period_umbrella.period_id = period.period_id
    INNER JOIN unit ON period.unit_id = unit.unit_id
    ";

    protected $queryGroupBy = "application_id";
    
}    
?>