<?php
/* 
* A view, suitable for inclusion in a 2D review list array,
* of the application data from the primary (non-lookup) tables 
* that can be efficiently inner joined.
* 
* This is the "foundational" view with which the other
* view data will be merged/appended. 
*/

class VW_ApplicantGroupListUserInfo extends VW_ReviewList
{
    
    protected $querySelect = 
    "
    SELECT DISTINCT
    application.id AS application_id,
    users_info.gender,
    countries_cit.iso_code AS cit_country_iso_code,
    countries_cit.name AS cit_country,
    institutes.name AS undergrad_institute_name
    ";

    
    protected $queryFrom = 
    "
    FROM application
    INNER JOIN lu_users_usertypes ON application.user_id = lu_users_usertypes.id
    INNER JOIN users_info ON lu_users_usertypes.id = users_info.user_id
    LEFT OUTER JOIN visatypes ON users_info.visastatus = visatypes.id
    LEFT OUTER JOIN countries AS countries_cit ON users_info.cit_country = countries_cit.id
    LEFT OUTER JOIN usersinst ON usersinst.user_id = application.user_id 
        AND usersinst.application_id = application.id
        AND usersinst.educationtype = 1
    LEFT OUTER JOIN institutes ON institutes.id = usersinst.institute_id
    ";    

    protected $queryGroupBy = "application.id";
    
}    
?>