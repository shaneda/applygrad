<?php
/*
Class for payment_item table operations
*/

class DB_PaymentItem extends DB_Applyweb
{
    
    public function get($paymentItemId) {
        
        $query = "SELECT * FROM payment_item WHERE payment_item_id = " . $paymentItemId;
        $resultArray = $this->handleSelectQuery($query, 'payment_item_id');
        return $resultArray;
    }
    
    public function find($paymentId) {
        
        $query = "SELECT * FROM payment_item WHERE payment_id = " . $paymentId;
        $resultArray = $this->handleSelectQuery($query, 'payment_item_id');
        return $resultArray;        
    }
    
    public function insert($paymentId, $programId, $paymentItemAmount) {
        
        $query = "INSERT INTO payment_item (payment_id, program_id, payment_item_amount) 
                    VALUES (" . $paymentId . ", " . $programId . ", " . $paymentItemAmount . ")";
        $numAffectedRows = $this->handleInsertQuery($query);
        $paymentItemId = self::$mysqli->insert_id;
        return $paymentItemId;        
    }

    public function update($paymentItemId, $paymentId, $programId, $paymentItemAmount) {
        $query = "UPDATE payment_item SET
                    payment_id = " . $paymentId . ",
                    program_id = " . $programId . ",  
                    payment_item_amount = " . $paymentItemAmount . "
                    WHERE payment_item_id = " . $paymentItemId;
        $numAffectedRows = $this->handleInsertQuery($query);
        return $numAffectedRows;         
    }
    
    public function delete($paymentId, $paymentItemId = NULL) {
    
        if (!$paymentId && !$paymentItemId) {
            return FALSE;
        }

        $query = "DELETE FROM payment_item";
        if ($paymentItemId) {
            $query .= " WHERE payment_item_id = " . $paymentItemId;    
        } elseif ($paymentId) {
            $query .= " WHERE payment_id = " . $paymentId;
        } else {
            $query .= " WHERE payment_item_id = -1";    
        }
        $numAffectedRows =  $this->handleUpdateQuery($query);
        return $numAffectedRows;
    }
    
}
?>