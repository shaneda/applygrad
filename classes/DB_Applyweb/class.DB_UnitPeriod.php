<?php
/*
Class for unit_period table data access.
*/

class DB_UnitPeriod extends DB_Applyweb
{

      public function get($periodId = NULL, $unitId = NULL) {

        $query = "SELECT unit_period.* FROM unit_period"; 
        if ($periodId) {
            $query .=  " WHERE period_id = " . $periodId ;   
        }
        if ($unitId) {
            if (!$periodId) {
                $query .=  " WHERE";    
            } else {
                $query .=  " AND";    
            }
            $query .=  " unit_id = " . $unitId;
        }
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }
    
    public function find($periodId = NULL, $unitId = NULL) {

        $resultArray = $this->get($periodId, $unitId);

        return $resultArray;           
    }

    public function save($recordArray) {

        $periodId = intval($recordArray['period_id']);
        $unitId = intval($recordArray['unit_id']);
        $valueList =  $unitId . ", " . $periodId;
        $updateList = "unit_id=" . $unitId . ", period_id=" . $periodId;
        
        $query = "INSERT INTO unit_period VALUES (" . $valueList . ")
            ON DUPLICATE KEY UPDATE " . $updateList;
        $numAffectedRows = $this->handleInsertQuery($query);
        
        return $numAffectedRows;
    }
    
    public function insert($recordArray) {
        return $this->save($recordArray);    
    }
    
    public function update($recordArray) {
        return $this->save($recordArray);    
    }
    
    public function delete($periodId = NULL, $unitId  = NULL) {
        
        if (!$periodId && !$unitId) {
            return FALSE;
        }

        $query = "DELETE FROM unit_period ";
        if ($periodId) {
            $query .=  " WHERE period_id = " . $periodId ;   
        }
        if ($unitId) {
            if (!$periodId) {
                $query .=  " WHERE";    
            } else {
                $query .=  " AND";    
            }
            $query .=  " unit_id = " . $unitId;   
        }
        
        $numAffectedRows = $this->handleUpdateQuery($query);
        
        return $numAffectedRows;
    }

}

?>