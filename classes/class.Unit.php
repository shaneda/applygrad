<?php
/*
* Required class files:
*   classes/DB_Applyweb/class.DB_Applyweb.php
*   classes/DB_Applyweb/class.DB_Unit.php
*   classes/DB_Applyweb/class.DB_DomainUnit.php
*   classes/DB_Applyweb/class.DB_DepartmentUnit.php
*   classes/DB_Applyweb/class.DB_ProgramsUnit.php
*/

class Unit
{    

    private $id;
    private $name;
    private $nameShort;
    private $description;
    private $url;
    private $oracleString;
    private $systemEmail;
    private $ccEmail;
    private $parentUnitId;

    private $childUnits = array();          // unit objects
    private $unitPeriods = array();         // 'umbrella' period objects
    private $unitDomainIds = array();       // Corresponding old domain id(s)
    private $unitDepartmentId;              // Corresponding old department id
    private $unitProgramIds = array();      // Corresponding old program id
    
    public function __construct($unitId = NULL, $unitType = NULL) {       
        $this->id = $unitId;
        if ($unitId) {
            $this->loadFromDb();            
        }
    }

    public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getNameShort() {
        return $this->nameShort;
    }

    public function setNameShort($nameShort) {
        $this->nameShort = $nameShort;
    }
   
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getOracleString() {
        return $this->oracleString;
    }

    public function setOracleString($oracleString) {
        $this->oracleString = $oracleString;
    }
   
    public function getSystemEmail() {
        return $this->systemEmail;
    }

    public function setSystemEmail($systemEmail) {
        $this->systemEmail = $systemEmail;
    }

    public function getCcEmail() {
        return $this->ccEmail;
    }

    public function setCcEmail($ccEmail) {
        $this->ccEmail = $ccEmail;
    }
   
    public function getParentUnitId() {
        return $this->parentUnitId;
    }
    
    public function setParentUnitId($parentUnitId) {
        $this->parentUnitId = $parentUnitId;
    }
    
    public function getParentUnit() {
        $parentUnit = NULL;
        if ($this->parentUnitId) {
            $parentUnit = new Unit($this->parentUnitId);     
        }
        return $parentUnit;    
    }
    
    public function getChildUnits() {
        return $this->childUnits;    
    }
    
    public function setChildUnits($childUnits) {
        $this->childUnits = $childUnits; 
    }
    
    public function addChildUnit($unit) {
        $this->childUnits[] = $unit;    
    }
    
    public function getDescendantUnitIds() {
        $descendantUnitIds = array();
        foreach($this->childUnits as $childUnit) {
            $childUnitId = $childUnit->getId();
            $descendantUnitIds[] = $childUnitId;
            $descendantUnitIds = array_merge( $descendantUnitIds, $childUnit->getDescendantUnitIds() );    
        }
        return $descendantUnitIds; 
    }
    
    public function getPeriods() {
        return $this->unitPeriods;
    }
    
    public function addPeriod($period) {
        $this->unitPeriods[] = $period;    
    }
   
    public function getUnitDomainIds() {
        return $this->unitDomainIds;
    }

    public function setUnitDomainIds($unitDomainIds) {
        $this->unitDomainIds = $unitDomainIds;
    }
    
    public function getUnitProgramIds() {
        return $this->unitProgramIds;
    }
    
    public function setUnitProgramIds($unitProgramIds) {
        $this->unitProgramIds = $unitProgramIds;
    }
   
    public function isProgram() {
        if ( count($this->childUnits) == 0 ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /*
    * Get the unit periods that are active at the level specified by the periodTypeId parameter
    * (default is 2 => 'application submission').   
    */
    public function getActivePeriods( $periodTypeId = 2, $includeChildren = TRUE, 
                                        $includeParent = TRUE, $searchProgramIds = NULL) {
                                            
        $activePeriods = array();
        
        foreach ($this->unitPeriods as $period)  {            
            if ($period->getPeriodTypeId() == $periodTypeId && $period->getStatus() == 'active') {
                    $activePeriods[] = $period;
                    continue;  
            }
            foreach ($period->getChildPeriods() as $subperiod) {
                if ($subperiod->getPeriodTypeId() == $periodTypeId 
                        && $subperiod->getStatus() == 'active') 
                {
                    $activePeriods[] = $period;
                    break;   
                } 
            }   
        }
        
        if ($includeChildren) {
            $childrenActivePeriods = array();
            foreach ($this->childUnits as $childUnit) {
                $childActivePeriods = $childUnit->getActivePeriods($periodTypeId, FALSE, FALSE);
                $childrenActivePeriods = array_merge( $childrenActivePeriods, $childActivePeriods);
            }
            $activePeriods = array_merge( $activePeriods, $childrenActivePeriods );
        } 
        
        if ($includeParent) {            
            // Get program ids of unit of interest to be passed through recursion.
            if (!$searchProgramIds) {
                $searchProgramIds = array_keys( $this->getPrograms() );
            }

            $parentUnit = $this->getParentUnit();
            if ($parentUnit) {
                $parentActivePeriods = $parentUnit->getActivePeriods($periodTypeId, FALSE, TRUE, $searchProgramIds);
                foreach ($parentActivePeriods as $parentPeriod) {                    
                    $periodProgramIds = array_keys( $parentPeriod->getPrograms() );
                    foreach ($searchProgramIds as $programId) {                       
                        if ( in_array($programId, $periodProgramIds) ) {                  
                            if ( !in_array($parentPeriod, $activePeriods) ) {
                                $activePeriods[] = $parentPeriod;    
                            }         
                        }
                    }
                }   
            }               
        }
            
        return $activePeriods;
    }
    
    public function getPrograms( $includeChildren = TRUE ) {       
        $programs = array();       
        if (!$this->childUnits) {
            $programs[$this->id] = $this->name;
        } else {
            if ($includeChildren) {
                foreach ($this->childUnits as $childUnit) {
                    $programs += $childUnit->getPrograms();
                }    
            }           
        }
        return $programs;    
    }
    
    public function getActivePrograms( $includeChildren = TRUE, $includeParent = TRUE ) {
        
        $activePrograms = array();
        $activePeriods = $this->getActivePeriods($includeChildren, $includeParent);
        foreach ($activePeriods as $activePeriod) {
            $periodPrograms = $activePeriod->getPrograms();
            $activePrograms += $periodPrograms;
        }
        return $activePrograms;
    }
    
    private function loadFromDb() {
        
        $DB_Unit = new DB_Unit();
        
        $unitArray = $DB_Unit->get($this->id);
        foreach ($unitArray as $unitRecord) {
            $this->name = $unitRecord['unit_name'];
            $this->nameShort = $unitRecord['unit_name_short'];
            $this->description = $unitRecord['unit_description'];
            $this->url = $unitRecord['unit_url'];
            $this->oracleString = $unitRecord['unit_oracle_string'];
            $this->systemEmail = $unitRecord['unit_system_email'];
            $this->ccEmail = $unitRecord['unit_cc_email'];
            if ($unitRecord['parent_unit_id']) {
                $this->parentUnitId = $unitRecord['parent_unit_id'];
            }
        }

        /*
        $childUnitQuery = "SELECT * FROM unit WHERE parent_unit_id = " . $this->id;
        $childUnitArray = $DB_Unit->handleSelectQuery($childUnitQuery);
        */
        $childUnitArray = $DB_Unit->find($this->id);
        foreach ($childUnitArray as $childUnitRecord) {
            $childUnitId = $childUnitRecord['unit_id'];
            $this->childUnits[] = new Unit($childUnitId);    
        }
        
        
        $periodQuery = "SELECT period.* FROM period, unit_period 
                        WHERE period.parent_period_id IS NULL
                        AND unit_period.unit_id = " . $this->id . "
                        AND period.period_id = unit_period.period_id
                        ORDER BY start_date DESC"; 
        $periodArray = $DB_Unit->handleSelectQuery($periodQuery);
        foreach ($periodArray as $periodRecord) {
            $periodId = $periodRecord['period_id'];
            $this->unitPeriods[] = new Period($periodId);    
        }
        
        $domainQuery = "SELECT domain.id
                        FROM domain_unit
                        INNER JOIN domain ON domain_unit.domain_id = domain.id
                        WHERE domain_unit.unit_id = " . $this->id;
        $domainArray = $DB_Unit->handleSelectQuery($domainQuery);
        foreach ($domainArray as $domainRecord) {
            $this->unitDomainIds[] = $domainRecord['id'];    
        }
        
        $programQuery = "SELECT programs.id
                            FROM programs_unit
                            INNER JOIN programs ON programs_unit.programs_id = programs.id
                            WHERE programs_unit.unit_id = " . $this->id;
        $programArray = $DB_Unit->handleSelectQuery($programQuery);
        foreach ($programArray as $programRecord) {
            $this->unitProgramIds[] = $programRecord['id'];    
        }   
    }
     
    public function getUnitRecord() {
       $unitRecord = array(
            'unit_id' => $this->id,
            'unit_name' => $this->name,
            'unit_name_short' => $this->nameShort,
            'unit_description' => $this->description,
            'unit_url' => $this->url,
            'unit_oracle_string' => $this->oracleString,
            'unit_system_email' => $this->systemEmail,
            'unit_cc_email' => $this->ccEmail,
            'parent_unit_id' => $this->parentUnitId
        );
        return $unitRecord;        
    }
    

    public function save() {
        
        $db_Unit = new DB_Unit();
        $unitRecord = $this->getUnitRecord();
        $this->id = $db_Unit->save($unitRecord);

        $db_DomainUnit = new DB_DomainUnit();
        $db_DomainUnit->delete(NULL, $this->id);
        foreach ($this->unitDomainIds as $domainId) {
            $db_DomainUnit->save( array('domain_id' => $domainId, 'unit_id' => $this->id) );
        }
        
        $db_ProgramsUnit = new DB_ProgramsUnit();
        $db_ProgramsUnit->delete(NULL, $this->id);
        foreach ($this->unitProgramIds as $programId) {
            $db_ProgramsUnit->save( array('programs_id' => $programId, 'unit_id' => $this->id) );
        }
        
        return TRUE;
    }


}
?>