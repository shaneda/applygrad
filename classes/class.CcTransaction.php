<?php

class CcTransaction
{
    private $ccId;
    private $applicationId;
    private $paymentId;
    private $storeNumber;
    private $date;
    private $time;
    private $cardholderNameLast;
    private $cardholderNameFirst;
    private $summaryRecords;
    private $detailRecords;  
  
    public function __construct($ccId) {
        $this->ccId = $ccId;
        $this->loadFromDb();
    }
    
    public function __get($property) {

        // http://us2.php.net/manual/en/language.oop5.overloading.php
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $property .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }
    
    private function loadFromDb() {
        
        // Get transaction record
        $DB_CcTransaction = new DB_CcTransaction();
        $transactionRecordArray = $DB_CcTransaction->get($this->ccId);
        if (isset($transactionRecordArray[0])) {
            $transactionRecord = $transactionRecordArray[0];
            $this->applicationId = $transactionRecord['application_id'];
            $this->paymentId = $transactionRecord['payment_id'];
            $this->storeNumber = $transactionRecord['store_number'];
            $this->cardholderNameLast = $transactionRecord['cardholder_name_last'];
            $this->cardholderNameFirst = $transactionRecord['cardholder_name_first'];
        }
                
        // Get summary records                    
        $DB_CcTransactionSummary = new DB_CcTransactionSummary();
        $this->summaryRecords = $DB_CcTransactionSummary->find($this->ccId); 
        
        // Get detail records
        $DB_CcTransactionDetail = new DB_CcTransactionDetail();
        $this->detailRecords = $DB_CcTransactionDetail->find($this->ccId);
    }
    
}

?>