<?php
/*
* Class for admission period business logic.
*/

class Period
{    

    private $DB_Period;
    private $DB_PeriodType;
    private $DB_PeriodProgram;
    private $DB_PeriodApplication;
    private $DB_ProgramGroup; 
    
    private $periodBase;
    
    private $childPeriodIds = array();
    private $programIds = array();
    private $applicationIds = array();
    private $programGroupIds = array();
     
    public function __construct($periodId = NULL, $parentPeriodId = NULL, $periodType = NULL) {

        $this->DB_Period = new DB_Period();
        $this->DB_PeriodType = new DB_PeriodType();
        $this->DB_PeriodProgram = new DB_PeriodProgram();
        $this->DB_PeriodApplication = new DB_PeriodApplication();
        $this->DB_ProgramGroup = new DB_ProgramGroup();
         
        if ($periodId) {

            $this->periodBase = new PeriodBase($periodId);                                  
            
            if ( $this->isUmbrella() ) {
                
                $this->periodBase = new UmbrellaPeriod($this->periodBase);
                
            } else {
                
                if (!$parentPeriodId) {
                    $parentPeriodId = $this->periodBase->getParentPeriodId();
                }
                
                $this->periodBase = new SubPeriod($this->periodBase, $parentPeriodId, $periodType);    
            }
            
            $this->loadFromDb();
            
        } else {
            
            $this->periodBase = new PeriodBase();
            
            if ($periodType && $periodType != 'umbrella') {
                $this->periodBase = new SubPeriod($this->periodBase, $parentPeriodId, $periodType);    
            } else {
                $this->periodBase = new UmbrellaPeriod($this->periodBase);        
            } 
        }
    }

    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ( $prefix == 'get' ) {               
            if ( property_exists($this, $property) ) {
                if ( isset($this->$property) ) {
                    return $this->$property;    
                } else {
                    return NULL;    
                }
            } else { 
                if (isset( $arguments[0]) ) {
                    return $this->periodBase->$method($arguments[0]);    
                } else {
                    return $this->periodBase->$method();    
                }
            }   
        }
        
        if ( $prefix == 'set') {
            if ( property_exists($this, $property) ) {
                $this->$property = $arguments[0];
                return TRUE;
            } else {
                return $this->periodBase->$method($arguments[0]);
            }
        }
        
        return FALSE;
    }
    
    public function isUmbrella() {
        
        if ( $this->periodBase->getPeriodTypeId() == 1 ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getPeriodType() {
        
        $periodTypeId = $this->getPeriodTypeId();
        $periodTypeArray = $this->DB_PeriodType->get($periodTypeId);
        
        if (isset($periodTypeArray[0])) {
            return $periodTypeArray[0]['period_type'];
        } else {
            return NULL;
        }        
    }
    
    public function getParentPeriod() {
        
        $parentPeriodId = $this->periodBase->getParentPeriodId();
        
        if ($parentPeriodId) {
            return new Period($parentPeriodId);     
        } else {
            return NULL;
        } 
    }
    
    public function getChildPeriods($periodTypeId = NULL) {
        
        $childPeriods = array();
        foreach ($this->childPeriodIds as $periodId) {
            
            $childPeriod = new Period($periodId);
            if ($periodTypeId) {
                $childPeriodTypeId = $childPeriod->getPeriodTypeId();
                if ($childPeriodTypeId != $periodTypeId) {
                    continue;
                }
            }
            $childPeriods[] = $childPeriod;   
        }
        
        return $childPeriods;    
    }

    public function getProgramGroups() {

        $programGroupIds = $this->getProgramGroupIds(); 
        $programGroups = array();
        foreach ($programGroupIds as $programGroupId) {
            $programGroups[$programGroupId] = new ProgramGroup($programGroupId);    
        }
        
        return $programGroups;
    }

    /*
    * legacy behavior 
    */
    public function getPrograms() {

        $programIds = $this->getProgramIds(); 
        $programs = array();
        foreach ($programIds as $programId) {
            $program = new Unit($programId);    
            $programs[$programId] = $program->getName();
        }
        
        return $programs;
    }

   
    public function addApplicationId($applicationId) {
    
        if ( !in_array($applicationId, $this->applicationIds) ) {
            
            $this->DB_PeriodApplication->save($this->getId(), $applicationId);
            
            return $this->loadApplicationIds();             
        }
        
        return TRUE;        
    }
    
     
    public function getStatus() {
        
        $currentTimestamp = time();
        
        $startDate = $this->periodBase->getStartDate(); 
        $startTimestamp = strtotime($startDate); 
        
        $endDate = $this->periodBase->getEndDate();
        if ($endDate == NULL) {
            $endTimestamp = NULL;    
        } else {
            $endTimestamp = strtotime($endDate);    
        }
        
        if ($startTimestamp > $currentTimestamp) {
            return 'future';
        }
        
        if ($endTimestamp != NULL && $endTimestamp < $currentTimestamp) {
            return 'closed';    
        }
        
        if ($endTimestamp != NULL && $endTimestamp >= $currentTimestamp) {
            return 'active';    
        }
        
        if ($endTimestamp == NULL) {
            $parentPeriod = $this->getParentPeriod();
            if ($parentPeriod) {
                $parentPeriodEndDate = $parentPeriod->getEndDate();
                $parentPeriodEndTimestamp = strtotime($parentPeriodEndDate);
                if ( $parentPeriodEndTimestamp == NULL
                    || $parentPeriodEndTimestamp > $currentTimestamp ) {
                    return 'active';
                } else {
                    return 'closed';        
                }
            } else {
                // This is an umbrella period with no end date specified.
                return 'active';
            } 
        }
        
        // Return FALSE by default (which indicates an error condition).
        return FALSE;         
    }
    
    private function loadFromDb() {
    
        $this->loadChildPeriodIds();
        $this->loadProgramGroupIds();
        $this->loadProgramIds();
        $this->loadApplicationIds();    

        return TRUE;
    }

    private function loadChildPeriodIds() {
        
        $unitId = $this->getUnitId();
        $periodId = $this->getId(); 
        
        $childPeriodArray = $this->DB_Period->find($unitId, $periodId);
        
        foreach ($childPeriodArray as $childPeriodRecord) {
            $this->childPeriodIds[] = $childPeriodRecord['period_id'];    
        }
        
        return TRUE;    
    }

     private function loadProgramGroupIds() {
    
        $this->programGroupIds = array();
        
        $programGroupArray = $this->DB_ProgramGroup->find( $this->getId() );
        
        foreach ($programGroupArray as $programGroupRecord) {
            $this->programGroupIds[] = $programGroupRecord['program_group_id'];
        }
        
        return TRUE;     
    }

     private function loadProgramIds() {
    
        $this->programIds = array();
        
        $periodId = $this->getId();
        $programsArray = $this->DB_PeriodProgram->find($periodId);
        
        foreach ($programsArray as $programRecord) {
            $this->programIds[] = $programRecord['unit_id'];
        }
        
        return TRUE;     
    }

    private function loadApplicationIds() {
    
        $this->applicationIds = array();
        
        $DB_PeriodApplication = new DB_PeriodApplication();
        $periodApplications = $DB_PeriodApplication->get( $this->getId() );
        
        foreach ($periodApplications as $periodApplication) {
            $this->applicationIds[] = $periodApplication['application_id'];
        }        
    
        return TRUE;
    }
    
    
    public function importValues($valueArray) {
        
        $this->periodBase->importValues($valueArray);
        
        return TRUE;      
    }    
     
    public function exportValues() {
        
        $values = $this->periodBase->exportValues();
        
        return $values;      
    }

    public function save() {
        
        $this->periodBase->save();
        
        $unitId = $this->getUnitId();
        $periodId = $this->getId();
                   
        $this->DB_PeriodProgram->delete($periodId);
        foreach ($this->programIds as $programUnitId) {
            $this->DB_PeriodProgram->save( 
                array('period_id' => $periodId, 'unit_id' => $programUnitId) 
            );
        }

        return TRUE;
    }

    public function delete() {

        if ( count($this->applicationIds) > 0 ) {
            return FALSE;
        }
        
        $periodId = $this->getId();
        
        if ( count($this->programIds) > 0 ) {            
            $this->DB_PeriodProgram->delete($periodId);
        }
        
        foreach ($this->programGroupIds as $programGroupId) {
            $programGroup = new ProgramGroup($programGroupId);
            $programGroup->delete();
        }
        
        foreach ($this->childPeriodIds as $childPeriodId) {
            $childPeriod = new Period($childPeriodId);
            $childPeriod->delete();
        }
        
        $this->periodBase->delete();
        
        return TRUE;
    }
    
}
?>