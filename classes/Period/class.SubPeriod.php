<?php
/*
* Class for admission subperiod business logic.
*/

class SubPeriod
{
    
    private $periodBase;

    public function __construct($periodBase, $parentPeriodId, $periodType = NULL) {       
        
        $this->periodBase = $periodBase;

        if ( !$this->periodBase->getId() ) {

            if ($parentPeriodId) {
                $this->periodBase->setParentPeriodId($parentPeriodId);    
            }
            
            switch ($periodType) {

                case 3:
                case 'application editing':
                
                    $periodTypeId = 3;
                    break;
            
                case 4:
                case 'placeout review':
                
                    $periodTypeId = 4;
                    break;
                
                case 7:
                case 'application viewing':
                
                    $periodTypeId = 7;
                    break;

                case 2:
                case 'application submission':
                default:
                    $periodTypeId = 2;
            }

            $this->periodBase->setPeriodTypeId($periodTypeId);            
        }
    }

    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ( $prefix == 'get') {
            if (isset( $arguments[0]) ) {
                return $this->periodBase->$method($arguments[0]);    
            } else {
                return $this->periodBase->$method();    
            }
        }
        
        if ( $prefix == 'set') {
            $this->periodBase->$method($arguments[0]);
            return TRUE;
        }
        
        return FALSE;
    }
    
    public function isUmbrella() {
        return FALSE;
    }

    public function getParentPeriod() {
        
        $parentPeriodId = $this->periodBase->getParentPeriodId();
        
        if ($parentPeriodId) {
            return new Period($parentPeriodId);     
        } else {
            return NULL;
        }  
    }

    public function getProgramIds() {
        
        $parentPeriod = $this->getParentPeriod();

        return $parentPeriod->getProgramIds();    
    }
    
    public function getApplications() {
        
        $parentPeriod = $this->periodBase->getParentPeriod();

        return $parentPeriod->getApplicationIds();   
    }

    public function importValues($valueArray) {
        
        $this->periodBase->importValues($valueArray);
        
        return TRUE;      
    }    
     
    public function exportValues() {
        
        return $values = $this->periodBase->exportValues();      
    }
    
    public function save() {
        
        return $this->periodBase->save();
    }
    
    public function delete() {
        
        return $this->periodBase->delete(); 
    }

}
?>