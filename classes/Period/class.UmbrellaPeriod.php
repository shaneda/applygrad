<?php
/*
* Class for admission umbrella period business logic.
*/

class UmbrellaPeriod
{
    
    private $DB_PeriodUmbrella;
    private $DB_UnitPeriod;
    
    private $periodBase;
    
    private $name;
    private $admissionTerm;
    private $admissionYear;
    private $higherFeeDate;
    private $lastPaymentDate;  

    public function __construct($periodBase) {       
        
        $this->DB_PeriodUmbrella = new DB_PeriodUmbrella();
        $this->DB_UnitPeriod = new DB_UnitPeriod();
        
        $this->periodBase = $periodBase;
        if ( $this->periodBase->getId() ) {
            $this->loadFromDb();    
        } else {
            $this->periodBase->setPeriodTypeId(1);    
        }
    }
    
    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ( $prefix == 'get') {
            
            if ( property_exists($this, $property) ) {
                if ( isset($this->$property) ) {
                    return $this->$property;    
                } else {
                    return NULL;    
                }   
            } else {
                if (isset( $arguments[0]) ) {
                    return $this->periodBase->$method($arguments[0]);    
                } else {
                    return $this->periodBase->$method();    
                }
            }
        }
        
        if ( $prefix == 'set') {

            if ( property_exists($this, $property) ) {
                $this->$property = $arguments[0];
                return TRUE;
            } else {
                return $this->periodBase->$method($arguments[0]);
            }
        }
        
        return FALSE;
    }

    public function isUmbrella() {
        return TRUE;
    }
    
    public function getParentPeriod() {
        return NULL;
    }
    
    public function useHigherFee() {
        
        $currentTimestamp = time();
        $higherFeeTimestamp = strtotime($this->higherFeeDate);

        if ($this->higherFeeDate == NULL) {
            return FALSE;
        } else {
            if ($currentTimestamp > $higherFeeTimestamp) {
                return TRUE;
            }
        }  
        
        return FALSE;
    }
    
    public function beforeLastPaymentDate() {
        
        $currentTimestamp = time();
        $lastPaymentTimestamp = strtotime($this->lastPaymentDate);
        
        if ($this->lastPaymentDate == NULL) {
            return TRUE;
        } else {
            if ($currentTimestamp <= $lastPaymentTimestamp) {
                return TRUE;
            }
        }  
        
        return FALSE;
    }
    
    private function loadFromDb() {

        $umbrellaArray = $this->DB_PeriodUmbrella->get( $this->getId() );
        foreach ($umbrellaArray as $umbrellaRecord) {           
            $this->admissionTerm = $umbrellaRecord['admission_term'];
            $this->admissionYear = $umbrellaRecord['admission_year'];
            $this->higherFeeDate = $umbrellaRecord['higher_fee_date'];
            $this->lastPaymentDate = $umbrellaRecord['last_payment_date'];
            $this->name = $umbrellaRecord['umbrella_name'];
        }

        return TRUE;
    }

    public function importValues($valueArray) {

        $fields = array(
            'admission_term' => 'admissionTerm',
            'admission_year' => 'admissionYear',
            'higher_fee_date' => 'higherFeeDate',
            'last_payment_date' => 'lastPaymentDate',
            'umbrella_name' => 'name'
        );
        
        foreach ($fields as $key => $field) {
            if ( isset($valueArray[$key]) ) {
                $this->$field = $valueArray[$key];
            }
        }        
 
        $this->periodBase->importValues($valueArray);
        
        return TRUE;      
    }    
     
    public function exportValues() {
        
        $values = $this->periodBase->exportValues();
        $values['admission_term'] = $this->admissionTerm;
        $values['admission_year'] = $this->admissionYear;
        $values['higher_fee_date'] = $this->higherFeeDate;
        $values['umbrella_name'] = $this->name;
        $values['last_payment_date'] = $this->lastPaymentDate;
        
        return $values;      
    }
    
    public function save() {
        
        $this->periodBase->save();

        $periodId = $this->getId();

        $this->DB_PeriodUmbrella->save( 
            array(
                'period_id' => $periodId, 
                'admission_term' => $this->admissionTerm,
                'admission_year' => $this->admissionYear,
                'higher_fee_date' => $this->higherFeeDate,
                'umbrella_name' => $this->name,
                'last_payment_date' => $this->lastPaymentDate
            ) 
        );
        
        $this->DB_UnitPeriod->save( 
            array('period_id' => $periodId, 'unit_id' => $this->getUnitId()) 
        );
               
        return TRUE;
    }
    

    public function delete() {
    
        $unitId = $this->getUnitId();
        $periodId = $this->getId();
        
        $periodUmbrellaDeleted = $this->DB_PeriodUmbrella->delete($periodId);
        if (!$periodUmbrellaDeleted) {
            return FALSE;
        }
        
        $unitPeriodDeleted = $this->DB_UnitPeriod->delete($periodId, $unitId);
        if (!$unitPeriodDeleted) {
            return FALSE;
        }
        
        return $this->periodBase->delete();
    }    
    
}
?>