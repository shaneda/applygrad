<?php
/*
* Required class files:
*   classes/DB_Applyweb/class.DB_Applyweb.php
*   classes/DB_Applyweb/class.DB_Period.php
*/

class PeriodBase
{    
    
    private $DB_Period;
    
    private $id;
    private $unitId;
    private $periodTypeId;
    private $description;
    private $startDate;
    private $endDate;
    private $parentPeriodId;

    public function __construct($periodId = NULL) {        
        
        $this->DB_Period = new DB_Period();
        
        $this->id = $periodId;
        if ($periodId) {
            $this->loadFromDb();            
        }
    }

    public function __isset($name) {
        return isset($this->$name);
    }

    // Dynamic getters/setters.
    public function __call($method, $arguments) {
        
        $prefix = strtolower(substr($method, 0, 3));
        $property = strtolower(substr($method, 3, 1)) . substr($method, 4);
        
        if (empty($prefix) || empty($property)) {
            return FALSE;
        }
        
        if ($prefix == 'get' && property_exists($this, $property)) {
            if ( $property == 'EndDate' || $property == 'StartDate' ) {
                if (isset( $arguments[0]) ) {
                    return $this->$method($arguments[0]);    
                } else {
                    return $this->$method();   
                }
            } elseif ( isset($this->$property) ) {
                return $this->$property;    
            } else {
                return NULL;
            }
        }
        
        if ( $prefix == 'set' && property_exists($this, $property) ) {
            $this->$property = $arguments[0];
            return TRUE;
        }
        
        return FALSE;
    }

    public function getStartDate($format = NULL) {
        return $this->formatDate($this->startDate, $format);
    }

    public function getEndDate($format = NULL) {
        return $this->formatDate($this->endDate, $format);
    }
    
    private function loadFromDb() {
        
        $periodArray = $this->DB_Period->get($this->id);
        foreach ($periodArray as $periodRecord) {           
            $this->unitId = $periodRecord['unit_id'];
            $this->periodTypeId = $periodRecord['period_type_id'];
            $this->description = $periodRecord['description'];
            $this->startDate = $periodRecord['start_date'];
            $this->endDate = $periodRecord['end_date'];
            if ($periodRecord['parent_period_id']) {
                $this->parentPeriodId = $periodRecord['parent_period_id'];
            }
        }

        return TRUE;
    }
    
    public function importValues($valueArray) {
        
        $fields = array(
            'period_id' => 'id',
            'unit_id' => 'unitId',
            'period_type_id' => 'periodTypeId',
            'description' => 'description',
            'start_date' => 'startDate',
            'end_date' => 'endDate',
            'parent_period_id' => 'parentPeriodId'
        );
        
        foreach ($fields as $key => $field) {
            if ( isset($valueArray[$key]) ) {
                $this->$field = $valueArray[$key];
            }
        }
        
        return TRUE;
    }
     
    public function exportValues() {
       
        $values = array(
            'period_id' => $this->id,
            'unit_id' => $this->unitId,
            'period_type_id' => $this->periodTypeId,
            'description' => $this->description,
            'start_date' => $this->startDate,
            'end_date' => $this->endDate,
            'parent_period_id' => $this->parentPeriodId
        );
        
        return $values;        
    }
    
    public function save() {
        
        $periodRecord = $this->exportValues();
        $this->id = $this->DB_Period->save($periodRecord);

        return TRUE;
    }

    public function delete() {
        
        $recordDeleted = $this->DB_Period->delete($this->id);
        
        if ($recordDeleted) {
            
            return TRUE;
        
        } else {
            
            return FALSE;    
        }
    }

    private function formatDate($date, $format = NULL) { 
        
        if ($format) {
            $timestamp = strtotime($date);
            return date($format, $timestamp);    
        }
        
        // Return the unformatted date by default.
        return $date;
    }
    

    /*
    * Legacy method(s).
    */
    public function getPeriodRecord() {        
        return $this->exportValues();
    }
    
}
?>