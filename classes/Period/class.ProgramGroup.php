<?php
class ProgramGroup
{

    private $DB_ProgramGroup;
    private $DB_ProgramGroupProgram;
    private $DB_ProgramGroupGroupType;
    private $DB_ProgramGroupRole;
    
    private $id;
    private $periodId;
    private $unitId;
    private $name;
    private $nameShort;
    private $description;
    private $groupTypeIds = array();
    private $programIds = array();      // array of unit_ids
    private $roles = array();
    
    public function __construct($programGroupId = NULL, $periodId = NULL) {       
        
        $this->DB_ProgramGroup = new DB_ProgramGroup();
        $this->DB_ProgramGroupGroupType = new DB_ProgramGroupGroupType();
        $this->DB_ProgramGroupProgram = new DB_ProgramGroupProgram();
        $this->DB_ProgramGroupRole = new DB_ProgramGroupRole();
        
        $this->id = $programGroupId;
        $this->periodId = $periodId;
        
        if ($programGroupId) {
            $this->loadFromDb();
        }
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function setId($programGroupId) {
        $this->id = $programGroupId;
        return TRUE;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function setName($name) {
        $this->name = $name;
        return TRUE;
    }
     
    public function getNameShort() {
        return $this->nameShort;
    }
    
    public function setNameShort($name) {
        $this->nameShort = $name;
        return TRUE;
    }

    public function getPeriodId() {
        return $this->periodId;
    }

    public function setPeriodId($periodId) {
        $this->periodId = $periodId;
    }
    
    public function getUnitId() {
        return $this->unitId;
    }
    
    public function setUnitId($unitId) {
        $this->unitId = $unitId;
        return TRUE;
    }
    
    public function getGroupTypeIds() {
        return $this->groupTypeIds;
    }
    
    public function getProgramIds() {
        return $this->programIds;
    }
    
    public function setProgramIds( $unitIds = array() ) {
        $this->programIds = $unitIds;
        return TRUE;
    }

    public function isApplyGroup() {
        if ( in_array(1, $this->groupTypeIds) ) {
             return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /*
    public function isAdminGroup() {
        if ( in_array(2, $this->groupTypeIds) ) {
             return TRUE;
        } else {
            return FALSE;
        }
    }
    */
    
    public function isReviewGroup() {
        if ( in_array(2, $this->groupTypeIds) ) {
             return TRUE;
        } else {
            return FALSE;
        }
    }
    
    private function loadFromDb() {
    
        $progamGroupArray = $this->DB_ProgramGroup->get($this->id);    
        $progamGroupTypeArray = $this->DB_ProgramGroupGroupType->find($this->id);
        $programGroupProgramArray = $this->DB_ProgramGroupProgram->find($this->id);
        $programGroupRoleArray = $this->DB_ProgramGroupRole->find($this->id);
    
        if ( isset($progamGroupArray[0]) ) {
        
            $this->periodId = $progamGroupArray[0]['period_id'];
            $this->unitId = $progamGroupArray[0]['unit_id'];
            $this->name = $progamGroupArray[0]['program_group_name'];
            $this->nameShort = $progamGroupArray[0]['program_group_name_short'];
            $this->description = $progamGroupArray[0]['program_group_description'];
            foreach ($progamGroupTypeArray as $progamGroupTypeRecord) {
                $this->groupTypeIds[] = $progamGroupTypeRecord['program_group_type_id'];    
            }
            foreach ($programGroupProgramArray as $programGroupProgramRecord) {
                $this->programIds[] = $programGroupProgramRecord['unit_id'];    
            }
            foreach ($programGroupRoleArray as $programGroupRoleRecord) {
                $roleRecord = array(
                    'users_id' => $programGroupRoleRecord['users_id'],
                    'role_id' => $programGroupRoleRecord['role_id'] 
                    );
                $this->roles[] = $roleRecord;    
            }
            
            return TRUE; 
            
        } else {
        
            return FALSE;
        }        
    }

    
    public function importValues($valueArray) {
        
        $fields = array(
            'program_group_id' => 'id',
            'period_id' => 'periodId',
            'unit_id' => 'unitId',
            'program_group_name' => 'name',
            'program_group_name_short' => 'nameShort',
            'program_group_description' => 'description',
            'program_group_type_id' => 'groupTypeIds',
            'program_id' => 'programIds',
            'roles' => 'roles'
        );
        
        foreach ($fields as $key => $field) {
            if ( isset($valueArray[$key]) ) {
                $this->$field = $valueArray[$key];
            }
        }
        
        return TRUE;
    }
    
     
    public function exportValues() {
       
        $values = array(
            'program_group_id' => $this->id,
            'period_id' => $this->periodId,
            'unit_id' => $this->unitId,
            'program_group_name' => $this->name,
            'program_group_name_short' => $this->nameShort,
            'program_group_description' => $this->description,
            'program_group_type_id' => $this->groupTypeIds,
            'program_id' => $this->programIds,
            'roles' => $this->roles
        );
        
        return $values;        
    }

    
    public function save() {
        
        $programGroupValues = $this->exportValues();
        $roleRecords = array_pop($programGroupValues);
        $programIds = array_pop($programGroupValues);
        $programGroupTypeIds = array_pop($programGroupValues);
        
        $this->id = $this->DB_ProgramGroup->save($programGroupValues);
        
        $unit = new Unit($this->unitId);
        $unitPrograms = $unit->getPrograms();
        $unitProgramIds = array_keys($unitPrograms);
        
        $this->DB_ProgramGroupProgram->delete($this->id);
        foreach ($programIds as $unitId) {
            
            if ( !in_array($unitId, $unitProgramIds) ) {
                continue;
            }
            
            $groupProgramValues = array(
                'program_group_id' => $this->id,
                'unit_id' => $unitId
            );
            $this->DB_ProgramGroupProgram->save($groupProgramValues);
        }

        $this->DB_ProgramGroupGroupType->delete($this->id);
        foreach ($programGroupTypeIds as $groupTypeId) {
            $groupTypeValues = array(
                'program_group_id' => $this->id,
                'program_group_type_id' => $groupTypeId
            );
            $this->DB_ProgramGroupGroupType->save($groupTypeValues);
        }
        
        $this->DB_ProgramGroupRole->delete($this->id);
        foreach ($roleRecords as $roleRecord) {
            $roleRecord['program_group_id'] = $this->id;
            $this->DB_ProgramGroupRole->save($roleRecord);    
        }
        
        
        return TRUE;
    }
    
    public function delete() {
        
        $this->DB_ProgramGroupRole->delete($this->id);
        
        foreach ($this->unitIds as $unitId) {
            $this->DB_ProgramGroupProgram->delete($this->id, $unitId);
        }
        
        $this->DB_ProgramGroup->delete($this->id);
        
        return TRUE;
    }

}
?>