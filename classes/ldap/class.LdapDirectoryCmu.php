<?php
/*
* require_once 'class.LdapDirectory.php';
*/

class LdapDirectoryCmu extends LdapDirectory
{
    const PERSON_CMU_BASE_DN = 'ou=person,dc=cmu,dc=edu';
    const ACCOUNT_ANDREW_BASE_DN = 'ou=account,dc=andrew,dc=cmu,dc=edu';
    const ANDREWPERSON_ANDREW_BASE_DN = 'ou=ANDREWPERSON,dc=ANDREW,dc=CMU,dc=EDU';

    public function __construct($hostname) {
        parent::__construct($hostname);
    }
    
    /*
    * Searches of account>andrew>cmu tree returning cmuRegularAccount entries 
    */
    public function getAccountAndrewByCmuandrewid($cmuandrewid) {
        $dn = 'uid=' . $cmuandrewid . ',' . self::ACCOUNT_ANDREW_BASE_DN;
        $filter = 'objectclass=cmuRegularAccount';
        $accountAndrewData = $this->search($dn, $filter);
        return $accountAndrewData;   
    }
    
    public function getAccountAndrewByGuid($guid) {
        $dn = self::ACCOUNT_ANDREW_BASE_DN;
        $filter = 'owner=guid=' . $guid . ',' . self::PERSON_CMU_BASE_DN;  
        $accountAndrewData = $this->search($dn, $filter);
        return $accountAndrewData;   
    }

    /*
    * Searches of andrewperson>andrew>cmu tree returning cmuAccountPerson entries 
    */
    public function getAndrewpersonAndrewByCmuandrewid($cmuandrewid) {
        $dn = 'uid=' . $cmuandrewid . ',' . self::ANDREWPERSON_ANDREW_BASE_DN;
        $filter = 'objectclass=cmuAccountPerson';
        $andrewpersonAndrewData = $this->search($dn, $filter);
        return $andrewpersonAndrewData;
    }
    
    public function getAndrewpersonAndrewByGuid($guid) {
        $dn = self::ANDREWPERSON_ANDREW_BASE_DN;
        $filter = 'owner=guid=' . $guid . ',' . self::PERSON_CMU_BASE_DN;
        $andrewpersonAndrewData = $this->search($dn, $filter);
        return $andrewpersonAndrewData;
    }
    
    public function getAndrewpersonAndrewByCmueceid($cmueceid) {
        $dn = self::ANDREWPERSON_ANDREW_BASE_DN;
        $filter = '(&(objectclass=cmuAccountPerson)(cmueceid=' . $cmueceid . '))';
        $andrewpersonAndrewData = $this->search($dn, $filter);
        return $andrewpersonAndrewData;
    }

    public function getAndrewpersonAndrewByCmuqatarid($cmuqatarid) {
        $dn = self::ANDREWPERSON_ANDREW_BASE_DN;
        $filter = '(&(objectclass=cmuAccountPerson)(cmuqatarid=' . $cmuqatarid . '))';
        $andrewpersonAndrewData = $this->search($dn, $filter);
        return $andrewpersonAndrewData;
    }    

    public function getAndrewpersonAndrewByCmueduid($cmueduid) {
        $dn = self::ANDREWPERSON_ANDREW_BASE_DN;
        $filter = '(&(objectclass=cmuAccountPerson)(cmueduid=' . $cmueduid . '))';
        $andrewpersonAndrewData = $this->search($dn, $filter);
        return $andrewpersonAndrewData;
    }
    
    /*
    * Searches of person>cmu tree returning cmuPerson entries 
    */    
    public function getPersonCmuByGuid($guid) {
        $baseDn = 'guid=' . $guid . ',' . self::PERSON_CMU_BASE_DN;
        $filter = 'objectclass=cmuPerson';
        $personCmuData = $this->search($baseDn, $filter);
        return $personCmuData;        
    }
        
    public function getPersonCmuByCmuandrewid($cmuandrewid) {
        $baseDn = self::PERSON_CMU_BASE_DN;
        $filter = '(&(objectclass=cmuPerson)(cmuandrewid=' . $cmuandrewid . '))';
        $personCmuData = $this->search($baseDn, $filter);
        return $personCmuData;        
    }
        
    public function getPersonCmuByCmueceid($cmueceid) {
        $dn = self::PERSON_CMU_BASE_DN;
        $filter = '(&(objectclass=cmuPerson)(cmueceid=' . $cmueceid . '))';
        $personCmuData = $this->search($dn, $filter);
        return $personCmuData;
    }
    
    public function getPersonCmuByCmuqatarid($cmuqatarid) {
        $dn = self::PERSON_CMU_BASE_DN;
        $filter = '(&(objectclass=cmuPerson)(cmuqatarid=' . $cmuqatarid . '))';
        $personCmuData = $this->search($dn, $filter);
        return $personCmuData;
    }
    
    public function getPersonCmuByCmueduid($cmueduid) {
        $dn = self::PERSON_CMU_BASE_DN;
        $filter = '(&(objectclass=cmuPerson)(cmueduid=' . $cmueduid . '))';
        $personCmuData = $this->search($dn, $filter);
        return $personCmuData;
    }

    /*
    * DEPRECATED: the cmucsid attribute is still part of the 
    * cmuPerson Objectclass schema, but campus seem to have
    * stopped using it as of 2/8/2012. 
    */
    public function getPersonCmuByCmucsid($cmucsid) {
        $baseDn = self::PERSON_CMU_BASE_DN;
        $filter = 'cmucsid=' . $cmucsid;
        $personCmuData = $this->search($baseDn, $filter);
        return $personCmuData;        
    }
    
}
?>