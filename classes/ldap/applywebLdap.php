<?php
/*
 * Use e-mail addresses from the applyweb users table to match users with their LDAP data. 
 * Record the data for matches in the scsUser, scsUserWebIso, and usersScsUser tables.
*/

//DebugBreak();
// Include the config and db connection files.
include '../../inc/config.php';

// Include the ldap classes.
require 'class.LdapDirectory.php';
require 'class.LdapDirectoryCmu.php';
require 'class.LdapDirectoryCmuCs.php';
require 'class.LdapDirectoryCmuAndrew.php';
require 'class.LdapPerson.php';
require 'class.LdapPeople.php';
 
// Connect to the db.
mysql_connect($db_host,$db_username,$db_password) or errordb("Couldn't connect to URA database.");
mysql_select_db($db) or errordb("Couldn't Select Database");
 
// Get all the "internal" users (i.e., everyone but students, recommenderes, and unassigned).
$usersQuery = 'SELECT DISTINCT(users.id) AS users_id, users.email, users.firstname, users.lastname 
    FROM lu_users_usertypes
    LEFT JOIN users ON lu_users_usertypes.user_id = users.id
    WHERE lu_users_usertypes.usertype_id NOT IN (-1, 5, 6)
    AND users.id IS NOT NULL
    ORDER BY users.id';
        
$usersQueryResult = mysql_query($usersQuery);

$ldapPeople = new LdapPeople();

$matchedGuid = array();         // indexed by ldap.guid
$matchedCmucsid = array();      // indexed by ldap.csid
$matchedNoUid = array();        // indexed by users.id

$unmatched = array();           // indexed by lu_users_usertypes.id 
$unmatchedNoHost = array();     // indexed by lu_users_usertypes.id

while ( $row = mysql_fetch_assoc($usersQueryResult) ) {

    $usersId = $row['users_id'];
    $luuId = $row['lu_users_usertypes_id'];
    
    $emailArray = explode('@', $row['email']);
    $usernameArray = explode('+', $emailArray[0]);
    $username = $usernameArray[0];
    $reminder = $usernameArray[1];
    $host = $emailArray[1];
    $searchEmail = $username . '@' . $host;

    // Start with a csAccount search.
    // $filter = '(|(maillocaladdress=' . $searchEmail . ')(mailroutingaddress=' . $searchEmail . '))';
    $filter = '(maillocaladdress=' . $searchEmail . ')';
    $ldapEntry = $ldapPeople->csLdapDirectory->search('ou=account,dc=cs,dc=cmu,dc=edu', $filter);
   
    if ($ldapEntry['count'] > 0) {
        
        // Add them to the matched array(s).
        $webIso = $ldapEntry[0]['uid'][0] . '@CS.CMU.EDU';
        $identityArray = $ldapPeople->getByWebIso($webIso);
        
        if ( isset($identityArray['guid']) ) {
            $guid = $identityArray['guid'];
            if ( !isset($matchedGuid[$guid]['ldap']) ) {
                $matchedGuid[$guid]['ldap'] = $identityArray;    
            }
            $matchedGuid[$guid]['applyweb'][] = $row; 
        } else {
            if ( isset($identityArray['cmucsid']) ) {
                $cmucsid = $identityArray['cmucsid'];
                if ( !isset($matchedCmucsid[$cmucsid]['ldap']) ) {
                    $matchedCmucsid[$cmucsid]['ldap'] = $identityArray;    
                }
                $matchedCmucsid[$cmucsid]['applyweb'][] = $row;
            } else {         
                $matchedNoUid[$luuId]['ldap'] = $identityArray;
                $matchedNoUid[$luuId]['applyweb'] = $row;    
            }
        }
           
    } else {
        
        if ($host == 'andrew.cmu.edu') {
            
            // Do an accountAndrew search on the cmuandrewid.
            $ldapEntry = 
                $ldapPeople->andrewLdapDirectory->getAccountAndrewByCmuandrewid($username);
            
        } elseif ($host == 'ece.cmu.edu') {
            
            // Do an andrewperson search on the cmueceid.
            $ldapEntry = 
                $ldapPeople->andrewLdapDirectory->getAndrewpersonAndrewByCmueceid($username);            
  
        } elseif ($host == 'qatar.cmu.edu') {
            
            // Do an andrewperson search on the cmueceid.
            $ldapEntry = 
                $ldapPeople->andrewLdapDirectory->getAndrewpersonAndrewByCmuqatarid($username);            
  
        } else {
            
            // Do an andrewperson search on the original e-mail address.
            $ldapEntry = 
                $ldapPeople->andrewLdapDirectory->search('ou=andrewperson,dc=andrew,dc=cmu,dc=edu', 'mail=' . $searchEmail ); 
            
        }
        
        if ($ldapEntry['count'] > 0) {
            
            $webIso = $ldapEntry[0]['uid'][0] . '@ANDREW.CMU.EDU';
            $identityArray = $ldapPeople->getByWebIso($webIso);
            
            if ( isset($identityArray['guid']) ) {
                $guid = $identityArray['guid'];
                if ( !isset($matchedGuid[$guid]['ldap']) ) {
                    $matchedGuid[$guid]['ldap'] = $identityArray;    
                }
                $matchedGuid[$guid]['applyweb'][] = $row; 
            } else {
                if ( isset($identityArray['cmucsid']) ) {
                    $cmucsid = $identityArray['cmucsid'];
                    if ( !isset($matchedCmucsid[$cmucsid]['ldap']) ) {
                        $matchedCmucsid[$cmucsid]['ldap'] = $identityArray;    
                    }
                    $matchedCmucsid[$cmucsid]['applyweb'][] = $row;
                } else {         
                    $matchedNoUid[$luuId]['ldap'] = $identityArray;
                    $matchedNoUid[$luuId]['applyweb'] = $row;    
                }       
            }
            
        } else {
            if ($host == '') {
                $unmatchedNoHost[] = $row;     
            } else {
                $unmatched[] = $row;    
            } 
        }

    }

}
mysql_free_result($result);

echo mysql_num_rows($usersQueryResult) . ' users records found<br/>';
echo count($matchedNoUid) . ' matched no guid or cmucsid<br/>';
echo count($unmatched) . ' unmatched<br/>';
echo count($unmatchedNoHost) . ' unmatchable: no host<br/><br/>'; 

/*
echo 'Matched records:<br/>'; 
print_r($matchedGuid);
print_r($matchedCmucsid);
print_r($matchedNoUid);
*/

// Add the records
foreach ($matchedGuid as $matchedRecord) {
    addToDb($matchedRecord);
}

foreach ($matchedCmucsid as $matchedRecord) {
    addToDb($matchedRecord);
}

//echo 'Unmatched records:<br/>';
//print_r($unmatched);
echo implode( ',' , array_keys($unmatched[0]) ) . "\n";
foreach($unmatched as $unmatchedRecord) {
    $recordString =  implode(',', $unmatchedRecord);
    echo $recordString . "\n";   
}

//print_r($unmatchedNoHost);
foreach($unmatchedNoHost as $unmatchedRecord) {
    $recordString =  implode(',', $unmatchedRecord);
    echo $recordString . "\n";   
}



function addToDb($matchedRecord) {

    // Get the LDAP info for the users records.
    $guid = $matchedRecord['ldap']['guid'];
    $cmucsid = $matchedRecord['ldap']['cmucsid'];
    $cmuandrewid = $matchedRecord['ldap']['cmuandrewid']; 
    $cmueceid = $matchedRecord['ldap']['cmueceid']; 
    $cmuqatarid = $matchedRecord['ldap']['cmuqatarid']; 
    
    /*
    $cn = $matchedRecord['ldap']['cn'];
    $cnArray = split(' ', $cn);
    $cnCount = count($cnArray);
    $cnFirstName = $cnArray[0];
    $cnLastName = $cnArray[$cnCount - 1];
    
    $sn = $matchedRecord['ldap']['sn'];
    $givenname = $matchedRecord['ldap']['givenname'];
    
    if ($givenname) {
        $firstName = $givenname;
    } else {
        $firstName = $cnFirstName;
    }
    
    if ($sn) {
        $lastName = $sn;
    } else {
        $lastName = $cnLastName;
    }
    */
    
    // 
    foreach($matchedRecord['applyweb'] as $applywebRecord) {
        
        $usersId = $applywebRecord['users_id'];
        
        // Add an ScsUser record for the corresponding user record.
        $query = sprintf("INSERT INTO scs_user (users_id, cmu_guid, cs_id, andrew_id, ece_id, qatar_id)
                VALUES (%d, '%s', '%s', '%s', '%s', '%s')", 
                $usersId, $guid, $cmucsid, $cmuandrewid, $cmueceid, $cmuqatarid);
        echo $query . '<br/>';
        $result = mysql_query($query);
        
        if (!$result) {
            // A record for that user is already in the database.
            // Skip to the next user.
            continue;
        }
        
        $scsUserId = mysql_insert_id();

        // Add the records for easy WebISO lookup.
        if ($cmucsid) {
            $webIso = $cmucsid . '@CS.CMU.EDU';
            $query = sprintf("INSERT INTO scs_user_webiso VALUES('%s', '%s')",
                        $scsUserId, $webIso);    
            $result = mysql_query($query);
            echo $query . '<br/>';
        }
    
        if ($cmuandrewid) {
            $webIso = $cmuandrewid . '@ANDREW.CMU.EDU';
            $query = sprintf("INSERT INTO scs_user_webiso VALUES('%s', '%s')",
                        $scsUserId, $webIso);
            $result = mysql_query($query);
            echo $query . '<br/>';    
        }
        
        if ($cmueceid) {
            $webIso = $cmueceid . '@ECE.CMU.EDU';
            $query = sprintf("INSERT INTO scs_user_webiso VALUES('%s', '%s')",
                        $scsUserId, $webIso);
            $result = mysql_query($query);
            echo $query . '<br/>';    
        }
        
        if ($cmuqatarid) {
            $webIso = $cmuqatarid . '@QATAR.CMU.EDU';
            $query = sprintf("INSERT INTO scs_user_webiso VALUES('%s', '%s')",
                        $scsUserId, $webIso);
            $result = mysql_query($query);
            echo $query . '<br/>';    
        }
        echo '<br/>';      
    }
}

?>