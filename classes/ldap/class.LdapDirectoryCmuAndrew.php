<?php
/*
* require_once 'class.LdapDirectory.php';
* require_once 'class.LdapDirectoryCmu.php';
*/

class LdapDirectoryCmuAndrew extends LdapDirectoryCmu
{
    
    const ANDREW_LDAP_SERVER = 'ldap.andrew.cmu.edu';
    
    public function __construct() {
        parent::__construct(self::ANDREW_LDAP_SERVER);    
    }
    
}

?>