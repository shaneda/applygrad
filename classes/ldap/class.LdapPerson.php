<?php

class LdapPerson
{
    // Search ids: guid, cmucsid, cmuandrewid, etc.
    public $personCmuSearchId;
    public $accountCsSearchId;
    public $accountAndrewSearchId;
    public $andrewpersonAndrewSearchId;
    
    // Entries format: Array( [count]=>[0,n] [, [0]=>Array() [, [1]=>Array() [...] ] ] )
    public $personCmuEntries = array();
    public $accountCsEntries = array();
    public $accountAndrewEntries = array();
    public $andrewpersonAndrewEntries = array();
    
    public function __construct() {
        $this->personCmuEntries['count'] = 0;
        $this->accountCsEntries['count'] = 0;
        $this->accountAndrewEntries['count'] = 0;
        $this->andrewpersonAndrewEntries['count'] = 0;
    }
    
    public function getGuid() {

        // Try the accountCs owner first.
        if ( !empty($this->accountCsEntries[0]['owner'][0]) ) {
            return $this->getGuidFromDn( $this->accountCsEntries[0]['owner'][0] );
        }
        
        // Then try the personCmu dn.
        if ( !empty($this->personCmuEntries[0]['dn']) ) {
            return $this->getGuidFromDn( $this->personCmuEntries[0]['dn'] ); 
        }
        
        // Then try the accountAndrew owner.            
        if ( !empty($this->accountAndrewEntries[0]['owner'][0]) ) {
            return $this->getGuidFromDn( $this->accountAndrewEntries[0]['owner'][0] );
        }
        
        // Then try the andrewperson owner.            
        if ( !empty($this->andrewpersonAndrewEntries[0]['owner'][0]) ) {
            return $this->getGuidFromDn( $this->andrewpersonAndrewEntries[0]['owner'][0] );
        }
        
        // Return NULL by default.
        return NULL;
    }

    public function getCmucsid() {

        // accountCs uid appears to be the only source as of 2/8/2012.
        if ( !empty($this->accountCsEntries[0]['uid'][0]) ) {
            return $this->accountCsEntries[0]['uid'][0];
        }

        // Return NULL by default.
        return NULL;
    }

    public function getCmuandrewid() {
        
        // Try the accountAndrew uid.            
        if ( !empty($this->accountAndrewEntries[0]['uid'][0]) ) {
            return $this->accountAndrewEntries[0]['uid'][0];
        }
        
        // Then try the personCmu cmuandrewid.          
        if ( !empty($this->personCmuEntries[0]['cmuandrewid'][0]) ) {        
            return $this->personCmuEntries[0]['cmuandrewid'][0]; 
        }

        // Then try the andrewperson cmuandrewid.
        if ( !empty($this->andrewpersonAndrewEntries[0]['cmuandrewid'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['cmuandrewid'][0]; 
        }
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getCmueduid() {
        
        // Try the personCmu cmueduid.          
        if ( !empty($this->personCmuEntries[0]['cmueduid'][0]) ) {        
            return $this->personCmuEntries[0]['cmueduid'][0]; 
        }

        // Then try the andrewperson cmueduid.
        if ( !empty($this->andrewpersonAndrewEntries[0]['cmueduid'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['cmueduid'][0]; 
        }
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getCmueceid() {
        
        // Try the personCmu cmueceid.          
        if ( !empty($this->personCmuEntries[0]['cmueceid'][0]) ) {        
            return $this->personCmuEntries[0]['cmueceid'][0]; 
        }

        // Then try the andrewperson cmueceid.
        if ( !empty($this->andrewpersonAndrewEntries[0]['cmueceid'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['cmueceid'][0]; 
        }
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getCmuqatarid() {
        
        // Try the personCmu cmuqatarid.          
        if ( !empty($this->personCmuEntries[0]['cmuqatarid'][0]) ) {        
            return $this->personCmuEntries[0]['cmuqatarid'][0]; 
        }

        // Then try the andrewperson cmueceid.
        if ( !empty($this->andrewpersonAndrewEntries[0]['cmuqatarid'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['cmuqatarid'][0]; 
        }
        
        // Return NULL by default.
        return NULL;
    }

    public function getCn() {
        
        // Try the accountCs cn first.
        if ( !empty($this->accountCsEntries[0]['cn'][0]) ) {
            return $this->accountCsEntries[0]['cn'][0];
        }
        
        // Then try the personCmu cn.          
        if ( !empty($this->personCmuEntries[0]['cn'][0]) ) {        
            return $this->personCmuEntries[0]['cn'][0]; 
        }
        
        // Then try the accountAndrew cn.          
        if ( !empty($this->accountAndrewEntries[0]['cn'][0]) ) {        
            return $this->accountAndrewEntries[0]['cn'][0]; 
        }
        
        // Then try the andrewperson cn.          
        if ( !empty($this->andrewpersonAndrewEntries[0]['cn'][0]) ) {        
            return $this->andrewpersonAndrewEntries[0]['cn'][0]; 
        }

        // Return NULL by default.
        return NULL;
    }

    public function getSn() {
        
        // Get it from the personCmu sn.          
        if ( !empty($this->personCmuEntries[0]['sn'][0]) ) {        
            return $this->personCmuEntries[0]['sn'][0]; 
        }

        // Don't try accountCs or accountAndrew. 
        // They don't have sn attributes.
        
        // Don't use the andrewperson sn.  
        // The value in at least one record is 'Account'.
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getGivenname() {
        
        // Try the personCmu givenname.          
        if ( !empty($this->personCmuEntries[0]['givenname'][0]) ) {        
            return $this->personCmuEntries[0]['givenname'][0]; 
        }
        
        // Then try the andrewperson givenname.          
        if ( !empty($this->andrewpersonAndrewEntries[0]['givenname'][0]) ) {        
            return $this->andrewpersonAndrewEntries[0]['givenname'][0]; 
        }

        // Don't try accountCs or accountAndrew. 
        // They don't have givenname attributes.
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getTitle() {
        
        // Try the personCmu title.          
        if ( !empty($this->personCmuEntries[0]['title'][0]) ) {        
            return $this->personCmuEntries[0]['title'][0]; 
        }

        // Then try the andrewperson title.
        if ( !empty($this->andrewpersonAndrewEntries[0]['title'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['title'][0]; 
        }
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getEdupersonschoolcollegename() {
        
        // Try the personCmu edupersonschoolcollegename.          
        if ( !empty($this->personCmuEntries[0]['edupersonschoolcollegename'][0]) ) {        
            return $this->personCmuEntries[0]['edupersonschoolcollegename'][0]; 
        }

        // Then try the andrewperson edupersonschoolcollegename.
        if ( !empty($this->andrewpersonAndrewEntries[0]['edupersonschoolcollegename'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['edupersonschoolcollegename'][0]; 
        }
        
        // Return NULL by default.
        return NULL;
    }
    
    public function getPreferredMail() {
        
        // For CS, the preferred e-mail address is the mail attribute of the account record
        if ( !empty($this->accountCsEntries[0]['mail'][0]) ) {
            return $this->accountCsEntries[0]['mail'][0]; 
        }
        
        // If the CS address isn't set, try the andrewPerson cmuPreferredMail attribute
        if ( !empty($this->andrewpersonAndrewEntries[0]['cmupreferredmail'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['cmupreferredmail'][0]; 
        }
        
        // And if that's not available, try the andrewPerson mail attribute
        if ( !empty($this->andrewpersonAndrewEntries[0]['mail'][0]) ) {
            return $this->andrewpersonAndrewEntries[0]['mail'][0]; 
        }
        
        // Return NULL by default. 
        return NULL; 
    }

    public function shortRecord() {
        
        $shortRecord = array();
        $shortRecord['guid'] = $this->getGuid();
        $shortRecord['cmucsid'] = $this->getCmucsid();
        $shortRecord['cmuandrewid'] = $this->getCmuandrewid();
        $shortRecord['cmueduid'] = $this->getCmueduid();
        $shortRecord['cmueceid'] = $this->getCmueceid();
        $shortRecord['cmuqatarid'] = $this->getCmuqatarid(); 
        $shortRecord['cn'] = $this->getCn();
        $shortRecord['sn'] = $this->getSn();
        $shortRecord['givenname'] = $this->getGivenname();
        $shortRecord['title'] = $this->getTitle();
        $shortRecord['edupersonschoolcollegename'] = $this->getEdupersonschoolcollegename();
        $shortRecord['preferredmail'] = $this->getPreferredMail();        
        return $shortRecord;        
    } 

    private function getGuidFromDn($dn) {
        
        $guid = NULL;
        $dnElements = explode(',', $dn);
        foreach ($dnElements as $dnElement) {
            $elementArray = explode('=', $dnElement);
            if ($elementArray[0] == 'guid') {
                $guid = $elementArray[1];
                return $guid;   
            }      
        }
        return $guid;
    }

}

?>