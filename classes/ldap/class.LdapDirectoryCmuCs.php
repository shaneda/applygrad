<?php
/*
* require_once 'class.LdapDirectory.php';
* require_once 'class.LdapDirectoryCmu.php';
*/

class LdapDirectoryCmuCs extends LdapDirectoryCmu
{
    
    const CS_LDAP_SERVER = 'ldap.srv.cs.cmu.edu';
    const ACCOUNT_CS_BASE_DN = 'ou=account,dc=cs,dc=cmu,dc=edu';
    
    public function __construct() {
        parent::__construct(self::CS_LDAP_SERVER);    
    }
    
    /*
    * Searches of account>cs>cmu tree returning cmuRegularAccount entries 
    */
    public function getAccountCsByCmucsid($cmucsid) {
        $baseDn = 'uid=' . $cmucsid . ',' . self::ACCOUNT_CS_BASE_DN;
        $filter = 'objectclass=cmuRegularAccount';
        $accountCsData = $this->search($baseDn, $filter);
        return $accountCsData;   
    }
    
    public function getAccountCsByGuid($guid) {
        $baseDn = self::ACCOUNT_CS_BASE_DN;
        $filter = 'owner=guid=' . $guid . ',' . self::PERSON_CMU_BASE_DN;
        $accountCsData = $this->search($baseDn, $filter);
        return $accountCsData;   
    }
    
}

?>