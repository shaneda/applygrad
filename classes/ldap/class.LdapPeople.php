<?php
/*
* require_once 'class.LdapDirectory.php';
* require_once 'class.LdapDirectoryCmu.php';
* require_once 'class.LdapDirectoryCmuCs.php';
* require_once 'class.LdapDirectoryCmuAndrew.php';
* require_once 'class.LdapPerson.php';
*/

class LdapPeople
{
    public $csLdapDirectory;
    public $andrewLdapDirectory;
       
    public function __construct() {       
        $this->csLdapDirectory = new LdapDirectoryCmuCs();
        $this->andrewLdapDirectory = new LdapDirectoryCmuAndrew();
        $this->csLdapDirectory->connect();
        $this->andrewLdapDirectory->connect();
    }
     
    // Use $return='object' to return LdapPerson object instead of short record array.
    public function getByWebIso($webIsoString, $return = 'short') {

        $webIsoArray = explode('@', $webIsoString);
        $uid = $webIsoArray[0];
        $realm = strtoupper($webIsoArray[1]);
        
        switch ($realm) {          
            
            case 'CS.CMU.EDU':
                $ldapPerson = $this->getLdapPersonByCmucsid($uid);
                break;    
            
            case 'ANDREW.CMU.EDU':
                $ldapPerson = $this->getLdapPersonByCmuandrewid($uid);
                break;
            
            case 'ECE.CMU.EDU':
                $ldapPerson = $this->getLdapPersonByCmueceid($uid);
                break;

            case 'QATAR.CMU.EDU':
                $ldapPerson = $this->getLdapPersonByCmuqatarid($uid);
                break;

            default:
                $ldapPerson = new LdapPerson();
        }
        
        if ($return == 'object') {
            return $ldapPerson;
        } else {
            return $ldapPerson->shortRecord();   
        }   
    }
   
    // Use $record='object' to return LdapPerson object instead of short record array.
    public function getByGuid($guid, $record = 'short') {

        // Instantiate a new person data object.
        $ldapPerson = new LdapPerson();
        
        // Start by trying to get the personCmu data from the andrew directory,
        // since guid=[guid],ou=person,dc=cmu,dc=edu is the owner of the other entries.
        $ldapPerson->personCmuSearchId = 'guid';
        $ldapPerson->personCmuEntries = 
                $this->andrewLdapDirectory->getPersonCmuByGuid($guid);        

        // Try to get the accountAndrew data with the cmuandrewid.
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($cmuandrewid) {
            $ldapPerson->accountAndrewSearchId = 'cmuandrewid';
            $ldapPerson->accountAndrewEntries = 
                $this->andrewLdapDirectory->getAccountAndrewByCmuandrewid($cmuandrewid);    
        }
            
        // If that didn't work, try to get the accountAndrew data with the guid.
        if ( !isset($ldapPerson->accountAndrewEntries[0]) ) {
            $ldapPerson->accountAndrewSearchId = 'guid';
            $ldapPerson->accountAndrewEntries = 
                $this->andrewLdapDirectory->getAccountAndrewByGuid($guid); 
        }
        
        // Try to get the andrewpersonAndrew entry with the cmuandrewid. 
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($cmuandrewid) {
            $ldapPerson->andrewpersonAndrewSearchId = 'cmuandrewid';
            $ldapPerson->andrewpersonAndrewEntries = 
                $this->andrewLdapDirectory->getAndrewpersonAndrewByCmuandrewid($cmuandrewid);    
        }

        // If that didn't work, try to get it with the guid.
        if ( !isset($ldapPerson->andrewpersonAndrewEntries[0]) ) {
            $ldapPerson->andrewpersonAndrewSearchId = 'guid';
            $ldapPerson->andrewpersonAndrewEntries = 
                $this->andrewLdapDirectory->getAndrewpersonAndrewByGuid($guid);
        }
        
        // Try to get the accountCs data with the guid.
        $ldapPerson->accountCsSearchId = 'guid';
        $ldapPerson->accountCsEntries = $this->csLdapDirectory->getAccountCsByGuid($guid);
            
        if ($record == 'object') {
            return $ldapPerson;
        } else {
            return $ldapPerson->shortRecord();   
        }   
    }
        
    private function getLdapPersonByCmucsid($cmucsid) {

        // Instantiate a new person data object.
        $ldapPerson = new LdapPerson();
        
        // Try to get the accountCs data with the cmucsid.
        $ldapPerson->accountCsSearchId = 'cmucsid';
        $ldapPerson->accountCsEntries = $this->csLdapDirectory->getAccountCsByCmucsid($cmucsid);
            
        // Try to get the personCmu data with the guid.
        $guid = $ldapPerson->getGuid();
        if ($guid) {
            $ldapPerson->personCmuSearchId = 'guid';
            $ldapPerson->personCmuEntries = $this->csLdapDirectory->getPersonCmuByGuid($guid);      
        }

        // Try to get the accountAndrew data with the cmuandrewid (from personCmu).
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($cmuandrewid) {
            $ldapPerson->accountAndrewSearchId = 'cmuandrewid';
            $ldapPerson->accountAndrewEntries = 
                $this->andrewLdapDirectory->getAccountAndrewByCmuandrewid($cmuandrewid);
        }
        
        // If that didn't work, try again with the guid.
        $guid = $ldapPerson->getGuid();
        if ($ldapPerson->accountAndrewEntries['count'] == 0 && $guid) {
            $ldapPerson->accountAndrewSearchId = 'guid';
            $ldapPerson->accountAndrewEntries = 
                $this->andrewLdapDirectory->getAccountAndrewByGuid($guid);   
        }
        
        // If it wasn't retrieved before, try to get the personCmu data
        // with the cmuandrewid (from accountAndrew).
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($ldapPerson->personCmuEntries['count'] == 0 && $cmuandrewid) {
            $ldapPerson->personCmuSearchId = 'cmuandrewid';
            $ldapPerson->personCmuEntries = $this->csLdapDirectory->getPersonCmuByCmuandrewid($cmuandrewid);             
        }
        
        // Try to get the andrewpersonAndrew data with the cmuandrewid.
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($cmuandrewid) {
            $ldapPerson->andrewpersonAndrewSearchId = 'cmuandrewid';
            $ldapPerson->andrewpersonAndrewEntries = 
                $this->andrewLdapDirectory->getAndrewpersonAndrewByCmuandrewid($cmuandrewid);
        }
        
        // If that didn't work, try to get the andrewpersonAndrew data with the guid.
        $guid = $ldapPerson->getGuid();
        if ($guid) {
            $ldapPerson->andrewpersonAndrewSearchId = 'guid';
            $ldapPerson->andrewpersonAndrewEntries = 
                $this->andrewLdapDirectory->getAndrewpersonAndrewByGuid($guid);
        }
        
        return $ldapPerson;        
    }

    private function getLdapPersonByCmuandrewid($cmuandrewid) {
        
        // Instantiate a new person data object.
        $ldapPerson = new LdapPerson();    
        
        // Try to get the accountAndrew data with the cmuandrewid.
        $ldapPerson->accountAndrewSearchId = 'cmuandrewid';
        $ldapPerson->accountAndrewEntries = 
            $this->andrewLdapDirectory->getAccountAndrewByCmuandrewid($cmuandrewid);
                
        // Try to get the andrewpersonAndrew data with the cmuandrewid.
        $ldapPerson->andrewpersonAndrewSearchId = 'cmuandrewid';
        $ldapPerson->andrewpersonAndrewEntries = 
            $this->andrewLdapDirectory->getAndrewpersonAndrewByCmuandrewid($cmuandrewid);
            
        // Try to get the personCmu entry with the guid.
        $guid = $ldapPerson->getGuid();
        if ($guid) {
            $ldapPerson->personCmuSearchId = 'guid';
            $ldapPerson->personCmuEntries = 
                $this->andrewLdapDirectory->getPersonCmuByGuid($guid);        
        }
        
        // If that didn't work, try again with the cmuandrewid.
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($ldapPerson->personCmuEntries['count'] == 0 && $cmuandrewid) {
            $ldapPerson->personCmuSearchId = 'cmuandrewid';
            $ldapPerson->personCmuEntries = $this->csLdapDirectory->getPersonCmuByCmuandrewid($cmuandrewid);             
        }

        // Try to get the accountCs data with the guid.
        $guid = $ldapPerson->getGuid();
        if ($guid) {
            $ldapPerson->accountCsSearchId = 'guid';
            $ldapPerson->accountCsEntries = $this->csLdapDirectory->getAccountCsByGuid($guid);    
        }
        
        return $ldapPerson;
    }

    private function getLdapPersonByCmueceid($cmueceid) {
        
        // Instantiate a new person data object.
        $ldapPerson = new LdapPerson();    
        
        // Try to get the personCmu data with the cmueceid.
        $ldapPerson->personCmuSearchId = 'cmueceid';
        $ldapPerson->personCmuEntries = 
            $this->andrewLdapDirectory->getPersonCmuByCmueceid($cmueceid);

        // Try to get the accountAndrew, andrewpersonAndrew, and accountCs data.
        $ldapPerson = $this->getCmuPersonAccounts($ldapPerson);
        
        return $ldapPerson;
    }
    
    private function getLdapPersonByCmuqatarid($cmuqatarid) {
        
        // Instantiate a new person data object.
        $ldapPerson = new LdapPerson();   
        
        // Try to get the personCmu data with the cmuqatarid.
        $ldapPerson->personCmuSearchId = 'cmuqatarid';
        $ldapPerson->personCmuEntries = 
            $this->andrewLdapDirectory->getPersonCmuByCmuqatarid($cmuqatarid); 

        // Try to get the accountAndrew, andrewpersonAndrew, and accountCs data.
        $ldapPerson = $this->getCmuPersonAccounts($ldapPerson);
        
        return $ldapPerson;
    }
    
    /*
    * Shared routine for getLdapPersonByCmueceid and getLdapPersonByCmuqatarid
    */
    private function getCmuPersonAccounts($ldapPerson) {

        // Try to get the accountAndrew entry with the cmuandrewid.
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($cmuandrewid) {
            $ldapPerson->accountAndrewSearchId = 'cmuandrewid';
            $ldapPerson->accountAndrewEntries = 
                $this->andrewLdapDirectory->getAccountAndrewByCmuandrewid($cmuandrewid);    
        }
            
        // If that didn't work, try to get it with the guid.
        if ( !isset($ldapPerson->accountAndrewEntries[0]) ) {
            $guid = $ldapPerson->getGuid();
            $ldapPerson->accountAndrewSearchId = 'guid';
            if ($guid) {
                $ldapPerson->accountAndrewEntries = 
                    $this->andrewLdapDirectory->getAccountAndrewByGuid($guid);    
            }
        }
        
        // Try to get the andrewpersonAndrew data with the cmuandrewid. 
        $cmuandrewid = $ldapPerson->getCmuandrewid();
        if ($cmuandrewid) {
            $ldapPerson->andrewpersonAndrewSearchId = 'cmuandrewid';
            $ldapPerson->andrewpersonAndrewEntries = 
                $this->andrewLdapDirectory->getAndrewpersonAndrewByCmuandrewid($cmuandrewid);    
        }

        // If that didn't work, try to get it with the guid.
        if ( !isset($ldapPerson->andrewpersonAndrewEntries[0]) ) {
            $guid = $ldapPerson->getGuid();
            if ($guid) {
                $ldapPerson->andrewpersonAndrewSearchId = 'guid';
                $ldapPerson->andrewpersonAndrewEntries = 
                    $this->andrewLdapDirectory->getAndrewpersonAndrewByGuid($guid);    
            }
        }
        
        // Try to get the accountCs data with the with the guid.
        $guid = $ldapPerson->getGuid();
        if ($guid) {
            $ldapPerson->accountCsSearchId = 'guid';
            $ldapPerson->accountCsEntries = $this->csLdapDirectory->getAccountCsByGuid($guid);    
        }    
    
        return $ldapPerson;  
    }
    
}
?>