<?php

class LdapDirectory
{

    protected $hostname;
    protected $connection;
    protected $connectionIsBound;
    
    public function __construct($hostname) {
        $this->hostname = $hostname;
    }

    public function connect($bindRdn = NULL, $bindPassword = NULL) {
        
        if ( !($this->connection && $this->connectionIsBound ) ) {
            $this->connection = ldap_connect($this->hostname)
                or die("Could not connect to " . $this->hostname);
            $this->connectionIsBound = ldap_bind($this->connection, $bindRdn, $bindPassword)
                or die("Could not bind the directory on " . $this->hostname);            
        }

        return TRUE; 
    }
    
    public function disconnect() {
        ldap_close($this->connection);
        return TRUE;
    }
      
    public function search($baseDn, $filter = 'objectclass=*') {
        
        if ( !($this->connection && $this->connectionIsBound ) ) {
            $this->connect();
        }
        
        $searchResource = ldap_search($this->connection, $baseDn, $filter);
        if ( $resultsArray = ldap_get_entries($this->connection, $searchResource) ) {
            $searchResults = $resultsArray;     
        } else {
            $searchResults = array();
            $searchResults['count'] = 0;    
        }
        ldap_free_result($searchResource);      
        
        return $searchResults;
    }

}

?>