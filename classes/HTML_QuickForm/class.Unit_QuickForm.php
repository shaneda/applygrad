<?php
//require("HTML/QuickForm.php");

class Unit_QuickForm extends HTML_QuickForm
{
    
    //private $header;    // HTML_QuickForm_element
    
    function __construct($formName = 'unit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, null, null, true); 
        
        $this->addElement('hidden', 'edit', 'unit');
        $this->addElement('hidden', 'unit_id');
        $this->addElement('text', 'unit_name', 'Full Name:', array('size' => 40, 'maxlength' => 100));
        $this->addElement('text', 'unit_name_short', 'Short Name:', array('size' => 10, 'maxlength' => 20));
        $this->addElement('textarea', 'unit_description', 'Description:', array('rows' => 2, 'cols' => 30));
        $this->addElement('text', 'unit_url', 'URL:', array('size' => 40, 'maxlength' => 255));
        $this->addElement('text', 'unit_oracle_string', 'Oracle String:', array('size' => 30, 'maxlength' => 50));
        $this->addElement('text', 'unit_system_email', 'System E-mail:', array('size' => 30, 'maxlength' => 100));
        $this->addElement('text', 'unit_cc_email', 'CC E-mail:', array('size' => 30, 'maxlength' => 100));
        
        $parentSelect = $this->addElement(
                                'select', 
                                'parent_unit_id', 
                                'Parent Unit:', 
                                null, 
                                array('size' => 5));
        $parentSelect->addOption('[none]', 'NULL');
        
        $this->addElement('submit', 'submitUnit', 'Save');
        $this->addElement('submit', 'submitUnit', 'Cancel');
        
        $this->addRule('unit_name', 'Please enter unit name', 'required', null, 'server', true);
        $this->addRule('unit_name_short', 'Please enter unit short name', 'required', null, 'server', true); 
        
        
        // Create a header, with a generic default value, as the first form element.
        /*
        $this->header = $this->addElement('header', 'header', 'Unit Details');
        */
        
        // Add validation rules.
        /*
        $this->addRule(array('startDate', 'unit', 'unitId', 'admissionPeriodId'),
                'The start date must not overlap with another period', 
                'callback', 'AdmissionPeriod_QuickForm::noPeriodOverlap',
                NULL, NULL, TRUE);
        
        $this->addRule(array('submitDeadline', 'unit', 'unitId', 'admissionPeriodId'),
                'The submission deadline must not overlap with another period', 
                'callback', 'AdmissionPeriod_QuickForm::noPeriodOverlap',
                NULL, NULL, TRUE);
        
        $this->addRule(array('submitDeadline', 'startDate'),
                'The submission deadline must be later than the start date', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('editDeadline', 'submitDeadline'),
                'The editing deadline must be later than the submission deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('viewDeadline', 'editDeadline'),
                'The viewing deadline must be later than the editing deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('admitTermDate', 'viewDeadline'),
                'The admission term date must be later than the viewing deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        */       
        
    }
    
    
    public function setHeaderLabel($headerString) {
        $this->header->setText($headerString);
        return TRUE;
    }
    
    static function noPeriodOverlap($fieldArrays) {
        
        $timestamp = self::dateArrayToUnixTimestamp($fieldArrays[0]);
        $date = date('Y-m-d', $timestamp);
        $unit = $fieldArrays[1];
        $unitId = $fieldArrays[2];
        $admissionPeriodId = $fieldArrays[3];
        
        global $dbConnection;
        $admissionPeriodTable = new AdmissionPeriod_Table($dbConnection, "AdmissionPeriod");
        $overlappingPeriods = $admissionPeriodTable->findDateOverlap($date, $unit, $unitId, $admissionPeriodId);
        $overlapCount = count($overlappingPeriods);

        if ($overlapCount == 0) {
            return TRUE;   
        } else {
            return FALSE;    
        }
        
    }    


    static function firstDateLater($dateElementArrays) {
        
        $firstDate = self::dateArrayToUnixTimestamp($dateElementArrays[0]);
        $secondDate = self::dateArrayToUnixTimestamp($dateElementArrays[1]);
        
        if ($firstDate > $secondDate) {
            return TRUE;   
        } else {
            return FALSE;    
        }
        
    }
    
    static function dateArrayToUnixTimestamp($dateElementArray) {
        
        $timestamp = mktime(0, 0, 0, $dateElementArray['m'], $dateElementArray['d'], $dateElementArray['Y']);
        return $timestamp;
    }
    
}

?>
