<?php
//require("HTML/QuickForm.php");

class NewUnit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'newUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'edit', 'newUnit');
        $this->addElement('submit', 'submitEditNewUnit', 'Add New Unit');
    }
    
}
?>