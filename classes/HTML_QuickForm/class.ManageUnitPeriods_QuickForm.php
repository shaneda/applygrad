<?php
//require("HTML/QuickForm.php");

class ManageUnitPeriods_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'manageUnitPeriods', $method = 'post', $action = 'periods.php') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'unit_id');
        $this->addElement('submit', 'submitManageUnitPeriods', 'Manage Periods');
    }
    
}
?>