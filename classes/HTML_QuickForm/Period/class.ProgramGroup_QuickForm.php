<?php
//require("HTML/QuickForm.php");

class ProgramGroup_QuickForm extends HTML_QuickForm
{
    
    function __construct($formName = 'programGroup', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, null, null, true); 
        
        $this->addElement('hidden', 'edit', 'programGroup'); 
        $this->addElement('hidden', 'period_id');
        $this->addElement('hidden', 'program_group_id');
        $this->addElement('text', 'program_group_name', 'Full Name:', array('size' => 40, 'maxlength' => 100));
        $this->addElement('text', 'program_group_name_short', 'Short Name:', array('size' => 40, 'maxlength' => 45));
        //$this->addElement('text', 'program_group_description', 'Description:', array('size' => 40, 'maxlength' => 45));

        $groupTypeGroup = $this->addGroup( 
                        array(),
                        'program_group_type_id',
                        'Group Type: ',
                        "<br/>\n"
                        );

        $unitSelect = $this->addElement(
                        'select', 
                        'unit_id', 
                        'Unit:',
                        null,
                        array('size' => 1)
                        );
        
        /*
        $programGroup = $this->addGroup( 
                        array(),
                        'program_id',
                        'Programs: ',
                        "<br/>\n"
                        ); 
        */
        
        $this->addElement('submit', 'submitProgramGroup', 'Save');
        $this->addElement('submit', 'submitProgramGroup', 'Cancel');
                
    }
        
    public function handleSelf(&$programGroup, &$period, &$unit = NULL) {
    
        
        //DebugBreak();
        $periodId = $period->getId();
        
        if ($unit) {
            $unitId = $unit->getId();
        } else {            
            $unitId = $period->getUnitId();
            $unit = new Unit($unitId);
        } 
        
        //$programGroup = new ProgramGroup($programGroupId, $periodId);
        $programGroupId = $programGroup->getId();

        /*
        * Set the form defaults.
        */
        if ($programGroupId) {
            $defaultValues = $programGroup->exportValues();
            //$programIds = array_pop($defaultValues);
            //$programGroupTypeIds = array_pop($defaultValues);    
        } else {
            $defaultValues['period_id'] = $periodId;
            if ($programGroupId) {
                $defaultValues['program_group_id'] = $programGroupId;    
            }            
        }
        $this->setDefaults($defaultValues);
        
        /* 
        * Create the checkbox groups & unit select.
        */
        $this->makeGroupTypeCheckbox();
        $this->makeUnitSelect($unit);
        //$this->makeProgramCheckbox($unit);
        
        /*
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitProgramGroup');
        $valid = FALSE;
        
        if ( (!$submitted && $programGroupId) || $submitValue == 'Cancel' ) {
            
            $this->switchButtons();   
            if ($programGroupId) {
                $this->setConstants($defaultValues);
            }
            $this->freeze(); 
        
            $valid = TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       
            
            $this->switchButtons();
            $this->freeze();

            /* 
            * Determine which checkboxes were submitted.
            * Set the new values and save.
            */
            $formValues = $this->exportValue('program_group_type_id');
            if ( is_array($formValues) ) {
                $checkedGroupTypeValues = array_keys($formValues);     
            } else {
                $checkedGroupTypeValues = array();     
            }
            $groupTypeIds = array();
            $groupTypeCheckboxGroup = $this->getElement('program_group_type_id'); 
            foreach ($groupTypeCheckboxGroup->getElements() as $groupTypeCheckbox) {
                $groupTypeId = $groupTypeCheckbox->getName();
                if ( in_array($groupTypeId, $checkedGroupTypeValues) ) {
                    $groupTypeIds[] = $groupTypeId;  
                }
            }
            
            /*
            $checkedProgramValues = array_keys( $this->exportValue('program_id') );
            $programIds = array();
            $programCheckboxGroup = $this->getElement('program_id');
            foreach ($programCheckboxGroup->getElements() as $programCheckbox) {
                $programId = $programCheckbox->getName();
                if ( in_array($programId, $checkedProgramValues) && $programId != 'programsAll') {
                    $programIds[] = $programId;  
                }
            }
            */
            
            $values = $this->exportValues(); 
            $values['program_group_type_id'] = $groupTypeIds;
            //$values['program_id'] = $programIds;
            
            $programGroup->importValues($values);
            $programGroup->save();
            
            $valid = TRUE; 
        
        }

        /*
        * Set which checkboxes are checked.
        */
        $this->setGroupTypeCheckbox($programGroup);
        //$this->setProgramCheckbox($programGroup);
        
        return $valid;      
    }
    
    private function makeGroupTypeCheckbox() {

        $DB_ProgramGroupType = new DB_ProgramGroupType();
        
        $groupTypeCheckboxGroup = $this->getElement('program_group_type_id');        
        $groupTypeCheckboxes = array();
        foreach ( $DB_ProgramGroupType->find() as $groupTypeRecord ) {
            $groupTypeCheckboxes[] = HTML_QuickForm::createElement(
                'checkbox', 
                $groupTypeRecord['program_group_type_id'], 
                NULL, 
                $groupTypeRecord['program_group_type_name'], 
                array('class' => 'groupType')
                );
        }
        $groupTypeCheckboxGroup->setElements($groupTypeCheckboxes);
           
        return TRUE;
    }

    private function setGroupTypeCheckbox(&$programGroup) {
    
        $groupTypeIds = $programGroup->getGroupTypeIds();
        $groupTypeCheckboxGroup = $this->getElement('program_group_type_id');
        foreach ($groupTypeCheckboxGroup->getElements() as $groupTypeCheckbox) {
            if ( in_array($groupTypeCheckbox->getName(), $groupTypeIds) ) {
                $groupTypeCheckbox->setChecked(TRUE);    
            } else {
                $groupTypeCheckbox->setChecked(FALSE);
            }
        }
        
        return TRUE;    
    }

     private function makeUnitSelect(&$unit) {

        $unitSelect = $this->getElement('unit_id');
        $unitSelect->addOption($unit->getNameShort(), $unit->getId());
        foreach ($unit->getChildUnits() as $program) {
            $unitSelect->addOption($program->getNameShort(), $program->getId());           
        }
           
        return TRUE;
    }

    private function makeProgramCheckbox(&$unit) {

        $programCheckboxGroup = $this->getElement('program_id');        
        $programCheckboxes = array();
        
        $programCheckboxes[] = HTML_QuickForm::createElement(
            'checkbox', 
            'programsAll', 
            NULL, 
            'Select All Programs', 
            array('id' => 'programsAll')
            );

        foreach ( $unit->getPrograms() as $program ) {
            $programCheckboxes[] = HTML_QuickForm::createElement(
                'checkbox', 
                $program->getId(), 
                NULL, 
                $program->getName(), 
                array('class' => 'program')
                );
        }
        $programCheckboxGroup->setElements($programCheckboxes);
           
        return TRUE;
    }

    private function setProgramCheckbox(&$programGroup) {
    
        $programIds = $programGroup->getProgramIds();
        $programCheckboxGroup = $this->getElement('program_id');
        foreach ($programCheckboxGroup->getElements() as $programCheckbox) {
            if ( in_array($programCheckbox->getName(), $programIds) ) {
                $programCheckbox->setChecked(TRUE);    
            }
        }
        
        return TRUE;    
    }

    private function switchButtons() {
        
        //$programCheckboxGroup = $this->getElement('program_id');
        //$programCheckboxElements = $programCheckboxGroup->getElements();
        //$checkAllPrograms = array_shift($programCheckboxElements);
        //$programCheckboxGroup->setElements($programCheckboxElements);
        
        $this->removeElement('submitProgramGroup');
        $this->removeElement('submitProgramGroup');
        $this->addElement('submit', 'editProgramGroup', 'Edit');
            
        return TRUE;        
    }


}
?>
