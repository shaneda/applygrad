<?php
//require("HTML/QuickForm.php");

class ProgramGroupProgram_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'programGroupProgram', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'edit', 'programGroupProgram');
        $this->addElement('hidden', 'program_group_id');
        $this->addElement('hidden', 'period_id');
        $this->addElement('checkbox', 'programsAll', null, 'Select All', array('id' => 'programsAll'));
        
        $programGroup = $this->addGroup( 
                            array(),
                            'unit_id',
                            null,
                            "<br/>\n"
                            );
        
        $this->addElement('submit', 'submitProgramGroupProgram', 'Save');
        $this->addElement('submit', 'submitProgramGroupProgram', 'Cancel');
    }

    public function handleSelf(&$programGroup) {
        
        /*
        * Set the hidden element values.
        */
        $defaultValues['program_group_id'] = $programGroup->getId();
        $defaultValues['period_id'] = $programGroup->getPeriodId();
        $this->setDefaults($defaultValues);

        /* 
        * Create the checkbox group.
        */
        $unit = new Unit( $programGroup->getUnitId() );
        $this->makeProgramCheckbox($unit);

        /*
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitProgramGroupProgram');

        if ( !$submitted || $submitValue == 'Cancel' ) {
            
            // Make "inactive" for display.
            $this->switchButtons();
            $this->freeze();  
        }

        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       

            // Make "inactive" for display.
            $this->switchButtons();
            $this->freeze();
            
            // Determine which checkboxes were submitted.
            $checkedValues = array_keys( $this->exportValue('unit_id') );
        
            // Set the new values and save.
            $programIds = array();
            $programCheckboxGroup = $this->getElement('unit_id');
            foreach ($programCheckboxGroup->getElements() as $programCheckbox) {
                $programId = $programCheckbox->getName();
                if ( in_array($programId, $checkedValues) ) {
                    $programIds[] = $programId;  
                }
            }
            
            $programGroup->setProgramIds($programIds);
            $programGroup->save();
        }

        /*
        * Set which checkboxes are checked.
        */
        $this->setProgramCheckbox($programGroup);
        
        return TRUE;    
    }
    
    private function makeProgramCheckbox(&$unit) {

        $programCheckboxGroup = $this->getElement('unit_id');        
        $programCheckboxes = array();
        
        $programs = $unit->getPrograms();
        foreach ($programs as $program ) {
            $programCheckboxes[] = HTML_QuickForm::createElement(
                'checkbox', 
                $program->getId(), 
                NULL, 
                $program->getName(), 
                array('class' => 'program')
                );
        }
        $programCheckboxGroup->setElements($programCheckboxes);
           
        return TRUE;
    }
    
    private function setProgramCheckbox(&$programGroup) {

        $programIds = $programGroup->getProgramIds();
        $programCheckboxGroup = $this->getElement('unit_id');
        foreach ($programCheckboxGroup->getElements() as $programCheckbox) {
            if ( in_array($programCheckbox->getName(), $programIds) ) {
                $programCheckbox->setChecked(TRUE);    
            }
        }
        
        return TRUE;
    }
    
    private function switchButtons() {
        
        $this->removeElement('programsAll');
        $this->removeElement('submitProgramGroupProgram');
        $this->removeElement('submitProgramGroupProgram');
        $this->addElement('submit', 'editProgramGroupProgram', 'Set Programs');
                
        return TRUE;
    }

}

?>