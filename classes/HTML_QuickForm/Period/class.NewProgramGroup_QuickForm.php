<?php
//require("HTML/QuickForm.php");

class NewProgramGroup_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'newProgramGroup', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'period_id');
        $this->addElement('hidden', 'edit', 'newProgramGroup');
        $this->addElement('submit', 'submitEditNewProgramGroup', 'Add New Program Group');
    }

    public function handleSelf(&$period) {

        $this->setDefaults( array(
            'period_id' => $period->getId() 
            ) 
        );
        
        if ( $this->isSubmitted() ) {
            $this->updateElementAttr(
                array('submitEditNewProgramGroup'),
                array('disabled' => 'true')
            );
            $this->freeze();  
        }
        
        return TRUE;    
    }
    
}
?>