<?php
//require("HTML/QuickForm.php");

class Period_QuickForm extends HTML_QuickForm
{
    
    //private $header;    // HTML_QuickForm_element
    
    function __construct($formName = 'period', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, null, null, true); 
        
        $this->addElement('hidden', 'edit', 'period'); 
        $this->addElement('hidden', 'period_id');
        $this->addElement('hidden', 'unit_id');
        $this->addElement('hidden', 'period_type_id');
        $this->addElement('hidden', 'parent_period_id');
        $this->addElement('text', 'description', 'Description:', array('size' => 40, 'maxlength' => 45));
        $this->addElement('text', 'start_date', 'Start Date:', 
            array('size' => 20, 'maxlength' => 20, 'class' => 'startDate') );
        $this->addElement('text', 'end_date', 'End Date:', 
            array('size' => 20, 'maxlength' => 20, 'class' => 'endDate'));       
        
        $this->addElement('submit', 'submitPeriod', 'Save');
        $this->addElement('submit', 'submitPeriod', 'Cancel');
                
    }
        
    public function handleSelf(&$period, &$unit) {
    
        $periodId = $period->getId();
        $periodTypeId = $period->getPeriodTypeId();
        $parentPeriodId = $period->getParentPeriodId(); 
        $unitId = $unit->getId();    

        /*
        * Set the form defaults.
        */
        //$defaultValues = $period->getPeriodRecord();
        $defaultValues = $period->exportValues();
        
        if ($periodId) {    
            $this->setDefaults($defaultValues);    
        } else {
            $this->setDefaults( 
                array(
                    'unit_id' => $unitId, 
                    'period_type_id' => $periodTypeId,
                    'parent_period_id' => $parentPeriodId
                ) 
            );    
        }
        
        /*
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitPeriod');
        
        if ( (!$submitted && $periodId) || $submitValue == 'Cancel' ) {
            
            $this->switchButtons();   
            if ($periodId) {
                $this->setConstants($defaultValues);
            }
            $this->freeze();    
        
            return TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       
            
            $this->switchButtons();
            $this->freeze();

            $values = $this->exportValues(); 
            $period->importValues($values);
            $period->save();
                               
            if ( $period->isUmbrella() ) {     
                $this->updateSubmissionPeriod($period);
                $this->updateEditingPeriod($period);               
            }
            
            return TRUE;
        
        } else {
            
            return FALSE;
            
        }          
    }
    
    private function switchButtons() {
        
        $this->removeElement('submitPeriod');
        $this->removeElement('submitPeriod');
        $this->addElement('submit', 'editPeriod', 'Edit');
            
        return TRUE;        
    }

    private function updateSubmissionPeriod(&$umbrellaPeriod) {
    
        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        } 
        
        $submissionPeriods = $umbrellaPeriod->getChildPeriods(2);
        if ( count($submissionPeriods) > 0) {
            
            $submissionPeriod = $submissionPeriods[0];    
        
        } else {
            
            $submissionPeriod = new Period(NULL, 'subperiod');
            $submissionPeriod->setParentPeriodId( $umbrellaPeriod->getId() );
        }
        $submissionPeriod->setPeriodTypeId(2);
        $submissionPeriod->setUnitId( $umbrellaPeriod->getUnitId() );
        $submissionPeriod->setStartDate( $umbrellaPeriod->getStartDate() );
        $submissionPeriod->save();
        
        return TRUE;
    }

    private function updateEditingPeriod(&$umbrellaPeriod) {
    
        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        } 
        
        $editingPeriods = $umbrellaPeriod->getChildPeriods(3);
        if ( count($editingPeriods) > 0) {
            
            $editingPeriod = $editingPeriods[0];    
        
        } else {
            
            $editingPeriod = new Period(NULL, 'subperiod');
            $editingPeriod->setParentPeriodId( $umbrellaPeriod->getId() );
        }
        $editingPeriod->setPeriodTypeId(3);
        $editingPeriod->setUnitId( $umbrellaPeriod->getUnitId() );
        $editingPeriod->setStartDate( $umbrellaPeriod->getStartDate() );
        $editingPeriod->save();
        
        return TRUE;
    }
    
}
?>