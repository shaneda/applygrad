<?php
//require("HTML/QuickForm.php");

class PeriodProgram_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'periodProgram', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'edit', 'periodProgram');
        $this->addElement('hidden', 'period_id');
        $this->addElement('hidden', 'unit_id');
        $this->addElement('checkbox', 'programsAll', null, 'Select All', array('id' => 'programsAll'));
        
        $programGroup = $this->addGroup( 
                            array(),
                            'program_unit_id',
                            null,
                            "<br/>\n"
                            );
        
        $this->addElement('submit', 'submitPeriodProgram', 'Save');
        $this->addElement('submit', 'submitPeriodProgram', 'Cancel');
    }

    public function handleSelf(&$umbrellaPeriod, &$unit) {
        
        $periodId = $umbrellaPeriod->getId();
        $unitId = $unit->getId();
        
        /*
        * Set the hidden element values.
        */
        $defaultValues['period_id'] = $periodId;
        $defaultValues['unit_id'] = $unitId;
        $this->setDefaults($defaultValues);

        /* 
        * Create the checkbox group.
        */
        $this->makeProgramCheckbox($unit);

        /*
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitPeriodProgram');

        if ( !$submitted || $submitValue == 'Cancel' ) {
            
            // Make "inactive" for display.
            $this->switchButtons();
            $this->freeze();  
        }

        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       

            // Make "inactive" for display.
            $this->switchButtons();
            $this->freeze();
            
            // Determine which checkboxes were submitted.
            $formValues = $this->exportValue('program_unit_id');
            if ( is_array($formValues) ) {
                $checkedValues = array_keys( $formValues );    
            } else {
                $checkedValues = array();
            }
            
            // Set the new values and save.
            $programIds = array();
            $programCheckboxGroup = $this->getElement('program_unit_id');
            foreach ($programCheckboxGroup->getElements() as $programCheckbox) {
                $programId = $programCheckbox->getName();
                if ( in_array($programId, $checkedValues) ) {
                    $programIds[] = $programId;  
                }
            }
            $umbrellaPeriod->setProgramIds($programIds);
            $umbrellaPeriod->save();
            
        }

        /*
        * Set which checkboxes are checked.
        */
        $this->setProgramCheckbox($umbrellaPeriod);
        
        return TRUE;    
    }
    
    private function makeProgramCheckbox(&$unit) {

        $programCheckboxGroup = $this->getElement('program_unit_id');        
        $programCheckboxes = array();
        foreach ( $unit->getPrograms() as $program ) {
            $programCheckboxes[] = HTML_QuickForm::createElement(
                'checkbox', 
                $program->getId(), 
                NULL, 
                $program->getName(), 
                array('class' => 'program')
                );
        }
        $programCheckboxGroup->setElements($programCheckboxes);
           
        return TRUE;
    }
    
    private function setProgramCheckbox(&$umbrellaPeriod) {
    
        $programIds = $umbrellaPeriod->getProgramIds();
        $programCheckboxGroup = $this->getElement('program_unit_id');
        foreach ($programCheckboxGroup->getElements() as $programCheckbox) {
            if ( in_array($programCheckbox->getName(), $programIds) ) {
                $programCheckbox->setChecked(TRUE);    
            }
        }
        
        return TRUE;    
    }
    
    private function switchButtons() {
        
        $this->removeElement('programsAll');
        $this->removeElement('submitPeriodProgram');
        $this->removeElement('submitPeriodProgram');
        $this->addElement('submit', 'editPeriodProgram', 'Set Programs');
                
        return TRUE;
    }

}

?>