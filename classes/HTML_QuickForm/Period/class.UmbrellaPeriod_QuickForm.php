<?php
//require("HTML/QuickForm.php");

class UmbrellaPeriod_QuickForm extends HTML_QuickForm
{
    
    //private $header;    // HTML_QuickForm_element
    
    function __construct($formName = 'period', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, null, null, true); 
        
        $this->addElement('hidden', 'edit', 'period'); 
        $this->addElement('hidden', 'period_id');
        $this->addElement('hidden', 'unit_id');
        $this->addElement('hidden', 'period_type_id');
        $this->addElement('hidden', 'parent_period_id');
        
        $this->addElement('text', 'umbrella_name', 'Name:', array('size' => 40, 'maxlength' => 45));
        $this->addElement('text', 'description', 'Description:', array('size' => 40, 'maxlength' => 45));

        $this->addElement('select', 'admission_term', 'Admission Term:', 
            array('Fall' => 'Fall', 'Spring' => 'Spring', 'Summer' => 'Summer') );
        
        $this->addElement('date', 'admission_year', 'Admission Year:', 
            array('format' => 'Y', 'minYear' => 2007, 'maxYear' => 2021) );
        
        $this->addElement('text', 'start_date', 'Start Date:', 
            array('size' => 20, 'maxlength' => 20, 'class' => 'startDate') );  
            
        $this->addElement('text', 'end_date', 'Close Date:', 
            array('size' => 20, 'maxlength' => 20, 'class' => 'endDate'));   

        $this->addElement('text', 'higher_fee_date', 'Lower Fee End:', 
            array('size' => 20, 'maxlength' => 20, 'id' => 'lowerFeeEndDate') );
            
        $this->addElement('text', 'last_payment_date', 'Last Payment Date:', 
            array('size' => 20, 'maxlength' => 20, 'id' => 'lastPaymentDate') );  
        
        $this->addElement('submit', 'submitPeriod', 'Save');
        $this->addElement('submit', 'submitPeriod', 'Cancel');
                
    }
        
    public function handleSelf(&$period, &$unit) {
    
        $periodId = $period->getId();
        $periodTypeId = $period->getPeriodTypeId();
        $parentPeriodId = $period->getParentPeriodId(); 
        $unitId = $unit->getId();

        $addDeleteButton = FALSE;
        if ( count( $period->getApplicationIds() ) == 0 ) {
            $addDeleteButton = TRUE;    
        }
        
        /*
        * Set the form defaults.
        */
        $defaultValues = $period->exportValues();
        $defaultValues['admission_year'] = array('Y' => $defaultValues['admission_year']);

        if ($periodId) {    
            $this->setDefaults($defaultValues);    
        } else {
            $this->setDefaults( 
                array(
                    'unit_id' => $unitId, 
                    'period_type_id' => $periodTypeId,
                    'parent_period_id' => $parentPeriodId
                ) 
            );    
        }
        
        /*
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitPeriod');
        $deleteValue = $this->getSubmitValue('deletePeriod');

        if ($submitted && $deleteValue == 'Delete') {
            
            $period->delete();
            header( 'Location: https://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . '?unit_id=' . $unit->getId() ); 
            exit;
        }
        
        if ( (!$submitted && $periodId) || $submitValue == 'Cancel' ) {
            
            $this->switchButtons($addDeleteButton);   
            if ($periodId) {
                $this->setConstants($defaultValues);
            }
            $this->freeze();    
        
            return TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       
            
            $this->switchButtons($addDeleteButton);
            $this->freeze();

            $values = $this->exportValues();
            $values['admission_year'] = $values['admission_year']['Y']; 
            $period->importValues($values);
            $period->save();
                  
            if ( $period->isUmbrella() ) {     
                
                $this->updateSubmissionPeriod($period);
                $this->updateEditingPeriod($period); 
                $this->updateSubperiod($period, 7);
                
                if (!$periodId) {
                    
                    // New period, so set defaults.
                    $this->makeDefaultApplicationPrograms($period);
                    $this->makeDefaultProgramGroups($period);
                }              
            }
            
            return TRUE;
        
        } else {
            
            return FALSE;
            
        }          
    }
    
    private function switchButtons($addDeleteButton = FALSE) {
        
        $this->removeElement('submitPeriod');
        $this->removeElement('submitPeriod');
        $this->addElement('submit', 'editPeriod', 'Edit');
        if ($addDeleteButton) {
            $this->addElement('submit', 'deletePeriod', 'Delete', array('id' => 'deletePeriod') );    
        }
            
        return TRUE;        
    }

    private function updateSubmissionPeriod(&$umbrellaPeriod) {
    
        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        } 
        
        $umbrellaPeriodId = $umbrellaPeriod->getId();
        
        $submissionPeriods = $umbrellaPeriod->getChildPeriods(2);
        if ( count($submissionPeriods) > 0) {
            
            $submissionPeriod = $submissionPeriods[0];    
        
        } else {
            
            $submissionPeriod = new Period(NULL, $umbrellaPeriodId, 'subperiod');
            $submissionPeriod->setParentPeriodId( $umbrellaPeriod->getId() );
        }
        $submissionPeriod->setPeriodTypeId(2);
        $submissionPeriod->setUnitId( $umbrellaPeriod->getUnitId() );
        $submissionPeriod->setStartDate( $umbrellaPeriod->getStartDate() );
        $submissionPeriod->save();
        
        return TRUE;
    }

    private function updateEditingPeriod(&$umbrellaPeriod) {
    
        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        } 
        
        $umbrellaPeriodId = $umbrellaPeriod->getId();
        
        $editingPeriods = $umbrellaPeriod->getChildPeriods(3);
        if ( count($editingPeriods) > 0) {
            
            $editingPeriod = $editingPeriods[0];    
        
        } else {
            
            $editingPeriod = new Period(NULL, $umbrellaPeriodId, 'subperiod');
            $editingPeriod->setParentPeriodId( $umbrellaPeriod->getId() );
        }
        $editingPeriod->setPeriodTypeId(3);
        $editingPeriod->setUnitId( $umbrellaPeriod->getUnitId() );
        $editingPeriod->setStartDate( $umbrellaPeriod->getStartDate() );
        $editingPeriod->save();
        
        return TRUE;
    }

    
    private function updateSubperiod(&$umbrellaPeriod, $subperiodTypeId) {

        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        } 
        
        $subPeriods = $umbrellaPeriod->getChildPeriods($subperiodTypeId);
        if ( count($subPeriods) > 0) {
            
            $subPeriod = $subPeriods[0];    
        
        } else {
            
            $subPeriod = new Period(NULL, $umbrellaPeriod->getId(), 'subperiod');
        }
        $subPeriod->setPeriodTypeId($subperiodTypeId);
        $subPeriod->setUnitId( $umbrellaPeriod->getUnitId() );
        $subPeriod->setStartDate( $umbrellaPeriod->getStartDate() );
        $subPeriod->save();
        
        return TRUE;
        
    }
    
    private function makeDefaultApplicationPrograms(&$umbrellaPeriod) {
        
        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        }        

        $periodId = $umbrellaPeriod->getId();
        
        $unit = new Unit( $umbrellaPeriod->getUnitId() );
        
        $latestPeriodId = NULL;
        $unitPeriodIds = $unit->getPeriodIds();
        if ( count($unitPeriodIds) > 0 ) {
            sort($unitPeriodIds);
            foreach ($unitPeriodIds  as $unitPeriodId ) {
                if ($unitPeriodId != $periodId) {
                    $latestPeriodId = $unitPeriodId;    
                }   
            }   
        }

        if ($latestPeriodId) {
        
            // "Clone" the programs from the latest period. 
            $modelPeriod = new Period($latestPeriodId);
            $programIds = $modelPeriod->getProgramIds(); 

            
        } else {
            
            // Default to the unit programs
            $programIds = array_keys( $unit->getPrograms() );
        }
        
        $umbrellaPeriod->setProgramIds($programIds);
        $umbrellaPeriod->save();
 
        
        return TRUE;
    }    

    private function makeDefaultProgramGroups(&$umbrellaPeriod) {

        if ( !$umbrellaPeriod->isUmbrella() ) {
            return FALSE;
        } 

        $periodId = $umbrellaPeriod->getId();
        
        $unit = new Unit( $umbrellaPeriod->getUnitId() );
        
        $latestPeriodId = NULL;
        $unitPeriodIds = $unit->getPeriodIds();
        if ( count($unitPeriodIds) > 0 ) {
            sort($unitPeriodIds);
            foreach ($unitPeriodIds  as $unitPeriodId ) {
                if ($unitPeriodId != $periodId) {
                    $latestPeriodId = $unitPeriodId;    
                }   
            }   
        }

        if ($latestPeriodId) {
            
            // "Clone" the groups from the latest period.          
            $modelPeriod = new Period($latestPeriodId);
            $modelProgramGroups = $modelPeriod->getProgramGroups();
            foreach ($modelProgramGroups as $modelProgramGroup) {
                $newProgramGroup = new ProgramGroup();
                $newProgramGroup->importValues( $modelProgramGroup->exportValues() );
                $newProgramGroup->setId(NULL); 
                $newProgramGroup->setPeriodId($periodId);
                $newProgramGroup->save();     
            }
            
        } else {
            
            // Create bare-bones groups for the unit and child units.
            $programGroup = new ProgramGroup(NULL, $periodId);
            $programGroup->setName( $unit->getName() );
            $programGroup->setNameShort( $unit->getNameShort() );
            $programGroup->setUnitId( $unit->getId() );
            $programIds = array_keys( $unit->getPrograms() );
            $programGroup->setProgramIds($programIds);
            $programGroup->save();
            
            foreach ($unit->getChildUnits() as $childUnit) {
                
                $programGroup = new ProgramGroup(NULL, $periodId);
                $programGroup->setName( $unit->getName() );
                $programGroup->setNameShort( $unit->getNameShort() );
                $programGroup->setUnitId( $unit->getId() );
                $programIds = array_keys( $childUnit->getPrograms() );
                $programGroup->setProgramIds($programIds);
                $programGroup->save();
            }            
        }

        return TRUE;
    }
    
}
?>