<?php
//require("HTML/QuickForm.php");

class NewPeriod_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'newPeriod', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'unit_id');
        $this->addElement('hidden', 'edit', 'newPeriod');
        $this->addElement('submit', 'submitEditNewPeriod', 'Add New Period');
    }

    public function handleSelf(&$unit) {

        $this->setDefaults( array( 'unit_id' => $unit->getId() ) );
        
        if ( $this->isSubmitted() ) {
            $this->updateElementAttr(
                array('submitEditNewPeriod'),
                array('disabled' => 'true')
            );
            $this->freeze();  
        }
        
        return TRUE;    
    }
    
}
?>