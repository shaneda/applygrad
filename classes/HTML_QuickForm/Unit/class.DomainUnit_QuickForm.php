<?php
//require("HTML/QuickForm.php");

class DomainUnit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'domainUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'edit', 'domainUnit');
        $this->addElement('hidden', 'unit_id');
        
        $domainSelect = $this->addElement(
                            'select', 
                            'domain_id', 
                            'Old Domain(s):',
                            null,
                            array('size' => 5, 'multiple' => 'true')
                            );
        $domainSelect->addOption('[none]', '');
        
        $this->addElement('submit', 'submitDomainUnit', 'Save');
        $this->addElement('submit', 'submitDomainUnit', 'Cancel');
    }
    
    public function handleSelf(&$unit) {
        
        $unitId = $unit->getId();
        
        /*
        * Set the select menu options. 
        */
        $this->makeDomainSelect($unitId);

        /*
        * Set the form defaults.
        */
        $unitDomainIds = $unit->getUnitDomainIds();
        $defaultValues['unit_id'] = $unitId;
        if (isset($unitDomainIds)) {
            $defaultValues['domain_id'] = $unitDomainIds;    
        }
        $this->setDefaults($defaultValues);       
        
        /* 
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitDomainUnit');

        if ( !$submitted || $submitValue == 'Cancel' ) {
            
            $this->removeElement('submitDomainUnit');
            $this->removeElement('submitDomainUnit');
            $this->addElement('submit', 'submitDomainUnit', 'Set Old Domain(s)');   
            $this->setConstants($defaultValues);
            $this->freeze();    
        
            return TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       

            $this->removeElement('submitDomainUnit');
            $this->removeElement('submitDomainUnit');
            $this->addElement('submit', 'submitDomainUnit', 'Set Old Domain(s)');   
            $this->freeze();

            $unit->setUnitDomainIds( $this->exportValue('domain_id') );
            $unit->save();
                               
            return TRUE;
        
        } else {
            
            return FALSE;
            
        }

    }

    private function makeDomainSelect($unitId) {

        $domainSelect = $this->getElement('domain_id');
         
        $DB_Domain = new DB_Domain();
        $domains = $DB_Domain->get();
        foreach ($domains as $domain) {
            if ( stristr($domain['name'], 'Archive') === FALSE ) {
                $domainSelect->addOption($domain['name'], $domain['id']);    
            }        
        }

        return TRUE;
    }

}

?>