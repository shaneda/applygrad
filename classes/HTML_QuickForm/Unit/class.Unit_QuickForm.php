<?php
//require("HTML/QuickForm.php");

class Unit_QuickForm extends HTML_QuickForm
{
    
    //private $header;    // HTML_QuickForm_element
    
    function __construct($formName = 'unit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, null, null, true); 
        
        $this->addElement('hidden', 'edit', 'unit');
        $this->addElement('hidden', 'unit_id');
        $this->addElement('text', 'unit_name', 'Full Name:', array('size' => 40, 'maxlength' => 100));
        $this->addElement('text', 'unit_name_short', 'Short Name:', array('size' => 10, 'maxlength' => 20));
        $this->addElement('textarea', 'unit_description', 'Description:', array('rows' => 2, 'cols' => 30));
        $this->addElement('text', 'unit_url', 'URL:', array('size' => 40, 'maxlength' => 255));
        $this->addElement('text', 'unit_oracle_string', 'Oracle String:', array('size' => 30, 'maxlength' => 50));
        $this->addElement('text', 'unit_system_email', 'System E-mail:', array('size' => 30, 'maxlength' => 100));
        $this->addElement('text', 'unit_cc_email', 'CC E-mail:', array('size' => 30, 'maxlength' => 100));
        
        $this->addElement('text', 'application_base_price', 'Base Price:',  
            array('size' => 10, 'maxlength' => 20));
        $this->addElement('text', 'application_program_price', 'Program Price:', 
            array('size' => 10, 'maxlength' => 20));
        
        $programTypeSelect = $this->addElement(
                                'select', 
                                'program_type_id', 
                                'Unit Type:', 
                                null, 
                                array('size' => 1));
        $programTypeSelect->addOption('[program group]', '');
        
        $parentSelect = $this->addElement(
                                'select', 
                                'parent_unit_id', 
                                'Parent Unit:', 
                                null, 
                                array('size' => 1));
        $parentSelect->addOption('[none]', '');

        $this->addElement('submit', 'submitUnit', 'Save');
        $this->addElement('button', 'cancelUnit', 'Cancel', array('onClick' => 'history.go(-1)'));
        
        $this->addRule('unit_name', 'Please enter unit name', 'required', null, 'server', true);
        $this->addRule('unit_name_short', 'Please enter unit short name', 'required', null, 'server', true); 
        
        
        // Create a header, with a generic default value, as the first form element.
        /*
        $this->header = $this->addElement('header', 'header', 'Unit Details');
        */
        
        // Add validation rules.
        /*
        $this->addRule(array('startDate', 'unit', 'unitId', 'admissionPeriodId'),
                'The start date must not overlap with another period', 
                'callback', 'AdmissionPeriod_QuickForm::noPeriodOverlap',
                NULL, NULL, TRUE);
        
        $this->addRule(array('submitDeadline', 'unit', 'unitId', 'admissionPeriodId'),
                'The submission deadline must not overlap with another period', 
                'callback', 'AdmissionPeriod_QuickForm::noPeriodOverlap',
                NULL, NULL, TRUE);
        
        $this->addRule(array('submitDeadline', 'startDate'),
                'The submission deadline must be later than the start date', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('editDeadline', 'submitDeadline'),
                'The editing deadline must be later than the submission deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('viewDeadline', 'editDeadline'),
                'The viewing deadline must be later than the editing deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('admitTermDate', 'viewDeadline'),
                'The admission term date must be later than the viewing deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        */       
        
    }
    

    public function handleSelf(&$unit, $adminUnitIds) {

        $unitId = $unit->getId();
        $unitParentId = $unit->getParentUnitId();
        $unitDescendantIds = $unit->getDescendantUnitIds();
        
        /* 
        * Set the program_type_id select options. 
        */
        $this->makeProgramTypeSelect();
        
        /* 
        * Set the parent_unit_id select options. 
        */
        $this->makeParentUnitSelect($unitId, $unitParentId, 
                    $unitDescendantIds, $adminUnitIds);
        
        /*
        * Set the form defaults.
        */
        $defaultValues = $unit->exportValues();
        if ($unitId) {
            $this->setDefaults($defaultValues);   
        }

        /*
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitUnit');
        
        if ( (!$submitted && $unitId) || $submitValue == 'Cancel' ) {
            
            $this->removeElement('submitUnit');
            $this->removeElement('cancelUnit');
            $this->addElement('submit', 'submitUnit', 'Edit Unit Details');   
            /*
            if ($unitId) {
                $this->setConstants($defaultValues);
            }
            */
            $this->setValues($unit);
            $this->freeze();    
        
            return TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       
            
            $this->removeElement('submitUnit');
            $this->removeElement('cancelUnit');
            $this->addElement('submit', 'editUnit', 'Edit Unit Details');    

            $unit->importValues( $this->exportValues() );
            $unit->save();
            
            $this->setConstants( $unit->exportValues() );
            $this->freeze();
                               
            return TRUE;
        
        } else {
            
            return FALSE;
            
        }
    
    }

    private function makeProgramTypeSelect() {
    
        $programTypeSelect = $this->getElement('program_type_id'); 
        
        $DB_ProgramType = new DB_ProgramType();
        $DB_ProgramTypeRecords = $DB_ProgramType->get(); 
        foreach ($DB_ProgramTypeRecords as $programTypeRecord) {
            $programTypeSelect->addOption($programTypeRecord['program_type'] . ' program', 
                                    $programTypeRecord['program_type_id']);
        }
        
        return TRUE;   
    }

    private function makeParentUnitSelect($unitId, $unitParentId, $unitDescendantIds, $adminUnitIds) {

        $parentUnitSelect = $this->getElement('parent_unit_id');
        
        $DB_Unit = new DB_Unit();
        $unitRecords = $DB_Unit->find(); 
        foreach ($unitRecords as $unitRecord) {
            
            $candidateParentId = $unitRecord['unit_id'];
            $candidateParent = new Unit($candidateParentId);
            
            // Don't give the option of making the unit its own parent 
            // making one of the unit's descendants its parent,
            // making a unit the user doesn't have rights for its parent,
            // or making a program its parent.
            if ( $candidateParentId != $unitId 
                && ( $candidateParentId == $unitParentId || in_array($candidateParentId, $adminUnitIds) )
                && !in_array($candidateParentId, $unitDescendantIds) 
                && ( !$candidateParent->isProgram() || !$candidateParent->getParentUnitId() )  
                ) 
            {    
                $candidateParentName = $candidateParent->getNameShort();   
                $parentUnitSelect->addOption($candidateParentName, $candidateParentId);    
            }         
        }

        return TRUE;
    }


    private function setValues(&$unit) {
    
        $values = $unit->exportValues();

        foreach ($values as $key => $value) {
             
            if ( $this->elementExists($key) ) {
                $element = $this->getElement($key);
                $element->setValue($value);    
            }
                
        }
        
        return TRUE;
    }
    
    
    /*
    public function setHeaderLabel($headerString) {
        $this->header->setText($headerString);
        return TRUE;
    }
    */
    
    
    static function noPeriodOverlap($fieldArrays) {
        
        $timestamp = self::dateArrayToUnixTimestamp($fieldArrays[0]);
        $date = date('Y-m-d', $timestamp);
        $unit = $fieldArrays[1];
        $unitId = $fieldArrays[2];
        $admissionPeriodId = $fieldArrays[3];
        
        global $dbConnection;
        $admissionPeriodTable = new AdmissionPeriod_Table($dbConnection, "AdmissionPeriod");
        $overlappingPeriods = $admissionPeriodTable->findDateOverlap($date, $unit, $unitId, $admissionPeriodId);
        $overlapCount = count($overlappingPeriods);

        if ($overlapCount == 0) {
            return TRUE;   
        } else {
            return FALSE;    
        }
        
    }    


    static function firstDateLater($dateElementArrays) {
        
        $firstDate = self::dateArrayToUnixTimestamp($dateElementArrays[0]);
        $secondDate = self::dateArrayToUnixTimestamp($dateElementArrays[1]);
        
        if ($firstDate > $secondDate) {
            return TRUE;   
        } else {
            return FALSE;    
        }
        
    }
    
    static function dateArrayToUnixTimestamp($dateElementArray) {
        
        $timestamp = mktime(0, 0, 0, $dateElementArray['m'], $dateElementArray['d'], $dateElementArray['Y']);
        return $timestamp;
    }
    
}

?>
