<?php
/*
* Form to send request to start a new unit form/record. 
*/
//require("HTML/QuickForm.php");

class NewUnit_QuickForm extends HTML_QuickForm
{

    public function __construct($formName = 'newUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'edit', 'newUnit');
        $this->addElement('submit', 'submitEditNewUnit', 'Add New Unit');
    }
    
    public function handleSelf() {

        if ( $this->isSubmitted() ) {
            $this->updateElementAttr(
                array('submitEditNewUnit'),
                array('disabled' => 'true')
            );
            $this->freeze();  
        }
        
        return TRUE;        
    }

}
?>