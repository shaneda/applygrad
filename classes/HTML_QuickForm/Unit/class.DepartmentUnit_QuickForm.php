<?php
//require("HTML/QuickForm.php");

class DepartmentUnit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'departmentUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true);
        
        $this->addElement('hidden', 'edit', 'departmentUnit');
        $this->addElement('hidden', 'unit_id');
        
        $programSelect = $this->addElement(
                            'select', 
                            'department_id', 
                            'Old Department:',
                            null,
                            array('size' => 5)
                            );
        $programSelect->addOption('[none]', '');
        
        $this->addElement('submit', 'submitDepartmentUnit', 'Save');
        $this->addElement('submit', 'submitDepartmentUnit', 'Cancel');
    }
    
    public function handleSelf(&$unit) {
        
        $unitId = $unit->getId();
        
        /*
        * Set the select menu options. 
        */
        $this->makeDepartmentSelect($unitId);

        /*
        * Set the form defaults.
        */
        $unitDepartmentId = $unit->getUnitDepartmentId();
        $defaultValues['unit_id'] = $unitId;
        if (isset($unitDepartmentId)) {
            $defaultValues['department_id'] = $unitDepartmentId;    
        }
        $this->setDefaults($defaultValues);        
        
        /* 
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitDepartmentUnit');

        if ( !$submitted || $submitValue == 'Cancel' ) {
            
            $this->removeElement('submitDepartmentUnit');
            $this->removeElement('submitDepartmentUnit');
            $this->addElement('submit', 'submitDepartmentUnit', 'Set Department');   
            $this->setConstants($defaultValues);
            $this->freeze();    
        
            return TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       

            $this->removeElement('submitDepartmentUnit');
            $this->removeElement('submitDepartmentUnit');
            $this->addElement('submit', 'submitDepartmentUnit', 'Set Department');   
            $this->freeze();

            $unit->setUnitDepartmentId( $this->exportValue('department_id') );
            $unit->save();
                               
            return TRUE;
        
        } else {
            
            return FALSE;
            
        }
        
    }

    private function makeDepartmentSelect($unitId) {

        $departmentSelect = $this->getElement('department_id');
        
        $DB_Department = new DB_Department();
        $departments = $DB_Department->get();
        foreach ($departments as $department) {
            if ( stristr($department['name'], 'Archive') === FALSE ) {
                $departmentSelect->addOption($department['name'], $department['id']);    
            }        
        }

        return TRUE;
    }

}

?>