<?php
//require("HTML/QuickForm.php");

class ProgramUnit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'programUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true);
        
        $this->addElement('hidden', 'edit', 'programUnit');
        $this->addElement('hidden', 'unit_id');
        
        $programSelect = $this->addElement(
                            'select', 
                            'programs_id', 
                            'Old Program:',
                            null,
                            array('size' => 5)
                            );
        $programSelect->addOption('[none]', '');
        
        $this->addElement('submit', 'submitProgramUnit', 'Save');
        $this->addElement('submit', 'submitProgramUnit', 'Cancel');
    }
    
    public function handleSelf(&$unit) {
        
        $unitId = $unit->getId();
        
        /*
        * Set the select menu options. 
        */
        $this->makeProgramSelect($unitId);

        /*
        * Set the form defaults.
        */
        $unitProgramId = $unit->getUnitProgramId();
        $defaultValues['unit_id'] = $unitId;
        if (isset($unitProgramId)) {
            $defaultValues['programs_id'] = $unitProgramId;    
        }
        $this->setDefaults($defaultValues); 
        
        /* 
        * Process the form.
        */
        $submitted = $this->isSubmitted();
        $submitValue = $this->getSubmitValue('submitProgramUnit');

        if ( !$submitted || $submitValue == 'Cancel' ) {
            
            $this->removeElement('submitProgramUnit');
            $this->removeElement('submitProgramUnit');
            $this->addElement('submit', 'submitProgramUnit', 'Set Old Program');   
            $this->setConstants($defaultValues);
            $this->freeze();    
        
            return TRUE;
        }
        
        if ( $submitted && $submitValue == 'Save' && $this->validate() ) {       

            $this->removeElement('submitProgramUnit');
            $this->removeElement('submitProgramUnit');
            $this->addElement('submit', 'submitProgramUnit', 'Set Old Program');   
            $this->freeze();

            $unit->setUnitProgramId( $this->exportValue('programs_id') );
            $unit->save();
                               
            return TRUE;
        
        } else {
            
            return FALSE;
            
        }
        
    }

    private function makeProgramSelect($unitId) {

        $programSelect = $this->getElement('programs_id');
        
        $DB_Programs = new DB_Programs();
        $programs = $DB_Programs->getWithLookup();
        foreach ($programs as $program) {
            if ( stristr($program['department'], 'Archive') === FALSE ) {
                $programSelect->addOption($program['name'], $program['id']);    
            }        
        }
           
        return TRUE;
    }

}

?>