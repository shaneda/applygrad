<?php
require("HTML/QuickForm.php");

class AdmissionPeriod_QuickForm extends HTML_QuickForm
{
    
    private $header;    // HTML_QuickForm_element
    
    function __construct($formName = '', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action);
        
        // Create a header, with a generic default value, as the first form element.
        $this->header = $this->addElement('header', 'header', 'Admission Period');
        
        $renderer = $this->defaultRenderer();
        $renderer->setHeaderTemplate(
           '<tr>
              <td colspan="2" class="tblHead">{header}</td>
           </tr>'  
        );
        
        // Add validation rules.
        $this->addRule(array('startDate', 'unit', 'unitId', 'admissionPeriodId'),
                'The start date must not overlap with another period', 
                'callback', 'AdmissionPeriod_QuickForm::noPeriodOverlap',
                NULL, NULL, TRUE);
        
        $this->addRule(array('submitDeadline', 'unit', 'unitId', 'admissionPeriodId'),
                'The submission deadline must not overlap with another period', 
                'callback', 'AdmissionPeriod_QuickForm::noPeriodOverlap',
                NULL, NULL, TRUE);
        
        $this->addRule(array('submitDeadline', 'startDate'),
                'The submission deadline must be later than the start date', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('editDeadline', 'submitDeadline'),
                'The editing deadline must be later than the submission deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('viewDeadline', 'editDeadline'),
                'The viewing deadline must be later than the editing deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);
        
        $this->addRule(array('admitTermDate', 'viewDeadline'),
                'The admission term date must be later than the viewing deadline', 
                'callback', 'AdmissionPeriod_QuickForm::firstDateLater',
                NULL, NULL, TRUE);       
        
    }
    
    
    public function setHeaderLabel($headerString) {
        $this->header->setText($headerString);
        return TRUE;
    }
    
    static function noPeriodOverlap($fieldArrays) {
        
        $timestamp = self::dateArrayToUnixTimestamp($fieldArrays[0]);
        $date = date('Y-m-d', $timestamp);
        $unit = $fieldArrays[1];
        $unitId = $fieldArrays[2];
        $admissionPeriodId = $fieldArrays[3];
        
        global $dbConnection;
        $admissionPeriodTable = new AdmissionPeriod_Table($dbConnection, "AdmissionPeriod");
        $overlappingPeriods = $admissionPeriodTable->findDateOverlap($date, $unit, $unitId, $admissionPeriodId);
        $overlapCount = count($overlappingPeriods);

        if ($overlapCount == 0) {
            return TRUE;   
        } else {
            return FALSE;    
        }
        
    }    


    static function firstDateLater($dateElementArrays) {
        
        $firstDate = self::dateArrayToUnixTimestamp($dateElementArrays[0]);
        $secondDate = self::dateArrayToUnixTimestamp($dateElementArrays[1]);
        
        if ($firstDate > $secondDate) {
            return TRUE;   
        } else {
            return FALSE;    
        }
        
    }
    
    static function dateArrayToUnixTimestamp($dateElementArray) {
        
        $timestamp = mktime(0, 0, 0, $dateElementArray['m'], $dateElementArray['d'], $dateElementArray['Y']);
        return $timestamp;
    }
    
}

?>
