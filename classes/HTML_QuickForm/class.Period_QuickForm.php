<?php
//require("HTML/QuickForm.php");

class Period_QuickForm extends HTML_QuickForm
{
    
    //private $header;    // HTML_QuickForm_element
    
    function __construct($formName = 'period', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, null, null, true); 
        
        $this->addElement('hidden', 'unit_id');
        $this->addElement('hidden', 'period_id');
        $this->addElement('text', 'description', 'Description:', array('size' => 40, 'maxlength' => 45));
        $this->addElement('text', 'start_date', 'Start Date:', array('size' => 10, 'maxlength' => 20));
        $this->addElement('text', 'end_date', 'Start Date:', array('size' => 10, 'maxlength' => 20));
        
        $parentSelect = $this->addElement(
                                'select', 
                                'parent_period_id', 
                                'Parent Unit:', 
                                null, 
                                array('size' => 5));
        $parentSelect->addOption('[none]', 'NULL');
        
        $this->addElement('submit', 'submitPeriod', 'Save');
        $this->addElement('submit', 'submitPeriod', 'Cancel');
        
        /*
        $this->addRule('unit_name', 'Please enter unit name', 'required', null, 'server', true);
        $this->addRule('unit_name_short', 'Please enter unit short name', 'required', null, 'server', true); 
        */
        
    }
        
    
}
?>