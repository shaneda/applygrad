<?php
//require("HTML/QuickForm.php");

class DomainUnit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'domainUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'edit', 'domainUnit');
        $this->addElement('hidden', 'unit_id');
        
        $domainSelect = $this->addElement(
                            'select', 
                            'domain_id', 
                            'Corresponds to domain(s):',
                            null,
                            array('size' => 5, 'multiple' => 'true')
                            );
        $domainSelect->addOption('[none]', '');
        
        $this->addElement('submit', 'submitDomainUnit', 'Save');
        $this->addElement('submit', 'submitDomainUnit', 'Cancel');
    }
    
}

?>