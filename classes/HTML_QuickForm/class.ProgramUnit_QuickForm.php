<?php
//require("HTML/QuickForm.php");

class ProgramUnit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'programUnit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true);
        
        $this->addElement('hidden', 'edit', 'programUnit');
        $this->addElement('hidden', 'unit_id');
        
        $programSelect = $this->addElement(
                            'select', 
                            'programs_id', 
                            'Corresponds to program:',
                            null,
                            array('size' => 5)
                            );
        $programSelect->addOption('[none]', '');
        
        $this->addElement('submit', 'submitProgramUnit', 'Save');
        $this->addElement('submit', 'submitProgramUnit', 'Cancel');
    }
    
}

?>