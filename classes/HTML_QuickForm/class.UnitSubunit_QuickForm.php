<?php
//require("HTML/QuickForm.php");

class UnitSubunit_QuickForm extends HTML_QuickForm
{

    function __construct($formName = 'unitSubunit', $method = 'post', $action = '') {

        parent::__construct($formName, $method, $action, '', null, true); 
        
        $this->addElement('hidden', 'unit_id');
        $this->addElement(
                    'select', 
                    'subunit_id', 
                    'Subunits:',
                     null,
                     array('size' => 5, 'multiple' => 'true')
                     );
        $this->addElement('submit', 'submitUnitSubunit', 'Save');
        $this->addElement('submit', 'submitUnitSubunit', 'Cancel');
    }
    
}
?>