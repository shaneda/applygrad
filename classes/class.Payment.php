<?php

class Payment 
{

    private $DB_Payment;
    private $DB_PaymentItem;
    
    private $id;
    private $applicationId;
    private $paymentType;
    private $amount;
    private $intentDate;
    private $status;
    private $lastModUserId;
    
    private $paymentItems = array();
    
    public function __construct($paymentId = NULL, $applicationId = NULL) {       
        
        $this->DB_Payment = new DB_Payment();
        $this->DB_PaymentItem = new DB_PaymentItem();
        
        $this->id = $paymentId;
        $this->applicationId = $applicationId;
        
        if ($paymentId) {
            $this->loadFromDb();
        }
    }
    
    public function getId() {
        return $this->id;
    }

    public function getApplicationId() {
        return $this->applicationId;
    }    

    public function setApplicationId($applicationId) {
        $this->applicationId = $applicationId;
    } 

    public function getPaymentType() {
        return $this->paymentType;    
    }

    public function getAmount() {
        return $this->amount;
    }
    
    public function setAmount($amount) {
        $this->amount = $amount;
        return TRUE;
    }
    
    public function getIntentDate() {
        return $this->intentDate;
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return TRUE;
    }
    
    public function getLastModUserId() {
        return $this->lastModUserId;
    }
    
    public function getPaymentItems() {
        return $this->paymentItems;
    }

    public function setPaymentItems( $paymentItems = array() ) {
        $this->paymentItems = $paymentItems;
        return TRUE;
    }
    
    public function addPaymentItem( $paymentItem = array() ) {
        $this->paymentItems[] = $paymentItem;
        return TRUE;    
    }

    public function loadFromDb() {
    
        $paymentArray = $this->DB_Payment->get($this->id);    
        $paymentItemArray = $this->DB_PaymentItem->find($this->id);
        
        if ( isset($paymentArray[0]) ) {
        
            $this->applicationId = $paymentArray[0]['application_id'];
            $this->paymentType = $paymentArray[0]['payment_type'];
            $this->amount = $paymentArray[0]['payment_amount'];
            $this->intentDate = $paymentArray[0]['payment_intent_date'];
            $this->status = $paymentArray[0]['payment_status'];
            $this->lastModUserId = $paymentArray[0]['last_mod_user_id'];
            $this->paymentItems = $paymentItemArray;
            
            return TRUE; 
            
        } else {
        
            return FALSE;
        }
            
    }
    
    public function importValues( $paymentRecord = array() ) {
    
        if ( isset($paymentRecord['payment_id']) ) {
            $this->id = $paymentRecord['payment_id'];
        }

        if ( isset($paymentRecord['application_id']) ) {
            $this->applicationId = $paymentRecord['application_id'];
        }

        if ( isset($paymentRecord['payment_type']) ) {
            $this->paymentType = $paymentRecord['payment_type'];
        }

        if ( isset($paymentRecord['payment_amount']) ) {
            $this->amount = $paymentRecord['payment_amount'];
        }
        
        if ( isset($paymentRecord['payment_intent_date']) ) {
            $this->intentDate = $paymentRecord['payment_intent_date'];
        }

        if ( isset($paymentRecord['payment_status']) ) {
            $this->status = $paymentRecord['payment_status'];
        }

        if ( isset($paymentRecord['last_mod_user_id']) ) {
            $this->lastModUserId = $paymentRecord['last_mod_user_id'];
        }
        
        return TRUE;
    }
    
    public function save() {
        
        $paymentRecord = array(
            'payment_id' => $this->id,
            'application_id' => $this->applicationId,
            'payment_type' => $this->paymentType,
            'payment_amount' => $this->amount,
            'payment_intent_date' => $this->intentDate,
            'payment_status' => $this->status,
            'last_mod_user_id' => $this->lastModUserId
            );
        
        $this->id = $this->DB_Payment->save($paymentRecord);
    
        foreach ($this->paymentItems as $paymentItemRecord) {
            
            $paymentItemRecord['payment_id'] = $this->id;    
            $this->DB_PaymentItem->save($paymentItemRecord);    
        }
    
        return TRUE;
    }
    
}
?>