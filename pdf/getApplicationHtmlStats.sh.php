#!/usr/local/bin/php5
<?
// Turn error reporting off.
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 'Off');

// Get the uid from the first command line argument.
$applicationId = $argv[1];

// Get the db from the second command line argument.
$dbMode = 'stats';
if (isset($argv[2]) && $argv[2] == 'ssn') {
    $dbMode = 'ssn';    
}

$applicationHtmlIncludeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/'); 

include $applicationHtmlIncludeBase . '../inc/commandlineConfig.inc.php';
$_SESSION['datafileroot'] = $datafileroot;
$_SESSION['A_allow_admin_edit'] = TRUE;
$_SESSION['A_userid'] = NULL;
$_SESSION['A_usertypeid'] = NULL;

/*
* For remote merge, change the db config and set the remote data dir base 
* to point to the production server.
*/
$remoteMerge = FALSE;
//$remoteMerge = TRUE; 
if ($remoteMerge) {
    $remoteDirectoryBase = '/usr0/apache2/htdocs/data/2009/';
    $db_host = "web05.srv.cs.cmu.edu";
    $db_username = "remote";
    $db_password = "r3m0t3us3r";
    
    // SSN/SASE is in their own database.
    if ($dbMode == 'ssn') {
        $db = "gradAdmissionsSSNProd";    
    } else {
        $db = "gradAdmissions2009New";    
    }
}


//include_once $applicationHtmlIncludeBase . '../inc/db_connect.php';
mysql_connect($db_host,$db_username,$db_password) or errordb("Couldn't connect to URA database.");
mysql_select_db($db) or errordb("Couldn't Select Database");

ob_start();
$exclude_scriptaculous = TRUE;
include_once $applicationHtmlIncludeBase . '../inc/functions.php';
include_once $applicationHtmlIncludeBase . '../inc/applicationFunctions.php';

// Include the data queries, variables.
include $applicationHtmlIncludeBase . '../inc/printViewData.inc.php';
$notices = ob_get_contents(); 
ob_end_clean();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Application of <?=$email?></title>
</head>
<body bgcolor="#ffffff">

<?php
// include big table
$mainTableWidth = '100%';
include $applicationHtmlIncludeBase . '../inc/tpl.printView.php';

// include recommend form answers
if (!isset($depts)) {
    $depts = array();
}
include $applicationHtmlIncludeBase . '../inc/printRecommendform.inc.php'; 
?>

</body>
</html>