#!/usr/bin/php
<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
ini_set('display_errors', 1); 
ini_set('memory_limit', '512M');

$convertIncludeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');

include $convertIncludeBase . '../inc/commandlineConfig.inc.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_VW_Applyweb.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.VW_AdminList.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.VW_AdminListBase.php'; 
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_Datafileinfo.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_Period.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include $convertIncludeBase . '../classes/class.Period.php';
include $convertIncludeBase . '../classes/class.FileConverter.php';
require_once $convertIncludeBase . '../pdf/dompdf-0.5.1/dompdf_config.inc.php';

$vw_adminListBase = new VW_AdminListBase();
$candidateApplicationIds = array();

$forceConvert = FALSE;
$convertUnsubmitted = FALSE; 
$convertSingleApplication = FALSE;
$convertDepartment = FALSE;
$singleApplicationId = NULL;
$singleDepartmentId = NULL;

/*
if ( isset($argc) && $argc > 1 ) { 
    if ($argv[1] < 100) {
        $convertDepartment = TRUE;    
    } else {
        $convertSingleApplication = TRUE;    
    }    
}
*/
if ( isset($argc) && $argc > 1 ) { 

    if ( $argc == 3 && $argv[1] == '-d' ) {
        
        $convertDepartment = TRUE;    
        $singleDepartmentId = intval($argv[2]);
        echo "convert department " . $singleDepartmentId . "\n"; 
    
    } elseif ( $argc == 3 && $argv[1] == '-a' ) {
    
        $convertSingleApplication = TRUE;    
        $singleApplicationId = intval($argv[2]);
        echo "convert application " . $singleApplicationId . "\n"; 
    
    } else {
        
        echo "Valid arguments: \n";
        echo " -a [applicationId] \n";
        echo " -d [departmentId]\n";
        exit;       
    }
}

if ($convertSingleApplication) {      

    //$candidateApplicationIds[0] = $argv[1];
    $candidateApplicationIds[0] = $singleApplicationId;
    $convertUnsubmitted = TRUE; 

} else {    

    $departmentApplicationIds = array();
    if ($convertDepartment) {
        
        /*
        $deptAppIdQuery = "SELECT DISTINCT lu_application_programs.application_id
                                            FROM lu_application_programs
                                            INNER JOIN lu_programs_departments
                                            ON lu_application_programs.program_id = lu_programs_departments.program_id
                                            WHERE lu_programs_departments.department_id = " . $argv[1];    
        */
        $deptAppIdQuery = "SELECT DISTINCT lu_application_programs.application_id
                                            FROM lu_application_programs
                                            INNER JOIN lu_programs_departments
                                            ON lu_application_programs.program_id = lu_programs_departments.program_id
                                            WHERE lu_programs_departments.department_id = " . $singleDepartmentId; 
        $departmentApplicationIds = array_keys( $vw_adminListBase->handleSelectQuery($deptAppIdQuery, 'application_id') );
    }
    
    // Get all periods
    $dbPeriod = new DB_Period();
    $periodRecords = $dbPeriod->get();
    foreach ($periodRecords as $periodRecord) {       
        // Only instantiate umbrella periods.
        if ($periodRecord['period_type_id'] == 1) {
            $period = new Period($periodRecord['period_id']);    
        } else {
            continue;
        }
        // Only get applications from active periods
        if ($period->getStatus() == 'active') {
            
            $periodApplicationIds = $period->getApplications();
            
            if ($convertDepartment)  {
                
                foreach ($periodApplicationIds as $periodApplicationId) {
                    if ( in_array($periodApplicationId, $departmentApplicationIds) ) {
                        $candidateApplicationIds[] = $periodApplicationId;    
                    }
                }
                
            } else {
            
                $candidateApplicationIds = array_merge($candidateApplicationIds, $periodApplicationIds);    
            }
        }       
    }
}
//print_r($candidateApplicationIds);



foreach ($candidateApplicationIds as $applicationId) {
    // $applicantRecord is a 2D array.
    $applicantRecord = $vw_adminListBase->find(NULL, NULL, NULL, $applicationId);
    print_r($applicantRecord);
    if (count($applicantRecord) == 0) {
        continue;    
    }    
    
    // Skip unsubmitted applications.
    if (!$convertUnsubmitted && $applicantRecord[0]['sbmd'] != 'yes') {
        continue;
    }

    // Set up an empty merge record.   
    $applicantLuuId = $applicantRecord[0]['user_id'];
    $applicantGuid = $applicantRecord[0]['guid']; 

    $mergeRecord = array(
        'application_id' => $applicationId,
        'guid' => $applicantGuid,
        'error' => 0,
        'message' => '',
        'base_conversion_needed' => 0,
        'base_write_error' => 0,
        'base_write_message' => '',
        'base_convert_error' => 0,
        'base_convert_message' => '',
        'base_html_file' => '',
        'base_pdf_file' => '',
        );
    
    $docpath = $convertIncludeBase . $datafileroot . '/' . $applicantGuid;
    echo $docpath;
    if ( !file_exists($docpath) ) {
        continue;    
    } else {
        $docpath .= '/'; 
    }
    
    $baseHtmlFile = $docpath . $applicationId . '_application_base.html';
    $basePdfFile =  $docpath . $applicationId . '_application_base.pdf';
    $mergeRecord['base_html_file'] = $baseHtmlFile;
    $mergeRecord['base_pdf_file'] = $basePdfFile;
    
    if ( !file_exists($basePdfFile)) {
        $forceConvert = TRUE;   
    }
 
    // Handle the base application.
    $baseHtml = getApplicationHtml($applicationId, $convertIncludeBase);
    if ( stringTextfileDifferent($baseHtml, $baseHtmlFile) || $forceConvert ) {       
        $mergeRecord['base_conversion_needed'] = 1;
        $baseHtmlWritten = FileConverter::string2file($baseHtml, $baseHtmlFile);

        if ($baseHtmlWritten) {             
            chmod($baseHtmlFile, 0775);
            if ($hostname == "WEB28.SRV.CS.CMU.EDU" ||
	       $hostname == "AMERICANMAID.SRV.CS.CMU.EDU" ||
           $hostname == "THETICK.SRV.CS.CMU.EDU") {
                chown($baseHtmlFile, 'www-data');    
            } else {
                chown($baseHtmlFile, 'wwwsrv');    
            }
            chgrp($baseHtmlFile, 'wwwdev');
            $baseHtmlConversion = FileConverter::html2pdf($baseHtmlFile, $basePdfFile);
            if ($baseHtmlConversion['error']) {
                // Try again with legal size.
                $baseHtmlConversion = FileConverter::html2pdf($baseHtmlFile, $basePdfFile, '11x17');    
            }
            if ($baseHtmlConversion['error']) {
                $mergeRecord['error'] = 1;
                $mergeRecord['message'] = 'Base HTML conversion failed';
                $mergeRecord['base_convert_error'] = 1;
                $mergeRecord['base_convert_message'] = implode(' ', $baseHtmlConversion['output']);    
            } else {
                chmod($basePdfFile, 0775);
		 if ($hostname == "WEB28.SRV.CS.CMU.EDU" ||
                       $hostname == "AMERICANMAID.SRV.CS.CMU.EDU" ||
                       $hostname == "THETICK.SRV.CS.CMU.EDU") {
                        chown($baseHtmlFile, 'www-data');
			chgrp($basePdfFile, 'wwwdev');
		      } else {
		                chown($basePdfFile, 'wwwsrv');
                		chgrp($basePdfFile, 'wwwdev');
                             }  
	           }
        }
      	   else {
            $mergeRecord['error'] = 1;
            $mergeRecord['message'] = 'Base HTML write failed: ' . $baseHtmlFile;
            $mergeRecord['base_write_error'] = 1;
            $mergeRecord['base_write_message'] = 'Base HTML write failed: ' . $baseHtmlFile;           
        }        
    }
    
    print_r($mergeRecord);
}

 
// ############## Functions #####################
 
function getApplicationHtml($applicationId, $basePath = '') {
    $path = $basePath . '../pdf/getApplicationHtml.sh.php'; 
    exec($path . ' ' . $applicationId, $output, $error);       
    if ($error) {
        return FALSE;
    } else {
        $htmlString = implode("\n", $output); 
        return $htmlString;    
    } 
}


function stringTextfileDifferent($instring, $infile) {
    
    $stringHash = hash("md5", $instring);
    $fileHash = '';
    if ( file_exists($infile) && realpath($infile) ) {    
        $fileHash = hash_file('md5', $infile);
    }
    if ($stringHash != $fileHash) {    
        return TRUE;
    } else {
        return FALSE;     
    }  
}
?>