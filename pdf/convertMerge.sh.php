#!/usr/bin/php5
<?php
error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
ini_set('display_errors', 1); 

/*
* NOTE: This script can now update the production server when run from a development machine (e.g., rogue).
* 
* When run "remotely", the script will rsync datafiles from the production server to the dev server,
* do the conversion on the dev server, write the conversion messages to the production db,
* and rsync the datafiles back to the production machine.
* 
* IMPORTANT: for remote merge to work, the script must be owned(?) and run by root. 
*/

ini_set('memory_limit', '512M');

$forcePs = FALSE;        // Convert all PDFs to PS before merge?
$forceMerge = FALSE;
$forceConvert = FALSE;
$handleOoService = TRUE;

$convertIncludeBase = str_replace('//', '/', realpath( dirname(__FILE__) ) . '/');

include $convertIncludeBase . '../inc/commandlineConfig.inc.php';

/*
* For remote merge, change the db config and set the remote data dir base 
* to point to the production server.
*/
$remoteMerge = FALSE;
//$remoteMerge = TRUE;
if ($remoteMerge) {
    $db_host = "banshee.srv.cs.cmu.edu";
    $db_username = "phdapp";
    $db_password = "phd321app";
    $db = "gradAdmissions2009New";
    $remoteDirectoryBase = '/usr0/wwwsrv/htdocs/data/';
    //$db = "gradAdmissions2008Test";
    //$remoteDirectoryBase = '/usr0/wwwsrv/htdocs/_test/data/2009/';    
}

include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_Applyweb.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_VW_Applyweb.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.VW_AdminList.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.VW_AdminListBase.php'; 
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_Datafileinfo.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_Period.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include $convertIncludeBase . '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include $convertIncludeBase . '../classes/class.Period.php';
include $convertIncludeBase . '../classes/class.FileConverter.php';
require_once $convertIncludeBase . '../pdf/dompdf-0.5.1/dompdf_config.inc.php';
//include $convertIncludeBase . '../classes/class.Process.php'; 

$candidateApplicationIds = array();
$convertUnsubmitted = FALSE;

$convertSingleApplication = FALSE;
$convertDepartment = FALSE;
$singleApplicationId = NULL;
$singleDepartmentId = NULL;

/*
if ( isset($argc) && $argc > 1 ) { 
    if ($argv[1] < 100) {
        $convertDepartment = TRUE;    
    } else {
        $convertSingleApplication = TRUE;    
    }    
}
*/
if ( isset($argc) && $argc > 1 ) { 

    if ( $argc == 3 && $argv[1] == '-d' ) {
        
        $convertDepartment = TRUE;    
        $singleDepartmentId = intval($argv[2]);
        echo "convert department " . $singleDepartmentId . "\n"; 
    
    } elseif ( $argc == 3 && $argv[1] == '-a' ) {
    
        $convertSingleApplication = TRUE;    
        $singleApplicationId = intval($argv[2]);
        echo "convert application " . $singleApplicationId . "\n"; 
    
    } else {
        
        echo "Valid arguments: \n";
        echo " -a [applicationId] \n";
        echo " -d [departmentId]\n";
        exit;       
    }
}

if ($convertSingleApplication) {      

    // Convert a single applicaton.
    //$candidateApplicationIds[0] = $argv[1];    
    $candidateApplicationIds[0] = $singleApplicationId;
    $convertUnsubmitted = TRUE; 
    $forceMerge = TRUE;
    $forceConvert = TRUE;
    $convertUnsubmitted = TRUE;

} else {    

    $departmentApplicationIds = array();
    if ($convertDepartment) {
        $DB_Applyweb = new DB_Applyweb();
/*
        $deptAppIdQuery = "SELECT DISTINCT lu_application_programs.application_id
                                            FROM lu_application_programs
                                            INNER JOIN lu_programs_departments
                                            ON lu_application_programs.program_id = lu_programs_departments.program_id
                                            WHERE lu_programs_departments.department_id = " . $argv[1];    
*/
        $deptAppIdQuery = "SELECT DISTINCT lu_application_programs.application_id
                                            FROM lu_application_programs
                                            INNER JOIN lu_programs_departments
                                            ON lu_application_programs.program_id = lu_programs_departments.program_id
                                            WHERE lu_programs_departments.department_id = " . $singleDepartmentId;
        $departmentApplicationIds = array_keys( $DB_Applyweb->handleSelectQuery($deptAppIdQuery, 'application_id') );
    }
    
    // Get all periods
    $dbPeriod = new DB_Period();
    $periodRecords = $dbPeriod->get();
    foreach ($periodRecords as $periodRecord) {       
        // Only instantiate umbrella periods.
        if ($periodRecord['period_type_id'] == 1) {
            $period = new Period($periodRecord['period_id']);    
        } else {
            continue;
        }
        // Only get applications from active periods
        if ($period->getStatus() == 'active') {
            
            $periodApplicationIds = $period->getApplications();
            
            if ($convertDepartment)  {
                
                foreach ($periodApplicationIds as $periodApplicationId) {
                    if ( in_array($periodApplicationId, $departmentApplicationIds) ) {
                        $candidateApplicationIds[] = $periodApplicationId;    
                    }
                }
                
            } else {
            
                $candidateApplicationIds = array_merge($candidateApplicationIds, $periodApplicationIds);    
            }
        }       
    }
}
//print_r($candidateApplicationIds);

$vw_adminListBase = new VW_AdminListBase();  
$db_datafileinfo = new DB_Datafileinfo();

$mergeDate = date("Y-m-d H:i:s", time());
$i = 0;
//DebugBreak();
if ($handleOoService) {
    FileConverter::startOoService();    
}
foreach ($candidateApplicationIds as $applicationId) {

    // Temporary: only do the first 5 applications.
    /*
    if ($i > 5) {
        break;    
    }
    */ 
    
    // $applicantRecord is a 2D array.
    $applicantRecord = $vw_adminListBase->find(NULL, NULL, NULL, $applicationId);
    
    if (count($applicantRecord) == 0) {
        continue;    
    }    
    
    // Skip unsubmitted applications.
    if (!$convertUnsubmitted && $applicantRecord[0]['sbmd'] != 'yes') {
        continue;
    }
        
    // Set up an empty merge record.   
    $applicantLuuId = $applicantRecord[0]['user_id'];
    $applicantGuid = $applicantRecord[0]['guid']; 

    $mergeRecord = array(
        'application_id' => $applicationId,
        'guid' => $applicantGuid,
        'merge_date' => $mergeDate,
        'merge_needed' => FALSE,
        'merged' => 0,
        'error' => 0,
        'message' => '',
        'base_write_error' => 0,
        'base_write_message' => '',
        'base_convert_error' => 0,
        'base_convert_message' => '',
        'search_text_write_error' => 0,
        'search_text_write_message' => '',
        'merge_file' => '',
        'base_pdf_file' => '',
        'datafiles' => array(
                'statement' => array(),
                'resume' => array(),
                'experience' => array(),
                'transcript' => array(),
                'grescore' => array(),
                'gresubjectscore' => array(),
                'toefliscore' => array(),
                'toeflcscore' => array(),
                'toeflpscore' => array(),
                'ieltsscore' => array(),
                'recommendation' => array()
            )
        );


    // Skip "bad" applications (i.e., that have known merge problems).
    // (Comment out the id of an application you want to convert/merge singly.)
    $badApplications = array(
        1705,
        6478,
        6863,
        7116,
        7204,
        7241,
        7410,
        7411,
        7482,
        7502,
        7628,
        7788,
        8161,
        8341,
        8496,
        8748,
        8789,
        8914,
        9689,
        9772,
        9857,
        10483,   
        10869,  // 2010 for 2011 applications start here
        10873,
        11055,
        11078,
        11373,
        11401,
        11406,
        11757,
        11758,
        11809,
        11931,
        12194,
        12346,
        12530,
        12592,
        12656,
        12694,
        12722,
        12855,
        12906,
        12968,
        13129,
        13198,
        13267,
        13606,
        13678,
        13895,  // 2011 for 2012 applications start after here
        15459,
        15533,
        15835,
        16099,
        16382,
        16528,
        16864,
        16968,
        17104,
        18238,
        18358
        );

    if ( in_array($applicationId, $badApplications) ) {
        $mergeRecord['message'] = 'Skipped known problem application';
        saveMergeRecord($vw_adminListBase, $mergeRecord);
        continue;
    }
    
    // Skip missing data directory (realpath() returns FALSE if file doesn't exist).
    //$docpath = realpath($datafileroot . '/' . $applicantGuid);
    $docpath = $convertIncludeBase . $datafileroot . '/' . $applicantGuid;
    
    /*
    * Rsync the production directory to the dev server.
    */    
    if ($remoteMerge) {
        $rsyncDownStatus = rsyncDown($docpath, $applicantGuid, $remoteDirectoryBase);
        if ($rsyncDownStatus['error']) {
            $mergeRecord['error'] = 1;
            $mergeRecord['message'] = 'rsync down failed: ' . implode(' ', $rsyncDownStatus['output']);
            saveMergeRecord($vw_adminListBase, $mergeRecord);
            continue;     
        }        
    }
    
    if ( !file_exists($docpath) ) {
        $mergeRecord['error'] = 1;
        $mergeRecord['message'] = 'Data directory missing: ' . $datafileroot . '/' . $applicantGuid; 
        saveMergeRecord($vw_adminListBase, $mergeRecord);
        continue;    
    } else {
        $docpath .= '/'; 
    }
    
    $baseHtmlFile = $docpath . $applicationId . '_application_base.html';
    $basePdfFile =  $docpath . $applicationId . '_application_base.pdf';
    $mergeRecord['base_pdf_file'] = $basePdfFile;
    $mergeFileName = $applicationId . '_merged.pdf';
    $mergeFile = $docpath . $mergeFileName;
    $mergeRecord['merge_file'] = $mergeFile; 
 
    // Handle the base application.
    $baseHtml = getApplicationHtml($applicationId, $convertIncludeBase);
    if ( stringTextfileDifferent($baseHtml, $baseHtmlFile) || $forceMerge ) {       
        $mergeRecord['merge_needed'] = TRUE;
        $baseHtmlWritten = FileConverter::string2file($baseHtml, $baseHtmlFile);       
        if ($baseHtmlWritten) {             
            chmod($baseHtmlFile, 0770);
            chown($baseHtmlFile, 'wwwsrv');
            chgrp($baseHtmlFile, 'wwwdev');
            $baseHtmlConversion = FileConverter::html2pdf($baseHtmlFile, $basePdfFile);
            if ($baseHtmlConversion['error']) {
                // Try again with legal size.
                $baseHtmlConversion = FileConverter::html2pdf($baseHtmlFile, $basePdfFile, '11x17');    
            }
            if ($baseHtmlConversion['error']) {
                $mergeRecord['error'] = 1;
                $mergeRecord['message'] = 'Base HTML conversion failed';
                $mergeRecord['base_convert_error'] = 1;
                $mergeRecord['base_convert_message'] = implode(' ', $baseHtmlConversion['output']);    
            } else {
                chmod($basePdfFile, 0770);
                chown($basePdfFile, 'wwwsrv');
                chgrp($basePdfFile, 'wwwdev');
            }  
        } else {
            $mergeRecord['error'] = 1;
            $mergeRecord['message'] = 'Base HTML write failed: ' . $baseHtmlFile;
            $mergeRecord['base_write_error'] = 1;
            $mergeRecord['base_write_message'] = 'Base HTML write failed: ' . $baseHtmlFile;           
        }        
    }
    
    if ($mergeRecord['error']) {
        saveMergeRecord($vw_adminListBase, $mergeRecord);
        //print_r($mergeRecord);
        continue;    
    }
    
    // Handle the datafiles.
    //$datafileinfos = $db_datafileinfo->find($applicantLuuId);
    // Added applicationId to keep from getting docs from same user's other apps.
    $datafileinfos = $db_datafileinfo->find($applicantLuuId, $applicationId);
    foreach ($datafileinfos as $datafileinfo) {        
        
        $isOrphanFile = FALSE;
        $datafileId = $datafileinfo['id'];
        switch ($datafileinfo['section']) {               
            case 1:
                $datafileType = 'transcript';
                $isOrphanFile = isOrphanFile($applicationId, $datafileId, $datafileType);
                break;
            case 2:
                $datafileType = 'resume';
                break;    
            case 3:
                $datafileType = 'recommendation';
                break;
            case 4:
                $datafileType = 'statement';
                break;
            case 5:
                $datafileType = 'experience';
                break;
            case 6:
                $datafileType = 'grescore';
                break;
            case 7:
                $datafileType = 'toefliscore';
                break;
            case 8:
                $datafileType = 'gresubjectscore';
                break;
            case 9:
                $datafileType = 'toeflcscore';
                break;
            case 10:
                $datafileType = 'toeflpscore';
                break;
            case 19:
                $datafileType = 'ieltsscore';
                break;
            case 20:
                $datafileType = 'gmatscore';
                break;
            default:
                $datafileType = $datafileinfo['section'];    
        }
        
        if ($isOrphanFile) {
            continue;
        }
        
        $filename = $datafileType . '_' . $datafileinfo['userdata'] . '.' . $datafileinfo['extension'];
        $filepath = $docpath . $datafileType . '/'; 
        $infile = $filepath . $filename;
        $outfile = $filepath . str_replace("." . $datafileinfo['extension'], ".pdf", $filename);
        
        $datafileRecord = array(
            'datafileinfo_id' => $datafileId,
            'datafile' => $infile,
            'converted' => 0,
            'convert_error' => 0,
            'convert_message' => '',
            'merge_filename' => $outfile,
            'merged' => 0,
            'merge_error' => 0,
            'merge_message' => ''
        );
        
        if ( !file_exists($infile) ) {
            $datafileRecord['convert_error'] = 1;
            $datafileRecord['convert_message'] = 'File missing';
            $datafileRecord['merge_error'] = 1;
            $datafileRecord['merge_message'] = 'File missing';
            $mergeRecord['datafiles'][$datafileType][] = $datafileRecord; 
            continue;
        }
         
        /*
        * Look for pdfs from converted docx files and skip the conversion
        * if they're available. 
        */
        if ( $datafileinfo['extension'] == 'docx'
            && file_exists($outfile) 
            && filemtime($outfile) > $datafileinfo['moddate'] ) 
        {
            $datafileRecord['convert_message'] = 'No change since previous merge';
            $mergeRecord['datafiles'][$datafileType][] = $datafileRecord;
            continue;         
        }
        
        
        if ( file_exists($outfile) && !datafileChange($datafileinfo, $mergeFile) && !$forceConvert ) {
            $datafileRecord['convert_message'] = 'No change since previous merge';
            $mergeRecord['datafiles'][$datafileType][] = $datafileRecord;
            continue;    
        } else {
            $mergeRecord['merge_needed'] = TRUE;    
        }

        switch($datafileinfo['extension']) {
            case "txt":
                $status = FileConverter::text2pdf($infile, $outfile);
                break;                    
            case "doc":
            case "docx":
            case "rtf":
                $status = FileConverter::doc2pdf($infile, $outfile);
                break;
            case "gif":
            case "jpg":
                $status = FileConverter::image2pdf($infile, $outfile);
                break; 
            case "pdf":
                $status = array('error' => 0, 'output' => array('native PDF'));
                break;    
            default:
                $status = array('error' => 1, 'output' => array('Unkown file type'));             
        }
        
        $datafileRecord['convert_message'] = implode(' ', $status['output']);
        
        if ($status['error']) {
            $datafileRecord['convert_error'] = 1;
            $datafileRecord['merge_error'] = 1;
            $datafileRecord['merge_message'] = 'Conversion error';
        } else {
            if ($datafileinfo['extension'] == 'pdf') {
                $datafileRecord['converted'] = 0;    
            } else {
                $datafileRecord['converted'] = 1;
                chmod($outfile, 0770);
                chown($outfile, 'wwwsrv');
                chgrp($outfile, 'wwwdev');                
            }
        }
        
        $mergeRecord['datafiles'][$datafileType][] = $datafileRecord;
    }
    

    // Handle the merge.
    $mergeFileBak = $mergeFile . '.bak';
    $mergeStatus = array('error' => 0, 'output' => array());
    if ($mergeRecord['merge_needed']) {
        if ( file_exists($mergeFile) ) {
            rename($mergeFile, $mergeFileBak);
        }
        $mergeStartTime = time();
        $mergeStatus = merge($mergeRecord);
        $mergeEndTime = time();
        $mergeTime = $mergeEndTime - $mergeStartTime . 's ';
        $mergeRecord['message'] = $mergeTime . $mergeRecord['message'];
        $mergeRecord['merged'] = 1;    
    }
    
    if ($mergeStatus['error']) {
        if ( file_exists($mergeFile) ) {
            unlink($mergeFile);
        }
        if ( file_exists($mergeFileBak) ) {
            rename($mergeFileBak, $mergeFile);
        }
        $mergeRecord['error'] = $mergeStatus['error'];
        $mergeRecord['message'] .= implode(' ', $mergeStatus['output']);
    } else {
        if ( file_exists($mergeFileBak) ) {
            unlink($mergeFileBak);
        }
        chmod($mergeFile, 0770);
        chown($mergeFile, 'wwwsrv');
        chgrp($mergeFile, 'wwwdev');
        $searchTextStatus = saveSearchText($vw_adminListBase, $mergeRecord);
        if ($searchTextStatus['error']) {
            $mergeRecord['search_text_write_error'] = 1;
            $mergeRecord['search_text_write_message'] = implode(' ', $searchTextStatus['output']);    
        }

        if ($remoteMerge) {
            $rsyncUpStatus = rsyncUp($docpath, $mergeFileName, $applicantGuid, $remoteDirectoryBase);
            if ($rsyncUpStatus['error']) {
                $mergeRecord['error'] = 1;
                $mergeRecord['message'] = 'rsync up failed: ' . implode(' ', $rsyncUpStatus['output']);    
            }          
        }  
    }
    
    if (!$mergeRecord['merge_needed'] && !$mergeRecord['error']) {
        $mergeRecord['message'] = 'No change since last merge';
    }
    saveMergeRecord($vw_adminListBase, $mergeRecord);
    //print_r($mergeRecord);
    $i++;
}
if ($handleOoService) {
    FileConverter::stopOoService();    
}
 

// ############## Functions #####################
 
function getApplicationHtml($applicationId, $basePath = '') {

    $path = $basePath . '../pdf/getApplicationHtml.sh.php'; 
    exec($path . ' ' . $applicationId, $output, $error);       
    if ($error) {
        return FALSE;
    } else {
        $htmlString = implode("\n", $output); 
        return $htmlString;    
    } 
}


function stringTextfileDifferent($instring, $infile) {
    
    $stringHash = hash("md5", $instring);
    $fileHash = '';
    if ( file_exists($infile) && realpath($infile) ) {    
        $fileHash = hash_file('md5', $infile);
    }
    if ($stringHash != $fileHash) {    
        return TRUE;
    } else {
        return FALSE;     
    }  
}


function datafileChange($datafileinfo, $mergeFile) {

    $fileExists = file_exists($mergeFile);
    $realPath = realpath($mergeFile);
    if ( !$fileExists && !$realPath ) {
        return TRUE;
    } 
    
    $lastMergeTimestamp = filemtime($mergeFile);
    $datafileTimestamp = @strtotime($datafileinfo['moddate']);
    if ( $datafileTimestamp - $lastMergeTimestamp > 60 ) {   
        return TRUE;
    } else {
        return FALSE;
    }
}


function merge(&$mergeRecord) {
    
    $output = $error = '';
    $status = array();

    $baseMergeCommand = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite';
    $baseMergeCommand .= ' -sOUTPUTFILE=' . $mergeRecord['merge_file'];
    $baseMergeCommand .= ' -dCompatibilityLevel=1.4 -r600 -dEncryptionR=3 -dPermissions=-4';
    //$baseMergeCommand .= ' -dPDFSETTINGS=/ebook -dColorImageResolution=72 -dGrayImageResolution=72'; 
    $baseMergeCommand .= ' -dPDFSETTINGS=/ebook ';      // good
    //$baseMergeCommand .= ' -dPDFSETTINGS=/printer ';  // better
    //$baseMergeCommand .= ' -dPDFSETTINGS=/prepress '; // best
    //$baseMergeCommand .= ' -dPDFSETTINGS=/ebook -dColorImageResolution=300 -dGrayImageResolution=300'; 
    //$baseMergeCommand .= ' -dDownsampleColorImages=false -dDownsampleGrayImages=false'; 
    $baseMergeCommand .= ' -dEmbedAllFonts=true -c .setpdfwrite -f '; 
    
    // Try progressively merging each datafile.
    $doFinalMerge = TRUE;
    /*
    $lastGoodMerge = str_replace('application_base.pdf', 'last_good_merge.pdf', $mergeRecord['base_pdf_file']); 
    copy($mergeRecord['base_pdf_file'], $lastGoodMerge);
    $mergeArgs = $lastGoodMerge . ' ';
    */
    $mergeArgs = $mergeRecord['base_pdf_file'] . ' ';
    foreach ($mergeRecord['datafiles'] as $datafileType => $datafileArray) {
        $datafileCount = count($datafileArray);
        for ($i = 0; $i < $datafileCount; $i++) {
            if ($datafileArray[$i]['convert_error']) {
                continue;
            }
            $mergeFile = $datafileArray[$i]['merge_filename'];
            $mergeStatus = handleMergeStep($baseMergeCommand, $mergeArgs, $mergeFile);
            $mergeArgs = $mergeStatus['merge_args'];
            if ($mergeStatus['error']) {
                $doFinalMerge = TRUE;
                $mergeRecord['datafiles'][$datafileType][$i]['merge_error'] = $mergeStatus['error'];
                $mergeRecord['datafiles'][$datafileType][$i]['merge_message'] = 
                    implode(' ', $mergeStatus['output']);    
            } else {
                $doFinalMerge = FALSE;
                //copy($mergeRecord['merge_file'], $lastGoodMerge);
                $mergeRecord['datafiles'][$datafileType][$i]['merge_filename'] = $mergeStatus['last_merge_arg'];
                $mergeRecord['datafiles'][$datafileType][$i]['merged'] = 1;    
            } 
        }
    }
    
    // Do the final merge if there was an error on the last try.
    $mergeCommand = $baseMergeCommand . ' ' . $mergeArgs . ' 2>&1'; 
    if ($doFinalMerge) {  
        exec($mergeCommand, $output, $error);    
    }
    echo $mergeCommand; 
    
    // Trying to use process class - can't get standard error.
    /*
    $mergeCommand = $baseMergeCommand . ' ' . $mergeArgs;
    echo $mergeCommand . "\n";
    $mergeProcess = new Process($mergeCommand, 120);
    $output = $mergeProcess->getOutput();
    $error = $mergeProcess->getError();
    */
    $status = array('error' => $error, 'output' => $output);
    
    return $status;
}


function handleMergeStep($baseMergeCommand, $previousMergeArgs, $newDatafile) {
 
    $status = array();
    
    $stepStartTime = time();
    
    // UNCOMMENT THIS to force all pdfs to be converted to ps for merge.
    /* 
    * NEW: SET VARIABLE ON LINE 16
    */
    global $forcePs;
    if ($forcePs) {
        if ( strpos($newDatafile, '.pdf') > 0 ) { 
            $psFile = str_replace('.pdf', '.ps', $newDatafile); 
            $psStatus = FileConverter::pdf2ps($newDatafile, $psFile);
            if (!$psStatus['error']) {
                $newDatafile = $psFile;
            }
        }
    }
    
    $newMergeArgs = $previousMergeArgs . ' ' . $newDatafile;
    $mergeCommand = $baseMergeCommand . ' ' . $newMergeArgs . ' 2>&1';
    exec($mergeCommand, $output, $error); 
    //echo $mergeCommand . "\n";
    
    // Trying to use process class - can't get standard error.
    /*
    $mergeCommand = $baseMergeCommand . ' ' . $newMergeArgs;
    echo $mergeCommand . "\n";
    $mergeProcess = new Process($mergeCommand, 120);
    $output = $mergeProcess->getOutput();
    $error = $mergeProcess->getError();
    */
    
    if ($error) {
        if ( strpos($newDatafile, '.pdf') > 0 ) {
            // Try to convert the problem file to ps with the xpdf tool and try the merge again
            $psFile = str_replace('.pdf', '.ps', $newDatafile); 
            $psStatus = FileConverter::pdf2ps($newDatafile, $psFile);
            if ($psStatus['error']) {               
                $status = array('error' => $error, 'output' => $output, 'merge_args' => $previousMergeArgs, 'last_merge_arg' => $psFile);                
            } else {               
                $status = handleMergeStep($baseMergeCommand, $previousMergeArgs, $psFile);
            }
        } else {
            $status = array('error' => $error, 'output' => $output, 'merge_args' => $previousMergeArgs, 'last_merge_arg' => $newDatafile);
        }
    } else {
        $status = array('error' => $error, 'output' => $output, 'merge_args' => $newMergeArgs, 'last_merge_arg' => $newDatafile);    
    }
    
    return $status;
}

function saveSearchText (&$db_applyweb, &$mergeRecord) {
    
    $status = array('error' => 0, 'output' => array());
    
    $mergeFile = $mergeRecord['merge_file'];
    $conversionStatus = FileConverter::pdf2text($mergeFile);
    if ($conversionStatus['error']) {
        return $conversionStatus;
    }
    
    $textFile = str_replace(".pdf", ".txt", $mergeFile);
    chmod($textFile, 0770);
    chown($textFile, 'wwwsrv');
    chgrp($textFile, 'wwwdev');
    $textString = shell_exec('strings ' . $textFile);
    $textUpdateQuery = "INSERT INTO searchText (application_id, guid, application_text) 
            VALUES (" . $mergeRecord['application_id'] . ",' "
             . $mergeRecord['guid'] . "', '" . $db_applyweb->escapeString($textString)."') "
             . " ON DUPLICATE KEY UPDATE application_text = '" . $db_applyweb->escapeString($textString) . "'";
    $recordCount = $db_applyweb->handleUpdateQuery($textUpdateQuery);
    if ($recordCount == -1) {
        $status['error'] = 1;
        $status['output'][0] = 'Search text update failed';
    }
    
    return $status;
}


function saveMergeRecord(&$db_applyweb, &$mergeRecord) {
    
    $mergeQuery = "INSERT INTO merge 
                    (merge_date, application_id, merged, error, message)
                    VALUES ('" . $mergeRecord['merge_date'] . "', "
                    . $mergeRecord['application_id'] . ", "
                    . $mergeRecord['merged'] . ", "
                    . $mergeRecord['error'] . ", " 
                    . "'" . $db_applyweb->escapeString($mergeRecord['message']) . "')";
    $recordCount = $db_applyweb->handleInsertQuery($mergeQuery);
    if ($recordCount != 1) {
        return FALSE;
    }

    if ($mergeRecord['merged'] && !$mergeRecord['error']) {
        $applicationMergefileQuery = "INSERT INTO application_merge_file VALUES ("
                                        . $mergeRecord['application_id'] . ", "
                                        . "'" . $mergeRecord['guid'] . "', "
                                        . "'" . $mergeRecord['merge_file'] . "', "
                                        . "'" . $mergeRecord['merge_date'] . "' "
                                        . ") ON DUPLICATE KEY UPDATE 
                                        merge_date = '" . $mergeRecord['merge_date'] . "'";
        $recordCount = $db_applyweb->handleUpdateQuery($applicationMergefileQuery);
        if ($recordCount == -1) {
             return FALSE;
        }    
    }
    
    //  Don't add records to merge_application, merge_datafile, failed_merges
    //  on no-merge, no-error (i.e., no change since last merge).
    if (!$mergeRecord['merged'] && !$mergeRecord['error']) {
        return TRUE;
    }

    $mergeIdQuery = "SELECT last_insert_id() AS merge_id";
    $mergeIdRecords = $db_applyweb->handleSelectQuery($mergeIdQuery);
    $mergeId = $mergeIdRecords[0]['merge_id']; 
    
    $mergeApplicationQuery = "INSERT INTO merge_application
                    (merge_id, base_write_error, base_write_message, 
                    base_convert_error, base_convert_message,
                    search_text_write_error, search_text_write_message)
                    VALUES (" . $mergeId . ", "
                    . $mergeRecord['base_write_error'] . ", "
                    . "'" . $db_applyweb->escapeString($mergeRecord['base_write_message']) . "', "
                    . $mergeRecord['base_convert_error'] . ", " 
                    . "'" . $db_applyweb->escapeString($mergeRecord['base_convert_message']) . "', "
                    . $mergeRecord['search_text_write_error'] . ", " 
                    . "'" . $db_applyweb->escapeString($mergeRecord['search_text_write_message']) . "')";
    $recordCount = $db_applyweb->handleInsertQuery($mergeApplicationQuery);
    if ($recordCount != 1) {
        return FALSE;
    }
    
    $failedMerges = array();
    foreach ($mergeRecord['datafiles'] as $datafileArray) {
        
        foreach ($datafileArray as $datafile) {
            
            if ($datafile['merge_error']) {
                $failedMerges[] = $datafile['datafile'];    
            }
            
            $mergeDatafileQuery = "INSERT INTO merge_datafile (merge_id, datafileinfo_id, 
                    converted, convert_error, convert_message, 
                    merge_filename, merged, merge_error, merge_message)
                    VALUES (" . $mergeId . ", "
                    . $datafile['datafileinfo_id'] . ", "
                    . $datafile['converted'] . ", "
                    . $datafile['convert_error'] . ", "
                    . "'" . $db_applyweb->escapeString($datafile['convert_message']) . "', "
                    . "'" . $datafile['merge_filename'] . "', " 
                    . $datafile['merged'] . ", "
                    . $datafile['merge_error'] . ", " 
                    . "'" . $db_applyweb->escapeString($datafile['merge_message']) . "')";
            $recordCount = $db_applyweb->handleInsertQuery($mergeDatafileQuery); 
            
            if ($recordCount != 1) {
                return FALSE;
            }     
        }
    }
    
    // Always delete failed merge records in case this merge had no failures.
    $failedDeleteQuery = "DELETE FROM failed_merges WHERE application_id=" . $mergeRecord['application_id'];
    $recordCount = $db_applyweb->handleInsertQuery($failedDeleteQuery); 
    
    // Do inserts for failures in this merge.
    if ( $mergeRecord['merged'] && count($failedMerges) > 0 ) {       
        foreach ($failedMerges as $failedMerge) {       
            $failedInsertQuery = "INSERT INTO failed_merges VALUES ("
                                . $mergeRecord['application_id'] . ", "
                                . "'" . $mergeRecord['guid'] . "', "
                                . "'" . $failedMerge . "')";
            $recordCount = $db_applyweb->handleInsertQuery($failedInsertQuery);
            if ($recordCount != 1) {
                return FALSE;
            }
        }    
    }
    
    return TRUE;
}

function rsyncDown($docpath, $guid, $productionDirectoryBase) {

    $user = get_current_user();
    
    $source = $productionDirectoryBase . $guid . '/';
    $target = $docpath;
    
    $command = 'rsync -azv -e ssh ';
    $command .= $user . '@banshee.srv.cs.cmu.edu:'. $source . ' ' . $target;
    $command .= ' 2>&1';
    //echo '*****' . $command . '*****';
    //echo "\n";

    exec($command, $output, $error);        
    $status = array('error' => $error, 'output' => $output);
    return $status;

}

function rsyncUp($docpath, $mergeFileName, $guid, $productionDirectoryBase) {

    $user = get_current_user();
    
    $source = $docpath . $mergeFileName;
    $target = $productionDirectoryBase . $guid . '/' . $mergeFileName;
    
    $command = 'rsync -azv -e ssh ';
    $command .= $source . ' ' . $user . '@banshee.srv.cs.cmu.edu:' . $target;
    $command .= ' 2>&1';
    //echo '*****' . $command . '*****';
    //echo "\n";

    exec($command, $output, $error);        
    $status = array('error' => $error, 'output' => $output);
    return $status;
   
}

function isOrphanFile($applicationId, $datafileId, $datafileType) {
    
    global $vw_adminListBase;
    
    $isOrphanFile = FALSE;
    
    switch ($datafileType) {
        
        case 'transcript':
        
            $query = "SELECT id FROM usersinst 
                        WHERE application_id = " . $applicationId . "
                        AND datafile_id = " . $datafileId;    
            break;
        
        default:
        
            $query = "";
            break;   
    }
    
    if ($query) {
        $dbRecord = $vw_adminListBase->handleSelectQuery($query);
        if ( count($dbRecord) == 0 ) {
            $isOrphanFile = TRUE;
        }
    }
    
    return $isOrphanFile;
}

?>