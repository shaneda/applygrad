// When the document loads do everything inside here ...
$(document).ready(function(){

        /*
        //  Function to control loading/unloading of content from menu links
        */
        $('.menu a[href=""]').click(function() { //start function when any link is clicked

            // set some variables for testing
            var selected_link_td = $(this).parent();
            var selected_link_tr = $(selected_link_td).parent();
            var selected_content_tr = $(selected_link_tr).next();
            //var selected_content_div = $(selected_link_tr).next().find(".content"); 
            
            // if "closing" an open tab/section and not opening a new one 
            if ( $(selected_link_td).hasClass("selected") ) {
            
                // remove the content row
                $(selected_content_tr).remove();
                
                // remove the "selected" status of the menu tr and td
                $(selected_link_td).removeClass("selected");
                $(selected_link_tr).removeClass("selected");
                
                // return the original row and link style
                $(selected_link_tr).children().css("border", "");
                $(selected_link_tr).children().css("background-color", "");
                $(this).css("color", "");
                $(this).css("text-decoration", "");           
             
            } else {
            
                // changing view in same record or changing record
            
                // turn off style to "close" open tabs
                $("td").css("border", "");
                $("td").css("background-color", ""); 
                $(".menu a").css("text-decoration", "underline");
                $(".menu a").css("color", "");
                $(".menu td").removeClass("selected");
                
                // set variable for checking to see if staying in same menu
                var old_menu_row = $("tr[class='menu selected']");

                // if selecting different section of same record
                if ( $(selected_link_tr).hasClass("selected") ) {
                
                    // identify the content div of interest 
                    var selected_content_div = $(selected_link_tr).next().find(".content");
                
                    // empty the content div
                    $(".content").html("");
                
                } else {
                
                    // switching to new record

                    // remove the old content row
                    $(old_menu_row).next().remove();
                    
                    // de-select old menu row
                    $(old_menu_row).removeClass("selected");
                    
                    // add a row/div beneath the present one
                    var bgcolor = $(selected_link_tr).attr("bgcolor");
                    $(selected_link_tr).after("<tr bgcolor="+bgcolor+"><td colspan=\"9\"><div class=\"content\" /></td></tr>");
                    
                    // select the new menu row                
                    $(selected_link_tr).addClass("selected"); 
                    
                    // set content variables based on new row
                    var selected_content_tr = $(selected_link_tr).next();
                    var selected_content_div = $(selected_link_tr).next().find(".content"); 
                
                } 

                // highlight the selected tab/link            
                $(selected_link_td).addClass("selected");
                $(selected_link_tr).children().css("border-top", "thin solid black");
                $(selected_content_tr).children().css("border-bottom", "thin solid black");
                $(selected_link_tr).children().css("background-color", "#FFFFFF");
                $(selected_content_tr).children().css("background-color", "#FFFFFF");  
                $(this).css("text-decoration", "none");
                $(this).css("color", "black");
            
                // get the page and application id from the element id, e.g. 193_payment
                var request_id = $(this).attr("id");
                var request_arguments = request_id.split("_");
                var application_id = request_arguments[0];
                var page = request_arguments[1];
            
                // submit the request
                $.ajax({
                    method: "get",url: "admin_single.php",data: "application_id="+application_id+"&page="+page,
                    success: function(html){ //so, if data is retrieved, store it in html
                        $(selected_content_div).show();
                        $(selected_content_div).html(html);
                        } // close success
                }); //close $.ajax(
            
            } // close if/else
            return false;
        
        }); //close click(
     
}); // close $(document).ready