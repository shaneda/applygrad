/*
// "Functions" to bind form elements with click events to update db with ajax requests.
// They depend on the livequery extension to bind events to elements
// created/modified after the DOM is initially loaded.
*/

$(document).ready(function(){

    /*
    // Set/Unset Fee Waived
    */
    $(".fee_waived").livequery("click" , function(event){

        // get application id from checkbox element id
        var applicationId = $(this).attr("id").split("_")[0];
        
        // check whether the box is checked        
        if ( $(this).is(":checked") ) {
            var feeWaived = 1;
            var msg = "Fee Waived";
            var menuMsg = "<span class=\"confirm_complete\">waived</span>";
        } else {
            var feeWaived = 0;
            var msg = "Fee Reinstated";
            var totalFees = $("#"+applicationId+"_total_fees_span").text();
            var menuMsg = "<span class=\"confirm\">due: " + totalFees + "</span>";
        }
        
        //get variables to reload form
        
        // below was for checkbox in menu row
        //var selected_content_div = $(this).parents("tr").next().find(".content");
        // this is for checkbox on payment page
        var selected_content_div = $("#"+applicationId+"_content_div");
        
        if  ( $(selected_content_div).is(":visible") ) {
            // below was for checkbox in menu row
            //var page = $(this).parents("tr").next().find("form").attr("id").split("_")[1];
            // this is for checkbox on payment page
            var page = $(this).parents("form").attr("id").split("_")[1];
        } else {
            var page = "";
        }
        
        
        $.get("admin_single_update.php", { mode: "fee_waived", application_id: applicationId, fee_waived: feeWaived },
            function(data){
                if (page == "payment") {
                    // update menu row
                    var paid_msg_id =  applicationId + "_paymentamt_msg";
                    $("#"+paid_msg_id).html(menuMsg);
                    // reload div
                    reloadForm(applicationId, page, $(selected_content_div));
                }                                
                alert(msg);
        
            },
            "text");

    }); // close click 

    
    /*
    // Set/Unset Payment Paid
    */
    $(".fee_paid").livequery("click" , function(event){

        // get application id from checkbox element id
        var applicationId = $(this).attr("id").split("_")[0];
        
        // get total fees amount
        var totalFees = $("#"+applicationId+"_total_fees_span").text();
        
        // check whether the box is checked        
        var msg = "Payment updated: ";
        if ( $(this).is(":checked") ) {
            var feePaid = 1;
            var amt = totalFees;
            msg = msg + "Paid";
            var menuMsg = "<span class=\"confirm_complete\"><b>paid</b></span>: <span class=\"confirm\">" + totalFees + "</span>";
        } else {
            var feePaid = 0;
            var amt = "NULL";
            msg = msg + "Unpaid";
            var menuMsg = "<span class=\"confirm\">due: " + totalFees + "</span>";
        }
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        

        
        $.get("admin_single_update.php", { mode: "fee_paid", application_id: applicationId, fee_paid: feePaid, amt: amt },
            function(data){
                // refresh payment message in menu row
                var paid_msg_id =  applicationId + "_paymentamt_msg";
                $("#"+paid_msg_id).html(menuMsg); 
                // reload div
                reloadForm(applicationId, page, $(selected_content_div));                
                alert(msg);          
            },
            "text");

    
    }); // close click     

    
    /*
    // Update Payment Amount
    */
    $(".update_payment_submit").livequery("click" , function(event){

        // set payment amount variable from form element
        var paymentAmount = $(this).prev().val();
        
        // get application id from checkbox element id
        var applicationId = $(this).attr("id").split("_")[0];
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get("admin_single_update.php", { mode: "payment_amount", application_id: applicationId, payment_amount: paymentAmount },
            function(data){
            
                // refresh payment messages in menu row
                //var paid_msg_id =  applicationId + "_paid_msg";
                //$("#"+paid_msg_id).text("paid");
                var paymentamt_msg_id =  applicationId + "_paymentamt_msg";
                var payment_amt_msg = "<span class=\"confirm_complete\">paid</span>: <span class=\"confirm\">" + parseInt(paymentAmount).toFixed(2) + "</span>";
                $("#"+paymentamt_msg_id).html(payment_amt_msg); 
                
                // reload div
                reloadForm(applicationId, page, $(selected_content_div));
                
                // set confirmation message
                var msg = "Payment updated: payment = " + parseInt(paymentAmount).toFixed(2);
                alert(msg);          
            },
            "text");

         return false;
    }); // close click     

     
    /*
    // Reset Payment
    */
    $(".reset_payment_submit").livequery("click" , function(event){
        
        // get application id from checkbox element id
        var applicationId = $(this).attr("id").split("_")[0];
        
        // get total fees amount
        var totalFees = $("#"+applicationId+"_total_fees_span").text();
        var menuMsg = "<span class=\"confirm\">due: " + totalFees + "</span>";
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
    
        // set confirmation message
        var msg = "Payment Reset";
        
        $.get("admin_single_update.php", { mode: "payment_reset", application_id: applicationId },
            function(data){
                
                // refresh payment messages in menu row
                //var paid_msg_id =  applicationId + "_paid_msg";
                //$("#"+paid_msg_id).text("");
                var paymentamt_msg_id =  applicationId + "_paymentamt_msg";
                $("#"+paymentamt_msg_id).html(menuMsg); 
                
                // reload content div
                reloadForm(applicationId, page, $(selected_content_div));
                alert(msg);          
            },
            "text");

         return false;
    }); // close click 

    
    /*
    // Set/Unset Transcript Received
    */
    $(".transcript_received").livequery("click" , function(event){

        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];  
        
        // get transcript id from checkbox element id 
        var transcriptId = $(this).attr("id").split("_")[0];
    
        // check whether the box is checked        
        var msg = "Transcript " + transcriptId + " Updated: "
        if ( $(this).is(":checked") ) {
            var transcriptReceived = 1;
            msg = msg + "Received";
        } else {
            var transcriptReceived = 0;
            msg = msg + "Not received";
        }    
                
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get("admin_single_update.php", { mode: "transcript_received", transcript_id: transcriptId, transcript_received: transcriptReceived },
            function(data){
                
                // update the count in the menu row
                var ct_transcripts_submitted_msg_id = applicationId + "_ct_transcripts_submitted_msg";
                var ct_transcripts_submitted = parseInt( $("#"+ct_transcripts_submitted_msg_id).text() );
                if (transcriptReceived){
                    ct_transcripts_submitted = ct_transcripts_submitted + 1;
                } else {
                    ct_transcripts_submitted = ct_transcripts_submitted - 1;
                }
                $("#"+ct_transcripts_submitted_msg_id).text(ct_transcripts_submitted);
                
                // reload the div
                reloadForm(applicationId, page, $(selected_content_div));                
                alert(msg);         
            },
            "text");
    
    }); // close click       

          
    /*
    // Set/Unset GRE General Score Received
    */
    $(".gre_general_received").livequery("click" , function(event){

        // check whether the box is checked        
        var msg = "GRE General Updated: ";
        if ( $(this).is(":checked") ) {
            var scoreReceived = 1;
            msg = msg + "Received";
        } else {
            var scoreReceived = 0;
            msg = msg + "Not received";
        }
    
        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];  
        
        // get gre score id from checkbox element id 
        var grescoreId = $(this).attr("id").split("_")[0];
                
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get("admin_single_update.php", { mode: "gre_general_received", grescore_id: grescoreId, score_received: scoreReceived },
            function(data){
                
                // update the menu row message
                var gre_received_msg_id = applicationId + "_gre_received_msg";
                if (scoreReceived){
                    $("#"+gre_received_msg_id).text("rcd");
                } else {
                    $("#"+gre_received_msg_id).text("");
                }
                               
                // reload the content div
                reloadForm(applicationId, page, $(selected_content_div));                
                alert(msg);         
            },
            "text");
    
    }); // close click

    
    /*
    // Update GRE General Score
    */
    $(".update_gre_general_score_submit").livequery("click" , function(event){

        // set score variables from form elements
        var verbalScore = $(this).parents(".gre_general_score").find(".gre_general_verbal_score").val();
        var verbalPercentile = $(this).parents(".gre_general_score").find(".gre_general_verbal_percentile").val();
        var quantitativeScore = $(this).parents(".gre_general_score").find(".gre_general_quantitative_score").val();
        var quantitativePercentile = $(this).parents(".gre_general_score").find(".gre_general_quantitative_percentile").val();
        var analyticalScore = $(this).parents(".gre_general_score").find(".gre_general_analytical_score").val();
        var analyticalPercentile = $(this).parents(".gre_general_score").find(".gre_general_analytical_percentile").val();
        var writingScore = $(this).parents(".gre_general_score").find(".gre_general_writing_score").val();
        var writingPercentile = $(this).parents(".gre_general_score").find(".gre_general_writing_percentile").val();
        
        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];
        
        // get score id from submit element id 
        var grescoreId = $(this).attr("id").split("_")[0]; 
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
    
        $.get("admin_single_update.php", 
                { mode: "gre_general_score", grescore_id: grescoreId, 
                verbal_score: verbalScore,
                verbal_percentile: verbalPercentile,
                quantitative_score: quantitativeScore,
                quantitative_percentile: quantitativePercentile,
                analytical_score: analyticalScore,
                analytical_percentile: analyticalPercentile,
                writing_score: writingScore,
                writing_percentile: writingPercentile},
            function(data){
            
                reloadForm(applicationId, page, $(selected_content_div));
                alert("GRE general score updated.");           
            },
            "text");

         return false;
    }); // close click 

    
    /*
    // Set/Unset GRE Subject Score Received
    */
    $(".gre_subject_received").livequery("click" , function(event){

        // check whether the box is checked        
        var msg = "GRE Subject Updated: ";
        if ( $(this).is(":checked") ) {
            var scoreReceived = 1;
            msg = msg + "Received";
        } else {
            var scoreReceived = 0;
            msg = msg + "Not received";
        }
    
        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];  
        
        // get gre score id from checkbox element id 
        var gresubjectscoreId = $(this).attr("id").split("_")[0];
                
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get("admin_single_update.php", { mode: "gre_subject_received", gresubjectscore_id: gresubjectscoreId, score_received: scoreReceived },
            function(data){
            
                // update the menu row message
                var gresubject_received_msg_id = applicationId + "_gresubject_received_msg";
                if (scoreReceived){
                    $("#"+gresubject_received_msg_id).text("rcd");
                } else {
                    $("#"+gresubject_received_msg_id).text("");
                }
            
                // reload the content div
                reloadForm(applicationId, page, $(selected_content_div));                
                alert(msg);         
            },
            "text");
    
    }); // close click

    
    /*
    // Update GRE Subject Score
    */
    $(".update_gre_subject_score_submit").livequery("click" , function(event){

        // set score variables from form elements
        var subjectName = $(this).parents(".gre_subject_score").find(".gre_subject_name").val();
        var subjectScore = $(this).parents(".gre_subject_score").find(".gre_subject_score").val();
        var subjectPercentile = $(this).parents(".gre_subject_score").find(".gre_subject_percentile").val();
        
        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];
        
        // get subjectscore id from submit element id 
        var gresubjectscoreId = $(this).attr("id").split("_")[0];
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
    
        $.get(
            "admin_single_update.php", 
            { 
                mode: "gre_subject_score", 
                gresubjectscore_id: gresubjectscoreId, 
                subject_name: subjectName,
                subject_score: subjectScore,
                subject_percentile: subjectPercentile 
            },
            function(data){
                reloadForm(applicationId, page, $(selected_content_div));
                alert("GRE subject score updated.");           
            },
            "text"
        );

         return false;
    }); // close click 


    /*
    // Set/Unset TOEFL Score Received
    */
    $(".toefl_received").livequery("click" , function(event){

        // check whether the box is checked        
        var msg = "TEOFL Updated: ";
        if ( $(this).is(":checked") ) {
            var scoreReceived = 1;
            msg = msg + "Received";
        } else {
            var scoreReceived = 0;
            msg = msg + "Not received";
        }
    
        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];  
        
        // get toefl score id from checkbox element id 
        var toeflscoreId = $(this).attr("id").split("_")[0];
                
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get("admin_single_update.php", { mode: "toefl_received", toeflscore_id: toeflscoreId, score_received: scoreReceived },
            function(data){
                
                // update the menu row message
                var toefl_received_msg_id = applicationId + "_toefl_received_msg";
                if (scoreReceived){
                    $("#"+toefl_received_msg_id).text("rcd");
                } else {
                    $("#"+toefl_received_msg_id).text("");
                }
                
                // reload the content div
                reloadForm(applicationId, page, $(selected_content_div));                
                alert(msg);         
            },
            "text");
    
    }); // close click

    
    /*
    // Update TOEFL Score
    */
    $(".update_toefl_score_submit").livequery("click" , function(event){

        // set score variables from form elements
        var section1Score = $(this).parents(".toefl_score").find(".toefl_section1_score").val();
        var section2Score = $(this).parents(".toefl_score").find(".toefl_section2_score").val();
        var section3Score = $(this).parents(".toefl_score").find(".toefl_section3_score").val();
        var essayScore = $(this).parents(".toefl_score").find(".toefl_essay_score").val();
        var totalScore = $(this).parents(".toefl_score").find(".toefl_total_score").val(); 
        
        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];
        
        // get score id from submit element id 
        var toeflscoreId = $(this).attr("id").split("_")[0]; 
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
    
        $.get("admin_single_update.php", 
                { mode: "toefl_score", toeflscore_id: toeflscoreId, 
                section1_score: section1Score,
                section2_score: section2Score,
                section3_score: section3Score,
                essay_score: essayScore,
                total_score: totalScore},
            function(data){
                reloadForm(applicationId, page, $(selected_content_div));
                alert("TOEFL score updated.");          
            },
            "text");

         return false;
    }); // close click

    
    /*
    // Upload Recommendation
    */
    $(".upload_recommendation_submit").livequery("click" , function(event){
        
        // identify the correct file element to send
        var file_element_id = $(this).prev().attr("id");

        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];
        
        // get applicant id from hidden element
        var applicantId = $(this).parents("form").find("input.applicant_id").attr("id");
        
        // get applicant guid from hidden element
        var applicantGuid = $(this).parents("form").find("input.applicant_guid").attr("id");
        
        // get recommendation id from submit element id 
        var recommendationId = $(this).attr("id").split("_")[0];
        
        // get upload mode (upload_new vs. update) from hidden element
        var uploadMode = $(this).prevAll("input.upload_mode").val();
        
        // construct URL for Ajax request
        var uploadURL = "admin_single_update.php?mode=upload_recommendation";
        uploadURL = uploadURL + "&application_id=" + applicationId;
        uploadURL = uploadURL + "&applicant_id=" + applicantId;
        uploadURL = uploadURL + "&applicant_guid=" + applicantGuid; 
        uploadURL = uploadURL + "&recommendation_id=" + recommendationId;
        uploadURL = uploadURL + "&upload_mode=" + uploadMode;
        
        // get variables to reload form
        //var selected_content_div = $(this).parents(".content");
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.ajaxFileUpload
        (
            {
                url: uploadURL,
                //url:'doajaxfileupload.php',
                secureuri:false,
                fileElementId: file_element_id,
                //fileElementId: 'fileToUpload',
                dataType: 'json',
                success: function (data, status)
                {
                    
                    // update the menu row count
                    if ( (uploadMode == "new") && ( $("#"+file_element_id).attr("value") != "" ) ) {
                        var ct_recommendations_submitted_msg_id = applicationId + "_ct_recommendations_submitted_msg";
                        var ct_recommendations_submitted = parseInt( $("#"+ct_recommendations_submitted_msg_id).text() ) + 1;
                        $("#"+ct_recommendations_submitted_msg_id).text(ct_recommendations_submitted);                    
                    }
                    
                    
                    // reload the div  
                    reloadForm(applicationId, page, $(selected_content_div)); 
                    if(typeof(data.error) != 'undefined')
                    {
                        if(data.error != '')
                        {
                            alert(data.error);
                        }else
                        {
                            alert(data.msg);
                        }
                    }
                },
                error: function (data, status, e)
                {
                    alert(e);
                }
            }
        )

        return false;
    }); // close click 
     


    /* 
    // Set/Unset Application Complete
    */ 
    $(".application_complete").livequery("click" , function(event){
        
        // check whether the box is checked        
        var msg = "Application Updated: ";
        if ( $(this).is(":checked") ) {
            var applicationComplete = 1;
            msg = msg + "Complete";
        } else {
            var applicationComplete = 0;
            msg = msg + "Not Complete";
        }
    
        // get application id from checkbox element id
        var applicationId = $(this).attr("id").split("_")[0];
        
        $.get("admin_single_update.php", { mode: "application_complete", application_id: applicationId, application_complete: applicationComplete },
            function(data){                             
                alert(msg);
            },
            "text");

    }); // close click    


    
    /*
    // Change publication status
    */
    $(".publication_status").livequery("change" , function(event){

        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];
        
        // get publication id from select element id
        var publicationId = $(this).attr("id").split("_")[0];
        
        // new status value
        var publicationStatus = $(this).val();
        //alert(publicationStatus);

        // variables to reload form
        var selected_content_div = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get(
            "admin_single_update.php", 
            { 
                mode: "publication_status", 
                publication_id: publicationId,
                status: publicationStatus 
            },
            function(data){
                reloadForm(applicationId, page, $(selected_content_div));
                alert("Publication Status Updated");
                //alert(data);           
            },
            "text"
        );

    }); 





   /*
    // Set/Unset Application Submitted
    */
    $(".application_submitted").livequery("click" , function(event){
        
        // confirm change before doing anything
        if ( confirm("Are you sure you want to change the submission status?") ) {

            // check whether the box is checked        
            var msg = "Application Updated: ";
            if ( $(this).is(":checked") ) {
                var applicationSubmitted = 1;
                msg = msg + "Submitted";
            } else {
                var applicationSubmitted = 0;
                msg = msg + "Not Submitted";
            }
    
            // get application id from checkbox element id
            var applicationId = $(this).attr("id").split("_")[0];
        
            $.get("admin_single_update.php", { mode: "application_submitted", application_id: applicationId, application_submitted: applicationSubmitted },
                function(data){                              
                    alert(msg);
                },
                "text");
        
        } else {
            // set the checkbox back to its previous state
            if ( $(this).is(":checked") ) {
                $(this).removeAttr("checked");
            } else {
                $(this).attr("checked", "checked");
            }
        }
    
    }); // close click 
 
 
}); //close ($document).ready


/*
// function to reload a form/div that has just been updated
*/
function reloadForm(application_id, page, selected_content_div) {

            // submit the request
            $.ajax({
                method: "get", url: "admin_single.php", data: "application_id="+application_id+"&page="+page,
                success: function(html){ 
                    $(selected_content_div).html(html);
                },
                error: function() {
                    $(selected_content_div).html("error");
                }
            }); //close $.ajax(

}


