
$(document).ready(function(){

    $("#special_consideration").click(function(event){

        var applicationId = $("#application_id").attr("value");
        var reviewerId = $("#reviewer_id").attr("value");
        
        if ( $(this).is(":checked") ) {
            var specialConsideration = 1; 
            msg = "Special consideration requested";
        } else {
            var specialConsideration = 0; 
            msg = "Request for special consideration rescinded"
        }
        
        $.get("updateSpecialConsideration.php", { 
                application_id: applicationId, 
                reviewer_id: reviewerId,
                special_consideration: specialConsideration
                },
                function(data){                             
                    alert(msg);
                },
                "text");

    });

    
    $("#technical_screen").click(function(event){

        var checkboxObject = $(this);
        var applicationId = $("#application_id").attr("value");
        var reviewerId = $("#reviewer_id").attr("value");
        
        updatePhoneScreen(checkboxObject, applicationId, reviewerId);

    });

    
    $("#language_screen").click(function(event){

        var checkboxObject = $(this);
        var applicationId = $("#application_id").attr("value");
        var reviewerId = $("#reviewer_id").attr("value");
        
        updatePhoneScreen(checkboxObject, applicationId, reviewerId);

    });

});



function updatePhoneScreen(checkboxObject, applicationId, reviewerId) {
    
    var type = $(checkboxObject).attr("id");
    
    if ( $(checkboxObject).is(":checked") ) {
        msg = type + " phone screen requested";
    } else {
        msg = "request for " + type + " phone screen rescinded"
    }
    
    if ( $("#technical_screen").is(":checked") ) {
        var technicalScreen = 1;
    } else {
        var technicalScreen = 0;
    }

    if ( $("#language_screen").is(":checked") ) {
        var languageScreen =  1;
    } else {
        var languageScreen =  0;
    }
    
    $.get("updatePhonescreen.php", { 
            application_id: applicationId, 
            reviewer_id: reviewerId,
            technical_screen: technicalScreen,
            language_screen: languageScreen 
            },
            function(data){                             
                alert(msg);
            },
            "text");
}

function updateSpecialConsideration(checkboxObject, applicationId, reviewerId) {
    
    if ( $(checkboxObject).is(":checked") ) {
        var specialConsideration = 1; 
        msg = "Special consideration requested";
    } else {
        var specialConsideration = 0; 
        msg = "Request for special consideration rescinded"
    }
    
    $.get("updateSpecialConsideration.php", { 
            application_id: applicationId, 
            reviewer_id: reviewerId,
            special_consideration: specialConsideration
            },
            function(data){                             
                alert(msg);
            },
            "text");

}
