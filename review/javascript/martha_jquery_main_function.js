/*
// Main function for opening a new row/div for an application record
// and switching the view in it.
*/
function changeView(link_object, application_id) {

            // set some variables for testing
            var selected_link_td = $(link_object).parent("td");
            var selected_link_tr = $(selected_link_td).parent("tr");
            
            // if "closing" an open tab/section and not opening a new one 
            if ( $(selected_link_td).hasClass("selected") ) {
            
                var selected_content_tr = $("#"+application_id+"_content_tr");
                
                // remove the content row
                $(selected_content_tr).remove();
                
                // remove the "selected" status of the menu tr and td
                $(selected_link_td).removeClass("selected");
                $(selected_link_tr).removeClass("selected");
                
                // change the indicator
                //$(link_object).children(".indicator").html("&rarr;");
                
                // return the original row and link style
                $(selected_link_tr).children().css("border", "");
                $(selected_link_tr).children().css("background-color", "");
                $(link_object).css("color", "");
                $(link_object).css("font-weight", "");
                $(link_object).css("text-decoration", "");           
             
            } else {
            
                // opening new record or changing view in same record
                            
                // turn off style to "close" open tabs
                $("td").css("border", "");
                $("td").css("background-color", ""); 
                $("tr.menu a").css("color", "");
                $("tr.menu a").css("font-weight", "");
                $("tr.menu td").removeClass("selected");
                
                // reset the arrow indicators
                $("span.indicator").html("&rarr;");

                // if selecting different section of same record
                if ( $(selected_link_tr).hasClass("selected") ) {
                
                    var selected_content_tr = $("#"+application_id+"_content_tr");
                    
                    // change the selected arrow indicator
                    $(link_object).children(".indicator").html("&darr;");
                
                    // identify the content div of interest
                    var selected_content_div = $("#"+application_id+"_content_div"); 
                
                    // empty the content div
                    $(selected_content_div).html("");
                
                } else {
                
                    // opening a new record

                    //var old_menu_row = $("tr[class='menu selected']");
                    var old_menu_row = $("tr.selected"); 
                    
                    // remove the old content row
                    $(old_menu_row).next().remove();
                    
                    // de-select old menu row
                    $(old_menu_row).removeClass("selected");
                    
                    // add a row/div beneath the present one
                    var bgcolor = $(selected_link_tr).attr("bgcolor");
                    $(selected_link_tr).after("<tr bgcolor="+bgcolor+" id=\""+application_id+"_content_tr\"><td colspan=\"8\"><div class=\"content\" id=\""+application_id+"_content_div\"><div id=\"loading\">LOADING</div></div></td></tr>");
                    
                    // select the new menu row                
                    $(selected_link_tr).addClass("selected");
                    
                    // change the arrow indicator
                    //$(link_object).children(".indicator").html("&darr;"); 
                    
                    // set content variables based on new row
                    var selected_content_tr = $(selected_link_tr).next();
                    var selected_content_div = $("#"+application_id+"_content_div"); 
                
                } 

                // highlight the selected tab/link            
                $(selected_link_td).addClass("selected");
                $(selected_link_tr).children().css("border-top", "1px solid black");
                $(selected_content_tr).children().css("border-bottom", "1px solid black");
                //$(selected_link_tr).children().css("background-color", "#FFFFFF");
                //$(selected_content_tr).children().css("background-color", "#FFFFFF");  
                //$(link_object).css("color", "black");
                $(link_object).css("font-weight", "bold");

                // show the content div
                $(selected_content_div).show();
                $("#loading").show("fast");
            
                // get the page and application id from the element id, e.g. 193_payment
                var request_id = $(link_object).attr("id");
                var request_arguments = request_id.split("_");
                var application_id = request_arguments[0];
                var page = request_arguments[1];
            
                // submit the request
                $.ajax({
                    method: "get",url: "admin_single.php",data: "application_id="+application_id+"&page="+page,
                    //beforeSend: function(){$("#loading").show("fast");},
                    //complete: function(){ $("#loading").hide("fast");},
                    success: function(html){ //so, if data is retrieved, store it in html
                        //$(selected_content_div).show();
                        $("#loading").hide("fast");
                        $(selected_content_div).html(html);
                        } // close success
                }); //close $.ajax(
            
            } // close if/else
            return false;    
}