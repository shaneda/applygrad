<? 
$exclude_scriptaculous = TRUE;
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../inc/applicationFunctions.php';

// Ensure page reloads on forward/back navigation to prevent 
// loss of jquery-modified data.
header("Cache-Control: no-cache");

if(!isset($_SESSION['A_userid']))
{
	echo " <script language='JavaScript'>window.close();</script>";
	header("Location: index.php");
	
}else
{
	if(($_SESSION['A_userid'] == "" || $_SESSION['A_userid'] == -1) && $_SESSION['A_userid'] != 0)
	{
	echo $_SESSION['A_userid'];
		echo " <script language='JavaScript'>window.close();</script>";
		header("Location: index.php");
		
	}
}

// Include the data queries, variables.
include_once '../inc/specialCasesAdmin.inc.php';
include '../inc/printViewData.inc.php';
include '../inc/reviewData.inc.php';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head>
<title>Application of <?=$email?></title>
<link href="../css/app2.css" rel="stylesheet" type="text/css">

<!-- PLB style -->
<style type="text/css">

@media print
{
html, body, form { auto }
}

@media screen
{
html, body, form { height: 99%; }
}

div.surround {
    width: 100%;
    height: 100%;
}

div.application {
    background-color: #ffffff;
<?
if ($readOnly) {
    echo '    height: 1%;';
    echo '    overflow: auto;';   
}
elseif ($thisDept != 25) {
    // ISRI-SE doesn't have the review form.
    echo '    height: 49%;';
    echo '    overflow: auto;';
} else {
    echo '    height: 49%;';
    echo '    overflow: auto;';    
}
?>    
}

div.review {
    background-color: #ffffff;
    <?
    if ($readOnly) {
        echo '    height: 98%;';
    } else {
        echo '    height: 48%;';   
    }
    ?>  
    overflow: auto;
}

#pdf_link a:visited { color: blue }

</style> 
<!-- end PLB style -->

<link href="../css/reviewView.css" rel="stylesheet" type="text/css"> 

<!-- PLB javascript -->
<script language="javascript" src="./javascript/jquery-1.2.6.js"></script>
<script language="javascript" src="./javascript/review.js"></script> 

<script language="javascript" src="../javascript/reviewView.js"></script> 

<script language="JavaScript">
function resetForm()
{
     for (var i=0, j=form1.elements.length; i<j; i++) {
        myType = form1.elements[i].type;
        if (myType == 'checkbox' || myType == 'radio') 
            form1.elements[i].checked = form1.elements[i].defaultChecked;
        if (myType == 'hidden' || myType == 'password' || myType == 'text' || myType == 'textarea')
        {
            //form1.elements[i].value = form1.elements[i].defaultValue;
        }
        if (myType == 'select-one' || myType == 'select-multiple')
        {
                /*
            for (var k=0, l=what.elements[i].options.length; k<l; k++)
            {
                form1.elements[i].options[k].selected = form1.elements[i].options[k].defaultSelected;
            }
            */
        }
    }
}
</script>

</head>
<body bgcolor=#eeeeee topmargin=5 leftmargin=10>
<form id="form1" name="form1" method="post">

<!-- PLB new divs, table -->
<div id="surround" class="surround"> <!-- new surrounding div -->

<div id="application" class="application"> <!-- new main div -->

<!-- end PLB new divs, table -->

<!-- PLB new table for pdf link -->
<?php 
include "../inc/pdf_link.inc.php"; 

// include big table
include '../inc/tpl.printView.php';

// include recommend form answers
include '../inc/printRecommendform.inc.php'; 
?>

</div> <!-- end new main div -->

<hr />
<div id="review" class="review">

<?php
if ($readOnly) {
    //DebugBreak();
    $reviewerInfo = getReviewerInfo($reviewerId);
    echo '<h4>Faculty Review of ' . $lName . ', ' . $fName . ' ' . $mName . ' ' . $applicantTitle;
    if ( isset($reviewerInfo[0]['reviewer_name']) ) {
        echo '<br/>' . $reviewerInfo[0]['reviewer_name'] . ', ' . $reviewerInfo[0]['department_name'] . '</h4>';    
    }
}
?>

<!-- PLB new table for special consideration, phone screen -->
<?php 
// PLB added test for RI
// PLB added test for MSE 01/19/09 


// PLB added test for MSE 01/19/09

if ($thisDept == 18 || $thisDept == 47 || $thisDept == 44) {

    include "../inc/mse_review.inc.php";
    
} else if ($thisDept == 2) {
    if( $thisDept != 3 && $thisDept != 18 && $thisDept != 44
            && !isset($_GET['rid'])
            && ($_SESSION['A_usertypename'] != 'Admissions Committee'
            || $_SESSION['A_usertypename'] != 'Admissions Chair') ){ 

            include "../inc/special_phone_email.inc.php"; 

        }  
        
        include "../inc/ml_review.inc.php";
    
} else {
    
    /*
    if ($thisDept == 1 && !isset($_GET['rid']) && 
        ($_SESSION['A_usertypename'] != 'Admissions Committee'
        || $_SESSION['A_usertypename'] != 'Admissions Chair') ) {
    
        include "../inc/reviewCommitteeCsd.inc.php";
        
    } else {
    */
        if( $thisDept != 3 && $thisDept != 18 && $thisDept != 44
            && !isset($_GET['rid'])
            && ($_SESSION['A_usertypename'] != 'Admissions Committee'
            || $_SESSION['A_usertypename'] != 'Admissions Chair') ){ 

            include "../inc/special_phone_email.inc.php"; 

        }  
        
        include "../inc/review.inc.php"; 
        
    //}
    
}
?>

</div> <!-- end review form div -->

</div> <!-- end surrounding div -->

</form>
</body>
</html>