<?PHP
	// bypass this page and redirect them back to admin/home.php
	header("Location: ../admin/home.php");
	die();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/template1.dwt" codeOutsideHTMLIsLocked="false" -->
<? include_once '../inc/config.php'; ?>
<? include_once '../inc/session_admin.php'; ?>
<? include_once '../inc/db_connect.php'; ?>
<? include_once '../inc/functions.php'; ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SCS Applyweb</title>
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<!-- InstanceBeginEditable name="head" -->
<?
$domains = array();
$departments = array();

//GET DOMAINS
$sql = "select distinct domain.id, domain.name
from ";
if($_SESSION['A_usertypeid'] != 0)
{
	$sql .= "lu_user_department
		left outer join lu_domain_department on lu_domain_department.department_id = lu_user_department.department_id
		left outer join domain on domain.id= lu_domain_department.domain_id
		left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id
		left outer join users on users.id = lu_users_usertypes.user_id
		 where users.id=".$_SESSION['A_usermasterid']." and(lu_users_usertypes.usertype_id = ".$_SESSION['A_usertypeid'].") ";
}else
{
	$sql .= " domain ";
}
$sql .= " order by domain.name";
//echo $sql;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	if($row['name'] != ""){
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($domains, $arr);
	}
}

//GET DEPARTMENTS
$sql = "select
department.id,
department.name,
department.enable_round1,
department.enable_round2,
department.enable_final
from  ";
if($_SESSION['A_usertypeid'] != 0)
{

	$sql .= " lu_user_department
	left outer join department on department.id = lu_user_department.department_id
	left outer join lu_users_usertypes on lu_users_usertypes.id = lu_user_department.user_id
	left outer join users on users.id = lu_users_usertypes.user_id
	where users.id=".$_SESSION['A_usermasterid']." and(lu_users_usertypes.usertype_id = ".$_SESSION['A_usertypeid'].") ";
}else
{
	$sql .= " department ";
}
$sql .= " group by department.id order by department.name";
//echo $sql;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['enable_round1']);
	array_push($arr, $row['enable_round2']);
	array_push($arr, $row['enable_final']);
	array_push($departments, $arr);
}
?>
<!-- InstanceEndEditable -->
<SCRIPT LANGUAGE="JavaScript" SRC="../inc/scripts.js"></SCRIPT>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body>
  <!-- InstanceBeginEditable name="formRegion" --><form id="form1" action="" method="post"><!-- InstanceEndEditable -->
   <div style="height:72px; width:781;" align="center">
	  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		  <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
			<strong>Online Admissions System</strong></td>
		  <td align="right">
		  <? 
		  $_SESSION['A_SECTION']= "2";
		  if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
		  ?>
		  You are logged in as:<br />
			<?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
			<br />
			<a href="../Templates/logout.php"><span class="subtitle">Logout</span></a> 
			<? } ?>
			</td>
		</tr>
	  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
  <? 
  if(strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student.php') === false
  && strstr($_SERVER['SCRIPT_NAME'], 'userroleEdit_student_formatted.php') === false
  ){ ?>
   <script language="JavaScript" src="../inc/menu.js"></script>
   <!-- items structure. menu hierarchy and links are stored there -->
   <!-- files with geometry and styles structures -->
   <script language="JavaScript" src="../inc/menu_tpl.js"></script>
   <? 
	if(isset($_SESSION['A_usertypeid']))
	{
		switch($_SESSION['A_usertypeid'])
		{
			case "0";
				?>
				<script language="JavaScript" src="../inc/menu_items.js"></script>
				<?
			break;
			case "1":
				?>
				<script language="JavaScript" src="../inc/menu_items_admin.js"></script>
				<?
			break;
			case "3":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			case "4":
				?>
				<script language="JavaScript" src="../inc/menu_items_auditor.js"></script>
				<?
			break;
			default:
			
			break;
			
		}
	} ?>
	<script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
	<? } ?>
  </div>
</div>
<br />
<br />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" colspan="3" valign="top">
	<div style="margin:7px; ">
	<span class="title"><!-- InstanceBeginEditable name="TitleRegion" -->Admissions Home <!-- InstanceEndEditable --></span><br />
<br />

	<!-- InstanceBeginEditable name="mainContent" -->
	<table width="100%"  border="0" cellspacing="2" cellpadding="4">
      <tr align="center" class="tblHead">
        <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 4){ ?>
		<td width="33%">Application</td>
		<? } ?>
        <td width="33%">Evaluation</td>
		<? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1){ ?>
        <td width="33%">admission</td>
		<? } ?>
      </tr>
      <tr valign="top">
	 <!-- APPLICATION COLUMN -->
	  <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 4){ ?>
        <td width="33%">
          <br /><fieldset><legend>Applicants by Department</legend>
		  <?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	//echo "<a href='applicantsByDepartment.php?id=".$departments[$i][0]."'>".$departments[$i][1]."</a><br>";
			echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
		  	echo "<a href='applicantsByDepartment.php?id=".$departments[$i][0]."&s=1'>submitted</a> ";
			if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1)
			{
				echo "<a href='applicantsByDepartment.php?id=".$departments[$i][0]."&s=0'>unsubmitted</a> ";
				echo "<a href='applicantsByDepartment.php?id=".$departments[$i][0]."'>all</a><br>";
			}
		  }
		  ?>
		  </fieldset>
		 <? if($_SESSION['A_usertypeid'] == 0){ ?>
		<fieldset><legend>Applicants by Domain</legend>
		 <?
		  for($i = 0; $i< count($domains); $i++)
		  {
		  	echo "<span class='subtitle'>".$domains[$i][1]."</span><br>";
		  	echo "<a href='adminSubmittedByDomain.php?id=".$domains[$i][0]."'>submitted</a> ";
			echo "<a href='applicantsByDomain.php?id=".$domains[$i][0]."&s=0'>unsubmitted</a> ";
			echo "<a href='applicantsByDomain.php?id=".$domains[$i][0]."'>all</a><br>";
			echo "<a href='recommedersnoreply.php?id=".$domains[$i][0]."'>Recommenders who haven't responded</a><br>";
		  }
		  ?></fieldset>
		  <? }//end superuser ?>
		  <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1){ ?>
		<fieldset><legend>Email Unsubmitted Applicants</legend>
		 <?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
			echo "<a href='remindUnsubmittedApps.php?id=".$departments[$i][0]."'>Send Email</a><br>";
		  }
		  ?></fieldset>

		  <fieldset><legend>Email Unsubmitted Recommenders</legend>
		 <?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
			echo "<a href='remindUnsubmittedRecs.php?id=".$departments[$i][0]."'>Send Email</a><br>";
		  }
		  ?></fieldset>
		<? }//end superuser or admin ?>
		  </td>
<? } ?>
<!-- REVIEW COLUMN -->
        <td width="33%">
		<? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 10){ ?>
		<fieldset>
		    <legend>Group Assignments</legend>
		    <table width="100%" border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td>Define Groups </td>
                <td><a href="reviewGroups.php?id=1">Edit</a></td>
                <td><a href="reviewGroups.php?id=2"></a></td>
              </tr>
              <tr>
                <td>Assign Reviewers </td>
                <td><a href="reviewGroups_assign.php?r=1">Round 1</a></td>
                <td><a href="reviewGroups_assign.php?r=2">Round 2</a> </td>
              </tr>
              <tr>
                <td>Assign Applicants </td>
                <td><a href="applicantsGroups_assign.php?r=1">Round 1</a> </td>
                <td><a href="applicantsGroups_assign.php?r=2">Round 2</a> </td>
              </tr>
            </table>
		    </fieldset>
		<br />
		<? } ?>
		
		<? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 2 || $_SESSION['A_usertypeid'] == 3 || $_SESSION['A_usertypeid'] == 11){?>
		<fieldset><legend>Reviews</legend>
		<?
		  for($i = 0; $i< count($departments); $i++)
		  {
		  	if($_SESSION['A_usertypeid'] != 11)
			{
				//echo "<a href='applicantsByDepartment.php?id=".$departments[$i][0]."'>".$departments[$i][1]."</a><br>";
				if($departments[$i][2] == 1 )
				{
					echo "<span class='subtitle'>".$departments[$i][1]."</span><br>";
				}
		
				if( ($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 0) ||
				$_SESSION['A_usertypeid'] != 4 
				&& $_SESSION['A_usertypeid'] != 3 )//faculty cannot access
				{
					echo "<a href='applicants_adm_committee_reviews.php?id=".$departments[$i][0]."&s=1&r=1'>Round 1</a> ";
				}
				if($departments[$i][3] == 1 )//if enabled for department
				{
					echo "<a href='applicants_adm_committee_reviews.php?id=".$departments[$i][0]."&s=1&r=2'>Round 2</a> ";
				}
				if(
				($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 0) ||
				$departments[$i][4] == 1 )//if enabled for department
				{
					echo "<a href='applicants_adm_committee_reviews.php?id=".$departments[$i][0]."&s=1&r=2&d=1'>Final</a> ";
					echo "<a href='applicants_round2_normalizations.php?id=".$departments[$i][0]."&s=12'>Normalizations</a> ";
				}
			}
			if( 
			($_SESSION['A_usertypeid'] == 1 || $_SESSION['A_usertypeid'] == 0) 
			|| $departments[$i][4] == 1 )//if enabled for department
			{
				echo "<a href='admitted.php?id=".$departments[$i][0]."&s=12'>List of Invited Applicants</a> ";
			}
			
			echo "<br>";
		  }
		  ?>
		</fieldset>
		<? }//end if ?>
		</td>

		<!-- ADMISSIONS COLUMN -->
		 <? if($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1){ ?>
        <td width="33%"><fieldset><legend>Invited Applicants</legend>

		<? for($i = 0; $i< count($departments); $i++) { ?>
		  	<span class="subtitle"><?=$departments[$i][1]?></span><br />
			<a href="admitted.php?id=<?=$departments[$i][0]?>">List of Invited Applicants</a>  <a href="admitted_ext.php?id=<?=$departments[$i][0]?>">Ext. List</a><br />
			<a href="admitted_text.php?id=<?=$departments[$i][0]?>">CSV of Invited Applicants</a><br />
			<a href="shippinglabels.php?id=<?=$departments[$i][0]?>">Print Labels</a><br />
			<?
			$sql = "select id,name from content where
			(contenttype_id=2)
			and (department_id=".intval($departments[$i][0]) . ") order by name";
			$result = mysql_query($sql) or die(mysql_error() . $sql);
			while($row = mysql_fetch_array( $result ))
			{
				?>
					<a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>" target="_blank">Preview</a> <?=$row['name']?><br />
				<?
			}
			?>
		<? } ?>
        </fieldset>
        <br />
<fieldset><legend>Applicants Who Accepted</legend>
Accepted Student List
</fieldset>
<br />
<fieldset><legend>Rejected Applicants</legend>
<? for($i = 0; $i< count($departments); $i++) { ?>
<span class="subtitle"><?=$departments[$i][1]?></span><br />
<a href="rejected.php?id=<?=$departments[$i][0]?>">List of Rejected Applicants</a><br />
<?
$sql = "select id,name from content where
(contenttype_id=3) and (department_id=".$departments[$i][0] . ") order by name";
$result = mysql_query($sql) or die(mysql_error() . $sql);
while($row = mysql_fetch_array( $result ))
{
	?>
		<a href="contentEdit_dept.php?id=<?=$row['id']?>">Edit</a>/<a href="contentPrint.php?id=<?=$row['id']?>" target="_blank">Preview</a> <?=$row['name']?><br />
	<?
}
?>
<? } ?>
</a>
</fieldset>
<br />

<fieldset><legend>Statistics</legend>
Applicants Stats<br />
Applicants Stats (Accepted)<br />
Area of Interest Matrix<br />
Round 2 Normalized Matrix
</fieldset>
<br />
<fieldset><legend>Reports</legend>
Admin Committee Reviews (Final)<br />
Committee Decisions
</fieldset>
</td>
<? } ?>
<!-- END ADMISSIONS COLUMN -->
      </tr>
    </table>
	<!-- InstanceEndEditable -->
	</div>
	</td>
  </tr>
</table>
<br />
<br />
</form>
</body>
<!-- InstanceEnd --></html>
