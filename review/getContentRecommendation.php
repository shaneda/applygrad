<?php

// Include the necessary config files.
include_once "../inc/config.php";
 
// Raise the memory limit for dompdf and include the library
ini_set('memory_limit', '64M'); 
require_once "./pdf_conversion/dompdf-0.5.1/dompdf_config.inc.php";

// Include the db classes.
include_once "./classes/class.db_applyweb.php";
include_once "./classes/class.db_merge_recommendations.php"; 
 
// Get the recommendation id from the get request.
$recommendation_id = $_GET['recommendation_id'];

// Get the recommendation data from the db.
$db_recommendations = new DB_Recommendations();
$recommendation_array = $db_recommendations->getRecommendationById($recommendation_id);
$recommendation = $recommendation_array[0];

// Get the recommendation "content" text and convert to html.
$content_text = $recommendation['content'];
$content_html = $recommendation['firstname'] . " " . $recommendation['lastname'] . "<br />";
$content_html .= $recommendation['title'] . ", " . $recommendation['company'] . "<br />";
$content_html .= $recommendation['email'] . "<br />";
$content_html .= $recommendation['address_cur_tel'] . "<br /><br />";
$content_html .= html_entity_decode($content_text);

// Set the output filename.
$output_filename = "recommendation_" . $recommendation['application_id'] . "_" . $recommendation_id . ".pdf";

  
echo $content_html;
  
// Load into dompdf and return pdf file.
$dompdf = new DOMPDF();
$dompdf->load_html($content_html); 
$dompdf->render();
// Set pdf streaming options to turn off accept-ranges header, apply stream compression,
// and turn off attachment in content-disposition header (so download dialog is not forced)
$stream_options = array('Accept-Ranges' => 0, 'compress' => 1, 'Attachment' => 0);
//$dompdf->stream($output_filename, $stream_options);

?>
