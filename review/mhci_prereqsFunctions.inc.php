<?php       

function getStudentName($studentLuId) {

    $query = "SELECT CONCAT(users.firstname, ' ', users.lastname) AS name
                FROM lu_users_usertypes
                INNER JOIN users ON users.id = lu_users_usertypes.user_id
                WHERE lu_users_usertypes.id = " . $studentLuId;   
    
    $result = mysql_query($query);
    
    while ($row = mysql_fetch_array($result)) {
    
        $name = $row['name'];
        
    }
    
    return $name;
}



function makeNavigation($studentLuId) {

    echo <<<EOB

    <button class="bodyButton" 
    onClick="parent.location='mhci_prereqs_design.php?studentLuId={$studentLuId}';return false;">
    Design
    </button>
    <button class="bodyButton" 
    onClick="parent.location='mhci_prereqs_programming.php?studentLuId={$studentLuId}';return false;">
    Programming
    </button>
    <button class="bodyButton" 
    onClick="parent.location='mhci_prereqs_statistics.php?studentLuId={$studentLuId}';return false;">
    Statistics
    </button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="bodyButton" onClick="parent.location='mhci_prereqs_intro.php';return false;">
    Back to list
    </button> 
    <br/><br/>

EOB;

}

?>
