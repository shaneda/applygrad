<?PHP
//DebugBreak();
$startTime = microtime(TRUE);
ini_set('memory_limit', '256M');

include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include '../inc/specialCasesAdmin.inc.php'; 
include '../inc/reviewDomainListFunctions.inc.php';
                                         
// Get the unit/period
$unit = filter_input(INPUT_GET, 'unit', FILTER_SANITIZE_STRING);
if (!$unit)
{
    $unit = filter_input(INPUT_POST, 'unit', FILTER_SANITIZE_STRING);    
}

$unitId = filter_input(INPUT_GET, 'unitId', FILTER_VALIDATE_INT);
if (!$unitId)
{
    $unitId = filter_input(INPUT_POST, 'unitId', FILTER_VALIDATE_INT);    
}

$periodId = filter_input(INPUT_GET, 'periodId', FILTER_VALIDATE_INT);
if (!$periodId)
{
    $periodId = filter_input(INPUT_POST, 'periodId', FILTER_VALIDATE_INT);    
}

if (!$unitId || !$periodId || 
    ($_SESSION['A_usertypeid'] != 0 && !isValidUnit($unit, $unitId, $_SESSION['roleDepartmentId']))) 
{
    header('Location: ../admin/home.php');
}

$unitName = getUnitName($unit, $unitId);
                                               
// Include the data/datagrid classes.      
include "../classes/class.ReviewDomainListData.php";
include "../classes/Structures_DataGrid/class.ReviewList_DataGrid.php";
include "../classes/Structures_DataGrid/class.ReviewDomainList_DataGrid.php";
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

// period
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";

if ($periodId) 
{
    $displayPeriodId = $_REQUEST['periodId']; 
    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

/*
* Set up header variables and include the standard page header.
*/
$pageTitle = 'Review Applications';

$pageCssFiles = array(
    '../css/reviewApplications.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.tablesorter.js',
    '../javascript/reviewApplications.js'
    );
include '../inc/tpl.pageHeader.php';


/*
* Handle the REQUEST variables. 
*/
// vars to be carried in every form
// $department_id
// $round
// $decision
// $groups
// $chkSemiblind
// $search_text

 
// round
if ( isset($_GET['r']) ) $round = $_GET['r'];
elseif ( isset($_POST['round']) ) $round = $_POST['round'];
else $round = 1;

// decision (final) round
if ( isset($_GET['d']) ) $decision = 1;
elseif ( isset($_POST['decision']) ) $decision = $_POST['decision'];
else $decision = 0;

// user -- not necessary right now
if ( isset($_SESSION['A_userid']) ) $reviewer_id = $_SESSION['A_userid'];
else $reviewer_id = FALSE;

// user type id
$usertypeid = $_SESSION['A_usertypeid'];

// groups
if ( isset($_REQUEST['groups']) )
{    
    $groups = $_REQUEST['groups'];
} else if (isHistoryDepartment($department_id)) {
    $groups = "myGroups";
} else {
    $groups = "allGroups";    
}

// set checked value for groups
if ( $groups == "myGroups")
{
	$myGroups_checked = "checked";
	$allGroups_checked = "";
} else {
	$myGroups_checked = "";
	$allGroups_checked = "checked";	
}

// group, column filters
if ( isset($_REQUEST['groupFilterValue']) )
{    
    $groupFilterValue = urldecode($_REQUEST['groupFilterValue']);
} else {
    $groupFilterValue = '';    
}
if ( isset($_REQUEST['columnFilterSelectValue']) )
{    
    $columnFilterSelectValue = $_REQUEST['columnFilterSelectValue'];
} else {
    $columnFilterSelectValue = '';    
}
if ( isset($_REQUEST['columnFilterValue']) )
{    
    $columnFilterValue = $_REQUEST['columnFilterValue'];
} else {
    $columnFilterValue = '';    
}

if ( isset($_REQUEST['tableSortList']) )
{    
    $tableSortList = $_REQUEST['tableSortList'];
} else {
    $tableSortList = '';    
}

$setSemiblind = 0; 
// PLB changed to allow CSD faculty to have semiblind review
// DAS added LTI faculty
//if ( $usertypeid == 3 )
if ( $usertypeid == 3 && $department_id != 1 && $department_id != 5 && $department_id != 6 )
{
    // faculty don't see comments
    $semiblind_review = 1;
    $chkSemiblind = 1;        
}
else
{
    // semi blind review 
    // 1 = off
    // 0 = on
    if ( isset($_REQUEST['setSemiblindStatus']) ) 
    { 
        // The user is turning semiblind on/off
        $setSemiblind = 1;
        
        // if checked, true
        if ( isset($_REQUEST['chkSemiblind']) ) 
        {
            $chkSemiblind = 1;
            $semiblind_review = 1;
        }
        else 
        {
            $chkSemiblind = 0;
            $semiblind_review = 0;
        }
    } 
    elseif ( isset($_REQUEST['chkSemiblind']) )
    {
        $chkSemiblind = $_REQUEST['chkSemiblind'];
        $semiblind_review = $chkSemiblind;
    }
    else 
    {
        //get department defaults for semi blind
        //$semiblind_review = getDefaultSemiblind( $department_id );
        $semiblind_review = 1;
        if ( $semiblind_review == 1 ) $chkSemiblind = 1;
        else $chkSemiblind = 0;
    }
    // set semi blind checkbox value
    if ( $chkSemiblind == 1 ) $chkSemiblind_checked = "checked";
    else $chkSemiblind_checked = "";
}

// search text
if ( isset($_POST['search_text']) ) $search_text = $_POST['search_text'];
else $search_text = "";

// Selected columns.
$selectedColumns = array();
if ( isset($_REQUEST['showColumns']) ) {
    $selectedColumns = $_REQUEST['showColumns'];    
}

/* 
* Set faculty view to turn off special consideration/phone screens (csd)
* and turn on faculty of interest in round 2 (all?). 
*/ 
$faculty_view = 0;
if ( $usertypeid == 3 ) {
    $faculty_view = 1;    
}

/*
* Get the appropriate datagrid.
*/    
$reviewList = new ReviewDomainListData($unit, $unitId, $unitName);
$baseArray = $reviewList->getData($periodId, $reviewer_id, $groups, $search_text);
$allCommentsOn = TRUE;
$dataGrid = new ReviewDomainList_DataGrid($round, $decision, $setSemiblind, $semiblind_review, 
                                                    $faculty_view, $search_text, $selectedColumns);    
//$dataGrid = new ReviewDomainList_DataGrid(); 
$dataGrid->bind($baseArray, array() , 'Array');
$recordCount = $dataGrid->getRecordCount(); 

// Get the datagrid columns.
$columns = $dataGrid->getColumns();

// set filter for groups
$groupFilterSelect = createGroupFilterSelect( $groups, $reviewer_id, 1, $round, $periodId, $columns );

// set filter columns
$filterColumns = setFilterColumns($columns);

// Use datagrid columns as selected if not sent in REQUEST
// or if toggling semiblind
if ( empty($selectedColumns) || $setSemiblind ) {
    foreach ($columns as $column) {
        $selectedColumns[] = $column->fieldName;    
    }  
}

// Set the form action href
$formActionHref = $_SERVER['PHP_SELF'] . '?unit=' . $unit . '&unitId=' . $unitId;
if ($periodId) 
{
    $formActionHref .= '&periodId=' . $periodId; 
}

?>
        <div id="export">
            <form action="reviewApplicationsExport.php" method="post" target="_blank"
            name="exportForm" id="exportForm" onsubmit='getCurrentTable()'>
            <input type="hidden" id="datatodisplay" name="datatodisplay" /> 
            <b>Export current table:</b>
            <input  type="submit" value="Save CSV File">
            </form>
        </div>


        <!-- PW code starts here -->
        <div id="refreshPage">
			
			<form action="<?php echo $formActionHref; ?>" method="POST" name="refreshForm" id="refreshForm">
				<input type='hidden' name='department_id' value='<?= $department_id ?>'>
				<input type='hidden' name='round' value='<?= $round ?>'>
				<input type='hidden' name='decision' value='<?= $decision ?>'>
				<input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
				<input type='hidden'name='search_text' value='<?= $search_text ?>'>	
				<input type='hidden' name='groups' value='<?= $groups ?>'>
                <input type='hidden' id='groupFilterValue' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
                <input type='hidden' id='columnFilterSelectValue' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                <input type='hidden' id='columnFilterValue' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
                <input type='hidden' id='tableSortList' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                <?php
                if ($periodId) {
                    if (is_array($periodId)) {
                        foreach($periodId as $periodIdItem) {
                            echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                        }
                    } else {
                        echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                    }
                }
                ?>
                
                <?php makeHiddenShowColumns( $selectedColumns ); ?>
				
                <b>View latest data:</b>
				<input type='submit' name='refreshPage' value='Refresh This Page' accesskey='l'>
			
			</form>
            

            
            			
		</div>


		
		<div id="pageHeading">
			<div id="departmentName"><?php echo $unitName . $applicationPeriodDisplay; ?></div>	
			<!--
            <?PHP
				// faculty title only
				if ( $usertypeid == 3 )
				{
			?>
				<div id="sectionTitle">Review Applications</div>
			<?PHP
				}
				// admin committee title	
				else
				{
                    if (isSocialDecisionSciencesDepartment($department_id)) {
                        ?>
                        <div id="sectionTitle">Review Applicants:  
                        <?PHP 
                        if ( $round == 2 and $decision )
                            print "Open House (Final)"; 
                        else if ($round == 1) 
                            print "Everyone";
                         else {
                            print "Open House";
                        }
                    } else {
			?>
				<div id="sectionTitle">Review Applicants: Round 
				<?PHP 
					if ( $round == 2 and $decision ) {
						print "$round (Final)"; 
					} else {
						print $round;
                    }
                }
				?>
				
				<span style="font-size: 15px;">(n=<?php echo $recordCount; ?>)</span>
				</div>
			<?PHP }	?>
            -->
		</div>

<!-- end b -->

<div id="displayOptions">

	<h3>Display Options</h3>
	<table border="0" cellpadding="0" cellspacing="1">   
        
        <tr>
            <!--
			<td align="right" valign="top"><b>Group:</b></td>
			<td>
				<form action="<?php echo $formActionHref; ?>" method="POST" name="groupsForm" id="groupsFrom">
					
					<input type='hidden' name='department_id' value='<?= $department_id ?>'>
					<input type='hidden' name='round' value='<?= $round ?>'>
					<input type='hidden' name='decision' value='<?= $decision ?>'>
					<input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
					<input type='hidden' name='search_text' value='<?= $search_text ?>'>
                    <input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                    <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>						
					<input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                    <?php
                    if ($periodId) {
                        if (is_array($periodId)) {
                            foreach($periodId as $periodIdItem) {
                                echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                            }
                        } else {
                            echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                        }
                    }
                    ?>
                    
					<?php makeHiddenShowColumns( $selectedColumns ); 
                    if (isHistoryDepartment($department_id) && $usertypeid == 3) {
                        $myGroups_checked = 'checked';
                        
                    }?>
                    
                    <input type='radio' name='groups' value='myGroups' <?= $myGroups_checked ?> onclick="this.form.submit()"> My Groups
					<?PHP
						if ( $groups == "myGroups" ) { print $groupFilterSelect;  }
                        if (!(isHistoryDepartment($department_id) && $usertypeid == 3)) {
					?>
					<br>
					<input type='radio' name='groups' value='allGroups' <?= $allGroups_checked ?> onclick="this.form.submit()"> All Groups
					<?PHP
						if ( $groups == "allGroups" ) print $groupFilterSelect;
                        }
					?>			
				</form>		
			</td>
            -->
            
            <!-- PLB added column picker -->
            <td rowspan="2" align="right" valign="top">
            <b>Columns:</b>
            </td>
            <td rowspan="2" width="50%" valign="top">
            
            <form action="<?php echo $formActionHref; ?>" method="POST" name="columnsForm" id="columnsForm">
                    
                    <input type='hidden' name='department_id' value='<?= $department_id ?>'>
                    <input type='hidden' name='round' value='<?= $round ?>'>
                    <input type='hidden' name='decision' value='<?= $decision ?>'>
                    <input type='hidden' name='groups' value='<?= $groups ?>'>
                    <input type='hidden' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
                    <!--
                    <input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                    <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
                    <input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                    -->
                    <input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
                    <input type='hidden' name='search_text' value='<?= $search_text ?>'>
                    <?php
                    if ($periodId) {
                        if (is_array($periodId)) {
                            foreach($periodId as $periodIdItem) {
                                echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                            }
                        } else {
                            echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                        }
                    }
                    ?>
            
            <?php
            $requiredColumns = $dataGrid->getRequiredColumns();
            foreach ($requiredColumns as $field => $label) {
                echo <<<EOB
                <input type="hidden" name="showColumns[]" value="{$field}" />       
EOB;
            }
            
            $selectableColumns = $dataGrid->getSelectableColumns(); 
            foreach ($selectableColumns as $field => $label) {
                
                $checked = '';
                if ( $dataGrid->getColumnByField($field) ) {
                    $checked = 'checked';
                }
                
                echo <<<EOB
                <span style="white-space:nowrap;">
                <input type="checkbox" name="showColumns[]" 
                    value="{$field}" {$checked}/>{$label}
                </span>        
EOB;
            }
            
            ?>
            <br/>
            <input type='button' name='changeColumns' value='Change Columns' onclick="this.form.submit()">
            <?php
            $defaultColumnsHref = $_SERVER['PHP_SELF'] . '?r=' .$round;
            $defaultColumnsHref .= '&chkSemiblind=' . $chkSemiblind;
            if ($periodId) {
                if (is_array($periodId)) {
                    $defaultColumnsHref .= '&period[]=' . implode('&period[]=', $periodId);  
                } else {
                    $defaultColumnsHref .= '&period=' . $periodId;    
                }
            }
            if ($groupFilterValue) {
                $defaultColumnsHref .= '&groupFilterValue=' . urlencode($groupFilterValue);    
            }
            if ($columnFilterSelectValue) {
                $defaultColumnsHref .= '&columnFilterSelectValue=' . $columnFilterSelectValue;    
            }
            if ($columnFilterValue) {
                $defaultColumnsHref .= '&columnFilterValue=' . urlencode($columnFilterValue);    
            }
            echo '&nbsp;&nbsp;<a href="' . $defaultColumnsHref .'" onClick="document.defaultColumnsForm.submit(); return false;">Restore default columns</a>'; 
            ?>
            </form>
            
            <form name="defaultColumnsForm" id="defaultColumnsForm" action="<?php echo $formActionHref; ?>" method="POST">
            <?php
            /*
            foreach ($requiredColumns as $field => $label) {
                echo <<<EOB
                <input type="hidden" name="showColumns[]" value="{$field}" />       
EOB;
            }
            */
            ?>
                <input type='hidden' name='department_id' value='<?= $department_id ?>'>
                <input type='hidden' name='round' value='<?= $round ?>'>
                <input type='hidden' name='decision' value='<?= $decision ?>'>
                <input type='hidden' name='groups' value='<?= $groups ?>'>
                <input type='hidden' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
                <input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
                <input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                <input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
                <input type='hidden' name='search_text' value='<?= $search_text ?>'>
                <?php
                if ($periodId) {
                    if (is_array($periodId)) {
                        foreach($periodId as $periodIdItem) {
                            echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                        }
                    } else {
                        echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                    }
                }
                ?>
            </form>
            </td>
            
            
		</tr>
		
		<?PHP

			// semiblind  for admin committee only
            // PLB changed to allow CSD faculty to have semiblind review
            // DAS added LTI faculty
			//if ( $usertypeid != 3 )
            if ( $usertypeid != 3 || $department_id == 1 || $department_id == 5 || $department_id == 6)
			{
		?>
		<tr>
			<!--
            <td align="right" valign="top">
				<b>Semi-Blind:</b>
			</td>
			<td>
				<form action="<?php echo $formActionHref; ?>" method="POST" name="semiblindForm" id="semiblindForm">
					<input type='hidden' name='department_id' value='<?= $department_id ?>'>
					<input type='hidden' name='round' value='<?= $round ?>'>
					<input type='hidden' name='decision' value='<?= $decision ?>'>
					<input type='hidden' name='groups' value='<?= $groups ?>'>
					<input type='hidden' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
                    <input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
                    <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'> 
                    <input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
                    <input type='hidden' name='search_text' value='<?= $search_text ?>'>						
					<input type="hidden" name="setSemiblindStatus" value="true">
                <?php
                if ($periodId) {
                    if (is_array($periodId)) {
                        foreach($periodId as $periodIdItem) {
                            echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                        }
                    } else {
                        echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                    }
                }
                ?>
                    
                    <?php makeHiddenShowColumns( $selectedColumns ); 
                    
                    // PLB added test to disable semi-blind control for MSE-MSIT departments 1/21/10.
                    $semiblindDisabled = '';
                    if ($department_id == 18 || $department_id == 19 ||
                        $department_id == 47 || $department_id == 48 || 
                        $department_id == 52 || isSocialDecisionSciencesDepartment($department_id)) 
                    {
                        if ($usertypeid != 0 && $usertypeid != 1) {
                            // Disable for everyone but su and admin
                            $semiblindDisabled = 'disabled';    
                        }          
                    }
                    ?>
                    
					<input type="checkbox" name="chkSemiblind" value="1" onclick="this.form.submit()" <?= $chkSemiblind_checked ?> <?= $semiblindDisabled ?> >
				</form>
			</td>
            -->
		</tr>
		<?PHP } ?>
				
	</table>
</div>

<!-- APPLICANTS TABLE
  ---------------------------------------------------------------------------------  -->
 <script type="text/javascript">
    document.body.className = "js";
</script> 
 
 
 <div id="applicants" >


  
 	<!-- PLB uncommented 01/12/09 -->
 	<?PHP
		if ( $search_text != "" )
		{
		// include clear search button
	?>
	<div id="clearSearch">				
		<form action="<?php echo $formActionHref; ?>" method="POST" name="clearSearchForm" id="clearSearchForm">
			<input type='hidden' name='department_id' value='<?= $department_id ?>'>
			<input type='hidden' name='round' value='<?= $round ?>'>
			<input type='hidden' name='decision' value='<?= $decision ?>'>
			<input type='hidden' name='groups' value='<?= $groups ?>'>
            <!-- We don't want to keep/execute the filters after searching -->
            <!--
            <input type='hidden' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
			<input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
            <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
            -->
            <input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
            <?php
            if ($periodId) {
                if (is_array($periodId)) {
                    foreach($periodId as $periodIdItem) {
                        echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                    }
                } else {
                    echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                }
            }
            ?>			
            <?php makeHiddenShowColumns( $selectedColumns ); ?>
            
            <input type='submit' name='search_submit' value='Clear Search'>
		</form>	
	</div>		
	<?PHP } ?> 	
	
	<div id="search">
		<form action="<?php echo $formActionHref; ?>" method="POST" name="searchForm" id="searchForm">
			<b>Search Applications:</b>
			<input type='hidden' name='department_id' value='<?= $department_id ?>'>
			<input type='hidden' name='round' value='<?= $round ?>'>
			<input type='hidden' name='decision' value='<?= $decision ?>'>
			<input type='hidden' name='groups' value='<?= $groups ?>'>
            <!-- We don't want to keep/execute the filters after searching -->
            <!--
            <input type='hidden' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
            <input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
            <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
            -->
            <input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
			<input type='text'name='search_text' value='<?= $search_text ?>'>
            <?php
            if ($periodId) {
                if (is_array($periodId)) {
                    foreach($periodId as $periodIdItem) {
                        echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                    }
                } else {
                    echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
                }
            }
            ?>			
            <?php makeHiddenShowColumns( $selectedColumns ); ?>
            
            <input type='submit' name='search_submit' value='Search'>
		</form>		
	</div>  
	
<?PHP
	if ($recordCount)
	{
?> 
	<div id="filter">
		<form action="<?php echo $formActionHref; ?>" name="filterForm" id="filterForm">
			<b>Filter:</b>
			<select name='column' id='column'>";
				<?= $filterColumns ?>
			</select> 	
			<!--
            <input name='filt' id='columnFilter' onkeyup="filterTable( this );" onkeydown="return preventSubmit(event);" type='text'>
		    -->
            <input name='filt' id='columnFilter' type='text'>
            <input type="button" id="applyFilter" name="applyFilter" value="Filter" />
            <input type="button" id="clearFilter" name="clearFilter" value="Clear Filter" />
        </form>
	</div> 					
	
	<div id="sortTip">
		<span id="tip">TIP!</span> 
		Sort multiple columns simultaneously by holding down the shift key and clicking a second, third or even fourth column header! 
	</div>	
	
	<!-- Edit form -->
	<form action="<?php echo $formActionHref; ?>" method="POST" name="editForm" id="editForm">
		<input type='hidden' name='department_id' value='<?= $department_id ?>'>
		<input type='hidden' name='round' value='<?= $round ?>'>
		<input type='hidden' name='decision' value='<?= $decision ?>'>
		<input type='hidden' name='groups' value='<?= $groups ?>'>
        <input type='hidden' name='groupFilterValue' class='groupFilterValue' value='<?= $groupFilterValue ?>'>
		<input type='hidden' name='columnFilterSelectValue' class='columnFilterSelectValue' value='<?= $columnFilterSelectValue ?>'>
        <input type='hidden' name='columnFilterValue' class='columnFilterValue' value='<?= $columnFilterValue ?>'>
        <input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'> 
        <input type='hidden' name='chkSemiblind' value='<?= $chkSemiblind ?>'>
		<input type='hidden'name='search_text' value='<?= $search_text ?>'>
		<input type='hidden' name='userid' value=''>
        <?php
        if ($periodId) {
            if (is_array($periodId)) {
                foreach($periodId as $periodIdItem) {
                    echo '<input type="hidden" name="period[]" value="' . $periodIdItem . '"/>';    
                }
            } else {
                echo '<input type="hidden" name="period" value="' . $periodId . '"/>';   
            }
        }
        ?>		
	</form>	


	<div id="dataGrid">
    <div style="clear: both;">
        &nbsp;
        <b><span id="rowCount"><?php echo $recordCount; ?></span> of <?php echo $recordCount; ?></b> 
        records displayed
    </div>
    <?PHP
		$dataGrid->render();
	?>
    </div>
	
 	<?PHP
		}
		else if ( $search_text != "" )
		{
	?>
 			<div id='noResults'><b>Your search - <i><?= $search_text ?></i> - did not match any documents.</b></div>		
	<?PHP		
		} 
		else 
		{
			print "<div id='noResults'><b>No applicants have been assigned to you.</b></div>";
		}
	?>
	
<!-- PW code ends here -->


<script type="text/javascript" language="javascript">
<?php
$tableSortListArray = array_map( 'intval', explode(',', $tableSortList) );
if ( count($tableSortListArray) % 2 == 0) {
    $tableSortListJson = json_encode( array_chunk($tableSortListArray, 2) );    
} else {
    $tableSortListJson = '[]';   
}
echo 'var globalTableSortList = "' . $tableSortListJson . '";';
?>

$(document).ready(function(){
    
    $("a.print").click(function() {
        var idArray = $(this).attr("id").split("_"); 
        var userId = idArray[1];
        var applicationId = idArray[2];
        window.open("../review/userroleEdit_student_print.php?id="+userId+"&applicationId="+applicationId);
        return false;
    });
});
</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php'; 
?>
