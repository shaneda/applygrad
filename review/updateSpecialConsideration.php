<?php
// include the db classes
include_once '../inc/config.php';
include "./classes/class.db_applyweb.php";
include "./classes/class.db_special_consideration.php";

$application_id = $_GET['application_id'];
$reviewer_id = $_GET['reviewer_id'];
$special_consideration = $_GET['special_consideration'];

// update the db
$db_special_consideration = new DB_SpecialConsideration();

//DebugBreak();

$status = $db_special_consideration->updateSpecialConsideration($application_id, $reviewer_id, $special_consideration);
echo $status;

?>
