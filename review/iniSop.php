<? 
$exclude_scriptaculous = TRUE;
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';

$appid = filter_input(INPUT_GET, 'appid', FILTER_VALIDATE_INT);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Statement of Purpose</title>
<link href="../css/app2.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="#ffffff">
<?php 
// Include INI SOP
include '../inc/iniSopPrint.inc.php';
?>
</body>
</html>