<?php
$startTime = microtime(TRUE);
ini_set('memory_limit', '64M');

// standard includes
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
                                               
// Include the data classes.
include "../classes/class.NormalizationListData.php";
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

include '../inc/reviewListFunctions.inc.php';

// User making the request
$luuId = $_SESSION['A_userid'];
$usertypeId = $_SESSION['A_usertypeid'];

// Save (AJAX request)
$save = filter_input(INPUT_POST, 'save', FILTER_VALIDATE_INT);
if ( $save && ($save == 1) && $usertypeId == 3 ) {
    $saveGroupId = filter_input(INPUT_POST, 'groupId', FILTER_VALIDATE_INT);
    $saveRound = filter_input(INPUT_POST, 'round', FILTER_VALIDATE_INT);
    $savePeriodId = filter_input(INPUT_POST, 'periodId', FILTER_VALIDATE_INT);
    $saveRankComment = filter_input(INPUT_POST, 'rankComment', FILTER_UNSAFE_RAW);
    $rankings = json_decode($_POST['rankings']);
    $rankingCount = count($rankings);
        
    if ($saveRankComment) {
        
        $updateNoteQuery1 = "INSERT INTO group_rank 
            (group_id, round, period_id, lu_users_usertypes_id, comment, ranking) 
            VALUES (" . intval($saveGroupId) . ", "
            . intval($saveRound) . ", "
            . intval($savePeriodId) . ", " 
            . intval($luuId) . ", '"
            . mysql_real_escape_string($saveRankComment) . "', '"
            . mysql_real_escape_string(implode(',', $rankings)) . "')
            ON DUPLICATE KEY UPDATE 
            comment = '" . mysql_real_escape_string($saveRankComment) . "',
            ranking = '" . mysql_real_escape_string(implode(',', $rankings)) . "'";
        mysql_query($updateNoteQuery1);
        
        $updateNoteQuery2 = "INSERT INTO group_rank_comment 
            (group_id, round, period_id, lu_users_usertypes_id, comment) 
            VALUES (" . intval($saveGroupId) . ", "
            . intval($saveRound) . ", "
            . intval($savePeriodId) . ", " 
            . intval($luuId) . ", '"
            . mysql_real_escape_string($saveRankComment) . "') 
            ON DUPLICATE KEY UPDATE 
            comment = '" . mysql_real_escape_string($saveRankComment) . "'";
        mysql_query($updateNoteQuery2);
    }
    
    for ($i = 0; $i < $rankingCount; $i++) {
        $updateQuery = "INSERT INTO group_rank_member VALUES 
            (" . intval($saveGroupId) . ","
            . intval($saveRound) . ","
            . intval($savePeriodId) . ","
            . intval($rankings[$i]) . ","
            . intval($luuId) . "," 
            . ($i + 1) . ")
            ON DUPLICATE KEY UPDATE
            lu_users_usertypes_id = " . intval($luuId) . ", 
            rank = " . ($i + 1);
        mysql_query($updateQuery);
    }
    echo '1';
    exit;
}

// Main request vars
$departmentId = filter_input(INPUT_GET, 'departmentId', FILTER_VALIDATE_INT);
$round = filter_input(INPUT_GET, 'round', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);
$requestGroupId = filter_input(INPUT_GET, 'group', FILTER_VALIDATE_INT);
$sort = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING);

// Check request vars if not AJAX save.
if ( (!$departmentId && !$groupId) 
    || !$periodId
    || ($_SESSION['A_usertypeid'] != 0 && $_SESSION['roleDepartmentId'] != $departmentId ) ) 
{
    header('Location: ../admin/home.php');
    exit;
}

// Group/coordinator/member data. 
if ($usertypeId == 3) {
    include '../inc/inc.rankApplicationsData.php';
} else {
    include '../inc/inc.rankApplicationsSummaryData.php';   
}

// Period display
$departmentName = '';
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = '';
if ($periodId) {

    $applicationPeriodDisplay = getDepartmentName($departmentId);
    
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    
    $startDate = $period->getStartDate('Y-m-d');
    $endDate = $period->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay .= '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

// Header
$pageTitle = 'Area Coordinator Rankings';

$pageCssFiles = array(
    '../css/jquery.ui.all.css',
    '../css/reviewApplications.css',
    '../css/rankApplications.css'
    );

$headJavascriptFiles = array(
    '../javascript/jquery-1.6.2.min.js',
    '../javascript//jquery-ui-1.8.16.custom.min.js',
    '../javascript/json2.js'
    );

include '../inc/tpl.pageHeader.php';
?>

<div id="pageHeading">
    <div id="departmentName"><?php echo $applicationPeriodDisplay; ?></div>            
    <div id="sectionTitle"><?php echo $pageTitle; ?>: Round <?php echo $round; ?></div>
</div>

<?
if ($usertypeId == 3) {
    if (count($groups) == 0) {
        echo '<div style="clear: both; font-weight: bold; margin-left: 20px;">
            <br>You have not been assigned to any ranking groups.</div>';
    } else {
        include '../inc/tpl.rankApplications.php';
    }
} else {
    include '../inc/tpl.rankApplicationsSummary.php';    
}
?>

<form action="" method="POST" name="editForm" id="editForm">
    <input type='hidden' name='userid' value=''>
</form>

<script type="text/javascript">

function openForm(userid, formname) { 
    var editForm = document.getElementById('editForm');
    editForm.elements['userid'].value = userid;
    editForm.action = formname;
    editForm.target = '_blank';
    editForm.submit();
}

$(document).ready(function() {
    
    $(".draggable").draggable({
        handle: '.draggable_handle', 
        cursor: 'pointer', 
        stack: '.draggable', 
        containment: 'parent'
    });
    //.resizable();
    
    $(".sortable").sortable({
        handle: 'span.name',
        cursor: 'move'    
    });

    $(".save").click(function() {
        
        var idPieces = $(this).attr("id").split("_");
        var groupId = idPieces[1];
        var round = idPieces[2];
        var periodId = idPieces[3];

        var rankComment = $("#groupRankComment_" + groupId).val(); 
        
        var rankings = new Array();
        $("#groupMembers_" + groupId).find("li").each(function (i) {
            var applicationIdPieces = $(this).attr("id").split("_");
            var applicationId = applicationIdPieces[1];
            rankings.push(applicationId);
        });

        $.post(
            'rankApplications.php', 
            { 
                save: "1",
                groupId: groupId,
                round: round,
                periodId: periodId,
                rankComment: rankComment, 
                rankings: JSON.stringify(rankings)
            },
            function(data){
                if (data != -1) {
                    $('#lastSavedInfo').html('');
                    alert('Rankings saved.');    
                } else {
                    alert(error);    
                }           
            },
            'text'
        );
        
    });
});
</script>


<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';


function makeLi($record) {
    
    global $departmentId;
    $applicationId = $record['application_id'];
    $luuId = $record['luu_id'];
    
    $li = '<li id="application_' . $applicationId . '" class="groupMember">';
    $li .= '<a href="javascript: openForm(\'' . $luuId . '\',';
    $li .= '\'../review/reviewApplicationSingle.php?applicationId=';
    $li .= $applicationId . '&v=2&r=2&d=' . $departmentId . '\')">';
    $li .= 'View</a>&nbsp;&nbsp;';
    $li .= '<span class="name">';
    $li .= '<i>(' . str_pad($record['score_rank'], 2, '0', STR_PAD_LEFT) . ' / '; 
    $li .= number_format($record['score_average'], 2) . ')</i>&nbsp;&nbsp;';
    $li .= $record['name'];
    $li .= '</span>';  
    $li .= '</li>';

    return $li;
}
?>