<?PHP
	
	// set golbal vars for directory and max file size
	define('DOC_DIR', '../../data/2009/ml_stats/'); 
	define('MAX_FILE_SIZE', '100000');
	
	// upload image
	function uploadFile()
	{	
		// initialize success/failure var
		$message = "";
		
		// test to see if a file has been uploaded
		if ( isset($_FILES['uploadfile']) )
		{
			// set destination path/name
			$fileName = basename( $_FILES['uploadfile']['name'] );
			$destination = DOC_DIR . $fileName; 
			
			// set temp file path/name
			$tmpFile = $_FILES['uploadfile']['tmp_name'];
			
			// move file from tmp to destination
			if( move_uploaded_file($tmpFile, $destination) ) 
			{
			    $message = "The file has been uploaded.";
			} else{
			    $message = "Failed to upload file.";
			}
		}
		
		// return message
		return $message;	
	}

	function deleteFile()
	{
		if ( isset($_POST['delete']) )
		{
			unset($_POST['delete']);

			foreach ( $_POST as $fileName => $val )
			{
				$pattern = '/@/';
				$replace = '.';
				$fileName = preg_replace($pattern, $replace, $fileName);				
				$filePath = DOC_DIR . $fileName;
				unlink( $filePath );
			}
		}
	}
		
	function getFileList()
	{
		$files = scandir( DOC_DIR );
		$fileList = array();
		if ( $files )
		{
			foreach ( $files as $fileName )
			{
				// exclude files beginning with a '.'
				$pattern = '/^\./';  
				if ( ! preg_match($pattern, $fileName) )
				{
					// get timestamp
					$filePath = DOC_DIR . $fileName;
					$fileDate = filemtime( $filePath );
					
					// format data
					$fileData['fileName'] = $fileName;
					$pattern = '/\./';
					$replace = '@';
					$fileData['alteredFileName'] = preg_replace($pattern, $replace, $fileName);
					$fileData['fileDate'] = date( "D M j, Y, g:i a", $fileDate);
					
					array_push( $fileList, $fileData );	
				}
			}
		}
		return $fileList;
	}
	
	// call upload image function 
	$message = uploadFile();
	deleteFile();
	$fileList = getFileList();

?>

<html>
<head>
	<title>Upload ML/Stats Application Files</title>
</head>
<body>

	<h1>Upload ML/Stats Application Files</h1>
	<div style="padding:10px"><?PHP print $message; ?></div>
	<form action="<?PHP print $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data" >
		
		<!-- input type="hidden" name="MAX_FILE_SIZE" value="<?= MAX_FILE_SIZE; ?>" / -->
	
		<table border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td>File:</td>
			<td><input type="file" name="uploadfile" ></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="Upload File"></td>
		</tr>
		</table>
	</form>
	
	<h2>Files</h2>
	<?PHP if ( $fileList ) { ?>
	<form action="<?PHP print $_SERVER['PHP_SELF']; ?>" method="POST">
	<input type='hidden' name='delete' value='delete'>
	<ul>
		<?PHP
			foreach ( $fileList as $fileData )
			{
				$fileName = $fileData['fileName'];
				$fileDate = $fileData['fileDate'];
				$alteredFileName = $fileData['alteredFileName'];
				
				$filePath = DOC_DIR . $fileName;
				print "<li><a href='$filePath'>$fileName</a> ( $fileDate ) <input type='submit' name='$alteredFileName' value='X'></li>";
			}	
		?>
	</ul>
	</form>
	<?PHP 
		} else {
			print "No application files have been uploaded";
		} 
	?>	
	
</body>