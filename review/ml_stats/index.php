<?PHP
	
	// inclues
	//include_once '../inc/session_admin.php';
	//print_r($_SESSION);
	
	// set golbal vars for directory and max file size
	define('DOC_DIR', '../../data/2009/ml_stats/'); 
	define('MAX_FILE_SIZE', '100000');
	

	function getFileList()
	{
		$files = scandir( DOC_DIR );
		$fileList = array();
		if ( $files )
		{
			foreach ( $files as $fileName )
			{
				$filePath = DOC_DIR . $fileName;
				$fileDate = filemtime( $filePath );
				
				$fileData['fileName'] = $fileName;
				$fileData['fileDate'] = date( "D M j, Y, g:i a", $fileDate);
				
				$pattern = '/^\./';  // exclude files beginning with a '.'
				if ( ! preg_match($pattern, $fileName) ) array_push( $fileList, $fileData );					
				//if ( $fileName != "." AND $fileName != ".." ) array_push( $fileList, $fileData );	
			}
		}
		return $fileList;
	}
	
	// call upload image function 
	$fileList = getFileList();

?>

<html>
<head>
	<title>ML/Stats Application Files</title>
</head>
<body>

	<h1>ML/Stats Application Files</h1>
	
	<?PHP if ( $fileList ) { ?>
	<ul>
		<?PHP
			foreach ( $fileList as $fileData )
			{
				$fileName = $fileData['fileName'];
				$fileDate = $fileData['fileDate'];
				
				
				$filePath = DOC_DIR . $fileName;
				print "<li><a href='$filePath'>$fileName</a> ( $fileDate )</li>";
			}	
		?>
	</ul>
	<?PHP 
		} else {
			print "No application files have been uploaded";		
		} 
	?>	
	
</body>