<?php
header("Content-type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: attachment; filename=export.csv");
//header("Pragma: no-cache");
header("Expires: 0");

/*
$doctypeDeclaration = '<?xml version="1.0"?>
<!DOCTYPE root [
<!ENTITY nbsp "&#160;">
]>';
*/
$document = str_replace('&nbsp;', ' ', $_REQUEST['datatodisplay']);
//$document = str_replace('<br />', '|', $_REQUEST['datatodisplay']); 
//$document = str_replace('<br /><br />', '|', $_REQUEST['datatodisplay']); 
$dom = new DomDocument();
$dom->loadHTML($document);

$rows = $dom->getElementsByTagName('tr');
$csv = '';
//DebugBreak();
foreach($rows as $row) {
    
    $thArray = array();
    $tdArray = array(); 
    
    $ths = $row->getElementsByTagName('th');
    foreach ($ths as $th) {
        $thText = $th->nodeValue;
        $thArray[] = '"' . $thText . '"';    
    }
    if ( !empty($thArray) ) {
        $csv .= implode(',', $thArray);   
    }
    
    $tds = $row->getElementsByTagName('td');
    foreach ($tds as $td) {
        $tdNodeArray = array();
        $tdNodes = $td->childNodes;
        $tdNodeCount = $tdNodes->length;
        if ($tdNodeCount > 1) {
            foreach($tdNodes as $tdNode) {
                $nodeType = $tdNode->nodeType;
                $tdNodeChildren = $tdNode->childNodes;
                $tdNodeChildrenCount = $tdNodeChildren->length;
                if ($tdNodeChildrenCount > 0) {
                    foreach ($tdNodeChildren as $tdNodeChild) {
                        $nodeType = $tdNodeChild->nodeType;
                        $nodeValue = trim($tdNodeChild->nodeValue);
                        if ($nodeType == XML_TEXT_NODE && $nodeValue != '') {
                            $tdNodeArray[] = str_replace('"', '""', $nodeValue);
                        }     
                    }   
                } else {
                    $nodeValue = trim($tdNode->nodeValue);
                    if ($nodeType == XML_TEXT_NODE && $nodeValue != '') {
                        $tdNodeArray[] = str_replace('"', '""', $nodeValue);
                    }    
                }     
            }
            $tdText = implode('|', $tdNodeArray);
        } else {
            $tdText = str_replace('"', '""', $td->nodeValue);    
        }
        $tdArray[] = '"' . $tdText . '"'; 
    }
    if ( !empty($tdArray) ) {
        $csv .= implode(',', $tdArray);   
    }
    
    $csv .= "\n";
} 

echo $csv;
?>