<?php
/*
Possible round/promotion states:
Round 1 - default
Round 1 - demoted from round 2
Round 2 - voted from round 1
Round 2 - promoted from round 1
Round 2 - demoted from round 3
Round 3 - voted from round 2
Round 3 - promoted from round 2
Round 3 - demoted from final
Final - voted from round 3
Final - promoted from round 3

Key to understanding lu_application_round2:
0 = default (no action)
1 = promoted to round 2
2 = demoted from round 2
*/

ini_set('memory_limit', '256M');
header("Cache-Control: no-cache");

// Standard includes.
/*
* // session_admin.php has the config and db includes!
* include_once '../inc/db_connect.php';
* include_once "../inc/config.php";
*/
include_once "../inc/session_admin.php";

// include the datagrid package
require('Structures/DataGrid.php');

// include the db classes
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

include "../classes/class.db_application_list.php";
include "../classes/class.Department.php";  

// Handle the GET request.
$departmentId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$periodId = filter_input(INPUT_GET, 'period', FILTER_VALIDATE_INT);
$roundLimit = filter_input(INPUT_GET, 'roundLimit', FILTER_VALIDATE_INT);

if (!$periodId) {
    if (is_array($_GET['period'])) {
        foreach($_GET['period'] as $periodIdRaw) {
            $periodId[] = filter_var($periodIdRaw, FILTER_VALIDATE_INT);    
        }
    }
}

if (!$departmentId && !$periodId) {
    header('Location: ../admin/home.php');
}

// Instantiate DB_Applyweb class.
$applicationList = new DB_ApplicationList(); 

// Get department programs.
$departmentProgramRecords = Department::getPrograms($departmentId);
$departmentProgramIds = array();
foreach ($departmentProgramRecords as $departmentProgramRecord) {
    $departmentProgramIds[] = $departmentProgramRecord['id'];
}

// Get promotion status.
$promotionStatusQuery = "SELECT DISTINCT promotion_history.*
                            FROM promotion_history
                            INNER JOIN period_application
                                ON promotion_history.application_id = period_application.application_id
                            /*
                            INNER JOIN lu_application_programs
                                ON period_application.application_id = lu_application_programs.application_id
                            INNER JOIN lu_programs_departments
                                ON lu_application_programs.program_id = lu_programs_departments.program_id
                            */
                            INNER JOIN
                            (
                            SELECT application_id,
                            program_id,
                            MAX(status_time) AS status_time
                            FROM promotion_history
                            GROUP BY application_id, program_id
                            ) AS promotion_latest
                                ON promotion_history.application_id = promotion_latest.application_id
                                AND promotion_history.program_id = promotion_latest.program_id
                                AND promotion_history.status_time = promotion_latest.status_time";
if (is_array($periodId)) {
    $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
    $promotionStatusQuery .= " WHERE period_application.period_id IN " . $admissionPeriodIds;
} else {
    $promotionStatusQuery .= " WHERE period_application.period_id = " . $periodId;   
}
$promotionStatusQuery .= " AND promotion_history.program_id IN (";
$promotionStatusQuery .= implode(',', $departmentProgramIds) . ")";
$promotionStatusArrayRaw = $applicationList->handleSelectQuery($promotionStatusQuery);
$promotionStatusArray = array();
foreach ($promotionStatusArrayRaw as $promotionStatusRecord) {
    $statusApplicationId = $promotionStatusRecord['application_id'];
    $statusProgramId = $promotionStatusRecord['program_id']; 
    $promotionStatusArray[$statusApplicationId][$statusProgramId] = $promotionStatusRecord;    
}

// Get round 1 voting for round 2.
$round2Query = "SELECT application.id AS application_id, 
                  round2_votes.min_vote,
                  round2_votes.max_vote,
                  round2_votes.vote_count 
                  FROM application
                  INNER JOIN period_application ON application.id = period_application.application_id
                  LEFT OUTER JOIN ( 
                      SELECT application_id, 
                      MIN( IFNULL(round2, 0) ) as min_vote,
                      MAX( IFNULL(round2, 0) ) as max_vote,
                      COUNT(round2) AS vote_count 
                      FROM review
                      INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id 
                        AND lu_users_usertypes.usertype_id = 2
                      WHERE review.round = 1
                      AND review.department_id = " . $departmentId . "
                      GROUP BY application_id
                      ) AS round2_votes ON application.id = round2_votes.application_id";
if (is_array($periodId)) {
    $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
    $round2Query .= " WHERE period_application.period_id IN " . $admissionPeriodIds;
} else {
    $round2Query .= " WHERE period_application.period_id = " . $periodId;   
}
$round2Query .= " GROUP BY application.id";
$round2Array = $applicationList->handleSelectQuery($round2Query, 'application_id');

// Get round2 voting for round 3.
$departmentRecord = Department::getRecord($departmentId);
$enableRound3 = $departmentRecord['enable_round3']; 


if ($enableRound3) {

    $round3Query = "SELECT application.id AS application_id, 
                  round2_votes.min_vote,
                  round2_votes.max_vote,
                  round2_votes.vote_count 
                  FROM application
                  INNER JOIN period_application ON application.id = period_application.application_id
                  LEFT OUTER JOIN ( 
                      SELECT application_id, 
                      MIN( IFNULL(round2, 0) ) as min_vote,
                      MAX( IFNULL(round2, 0) ) as max_vote,
                      COUNT(round2) AS vote_count 
                      FROM review
                      INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id 
                        AND lu_users_usertypes.usertype_id = 2
                      WHERE review.round = 2
                      AND review.department_id = " . $departmentId . "
                      GROUP BY application_id
                      ) AS round2_votes ON application.id = round2_votes.application_id";
    if (is_array($periodId)) {
        $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
        $round3Query .= " WHERE period_application.period_id IN " . $admissionPeriodIds;
    } else {
        $round3Query .= " WHERE period_application.period_id = " . $periodId;   
    }
    $round3Query .= " GROUP BY application.id";
    $round3Array = $applicationList->handleSelectQuery($round3Query, 'application_id');    
}

$enableRound4 = $departmentRecord['enable_round4']; 


if ($enableRound4) {

    $round4Query = "SELECT application.id AS application_id, 
                  round3_votes.min_vote,
                  round3_votes.max_vote,
                  round3_votes.vote_count 
                  FROM application
                  INNER JOIN period_application ON application.id = period_application.application_id
                  LEFT OUTER JOIN ( 
                      SELECT application_id, 
                      MIN( IFNULL(round3, 0) ) as min_vote,
                      MAX( IFNULL(round3, 0) ) as max_vote,
                      COUNT(round3) AS vote_count 
                      FROM review
                      INNER JOIN lu_users_usertypes ON review.reviewer_id = lu_users_usertypes.id 
                        AND lu_users_usertypes.usertype_id = 2
                      WHERE review.round = 3
                      AND review.department_id = " . $departmentId . "
                      GROUP BY application_id
                      ) AS round3_votes ON application.id = round3_votes.application_id";
    if (is_array($periodId)) {
        $admissionPeriodIds = '(' . implode(',' , $periodId) . ')';
        $round4Query .= " WHERE period_application.period_id IN " . $admissionPeriodIds;
    } else {
        $round4Query .= " WHERE period_application.period_id = " . $periodId;   
    }
    $round4Query .= " GROUP BY application.id";
    $round4Array = $applicationList->handleSelectQuery($round4Query, 'application_id');    
}

// Set data array to bind to datagrid.
// Get application list.
$applicationRecordsAll = $applicationList->getPromoteByDepartment($departmentId, NULL, NULL, $periodId);
$applicationRecords = array();

for($i = 0; $i < count($applicationRecordsAll); $i++) {
    
    $applicationId = $applicationRecordsAll[$i]['application_id'];
    $programId = $applicationRecordsAll[$i]['program_id'];
    
    $currentRound = 1; 
    $votedRound = 1;
    $promotionStatusRound = 0;
    $promotionStatusMethod = NULL;
    $promotionStatusTime = NULL;
    if ( isset($promotionStatusArray[$applicationId][$programId]) ) {
        $promotionStatusRound = $promotionStatusArray[$applicationId][$programId]['round'];
        $promotionStatusMethod = $promotionStatusArray[$applicationId][$programId]['promotion_method'];
        $promotionStatusTime = $promotionStatusArray[$applicationId][$programId]['status_time'];
         
    }
    
    if ( isset($round2Array[$applicationId]) ) {
        
        $round1MinVote = $round2Array[$applicationId]['min_vote'];
        $round1MaxVote = $round2Array[$applicationId]['max_vote'];
        $round1VoteCount = $round2Array[$applicationId]['vote_count'];
        
        if ($departmentId == 1) {           // CSD

            if ($round1MinVote != 0 && $round1VoteCount >= 2) {
                $votedRound = 2;        
            }

        } elseif ($departmentId == 74) {     // CS MS

            if ($round1MaxVote = 1 && $round1VoteCount > 0) {
                $votedRound = 2;        
            } 
            
        } elseif ($departmentId == 2) {     // ML

            if ($round1MinVote != 0 && $round1VoteCount >= 3) {
                $votedRound = 2;        
            }            

        } elseif ($departmentId == 58) {     // Statistics-MS

            if ($round1MinVote != 0 && $round1VoteCount >= 1) {
                $votedRound = 2;        
            }            
        }
        elseif ($departmentId == 91) {     // Psychology

            if ($round1MaxVote == 1) {
                $votedRound = 2;        
            }            
        }
    
    }

    if ( isset($round3Array[$applicationId]) ) {
        
        $round2MinVote = $round3Array[$applicationId]['min_vote'];
        $round2VoteCount = $round3Array[$applicationId]['vote_count'];
        
        if ($departmentId == 1) {           // CSD

            if ($votedRound == 2 && $round2MinVote != 0 && $round2VoteCount >= 2) {
                $votedRound = 3;        
            }
            
        } elseif ($departmentId == 2) {     // ML

            if ($votedRound == 2 && $round2MinVote != 0 && $round2VoteCount >= 3) {
                $votedRound = 3;        
            }            
        }
    } 
    
    if ( isset($round4Array[$applicationId]) ) {
        
        $round3MinVote = $round4Array[$applicationId]['min_vote'];
        $round3VoteCount = $round4Array[$applicationId]['vote_count'];
        
        if ($departmentId == 1) {           // CSD

            if ($votedRound == 3 && $round3MinVote != 0 && $round3VoteCount >= 2) {
                $votedRound = 4;        
            }
            
        } elseif ($departmentId == 2) {     // ML

            if ($votedRound == 3 && $round3MinVote != 0 && $round3VoteCount >= 3) {
                $votedRound = 4;        
            }            
        }
    }
    
    if ($promotionStatusRound) {
        $currentRound = $promotionStatusRound;
    } else {    
        $currentRound = $votedRound;
    }
    
    // Show only rounds selected in limit filter.
    if ( ($roundLimit > 1) && ($currentRound < $roundLimit) ) {
        continue;
    }
    
    $applicationRecords[$i] = $applicationRecordsAll[$i];
    $applicationRecords[$i]['voting_round'] = $votedRound;
    $applicationRecords[$i]['promotion_method'] = $promotionStatusMethod;
    $applicationRecords[$i]['status_time'] = $promotionStatusTime;
    $applicationRecords[$i]['unset_round'] = $votedRound;
    $applicationRecords[$i]['round'] = $currentRound;
     
}

// Create the DataGrid, Bind it's Data Source and Define it's columns
$dg = new Structures_DataGrid();
// bind application list data using array driver
$dg->bind($applicationRecords ,array() , 'Array');

// create the columns
$dg->addColumn(new Structures_DataGrid_Column('', 'user_id', NULL, array('width' => '5%', 'align' => 'center'), null, 'printPrintLink()', $dg->getCurrentRecordNumberStart()));
$dg->addColumn(new Structures_DataGrid_Column('Name', 'name', NULL, array('width' => '20%', 'align' => 'left'), null, 'printFullName()'));
$dg->addColumn(new Structures_DataGrid_Column('Program', 'program', NULL, array('width' => '25%', 'align' => 'left'), null));
$dg->addColumn(new Structures_DataGrid_Column('Current Round', 'round', NULL, array('width' => '10%', 'align' => 'left'), null, 'printPromotion()'));
$dg->addColumn(new Structures_DataGrid_Column('Promote/Demote to Round', 'promotion_status', NULL, array('width' => '35%', 'align' => 'center'), null, 'printPromoteRadio2()'));

// Define the Look and Feel
$dg->renderer->setTableHeaderAttributes(array('bgcolor' => '#CCCCCC'));
$dg->renderer->setTableEvenRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu')
    );
$dg->renderer->setTableOddRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu')
    );
$dg->renderer->setTableAttribute('width', '100%');
$dg->renderer->setTableAttribute('border', '0');
$dg->renderer->setTableAttribute('cellspacing', '0');
$dg->renderer->setTableAttribute('cellpadding', '5px');
$dg->renderer->setTableAttribute('class', 'datagrid');
$dg->renderer->sortIconASC = '&uArr;';
$dg->renderer->sortIconDESC = '&dArr;';


/*
* Set up header variables and include the standard page header
* so the browser can go ahead and process the <head>.
*/
$pageTitle = 'Promote/Demote Applicants';

$pageCssFiles = array(
    '../css/jquery.ui.all.css',
    '../css/reviewApplications.css',
    '../css/promoteByDepartment.css'
    );

$headJavascriptFiles = array(
    '../javascript/jquery-1.6.2.min.js',
    '../javascript//jquery-ui-1.8.16.custom.min.js',
    );
    
$pageJavascriptFiles = array(
    '../inc/scripts.js',
    '../javascript/promoteByDepartment.js'
    );

// Get the <head></head> and <body> template
include '../inc/tpl.pageHeader.php';

$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    
    // Compbio kluge 10/13/09: check to see if $_REQUEST['period'] was an array of ids 
    if (is_array($_REQUEST['period'])) {
        // The scs period will be first and the compbio period will be second
        $displayPeriodId = $_REQUEST['period'][1];    
    } else {
        $displayPeriodId = $_REQUEST['period']; 
    }
    
    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

//$department_name = $departmentRecord[0]['name'];
$department_name = $departmentRecord['name']; 
?>

<div id="pageHeading">
    <div id="departmentName"><?php echo $department_name . $applicationPeriodDisplay; ?></div>    

        <div id="sectionTitle">Promote/Demote Applicants
        <span style="font-size: 15px;">(n=<?php echo count($applicationRecords); ?>)</span>
        </div>

</div>
<br/>
<br/>

<form name="filterForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
<input type="hidden" name="id" id="departmentId" value="<?php echo $departmentId; ?>" />
<?php
if (is_array($periodId)) {
    foreach ($periodId as $arrayPeriodId) {
    ?> 
        <input type="hidden" name="period[]" value="<?php echo $arrayPeriodId; ?>" />     
    <?php   
    }
} else {
?>
    <input type="hidden" name="period" value="<?php echo $periodId; ?>" />
<?php  
}    

$option1Selected = $option2Selected = $option3Selected = $option4Selected = '';
switch ($roundLimit) {
    case 1:
        $option1Selected = 'selected="selected"';
        break;
    case 2:
        $option2Selected = 'selected="selected"';
        break;
    case 3:
        $option3Selected = 'selected="selected"';
        break;
    case 4:
        $option4Selected = 'selected="selected"';
        break;
    default:
}
?>

<div style="margin: 5px; clear: left; float: left;">
Show: 
<select name="roundLimit" onChange="document.filterForm.submit();">
    <option value="1" <?php echo $option1Selected; ?>>All Rounds</option>   
<?php
if ($enableRound3) {
?>
    <option value="2" <?php echo $option2Selected; ?>>Rounds 2 and 3 Only</option> 
    <option value="3" <?php echo $option3Selected; ?>>Round 3 Only</option> 
<?php    
    if ($enableRound4) {
?>
    <option value="4" <?php echo $option4Selected; ?>>Rounds 3 and 4 Only</option>
<?php 
    }
}   else {
?>
    <option value="2" <?php echo $option2Selected; ?>>Round 2 Only</option> 
<?php
}    
?>
</select>
</div>

<div style="margin: 5px; float: right;">
<input type="button" value="Refresh Data" onClick="document.filterForm.submit();" />    
</div>
</form>

<div id="promotionHistory" title="Promotion History" style="clear: both;"></div>

        
<div id="dataGrid" style="clear: both;">
<?php
// render the datagrid
$dg->render();
?>
</div>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php';

//############### callback functions for datagrid rendering

function printPrintLink($params, $recordNumberStart) {
    extract($params);
    $userId = $record['user_id'];
    $applicationId = $record['application_id'];
    $link = $params['currRow'] + $recordNumberStart . '.' . '&nbsp;&nbsp;<a class="menu print" href=""';
    $link .= ' onClick="window.open(\'../review/userroleEdit_student_print.php?id='. $userId . '&applicationId=' . $applicationId . '\'); return false;"';
    $link .= ' title="Print Application ' . $applicationId . '">View&nbsp;/&nbsp;Print</a>';
    //$link .= "<a class=\"menu\" href=\"\" title=\"Print Application\" onClick=\"window.open('../admin/userroleEdit_student_formatted.php?id=" . $userId . "'); return false;\">Print</a>";
    return $link;
}


function printFullName1($params)
{   
    extract($params);
    return "<b>" . $record['name'] . "</b><br /><a title=\"E-mail Applicant\" href=\"mailto:" . $record['email'] . "\">" . $record['email'] . "</a>";
}

function printFullName($params) {   
    extract($params);
    $name = '<span id="applicantName_' . $record['application_id'] . '_' . $record['program_id'] . '" style="font-weight: bold;">';
    $name .= $record['name'] . '</span><br />';
    $name .= '<a title="E-mail Applicant" href="mailto:' . $record['email'] . '">' . $record['email'] . '</a>'; 

    return $name;
}

function printRound2($params) {
    extract($params);
    $round = $record['round'];
    $id = 'round2Span_' . $record['application_id'] . '_' . $record['program_id'];
    if ($round == 2) {
        $round2 = '<span class="yes" id="' . $id . '">Y</span>';     
    } else {
        $round2 = '<span class="no" id="' . $id . '">N</span>';
    }
    return $round2;    
}

function printVote($params) {
    extract($params);
    
    // TEMPORARY test for ML
    if ( isset($record['voting_round']) ) {
    
        $round = $record['voting_round'];
        
    } else {
    
        $voteCount = $record['vote_count'];
        $minVote = $record['min_vote'];

        if ($minVote > 0 && $voteCount > 2) {
            $round = 2;
        } else {
            $round = 1;
        }
    
    }
    
    return $round;  
}

function printPromotion($params) {
    extract($params);
    
    // TEMPORARY test for ML
    if ( isset($record['voting_round']) ) {
    
        if ($record['promotion_method']) {
            
            if ($record['promotion_method'] == 'default') {
                $status = $record['voting_round'];
                if ($record['voting_round'] >  1) {
                    $status .= '&nbsp;&nbsp;<i>voting,&nbsp;reset</i>';
                } else {
                    $status .= '&nbsp;&nbsp;<i>reset</i>';     
                } 
            } else {
                //  RI summer scholars
                if ($record['round'] == 3 && $record['program_id'] == 100002) {
                    $status = $record['round'] . '&nbsp;&nbsp;<i>' . $record['promotion_method'] . '<br /><i>' . $record['status_time'] . '</i>'; 
                } else {
                       $status = $record['round'] . '&nbsp;&nbsp;<i>' . $record['promotion_method'] . '</i>';
                }   
            }
            
            
        } else {
            
            $status = $record['voting_round'];
            if ($record['voting_round'] >  1) {
                $status .= '&nbsp;&nbsp;<i>voting</i>';
            }   
        }
        
    } else {
        
    $round2 = $record['promotion_status'];
    switch ($round2) {
        
        case 1:      
            $status = 'promoted';
            break;

        case 2:
            $status = 'demoted';
            break;
        
        case 0:
        default:
            $status = 'default';
    }
    
    }
    
    $applicationId = $record['application_id'];
    $programId = $record['program_id'];
    $idKey = $applicationId . '_' . $programId;
    
    return '<span class="currentStatus" id="currentStatus_' . $idKey  . '">' . $status . '</span>';    
}


function printPromoteRadio($params) {

    extract($params);
    $application_id = $record['application_id'];
    $program_id = $record['program_id'];
    $promote_status = $record['promotion_status'];
    $round = $record['round'];
    $unsetRound = $record['unset_round'];
    $id_key = $application_id . '_' . $program_id;
    
    $normal_checked = $promoted_checked = $demoted_checked = '';
    $promotedStyle = $demotedStyle = '';
    if ($round == 1) {
        $promotedDisabled = '';
        $demotedStyle = 'style="color: gray;"';
        $demotedDisabled = 'disabled="disabled"';    
    }
    if ($round == 2) {
        $promotedDisabled = 'disabled="disabled"';
        $promotedStyle = 'style="color: gray;"';
        $demotedDisabled = '';    
    }    
    switch ($promote_status) {

        case 0:
        
            $normal_checked = "checked";
            break;
        
        case 1:
        
            $promoted_checked = "checked";
            break;  
        
        case 2:
        
           $demoted_checked = "checked";
           break;
    }
    
    
    $radio = '<input type="hidden" name="unsetRound" value="' . $unsetRound . '" id="unsetRound_' . $id_key . '"/>';
    
    $radio .= '<input type="radio" name="promotionStatus_' . $id_key . '" id="statusPromote_' . $id_key . '"';
    $radio .= ' class="promotionStatus" value="1"' . $promoted_checked . ' ' . $promotedDisabled . '>';
    $radio .= '&nbsp;<span ' . $promotedStyle . ' id="statusPromoteSpan_' . $id_key . '">Promote to Round 2</span> &nbsp;&nbsp;';
    
    $radio .= '<input type="radio" name="promotionStatus_' . $id_key . '" id="statusDemote_' . $id_key . '"';
    $radio .= ' class="promotionStatus" value="2"' . $demoted_checked . ' ' . $demotedDisabled . '>'; 
    $radio .= '<span ' . $demotedStyle . ' id="statusDemoteSpan_' . $id_key . '">&nbsp;Demote from Round 2</span>';
    
    $resetDisabled = '';
    if ($promoted_checked != "checked" && $demoted_checked != "checked") {
        $resetDisabled = 'disabled';
    }
    $radio .= ' &nbsp;&nbsp;<input style="font-size: 10px;" class="promotionReset"';
    $radio .= ' id="statusReset_' . $id_key . '" type="button" value="Unset" ' . $resetDisabled . '/>';
    
    return $radio;
}

function printPromoteRadio2($params) {

    global $enableRound3;
    global $enableRound4;
    
    extract($params);
    
    $applicationId = $record['application_id'];
    $programId = $record['program_id'];
    $idKey = $applicationId . '_' . $programId;
    
    $currentRound = $record['round']; 
    $votedRound = $record['unset_round'];
    $promotionMethod = $record['promotion_method'];
    
    $round1Checked = $round2Checked = $round3Checked = $round4Checked = '';
    switch ($currentRound) {
        
        case 1:      
            $round1Checked = 'checked';
            break;

        case 2:
            $round2Checked = 'checked';
            break;
        
        case 3:
            $round3Checked = 'checked';
            break;

         case 4:
            $round4Checked = 'checked';
            break;
            
        case 0:
        default:

    }
    
    $radio = '<input type="hidden" name="votedRound" value="' . $votedRound . '" id="votedRound_' . $idKey . '"/>';
    $radio .= '<input type="hidden" name="currentRound" value="' . $currentRound . '" id="currentRound_' . $idKey . '"/>';
    $radio .= '<input type="hidden" name="currentPromotionMethod" value="' . $promotionMethod . '" id="currentPromotionMethod_' . $idKey . '"/>';
    
    $radio .= '<input type="radio" name="round_' . $idKey . '" id="round1Radio_' . $idKey . '"';
    $radio .= ' class="promotionRound" value="1"' . $round1Checked . ' ' . '>&nbsp;1';
    
    $radio .= ' &nbsp;&nbsp;<input type="radio" name="round_' . $idKey . '" id="round2Radio_' . $idKey . '"';
    $radio .= ' class="promotionRound" value="2"' . $round2Checked . '>&nbsp;2'; 
    
    if ($enableRound3) {

        $radio .= ' &nbsp;&nbsp;<input type="radio" name="round_' . $idKey . '" id="round3Radio_' . $idKey . '"';
        $radio .= ' class="promotionRound" value="3"' . $round3Checked . '>&nbsp;3';        
    }
    
    if ($enableRound4) {

        $radio .= ' &nbsp;&nbsp;<input type="radio" name="round_' . $idKey . '" id="round4Radio_' . $idKey . '"';
        $radio .= ' class="promotionRound" value="4"' . $round4Checked . '>&nbsp;4';        
    }

    $resetDisabled = '';
    if ( ($promotionMethod == 'default') || ($currentRound == $votedRound) ) {
        $resetDisabled = 'disabled';
    }
    $radio .= ' &nbsp;&nbsp;&nbsp;<input style="font-size: 10px;" class="promotionReset"';
    $radio .= ' id="resetToVoting_' . $idKey . '" type="button" value="Set to Voting" ' . $resetDisabled . '/>';
    
    return $radio;
}

?>