<? 
$exclude_scriptaculous = TRUE;
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../inc/applicationFunctions.php';
include_once '../inc/specialCasesAdmin.inc.php';

// Ensure page reloads on forward/back navigation to prevent 
// loss of jquery-modified data.
header("Cache-Control: no-cache");

if(!isset($_SESSION['A_userid']))
{
	echo " <script language='JavaScript'>window.close();</script>";
	header("Location: index.php");
	
}else
{
	if(($_SESSION['A_userid'] == "" || $_SESSION['A_userid'] == -1) && $_SESSION['A_userid'] != 0)
	{
	echo $_SESSION['A_userid'];
		echo " <script language='JavaScript'>window.close();</script>";
		header("Location: index.php");		
	}
}

if ($_SESSION['A_usertypename'] == 'Super User' || $_SESSION['A_usertypename'] == 'Administrator' || $_SESSION['A_usertypename'] == 'Admissions Committee'
    || $_SESSION['A_usertypename'] == 'Faculty' || $_SESSION['A_usertypename'] == 'Admissions Chaire' || $_SESSION['A_usertypename'] == 'Area Coordinator') {

        // Include the data queries, variables.
        include '../inc/printViewData.inc.php';

        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <head>
        <title>Application of <?=$email?></title>
        <link href="../css/app2.css" rel="stylesheet" type="text/css" />
        </head>
        <body bgcolor="#ffffff">
        <?php 
        // include link to merged pdf
        include "../inc/pdf_link.inc.php";   

        // include big table
        include '../inc/tpl.printView.php';

        // Include INI SOP
        include '../inc/iniSopPrint.inc.php'; 

        // Include EM2 SOP
        include '../inc/em2SopPrint.inc.php';   

        // include recommend form answers
        include '../inc/printRecommendform.inc.php';
} else {
    echo "You are not authorized to view this page";
}

?>
</body>
</html>