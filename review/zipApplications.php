<?php
function zipFilesAndDownload($file_names,$archive_file_name,$file_path)
{
    chmod($archive_file_name,0666);
    unlink($archive_file_name);
    
        //echo $file_path;die;
    $zip = new ZipArchive();
    //create the file and throw the error if unsuccessful
    if ($zip->open($archive_file_name, ZIPARCHIVE::CREATE )!==TRUE) {
        exit("cannot open <$archive_file_name>\n");
    }
    //add each files of $file_name array to archive
    foreach($file_names as $files)
    {
        $new_filename = substr($files,strrpos($files,'/') + 1);
        $zip->addFile($file_path.$files,$new_filename);
        //echo $file_path.$files,$files."

    }
    $zip->close();
    //then send the headers to force download the zip file
    header("Content-type: application/octet-stream"); 
    header("Content-Disposition: attachment; filename=$archive_file_name");
    header("Content-length: " . filesize($archive_file_name));
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    readfile("$archive_file_name"); 
    exit;
}

if (isset ($_POST['mfiles']) && count($_POST['mfiles']) > 0) {
    //Download Files path
    $file_path=$_SERVER['DOCUMENT_ROOT'];
    //$file_path="/usr0/apache2";
    
    $files = json_decode($_POST['mfiles'][0], true);
    $file_names = array();
    foreach ($files as $file) {
        array_push($file_names, substr($file, 2));
    }
    $file_names = array_reverse($file_names);
    

//Archive name
$archive_file_name='mergedPdfs.zip';

zipFilesAndDownload($file_names,$archive_file_name,$file_path);   
    
} else {
    echo "No files selected for merge";
}

?>
