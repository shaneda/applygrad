<?php
$exclude_scriptaculous = TRUE;
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include_once '../inc/functions.php';
include_once '../inc/applicationFunctions.php';
include_once '../inc/specialCasesAdmin.inc.php';

$recommendId = filter_input(INPUT_GET, 'recommendId', FILTER_VALIDATE_INT);
$applicationId = filter_input(INPUT_GET, 'applicationId', FILTER_VALIDATE_INT); 

include '../inc/printViewData.inc.php';
include '../inc/reviewData.inc.php';

include '../inc/printRecommendform.inc.php';
?>