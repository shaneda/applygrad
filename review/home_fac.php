<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link href="../css/SCSStyles.css" rel="stylesheet" type="text/css" />
</head>

<body><form action="" method="post">
<div style="height:72px; width:781">
  <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="75%"><span class="title">Carnegie Mellon School for Computer Sciences</span><br />
        <strong>Online Admissions System</strong></td>
      <td align="right"><strong>Welcome, Brian Martin<br />
        </strong><a href="#">Log out</a></td>
    </tr>
  </table>
</div>
<div style="height:10px">
  <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /></div>
</div>
<br />
<br />
<table width="781" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <td colspan="3" valign="top" class="tblHead">Faculty Review Home </td>
    </tr>
  <tr>
    <td width="33%" valign="top" class="tblItem"><fieldset><legend class="subtitle">Tasks</legend>
	    <a href="#">Review Candidates</a>
    </fieldset> </td>
    <td width="34%" rowspan="2" valign="top" class="tblItem"><fieldset><legend class="subtitle">Recommended to You</legend>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td><strong>Reviewed</strong></td>
    <td><strong>Name</strong></td>
    <td><strong>Recommender</strong></td>
  </tr>
  <tr>
    <td class="tblItemAlt"><input type="checkbox" name="checkbox" value="checkbox" /></td>
    <td class="tblItemAlt"><a href="#" target="_blank">Jim Smith</a> </td>
    <td class="tblItemAlt"><a href="#" target="_blank">John Doe</a> </td>
  </tr>
</table>

	</fieldset> </td>
    <td width="33%" valign="top"><fieldset><legend class="subtitle">How You Scored</legend>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td><strong>Avg. Point Score </strong></td>
    <td>3</td>
  </tr>
  <tr>
    <td class="tblItemAlt">&nbsp;</td>
    <td class="tblItemAlt">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

	</fieldset> </td>
  </tr>
  <tr>
    <td class="tblItem"><fieldset style="background-color:#FFFFFF"><legend class="subtitle">Saved Views</legend>
	    <a href="#" target="_blank">Unnamed 1 <em>(01/01/2007)</em></a><br />
	    <a href="#" target="_blank">Unnamed 2 <em>(01/01/2007)</em></a>
	</fieldset> </td>
    <td><fieldset><legend class="subtitle">Recently Viewed</legend>
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td class="tblItem"><a href="#" target="_blank">Lastname, Firstname</a> </td>
  </tr>
  <tr>
    <td class="tblItemAlt"><a href="#" target="_blank">Lastname, Firstname</a></td>
  </tr>
  <tr>
    <td class="tblItem"><a href="#" target="_blank">Lastname, Firstname</a></td>
  </tr>
</table>

	</fieldset> </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
