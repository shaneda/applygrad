<?php
/*
* Allow promote/demote only for admins and committee chairs.
*/
 
session_start();
if ( !isset($_SESSION['A_usermasterid'])
        || ($_SESSION['A_usertypeid'] != 0
        && $_SESSION['A_usertypeid'] != 1
        && $_SESSION['A_usertypeid'] != 10) )
{
    exit;    
}


/*
* Handle the request variables.
*/

//$promotionStatus = $_GET['promotion_status'];
$mode = filter_input(INPUT_GET, 'mode', FILTER_SANITIZE_STRING);
$applicationId = filter_input(INPUT_GET, 'application_id', FILTER_VALIDATE_INT);
$programId = filter_input(INPUT_GET, 'program_id', FILTER_VALIDATE_INT);
$departmentId = filter_input(INPUT_GET, 'department_id', FILTER_VALIDATE_INT);
$previousRound = filter_input(INPUT_GET, 'previous_round', FILTER_VALIDATE_INT);
$newRound = filter_input(INPUT_GET, 'new_round', FILTER_VALIDATE_INT);

// Include the db classes
include_once '../inc/config.php';
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include "../classes/class.db_application.php";
$db_application = new DB_Application(); 

if ($mode == 'getPromotionHistory') {
    
    $historyQuery = "SELECT 
                        if ( round = 0, '[voting]', CONCAT('Round ', round) ) AS round, 
                        IF (promotion_method = 'default', 'reset', promotion_method), 
                        status_time, 
                        CONCAT(users.firstname, ' ', users.lastname) AS user 
                        FROM promotion_history
                        LEFT OUTER JOIN users ON promotion_history.users_id = users.id
                        WHERE application_id = " . $applicationId . "
                        AND program_id = " . $programId . "
                        ORDER BY status_time DESC";
    $historyRecords = $db_application->handleSelectQuery($historyQuery); 

    if ( count($historyRecords) > 0 ) {

        $returnHtml = '<table>';
        foreach($historyRecords as $historyRecord) {
            $returnHtml .= '<tr><td>';
            $returnHtml .= implode('</td><td>', $historyRecord);    
            $returnHtml .= '<tr></td>';
        }
        $returnHtml .= '</table>';
        
    } else {

        $returnHtml = '<p>No promotions or demotions.</p>';        
    }
    

    
    echo $returnHtml;
    exit;
}


// Update the db.
//$status = $db_application->updatePromotionStatus($applicationId, $programId, $promotionStatus);

// KLUGE: $previousRound is NULL in GET request on reset, evaulates to FALSE in filter_input().
$promotionMethod = NULL;
if ($previousRound === FALSE || $previousRound == 'NULL') {
    $promotionMethod = 'default';    
} elseif ($newRound > $previousRound) {
    $promotionMethod = 'promotion';
} elseif ($previousRound > $newRound) {
    $promotionMethod = 'demotion';    
}

if ($promotionMethod)  {

    $dbStatus = $db_application->updatePromotionHistory($applicationId, $programId, 
        $newRound, $promotionMethod, $_SESSION['A_usermasterid']);

    if ($dbStatus) {

       $programStatusQuery = "SELECT promotion_history.program_id,
                            promotion_history.round
                            FROM promotion_history
                            INNER JOIN lu_programs_departments
                                ON promotion_history.program_id = lu_programs_departments.program_id
                            INNER JOIN (
                              SELECT application_id, program_id, 
                              MAX(status_time) AS latest_status
                              FROM promotion_history
                              WHERE application_id = " . $applicationId . " 
                              GROUP BY application_id, program_id
                            ) AS program_status
                                  ON promotion_history.application_id = program_status.application_id
                                  AND promotion_history.program_id = program_status.program_id
                            WHERE promotion_history.application_id = " . $applicationId . "
                            AND lu_programs_departments.department_id = 
                                (SELECT lu_programs_departments.department_id
                                FROM lu_programs_departments
                                WHERE lu_programs_departments.program_id = " . $programId . ")
                            AND promotion_history.status_time = program_status.latest_status";

        $programStatusArray = $db_application->handleSelectQuery($programStatusQuery);
        
        $promotionStatusRound = 1;
        foreach ($programStatusArray as $programStatusRecord) {
            // Get the max round for all programs.
            if ( $programStatusRecord['round'] > $promotionStatusRound ) {
                $promotionStatusRound = $programStatusRecord['round'];   
            }
        }
        
        $dbStatus = $db_application->updatePromotionStatus($applicationId, $departmentId, $promotionStatusRound);
        
        if ($dbStatus == 0) {
            // Indicates no change, so no records updated = not an error condition.
            $dbStatus = 1;
        }
    }
    
    echo $dbStatus;    
}

?>