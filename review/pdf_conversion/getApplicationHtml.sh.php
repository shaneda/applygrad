#!/usr/local/bin/php5 
<?
  //
// Turn error reporting off.
error_reporting(0);

// Get the uid from the first command line argument.
$uid = $argv[1];

// Set database variables and include the db classes.
// CHANGE THIS AS NECESSARY BEFORE RUNNING SCRIPT.
$db = "gradAdmissions2009New";
//$db = "gradAdmissions2008Test"; 
$db_host = "localhost";
$db_username = "phdapp";
//$db_username = "phdApp";
$db_password = "phd321app";

?>

<? // @include_once '../../inc/config_development.php'; ?>
<? // include_once '../../inc/session_admin.php'; ?>
<? @include_once '../../inc/db_connect.php'; ?>
<? @include_once '../../inc/functions.php'; ?>
<? @include_once '../../inc/applicationFunctions.php'; ?>

<?

$err = "";

// ### PB commented out to allow setting via command line.
//$uid = -1;

$umasterid = -1;
$appid = -1;
$view = 1;
$round = -1;
$reviewerId = $_SESSION['A_userid'];
$allowEdit = $_SESSION['A_allow_admin_edit'];
$thisDept = 1;
$semiblind_review = 0;
$facVote = 0;
$rank = 0;
$showDecision = false;
$decision = "";
$admit_to = "NULL";
$admissionStatus = "NULL";
$admit_comments = "";


$myPrograms = array();
$myPrograms2 = array();

$countries = array();
$states = array();
$statii = array();
$genders = array(array('M', 'Male'), array('F','Female'));
$ethnicities = array();
$titles = array(array('Mr.', 'Mr.'), array('Ms.','Ms.'));
$err = "";
$mode = "edit";
$title = "";
$fName = "";
$lName = "";
$mName = "";
$initials = "";
$email = "";
$gender = "";
$ethnicity = "";
$pass = "";
$passC = "";
$dob = "";
$visaStatus = -1;
$resident = "";
$street1 = "";
$street2 = "";
$street3 = "";
$street4 = "";
$city = "";
$state = "";
$postal = "";
$country = "";
$tel = "";
$telMobile = "";
$telWork = "";
$homepage = "";
$streetP1 = "";
$streetP2 = "";
$streetP3 = "";
$streetP4 = "";
$cityP = "";
$stateP = "";
$postalP = "";
$countryP = "";
$telP = "";
$telPMobile = "";
$telPWork = "";
$countryCit = "";
$curResident = "";

$degrees = array();
$uniTypes = array();
$institutes = array();
$myInstitutes = array();
$scales = array();
$transUploaded = true;
$comp = false;

$arr = array(2, "Graduate");
array_push($uniTypes, $arr);
$arr = array(3, "Additional College/University");
array_push($uniTypes, $arr);

$sectioncomplete = false;
$grecomplete = false;
$grescores = array();
$gresubjectscores = array();
$toefliscores = array();
$toeflcscores = array();
$toeflpscores = array();
$greScoreRange1 = array();
$greScoreRange2 = array();
$grePercRange1 = array();

$employments = array();
$pubs = array();
$fellows = array();
$crossDeptProgs = array();
$crossDeptProgsOther = "";
$advisors = array();//FACULTY DESIGNATED AS ADVISORS
$advisor1 = "";
$advisor2 = "";
$advisor3 = "";
$advisor4 = "";
$expResearch = "";
$expInd = "";
$exFileId = "";
$exFileDate = "";
$exFileSize = "";
$exFileExt = "";
$pier = "No";
$wFellow = "No";
$port = "No";
$portLink = "";
$prog = "";
$design = "";
$stats = "";
$enrolled = 0;
$referral = "";
$honors = "";
$otherUni = "";
$tmpCrossDeptProgsId = "";
$permission= 0;
$sent_to_program = "";
$cur_enrolled = "";

$myRecommenders = array();
$buckley = false;

$myAreas = array();
$myAreasAll = array();



$background = "";
$grades = "";
$statement = "";
$comments = "";
$point = "";
$pointCertainty = "";
$point2 = "";
$point2Certainty = "";
$private_comments = "";
$round2 = "";
$touched = 0;
$admit_vote = "";
$recruited = "";
$grad_name = "";
$pertinent_info = "";
$advise_time = "";
$commit_money = "";
$fund_source = "";
$interview = "";
$brilliance = "";
$other_interest = "";

$csdVoteVals = array(
array(1.1, 1.1),
array(1.2, 1.2),
array(1.3, 1.3),
array(1.5, 1.5),
array(1.7, 1.7),
array(1.9, 1.9),
array(2, 2),
array(2.5, 2.5),
array(3, 3),
array(4, 4),
array(5, 5)
);

$csdFacVoteVals = array(
array(1, 1),
array(2, 2),
array(3, 3),
array(4, 4),
array(5, 5),
array(6, 6),
array(7, 7),
array(8, 8),
array(9, 9),
array(10, 10)
);

$ltiSeminars = array(
array(1, "IR"),
array(2, "MT"),
array(3, "SR"),
array(4, "CL"),
array(5, "BIO"),
array(6, "MCALL"),
array(7, "ML")
);

$ltiVoteVals = array(
array(1, 1),
array(1.5, 1.5),
array(2, 2),
array(2.5, 2.5),
array(3, 3)
);

$certaintyVals = array(
array(5, 5),
array(4, 4),
array(3, 3),
array(2, 2),
array(1, 1)
);

$decisionVals = array(
array('A1', 'A1'),
array('A2', 'A2'),
array('B1', 'B1'),
array('B2', 'B2'),
array('S', 'S'),
array('R', 'R'),
);

$committeeReviews = array();
$committeeReviewsSeminars = array();
//GET ID OF APPLICATION
if(isset($_POST['userid']))
{
	$uid = intval($_POST['userid']);
}
if(isset($_GET['id']))
{
	$uid = intval($_GET['id']);
}
//VIEW TYPE
if(isset($_POST['v']))
{
	$view = intval($_POST['v']);
}
if(isset($_GET['v']))
{
	$view = intval($_GET['v']);
}
//ROUND NUMBER
if(isset($_POST['r']))
{
	$round = intval($_POST['r']);
}
if(isset($_GET['r']))
{
	$round = intval($_GET['r']);
}
//REVIEWER ID
if(isset($_POST['rid']))
{
	$reviewerId = intval($_POST['rid']);
}
if(isset($_GET['rid']))
{
	$reviewerId = intval($_GET['rid']);
	$allowEdit = false;
}
//DEPARTMENT ID
if(isset($_POST['d']))
{
	$thisDept = intval($_POST['d']);
}
if(isset($_GET['d']))
{
	$thisDept = intval($_GET['d']);
}
if(isset($_GET['showDecision']))
{
	if($_GET['showDecision'] == 1)
	{
		$showDecision = true;
	}
}
if(isset($_POST['showDecision']))
{
	if($_POST['showDecision'] == 1)
	{
		$showDecision = true;
	}
}

if($showDecision == false)
{
	$sql = "select name, semiblind_review from department where id=".$thisDept;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$deptName = $row['name'];
		if(!isset($_POST['btnSemiblind']) && !isset($_POST['chkSemiblind']))
		{
			$semiblind_review = intval($row['semiblind_review']);
		}
	}
	if(isset($_POST['btnSemiblind']))
	{
		if(intval($_POST['chkSemiblind']) == 1)
		{
			$semiblind_review = 0;
		}
		else
		{
			$semiblind_review = 1;
		}

	}
	else
	{
		if(isset($_POST['chkSemiblind']))
		{
			$semiblind_review = intval($_POST['chkSemiblind']);
		}
	}
}


if($uid > -1)
{
	//GET USER INFO
	$sql = "SELECT
	users.id as usermasterid,
	users.email,
	users.title,
	users.firstname,
	users.middlename,
	users.lastname,
	users.initials,
	users_info.gender,
	users_info.dob,
	users_info.address_cur_street1,
	users_info.address_cur_street2,
	users_info.address_cur_street3,
	users_info.address_cur_street4,
	users_info.address_cur_city,
	cur_states.name as currentstate,
	users_info.address_cur_pcode,
	cur_countries.name as currentcountry,
	users_info.address_cur_tel,
	users_info.address_cur_tel_mobile,
	users_info.address_cur_tel_work,
	users_info.address_perm_street1,
	users_info.address_perm_street2,
	users_info.address_perm_street3,
	users_info.address_perm_street4,
	users_info.address_perm_city,
	perm_states.name as permstate,
	users_info.address_perm_pcode,
	perm_countries.name as permcountry,
	users_info.address_perm_tel,
	users_info.address_perm_tel_mobile,
	users_info.address_perm_tel_work,
	ethnicity.name as ethnicity,
	users_info.visastatus,
	users_info.homepage,
	cit_countries.name as citcountry,
	users_info.cur_pa_res,
	application.id as appid
	
	FROM lu_users_usertypes
	inner join users on users.id = lu_users_usertypes.user_id
	left outer join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join states as cur_states on cur_states.id = users_info.address_cur_state
	left outer join states as perm_states on perm_states.id = users_info.address_perm_state
	left outer join countries as cur_countries on cur_countries.id = users_info.address_cur_country
	left outer join countries as perm_countries on perm_countries.id = users_info.address_perm_country
	left outer join countries as cit_countries on cit_countries.id = users_info.cit_country
	left outer join application on application.user_id = lu_users_usertypes.id
	left outer join ethnicity on ethnicity.id = users_info.ethnicity
	where lu_users_usertypes.id=".$uid;
	//echo $sql;
	$result = mysql_query($sql)
	or die(mysql_error());

	while($row = mysql_fetch_array( $result ))
	{
		$umasterid = $row['usermasterid'];
		$email = $row['email'];
		$title = $row['title'];
		$fName = $row['firstname'];
		$mName = $row['middlename'];
		$lName = $row['lastname'];
		$initials = $row['initials'];
		$gender = $row['gender'];
		$dob = formatUSdate($row['dob'],'-','/');
		$street1 = $row['address_cur_street1'];
		$street2 = $row['address_cur_street2'];
		$street3 = $row['address_cur_street3'];
		$street4 = $row['address_cur_street4'];
		$city = $row['address_cur_city'];
		$state = $row['currentstate'];
		$postal = $row['address_cur_pcode'];
		$country = $row['currentcountry'];
		$tel = $row['address_cur_tel'];
		$telMobile = $row['address_cur_tel_mobile'];
		$telWork = $row['address_cur_tel_work'];
		$streetP1 = $row['address_perm_street1'];
		$streetP2 = $row['address_perm_street2'];
		$streetP3 = $row['address_perm_street3'];
		$streetP4 = $row['address_perm_street4'];
		$cityP = $row['address_perm_city'];
		$stateP = $row['permstate'];
		$postalP = $row['address_perm_pcode'];
		$countryP = $row['permcountry'];
		$ethnicity = $row['ethnicity'];
		$visaStatus = $row['visastatus'];
		$homepage = $row['homepage'];
		$countryCit = $row['citcountry'];
		if($row['cur_pa_res'] == 1)
		{
			$curResident = "Yes";
		}
		$appid = $row['appid'];
	}
	
	
	if(isset($_POST["btnSubmitFinal"]))
	{
		$vars = $_POST;
		foreach($vars as $key => $value)
		{
			$arr = split("_", $key);
			if(strstr($key, 'comments_') !== false)
			{
				$itemId = $arr[1];
				$comments = $_POST["comments_".$arr[1]];
				$decision = $_POST["decision_".$arr[1]];
				$faccontact = $_POST["faccontact_".$arr[1]];
				$stucontact = $_POST["stucontact_".$arr[1]];
				$admissionStatus = $_POST["admissionStatus_".$arr[1]];
				
				$sql = "update lu_application_programs set 
				decision = '".$decision."',
				admission_status = ".intval($admissionStatus).",
				admit_comments = '".addslashes($comments)."',
				faccontact = '".addslashes($faccontact)."',
				stucontact='".addslashes($stucontact)."'
				where application_id = ".$appid."
				and program_id = ".$itemId;
				;
				mysql_query($sql) or die(mysql_error().$sql);
				//echo $sql;
			}
		}
	}

	//GET USER SELECTED PROGRAMS
	$sql = "SELECT programs.id,
	degree.name as degreename,
	fieldsofstudy.name as fieldname,
	choice,
	lu_application_programs.id as itemid,
	programs.programprice,
	programs.baseprice,
	programs.linkword,
	domain.name as domainname,
	lu_application_programs.decision,
	lu_application_programs.admission_status,
	lu_application_programs.admit,
	lu_application_programs.admit_comments,
	group_concat(distinct lu_programs_departments.department_id) as department_id,
	lu_application_programs.faccontact,
	lu_application_programs.stucontact
	FROM lu_application_programs
	inner join programs on programs.id = lu_application_programs.program_id
	inner join degree on degree.id = programs.degree_id
	inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
	left outer join lu_programs_departments on lu_programs_departments.program_id = programs.id
	left outer join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
	left outer join domain on domain.id = lu_domain_department.domain_id
	where lu_application_programs.application_id = ".$appid." group by programs.id order by choice";

	$result = mysql_query($sql) or die(mysql_error() . $sql);


	while($row = mysql_fetch_array( $result ))
	{
		$dept = "";
		$baseprice = 0.0;
		$oracle = "";
		$depts = getDepartments($row[0]);
		$tmpDept = "";

		if(count($depts) > 1)
		{

			$dept = " - ";
			for($i = 0; $i < count($depts); $i++)
			{
				$dept .= $depts[$i][1];
				$oracle = $depts[$i][2];
				$baseprice = $depts[$i][3];
				if($i < count($depts)-1)
				{
					$dept .= "/";
				}

			}
		}

		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row['degreename']);
		array_push($arr, $row['linkword']);
		array_push($arr, $row['fieldname'].$dept);
		array_push($arr, $row['choice']);
		array_push($arr, $row['itemid']);
		array_push($arr, $row['programprice']);
		array_push($arr, $oracle);
		array_push($arr, $row['baseprice']);
		array_push($arr, $row['domainname']);
		array_push($arr, $row['decision']);
		array_push($arr, $row['admission_status']);
		array_push($arr, $row['admit']);
		array_push($arr, $row['admit_comments']);
		array_push($arr, $row['department_id']);
		array_push($arr, $row['faccontact']);
		array_push($arr, $row['stucontact']);
		
		array_push($myPrograms, $arr);
		
		
		$arr = array();
		array_push($arr, $row[0]);
		array_push($arr, $row['degreename'] . " " . $row['linkword']. " ". $row['fieldname'].$dept);
		array_push($myPrograms2, $arr);
	}


}//END IF UID



function getDepartments($itemId)
{
	$ret = array();
	$sql = "SELECT
	department.id,
	department.name,
	department.oraclestring
	FROM lu_programs_departments
	inner join department on department.id = lu_programs_departments.department_id
	where lu_programs_departments.program_id = ". $itemId;

	$result = mysql_query($sql)
	or die(mysql_error());

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($arr, $row["oraclestring"]);
		array_push($ret, $arr);
	}
	return $ret;
}

//GET EDUCATION
$sql = "SELECT
	usersinst.id,institutes.name,date_entered,
	date_grad,date_left,degreesall.name as degree,
	major1,major2,major3,minor1,minor2,gpa,gpa_major,
	gpascales.name as gpa_scale,transscriptreceived,datafile_id,educationtype,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension
	FROM usersinst
	left outer join institutes on institutes.id = usersinst.institute_id
	left outer join datafileinfo on datafileinfo.id = usersinst.datafile_id
	left outer join gpascales on gpascales.id = usersinst.gpa_scale
	left outer join degreesall on degreesall.id = usersinst.degree
	where usersinst.user_id=".$uid. " order by usersinst.educationtype";
	$result = mysql_query($sql) or die(mysql_error());
	$comp = true;
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['name']);
		if($row['name'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_entered'],'-','/')  );
		if($row['date_entered'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_grad'],'-','/') );
		if($row['date_grad'] == ""){$comp = false; }
		array_push($arr, formatUSdate2($row['date_left'],'-','/') );
		array_push($arr, $row['degree']);
		if($row['degree'] == ""){$comp = false; }
		array_push($arr, $row['major1']);
		array_push($arr, $row['major2']);
		array_push($arr, $row['major3']);
		array_push($arr, $row['minor1']);
		array_push($arr, $row['minor2']);
		array_push($arr, $row['gpa']);
		if($row['gpa'] == ""){$comp = false; }
		array_push($arr, $row['gpa_major']);
		if($row['gpa_major'] == ""){$comp = false; }
		array_push($arr, $row['gpa_scale']);
		if($row['gpa_scale'] == ""){$comp = false; }
		array_push($arr, $row['transscriptreceived']);
		array_push($arr, $row['datafile_id']);
		if($row['datafile_id'] == ""){$comp = false; }
		array_push($arr, $row['educationtype']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($myInstitutes, $arr);
	}
//GET SCORES

//LOOK FOR GRE GENERAL SCORE
$sql = "select grescore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from grescore
left outer join datafileinfo on datafileinfo.id = grescore.datafile_id
where application_id = " .$appid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['verbalscore']);
	array_push($arr, $row['verbalpercentile']);
	array_push($arr, $row['quantitativescore']);
	array_push($arr, $row['quantitativepercentile']);
	array_push($arr, $row['analyticalwritingscore']);
	array_push($arr, $row['analyticalwritingpercentile']);
	array_push($arr, $row['analyticalscore']);
	array_push($arr, $row['analyticalpercentile']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['datafile_id']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($grescores,$arr);

}
//LOOK FOR GRE SUBJECT SCORE
$sql = "select gresubjectscore.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from gresubjectscore
left outer join datafileinfo on datafileinfo.id = gresubjectscore.datafile_id
where application_id = " .$appid;
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['name']);
	array_push($arr, $row['score']);
	array_push($arr, $row['percentile']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['datafile_id']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($gresubjectscores,$arr);
}
//LOOK FOR TOEFL SCORE

//LOOK FOR TOEFL IBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'IBT'";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['section1']);
	array_push($arr, $row['section2']);
	array_push($arr, $row['section3']);
	array_push($arr, $row['essay']);
	//echo $row['essay'];
	array_push($arr, $row['total']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($arr, $row['datafile_id']);
	array_push($toefliscores,$arr);
}
//LOOK FOR TOEFL CBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'CBT'";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['section1']);
	array_push($arr, $row['section2']);
	array_push($arr, $row['section3']);
	array_push($arr, $row['essay']);
	array_push($arr, $row['total']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($arr, $row['datafile_id']);
	array_push($toeflcscores,$arr);
}
//LOOK FOR TOEFL PBT SCORE
$sql = "select toefl.*,
datafileinfo.moddate,
datafileinfo.size,
datafileinfo.extension
from toefl
left outer join datafileinfo on datafileinfo.id = toefl.datafile_id
where application_id = " .$appid . " and toefl.type = 'PBT'";
$result = mysql_query($sql)	or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, formatUSdate2($row['testdate'],'-','/'));
	array_push($arr, $row['section1']);
	array_push($arr, $row['section2']);
	array_push($arr, $row['section3']);
	array_push($arr, $row['essay']);
	array_push($arr, $row['total']);
	array_push($arr, $row['scorereceived']);
	array_push($arr, $row['moddate']);
	array_push($arr, $row['size']);
	array_push($arr, $row['extension']);
	array_push($arr, $row['datafile_id']);
	array_push($toeflpscores,$arr);
}

//SUPPLEMENTAL INFO

$sql = "select pier, womenfellowship, portfoliosubmitted,portfolio_link, area1,area2,area3,
referral_to_program, other_inst, cross_dept_progs,cross_dept_progs_other, records_permission,
if(sent_to_program=1,'yes','no') as sent_to_program,
if(cur_enrolled=1,'yes','no') as cur_enrolled,
honors
from application where id=".$appid;
$result = mysql_query($sql) or die(mysql_error());
$tmpProgs = "";
while($row = mysql_fetch_array( $result ))
{
	if($row['womenfellowship'] == 1)
	{
		$wFellow = "Yes";
	}
	if($row['pier'] == 1)
	{
		$pier = "Yes";
	}
	if($row['portfoliosubmitted'] == 1)
	{
		$port = "Yes";
	}
	$prog = $row['area1'];
	$design = $row['area2'];
	$stats = $row['area3'];
	$portLink = stripslashes($row['portfolio_link']);
	$referral = stripslashes($row['referral_to_program']);
	$otherUni = stripslashes($row['other_inst']);
	$crossDeptProgs = $row['cross_dept_progs'];
	$crossDeptProgsOther = $row['cross_dept_progs_other'];
	$permission = $row['records_permission'];
	$sent_to_program = $row['sent_to_program'];
	$cur_enrolled = $row['cur_enrolled'];
	$honors = $row['honors'];
}

$sql = "select
id,
title,
author,
forum,
citation,
url,
status
from publication where application_id=".$appid . " order by id";
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['title']);
	array_push($arr, $row['author']);
	array_push($arr, $row['forum']);
	array_push($arr, $row['citation']);
	array_push($arr, $row['url']);
	array_push($arr, $row['status']);
	array_push($pubs, $arr);
}
$sql = "select id,
name,
amount,
status,
applied_date,
award_date,
duration
from fellowships where application_id=".$appid;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr, $row['id']);
	array_push($arr, $row['name']);
	array_push($arr, $row['amount']);
	array_push($arr, $row['status']);
	array_push($arr, formatUSdate2($row['applied_date'],'-','/'));
	array_push($arr, formatUSdate2($row['award_date'],'-','/'));
	array_push($arr, $row['duration']);
	array_push($fellows, $arr);
}


function getRecommenders($appid)
{
	global $buckley;
	$ret = array();
	$letter = "";
	//RETRIEVE USER INFORMATION
	$sql = "SELECT
	recommend.id,
	recommend.rec_user_id,
	recommend.datafile_id,
	recommend.submitted,
	recommend.recommendtype,
	users.title,
	users.firstname,
	users.lastname,
	users.email,
	users_info.address_perm_tel,
	lu_users_usertypes.id as uid,
	users_info.company,
	reminder_sent_count,
	datafileinfo.moddate,
	datafileinfo.size,
	datafileinfo.extension,
	recommendationtypes.name
	FROM recommend
	left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
	left outer join users on users.id = lu_users_usertypes.user_id
	left join users_info on users_info.user_id = lu_users_usertypes.id
	left outer join datafileinfo on datafileinfo.id = recommend.datafile_id
	left outer join recommendationtypes on recommendationtypes.id = recommend.recommendtype
	where application_id=".$appid. " order by recommend.id";
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['id']);
		array_push($arr, $row['rec_user_id']);
		array_push($arr, $row['firstname']);
		array_push($arr, $row['lastname']);
		array_push($arr, $row['title']);
		array_push($arr, $row['company']);
		array_push($arr, $row['email']);
		array_push($arr, $row['address_perm_tel']);
		array_push($arr, $row['recommendtype']);
		array_push($arr, $row['datafile_id']);
		array_push($arr, $row['moddate']);
		array_push($arr, $row['size']);
		array_push($arr, $row['extension']);
		array_push($arr,$row['reminder_sent_count']);
		array_push($arr,$row['name']);
		array_push($arr, 0);//ARRAY INFO
		array_push($ret, $arr);
	}

	$sql = "select buckleywaive from application where id=".$appid;
	$result = mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array( $result ))
	{
		$buckley = $row['buckleywaive'];
	}
	return $ret;
}
$myRecommenders = getRecommenders($appid);

function getMyAreas($id, $appid)
{
	global $myAreasAll;
	$ret = array();
	$sql = "SELECT
	interest.id,
	interest.name
	FROM
	lu_application_programs
	inner join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
	inner join interest on interest.id = lu_application_interest.interest_id
	where lu_application_programs.program_id=".$id." and lu_application_programs.application_id=".$appid . " order by lu_application_interest.choice" ;

	$result = mysql_query($sql)	or die(mysql_error());

	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row["id"]);
		array_push($arr, $row["name"]);
		array_push($ret, $arr);
		$doAdd = true;
		for($j = 0; $j < count($myAreasAll); $j++)
		{
			if($myAreasAll[$j][0] == $row["id"])
			{
				$doAdd = false;
			}
		}
		if($doAdd == true)
		{
			array_push($myAreasAll, $arr);
		}
	}
	return $ret;
}

//ADVISORS

$sql = "select
concat(users.firstname, ' ',users.lastname) as name2,
name
from
lu_application_advisor
left outer join lu_users_usertypes on lu_users_usertypes.id = lu_application_advisor.advisor_user_id
left outer join users on users.id = lu_users_usertypes.user_id
where application_id=".$appid . " order by lu_application_advisor.id" ;
$result = mysql_query($sql) or die(mysql_error());
$myAdvisors = array();
$k=0;
while($row = mysql_fetch_array( $result ))
{
    // PLB replaced logic below to fill/use myAdvisors array
    // Get up to 12 possible advisors
    if ($k < 12) {
            
        if($row['name2'] == "") {

            $myAdvisors[$k] = $row['name'];

        } else {

            $myAdvisors[$k] = $row['name2'];
        }        

    }

/*
    if($k == 0)
    {
        if($row['name2'] == "")
        {
            $advisor1 = $row['name'];
        }else
        {
            $advisor1 = $row['name2'];
        }
    }
    if($k == 1)
    {
        if($row['name2'] == "")
        {
            $advisor2 = $row['name'];
        }else
        {
            $advisor2 = $row['name2'];
        }
    }
    if($k == 2)
    {
        if($row['name2'] == "")
        {
            $advisor3 = $row['name'];
        }else
        {
            $advisor3 = $row['name2'];
        }
    }
    if($k == 3)
    {
        if($row['name2'] == "")
        {
            $advisor4 = $row['name'];
        }else
        {
            $advisor4 = $row['name2'];
        }
    }
*/ 
	$k++;
}

//POS REVIEW UPDATES HERE
if(isset($_POST['btnSubmit']))
{

	$doUpdate = false;

	//GET POSTED VARS
	/* AVAILABLE FIELDS
	background
	grades
	statement
	comments
	point
	pointCertainty
	point2
	point2Certainty
	private_comments
	round2
	touched
	admit_vote
	recruited
	grad_name
	pertinent_info
	advise_time
	commit_money
	fund_source
	*/
	$background = "";
	$grades = "";
	$statement = "";
	$comments = "";
	$point = "NULL";
	$pointCertainty = "NULL";
	$point2 = "NULL";
	$point2Certainty = "NULL";
	$private_comments = "";
	$round2 = "";
	$touched = 0;
	$admit_vote = "";
	$recruited = "";
	$grad_name = "";
	$pertinent_info = "";
	$advise_time = "";
	$commit_money = "";
	$fund_source = "";
	$interview = "";
	$brilliance = "";
	$other_interest = "";
	$facVote = 0;
	$rank = "NULL";

	if(isset($_POST['background']))
	{
		$background = addslashes(htmlspecialchars($_POST['background']));
	}
	if(isset($_POST['grades']))
	{
		$grades = addslashes(htmlspecialchars($_POST['grades']));
	}
	if(isset($_POST['statement']))
	{
		$statement = addslashes(htmlspecialchars($_POST['statement']));
	}
	if(isset($_POST['comments']))
	{
		$comments = addslashes(htmlspecialchars($_POST['comments']));
	}
	if(isset($_POST['point']))
	{
		if($_POST['point'] != "")
		{
			$point = addslashes(htmlspecialchars($_POST['point']));
			if($thisDept == 3)
			{
				if($point != "")
				{
					$point = floatval($point);
					if($point < 5)
					{
						$point=5.00;
					}
					if($point > 10)
					{
						$point=10;
					}
				}
			}
		}
		//$point = floatval($point);
	}
	if(isset($_POST['pointCertainty']))
	{
		$pointCertainty = addslashes(htmlspecialchars($_POST['pointCertainty']));
		$pointCertainty = floatval($pointCertainty);
	}
	if(isset($_POST['point2']))
	{
		if($_POST['point2'] != "")
		{
			$point2 = addslashes(htmlspecialchars($_POST['point2']));
			$point2 = floatval($point2);
		}
	}
	if(isset($_POST['point2Certainty']))
	{
		$point2Certainty = addslashes(htmlspecialchars($_POST['point2Certainty']));
		$point2Certainty = floatval($point2Certainty);
	}
	if(isset($_POST['private_comments']))
	{
		$private_comments = addslashes(htmlspecialchars($_POST['private_comments']));
	}
	if(isset($_POST['round2']))
	{
		$round2 = addslashes(htmlspecialchars($_POST['round2']));
	}
	if(isset($_POST['touched']))
	{
		$touched = intval($_POST['touched']);
	}
	if(isset($_POST['admit_vote']))
	{
		$admit_vote = addslashes(htmlspecialchars($_POST['admit_vote']));
	}
	if(isset($_POST['recruited']))
	{
		$recruited = addslashes(htmlspecialchars($_POST['recruited']));
	}
	if(isset($_POST['grad_name']))
	{
		$grad_name = addslashes(htmlspecialchars($_POST['grad_name']));
	}
	if(isset($_POST['pertinent_info']))
	{
		$pertinent_info = addslashes(htmlspecialchars($_POST['pertinent_info']));
	}
	if(isset($_POST['advise_time']))
	{
		$advise_time = addslashes(htmlspecialchars($_POST['advise_time']));
	}
	if(isset($_POST['commit_money']))
	{
		$commit_money = addslashes(htmlspecialchars($_POST['commit_money']));
	}
	if(isset($_POST['fund_source']))
	{
		$fund_source = addslashes(htmlspecialchars($_POST['fund_source']));
	}
	if(isset($_POST['interview']))
	{
		$interview = addslashes(htmlspecialchars($_POST['interview']));
	}
	if(isset($_POST['brilliance']))
	{
		$brilliance = addslashes(htmlspecialchars($_POST['brilliance']));
	}
	if(isset($_POST['other_interest']))
	{
		$other_interest = addslashes(htmlspecialchars($_POST['other_interest']));
	}
	if(isset($_POST['facVote']))
	{
		$facVote = intval($_POST['facVote']);
	}
	if(isset($_POST['rank']))
	{
		$rank = intval($_POST['rank']);
	}
	$vars = $_POST;
	foreach($vars as $key => $value)
	{
		$arr = split("_", $key);
		if( strstr($key, 'seminars') !== false )
		{
			//$arr1 = array();
			array_push($committeeReviewsSeminars, $arr[1]);
			//array_push($committeeReviewsSeminars, $arr1);
		}
	}//END FOR each key


	//first see if a record exists for me
	$reviewId = -1;
	$sql = "select id from review where reviewer_id =".$reviewerId." and application_id=".$appid . " and round=".$round;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$reviewId = $row['id'];
		$doUpdate = true;
	}
	//if no record, insert one
	if($doUpdate == false)
	{
		$sql = "insert into review(
		application_id,
		reviewer_id,
		background,
		grades,
		statement,
		comments,
		point,
		point_certainty,
		point2,
		point2_certainty,
		private_comments,
		round2,
		touched,
		admit_vote,
		recruited,
		grad_name,
		pertinent_info,
		advise_time,
		commit_money,
		fund_source,
		round,
		interview,
		brilliance,
		other_interest,
		fac_vote,
		rank,
		department_id
	)values(
		".$appid.",
		".$reviewerId.",
		'".$background."',
		'".$grades."',
		'".$statement."',
		'".$comments."',
		".$point.",
		".$pointCertainty.",
		".$point2.",
		".$point2Certainty.",
		'".$private_comments."',";
		if($round2 == ""){
			$sql .= "NULL,";
		}else
		{
			$sql .= intval($round2).",";
		}
		$sql .= $touched.",
		'".$admit_vote."',
		".intval($recruited).",
		'".$grad_name."',
		'".$pertinent_info."',
		'".$advise_time."',
		'".$commit_money."',
		'".$fund_source."',
		".$round.",
		'".$interview."',
		'".$brilliance."',
		'".$other_interest."',
		".$facVote.",
		".$rank.",
		".$thisDept."
		)";
		if(intval($reviewerId) > 0)
		{
			mysql_query($sql) or die(mysql_error().$sql);
		}
		$reviewId = mysql_insert_id();
		//echo $sql;
	}else
	{
		//if a record ecists, update
		$sql = "update review set
		background='".$background."',
		grades='".$grades."',
		statement='".$statement."',
		comments='".$comments."',
		point=".$point.",
		point_certainty=".$pointCertainty.",
		point2=".$point2.",
		point2_certainty=".$point2Certainty.",
		private_comments='".$private_comments."',
		round2=".intval($round2).",
		touched=".$touched.",
		admit_vote='".$admit_vote."',
		recruited=".intval($recruited).",
		grad_name='".$grad_name."',
		pertinent_info='".$pertinent_info."',
		advise_time='".$advise_time."',
		commit_money='".$commit_money."',
		fund_source='".$fund_source."',
		interview='".$interview."',
		brilliance='".$brilliance."',
		other_interest='".$other_interest."',
		fac_vote=".$facVote.",
		rank=".$rank.",
		department_id = ".$thisDept."
		where supplemental_review IS NULL and reviewer_id =".$reviewerId." and application_id=".$appid . " and round=".$round;
		if(intval($reviewerId) > 0)
		{
			mysql_query($sql) or die(mysql_error().$sql);
		}
		//echo $sql;
	}
	//HANDLE ADDITIONAL REVIEW ITEMS FOR AOI
	$sql = "delete from review where supplemental_review IS NOT NULL and reviewer_id =".$reviewerId." and application_id=".$appid . " and round=".$round;
	mysql_query($sql) or die(mysql_error().$sql);
	foreach($vars as $key => $value)
	{
		$arr = split("_", $key);
		if(strstr($key, 'point_') !== false)
		{
			$tmpid = $arr[1];
			$itemId = $arr[0];
			$pv = $_POST["point_".$arr[1]];
			$pvc = "NULL";
			if(isset($_POST["pointCertainty_".$arr[1]]))
			{
				$pvc = $_POST["pointCertainty_".$arr[1]];
			}
			if(isset($_POST["facVote"]))
			{
				$facVote = $_POST["facVote"];
			}
			if($arr[0] == "point")
			{
				$sql = "insert into review(
				application_id,
				reviewer_id,
				point,
				point_certainty,
				round,
				supplemental_review,
				fac_vote,
				department_id
			)values(
				".$appid.",
				".$reviewerId.",
				".floatval($pv).",
				".floatval($pvc).",
				".$round.",
				".intval($arr[1]).",
				".$facVote.",
				".$thisDept."
				)";
				if(intval($reviewerId) > 0)
				{
					mysql_query($sql) or die(mysql_error().$sql);
				}
			}
		}
	}//END FOR


	//HANDLE LTI ADDITIONAL INTERESTS
	if($thisDept == 6 )
	{
		$lu_review_interest_id = -1;
		$sql = "select id from lu_review_interest where review_id=". $reviewId;

		$result = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_array( $result ))
		{
			$lu_review_interest_id = $row['id'];
		}
		if($lu_review_interest_id > -1)
		{
			$sql = "delete from lu_review_interest where review_id=". $reviewId;
			mysql_query($sql) or die(mysql_error());
		}
		for($i = 0; $i < count($committeeReviewsSeminars);$i++)
		{
			$sql = "insert into lu_review_interest(review_id, program_id) values(".$reviewId.",".$committeeReviewsSeminars[$i][0].")";
			if(intval($reviewerId) > 0)
			{
				mysql_query($sql) or die(mysql_error().$sql);
			}
		}

	}

}
//END UPDATE REVIEW




//GET REVIEWS
$sql = "select
review.id,
review.application_id,
users.firstname,
users.lastname,
review.reviewer_id,
lu_user_department.department_id,
GROUP_CONCAT(revgroup.name) as groups,
review.background,
review.grades,
review.statement,
review.comments,
review.point,
review.point_certainty,
review.point2,
review.point2_certainty,
review.private_comments,
review.round2,
review.touched,
review.admit_vote,
review.recruited,
review.grad_name,
review.pertinent_info,
review.advise_time,
review.commit_money,
review.fund_source,
lu_users_usertypes.usertype_id,
review.round,
review.interview,
review.brilliance,
review.other_interest,
review.supplemental_review,
review.fac_vote,
review.rank


from review
left outer join lu_users_usertypes on lu_users_usertypes.id = review.reviewer_id
left outer join users on users.id = lu_users_usertypes.user_id

left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = review.reviewer_id
left outer join revgroup on revgroup.id = lu_reviewer_groups.group_id

left outer join lu_user_department on lu_user_department.user_id = review.reviewer_id

where 
review.application_id=".$appid ;
if($_SESSION['A_usertypeid'] != 0)
{ 
	$sql .= " and (lu_user_department.department_id = ".$thisDept. "  ) ";
}
if($round == 1)
{
	$sql . " and review.round=".$round;
}
$sql .=" group by review.id";
//echo $sql;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
	$arr = array();
	array_push($arr,$row['id']);
	array_push($arr,$row['application_id']);
	array_push($arr,$row['firstname']);
	array_push($arr,$row['lastname']);
	array_push($arr,$row['reviewer_id']);
	array_push($arr,$row['groups']);
	array_push($arr,$row['background']);
	array_push($arr,$row['grades']);
	array_push($arr,$row['statement']);
	array_push($arr,$row['comments']);
	array_push($arr,$row['point']);//10
	array_push($arr,$row['point2']);
	array_push($arr,$row['private_comments']);
	array_push($arr,$row['round2']);
	array_push($arr,$row['touched']);
	array_push($arr,$row['admit_vote']);
	array_push($arr,$row['recruited']);
	array_push($arr,$row['grad_name']);
	array_push($arr,$row['pertinent_info']);
	array_push($arr,$row['advise_time']);
	array_push($arr,$row['commit_money']);//20
	array_push($arr,$row['fund_source']);
	array_push($arr,$row['usertype_id']);
	array_push($arr,$row['round']);
	array_push($arr,$row['interview']);
	array_push($arr,$row['brilliance']);
	array_push($arr,$row['other_interest']);
	array_push($arr,$row['point_certainty']);
	array_push($arr,$row['point2_certainty']);
	array_push($arr,$row['supplemental_review']);
	array_push($arr,$row['fac_vote']);//30
	array_push($arr,$row['rank']);
	array_push($arr,$row['department_id']);
	array_push($committeeReviews, $arr);
}

function getReviewSeminars($reviewId)
{
	$ret = array();
	$sql = "select id, program_id from lu_review_interest where review_id=".$reviewId;
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$arr = array();
		array_push($arr, $row['program_id']);
		array_push($ret, $arr);
	}
	return $ret;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Application of <?=$email?></title>
<link href="../css/app2.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" topmargin="5" leftmargin="10">

<table border=0  cellpadding=0 cellspacing=0 width=100% bgcolor=#C5C5C5>
<!-- NAME TABLE -->

  <tr>
    <td rowspan=3 valign=left><font size="+1">&nbsp;<?=$lName .", ".$fName ." ". $mName ." ". $title ?>
      <input type="hidden" name="userid" value="<?=$uid?>">
	   <input type="hidden" name="appid" value="<?=$appid?>">
    </font></td>
    <td valign=middle><strong><?
      $filePath=getFilePath(2, 1, $uid, $umasterid, $appid);
      if ($filePath !== null) {?>
        <a target="_blank" href=<?=$filePath?> >Resume</a></font>
      <? } ?>
    </strong></td>

    <td valign=middle><strong><?
      $filePath=getFilePath(4, 1, $uid, $umasterid, $appid);
      if ($filePath !== null) {?>
        <a target="_blank" href=<?=$filePath?> >Statement of Purpose</a></font>
      <? } ?>
    </strong></td>
  </tr>
  <tr>
    <td valign=middle><strong><?
    $ex = getExperience($appid, 1);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href ="<?=getFilePath(5, 1, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Research Experience</a><?
    }
   ?></strong>&nbsp;</td>

    <td valign=middle><strong><?
    $ex = getExperience($appid, 2);
    for($i = 0; $i < count($ex); $i++)
    {
     ?><a href ="<?=getFilePath(5, 2, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Industry Experience</a><?
    }
    ?></strong>&nbsp;</td>
  </tr>
  <tr>
    <td valign=middle><strong><?
    $ex = getExperience($appid, 4);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href ="<?=getFilePath(5, 4, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Professional Experience</a><?
    }
   ?></strong>&nbsp;</td>
    <td valign=middle><strong><?
    $ex = getExperience($appid, 3);
    for($i = 0; $i < count($ex); $i++)
    {
    ?><a href ="<?=getFilePath(5, 3, $uid, $umasterid, $appid, $ex[$i][0]);?>" target="_blank">Academic Experience</a><?
    }
   ?></strong>&nbsp;</td>
  </tr>


<!-- END NAME TABLE -->
</table>

<table border=1 cellpadding=0 cellspacing=0 width=100%>
<!-- PROGRAMS TABLE -->

<?
for($i = 0; $i < count($myPrograms); $i++)
{
	?>

	<tr>
	<td width=250><strong><font size=-1><?=$myPrograms[$i][1] . " ".$myPrograms[$i][2] . " ".$myPrograms[$i][3] ?></font></strong></td>
	<td width=489>&nbsp;
	<?
	$myAreas = getMyAreas($myPrograms[$i][0],$appid);
	for($j = 0; $j< count($myAreas); $j++)
	{
		echo $myAreas[$j][1];
		if($j < count($myAreas)-1 )
		{
			echo ", ";
		}
	}
	?>
	</td>
	</tr>
	<?
}
?>
<!-- END PROGRAMS TABLE -->
</table>


<table border="1"  cellpadding="1" cellspacing="0" width="100%">
<!-- ADDRESS TABLE -->


<tr>
<td width=369 bgcolor=c5c5c5 align=center><strong>Current Address</td>
<td width=370 bgcolor=c5c5c5 align=center><strong>Permanent Address</td>
</tr>

<tr>
<td width=369 align=left valign=top><Psmall>
<?
echo $street1;
if($street2 != "")
{
	echo "<br>".$street2;
}
if($street3 != "")
{
	echo "<br>".$street3;
}
if($street4 != "")
{
	echo "<br>".$street4;
}
echo "<br>".$city;
if($state != "")
{
	echo ", ".$state;
}
echo " ".$postal;
if($country != "")
{
	echo "<br>".$country;
}
if($tel != "")
{
	echo "<br>Tel: ".$tel;
}
if($telMobile != "")
{
	echo "<br>Mobile: ".$telMobile;
}
if($homepage != "")
{
	echo "<br><a href='".$homepage."' target='_blank'>".$homepage . "</a>";
}
if($curResident != NULL && $curResident != "")
{
	echo "<br>PA Resident: " . $curResident;
}
?></td>

<td width=370 align=left valign=top>
<?
echo "&nbsp;".$streetP1;
if($streetP2 != "")
{
	echo "<br>&nbsp;".$streetP2;
}
if($streetP3 != "")
{
	echo "<br>&nbsp;".$streetP3;
}
if($streetP4 != "")
{
	echo "<br>&nbsp;".$streetP4;
}
echo "<br>&nbsp;".$cityP;
if($stateP != "")
{
	echo ", ".$stateP;
}
echo " ".$postalP;
if($countryP != "")
{
	echo "<br>&nbsp;".$countryP;
}

?></td>
</tr>


<!-- END ADDRESS TABLE -->
</table>


<table border="1"  cellpadding="0" cellspacing="0" width="100%">
  <!-- BIO TABLE -->
  <tr bgcolor=c5c5c5>
    <td align=center colspan="2"><strong>Biographical Info</strong></td>
    <td align=center colspan="2"><strong>Immigration and Ethnic Info</strong></td>
  </tr>
  <tr>
        <td align=right><strong>Gender &nbsp; </strong></td>
        <td>&nbsp;
              <?=$gender?>
        </td>
        <td align=right><strong>Citizenship</strong></td>
        <td>&nbsp;
              <?=$countryCit?>
        </td>
  </tr>
  <tr>
        <td width=100 align=right><strong>Date of Birth &nbsp;</strong></td>
        <td width=179>&nbsp;
              <?=formatUSDate($dob)?>
        </td>
        <td width=100 align=right><strong>Ethnicity &nbsp; </strong></td>
        <td ><Psmall>
          &nbsp;
          <?=$ethnicity?>
        </td>
  </tr>
  <tr>
        <td width=100 align=right><strong>Email &nbsp;</strong></td>
        <td width=179>&nbsp;
              <?=$email?>
        </td>
        <td align=right>&nbsp;</td>
        <td><Psmall>&nbsp;</td>
  </tr>
  <!-- END BIO TABLE -->
</table>


<? if($_SESSION['A_usertypeid'] != 11){//HID FROM STUDENT CONTACT ?>


<table border=1  cellpadding=0 cellspacing=0 width=100%>
<!-- UNIV TABLE -->

<? for($i = 0; $i < count($myInstitutes); $i++){
if($myInstitutes[$i][1] != ""){
	$title = "";
	switch($myInstitutes[$i][16])
	{
		case "1":
			$title = "Undergraduate";
		break;
		case "2":
			$title = "Graduate";
		break;
		case "3":
			$title = "Additional";
		break;

	}//end switch
	?>

	<tr>
	<td bgcolor=c5c5c5 width=220><strong><?=$title?> College/University</td>
	<td width=520><strong>&nbsp;<?=$myInstitutes[$i][1]?> <?
              $filePath=getFilePath(1, $myInstitutes[$i][0], $uid, $umasterid, $appid, $myInstitutes[$i][15]);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Transcript)</a></font>
              <? } ?>
        </td>
	</tr>

	 <tr>
	  <td colspan=3>
	  <table border=1 cellpadding=0 cellspacing=0 width=100%>
	   <tr>
	    <td width=50 align=right><strong>Degree</strong>&nbsp;</td>
	    <td width=200>&nbsp;<?=$myInstitutes[$i][5]?></td>
	    <td align=right><strong><? if($myInstitutes[$i][16] == 3){ ?>
		Date Graduated/Left
		<? } else { ?> Date Graduated <? } ?></strong>&nbsp;</td>
	    <td>&nbsp;<?=$myInstitutes[$i][3]?></td>
	    <td width="80" align=right><strong>GPA Overall&nbsp;</strong>
	    <td width="100">&nbsp;<?=$myInstitutes[$i][11]?> / <?=$myInstitutes[$i][13]?>		</td>
	   </tr>


	   <tr>
	    <td width=50 align=right><strong>Major</strong>&nbsp;</td>
	    <td colspan=3>&nbsp;<?=$myInstitutes[$i][6]?></td>
	    <td width="80" align=right>&nbsp;<strong>GPA Major</strong>&nbsp;</td>
	    <td width="100">&nbsp;<?=$myInstitutes[$i][12]?>	/ <?=$myInstitutes[$i][13]?>                </td>
           </tr>

	  </table>
          </td>
	 </tr>
	<?
} }//end for myInstitutes
?>

<!-- END UNIV TABLE -->
</table>


<table border="1"  cellpadding="0" cellspacing="0" width="100%">
<!-- SCORES TABLE -->

    <tr>
      <td width="33%" valign="top"><? for($i = 0; $i < count($grescores); $i++){ ?>
        <table border=1 cellpadding=0 cellspacing=0>
          <tr>

            <td colspan="5" align=center bgcolor="c5c5c5"><strong>GRE General Test</strong><?
              $filePath=getFilePath(6, $grescores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Scores)</a></font>
              <? } ?>
              </td>

          </tr>
          <tr>
            <td width=146 align=right><strong>Test Date&nbsp;</strong></td>
            <td colspan=2>&nbsp;<?=$grescores[$i][1] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Verbal&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][2] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][3] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Quantitative&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][4] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][5] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Analytical Writing&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][6] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][7] ?></td>
          </tr>
          <tr>
            <td width=146 align=right><strong>Analytical&nbsp;</strong></td>
            <td width=50>&nbsp;<?=$grescores[$i][8] ?></td>
            <td width=50>&nbsp;<?=$grescores[$i][9] ?></td>
          </tr>
        </table>
        <? } ?></td>

      <td width="33%" valign="top"><? for($i = 0; $i < count($gresubjectscores); $i++){
	 ?>
        <table border=1 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="5" align=center bgcolor="c5c5c5"><strong>GRE Subject Test</strong><?
              $filePath=getFilePath(8, $grescores[$i][0], $uid, $umasterid, $appid);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Scores)</a></font>
              <? } ?>
              </td>
          </tr>
          <tr>
            <td width=163 align=right><strong>Test Date</strong></td>
            <td>&nbsp;<?=$gresubjectscores[$i][1] ?></td>
          </tr>
          <tr>
            <td width=163 align=right><strong>
              &nbsp;<?=$gresubjectscores[$i][2] ?>
            </strong></td>
            <td width=80>&nbsp;<?=$gresubjectscores[$i][3] ?></td>
          </tr>
        </table>
        <?  } ?></td>

      <td valign="top"><? for($i = 0; $i < count($toefliscores); $i++){
 if($toefliscores[$i][1] != "00/0000" && $toefliscores[$i][1] != "")
	{?>

        <table border=1 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="4" align=center bgcolor="c5c5c5"><strong>TOEFL IBT</strong><?
              $filePath=getFilePath(7, $toefliscores[$i][0], $uid, $umasterid, $appid, $toefliscores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Scores)</a></font>
              <? } ?>
              </td>

          </tr>
          <tr>
            <td width=65 align=right><strong>Test Date&nbsp;</strong></td>
            <td width=55>&nbsp;<?=$toefliscores[$i][1]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Reading&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][4]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Listening&nbsp;</strong></td>
            <td valign=middle>&nbsp;<?=$toefliscores[$i][3]?></td>
            <td width=89 align=right><strong>Total Score</strong> &nbsp;</td>
            <td width=49 align=left>&nbsp;<?=$toefliscores[$i][6]?></td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Speaking&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][2]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Writing&nbsp;</strong></td>
            <td>&nbsp;<?=$toefliscores[$i][5]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
        </table>
        <? } } ?>
        <? for($i = 0; $i < count($toeflcscores); $i++){
	if($toeflcscores[$i][1] != "00/0000" && $toeflcscores[$i][1] != "")
	{ ?>

        <table border=1 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="4" align=center bgcolor="c5c5c5"><strong>TOEFL CBT</strong><?
              $filePath=getFilePath(9, $toeflcscores[$i][0], $uid, $umasterid, $appid, $toeflcscores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Scores)</a></font>
              <? } ?>
              </td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Test Date&nbsp;</strong></td>
            <td width=55>&nbsp;<?=$toeflcscores[$i][1]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Listening&nbsp;</strong></td>
            <td>&nbsp;<?=$toeflcscores[$i][3]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Structure/Writing&nbsp;</strong></td>
            <td valign=middle><?=$toeflcscores[$i][2]?></td>
            <td width=89 align=right><strong>Total Score</strong> &nbsp;</td>
            <td width=49 align=left>&nbsp;<?=$toeflcscores[$i][6]?></td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Reading&nbsp;</strong></td>
            <td>&nbsp;<?=$toeflcscores[$i][4]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Essay Rating &nbsp;</strong></td>
            <td>&nbsp;<?=$toeflcscores[$i][5]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
        </table>
        <? } } ?>
        <? for($i = 0; $i < count($toeflpscores); $i++){
	if($toeflpscores[$i][1] != "00/0000" && $toeflpscores[$i][1] != "")
	{  ?>

        <table border=1 cellpadding=0 cellspacing=0>
          <tr>
            <td colspan="4" align=center bgcolor="c5c5c5"><strong>TOEFL PBT</strong><?
              $filePath=getFilePath(10, $toeflpscores[$i][0], $uid, $umasterid, $appid, $toeflpscores[$i][11]);
              if ($filePath !== null) {?>
              <a target="_blank" href=<?=$filePath?> >(Scores)</a></font>
              <? } ?>
              </td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Test Date&nbsp;</strong></td>
            <td width=55>&nbsp;<?=$toeflpscores[$i][1]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Section 1 &nbsp;</strong></td>
            <td>&nbsp;<?=$toeflpscores[$i][2]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Section 2 &nbsp;</strong></td>
            <td valign=middle><?=$toeflpscores[$i][3]?></td>
            <td width=89 align=right><strong>Total Score</strong> &nbsp;</td>
            <td width=49 align=left>&nbsp;<?=$toeflpscores[$i][6]?></td>
          </tr>
          <tr>
            <td width=65 align=right><strong>Section 3 &nbsp;</strong></td>
            <td>&nbsp;<?=$toeflpscores[$i][4]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
          <tr>
            <td align=right><strong>TWE Score&nbsp;</strong></td>
            <td>&nbsp;<?=$toeflpscores[$i][5]?></td>
            <td width=89 colspan=2>&nbsp;</td>
          </tr>
        </table>
        <? } } ?>

<!-- END SCORES TABLE -->
</table>


<? if(count($myRecommenders) > 0){ ?>
<table border="1"  cellpadding="0" cellspacing="0" width="100%"> 
<!-- RECOMMEND TABLE -->

      <tr>
        <td bgcolor="c5c5c5"><strong>Recommenders</strong></td>
        <td bgcolor="c5c5c5"><strong>Type</strong></td>
        <td bgcolor="c5c5c5"><strong>Title / Affiliation </strong></td>
        <td bgcolor="c5c5c5"><strong>Email / Phone </strong></td>
        <td bgcolor="c5c5c5"><strong>Waive<br>Buckley</strong></td>
      </tr>
      <? for($i = 0; $i < count($myRecommenders); $i++){
	  if($myRecommenders[$i][2] != ""){
	  ?>
	  <tr>
        <td>
		<? if($myRecommenders[$i][9] != ""){
			$recFilePath = getFilePath(3, $myRecommenders[$i][0], $uid, $umasterid, $appid, $myRecommenders[$i][9]);
			?>
			<input name="txtRecId" type="hidden" value="<?=$myRecommenders[$i][9]?>">
			<? if($recFilePath != ""){ ?>
				<a href="<?=$recFilePath?>" target="_blank">
					<?=$myRecommenders[$i][2]?> <?=$myRecommenders[$i][3]?>
				</a><?
			}else
			{
				echo $myRecommenders[$i][2]. " ". $myRecommenders[$i][3];
			}
		}else
		{
			echo $myRecommenders[$i][2]." " .$myRecommenders[$i][3];
		} ?>&nbsp;		</td>
        <td><?=$myRecommenders[$i][14]?>&nbsp;</td>
        <td><?=$myRecommenders[$i][4]?> - <?=$myRecommenders[$i][5]?>&nbsp;</td>
        <td><?=$myRecommenders[$i][6]?> - <?=$myRecommenders[$i][7]?>&nbsp;</td>
        <td><? if($buckley == 1){ echo "Yes";}else{ echo "No";} ?></td>
      </tr>
	  <? } } ?>

<!-- END RECOMMEND TABLE -->
</table>
<? } ?>




<!--  BEGIN EMPLOYMENT TABLE  -->
<? 



    if (marray_search( "MSE-Dist", $myPrograms) == 0 
            || marray_search( "MSE-Campus", $myPrograms) == 0
            || marray_search( "MSIT-ESE", $myPrograms) == 0
            || marray_search( "MSE-Portugal", $myPrograms) == 0
            || marray_search( "MSE-MSIT-Korea", $myPrograms) == 0
             ){ 
                 $employments = getEmployments($appid);?>

<? if(count($pubs) > 0){ ?>
<table border="1"  cellpadding="0" cellspacing="0" width="100%"> 
<!-- EMPLOYMENT TABLE -->

  <tr><td colspan=4 bgcolor=c5c5c5 align=center><strong>Employment History</strong></td></tr>
  <tr>
    <td valign="top" bgcolor=#C5C5C5><strong>Company:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Start Date:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>End Date:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Years Exp.:</strong></td>
  </tr>
  <? for($i = 0; $i < count($employments); $i++){ ?>
  <tr>
    <td valign="top">
            <?=$employments[$i][1] ?>&nbsp;</td>
    <td valign="top">
            <?=$employments[$i][2] ?>&nbsp;</td>
    <td valign="top">
            <?= $employments[$i][3] ?>&nbsp;</td>
    <td valign="top">
            <?= $employments[$i][4] ?>&nbsp;</td>
  </tr>

  <? }//END PUBS LOOP ?>

<!-- END EMPLOYMENT TABLE -->
</table>
<? } ?>
<? } ?>

<!-- PLB added 02/04/09 -->
<div style="page-break-after: always;"> </div>

<? if(count($pubs) > 0){ ?>
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
<!-- PUBLICATION TABLE -->

  <tr><td colspan=4 bgcolor=c5c5c5 align=center><strong>Publications</strong></td></tr>
  <tr>
    <td valign="top" bgcolor=#C5C5C5><strong>Title:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Author(s):</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Name of Journal or Conference:</strong></td>
    <td valign="top" bgcolor=#C5C5C5><strong>Status:</strong></td>
  </tr>
  <? for($i = 0; $i < count($pubs); $i++){ ?>
  <tr>
    <td valign="top">
            <? if($pubs[$i][5] != "") {
			$header = "";
			if(strstr($pubs[$i][5], "http") === false)
			{
				$header = "http://";
			}
			?>
			<a href = "<?=$header.$pubs[$i][5] ?>" target="_blank"><?=$pubs[$i][1] ?></a>
			<? }else{ ?>
			<?=$pubs[$i][1] ?>
			<? } ?>		  </td>
    <td valign="top">
            <?=$pubs[$i][2] ?>&nbsp;</td>
    <td valign="top">
            <?php
            // PLB changed 02/04/09
            $journal_name = htmlspecialchars($pubs[$i][3]);
            /*
            <?= $pubs[$i][3] ?>&nbsp;</td>
            */
            ?>
            <?= $journal_name ?>&nbsp;</td>
    <td valign="top">
            <?= $pubs[$i][6] ?>&nbsp;</td>
  </tr>

  <?
    // PLB added test to break up big tables
  	if ( ($i % 2) == 0) {
      ?>
        </table>
        <table border="1"  cellpadding="1" cellspacing="0" width="100%">
      <?
  	}
  
   }//END PUBS LOOP ?>

<!-- END PUBS TABLE -->
</table>
<? } ?>


<? if(count($fellows) > 0){ ?>
<table border="1"  cellpadding="1" cellspacing="0" width="100%" class="tblItem">
<!-- FELLOWSHIP TABLE -->

  <tr><td colspan=6 bgcolor=c5c5c5 align=center><strong>Fellowships</strong></td></tr>
  <tr>
   <td valign="top" bgcolor=#C5C5C5><strong>Name</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Amount/Year</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Duration (years)</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Applied Date </strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Award Date</strong></td>
   <td valign="top" bgcolor=#C5C5C5><strong>Status:</strong></td>
  </tr>
  <? for($i = 0; $i < count($fellows); $i++){ ?>
  <tr>
   <td valign="top"><p><?= $fellows[$i][1]?>
    <br>
   </td>
   <td>$
     <?= $fellows[$i][2] ?></td>
   <td valign="top"><p>
     <?= $fellows[$i][6] ?>
     <br>
   </td>
   <td valign="top"><?= $fellows[$i][4] ?>&nbsp;</td>
   <td valign="top"><?= $fellows[$i][5] ?>&nbsp;</td>
   <td valign="top"><?= $fellows[$i][3] ?></td>
  </tr>
  <? } //END FELLOWS LOOP ?>

<!-- END FELLOW TABLE -->
</table>
<? } ?>


<?
 if( marray_search( "SCS", $myPrograms) !== false
|| marray_search( "CSD", $myPrograms) !== false
|| marray_search( "RI", $myPrograms) !== false
){ ?>
<table border="1"  cellpadding="1" cellspacing="0" width="100%">
<!-- MISC/SUPP TABLE -->
  <tr>
    <td  valign="top"><strong>Women@IT Fellowship:</strong> <?=$wFellow?></td>
    <td  valign="top"><strong>PIER:</strong> <?=$pier?></td>
    <td  valign="top"><strong>Sending Portfolio (HCII):</strong> <?=$port?>&nbsp;&nbsp;
    <strong>URL:</strong>&nbsp <?
    if ($portLink != NULL || $portLink != "" ) { ?>
      <a href=http://<?=$portLink?> target=_BLANK><?=$portLink?></a>
    <?}?>
  </tr>
<!-- END MISC/SUPP TABLE -->
</table>
<? } ?>


<?
// PLB replaced test to use myAdvisors array 
// if($advisor1 != "" || $advisor2 != "" || $advisor3 != "" || $advisor4 != ""){ 
if ( count($myAdvisors) > 0 ) {
?>
<table border=1  cellpadding=1 cellspacing=0 width=100% clas="tblItem">
<!-- COMPBIO TABLE TABLE -->

<tr><td colspan=3 align=center bgcolor=#C5C5C5><strong> Advisors </strong></td></tr>
<!-- ADVISORS -->
<tr>
  <td colspan=3 width=100%>
  <?
  // PLB replaced logic below to use myAdvisors array
  for ( $i = 0; $i < count($myAdvisors); $i++ ) {
  
        echo $myAdvisors[$i] . "<br />";
      
  }
  /*
    if($advisor1 != ""){echo $advisor1."<br>";}
    if($advisor1 != ""){echo $advisor2."<br>";}
    if($advisor1 != ""){echo $advisor3."<br>";}
    if($advisor1 != ""){echo $advisor4;}
  */
  ?>
  </td>
</tr>

<!-- END COMPBIO TABLE -->
</table>
<? } ?>


<? if( $cur_enrolled == "yes" || $honors != "" ){ ?>
<table border=1  cellpadding=1 cellspacing=0 width=100% clas="tblItem">
  <!-- CNBC TABLE -->
  <tr>
    <td bgcolor="#c5c5c5"><strong>Currently Enrolled </strong></td>
    <td bgcolor="#c5c5c5"><strong>Honors</strong></td>
  </tr>
  <tr>
    <td><?=$cur_enrolled?></td>
    <td><?=stripslashes($honors)?></td>
  </tr>
  <!-- END CNBC TABLE -->
</table>
<? } ?>
<? if($otherUni != "" ){ ?>
<table border=1  cellpadding=1 cellspacing=0 width=100% clas="tblItem">
<!-- CNBC TABLE -->
<tr>
  <td bgcolor="#c5c5c5"><strong>Other Institutes </strong></td>
  <td bgcolor="#c5c5c5"><strong>Other Programs </strong></td>
</tr>

<tr>
  <td><?=$otherUni?>&nbsp;</td>
  <td><?

	if(!is_array($crossDeptProgs))
	{

		$crossDeptProgs = explode(",",$crossDeptProgs);

	}

	$tmpDisplayItems = array();
	
	for($i = 0; $i < count($crossDeptProgs); $i++)
	{
		$showItem = true;
		if($crossDeptProgs[$i] != "Other")
		{
			for($k = 0; $k < count($tmpDisplayItems); $k++)
			{
				if($tmpDisplayItems[$k] == $crossDeptProgs[$i])
				{
					$showItem = false;
					break;
				}
			}
			if($showItem == true)
			{
				array_push($tmpDisplayItems, $crossDeptProgs[$i]);
				echo $crossDeptProgs[$i]. "<br>";
			}
			
			
		}
	}
	?> &nbsp;</td>
  </tr>
<tr>
  <td>Authorize to collect information: <? if($permission == 1){ echo "Yes";}else{echo "No";}?></td>
  <td>&nbsp;</td>
</tr>

<!-- END CNBC TABLE -->
</table>
<? } ?>

<? if($_SESSION['A_usertypeid'] != 12){//HIDE FROM FAC CONTATCS ?>
<!-- REVIEW STUFF GOES HERE -->
<!-- COMMITTEE REVIEW ROUNDS 1 AND 2 (CSD, RI) -->
<? if($view == 2 && ($thisDept !=6 )){?>
<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<!--<input name="private_comments" type="hidden" value="<?=$private_comments?>">-->
<? if($round == 2){?>
<input name="round2" type="hidden" value="<?=$round2?>">
<? } ?>
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<input name='btnSemiblind' id='btnSemiblind' type='Submit' value='Toggle Semi-blind Review' >
<input name="showDecision" type="hidden" value="<?=$showDecision?>">

<table cellspacing="0" cellpadding="0">
   <? if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="150"><strong>Committee <br>
      Scores:</strong></td>
    <td valign="top"><strong>
	<? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept )//NOT A SUPPLEMENTAL OR FAC VOTE
		{
			?><input name="revdept" type="hidden" value="<?=$committeeReviews[$i][32]?>"><?
			echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
			if($round > 1)
			{
				echo " (round ".$committeeReviews[$i][23].")";
			}
			//1st score
			?>
			<input name="score1" type="hidden" value="<?=$committeeReviews[$i][10]?>">
			<input name="score1" type="hidden" value="<?=$committeeReviews[$i][11]?>">
			<?
			if($thisDept == 3 )
			{
				echo " Score1: ". $committeeReviews[$i][10]. " Score2: ".$committeeReviews[$i][11];//show point 2
			}else
			{
				echo " Score: ". $committeeReviews[$i][10];//show point 1
			}
			/*
			//2nd score
			if($thisDept == 3 && count($myPrograms2) > 1)
			{	
				$showScore2 = false;
				for($k = 0; $k < count($myPrograms2); $k++)
				{
					if($myPrograms2[$k][1] == "M.S. in Robotics")
					{
						$showScore2 = true;
						break;
					}
				}
				if($showScore2 == true)
				{
					echo "/".$committeeReviews[$i][11];
				}
			}
			*/
			//1st score
			if($thisDept == 3 )
			{
				echo ", Certainty1: ". $committeeReviews[$i][28] . " Certainty2: ".$committeeReviews[$i][28] ;//show certainty 2
			}else
			{
				echo ", Certainty: ". $committeeReviews[$i][28];//show certainty 2
			}
			/*
			//2nd score
			
			if($thisDept == 3 && count($myPrograms2) > 1)
			{	
				$showScore2 = false;
				for($k = 0; $k < count($myPrograms2); $k++)
				{
					if($myPrograms2[$k][1] == "M.S. in Robotics")
					{
						$showScore2 = true;
						break;
					}
				}
				if($showScore2 == true)
				{
					echo "/".$committeeReviews[$i][28];
				}
			}
			*/
			
			if($committeeReviews[$i][13] == 1)
			{
				echo ", Round 2: Yes";
			}else
			{
				echo ", Round 2: No";
			}
			
			echo "<br>";
		}
	 } ?>
	</strong></td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" width="740">    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="150"><strong>Committee <br>Comments:</strong>	  </td>
    <td valign="top">
	<? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept)
		{
			echo "<input name='txtReviewerId' type='hidden' value='".$committeeReviews[$i][4]."'><strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (round ".$committeeReviews[$i][23].")";
			}
			echo "</strong><br>";
			echo $committeeReviews[$i][9]."<br>";
		}
	 } ?>	</td>
  </tr>
  
  <tr>
    <td colspan="2"><hr align="left" width="740">    </td>
  </tr>
  <? }//end semiblind reviews ?>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="150"><strong>Areas of <br> Interest:</strong></td>
    <td valign="top">	</td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" width="740"></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="150"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top" width="590">
	<? for($i = 0; $i < count($committeeReviews); $i++)
	{
	
		if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" )
		{

				?>
				<a linkindex="1" href="userroleEdit_student_formatted.php?v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
				<?=$committeeReviews[$i][2] . " ". $committeeReviews[$i][3]?></a> (<?=$committeeReviews[$i][15]?>)<br>
			  <?
	
		}
	 } ?></tr>
	 
	 
	 <? if($showDecision == false){ ?>
  <tr>
    <td colspan="2"><hr align="left" width="740"></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Comments: </strong> </td>
    <td>
	<?

	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" )
		{
			$comments = $committeeReviews[$i][9];
		}
	 }

	 showEditText($comments, "textarea", "comments", $allowEdit, false, 80); ?>	</td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong>Personal<br>
      Comments: </strong> </td>
    <td>
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$private_comments = $committeeReviews[$i][12];
		}
	 }
	?>
	<? showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 80); ?>	</td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Ranking<? if($thisDept == 3){echo " (Phd)";} ?>: </strong> </td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$point = $committeeReviews[$i][10];
		}
	 }
	if($thisDept == 3)
	{
		echo "Enter a number from 5.000 to 10: ";
		showEditText($point, "textbox", "point", $allowEdit, false);
	}else
	{
		showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
	}
	?></td>
  </tr>

  <tr>
    <td bgcolor="#c5c5c5"><strong>Certainty<? if($thisDept == 3){echo " (PhD)";} ?>:</strong></td>
    <td><table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">High-</td>
          <td align="center"><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$pointCertainty = $committeeReviews[$i][27];
		}
	 }

	showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals);
	?></td>
          <td align="center">-Low </td>
        </tr>
      </table></td>
  </tr>
  <? 
  /*
  DISPLAY 2ND SCORE FOR RI
  */
  if($thisDept == 3){
  ?>
  <tr>
    <td bgcolor="#c5c5c5"><strong>Ranking<? if($thisDept == 3){echo " (M.S.)";} ?>: </strong></td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$point2 = $committeeReviews[$i][11];
		}
	 }
	if($thisDept == 3)
	{
		echo "Enter a number from 5.000 to 10: ";
		showEditText($point2, "textbox", "point2", $allowEdit, false);
	}else
	{
		showEditText($point2, "radiogrouphoriz3", "point2", $allowEdit, false, $csdVoteVals);
	}
	?></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5"><strong>Certainty<? if($thisDept == 3){echo " (M.S.)";} ?>:</strong></td>
    <td><table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center">High-</td>
        <td align="center"><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$point2Certainty = $committeeReviews[$i][28];
		}
	 }

	showEditText($point2Certainty, "radiogrouphoriz3", "point2Certainty", $allowEdit, false, $certaintyVals);
	?></td>
        <td align="center">-Low </td>
      </tr>
    </table></td>
  </tr>
   <? }else{?>
  	<input name="point2" type="hidden" value="<?=$point2?>">
  <? }//end if $doDisplay ?>
  <? if($round  == 1 ){ ?>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Round 2: </strong> </td>
    <td>
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$round2 = $committeeReviews[$i][13];
		}
	 }

	showEditText($round2, "radiogrouphoriz2", "round2", $allowEdit, false, array(array(1,"yes"),array(0,"No")) ); ?>	</td>
  </tr>
 
 <? }//end if round1 ?>
  <tr>
    <td colspan="2">
    <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
 	<br>
 	<br>   
	<? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>	</td>
  </tr>
  <? }//end not admin ?>
</table>
	
	
	<!-- FINAL ROUND DECISION STUFF -->
	<? } ?>
	<? if($_SESSION["A_usertypeid"] == 1 && $showDecision == true){ ?>
	
<table width=740 border =0 cellpadding="0" cellspacing=2 cellpading=2>
<? 
for($x = 0; $x < count($myPrograms); $x++)
{
	$aDepts = split(",", $myPrograms[$x][14]);
	//FIRST CHECK IF THE PROGRAM BELONGS TO THIS DEPARTMENT
	for($j = 0; $j < count($aDepts); $j++)
	{
		if($aDepts[$j] == $thisDept)
		{
			//CHECK IF PROGRAM IS ELIGIBLE FOR ROUND2
			?>
			
			<tr><td width="740">
<hr width=740 align=left>
<strong>Admit Information for program:</strong> <em><?=$myPrograms[$x][1] . " ".$myPrograms[$x][2] . " ".$myPrograms[$x][3]?> </em>
</td>
</tr>


<tr>
<td>
<table border=0 cellpadding=0 cellspacing=2>
<tr>

<td bgcolor=c5c5c5 width=150> <font color=000099><b> Comments: </b> </td>
<td><?
	$comments = $myPrograms[$x][13];
	showEditText($comments, "textarea", "comments_".$myPrograms[$x][0], $allowEdit, false, 80); ?></td>
<td valign=top align=right bgcolor=c5c5c5 width=150> <font color=000099><b> Decision: </b> </td>
<td valign=top>
<? 
$decision = $myPrograms[$x][10] ;
showEditText($decision, "listbox", "decision_".$myPrograms[$x][0], $_SESSION['A_allow_admin_edit'], true, $decisionVals); 
?></td>
</tr>
</table></td>
</tr>

<tr>
<td>
<table border=0 cellpadding=0 cellspacing="2">
<tr><td width=370><font color=000099><strong>Faculty Contact:&nbsp;</strong></font>
<? 
$faccontact = $myPrograms[$x][15];
showEditText($faccontact, "textbox", "faccontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); ?>
</td>
<td width=370><strong><font color=000099>Student Contact:&nbsp;</font></strong>
<? 
$stucontact = $myPrograms[$x][16];
showEditText($stucontact, "textbox", "stucontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); ?></td>
</tr>
</table></td>
</tr>
<tr>
<td>
<table border=0 width=680 bgcolor=c5c5c5 cellpadding=0 cellspacing=2>
<tr><td><font color=000099><h4>Admissions Status
</td></tr>
<tr>
<td width=340 colspan=2><font color=000099>
<? 
$admissionStatus = $myPrograms[$x][11];
showEditText($admissionStatus, "radiogrouphoriz2", "admissionStatus_".$myPrograms[$x][0], $allowEdit, false, array(array(0,"Reject"),array(1,"Waitlist"),array(2,"Admit"),array(3,"Reset")   ) ); ?></tr>
<!--
<tr>
<td width=70>
<font color=000099> &nbsp;&nbsp;&nbsp;<b>Admit to: </td><td width=270 align=left>
<?  showEditText($admit_to, "listbox", "admitto_".$myPrograms[$x][0], $allowEdit, false,  $myPrograms2); ?>
</td>
</tr>
-->
</table></td>
</tr>

			
			
			<?
		}//END IF DEPTS MATCH
	}//END FOR COUNT aDEPTS	
	
 }//END FOR EACH PROGRAM?>
 <tr>
  <td><? showEditText("Save", "button", "btnSubmitFinal", $allowEdit); ?></td>
</tr>
</table>
	
	
<? }//end show decision ?><!-- LTI REVIEWS -->
<? if($thisDept == 6 && $round ==1 && $view == 2 ){ ?>
<table width="100%" cellpadding="0" cellspacing="2">
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Coordinator Notes: </strong> </td>
    <td bgcolor="#dedede"><?
	//GET LTI COORDINATOR ID
	$coordId = -1;
	$sql = "select lu_users_usertypes.id from lu_users_usertypes
	left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
	where lu_user_department.department_id=".$thisDept." and lu_users_usertypes.usertype_id=1";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$coordId = $row['id'];
	}

	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $coordId)
		{
			$comments = $committeeReviews[$i][9];
		}
    }
	echo $comments;
	?>    </td>
  </tr>
  <tr>
    <td colspan="2"><hr />
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Background: </strong> </td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		
		if($committeeReviews[$i][29] == "")
		{
			echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
			echo $committeeReviews[$i][6] . "<br>";
		}
	 }
	 ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5" width="150"><strong> Area(s) of Interest</strong>: </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Add Interest: </strong> </td>
    <td bgcolor="#dedede">
	
	<? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>". $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			$committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
			for($j = 0; $j < count($committeeReviewsSeminars); $j++)
			{
				$idx = $committeeReviewsSeminars[$j][0];
				if($idx == "")
				{
					$idx = 0;
				}else
				{
					$idx = $idx  -1;
				}
				//echo $committeeReviewsSeminars[$j][0]. " ";
				if(marray_search($idx, $ltiSeminars) !== false)
				{
					echo $ltiSeminars[ $idx ][1]. " ";
				}
				
			}
			
			echo "<br>";
		}
	 } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Other Interests: </strong> </td>
    <td>
	 
	 <? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][26]."<br>";
		}
	 } ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Statement: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][8]."<br>";
		}
	 } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Grades: </strong> </td>
    <td><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][7]."<br>";
		}
	 } ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Interview: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][24]."<br>";
		}
	 } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Refer to: </strong> </td>
    <td><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][17]."<br>";
		}
	 } ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Brilliance: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][24]."<br>";
		}
	 } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Finished:</strong> </td>
    <td> Yes </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Other: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
			echo $committeeReviews[$i][9]."<br>";
		}
	 } ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> PhD Score: </strong> </td>
    <td><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
//			echo $committeeReviews[$i][10]."<br>";
			echo number_format($committeeReviews[$i][10],2) ."<br>";
		}
	 } ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> MLT Score: </strong> </td>
    <td bgcolor="#dedede"><? for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
			if($round > 1)
			{
				echo " (".$committeeReviews[$i][23].")";
			}
			echo "</strong>";
//			echo $committeeReviews[$i][11]."<br>";
			echo number_format($committeeReviews[$i][11],2)."<br>";
		}
	 } ?></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" ><strong>Avgs:</strong></td>
    <td>
	<? 
	$totalP = 0;
	$totalM = 0;
	for($i = 0; $i < count($committeeReviews); $i++){
		if($committeeReviews[$i][29] == "")
		{
			if($committeeReviews[$i][10] > 0)
			{
				$totalP += $committeeReviews[$i][10];
			}
			if($committeeReviews[$i][11] > 0)
			{
				$totalM += $committeeReviews[$i][11];
			}
		}
	 } 
	 
	 if($totalP > 0)
	 {
	 	$totalP = $totalP/count($committeeReviews);
	 }
	 if($totalM > 0)
	 {
	 	$totalM = $totalM/count($committeeReviews);
	 }
//	 echo "PhD: ".$totalP . " MS: ".$totalM;
	 echo "PhD: ".number_format($totalP,2) . " MS: ".number_format($totalM,2);

	 ?>
	</td>
  </tr>
</table>
<!-- END LTI REVIEWS -->
<? }//END LTI REVIEWS ?>
<!-- LTI COORDINATOR -->
<? if($thisDept == 6 && $round ==1 && $view == 2 && ($_SESSION['A_usertypeid']==0 || $_SESSION['A_usertypeid']==1 )){ ?>
<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<input name="point" type="hidden" value="<?=$point?>">
<input name="point2" type="hidden" value="<?=$point2?>">
<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">
<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<!--<input name="touched" type="hidden" value="<?=$touched?>">-->
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<table width="100%" cellpadding="0" cellspacing="0">

  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Note: </strong> </td>
    <td><?

	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$comments = $committeeReviews[$i][9];
		}
	 }

	 showEditText($comments, "textarea", "comments", $allowEdit, false, 80); ?>    </td>
  </tr>

  <tr>
    <td bgcolor="#c5c5c5"><strong>Refer to: </strong></td>
    <td bgcolor="#dedede"><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$grad_name = $committeeReviews[$i][17];
		}
	 }
	showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Finished: </strong> </td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$touched = $committeeReviews[$i][14];
		}
	 }

	showEditText($touched, "radiogrouphoriz2", "touched", $allowEdit, false, array(array(1,"yes"), array(0,"No")) ); ?></td>
  </tr>
  <? if($round == 1){ ?>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Round 2: </strong> </td>
    <td bgcolor="#dedede">&nbsp;</td>
  </tr>
  <? } ?>
  <tr>
    <td colspan="2">
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />		
		<br><br>    
    <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>    </td>
  </tr>
</table>
<? }//end LTI COORDINATOR ?>


<!-- LTI COMMITTEE -->
<? if($thisDept == 6 && $round ==1 && $view == 2 && ($_SESSION['A_usertypeid']== 2 )){ ?>
<!--<input name="background" type="hidden" value="<?=$background?>">-->
<!--<input name="grades" type="hidden" value="<?=$grades?>">-->
<!--<input name="statement" type="hidden" value="<?=$statement?>">-->
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<!--<input name="touched" type="hidden" value="<?=$touched?>">-->
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<table width="100%" cellpadding="0" cellspacing="2">
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Coordinator Notes: </strong> </td>
    <td bgcolor="#dedede">
	<?
	//GET LTI COORDINATOR ID
	$coordId = -1;
	$sql = "select lu_users_usertypes.id from lu_users_usertypes
	left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes.id
	where lu_user_department.department_id=".$thisDept." and lu_users_usertypes.usertype_id=1";
	$result = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_array( $result ))
	{
		$coordId = $row['id'];
	}

	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $coordId)
		{
			$comments = $committeeReviews[$i][9];
		}
    }
	echo $comments;
	?>	</td>
  </tr>
  <tr>
    <td colspan="2"><hr />
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Background: </strong> </td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$background = $committeeReviews[$i][6];
		}
	 }
	 showEditText($background, "textarea", "background", $allowEdit, false, 80); ?>
	 </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5" width="150"><strong> Area(s) of Interest</strong>: </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Add Interest: </strong> </td>
    <td bgcolor="#dedede">
	<?

	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$committeeReviewsSeminars = getReviewSeminars($committeeReviews[$i][0]);
		}
	 }
	showEditText($committeeReviewsSeminars, "checklist", "seminars", $allowEdit, false, $ltiSeminars ); ?> </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Other Interests: </strong> </td>
    <td>
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$other_interest = $committeeReviews[$i][26];
		}
	 }
	showEditText($other_interest, "textbox", "other_interest", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Statement: </strong> </td>
    <td bgcolor="#dedede">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$statement = $committeeReviews[$i][8];
		}
	 }
	 showEditText($statement, "textbox", "statement", $allowEdit, false, null,true,40); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Grades: </strong> </td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$grades = $committeeReviews[$i][7];
		}
	 }
	 showEditText($grades, "textbox", "grades", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Interview: </strong> </td>
    <td bgcolor="#dedede"><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$interview = $committeeReviews[$i][24];
		}
	 }
	showEditText($interview, "textbox", "interview", $allowEdit, false, null,true,40); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Refer to: </strong> </td>
    <td>
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$grad_name = $committeeReviews[$i][17];
		}
	 }
	showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Brilliance: </strong> </td>
    <td bgcolor="#dedede"><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$brilliance = $committeeReviews[$i][24];
		}
	 }
	showEditText($brilliance, "textbox", "brilliance", $allowEdit, false, null,true,40); ?>    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> Other: </strong> </td>
    <td><?

	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$comments = $committeeReviews[$i][9];
		}
	 }

	 showEditText($comments, "textarea", "comments", $allowEdit, false, 80); ?>
    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> PhD Score: </strong> </td>
    <td bgcolor="#dedede">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$point = $committeeReviews[$i][10];
		}
	 }

	showEditText($point, "radiogrouphoriz2", "point", $allowEdit, false, $ltiVoteVals);

	?>	</td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="150"><strong> MLT Score: </strong> </td>
    <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$point2 = $committeeReviews[$i][11];
		}
	 }

	showEditText($point2, "radiogrouphoriz2", "point2", $allowEdit, false, $ltiVoteVals);

	?></td>
  </tr>
  <tr>
    <td colspan="2"></td>
  </tr>
</table>
<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
<br><br>
<? showEditText("Save", "button", "btnSubmit", $allowEdit); ?>
<? }//END LTI COMMITTEE ?>



<!-- FACULTY REVIEW FOR ROUND 2 -->
<? if($view == 3 && $round == 2){ ?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="comments" type="hidden" value="<?=$comments?>">
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="facVote" type="hidden" value="1">
<!--<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">-->
<!--<input name="recruited" type="hidden" value="<?=$recruited?>">-->
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<!--<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">-->
<!--<input name="advise_time" type="hidden" value="<?=$advise_time?>">-->
<!--<input name="commit_money" type="hidden" value="<?=$commit_money?>">-->
<!--<input name="fund_source" type="hidden" value="<?=$fund_source?>">-->

<table cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>PART I.  INFORMATION FOR THE ADMISSIONS COMMITTEE. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Do you think we should admit this applicant?  Select one, and explain why by provding other pertinent information that might be useful to the Admissions Committee and your colleagues (e.g. direct knowledge of the student or his/her research, calibration of the recommenders, etc.) </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$admit_vote = $committeeReviews[$i][15];
		}
	 }

	showEditText($admit_vote, "radiogrouphoriz2", "admit_vote", $allowEdit, false,
	array(
	array("DEFINITELY","DEFINITELY"),
	array("Probably Yes","Probably Yes"),
	array("Probably No","Probably No"),
	array("DEFINITELY NOT","DEFINITELY NOT")
	)
	); ?>
	<br>

<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$pertinent_info = $committeeReviews[$i][18];
		}
	 }
	 showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 80); ?>
  </td>
  </tr>





<!--OTHER FAC COMMENTS -->  
<tr>
    <td colspan="2" bgcolor="#c5c5c5">
    <strong>Other faculty comments    
    </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "")
		{
			echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] ."</strong><br>".$committeeReviews[$i][18]."<br>";
		}
	}
	?>
	</td>
  </tr> 



<!-- END OTHER FAC COMMENTS -->
<tr>
    <td colspan="2" bgcolor="#c5c5c5">
    <strong>Please rank at least your top 3 choices. This applicant is my:    
    </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	$rank = 0;
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$rank = $committeeReviews[$i][31];
		}
	 }
	$arr = array(array(0,"This is not a top 5 choice"),	array(1,"1st Choice"),array(2,"2nd Choice") ,array(3,"3rd Choice"),array(4,"4th Choice"),array(5,"5th Choice")   );
	if($rank == "")
	{
		$rank = 0;
	}
	if($allowEdit == true)
	{
		
		showEditText($rank, "radiogrouphoriz2", "rank", $allowEdit, false, $arr 	); 
	}else
	{
		
		echo $arr[$rank][1];
		
		
	}	
	?>
	</td>
  </tr>  
  
  
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would be willing to play an active role in recruiting this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$recruited = $committeeReviews[$i][16];
		}
	 }

	 $arr = array(	array(0, "No"), array(1,"Yes"));
	if($allowEdit == true)
	{
		showEditText($recruited, "radiogrouphoriz2", "recruited", $allowEdit, false, $arr  	); 
	}else
	{
		if($recruited != "")
		{
			echo $arr[$recruited][1];
		}
	}	
	?>

	</td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I suggest the following graduate students as possible recruiting contacts for this student: </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$grad_name = $committeeReviews[$i][17];
		}
	 }
	showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <!--
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Other pertinent information that might be useful to the Admissions Committee (e.g. direct       knowledge of the student or his/her research, calibration of the recommenders, etc.) </strong></td>
  </tr>
 
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$pertinent_info = $committeeReviews[$i][18];
		}
	 }
	 showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 80); ?>
    </td>
  </tr>
   -->
  <tr>
    <td colspan="2"><hr align="left" width="740" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>PART II.  INFORMATION FOR THE DEPARTMENT CHAIR. <br />
      In the event that this student is considered on the borderline, your  answers to the following questions could make a difference in the  decision the department chair makes on whether or not to offer  admission to the student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would commit my time and energy to advise this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
		<?
		for($i = 0; $i < count($committeeReviews); $i++)
		{
			if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
			{
				$advise_time = $committeeReviews[$i][19];
			}
		 }
//		 $arr =  array(	array(1,"Yes"),	array(0,"No") );
		 $arr =  array(	array(0, "No"), array(1,"Yes") );
		if($allowEdit == true)
		{
		 
			showEditText($advise_time, "radiogrouphoriz2", "advise_time", $allowEdit, false,$arr 	); 
		}else
		{
			if($recruited != "")
			{
				echo $arr[$advise_time][1];
			}
		}		
		?>
	</td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would commit my money to advise this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
	<?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$commit_money = $committeeReviews[$i][20];
		}
	 }
//	 $arr = array(	array(1,"Yes"),	array(0,"No") ) ;
	 $arr = array(	array(0,"No"), array(1,"Yes") ) ;
	 if( $allowEdit == true)
	 {
	 
		showEditText($commit_money, "radiogrouphoriz2", "commit_money", $allowEdit, false, $arr	); 
	}else
	{
		if($recruited != "")
		{
			echo $arr[$commit_money][1];
		}
	}
	?>
     <br />
      <strong> If yes, please state your probable source of funding.</strong> <br />
	  <?
	  	for($i = 0; $i < count($committeeReviews); $i++)
		{
			if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
			{
				$fund_source = $committeeReviews[$i][21];
			}
		 }
	   showEditText($fund_source, "textarea", "fund_source", $allowEdit, false, 80); ?><br>
<br>

      </td>
  </tr>
   <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
     <!-- REMOVE OVERALL RANKING  
      <tr>
        <td bgcolor="#c5c5c5"><strong>Ranking - Overall:</strong></td>
        <td><?
	for($i = 0; $i < count($committeeReviews); $i++)
	{
		if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
		{
			$point = $committeeReviews[$i][10];
		}
	 }
	if($thisDept == 3)
	{
		echo "Enter a number from 5.000 to 10: ";
		showEditText($point, "textbox", "point", $allowEdit, false);
	}else
	{
		showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
	}
	?></td>
        </tr>
   
        
      <tr>
        <td width="150" bgcolor="#c5c5c5"><strong>Certainty:</strong></td>
        <td><table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center">High-</td>
            <td align="center"><?
				for($i = 0; $i < count($committeeReviews); $i++)
				{
					if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
					{
						$pointCertainty = $committeeReviews[$i][27];
					}
				 }
			
				showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals);
			
				?></td>
            <td align="center">-Low </td>
          </tr>
        </table>
                  </td>
        </tr>
    </table></td>
  </tr>
     -->
   <? if(count($myAreasAll) > 0){  ?>
       <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Please discuss with your colleagues and enter a score that reflects the strength of this applicant within the research areas listed below.</strong></td>
  </tr>
  
  <? 
  }
  	for($j = 0; $j < count($myAreasAll); $j++){ ?>
	  <tr>
	  <td colspan="2">
	
	
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td bgcolor="#c5c5c5" WIDTH="150"><strong>
	          <?=$myAreasAll[$j][1]?>
	          :</strong></td>
	        <td><?
			$val = 0;
			for($i = 0; $i < count($committeeReviews); $i++)
			{
				if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
				{
					$val = $committeeReviews[$i][10];
				}
			 }
			
			if($thisDept == 3)
			{
				echo "Enter a number from 5.000 to 10: ";
				showEditText($val, "textbox", "point_".$myAreasAll[$j][0], $allowEdit, false);
			}else
			{
				?><TABLE BORDER=0>
				<tr>
				  <td>Low-</td>
				  <td>
				<?
				showEditText($val, "radiogrouphoriz3", "point_".$myAreasAll[$j][0], $allowEdit, false, $csdFacVoteVals);
				?></td>
				  <td>-High</td>
				</tr></table><?
			}
			
			?></td>
	        </tr>
				  <!-- DONT SHOW CERTAINTY
						<tr>
						  <td width="150" bgcolor="#c5c5c5"><strong>Certainty:</strong></td>
						  <td><table border="0" cellspacing="0" cellpadding="0">
						 <tr>
						<td align="center">High-</td>
						<td align="center"><?
						$val = 0;
						for($i = 0; $i < count($committeeReviews); $i++)
						{
						if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
						{
						$val = $committeeReviews[$i][27];
						}
						 }
						
						showEditText($val, "radiogrouphoriz3", "pointCertainty_".$myAreasAll[$j][0], $allowEdit, false, $certaintyVals);
						
						?></td>
						<td align="center">-Low</td>
						 </tr>
						  </table>
						</td>
						</tr>
				-->
	    </table>
	
	
	
	
	  </td>
	  </tr>
  <? }//END AOI REVIEWS ?>
  <tr>
    <td colspan="2">
	<input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />
	<br>
	<br>    
    <? showEditText("Save", "button", "btnSubmit", $allowEdit); ?></td>
  </tr>
</table>
<? }//end if view ?>
<!-- END REVIEW STUFF -->

<? 
}//END FAC CONTACT HIDE
}//end student contact HIDE ?>


</body>
</html>
