<?PHP
	$fn = "plb-run-0109.txt";
	$file = file($fn);
	
	$failures = array();
	$count = 0;
	for ($i = 0; $i < count($file); $i++)
	{
		$line = $file[$i];
		
		$id_pattern = "/application id/";
		if ( preg_match($id_pattern, $line) )
		{
			$parts = split(":", $line);
			$application_id = trim($parts[1]);
		}

		$guid_pattern = "/applicant guid/";
		if ( preg_match($guid_pattern, $line) )
		{
			$parts = split(":", $line);
			$guid = trim($parts[1]);
		}
						
		$merge_failed_pattern = "/Merge failed for/";
		if ( preg_match($merge_failed_pattern, $line) )
		{
			$parts = split("for", $line);
			$fullPath = trim($parts[1]);
			$pathParts = split("htdocs", $fullPath);
			$webPath = $pathParts[1];
			$fileName = basename($webPath);
			
			if ( isset( $failures[$application_id]) )
			{
				array_push($failures[$application_id]['files'], $webPath);
			}
			else
			{
				$failures[$application_id]['guid'] = $guid;
				$failures[$application_id]['files'] = array($webPath);
			}

		}
		
		$conversion_failed_pattern = "/Conversion error for/";
		if ( preg_match($conversion_failed_pattern, $line) )
		{
			$prev_line = $file[$i-1];
			$parts = split(" ", $prev_line);
			$fullPath = trim($parts[2]);
			$pathParts = split("htdocs", $fullPath);
			$webPath = $pathParts[1];

			$fileName = basename($webPath);
			
			if ( isset( $failures[$application_id]['files']) )
			{
				array_push($failures[$application_id]['files'], $webPath);
			}
			else
			{
				$failures[$application_id]['guid'] = $guid;
				$failures[$application_id]['files'] = array($webPath);
			}
		}		
		
	}
	
 	mysql_connect('localhost', 'phdapp', 'phd321app') or die ('Error connecting to mysql');
    mysql_select_db('gradAdmissions2008Test');
    //mysql_select_db('gradAdmissions2009New');
    
    foreach ( $failures as $application_id => $applicant )
    {
	    
        // Delete existing records for the application
        $delete_query = "DELETE FROM failed_merges WHERE application_id = " . $application_id;
        $delete_result =  mysql_query($delete_query);  
        if (! $delete_result ) print "$delete_query<br>";
        
        $guid = $applicant['guid'];
	    $files = $applicant['files'];
	    foreach ( $files as $file )
	    {
		 	$query = "INSERT INTO failed_merges ( application_id, guid, file) VALUES ('$application_id', '$guid', '$file')";
		 	$result = mysql_query($query);  
		 	if (! $result ) print "$query<br>";
	    }
	    
    }
    //print_r($failures);
    
    mysql_close();

?>
done