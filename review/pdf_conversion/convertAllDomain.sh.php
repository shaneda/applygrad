#!/usr/local/bin/php5 
<?php
// put this outside for production
#!/usr/local/bin/php5  

/*
// Script for converting non-pdf application documents to pdf
// and merging all pdfs into single, complete application pdf.
// Conversions, merges occur in applicant's guid directories.
*/

// Include the config file to use $datafileroot
//@include_once '../../inc/config.php';

// Raise the memory limit for dompdf and include the library.
ini_set('memory_limit', '512M'); 
require_once "./dompdf-0.5.1/dompdf_config.inc.php";

// Include files for search text build.
include_once "../../inc/config.php";


// Set database variables and include the db classes.
// CHANGE AS NECESSARY BEFORE RUNNING SCRIPT
$db = "gradAdmissions2009New";
//$db = "gradAdmissions2008Test"; 
$db_host = "localhost";
$db_username = "phdapp";
//$db_username = "phdApp"; 
$db_password = "phd321app";

include_once "../../inc/db_connect.php"; 


include "../classes/class.db_applyweb.php";
include "../classes/class.db_merge_application_list.php";
include "../classes/class.db_applicant.php";
include "../classes/class.db_datafiles.php";
include "../classes/class.db_merge_recommendations.php";

// Instantiate db classes
$db_application_list = new DB_ApplicationList();
$db_applicant = new DB_Applicant();
$db_datafiles = new DB_Datafiles();
$db_recommendations = new DB_Recommendations();

/*        
// Get the ids of the submitted applications
// Use argv for application id to do a single conversion.
if ($argc > 1) {
    if ($argv[1] == 'domain')   {
         $submitted_applications = $db_application_list->getByDomain($argv[2]);
         $single_merge_needed = 0;
    }
    else
        {
             $submitted_applications = array( array( "id" => $argv[1] ) );
             $single_merge_needed = 1;
        }    
} else {

    // PLB changed to do only round 2 applications 01/09/09
    //$submitted_applications = $db_application_list->getSubmitted();
    //$submitted_applications = $db_application_list->getSubmittedRound2();
    $submitted_applications = $db_application_list->getByDomain($argv[1]);
    $single_merge_needed = 0;     
    
}
 */

 /// for testing purposes
 $submitted_applications = $db_application_list->getByDomain(2);
         $single_merge_needed = 0;
// Convert the files for each application.
//for ( $i = 0; $i < 10; $i++ ) { 
$i = 1;
foreach($submitted_applications as $application) {
    
    // Skip application 5001 b/c it has severe merge issues
    if ($application['id'] == 5001) {
    
        continue;
        
    }
    
    // Start an execution timer.
    $start_time = time();
    
    // Increase the max execution time by two minutes
    $new_execution_time = ini_get('max_execution_time') + 120;
    set_time_limit($new_execution_time);

    // Get the application id.
    //$application = $submitted_applications[$i];  
    $application_id = $application['id'];
      
    echo $i . ". application id: " . $application_id . "\n";
    
    // Get the applicant's user_id and guid.
    $applicant_info = $db_applicant->getApplicantInfo($application_id);
    $applicant_guid = $applicant_info[0]['guid'];
    $uid = $applicant_info[0]['luu_id'];
    echo "applicant guid: " . $applicant_guid . "\n";
    
    echo "Memory usage:" . memory_get_usage() . "\n";
       
    // Set the docpath for the applicant's files.
   $docpath = "/usr0/wwwsrv/htdocs/data/2009/" . $applicant_guid . "/";
//    $docpath = "../" . $datafileroot . "/" . $applicant_guid . "/";
                        //           DebugBreak();
	// Convert the base application.
	$base_merge_file = handleBaseApplication($uid, $application_id, $docpath, $single_merge_needed);

    // Skip application if base conversion fails.
    if ($base_merge_file['base_filename'] == "") {
        
        print_r($base_merge_file);
        echo "Conversion of base application failed: skipping " . $application_id . "\n\n";
        $i++;
        continue;
        
    }
    
    // Get the timestamp of the previous merge file for use in the datafile conversion.
    $previous_merge_filename = $docpath . $application_id . "_merged.pdf";        
    $previous_merge_timestamp = 0;
    if ( file_exists($previous_merge_filename) ) {
        
        $previous_merge_timestamp = filemtime($previous_merge_filename);    
    
    }
    
    // Convert the datafiles.
    $applicant_datafile_info = $db_datafiles->getDatafiles($application_id);
    $datafile_merge_files = handleDatafiles($applicant_datafile_info, $docpath, $previous_merge_timestamp);

    // Convert the "content" (non-datafile) recommendations.
    $applicant_recommendations = $db_recommendations->getRecommendations($application_id);
    $recommendation_merge_files = handleContentRecommendations($applicant_recommendations, $application_id, $docpath);             

    // Check for need to merge
    if ( $single_merge_needed || $base_merge_file['merge_needed'] || 
        $datafile_merge_files['merge_needed'] || $recommendation_merge_files['merge_needed'] ) {
        
        echo "Attempting merge\n";
        
        // Merge the all the pdfs.
        // PLB changed to stepwise merge 01/09/09
        //$merge_status = mergePdfs($base_merge_file, $datafile_merge_files, $recommendation_merge_files, $application_id, $docpath);
        $merge_status = mergePdfsStepwise($base_merge_file, $datafile_merge_files, $recommendation_merge_files, $application_id, $docpath);

        // Update datafile info if merge successful.
        //if ($merge_status['error'] && ($merge_status['error'] == 1) ) { // unrecoverable ghostscript error
        if ($merge_status['error']) {
            
            echo "Merge failed.\n";
            print_r($merge_status);
            
            // Delete any malformed merge file.
            $merge_file =  $docpath . $application_id . "_merged.pdf";
            if ( file_exists($merge_file) ) {
            
                $unlinked = unlink($merge_file);
                if ($unlinked) {
                
                    echo "Merge file deleted.\n";
                    
                } else {
                
                    echo "Unable to delete merge file.\n";
                    
                }
                
                
                
            } else {
            
                echo "No merge file created.";
                
            }
            
        /*
        } elseif ($merge_status['error']) {
            
            echo "Merge may have errors.\n";
            print_r($merge_status);
        */
        } else {
        
             echo "Merge successful.\n";
             
             // Do the search text build.
             buildSearchTextFromPDF ($applicant_guid, $application_id . "_merged.pdf", $docpath);
             
             
        }

         // Update the datafile info for the successful conversions. 
         foreach($datafile_merge_files as $datafile_array_key => $datafile_array) {
             
             // Skip the merge_needed element.
             if ( ($datafile_array_key != "merge_needed") && ($datafile_array_key != "conversion_error") ) {
             
                // Loop the statement, transcript, etc. arrays.
                foreach ($datafile_array as $datafile_key => $datafile) {
                    
                    // Update db if there was no conversion error.
                    // (A double check, since files are only added if there was no error.)
                    if (!$datafile_merge_files['conversion_error'][$datafile_key]) {
                    
                        // Set the update arguments.
                        $recommendation_id = $datafile_key;
                        $type = "application/pdf";
                        $extension = "pdf";
                        $size = filesize($datafile);
                        
                        // Update the db.
                //        $db_datafiles->updateConvertedDatafile($recommendation_id, $type, $extension, $size);
                    }
                    
                }    
                 
             }
             
         } 
     
    } else {
    
        // Nothing has changed since last merge.
        echo "No merge needed.\n";
        
        // Do the search text build if it wasn't done before.
        $merge_text_file = $docpath . $application_id . "_merged.txt";
        if ( !file_exists ($merge_text_file) ) {
          
            buildSearchTextFromPDF ($applicant_guid, $application_id . "_merged.pdf", $docpath);
            
        }
        
               
        
    }    
     
    // Stop the execution timer.
    $end_time = time();
    $execution_time = $end_time - $start_time;
    echo "Conversion time: " . $execution_time . " seconds\n";
    
    // Wait a few seconds before moving to next application
    echo "Memory usage:" . memory_get_usage() . "\n\n";
    sleep(2);   
    $i++;

}


//############ functions #####################

/*
// Check for any difference between a string and the contents of a text file.
// Filename must be absolute path to file.
// Returns TRUE if there is a difference and FALSE if there is not.
*/
function stringTextfileDifferent($comparison_string, $comparison_text_filename) {
    
    // Hash the string.
    $string_hash = hash("md5", $comparison_string);
    //echo $string_hash . "\n";
    
    // Hash the file contents if it exists.
    if ( file_exists($comparison_text_filename) ) {
    
        $file_hash = hash_file("md5", $comparison_text_filename);
        
    } else {
    
        $file_hash = "";
        
    }
    //echo $comparison_text_filename . "\n";
    //echo $file_hash . "\n";
        
    // Compare the hashes.
    if ($string_hash != $file_hash) {
    
        return TRUE;
        
    }
    
    return FALSE;    
}


/*
// Write an html string to an html file.
// Returns TRUE/FALSE on success/failure.
*/
function htmlString2HtmlFile($in_html, $outfile) {
    
    // write file if non-empty string
    if ($in_html == "") {

        return FALSE;
        
    } else {
        
        $fp = fopen($outfile, "w");
        
        if ($fp === FALSE) {
            
             return FALSE;
        
        }
        
        $write_status = fwrite($fp, $in_html);
            
        if ($write_status === FALSE) {
            
            fclose($fp);
            
            return FALSE;
                
        } 
            
        fclose($fp);        
        
        echo "Html written: " . $outfile . "\n";
        
        return TRUE;
        
    }
}


/*
// Convert an html file to a pdf file.
// Returns TRUE/FALSE on success/failure.
*/
function htmlFile2PdfFile($in_filename, $out_filename, $paper_size="letter", $paper_orientation="portrait") {

    // Execute dompdf.php to do the conversion
    $conversion_command = "./dompdf-0.5.1/dompdf.php ";
    $conversion_command .= $in_filename;
    $conversion_command .= " -f " . $out_filename;
    $conversion_command .= " -p " . $paper_size;
    $conversion_command .= " -o " . $paper_orientation;
    
    echo "Memory usage:" . memory_get_usage() . "\n";
    echo $conversion_command . "\n";    
    exec($conversion_command . " 2>&1", $output, $error);
    echo "Memory usage:" . memory_get_usage() . "\n";
    
    if ($error) {

        print_r($output);
        return FALSE;
        
    } else {      
        
        return TRUE;
        
    }
} 

/*
// Convert a text file to a pdf file.
// Returns TRUE/FALSE on success/failure.
*/
function textFile2PdfFile($in_text_filename, $out_filename, $paper_size="letter", $paper_orientation="portrait") {
    
    // Get the text file contents
    $handle = fopen($in_text_filename, "r");
    $contents = fread($handle, filesize($in_text_filename));
    fclose($handle);
    
    // Set up preformatted text if contents are not empty
    if ($contents == "") {
    
        return FALSE;
        
    } else {
    
        // Write as preformatted html file.
        // PLB removed html pre-formatting 01/09/09
        //$in_html = "<pre>" . $contents . "</pre>";
        $in_html = $contents;
        $html_filename = str_replace(".txt", ".html", $in_text_filename);
        $html_written = htmlString2HtmlFile($in_html, $html_filename);
        
    }
    
    // Return false if html file write fails
    if (!$html_written) {
    
        return FALSE;
        
    }
    
    // Wait a few seconds for file to be written
    sleep(2);
    
    // Execute dompdf.php to do the conversion
    $conversion_command = "./dompdf-0.5.1/dompdf.php ";
    $conversion_command .= $html_filename;
    $conversion_command .= " -f " . $out_filename;
    $conversion_command .= " -p " . $paper_size;
    $conversion_command .= " -o " . $paper_orientation;
    
    echo "Memory usage:" . memory_get_usage() . "\n";
    echo $conversion_command . "\n";    
    exec($conversion_command . " 2>&1", $output, $error);
    echo "Memory usage:" . memory_get_usage() . "\n";
    
    if ($error) {

        return FALSE;
        
    } else {      
        
        return TRUE;
        
    }    

}


/*
// Handle processing of the base application.
// Return a filled 2d array of merge file info on success.
// Return an empty array on failure.
*/
function handleBaseApplication ($uid, $application_id, $docpath, $single_merge_needed=0) {

    // Initialize an array for the base application file info.
    $base_application_merge_array = array(
                                    "merge_needed" => 0,
                                    "base_filename" => ""
                                    );
    
    // Set paths/filenames of the base pdf and merge files.
    $base_pdf_filename = $docpath . $application_id . "_application_base.pdf";
    $merge_filename = $docpath . $application_id . "_merged.pdf";

    // Capture the html from printApplicationHtml.php in the output buffer and save as a string.
    $application_html = "";
    exec("./getApplicationHtml.sh.php " . $uid, $output_array, $error);

    $application_html = implode("\n", $output_array);
    
    if ($error) {
    
        echo "Error getting base html.\n";
        return $base_application_merge_array;
    
    }
    
    // Return array if html capture fails.
    if ($application_html == "") {
        
        echo "Empty base html returned.\n";
        
		if ( file_exists($base_pdf_filename) ) {
        
            // Add the old file to the merge array.
            $base_application_merge_array['base_filename'] = $base_pdf_filename;
            
            // Indicate merge needed if there is no merge file.
            // Assume if both base and merge file are present, they were created in the same conversion.
            if ( !file_exists($merge_filename) ) {
            
                $base_application_merge_array['merge_needed'] = 1;
            
            }    
            
        }
        
        // Return empty base_filename if no base pdf exists.
        //print_r($base_application_merge_array);
        return $base_application_merge_array;
        
    }
    
    // Check whether the base has changed since the last conversion.
    $base_html_filename = $docpath . $application_id . "_application_base.html";
    $base_html_different = stringTextfileDifferent($application_html, $base_html_filename);    
    
    // Convert the base application if it has changed.
    if ( $base_html_different || $single_merge_needed ) {

		// Write the html string to an html file for use in next conversion.
		$base_html_written = htmlString2HtmlFile($application_html, $base_html_filename);
        // Wait a few seconds before moving on to pdf conversion
        sleep(2);
        
        // return array if html not written
        if (!$base_html_written) {
            
            echo "Error writing base html.\n";
            return $base_application_merge_array;
            
        } 
        
		
		// Convert the html file to a pdf file.        
		//$base_html_converted = htmlString2PdfFile($application_html, $base_pdf_filename, "11x17");
        $base_html_converted = htmlFile2PdfFile($base_html_filename, $base_pdf_filename, "11x17");
		
		// Fill the merge array if conversion is successful.
		if ($base_html_converted) {
		
			$base_application_merge_array['merge_needed'] = 1;
            $base_application_merge_array['base_filename'] = $base_pdf_filename;
		
		}

    } else {
    
        if ( file_exists($base_pdf_filename) ) {
        
            // Add the old file to the merge array.
            $base_application_merge_array['base_filename'] = $base_pdf_filename;
            
            // Indicate merge needed if there is no merge file.
            // Assume if both base and merge file are present, they were created in the same conversion.
            if ( !file_exists($merge_filename) ) {
            
                $base_application_merge_array['merge_needed'] = 1;
            
            }     
            
        } else {
        
            // Convert the html file to a pdf file.
            $base_html_converted = htmlFile2PdfFile($base_html_filename, $base_pdf_filename, "11x17");
            
            // Fill the merge array if conversion is successful.
            if ($base_html_converted) {
            
                $base_application_merge_array['merge_needed'] = 1;
                $base_application_merge_array['base_filename'] = $base_pdf_filename;
            
            }
            
        }       
        
    }
    
	// Return empty base_filename if no base pdf exists.
    //print_r($base_application_merge_array);
    return $base_application_merge_array;    
}


/*
// Convert image files.
// Filenames must have an absolute path.
*/
function convertImage($infile, $outfile) {
    
    $convert_command = "convert " . $infile . " " . $outfile;
    echo $convert_command;
    exec($convert_command . " 2>&1", $output, $error);
    
    $conversion_status = array('error' => $error, 'output' => $output);
    return $conversion_status;

}


/*
// Convert a doc or rtf file to pdf.
// Filenames must have an absolute path.
*/
function convertSingleDoc($infile, $outfile) {

    // Set the path to the python script that does the conversion  
    $python_converter =  "./DocumentConverter.py";

    // Set the path to the temp file to handle the open office output
    $tmpfile = "./ooffice_tmp";
    
    // Open an x-windows virtual frame buffer,
    // start open office as a service listening on port 8100,
    // do the conversion, and clean up
    exec("killall -u `whoami` Xvfb");
    exec("Xvfb :25 -screen 0 800x600x24  &");
    exec("killall -u `whoami` -q soffice");
    exec('ooffice -accept="socket,host=localhost,port=8100;urp;OpenOffice.ServiceManager" -norestore -nofirststartwizard-nologo -invisible -display :25 > ' . $tmpfile . ' &');
    exec('sleep 5');
    $convert_command = "python " . $python_converter . " " . $infile . " " . $outfile;
    echo $convert_command . "\n";
    exec($convert_command . " 2>&1", $output, $error);
    exec("killall -u `whoami` -q soffice");
    exec("killall -u `whoami` Xvfb");
    
    $conversion_status = array('error' => $error, 'output' => $output);
    return $conversion_status;
    
}


/*
// Handle processing of the datafiles.
// Return a 2d array of merge file info.
*/
function handleDatafiles($datafile_info_array, $docpath, $previous_merge_timestamp) {
    
    // Initialize the merge array .
    $datafile_merge_array = array(
                            "merge_needed" => 0,
                            "conversion_error" => array(),
                            "statement" => array(),
                            "resume" => array(),
                            "experience" => array(),
                            "transcript" => array(),
                            "grescore" => array(),
                            "gresubjectscore" => array(),
                            "toefliscore" => array(),
                            "recommendation" => array()
                            );
    
    foreach($datafile_info_array as $datafile) {
        
        // Set the absolute filepath for the infile.
        $filepath = $docpath . $datafile['filetype_name'] . "/";
        $infile = $filepath . $datafile['datafile'];
        
        // Don't bother if file doesn't exist.
        if ( file_exists($infile) ) {
            
			// Convert a new non-pdf file, if necessary. 
			switch($datafile['extension']) {
				
                case "txt":
                    $outfile = $filepath . str_replace("." . $datafile['extension'], ".pdf", $datafile['datafile']);
                    $conversion_error = 0;
                    $text_converted = textFile2PdfFile($infile, $outfile);
                    if (!$text_converted) {
                        $conversion_error = 1;
                    }
                    break;
                
                case "doc":
				case "rtf":
					$outfile = $filepath . str_replace("." . $datafile['extension'], ".pdf", $datafile['datafile']);
					$conversion_status = convertSingleDoc($infile, $outfile);
                    $conversion_error = $conversion_status['error'];
					break;
				
                case "gif":
				case "jpg":
					$outfile = $filepath . str_replace("." . $datafile['extension'], ".pdf", $datafile['datafile']);
					$conversion_status = convertImage($infile, $outfile);
                    $conversion_error = $conversion_status['error'];
					break;
				
                default:
					// File is already pdf; no conversion necessary; the outfile is simply the infile.
                    $conversion_error = 0;
                    $conversion_status = "";
					$outfile = $infile;
			}

            // Simplify reference to the datafile id.
            $datafile_id = $datafile['id'];
            
            // Add conversion error status of the datafile to the merge array.
            $datafile_merge_array['conversion_error'][$datafile_id] = $conversion_error;

            // Skip the datafile if there is a conversion error.
            if ($conversion_error) {

                echo "Conversion error for datafile :" . $datafile_id . "\n";
                print_r($conversion_status);
                continue;
                
            }
            
            // Proceed if no error.
            
            // Check to see if the file has been modified since last merge.
            $infile_timestamp = @strtotime($datafile['moddate']);
            
            // Indicate need to merge if the file has been modified more 
            // than a minute after last conversion.
            //if ( $infile_timestamp != $previous_merge_timestamp ) {
            if ( abs($infile_timestamp - $previous_merge_timestamp) > 60 ) {   
                $datafile_merge_array['merge_needed'] = 1;
            }
        
            // Fill the merge array as appropriate.
            switch($datafile['filetype_name']) {

                case "statement":
                    $datafile_merge_array['statement'][$datafile_id] = $outfile;
                    break;

                case "resume":
                    $datafile_merge_array['resume'][$datafile_id] = $outfile;
                    break;

                case "experience":
                    $datafile_merge_array['experience'][$datafile_id] = $outfile;
                    break;

                case "transcript":
                    $datafile_merge_array['transcript'][$datafile_id] = $outfile;
                    break;

                case "grescore":
                    $datafile_merge_array['grescore'][$datafile_id] = $outfile;
                    break;

                case "gresubjectscore":
                    $datafile_merge_array['gresubjectscore'][$datafile_id] = $outfile;
                    break;

                case "toefliscore":
                    $datafile_merge_array['toefliscore'][$datafile_id] = $outfile;
                    break;

                case "recommendation":
                    $datafile_merge_array['recommendation'][$datafile_id] = $outfile;
                    break;

                default:
                    // Do nothing.
            } 
        
        }
                
    }
    
    //print_r($datafile_merge_array);
    return $datafile_merge_array;
}


/*
// Handle processing of the recommendations that were submitted as "content".
// Return a 2d array of file info for the merge.
*/
function handleContentRecommendations($recommendation_array, $application_id, $docpath) {

    // initialize merge array
    $recommendation_content_merge_array = array(
                                          "merge_needed" => 0,
                                          "content_recommendation" => array(),
                                          );
    
    foreach($recommendation_array as $recommendation) {

        $recommendation_content_length = strlen($recommendation['content']);
        
        if ( ($recommendation['datafile'] == NULL) && ($recommendation_content_length > 100) ) {

            // Construct preformatted html from the db text
            $content_html = $recommendation['firstname'] . " " . $recommendation['lastname'] . "<br />";
            $content_html .= $recommendation['title'] . ", " . $recommendation['company'] . "<br />";
            $content_html .= $recommendation['email'] . "<br />";
            $content_html .= $recommendation['address_cur_tel'] . "<br />";
            $content_html .= html_entity_decode($recommendation['content']);
            
            // Set up a file path/name for the files
            $recommendation_id = $recommendation['recommendation_id'];
            $recommendation_path = $docpath . "recommendation/";
            $recommendation_filename_base = "recommendation_" . $application_id . "_" . $recommendation_id; 
            $recommendation_html_filename = $recommendation_path . $recommendation_filename_base . ".html";
            $recommendation_pdf_filename = $recommendation_path . $recommendation_filename_base . ".pdf";
            
            // Check whether the base has changed since the last conversion.
            $recommendation_html_different = stringTextfileDifferent($content_html, $recommendation_html_filename);
            
            if ($recommendation_html_different) {
            
                echo "Converting recommendation " . $recommendation_id . " content.\n";
                
                // Create recommendations directory if it doesn't already exist
                if ( !file_exists($recommendation_path) ) {
                
                    echo "Creating directory " . $recommendation_path . ".\n"; 
                    mkdir($recommendation_path, 0755);        
                    
                }
                
                // Write the html string to an html file for use in next conversion.
                $recommendation_html_written = htmlString2HtmlFile($content_html, $recommendation_html_filename);
                
                // Convert the html file to a pdf file.
                $recommendation_html_converted = htmlFile2PdfFile($recommendation_html_filename, $recommendation_pdf_filename);
                
                // Fill the merge array if conversion is successful.
                if ($recommendation_html_converted) {
        
                    echo "Recommendation content converted.\n";
                    
                    $recommendation_content_merge_array['merge_needed'] = 1;
                    $recommendation_content_merge_array['content_recommendation'][$recommendation_id] = $recommendation_pdf_filename; 
        
                } else {
                
                    echo "Recommendation content conversion failed.\n";
                    
                } 
        
            } else {
            
                if ( file_exists($recommendation_pdf_filename) ) {
        
                    // Add the old file to the merge array.
                    $recommendation_content_merge_array['content_recommendation'][$recommendation_id] = $recommendation_pdf_filename;    
            
                }    
            
            }
                      
        }
        
    }
    
    //print_r($recommendation_content_merge_array);
    return $recommendation_content_merge_array;
}


/*
// Merge the post-conversion pdfs eliminating files that cause errors.
*/
function mergePdfsStepwise($application_base_merge_array, $datafile_merge_array, 
                    $recommendation_content_merge_array, $application_id, $docpath) {
    
    // Set up the initial merge command
    $output_file =  $docpath . $application_id . "_merged.pdf";
    $base_merge_command = "gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -dColorImageResolution=72";
    $base_merge_command .= " -dGrayImageResolution=72 -dEmbedAllFonts=true -sOUTPUTFILE=" . $output_file . "  ";
                        
    // Go through the merge arrays to create the argument list for the merge command.
    // Assume the base application will merge without error.
    $merge_args = $application_base_merge_array['base_filename'] . " "; 
    
    foreach ($datafile_merge_array['statement'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['resume'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['experience'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['transcript'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['grescore'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['gresubjectscore'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['toefliscore'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($datafile_merge_array['recommendation'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    foreach ($recommendation_content_merge_array['content_recommendation'] as $merge_file) {
        $merge_args = handleMergeStep($base_merge_command, $merge_args, $merge_file);
    }
    
    // Do the final merge.
    $merge_command = $base_merge_command . $merge_args;
    echo $merge_command . "\n";
    exec($merge_command . " 2>&1", $output, $error);
    
    //print_r($output);
    //print_r($error);
    $merge_status = array('error' => $error, 'output' => $output);
    return $merge_status;    
}

// Return string of merge arguments for each step of the stepwise merge.
// Return new string if merge step is successful, old string if merge step fails.
function handleMergeStep($base_merge_command, $previous_merge_args, $new_file_arg) {

    $new_merge_args = $previous_merge_args . " " . $new_file_arg;
    $merge_command = $base_merge_command . $new_merge_args; 
    exec($merge_command . " 2>&1", $output, $error);
   
    if ($error) {
    
        echo "Merge failed for " . $new_file_arg . "\n";
        return $previous_merge_args;
    
    } else {
    
        return $new_merge_args;
        
    }
    
}


/*
// Merge the post-conversion pdfs all in one shot.
*/
function mergePdfs($application_base_merge_array, $datafile_merge_array, 
					$recommendation_content_merge_array, $application_id, $docpath) {
    
    // Go through the merge arrays to create the argument list for the merge command.
    $merge_args = $application_base_merge_array['base_filename'] . " "; 
    
    foreach ($datafile_merge_array['statement'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['resume'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['experience'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['transcript'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['grescore'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['gresubjectscore'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['toefliscore'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($datafile_merge_array['recommendation'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    foreach ($recommendation_content_merge_array['content_recommendation'] as $merge_file) {
        $merge_args .= $merge_file . " ";
    }
    
    // Do the merge.
    $output_file =  $docpath . $application_id . "_merged.pdf";
    $merge_command = "gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -dColorImageResolution=72";
    $merge_command .= " -dGrayImageResolution=72 -dEmbedAllFonts=true -sOUTPUTFILE=";
    $merge_command .= $output_file . "  ";
    $merge_command .= $merge_args;
    echo $merge_command . "\n";
    exec($merge_command . " 2>&1", $output, $error);
    
    //print_r($output);
    //print_r($error);
    $merge_status = array('error' => $error, 'output' => $output);
    return $merge_status;    
}

// Dale's function to convert pdf to text and update search table in db.
function buildSearchTextFromPDF ($guid, $pdf_filename, $docpath) {
    
    // Do the pdf to text conversion
    $conversion_command = "pdftotext " . $docpath . $pdf_filename;
    echo $conversion_command . "\n";
    exec($conversion_command);
    
    // Get the text file name 
    $filename = str_replace(".pdf", ".txt", $pdf_filename);
    
    $arr = explode("_",  $filename);
    //$myFile = $datafileroot."/".$guid."/".$filename;
    $myFile = $docpath . $filename;
    $theData = shell_exec('strings '.$myFile);
    $sql = "INSERT INTO searchText (application_id,guid,application_text) VALUES (".$arr[0].",'".$guid."','";
    $sql .= addslashes($theData)."') ON DUPLICATE KEY UPDATE application_text = '".addslashes($theData)."'";
    $result = mysql_query($sql); //or die(mysql_error().$sql);
    $error = mysql_error();
    if ($error == "") {
        echo "Search text built.\n";          
        return TRUE;
    } else {
        echo "Search text build failed.\n";
        return FALSE;
    }
}




// ########### NOT CURRENTLY USED ##################################
/* 
// convert a set of doc or rtf files to pdf
// file array is array of associative arrays of format:
// array('infile' => 'infile_name', 'outfile' => 'outfile_name')
// filenames must have absolute path
*/
function convertmultipleDocs($file_array) {

    // Set the path to the python script that does the conversion  
    $python_converter =  "./DocumentConverter.py";

    // Set the path to the temp file to handle the open office output
    $tmpfile = "./ooffice_tmp";
    
    // Open an x-windows virtual frame buffer,
    // start open office as a service listening on port 8100,
    // do the conversion, and clean up
    exec("killall -u `whoami` Xvfb");
    exec("Xvfb :25 -screen 0 800x600x24  &");
    exec("killall -u `whoami` -q soffice");
    exec('ooffice -accept="socket,host=localhost,port=8100;urp;OpenOffice.ServiceManager" -norestore -nofirststartwizard-nologo -invisible -display :25 > ' . $tmpfile . ' &');
    exec('sleep 2');
    
    foreach($file_array as $file) {
        
        $infile = $file['infile'];
        $outfile = $file['outfile'];
        exec("python " . $python_converter . " " . $infile . " " . $outfile);
    }
    
    exec("killall -u `whoami` -q soffice");
    exec("killall -u `whoami` Xvfb");
    
}

/*
// Convert an html string to a pdf file.
// Returns TRUE/FALSE on success/failure.
*/
function htmlString2PdfFile($in_html, $outfile, $paper_size="letter", $paper_orientaion="portrait") {
    
    // string variable to hold pdf output
    $filestring = "";
    
    // load html into dompdf and output to string
    echo "Memory usage:" . memory_get_usage() . "\n";
    $dompdf = new DOMPDF();
    $dompdf->load_html($in_html);
    $dompdf->set_paper($paper_size, $paper_orientaion);
    $dompdf->render();
    $filestring = $dompdf->output();
    
    // unset dompdf object just in case
    unset($dompdf);
    echo "Memory usage:" . memory_get_usage() . "\n";
    
    // write file if non-empty filestring
    if ($filestring == "") {

        return FALSE;
        
    } else {
        
        $fp = fopen($outfile, "w");
        
        if ($fp === FALSE) {
            
             return FALSE;
        
        }
        
        $write_status = fwrite($fp, $filestring);
            
        if ($write_status === FALSE) {
            
            fclose($fp);
            
            return FALSE;
                
        } 
            
        fclose($fp);        
        
        return TRUE;
        
    }
} 
  
?>
