<?php

/*
Class for returning the Robotics review lists as datagrids.
Must include class.review_list.php before including this class.
RI department_id = 3 
*/

class RiReviewList extends ReviewList
{
    
    function __construct($department_id=3) {
       parent::__construct($department_id);
       $this->department_id = $department_id;
    }
    
    function getRound1($reviewer_id, $groups="allGroups", $semiblind=1, $faculty_view=0, $searchString="") {
    
        // Get the data
        $results_array = $this->getRound1Data($reviewer_id, $groups, $semiblind, $searchString);
        
        // Bind the query results to the datagrid
        $dataGrid = new Structures_DataGrid();
        $dataGrid->bind($results_array ,array() , 'Array');
        
        // Define the datagrid columns
        $dataGrid->addColumn( new Structures_DataGrid_Column('Touched?', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Name', 'name', null, array(), null, 'printEditLink()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Ctzn', 'ctzn_code', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('M/F', 'm_f', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Programs', 'program', null, array(), null, 'printPrograms()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Interests', 'interest', null, array(), null, 'printInterests()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Undergrad', 'undergrad', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Special Cons?', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Lang Screen?', 'language_screen_requested', null, array("title" => "Has someone requested a language phone screen for this applicant?"), null, 'printLanguageScreen()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Tech Screen?', 'technical_screen_requested', null, array("title" => "Has someone requested a technical phone screen for this applicant?"), null, 'printTechnicalScreen()') );        
        $dataGrid->addColumn( new Structures_DataGrid_Column('Groups', 'groups', null, array(), null, 'printGroups()') );  
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Revr', 'revr', null, array(), null, 'printReviewers()') ); 
        if (!$semiblind) {
            
            $dataGrid->addColumn( new Structures_DataGrid_Column('Avg PhD Score', 'phd_avg_rank', null, array(), null) );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Avg MS Score', 'ms_avg_rank', null, array(), null) );
            //$dataGrid->addColumn( new Structures_DataGrid_Column('Round2', 'round2', null, array(), null, 'printVotes()') ); 
            $dataGrid->addColumn( new Structures_DataGrid_Column('PhD Scores', 'rank', null, array(), null, 'printPhdRank()') );
            $dataGrid->addColumn( new Structures_DataGrid_Column('MS Scores', 'ms_rank', null, array(), null, 'printMsRank()') );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Comments', 'comments', null, array(), null, 'printComments()') );            
        
        }
        if ($searchString != "") {
        
           $dataGrid->addColumn( new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) );
            
        }
        
        return $dataGrid; 
        
    }

    
    function getRound1Data($reviewer_id, $groups="allGroups", $semiblind=1, $searchString="") {
       
        // Set the search select and search join
        $this->parseSearchString($searchString); 
        
        // Check for limit to reviewer's groups
        $this->setGroupsJoin($reviewer_id, $groups);  
           
        // SEMIBLIND QUERY
        $semiblind_query = "select   
        application.id as application_id,
        if (reviewer.touch_count > 0, 1, 0) as touched,
		IFNULL(special_consideration.requested, 0) as special_consideration_requested,
		IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
		IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
		concat(users.lastname,', ', users.firstname) as name,
		countries.name as ctzn,
		gender as m_f,

		group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
		group_concat(distinct institutes.name separator '
') as undergrad ,
 
			concat(',',			
			group_concat( distinct 
			
			
					if( review.supplemental_review is null, review.reviewer_id,'')	
				
				  ),',') as revrid,
 group_concat( distinct 
						case lu_user_department.department_id when 3 then 
                        /*
                                  concat(substring(users2.firstname,1,1), 
                                         substring(users2.lastname,1,1),
                                         ' ',
                                         substring(if( review.supplemental_review is null, 
                                                       round(review.point,1),
                                                       ''),1,3 ),
                                         '/',
				         substring(if( review.supplemental_review is null, 
                                                       round(review.point2,1) ,
                                                       ''),1,3 )
                                        )
					      */
                          concat(substring(users2.firstname,1,1), 
                                         substring(users2.lastname,1,1) )
                            end 
                                                separator '|')  as revr ,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as groups,
  group_concat( distinct 
				   	case lu_user_department.department_id when 3 then 
					if((3=3 || 3=6),

 					  concat(substring(if( review.supplemental_review is null, review.point,''),1,3),
						 	  if( review.supplemental_review is null, concat('/', round(review.point2,1) ),'')
					  ),  
					  concat('',
						 if(review.round2 is null,'', case review.round2 when 1 then 'yes' when 0 then 'no' end), ': ', 
						 substring(if( review.supplemental_review is null, review.point,''),1,3)
					  )
							
					)
					
				        end  
                                        separator '|')  as round2,
 group_concat( distinct
					case review.fac_vote when 0 then
					case review.round when 1 then
						case lu_user_department.department_id when 3 then 
							concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
						end  end
						end
						separator '|') as rank ,
 group_concat( distinct 
						case review.fac_vote when 0 then
						case review.round when 1 then
							case lu_user_department.department_id when 3 then 
								concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
							end  end
						end
						separator '|') as ms_rank 
        
        /* PLB added search select 01/12/09 */
        " . $this->searchSelect . " 
                        
                        from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		inner join application on application.user_id = lu_users_usertypes.id
		inner join lu_application_programs on lu_application_programs.application_id = application.id
		inner join programs on programs.id = lu_application_programs.program_id
		inner join degree on degree.id = programs.degree_id
		inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
		left outer join institutes on institutes.id = usersinst.institute_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
		left outer join interest on interest.id = lu_application_interest.interest_id

		left outer join review on review.application_id = application.id 

		/*reviewers - round 1*/
		left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
		left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
		left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
		/*review groups - round 1*/
		left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
		
		/*application groups - round 1*/
		left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
		and lu_application_groups.round = 1 
		
		left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
		
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
		LEFT OUTER JOIN (
			SELECT application_id, 
			MAX(special_consideration) as requested
			FROM special_consideration 
			GROUP BY application_id            
		) as special_consideration ON application.id = special_consideration.application_id
		
		LEFT OUTER JOIN (
			SELECT application_id, 
			MAX(technical_screen) as technical_screen_requested,
			MAX(language_screen) as language_screen_requested
			FROM phone_screens
			GROUP BY application_id            
		) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this ->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
		
		   where application.submitted=1
		and review.supplemental_review IS NULL
		and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   )  group by  users.id      order BY users.lastname,users.firstname"; 

        // REGULAR QUERY 
        // PLB added first six fields in select 
        // and changed separators from <br> to |           
        $query = "select   
        application.id as application_id,
        if (reviewer.touch_count > 0, 1, 0) as touched,
		IFNULL(special_consideration.requested, 0) as special_consideration_requested,
		IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
		IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
		concat(users.lastname,', ', users.firstname) as name,
		countries.name as ctzn,
		gender as m_f,

		group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
		group_concat(distinct institutes.name separator '|') as undergrad ,
 
			concat(',',			
			group_concat( distinct 
			
			
					if( review.supplemental_review is null, review.reviewer_id,'')	
				
				  ),',') as revrid,
 group_concat( distinct 
					case lu_user_department.department_id when 3 then 
                                          concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1))
					end 
                                        separator '|')  as revr ,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as groups,
  group_concat( distinct 
				case lu_user_department.department_id when 3 then 
				if((3=3 || 3=6),

					concat(substring(
						if( review.supplemental_review is null, round(review.point,1),''),1,4),
						if( review.supplemental_review is null, concat('/', round(review.point2,1) ),'')
					),  
				  	concat('',
						if(review.round2 is null,'',case review.round2 when 1 then 'yes' when 0 then 'no' end), ': ', 
						substring(if( review.supplemental_review is null, review.point,''),1,3)
					)
						
				)
					
				end  
                                separator '|')  as round2,
 group_concat( distinct
					case review.fac_vote when 0 then
					case review.round when 1 then
						case lu_user_department.department_id when 3 then
                            /* PLB changed not to display null score as 0 */
                            /*
							concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                            */
                            if ( (review.point is null) || (review.point = 0), '',
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                            )
                        end  end
						end
						separator '|') as rank ,
 group_concat( distinct 
						case review.fac_vote when 0 then
						case review.round when 1 then
							case lu_user_department.department_id when 3 then
                            /* PLB changed not to display null score as 0 */ 
                            /* 
							concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            */
                            if ( (review.point2 is null) || (review.point2 = 0), '',
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )    
                            ) 
                            end  end
						end
						separator '|') as ms_rank ,
 
  /* PLB added phd_avg_rank, ms_avg_rank  */
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank,                       
 
 group_concat( distinct 
					if( 0 = 3,
						
						case review.fac_vote when 1 then
							case review.round when 1 then
								case lu_user_department.department_id when 3 then 
									review.comments  
								end
							
							end
						end
					
					,
					
					case review.fac_vote when 0 then
						case review.round when 1 then
							case lu_user_department.department_id when 3 then 
								review.comments  
							end
							
						end
					
					end
					)
					separator '|') as comments 
                    
                    /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
                    
                    from lu_users_usertypes
		inner join users on users.id = lu_users_usertypes.user_id
		left outer join users_info on users_info.user_id = lu_users_usertypes.id
		left outer join countries on countries.id = users_info.cit_country
		inner join application on application.user_id = lu_users_usertypes.id
		inner join lu_application_programs on lu_application_programs.application_id = application.id
		inner join programs on programs.id = lu_application_programs.program_id
		inner join degree on degree.id = programs.degree_id
		inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
		left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
		left outer join institutes on institutes.id = usersinst.institute_id
		left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
		left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
		left outer join interest on interest.id = lu_application_interest.interest_id

		left outer join review on review.application_id = application.id 

		/*reviewers - round 1*/
		left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
		left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
		left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
		/*review groups - round 1*/
		left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
		
		/*application groups - round 1*/
		left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
		and lu_application_groups.round = 1 
		
		left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
		
        /* PLB added three following joins and groupsJoin */
        
		LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
		LEFT OUTER JOIN (
			SELECT application_id, 
			MAX(special_consideration) as requested
			FROM special_consideration 
			GROUP BY application_id            
		) as special_consideration ON application.id = special_consideration.application_id
		
		LEFT OUTER JOIN (
			SELECT application_id, 
			MAX(technical_screen) as technical_screen_requested,
			MAX(language_screen) as language_screen_requested
			FROM phone_screens
			GROUP BY application_id            
		) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
		
		
		   where application.submitted=1
		and review.supplemental_review IS NULL
		and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   )  group by  users.id      order BY users.lastname,users.firstname";
        
        if ($semiblind) {
        
            $results_array = $this->handleSelectQuery($semiblind_query);
            
        } else {
        
            $results_array = $this->handleSelectQuery($query);
            
        }
         
        return $results_array;     
        
    }
 

    function getRound2($reviewer_id, $groups="allGroups", $semiblind=1, $faculty_view=0, $searchString="") {
    
        // Get the data
        $results_array = $this->getRound2Data($reviewer_id, $groups, $semiblind, $searchString);
        
        // Bind the query results to the datagrid
        $dataGrid = new Structures_DataGrid();
        $dataGrid->bind($results_array ,array() , 'Array');
        
        // Define the datagrid columns
        $dataGrid->addColumn( new Structures_DataGrid_Column('Touched?', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Name', 'name', null, array(), null, 'printEditLink()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Ctzn', 'ctzn_code', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('M/F', 'm_f', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Programs', 'program', null, array(), null, 'printPrograms()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Interests', 'interest', null, array(), null, 'printInterests()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Undergrad', 'undergrad', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Special Cons?', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Lang Screen?', 'language_screen_requested', null, array("title" => "Has someone requested a language phone screen for this applicant?"), null, 'printLanguageScreen()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Tech Screen?', 'technical_screen_requested', null, array("title" => "Has someone requested a technical phone screen for this applicant?"), null, 'printTechnicalScreen()') );        
        $dataGrid->addColumn( new Structures_DataGrid_Column('Groups', 'revgrp2', null, array(), null, 'printGroups2()') );  
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Revr', 'revr', null, array(), null, 'printReviewers()') );  
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Round2', 'round2', null, array(), null, 'printVotes()') );
        if (!$semiblind) {
            
            $dataGrid->addColumn( new Structures_DataGrid_Column('Avg PhD Score', 'phd_avg_rank', null, array(), null) );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Avg MS Score', 'ms_avg_rank', null, array(), null) );
            $dataGrid->addColumn( new Structures_DataGrid_Column('PhD Scores', 'rank', null, array(), null, 'printPhdRank()') );
            $dataGrid->addColumn( new Structures_DataGrid_Column('MS Scores', 'ms_rank', null, array(), null, 'printMsRank()') );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Comments', 'comments', null, array(), null, 'printComments()') );            
        
        }
        if ($searchString != "") {
        
           $dataGrid->addColumn( new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) );
            
        }
        
        return $dataGrid; 
        
    }


    function getRound2Data($reviewer_id, $groups="allGroups", $semiblind=1, $searchString="") {
       
        // Set the search select and search join
        $this->parseSearchString($searchString); 
        
        // Check for limit to reviewer's groups
        $this->setGroupsJoin($reviewer_id, $groups);  
           
        // SEMIBLIND QUERY
        $semiblind_query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                        end
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        end
                        separator '|') as ms_rank
                         
                         /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
 
 from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
                /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
        
               where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname"; 

        
        // REGULAR QUERY           
        $query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2, 
                case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    /* PLB added test not to display null scores as 0 */
                    if( (review.point is null) || (review.point = 0) , '', /* begin PLB if */
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                        
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                        ) /* end PLB if */ 
                        end /* end review.fac_vote case */
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        /* PLB added test not to display null scores as 0 */
                        if( (review.point2 is null) || (review.point2 = 0), '', /* begin PLB if */
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        
                        ) /* end PLB if */
                        end  /* end review.fac_vote case */
                        separator '|') as ms_rank ,
 
  /* PLB added phd_avg_rank, ms_avg_rank  */

  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank,                       
                        
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when 3 then 
                                            review.comments
                                        end
                                    
                        end
                    
                    end
                    )
                    separator '|') as comments 
                    
                    /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
                    
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
        
               where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname";
        
        if ($semiblind) {
        
            $results_array = $this->handleSelectQuery($semiblind_query);
            
        } else {
        
            $results_array = $this->handleSelectQuery($query);
            
        }
         
        return $results_array;     
        
    }

    
    function getFinal($reviewer_id, $groups="allGroups", $semiblind=1, $faculty_view=0, $searchString="") {
    
        // Get the data
        $results_array = $this->getRound3Data($reviewer_id, $groups, $semiblind, $searchString);
        
        // Bind the query results to the datagrid
        $dataGrid = new Structures_DataGrid();
        $dataGrid->bind($results_array ,array() , 'Array');
        
        // Define the datagrid columns
        $dataGrid->addColumn( new Structures_DataGrid_Column('Touched?', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Name', 'name', null, array(), null, 'printEditLink()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Ctzn', 'ctzn_code', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('M/F', 'm_f', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Programs', 'program', null, array(), null, 'printPrograms()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Interests', 'interest', null, array(), null, 'printInterests()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Undergrad', 'undergrad', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Special Cons?', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Lang Screen?', 'language_screen_requested', null, array("title" => "Has someone requested a language phone screen for this applicant?"), null, 'printLanguageScreen()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Tech Screen?', 'technical_screen_requested', null, array("title" => "Has someone requested a technical phone screen for this applicant?"), null, 'printTechnicalScreen()') );        
        $dataGrid->addColumn( new Structures_DataGrid_Column('Groups', 'revgrp2', null, array(), null, 'printGroups2()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Avg PhD Score', 'phd_avg_rank', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Avg MS Score', 'ms_avg_rank', null, array(), null) );
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Revr', 'revr', null, array(), null, 'printReviewers()') );  
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Round2', 'round2', null, array(), null, 'printVotes()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('PhD Scores', 'rank', null, array(), null, 'printPhdRank()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('MS Scores', 'ms_rank', null, array(), null, 'printMsRank()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Decision', 'decision', null, array(), null, 'printDecision()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Admit To', 'admit_to', null, array(), null, 'printAdmitTo()') );  
        if (!$semiblind) {

            // $dataGrid->addColumn( new Structures_DataGrid_Column('Comments', 'comments', null, array(), null, 'printComments()') );            
        
        }
        if ($searchString != "") {
        
           $dataGrid->addColumn( new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) );
            
        }
        
        return $dataGrid; 
        
    }

    function getRound3Data($reviewer_id, $groups="allGroups", $semiblind=1, $searchString="") {
       
        // Set the search select and search join
        $this->parseSearchString($searchString); 
        
        // Check for limit to reviewer's groups
        $this->setGroupsJoin($reviewer_id, $groups);  
           
        // SEMIBLIND QUERY
        $semiblind_query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat(distinct 
            case lu_programs_departments.department_id when  3 then
                lu_application_programs.decision
            end
            separator '|'
            ) as decision  ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then 
                
                    case lu_application_programs.admission_status when 1 then 
                        concat(if(degree.name is not null, degree.name,'') , ' ', if(programs.linkword is not null, programs.linkword,'') , ' ',if(fieldsofstudy.name is not null, fieldsofstudy.name,'')  , '(wait)') 
                    when 2 then
                        concat(degree.name, ' ', if(programs.linkword is not null, programs.linkword,''), ' ', fieldsofstudy.name)
                    end
                end  separator '|') as admit_to,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                        end
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        end
                        separator '|') as ms_rank, 

 /* PLB added phd_avg_rank, ms_avg_rank  */
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank                       
 
    /* PLB added search select 01/12/09 */
    " . $this->searchSelect . "  
                        
 from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
                /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
               
        
               where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname";

        
        // REGULAR QUERY           
        $query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote,
group_concat(distinct if(revgroup.department_id = 3 , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = 3 , revgroup2.name , '')  separator '|') as revgrp2,
 group_concat(distinct 
            case lu_programs_departments.department_id when  3 then
                lu_application_programs.decision
            end
            separator '|'
            ) as decision  ,
group_concat( distinct 
                case lu_user_department.department_id when 3 then 
                
                    case lu_application_programs.admission_status when 1 then 
                        concat(if(degree.name is not null, degree.name,'') , ' ', if(programs.linkword is not null, programs.linkword,'') , ' ',if(fieldsofstudy.name is not null, fieldsofstudy.name,'')  , '(wait)') 
                    when 2 then
                        concat(degree.name, ' ', if(programs.linkword is not null, programs.linkword,''), ' ', fieldsofstudy.name)
                    end
                end  separator '|') as admit_to,
 group_concat( distinct
                    case review.fac_vote when 0 then
                    /* PLB added test not to display null scores as 0 */
                    if( (review.point is null) || (review.point = 0), '', /* begin PLB if */
                    case review.round when 1 then
                        case lu_user_department.department_id when 3 then 
                            concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round( if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)   )
                        end when 2 then
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point is null,'',if(review.point=0,'',review.point ))   ,2)  )
                            end  end
                    ) /* end PLB if */
                        end  /* end review.fac_vote case */
                        separator '|') as rank ,
 group_concat( distinct 
                        case review.fac_vote when 0 then
                        /* PLB added test not to display null scores as 0 */
                        if( (review.point2 is null) || (review.point2 = 0), '', /* begin PLB if */
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)   )
                            end when 2 then    
                            case lu_user_department2.department_id when 3 then 
                                concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1),':',round(if(review.point2 is null,'',if(review.point2=0,'',review.point2 ))   ,2)  )
                            end  end
                        ) /* end PLB if */    
                        end /* end review.fac_vote case */
                        separator '|') as ms_rank ,
                        
  /* PLB added phd_avg_rank, ms_avg_rank  */
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point end  ),2 ),'' ) as phd_avg_rank,
  if( review.round = 1 ,round( avg(  case lu_user_department.department_id when 3 then review.point2 end  ),2 ),'' ) as ms_avg_rank,                       
                        
 
group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when 3 then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when 3 then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when 3 then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when 3 then 
                                            review.comments
                                        end
                                    
                        end
                    
                    end
                    )
                    separator '|') as comments 
                    
                    /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
                    
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
               
        /* PLB added three following joins and groupsJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
               
               where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=3  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = 3   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname";

        
        if ($semiblind) {
        
            $results_array = $this->handleSelectQuery($semiblind_query);
            
        } else {
        
            $results_array = $this->handleSelectQuery($query);
            
        }
         
        return $results_array;     
     
    }    
    

} // end class   


?>
