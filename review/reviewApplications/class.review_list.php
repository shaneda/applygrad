<?php

/*
Base class for retrieving application data for the review process and returning a datagrid object.
Must include Structures/DataGrid.php and review_classes/class.db_applyweb.php before including this class.
Methods are based on CSD query/display. 
*/

class ReviewList extends DB_Applyweb 
{
    
    protected $department_id;
    protected $groupsJoin;
    protected $searchSelect;
    protected $searchJoin;

    function __construct($department_id) {
       parent::__construct();
       $this->department_id = $department_id;
    } 
    
    function getRound1($reviewer_id, $groups="allGroups", $semiblind=1, $faculty_view=0, $searchString="") {
    
        // Get the data
        $this->setMaxGroupConcat();
        $results_array = $this->getRound1Data($reviewer_id, $groups, $semiblind, $searchString);
        $results_array = $this->getAvgRank($results_array); 
        
        // Bind the query results to the datagrid
        $dataGrid = new Structures_DataGrid();
        $dataGrid->bind($results_array ,array() , 'Array');
        
        // Define the datagrid columns
        $dataGrid->addColumn( new Structures_DataGrid_Column('Touched?', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Name', 'name', null, array(), null, 'printEditLink()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Ctzn', 'ctzn_code', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('M/F', 'm_f', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Programs', 'program', null, array(), null, 'printPrograms()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Interests', 'interest', null, array(), null, 'printInterests()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Undergrad', 'undergrad', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Special Cons?', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Lang Screen?', 'language_screen_requested', null, array("title" => "Has someone requested a language phone screen for this applicant?"), null, 'printLanguageScreen()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Tech Screen?', 'technical_screen_requested', null, array("title" => "Has someone requested a technical phone screen for this applicant?"), null, 'printTechnicalScreen()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Groups', 'groups', null, array(), null, 'printGroups()') );  
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Revr', 'revr', null, array(), null, 'printReviewers()') );  
        if (!$semiblind) {
        
            $dataGrid->addColumn( new Structures_DataGrid_Column('Avg Rank', 'avg_rank', null, array(), null) );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Round 2?', 'round2', null, array(), null, 'printVotes()') );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Rank/ Conf', 'rank', null, array(), null, 'printReveiwerRanks()') );
            $dataGrid->addColumn( new Structures_DataGrid_Column('Comments', 'comments', null, array(), null, 'printComments()') );
            
        }       
        if ($searchString != "") {
        
           $dataGrid->addColumn( new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) ); 
        }
        
        return $dataGrid; 
        
    }

    
    function getRound1Data($reviewer_id, $groups="allGroups", $semiblind=1, $searchString="") {
       
        // Set the search select and search join
        $this->parseSearchString($searchString); 
        
        // Check for limit to reviewer's groups
        $this->setGroupsJoin($reviewer_id, $groups);
           
        // SEMIBLIND QUERY
        // PLB added first six fields in select 
        // and changed separators from <br> to |
        $semiblind_query = "select
                    application.id as application_id, 
                    if (reviewer.touch_count > 0, 1, 0) as touched,
                    IFNULL(special_consideration.requested, 0) as special_consideration_requested,
                    IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
                    IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
                    countries.iso_code as ctzn_code,
                    lu_users_usertypes.id,
                    concat(users.lastname,', ', users.firstname) as name,
                    countries.name as ctzn,                   
                    gender as m_f,
                    group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
                    group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')') order by lu_application_interest.choice  separator '|') as interest,
                    group_concat(distinct institutes.name separator '|') as undergrad,
                    concat(',', group_concat( distinct if( review.supplemental_review is null, review.reviewer_id,'') ), ',') as revrid,
                    group_concat( distinct 
                        case lu_user_department.department_id 
                        when " . $this->department_id . " then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1))
                        end 
                    separator '|') as revr,
                    group_concat(distinct if(revgroup.department_id = " . $this->department_id . " , revgroup.name , '')  separator '|') as groups,
                    
                    group_concat( distinct  
                        case lu_user_department.department_id 
                        when " . $this->department_id . " then 
                        if((" . $this->department_id . "=3 || " . $this->department_id . "=6),
                        concat(substring(
                            if( review.supplemental_review is null, round(review.point,1),''),1,4),
                            if( review.supplemental_review is null, concat('/', round(review.point2,1) ),'')
                            ),  
                        /* 
                        PLB removed scores from round2 
                        - also removed round 2 from semiblind in datagrid rendering
                        */
                        /* 
                        concat('',
                        if(review.round2 is null,'',case review.round2 when 1 then 'yes' when 0 then 'no' end), ': ', 
                        substring(if( review.supplemental_review is null, review.point,''),1,3)
                        )
                         */  
                        if(review.round2 is null,'',case review.round2 when 1 then 'yes' when 0 then 'no' end)
                         
                        )
                                            
                        end  
                    separator '|')  as round2,
                    group_concat(distinct 
                        case lu_user_department.department_id 
                        when " . $this->department_id . " then
                        concat(
                        substring(if( review.supplemental_review is null, review.point,''),1,3)
                        ,'/',
                        substring(if( review.supplemental_review is null, round(review.point2,1) ,''),1,3 )
                        )
                        end
                    separator '|') as rank
                    
                    /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
                    
                    FROM lu_users_usertypes
                    inner join users on users.id = lu_users_usertypes.user_id
                    left outer join users_info on users_info.user_id = lu_users_usertypes.id
                    left outer join countries on countries.id = users_info.cit_country
                    inner join application on application.user_id = lu_users_usertypes.id
                    inner join lu_application_programs on lu_application_programs.application_id = application.id
                    inner join programs on programs.id = lu_application_programs.program_id
                    inner join degree on degree.id = programs.degree_id
                    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
                    left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
                    left outer join institutes on institutes.id = usersinst.institute_id
                    left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
                    left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
                    left outer join interest on interest.id = lu_application_interest.interest_id
                    left outer join review on review.application_id = application.id 
                    /*reviewers - round 1*/
                    left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
                    left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
                    left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
                    /*review groups - round 1*/
                    left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1        
                    /*application groups - round 1*/
                    left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
                    and lu_application_groups.round = 1                     
                    left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id

                    /* PLB added three following joins and groupJoin */
                    
                    LEFT OUTER JOIN (
                        SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
                        FROM review
                        GROUP BY application_id, reviewer_id, department_id
                        HAVING reviewer_id = " . $reviewer_id . "
                        AND department_id = " . $this->department_id . "
                    ) AS reviewer on reviewer.application_id = application.id
                    
                    LEFT OUTER JOIN (
                        SELECT application_id, 
                        MAX(special_consideration) as requested
                        FROM special_consideration 
                        GROUP BY application_id            
                    ) as special_consideration ON application.id = special_consideration.application_id
                    
                    LEFT OUTER JOIN (
                        SELECT application_id, 
                        MAX(technical_screen) as technical_screen_requested,
                        MAX(language_screen) as language_screen_requested
                        FROM phone_screens
                        GROUP BY application_id            
                    ) as phone_screens ON application.id = phone_screens.application_id
                    
                    
                    " . $this->groupsJoin . "
                    
                    /* PLB added search join 01/12/09 */
                    " . $this->searchJoin . "
                    
                    where application.submitted=1
                    and review.supplemental_review IS NULL
                    and lu_programs_departments.department_id=" . $this->department_id . " 
                    and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = " . $this->department_id . ")
                    group by  users.id  
                    order BY users.lastname,users.firstname"; 

        // REGULAR QUERY 
        // PLB added first six fields in select 
        // and changed separators from <br> to |            
        $query = "select
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,       
        gender as m_f,
        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid,
 group_concat( distinct 
                        case lu_user_department.department_id when " . $this->department_id . " then 
                                  concat(substring(users2.firstname,1,1), 
                                         substring(users2.lastname,1,1)
                                        )
                            end 
                                                separator '|')  as revr ,
group_concat(distinct if(revgroup.department_id = " . $this->department_id . " , revgroup.name , '')  separator '|') as groups,
  group_concat( distinct 
                       case lu_user_department.department_id when " . $this->department_id . " then 
                    if((" . $this->department_id . "=3 || " . $this->department_id . "=6),

                       concat(substring(if( review.supplemental_review is null, review.point,''),1,3),
                               if( review.supplemental_review is null, concat('/', round(review.point2,1) ),'')
                      ),  
                      
                      /* PLB changed to have initials, then vote for round 2*/
                      /*
                      concat('',
                         if(review.round2 is null,'', case review.round2 when 1 then 'yes' when 0 then 'no' end), ': ', 
                         substring(if( review.supplemental_review is null, review.point,''),1,3)
                      )
                      */
                      concat( concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)), ': ',
                         if(review.round2 is null,'', case review.round2 when 1 then 'yes' when 0 then 'no' end) 
                      )      
                    )
                    
                        end  
                                        separator '|')  as round2,
                                        
                    group_concat(distinct 
                        case lu_user_department.department_id 
                        when " . $this->department_id . " then
                        /* PLB added initials at beginning of rank */
                        /* PLB changed point2 to point_certainty */
                        concat( concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)), ': ', 
                        /* concat( */
                        substring(if( review.supplemental_review is null, review.point,''),1,3)
                        ,'/',
                        substring(if( review.supplemental_review is null, round(review.point_certainty,1) ,''),1,3 )
                        )
                        end
                    separator '|') as rank,                    
 
 /* PLB replaced w/ getAvgRank method 01/20/09 */
 /*                   
 if( review.round = 1 ,round( avg(  case lu_user_department.department_id when " . $this->department_id . " then review.point end  ),2 ),'' ) as avg_rank ,
 */
 
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    review.comments  
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when " . $this->department_id . " then 
                                review.comments  
                            end
                            
                        end
                    
                    end
                    )
                    separator '|') as comments
                    
                    /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
                      
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
        
        /* PLB added three following joins and groupJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        
        " . $this->groupsJoin . " 
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . "
        
        where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=" . $this->department_id . "  
        and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = " . $this->department_id . "   )  
        group by  users.id      order BY users.lastname,users.firstname";
        
        if ($semiblind) {
        
            $results_array = $this->handleSelectQuery($semiblind_query);
            
        } else {
        
            $results_array = $this->handleSelectQuery($query);
            
        }
         
        return $results_array;     
        
    }
  
        
    function getRound2($reviewer_id, $groups="allGroups", $semiblind=1, $faculty_view=0, $searchString="") {
    
        // Get the data
        $this->setMaxGroupConcat();
        $results_array = $this->getRound2Data($reviewer_id, $groups, $semiblind, $searchString);
        $results_array = $this->getFacultyOfInterest($results_array);
        $results_array = $this->getAvgRank($results_array);
        
        // Bind the query results to the datagrid
        $dataGrid = new Structures_DataGrid();
        $dataGrid->bind($results_array ,array() , 'Array');
        
        // Define the datagrid columns
        // PLB changed column label 01/20/09
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Touched?', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Revd', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Name', 'name', null, array(), null, 'printEditLink()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Ctzn', 'ctzn_code', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('M/F', 'm_f', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Programs', 'program', null, array(), null, 'printPrograms()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Interests', 'interest', null, array(), null, 'printInterests()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Undergrad', 'undergrad', null, array(), null) );
        
        // In faculty view, show faculty of interest, and hide special consideration and phone screens.
        if ($faculty_view) {
        
            $dataGrid->addColumn( new Structures_DataGrid_Column('Possible Advisors', 'possible_advisor_names', null, array(), null) );
            
        } else {
            // PLB changed column label, phone screen display 01/20/09
            $dataGrid->addColumn( new Structures_DataGrid_Column('Sp', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
            //$dataGrid->addColumn( new Structures_DataGrid_Column('Special Cons?', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
            //$dataGrid->addColumn( new Structures_DataGrid_Column('Lang Screen?', 'language_screen_requested', null, array("title" => "Has someone requested a language phone screen for this applicant?"), null, 'printLanguageScreen()') );
            //$dataGrid->addColumn( new Structures_DataGrid_Column('Tech Screen?', 'technical_screen_requested', null, array("title" => "Has someone requested a technical phone screen for this applicant?"), null, 'printTechnicalScreen()') );                   
        }
  
        $dataGrid->addColumn( new Structures_DataGrid_Column('Groups', 'revgrp2', null, array(), null, 'printGroups2()') );
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Revr', 'revr', null, array(), null, 'printReviewers()') );
        if (!$semiblind) {
        
            // PLB added test to show pertinent_info only for faculty
            if ($faculty_view) {
                
                $dataGrid->addColumn( new Structures_DataGrid_Column('Comments', 'pertinent_info', null, array(), null, 'printPertinentInfo()') );
                
            } else {
                
                $dataGrid->addColumn( new Structures_DataGrid_Column('Avg Rank', 'avg_rank', null, array(), null) );
                $dataGrid->addColumn( new Structures_DataGrid_Column('Comments', 'comments', null, array(), null, 'printCommentsPertinent()') );
            
            }
        }
        if ($searchString != "") {
        
           $dataGrid->addColumn( new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) ); 
        }
        
        return $dataGrid; 
        
    }


    function getRound2Data($reviewer_id, $groups="allGroups", $semiblind=1, $searchString="") {
       
        // Set the search select and search join
        $this->parseSearchString($searchString); 
        
        // Check for limit to reviewer's groups
        $this->setGroupsJoin($reviewer_id, $groups);
        
        // SEMIBLIND QUERY 
        // PLB added first six fields in select 
        // and changed separators from <br> to |
        // PLB added 7th field (possible advisors) 01/12/09
        $semiblind_query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        /*possible_advisors.possible_advisor_names,*/
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when " . $this->department_id . " then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote
,
group_concat(distinct if(revgroup.department_id = " . $this->department_id . " , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = " . $this->department_id . " , revgroup2.name , '')  separator '|') as revgrp2 

    /* PLB added search select 01/12/09 */
    " . $this->searchSelect . " 

    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added possible advisors 01/12/09 */
        /*
        LEFT OUTER JOIN (
            SELECT application.id as application_id, GROUP_CONCAT( names.name separator ', ') AS possible_advisor_names
                FROM application
                INNER JOIN (
                (SELECT application_id, name
                FROM lu_application_advisor,
                lu_programs_departments
                WHERE lu_application_advisor.program_id = lu_programs_departments.program_id
                AND lu_programs_departments.department_id = " . $this->department_id . " 
                AND name != ''
                AND name IS NOT NULL)
                UNION 
                (SELECT application_id, concat( users.firstname, ' ', users.lastname ) AS name
                FROM lu_application_advisor
                INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = lu_application_advisor.advisor_user_id
                INNER JOIN users ON users.id = lu_users_usertypes.user_id
                INNER JOIN lu_programs_departments ON lu_application_advisor.program_id = lu_programs_departments.program_id
                WHERE lu_programs_departments.department_id = " . $this->department_id . "  
                ) 
                ) as names on names.application_id = application.id
                GROUP BY application.id
        ) AS possible_advisors on possible_advisors.application_id = application.id
        */
        /* PLB added three following joins and groupJoin */
        
        /* pworona added possible advisors 01/14/09 */
        /*
  		LEFT OUTER JOIN (
			SELECT ap.application_id, ap.program_id, 
				GROUP_CONCAT( aa.name separator ', ') AS possible_advisor_names
			FROM lu_application_programs ap
			LEFT JOIN lu_application_advisor aa
			ON ap.application_id = aa.application_id and ap.program_id = aa.program_id
			WHERE aa.name IS NOT NULL
			GROUP BY ap.application_id
        ) AS possible_advisors on possible_advisors.application_id = application.id
        */
        /* pworona added possible advisors 01/14/09 */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . " 
        
        where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=" . $this->department_id . "  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = " . $this->department_id . "   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname"; 

        // REGULAR QUERY 
        // PLB added first six fields in select 
        // and changed separators from <br> to |
        // PLB added 7th field (possible advisors) 01/12/09            
        $query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        /*possible_advisors.possible_advisor_names,*/
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,       
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when " . $this->department_id . " then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote
,
group_concat(distinct if(revgroup.department_id = " . $this->department_id . " , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = " . $this->department_id . " , revgroup2.name , '')  separator '|') as revgrp2
,

/* PLB replaced w/ getAvgRank method 01/20/09 */ 
/*
 if( review.round = 1 ,round( avg(  case lu_user_department.department_id when " . $this->department_id . " then review.point end  ),2 ),'' ) as avg_rank ,
*/

 
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when " . $this->department_id . " then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when " . $this->department_id . " then 
                                            review.comments
                                        end
                                    
                        end
                    
                    /* PLB added case to show faculty comments in round 2 */
                    /* PLB changed to separate field 01/12/09
                    when 1 then
                        case review.round when 2 then
                            case lu_user_department2.department_id when " . $this->department_id . " then 
                                review.pertinent_info
                            end    
                        end
                    */
                    
                    end  /* end review.fac_vote case */
                    )
                    separator '|') as comments,
                    
                    /* PLB added pertinent_info field 01/12/09 */
                    group_concat( distinct
                        case review.fac_vote when 1 then
                            case review.round when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    review.pertinent_info
                                end    
                            end
                        end
                     separator '|') as pertinent_info 
        
        /* PLB added search select 01/12/09 */
        " . $this->searchSelect . " 
                     
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
       
        /* PLB added possible advisors 01/12/09 */
        /*
        LEFT OUTER JOIN (
            SELECT application.id as application_id, GROUP_CONCAT( names.name separator ', ') AS possible_advisor_names
                FROM application
                INNER JOIN (
                (SELECT application_id, name
                FROM lu_application_advisor,
                lu_programs_departments
                WHERE lu_application_advisor.program_id = lu_programs_departments.program_id
                AND lu_programs_departments.department_id = " . $this->department_id . "
                AND name != ''
                AND name IS NOT NULL)
                UNION 
                (SELECT application_id, concat( users.firstname, ' ', users.lastname ) AS name
                FROM lu_application_advisor
                INNER JOIN lu_users_usertypes ON lu_users_usertypes.id = lu_application_advisor.advisor_user_id
                INNER JOIN users ON users.id = lu_users_usertypes.user_id
                INNER JOIN lu_programs_departments ON lu_application_advisor.program_id = lu_programs_departments.program_id
                WHERE lu_programs_departments.department_id = " . $this->department_id . " ) 
                ) as names on names.application_id = application.id
                GROUP BY application.id
        ) AS possible_advisors on possible_advisors.application_id = application.id
        */
        /* PLB added three following joins and groupJoin */
        
        /* pworona added possible advisors 01/14/09 */
        /*
  		LEFT OUTER JOIN (
			SELECT ap.application_id, ap.program_id, 
				GROUP_CONCAT( aa.name separator ', ') AS possible_advisor_names
			FROM lu_application_programs ap
			LEFT JOIN lu_application_advisor aa
			ON ap.application_id = aa.application_id and ap.program_id = aa.program_id
			WHERE aa.name IS NOT NULL
			GROUP BY ap.application_id
        ) AS possible_advisors on possible_advisors.application_id = application.id
        */
        /* pworona added possible advisors 01/14/09 */
                
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . "  
        
               where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=" . $this->department_id . "  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = " . $this->department_id . "   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname";
        
        
        if ($semiblind) {
        
            $results_array = $this->handleSelectQuery($semiblind_query);
            
        } else {
        
            $results_array = $this->handleSelectQuery($query);
            
        }
         
        return $results_array;     
        
    }

 
    function getFinal($reviewer_id, $groups="allGroups", $semiblind=1, $faculty_view=0, $searchString="") {
    
        // Get the data
        $this->setMaxGroupConcat();
        $results_array = $this->getRound3Data($reviewer_id, $groups, $semiblind, $searchString);
        $results_array = $this->getAvgRank($results_array);
        
        // Bind the query results to the datagrid
        $dataGrid = new Structures_DataGrid();
        $dataGrid->bind($results_array ,array() , 'Array');
        
        // Define the datagrid columns
        // PLB changed column label 01/20/09
        $dataGrid->addColumn( new Structures_DataGrid_Column('Revd', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Touched?', 'touched', null, array("title" => "Have I touched this application yet?"), null, 'printTouched()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Name', 'name', null, array(), null, 'printEditLink()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Ctzn', 'ctzn_code', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('M/F', 'm_f', null, array(), null) );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Programs', 'program', null, array(), null, 'printPrograms()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Interests', 'interest', null, array(), null, 'printInterests()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Undergrad', 'undergrad', null, array(), null) );
        // PLB changed column label, phone screen display 01/20/09 
        $dataGrid->addColumn( new Structures_DataGrid_Column('Sp', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Special Cons?', 'special_consideration_requested', null, array("title" => "Has someone requested that this applicant receive special consideration?"), null, 'printSpecialConsideration()') );
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Lang Screen?', 'language_screen_requested', null, array("title" => "Has someone requested a language phone screen for this applicant?"), null, 'printLanguageScreen()') );
        //$dataGrid->addColumn( new Structures_DataGrid_Column('Tech Screen?', 'technical_screen_requested', null, array("title" => "Has someone requested a technical phone screen for this applicant?"), null, 'printTechnicalScreen()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Groups', 'revgrp2', null, array(), null, 'printGroups2()') );  
        $dataGrid->addColumn( new Structures_DataGrid_Column('Revrs', 'revr', null, array(), null, 'printReviewers()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Decision', 'decision', null, array(), null, 'printDecision()') );
        $dataGrid->addColumn( new Structures_DataGrid_Column('Admit To', 'admit_to', null, array(), null, 'printAdmitTo()') ); 
        if (!$semiblind) {
        
            $dataGrid->addColumn( new Structures_DataGrid_Column('Avg Rank', 'avg_rank', null, array(), null) );
            
        }
        if ($searchString != "") {
        
           $dataGrid->addColumn( new Structures_DataGrid_Column('Search Results', 'searchContext', null, array(), null) ); 
        }
        
        return $dataGrid; 
        
    }
 

    function getRound3Data($reviewer_id, $groups="allGroups", $semiblind=1, $searchString="") {
       
        // Set the search select and search join
        $this->parseSearchString($searchString); 
        
        // Check for limit to reviewer's groups
        $this->setGroupsJoin($reviewer_id, $groups);        
        
        // SEMIBLIND QUERY 
        // PLB added first six fields in select 
        // and changed separators from <br> to |
        $semiblind_query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code, 
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when " . $this->department_id . " then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote
,
group_concat(distinct if(revgroup.department_id = " . $this->department_id . " , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = " . $this->department_id . " , revgroup2.name , '')  separator '|') as revgrp2
 ,
 group_concat(distinct 
            case lu_programs_departments.department_id when  " . $this->department_id . " then
                lu_application_programs.decision
            end
            separator '|'
            ) as decision  ,
group_concat( distinct 
                case lu_user_department.department_id when " . $this->department_id . " then 
                
                    case lu_application_programs.admission_status when 1 then 
                        concat(if(degree.name is not null, degree.name,'') , ' ', if(programs.linkword is not null, programs.linkword,'') , ' ',if(fieldsofstudy.name is not null, fieldsofstudy.name,'')  , '(wait)') 
                    when 2 then
                        concat(degree.name, ' ', if(programs.linkword is not null, programs.linkword,''), ' ', fieldsofstudy.name)
                    end
                end  separator '|') as admit_to 

                /* PLB added search select 01/12/09 */
                " . $this->searchSelect . " 
                
                from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added three following joins and groupJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . "  
               
        where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=" . $this->department_id . "  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = " . $this->department_id . "   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname"; 

                    
        // REGULAR QUERY
        // PLB added first six fields in select 
        // and changed separators from <br> to |
        $query = "select   
        application.id as application_id,    
        if (reviewer.touch_count > 0, 1, 0) as touched,
        IFNULL(special_consideration.requested, 0) as special_consideration_requested,
        IFNULL(phone_screens.technical_screen_requested, 0) as technical_screen_requested,
        IFNULL(phone_screens.language_screen_requested, 0) as language_screen_requested,
        countries.iso_code as ctzn_code,
        lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,       
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '|') as program,
group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '|') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid ,
group_concat( distinct 
                        case review.fac_vote when 0 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1)  )
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    concat(substring(users3.firstname,1,1), substring(users3.lastname,1,1)  )
                                end
                            end
                        end
                      separator '|')  as revr
 ,
group_concat( distinct 
                case lu_user_department.department_id when " . $this->department_id . " then concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1),':',
                    case review.round2 when 1 then 'yes' when 0 then 'no' end)
                end  separator '|')  as round2
, case lu_application_programs.round2 when 0 then '' when 1 then 'promoted' when 2 then 'demoted' end as promote
,
group_concat(distinct if(revgroup.department_id = " . $this->department_id . " , revgroup.name , '')  separator '|') as revgrp1 ,
group_concat(distinct if(revgroup2.department_id = " . $this->department_id . " , revgroup2.name , '')  separator '|') as revgrp2
 ,
 group_concat(distinct 
            case lu_programs_departments.department_id when  " . $this->department_id . " then
                lu_application_programs.decision
            end
            separator '|'
            ) as decision  ,
group_concat( distinct 
                case lu_user_department.department_id when " . $this->department_id . " then 
                
                    case lu_application_programs.admission_status when 1 then 
                        concat(if(degree.name is not null, degree.name,'') , ' ', if(programs.linkword is not null, programs.linkword,'') , ' ',if(fieldsofstudy.name is not null, fieldsofstudy.name,'')  , '(wait)') 
                    when 2 then
                        concat(degree.name, ' ', if(programs.linkword is not null, programs.linkword,''), ' ', fieldsofstudy.name)
                    end
                end  separator '|') as admit_to,

/* PLB replaced w/ getAvgRank method 01/20/09 */
/*
if( review.round = 1 ,round( avg(  case lu_user_department.department_id when " . $this->department_id . " then review.point end  ),2 ),'' ) as avg_rank ,
*/
 group_concat( distinct 
                    if( 0 = 3,
                        
                        case review.fac_vote when 1 then
                            case review.round when 1 then
                                case lu_user_department.department_id when " . $this->department_id . " then 
                                    review.comments  
                                end
                            
                            when 2 then
                                case lu_user_department2.department_id when " . $this->department_id . " then 
                                    review.comments
                                end
                            
                            end
                        end
                    
                    ,
                    
                    case review.fac_vote when 0 then
                        case review.round when 1 then
                            case lu_user_department.department_id when " . $this->department_id . " then 
                                review.comments  
                            end
                            
                                    when 2 then
                                        case lu_user_department2.department_id when " . $this->department_id . " then 
                                            review.comments
                                        end
                                    
                        end
                    
                    end
                    )
                    separator '|') as comments 

                    /* PLB added search select 01/12/09 */
                    " . $this->searchSelect . " 
                    
                    from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
         
            left outer join lu_users_usertypes as lu_users_usertypes3 on lu_users_usertypes3.id = review.reviewer_id and review.round = 2
            left outer join lu_user_department as lu_user_department2 on lu_user_department2.user_id = lu_users_usertypes3.id
            left outer join users as users3 on users3.id = lu_users_usertypes3.user_id
        
            /*review groups - round 2*/
            left outer join lu_reviewer_groups as lu_reviewer_groups2 on lu_reviewer_groups2.reviewer_id = lu_users_usertypes3.id
            and lu_reviewer_groups2.round = 2
            
            /*application groups - round 2*/    
            left outer join lu_application_groups as lu_application_groups2 on lu_application_groups2.application_id = application.id
            and lu_application_groups2.round = 2     
            left outer join revgroup as revgroup2 on revgroup2.id = lu_application_groups2.group_id 
        
        /* PLB added three following joins and groupJoin */
        
        LEFT OUTER JOIN (
            SELECT application_id, reviewer_id, department_id, count( id ) AS touch_count
            FROM review
            GROUP BY application_id, reviewer_id, department_id
            HAVING reviewer_id = " . $reviewer_id . "
            AND department_id = " . $this->department_id . "
        ) AS reviewer on reviewer.application_id = application.id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(special_consideration) as requested
            FROM special_consideration 
            GROUP BY application_id            
        ) as special_consideration ON application.id = special_consideration.application_id
        
        LEFT OUTER JOIN (
            SELECT application_id, 
            MAX(technical_screen) as technical_screen_requested,
            MAX(language_screen) as language_screen_requested
            FROM phone_screens
            GROUP BY application_id            
        ) as phone_screens ON application.id = phone_screens.application_id
        
        " . $this->groupsJoin . "
        
        /* PLB added search join 01/12/09 */
        " . $this->searchJoin . "         
               
        where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=" . $this->department_id . "  and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id = " . $this->department_id . "   ) 
        and (lu_application_programs.round2 = 0 or lu_application_programs.round2 = 1 ) 
        and (lu_application_programs.round2 != 2)  group by  users.id     
        having (round2 NOT REGEXP '[[:<:]]no[[:>:]]' or promote='promoted')   
        order BY users.lastname,users.firstname";
        
        
        if ($semiblind) {
        
            $results_array = $this->handleSelectQuery($semiblind_query);
            
        } else {
        
            $results_array = $this->handleSelectQuery($query);
            
        }
         
        return $results_array;     
        
    }


    // pworona: append faculty of interest.  
    // added 01/14/09
    function getFacultyOfInterest($applications)
    {
        $updatedApplications = array();
        foreach ( $applications as $applicant )
        {
             $application_id = $applicant['application_id'];  
             $query = "SELECT GROUP_CONCAT( aa.name separator ', ') AS possible_advisor_names
                FROM lu_application_programs ap
                LEFT JOIN lu_application_advisor aa
                ON ap.application_id = aa.application_id and ap.program_id = aa.program_id
                WHERE ap.application_id = '$application_id' 
                AND aa.name IS NOT NULL
                GROUP BY ap.application_id";
                $results_array = $this->handleSelectQuery($query);
            if ( isset($results_array[0]['possible_advisor_names']) )
                $applicant['possible_advisor_names'] = $results_array[0]['possible_advisor_names'];
            array_push($updatedApplications, $applicant);
        }
        return $updatedApplications;
    }

    // PLB: append avg rank.  
    // added 01/20/09
    function getAvgRank($applications)
    {
        $updatedApplications = array();
        foreach ( $applications as $applicant )
        {
             $application_id = $applicant['application_id'];  
             $query = "SELECT application_id, round( avg( review.point ) , 2 ) AS avg_rank
                FROM review
                WHERE department_id = " . $this->department_id . "
                AND application_id = " . $application_id . "
                AND fac_vote = 0
                GROUP BY application_id";
            $results_array = $this->handleSelectQuery($query);
            if ( isset($results_array[0]['avg_rank']) )
                $applicant['avg_rank'] = $results_array[0]['avg_rank'];
            array_push($updatedApplications, $applicant);
        }
        return $updatedApplications;
    }
  


    // Function to construct subquery for use in limiting queries to a particular reviewer.   
    function setGroupsJoin($reviewer_id, $groups) {
    
         if ($groups == "myGroups") {
        
             $this->groupsJoin = "inner join (
                            SELECT lu_application_groups.application_id
                            FROM lu_application_groups
                            INNER JOIN lu_reviewer_groups ON lu_application_groups.group_id = lu_reviewer_groups.group_id
                            WHERE lu_reviewer_groups.reviewer_id = " . $reviewer_id . "
                            ) as my_applications on my_applications.application_id = application.id";
            
        } else {
        
            $this->groupsJoin = "";
            
        }
          
    }

    
    // Function construct subquery and where clause for full-text search.
    function parseSearchString($searchString) {
    
	    // PWORONA -- changed substirng match to -10
        if ( $searchString != "" )
        {
            // format search string for boolean mode query
            $formattedSearchString = "";
            $ssParts = split( " ", $searchString);
            foreach ( $ssParts as $word )
            {
                $formattedSearchString .= "+" . $word . " ";
            }
            $formattedSearchString = trim($formattedSearchString);
            
            // addition to select parameters
            $stringEnd = strlen($searchString) + 200;
            $this->searchSelect = ", SUBSTRING( searchMatches.application_text, 
                LOCATE('$searchString', searchMatches.application_text) -10, $stringEnd ) as searchContext";
            // addition to join
            $this->searchJoin = " INNER JOIN (
                    SELECT application_id, application_text 
                    FROM searchText st 
                    WHERE MATCH (application_text) 
                    AGAINST( '$formattedSearchString' IN BOOLEAN MODE ) 
                ) AS searchMatches 
                ON searchMatches.application_id = application.id ";
        } else {
            $this->searchJoin = $this->searchSelect = "";    
        }
        
        return TRUE;
    }


    function setMaxGroupConcat() {
    
        $query = "SET SESSION group_concat_max_len=4096";
        $results_array = $this->handleInsertQuery($query);
            
        return TRUE;
    }


}    

// #### Shared callback functions for datagrid.

function printTouched($params) {
    extract($params);
    $touched = $record['touched'];
    if ($touched) {
    
        $check = "<div class='yes'>Y</div>";
        
    } else {
    
        $check = "<div class='no'>N</div>";
        
    }
    
    return $check;
}

function printSpecialConsideration($params) {
    extract($params);
    $special_consideration_requested = $record['special_consideration_requested'];
    if ($special_consideration_requested) {
    
        $check = "<div class='yes'>Y</div>";
        
    } else {
    
        $check = "<div class='no'>N</div>";
        
    }
    
    return $check;
}

function printLanguageScreen($params) {
    extract($params);
    $language_screen_requested = $record['language_screen_requested'];
    if ($language_screen_requested) {
    
        $check = "<div class='yes'>Y</div>";
        
    } else {
    
        $check = "<div class='no'>N</div>";
        
    }
    
    return $check;
}

function printTechnicalScreen($params) {
    extract($params);
    $technical_screen_requested = $record['technical_screen_requested'];
    if ($technical_screen_requested) {
    
        $check = "<div class='yes'>Y</div>";
        
    } else {
    
        $check = "<div class='no'>N</div>";
        
    }
    
    return $check;
}
 
function printEditLink($params) {
    extract($params);
    
    // get "global" variables for setting up proper link
    global $usertypeid;
    global $round;
    global $department_id;
    global $decision;
    
    if ($usertypeid == 3) {
    
        $view = "3";
        
        if ($round == 1) {
        
            $round_param = 2;
            
        } else {
        
            $round_param = $round;
            
        } 
        
        
    } else {
    
        $view = "2";
        $round_param = $round;
        
    }
 
    
    if ($decision) {
    
        $show_decision = "&showDecision=1";
        
    } else {
    
        $show_decision = "";
        
    }
    
    $user_id = $record['id'];
    $name = $record['name'];
    $link = '<a class="menu" href="javascript: openForm(\'';
    $link .= $user_id . '\',\'../review/userroleEdit_student_review.php?v=' . $view . '&r=' . $round_param;
    $link .= '&d=' . $department_id . $show_decision . '\')"' ;
    $link.= 'id="edit_' . $user_id . '" title="application id: ' . $record['application_id'] . '">' . $name . '</a>';
    return $link;
}


function printPrograms($params) {
    extract($params);
    $interests = str_replace("|", "<br />" , $record['program']);
    return $interests;
} 

function printInterests($params) {
    extract($params);
    $interests = str_replace("|", "<br />" , $record['interest']);
    return $interests;
}

function printGroups($params) {
    extract($params);
    $groups = str_replace("|", "<br />" , $record['groups']);
    return $groups;
}

function printGroups2($params) {
    extract($params);
    $groups = str_replace("|", "<br />" , $record['revgrp2']);
    return $groups;
}

function printReviewers($params) {
    extract($params);
    $reviewers = str_replace("|", "<br />" , $record['revr']);
    return $reviewers;
}

function printVotes($params) {
    extract($params);
    $votes = str_replace("|", "<br />" , $record['round2']);
    return $votes;
}


function printDecision($params) {
    extract($params);
    $decision = htmlspecialchars_decode( str_replace("|", "<br />" , $record['decision']) );
    return $decision;
}

function printAdmitTo($params) {
    extract($params);
    $admit_to = htmlspecialchars_decode( str_replace("|", "<br />" , $record['admit_to']) );
    return $admit_to;
}


// ### CSD callback functions

function printReveiwerRanks($params) {
    extract($params);
    $rank = str_replace("|", "<br />" , $record['rank']);
    return $rank;
}

// PLB added new functions 01/12/09

function printCommentsPertinent($params) {
    extract($params);
    $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['comments']) );
    $comments .= "<br /><br />" . htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['pertinent_info']) );
    return $comments;
}

function printPertinentInfo($params) {
    extract($params);
    $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['pertinent_info']) );
    return $comments;
}


// ### RI/LTI callback functions

function printComments($params) {
    extract($params);
    $comments = htmlspecialchars_decode( str_replace("|", "<br /><br />" , $record['comments']) );
    return $comments;
}

function printPhdRank($params) {
    extract($params);
    $scores = str_replace("|", "<br />" , $record['rank']);
    return $scores;
}

function printMsRank($params) {
    extract($params);
    $scores = str_replace("|", "<br />" , $record['ms_rank']);
    return $scores;
}


?>
