<?php
//
// Include files needed by classes in the reviewApplications directory.
// Paths are relative to reviewApplications.php
require_once('Structures/DataGrid.php');
include_once "./classes/class.db_applyweb.php";
include_once "./reviewApplications/class.review_list.php";
  
// Set the appropriate method for the round.
switch ($round) {

    case 2:
    
        if ($decision == 1) {
        
            $method = "getFinal";
            
        } else {
        
            $method = "getRound2";
            
        }
        
        break; 

    case 3:
    
        $method = "getFinal";
        break;
    
    case 1:
    default:
    
        $method = "getRound1";
    
}

// Set the review list according to department id.
// Include the appropriate subclass if necessary; 
// paths are relative to reviewApplications.php. 
switch($department_id) {

	case 3:
	
		// Robotics Institute
        include "./reviewApplications/class.review_list_ri.php";
		$review_list = new RiReviewList($department_id);
		break;
        
	case 6:
    case 83:
	
		// LTI
        include "./reviewApplications/class.review_list_lti.php";
		$review_list = new LtiReviewList($department_id);
		break;
	
	default:
	
		// CSD and CSD-like departments
        $review_list = new ReviewList($department_id);
}

// Set faculty view to turn off special consideration/phone screens
// and turn on faculty of interest in round 2. 
if ( $usertypeid == 3 ) {

    $faculty_view = 1;    
    
} else {

    $faculty_view = 0;
    
}

// Call the appropriate review list method to get the datagrid.
// PLB added faculty view, search string 01/12/09 
//$dataGrid = $review_list->$method($reviewer_id, $groups, $semiblind_review);
$dataGrid = $review_list->$method($reviewer_id, $groups, $semiblind_review, $faculty_view, $search_text);

// Set table attributes
$dataGrid->renderer->setTableHeaderAttributes( array('color' => 'white', 'bgcolor' => '#99000') );
$dataGrid->renderer->setTableEvenRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
$dataGrid->renderer->setTableOddRowAttributes( array('valign' => 'top', 'bgcolor' => '#EEEEEE') );
$dataGrid->renderer->setTableAttribute('width', '100%');
$dataGrid->renderer->setTableAttribute('border', '0');
$dataGrid->renderer->setTableAttribute('cellspacing', '1');
$dataGrid->renderer->setTableAttribute('cellpadding', '5px');
$dataGrid->renderer->setTableAttribute('id', 'applicantTable');
$dataGrid->renderer->sortIconASC = '&uArr;';
$dataGrid->renderer->sortIconDESC = '&dArr;';

?>
