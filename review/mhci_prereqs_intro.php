<?php
ini_set('memory_limit', '64M');

header("Cache-Control: no-cache");

include_once "../inc/config.php";
include_once "../inc/session_admin.php";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>MHCI Prerequisites</title>

<!-- keep style here for now -->
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<style type="text/css">

    td, th  {
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12px;
    }
    
    tr a { color: blue; text-decoration: none; }
    tr a:hover { text-decoration: underline; }
    
    th a { color: black; text-decoration: none; }
    
    td.selected a { font-weight: bold; }
    tr.selected td { border-top: 1px solid black; }
    td.content { border-bottom: 1px solid black; }
    
    /*
    a.menu { color: black; }
    */
    
    .confirm { color: blue; }
    .confirm_complete { font-style: italic; font-weight : bold; }

    div.content { height: 250px; width: 100%; overflow: auto; display: none; }
    
    #loading {
        clear:both; 
        background:url(wait.gif) center top no-repeat; 
        text-align:center;
        padding:33px 0px 0px 0px; 
        font-size:12px;
        font-family:Verdana, Arial, Helvetica, sans-serif; }

    
    #content_main { 
        position:absolute; 
        height: 300px; 
        /*
        width:500px;
        */
        top:0;
        left: 0;
        right:0;
        background-color: #FFFFFF;
        display: none; 
    }

</style>

<!-- include the jquery libraries -->
<script language="javascript" src="./javascript/jquery-1.2.6.min.js"></script>

</head>

<body>

<!-- original header here -->
<!--
<div style="height:72px; width:781;" align="center">
      <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
-->
<div style="height:72px;" align="left">
    <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="75%">
            <span class="title">Carnegie Mellon School for Computer Sciences</span><br />
            <strong>Online Admissions System</strong>
            </td>
            <td align="right">
            <?php 
            $_SESSION['A_SECTION']= "2";
            if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
            ?>
            You are logged in as:<br />
            <?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
            <br />
            <a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
            <?php } ?>
            </td>
        </tr>
    </table>
</div>

<!-- original menu div -->
<div style="height:10px" align="center">
        <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
        <script language="JavaScript" src="../inc/menu.js"></script>
        <!-- items structure. menu hierarchy and links are stored there -->
        <!-- files with geometry and styles structures -->
        <script language="JavaScript" src="../inc/menu_tpl.js"></script>
        <script language="JavaScript" src="../inc/menu_items_review.js"></script>
        <script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
    </div>
</div>
<br />
<br />

<?php

echo '<div style="margin: 10px;"><span class="title">MHCI Prerequisites</span><br /><br />'; 

// include the datagrid package
require('Structures/DataGrid.php');

// Create the DataGrid, Bind it's Data Source and Define it's columns
$dg = new Structures_DataGrid();


// Get the data
include "../inc/mhci_prereqsSummaryData.class.php";
$summaryData = new MHCI_PrereqsSummaryData();
$statusArray = $summaryData->getStatusFlat();

// bind application list data using array driver
$dg->bind($statusArray ,array() , 'Array');

// Create the columns

// Row number
$dg->addColumn(
        new Structures_DataGrid_Column(
            '', 
            'application_id', 
            'application_id', 
            array(
                'width' => '2%',
                'align' => 'left'
                ), 
            null, 
            'printNumber()', 
            $dg->getCurrentRecordNumberStart()
            )
        );        

// Application link
$dg->addColumn(
        new Structures_DataGrid_Column(
            '', 
            'lu_users_usertypes_id', 
            'lu_users_usertypes_id', 
            array(
                'width' => '8%',
                'align' => 'left'
                ), 
            null, 
            'printPrintLink()'
            )
        );

// Name
$dg->addColumn(
    new Structures_DataGrid_Column(
        'Name', 
        'name', 
        'name', 
        array(
            'width' => '15%',
            'align' => 'left'
            ),
        null, 
        'printFullName()'
        )
    );

// Design Status
$dg->addColumn(
    new Structures_DataGrid_Column(
        'Design status', 
        'design_status', 
        'design_status', 
        array(
            'width' => '25%',
            'align' => 'center'
            ),
        null,
        'printDesignStatus()' 
        )
    );


    

// Programming Status
$dg->addColumn(
    new Structures_DataGrid_Column(
        'Programming status', 
        'programming_status', 
        'programming_status', 
        array(
            'width' => '25%',
            'align' => 'center'
            ),
        null,
        'printProgrammingStatus()'
        )
    );

// Statistics Status
$dg->addColumn(
    new Structures_DataGrid_Column(
        'Statistics status', 
        'statistics_status', 
        'statistics_status', 
        array(
            'width' => '25%',
            'align' => 'center'
            ),
        null,
        'printStatisticsStatus()'
        )
    );

// Define the Look and Feel
$dg->renderer->setTableHeaderAttributes(array('bgcolor' => '#CCCCCC'));
$dg->renderer->setTableEvenRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu')
    );
$dg->renderer->setTableOddRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu')
    );
$dg->renderer->setTableAttribute('width', '100%');
$dg->renderer->setTableAttribute('border', '0');
$dg->renderer->setTableAttribute('cellspacing', '0');
$dg->renderer->setTableAttribute('cellpadding', '5px');
$dg->renderer->setTableAttribute('class', 'datagrid');
$dg->renderer->sortIconASC = '&uArr;';
$dg->renderer->sortIconDESC = '&dArr;';

// render the datagrid
$dg->render();


// callback functions for datagrid rendering

function printNumber($params, $recordNumberStart) {
    extract($params);
    $user_id = $record['lu_users_usertypes_id'];
    $recordNumber = $params['currRow'] + $recordNumberStart . ".";
    return $recordNumber;
}

function printPrintLink($params) {
    extract($params);
    $user_id = $record['lu_users_usertypes_id'];
    $link = "<a class=\"menu\" href=\"\" title=\"Print Application\""; 
    $link .= " onClick=\"window.open('../admin/userroleEdit_student_formatted.php?id=" . $user_id . "'); return false;\">";
    $link .= "Print/View Application</a>";
    return $link;
}


function printFullName($params)
{   
    extract($params);
    return "<b>" . $record['name'] . "</b><br /><a title=\"E-mail Applicant\" href=\"mailto:" . $record['email'] . "\">" . $record['email'] . "</a>";
}


function printDesignStatus($params) {

    extract($params);
    $studentId = $record['lu_users_usertypes_id'];
    $status = $record['design_status'];
    
    return '<a href="mhci_prereqs_design.php?studentLuId=' . $studentId . '">' . $status . '</a>';
}


function printProgrammingStatus($params) {

    extract($params);
    $studentId = $record['lu_users_usertypes_id'];
    $status = $record['programming_status'];
    
    return '<a href="mhci_prereqs_programming.php?studentLuId=' . $studentId . '">' . $status . '</a>';
}


function printStatisticsStatus($params) {

    extract($params);
    $studentId = $record['lu_users_usertypes_id'];
    $status = $record['statistics_status'];
    
    return '<a href="mhci_prereqs_statistics.php?studentLuId=' . $studentId . '">' . $status . '</a>';
}


function printFiller($params) {

    extract($params);

    $filler = "Plan: Give me 100 dollars and we'll call it square.";
    return $filler;
}


?>

</body>
</html>
