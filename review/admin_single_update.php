<?php

// include the config class
include_once("../inc/config.php");

// include the db classes
include "./classes/class.db_applyweb.php";
include "./classes/class.db_application.php";
include "./classes/class.db_payment.php";
include "./classes/class.db_education.php";
include "./classes/class.db_test_scores.php";
include "./classes/class.db_recommendations.php";
include "./classes/class.db_publications.php";

// determine case from the mode argument
$mode = $_REQUEST['mode'];

// get the application id
$application_id = $_REQUEST['application_id'];

$return_string = "";

switch ($mode) {

    case "fee_waived":
    
        $fee_waived = $_GET['fee_waived'];
        $payment = new DB_Payment();
        $status = $payment->updateWaivePayment($application_id, $fee_waived);
        $return_string = $status; 
        break;


    case "application_submitted":
    
        $application_submitted = $_GET['application_submitted'];
        $application = new DB_Application();
        $status = $application->updateSubmitted($application_id, $application_submitted);
        $return_string = $status; 
        break;

        
    case "application_complete":
        
        $application_complete = $_GET['application_complete'];
        $application = new DB_Application();
        $status = $application->updateComplete($application_id, $application_complete);
        $return_string = $status; 
        break;

        
    case "fee_paid":
        
        $fee_paid = $_GET['fee_paid'];
        $amt = $_GET['amt']; 
        $payment = new DB_Payment();
        $status = $payment->updateFeePaid($application_id, $fee_paid, $amt);
        $return_string = $status; 
        break;

    
    case "payment_amount":

        $payment_amount = $_GET['payment_amount'];
        $payment = new DB_Payment();
        $status = $payment->updatePaymentAmount($application_id, $payment_amount);
        $return_string = $status;
        break;

        
    case "payment_reset":
        
        $payment = new DB_Payment();
        $status = $payment->resetPayment($application_id);
        $return_string = $status;
        break;

    
    case "transcript_received":

        $transcript_id = $_GET['transcript_id'];
        $transcript_received = $_GET['transcript_received'];
        $education = new DB_Education();
        $status = $education->updateTranscriptReceived($transcript_id, $transcript_received);
        $return_string = $status;
        break;

        
    case "gre_general_received":
        
        $grescore_id = $_GET['grescore_id'];
        $score_received = $_GET['score_received'];
        $test_scores = new DB_TestScores();
        $status = $test_scores->updateGREGeneralReceived($grescore_id, $score_received);
        $return_string = $status;
        break;

        
    case "gre_general_score":
        
        $grescore_id = $_GET['grescore_id'];
        $verbal_score = $_GET['verbal_score'];
        $verbal_percentile = $_GET['verbal_percentile'];
        $quantitative_score = $_GET['quantitative_score'];
        $quantitative_percentile = $_GET['quantitative_percentile'];
        $analytical_score = $_GET['analytical_score'];
        $analytical_percentile = $_GET['analytical_percentile'];
        $writing_score = $_GET['writing_score'];
        $writing_percentile = $_GET['writing_percentile'];
        
        $test_scores = new DB_TestScores();
        $status = $test_scores->updateGREGeneralScore($grescore_id, 
                                    $verbal_score, $verbal_percentile, 
                                    $quantitative_score, $quantitative_percentile,
                                    $analytical_score, $analytical_percentile,
                                    $writing_score, $writing_percentile);
        $return_string = $status;
        break;

        
    case "gre_subject_received":

        $gresubjectscore_id = $_GET['gresubjectscore_id'];
        $score_received = $_GET['score_received'];
        $test_scores = new DB_TestScores();
        $status = $test_scores->updateGRESubjectReceived($gresubjectscore_id, $score_received);
        $return_string = $status;
        break;

     case "gre_subject_score":
        
        $gresubjectscore_id = $_GET['gresubjectscore_id'];
        $subject_name = $_GET['subject_name'];
        $subject_score = $_GET['subject_score'];
        $subject_percentile = $_GET['subject_percentile'];
        
        $test_scores = new DB_TestScores();
        $status = $test_scores->updateGRESubjectScore($gresubjectscore_id,
                                    $subject_name,
                                    $subject_score, 
                                    $subject_percentile);
        $return_string = $status;
        break;
 
 
    case "toefl_received":
        
        $toeflscore_id = $_GET['toeflscore_id'];
        $score_received = $_GET['score_received'];
        $test_scores = new DB_TestScores();
        $status = $test_scores->UpdateTOEFLReceived($toeflscore_id, $score_received);
        $return_string = $status;
        break;

        
    case "toefl_score":
        
        $toeflscore_id = $_GET['toeflscore_id'];
        $section1_score = $_GET['section1_score'];
        $section2_score = $_GET['section2_score'];
        $section3_score = $_GET['section3_score'];
        $essay_score = $_GET['essay_score'];
        $total_score = $_GET['total_score'];
        
        $test_scores = new DB_TestScores();
        $status = $test_scores->updateTOEFLScore($toeflscore_id, 
                                    $section1_score, $section2_score, 
                                    $section3_score, $essay_score,
                                    $total_score);
        $return_string = $status;
        break;

        
    case "publication_status":
        
        $publication_id = $_GET['publication_id'];
        $publication_status = $_GET['status'];
        
        $publications = new DB_Publications();
        $query_status = $publications->updatePublicationStatus($publication_id, $publication_status); 
        
        $return_string = $query_status;
        break;

        
    case "upload_recommendation":
        
        $recommendation_id = $_REQUEST['recommendation_id'];  
        $applicant_luu_id = $_REQUEST['applicant_id'];
        $applicant_guid = $_REQUEST['applicant_guid'];
        
        $file_type = $_FILES['fileToUpload']['type']; 
        $file_extension = substr ( strrchr($_FILES['fileToUpload']['name'], "."), 1);
        $file_size = $_FILES['fileToUpload']['size'];
        
        $section = 3; // code for recommendation
        $user_data = $application_id . "_" . $recommendation_id;
        
        $upload_mode = $_REQUEST['upload_mode'];
        
        //$new_filedir =  "../../data/2009/" . $applicant_guid . "/";                                         
        $new_filedir =  $datafileroot . "/" . $applicant_guid . "/recommendation/";
        $new_filename = "recommendation_" . $user_data . "." . $file_extension;
        $new_filepath = $new_filedir . $new_filename;
        
        
         $database_status = 1;
        // check to see that a file was actually submitted 
        if(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none'){
            
            $error = "No file selected.";
            $msg = "";
            $return_string = "{error: '" . $error . "', msg: '" . $msg . "'}";
            
        } else {
            
            // upload the file
            $return_string = handleAjaxFileUpload($new_filedir, $new_filepath);
            
            // check for file upload error
            $json_object = json_decode($return_string);
            $upload_error = $json_object->{'error'};
            
            // if no upload error
            if ( $upload_error == "" ) {
                
                // do the database insert/update 
                $recommendation = new DB_Recommendations();
                
                if ($upload_mode == "update"){

                    $database_status = $recommendation->updateFile($recommendation_id, 
                                            $file_type, 
                                            $file_extension, 
                                            $file_size, 
                                            $applicant_luu_id,
                                            $section,
                                            $user_data);        
                } else {
            
                    $database_status = $recommendation->addFile($recommendation_id, 
                                            $file_type, 
                                            $file_extension, 
                                            $file_size, 
                                            $applicant_luu_id,
                                            $section,
                                            $user_data);
               }
               
            } // end if upload error

        } // end file empty if/else

        // set return JSON based on database status       
        if ( $database_status >= 1 ) {
             // do nothing to change return string messages
        } else {
            $error = "Database update failed.";
            $msg = "File not uploaded.";
            $return_string = "{error: '" . $error . "', msg: '" . $msg . "'}";
        }
        
        break;

    default:
        $return_string = "unkown case: " . $mode;
}


// output the return data
echo $return_string;



/*
// Function to handle file upload for upload_recommendation case 
*/
function handleAjaxFileUpload ($new_filedir, $new_filepath) {
    $error = "";
    $msg = "";
    $fileElementName = 'fileToUpload';
    if(!empty($_FILES[$fileElementName]['error']))
    {
        switch($_FILES[$fileElementName]['error'])
        {

            case '1':
                $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                break;
            case '2':
                $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                break;
            case '3':
                $error = 'The uploaded file was only partially uploaded';
                break;
            case '4':
                $error = 'No file was uploaded.';
                break;

            case '6':
                $error = 'Missing a temporary folder';
                break;
            case '7':
                $error = 'Failed to write file to disk';
                break;
            case '8':
                $error = 'File upload stopped by extension';
                break;
            case '999':
            default:
                $error = 'No error code avaiable';
        }
    } elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
    {
        $error = 'No file was uploaded...';
    } else 
    {
            $msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
            $msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']) . ", ";
            $msg .= " Copied As: " . $new_filepath;
            
            // PB - copy uploaded file
            $tmp_name = $_FILES['fileToUpload']['tmp_name'];
            
            // make the guid directory if not there already
            if ( !file_exists($new_filedir) ) {
                mkdir($new_filedir, 0755, TRUE);
            }
            
            if ( !move_uploaded_file($tmp_name, $new_filepath) ) {
                $error = "Upload of " . $new_filepath . " failed.";
                $msg .= " " . $new_filepath . " not copied";
            }
            
            //for security reason, we force to remove all uploaded file
            @unlink($_FILES['fileToUpload']);        
    }
            
    $return_string = '{';
    $return_string .= '"error": "' . $error . '",';
    $return_string .= '"msg": "' . $msg . '"';
    $return_string .= '}';

    return $return_string;

}



?>