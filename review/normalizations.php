 <?php
ini_set('memory_limit', '256M');
$startTime = microtime(TRUE);

include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';

include "../classes/class.NormalizationListData.php";
include "../classes/DB_Applyweb/class.DB_VW_Applyweb.php"; 
include "../classes/Structures_DataGrid/class.ReviewList_DataGrid.php";
include "../classes/Structures_DataGrid/class.NormalizationListScores_DataGrid.php";
include "../classes/Structures_DataGrid/class.NormalizationListScoresMse_DataGrid.php";
include "../classes/Structures_DataGrid/class.NormalizationListScoresMsCs_DataGrid.php";
include "../classes/Structures_DataGrid/class.NormalizationListScoresLti_DataGrid.php";
include "../classes/Structures_DataGrid/class.NormalizationListScoresDesign_DataGrid.php";
include "../classes/Structures_DataGrid/class.NormalizationListNormalizations_DataGrid.php";
include '../classes/DB_Applyweb/class.VW_Programs.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';
// include '../inc/specialCasesAdmin.inc.php';

/*
* Handle the request variables.
*/
if ( isset($_REQUEST['departmentId']) ) {
    $departmentId = $department_id = $_REQUEST['departmentId'];    
} elseif ( isset($_REQUEST['id']) ) {
    $departmentId = $department_id = $_REQUEST['id'];     
} else {
    // send the user to the home page.
    header('Location: ../admin/index.php');
}
// Get the department name, enable_round2 for the page heading & round selector.
$sql = "select name, enable_round2, enable_round3, enable_round4 from department where id = " . $departmentId;
$result = mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array( $result ))
{
    $deptName = $row['name'];
    $enableRound2 = $row['enable_round2'];
    $enableRound3 = $row['enable_round3'];
    $enableRound4 = $row['enable_round4'];
}

if ( isset($_REQUEST['periodId']) ) {
    $periodId = $_REQUEST['periodId'];    
} elseif ( isset($_REQUEST['period']) ) {
    $periodId = $_REQUEST['period'];     
} else {
    // send the user to the home page.
    header('Location: ../admin/index.php');    
}

if ( isset($_REQUEST['programId']) ) {
    $programId = $_REQUEST['programId'];      
} else {
    if ($departmentId == 18) {
        $programId = 21;    
    } else {
        $programId = NULL;    
    } 
}

if ( isset($_REQUEST['round']) ) {
    $round = $_REQUEST['round'];       
} else {
    if ($departmentId == 1) {
        $round = 2;   
    } else {
        $round = 1;
    }
        
}

if ( isset($_REQUEST['tableSortList']) )
{    
    $tableSortList = $_REQUEST['tableSortList'];
} else {
    $tableSortList = '';    
}

/*
* Get program info for a tabbed program menu for RI, LTI, and MSE-MSIT. 
*/
$displayByProgramDepartments = array(
    3 => array( 'name' => 'Robotics Institute', 'defaultProgramId' => 'phd' ),
    6 => array( 'name' => 'Language Technologies Institute', 'defaultProgramId' => 'phd' ),
    18 => array( 'name' => 'MSE-MSIT Campus', 'defaultProgramId' => NULL )
);

$departmentPrograms = array();
if ( in_array($departmentId, array_keys($displayByProgramDepartments)) ) {
    $VW_Programs = new VW_Programs();
    $departmentPrograms = $VW_Programs->find('department', $departmentId);
    if ( !isset($programId) ) {
        $programId = $displayByProgramDepartments[$departmentId]['defaultProgramId'];
    }    
} else if ($department_id == 83) {
    $programId = 100035;
}


// Programs that should use point2 as the score.
// NOTE: MSE-MSIT and MSE Portugal also use point2 for add'l MSIT score 
// for MSE (program ids 21, 37) applicants.
$point2Programs = array(
    6 => 'Ph.D./M.S. in Language and Information Technologies',
    12 => 'M.S. in Robotics',
    'ms' => 'All LTI M.S.',
    100035 => 'IIS'
    );
$score = 1;

if ( in_array($programId, array_keys($point2Programs)) ) {
    $score = 2;   
}

if (isRiMastersDepartment($departmentId)) {
    $score = 2;
}

if (isRissDepartment($departmentId)) {
    $score = 2;
}



/*
* Get the score and normalization data and bind them to datagrids. 
*/
$dataProgramId = $programId;
if ($departmentId == 3) {
    if ($programId == 'phd') {
        $dataProgramId = array(4, 17, 100029);    
    }
} elseif ($departmentId == 6) {
    if ($programId == 'phd') {
        $dataProgramId = array(5, 6, 34);    
    } else {
        $dataProgramId = array(5, 6, 34);    
    }
} elseif ($departmentId == 83) {
    $dataProgramId = array(100035);
}    
$normalizationData = new NormalizationListData($departmentId, $periodId, $dataProgramId, $round);
 
$scores = $normalizationData->getScores($score); 
if (isMseMsitDepartment($departmentId)) 
{
    $scoresDatagrid = new NormalizationListScoresMse_DataGrid(2, TRUE);    
}
if (isDesignDepartment($departmentId) || isDesignPhdDepartment($departmentId) || isDesignDdesDepartment($departmentId)) 
{
    $scoresDatagrid = new NormalizationListScoresDesign_DataGrid(2, TRUE);    
}
elseif ($departmentId == 74) 
{
    $scoresDatagrid = new NormalizationListScoresMsCs_DataGrid(2, TRUE);    
}
elseif ($departmentId == 6)
{
    $scoresDatagrid = new NormalizationListScoresLti_DataGrid(2, TRUE); 
} 
else 
{
    $scoresDatagrid = new NormalizationListScores_DataGrid(2, TRUE);    
}
 
if ( isset($scores[0]) ) {
    $scoresDatagrid->addReviewerColumns( array_keys($scores[0]) );    
}
$scoresDatagrid->bind($scores, array() , 'Array');
$datagridColumns = $scoresDatagrid->getColumns();

$normalizations = $normalizationData->getNormalizations($score);
$normalizationsDatagrid = new NormalizationListNormalizations_DataGrid(2, TRUE);
$normalizationsDatagrid->bind($normalizations, array() , 'Array');


/*
* Set up and include the standard header.
*/
$pageTitle = 'Normalized Scores';
$pageCssFiles = array(
    '../css/normalizations.css'
    );

$pageJavascriptFiles = array(
    '../javascript/jquery-1.2.6.min.js',
    '../javascript/jquery.tablesorter.js',
    '../javascript/normalizations.js',
    '../javascript/common.js',
    '../inc/scripts.js'
    );
include '../inc/tpl.pageHeader.php';

/*
* JS lameness copied, for now, from orignal normalization script. 
*/
echo "<SCRIPT LANGUAGE='JavaScript'>\n";
echo "function openForm(userid, formname) { \n";
echo "document.form1.action = formname;\n";
echo "document.form1.target = '_blank';\n";
echo "document.form1.userid.value = userid;\n";
echo "document.form1.submit();\n";
echo "}\n";
echo "</script>\n";

/*
*Get department, period info for heading.
*/
$startDate = NULL;
$endDate = NULL;
$applicationPeriodDisplay = "";
if ($periodId) {
    
    if (is_array($periodId)) {
        // The scs period will be first and the compbio/mrsd period will be second
        $displayPeriodId = $periodId[1];    
    } else {
        $displayPeriodId = $periodId; 
    }
    
    $period = new Period($displayPeriodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

/*
* Start display.
*/
?> 

<div id="export" style="padding: 20px; float: right; clear: right;">
    <form accept-charset="utf-8" action="reviewApplicationsExport.php" method="post" target="_blank"
    name="exportForm" id="exportForm" onsubmit="getTable('applicantTable')">
        <input type="hidden" id="datatodisplay" name="datatodisplay" /> 
        <b>Export current scores table:</b>
        <input  type="submit" value="Save CSV File">
    </form>
</div> 

<?php
if (isMseMsitDepartment($departmentId)) {   
?>
<div id="refresh" style="padding: 20px; float: right;">
    <b>View latest data:</b>
    <input type='button' name='refreshPage' value='Refresh This Page'
        onClick="document.forms['paramSelector'].submit();">
</div>
<?php
}    
?>

<div id="pageHeading">
    <div id="departmentName">
    <?php
    echo $deptName;
    if ($applicationPeriodDisplay) {
        echo $applicationPeriodDisplay;
    }
    ?>
    </div>
    <div id="sectionTitle">
    <?php 
    echo $pageTitle; 
    if ($departmentId == 1) {
        echo ', Round 2';
    }
    ?>
    </div>
</div>

<div id="applicants" style="padding-top: 10px;">

<form accept-charset="utf-8" action="" method="GET" id="paramSelector" name="paramSelector">
    <input type="hidden" name="departmentId" value="<?php echo $departmentId; ?>" />
    <?php
    if (is_array($periodId)) {
        foreach ($periodId as $periodIdItem) {
            echo '<input type="hidden" name="periodId[]" value="' . $periodIdItem . '" />';    
        }
    } else {
        echo '<input type="hidden" name="periodId" value="' . $periodId . '" />';   
    }
    ?>
    <input type='hidden' name='tableSortList' class='tableSortList' value='<?= $tableSortList ?>'>
<?php
$printSubmit = FALSE;
if ($departmentId == 6) {
    $phdSelected = $msSelected = '';
    if ($programId == 'phd') {
        $phdSelected = 'selected';
    } elseif ($programId == 'ms') {
        $msSelected = 'selected';
    }
?>
    Scores:&nbsp;<select name="programId">';
        <option value="phd" <?php echo $phdSelected; ?> >All Ph.D.</option>
        <option value="ms" <?php echo $msSelected; ?> >All M.S.</option>     
    </select>     
<?php
} elseif ( count($departmentPrograms) > 0 ) { 
    echo 'Program:&nbsp;<select name="programId">';
    /*
    if ($departmentId == 18) {
        if (!$programId) {
            $selected = 'selected';    
        }
        echo '<option value="" ' . $selected . ' >All MSE-MSIT Campus Programs</option>';         
    }
    */
    foreach ($departmentPrograms as $program) {
        
        if ($program['program_id'] == 30) {
            // Skip MBA/ MSE
            continue;
        }
        
        if ($program['program_id'] == 21) {
            $programName = $program['program_name'] . ' + MBA/MSE';     
        } else {
            $programName = $program['program_name'];     
        }
        
        
        
        $printSubmit = TRUE; 
        $selected = '';
        if ($program['program_id'] == $programId
            || (!$programId && $program['program_id'] == 21) ) 
        {
            $selected = 'selected';
        }
        
        echo '<option value="' . $program['program_id'] . '" ' . $selected . ' >' . $programName . '</option>';    
    }
    if ($departmentId == 3) {
        $selected = '';
        if ($programId == 'phd') {
            $selected = 'selected';    
        }
        echo '<option value="phd" ' . $selected . ' >All Ph.D.</option>';    
    } 
    echo '</select>';
} 

if ($departmentId != 1 && $enableRound2) {
    $printSubmit = TRUE;
    $round1Selected = $round2Selected = '';
    if ($enableRound3) {
        $round3Selected = '';
    } 
    if ($enableRound4) {
        $round4Selected = '';
    }
    if ($round == 2) {
        $round2Selected = 'selected';    
    } else if ($round == 3) {
        $round3Selected = 'selected';    
    }  else if ($round == 4) {
        $round4Selected = 'selected';    
    } else {
        $round1Selected = 'selected';    
    }
?>
    &nbsp;Round:&nbsp;<select name="round">
        <option value="1" <?php echo $round1Selected; ?> >1</option>
        <option value="2" <?php echo $round2Selected; ?> >2</option> 
        <?php 
        if ($enableRound3) echo '<option value="3"' . $round3Selected .'>3</option>';
        if ($enableRound4) echo '<option value="4"' . $round3Selected .'>4</option>';
        ?>    
    </select> 
<?php
}

if ($printSubmit) {
    echo ' &nbsp;<input type="submit" value="Change Scores" />';
}
?>
</form>
<?php
if ($printSubmit) {
    echo '<br/>';
}
?>

<div id="sortTip">
    <span id="tip">TIP!</span> 
    Sort multiple columns simultaneously by holding down the shift key and clicking a second, third or even fourth column header! 
</div>

<!-- form to support JS lameness -->
<form accept-charset="utf-8" id='form1' action='' method='POST' name='form1'>
<input name='userid' type='hidden' id='userid' value='' />
 
<?php echo count($scores) . " Applicants Total"; 

 $scoresDatagrid->render();

 ?>

 <br/>
 
 <?php
 $normalizationsDatagrid->render(); 
 ?>
<!--- </div> --->
</form>

<script type="text/javascript" language="javascript">
<?php
$tableSortListArray = array_map( 'intval', explode(',', $tableSortList) );
if ( count($tableSortListArray) % 2 == 0) {
    $tableSortListJson = json_encode( array_chunk($tableSortListArray, 2) );    
} else {
    $tableSortListJson = '[]';   
}
echo 'var globalTableSortList = "' . $tableSortListJson . '";';
?>
</script>

<?php
// Include the standard page footer.
include '../inc/tpl.pageFooter.php'; 
?>
