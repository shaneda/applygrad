<?php
include_once "../inc/config.php";
include_once "../inc/session_admin.php";
include_once '../inc/db_connect.php'; 

$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>MHCI Statistics Prerequisite</title>

<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<link rel="stylesheet" href="../css/MHCI_style.css" type="text/css"> 
<!-- include the jquery libraries -->
<script language="javascript" src="./javascript/jquery-1.2.6.min.js"></script>
<script language="javascript" src="../inc/mhci_prereqs.js"></script>

</head>

<body>

<!-- original header here -->
<!--
<div style="height:72px; width:781;" align="center">
      <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
-->
<div style="height:72px;" align="left">
    <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="75%">
            <span class="title">Carnegie Mellon School for Computer Sciences</span><br />
            <strong>Online Admissions System</strong>
            </td>
            <td align="right">
            <?php 
            $_SESSION['A_SECTION']= "2";
            if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
            ?>
            You are logged in as:<br />
            <?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
            <br />
            <a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
            <?php } ?>
            </td>
        </tr>
    </table>
</div>

<!-- original menu div -->
<div style="height:10px" align="center">
        <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
        <script language="JavaScript" src="../inc/menu.js"></script>
        <!-- items structure. menu hierarchy and links are stored there -->
        <!-- files with geometry and styles structures -->
        <script language="JavaScript" src="../inc/menu_tpl.js"></script>
        <script language="JavaScript" src="../inc/menu_items_review.js"></script>
        <script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
    </div>
</div>
<br />
<br />

<form action="" method="post" name="form1" id="form1" onSubmit="return verifyForm()"> 

<br />
<br />

<?php
// Include shared functions
include "./mhci_prereqsFunctions.inc.php";

$studentLuId = $_REQUEST['studentLuId']; 

makeNavigation($studentLuId);

$studentName = getStudentName($studentLuId); 
?>

<div id="pageTitle" class="pageHeaderText">
    Statistics Prerequisite for <?php echo $studentName; ?>
</div>
<br/><br/><br/>

<?php
include "../inc/mhci_prereqsAssessmentForm.class.php";
include "../inc/mhci_prereqsCourseForm.class.php";
include "../inc/mhci_prereqsCourseFormController.class.php";
include "../inc/mhci_prereqsReferencesSummary.class.php";
include "../inc/mhci_prereqsConversationForm.class.php";
$view = "reviewer";
$reviewerLuId = $_SESSION['A_userid']; 
?>

<div id="assessmentForm">
<?php
$assessmentForm = new MHCI_PrereqsAssessmentForm($studentLuId, $view, "statistics");
$assessmentForm->render();
?>
</div>

<hr />
<br />
<hr />

<div id="courseForms">
<?php
$courseFormController = new MHCI_PrereqsCourseFormController($studentLuId, $view, "statistics", TRUE);
$courseFormController->render();
?>
</div>

<br />
<hr />

<div id="references">
<?php
$referencesSummary = new MHCI_PrereqsReferencesSummary($studentLuId, $view);
$referencesSummary->render();
?>
</div>

<br />
<hr />


<div id="conversationForm">
<?php
$conversationForm = new MHCI_PrereqsConversationForm($studentLuId, $reviewerLuId, $view, "statistics");
$conversationForm->render();
?>
</div>

<hr/>



</body>
</html>