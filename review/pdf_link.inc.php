<?php

// include the db classes
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once "../classes/class.db_applicant.php";

// get the applicant's guid
$db_applicant = new DB_Applicant();
$applicant_info = $db_applicant->getApplicantInfo($appid);
$applicant_guid = $applicant_info[0]['guid'];

// set up the pdf href
$pdf_path = $datafileroot . "/" . $applicant_guid . "/" . $appid . "_merged.pdf";
//$pdf_path = "/htdocs/data/2009/" . $applicant_guid . "/" . $appid . "_merged.pdf";

$pdf_link = '<a target="_blank" href="' . $pdf_path . '">View merged application (PDF)</a>';

// if the pdf file is already there, just show the link
if ( file_exists($pdf_path) ) {

    $pdf_link_div_html = $pdf_link;
    
    // PLB added 01/05/09
    // Get/display the info for files that failed in the merge process
    include_once "../classes/class.db_datafiles.php";
    $db_datafiles = new DB_Datafiles();
    $failed_merges = $db_datafiles->getFailedMergeFiles($appid);
    
    // Display warning if any files failed the merge    
    if ( count($failed_merges) > 0 ) {
    
        $pdf_link_div_html .= '<br />Missing from merged application: ';
        
        foreach ($failed_merges as $failed_merge) {
            
            $file = $failed_merge['file'];
            $filepath = $datafileroot . "/" . strstr($file, $applicant_guid);
            $pdf_link_div_html .= '<a target="_blank" href="' . $filepath . '">' . basename($file) . '</a>&nbsp; '; 
            
        }        
        
    }
    

    
} else {
    // do an ajax request to assemble it and show the link when done
    $pdf_link_div_html = "";    

// PLB removed on-the-fly pdf creation 01/12/09
/*
<script language="javascript">

$.get("./pdf_conversion/mergePdf.php", {
            id: <?= $uid ?>, 
            application_id: <?= $appid ?>
            },
            function(data){
                $("#pdf_link").html('<?= $pdf_link ?>');      
            },
            "text");


</script>
*/

} ?>

<table width="700">
    <tr>
        <td align="left">
        <div id="pdf_link">
            <?= $pdf_link_div_html ?>
        </div>
        </td>
    </tr>
</table>
<br />