<?php

/*
Class for application publications
Primary tables: publication
*/

class DB_Publications extends DB_Applyweb
{

    //protected $application_id;
    
    function getPublications($application_id) {
    
        $query = "SELECT publication.application_id, 
                    publication.*
                    FROM publication                
                    WHERE publication.application_id = " . $application_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }


    function updatePublicationStatus($publication_id, $status) {
   
       $query = "UPDATE publication SET status = '" . $status . "'
                WHERE id = " . $publication_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status; 
   }    
    
}

?>
