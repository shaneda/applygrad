<?php

/*
Class for retrieving application period info
*/

class DB_ApplicationPeriod extends DB_Applyweb
{  

    // function to get application period info for a department
    function getApplicationPeriodsByDepartment($department_id) {
    
        $period_query = "SELECT * FROM application_periods";
        $period_query .= " WHERE department_id = " . $department_id;
        $period_query .= " ORDER BY end_date DESC, start_date DESC";
        $period_array = $this->handleSelectQuery($period_query);    
        return $period_array;
        
    }   



    // function to get info for a single application period
    function getApplicationPeriod($application_period_id) {
    
        $period_query = "SELECT * FROM application_periods WHERE application_period_id = " . $application_period_id;
        $period_array = $this->handleSelectQuery($period_query);    
        return $period_array;
        
    }   


}
  
?>
