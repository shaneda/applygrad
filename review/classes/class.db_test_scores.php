<?php
/*
Class for test scores
Primary table(s): grescore, gresubjectscore
*/

class DB_TestScores extends DB_Applyweb
{

    //protected $application_id;

    //##### GRE General ##############
    
    function getGREGeneral($application_id) {
        
        $query = "SELECT application.id as application_id, grescore.* 
                    FROM grescore 
                    inner join application on grescore.application_id = application.id
                    WHERE application_id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array; 
    }
        

   function updateGREGeneralReceived($grescore_id, $score_received) {
   
       $query = "UPDATE grescore SET scorereceived=" . $score_received . 
                " WHERE id=" . $grescore_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;  
   }


   function updateGREGeneralScore($grescore_id, 
                                    $verbal_score, $verbal_percentile, 
                                    $quantitative_score, $quantitative_percentile,
                                    $analytical_score, $analytical_percentile,
                                    $writing_score, $writing_percentile) {
   
       // writing score (analyticalwritingscore in the db) is a decimal
       // whereas every other score element is a char/string
       // set it to null if it's empty
       if ( intval($writing_score) == 0 ) {
            $writing_score = "NULL";
       }
                                        
       $query = "UPDATE grescore SET 
                verbalscore='" . $verbal_score .
                "', verbalpercentile='" . $verbal_percentile . 
                "', quantitativescore='" . $quantitative_score .
                "', quantitativepercentile='" . $quantitative_percentile .
                "', analyticalscore='" . $analytical_score .
                "', analyticalpercentile='" . $analytical_percentile .
                "', analyticalwritingscore=" . $writing_score .
                ", analyticalwritingpercentile='" . $writing_percentile . 
                "' WHERE id=" . $grescore_id;

       $status = $this->handleUpdateQuery($query);

       return $status;
   }


    //##### GRE Subject ##############  

    function getGRESubject($application_id) {
        
        $query = "SELECT * FROM gresubjectscore
                    WHERE (
                    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%'  AND testdate IS NOT NULL)
                    OR (name != '' AND name IS NOT NULL)
                    OR (score !=0 AND score IS NOT NULL)
                    OR (percentile != 0 AND percentile IS NOT NULL)
                    ) 
                    AND application_id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array; 
    }

    
   function updateGRESubjectReceived($gresubjectscore_id, $score_received) {
   
       $query = "UPDATE gresubjectscore SET scorereceived=" . $score_received . 
                " WHERE id=" . $gresubjectscore_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }


   function updateGRESubjectScore($gresubjectscore_id, 
                                    $subject_name,
                                    $subject_score, 
                                    $subject_percentile) {
                                        
       $query = "UPDATE gresubjectscore SET 
                name = '" . $subject_name .
                "', score = '" . $subject_score .
                "', percentile = '" . $subject_percentile . 
                "' WHERE id=" . $gresubjectscore_id;

       $status = $this->handleUpdateQuery($query);

       return $status;
   }

   
    //##### TOEFL ##############  

    function getTOEFL($application_id) {
        
        $query = "SELECT * FROM toefl
                    WHERE ( 
                    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%'  AND testdate IS NOT NULL)
                    OR (toefl.section1 != 0 AND toefl.section1 IS NOT NULL)
                    OR (toefl.section2 != 0 AND toefl.section2 IS NOT NULL)
                    OR (toefl.section3 != 0 AND toefl.section3 IS NOT NULL)
                    OR (toefl.essay != 0 AND toefl.essay IS NOT NULL)
                    OR (toefl.total != 0 AND toefl.total IS NOT NULL)
                    ) AND application_id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array; 
    }


    function updateTOEFLReceived($toeflscore_id, $score_received) {

       $query = "UPDATE toefl SET scorereceived=" . $score_received . 
                " WHERE id=" . $toeflscore_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
    }


    function updateTOEFLScore($toeflscore_id, $section1_score, 
                                $section2_score, $section3_score, 
                                $essay_score, $total_score) {
       
       if ( intval($section1_score) == 0 ) {
            $section1_score = "NULL";
       }
       if ( intval($section2_score) == 0 ) {
            $section2_score = "NULL";
       }       
       if ( intval($section3_score) == 0 ) {
            $section3_score = "NULL";
       }
       if ( intval($essay_score) == 0 ) {
            $essay_score = "NULL";
       }                            
       if ( intval($total_score) == 0 ) {
            $total_score = "NULL";
       }
       $query = "UPDATE toefl SET 
                section1=" . $section1_score . 
                ", section2=" . $section2_score .
                ", section3=" . $section3_score .
                ", essay=" . $essay_score .
                ", total=" . $total_score . 
                " WHERE id=" . $toeflscore_id;

       $status = $this->handleUpdateQuery($query);
       
       return $status;
    }

   
//###### IELTS Score
      
    function getIELTS($application_id) {
        
        $query = "SELECT * FROM ieltsscore
                    WHERE (
                    (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
                    OR (listeningscore != 0 AND listeningscore IS NOT NULL)
                    OR (readingscore != 0 AND readingscore IS NOT NULL)
                    OR (writingscore != 0 AND writingscore IS NOT NULL)
                    OR (speakingscore != 0 AND speakingscore IS NOT NULL)
                    OR (overallscore != 0 AND overallscore IS NOT NULL)
                    ) AND application_id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;         
    }

    function updateIELTSReceived($ieltsscore_id, $score_received) {

        $query = "UPDATE ieltsscore SET scorereceived = " . $score_received . 
                " WHERE id = " . $ieltsscore_id;

        $status = $this->handleUpdateQuery($query);

        return $status;        
    } 
    
    function updateIELTSScore($ieltsscore_id, $listening_score, $reading_score, 
                                    $writing_score, $speaking_score, $overall_score) 
    {

        if ( intval($listening_score) == 0 ) {
            $listening_score = "NULL";
        } else {
            $listening_score = number_format( floatval($listening_score), 1 );   
        }
        if ( intval($reading_score) == 0 ) {
            $reading_score = "NULL";
        } else {
            $reading_score = number_format( floatval($reading_score), 1 );   
        }
        if ( intval($writing_score) == 0 ) {
            $writing_score = "NULL";
        } else {
            $writing_score = number_format( floatval($writing_score), 1 );   
        }
        if ( intval($speaking_score) == 0 ) {
            $speaking_score = "NULL";
        } else {
            $speaking_score = number_format( floatval($speaking_score), 1 );   
        }
        if ( intval($overall_score) == 0 ) {
            $overall_score = "NULL";
        } else {
            $overall_score = number_format( floatval($overall_score), 1 );   
        }

        $query = "UPDATE ieltsscore SET 
                    listeningscore = " . $listening_score . ",
                    readingscore = " . $reading_score . ",
                    writingscore = " . $writing_score . ",
                    speakingscore = " . $speaking_score . ",
                    overallscore = " . $overall_score . "
                    WHERE  id = " . $ieltsscore_id;
        
        $status = $this->handleUpdateQuery($query);

        return $status;
    }    

// ####### GMAT

    function getGMAT($application_id) {
        
        $query = "SELECT * FROM gmatscore 
                    WHERE
                    (
                        (testdate != '0000-00-00' AND testdate NOT LIKE '0000-00-01%' AND testdate IS NOT NULL)
                        OR (verbalscore IS NOT NULL)
                        OR (verbalpercentile IS NOT NULL)
                        OR (quantitativescore IS NOT NULL)
                        OR (quantitativepercentile IS NOT NULL)
                        OR (totalscore IS NOT NULL)
                        OR (totalpercentile IS NOT NULL) 
                        OR (analyticalwritingscore IS NOT NULL) 
                        OR (analyticalwritingpercentile IS NOT NULL)
                    )
                    AND application_id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array; 
    }
        

   function updateGMATReceived($gmatscore_id, $score_received) {
   
       $query = "UPDATE gmatscore SET scorereceived=" . $score_received . 
                " WHERE id=" . $gmatscore_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;  
   }


   function updateGMATScore($gmatscore_id, $verbal_score, $verbal_percentile, 
                                $quantitative_score, $quantitative_percentile,
                                $total_score, $total_percentile,
                                $writing_score, $writing_percentile) {
   
       // writing score (analyticalwritingscore in the db) is a decimal
       // whereas every other score element is a char/string
       // set it to null if it's empty
       if ( intval($writing_score) == 0 ) {
            $writing_score = "NULL";
       }
                                        
       $query = "UPDATE gmatscore SET 
                verbalscore=" . $verbal_score .
                ", verbalpercentile=" . $verbal_percentile . 
                ", quantitativescore=" . $quantitative_score .
                ", quantitativepercentile=" . $quantitative_percentile .
                ", totalscore=" . $total_score .
                ", totalpercentile=" . $total_percentile .
                ", analyticalwritingscore=" . $writing_score .
                ", analyticalwritingpercentile=" . $writing_percentile . 
                " WHERE id=" . $gmatscore_id;

       $status = $this->handleUpdateQuery($query);

       return $status;
   }



}
?>