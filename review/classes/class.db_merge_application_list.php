<?php

/*
Class for retrieving lists of applications
*/

class DB_ApplicationList extends DB_Applyweb
{

    /*
    // Return list of application ids for a domain
    */
    function getByDomain($domain_id) {

        $query = 'SELECT application.id
            FROM lu_users_usertypes
            inner join users on users.id = lu_users_usertypes.user_id
            inner join application on application.user_id = lu_users_usertypes.id            
            inner join lu_application_programs on lu_application_programs.application_id = application.id
            inner join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
            inner join lu_domain_department on lu_domain_department.department_id = lu_programs_departments.department_id
            WHERE lu_users_usertypes.usertype_id = 5
            AND lu_domain_department.domain_id = ' . $domain_id;
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }
    

    /*
    // Return list of ids for all submitted applications
    */
    function getSubmitted() {

        $query = 'SELECT DISTINCT application.id
            FROM lu_users_usertypes
            inner join application on application.user_id = lu_users_usertypes.id
            inner join users on lu_users_usertypes.user_id = users.id 
            WHERE lu_users_usertypes.usertype_id = 5
            AND application.submitted = 1';
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }


    /*
    // Return list of ids for all round 2 applications
    */
    function getSubmittedRound2() {

        $query = "SELECT DISTINCT application.id
            FROM lu_users_usertypes
            inner join application on application.user_id = lu_users_usertypes.id
            inner join users on lu_users_usertypes.user_id = users.id 
            
            inner join (
                SELECT application_id, MIN(round2) as round2 
                    FROM 
                    (
                        SELECT * FROM review
                        WHERE round = 1
                    ) as round1_reviews
                    GROUP BY application_id
                    HAVING round2 != 0
                    UNION
                    SELECT DISTINCT application_id, '1' as round2
                    FROM lu_application_programs
                    WHERE round2 = 1
            ) as round2 on round2.application_id = application.id     
                    
            WHERE lu_users_usertypes.usertype_id = 5
            AND application.submitted = 1";
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }


    /*
    // Return list of ids for all submitted applications
    // that have not been converted/merged yet.
    */
    function getSubmittedUnconverted() {

        $query = 'SELECT DISTINCT application.id
            FROM lu_users_usertypes
            inner join application on application.user_id = lu_users_usertypes.id
            inner join users on lu_users_usertypes.user_id = users.id 
            WHERE lu_users_usertypes.usertype_id = 5
            AND application.submitted = 1
            AND application.id NOT IN
            (SELECT application_id FROM searchText)';
      
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;  
    }



    // Not currently used (?)
    function getCsdRound1() {
    
        $query = "select   lu_users_usertypes.id,
        concat(users.lastname,', ', users.firstname) as name,
        countries.name as ctzn,
        gender as m_f,

        group_concat(distinct concat(degree.name, ' ', programs.linkword, ' ', fieldsofstudy.name) separator '<br />') as program,
        group_concat(distinct concat(interest.name,' (',lu_application_interest.choice+1,')')  order by lu_application_interest.choice  separator '|') as interest,
        group_concat(distinct institutes.name separator '<br />') as undergrad ,
 
            concat(',',            
            group_concat( distinct 
            
            
                    if( review.supplemental_review is null, review.reviewer_id,'')    
                
                  ),',') as revrid,
 group_concat( distinct 
                    case lu_user_department.department_id when 1 then 
                                          concat(substring(users2.firstname,1,1), substring(users2.lastname,1,1))
                    end 
                                        separator '<br />')  as revr ,
group_concat(distinct if(revgroup.department_id = 1 , revgroup.name , '')  separator '|') as groups,
  group_concat( distinct 
                case lu_user_department.department_id when 1 then 
                if((1=3. || 1=6),

                    concat(substring(
                        if( review.supplemental_review is null, round(review.point,1),''),1,4),
                        if( review.supplemental_review is null, concat('/', round(review.point2,1) ),'')
                    ),  
                      concat('',
                        if(review.round2 is null,'',case review.round2 when 1 then 'yes' when 0 then 'no' end), ': ', 
                        substring(if( review.supplemental_review is null, review.point,''),1,3)
                    )
                        
                )
                    
                end  
                                separator '<br />')  as round2 from lu_users_usertypes
        inner join users on users.id = lu_users_usertypes.user_id
        left outer join users_info on users_info.user_id = lu_users_usertypes.id
        left outer join countries on countries.id = users_info.cit_country
        inner join application on application.user_id = lu_users_usertypes.id
        inner join lu_application_programs on lu_application_programs.application_id = application.id
        inner join programs on programs.id = lu_application_programs.program_id
        inner join degree on degree.id = programs.degree_id
        inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
        left outer join usersinst on usersinst.user_id = lu_users_usertypes.id and usersinst.educationtype=1
        left outer join institutes on institutes.id = usersinst.institute_id
        left outer join lu_programs_departments on lu_programs_departments.program_id = lu_application_programs.program_id
        left outer join lu_application_interest on lu_application_interest.app_program_id = lu_application_programs.id
        left outer join interest on interest.id = lu_application_interest.interest_id

        left outer join review on review.application_id = application.id 

        /*reviewers - round 1*/
        left outer join lu_users_usertypes as lu_users_usertypes2 on lu_users_usertypes2.id = review.reviewer_id and review.round = 1
        left outer join lu_user_department on lu_user_department.user_id = lu_users_usertypes2.id
        left outer join users as users2 on users2.id = lu_users_usertypes2.user_id
        /*review groups - round 1*/
        left outer join lu_reviewer_groups on lu_reviewer_groups.reviewer_id = lu_users_usertypes2.id and lu_reviewer_groups.round = 1
        
        /*application groups - round 1*/
        left outer join lu_application_groups as lu_application_groups on lu_application_groups.application_id = application.id
        and lu_application_groups.round = 1 
        
        left outer join revgroup as revgroup on revgroup.id = lu_application_groups.group_id
           where application.submitted=1
        and review.supplemental_review IS NULL
        and lu_programs_departments.department_id=1 
        and (lu_programs_departments.department_id IS NULL or lu_programs_departments.department_id)
        group by  users.id  
        order BY users.lastname,users.firstname"; 
     
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;     
        
    }


}

?>
