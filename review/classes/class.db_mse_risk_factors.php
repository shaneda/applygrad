<?php

/*
Class for MSE risk factors
Primary tables: mse_risk_factors
*/

class DB_MseRiskFactors extends DB_Applyweb
{

    //protected $application_id;
   
    function getRiskFactors($application_id, $reviewer_id) {
    
        $query = "SELECT * FROM mse_risk_factors                 
                    WHERE application_id = " . $application_id . "
                    AND reviewer_id =  " . $reviewer_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    function updateRiskFactors($application_id, $reviewer_id, $language, $experience, $academic, $other) {
    
        $query = "REPLACE INTO mse_risk_factors VALUES ("
                    . $application_id . ","
                    . $reviewer_id . ","
                    . $language . ","
                    . $experience . ","
                    . $academic . ","
                    . $other .
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }
    

    
}

?>
