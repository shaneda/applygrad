<?php
/*
Class for application admin info
Primary table(s): application
*/

class DB_Application extends DB_Applyweb
{
    
    //protected $application_id;


    function getApplication($application_id) {
        
    }
   
    function updateSubmitted($application_id, $submitted) {
   
        if ($submitted) {
            $submitted_date = "NOW()";
        } else {
            $submitted_date = "NULL";
        }
        
        $query = "UPDATE application SET submitted = " . $submitted .
                ", submitted_date = " . $submitted_date . 
                " WHERE id = " . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $query; 
   }


   function updateComplete($application_id, $complete) {
   
       $query = "UPDATE application SET sent_to_program=" . $complete . 
                " WHERE id=" . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }

   
   function updatePromotionStatus($application_id, $program_id, $promotion_status) {
   
       $query = "UPDATE lu_application_programs" .
                " SET round2 = '" . $promotion_status . "' 
                 WHERE application_id = " . $application_id .
                " AND program_id = " . $program_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }
   


}

?>
