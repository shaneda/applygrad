<?php

/*
Class for recommendations
Primary tables: recommend, users, users_info, datafileinfo
*/

class DB_Recommendations extends DB_Applyweb
{

    //protected $application_id;
    
    function getRecommendations($application_id) {
    
        $query = "SELECT recommend.application_id,
                    recommend.id as recommendation_id,
        			case
                        when recommendtype = 1 then 'normal'
                        when recommendtype = 2 then 'academic'
                        when recommendtype = 3 then 'industrial'
                        end as recommendtype_name,
                    recommend.*, 
                    users.*,
                    users_info.*,
                    concat( 'recommendation_', datafileinfo.userdata, '.', datafileinfo.extension ) as datafile
                    FROM recommend
                    left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
                    left outer join users on users.id = lu_users_usertypes.user_id
                    left outer join users_info on users_info.user_id = lu_users_usertypes.id
                    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id                  
                    WHERE recommend.application_id = " . $application_id . " 
                    ORDER BY users.lastname";
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    function getRecommendationById($recommendation_id) {
    
        $query = "SELECT recommend.*,
                    case
                        when recommendtype = 1 then 'normal'
                        when recommendtype = 2 then 'academic'
                        when recommendtype = 3 then 'industrial'
                        end as recommendtype_name,
                    users.*,
                    users_info.*,
                    concat( 'recommendation_', datafileinfo.userdata, '.', datafileinfo.extension ) as datafile
                    FROM recommend
                    left outer join lu_users_usertypes on lu_users_usertypes.id = recommend.rec_user_id
                    left outer join users on users.id = lu_users_usertypes.user_id
                    left outer join users_info on users_info.user_id = lu_users_usertypes.id
                    left outer join datafileinfo on datafileinfo.id = recommend.datafile_id                  
                    WHERE recommend.id = " . $recommendation_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }
    
    
}

?>
