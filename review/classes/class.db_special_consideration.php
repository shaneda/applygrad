<?php

/*
Class for special consideration request
Primary tables: application, phone_screens
*/

class DB_SpecialConsideration extends DB_Applyweb
{

    //protected $application_id;

    // Check whether special consideration has been requested by *anyone*.
    // This should always return a record for a valid application id.
    function getSpecialConsiderationRequested($application_id) {
    
        /*
        $query = "SELECT application_id,
                    MAX(special_consideration) as special_consideration_requested
                    FROM special_consideration
                    WHERE application_id = " . $application_id . ";
                    GROUP BY application_id";
        */
        $query = "SELECT application.id,
                    IFNULL(special_consideration.requested, 0) as special_consideration_requested
                    FROM application
                    LEFT OUTER JOIN (
                        SELECT application_id, 
                        MAX(special_consideration) as requested
                        FROM special_consideration 
                        GROUP BY application_id            
                    ) as special_consideration ON application.id = special_consideration.application_id
                    WHERE application.id = " . $application_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }
    

    
    // Check whether a particular reviewer has requested special consideration.
    function getReviewerSpecialConsideration($application_id, $reviewer_id) {
    
        $query = "SELECT * FROM special_consideration                
                    WHERE application_id = " . $application_id . "
                    AND reviewer_id =  " . $reviewer_id;
    
        $results_array = $this->handleSelectQuery($query);
    
        return $results_array;
    }

    
    function updateSpecialConsideration($application_id, $reviewer_id, $special_consideration) {
    
        $query = "REPLACE INTO special_consideration VALUES ("
                    . $application_id . ","
                    . $reviewer_id . ","
                    . $special_consideration .
                    ")";
    
        $status = $this->handleInsertQuery($query);
    
        return $status;
    }
    

    
}

?>
