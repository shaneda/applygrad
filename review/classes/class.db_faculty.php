<?php
/*
 * Class for faculty biographic/demographic info
 * Primary tables: users, usertypes, department
 */

class DB_Faculty extends DB_Applyweb
{
    
    function getFacultyInfo( $userID = FALSE, $department ) {
        
        if ( $userID )
   		{
        	// get specific faculty
        	$specificFacultyQuery = " AND lu_users_usertypes.id = $userID ";
        } else {
        	$specificFacultyQuery = "";
        } 
        
		//get all faculty
		$query = "
			SELECT lu_users_usertypes.id, firstname, lastname, email, usertypes.name AS usertype, department.name AS dept
			FROM lu_users_usertypes
			LEFT OUTER JOIN users ON users.id = lu_users_usertypes.user_id
			LEFT OUTER JOIN usertypes ON usertypes.id = lu_users_usertypes.usertype_id
			LEFT OUTER JOIN lu_user_department ON lu_user_department.user_id = lu_users_usertypes.id
			LEFT OUTER JOIN department ON department.id = lu_user_department.department_id
			WHERE lu_users_usertypes.usertype_id = 3 
				AND lu_user_department.department_id = $department
				$specificFacultyQuery
			ORDER BY users.lastname, users.firstname
		";
		
		// different user types
		//WHERE (lu_users_usertypes.usertype_id = 1 OR lu_users_usertypes.usertype_id = 2 OR lu_users_usertypes.usertype_id = 3)
		
		$results_array = $this->handleSelectQuery($query);
        return $results_array;     
    }
    

    
}

?>