<?php
/*
Class for payment-related info
Primary table(s): application
*/

class DB_Payment extends DB_Applyweb
{
    
    //protected $application_id;


    function getPayment($application_id) {
        
        $query = "SELECT application.id,
                    application.paid, 
                    application.paymentdate, 
                    application.paymentamount,
                    application.waive 
                    FROM application 
                    WHERE application.id = " . $application_id;
        
        $results_array = $this->handleSelectQuery($query);
        
        return $results_array;
    }
   

    function updateWaivePayment($application_id, $waive_payment) {
   
       $query = "UPDATE application SET waive=" . $waive_payment . 
                " WHERE id=" . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status; 
   }


   function updateFeePaid($application_id, $fee_paid, $amt) {
   
        if ($fee_paid) {
            $payment_date = "NOW()";
        } else {
            $payment_date = "NULL";
        }
       
       $query = "UPDATE application SET paid = " . $fee_paid .
                ", paymentdate = " . $payment_date .
                ", paymentamount = " . $amt .  
                " WHERE id = " . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status;
   }

    
   function updatePaymentAmount($application_id, $payment_amount) {
   
       $query = "UPDATE application SET 
                paymentamount = " . $payment_amount .
                ", paid = 1" . 
                ", paymentdate = NOW()" . 
                " WHERE id = " . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status; 
   }
   
   function resetPayment($application_id) {
   
       $query = "UPDATE application SET
                paid = 0, 
                paymentdate = NULL,
                paymentamount = NULL,
                paymenttype = NULL
                WHERE id = " . $application_id;
       
       $status = $this->handleUpdateQuery($query);
       
       return $status; 
   }

   
   function getTotalFees($application_id) {
        
        $query = "SELECT programs.id,
                    degree.name as degreename,
                    fieldsofstudy.name as fieldname,
                    choice,
                    lu_application_programs.id as itemid,
                    programs.programprice,
                    programs.baseprice
                    FROM lu_application_programs
                    inner join programs on programs.id = lu_application_programs.program_id
                    inner join degree on degree.id = programs.degree_id
                    inner join fieldsofstudy on fieldsofstudy.id = programs.fieldofstudy_id
                    where lu_application_programs.application_id = ".$application_id." order by choice";
        
        $results_array = $this->handleSelectQuery($query);
        
        $total_fees = 0.0;
        $i = 0;
        foreach ($results_array as $row){

            if($i == 0) {
                $total_fees = $row['baseprice'];
            } else {
                $total_fees += $row['programprice'];
            }
            $i++;
        }
        
        $total_fees_array = array(
                        'application_id' => $application_id,
                        'total_fees' => $total_fees       
                        );

        
        return $total_fees_array;
    }

}

?>
