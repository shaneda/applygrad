<?php
include_once '../inc/config.php';
include_once '../inc/session_admin.php';
include_once '../inc/db_connect.php';
include '../classes/DB_Applyweb/class.DB_Applyweb.php';
include '../classes/DB_Applyweb/class.DB_Period.php';
include '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include '../classes/class.Period.php';

/*
* For testing:
* periodId 358
* departmentId 1
* reviewerId 37862
*/
$periodId = filter_input(INPUT_GET, "periodId", FILTER_VALIDATE_INT);
$departmentId = filter_input(INPUT_GET, "departmentId", FILTER_VALIDATE_INT);
$reviewerId = filter_input(INPUT_GET, "reviewerId", FILTER_VALIDATE_INT);

/*
* Graph data 
*/
$scoreKey = array(
    1 => "Top 10 (1%)",
    2 => "Top 25 (2.5%)",
    3 => "Top 50 (5%)",
    4 => "Top 100 (10%)",
    5 => "Top 250 (25%)",
    6 => "Bottom 75%",
    7 => "Bottom 50%"
);

$round1DataTable = array(
    'cols' => array(
        array('id' => '', 'label' => 'Score', 'pattern' => '', 'type' => 'string'),
        array('id' => '', 'label' => 'Count', 'pattern' => '', 'type' => 'number')       
    ),
    'rows' => array()    
);

$round2DataTable = array(
    'cols' => array(
        array('id' => '', 'label' => 'Score', 'pattern' => '', 'type' => 'string'),
        array('id' => '', 'label' => 'Count', 'pattern' => '', 'type' => 'number')       
    ),
    'rows' => array()    
);

$allRoundsDataTable = array(
    'cols' => array(
        array('id' => '', 'label' => 'Score', 'pattern' => '', 'type' => 'string'),
        array('id' => '', 'label' => 'Count', 'pattern' => '', 'type' => 'number')       
    ),
    'rows' => array()    
);

foreach($scoreKey as $key => $value)
{
    $round1DataTable['rows'][] = array(
        'c' => array(
            array('v' => $value, 'f' => null),
            array('v' => 0, 'f' => null)
        )
    ); 
    
    $round2DataTable['rows'][] = array(
        'c' => array(
            array('v' => $value, 'f' => null),
            array('v' => 0, 'f' => null)
        )
    ); 
    
    $allRoundsDataTable['rows'][] = array(
        'c' => array(
            array('v' => $value, 'f' => null),
            array('v' => 0, 'f' => null)
        )
    );    
}

// Round 1
$round1Query = "SELECT point AS score, COUNT(point) AS score_count
    FROM review
    INNER JOIN period_application 
        ON review.application_id = period_application.application_id
        AND period_application.period_id = " . $periodId . "
    WHERE round = 1 
    AND department_id = " . $departmentId . "
    AND reviewer_id = " . $reviewerId . "
    GROUP BY score
    ORDER BY score";
$round1Result = mysql_query($round1Query);
$count = $round1Count = 0;
while ($row = mysql_fetch_array($round1Result))
{
    if (array_key_exists(intval($row['score']), $scoreKey))
    {
        $heading = $scoreKey[intval($row['score'])];
        $count = $row['score_count'];
        $rowIndex = intval($row['score']) - 1;
        $round1DataTable['rows'][$rowIndex]['c'][1]['v'] = intval($count);
        $round1Count += $count; 
    }
}
$round1Title = 'Round 1 Scores, n = ' . $round1Count; 

// Round 2
$round2Query = "SELECT point AS score, COUNT(point) AS score_count
    FROM review
    INNER JOIN period_application 
        ON review.application_id = period_application.application_id
        AND period_application.period_id = " . $periodId . "
    WHERE round = 2
    AND department_id = " . $departmentId . "
    AND reviewer_id = " . $reviewerId . "
    GROUP BY score
    ORDER BY score";
$round2Result = mysql_query($round2Query);
$count = $round2Count = 0;
while ($row = mysql_fetch_array($round2Result))
{
    if (array_key_exists(intval($row['score']), $scoreKey))
    {
        $heading = $scoreKey[intval($row['score'])];
        $count = $row['score_count'];
        $rowIndex = intval($row['score']) - 1;
        $round2DataTable['rows'][$rowIndex]['c'][1]['v'] = intval($count);
        $round2Count += $count; 
    }
}
$round2Title = 'Round 2 Scores, n = ' . $round2Count; 

// All Rounds
$allRoundsQuery = "SELECT point AS score, COUNT(point) AS score_count
    FROM review
    INNER JOIN period_application 
        ON review.application_id = period_application.application_id
        AND period_application.period_id = " . $periodId . "
    WHERE department_id = " . $departmentId . "
    AND reviewer_id = " . $reviewerId . "
    GROUP BY score
    ORDER BY score";
$allRoundsResult = mysql_query($allRoundsQuery);
$count = $allRoundsCount = 0;
while ($row = mysql_fetch_array($allRoundsResult))
{
    if (array_key_exists(intval($row['score']), $scoreKey))
    {
        $heading = $scoreKey[intval($row['score'])];
        $count = $row['score_count'];
        $rowIndex = intval($row['score']) - 1;
        $allRoundsDataTable['rows'][$rowIndex]['c'][1]['v'] = intval($count);
        $allRoundsCount += $count; 
    }
}
$allRoundsTitle = 'All Scores, n = ' . $allRoundsCount; 

/*
* Template data 
*/
$departmentName = getDepartmentName($departmentId);
$reviewerName = getReviewerName($reviewerId);
if ( $periodId ) 
{
    $period = new Period($periodId);
    $submissionPeriods = $period->getChildPeriods(2);
    $submissionPeriodCount = count($submissionPeriods);
    if ($submissionPeriodCount > 0) {
        $displayPeriod = $submissionPeriods[0];  
    } else {
        $displayPeriod = $period;
    }
    $startDate = $displayPeriod->getStartDate('Y-m-d');
    $endDate = $displayPeriod->getEndDate('Y-m-d');
    $periodDates = $startDate . '&nbsp;&#8211;&nbsp;' . $endDate; 
    $periodName = $period->getName();
    $periodDescription = $period->getDescription();
    $applicationPeriodDisplay = '<br/>';
    if ($periodName) {
        $applicationPeriodDisplay .= $periodName . ' (' . $periodDates . ')';
    } elseif ($periodDescription) {
        $applicationPeriodDisplay .= $periodDescription . ' (' . $periodDates . ')';    
    } else {
        $applicationPeriodDisplay .= $periodDates;     
    }
}

$pageTitle = 'Score Histograms';

$pageCssFiles = array(
    '../css/reviewApplications.css'
    );

$pageJavascriptFiles = array();

include '../inc/tpl.pageHeader.php';
?>

<div id="pageHeading" style="clear: both;">
    <div id="departmentName"><?php echo $departmentName . $applicationPeriodDisplay; ?></div>    
    <div id="sectionTitle">Score Histograms, <?php echo $reviewerName; ?></div>
</div>
        
<!--Divs that will hold the charts-->
<div id="round1_div" style="clear: both;"></div>
<div id="round2_div" style="clear: both;"></div>
<div id="all_rounds_div" style="clear: both;"></div>

<!-- 
https://google-developers.appspot.com/chart/interactive/docs/quick_start 
https://google-developers.appspot.com/chart/interactive/docs/php_example
-->
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

  // Load the Visualization API and the piechart package.
  google.load('visualization', '1.0', {'packages':['corechart']});
 
  // Set a callback to run when the Google Visualization API is loaded.
  google.setOnLoadCallback(drawCharts);

  // Callback that creates and populates a data table, 
  // instantiates the chart, passes in the data and draws it.
  function drawCharts() {
        drawRound1Chart();
        drawRound2Chart();
        drawAllRoundsChart();
  }
  function drawRound1Chart() {

      // Create the data table.
      var title = '<?php echo $round1Title; ?>';
      var jsonData = '<?php echo json_encode($round1DataTable) ?>';
      var data = new google.visualization.DataTable(jsonData);
      
      // Set chart options
      var options = {'title':title, 'width':600, 'height':450};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.BarChart(document.getElementById('round1_div'));
      chart.draw(data, options);
  }
  
  function drawRound2Chart() {

      // Create the data table.
      var title = '<?php echo $round2Title; ?>';
      var jsonData = '<?php echo json_encode($round2DataTable); ?>';
      var data = new google.visualization.DataTable(jsonData);
      
      // Set chart options
      var options = {'title':title, 'width':600, 'height':450};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.BarChart(document.getElementById('round2_div'));
      chart.draw(data, options);
  }
  
    function drawAllRoundsChart() {

      // Create the data table.
      var title = '<?php echo $allRoundsTitle; ?>';
      var jsonData = '<?php echo json_encode($allRoundsDataTable); ?>';
      var data = new google.visualization.DataTable(jsonData);
      
      // Set chart options
      var options = {'title':title, 'width':600, 'height':450};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.BarChart(document.getElementById('all_rounds_div'));
      chart.draw(data, options);
  }
</script>

<?php
include '../inc/tpl.pageFooter.php'; 

function getDepartmentName($department_id)
{
    $query = "SELECT name FROM department where id= $department_id";
    $result = mysql_query($query) or die(mysql_error());
    $department_name = "";
    while($row = mysql_fetch_array( $result ))
    {
        $department_name = $row['name'];
    }
    return $department_name;
}

function getReviewerName($reviewerId)
{
    $query = "SELECT CONCAT(users.firstname, ' ', users.lastname) AS name
        FROM users INNER JOIN lu_users_usertypes ON users.id = lu_users_usertypes.user_id
        WHERE lu_users_usertypes.id = " . $reviewerId;
    $result = mysql_query($query) or die(mysql_error());
    $reviewerName = "";
    while($row = mysql_fetch_array( $result ))
    {
        $reviewerName = $row['name'];
    }
    return $reviewerName;
}
?>