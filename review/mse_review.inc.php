<? 
if($_SESSION['A_usertypeid'] != 11){//HID FROM STUDENT CONTACT ?>

<? if($_SESSION['A_usertypeid'] != 12){//HIDE FROM FAC CONTATCS ?>
<!-- REVIEW STUFF GOES HERE -->

<!-- PLB added div -->
<div style="margin-bottom: 5px;">

<? if($view == 2) { 

// Hidden versions of "active" form inputs are commented out.    
    
?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<!--<input name="comments" type="hidden" value="<?=$comments?>">-->
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">
<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">
<!--<input name="private_comments" type="hidden" value="<?=$private_comments?>"> -->
<? if($round == 2){?>
<input name="round2" type="hidden" value="<?=$round2?>">
<? } ?>
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">
<input name="recruited" type="hidden" value="<?=$recruited?>">
<input name="grad_name" type="hidden" value="<?=$grad_name?>">
<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">
<input name="advise_time" type="hidden" value="<?=$advise_time?>">
<input name="commit_money" type="hidden" value="<?=$commit_money?>">
<input name="fund_source" type="hidden" value="<?=$fund_source?>">
<input name="chkSemiblind" type="hidden" value="<?=$semiblind_review?>">
<?php  
// PLB added test to disable semi-blind control for MSE-MSIT departments 1/21/10.
$semiblindDisabled = '';
if (($thisDept == 18 || $thisDept == 19 ||
    $thisDept == 47 || $thisDept == 48 || $thisDept == 52) 
    && $_GET['r'] == 1)
{
    if ($_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1) {
        // Disable for everyone but su and admin
        $semiblindDisabled = 'disabled';    
    }          
}
?>
<input name='btnSemiblind' id='btnSemiblind' type='Submit' value='Toggle Semi-blind Review' <?php echo $semiblindDisabled; ?> >
<input name="showDecision" type="hidden" value="<?=$showDecision?>">

<? 
// PLB added save button here
print "&nbsp;&nbsp;";

if (($thisDept == 18 || $thisDept == 19 ||
    $thisDept == 47 || $thisDept == 48 || $thisDept == 52) 
    && $_GET['r'] == 2 && $_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
    {
       showEditText("Save", "button", "btnSubmit", false, false, array(array()), false); 
    } else
    showEditText("Save", "button", "btnSubmit", $allowEdit); 

// also added close div
?>
</div>


<table width="700" border="0"  cellspacing="0" cellpadding="0">
   <? if($semiblind_review != 1){ ?>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Committee <br>
      Scores:</strong></td>
    <td valign="top"><strong>
    <? 
    // DAS added the risk factors to the individual reviewers
    include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
    include_once "../classes/class.db_mse_risk_factors.php";
    
    for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept )//NOT A SUPPLEMENTAL OR FAC VOTE
        {
            ?><input name="revdept" type="hidden" value="<?=$committeeReviews[$i][32]?>"><?
            echo $committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5]  ;
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            //1st score
            ?>
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][10]?>">
            <input name="score1" type="hidden" value="<?=$committeeReviews[$i][11]?>">
            <?
            if($thisDept == 3 )
            {
                echo " Score1: ". $committeeReviews[$i][10]. " Score2: ".$committeeReviews[$i][11];//show point 2
            }else
            {
                // PLB changed 01/22/09
                //echo " Score: ". $committeeReviews[$i][10];//show point 1
                echo " Evaluation: ". $committeeReviews[$i][10];
            }

            //1st score
            if($thisDept == 3 )
            {
                echo ", Certainty1: ". $committeeReviews[$i][28] . " Certainty2: ".$committeeReviews[$i][28] ;//show certainty 2
            }else
            {
                // PLB commented out 01/22/09
                //echo ", Certainty: ". $committeeReviews[$i][28];//show certainty 2
            }
            /*
            //2nd score
            
            if($thisDept == 3 && count($myPrograms2) > 1)
            {    
                $showScore2 = false;
                for($k = 0; $k < count($myPrograms2); $k++)
                {
                    if($myPrograms2[$k][1] == "M.S. in Robotics")
                    {
                        $showScore2 = true;
                        break;
                    }
                }
                if($showScore2 == true)
                {
                    echo "/".$committeeReviews[$i][28];
                }
            }
            */
            //PLB added MSIT Evaluation (point2) 01/22/09
            $msit_score = $committeeReviews[$i][11];
            if ($msit_score != NULL && $thisDept != 47) {
            
                echo ", MSIT Evaluation: ". $msit_score;   
            
            }
            
            // PLB commented out 01/22/09
            /*
            if($committeeReviews[$i][13] == 1)
            {
                echo ", Round 2: Yes";
            }else
            {
                echo ", Round 2: No";
            }
            */
            if (!isset($db_risk_factors)) {
    
         $db_risk_factors = new DB_MseRiskFactors();
        
    }
    
 
      $risk_factors = $db_risk_factors->getRiskFactors($appid, $committeeReviews[$i][4]);
    
    $risk_factor_language_checked = "";
    $risk_factor_experience_checked = "";
    $risk_factor_academic_checked = "";
    $risk_factor_other_checked = "";
    if (count($risk_factors) > 0)
        {
            echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Risk(s): ";
        }
    foreach ($risk_factors as $risk_factor) {
    
        if ($risk_factor['language'] == 1) {
            echo "Language ";
        }
        
        if ($risk_factor['experience'] == 1) {
            echo "Experience ";
        }
        
        if ($risk_factor['academic'] == 1) {
            echo "Academic ";
        }
        
        if ($risk_factor['other'] == 1) {
            echo "Other";
        }
        
    }
            
            echo "<br>";
        }
     } ?>
    </strong></td>
  </tr>
  <tr>
    <td colspan="2"><hr align="left" >    </td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Committee <br>Comments:</strong>      </td>
    <td valign="top">
    <? for($i = 0; $i < count($committeeReviews); $i++){
        if($committeeReviews[$i][29] == "" && $committeeReviews[$i][30]==0 && $committeeReviews[$i][32]== $thisDept)
        {
            echo "<input name='txtReviewerId' type='hidden' value='".$committeeReviews[$i][4]."'><strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] . ", " . $committeeReviews[$i][5];
            if($round > 1)
            {
                echo " (round ".$committeeReviews[$i][23].")";
            }
            echo "</strong><br>";
            echo $committeeReviews[$i][9]."<br>";
        }
     } ?>    </td>
  </tr>
  
  <tr>
    <td colspan="2"><hr align="left">    </td>
  </tr>
  <? }//end semiblind reviews ?>
  

  <!-- PLB commented out 01/22/09
  <tr>
    <td bgcolor="#c5c5c5" valign="top" width="50"><strong>Faculty<br>  Reviews:</strong></td>
    <td valign="top" width="200">
    <? for($i = 0; $i < count($committeeReviews); $i++)
    {
    
        if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "" )
        {

                ?>
                <?php 
                //PLB changed link here
                /*
                <a linkindex="1" href="userroleEdit_student_formatted.php?v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                */
                ?>
                <a linkindex="1" href="userroleEdit_student_review.php?v=3&r=2&id=<?=$uid?>&rid=<?=$committeeReviews[$i][4]?>" target="_blank">
                <?=$committeeReviews[$i][2] . " ". $committeeReviews[$i][3]?></a> (<?=$committeeReviews[$i][15]?>)<br>
              <?
    
        }
     } ?></tr>
     -->
     
     <? if($showDecision == false){ ?>
  <tr>
    <td colspan="2"><hr align="left" ></td>
  </tr>
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Comments: </strong> </td>
    <td>
    <?

    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "" )
        {
            $comments = $committeeReviews[$i][9];
        }
     }

     showEditText($comments, "textarea", "comments", $allowEdit, false, 80); ?>    </td>
  </tr>
  <!-- PLB commented out 01/21/09
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong>Personal<br>
      Comments: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $private_comments = $committeeReviews[$i][12];
        }
     }
    ?>
    <? showEditText($private_comments, "textarea", "private_comments", $allowEdit, false, 40); ?>    </td>
  </tr>
  -->
  
  <!-- PLB added risk factors 01/22/09 -->
  
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong>Risk<br>
      Factors: </strong> </td>
    <td>
    <?
    // Get the risk factor values from the db
    //include_once "./classes/class.db_applyweb.php";
    
    include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
    include_once "../classes/class.db_mse_risk_factors.php";
    
    if (!isset($db_risk_factors)) {
    
         $db_risk_factors = new DB_MseRiskFactors();
        
    }
    
    $risk_factors = $db_risk_factors->getRiskFactors($appid, $reviewerId);
    
    $risk_factor_language_checked = "";
    $risk_factor_experience_checked = "";
    $risk_factor_academic_checked = "";
    $risk_factor_other_checked = "";
    
    foreach ($risk_factors as $risk_factor) {
    
        if ($risk_factor['language'] == 1) {
            $risk_factor_language_checked = "checked";
        }
        
        if ($risk_factor['experience'] == 1) {
            $risk_factor_experience_checked = "checked";
        }
        
        if ($risk_factor['academic'] == 1) {
            $risk_factor_academic_checked = "checked";
        }
        
        if ($risk_factor['other'] == 1) {
            $risk_factor_other_checked = "checked";
        }
        
    }
     DebugBreak();
    ?>    
    <input name="risk_factors[]" type="checkbox" class="tblItem" value="language" <?= $risk_factor_language_checked ?> />Language
    <input name="risk_factors[]" type="checkbox" class="tblItem" value="experience" <?= $risk_factor_experience_checked ?> />Experience
    <input name="risk_factors[]" type="checkbox" class="tblItem" value="academic" <?= $risk_factor_academic_checked ?> />Academic
    <input name="risk_factors[]" type="checkbox" class="tblItem" value="other" <?= $risk_factor_other_checked ?> />Other
    <input name="mse_review" type="hidden" value="true" />
    </td>
  </tr>
  
  
  <tr>
  <?php
  // PLB added test to display "Score" instead of "Ranking" for RI
  // PLB added test to display "Evaluation" for MSE  01/19/09
  if($thisDept == 3){
      $ranking_label = "Score";
  } elseif ($thisDept == 18) {
    $ranking_label = "Evaluation";
  } else {
    $ranking_label = "Ranking";
  }
  
  ?>
    <td bgcolor="#c5c5c5" width="50"><strong><?= $ranking_label ?><? if($thisDept == 3){echo " (Phd)";} ?>: </strong> </td>
    <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $point = $committeeReviews[$i][10];
        }
     }
    /* PLB - original test to determine ranking scale
    if($thisDept == 3)
    {
        echo "Enter a number from 5.000 to 10: ";
        showEditText($point, "textbox", "point", $allowEdit, false);
    }else
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
    }    
    */
    // PLB new test to include different scales for RI and machine learning 
    if($thisDept == 3) // robotics institute
    {
        // PLB edit here to change scale.
        echo "Enter a number from 5.0 to 10: ";
        showEditText($point, "textbox", "point", $allowEdit, false);
    } elseif ($thisDept == 2) // machine learning
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $machineLearningVoteVals);
    } elseif ($thisDept == 8) // PLB added comp bio 01/05/09
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $compBioVoteVals);
    } elseif ($thisDept == 18 || $thisDept == 47 || $thisDept == 44) // PLB added MSE 01/19/09
    {   
        ?> <div id="mse_score"> <?
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $mseVoteVals);
        ?> </div> <?
    } else
    { 
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
    }
    ?></td>
  </tr>

  <?php
 
  /*
  DISPLAY 2ND SCORE FOR MSE
  Display logic - PLB 01/21/09 
  */

  // Display only for MSE program id = 21
  $program_id = $myPrograms[0][0];
  if ($program_id == 21 || $program_id == 37) {
  ?>
  <tr>
    <td bgcolor="#c5c5c5"><div class="mse_msit" id="mse_msit_label"><strong>MSIT Evaluation: </strong></div></td>
    <td>
    
    <script language="javascript">
        $(document).ready(function () {
            
            $("#mse_score input").click(function (event) {
            
                var $target = $(event.target);
                
                if($target.attr("value") == "4") {
                
                    alert("Please evaluate this candidate for MSIT admission.");
                    $("#mse_msit_label").css("visibility", "visible"); 
                    $("#mse_msit_score").css("visibility", "visible");
                    <?php
                    // Keep the "Accept" selection from the db if user is
                    // changing selection without saving. 
                    if($point2 == 1) { 
                    ?>
                    $("#mse_msit_score input[value='1']").attr("checked", "checked");
                    <?php
                    } elseif($point2 == 2) { 
                    ?>
                    $("#mse_msit_score input[value='2']").attr("checked", "checked");
                    <?php
                    } else { 
                    ?>
                    $("#mse_msit_score input[value='3']").attr("checked", "checked");
                    <?php
                    }
                    ?>
                
                } else {
                
                    $("#mse_msit_label").css("visibility", "hidden"); 
                    $("#mse_msit_score").css("visibility", "hidden");
                    $("#mse_msit_score input").removeAttr("checked");
                    
                }
                    
            });
            
        });
    </script>
 
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $point2 = $committeeReviews[$i][11];
        }
     }
     
     ?><div class="mse_msit" id="mse_msit_score"><?
     showEditText($point2, "radiogrouphoriz3", "point2", $allowEdit, false, $mseMsitVoteVals);
     ?></div><?
     
    // Hide the MSE-MSIT score form on page load unless the MSE score is "marginal"
    if ($point == 4) {

        // Do nothing
        
    } else {
        
        // Hide the form
        ?>
        <script language="javascript">
            $(document).ready(function () {
                $("#mse_msit_label").css("visibility", "hidden"); 
                $("#mse_msit_score").css("visibility", "hidden");
            });
        </script>       
        <?
    } 
         
    ?></td>
  </tr>
      
  <?php
  
  } // end MSE program id if

  if($round  == 1 ){ ?>
  <!-- PLB commented out 01/21/09
  <tr>
    <td bgcolor="#c5c5c5" width="50"><strong> Round 2: </strong> </td>
    <td>
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $round2 = $committeeReviews[$i][13];
        }
     }

    showEditText($round2, "radiogrouphoriz2", "round2", $allowEdit, false, array(array(1,"yes"),array(0,"No")) ); ?>    </td>
  </tr>
  -->
 
 <? }//end if round1 ?>
  <tr>
    <td colspan="2">
    <!-- PLB added div -->
    <div style="margin: 5px;">
    <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />

<!-- PLB commented out br, added spaces
     <br>
     <br>
     -->
     &nbsp;&nbsp;   
    <? if (($thisDept == 18 || $thisDept == 19 ||
    $thisDept == 47 || $thisDept == 48 || $thisDept == 52) 
    && $_GET['r'] == 2 && $_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
    {
       showEditText("Save", "button", "btnSubmit", false, false, array(array()), false); 
    } else
    showEditText("Save", "button", "btnSubmit", $allowEdit); ?>    
    </div>
    </td>
  </tr>
  <? }//end not admin ?>
</table>
    
    
    <!-- FINAL ROUND DECISION STUFF -->
    <? } ?>  

    <? 
       if($_SESSION["A_usertypeid"] == 1 && $showDecision == true){ ?>
    
<table border =0 cellpadding="0" cellspacing=2 cellpading=2>
<? 
for($x = 0; $x < count($myPrograms); $x++)
{
    $aDepts = split(",", $myPrograms[$x][14]);
    //FIRST CHECK IF THE PROGRAM BELONGS TO THIS DEPARTMENT
    for($j = 0; $j < count($aDepts); $j++)
    {
        if($aDepts[$j] == $thisDept)
        {
            //CHECK IF PROGRAM IS ELIGIBLE FOR ROUND2
            ?>
            
            <tr><td >
<hr  align=left>
<strong>Admit Information for program:</strong> <em><?=$myPrograms[$x][1] . " ".$myPrograms[$x][2] . " ".$myPrograms[$x][3]?> </em>
</td>
</tr>


<tr>
<td>
<table border=0 cellpadding=0 cellspacing=2>
<tr>

<td bgcolor=c5c5c5 width=50> <font color=000099><b> Comments: </b> </td>
<td><?
    $comments = $myPrograms[$x][13];
    showEditText($comments, "textarea", "comments_".$myPrograms[$x][0], $allowEdit, false, 60); ?></td>
<td valign=top align=right bgcolor=c5c5c5 width=50> <font color=000099><b> Decision: </b> </td>
<td valign=top>
<? 
$decision = $myPrograms[$x][10] ;
showEditText($decision, "listbox", "decision_".$myPrograms[$x][0], $_SESSION['A_allow_admin_edit'], true, $decisionVals); 
?></td>
</tr>
</table></td>
</tr>

<tr>
<td>
<table border=0 cellpadding=0 cellspacing="2">
<tr><td width="50%"><font color=000099><strong>Faculty Contact:&nbsp;</strong></font>
<? 
$faccontact = $myPrograms[$x][15];
showEditText($faccontact, "textbox", "faccontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); ?>
</td>
<td width="50%"><strong><font color=000099>Student Contact:&nbsp;</font></strong>
<? 
$stucontact = $myPrograms[$x][16];
showEditText($stucontact, "textbox", "stucontact_".$myPrograms[$x][0], $allowEdit,false,null,true,30); ?></td>
</tr>
</table></td>
</tr>
<tr>
<td>
<table border=0  bgcolor=c5c5c5 cellpadding=0 cellspacing=2>
<tr><td><font color=000099><h4>Admissions Status
</td></tr>

<tr>
<td  colspan=2><font color=000099>
<? 
$admissionStatus = $myPrograms[$x][11];
showEditText($admissionStatus, "radiogrouphoriz2", "admissionStatus_".$myPrograms[$x][0], $allowEdit, false, array(array(0,"Reject"),array(1,"Waitlist"),array(2,"Admit"),array(3,"Reset")   ) ); ?></tr>
<!--
<tr>
<td >
<font color=000099> &nbsp;&nbsp;&nbsp;<b>Admit to: </td><td width=270 align=left>
<?  
    showEditText($admit_to, "listbox", "admitto_".$myPrograms[$x][0], $allowEdit, false,  $myPrograms2); ?>
</td>
</tr>
-->
</table></td>
</tr>

            
            
            <?
        }//END IF DEPTS MATCH
    }//END FOR COUNT aDEPTS    
    
 }//END FOR EACH PROGRAM
 
 // DAS  Check for MSIT review for MSE Applicant
 
 if (($msit_score != NULL || sizeof($mseMsitVoteVals > 0))&& $thisDept != 47) {
     $comments_msit = "";
     $admissionStatus_msit = NULL;
     $periodSql = "select period_id from period_application
                    where application_id = " . $appid;
                    $periodResult = mysql_query($periodSql) or die(mysql_error() . $periodSql);

                    while($row = mysql_fetch_array($periodResult ))
                    {
                        $currentPeriod = $row["period_id"];
                    }
     $getAdmissionsSql = "select * from application_decision
     where application_id = " . $appid .
     " and period_id = " . $currentPeriod .
     " and (program_id = 29 or program_id = 100003)";
     $admissionsResult = mysql_query($getAdmissionsSql) or die(mysql_error() . $getAdmissionsSql);
     while($row = mysql_fetch_array($admissionsResult ))
                    {
                        $comments_msit = $row["comments"];
                        $admissionStatus_msit = $row["admission_status"];
                    }
 ?>
 <tr><td >
<hr  align=left>
<strong>Admit Information for program:</strong> <em>MSIT </em>
</td>
</tr>
<tr>
    <td>
        <table border=0 cellpadding=0 cellspacing=2>
            <tr>

                <td bgcolor=c5c5c5 width=50> <font color=000099><b> Comments: </b> </td>
                <td><font color=000099><?
    showEditText($comments_msit, "textarea", "comments_msit", $allowEdit, false, 60); ?></td>

            </tr>
</table></td>
</tr>
<tr>
<td>
<table border=0  bgcolor=c5c5c5 cellpadding=0 cellspacing=2>
<tr><td><font color=000099><h4>Admissions Status
</td></tr>
<tr>
<td  colspan=2><font color=000099>
<? 
//debugBreak();

showEditText($admissionStatus_msit, "radiogrouphoriz2", "admissionStatus_msit", $allowEdit, false, array(array(0,"Reject"),array(1,"Waitlist"),array(2,"Admit"),array(3,"Reset")   ) ); ?></tr>
<!--
<tr>
<td >
<font color=000099> &nbsp;&nbsp;&nbsp;<b>Admit to: </td><td width=270 align=left>
<?  showEditText($admit_to, "listbox", "admitto_".$myPrograms[$x][0], $allowEdit, false,  $myPrograms2); ?>
</td>
</tr>
-->
</table></td>
</tr>
<?
 }  // DAS end check for MSIT review for MSE Applicant?>
 
 <tr>
  <td><? if (($thisDept == 18 || $thisDept == 19 ||
    $thisDept == 47 || $thisDept == 48 || $thisDept == 52) 
    && $_GET['r'] == 2 && $_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
    {
       showEditText("Save", "button", "btnSubmitFinal", $false, false, array(array()), false); 
    } else
    showEditText("Save", "button", "btnSubmitFinal", $allowEdit); ?></td>
</tr>
</table>
    
    
<? }//end show decision ?>




<!-- FACULTY REVIEW FOR ROUND 2 -->
<? if($view == 3 && $round == 2){ ?>

<input name="background" type="hidden" value="<?=$background?>">
<input name="grades" type="hidden" value="<?=$grades?>">
<input name="statement" type="hidden" value="<?=$statement?>">
<input name="comments" type="hidden" value="<?=$comments?>">
<!--<input name="point" type="hidden" value="<?=$point?>">-->
<!--<input name="point2" type="hidden" value="<?=$point2?>">-->
<!--<input name="pointCertainty" type="hidden" value="<?=$pointCertainty?>">-->
<!--<input name="point2Certainty" type="hidden" value="<?=$point2Certainty?>">-->
<input name="private_comments" type="hidden" value="<?=$private_comments?>">
<input name="round2" type="hidden" value="<?=$round2?>">
<input name="touched" type="hidden" value="<?=$touched?>">
<input name="facVote" type="hidden" value="1">
<!--<input name="admit_vote" type="hidden" value="<?=$admit_vote?>">-->
<!--<input name="recruited" type="hidden" value="<?=$recruited?>">-->
<!--<input name="grad_name" type="hidden" value="<?=$grad_name?>">-->
<!--<input name="pertinent_info" type="hidden" value="<?=$pertinent_info?>">-->
<!--<input name="advise_time" type="hidden" value="<?=$advise_time?>">-->
<!--<input name="commit_money" type="hidden" value="<?=$commit_money?>">-->
<!--<input name="fund_source" type="hidden" value="<?=$fund_source?>">-->

<table cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>PART I.  INFORMATION FOR THE ADMISSIONS COMMITTEE. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Do you think we should admit this applicant?  Select one, and explain why by provding other pertinent information that might be useful to the Admissions Committee and your colleagues (e.g. direct knowledge of the student or his/her research, calibration of the recommenders, etc.) </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $admit_vote = $committeeReviews[$i][15];
        }
     }

    showEditText($admit_vote, "radiogrouphoriz2", "admit_vote", $allowEdit, false,
    array(
    array("DEFINITELY","DEFINITELY"),
    array("Probably Yes","Probably Yes"),
    array("Probably No","Probably No"),
    array("DEFINITELY NOT","DEFINITELY NOT")
    )
    ); ?>
    <br>

<?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $pertinent_info = $committeeReviews[$i][18];
        }
     }
     showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 60); ?>
  </td>
  </tr>





<!--OTHER FAC COMMENTS -->  
<tr>
    <td colspan="2" bgcolor="#c5c5c5">
    <strong>Other faculty comments    
    </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][30] == 1 && $committeeReviews[$i][29] == "")
        {
            echo "<strong>".$committeeReviews[$i][2] . " ". $committeeReviews[$i][3] ."</strong><br>".$committeeReviews[$i][18]."<br>";
        }
    }
    ?>
    </td>
  </tr> 



<!-- END OTHER FAC COMMENTS -->
<tr>
    <td colspan="2" bgcolor="#c5c5c5">
    <strong>Please rank at least your top 3 choices. This applicant is my:    
    </strong>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    $rank = 0;
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $rank = $committeeReviews[$i][31];
        }
     }
    $arr = array(array(0,"This is not a top 5 choice"),    array(1,"1st Choice"),array(2,"2nd Choice") ,array(3,"3rd Choice"),array(4,"4th Choice"),array(5,"5th Choice")   );
    if($rank == "")
    {
        $rank = 0;
    }
    if($allowEdit == true)
    {
        
        showEditText($rank, "radiogrouphoriz2", "rank", $allowEdit, false, $arr     ); 
    }else
    {
        
        echo $arr[$rank][1];
        
        
    }    
    ?>
    </td>
  </tr>  
  
  
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would be willing to play an active role in recruiting this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $recruited = $committeeReviews[$i][16];
        }
     }

     $arr = array(    array(0, "No"), array(1,"Yes"));
    if($allowEdit == true)
    {
        showEditText($recruited, "radiogrouphoriz2", "recruited", $allowEdit, false, $arr      ); 
    }else
    {
        if($recruited != "")
        {
            echo $arr[$recruited][1];
        }
    }    
    ?>

    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I suggest the following graduate students as possible recruiting contacts for this student: </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $grad_name = $committeeReviews[$i][17];
        }
     }
    showEditText($grad_name, "textbox", "grad_name", $allowEdit, false, null,true,40); ?>
    </td>
  </tr>
  <!--
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Other pertinent information that might be useful to the Admissions Committee (e.g. direct       knowledge of the student or his/her research, calibration of the recommenders, etc.) </strong></td>
  </tr>
 
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $pertinent_info = $committeeReviews[$i][18];
        }
     }
     showEditText($pertinent_info, "textarea", "pertinent_info", $allowEdit, false, 60); ?>
    </td>
  </tr>
   -->
  <tr>
    <td colspan="2"><hr align="left" />
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>PART II.  INFORMATION FOR THE DEPARTMENT CHAIR. <br />
      In the event that this student is considered on the borderline, your  answers to the following questions could make a difference in the  decision the department chair makes on whether or not to offer  admission to the student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would commit my time and energy to advise this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
        <?
        for($i = 0; $i < count($committeeReviews); $i++)
        {
            if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
            {
                $advise_time = $committeeReviews[$i][19];
            }
         }
//         $arr =  array(    array(1,"Yes"),    array(0,"No") );
         $arr =  array(    array(0, "No"), array(1,"Yes") );
        if($allowEdit == true)
        {
         
            showEditText($advise_time, "radiogrouphoriz2", "advise_time", $allowEdit, false,$arr     ); 
        }else
        {
            if($recruited != "")
            {
                echo $arr[$advise_time][1];
            }
        }        
        ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>I would commit my money to advise this student. </strong></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#ffffff">
    <?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $commit_money = $committeeReviews[$i][20];
        }
     }
//     $arr = array(    array(1,"Yes"),    array(0,"No") ) ;
     $arr = array(    array(0,"No"), array(1,"Yes") ) ;
     if( $allowEdit == true)
     {
     
        showEditText($commit_money, "radiogrouphoriz2", "commit_money", $allowEdit, false, $arr    ); 
    }else
    {
        if($recruited != "")
        {
            echo $arr[$commit_money][1];
        }
    }
    ?>
     <br />
      <strong> If yes, please state your probable source of funding.</strong> <br />
      <?
          for($i = 0; $i < count($committeeReviews); $i++)
        {
            if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
            {
                $fund_source = $committeeReviews[$i][21];
            }
         }
       showEditText($fund_source, "textarea", "fund_source", $allowEdit, false, 60); ?><br>
<br>

      </td>
  </tr>
   <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
     <!-- REMOVE OVERALL RANKING  
      <tr>
        <td bgcolor="#c5c5c5"><strong>Ranking - Overall:</strong></td>
        <td><?
    for($i = 0; $i < count($committeeReviews); $i++)
    {
        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
        {
            $point = $committeeReviews[$i][10];
        }
     }
    if($thisDept == 3)
    {
        echo "Enter a number from 5.000 to 10: ";
        showEditText($point, "textbox", "point", $allowEdit, false);
    }else
    {
        showEditText($point, "radiogrouphoriz3", "point", $allowEdit, false, $csdVoteVals);
    }
    ?></td>
        </tr>
   
        
      <tr>
        <td width="50" bgcolor="#c5c5c5"><strong>Certainty:</strong></td>
        <td><table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center">High-</td>
            <td align="center"><?
                for($i = 0; $i < count($committeeReviews); $i++)
                {
                    if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == "")
                    {
                        $pointCertainty = $committeeReviews[$i][27];
                    }
                 }
            
                showEditText($pointCertainty, "radiogrouphoriz3", "pointCertainty", $allowEdit, false, $certaintyVals);
            
                ?></td>
            <td align="center">-Low </td>
          </tr>
        </table>
                  </td>
        </tr>
    </table></td>
  </tr>
     -->
   <? 
   if(count($myAreasAll) > 0){  ?>
       <tr>
    <td colspan="2" bgcolor="#c5c5c5"><strong>Please discuss with your colleagues and enter a score that reflects the strength of this applicant within the research areas listed below.</strong></td>
  </tr>
  
  <? 
  }
      for($j = 0; $j < count($myAreasAll); $j++){ ?>
      <tr>
      <td colspan="2">
    
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#c5c5c5" WIDTH="50"><strong>
              <?=$myAreasAll[$j][1]?>
              :</strong></td>
            <td><?
            $val = 0;
            for($i = 0; $i < count($committeeReviews); $i++)
            {
                if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
                {
                    $val = $committeeReviews[$i][10];
                }
             }
            
            if($thisDept == 3)
            {
                echo "Enter a number from 5.000 to 10: ";
                showEditText($val, "textbox", "point_".$myAreasAll[$j][0], $allowEdit, false);
            }else
            {
                ?><TABLE BORDER=0>
                <tr>
                  <td>Low-</td>
                  <td>
                <?
                showEditText($val, "radiogrouphoriz3", "point_".$myAreasAll[$j][0], $allowEdit, false, $csdFacVoteVals);
                ?></td>
                  <td>-High</td>
                </tr></table><?
            }
            
            ?></td>
            </tr>
                  <!-- DONT SHOW CERTAINTY
                        <tr>
                          <td width="50" bgcolor="#c5c5c5"><strong>Certainty:</strong></td>
                          <td><table border="0" cellspacing="0" cellpadding="0">
                         <tr>
                        <td align="center">High-</td>
                        <td align="center"><?
                        $val = 0;
                        for($i = 0; $i < count($committeeReviews); $i++)
                        {
                        if($committeeReviews[$i][4] == $reviewerId && $committeeReviews[$i][29] == $myAreasAll[$j][0])
                        {
                        $val = $committeeReviews[$i][27];
                        }
                         }
                        
                        showEditText($val, "radiogrouphoriz3", "pointCertainty_".$myAreasAll[$j][0], $allowEdit, false, $certaintyVals);
                        
                        ?></td>
                        <td align="center">-Low</td>
                         </tr>
                          </table>
                        </td>
                        </tr>
                -->
        </table>
    
    
    
    
      </td>
      </tr>
  <? }//END AOI REVIEWS ?>
  <tr>
    <td colspan="2">
    <input name="btnReset" type="button" id="btnReset" class="tblItem" value="Reset Scores" onClick="resetForm() " />

<!-- PLB commented out line breaks, added spaces
    <br>
    <br>
-->
&nbsp;&nbsp;
    <? if (($thisDept == 18 || $thisDept == 19 ||
    $thisDept == 47 || $thisDept == 48 || $thisDept == 52) 
    && $_GET['r'] == 2 && $_SESSION['A_usertypeid'] != 0 && $_SESSION['A_usertypeid'] != 1)
    {
       showEditText("Save", "button", "btnSubmit", $false, false, array(array()), false); 
    } else
    showEditText("Save", "button", "btnSubmit", $allowEdit); ?></td>
  </tr>
</table>


<? }//end if faculty round 2 view ?>
<!-- END REVIEW STUFF -->


<? 
}//END FAC CONTACT HIDE
}//end student contact HIDE

//DebugBreak();
?>

