<?php
ini_set('memory_limit', '64M');

header("Cache-Control: no-cache");

include_once "../inc/config.php";
include_once "../inc/session_admin.php";
//debugbreak();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Application Admin</title>

<!-- keep style here for now -->
<link href="../css/SCSStyles_.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/menu.css">
<style type="text/css">

    td, th  {
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12px;
    }
    
    tr a { color: blue; text-decoration: none; }
    tr a:hover { text-decoration: underline; }
    
    th a { color: black; text-decoration: none; }
    
    td.selected a { font-weight: bold; }
    tr.selected td { border-top: 1px solid black; }
    td.content { border-bottom: 1px solid black; }
    
    a.menu { color: black; }
    
    .confirm { color: blue; }
    .confirm_complete { font-style: italic; font-weight : bold; }

    div.content { height: 250px; width: 100%; overflow: auto; display: none; }
    
    #loading {
        clear:both; 
        background:url(wait.gif) center top no-repeat; 
        text-align:center;
        padding:33px 0px 0px 0px; 
        font-size:12px;
        font-family:Verdana, Arial, Helvetica, sans-serif; }

    
    #content_main { 
        position:absolute; 
        height: 300px; 
        /*
        width:500px;
        */
        top:0;
        left: 0;
        right:0;
        background-color: #FFFFFF;
        display: none; 
    }

</style>

<!-- include the jquery libraries -->
<script language="javascript" src="./javascript/jquery-1.2.6.min.js"></script>
<script language="javascript" src="./javascript/jquery.livequery.js"></script>
<script type="text/javascript" src="./javascript/ajaxfileupload.js"></script>

<!-- include Phil's jquery code for displaying forms in hidden divs -->
<?
// 
?>
<script language="javascript" src="./javascript/martha_jquery.js"> </script> 

<?
/*
<script language="javascript" src="./javascript/martha_jquery_main_function.js"> </script>
<script language="javascript" src="./javascript/martha_jquery_forms.js"> </script>
*/
?>



</head>

<body>

<!--
<div id="content_main"></div>
-->

<!-- original header here -->
<!--
<div style="height:72px; width:781;" align="center">
      <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
-->
<div style="height:72px;" align="left">
    <table width="781" height="72" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="75%">
            <span class="title">Carnegie Mellon School for Computer Sciences</span><br />
            <strong>Online Admissions System</strong>
            </td>
            <td align="right">
            <?php 
            $_SESSION['A_SECTION']= "2";
            if(isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] > -1){ 
            ?>
            You are logged in as:<br />
            <?=$_SESSION['A_email']?> - <?=$_SESSION['A_usertypename']?>
            <br />
            <a href="../admin/logout.php"><span class="subtitle">Logout</span></a> 
            <?php } ?>
            </td>
        </tr>
    </table>
</div>

<!-- original menu div -->
<div style="height:10px" align="center">
        <div align="center"><img src="../images/header-rule.gif" width="781" height="12" /><br />
        <script language="JavaScript" src="../inc/menu.js"></script>
        <!-- items structure. menu hierarchy and links are stored there -->
        <!-- files with geometry and styles structures -->
        <script language="JavaScript" src="../inc/menu_tpl.js"></script>
        <script language="JavaScript" src="../inc/menu_items_review.js"></script>
        <script language="JavaScript">new menu (MENU_ITEMS, MENU_TPL);</script>
    </div>
</div>
<br />
<br />

<?php

// include the datagrid package
require('Structures/DataGrid.php');

// include the db classes
include "./classes/class.db_applyweb.php";
include "./classes/class.db_application_list.php";
/*
include "./classes/class.db_application.php";
include "./classes/class.db_test_scores.php";
include "./classes/class.db_education.php";
include "./classes/class.db_recommendations.php";
*/
include "./classes/class.db_payment.php";
include "./classes/class.db_domain.php";

// include the display functions
include "./classes/display.inc.php";

// get an application list for department=1 from the db
if ( isset($_REQUEST['id']) ) {
    $department_id = $_REQUEST['id'];
} else {
    $department_id = 1;
}

// get the application period info
// PLB added 02/06/09
if ( isset($_REQUEST['period']) ) {
    $application_period_id = $_REQUEST['period'];
    include "./classes/class.db_application_period.php";
    $applicationPeriod = new DB_ApplicationPeriod();
    $applicationPeriodRecords = $applicationPeriod->getApplicationPeriod($application_period_id);
    if ( count($applicationPeriodRecords) > 0 ) {
        $start_date = $applicationPeriodRecords[0]['start_date'];
        $end_date = $applicationPeriodRecords[0]['end_date'];
        $application_period_display = date( "n/j/Y", strtotime($start_date) );
        $application_period_display .= " - " . date( "n/j/Y", strtotime($end_date) );         
    } else {
        $start_date = NULL;
        $end_date = NULL;
        $application_period_display = ""; 
    }    
} else {
    $start_date = NULL;
    $end_date = NULL;
    $application_period_display = "";
}


$applicationList = new DB_ApplicationList();
$applicationListRecords = $applicationList->getAdminByDepartment($department_id, $start_date, $end_date);
$record_count = count($applicationListRecords); 

// display domain/list info
$domain = new DB_Domain();
$department_record = $domain->getDepartmentInfo($department_id);
echo '<div style="margin: 10px;"><span class="title">' . $department_record[0]["name"] . ' Applicants (submitted)';
echo ' ' . $application_period_display . '</span>';
echo '<br />' . $record_count . ' of ' . $record_count . ' applications</div>';
//echo "datafileroot: " . $datafileroot;

// Create the DataGrid, Bind it's Data Source and Define it's columns
$dg = new Structures_DataGrid(); // maxes out at 270?
//$dg->enableStreaming(500);

// bind application list data using array driver
$dg->bind($applicationListRecords ,array() , 'Array');

// create the columns
$dg->addColumn(new Structures_DataGrid_Column('', 'user_id', 'user_id', array('width' => '5%', 'align' => 'center'), null, 'printPrintLink()', $dg->getCurrentRecordNumberStart()));
$dg->addColumn(new Structures_DataGrid_Column('Name', 'name', 'name', array('width' => '20%', 'align' => 'left'), null, 'printFullName()'));
$dg->addColumn(new Structures_DataGrid_Column('', 'gre_rcvd', 'gre_rcvd', array('width' => '13%', 'align' => 'center'), null, 'printTestScoresLink()'));
$dg->addColumn(new Structures_DataGrid_Column('', 'trans_rcvd', 'trans_rcvd', array('width' => '13%', 'align' => 'center'), null, 'printTranscriptLink()'));
$dg->addColumn(new Structures_DataGrid_Column('', 'submitted', 'submitted', array('width' => '13%', 'align' => 'center'), null, 'printRecommendationsLink()'));
$dg->addColumn(new Structures_DataGrid_Column('', 'toefl_rcvd', 'toefl_rcvd', array('width' => '13%', 'align' => 'center'), null, 'printPaymentLink()'));
$dg->addColumn(new Structures_DataGrid_Column('Complete', 'cmp', null, array('width' => '10%', 'align' => 'center'), null, 'printCompleteCheckbox()'));  
$dg->addColumn(new Structures_DataGrid_Column('', null, null, array('width' => '13%', 'align' => 'center'), null, 'printPublicationsLink()'));  

// Define the Look and Feel
/*
$dg->renderer->setTableHeaderAttributes(array('bgcolor' => '#B9C7D5'));
$dg->renderer->setTableEvenRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#ECEFF4', 'class' => 'menu', 
    'onMouseOver' => 'this.style.backgroundColor=\'#FAF6F0\';',
    'onMouseOut' => 'this.style.backgroundColor=\'#ECEFF4\';'
    ));
$dg->renderer->setTableOddRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#E7EBEF', 'class' => 'menu',
    'onMouseOver' => 'this.style.backgroundColor=\'#FAF6F0\';',
    'onMouseOut' => 'this.style.backgroundColor=\'#E7EBEF\';'    
    ));
*/
$dg->renderer->setTableHeaderAttributes(array('bgcolor' => '#CCCCCC'));
$dg->renderer->setTableEvenRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#FFFFFF', 'class' => 'menu', 
    //'onMouseOver' => 'this.style.backgroundColor=\'#EEEEEE\';',
    //'onMouseOut' => 'this.style.backgroundColor=\'#FFFFFF\';'
    ));
$dg->renderer->setTableOddRowAttributes(
    array('valign' => 'top', 'bgcolor' => '#EEEEEE', 'class' => 'menu',
    //'onMouseOver' => 'this.style.backgroundColor=\'#EEEEEE\';',
    //'onMouseOut' => 'this.style.backgroundColor=\'#EEEEEE\';'    
    ));
$dg->renderer->setTableAttribute('width', '100%');
$dg->renderer->setTableAttribute('border', '0');
$dg->renderer->setTableAttribute('cellspacing', '0');
$dg->renderer->setTableAttribute('cellpadding', '5px');
$dg->renderer->setTableAttribute('class', 'datagrid');
$dg->renderer->sortIconASC = '&uArr;';
$dg->renderer->sortIconDESC = '&dArr;';

// render the datagrid
//$test = $dg->render();
$dg->render();

// for debugging error when rendering over 270 rows 
//debugbreak(); 
/*       
if (PEAR::isError($test)) {
    echo $test->getMessage(); 
} 
*/

// callback functions for datagrid rendering

function printPrintLink($params, $recordNumberStart) {
    extract($params);
    $user_id = $record['user_id'];
    //$link = "<a class=\"menu\" href=\"\" title=\"Print Application\" onClick=\"window.open('../admin/userroleEdit_student_formatted.php?id=" . $user_id . "'); return false;\">Print</a>";
    // PLB added row numbers 2/2/09
    $link = $params['currRow'] + $recordNumberStart . "." . "&nbsp;&nbsp;";
    $link .= '<a class="menu" href="javascript:;" id="print_'. $user_id . '" title="Print Application">Print</a>';
    return $link;
}


function printFullName($params)
{   
    extract($params);
    return "<b>" . $record['name'] . "</b><br /><a title=\"E-mail Applicant\" href=\"mailto:" . $record['email'] . "\">" . $record['email'] . "</a>";
}


function printTestScoresLink($params) {
    extract($params);
    $application_id = $record['application_id'];
    $gre_received = $record['gre_rcvd'];
    $gre_received_msg_id = $application_id . "_gre_received_msg";
    $ct_gresubject_entered = $record['ct_gresubject_entered'];
    $ct_gresubject_received = $record['ct_gresubject_received'];
    $gresubject_received_msg_id = $application_id . "_gresubject_received_msg";
    $toefl_entered = $record['toefl_entered'];
    $toefl_received = $record['toefl_submitted'];
    $toefl_received_msg_id = $application_id . "_toefl_received_msg";
    $link_id = $application_id . "_testscores";     
    //$link = '<a class="menu" href="" title="Edit Test Scores" id="' . $link_id . '" onClick="changeView(this,' . $application_id . '); return false;">';
    $link = '<a class="menu" href="javascript:;" title="Edit Test Scores" id="' . $link_id . '">';
    $link .= 'Test Scores</a>';
    $link .= '<br /><span class="confirm">GRE</span> <span class="confirm_complete" id="' . $gre_received_msg_id . '">';
    if ( $gre_received ) {
        $link .= 'rcd';
    }
    $link .= '</span>';
    if ( $ct_gresubject_entered ) {
        $link .= '<br /><span class="confirm">GRE Sub.</span> <span class="confirm_complete" id="' . $gresubject_received_msg_id . '">';
        if ( $ct_gresubject_received ) {
           $link .= 'rcd';
        }
        $link .= '</span>';  
    }
    if ( $toefl_entered ) {
        $link .= '<br /><span class="confirm">TOEFL</span> <span class="confirm_complete" id="' . $toefl_received_msg_id . '">';
        if ( $toefl_received ) {
           $link .= 'rcd';
        }
        $link .= '</span>';  
    } 
    
    return $link;
}


function printTranscriptLink($params) {
    extract($params);
    $application_id = $record['application_id'];
    $ct_transcripts_submitted_msg_id = $application_id . "_ct_transcripts_submitted_msg";
    $link_id = $application_id . "_transcripts";  
    //$link = '<a class="menu" href="" title="Edit Transcripts" id="' . $link_id . '" onClick="changeView(this,' . $application_id . '); return false;">';
    $link = '<a class="menu" href="javascript:;" title="Edit Transcripts" id="' . $link_id . '">';
    $link .= 'Transcripts</a>'; 
    $link .= '<br /><span class="confirm"><span id="' . $ct_transcripts_submitted_msg_id . '">' . $record['ct_transcripts_received'] . '</span>';
    $link .= ' of ' .  $record['ct_transcripts_submitted'] . '</span>';  
    return $link;
}


function printRecommendationsLink($params) {
    extract($params);
    $application_id = $record['application_id'];
    $link_id = $application_id . "_recommendations";
    $ct_recommendations_submitted_msg_id = $application_id . "_ct_recommendations_submitted_msg";     
    //$link = '<a class="menu" href="" title="Edit Recommendations" id="' . $link_id . '" onClick="changeView(this,' . $application_id . '); return false;">';
    $link = '<a class="menu" href="javascript:;" title="Edit Recommendations" id="' . $link_id . '">';
    $link .= 'Recommendations</a>';
    $link .= '<br /><span class="confirm"><span id="' . $ct_recommendations_submitted_msg_id . '">' . $record['ct_recommendations_submitted'] . '</span>';
    $link .= " of " .  $record['ct_recommendations_requested'] . '</span>';
    return $link;
}

function printPaymentLink($params) {
    extract($params);
    $application_id = $record['application_id'];
    $paid_msg_id = $application_id . "_paid_msg";
    $paymentamt_msg_id = $application_id . "_paymentamt_msg";
    $paymentamt = $record['paymentamount'];
    $waived = $record['wd'];
    $paid = $record['pd'];
    $link_id = $application_id . "_payment"; 
    //$link = '<a class="menu" href="" title="Edit Payment" id="' . $link_id . '" onClick="changeView(this,' . $application_id . '); return false;">';
    $link = '<a class="menu" href="javascript:;" title="Edit Payment" id="' . $link_id . '">';
    $link .= 'Payment</a>';
    
    if ($waived == "yes") {
        $payment_msg = "<span class=\"confirm_complete\">waived</span>";
    } elseif ($paid == "yes") {
        $payment_msg = "<span class=\"confirm_complete\">paid</span>: <span class=\"confirm\">" . $paymentamt . "</span>";
    } else {
        $payment = new DB_Payment();
        $total_fees_record = $payment->getTotalFees($application_id);
        $total_fees = number_format($total_fees_record['total_fees'], 2);
        $payment_msg = '<span class="confirm">due: ' . $total_fees . '</span>';
    }
    $link .= '<div id="' . $paymentamt_msg_id . '">' . $payment_msg . '</div>';
    return $link;
}

function printCompleteCheckbox($params) {
    extract($params);
    $application_id = $record['application_id'];
    $checkbox_id = $application_id . "_application_complete";
    if ($record['cmp'] == "yes") {
        $checked = "checked";
    } else {
        $checked = "";
    }
    $box = '<input type="checkbox" class="application_complete" id="'. $checkbox_id . '" ' . $checked . ' />';
    return $box;
}


function printPublicationsLink($params) {
    extract($params);
    $application_id = $record['application_id'];
    $link_id = $application_id . "_publications";
    if ( $record['ct_publications'] == NULL ) {
        $ct_publications = "0";
    } else {
        $ct_publications = $record['ct_publications'];
    }
    //$link = '<a class="menu" href="" title="Edit Publications" id="' . $link_id . '" onClick="changeView(this,' . $application_id . '); return false;">';
    $link = '<a class="menu" href="javascript:;" title="Edit Publications" id="' . $link_id . '">';
    $link .= 'Publications</a>';
    $link .= '<br /><span class="confirm">' . $ct_publications . '</span>';
    return $link;
}


// not currently used
function printFeeWaivedCheckbox($params) {
    extract($params);
    $application_id = $record['application_id'];
    $checkbox_id = $application_id . "_fee_waived";
    if ($record['wd'] == "yes") {
        $checked = "checked";
    } else {
        $checked = "";
    }
    $box = '<input type="checkbox" class="fee_waived" id="'. $checkbox_id . '" ' . $checked . ' />';
    return $box;
}

// not currently used
function printSubmittedCheckbox($params) {
    extract($params);
    $application_id = $record['application_id'];
    $checkbox_id = $application_id . "_application_submitted";
    if ($record['sbmd'] == "yes") {
        $checked = "checked";
    } else {
        $checked = "";
    }
    $box = '<input type="checkbox" class="application_submitted" id="'. $checkbox_id . '" ' . $checked . ' />';
    return $box;
}

?>

</body>
</html>
