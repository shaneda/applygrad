<?php
header("Cache-Control: no-cache");

include "./classes/class.db_applyweb.php";
include "./classes/class.db_payment.php";
include "./classes/class.db_test_scores.php";
include "./classes/class.db_education.php";
include "./classes/class.db_recommendations.php";
include "./classes/class.db_publications.php";
include "./classes/display.inc.php";

// for testing purposes
//$luu_id = 882;
//$application_id = 470;

// get the request arguments 
$application_id = $_GET['application_id'];
$page = $_GET['page'];

switch($page) {

    case "testscores":
    case "grescore":
    case "gresubjectscore":
    case "toeflscore":

        $test_scores = new DB_TestScores();
        $gre_records = $test_scores->getGREGeneral($application_id);
        $gre_subject_records = $test_scores->getGRESubject($application_id);
        $toefl_records = $test_scores->getTOEFL($application_id);
        displayTestScores($gre_records, $gre_subject_records, $toefl_records);
        break;

    case "transcripts":

        $education = new DB_Education();
        $education_records = $education->getAll($application_id);
        displayEducation($education_records);
        break;

    case "recommendations":
        $recommendations = new DB_Recommendations();
        $recommendation_records = $recommendations->getAll($application_id);
        displayRecommendations($recommendation_records);
        break;    
        
    case "payment":
        $payment = new DB_Payment();
        $payment_records = $payment->getPayment($application_id);
        $total_fees_record = $payment->getTotalFees($application_id);
        displayPayment($payment_records, $total_fees_record);
        break;

    case "publications":
        $publications = new DB_Publications();
        $publication_records = $publications->getPublications($application_id);
        displayPublications($publication_records);
        break;

    default:
    
        print "admin case not found";

}

?>

<?php
/*
<div class="closer" style="margin: 5px; font-size: 14px;">
    <a href="" style="text-decoration: none;" onClick="javascript: closeContentDiv(this, <?= $application_id ?>); return false;">[X] Close</a>
</div>
*/
?>