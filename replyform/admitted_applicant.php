<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php'; 
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';
include_once '../inc/applicationFunctions.php';
include_once '../apply/header_prefix.php'; 
include "../classes/DB_Applyweb/class.DB_Applyweb.php";
include_once '../replyform/classes/class.InvitedUser.php';
include_once '../classes/class.db_ReplyForm.php';
include_once '../classes/class.Period.php';
include_once '../replyform/stubs.php';
include_once '../inc/specialCasesApply.inc.php';
include_once '../inc/specialCasesAdmin.inc.php';

// Include the payment manager classes.
include "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
include "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
include '../classes/class.RegistrationFeePaymentManager.php';
include '../classes/class.RegistrationFeePayment.php';



if (!isset($_SESSION['open_reply_period'])) {
    // DebugBreak();
    header("Location: https://".$_SERVER['SERVER_NAME']."/apply/offline.php");
}

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
    $domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}

$open_period_array = $_SESSION['open_reply_period'];

$department_id = $_SESSION['reply_department'];     

include_once "form_data.php";                        
 
$prgsAccepted = applicant_admitted_programs($application_id, $_SESSION['activeReplyUmbrellaId'], $domainid, $department_id);


$id = -1;
if (!isset($err)) {
    $err = "";
}

if (isset($_POST['Submit1'])) {  
     logReplyformAccess("submitted data");
     $err = "Thank you, your data has been submitted";     
}
  
?>

<?php
$pageTitle = ''; 
include '../inc/tpl.pageHeaderReplyform.php';
?>
        <table width="55%" style="height: 400" border="0" cellpadding="0" cellspacing="0">
			
            <tr>
                <td>
                 <div style="margin:20px;width:200px;"><span class="title">Admitted Applicant</span> </div>
                </td>

                    <td style="text-align:left;width:20px">    
         
                            <?
                            if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                            && strstr($_SERVER['SCRIPT_NAME'], 'admitted_applicant.php') === false
                            ){ ?>
                                <a href="../apply/logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                                <? } else
                                            {
                                                if(strstr($_SERVER['SCRIPT_NAME'], 'index.php') === false 
                                                && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                                                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                                                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                                                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
                                                && strstr($_SERVER['SCRIPT_NAME'], 'admitted_applicant.php') === false)
                                                {
                                                //session_unset();
                                                //destroySession();
                                                //header("Location: index.php");
                                            ?><a href="home.php?unit=<?php echo $_SESSION['reply_department']; ?>" class="subtitle">Session expired - Please Login Again</a><?
                                                }
                                            }
                         ?>
                         </td>
                   
                   </tr>
                   <tr>
                        <td>
                            <div><p>
                                <?php
                                    $sql = "select content from content where name='Replyform Information' and domain_id=".$domainid;
                                    $result = mysql_query($sql)    or die(mysql_error());
                                    if ($result) {
                                        while($row = mysql_fetch_array( $result )) 
                                        {
                                            echo "<b>" . html_entity_decode($row["content"]) . "</b>";
                                        } 
                                    }                                                               
                                ?>
                            </p> </div>
                        </td>
                   </tr>
                   
                   <?php
                   if (isIniDomain($domainid) 
                        || isDesignDomain($domainid)
                        || isMseMsitEbizDomain($domainid)
                        || isMsaii($department_id)
                        || isPrivacy($department_id)
                        || isDesignPhdDomain($domainid)
                        || isMseCampus($department_id)
                        || isMseMITS($department_id)
                        || isMseESE($department_id)) 
                   {
                       if (!isMseMsitEbizDomain($domainid) && !isMsaii($department_id))
                       {
                            $admissionLetterLink = getAdmissionLetterLink();
                       } else {
                           $admissionLetterLink = "";
                       }
                       $paymentLink = getPaymentLink();
                       if ($admissionLetterLink || $paymentLink)
                       {
                   ?>
                   <tr>
                        <td>
                        <div style="margin-left:20px;">
                            <?php
                            if ($admissionLetterLink)
                            {
                                echo $admissionLetterLink;
                            }
                            if ($paymentLink)
                            {
                                if ($admissionLetterLink)
                                {
                                    echo '<br>';
                                }
                                echo $paymentLink;
                            }
                            ?>
                        </div>
                        </td>
                   </tr>
                   <?php
                        }
                   }
                   ?>
                   
                   <?php
                   if (isEnglishDomain($domainid)) {
                       ?>
                   <tr>
                        <td>
                        <div style="margin:20px"> 
                        <span class="tblItem">
            If you have question or concerns about completing this form, please email the Assistant Director of Graduate Programs 
	    <a href="mailto:EnglishGrad-admin@andrew.cmu.edu" target="_top">
                EnglishGrad-admin@andrew.cmu.edu </td>
                   </tr>
                   <?php } ?>
                    <tr>
                        <td>
                            <div style="margin:20px"><span class="errorSubTitle"><?=$err;?></span></div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"> </div>
                        </td>
                   </tr>
                  
                   <tr>
                        <td>
                            <div style="margin:20px">
                             <?PHP
    $results_array = array();
    if (sizeof ($prgsAccepted) > 1) {
        $prgsAcceptedArray = array($prgsAccepted[0], $prgsAccepted[1]);
        $prgQuery = "select CONCAT(d.name, ' ', p.linkword, ' ', f.name) as name
        FROM programs p
        inner join degree d on d.id = p.degree_id
        inner join fieldsofstudy f on f.id = p.fieldofstudy_id
        WHERE p.id IN (" . $prgsAccepted[0] . ", " . $prgsAccepted[1] . ")
        ORDER BY p.id ASC ";
        
        $result3 = mysql_query($prgQuery) or die(mysql_error());
        while ($row = mysql_fetch_array($result3)) {
            $results_array[] = $row['name'];
        }
    }
    if (sizeof($results_array) > 1) {
         ?>
        <input type="hidden" name="program" id="program" value="<?php print min($prgsAcceptedArray) ?>" />
        <input type="radio" name="decision" id="decision_accept" value= "accept" 
            <?PHP print $accept_status; ?>> I <b>ACCEPT</b>  your offer of admission for <?php print $results_array[0]; ?>
<br /> <br />
<input type="hidden" name="program2" id="program2" value="<?php print max($prgsAcceptedArray) ?>" />
<input type="radio" name="decision" id="decision_accept2" value= "accept2" 
    <?PHP print $accept_status2; ?>> I <b>ACCEPT</b>  your offer of admission for <?php print $results_array[1]; ?>
<br /> <br />
What were your primary reasons for deciding to come to CMU?
<br />                                        
<textarea name ="accept_reasons" rows="4" cols="50"><?php echo $accept_reasons; ?>
</textarea>
<br /><br />
<?php 

if ($department_id != 2 
    && !isEnglishProgram($prgsAccepted[0]) 
    && !isIniDomain($domainid)
    && !isDesignDomain($domainid)
    && !isMseMsitDomain($domainid)
    && !isPrivacyDomain($domainid)) { 
?>
<input type="radio" name="decision" id="decision_defer" value="defer" <?PHP print $defer_status; ?>>I <b>ACCEPT</b> your offer of admission and request a one year <b>DEFERRAL</b>.
<br />
You must inform us by January 1st if you will enroll for the following Fall semester<br /> <br />
<?php } ?>
<?php if ($department_id == 2 || $department_id == 88) { ?>
    <input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission, instead I plan to:
    <textarea  name="other_choice_location" rows="4" cols="50"> <?php echo $other_choice_location ?>
    </textarea>

    <br /> <br />
<?php } else {  ?>
    <input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission.
    <br /> <br />
<?php }
    } else { ?>
 
<input type="radio" name ="decision"  value= "accept" 
<?PHP print $accept_status; ?>> I <b>ACCEPT</b>  your offer of admission
<br /> <br />
What were your primary reasons for deciding to come to CMU?
<br />                                        
<textarea name ="accept_reasons" rows="4" cols="50"><?php echo $accept_reasons; ?>
</textarea>
<br /><br />
<?php 
if((isScsDomain($domainid) && $department_id != 2) && (isScsDomain($domainid) && $department_id != 6) 
    && (isScsDomain($domainid) && $department_id != 20) && (isScsDomain($domainid) && $department_id != 83) 
    && (isScsDomain($domainid) && $department_id != 88)
    && !isEnglishDomain($domainid) 
    && !isIniDomain($domainid)
    && !isDesignDomain($domainid)
    && !isDesignPhdDomain($domainid)
    && !isMseMsitEbizDomain($domainid)
    ) {?>
<input type="radio" name ="decision"  value="defer" <?PHP print $defer_status; ?>>I <b>ACCEPT</b> your offer of admission and request a one year <b>DEFERRAL</b>.
<br />
You must inform us by January 1st if you will enroll for the following Fall semester<br /> <br />
<?php } else if (isPrivacyDomain($domainid))
            {?>
                <input type="radio" name ="decision"  value="defer" <?PHP print $defer_status; ?>>I <b>ACCEPT</b> your offer of admission and request a one year <b>DEFERRAL</b>.
                <br />
                You must inform us by January 1st if you will enroll for the following Fall semester<br /> <br />
                <?php } ?>
<?php if ($department_id == 2 || $department_id == 88 ) { ?>
    <input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission, instead I plan to:
    <textarea rows= "4" cols="50" name="other_choice_location"><?php echo $other_choice_location ?> </textarea>

    <br /> <br />
<?php } else {  ?>
    <input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission.
    <br /> <br />
<?php } ?>


<?php } if ($department_id != 88 && $department_id != 2) {?>
Do you plan to attend another university or accept a job offer?  
<br /> <input type="radio" name ="other_choice"  value= "attend_other" <?PHP print $attend_other; ?>>
Plan to attend another university
<br /> <input type="radio" name ="other_choice"  value= "take_job" <?PHP print $take_job; ?>>
Plan to take a job
<br /><br />If so, where?
<br /><br />
<input type = "text" size = "65" name="other_choice_location" value=<?php echo "\"" . $other_choice_location . "\"" ?> />
<br /><br /> 
<?php } 
if ($department_id == 2 || $department_id == 88) {?>
Factors that influenced my decision were:
<?php } else { ?>
What were your primary reasons for your decision?
<?php } ?>
<br />
<textarea name="decision_reasons" rows="4" cols="50"><?php echo trim($reason_for_other); ?>
</textarea><br />
<br />
<?php 
if ($department_id != 88 && $department_id != 2 
    && !isMseMsitEbizDomain($domainid)
    && !isMsaii($department_id)
    && !isPrivacy($department_id)
    && !isMseMsitDomain($domainid)) {
?>
<h4>We would appreciate your cooperation in answering the following questions.  Your reply will be used for statistical purposes only.</h4>
Did you attend the Open House or visit Carnegie Mellon before making your decision?
<br /><br /> <input type= "radio" name="visit"  value= "yes" <?PHP print $visit_status; ?>>
Yes
<br /> <input type= "radio" name="visit"  value= "no" <?PHP print $visit_status_no; ?>>
No
<br /><br />
Was your visit helpful? <br />
<br /> <input type= "radio" name="visit_helpful"  value= "yes" <?PHP print $visit_helpful_status; ?>>
Yes
<br /> <input type= "radio" name="visit_helpful"  value= "no" <?PHP print $visit_helpful_status_no; ?>>
No
<br /><br /> Why or why not?
<br />
<textarea name="visit_comments" rows="4" cols="50"><?php echo trim($visit_comments); ?>
</textarea><br /><br />
<?php } ?>

Please list the other graduate schools to which you applied.
<br />
<textarea name="other_schools_applied" rows="4" cols="50"><?php echo $other_applied_schools; ?>
</textarea>
<br /><br />
<?php
    if (!isDesignDomain($domainid)) {
?>
Please list the other graduate schools to which you were accepted.
<br />
<textarea name="other_schools_accepted" rows="4" cols="50"><?php echo $other_schools_accepted; ?>
</textarea><br />

<?php
    }
if (isIniDomain($domainid) || isMseMsitEbizDomain($domainid) || isMsaii($department_id) || isPrivacy($department_id))
{
?>
<br />
Please indicate your marital status.
<br /><br /> <input type= "radio" name="marital_status"  value="single" <?PHP print $marital_status_single_status; ?>>
Single
<br /> <input type= "radio" name="marital_status"  value="married" <?PHP print $marital_status_married_status; ?>>
Married
<br /><br />
Are you affiliated with CMU?<br />
<br /> <input type= "radio" name="affiliated_cmu"  value="yes" <?PHP print $affiliated_cmu_status; ?>>
Yes
<br /> <input type= "radio" name="affiliated_cmu"  value="no" <?PHP print $affiliated_cmu_no_status; ?>>
No

<?    
}  
if (isMseMsitDomain($domainid))
{
?>
<br />
Please indicate your marital status.
<br /><br /> <input type= "radio" name="marital_status"  value="single" <?PHP print $marital_status_single_status; ?>>
Single
<br /> <input type= "radio" name="marital_status"  value="married" <?PHP print $marital_status_married_status; ?>>
Married
<br />
<?    
}  
?>

<P>
<input type = "Submit" name= "Submit1"  VALUE = "Submit Decision">
            </div>
                        </td>
                        
                   </tr>
                   <tr> 
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"></div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                        </td>
                   </tr>	
                   </table>
<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';

function getAdmissionLetterLink()
{
    $guid = NULL;
    $query = "SELECT guid FROM users WHERE id = " . $_SESSION['usermasterid'];
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result))
    {
        $guid = $row['guid'];
    }
    
    if (!$guid)
    {
        return NULL;
    }
    
    $filename = 'admissionLetter_' . $_SESSION['application_id'] . '_' . $_SESSION['reply_department'] . '.pdf';
    $path = $_SESSION['datafileroot'] . '/' . $guid . '/' . $filename;
    $fileLink = '';
    
    if (file_exists($path))
    {
        $fileHref = '../apply/fileDownload.php?file=' . urlencode($filename) .'&amp;guid=' . $guid;
        $fileLink = '<a href="' . $fileHref . '" target="_blank">View/Print Admission Letter</a>';
    }
    
    return $fileLink;     
}

function getPaymentLink()
{
    global $department_id;
    global $db_info;
    $paymentLink = '';
    
    if ($db_info)
    {
        foreach($db_info as $programId => $invitedUser)
        {

            if ($invitedUser->decision == 'accept')
            {
                $paymentManager = new RegistrationFeePaymentManager($_SESSION['application_id'], $department_id);
                
                if ($paymentManager->getFeePaid())
                {
                    $paymentLink = '<b>Registration fee paid</b>';    
                }
                elseif ($paymentManager->getFeeWaived())
                {
                    $paymentLink = '<i>Registration fee waived</i>';    
                }
                elseif (!$paymentManager->getBalanceDue())
                {
                    $paymentLink = '<i>Registration fee payment pending</i>';    
                }
                else
                {
                    $paymentLink = '<a href="payRegistrationFee.php">Pay Registration Fee</a>';    
                }
                 
                break;
            }
        }
    }
    
    return $paymentLink;    
}
?>