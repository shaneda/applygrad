<html>
<head>

 <?php
 $id = -1;
 include_once '../replyform/form_data.php';
 include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
 include_once "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
 include_once "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
 include_once "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
 include_once '../classes/class.RegistrationFeePaymentManager.php';
 include_once '../classes/class.RegistrationFeePayment.php';
 include_once '../replyform/stubs.php';
 ?>

 <title><?=$HEADER_PREFIX[$domain_id]?></title>
<link rel="stylesheet" href="../app2.css" type="text/css" />
<script type="text/javascript" src="../inc/scripts.js"></script>
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
</head>
        <body bgcolor="white">
        <form accept-charset="utf-8" action="" method="post" name="form1" id="form1" enctype="multipart/form-data" action="admitted_applicant.php">
        <?php
                $pageTitle = ''; 
                include '../inc/tpl.pageHeaderApply.php';
            ?>
        <table width="55%" style="height: 400" border="0" cellpadding="0" cellspacing="0">       
            <tr>
                <td>
                 <div style="margin:20px;width:400px;"><span class="title">Admitted Applicant - <br /><b><?php echo $applicant_name; ?></b></span> </div>
                </td>            
                   </tr>
                    <tr>
                        <td>
                            <div style="margin:20px"><span class="errorSubTitle"><?=$err;?></span></div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"> </div>
                        </td>
                   </tr>
                   <?php
                   $admissionLetterLink = getAdmissionLetterLink2($application_id, $current_dept);
                   $paymentLink = getPaymentLink2($application_id, $current_dept);
                   if ($admissionLetterLink || $paymentLink)
                       {
                   ?>
                   <tr>
                        <td>
                        <div style="margin-left:20px;">
                            <?php
                            if ($admissionLetterLink)
                            {
                                echo $admissionLetterLink;
                            }
                            if ($paymentLink)
                            {
                                if ($admissionLetterLink)
                                {
                                    echo '<br>';
                                }
                                echo $paymentLink;
                            }
                            ?>
                        </div>
                        </td>
                   </tr>
                   <?php } ?>
                   <tr>
                        <td>
                            <div style="margin:20px">
                            
<?PHP
    $results_array = array();
    if (sizeof ($db_info) > 1) {
        $prgsAccepted = array();
        foreach ($db_info as $key => $value) {
            $prgsAccepted[] = $key;
        }
        $prgQuery = "select CONCAT(d.name, ' ', p.linkword, ' ', f.name) as name
        FROM programs p
        inner join degree d on d.id = p.degree_id
        inner join fieldsofstudy f on f.id = p.fieldofstudy_id
        WHERE p.id IN (" . $prgsAccepted[0] . ", " . $prgsAccepted[1] . ")
        ORDER BY p.id ASC ";
        
        $result3 = mysql_query($prgQuery) or die(mysql_error());
        while ($row = mysql_fetch_array($result3)) {
            $results_array[] = $row['name'];
        }
    }
    
    if (sizeof($results_array) > 1) {
        $status = "";
        $status2 = "";
        $status_decline = "";
        if ($db_info1->decision == 'decline') {
            $status = "";
        } else {
            $status = 'checked';
        }
        if ($db_info2->decision == 'decline') {
            $status2 = "";
        } else {
            $status2 = 'checked';
        }
        if ($db_info1->decision == 'decline' && $db_info2->decision == 'decline') {
            $status_decline = 'checked';
        }    
         ?>
         <input type="hidden" name="program" id="program" value="<?php print min($prgsAccepted) ?>" />
        <input type="radio" name="decision" id="decision_accept" value= "accept" 
            <?PHP print $status; ?>> I <b>ACCEPT</b>  your offer of admission for <?php print $results_array[0]; ?>
<br /> <br />
<input type="hidden" name="program2" id="program2" value="<?php print max($prgsAccepted) ?>" />
<input type="radio" name="decision" id="decision_accept2" value= "accept2" 
    <?PHP print $status2; ?>> I <b>ACCEPT</b>  your offer of admission for <?php print $results_array[1]; ?>
<br /> <br />
What were your primary reasons for deciding to come to CMU?
<br />                                        
<textarea name ="accept_reasons" rows="4" cols="50"><?php echo $accept_reasons; ?>
</textarea>
<br /><br />
<input type="radio" name="decision" id="decision_decline" value= "decline" <?PHP print $status_decline; ?>>I <b>DECLINE</b> your offer of admission.
<br /> <br />
    <?php } else {  
    ?>
<input type="radio" name ="decision"  value= "accept" 
<?PHP print $accept_status; ?>> I <b>ACCEPT</b>  your offer of admission
<br /> <br />
What were your primary reasons for deciding to come to CMU?
<br />                                        
<textarea name ="accept_reasons" rows="4" cols="50"><?php echo $accept_reasons; ?>
</textarea>
<br /><br />
<input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission.
<br /> <br />
<?php } ?>
Do you plan to attend another university or accept a job offer?  
<br /> <input type="radio" name ="other_choice"  value= "attend_other" <?PHP print $attend_other; ?>>
Plan to attend another university
<br /> <input type="radio" name ="other_choice"  value= "take_job" <?PHP print $take_job; ?>>
Plan to take a job
<br /><br />If so, where?
<br /><br />
<input type = "text" size = "65" name="other_choice_location" value=<?php echo "\"" . $other_choice_location . "\"" ?> />
<br /><br /> 

What were your primary reasons for your decision?
<br />
<textarea name="decision_reasons" rows="4" cols="50"><?php echo trim($reason_for_other); ?>
</textarea><br />
<br />
<h4>We would appreciate your cooperation in answering the following questions.  Your reply will be used for statistical purposes only.</h4>
Did you attend the Open House or visit Carnegie Mellon before making your decision?
<br /><br /> <input type= "radio" name="visit"  value= "yes" <?PHP print $visit_status; ?>>
Yes
<br /> <input type= "radio" name="visit"  value= "no" <?PHP print $visit_status_no; ?>>
No
<br /><br />
Was your visit helpful? <br />
<br /> <input type= "radio" name="visit_helpful"  value= "yes" <?PHP print $visit_helpful_status; ?>>
Yes
<br /> <input type= "radio" name="visit_helpful"  value= "no" <?PHP print $visit_helpful_status_no; ?>>
No
<br /><br /> Why or why not?
<br />
<textarea name="visit_comments" rows="4" cols="50"><?php echo trim($visit_comments); ?>
</textarea><br /><br />


Please list the other graduate schools to which you applied.
<br />
<textarea name="other_schools_applied" rows="4" cols="50"><?php echo $other_applied_schools; ?>
</textarea>
<!--- <br /><br />
Please list the other graduate schools to which you were accepted.
<br />
<textarea name="other_schools_accepted" rows="4" cols="50"><?php echo $other_schools_accepted; ?>
</textarea><br />    --->
<P>
<input type = "Submit" name= "Submit1"  VALUE = "Submit Decision">
            </div>
                        </td>
                        
                   </tr>
                   <tr> 
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"></div>
                        </td>
                   </tr>                       
        </form>
        </body>
</html>

