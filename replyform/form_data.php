<?PHP
include_once '../replyform/classes/class.InvitedUser.php';
include_once '../replyform/stubs.php';
include_once '../inc/specialCasesAdmin.inc.php';

$accept_status = "unchecked";
$accept_program_short = "unchecked";
$accept_program_medium = "unchecked";
$accept_program_long = "unchecked";
$decline_status = "unchecked";
$defer_status = "unchecked";
$accept_status2 = "unchecked";
$accept_program2_short = "unchecked";
$accept_program2_long = "unchecked";
$decline_status2 = "unchecked";
$defer_status2 = "unchecked";
$prog_length = "";
$attend_acc = "unchecked";

$attend_other = "unchecked";
$take_job = "unchecked";
$other_choice_location = "";
$attend_acc = "unchecked";
$not_attend_acc = "unchecked";

$accept_reasons = "";
$reason_for_other = "";
$visit_status = "";
$visit_status_no = "";
$visit_comments = "";
$other_applied_schools = "";
$other_schools_accepted = "";
$visit_helpful_status = "";
$visit_helpful_status_no = "";
$marital_status_single = "";
$marital_status_married = "";
$affiliated_cmu = "";
$affiliated_cmu_no = "";
if (!isset($err)) {
    $err = "";
}


function getDbRecord($application_id, $deptId) {
        $data_recs = array();
        $query = "SELECT sd.* FROM student_decision sd 
                  INNER JOIN lu_programs_departments lpd on lpd.program_id = sd.program_id and lpd.department_id = " . $deptId .  
                  " WHERE sd.application_id = " . $application_id . " ORDER BY program_id ASC";
        $results_array = mysql_query($query);
        $num_rows = mysql_num_rows($results_array);
     //   $results_array = $this->handleSelectQuery($query);
        if ($num_rows > 0) {
            while ($record_row = mysql_fetch_array($results_array)) {
                $student_decision = new InvitedUser();
                $student_decision->updateFromDbRecord($record_row);
                $curPrg = $student_decision->program_id;
                $data_recs[$curPrg] = $student_decision;   
            }
            return $data_recs;
        } else {
                 return FALSE;
        }
}

//   *******************************************
//     TODO: Get current state from db here
//      set variables above to information in db
//   *********************************************
$application_id = -1;

if (!isset($_SESSION)) {
    $exclude_scriptaculous = TRUE;
    include_once '../inc/config.php'; 
    include_once '../inc/session.php'; 
    include_once '../inc/db_connect.php';
}

if (isset ($_SESSION['userid'])) {
    $userid =  $_SESSION['userid'];
}

if (isset ($_GET['applicationId'])) {
    $application_id =  $_GET['applicationId'];
} else if (isset ($_SESSION['application_id'])) {
    $application_id =  $_SESSION['application_id'];
}

if (isset ($department_id)) {
    $current_dept = $department_id;
} else if (isset ($department)) {
    $current_dept = $department;
    
} else if (isset($unit_id)) {
      $current_dept = $unit_id;
} else if (isset($_SESSION['reply_department'])) {
      $current_dept = $_SESSION['reply_department'];
} else if (isset($_SESSION['roleDepartmentId'])) {
      $current_dept = $_SESSION['roleDepartmentId'];
} 
$db_info1 = $db_info2 = NULL;
$db_info = getDbRecord($application_id, $current_dept); 

if ($db_info) {
    $db_info1 =  &reset($db_info);
    $db_info2 = &next($db_info);
    //   fill in values for template
    $selected_decision_radio = NULL;
    $selected_program_length_radio = NULL;
    
    if (!$db_info2) {
        if ($db_info1->decision == 'decline') {
           $selected_decision_radio = "decline"; 
        } else if ($db_info1->decision == 'defer') {
           $selected_decision_radio = "defer"; 
        } else if ($db_info1->decision == 'accept') {
           $selected_decision_radio = "accept"; 
        }
    } else {
         if ($db_info1->decision == 'decline' && $db_info2->decision == 'decline') {
           $selected_decision_radio = "decline"; 
        } else if ($db_info1->decision == 'accept') {
           $selected_decision_radio = "accept"; 
        } else if ($db_info2->decision == 'accept') {
           $selected_decision_radio = "accept2"; 
        } else if ($db_info1->decision == 'defer') {
           $selected_decision_radio = "defer"; 
        } else if ($db_info2->decision == 'defer') {
           $selected_decision_radio = "defer2"; 
        }
    }
    if ($selected_decision_radio == "accept") 
    {
        $accept_status = "checked";
    }
    if ($selected_decision_radio == "accept2") 
    {
        $accept_status2 = "checked";
    }
    else if ($selected_decision_radio == "decline") {
        $decline_status = "checked";
    } 
    else if ($selected_decision_radio == "defer") {
        $defer_status = "checked";
    }
                                                           
    if ((isset($departmentTemplateName) && ($departmentTemplateName == "INI")) ||
        (isset($_SESSION['domain_name']) && $_SESSION['domain_name'] == "INI") ||
        // this is for the applicant to log in
        (isset($_SESSION['domainname']) && $_SESSION['domainname'] == "INI") ||
        $current_dept == 20)  {
         
        if (!$db_info2) {
            if ($db_info1->prog_length == 'long') {
               $selected_program_length_radio = "long"; 
            } else if ($db_info1->prog_length == 'short') {
               $selected_program_length_radio = "short"; 
            } else if ($db_info1->prog_length == 'medium') {
               $selected_program_length_radio = "medium"; 
            } 
        } else {
             if ($db_info1->prog_length == 'long') {
               $selected_program_length_radio = "long"; 
            } else if ($db_info2->prog_length == 'long') {
               $selected_program_length_radio = "long"; 
            } else if ($db_info1->prog_length == 'short') {
               $selected_program_length_radio = "short"; 
            } else if ($db_info2->prog_length == 'short') {
               $selected_program_length_radio = "short"; 
            }
        }
        if ($selected_decision_radio == "accept") 
        {
            if ($selected_program_length_radio == "short") {
                $accept_program_short = "checked";
                $accept_program_medium = "unchecked";
                $accept_program_long = "unchecked";
                $prog_length = "short";
            } else if ($selected_program_length_radio == "long") {
                    $accept_program_short = "unchecked";
                    $accept_program_medium = "unchecked";
                    $accept_program_long = "checked";
                    $prog_length = "long";
                 }
                else {
                    if ($selected_program_length_radio == "medium") {
                        $accept_program_short = "unchecked";
                        $accept_program_medium = "checked";
                        $accept_program_long = "unchecked";
                        $prog_length = "medium";
                     }
                     }
        }
        if ($selected_decision_radio == "accept2") 
        {
            if ($selected_program_length_radio == "short") {
                $accept_program2_short = "checked";
                $accept_program2_short = "unchecked";
            } else {
                if ($selected_program_length_radio == "long") {
                    $accept_program2_short = "unchecked";
                    $accept_program2_long = "checked";
                 }
            }
        }
    }
    
    if (!is_null($db_info1->attend_acc)) {
    
        if ($db_info1->attend_acc == "Yes") {
            $attend_acc = " checked ";
            $not_attend_acc = " unchecked ";

        }
        else if ($db_info1->attend_acc == "No") {
            $attend_acc = " unchecked ";
            $not_attend_acc = " checked ";
        }
    }
    
    if (!is_null($db_info1->other_choice)) {
        $selected_plan_radio = $db_info1->other_choice;
    
        if ($selected_plan_radio == "attend_other") {
            $attend_other = " checked ";
            $doing_other = 1;

        }
        else if ($selected_plan_radio == "take_job") {
            $take_job = " checked ";
            $doing_other = 0;
        }
    }
    
    if (!is_null($db_info1->visit)) {    
        $selected_visit_radio = $db_info1->visit;
        
            if ($selected_visit_radio == "1") {
                $visit_status = " checked ";
                $visit = 1;
            }
            else if ($selected_visit_radio == "0") {
                $visit_status_no = " checked "; 
                $visit = 0;
            }
    }
    if (!is_null($db_info1->visit_helpful)) {
        $visit_helpful =  $db_info1->visit_helpful;
        if ($visit_helpful == 0) {
            $visit_helpful_status_no =  " checked ";
        } else if ($visit_helpful == 1) {
              $visit_helpful_status =  " checked ";
        }
    }
    // DebugBreak();
    if (!is_null($db_info1->accept_reasons)) {
        $accept_reasons = $db_info1->accept_reasons;
    }
    if (!is_null($db_info1->other_choice)) {
        // DebugBreak();
        $other_choice = $db_info1->other_choice;
    }
    if (!is_null($db_info1->other_choice_location)) {
        $other_choice_location = $db_info1->other_choice_location;
    }
    if (!is_null($db_info1->decision_reasons)) {
        $reason_for_other = $db_info1->decision_reasons;
    }
    if (!is_null($db_info1->visit)) {
        $visit =  $db_info1->visit;  
    }
    if (!is_null($db_info1->visit_helpful)) {
        $visit_helpful =  $db_info1->visit_helpful;  
    }
    if (!is_null($db_info1->visit_comments)) {
        $visit_comments =  $db_info1->visit_comments;  
    } 
    if (!is_null($db_info1->other_schools_applied)) {
        $other_applied_schools = $db_info1->other_schools_applied;  
    }
    if (!is_null($db_info1->other_schools_accepted)) {
        $other_schools_accepted = $db_info1->other_schools_accepted;  
    } 
    // insert or update into database 
    // DebugBreak();
    if (isset ($visit)) {
        // DebugBreak();
    }
    if (isset ($doing_other)) {
        // DebugBreak();
    } 
    
    if (!is_null($db_info1->marital_status)) 
    {
        $marital_status = $db_info1->marital_status;
    
        if ($marital_status == "single") {
            $marital_status_single_status = " checked ";
        }
        else if ($marital_status == "married") {
            $marital_status_married_status = " checked ";
        }
    } 
    
    if (!is_null($db_info1->affiliated_cmu)) 
    {    
        $selected_affiliated_cmu_radio = $db_info1->affiliated_cmu;
        
        if ($selected_affiliated_cmu_radio == "1") {
            $affiliated_cmu_status = " checked ";
            $affiliated_cmu = 1;
        }
        else if ($selected_affiliated_cmu_radio == "0") {
            $affiliated_cmu_no_status = " checked "; 
            $affiliated_cmu = 0;
        }
    }
} 

if (isset($_POST['Submit1'])) 
{
    $err = "";
    if (isset($_POST['decision'])) 
    {
        $decisionProgram1 = filter_input(INPUT_POST, 'program', FILTER_VALIDATE_INT); 
        $decisionProgram2 = filter_input(INPUT_POST, 'program2', FILTER_VALIDATE_INT);
        $selected_decision_radio = $_POST['decision'];
        
        if ($selected_decision_radio == "accept") 
        {
            if ($_SESSION['domainname'] == 'SCS' && $current_dept == 20) {
                if (!isset($_POST['prog_length'])) {
                    $err .= "Please select program length for progam you are accepting the offer for";
                } else {
                    if($_POST['prog_length']  == 'long') {
                        $accept_program_short = "unchecked";
                        $accept_program_medium = "unchecked";
                        $accept_program_long = "checked";
                    } else if ($_POST['prog_length']  == 'medium') {
                        $accept_program_short = "unchecked";
                        $accept_program_medium = "checked";
                        $accept_program_long = "unchecked";
                    }
                    else  {
                        $accept_program_short = "checked";
                        $accept_program_medium = "unchecked";
                        $accept_program_long = "unchecked";
                    }
                    $prog_length = $_POST['prog_length'];
                }
            }
            $decision1 = 'accept';
            $decision2 = 'decline';
            $accept_status = "checked";
            $accept_status2 = "unchecked";
            $defer_status = "unchecked";
            $decline_status = "unchecked";
        }
        if ($selected_decision_radio == "accept2") 
        {   
            $decision1 = 'decline';
            $decision2 = 'accept';
            $accept_status = "unchecked";
            $accept_status2 = "checked";
            $defer_status = "unchecked";
            $decline_status = "unchecked";
        }
        else if ($selected_decision_radio == "defer") 
        {
            $decision1 = $decision2 = 'defer';
            $accept_status = "unchecked";
            $accept_status2 = "unchecked";
            $defer_status = "checked";
            $decline_status = "unchecked";
        }
        else if ($selected_decision_radio == "decline") 
        {
            $decision1 = $decision2 = 'decline';
            $accept_status = "unchecked";
            $accept_status2 = "unchecked";
            $defer_status = "unchecked";
            $decline_status = "checked";
        } 

        if ($db_info1) 
        {
            $db_info1->decision = $decision1;
            $db_info1->prog_length = $prog_length;
        }
        
        if ($db_info2)
        {
            $db_info2->decision = $decision2;
            $db_info1->prog_length = $prog_length;    
        }
    }
    
    if ($current_dept == 20) {
        
        if (isset($_POST['attend_acc'])) {
            if ($_POST['attend_acc'] == "Yes") {
                $attend_acc = "checked";
                $not_attend_acc = "unchecked";
            } else if ($_POST['attend_acc'] == "No") {
                $attend_acc = "unchecked";
                $not_attend_acc = "checked";
            }
            if ($db_info1) 
                {
                    $db_info1->attend_acc = $_POST['attend_acc'];
                }
                
                if ($db_info2) 
                {
                    $db_info2->attend_acc = $_POST['attend_acc'];
                }

           }
    }
    
    if (isset($_POST['other_choice'])) 
    {
        $selected_plan_radio = $_POST['other_choice'];
    
        if ($selected_plan_radio == "attend_other") 
        {
            $attend_other = " checked ";
            $doing_other = 1;
        }
        else if ($selected_plan_radio == "take_job") 
        {
            $take_job = " checked ";
            $doing_other = 0;
        }
        
        if ($db_info1) 
        {
            $db_info1->other_choice = $selected_plan_radio;
        }
        
        if ($db_info2) 
        {
            $db_info2->other_choice = $selected_plan_radio;
        }
    }
    
    if (isset($_POST['visit'])) 
    {    
        $selected_visit_radio = $_POST['visit'];
        
        if ($selected_visit_radio == "yes") 
        {
            $visit_status = " checked ";
            $visit_status_no = "";
            $visit = 1;
        }
        else if ($selected_visit_radio == "no") 
        {
            $visit_status_no = " checked ";
            $visit_status = ""; 
            $visit = 0;
        }
        
        if ($db_info1) 
        {
            $db_info1->visit = $visit;
        }
        
        if ($db_info2) 
        {
            $db_info2->visit = $visit;
        }
    }
    
    if (isset($_POST['visit_helpful'])) 
    {   
        $selected_visit_helpful_radio = $_POST['visit_helpful'];
        if ($selected_visit_helpful_radio == "yes") 
        {
            $visit_helpful_status = " checked ";
            $visit_helpful_status_no = "";
            $visit_helpful = 1;
        }
        else if ($selected_visit_helpful_radio == "no") 
        {
            $visit_helpful_status_no = " checked ";
            $visit_helpful_status = ""; 
            $visit_helpful = 0;
        }
        
        if ($db_info1) {
            $db_info1->visit_helpful = $visit_helpful;
        }
        
        if ($db_info2) {
            $db_info2->visit_helpful = $visit_helpful;
        }
    }
    
    if (isset($_POST['accept_reasons']) && trim($_POST['accept_reasons']) != "") 
    {
        $accept_reasons = $_POST['accept_reasons'];
        
        if ($db_info1) 
        {
            $db_info1->accept_reasons = $accept_reasons;
        }
        if ($db_info2) 
        {
            $db_info2->accept_reasons = $accept_reasons;
        }
    }
    
    if (isset($_POST['other_choice_location']) && trim($_POST['other_choice_location']) != "") 
    {
        $other_choice_location = $_POST['other_choice_location'];
        
        if ($db_info1) 
        {
            $db_info1->other_choice_location =  $other_choice_location;
        }
        
        if ($db_info2) 
        {
            $db_info2->other_choice_location =  $other_choice_location;
        }
    }
    
    if (isset($_POST['decision_reasons']) && trim($_POST['decision_reasons']) != "") 
    {
        $reason_for_other = $_POST['decision_reasons'];
        
        if ($db_info1) 
        {
            $db_info1->decision_reasons = $reason_for_other;
        }
        
        if ($db_info2) 
        {
            $db_info2->decision_reasons = $reason_for_other;
        }
    }
    
    if (isset($_POST['visit_comments']) && trim($_POST['visit_comments']) != "") 
    {
        $visit_comments =  $_POST['visit_comments'];
        
        if ($db_info1) 
        {
            $db_info1->visit_comments = $visit_comments;
        }  
        
        if ($db_info2) 
        {
            $db_info2->visit_comments = $visit_comments;
        } 
    } 
    
    if (isset($_POST['other_schools_applied']) && trim($_POST['other_schools_applied']) != "") 
    {
        $other_applied_schools = $_POST['other_schools_applied'];
        
        if ($db_info1) 
        {
            $db_info1->other_schools_applied = $other_applied_schools;
        }  
        
        if ($db_info2) 
        {
            $db_info2->other_schools_applied = $other_applied_schools;
        }
    }
    
    if (isset($_POST['other_schools_accepted']) && trim($_POST['other_schools_accepted']) != "") 
    {
        $other_schools_accepted = $_POST['other_schools_accepted'];
        
        if ($db_info1) 
        {
            $db_info1->other_schools_accepted = $other_schools_accepted;
        } 
        
        if ($db_info2) 
        {
            $db_info2->other_schools_accepted = $other_schools_accepted;
        } 
    } 
    
    if (isset($_POST['marital_status'])) 
    {
        $marital_status_radio = $_POST['marital_status'];
    
        if ($marital_status_radio == "single") 
        {
            $marital_status_single_status = " checked ";
            $marital_status_married_status = '';
        }
        else if ($marital_status_radio == "married") 
        {
            $marital_status_married_status = " checked ";
            $marital_status_single_status = '';
        }
        
        if ($db_info1) 
        {
            $db_info1->marital_status = $marital_status_radio;
        }
        
        if ($db_info2) 
        {
            $db_info2->marital_status = $marital_status_radio;
        }
    }
    
    if (isset($_POST['affiliated_cmu'])) 
    {   
        $affiliated_cmu_radio = $_POST['affiliated_cmu'];
        if ($affiliated_cmu_radio == "yes") 
        {
            $affiliated_cmu_status = " checked ";
            $affiliated_cmu_no_status = "";
            $affiliated_cmu = 1;
        }
        else if ($affiliated_cmu_radio == "no") 
        {
            $affiliated_cmu_no_status = " checked ";
            $affiliated_cmu_status = ""; 
            $affiliated_cmu = 0;
        }
        
        if ($db_info1) {
            $db_info1->affiliated_cmu = $affiliated_cmu;
        }
        
        if ($db_info2) {
            $db_info2->affiliated_cmu = $affiliated_cmu;
        }
    }
    
    // insert or update into database 
    // DebugBreak();
    if (isset ($visit)) {
        // DebugBreak();
    }
    if (isset ($doing_other)) {
        // DebugBreak();
    }
     
    if (isset($_SESSION['activeReplyUmbrellaId'])) {  //  Coming from applicant view
        $prgsAccepted = applicant_admitted_programs($application_id, $_SESSION['activeReplyUmbrellaId'], $_SESSION['domainid'], $current_dept);
    } else {      //  this is the admin side of the world

        $prgsAccepted = applicant_admitted_programs($application_id, $activeReplyUmbrellaId, $_SESSION['domainid'], $current_dept);
    }
    sort($prgsAccepted);
    foreach ($prgsAccepted as $prgId) {
       $admitted_program_id = $prgId;
       if ($db_info[$prgId]) {
           // update record
           $update_query = $db_info[$prgId]->buildUpdateCommand($admitted_program_id);
           mysql_query($update_query);
       } else {
           // insert record
           $columns = "(application_id, program_id, submitted, submitted_date";
           $values = "(" . $application_id . ", " . $admitted_program_id . ", 1, now()";
           $base_post_vars = array('accept_reasons', 'other_choice_location', 'decision_reasons', 'visit_comments', 'other_schools_applied', 'other_schools_accepted', 'marital_status');
           // DebugBreak();
           foreach ($base_post_vars as $var) {
               if (isset($_POST[$var])) {
                   if ($_POST[$var] != "" && trim($_POST[$var]) !== "") {
                       $columns .= ", " . $var;
                       $values .=  ", '" . trim(mysql_real_escape_string($_POST[$var])) . "'";
                   }
               }
           }
           
           if (isset($_POST['decision']))
           {
               $columns .= ", decision";
               
               if (($prgId == $decisionProgram1 && $_POST['decision'] == 'accept2')
                    || ($prgId == $decisionProgram2 && $_POST['decision'] == 'accept'))
               {
                   // The other program has been accepted, so decline this one.
                   $values .=  ", 'decline'";
               }
               elseif ($prgId == $decisionProgram2 && $_POST['decision'] == 'accept2')
               {
                    // This is the second program, which has been accepted
                   $values .=  ", 'accept'";    
               }
               else
               {
                   $values .=  ", '" . trim(mysql_real_escape_string($_POST['decision'])) ."'";    
               }
                       
           }
           
           if (isset($_POST['prog_length'])) {
               $columns .= ", prog_length";
               $values .=  ", '" . trim(mysql_real_escape_string($_POST['prog_length'])) ."'";
           }

           if ($current_dept == 20) {
               if (isset($_POST['attend_acc'])) {
                   $columns .= ", attend_acc";
                   $values .=  ", '" . trim(mysql_real_escape_string($_POST['attend_acc'])) ."'";
               } 
           }
           if (isset($_POST['other_choice'])) {
               $columns .= ", other_choice";
               $values .=  ", '" . trim(mysql_real_escape_string($_POST['other_choice'])) ."'";
           }
           if (isset($_POST['visit'])) {
               // DebugBreak();
               $columns .= ", visit";
               $values .=  ", '" . $_POST['visit'] . "'";
           }
           if (isset($_POST['visit_helpful'])) {
               // DebugBreak();
               $columns .= ", visit_helpful";
               $values .=  ", '" . $_POST['visit_helpful'] . "'";
           }
           
           if (isset($_POST['affiliated_cmu'])) {
               // DebugBreak();
               $columns .= ", affiliated_cmu";
               $values .=  ", '" . $_POST['affiliated_cmu'] . "'";
           }
          
           $columns .= ")";
           $values .= ")";
           $dbquery = "insert into student_decision " . $columns . " values " . $values;
           mysql_query($dbquery) or die(mysql_error());
           
       }
   }    
        
        if (!isset($domain_id) && isset($_SESSION['domainid'])) {
            $domain_id = $_SESSION['domainid'];
        }
        
        if (isIniDomain($domain_id) 
            || isDesignDomain($domain_id)
            || isDesignPhdDomain($domain_id)
            || isMseMsitEbizDomain($domain_id)
            || isMsaii($current_dept)
            || isPrivacy($current_dept)
            || isMseMITS($current_dept)
            || isMseCampus($current_dept)
            || isMseESE($current_dept)) {
                updateRegistrationFeeStatus($application_id, $current_dept);
        }
   
        $db_info = getDbRecord($application_id, $current_dept);
   
        if ($err == "") {
            $err = "Thank you, your information has been submitted";
        }    
    }
    
    
    function updateRegistrationFeeStatus($applicationId, $departmentId)
    {
        global $domainid;
        
        if (isset($_POST['decision']))
        {
            $decision = $_POST['decision'];
            $program1 = filter_input(INPUT_POST, 'program', FILTER_VALIDATE_INT); 
            $program2 = filter_input(INPUT_POST, 'program2', FILTER_VALIDATE_INT);
            
            if ($decision == 'accept')
            {
                $programId = $program1;            
            }
            elseif ($decision == 'accept2')
            {
                $programId = $program2;        
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }

        // Check for a status record for this department (should only be one)
        $existingStatusRecord = NULL;
        $exisitingStatusQuery = "SELECT * FROM registration_fee_status 
            WHERE application_id = " . $applicationId . "
            AND department_id = " . $departmentId . "
            LIMIT 1";
        $exisitingStatusResult = mysql_query($exisitingStatusQuery);
        if ($exisitingStatusResult) {
            while ($row = mysql_fetch_array($exisitingStatusResult))
            {
                $existingStatusRecord = $row;   
            } 
        }
        
        if (!$existingStatusRecord)
        {
            if (!isset($domainid)) {
                $domainid = $_SESSION['domainid'];
            }

            $registrationFee = getRegistrationFee($domainid);
            
            $newStatusQuery = "INSERT INTO registration_fee_status
                (application_id, department_id, amount, paid, waived) 
                VALUES (" 
                . $applicationId . ", "
                . $departmentId . ", "
                . $registrationFee . ", 0, 0)";
                
            mysql_query($newStatusQuery); 
        }
        
         
        
        // Insert/update status record on first 'accept'
//        foreach ($invitedUsers as $programId => $invitedUser)
//        {
//            if ($invitedUser->decision == 'accept')
//            {
//                $registrationFee = getRegistrationFee($programId);
//                
//                if (!$existingStatusRecord)
//                {
//                    $newStatusQuery = "INSERT INTO registration_fee_status
//                        (application_id, department_id, amount, paid, waived) 
//                        VALUES (" 
//                        . $applicationId . ", "
//                        . $departmentId . ", "
//                        . $registrationFee . ", 0, 0)";
//                }
//                
//                mysql_query($newStatusQuery);
//                break;       
//            }
//        }
    }
    
    function getRegistrationFee($domainId)
    {   
        $registrationFee = 0;

        if (isIniDomain($domainId))
        {
            $registrationFee = 100.00;    
        }
        
        if (isDesignDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        
        if (isStatisticsDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        
        if (isDesignPhdDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        
        if (isMseMsitEbizDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        
        if (isMITSDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        if (isMseMsitDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        if (isMseMsitESEDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        if (isPrivacyDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        if (isSCSDomain($domainId))
        {
            $registrationFee = 500.00;    
        }
        return $registrationFee;    
    }
   
?>
