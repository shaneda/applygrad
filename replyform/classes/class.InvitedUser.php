<?php
  class InvitedUser {
      private $application_id = NULL;
      private $program_id = NULL;
      private $decision = NULL;
      private $accept_reasons = NULL;
      private $other_choice = NULL;
      private $other_choice_location = NULL;
      private $decision_reasons = NULL;
      private $visit = NULL;
      private $visit_helpful = NULL;
      private $visit_comments = NULL;
      private $other_schools_applied = NULL;
      private $other_schools_accepted = NULL;
      private $submitted = NULL;
      private $submitted_date = NULL;
      private $marital_status = NULL;
      private $affiliated_cmu = NULL;
      private $prog_length = NULL;
      private $attend_acc = NULL;
      
      public function __get($property) {
        if (property_exists($this, $property)) {
          return $this->$property;
        }
      }

      public function __set($property, $value) {
        if (property_exists($this, $property)) {
          $this->$property = $value;
        }
        return $this;
      }
      
       public function updateFromDbRecord($record_row) {
        if ($record_row) {
            
            
            
            if (!is_null($record_row['application_id'])) {
                $this->application_id = $record_row['application_id'];
            }
            if (!is_null($record_row['program_id'])) {
                $this->program_id = $record_row['program_id']; 
            }
            if (!is_null($record_row['decision'])) {
                $this->decision = $record_row['decision']; 
            }
            if (!is_null($record_row['accept_reasons'])) {
                $this->accept_reasons = $record_row['accept_reasons'];   
            }
            if (!is_null($record_row['other_choice'])) {
                $this->other_choice = $record_row['other_choice'];  
            }
            if (!is_null($record_row['other_choice_location'])) {
                $this->other_choice_location = $record_row['other_choice_location']; 
            }
            if (!is_null($record_row['decision_reasons'])) {
                $this->decision_reasons = $record_row['decision_reasons'];
            }
            if (!is_null($record_row['visit'])) {
                $this->visit = $record_row['visit'];
            }
            if (!is_null($record_row['visit_helpful'])) {
                $this->visit_helpful = $record_row['visit_helpful'];
            }
            if (!is_null($record_row['visit_comments'])) {
                $this->visit_comments = $record_row['visit_comments']; 
            }
            if (!is_null($record_row['other_schools_applied'])) {
                $this->other_schools_applied = $record_row['other_schools_applied']; 
            }
            if (!is_null($record_row['other_schools_accepted'])) {
                $this->other_schools_accepted = $record_row['other_schools_accepted']; 
            }
            if (!is_null($record_row['submitted'])) {
                $this->submitted = $record_row['submitted']; 
            }
            if (!is_null($record_row['submitted_date'])) {
                $this->submitted_date = $record_row['submitted_date']; 
            }
            
            if (!is_null($record_row['marital_status'])) {
                $this->marital_status = $record_row['marital_status']; 
            }
            if (!is_null($record_row['affiliated_cmu'])) {
                $this->affiliated_cmu = $record_row['affiliated_cmu']; 
            }
            if (!is_null($record_row['prog_length'])) {
                $this->prog_length = $record_row['prog_length']; 
            }
            if (!is_null($record_row['attend_acc'])) {
                $this->attend_acc = $record_row['attend_acc']; 
            }
            // DebugBreak();
            
        } else {
            return $results_array;
            }
      }
      
      public function buildUpdateCommand($program_id)  {
           $fields = "";
           $base_post_vars = array('accept_reasons', 'other_choice_location', 'decision_reasons', 'visit_comments', 'other_schools_applied', 'other_schools_accepted', 'marital_status', 'affiliated_cmu');
           foreach ($base_post_vars as $var) {
               if ($this->$var != "" && trim($this->$var) !== "") {
                   if ($fields != "")
                    {
                        $fields .= ", " . $var . " = \"" . trim(mysql_real_escape_string($this->$var)) . "\"";
                    } else
                    {
                        $fields .= $var . " = \"" . trim(mysql_real_escape_string($this->$var)) . "\"";
                    } 
                   
               }
           }
           $radio_fields = array('decision', 'other_choice', 'visit', 'visit_helpful', 'marital_status', 'affiliated_cmu', 'prog_length', 'attend_acc');
           foreach ($radio_fields as $var) {
               if ($var != 'visit' && $var != 'visit_helpful' && $var != 'affiliated_cmu')  {
                   if (!is_null($this->$var)) {
                       if ($fields != "")
                            {
                                $fields .= ", " . $var . " = \"" . trim(mysql_real_escape_string($this->$var)) ."\"";
                            } else
                            {
                                $fields .= $var . " = \"" . trim(mysql_real_escape_string($this->$var)) ."\"";
                            }
                       } 
                   } else {
                        if (!is_null($this->$var)) {
                       if ($fields != "")
                            {
                                $fields .= ", " . $var . " = " . $this->$var ;
                            } else
                            {
                                $fields .= $var . " = " . $this->$var;
                            }
                       }
                   }
           }
           if ($fields != "")
           {
              $fields .= ", submitted_date = now()";
                    } else
                    {
                        $fields .= "submitted_date = now()";
                    } 
           
           $dbquery = "update student_decision set " . $fields . " where application_id = " . $this->application_id 
                    . " and program_id = " . $program_id;
           // DebugBreak();
           return  $dbquery;
      }

  }
  
?>
