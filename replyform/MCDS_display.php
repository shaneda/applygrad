<html>
<head>

 <?php
 $id = -1; 
 $exclude_scriptaculous = TRUE;
 include_once '../inc/functions.php';
 include_once '../inc/applicationFunctions.php';
 include_once '../apply/header_prefix.php';
 include_once '../inc/specialCasesApply.inc.php'; 
 include_once '../replyform/form_data.php';
 include_once "../classes/DB_Applyweb/class.DB_Applyweb.php";
 include_once "../classes/DB_Applyweb/class.DB_Applyweb_Table.php";
 include_once "../classes/DB_Applyweb/Table/class.DB_RegistrationFeeStatus.php";
 include_once "../classes/DB_Applyweb/Table/class.DB_RegistrationFeePayment.php";
 include_once '../classes/class.RegistrationFeePaymentManager.php';
 include_once '../classes/class.RegistrationFeePayment.php';
 include_once '../replyform/stubs.php';
 $domainname = $_SESSION['domainname'];
 $domainid = $_SESSION['domainid'];
 $sesEmail = ""; 
 
 if ($domainid == -1) {
     echo "You have reached this page in error.  Please go back to the URL given in your acceptance and try again";
 } else {
 ?>

 <title><?=$HEADER_PREFIX[$_SESSION['domainid']]?></title>
<link rel="stylesheet" href="../app2.css" type="text/css" />
<script type="text/javascript" src="../inc/scripts.js"></script>
<script language="javascript" src="../javascript/jquery-1.2.6.min.js"></script>
<script language="javascript">
    $(document).ready(function () {
            $("input[name$='decision']").change(function () { //use change event
            if (this.value == "accept" || this.value == "accept2") { //check value if it is accept
                $('#accept').show(); //than show
                } else {
                $('#accept').hide(); //else hide
                }
                });
}); 
    $(document).ready(function () {
            var value = $('input[name="decision"]:checked').val();
            if (value == "accept" || value == "accept2") { //check value if it is accept
                $('#accept').show(); //than show
                } else {
                $('#accept').hide(); //else hide
                }                                    
                });
        
            
</script>
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
</head>
        <body bgcolor="white">
        <form accept-charset="utf-8" action="" method="post" name="form1" id="form1" enctype="multipart/form-data" action="admitted_applicant.php">
        <?php
                $pageTitle = ''; 
                include '../inc/tpl.pageHeaderApply.php';
            ?>
        <table width="97%" style="height: 400" border="0" cellpadding="0" cellspacing="0">       
            <tr>
                <td>
                 <div style="margin:20px;width:400px;"><span class="title">Admitted Applicant - <br /><b><?php echo getNameFromApplication($application_id); ?></b></span> </div>
                </td>            
                   </tr>
                    <tr>
                        <td>
                            <div style="margin:20px"><span class="errorSubTitle"><?=$err;?></span></div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"> </div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <div style="margin:20px">
                            
<?PHP
    $results_array = array();
    if (sizeof ($db_info) > 1) {
        $prgsAccepted = array();
        foreach ($db_info as $key => $value) {
            $prgsAccepted[] = $key;
        }
        $prgQuery = "select CONCAT(d.name, ' ', p.linkword, ' ', f.name) as name
        FROM programs p
        inner join degree d on d.id = p.degree_id
        inner join fieldsofstudy f on f.id = p.fieldofstudy_id
        WHERE p.id IN (" . $prgsAccepted[0] . ", " . $prgsAccepted[1] . ")
        ORDER BY p.id ASC ";
        
        $result3 = mysql_query($prgQuery) or die(mysql_error());
        while ($row = mysql_fetch_array($result3)) {
            $results_array[] = $row['name'];
        }
    }
    
    if (isset($_SESSION['activeReplyUmbrellaId'])) {
         $application_period_num = $_SESSION['activeReplyUmbrellaId'];
       } else if (isset($activeReplyUmbrellaId)) {
            $application_period_num = $activeReplyUmbrellaId;
          }
    
    $acceptProgs = applicant_admitted_programs ($application_id, $application_period_num, $domainid, $current_dept);
    if ($db_info === FALSE) {
        $prgsAccepted = array();
        foreach ($acceptProgs as $prog) {
            $prgsAccepted[] = $prog;
        }
        if (sizeof($acceptProgs) > 1) {
            $prgQuery = "select CONCAT(d.name, ' ', p.linkword, ' ', f.name) as name
            FROM programs p
            inner join degree d on d.id = p.degree_id
            inner join fieldsofstudy f on f.id = p.fieldofstudy_id
            WHERE p.id IN (" . $prgsAccepted[0] . ", " . $prgsAccepted[1] . ")
            ORDER BY p.id ASC ";
        } else {
            $prgQuery = "select CONCAT(d.name, ' ', p.linkword, ' ', f.name) as name
            FROM programs p
            inner join degree d on d.id = p.degree_id
            inner join fieldsofstudy f on f.id = p.fieldofstudy_id
            WHERE p.id IN (" . $prgsAccepted[0] . ")
            ORDER BY p.id ASC ";
        }
        
        $result3 = mysql_query($prgQuery) or die(mysql_error());
        while ($row = mysql_fetch_array($result3)) {
            $results_array[] = $row['name'];
        }
        
    }

    if (sizeof($results_array) > 1){
        /*
        Should not happen in single program departments
         */ } else {  
    ?>
<input type="radio" name ="decision"  value= "accept" 
<?PHP print $accept_status; ?>> I <b>ACCEPT</b>  your offer of admission
   
<script language="javascript" src="../javascript/jquery-1.2.6.min.js"></script>

<script language="javascript" src="../../javascript/reviewView.js"></script> 
     
    <div id="accept" class="desc">
    <div>
    Program length desired:
     <br />
<!--     <div><label><input type="radio" name="prog_length" value="short"<?PHP print $accept_program_short; ?>>12-months (Without Internship) Fall/Spring/Summer (3-Semesters)</label></div>  
-->
     <div><label><input type="radio" name="prog_length" value="medium"<?PHP print $accept_program_medium; ?>>16-months (With Summer Internship) Fall/Spring/Fall (3-Semesters)</label></div>
     <div><label><input type="radio" name="prog_length" value="long"<?PHP print $accept_program_long; ?>>20-months (Additional Course work required to attain Degree (4-Semesters)</label></div>
    </div>
    <br />
    <br />
<!--    <div>
        <div>Do you plan to attend the <a href="http://www.cmu.edu/icc/programs/summer/">ACC Extensive Summer Program</a></div>
        <input type="radio" name ="attend_acc"  value= "Yes" <?PHP print $attend_acc; ?>> Yes
        <br />
        <input type="radio" name ="attend_acc"  value= "No" <?PHP print $not_attend_acc; ?>> No
    </div>
-->
    </div>
<br />

<br /> <br />
What were your primary reasons for deciding to come to CMU?
<br />                                        
<textarea name ="accept_reasons" rows="4" cols="50"><?php echo $accept_reasons; ?>
</textarea>
<br /><br />
<input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission.
<br /> <br />
<?php } ?>
Do you plan to attend another university or accept a job offer?  
<br /> <input type="radio" name ="other_choice"  value= "attend_other" <?PHP print $attend_other; ?>>
Plan to attend another university
<br /> <input type="radio" name ="other_choice"  value= "take_job" <?PHP print $take_job; ?>>
Plan to take a job
<br /><br />If so, where?
<br /><br />
<input type = "text" size = "65" name="other_choice_location" value=<?php echo "\"" . $other_choice_location . "\"" ?> />
<br /><br /> 

What were your primary reasons for your decision?
<br />
<textarea name="decision_reasons" rows="4" cols="50"><?php echo trim($reason_for_other); ?>
</textarea><br />
<br />
<h4>We would appreciate your cooperation in answering the following questions.  Your reply will be used for statistical purposes only.</h4>
<!--Did you attend the Open House or visit Carnegie Mellon before making your decision?
<br /><br /> <input type= "radio" name="visit"  value= "yes" <?PHP print $visit_status; ?>>
Yes
<br /> <input type= "radio" name="visit"  value= "no" <?PHP print $visit_status_no; ?>>
No
<br /><br />
Was your visit helpful? <br />
<br /> <input type= "radio" name="visit_helpful"  value= "yes" <?PHP print $visit_helpful_status; ?>>
Yes
<br /> <input type= "radio" name="visit_helpful"  value= "no" <?PHP print $visit_helpful_status_no; ?>>
No
<br /><br /> Why or why not?
<br />
<textarea name="visit_comments" rows="4" cols="50"><?php echo trim($visit_comments); ?>
</textarea>  -->
<br />


Please list the other graduate schools to which you applied.
<br />
<textarea name="other_schools_applied" rows="4" cols="50"><?php echo $other_applied_schools; ?>
</textarea>
<br /><br />
Please list the other graduate schools to which you were accepted.
<br />
<textarea name="other_schools_accepted" rows="4" cols="50"><?php echo $other_schools_accepted; ?>
</textarea><br />
<!--
<br />
Please indicate your marital status.
<br /><br /> <input type= "radio" name="marital_status"  value="single" <?PHP print $marital_status_single_status; ?>>
Single
<br /> <input type= "radio" name="marital_status"  value="married" <?PHP print $marital_status_married_status; ?>>
Married
<br /><br />
Are you affiliated with CMU?<br />
<br /> <input type= "radio" name="affiliated_cmu"  value="yes" <?PHP print $affiliated_cmu_status; ?>>
Yes
<br /> <input type= "radio" name="affiliated_cmu"  value="no" <?PHP print $affiliated_cmu_no_status; ?>>
No
-->

<P>
<input type = "Submit" name= "Submit1"  VALUE = "Submit Decision">
            </div>
                        </td>
                        
                   </tr>
                   <tr> 
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"></div>
                        </td>
                   </tr>                       
        </form>
        </body>
</html>
<?php }
?>

