<?php
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
 
  //badly named function - returns the list of programs from an open period
  function openProgramReplyPeriods($domainId) {      
      /*
      $openReplyPeriods = array of department => array (program ids)
       foreach department in $domainId {
        foreach program in department {
            if (program has open period) {
                if $openReplyPerdiods[department] does not exist {
                    set $openReplyPeriods [] to array([department] => array(programId) }
                    else {
                         set $openReplyPeriods[department] to array(programId) }
                    }}}
  }
  */
      //  $replyPeriods = array(department => array(program_ids));
      $replyPeriods = array();
      $domainUnit = unitFromDomain($domainId);
      $depts = convertUnitsToDepartments(domainUnitDepartments($domainUnit));
      foreach ($depts as $dept) {
          $progs = getDepartmentPrograms($dept);
          $testProgs =  getDepartmentPrograms(1);
          $replyPeriods[$dept] = $progs;
      }
      return $replyPeriods;
  }
  
  function openDepartmentPrograms ($domain) {
      $sql = "SELECT ldd.department_id , lpd.program_id 
        FROM lu_domain_department ldd
        inner join lu_programs_departments lpd 
        on lpd.department_id = ldd.department_id
        where ldd.domain_id = " . $domain;
      $results = mysql_query($sql);
      $program_ids = array();   
      while ($row = mysql_fetch_array($results)) {
            $program_ids[$row['department_id']][] = $row['program_id'];
      }
      return $program_ids;
  }
  
  function openProgramsForReply ($period_array, $department = 1) {
      $testsize = sizeof($period_array);
      if ($testsize > 1) {
      }
      $programs = $period_array[$department];     
      return  $programs;
  }
  
  function printProgramsforSql ($programs)  {
      $string_for_in = "(";
      $length = sizeof($programs);
      for ($i = 0; $i < $length; $i++) {
        if ($i == 0) {
          
            $string_for_in .= $programs[$i];
          } else {
                $string_for_in .= ", " . $programs[$i];
          }
      }
      $string_for_in .= ")";
      return $string_for_in;  
  }

  
  function applicant_admitted_programs ($application_id, $period_id = -1, $domain_id = -1, $department = 1)  {
      
        if (isIniDomain($domain_id))
        {
            $sql = "SELECT admission_program_id AS program_id 
                FROM application_decision_ini
                WHERE application_decision_ini.admission_status = 2 
                AND application_decision_ini.application_id = " . $application_id;  
        }
        elseif (isDesignDomain($domain_id))
        {
            $sql = "SELECT admission_program_id AS program_id 
                FROM application_decision_design
                WHERE application_decision_design.admission_status = 2 
                AND application_decision_design.application_id = " . $application_id;  
        }
        else
        {
            $sql = "SELECT lap.program_id 
                    FROM lu_application_programs lap
                    INNER JOIN period_application pa on pa.application_id =  lap.application_id AND pa.period_id = " . $period_id .
                    " INNER JOIN lu_programs_departments lpd on lpd.program_id = lap.program_id and lpd.department_id = " . $department . 
                    " WHERE lap.admission_status = 2 and lap.application_id = " . $application_id . " order by lap.program_id";
        }  
      $results = mysql_query($sql);
      $program_ids = array();   
      while ($row = mysql_fetch_array($results)) {
            $program_ids[] = $row['program_id'];
      }
      return $program_ids;
  }
  
  function unitFromDomain ($domain_id) {
      $sql = "SELECT unit_id FROM domain_unit WHERE domain_id = " . $domain_id;
      $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $unit_id = $row['unit_id'];
      }
      return $unit_id;
  }
  
  function domainUnitDepartments ($parentUnitId) {
      $departments = array();
      $sql = "SELECT unit.unit_id 
        FROM unit 
        INNER JOIN department_unit on unit.unit_id = department_unit.unit_id
        WHERE unit.unit_id = " . $parentUnitId . " 
        OR unit.parent_unit_id = " . $parentUnitId;
      $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $departments[] = $row['unit_id'];
      }
      return $departments;
  }
  
  function convertUnitsToDepartments ($units) {
       $sql_in_string = printProgramsforSql($units);
       $departments = array();
       $sql = "SELECT department_id FROM department_unit WHERE unit_id IN " . $sql_in_string;
       $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $departments[] = $row['department_id'];
      }
      return $departments;
  }
  
  function getDepartmentPrograms ($department_id) {
      $programs = array();
      $sql = "SELECT program_id FROM lu_programs_departments WHERE department_id = " . $department_id;
      $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $programs[] = $row['program_id'];
      }
      return $programs;      
  }
  
  function getNameFromApplication ($application_id) {
      $name = "";
      $sql = "select concat(u.lastname, ', ', u.firstname) as name
            from application a
            inner join lu_users_usertypes luu on luu.id = a.user_id
            inner join users u on u.id = luu.user_id
            where a.id = " . $application_id;
      $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $name = $row['name'];
      }
      return $name;
      }
   
   function getDomainFromDepartment($deptId) {
       $domain = array();
       if ($deptId == 3) {   // kludge for RI
           return 3;
       } else {
           $sql = "SELECT * from lu_domain_department
                where department_id = " . $deptId;
           $results = mysql_query($sql);
           while ($row = mysql_fetch_array($results)) {
                $domain[] = $row['domain_id'];
                }
           if (sizeof($domain) == 1) {
               return $domain[0];
           } else if (sizeof($domain) > 1) {   // kludgery for SCS/RI domain crap
                    $smallest = 999;
                    foreach ($domain as $value) {
                        if ($value < $smallest) {
                            $smallest = $value;
                        }
                    }
                    return $smallest;
                }
               
           }
       }
       
   function domainNameFromId ($domain_id) {
      $name = "";
      $sql = "SELECT name FROM domain WHERE id = " . $domain_id;
      $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $name = $row['name'];
      }
      return $name;      
  }
  
  function getDepartmentUnit($departmentId) {
    
    if (!$departmentId) {
        return NULL;
    }
    
    $unitId = NULL;
    
    $query = "SELECT unit_id FROM department_unit 
                WHERE department_id = " . $departmentId;
    $query .= " LIMIT 1";
    
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result)) {
        $unitId = $row['unit_id'];
    }
    
    if ($unitId) {
        return new Unit($unitId);    
    } else {
        return NULL;
    }
}

   function getDomainUnitforUnit($unitnum) {
       $sql = "SELECT parent_unit_id FROM unit WHERE unit_id = " . $unitnum;
       $results = mysql_query($sql);
      while ($row = mysql_fetch_array($results)) {
        $parent = $row['parent_unit_id'];
      }
      if (is_null($parent)) {
            return $unitnum;
      } else {
                return getDomainUnitforUnit($parent);
            }
   }
       
  
    function getAdmissionLetterLink2($applicationId, $department)
{
    $guid = NULL;
    $query = "SELECT users.guid
              FROM users
              INNER JOIN lu_users_usertypes ON lu_users_usertypes.user_id = users.id
              INNER JOIN application on application.user_id = lu_users_usertypes.id 
              AND application.id = " . $applicationId;
    $result = mysql_query($query);
    while ($row = mysql_fetch_array($result))
    {
        $guid = $row['guid'];
    }
    
    if (!$guid)
    {
        return NULL;
    }
    
    $filename = 'admissionLetter_' . $applicationId . '_' . $department . '.pdf';
    $path = $_SESSION['datafileroot'] . '/' . $guid . '/' . $filename;
    $fileLink = '';
    
    if (file_exists($path))
    {
        $fileHref = '../apply/fileDownload.php?file=' . urlencode($filename) .'&amp;guid=' . $guid;
        $fileLink = '<a href="' . $fileHref . '" target="_blank">View/Print Admission Letter</a>';
    }
    
    return $fileLink;     
}

function getPaymentLink2($applicationId, $department)
{  
    $paymentLink = '';
    $db_info1 = $db_info2 = NULL;
    $db_info = getDbRecord($applicationId, $department);
    
    if ($db_info)
    {
        foreach($db_info as $programId => $invitedUser)
        {

            if ($invitedUser->decision == 'accept')
            {
                $paymentManager = new RegistrationFeePaymentManager($applicationId, $department);
                
                if ($paymentManager->getFeePaid())
                {
                    $paymentLink = '<b>Admission deposit paid</b>';    
                }
                elseif ($paymentManager->getFeeWaived())
                {
                    $paymentLink = '<i>Admission deposit waived</i>';    
                }
                elseif (!$paymentManager->getBalanceDue())
                {
                    $paymentLink = '<i>Admission deposit payment pending</i>';    
                }
                else
                {
                    $paymentLink = '<a href="payRegistrationFee.php">Pay Registration Fee</a>';    
                }
                 
                break;
            }
        }
    }
    
    return $paymentLink;    
}
?>
