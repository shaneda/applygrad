<html>
<head>

 <?php
 $id = -1;
 include_once '../replyform/form_data.php';
 ?>

 <title><?=$HEADER_PREFIX[$domain_id]?></title>
<link rel="stylesheet" href="../app2.css" type="text/css" />
<script type="text/javascript" src="../inc/scripts.js"></script>
<link href="../css/SCSStyles_<?=$domainname?>.css" rel="stylesheet" type="text/css">
</head>
        <body bgcolor="white">
        <form accept-charset="utf-8" action="" method="post" name="form1" id="form1" enctype="multipart/form-data" action="admitted_applicant.php">
        <?php
                $pageTitle = ''; 
                include '../inc/tpl.pageHeaderApply.php';
            ?>
        <table width="55%" style="height: 400" border="0" cellpadding="0" cellspacing="0">       
            <tr>
                <td>
                 <div style="margin:20px;width:400px;"><span class="title">Admitted Applicant - <br /><b><?php echo $applicant_name; ?></b></span> </div>
                </td>            
                   </tr>
                    <tr>
                        <td>
                            <div style="margin:20px"><span class="errorSubTitle"><?=$err;?></span></div>
                        </td>
                   </tr>
                   <tr>
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"> </div>
                        </td>
                   </tr>
                  
                   <tr>
                        <td>
                            <div style="margin:20px">
                            

<input type="radio" name ="decision"  value= "accept" 
<?PHP print $accept_status; ?>> I <b>ACCEPT</b>  your offer of admission
<br /> <br />
What were your primary reasons for deciding to come to CMU?
<br />                                        
<textarea name ="accept_reasons" rows="4" cols="50"><?php echo $accept_reasons; ?>
</textarea>
<br /><br />
<input type="radio" name ="decision"  value= "decline" <?PHP print $decline_status; ?>>I <b>DECLINE</b> your offer of admission.
<br /> <br />
Do you plan to attend another university or accept a job offer?  
<br /> <input type="radio" name ="other_choice"  value= "attend_other" <?PHP print $attend_other; ?>>
Plan to attend another university
<br /> <input type="radio" name ="other_choice"  value= "take_job" <?PHP print $take_job; ?>>
Plan to take a job
<br /><br />If so, where?
<br /><br />
<input type = "text" size = "65" name="other_choice_location" value=<?php echo "\"" . $other_choice_location . "\"" ?> />
<br /><br /> 

What were your primary reasons for your decision?
<br />
<textarea name="decision_reasons" rows="4" cols="50"><?php echo trim($reason_for_other); ?>
</textarea><br />
<br />
<h4>We would appreciate your cooperation in answering the following questions.  Your reply will be used for statistical purposes only.</h4>
Did you attend the Open House or visit Carnegie Mellon before making your decision?
<br /><br /> <input type= "radio" name="visit"  value= "yes" <?PHP print $visit_status; ?>>
Yes
<br /> <input type= "radio" name="visit"  value= "no" <?PHP print $visit_status_no; ?>>
No
<br /><br />
Was your visit helpful? <br />
<br /> <input type= "radio" name="visit_helpful"  value= "yes" <?PHP print $visit_helpful_status; ?>>
Yes
<br /> <input type= "radio" name="visit_helpful"  value= "no" <?PHP print $visit_helpful_status_no; ?>>
No
<br /><br /> Why or why not?
<br />
<textarea name="visit_comments" rows="4" cols="50"><?php echo trim($visit_comments); ?>
</textarea><br /><br />


Please list the other graduate schools to which you applied.
<br />
<textarea name="other_schools_applied" rows="4" cols="50"><?php echo $other_applied_schools; ?>
</textarea>
<br /><br />
Please list the other graduate schools to which you were accepted.
<br />
<textarea name="other_schools_accepted" rows="4" cols="50"><?php echo $other_schools_accepted; ?>
</textarea><br />
<P>
<input type = "Submit" name= "Submit1"  VALUE = "Submit Decision">
            </div>
                        </td>
                        
                   </tr>
                   <tr> 
                        <td>
                            <div style="margin:20px"><hr size="1" noshade color="#990000"></div>
                        </td>
                   </tr>                       
        </form>
        </body>
</html>

