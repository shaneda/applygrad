<?php
// NOTE: $_GET['unit'] param is really department.id
// https://web28.srv.cs.cmu.edu/replyform/home.php?unit=93

include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php'; 
$exclude_scriptaculous = TRUE;
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php';
include_once '../classes/DB_Applyweb/class.DB_Applyweb.php';
include_once '../classes/DB_Applyweb/class.DB_Unit.php';
include_once '../classes/DB_Applyweb/class.DB_Period.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodProgram.php';
include_once '../classes/DB_Applyweb/class.DB_PeriodApplication.php';
include_once '../classes/class.Period.php';
include_once '../classes/class.Unit.php'; 
include '../classes/class.db_ReplyForm.php';
include_once '../inc/specialCasesApply.inc.php';
include './stubs.php';

function getUnitId ($domainId) {
    $unitId = -1;
    $sql = "SELECT unit_id FROM domain_unit
                WHERE domain_id = " . $domainId;
    
    $result = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_array( $result )) 
        {  
            $unitId = $row['unit_id'];
        }          
    return $unitId;
}


$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";

if(isset($_SESSION['domainid']))
{
    if($_SESSION['domainid'] > -1)
    {
        $domainid = $_SESSION['domainid'];
    }
}
if ($domainid == -1) {
    if (isset($_SESSION['A_usertypeid']) && $_SESSION['A_usertypeid'] == 19) {
        $deptRoleId = $_SESSION['roleDepartmentId'];
        $domainid = 37;
        $_SESSION['A_domanid'] = 37;
    } else {
        //header("Location: nodomain.php");
    }
}
if(isset($_SESSION['domainname']))
{
    $domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['email']))
{
    $sesEmail = $_SESSION['email'];
}

if (!isset($err)) {
    $err = "";
}
$accounts = array();
$recommender = false;
//session_unset($_SESSION['email']);
if(isset($_GET["l"]))
{
    $level = intval(htmlspecialchars($_GET["l"]));
    if($level != 1 || $level != 2)
    {
        //$err .= "Invalid domain request.<br>";
        $_SESSION['level'] = 2;
    }else
    {
        $_SESSION['level'] = $level;
    }
    
}

if(!isset($_POST['btnSubmit']) && !isset($_POST['btnLogin']))
{
    $_SESSION['firstname'] = "";
    $_SESSION['lastname'] = "";
    $_SESSION['userid'] = -1;
    $_SESSION['usermasterid'] = -1;
    $_SESSION['appid'] = -1;
    $_SESSION['email'] = "";
    
}



//ALLOW ADMIN TO ASSUME ROLE OF APPLICANT
if(isset($_GET['a']) && isset($_GET['uid']) && ($_SESSION['A_usertypeid'] == 0 || $_SESSION['A_usertypeid'] == 1))
{
    $_SESSION['appid'] = -1;
    $_SESSION['usermasterid'] = -1;
    $_SESSION['userid'] = -1;
    $_SESSION['firstname'] = "";
    $_SESSION['lastname'] = "";
    $_SESSION['email'] = "";
    $_SESSION['usertypeid'] = -1;
    
    $guid = addslashes(htmlspecialchars($_GET['uid']));
    //LOOKUP USER BY GUID
    $sql = $sql = "select users.id, lu_users_usertypes.id as uid,
        usertypes.id as typeid,
        usertypes.name as typename,
        users.firstname,
        users.lastname,
        users.email
        from users 
        inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
        inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
        where guid = '".htmlspecialchars($guid)."' and usertypes.id=5";
        $result = mysql_query($sql) or die(mysql_error());
        $doLogin = false;
        while($row = mysql_fetch_array( $result )) 
        {
        
            // PLB added check for applicationId param 
            // to enable multiple applications per user 9/22/09.
            if ( isset($_GET['applicationId']) ) {
                $_SESSION['appid'] = $_GET['applicationId'];    
            }
            
            $_SESSION['usermasterid'] = $row['id'];
            $_SESSION['userid'] = $row['uid'];
            $_SESSION['firstname'] = $row['firstname'];
            $_SESSION['lastname'] = $row['lastname'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['usertypeid'] = $row['typeid'];
            $arr = array();
            //array_push($arr,$row['uid']);
            array_push($arr,$row['typeid']);
            array_push($arr,$row['typename']);
            array_push($accounts, $arr);
            $doLogin = true;
        }
        if($doLogin == true)
        {
            if (isset ($_GET['role'])) {
            header("Location: admitted_applicant.php?role=reviewer");
            } else  {
                header("Location: admitted_applicant.php");
            }
        }

}


  //unit in this case will be the department id

if (!isset ($_GET['unit'])) {
    echo "You have reached this page in error.  Please go back to the URL given in your acceptance and try again";
} else {
    
    $unit_id = intval(htmlspecialchars($_GET['unit']));
    $_SESSION['reply_department'] = $unit_id;
    $dept_unit = getDepartmentUnit($unit_id);
    $dept_unit_id = $dept_unit->getId();
    $domain_unit_number = getDomainUnitforUnit($dept_unit_id);
    $domain_unit = new Unit($domain_unit_number);

    $programcheck = $domain_unit->getPrograms();
    // DebugBreak();
    $test = array();
    // $unit_domain_id = $domain_unit->getUnitDomainIds();
    $selected_domain = getDomainFromDepartment($unit_id);
    
    //$selected_domain = $unit_domain_id[0];
    
    if($_SESSION['domainid'] != $selected_domain)
    {
        $domain = $selected_domain;
        $sql = "select id, name from domain where id = ".$domain;
        $result = mysql_query($sql) or die(mysql_error());
        while($row = mysql_fetch_array( $result )) 
        {
            
            $_SESSION['domainid'] = $row['id'];
            $_SESSION['domainname'] = $row['name'];
        }
        if($_SESSION['domainid'] < 1 || $_SESSION['domainname'] == "")
        {
            $err .= "Invalid domain request.<br>";
        }else
        {  
            if (isEnglishDomain($_SESSION['domainid'])) {
                 $openPeriods = openDepartmentPrograms($_SESSION['domainid']);
            } else {
                 $openPeriods = openProgramReplyPeriods($_SESSION['domainid']);
            }
            if ((sizeof ($openPeriods)) > 0) {
                // DebugBreak();
                $srvname = $_SERVER["SERVER_NAME"];
                $scrname = $_SERVER["SCRIPT_NAME"];
                $query =  $_SERVER['QUERY_STRING'];
                header("Location: https://".$srvname.$scrname."?".$query);
            } else {
                // DebugBreak();
                $srvname = $_SERVER["SERVER_NAME"];
                header("Location: https://".$srvname."/apply/offline.php");
            }
        }
    }
///     dales   removed   }

if(isset($_POST['btnLogin']))
{
    if(isset($_POST['txtEmail']) && isset($_POST['txtPass']) )
    {
        if($recommender == true)
        {
            $passwordHash = htmlspecialchars(trim($_POST['txtPass']));
            
        }else
        {
            $passwordHash = sha1( htmlspecialchars( trim($_POST['txtPass']) ) );
        } 
    
     if (isEnglishDomain($_SESSION['domainid'])) {
         $openPeriods = openDepartmentPrograms($_SESSION['domainid']);
    } else {
         $openPeriods = openProgramReplyPeriods($_SESSION['domainid']);
    } 

    $activeReplyPeriods = $domain_unit->getActivePeriods(5);
    foreach ($activeReplyPeriods as $activeReplyPeriod) {
         $activeReplyPlaceoutPeriods = $activeReplyPeriod->getChildPeriods(5);
         $activeReplyUmbrellaId = $activeReplyPeriod->getId();
         $_SESSION['activeReplyUmbrellaId'] = $activeReplyPeriod->getId();
    }
    $_SESSION['open_reply_period'] = $openPeriods;

    $testUser = new DB_Invited_User();
    $isAuthenticated = $testUser->AuthenticatedUserP($_POST['txtEmail'], $passwordHash);
     if (!isset($isAuthenticated) || (sizeOf($isAuthenticated) < 1))  {
          $err .= "User authentication failed<br/>";
    } elseif (sizeOf($isAuthenticated) > 1)  {
        $err .= "Multiple users match credentials<br/>";
    } else {
        $luu_id = $isAuthenticated[0]['uid'];
    //    // DebugBreak();
    }
    if (isset($luu_id)) {
        // DebugBreak();
        $isAuthorized = $testUser->AuthorizedUserP($luu_id, $activeReplyUmbrellaId, $unit_id);
        if (sizeOf($isAuthorized) < 1)  {
              $err .= "User authorization failed<br/>";
        } elseif (sizeOf($isAuthorized) > 1)  {
            // $err .= "Admitted to multiple programs<br/>";
        } else {
    //            // DebugBreak();
        }
    } 

        if ($err == "") {
            $_SESSION['usermasterid'] = $isAuthenticated[0]['id'];
            $_SESSION['userid'] = $isAuthenticated[0]['uid'];                                
            $_SESSION['firstname'] = $isAuthenticated[0]['firstname'];
            $_SESSION['lastname'] = $isAuthenticated[0]['lastname'];
            $_SESSION['email'] = $isAuthenticated[0]['email'];
            $_SESSION['usertypeid'] = $isAuthenticated[0]['typeid'];
            $_SESSION['application_id'] = $isAuthorized[0]['id'];
            $_SESSION['activeReplyPeriod'] = $activeReplyPeriod;$_SESSION['replyform_enabled'] = TRUE;
            $arr = array();
            //array_push($arr,$row['uid']);
            array_push($arr,$isAuthenticated[0]['typeid']);
            array_push($arr,$isAuthenticated[0]['typename']);
            array_push($arr, $isAuthorized[0]['id']);
            array_push($accounts, $arr);
        }

        if($_SESSION['userid'] == -1 || $_SESSION['usermasterid'] == -1)
        {
            $err .= "Invalid username or password.<br>";
        }else
        {   
            if(count($accounts) > 1)
            {
           //    // DebugBreak();
            }
            else {
                if (sizeof($isAuthorized) > 0)   {
                    if (count($accounts) == 1) 
                        { 
                            $_SESSION['replyform_enabled'] = TRUE;
                            if ($domainname != "INI" && $dept_unit->getNameShort() != 'MCDS') {  
                                 header("Location: admitted_applicant.php");
                            } else { 
                                $departmentTemplateName = $dept_unit->getNameShort();
                                header("Location: ".$departmentTemplateName . "_display.php");
                            }
                           
                            }
                } else {
                    $err .= "<br />You are not authorized";
                }
            }
        }
    }
    else{
        $err .= "Please enter both your email address and password.<br>";
        $_SESSION['userid'] = -1;
    }
}else
{
    if(isset($_POST['btnSubmit']))
    {
        if(intval($_POST['lbUserType']) > -1)
        {
            $sql = "select lu_users_usertypes.id as uid
            from users 
            inner join lu_users_usertypes on lu_users_usertypes.user_id = users.id
            inner join usertypes on usertypes.id = lu_users_usertypes.usertype_id
            where users.id =".$_SESSION['usermasterid'] . " and lu_users_usertypes.usertype_id=".intval($_POST['lbUserType']);
            $result = mysql_query($sql) or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                $_SESSION['userid'] = $row['uid'];
                
            }
            $_SESSION['usertypeid'] = htmlspecialchars($_POST['lbUserType']);
            header("Location: admitted_applicant.php");
        }else
        {
            $err .="Please select a user type to log in as.<br>";
        }
    }
    
}
//GET ACCOUNT TYPES
$sesEmail = $_SESSION['email'];
?>


<?php
$pageTitle = '';
include '../inc/tpl.pageHeaderReplyform.php';
?>
        <table width="95%" height="400" border="0" cellpadding="0" cellspacing="0">
            
          <tr>
            <td valign="top">            
            <div style="text-align:right; width:680px">
            <? if($sesEmail != "" && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
            && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false
            ){ ?>
                <a href="../apply/logout.php<? if($domainid != -1){echo "?domain=".$domainid;}?>" class="subtitle">Logout </a>
                <!-- DAS Removed - <? echo $sesEmail;?>   -->
            <? }else
            {
                if(strstr($_SERVER['SCRIPT_NAME'], 'home.php') === false 
                && strstr($_SERVER['SCRIPT_NAME'], 'logout.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'accountCreate.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'forgotPassword.php') === false
                && strstr($_SERVER['SCRIPT_NAME'], 'newPassword.php') === false)
                {
                    //session_unset();
                    //destroySession();
                    //header("Location: index.php");
                  //                    // DebugBreak() ;
                    ?><a href="home.php?unit=<?php echo $unit_id; ?>" class="subtitle">Session expired - Please Login Again</a><?
                }
            } ?>
            </div>
            <div style="margin:20px;width:660px">
            <br>
            <br>
            <div class="tblItem" id="contentDiv">
            <span class="errorSubtitle"><?=$err;?></span>            
            <div style="width:100%" class="tblItem">
            <?
 //           $domainid = 1;
            if(isset($_SESSION['domainid']))
            {
                if($_SESSION['domainid'] > -1)
                {
                    $domainid = $_SESSION['domainid'];
                }
            }
            
            if ($domainid == -1) {
                if ($_SESSION['A_usertypeid'] == 19) {
                    $deptRoleId = $_SESSION['roleDepartmentId'];
                    $domainid = 37;
                    $_SESSION['A_domanid'] = 37;
                    
                    
                } else {
                    header("Location: nodomain.php");
                }
            }
            
                $sql = "select content from content where name='ReplyForm Index Page' and domain_id=".$domainid . " and department_id = " . $unit_id;
            
            $result = mysql_query($sql)    or die(mysql_error());
            while($row = mysql_fetch_array( $result )) 
            {
                echo html_entity_decode($row["content"]);
            }
            ?>
            
              <div align="center">
              <? if(count($accounts) > 1){ ?>
              <br>
              You are authorized to log in under more than one account type.<br>
            Please choose the account that you wish to use from the list below:
            <br>
            <? showEditText("", "listbox", "lbUserType", true,true, $accounts); ?>
            <? showEditText("Login", "button", "btnSubmit", true); ?>
            <span class="subtitle">
            
            </span><br>
            <? } else{ 
                    if ($unit_id == 79
                        || $unit_id == 18
                        || $unit_id == 82) {
                        ?>
                        <h2><strong>You are in a different system than you applied through.  Your password <br />
                        from the application system will not work.  Please use the <br /> 
                        <a href="forgotPassword.php" style="font-size: 18px; text-decoration: none">Forgot Password</a> <br />link
                        with the email address you used to apply to to set your password. </strong> </h2> <br /> <br />
                    <?php } ?>
             <table width="150" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td colspan="2" class="subTitle"><?
                    
                        echo "Returning Applicant";
                    
                    ?></td>
                  </tr>
                  <tr>
                    <td class="tblItem">User ID :</td>
                    <td class="tblItem"><input name="txtEmail" type="text" class="tblItem" id="txtEmail" maxlength="100" tabindex="1" ></td>
                  </tr>
                  <tr>
                    <td class="tblItem">Password:</td>
                    <td class="tblItem"><input name="txtPass" type="password" class="tblItem" maxlength="50" tabindex="2"></td>
                  </tr>
                  <tr>
                    <td class="tblItem"><input type="submit" name="btnLogin" value="Login" class="tblItem" tabindex="3" alt="Login"></td>
                    <td class="tblItem"><a href="forgotPassword.php<? if($recommender == true){echo "?r=1";} ?>">Forgot Password?</a> </td>
                  </tr>
                  
                </table>
            <? }//end if accounts ?>
 </div></div></div>

            </div>
            </td>
          </tr>
        </table>
<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
}
?>
