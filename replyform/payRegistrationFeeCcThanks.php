<?php 
include_once '../inc/config.php'; 
include_once '../inc/session.php'; 
include_once '../inc/db_connect.php';
$exclude_scriptaculous = TRUE; // Don't do the scriptaculous include in function.php    
include_once '../inc/functions.php';
include_once '../apply/header_prefix.php'; 

$_SESSION['SECTION']= "1";
$domainname = "";
$domainid = -1;
$sesEmail = "";
if(isset($_SESSION['domainname']))
{
	$domainname = $_SESSION['domainname'];
}
if(isset($_SESSION['domainid']))
{
	$domainid = $_SESSION['domainid'];
}
if(isset($_SESSION['email']))
{
	$sesEmail = $_SESSION['email'];
}

if(isset($_GET))
{
//	echo "get";
}

if(isset($_POST))
{
//	echo "post";
}


// Include the shared page header elements.
$pageTitle = 'Payment Complete';
include '../inc/tpl.pageHeaderReplyform.php';
?>

<span class="tblItem">
Your credit card payment has been received.  Please allow us several days
to process and credit your application fee.  Use the same User ID and
Password to access the submitted online application form and  track the
status of your application.
<br>
<br> 
</span>
<span class="subtitle"><a href="admitted_applicant.php">Return to homepage</a></span>
<br>

<?php
// Include the shared page footer elements. 
include '../inc/tpl.pageFooterApply.php';
?> 