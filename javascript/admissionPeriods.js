$(document).ready(function(){

    $("button.delete").click(function () {

        var record = $(this).parents(".record");
        var period = record.children(".description").text(); 
        var message = 'Delete ' + period + '?';
        if ( confirm(message) ) {
            var href = record.children(".admissionPeriodId").children("a").attr("href");
            window.location = href.replace('action=update', 'action=delete');
        } else {
            return false;
        }
      
    });    
    
});