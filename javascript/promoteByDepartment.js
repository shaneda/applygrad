$(document).ready(function(){ 

    $("#promotionHistory").dialog({ width: 460, autoOpen: false });
    
    $(".currentStatus").click(function(){

        var idArray = $(this).attr('id').split('_');
        var targetName = idArray[0];
        var applicationId = idArray[1];
        var programId = idArray[2]; 
        var applicantName = $('#applicantName_' + applicationId + '_' + programId).text();       

        
        $("#promotionHistory").attr('title', applicantName + ' - Promotion History');
        
        $.get("updatePromotionStatus.php", 
            {
            mode: 'getPromotionHistory',
            application_id: applicationId,
            program_id: programId
            },
            function(data)
            {
                if (data) 
                {
                    $("#promotionHistory").html('<b>' + applicantName + '</b><br/>' + data);    
                } 
                else 
                {  
                }
            },
            "text");
        
        $("#promotionHistory").dialog('open');        
    });
    
    $("table.datagrid").click(function(event){
    
        var $target = $(event.target);

        /*
        * New version for input.promotionStatus
        */
        if ( $target.is("input.promotionRound") ) {
            
            var idArray = $target.attr('id').split('_');
            var targetName = idArray[0];
            var applicationId = idArray[1];
            var programId = idArray[2];
            var departmentId = $('#departmentId').val();
            
            var promotionRoundValue = $target.attr('value');
            var currentRound = $('#currentRound_' + applicationId + '_' + programId);
            var currentRoundValue = currentRound.val();
            
            var applicantName = $('#applicantName_' + applicationId + '_' + programId).text();
            var error = 'Error: unable to update promotion status for ' + applicantName; 
            var message = applicantName + ' ';
            if (promotionRoundValue > currentRoundValue) {
                message += 'promoted';
            } else {
                message += 'demoted';    
            }
            message += ' from round ' + currentRoundValue;
            message += ' to round ' + promotionRoundValue;
            
            if (promotionRoundValue != currentRoundValue)  {
                
                var currentStatusMessage = promotionRoundValue;
                if (promotionRoundValue > currentRoundValue) {
                    currentStatusMessage += '&nbsp;&nbsp;<i>promotion</i>';
                } else {
                    currentStatusMessage += '&nbsp;&nbsp;<i>demotion</i>';     
                }
                
                $.get(
                    "updatePromotionStatus.php", 
                    {
                    application_id: applicationId,
                    program_id: programId, 
                    department_id: departmentId, 
                    previous_round: currentRoundValue,
                    new_round: promotionRoundValue
                    },
                    function(data)
                    {                                 
                        if (data == 1 || data == 2) 
                        {                        
                            currentRound.val(promotionRoundValue);
                            $('#currentStatus_' + applicationId + '_' + programId).html(currentStatusMessage);
                            $('#resetToVoting_' + applicationId + '_' + programId).removeAttr('disabled');
                            alert(message);
                        } 
                        else 
                        {
                            if (data == -1) 
                            {
                                $('#round' + promotionRoundValue + 'Radio_' + applicationId + '_' + programId).attr('checked', false);
                                $('#round' + currentRoundValue + 'Radio_' + applicationId + '_' + programId).attr('checked', true);
                                alert(error);    
                            } 
                        }
                    },
                    "text"
                );                
            }
        }

    
        /*
        * New version for input.promotionReset
        */        
        if ($target.is("input.promotionReset") ) {

            var idArray = $target.attr('id').split('_');
            var targetName = idArray[0];
            var applicationId = idArray[1];
            var programId = idArray[2];
            var departmentId = $('#departmentId').val();
            
            var promotionRoundValue = $target.attr('value');
            var currentRound = $('#currentRound_' + applicationId + '_' + programId);
            var votedRoundValue = $('#votedRound_' + applicationId + '_' + programId).val();            
            
            var applicantName = $('#applicantName_' + applicationId + '_' + programId).text();
            var error = 'Error: unable to reset promotion status for ' + applicantName; 
            var message = 'Promotion status set to voting for ' + applicantName;           
            
            if (promotionRoundValue != votedRoundValue)  {

                var currentStatusMessage = votedRoundValue;
                if (votedRoundValue > 1) {
                    currentStatusMessage += '&nbsp;&nbsp;<i>voting,&nbsp;reset</i>';
                } else {
                    currentStatusMessage += '&nbsp;&nbsp;<i>reset</i>';     
                }

                $.get("updatePromotionStatus.php", 
                    {
                    application_id: applicationId,
                    program_id: programId, 
                    department_id: departmentId, 
                    previous_round: 'NULL',
                    new_round: 0
                    },
                    function(data)
                    {
                        if (data == 1 || data == 2) 
                        {
                            currentRound.val(votedRoundValue);
                            $('#currentStatus_' + applicationId + '_' + programId).html(currentStatusMessage);
                            $('#round' + votedRoundValue + 'Radio_' + applicationId + '_' + programId).attr('checked', true);
                            $('#resetToVoting_' + applicationId + '_' + programId).attr('disabled', 'disabled'); 
                            
                            alert(message);   
                        } 
                        else 
                        {
                            alert(error);    
                        }
                    },
                    "text");
            }
        }

        
        /*
        * Original version for input.promotionStatus.
        */
        /*
        if ( $target.is("input.promotionStatus") ) {
            
            var id_array = $target.attr('id').split('_');
            var targetName = id_array[0];
            var applicationId = id_array[1];
            var programId = id_array[2];
            var applicantName = $('#applicantName_' + applicationId + '_' + programId).text();
            var promotionStatus = $target.attr('value');
            
            var message = 'Promotion status for ' + applicantName + ' updated: \n';
            if (promotionStatus == 1) {
                message += 'Promoted';  
            } else if (promotionStatus == 2) {
                message += 'Demoted';
            } else {
                message += 'Normal';
            }
            //message += ' (' + applicationId + '-' + programId + ')';
            var error = 'Error: unable to update promotion status for ' + applicantName;

            $.get("updatePromotionStatus.php", { 
                application_id: applicationId,
                program_id: programId, 
                promotion_status: promotionStatus
                },
                function(data){                             
                    if (data == 1) {
                        $('#statusReset_' + applicationId + '_' + programId).removeAttr('disabled');
                        $('#statusPromoteSpan_' + applicationId + '_' + programId).removeAttr('style');
                        $('#statusPromote_' + applicationId + '_' + programId).removeAttr('disabled');
                        $('#statusDemoteSpan_' + applicationId + '_' + programId).removeAttr('style');
                        $('#statusDemote_' + applicationId + '_' + programId).removeAttr('disabled');
                        $target.attr('disabled', 'disabled');
                        $target.next('span').attr('style', 'color: gray;');
                        if (targetName == 'statusPromote') {
                            $('#round2Span_' + applicationId + '_' + programId).text('Y');
                            $('#round2Span_' + applicationId + '_' + programId).attr('class', 'yes');    
                        } else {
                            $('#round2Span_' + applicationId + '_' + programId).text('N');
                            $('#round2Span_' + applicationId + '_' + programId).attr('class', 'no');   
                        }
                        alert(message);    
                    } else {
                        if (data == -1) {
                            alert(error);    
                        } 
                    }
                },
                "text");
        }
        */
        
        /*
        * Original version for input.promotionReset
        */        
        /*
        if ($target.is("input.promotionReset") ) {
            
            alert('!!!!');
            return false;
            
            var id_array = $target.attr('id').split('_');
            var applicationId = id_array[1];
            var programId = id_array[2];
            var applicantName = $('#applicantName_' + applicationId + '_' + programId).text();
            var unsetRound = $('#unsetRound_' + applicationId + '_' + programId).attr('value');
            
            var message = 'Promotion status for ' + applicantName + ' has been reset';
            //messsage += ' (' + applicationId + '-' + programId + ')'; 
            var error = 'Error: unable to reset promotion status for ' + applicantName;
            
            $.get("updatePromotionStatus.php", { 
                application_id: applicationId,
                program_id: programId, 
                promotion_status: 'NULL'
                },
                function(data){
                    if (data == 1) {
                        $('#statusPromote_' + applicationId + '_' + programId).attr('checked', false);
                        $('#statusDemote_' + applicationId + '_' + programId).attr('checked', false);
                        $target.attr('disabled', 'disabled');
                        $('#statusPromoteSpan_' + applicationId + '_' + programId).removeAttr('style');
                        $('#statusPromote_' + applicationId + '_' + programId).removeAttr('disabled');
                        $('#statusDemoteSpan_' + applicationId + '_' + programId).removeAttr('style');
                        $('#statusDemote_' + applicationId + '_' + programId).removeAttr('disabled');
                        if (unsetRound == 2) {
                            $('#statusPromoteSpan_' + applicationId + '_' + programId).attr('style', 'color: gray;');
                            $('#statusPromote_' + applicationId + '_' + programId).attr('disabled', 'disabled');                            
                            $('#round2Span_' + applicationId + '_' + programId).text('Y');
                            $('#round2Span_' + applicationId + '_' + programId).attr('class', 'yes');    
                        } else {
                            $('#statusDemoteSpan_' + applicationId + '_' + programId).attr('style', 'color: gray;');
                            $('#statusDemote_' + applicationId + '_' + programId).attr('disabled', 'disabled');
                            $('#round2Span_' + applicationId + '_' + programId).text('N');
                            $('#round2Span_' + applicationId + '_' + programId).attr('class', 'no');  
                        }
                        alert(message);   
                    } else {
                        alert(error);    
                    }
                },
                "text");
        }
        */

    
    });    
    
});