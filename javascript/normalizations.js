$(document).ready(function() {         

    $.tablesorter.addWidget({
        // give the widget a id
        id: "sortPersist",
        // format is called when the on init and when a sorting has finished
        format: function(table) {
            var sortList = table.config.sortList;
            if (sortList.length > 0) {
                $("input.tableSortList").val(sortList);
            }
        }
    });

    var tableSortList;
    if (typeof(globalTableSortList) != "undefined" && globalTableSortList != null) {
        tableSortList = globalTableSortList;
    }

    $("#applicantTable").tablesorter({
        widgets: ['sortPersist'],
        sortList: eval(tableSortList)
    });
    
    $("#normalizationsTable").tablesorter();     

});