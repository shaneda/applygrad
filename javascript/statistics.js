$(document).ready(function(){

    $(".rowHeading").mouseover(function(){
        $(this).parent().children().addClass("highlighted");  
    });

    $(".rowHeading").mouseout(function(){
        $(this).parent().children().removeClass("highlighted"); 
    });
    
    $(".columnHeading").mouseover(function(){
        var columnNumber = $(this).parent().children().index(this) + 1;
        if ( columnNumber != 1 ) {
            $(this).addClass("highlighted");    
            $(this).parents("table").find("td:nth-child(" + columnNumber + ")").addClass("highlighted");    
        }  
    });

    $(".columnHeading").mouseout(function(){
        var columnNumber = $(this).parent().children().index(this) + 1;
        if ( columnNumber != 1 ) {
            $(this).removeClass("highlighted");    
            $(this).parents("table").find("td:nth-child(" + columnNumber + ")").removeClass("highlighted");
        }
    });
    
    $(".crosstabCell").mouseover(function(){
        var columnNumber = $(this).parent().children().index(this) + 1;
        $(this).addClass("highlighted");
        $(this).siblings(":first").addClass("highlighted");
        $(this).parents("table").find("th:nth-child(" + columnNumber + ")").addClass("highlighted");   
    });    

    $(".crosstabCell").mouseout(function(){
        var columnNumber = $(this).parent().children().index(this) + 1;
        $(this).removeClass("highlighted"); 
        $(this).siblings(":first").removeClass("highlighted");
        $(this).parents("table").find("th:nth-child(" + columnNumber + ")").removeClass("highlighted");
    }); 
    
    
}); // close ($document).ready 