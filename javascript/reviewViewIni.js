$(document).ready(function(){

    /*
    * Start by hiding everything. 
    */
    $('.heading').children('.switch').html(' + '); 
    $('.accordion .heading').next().find('*').andSelf().hide();
    
    /*
    * Unhide sections opened prior to save POST request.
    */ 
    var allHeadingsOpen = true;
    $('.accordion .heading').each( function(){
    
        var headingId = $(this).attr('id');
        
        if( headingId == 'programs' ) {
            
            if ($('#programs_open').val() != '0') {
                $('.programs .heading').next().find('*').andSelf().show();
                $('.programs .heading').children('.switch').html(' &ndash; ');         
            } else {
                allHeadingsOpen = false;    
            }            
                    
        } else if ( headingId == 'supportingDocuments' ) {

            if ($('#supportingDocuments_open').val() != '0') {
                $('.supportingDocuments .heading').next().find('*').andSelf().show();
                $('.supportingDocuments .heading').children('.switch').html(' &ndash; ');         
            } else {
                allHeadingsOpen = false;
            }          
            
        } else {
            
            if ( $('#' + headingId + '_open').val() == '1' ) {
                $(this).next().find('*').andSelf().show();
                $(this).children('.switch').html(' &ndash; ');   
            } else {
                allHeadingsOpen = false;
            }
        }
        
    });
    
    if (allHeadingsOpen) {
        $('.applicantName').children('.switch').html(' &ndash;&nbsp;collapse&nbsp;all ');    
    } else {
        $('.applicantName').children('.switch').html(' +&nbsp;expand&nbsp;all ');    
    }
    
    $(".heading").keydown(function(event){
        if(event.keyCode == 13){    
            return false;
        }
    });
    
    $(".heading").keyup(function(event){
        if(event.keyCode == 13){
            $(this).click();
            return false;
        }
    });    

    $('.applicantName').click(function() {
        
        if ( $(this).children('.switch').html() == ' +&nbsp;expand&nbsp;all ' ) {
            
            $('.accordion .heading').next().find('*').andSelf().show();
            $('.accordion .heading').children('.switch').html(' &ndash; ');
            $(this).children('.switch').html(' &ndash;&nbsp;collapse&nbsp;all ');
            $('.accordion .heading').each( function(){
                var headingId = $(this).attr('id');
                $('#' + headingId + '_open').val('1');    
            });    
            
        } else {
            
            $('.accordion .heading').next().find('*').andSelf().hide();
            $('.accordion .heading').children('.switch').html(' + ');
            $(this).children('.switch').html(' +&nbsp;expand&nbsp;all ');
            $('.accordion .heading').each( function(){
                var headingId = $(this).attr('id');
                $('#' + headingId + '_open').val('0');    
            });
            
        }
        
        return false;
    });
    
    $('.accordion .heading').click(function(event) {
        var section = $(this).next();
        section.find('*').andSelf().toggle();
        if (section.is(':hidden')) {
            $('#' + event.target.id + "_open").val('0');
            $(this).children('.switch').html(' + ');
        } else {
            $('#' + event.target.id + "_open").val('1');
            $(this).children('.switch').html(' &ndash; ');
        }
        if ( $('.accordion .heading').next(':hidden').size() == 0 ) {
            $('.applicantName').children('.switch').html(' &ndash;&nbsp;collapse&nbsp;all ');    
        } else {
            $('.applicantName').children('.switch').html(' +&nbsp;expand&nbsp;all ');
        };
        return false;
    });
    
});

function toggleSemiblind() {
    if ($("#summary").is(":visible") ) {
        $("#summary").hide();
        $("#toggle_open").val("0");
        $("#form").height("66%");
        $("#semiblind").height("6%");
        $("#moeller").height("20%");
    } else {
        $("#form").height("44%");
        $("#semiblind").height("28%");
        $("#moeller").height("20%");
        $("#toggle_open").val("1");
        $("#summary").show();
    }
    return false;
}

function changeView(layout) {
    $('#viewLayout').val(layout);
    $('.clientState').clone().appendTo('#changeViewForm');
    $('#changeViewForm').submit();
    return false;    
}