$(document).ready(function() 
    {         

        $("#columnFilter").keyup(function(event){
            if(event.keyCode == 13){
                $("#applyFilter").click();
                return false;
            }
        });
        
        $("#columnFilter").keydown(function(event){
            if(event.keyCode == 13){
                return false;
            }
        });

        $("#applyFilter").click(function(){
            filterTable( document.getElementById('columnFilter') );
            return false;        
        });
        
        $("#clearFilter").click(function(){
            $("#column option:selected").removeAttr("selected");
            $("#column option[value='-1']").attr("selected", "selected");
            $("#columnFilter").val('');
            filterTable( document.getElementById('columnFilter') );
            return false;        
        });
        
        $.tablesorter.addWidget({
                // give the widget a id
                id: "renumber",
                // format is called when the on init and when a sorting has finished
                format: function(table) {
                    $(table).find('tr:visible').each(function(i) {
                        var numberText = i + '.';
                        $(this).children('td.row_number').text(numberText);
                    });
                }
        });

        $.tablesorter.addWidget({
            // give the widget a id
            id: "sortPersist",
            // format is called when the on init and when a sorting has finished
            format: function(table) {
                var sortList = table.config.sortList;
                if (sortList.length > 0) {
                    $("input.tableSortList").val(sortList);
                }
            }
        });

        var tableSortList;
        if (typeof(globalTableSortList) != "undefined" && globalTableSortList != null) {
            tableSortList = globalTableSortList;
        }
        
        $("#applicantTable").tablesorter({
            // Turn off sorting on the first two columns
            headers: { 0: { sorter: false}, 1: {sorter: false} },
            widgets: ['renumber', 'sortPersist'],
            sortList: eval(tableSortList)
        });


        var groupFilterSelect = $("#groupFilter");
        if (groupFilterSelect.length > 0) { 
            var groupFilterVal = $('#groupFilterValue').val();
            groupFilterSelect.val(groupFilterVal);           
        }
        
        var columnFilterInputValue = $('#columnFilterValue').val();
        if (columnFilterInputValue != '') {
            
            var columnFilterSelect = $('#column'); 
            var columnFilterSelectVal = $('#columnFilterSelectValue').val(); 
            columnFilterSelect.val(columnFilterSelectVal);
            
            var columnFilterInput = $('#columnFilter');
            columnFilterInput.val(columnFilterInputValue);
            filterTable(columnFilterInput[0]);

        }

        $("#applicantTable").css('visibility', 'visible');        
    } 
);

/*
* Clean up memory leaks?
*/
$(window).unload(function() {
        $('*').unbind();
});
      

function filterTable( inputTextField )
{
    var colField = document.getElementById('column');
    var colNum = colField.options[colField.selectedIndex].value;
    var text = inputTextField.value;
    var tableID = 'applicantTable';
    $("input.columnFilterSelectValue").val(colField.value);
    $("input.columnFilterValue").val(text);
    filter( text.toLowerCase(), tableID, colNum )

    if (text == '') {
        var groupSelect = $("#groupFilter");
        var colNum = $("#applicantTable th").index( $("th:contains('Groups')") );  
    }
}

function filterGroups( groupListSelect, colNum )
{
    var text = "";
    if ( groupListSelect.selectedIndex != -1 
            && groupListSelect.options[groupListSelect.selectedIndex].value != -1 ) { 

        text = groupListSelect.options[groupListSelect.selectedIndex].value;
    }
    var tableID = 'applicantTable';
    $("input.groupFilterValue").val(text);
    filter( text.toLowerCase(), tableID, colNum );
    return false;
}

function filter (text, tableID, colNum)
{
    var table = document.getElementById(tableID);
    var ele1;
    var ele2;
    /* subtract 1 for the heading row */
    var displayedRowCount = table.rows.length - 1;
    var displayedRowNumber = 0;
    for (var r = 1; r < table.rows.length; r++)
    {
        var cells = table.rows[r].cells;
        var match = 0;
        for ( var c = 0; c < cells.length; c++)
        {
            if ( colNum == c || colNum == -1 )
            {
                ele1 = cells[c].innerHTML.replace(/<[^>]+>/g,"");
                ele2 = ele1.replace(/&nbsp;/g, " ");
                if (ele2.toLowerCase().indexOf(text)>=0)
                {
                    match = 1;
                    break;
                } else {
                    match = 0;
                }
            }        
        }
        
        if ( match == 1 )
        {
            displayedRowNumber++;
            var displayStyle = '';
        } else {
            var displayStyle = 'none';
            displayedRowCount--;
        }
        table.rows[r].style.display = displayStyle;
        if (displayStyle == 'none')
        {
            $(table.rows[r]).find('.shiftclick').addClass('filterHidden');
        }
        else
        {
            $(table.rows[r]).find('.shiftclick').removeClass('filterHidden');
        }
        
        $(table.rows[r]).children('.row_number').text(displayedRowNumber+'.');
        $("#rowCount").text( '' + displayedRowCount );
    }

}

function preventSubmit(event)
{
    if (event.keyCode == 13)
    {
        return false;
    }
}

function openForm(userid, formname) { 
    var editForm = document.getElementById('editForm');
    editForm.elements['userid'].value = userid;
    editForm.action = formname;
    editForm.target = '_blank';
    editForm.submit();
}

/* This gets only the visible rows */
function getCurrentTable() {
    $("#datatodisplay").val( 
        $("<div/>").append(
            $("<table>").append( 
                $("#applicantTable tr:visible").clone()
                ) 
            ).html() 
        );
}