/* 
* This gets the visible rows of a table and clones them
* to a new #datatodisplay dom element.
*/
function getTable(targetTableId) {
    var targetTable = '#' + targetTableId + '  tr:visible'; 
    $("#datatodisplay").val( 
        $("<div/>").append(
            $("<table>").append( 
                $(targetTable).clone()
                ) 
            ).html() 
        );
}