$(document).ready(function(){ 

    $("table.datagrid").click(function(event){
    
        var $target = $(event.target);
        
        if ( $target.is("input.studentDecision") ) {

            
            var id_array = $target.attr('id').split('_');
            var applicationId = id_array[1];
            var programId = id_array[2];
            var decision = $target.attr('value');
            var applicantName = $('#applicantName_' + applicationId).text();
            var msg = "Decision status updated for " + applicantName + ":\n" + decision;
            //msg += " (" + applicationId + "-" + programId + ")"
            //alert(msg);

            $.get("updateStudentDecision.php", { 
                application_id: applicationId,
                program_id: programId, 
                decision: decision
                },
                function(data){                             
                    alert(msg);
                },
                "text");
        }
    
    });    
    
});