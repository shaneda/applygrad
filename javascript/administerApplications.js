$(document).ready(function(){

	/*
    * Bindings to change a filter select's color when it's "on"
    */
    $(window).load( function() {
        var optionValue;
        var selectName;
        $('select.filter option:selected').each( function() {
            optionValue = $(this).text();
            selectName = $(this).parent().attr('name'); 
            if ( optionValue == 'No Filter') {
                //$(this).parent().addClass('off');
                //$(this).parent().attr('style', 'background-color: white; padding: 0px; margin: 0px; font-size: ');
            } else {
                $('#' + selectName + 'Label').addClass('filterOn'); 
                //$(this).parent().addClass('on');
                //$(this).parent().attr('style', 'background-color: #FFFFE0; padding: 0px; margin: 0px;');    
            }    
        });
    });

    $('select.filter').change( function() {
        var optionValue = $('option:selected', this).text();
        var selectName = $(this).attr('name');
        if ( optionValue == 'No Filter') {
            $('#' + selectName + 'Label').removeClass('filterOn');
            //$(this).removeClass('on');
            //$(this).attr('style', 'background-color: white; padding: 0px; margin: 0px;');
        } else {
            $('#' + selectName + 'Label').addClass('filterOn');
            //$(this).addClass('on');
            //$(this).attr('style', 'background-color: #FFFFE0; padding: 0px; margin: 0px;');    
        }
    }); 
    
    
    /*
    * Function bindings to handle the "jump to applicant" autocomplete.
    * Added compbio kluge to handle multiple periods 10/13/09
    */
    var autocompleteExtraParams = {};
    var jumpToPeriodIds = $("input.jumpToPeriod");
    if (jumpToPeriodIds.size() > 1) {
        var periodIds = [];
        jumpToPeriodIds.each(function() {
            periodIds.push( $(this).attr('value') ); 
        });
        autocompleteExtraParams = {
            'unit': $("#jumpToUnit").attr("value"), 
            'unitId': $("#jumpToUnitId").attr("value"),
            'periodId[]': periodIds,
            'submitted': 1 
        };        
    } else {
        autocompleteExtraParams = {
            'unit': $("#jumpToUnit").attr("value"), 
            'unitId': $("#jumpToUnitId").attr("value"),
            'periodId': $("#jumpToPeriod").attr("value"),
            'submitted': 1 
        };    
    }

    
    $("input#suggest").autocomplete("administerApplicationsAutocomplete.php", {
        extraParams: autocompleteExtraParams,
        formatItem: function(row, i, max) {
            //return row[4] + ", " + row[5] + " (" + row[7] + ")";
            return row[3] + ", " + row[4] + " (" + row[6] + ")";
        },
        formatResult: function(row) {
            //return row[4] + ", " + row[5] + " (" + row[7] + ")";
            return row[3] + ", " + row[4] + " (" + row[6] + ")";
        }
    });

    $('input#suggest').setOptions({
        max: 15
    });

    $('input#suggest').result(function(event, data) {
        //$("#applicationId").val(data[0]);
        $("#luuId").val(data[1]);
    });
     
    
    /*
	* The following binds, through delegation, menu links with the handleMenuClick
    * function to open a new row/div for an individual application record.
	*/
	$('table.datagrid').click( function(event) {
	
		// check if clicked link is a menu link
		var $target = $(event.target);
		
        if ( $target.is("a.menu") ) {
            
            // handle the click
            handleMenuClick($target);

        } else if ($target.is("div.close")) {
            
            var applicationId = $target.attr("id").split("_")[1];
            
            // remove the content row
            $('#contentTr_' + applicationId).remove(); 
            
            // remove the "selected" status of the menu tr and td
            $("td.selected").removeClass("selected");
            $("tr.selected").removeClass("selected");
        }
	});


	/*
	* The following bind form element click events with functions to update the db via ajax requests.
	* They depend on the livequery extension to bind events to elements
	* created/modified after the DOM is initially loaded.
	*/
    
    $("input.waiveToefl", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var applicantName = $('#applicantName_' + applicationId).text();       
        
        var waiveToefl = 0;
        var message = 'Application for ' + applicantName + ' updated: \n';
        if ( $(this).is(":checked") ) {
            waiveToefl = 1;
            message += 'TOEFL Waived';
        } else {
            message += 'TOEFL Not Waived';
        }
        
        var error = 'Error: unable to waive TOEFL for ' + applicantName;

        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'waiveToefl', applicationId: applicationId, waiveToefl: waiveToefl },
            function(data){             
                reloadForm(applicationId, 'waiveToefl', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            'text'
        );
            
        return false;
    });
    
    /* Set/Unset GRE General Score Received */
    $("input.greReceived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var greScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();       
        
        var scoreReceived = 0;
        var message = 'GRE General for ' + applicantName + ' updated: \n';
        if ( $(this).is(":checked") ) {
            scoreReceived = 1;
            message += 'Received';
        } else {
            message += 'Not Received';
        }
        
        var error = 'Error: unable to update GRE General for ' + applicantName;

        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'greReceived', greScoreId: greScoreId, scoreReceived: scoreReceived },
            function(data){             
                reloadForm(applicationId, 'greScore', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            'text'
        );
            
        return false;
    });
    
    /*
    // Update GRE General Score
    */
    $("input.updateGreScore", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var greScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text(); 
        
        var testDate = $('#greTestDate_' + applicationId + '_' + greScoreId).val();
        
        var verbalScore = $('#greVerbalScore_' + applicationId + '_' + greScoreId).val();
        var verbalPercentile = $('#greVerbalPercentile_' + applicationId + '_' + greScoreId).val();
        
        var quantitativeScore = $('#greQuantitativeScore_' + applicationId + '_' + greScoreId).val();
        var quantitativePercentile = $('#greQuantitativePercentile_' + applicationId + '_' + greScoreId).val();
        
        var analyticalScore = $('#greAnalyticalScore_' + applicationId + '_' + greScoreId).val();
        var analyticalPercentile = $('#greAnalyticalPercentile_' + applicationId + '_' + greScoreId).val();
        
        var writingScore = $('#greWritingScore_' + applicationId + '_' + greScoreId).val();
        var writingPercentile = $('#greWritingPercentile_' + applicationId + '_' + greScoreId).val();
    
        var message = 'GRE General score updated for ' + applicantName;
        var error = 'Error: unable to update GRE General score for ' + applicantName;
        
        $.get(
            'administerApplicationsSingleUpdate.php', 
            { 
            mode: "updateGreScore", 
            greScoreId: greScoreId,
            testDate: testDate, 
            verbalScore: verbalScore,
            verbalPercentile: verbalPercentile,
            quantitativeScore: quantitativeScore,
            quantitativePercentile: quantitativePercentile,
            analyticalScore: analyticalScore,
            analyticalPercentile: analyticalPercentile,
            writingScore: writingScore,
            writingPercentile: writingPercentile
            },
            function(data){
                reloadForm(applicationId, 'greScore', $('#contentDiv_' + applicationId));
                if (data != -1) {
                    if (data != 0 && data != 1)
                    {
                        alert(data);    
                    }
                    else
                    {
                        alert(message);    
                    }   
                } else {
                    alert(error);    
                }           
            },
            'text'
        );

         return false;
    });    
    

    /* Set/Unset GRE Subject Score Received */
    $("input.greSubjectReceived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var greSubjectScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();       
        
        var scoreReceived = 0;
        var message = 'GRE Subject for ' + applicantName + ' updated: \n';
        if ( $(this).is(":checked") ) {
            scoreReceived = 1;
            message += 'Received';
        } else {
            message += 'Not Received';
        }
        
        var error = 'Error: unable to update GRE Subject for ' + applicantName;

        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'greSubjectReceived', greSubjectScoreId: greSubjectScoreId, scoreReceived: scoreReceived },
            function(data){            
                reloadForm(applicationId, 'greSubjectScore', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }        
            },
            'text'
        );
            
        return false;
    });
    
    /* Update GRE Subject Score */
    $("input.updateGreSubjectScore", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var greSubjectScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();
        
        var subjectName = $('#greSubjectName_' + applicationId + '_' + greSubjectScoreId).val();
        var subjectScore = $('#greSubjectScore_' + applicationId + '_' + greSubjectScoreId).val();
        var subjectPercentile = $('#greSubjectPercentile_' + applicationId + '_' + greSubjectScoreId).val();
        
        var message = 'GRE Subject score updated for ' + applicantName;
        var error = 'Error: unable to update GRE Subject score for ' + applicantName;
        
        $.get(
            "administerApplicationsSingleUpdate.php", 
            { 
                mode: "updateGreSubjectScore", 
                greSubjectScoreId: greSubjectScoreId, 
                subjectName: subjectName,
                subjectScore: subjectScore,
                subjectPercentile: subjectPercentile 
            },
            function(data){
                reloadForm(applicationId, 'greScore', $('#contentDiv_' + applicationId));
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }           
            },
            "text"
        );

        return false;  
    });    
    
    
    /* Set/Unset GMAT Score Received */
    $("input.gmatReceived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var gmatScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();       
        
        var scoreReceived = 0;
        var message = 'GMAT for ' + applicantName + ' updated: \n';
        if ( $(this).is(":checked") ) {
            scoreReceived = 1;
            message += 'Received';
        } else {
            message += 'Not Received';
        }
        
        var error = 'Error: unable to update GMAT for ' + applicantName;

        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'gmatReceived', gmatScoreId: gmatScoreId, scoreReceived: scoreReceived },
            function(data){             
                reloadForm(applicationId, 'gmatScore', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            'text'
        );
            
        return false;
    });
    
    /*
    // Update GMAT Score
    */
    $("input.updateGmatScore", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var gmatScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text(); 
        
        var verbalScore = $('#gmatVerbalScore_' + applicationId + '_' + gmatScoreId).val();
        var verbalPercentile = $('#gmatVerbalPercentile_' + applicationId + '_' + gmatScoreId).val();
        
        var quantitativeScore = $('#gmatQuantitativeScore_' + applicationId + '_' + gmatScoreId).val();
        var quantitativePercentile = $('#gmatQuantitativePercentile_' + applicationId + '_' + gmatScoreId).val();
        
        var totalScore = $('#gmatTotalScore_' + applicationId + '_' + gmatScoreId).val();
        var totalPercentile = $('#gmatTotalPercentile_' + applicationId + '_' + gmatScoreId).val();
        
        var writingScore = $('#gmatWritingScore_' + applicationId + '_' + gmatScoreId).val();
        var writingPercentile = $('#gmatWritingPercentile_' + applicationId + '_' + gmatScoreId).val();
    
        var message = 'GMAT score updated for ' + applicantName;
        var error = 'Error: unable to update GMAT score for ' + applicantName;
        
        $.get(
            'administerApplicationsSingleUpdate.php', 
            { 
            mode: "updateGmatScore", 
            gmatScoreId: gmatScoreId, 
            verbalScore: verbalScore,
            verbalPercentile: verbalPercentile,
            quantitativeScore: quantitativeScore,
            quantitativePercentile: quantitativePercentile,
            totalScore: totalScore,
            totalPercentile: totalPercentile,
            writingScore: writingScore,
            writingPercentile: writingPercentile
            },
            function(data){
                reloadForm(applicationId, 'gmatScore', $('#contentDiv_' + applicationId));
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }           
            },
            'text'
        );

         return false;
    }); 


    /* Set/Unset TOEFL Score Received */
    $("input.toeflReceived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var toeflScoreId = idPieces[2];
        var toeflType = $('#toeflType_' + applicationId + '_' + toeflScoreId).val();
        var applicantName = $('#applicantName_' + applicationId).text();       
        
        var scoreReceived = 0;
        var message = 'TOEFL ' + toeflType + ' for ' + applicantName + ' updated: \n';
        if ( $(this).is(":checked") ) {
            scoreReceived = 1;
            message += 'Received';
        } else {
            message += 'Not Received';
        }
        
        var error = 'Error: unable to update TOEFL ' + toeflType + ' for ' + applicantName;

        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'toeflReceived', toeflScoreId: toeflScoreId, scoreReceived: scoreReceived },
            function(data){            
                reloadForm(applicationId, 'toeflScore', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            'text'
        );
            
        return false;
    });    

    /* Update TOEFL Score */
    $("input.updateToeflScore", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var toeflScoreId = idPieces[2];
        var toeflType = $('#toeflType_' + applicationId + '_' + toeflScoreId).val();
        var applicantName = $('#applicantName_' + applicationId).text();
        var message = 'TOEFL ' + toeflType + ' score updated for ' + applicantName;
        
        var testDate = $('#toeflTestDate_' + applicationId + '_' + toeflScoreId).val();
        
        var section1Score = $('#toeflSection1Score_' + applicationId + '_' + toeflScoreId).val();
        var section2Score = $('#toeflSection2Score_' + applicationId + '_' + toeflScoreId).val();
        var section3Score = $('#toeflSection3Score_' + applicationId + '_' + toeflScoreId).val();
        var essayScore = $('#toeflEssayScore_' + applicationId + '_' + toeflScoreId).val();
        var totalScore = $('#toeflTotalScore_' + applicationId + '_' + toeflScoreId).val(); 
        
        var error = 'Error: unable to update TOEFL ' + toeflType + ' score for ' + applicantName;
        
        $.get("administerApplicationsSingleUpdate.php", 
            { 
            mode: "updateToeflScore",
            testDate: testDate, 
            toeflScoreId: toeflScoreId, 
            section1Score: section1Score,
            section2Score: section2Score,
            section3Score: section3Score,
            essayScore: essayScore,
            totalScore: totalScore
            },
            function(data){
                reloadForm(applicationId, 'toeflScore', $('#contentDiv_' + applicationId));
                if (data != -1) {
                    if (data != 0 && data != 1)
                    {
                        alert(data);    
                    }
                    else
                    {
                        alert(message);    
                    }   
                } else {
                    alert(error);    
                }         
            },
            "text");

         return false;
         
    }); 


    /* Set/Unset IELTS Score Received */
    $("input.ieltsReceived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var ieltsScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();       
        
        var scoreReceived = 0;
        var message = 'IELTS for ' + applicantName + ' updated: \n';
        if ( $(this).is(":checked") ) {
            scoreReceived = 1;
            message += 'Received';
        } else {
            message += 'Not Received';
        }
        
        var error = 'Error: unable to update IELTS for ' + applicantName; 

        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'ieltsReceived', ieltsScoreId: ieltsScoreId, scoreReceived: scoreReceived },
            function(data){             
                reloadForm(applicationId, 'ieltsScore', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            'text'
        );
            
        return false;
    });

    /*
    // Update IELTS Score
    */
    $("input.updateIeltsScore", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var ieltsScoreId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text(); 
        
        var listeningScore = $('#ieltsListeningScore_' + applicationId + '_' + ieltsScoreId).val(); 
        var readingScore = $('#ieltsReadingScore_' + applicationId + '_' + ieltsScoreId).val();
        var writingScore = $('#ieltsWritingScore_' + applicationId + '_' + ieltsScoreId).val();
        var speakingScore = $('#ieltsSpeakingScore_' + applicationId + '_' + ieltsScoreId).val();
        var overallScore = $('#ieltsOverallScore_' + applicationId + '_' + ieltsScoreId).val();
        
        var message = 'IELTS score updated for ' + applicantName;
        var error = 'Error: unable to update IELTS score for ' + applicantName;
        
        $.get(
            'administerApplicationsSingleUpdate.php', 
            { 
            mode: "updateIeltsScore",
            ieltsScoreId: ieltsScoreId, 
            listeningScore: listeningScore, 
            readingScore: readingScore,
            writingScore: writingScore,
            speakingScore: speakingScore,
            overallScore: overallScore
            },
            function(data){
                reloadForm(applicationId, 'ieltsScore', $('#contentDiv_' + applicationId));
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }          
            },
            'text'
        );

         return false;
    }); 
        
    
    /* Set/Unset Transcript Received */
    $("input.transcriptReceived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var transcriptId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();
        var institution = $('#institution_' + applicationId + '_' + transcriptId).text();
        var degree = $('#degree_' + applicationId + '_' + transcriptId).text();
              
        var transcriptReceived = 0;
        var message = 'Transcript for ' + applicantName +  ' from ' + institution + ' (' + degree + ') updated: \n';
        if ( $(this).is(":checked") ) {
            transcriptReceived = 1;
            message += 'Received';
        } else {
            message += 'Not Received';
        }
        
        var error = 'Error: unable to update transcript for ' + applicantName +  ' from ' + institution + ' (' + degree + ')';  
        
        $.get("administerApplicationsSingleUpdate.php", 
            { mode: 'transcriptReceived', transcriptId: transcriptId, transcriptReceived: transcriptReceived },
            function(data){
                reloadForm(applicationId, 'transcripts', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            "text");
    
        return false;
    }); 
    
    $("input.updateConvertedGpa", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var transcriptId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();
        var institution = $('#institution_' + applicationId + '_' + transcriptId).text();
        var degree = $('#degree_' + applicationId + '_' + transcriptId).text();
        var convertedGpa = $('#convertedGpa_' + applicationId + '_' + transcriptId).val();
        
        var message = 'Converted GPA for ' + applicantName +  ' from ' + institution + ' (' + degree + ') updated.';

        var error = 'Error: unable to update Converted GPA for ' + applicantName +  ' from ' + institution + ' (' + degree + ')';  
        
        $.get("administerApplicationsSingleUpdate.php", 
            { mode: 'updateConvertedGpa', transcriptId: transcriptId, convertedGpa: convertedGpa },
            function(data){
                reloadForm(applicationId, 'transcripts', $('#contentDiv_' + applicationId));                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }         
            },
            "text");
    
        return false;
    });    

    
    /* Upload Recommendation */
    $("input.uploadRecommendation", "div.content").livequery("click" , function(event){
        
        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var recommendationId = idPieces[2];
        var applicantId = $('#applicantId_' + applicationId).val();
        var applicantGuid = $('#applicantGuid_' + applicationId).val();
        var applicantName = $('#applicantName_' + applicationId).text();
        var recommenderName = $('#recommenderName_' + applicationId + '_' + recommendationId).text(); 
        var uploadMode = $('#uploadMode_' + applicationId + '_' + recommendationId).val();

        var uploadURL = "administerApplicationsSingleUpdate.php?mode=uploadRecommendation";
        uploadURL += "&applicationId=" + applicationId;
        uploadURL += "&applicantId=" + applicantId;
        uploadURL += "&applicantGuid=" + applicantGuid; 
        uploadURL += "&recommendationId=" + recommendationId;
        uploadURL += "&uploadMode=" + uploadMode;
        
        var fileElementId = 'uploadRecommendationFile_' + applicationId + '_' + recommendationId;
        
        var message = 'Recommendation letter for ' + applicantName + ' from ' + recommenderName + '\n';
        var errorMessage = message + 'Upload Failed \n\n';
        var successMessage = message + 'Upload Successful \n\n';
        
        $.ajaxFileUpload(
        {                
            url: uploadURL,
            secureuri: false,
            fileElementId: fileElementId,
            dataType: 'json',
            success: function (data, status)
            {
                reloadForm(applicationId, 'recommendations', $('#contentDiv_' + applicationId)); 
                if(typeof(data.error) != 'undefined') 
                {
                    if(data.error != '') {
                        alert(errorMessage + data.error);
                    } else {
                        alert(successMessage + data.msg);
                    }
                }
            },
            error: function (data, status, e){ alert(e); }
        });

        return false;     
    });
    

    /* Save note */
    $("input.saveNote", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var note = $('#note_' + applicationId).val();       
        
        $.get(
            'administerApplicationsSingleUpdate.php', 
            { mode: 'saveNote', applicationId: applicationId, note: note },
            function(data){            
                reloadForm(applicationId, 'notes', $('#contentDiv_' + applicationId));                
                /*
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }
                */        
            },
            'text'
        );
            
        return false;
    });
    
    /*
    * Set/Unset Hide Application
    */
    $("input.hideApplication", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var applicantName = $('#applicantName_' + applicationId).text();
        
        var hideApplication = 0;
        var alertMessage = "Application not hidden";  
        if ( $(this).is(":checked") ) {
            hideApplication = 1;
            alertMessage = "Application hidden";
        }
        alertMessage = alertMessage + ' for ' + applicantName + '.';
        
        var error = "Error: unable to hide application for " + applicantName;
             
        $.get('administerApplicationsSingleUpdate.php', 
            { mode: 'hideApplication', applicationId: applicationId, hideApplication: hideApplication },
            function(data){
                reloadForm(applicationId, 'notes', $('#contentDiv_' + applicationId));                              
                if (data != -1) {
                    alert(alertMessage);    
                } else {
                    alert(error);    
                } 
            },
            'text');
    });
    
    
    /*
    * Set/Unset Fee Waived
    */
    $("input.feeWaived", "div.content").livequery("click" , function(event){

        var applicationId = $(this).attr("id").split("_")[1];
        var applicantName = $('#applicantName_' + applicationId).text();
        
        var feeWaived = 0;
        var alertMessage = "Fee reinstated";
        var totalFees = $('#totalFees_' + applicationId).text();
        //var menuMessage = '<span class="confirm">due: ' + totalFees + '</span>';     
        if ( $(this).is(":checked") ) {
            feeWaived = 1;
            alertMessage = "Fee waived";
            //menuMessage = '<span class="confirm_complete">waived</span>';
        }
        alertMessage = alertMessage + ' for ' + applicantName + '.';
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        var page = '';
        if  ( selectedContentDiv.is(":visible") ) {
            var page = $('#payment_' + applicationId).attr("id").split("_")[0];
        }
        
        var error = "Error: unable to waive fee for " + applicantName;
                
        $.get('administerApplicationsSingleUpdate.php', { mode: 'feeWaived', applicationId: applicationId, feeWaived: feeWaived },
            function(data){
                if (page == 'payment') {
                    //$('#paymentMessage_' + applicationId).html(menuMessage);
                    reloadForm(applicationId, page, selectedContentDiv);
                }                                
                if (data != -1) {
                    alert(alertMessage);    
                } else {
                    alert(error);    
                } 
            },
            'text');
    });

    /*
    * Set/Unset Use Lower Fee
    */
    $("input.useLowerFee", "div.content").livequery("click" , function(event){

        var applicationId = $(this).attr("id").split("_")[1];
        var applicantName = $('#applicantName_' + applicationId).text();
        
        var useLowerFee = 0;
        var alertMessage = "Higher fee waiver removed";  
        if ( $(this).is(":checked") ) {
            useLowerFee = 1;
            alertMessage = "Higher fee waived";
        }
        alertMessage = alertMessage + ' for ' + applicantName + '.';
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        var page = '';
        if  ( selectedContentDiv.is(":visible") ) {
            var page = $('#payment_' + applicationId).attr("id").split("_")[0];
        }

        var error = "Error: unable to waive higher fee for " + applicantName;
                
        $.get('administerApplicationsSingleUpdate.php', 
            { mode: 'useLowerFee', applicationId: applicationId, useLowerFee: useLowerFee },
            function(data){
                if (page == 'payment') {
                    reloadForm(applicationId, page, selectedContentDiv);
                }                                
                if (data != -1) {
                    alert(alertMessage);    
                } else {
                    alert(error);    
                } 
            },
            'text');
    });
    
    /*
    // Set/Unset Payment Paid
    */
    $("input.paymentPaid", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var paymentId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();
        //var totalFees = $('#totalFees_' + applicationId).text();
        var paymentAmount = $('#paymentAmount_' + applicationId + '_' + paymentId).text();
        var paymentDate = $('#paymentDate_' + applicationId + '_' + paymentId).text();
        var paymentType = $('#paymentType_' + applicationId + '_' + paymentId).text();
        var systemConfirm = $('#systemConfirm_' + applicationId + '_' + paymentId).attr("value"); 
               
        var paymentPaid = 0;
        var message = paymentType + ' payment of ' + paymentAmount + ' on ' + paymentDate + ' by ' + applicantName;
        //var menuMessage = '';
        if ( $(this).is(":checked") ) {
            paymentPaid = 1;
            message = 'Confirmed Paid: ' + message;
            //menuMessage = '<span class="confirmComplete"><b>paid</b></span>: <span class="confirm">' + totalFees + '</span>';
        } else {
            
            message = 'Confirmed Unpaid: ' + message; 
            //menuMessage = '<span class="confirm">due: ' + totalFees + '</span>';
        }
        
        var error = 'Error: unable to update ' + ' payment of ' + paymentAmount + ' on ' + paymentDate + ' by ' + applicantName;
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        $.get("administerApplicationsSingleUpdate.php", { 
            mode: "paymentPaid", 
            applicationId: applicationId,
            paymentId: paymentId,
            systemConfirm: systemConfirm,
            paymentType: paymentType, 
            paymentPaid: paymentPaid},
            function(data){
                //$('#paymentMessage_' + applicationId).html(menuMessage); 
                reloadForm(applicationId, 'payment', selectedContentDiv);                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }          
            },
            "text");
    });      

    /*
    // Void Payment
    */
    $("input.voidPayment", "div.content").livequery("click" , function(event){
        
        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var paymentId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();
        var paymentAmount = $('#paymentAmount_' + applicationId + '_' + paymentId).text();
        var paymentDate = $('#paymentDate_' + applicationId + '_' + paymentId).text();
        var paymentType = $('#paymentType_' + applicationId + '_' + paymentId).text();
        
        var message = 'Void ' + paymentType + ' payment of ' + paymentAmount + ' on ' + paymentDate + '?';
        if ( !confirm(message) ) {
            return false;
        }

        var selectedContentDiv = $('#contentDiv_' + applicationId);
        $.get("administerApplicationsSingleUpdate.php", { 
                mode: "paymentVoid", 
                applicationId: applicationId,
                paymentId: paymentId},
            function(data){
                reloadForm(applicationId, 'payment', selectedContentDiv);         
            },
            "text");
    });

    /*
    // Add new payment
    */
    $("input.submitNewPayment", "div.content").livequery("click" , function(event){

        var applicationId = $(this).attr("id").split("_")[1];
        var paymentType = 2;
        if ( $('#newPaymentType_' + applicationId + '_1').is(":checked") ) {
            paymentType = 1;
        }
        if ( $('#newPaymentType_' + applicationId + '_3').is(":checked") ) {
            paymentType = 3;
        }
        var newPaymentAmount = $('#newPaymentAmount_' + applicationId).attr('value');
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        $.get("administerApplicationsSingleUpdate.php", { 
            mode: "newPayment", 
            applicationId: applicationId,
            paymentType: paymentType, 
            paymentAmount: newPaymentAmount,
            paymentPaid: 1},
            function(data){
                reloadForm(applicationId, 'payment', selectedContentDiv);        
            },
            "text");

         return false;
         
    });  
     

    /* 
    // Set/Unset Application Complete
    */ 
    $("input.applicationComplete", "tr.menu").livequery("click" , function(event){
              
        var applicationId = $(this).attr("id").split("_")[1];
        var applicantName = $('#applicantName_' + applicationId).text();        
        var message = applicantName + ' updated: ';
        var applicationComplete = 0;
        if ( $(this).is(":checked") ) {
            applicationComplete = 1;
            message += "Application Complete";
        } else {            
            message += "Application Not Complete";
        }
        
        var error = 'Error: unable to update completion status for ' + applicantName;

        $.get("administerApplicationsSingleUpdate.php", 
            { mode: "applicationComplete", 
                applicationId: applicationId, 
                applicationComplete: applicationComplete },
            function(data){                             
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }
            },
            "text");
    });   


    /*
    // Change publication status
    */
    $("select.publication_status", "div.content").livequery("change" , function(event){

        // get application id from form id
        var applicationId = $(this).parents("form").attr("id").split("_")[0];
        
        // get publication id from select element id
        var publicationId = $(this).attr("id").split("_")[0];
        
        // new status value
        var publicationStatus = $(this).val();
        //alert(publicationStatus);

        // variables to reload form
        var selectedContentDiv = $("#"+applicationId+"_content_div");
        var page = $(this).parents("form").attr("id").split("_")[1];
        
        $.get(
            "administerApplicationsSingleUpdate.php", 
            { 
                mode: "publication_status", 
                publication_id: publicationId,
                status: publicationStatus 
            },
            function(data){
                reloadForm(applicationId, page, selectedContentDiv);
                alert("Publication Status Updated");
                //alert(data);           
            },
            "text"
        );

    }); 


   /*
    // Set/Unset Application Submitted
    */
    $(".application_submitted").livequery("click" , function(event){
        
        // confirm change before doing anything
        if ( confirm("Are you sure you want to change the submission status?") ) {

            // check whether the box is checked        
            var msg = "Application Updated: ";
            if ( $(this).is(":checked") ) {
                var applicationSubmitted = 1;
                msg = msg + "Submitted";
            } else {
                var applicationSubmitted = 0;
                msg = msg + "Not Submitted";
            }
    
            // get application id from checkbox element id
            var applicationId = $(this).attr("id").split("_")[0];
        
            $.get("administerApplicationsSingleUpdate.php", { mode: "application_submitted", applicationId: applicationId, application_submitted: applicationSubmitted },
                function(data){                              
                    alert(msg);
                },
                "text");
        
        } else {
            // set the checkbox back to its previous state
            if ( $(this).is(":checked") ) {
                $(this).removeAttr("checked");
            } else {
                $(this).attr("checked", "checked");
            }
        }
    
    });
     
}); // close ($document).ready

/*
* Main function for opening a new row/div for an application record
* and switching the view in it.
*/
function handleMenuClick(linkObject) {

	if ( $(linkObject).hasClass("print") ) {
	    // open a new window, don't open content div 
		var idArray = $(linkObject).attr("id").split("_"); 
        var userId = idArray[1];
        var applicationId = idArray[2];
        if ($("#unit").val() == "department")
        {
            var unitId = $("#unitId").val();
            window.open("../review/userroleEdit_student_print.php?d="+unitId+"&id="+userId+"&applicationId="+applicationId);
            return false;        
        }
        window.open("../review/userroleEdit_student_print.php?id="+userId+"&applicationId="+applicationId);
		return false;
	}
    
    if ( $(linkObject).hasClass("login") ) {
        // follow the link, don't open the content div
        return false;
    } 

    // Otherwise, open the content div
    
    // set some variables for testing
    var selectedLinkTd = $(linkObject).parent("td");
    var selectedLinkTr = $(selectedLinkTd).parent("tr");
    
    // Get the number of columns in the datagrid
    var colspan = selectedLinkTr.children().length;
    
    // get the page and application id from the element id, e.g. payment_193
    var request_id = $(linkObject).attr("id");
    var requestArguments = request_id.split("_");
    var page = requestArguments[0];
    var applicationId = requestArguments[1];
    var unit = $('#filterUnit').val();
    var unitId = $('#filterUnitId').val();

    // if "closing" an open tab/section and not opening a new one 
    if ( selectedLinkTd.hasClass("selected") ) {
        
        // remove the content row
        var selectedContentTr = $('#contentTr_' + applicationId);
        selectedContentTr.remove(); 
        
        // remove the "selected" status of the menu tr and td
        selectedLinkTd.removeClass("selected");
        selectedLinkTr.removeClass("selected");
     
    } else {
    
        // opening new record or changing view in same record
                    
        // turn off style to "close" open tabs
        $("tr.menu td.selected").removeClass("selected");

        // if selecting different section of same record
        if ( selectedLinkTr.hasClass("selected") ) {
        
            // identify content tr of interest
            var selectedContentTr = $('#contentTr_' + applicationId);
        
            // identify the content div of interest
            var selectedContentDiv = $('#contentDiv_' + applicationId); 
        
            // empty the content div
            selectedContentDiv.html("");
        
        } else {
        
            // opening a new record

            var oldMenuRow = $("tr.selected", "table.datagrid");

            // remove the old content row
            oldMenuRow.next().remove();
            
            // de-select old menu row
            oldMenuRow.removeClass("selected");
            
            // add a row/div beneath the present one
            var bgcolor = $(selectedLinkTr).attr("bgcolor");
            var newRow = '<tr bgcolor="' + bgcolor + '" id="contentTr_' + applicationId + '">';
            newRow += '<td colspan="' +  colspan + '" class="content">';
            newRow += '<div class="closeBar"><div id="close_' + applicationId + '" class="close">X Close</div></div>';
            newRow += '<div class="content" id="contentDiv_' + applicationId + '">';
            newRow += '<div id="loading">LOADING</div>';
            newRow += '</div></td></tr>';
            selectedLinkTr.after(newRow);
            
            // add id and select the new row 
            selectedLinkTr.addClass("selected");
            
            // set content variables based on new row
            var selectedContentDiv = $('#contentDiv_' + applicationId); 
        
        } 

        // highlight the selected tab/link            
        selectedLinkTd.addClass("selected");

        // show the content div
        selectedContentDiv.show();
        var loading = $("#loading");
        loading.show();
    
        // submit the request
        if (page == 'ccPaymentDetails') {
            
            var paymentId = applicationId;
            
            $.ajax({
                method: 'get',
                url: 'getCcPaymentDetails.php',
                data: 'paymentId=' + paymentId,
                success: function(html){ //so, if data is retrieved, store it in html
                    loading.css("display", "none");
                    selectedContentDiv.html(html);
                    } // close success
            }); //close $.ajax(             
            
        } else if (page == 'ccPaymentSummary') {
            
            var paymentId = applicationId;
            
            $.ajax({
                method: 'get',
                url: 'getCcPaymentSummary.php',
                data: 'paymentId=' + paymentId,
                success: function(html){ //so, if data is retrieved, store it in html
                    loading.css("display", "none");
                    selectedContentDiv.html(html);
                    } // close success
            }); //close $.ajax(             
            
        } else {
            
            $.ajax({
                method: 'get',
                url: 'administerApplicationsSingle.php',
                //data: 'applicationId=' + applicationId + '&page=' + page,
                data: 'applicationId=' + applicationId + '&page=' + page + '&unit=' + unit + '&unitId=' + unitId,
                //beforeSend: function(){$("#loading").show("fast");},
                //complete: function(){ $("#loading").hide("fast");},
                success: function(html){ //so, if data is retrieved, store it in html
                    //$(selectedContentDiv).show();
                    loading.css("display", "none");
                    selectedContentDiv.html(html);
                    } // close success
            }); //close $.ajax(                   
        }
        

    
    } // close close/open div if/else
    return false;
            
}

/* 
* Function to reload a form/div that has just been updated. 
*/
function reloadForm(applicationId, page, selectedContentDiv) {

    var unit = $('#filterUnit').val();
    var unitId = $('#filterUnitId').val();
    
    $.ajax({
        method: "get", 
        url: "administerApplicationsSingle.php", 
        data: 'applicationId=' + applicationId + '&page=' + page + '&unit=' + unit + '&unitId=' + unitId,
        success: function(html){ 
            selectedContentDiv.html(html);
        },
        error: function() {
            selectedContentDiv.html("error");
        }
    }); 
}

/*
* Function for restoring the default filter/list.
*/
function restoreDefaultFilter() {
    document.filterForm.submissionStatus.value = 'submitted';
    document.filterForm.testScoreStatus.value = 'allTestScore';
    document.filterForm.completionStatus.value = 'allCompletion';
    document.filterForm.transcriptStatus.value = 'allTranscript';
    document.filterForm.paymentStatus.value = 'paidWaived';
    document.filterForm.recommendationStatus.value = 'allRecommendation';
    document.filterForm.round.value = '1';
    document.filterForm.hiddenStatus.value = 'unhidden';
    document.filterForm.submit();
    return false;
}

function restoreDefaultPlaceoutFilter() {
    document.filterForm.designStatus.value = 'allDesign';
    document.filterForm.programmingStatus.value = 'allProgramming';
    document.filterForm.statisticsStatus.value = 'allStatistics';
    document.filterForm.submit();
    return false;
}

/*
* Function for showing the full list with no filter.
*/
function setNoFilter() {
    document.filterForm.submissionStatus.value = 'allSubmission';
    document.filterForm.testScoreStatus.value = 'allTestScore';
    document.filterForm.completionStatus.value = 'allCompletion';
    document.filterForm.transcriptStatus.value = 'allTranscript';
    document.filterForm.paymentStatus.value = 'allPayment';
    document.filterForm.recommendationStatus.value = 'allRecommendation';
    document.filterForm.round.value = '1';
    document.filterForm.hiddenStatus.value = 'allHidden';
    document.filterForm.submit();
    return false;
}