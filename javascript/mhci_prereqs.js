$(document).ready(function(){

    
    $(".assessmentRadio:not(:checked)").click( function() {
        
        var prefix = $(this).attr("id").split("_")[0];
        $("#" + prefix + "_assessmentSubmit").removeAttr("disabled");    

    })
    
    
    $(".statusSelect").change( function() {
    
        var prefix = $(this).attr("id").split("_")[0];
        $("#" + prefix + "_assessmentSubmit").removeAttr("disabled");

    })
    
    
    $(".statusModifier").keyup( function() {
    
        var prefix = $(this).attr("id").split("_")[0];
        $("#" + prefix + "_assessmentSubmit").removeAttr("disabled");

    })
    
    
    $(".requiredData").keyup( function() {
        
        var prefix = $(this).attr("id").split("_")[0];
        var enable = "true";
        $(".requiredData[id^=" + prefix +"]").each(function() {

            //alert ($(this).attr("id") + ": " + $(this).val());
            if ( $(this).val() == "") {
                
                enable = "false";
                return false;    
                
            }
            
        });
        
        if (enable == "true") {

            $("#" + prefix + "_courseSubmit").removeAttr("disabled");
            $("#" + prefix + "_codeSampleSubmit").removeAttr("disabled");             
            
        } else {
            
            $("#" + prefix + "_courseSubmit").attr("disabled", "disabled");
            $("#" + prefix + "_codeSampleSubmit").attr("disabled", "disabled"); 
            
        }        

    })
    
    
});


function uploadCodeSample(buttonObject) {
    
    var prefix = $(buttonObject).attr("id").split("_")[0];
    var studentId = $("#" + prefix + "_studentId").val();
    var sampleId = $("#" + prefix + "_courseDatafileId").val();
    var codeDescription = $("#" + prefix + "_codeDescriptionInput").val();
    var noCache = new Date().getTime() + ""; 

    $.get("mhci_prereqsUploadCodeSample.php", {
        studentId: studentId,
        sampleId: sampleId,
        codeDescription: codeDescription,
        noCache: noCache
        },
        function(data){
            //alert(data);
            window.location = "fileUpload.php?" + data;
        },
        "text");
    

}


function uploadCourseDocument(buttonObject) {
    var prefix = $(buttonObject).attr("id").split("_")[0];
    var studentId = $("#" + prefix + "_studentId").val();
    var courseDatafileId = $("#" + prefix + "_courseDatafileId").val();
    var courseId = $("#" + prefix + "_courseId").val();
    var courseType = $("#" + prefix + "_courseType").val();       
    var courseName = $("#" + prefix + "_courseNameInput").val();
    var courseTime = $("#" + prefix + "_courseTimeInput").val();
    var courseInstitution = $("#" + prefix + "_courseInstitutionInput").val();
    var courseGrade = $("#" + prefix + "_courseGradeInput").val();
    var applicationId = $("#" + prefix + "_applicationId").val();
    var periodId = $("#" + prefix + "_periodId").val();
    var keywordTextarea = $("#" + prefix + "_keywordTextarea").val();
    var noCache = new Date().getTime() + ""; 

    $.get("mhci_prereqsUploadCourseDocument.php", {
        studentId: studentId,
        courseDatafileId: courseDatafileId,
        courseId: courseId,
        courseType: courseType,
        courseName: courseName,
        courseTime: courseTime,
        courseInstitution: courseInstitution,
        courseGrade: courseGrade,
        periodId: periodId,
        applicationId: applicationId,
        keywordTextarea: keywordTextarea,
        newUploadFormName: prefix,
        noCache: noCache
        },
        function(data){
            //alert(data);
            window.location = "../apply/fileUpload.php?" + data;
        },
        "text");
    

}



function persistCourseData(buttonObject) {
    
    var prefix = $(buttonObject).attr("id").split("_")[0];       
    var courseName = $("#" + prefix + "_courseNameInput").val();
    var courseTime = $("#" + prefix + "_courseTimeInput").val();
    var courseInstitution = $("#" + prefix + "_courseInstitutionInput").val();
    var courseGrade = $("#" + prefix + "_courseGradeInput").val();
    var keywordTextarea = $("#" + prefix + "_keywordTextarea").val();
    var noCache = new Date().getTime() + ""; 

    $.get("mhci_persistCourseData.php", { 
        courseName: courseName,
        courseTime: courseTime,
        courseInstitution: courseInstitution,
        courseGrade: courseGrade,
        keywordTextarea: keywordTextarea,
        newUploadFormName: prefix,
        noCache: noCache
        },
        function(data){
        },
        "text");

}
