$(document).ready(function(){

    $(".startDate").datepicker({ dateFormat: 'yy-mm-dd 00:00:00' });
    $(".endDate").datepicker({ dateFormat: 'yy-mm-dd 23:59:59' });
    $("#lowerFeeEndDate").datepicker({ dateFormat: 'yy-mm-dd 23:59:59' });
    $("#lastPaymentDate").datepicker({ dateFormat: 'yy-mm-dd 23:59:59' });

    $("#programsAll").click(function(){        
        var checked_status = this.checked;
        $("input.program").each(function(){
            this.checked = checked_status;
        });
    });

    $(".tooltip").tooltip({
        showURL: false,
        bodyHandler: function() {
            return $(this).next().html();
        }
    });
    
    $("#deletePeriod").click(function(){

        var msg = "This will delete the entire period configuration and cannot be undone."; 
        var agree = confirm(msg + " Are you sure you wish to continue?");
        
        if (agree) {
            return true;
        } else {
            return false;
        }
    });  
    
    
});