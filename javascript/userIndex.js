$(document).ready(function(){
    
    $('#suggest').autocomplete('../admin/userIndex.php', {
        extraParams: { index: $('#userIndexType').val() },
        formatItem: function(row, i, max) {
            return row[1] + ", " + row[2] + " (" + row[4] + ")";
        },
        formatResult: function(row) {
            return row[1] + ", " + row[2] + " (" + row[4] + ")";
        }
    });
    
    $("input#suggest").focus();

    $("input#suggest").result(function(event, data) {
        $("#searchUserId").val(data[0]);
    });
    
});