$(document).ready(function(){

	/*
    * Bindings to change a filter select's color when it's "on"
    */
    $(window).load( function() {
        var optionValue;
        var selectName;
        $('select.filter option:selected').each( function() {
            optionValue = $(this).text();
            selectName = $(this).parent().attr('name'); 
            if ( optionValue == 'No Filter') {
            } else {
                $('#' + selectName + 'Label').addClass('filterOn');   
            }    
        });
    });

    $('select.filter').change( function() {
        var optionValue = $('option:selected', this).text();
        var selectName = $(this).attr('name');
        if ( optionValue == 'No Filter') {
            $('#' + selectName + 'Label').removeClass('filterOn');
        } else {
            $('#' + selectName + 'Label').addClass('filterOn');
        }
    }); 
    
    
    /*
    * Function bindings to handle the "jump to applicant" autocomplete.
    * Added compbio kluge to handle multiple periods 10/13/09
    */
    var autocompleteExtraParams = {};
    var jumpToPeriodIds = $("input.jumpToPeriod");
    if (jumpToPeriodIds.size() > 1) {
        var periodIds = [];
        jumpToPeriodIds.each(function() {
            periodIds.push( $(this).attr('value') ); 
        });
        autocompleteExtraParams = {
            'unit': $("#jumpToUnit").attr("value"), 
            'unitId': $("#jumpToUnitId").attr("value"),
            'periodId[]': periodIds,
            'submitted': 1 
        };        
    } else {
        autocompleteExtraParams = {
            'unit': $("#jumpToUnit").attr("value"), 
            'unitId': $("#jumpToUnitId").attr("value"),
            'periodId': $("#jumpToPeriod").attr("value"),
            'submitted': 1 
        };    
    }

    
    $("input#suggest").autocomplete("administerRegistrationsAutocomplete.php", {
        extraParams: autocompleteExtraParams,
        formatItem: function(row, i, max) {
            //return row[4] + ", " + row[5] + " (" + row[7] + ")";
            return row[3] + ", " + row[4] + " (" + row[6] + ")";
        },
        formatResult: function(row) {
            //return row[4] + ", " + row[5] + " (" + row[7] + ")";
            return row[3] + ", " + row[4] + " (" + row[6] + ")";
        }
    });

    $('input#suggest').setOptions({
        max: 15
    });

    $('input#suggest').result(function(event, data) {
        //$("#applicationId").val(data[0]);
        $("#luuId").val(data[1]);
    });
     
    
    /*
	* The following binds, through delegation, menu links with the handleMenuClick
    * function to open a new row/div for an individual application record.
	*/
	$('table.datagrid').click( function(event) {
	
		// check if clicked link is a menu link
		var $target = $(event.target);
		
        if ( $target.is("a.menu") ) {
            
            // handle the click
            handleMenuClick($target);

        } else if ($target.is("div.close")) {
            
            var applicationId = $target.attr("id").split("_")[1];
            
            // remove the content row
            $('#contentTr_' + applicationId).remove(); 
            
            // remove the "selected" status of the menu tr and td
            $("td.selected").removeClass("selected");
            $("tr.selected").removeClass("selected");
        }
	});


	/*
	* The following bind form element click events with functions to update the db via ajax requests.
	* They depend on the livequery extension to bind events to elements
	* created/modified after the DOM is initially loaded.
	*/

    /*
    * Set/Unset Fee Waived
    */
    $("input.feeWaived", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var departmentId = idPieces[2];
        var applicantName = $('#applicantName_' + applicationId).text();
        
        var feeWaived = 0;
        var alertMessage = "Fee reinstated";
        var totalFees = $('#totalFees_' + applicationId).text();    
        if ( $(this).is(":checked") ) {
            feeWaived = 1;
            alertMessage = "Fee waived";
        }
        alertMessage = alertMessage + ' for ' + applicantName + '.';
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        var page = '';
        if  ( selectedContentDiv.is(":visible") ) {
            var page = $('#payment_' + applicationId).attr("id").split("_")[0];
        }
        
        var error = "Error: unable to waive fee for " + applicantName;
                
        $.get('administerRegistrationsSingleUpdate.php', { 
            mode: 'feeWaived', 
            applicationId: applicationId, 
            departmentId: departmentId, 
            feeWaived: feeWaived },
            function(data){
                if (page == 'payment') {
                    //$('#paymentMessage_' + applicationId).html(menuMessage);
                    reloadForm(applicationId, departmentId, page, selectedContentDiv);
                }                                
                if (data != -1) {
                    alert(alertMessage);    
                } else {
                    alert(error);    
                } 
            },
            'text');
    });

    /*
    // Set/Unset Payment Paid
    */
    $("input.paymentPaid", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var departmentId = idPieces[2];
        var paymentId = idPieces[3];
        var applicantName = $('#applicantName_' + applicationId).text();
        var paymentAmount = $('#paymentAmount_' + applicationId + '_' + paymentId).text();
        var paymentDate = $('#paymentDate_' + applicationId + '_' + paymentId).text();
        var paymentType = $('#paymentType_' + applicationId + '_' + paymentId).text();
        var systemConfirm = $('#systemConfirm_' + applicationId + '_' + paymentId).attr("value"); 
               
        var paymentPaid = 0;
        var message = paymentType + ' payment of ' + paymentAmount + ' on ' + paymentDate + ' by ' + applicantName;

        if ( $(this).is(":checked") ) {
            paymentPaid = 1;
            message = 'Confirmed Paid: ' + message;
        } else {
            message = 'Confirmed Unpaid: ' + message; 
        }
        
        var error = 'Error: unable to update ' + ' payment of ' + paymentAmount + ' on ' + paymentDate + ' by ' + applicantName;
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        $.get("administerRegistrationsSingleUpdate.php", { 
            mode: "paymentPaid", 
            applicationId: applicationId,
            departmentId: departmentId,
            paymentId: paymentId,
            systemConfirm: systemConfirm,
            paymentType: paymentType, 
            paymentPaid: paymentPaid},
            function(data){
                reloadForm(applicationId, departmentId, 'payment', selectedContentDiv);                
                if (data != -1) {
                    alert(message);    
                } else {
                    alert(error);    
                }          
            },
            "text");
    });      

    /*
    // Void Payment
    */
    $("input.voidPayment", "div.content").livequery("click" , function(event){
        
        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var departmentId = idPieces[2];
        var paymentId = idPieces[3];
        var applicantName = $('#applicantName_' + applicationId).text();
        var paymentAmount = $('#paymentAmount_' + applicationId + '_' + paymentId).text();
        var paymentDate = $('#paymentDate_' + applicationId + '_' + paymentId).text();
        var paymentType = $('#paymentType_' + applicationId + '_' + paymentId).text();
        
        var message = 'Void ' + paymentType + ' payment of ' + paymentAmount + ' on ' + paymentDate + '?';
        if ( !confirm(message) ) {
            return false;
        }

        var selectedContentDiv = $('#contentDiv_' + applicationId);
        $.get("administerRegistrationsSingleUpdate.php", { 
                mode: "paymentVoid", 
                applicationId: applicationId,
                departmentId: departmentId,
                paymentId: paymentId},
            function(data){
                reloadForm(applicationId, departmentId, 'payment', selectedContentDiv);         
            },
            "text");
    });

    /*
    // Add new payment
    */
    $("input.submitNewPayment", "div.content").livequery("click" , function(event){

        var idPieces = $(this).attr("id").split("_");
        var applicationId = idPieces[1];
        var departmentId = idPieces[2];
        var paymentType = 2;
        if ( $('#newPaymentType_' + applicationId + '_1').is(":checked") ) {
            paymentType = 1;
        }
        if ( $('#newPaymentType_' + applicationId + '_3').is(":checked") ) {
            paymentType = 3;
        }
        var newPaymentAmount = $('#newPaymentAmount_' + applicationId).attr('value');
        
        var selectedContentDiv = $('#contentDiv_' + applicationId);
        $.get("administerRegistrationsSingleUpdate.php", { 
            mode: "newPayment", 
            applicationId: applicationId,
            departmentId: departmentId,
            paymentType: paymentType, 
            paymentAmount: newPaymentAmount,
            paymentPaid: 1},
            function(data){
                reloadForm(applicationId, departmentId, 'payment', selectedContentDiv);        
            },
            "text");

         return false;
         
    });
     
}); // close ($document).ready

/*
* Main function for opening a new row/div for an application record
* and switching the view in it.
*/
function handleMenuClick(linkObject) {

	if ( $(linkObject).hasClass("print") ) {
	    // open a new window, don't open content div 
		var idArray = $(linkObject).attr("id").split("_"); 
        var userId = idArray[1];
        var applicationId = idArray[2];
        window.open("../review/userroleEdit_student_print.php?id="+userId+"&applicationId="+applicationId);
		return false;
	}
    
    if ( $(linkObject).hasClass("login") ) {
        // follow the link, don't open the content div
        return false;
    } 

    // Otherwise, open the content div
    
    // set some variables for testing
    var selectedLinkTd = $(linkObject).parent("td");
    var selectedLinkTr = $(selectedLinkTd).parent("tr");
    
    // Get the number of columns in the datagrid
    var colspan = selectedLinkTr.children().length;
    
    // get the page and application id from the element id, e.g. payment_193
    var request_id = $(linkObject).attr("id");
    var requestArguments = request_id.split("_");
    var page = requestArguments[0];
    var applicationId = requestArguments[1];
    var departmentId = requestArguments[2];
    var unit = $('#filterUnit').val();
    var unitId = $('#filterUnitId').val();

    // if "closing" an open tab/section and not opening a new one 
    if ( selectedLinkTd.hasClass("selected") ) {
        
        // remove the content row
        var selectedContentTr = $('#contentTr_' + applicationId);
        selectedContentTr.remove(); 
        
        // remove the "selected" status of the menu tr and td
        selectedLinkTd.removeClass("selected");
        selectedLinkTr.removeClass("selected");
     
    } else {
    
        // opening new record or changing view in same record
                    
        // turn off style to "close" open tabs
        $("tr.menu td.selected").removeClass("selected");

        // if selecting different section of same record
        if ( selectedLinkTr.hasClass("selected") ) {
        
            // identify content tr of interest
            var selectedContentTr = $('#contentTr_' + applicationId);
        
            // identify the content div of interest
            var selectedContentDiv = $('#contentDiv_' + applicationId); 
        
            // empty the content div
            selectedContentDiv.html("");
        
        } else {
        
            // opening a new record

            var oldMenuRow = $("tr.selected", "table.datagrid");

            // remove the old content row
            oldMenuRow.next().remove();
            
            // de-select old menu row
            oldMenuRow.removeClass("selected");
            
            // add a row/div beneath the present one
            var bgcolor = $(selectedLinkTr).attr("bgcolor");
            var newRow = '<tr bgcolor="' + bgcolor + '" id="contentTr_' + applicationId + '">';
            newRow += '<td colspan="' +  colspan + '" class="content">';
            newRow += '<div class="closeBar"><div id="close_' + applicationId + '" class="close">X Close</div></div>';
            newRow += '<div class="content" id="contentDiv_' + applicationId + '">';
            newRow += '<div id="loading">LOADING</div>';
            newRow += '</div></td></tr>';
            selectedLinkTr.after(newRow);
            
            // add id and select the new row 
            selectedLinkTr.addClass("selected");
            
            // set content variables based on new row
            var selectedContentDiv = $('#contentDiv_' + applicationId); 
        
        } 

        // highlight the selected tab/link            
        selectedLinkTd.addClass("selected");

        // show the content div
        selectedContentDiv.show();
        var loading = $("#loading");
        loading.show();
    
        // submit the request
        if (page == 'ccPaymentDetails') {
            
            var paymentId = applicationId;
            
            $.ajax({
                method: 'get',
                url: 'getCcPaymentDetails.php',
                data: 'paymentId=' + paymentId,
                success: function(html){ //so, if data is retrieved, store it in html
                    loading.css("display", "none");
                    selectedContentDiv.html(html);
                    } // close success
            }); //close $.ajax(             
            
        } else if (page == 'ccPaymentSummary') {
            
            var paymentId = applicationId;
            
            $.ajax({
                method: 'get',
                url: 'getCcPaymentSummary.php',
                data: 'paymentId=' + paymentId,
                success: function(html){ //so, if data is retrieved, store it in html
                    loading.css("display", "none");
                    selectedContentDiv.html(html);
                    } // close success
            }); //close $.ajax(             
            
        } else {
            
            $.ajax({
                method: 'get',
                url: 'administerRegistrationsSingle.php',
                data: 'applicationId=' + applicationId + '&departmentId= ' + departmentId + '&page=' + page + '&unit=' + unit + '&unitId=' + unitId,
                success: function(html){ //so, if data is retrieved, store it in html
                    loading.css("display", "none");
                    selectedContentDiv.html(html);
                    } // close success
            }); //close $.ajax(                   
        }

    } // close close/open div if/else
    
    return false;
}

/* 
* Function to reload a form/div that has just been updated. 
*/
function reloadForm(applicationId, departmentId, page, selectedContentDiv) {

        $.ajax({
            method: "get", 
            url: "administerRegistrationsSingle.php", 
            data: "applicationId=" + applicationId + "&departmentId=" + departmentId + "&page=" + page,
            success: function(html){ 
                selectedContentDiv.html(html);
            },
            error: function() {
                selectedContentDiv.html("error");
            }
        }); 
}

/*
* Function for restoring the default filter/list.
*/
function restoreDefaultFilter() {
    document.filterForm.paymentStatus.value = 'allPayment';
    document.filterForm.submit();
    return false;
}

/*
* Function for showing the full list with no filter.
*/
function setNoFilter() {
    document.filterForm.paymentStatus.value = 'allPayment';
    document.filterForm.submit();
    return false;
}